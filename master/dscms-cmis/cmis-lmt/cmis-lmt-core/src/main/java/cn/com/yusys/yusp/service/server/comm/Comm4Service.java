package cn.com.yusys.yusp.service.server.comm;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmPropQueryDto;
import cn.com.yusys.yusp.dto.ApprLmtChgFiledListDto;
import cn.com.yusys.yusp.dto.server.cmiscus0005.req.CmisCus0005ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0005.resp.CmisCus0005RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0023.req.CmisCus0023ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0023.resp.CmisCus0023RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0047.req.CmisLmt0047LmtSubDtoList;
import cn.com.yusys.yusp.dto.server.cmislmt0047.req.CmisLmt0047ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0047.resp.CmisLmt0047ContRelDtoList;
import cn.com.yusys.yusp.dto.server.cmislmt0047.resp.CmisLmt0047RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.server.cmiscfg0003.req.CmisCfg0003ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0003.resp.CmisCfg0003RespDto;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.server.cmislmt0047.CmisLmt0047Service;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.util.*;

@Service
public class Comm4Service {

    private static final Logger logger = LoggerFactory.getLogger(Comm4Service.class);

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService;

    @Autowired
    private ApprCoopSubInfoService apprCoopSubInfoService ;

    @Autowired
    private ApprCoopInfoService apprCoopInfoService ;

    @Autowired
    private LmtContRelService lmtContRelService ;

    @Autowired
    private ApprLmtSubBasicInfoMapper apprLmtSubBasicInfoMapper ;

    @Autowired
    private LmtWhiteInfoService lmtWhiteInfoService ;

    @Autowired
    private LmtWhiteInfoMapper lmtWhiteInfoMapper ;

    @Autowired
    private LmtContRelMapper lmtContRelMapper ;

    @Autowired
    private LmtSubPrdMappConfService lmtSubPrdMappConfService ;

    @Autowired
    private ApprStrMtableInfoService apprStrMtableInfoService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient ;

    @Autowired
    private AdminSmPropService adminSmPropService ;

    @Autowired
    private ContAccRelService contAccRelService;

    @Autowired
    private ApprLmtChgDetailService apprLmtChgDetailService ;

    @Autowired
    private CmisCusClientService cmisCusClientService ;

    @Autowired
    private DscmsCfgQtClientService dscmsCfgQtClientService ;

    @Autowired
    private ApprStrOrgInfoService apprStrOrgInfoService ;

    @Autowired
    private LmtDiscOrgService lmtDiscOrgService ;

    @Autowired
    private CusGrpMemberRelMapper cusGrpMemberRelMapper ;

    @Autowired
    private Comm4LmtCalFormulaUtils comm4LmtCalFormulaUtils ;

    @Autowired
    private CmisLmt0047Service cmisLmt0047Service ;

    /**
     * @方法名称: checkLimit
     * @方法描述: 额度校验，
     * @参数与返回说明: 校验本次合同总额，是否超高分项敞口金额
     * @算法描述: 批复结构：
     * 批复主信息->
     * 额度分项 ->
     * 产品分项
     * 如果是产品分项，则先判断产品分项是否足额，判断通过则根据上级目录获取到额度分项，根据判断额度分项是否足额
     * 批复主信息->额度分项  1 ： N
     * 额度分项->产品分项  1:N
     */
    public void checkSpacLimit(ApprLmtSubBasicInfo apprLmtSubBasicInfo, BigDecimal bizSpacAmtCny, Map<String, String> paramMap) {
        logger.info("额度校验处理");
        //分项已用敞口金额
        BigDecimal spacOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getSpacOutstndAmt());
        BigDecimal spacAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getSpacAmt()) ;
        //批复分项编号
        String apprSubSerno = apprLmtSubBasicInfo.getApprSubSerno();
        //是否无缝衔接
        String isFollowBiz = paramMap.get("isFollowBiz") ;
        //是否合同重签
        String isBizRev = paramMap.get("isBizRev") ;
        //原交易业务编号
        String origiDealBizNo = paramMap.get("origiDealBizNo") ;
        //原交易业务恢复类型
        String origiRecoverType = paramMap.get("origiRecoverType") ;

        //交易编号
        String dealBizNo = paramMap.get("dealBizNo") ;
        //系统编号
        String sysId = paramMap.get("sysId") ;
        //所属链条
        String belgLine = paramMap.get("belgLine") ;
        //交易属性 1-合同 2-台账
        String bizAttr = paramMap.get("bizAttr") ;
        //可出账金额
        BigDecimal avlOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getAvlOutstndAmt()) ;

        BigDecimal recoverBizSpacBalanceAmtCny = BigDecimal.ZERO ;
        BigDecimal recoverBizTotalBalanceAmtCny = BigDecimal.ZERO ;

        //判断该交易是否已经成功
        if(judgeLmtDealIsSuss(apprSubSerno, dealBizNo, sysId)){
            //判断该交易是否已经成功，如果已经成功，进行粗呢撤销操作
            //获取原交易合同信息
            LmtContRel lmtContRel = lmtContRelService.selectLCRelByDBizNoAngSubNo(dealBizNo, apprSubSerno, sysId) ;
            if(lmtContRel !=null){
                //分项敞口已用余额 = 原交易敞口余额+分项敞口可用余额  TODO:这里是不是应该减啊？
                recoverBizSpacBalanceAmtCny = recoverBizSpacBalanceAmtCny.add(BigDecimalUtil.replaceNull(lmtContRel.getBizSpacBalanceAmtCny())) ;
                spacOutstndAmt = spacOutstndAmt.subtract(BigDecimalUtil.replaceNull(lmtContRel.getBizSpacBalanceAmtCny())) ;
                logger.info("额度分项编号【"+apprSubSerno+"】,交易已成功，撤销恢复金额:"+lmtContRel.getBizSpacBalanceAmtCny()+";分项敞口已用金额【spacOutstndAmt】:"+spacOutstndAmt);
                if(CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
                    BigDecimal bizTotalBalanceAmtCny =  BigDecimalUtil.replaceNull(lmtContRel.getBizTotalBalanceAmtCny()) ;
                    recoverBizTotalBalanceAmtCny = recoverBizTotalBalanceAmtCny.add(bizTotalBalanceAmtCny) ;
                    avlOutstndAmt = avlOutstndAmt.add(bizTotalBalanceAmtCny) ;
                }
            }
        }

        //合同重签，判断是否恢复额度，是的话，按照合同金额进行恢复额度。
        if(CmisLmtConstants.YES_NO_Y.equals(isBizRev)){
            logger.info("合同重签处理开始");
            //获取原交易合同信息
            LmtContRel lmtContRel = lmtContRelService.selectLCRelByDBizNoAngSubNo(origiDealBizNo, apprSubSerno, sysId) ;
            if(Objects.nonNull(lmtContRel)){
                //是否可恢复
               /* if(CmisLmtConstants.YES_NO_Y.equals(apprLmtSubBasicInfo.getIsRevolv())){
                    Map<String, BigDecimal> amtMap = contAccRelService.getContAccRelBal(origiDealBizNo,CmisLmtConstants.STD_ZB_BIZ_STATUS_STR) ;
                    BigDecimal accAlance = amtMap.get("accalance") ;
                    BigDecimal accSpacAlance = amtMap.get("accspacalance") ;

                    BigDecimal recoverSpacLmtAmt = BigDecimalUtil.replaceNull(lmtContRel.getBizSpacBalanceAmtCny()).subtract(accSpacAlance) ;
                    BigDecimal recoverLmtAmt = BigDecimalUtil.replaceNull(lmtContRel.getBizTotalBalanceAmtCny()).subtract(accAlance) ;

                    recoverBizSpacBalanceAmtCny = recoverBizSpacBalanceAmtCny.add(recoverSpacLmtAmt) ;
                    //分项敞口已用余额 = 原交易敞口余额+分项敞口可用余额
                    spacOutstndAmt = spacOutstndAmt.subtract(recoverLmtAmt) ;

                    logger.info("额度分项编号【"+apprSubSerno+"】,合同重签，撤销恢复金额:"+lmtContRel.getBizSpacBalanceAmtCny()+";分项敞口已用金额【spacOutstndAmt】:"+spacOutstndAmt);
                    if(CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
                        recoverBizTotalBalanceAmtCny = recoverBizTotalBalanceAmtCny.add(recoverSpacLmtAmt) ;
                        avlOutstndAmt = avlOutstndAmt.add(recoverLmtAmt) ;
                    }
                }*/
                //update by 20210929 合同重签类合同结清，原业务不挂到新合同下，只有循环额度以及最高额协议才会做重签，所以根据合同余额恢复已用额度，已用敞口额度
                BigDecimal recoverSpacLmtAmt = BigDecimalUtil.replaceNull(lmtContRel.getBizSpacBalanceAmtCny()) ;
                BigDecimal recoverLmtAmt = BigDecimalUtil.replaceNull(lmtContRel.getBizTotalBalanceAmtCny()) ;
                recoverBizSpacBalanceAmtCny = recoverBizSpacBalanceAmtCny.add(recoverSpacLmtAmt) ;
                //分项敞口已用余额 = 原交易敞口余额+分项敞口可用余额
                spacOutstndAmt = spacOutstndAmt.subtract(recoverLmtAmt) ;
                logger.info("额度分项编号【"+apprSubSerno+"】,合同重签，撤销恢复敞口金额:"+recoverSpacLmtAmt+";分项敞口可用金额【spacOutstndAmt】:"+spacOutstndAmt);
            }
            logger.info("合同重签处理结束");
        }

        //无缝衔接，判断原交易业务恢复类型，撤销恢复，恢复分项金额；如果是结清恢复判断分项是否可循环，恢复原交易合同余额。不可循环不回复分项。
        if(CmisLmtConstants.YES_NO_Y.equals(isFollowBiz)){
            //获取原交易合同信息
            LmtContRel lmtContRel = lmtContRelService.selectLCRelByDBizNoAngSubNo(origiDealBizNo, apprSubSerno, sysId) ;
            if(lmtContRel !=null){
                String dealBizType = lmtContRel.getDealBizType() ;
                String bizStatus = lmtContRel.getBizStatus() ;
                //结清恢复
                if(CmisLmtConstants.STD_RECOVER_TYPE_01.equals(origiRecoverType)){
                    //额度可循环
                    if(CmisLmtConstants.YES_NO_Y.equals(apprLmtSubBasicInfo.getIsRevolv())){
                        //是否可恢复
                        if(checkRecoverLmtApprBal(dealBizType, bizStatus)){
                            recoverBizSpacBalanceAmtCny = recoverBizSpacBalanceAmtCny.add(BigDecimalUtil.replaceNull(lmtContRel.getBizSpacBalanceAmtCny())) ;
                            //分项敞口已用余额 = 原交易敞口余额+分项敞口可用余额
                            spacOutstndAmt = spacOutstndAmt.subtract(lmtContRel.getBizSpacBalanceAmtCny()) ;
                            logger.info("额度分项编号【"+apprSubSerno+"】,无缝衔接，撤销恢复金额:"+lmtContRel.getBizSpacBalanceAmtCny()+";分项敞口已用金额【spacOutstndAmt】:"+spacOutstndAmt);
                            if(CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
                                recoverBizTotalBalanceAmtCny = recoverBizTotalBalanceAmtCny.add(BigDecimalUtil.replaceNull(lmtContRel.getBizTotalBalanceAmtCny())) ;
                                avlOutstndAmt = avlOutstndAmt.add(BigDecimalUtil.replaceNull(lmtContRel.getBizTotalBalanceAmtCny())) ;
                            }
                        }
                    }
                }else if(CmisLmtConstants.STD_RECOVER_TYPE_06.equals(origiRecoverType)){
                    recoverBizSpacBalanceAmtCny = recoverBizSpacBalanceAmtCny.add(BigDecimalUtil.replaceNull(lmtContRel.getBizSpacBalanceAmtCny())) ;
                    //分项敞口已用余额 = 原交易敞口余额+分项敞口可用余额
                    spacOutstndAmt = spacOutstndAmt.subtract(lmtContRel.getBizSpacBalanceAmtCny()) ;
                    logger.info("额度分项编号【"+apprSubSerno+"】,无缝衔接，撤销恢复金额:"+lmtContRel.getBizSpacBalanceAmtCny()+";分项敞口已用金额【spacOutstndAmt】:"+spacOutstndAmt);
                    if(CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
                        recoverBizTotalBalanceAmtCny = recoverBizTotalBalanceAmtCny.add(BigDecimalUtil.replaceNull(lmtContRel.getBizTotalBalanceAmtCny())) ;
                        avlOutstndAmt = avlOutstndAmt.add(BigDecimalUtil.replaceNull(lmtContRel.getBizTotalBalanceAmtCny())) ;
                    }
                }
            }
        }

        /**交易属性 1-合同 2-台账
         * 此接口调用一般是 合同占用分项信息，但是针对代开信用证和代开保函的业务，是只会存在台账占用分项信息，缺少合同环节，所以此处送台账时
         * 需考虑合同占用时的相关信息
         * 如果是台账占用，校验本次占用金额<= 可出账金额
         */
        if(CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
            if(avlOutstndAmt.compareTo(bizSpacAmtCny)<0){
                logger.info("cmislmt0009 额度分项编号【"+apprSubSerno+"】，可出账金额【avlOutstndAmt】:"+avlOutstndAmt+"小于本次占用金额【bizTotalAmt】:"+bizSpacAmtCny);
                throw new YuspException(EclEnum.ECL070071.key, EclEnum.ECL070071.value);
            }
        }

        if ((spacOutstndAmt.add(bizSpacAmtCny).compareTo(spacAmt) > 0)) {
            logger.info("额度分项编号【"+apprSubSerno+"】,分项敞口已用金额【spacOutstndAmt】:"+spacOutstndAmt+"+;本次占用敞口金额【bizSpacAmtCny】:"+bizSpacAmtCny+"大于敞口金额【spacAmt】"+spacAmt);
            throw new YuspException(EclEnum.ECL070006.key, EclEnum.ECL070006.value);
        }
        //获取父级编号
        String lmtSubNo = apprLmtSubBasicInfo.getParentId();
        if(StringUtils.isNotEmpty(lmtSubNo)){
            apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(lmtSubNo);
            logger.info("校验父级编号，分项信息{}", JSON.toJSONString(apprLmtSubBasicInfo)) ;
            //检验 分项批复信息不允许为空
            if(Objects.nonNull(apprLmtSubBasicInfo)){
                avlOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getAvlOutstndAmt()).add(recoverBizTotalBalanceAmtCny) ;
                spacOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getSpacOutstndAmt()).subtract(recoverBizSpacBalanceAmtCny) ;
                spacAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getSpacAmt()) ;
                /**交易属性 1-合同 2-台账
                 * 此接口调用一般是 合同占用分项信息，但是针对代开信用证和代开保函的业务，是只会存在台账占用分项信息，缺少合同环节，所以此处送台账时
                 * 需考虑合同占用时的相关信息
                 * 如果是台账占用，校验本次占用金额<= 可出账金额
                 */
                if(CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
                    if(avlOutstndAmt.compareTo(bizSpacAmtCny)<0){
                        logger.info("cmislmt0009 额度分项编号【"+apprLmtSubBasicInfo.getApprSubSerno()+"】，可出账金额【avlOutstndAmt】:"+avlOutstndAmt+"小于本次占用金额【bizTotalAmt】:"+bizSpacAmtCny);
                        throw new YuspException(EclEnum.ECL070071.key, EclEnum.ECL070071.value);
                    }
                }

                if ((spacOutstndAmt.add(bizSpacAmtCny).compareTo(spacAmt) > 0)) {
                    logger.info("额度分项编号【"+apprLmtSubBasicInfo.getApprSubSerno()+"】,分项敞口已用金额1【spacOutstndAmt】:"+spacOutstndAmt+"+;本次占用敞口金额【bizSpacAmtCny】:"+bizSpacAmtCny+"大于敞口金额【spacAmt】"+spacAmt);
                    throw new YuspException(EclEnum.ECL070006.key, EclEnum.ECL070006.value);
                }
            }
        }
    }

    /**
     * @方法名称: checkLimitTotal
     * @方法描述: 为防止其中一个分项放款后压降，导致总额度小于合同本次占用金额+已用金额
     * 例如：批复下存在两个分项：
     * A 敞口授信100万   合同占用70 万  放款 70 万
     * B 敞口授信20 万   合同占用 0      放款 0
     * 对A进行压降处理：
     * A 敞口授信 50 万  合同占用70万   放款70 万
     * 针对B进场合同签订20 万：  分项层级的占用足额
     * 授信总额为70万  ，合同占用已经是70 万 不足额 校验不通过
     * 当A 授信下金额还款，额度释放出20 万后，B方可签订合同，检验通过
     * @参数与返回说明: [prdNo]产品编号； 最高额授信协议 返回true ； 非最高额授信协议返回：false
     * @算法描述: 判断当前传入的分项批复是产品分项还是额度分项
     * 产品分项： 产品分项需要获取父级额度分项，根据额度分项获取上级批复编号，根据批复编号，查询该批复下额度分项信息，
     * 汇总批复下额度分项信息中的已用金额，以授信总额
     * 额度分项： 根据额度分项获取父级目录批复编号，根据批复编号查询批复分项汇总已用金额和授信总额
     * 判断：本次授信总额+已用总额 <= 批复分项汇总总额
     */
    public void checkLimitTotal(String lmtSubNo, BigDecimal bizTotalAmt, Map<String, String> paramMap) {
        String parentId = apprLmtSubBasicInfoService.selectApprNoBySubNo(lmtSubNo) ;
        //获取汇总金额
        Map<String, BigDecimal> amtMap = apprLmtSubBasicInfoService.selectLmtSubTotal(parentId);
        if(amtMap!=null){
            //判断本次授信总额+已用总额 是否小于批复分项汇总
            BigDecimal outstndAmt = amtMap.get("outstndAmt");
            BigDecimal avlAmt = amtMap.get("avlAmt");
            //系统编号
            String sysId = paramMap.get("sysId") ;
            //交易编号
            String dealBizNo = paramMap.get("dealBizNo") ;

            //判断该交易是否已经成功
            if(judgeLmtDealIsSuss(lmtSubNo, dealBizNo, sysId)){
                //判断该交易是否已经成功，如果已经成功，进行粗呢撤销操作
                //获取原交易合同信息
                LmtContRel lmtContRel = lmtContRelService.selectLCRelByDBizNoAngSubNo(dealBizNo, lmtSubNo, sysId) ;
                if(Objects.nonNull(lmtContRel)){
                    //分项敞口已用余额 = 原交易敞口余额+分项敞口可用余额  TODO:这里是不是应该减啊？
                    outstndAmt = outstndAmt.subtract(BigDecimalUtil.replaceNull(lmtContRel.getBizTotalBalanceAmtCny())) ;
                    logger.info("批复分项编号【"+lmtSubNo+"】,交易流水号【"+dealBizNo+"】，已交易成功，撤销占用:"+lmtContRel.getBizTotalBalanceAmtCny()+";分项已用总金额【outstndAmt】:"+outstndAmt);
                }
            }

            //是否无缝衔接
            String isFollowBiz = paramMap.get("isFollowBiz") ;
            //原交易业务编号
            String origiDealBizNo = paramMap.get("origiDealBizNo") ;
            //原交易业务恢复类型
            String origiRecoverType = paramMap.get("origiRecoverType") ;
            String isBizRev = paramMap.get("isBizRev") ;
            //合同重签，判断是否恢复额度，是的话，按照合同金额进行恢复额度。
            if(CmisLmtConstants.YES_NO_Y.equals(isBizRev)){
                //获取原交易合同信息
                LmtContRel lmtContRel = lmtContRelService.selectLCRelByDBizNoAngSubNo(origiDealBizNo, lmtSubNo, sysId) ;
                if(Objects.nonNull(lmtContRel)){
                    BigDecimal recoverLmtAmt = BigDecimalUtil.replaceNull(lmtContRel.getBizTotalBalanceAmtCny()) ;
                    outstndAmt = outstndAmt.add(recoverLmtAmt) ;
                }
            }

            //是无缝衔接，判断原交易业务恢复类型，撤销恢复，恢复分项金额；如果是结清恢复判断分项是否可循环，恢复原交易合同余额。不可循环不恢复分项。
            if(CmisLmtConstants.YES_NO_Y.equals(isFollowBiz)){
                //获取原交易合同信息
                LmtContRel lmtContRel = lmtContRelService.selectLCRelByDBizNoAngSubNo(origiDealBizNo, lmtSubNo, sysId) ;
                //获取分项信息
                if(lmtContRel !=null){
                    String dealBizType = lmtContRel.getDealBizType() ;
                    String bizStatus = lmtContRel.getBizStatus() ;
                    ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(lmtContRel.getLimitSubNo()) ;
                    //结清恢复
                    if(CmisLmtConstants.STD_RECOVER_TYPE_01.equals(origiRecoverType)){
                        //额度可循环
                        if(CmisLmtConstants.YES_NO_Y.equals(apprLmtSubBasicInfo.getIsRevolv())){
                            //需恢复额度
                            if(checkRecoverLmtApprBal(dealBizType, bizStatus)){
                                //已用总额 = 已用总额 - 本次恢复金额
                                outstndAmt = outstndAmt.subtract(lmtContRel.getBizTotalBalanceAmtCny()) ;
                                logger.info("批复台账编号【"+parentId+"】,无缝衔接，撤销恢复金额:"+lmtContRel.getBizTotalBalanceAmtCny()+";分项已用总金额【outstndAmt】:"+outstndAmt);
                            }
                        }
                    }else if(CmisLmtConstants.STD_RECOVER_TYPE_06.equals(origiRecoverType)){
                        //已用总额 = 已用总额 - 本次恢复金额
                        outstndAmt = outstndAmt.subtract(lmtContRel.getBizTotalBalanceAmtCny()) ;
                        logger.info("批复台账编号【"+parentId+"】,无缝衔接，撤销恢复金额:"+lmtContRel.getBizTotalBalanceAmtCny()+";分项已用总金额【outstndAmt】:"+outstndAmt);
                    }
                }
            }

            if ((bizTotalAmt.add(outstndAmt)).compareTo(avlAmt) > 0) {
                logger.info("批复台账编号【"+parentId+"】,分项已用总金额【avlOutstndAmt】:"+outstndAmt+"+;本次占用总金额【bizTotalAmt】:"+bizTotalAmt+"大于总金额【avlAmt】"+avlAmt);
                throw new YuspException(EclEnum.ECL070006.key, EclEnum.ECL070006.value);
            }
        }
    }

    /**
     * @方法名称: getEndDateAddGaraperDate
     * @方法描述: 获取到期日+宽限期后的日期
     * @参数与返回说明: 校验不通过，报错到上级处理
     * @算法描述: 无
     */
    public String getEndDateAddGaraperDate(String endDate, int lmtGraper) {
        return DateUtils.addMonth(endDate, "yyyy-MM-dd", lmtGraper);
    }

    /**
     * @方法名称: checkLmtEndDate
     * @方法描述: 校验到期日
     * @参数与返回说明: contEndDate 是否大于 endDate
     * @算法描述: 大于返回true　小于等于返回　false
     */
    public boolean checkLmtEndDate(String contEndDate, String endDate) {
        if(contEndDate.compareTo(endDate) > 0){
            return true;
        }
        return false;
    }

    /**
     * @方法名称: validCoopLmtType
     * @方法描述: 校验合作方授信信息 校验规则
     *    合作方额度结构：
     *          合作方额度主信息->
     *                  合作方额度分项信息
     *          1、额度校验：主信息额度校验：
     *                授信总额：本次总额+分项已用金额<= 授信总额
     *                单户限额：该客户合同占用总额LMT_CONT_REL 中占用总金额+本次占用 <= 单户限额
     *                单笔业务限额： 本次占用总额<= 单笔业务限额
     *            分项额度校验：
     *                本次占用总额+已用金额<= 分项授信总额
     *          2、合作方额度状态不为“生效”不允许占用
     *          3、用信到期日超过额度到期日不允许占用（该规则删除）
     * @参数与返回说明: 校验不通过，报错到上级处理
     * @算法描述: 授信总额校验：
     *                根据当前分项信息，查询该合作方所有分项信息，本次总额+分项已用金额<= 授信总额
     *            单户限额：
     *                根据分项，获取客户号，分项编号，通过客户号分项编号查询该客户分项占用关系信息（LMT_CONT_REL） 汇总占用总余额,占用总金额+本次占用 <= 单户限额
     */
    public void validCoopLmtType(CmisLmt0009OccRelListReqDto occSubRel, String finalExptType, CmisLmt0009ReqDto reqDto){
        logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.value + "：合作方额度校验开始【{}】", JSON.toJSONString(occSubRel));

        //客户号
        String cusId = reqDto.getCusId() ;
        //是否专业担保公司
        String isProGuarCom = occSubRel.getIsProGuarCom();
        //专业担保公司客户号
        String guarCusid = occSubRel.getGuarCusid();
        //分项编号
        String lmtSubNo = occSubRel.getLmtSubNo() ;
        //产品编号
        String prdId = reqDto.getPrdId() ;
        //用信品种类型属性
        String prdTypeProp =  occSubRel.getPrdTypeProp() ;
        //额度分项，查询合作方分项信息
        ApprCoopSubInfo apprCoopSubInfo = null ;
        if(CmisLmtConstants.YES_NO_Y.equals(isProGuarCom)){
            //add by 20211006 防止担保公司重复占用，eg:推送占用列表中已经存在该担保公司的记录，又推送了记录时担保公司的信息，重复，此处只占用一次
            if(check09ProGuarComRev(reqDto.getCmisLmt0009OccRelListReqDtoList(), guarCusid)){
                return ;
            }
            apprCoopSubInfo = apprCoopSubInfoService.selectSubInfoByCusIdAndPrdId(prdTypeProp, guarCusid) ;
        }else{
            apprCoopSubInfo = apprCoopSubInfoService.selectAcsiInfoBySubSerno(lmtSubNo) ;
        }
        //检验 分项批复信息不允许未空
        Optional.ofNullable(apprCoopSubInfo).orElseThrow(()->new YuspException(EclEnum.ECL070011.key, EclEnum.ECL070011.value)) ;

        //额度状态 额度状态只有正常的情况才允许占用
        if(!CmisLmtConstants.STD_ZB_APPR_ST_01.equals(apprCoopSubInfo.getStatus())){
            throw new YuspException(EclEnum.ECL070003.key, "合作方"+ EclEnum.ECL070003.value);
        }

        //合作方额度主信息校验 serno 批复台账编号
        String apprSerno = apprCoopSubInfo.getApprSerno() ;
        //根据批复台账编号，获取合作方批复台账信息
        ApprCoopInfo apprCoopInfo = apprCoopInfoService.selectACIBySerno(apprSerno) ;
        //检验 分项批复信息不允许未空
        Optional.ofNullable(apprCoopInfo).orElseThrow(()->new YuspException(EclEnum.ECL070012.key, "合作方" + EclEnum.ECL070012.value)) ;

        //校验适用机构如不是全行性适用，当前机构是否在适用机构中
        String isWholeBankSuit = apprCoopInfo.getIsWholeBankSuit() ;
        if(CmisLmtConstants.YES_NO_N.equals(isWholeBankSuit)){
            String accNo = apprCoopInfo.getApprSerno() ;
            String inputBrId = reqDto.getInputBrId() ;
            //根据当前机构和合作方台账编号查询适用机构
            List<ApprStrOrgInfo> apprStrOrgInfo = apprStrOrgInfoService.selectByAccNoAndOrg(inputBrId, accNo) ;
            if(CollectionUtils.isEmpty(apprStrOrgInfo)){
                throw new YuspException(EclEnum.ECL070139.key,"合作方"+ EclEnum.ECL070139.value);
            }
        }

        //是否无缝衔接
        String isFollowBiz = reqDto.getIsFollowBiz() ;
        //原交易业务编号
        String origiDealBizNo = reqDto.getOrigiDealBizNo() ;
        //校验分项额度信息  授信金额
        BigDecimal avlAmt = BigDecimalUtil.replaceNull(apprCoopSubInfo.getAvlAmt()) ;
        //已用金额
        BigDecimal outstndAmt = BigDecimalUtil.replaceNull(apprCoopSubInfo.getOutstndAmt()) ;
        //获取 批复授信总额
        BigDecimal lmtAmt = BigDecimalUtil.replaceNull(apprCoopInfo.getLmtAmt()) ;
        //获取批复分项单笔业务限额
        BigDecimal sigBussAmt = BigDecimalUtil.replaceNull(apprCoopSubInfo.getSigBussAmt()) ;

        BigDecimal recoverAmt = BigDecimal.ZERO ;
        //判断是否无缝衔接
        if(CmisLmtConstants.YES_NO_Y.equals(isFollowBiz)){
            //根据原业务编号，获取原合同信息
            ApprCoopSubInfo oriApprCoopSubInfo = apprCoopSubInfoService.selectAcsiInfoBySubSerno(origiDealBizNo) ;
            recoverAmt = BigDecimalUtil.replaceNull(oriApprCoopSubInfo.getOutstndAmt()) ;
            //合作方占用，无论是结清恢复还是撤销恢复，都恢复分项金额
            outstndAmt = outstndAmt.add(recoverAmt) ;
            logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key+":"+DscmsLmtEnum.TRADE_CODE_CMISLMT0009.value+":" +
                    "无缝衔接,已用金额 【outstndAmt】"+outstndAmt);
        }

        String dealBizNo = reqDto.getDealBizNo() ;
        String apprSubSerno = apprCoopSubInfo.getApprSubSerno() ;
        String sysId = reqDto.getSysId() ;

        //判断该合同是成功
        if(judgeLmtDealIsSuss(apprSubSerno, dealBizNo, sysId)){
            //判断该交易是否已经成功，如果已经成功，进行粗呢撤销操作
            //获取原交易合同信息
            LmtContRel lmtContRel = lmtContRelService.selectLCRelByDBizNoAngSubNo(dealBizNo, apprSubSerno, sysId) ;
            if(lmtContRel !=null){
                //分项敞口已用余额 = 原交易敞口余额+分项敞口可用余额
                recoverAmt = BigDecimalUtil.replaceNull(lmtContRel.getBizTotalBalanceAmtCny()) ;
                outstndAmt = outstndAmt.subtract(recoverAmt) ;
                logger.info("额度分项编号【"+apprSubSerno+"】,无缝衔接，撤销恢复金额:"+lmtContRel.getBizSpacBalanceAmtCny()+";" +
                        "分项敞口已用金额【outstndAmt】:"+outstndAmt);
            }
        }

        /** 张家港市农业融资担保有限公司（客户号：8001092178，专业担保公司），
         * 信保贷产品使用该合作方担保额度时不做超额校验控制 ，出账时不做超额校验控制
         * 江苏扬帆广睿国际贸易有限公司（客户号：8000749823，集群贷市场方），合同占用合作方额度是，
         * 不做超额校验控制 ，出账时做超额校验控制**/
        if (checkSpecialCoopCus(apprCoopInfo.getCusId(), prdTypeProp, apprCoopSubInfo.getLimitSubNo())) return;

        /**********限额校验开始*********/
        //合同占用总额+已用金额>分项总额 校验不通过
        if((outstndAmt.add(occSubRel.getBizTotalAmt()).compareTo(avlAmt)>0)){
            logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key+":"+DscmsLmtEnum.TRADE_CODE_CMISLMT0009.value+":" +
                    "已用金额【outstndAmt】"+outstndAmt+"+本次占用金额【BizTotalAmt】："+occSubRel.getBizTotalAmt()+",大于分项总额【avlAmt】："+avlAmt);
            throw new YuspException(EclEnum.ECL070006.key, "合作方"+ EclEnum.ECL070006.value);
        }

        String lmtCoopUncheckPrd = getSysParameterByName(CmisLmtConstants.LMT_COOP_UNCHECK_PRD) ;
        //例外类型 03-允许突破限额 不校验额度是否足额 || 信保贷等配置型产品不做是否足额校验
        if(!(finalExptType.contains(CmisLmtConstants.EXPT_TYPE_03) || lmtCoopUncheckPrd.contains(prdId))){
            //update by lizx 2021-11-16 合作方单笔单户不放在合同校验
            //sigAndSigBussCheck(occSubRel, cusId, apprCoopSubInfo, apprCoopInfo, sigAmt, sigBussAmt, recoverAmt);

            //授信总额：本次业务占用总额+台账下分项已占用金额之和<= 授信总额
            Map<String, BigDecimal> map = apprCoopInfoService.getSubOutTotal(apprSerno) ;
            //获取批复下所有分项已用额度之和
            BigDecimal outTotal = BigDecimalUtil.replaceNull(map.get("outstndAmt")) ;
            outTotal = outTotal.subtract(recoverAmt) ;
            if((outTotal.add(occSubRel.getBizTotalAmt())).compareTo(lmtAmt)>0){
                logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key+":"+DscmsLmtEnum.TRADE_CODE_CMISLMT0009.value+":" +
                        "批复下所有分项已用额度之和【outTotal】："+outTotal+"+本次占用金额【BizTotalAmt】"+occSubRel.getBizTotalAmt()+",大于授信总额【lmtAmt】："+lmtAmt);
                throw new YuspException(EclEnum.ECL070014.key, "合作方"+ EclEnum.ECL070014.value);
            }
            /**********限额校验结束*********/
        }
        logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.value + "：合作方额度校验结束");
    }

    /**
     * @作者:lizx
     * @方法名称: checkSpecialCoopCus
     * @方法描述:  
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/10/14 16:41
     * @param cusId: 
     * @param prdTypeProp: 
     * @param limitSubNo:
     * @return: boolean
     * @算法描述: 无
    */
    public boolean checkSpecialCoopCus(String cusId, String prdTypeProp, String limitSubNo) {
        logger.info("合作方特殊客户不管控额度，判断开始 START ---------------> cusId : ", cusId) ;

        /** 张家港市农业融资担保有限公司（客户号：8001092178，专业担保公司），
         * 信保贷产品使用该合作方担保额度时不做超额校验控制 ，出账时不做超额校验控制 **/
        if(CmisLmtConstants.STD_COOP_CUS_ID_8001092178.equals(cusId) && CmisLmtConstants.STD_ZB_PARTNER_TYPE_2.equals(limitSubNo)){
            if(CmisLmtConstants.STD_PRD_TYPE_PROP_P010.equals(prdTypeProp)){
                logger.info("张家港市农业融资担保有限公司（客户号：8001092178，专业担保公司），信保贷产品使用该合作方担保额度时不做超额校验控制 ，出账时不做超额校验控制{}", cusId) ;
                return true;
            }
        }

        /**
         * 江苏扬帆广睿国际贸易有限公司（客户号：8000749823，集群贷市场方），合同占用合作方额度是，
         * 不做超额校验控制 ，出账时做超额校验控制
         */
        if(CmisLmtConstants.STD_COOP_CUS_ID_8000749823.equals(cusId) && CmisLmtConstants.STD_ZB_PARTNER_TYPE_4.equals(limitSubNo)){
            logger.info("江苏扬帆广睿国际贸易有限公司（客户号：8000749823，集群贷市场方），合同占用合作方额度是，不做超额校验控制 ，出账时做超额校验控制{}", cusId) ;
            return true;
        }

        logger.info("合作方特殊客户不管控额度，判断结束 END ---------------> cusId : ", cusId) ;
        return false;
    }

    /**
     * @作者：李召星
     * @方法名称: updateSubInfoAmt
     * @方法描述: 更改分项相关金额
     * @参数与返回说明:
     * @算法描述：合同重签是否重签合同：
     */
    public void updateSubInfoAmt(String apprSubSerno, LmtContRel lmtContRel, Map<String, String> resultMap) throws Exception{
        if(CmisLmtConstants.STD_ZB_LMT_TYPE_03.equals(lmtContRel.getLmtType())){
            logger.info("额度分项编号为【"+apprSubSerno+"】的分项占用关系记录里的授信类型为合作方额度");
            //合作方额度，查询合作方分项信息，更新合作方额度金额
            ApprCoopSubInfo apprCoopSubInfo = apprCoopSubInfoService.selectAcsiInfoBySubSerno(apprSubSerno) ;
            Optional.ofNullable(apprCoopSubInfo).orElseThrow(()->new YuspException(EclEnum.ECL070007.key,EclEnum.ECL070007.value));
            //合作方已用额度-本次合同金额
            BigDecimal bfOutstndAmt = BigDecimalUtil.replaceNull(apprCoopSubInfo.getOutstndAmt()) ;
            BigDecimal outstndAmt = apprCoopSubInfo.getOutstndAmt().subtract(lmtContRel.getBizTotalBalanceAmtCny()) ;
            apprCoopSubInfo.setOutstndAmt(outstndAmt);
            logger.info("【"+apprSubSerno+"】更新合作已用额度【outstndAmt】："+bfOutstndAmt+"------------->"+outstndAmt);
            apprCoopSubInfoService.updateByPKeyInApprLmtChgDetails(apprCoopSubInfo, resultMap) ;
        }else if(CmisLmtConstants.STD_ZB_LMT_TYPE_06.equals(lmtContRel.getLmtType())) {
            logger.info("额度分项编号为【"+apprSubSerno+"】的分项占用关系记录里的授信类型为白名单额度");
            //白名单额度，查询白名单对应额度信息，更新白名单额度金额
            recoverWhiteInfo(apprSubSerno, lmtContRel, resultMap) ;
        }else {
            logger.info("额度分项编号为【"+apprSubSerno+"】的分项占用关系记录里的授信类型为单一额度");
            recoverApprLmtSubBasicInfo(apprSubSerno, lmtContRel, resultMap) ;
        }
    }

    /**
     * @作者:lizx
     * @方法名称: settleRecover
     * @方法描述:  
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/7/16 14:38
     * @param lmtContRel:
     * @return: void
     * @算法描述: 无
    */
    public void settleRecover(LmtContRel lmtContRel, Map<String, String> resultMap, String isRecoverCoop) throws Exception{
        String apprSubSerno = lmtContRel.getLimitSubNo() ;
        //恢复额度信息
        updateSubInfoAmt(apprSubSerno, lmtContRel, resultMap) ;
        //恢复合同金额 ，结清恢复，不需要处理分项已用总额和已用敞口总额
        BigDecimal bfBizTotalBalanceAmtCny = lmtContRel.getBizTotalBalanceAmtCny() ;
        lmtContRel.setBizTotalBalanceAmtCny(BigDecimal.ZERO);

        BigDecimal bfBizSpacBalanceAmtCny = lmtContRel.getBizSpacBalanceAmtCny() ;
        lmtContRel.setBizSpacBalanceAmtCny(BigDecimal.ZERO);
        lmtContRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_300);

        logger.info("结清恢复，额度占用状态更改为【300-结清已使用】，占用敞口余额（折人民币)更改为【0】，占用总余额（折人民币)更改为【0】");
        lmtContRelService.updateByPKeyInApprLmtChgDetails(lmtContRel, resultMap);
    }

    /**
     * @作者：李召星
     * @方法名称: recoverWhiteInfo
     * @方法描述: 更改白名单jine
     * @参数与返回说明:
     * @算法描述：合同重签是否重签合同：
     */
    public void recoverWhiteInfo(String apprSubSerno, LmtContRel lmtContRel, Map<String, String> resultMap) throws Exception{
        logger.info("额度白名单恢复处理---------------------->开始");
        ApprLmtChgDetail apprLmtChgDetail = initApprLmtChgDetail(resultMap) ;
        //白名单额度，查询白名单对应额度信息，更新白名单额度金额
        LmtWhiteInfo lmtWhiteInfo = lmtWhiteInfoService.selectLmtWhiteInfoBySubNo(apprSubSerno);
        Optional.ofNullable(lmtWhiteInfo).orElseThrow(() -> new YuspException(EclEnum.ECL070009.key, EclEnum.ECL070009.value));

        BigDecimal bfSigUseAmt = lmtWhiteInfo.getSigUseAmt() ;
        //白名单已用限额-合同金额
        BigDecimal sigUseAmt = lmtWhiteInfo.getSigUseAmt().subtract(lmtContRel.getBizTotalBalanceAmtCny()) ;
        lmtWhiteInfo.setSigUseAmt(sigUseAmt);
        logger.info("【"+lmtWhiteInfo.getSubAccNo()+"】白名单已用限额【SigUseAmt】更改为："+sigUseAmt) ;
        lmtWhiteInfoMapper.updateByPrimaryKey(lmtWhiteInfo) ;

        apprLmtChgDetail.setUpdTableName(CmisLmtConstants.LMT_WHITE_INFO);
        apprLmtChgDetail.setFiled1(CmisLmtConstants.LMT_WHITE_INFO_SIG_USE_AMT);
        apprLmtChgDetail.setUpdbfFiled1(bfSigUseAmt);
        apprLmtChgDetail.setUpdafFiled1(sigUseAmt);
        apprLmtChgDetail.setPkIdRel(lmtWhiteInfo.getPkId());
        apprLmtChgDetailService.insert(apprLmtChgDetail) ;
        logger.info("额度白名单恢复处理---------------------->结束");
    }

    /**
     * @作者：李召星
     * @方法名称: recoverApprLmtSubBasicInfo
     * @方法描述: 合同撤销占用等相关处理
     * @参数与返回说明:
     * @算法描述：：
     */
    public void recoverApprLmtSubBasicInfo(String apprSubSerno, LmtContRel lmtContRel ,Map<String, String> resultMap) throws Exception{
        String bizAttr = lmtContRel.getBizAttr() ;
        //占用总余额（折人民币）
        BigDecimal bizTotalBalanceAmtCny = BigDecimalUtil.replaceNull(lmtContRel.getBizTotalBalanceAmtCny()) ;
        //占用敞口余额（折人民币）
        BigDecimal bizSpacBalanceAmtCny = BigDecimalUtil.replaceNull(lmtContRel.getBizSpacBalanceAmtCny()) ;
        //查询分项信息
        ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(apprSubSerno) ;
        Optional.ofNullable(apprLmtSubBasicInfo).orElseThrow(()->new YuspException(EclEnum.ECL070007.key,EclEnum.ECL070007.value));

        String bizStatus = lmtContRel.getBizStatus() ;
        //敞口已用额度减少本次恢复合同总敞口金额
        if(CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
            logger.info("【Lmt_Cont_rel】的BizAttr为【2-台账】，状态为{}【200-生效】，进行恢复已用总额和已用敞口余额", bizStatus);
            if(CmisLmtConstants.STD_ZB_BIZ_STATUS_200.equals(bizStatus)){
                //批复分项已用总额 = 批复分项贷款余额 - 占用关系里的占用总余额（折人民币）
                BigDecimal loanBalance = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanBalance()) ;
                apprLmtSubBasicInfo.setLoanBalance(loanBalance.subtract(bizTotalBalanceAmtCny));

                //批复分项用信敞口余额 = 批复分项用信敞口余额 - 占用敞口余额（折人民币）
                BigDecimal loanSpacBalance = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanSpacBalance()) ;
                apprLmtSubBasicInfo.setLoanSpacBalance(loanSpacBalance.subtract(bizSpacBalanceAmtCny));
            }
            //处理可出账金额已出账金额
            recoverAvlAndPvpOutstndAmt(apprLmtSubBasicInfo, bizTotalBalanceAmtCny, bizSpacBalanceAmtCny) ;
        }
        //批复分项敞口已用额度 = 批复分项敞口已用额度 - 占用敞口余额（折人民币）
        BigDecimal spacOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getSpacOutstndAmt()) ;
        apprLmtSubBasicInfo.setSpacOutstndAmt(spacOutstndAmt.subtract(bizSpacBalanceAmtCny));

        //批复分项已用额度 = 批复分项已用额度 - 占用总余额（折人民币）
        //修改前金额
        BigDecimal outstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getOutstndAmt()) ;
        apprLmtSubBasicInfo.setOutstndAmt(outstndAmt.subtract(bizTotalBalanceAmtCny));
        apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfo, resultMap) ;

        if(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_02.equals(apprLmtSubBasicInfo.getLmtSubType())){
            String parentId = apprLmtSubBasicInfo.getParentId();
            if (StringUtils.isNotEmpty(parentId)){
                recoverApprLmtSubBasicInfo(parentId, lmtContRel, resultMap) ;
            }
        }
    }

    /**
     * @作者:lizx
     * @方法名称: createApprLmtChgFileRecord
     * @方法描述:  数据存入留痕明细表中
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/21 16:56
     * @param apprLmtChgFiledListDtos:字段修改list 明细
     * @param apprLmtChgDetail:修改实例
     * @return: void
     * @算法描述: 无
    */
    public void createApprLmtChgFileRecord7(List<ApprLmtChgFiledListDto> apprLmtChgFiledListDtos ,ApprLmtChgDetail apprLmtChgDetail) throws Exception{
        for (int i=1 ; i<=apprLmtChgFiledListDtos.size();i++) {
            ApprLmtChgFiledListDto apprLmtChgFiledListDto = apprLmtChgFiledListDtos.get(i-1) ;
            String setFiledMethod = "setFiled" + i;
            String setUpdbfFiledMethod = "setUpdbfFiled" + i;
            String setUpdafFiledMethod = "setUpdafFiled" + i;

            String filed = apprLmtChgFiledListDto.getFiled() ;
            BigDecimal bfFiledValue = apprLmtChgFiledListDto.getUpdbfFiled() ;
            BigDecimal afFiledValue = apprLmtChgFiledListDto.getUpdafFiled() ;

            logger.info("修改表数据，数据留痕记录：【{}】,修改前值【{}】,修改后值【{}】", filed, bfFiledValue, afFiledValue);
            apprLmtChgDetail.getClass().getDeclaredMethod(setFiledMethod, String.class).invoke(apprLmtChgDetail, filed);
            apprLmtChgDetail.getClass().getDeclaredMethod(setUpdbfFiledMethod, BigDecimal.class).invoke(apprLmtChgDetail, bfFiledValue) ;
            apprLmtChgDetail.getClass().getDeclaredMethod(setUpdafFiledMethod, BigDecimal.class).invoke(apprLmtChgDetail, afFiledValue) ;
        }
        logger.info("修改表数据，数据留痕记录：【{}】", JSON.toJSONString(apprLmtChgDetail));
        apprLmtChgDetailService.insert(apprLmtChgDetail) ;
    }
    /**
     * @作者:lizx
     * @方法名称: createApprLmtChgFileRecord
     * @方法描述:  数据存入留痕明细表中
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/21 16:56
     * @param apprLmtChgFiledListDtos:字段修改list 明细
     * @param apprLmtChgDetail:修改实例
     * @return: void
     * @算法描述: 无
     */
    public void createApprLmtChgFileRecord(List<ApprLmtChgFiledListDto> apprLmtChgFiledListDtos ,ApprLmtChgDetail apprLmtChgDetail){
        Map<String ,String> paraMap = new HashMap<>() ;
        for (int i=1 ; i<=apprLmtChgFiledListDtos.size();i++) {
            ApprLmtChgFiledListDto apprLmtChgFiledListDto = apprLmtChgFiledListDtos.get(i-1) ;
            switch(i){
                case 1:
                    apprLmtChgDetail.setFiled1(apprLmtChgFiledListDto.getFiled());
                    apprLmtChgDetail.setUpdbfFiled1(apprLmtChgFiledListDto.getUpdbfFiled());
                    apprLmtChgDetail.setUpdafFiled1(apprLmtChgFiledListDto.getUpdafFiled());
                    break ;
                case 2:
                    apprLmtChgDetail.setFiled2(apprLmtChgFiledListDto.getFiled());
                    apprLmtChgDetail.setUpdbfFiled2(apprLmtChgFiledListDto.getUpdbfFiled());
                    apprLmtChgDetail.setUpdafFiled2(apprLmtChgFiledListDto.getUpdafFiled());
                    break ;
                case 3:
                    apprLmtChgDetail.setFiled3(apprLmtChgFiledListDto.getFiled());
                    apprLmtChgDetail.setUpdbfFiled3(apprLmtChgFiledListDto.getUpdbfFiled());
                    apprLmtChgDetail.setUpdafFiled3(apprLmtChgFiledListDto.getUpdafFiled());
                    break ;
                case 4:
                    apprLmtChgDetail.setFiled4(apprLmtChgFiledListDto.getFiled());
                    apprLmtChgDetail.setUpdbfFiled4(apprLmtChgFiledListDto.getUpdbfFiled());
                    apprLmtChgDetail.setUpdafFiled4(apprLmtChgFiledListDto.getUpdafFiled());
                    break ;
                case 5:
                    apprLmtChgDetail.setFiled5(apprLmtChgFiledListDto.getFiled());
                    apprLmtChgDetail.setUpdbfFiled5(apprLmtChgFiledListDto.getUpdbfFiled());
                    apprLmtChgDetail.setUpdafFiled5(apprLmtChgFiledListDto.getUpdafFiled());
                    break ;
                case 6:
                    apprLmtChgDetail.setFiled6(apprLmtChgFiledListDto.getFiled());
                    apprLmtChgDetail.setUpdbfFiled6(apprLmtChgFiledListDto.getUpdbfFiled());
                    apprLmtChgDetail.setUpdafFiled6(apprLmtChgFiledListDto.getUpdafFiled());
                    break ;
                case 7:
                    apprLmtChgDetail.setFiled7(apprLmtChgFiledListDto.getFiled());
                    apprLmtChgDetail.setUpdbfFiled7(apprLmtChgFiledListDto.getUpdbfFiled());
                    apprLmtChgDetail.setUpdafFiled7(apprLmtChgFiledListDto.getUpdafFiled());
                    break ;
                default:
                    break ;
            }
        }
        logger.info("修改表数据，数据留痕记录：【{}】", JSON.toJSONString(apprLmtChgDetail));
        apprLmtChgDetailService.insert(apprLmtChgDetail) ;
    }

    /**
     * @作者:lizx
     * @方法名称: initLmtChgFiled
     * @方法描述:  初始化字段修改值
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/21 16:55
     * @param filed: 字段名
     * @param bfFiledValue: 修改前金额
     * @param afFiledValue: 修改后金额
     * @param apprLmtChgFiledListDtos:修改后放入list 中
     * @return: void
     * @算法描述: 无
    */
    public void initLmtChgFiled(String filed ,BigDecimal bfFiledValue ,BigDecimal afFiledValue ,List<ApprLmtChgFiledListDto> apprLmtChgFiledListDtos){
        ApprLmtChgFiledListDto apprLmtChgFiledListDto = new ApprLmtChgFiledListDto() ;
        apprLmtChgFiledListDto.setFiled(filed);
        apprLmtChgFiledListDto.setUpdbfFiled(bfFiledValue);
        apprLmtChgFiledListDto.setUpdafFiled(afFiledValue);
        apprLmtChgFiledListDtos.add(apprLmtChgFiledListDto) ;
    }

    /**
     * 描述：根据用信产品编号和分项父级编号，获取分项明细信息
     * @param parentId 分项父级编号
     * @param prdId 额度用信产品编号
     * @return
     */
    public ApprLmtSubBasicInfo selectSubBasicInfoByParentId(String parentId, String prdId, String limitStrNo, String lmtBizTypeProp){
        String limitSubNo =getLimitSubNoByPrdIdAndStrNo(prdId, limitStrNo) ;
        if(StringUtils.isBlank(limitSubNo)) throw new YuspException(EclEnum.ECL070082.key,EclEnum.ECL070082.value);
        //根据二级分项父级编号和分项产品编号获取到分项信息
        return apprLmtSubBasicInfoService.selectByPrdAndPraParam(parentId, limitSubNo, lmtBizTypeProp) ;
    }

    /**
     * 查询客户名下有效标准化资产授信（未到期）金额之和
     * @param cusId
     * @return
     */
    public BigDecimal getTotalAssetLmtAmt(String cusId){
        return apprStrMtableInfoService.selectTotalAssetLmtAmtByCusId(cusId);
    }

    /**
     * 查询客户名下标准化资产余额（已到期部分）
     * @param cusId
     * @return
     */
    public BigDecimal getTotalAssetBalanceAmt(String cusId){
        return apprStrMtableInfoService.selectTotalAssetBalanceAmtByCusId(cusId);
    }

    /**
     * 查询客户名下有效非标资产授信（未到期）金额之和
     * @param cusId 客户号
     * @return
     */
    public BigDecimal getTotalAssetLmtAmtNonStandardByCusId(String cusId){
        return apprStrMtableInfoService.selectTotalAssetLmtAmtNonStandardByCusId(cusId);
    }

    /**
     * 查询客户名下非标资产余额（已到期部分）
     * @param cusId 客户号
     * @return
     */
    public BigDecimal getTotalAssetBalanceAmtNonStandardByCusId(String cusId){
        return apprStrMtableInfoService.selectTotalAssetBalanceAmtNonStandardByCusId(cusId);
    }

    /**
     * 查询客户（综合授信+主体授信）未到期的授信金额
     * @param cusId
     * @return
     */
    public BigDecimal selectTotalLmtAmtByCusId(String cusId,String instuCde,String isQueryCth,String isQueryDfx){
        QueryModel model = new QueryModel();
        model.addCondition("cusId",cusId);
        model.addCondition("instuCde",instuCde);
        model.addCondition("isQueryCth",isQueryCth);
        model.addCondition("isQueryDfx",isQueryDfx);
        return apprStrMtableInfoService.selectTotalLmtAmtByCusId(model);
    }

    /**
     * 查询客户（综合授信+主体授信）已到期的业务余额
     * @param cusId
     * @return
     */
    public BigDecimal selectTotalBalanceAmtByCusId(String cusId,String instuCde,String isQueryCth,String isQueryDfx){
        QueryModel model = new QueryModel();
        model.addCondition("cusId",cusId);
        model.addCondition("instuCde",instuCde);
        model.addCondition("isQueryCth",isQueryCth);
        model.addCondition("isQueryDfx",isQueryDfx);
        return apprStrMtableInfoService.selectTotalBalanceAmtByCusId(model);
    }

    /**
     * 查询客户 委托贷款未到期的授信金额
     * @param cusId
     * @return
     */
    public BigDecimal selectTotalWtLmtAmtByCusId(String cusId,String instuCde,String isQueryDfx){
        QueryModel model = new QueryModel();
        model.addCondition("cusId",cusId);
        model.addCondition("instuCde",instuCde);
        model.addCondition("isQueryDfx",isQueryDfx);
        return apprStrMtableInfoService.selectTotalWtLmtAmtByCusId(model);
    }
    /**
     * 查询客户 委托贷款 已到期的业务余额
     * @param cusId
     * @return
     */
    public BigDecimal selectTotalWtBalanceAmtByCusId(String cusId,String instuCde,String isQueryDfx){
        QueryModel model = new QueryModel();
        model.addCondition("cusId",cusId);
        model.addCondition("instuCde",instuCde);
        model.addCondition("isQueryDfx",isQueryDfx);
        return apprStrMtableInfoService.selectTotalWtBalanceAmtByCusId(model);
    }


    /**
     * 生成主键id
     * @return
     */
    public String generatePkId(){
        long start = System.currentTimeMillis();
        String sequenceTemplate = sequenceTemplateClient.getSequenceTemplate(CmisBizConstants.PK_VALUE, new HashMap<>());
        logger.info("pkId生成消耗时间：{}",System.currentTimeMillis()-start);
        return sequenceTemplate;
    }

    /**
     * 生成序列号
     * @param CodeName(序列号生成规则) 默认 CmisBizConstants.BIZ_SERNO
     * @return
     */
    public String generateSerno(String CodeName){
        if (cn.com.yusys.yusp.commons.util.StringUtils.isBlank(CodeName)){
            CodeName = CmisBizConstants.BIZ_SERNO;
        }
        long start = System.currentTimeMillis();
        String sequenceTemplate = sequenceTemplateClient.getSequenceTemplate(CodeName, new HashMap<>());
        logger.info("序列号生成消耗时间：{}",System.currentTimeMillis()-start);
        return sequenceTemplate;
    }

    /**
     * @作者:lizx
     * @方法名称: updateLmtContRelRelSubNo
     * @方法描述:  查询原分项下业务，将未结清业务挂靠到新分项下
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/6 13:54
     * @param origiAccSubNo:
     * @param accSubNo:
     * @return: void
     * @算法描述: 无
    */
    public void updateLmtContRelRelSubNo(String origiAccSubNo, String accSubNo){
        List<LmtContRel> lmtContRelList = lmtContRelService.selectOutStandingLoadCont(origiAccSubNo) ;
        //原分项批复下的业务挂到新分项下
        if(CollectionUtils.nonEmpty(lmtContRelList)){
            for (LmtContRel lmtContRel : lmtContRelList) {
                logger.info("【{}】额度占用关系，从【{}】，挂靠到【{}】", lmtContRel.getDealBizNo(), origiAccSubNo, accSubNo);
                lmtContRel.setLimitSubNo(accSubNo);
                lmtContRelMapper.updateByPrimaryKey(lmtContRel) ;
            }
        }
    }

    /**
     * @作者:lizx
     * @方法名称: updateContAccRelLmtCont
     * @方法描述:  根据新老合同信息，将原台账向下数据挂靠到新数据向下
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/9/25 17:42
     * @param origiDealBizNo:
     * @param dealBizNo:
     * @return: void
     * @算法描述: 无
    */
    public void updateContAccRelLmtCont(String origiDealBizNo, String dealBizNo){
        List<ContAccRel> accRels = contAccRelService.queryUnsettleListByDealBizNo(origiDealBizNo) ;
        if(CollectionUtils.nonEmpty(accRels)){
            for (ContAccRel contAccRel : accRels) {
                logger.info("台账挂靠，将{}下的未结清台账信息挂靠到{}下", origiDealBizNo, dealBizNo);
                contAccRel.setDealBizNo(dealBizNo);
                contAccRelService.update(contAccRel) ;
            }
        }
    }

    /**
     * 获取系统参数
     * @param configName
     * @return
     */
    public String getSysParameterByName(String configName){
        try {
            AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
            adminSmPropQueryDto.setPropName(configName);
            return adminSmPropService.getPropValue(adminSmPropQueryDto).getData().getPropValue();
        } catch (Exception e) {
            logger.error("获取系统参数{}失败{}",configName,e);
            return "";
        }
    }

    /**
     * @作者:lizx
     * @方法名称: IsEqualsTwoStr
     * @方法描述:  判单两个字符传是否相等
     * @参数与返回说明: 相等则返回true 否则 false
     * @算法描述: 无
     * @日期：2021/7/14 10:43
     * @param str1:
     * @param str2:
     * @return: boolean
     * @算法描述: 无
    */
    public boolean IsEqualsTwoStr(String str1, String str2){
        boolean result = true ;
        if(StringUtils.isEmpty(str1)){
            if (StringUtils.isNotEmpty(str2)){
                result = false ;
            }
        }else{
            if (!str1.equals(str2)){
                result = false ;
            }
        }
        return result ;
    }

    /**
     * 获取一个或多个批复分项编号（以逗号隔开）的用信余额总和
     * @param apprSubSernos
     * @return
     */
    public BigDecimal getBalanceByApprSubSernos(String apprSubSernos){
        //根据分项编号
        BigDecimal contBal = contAccRelService.selectAccTotalBalByLimitSubNo(apprSubSernos) ;
        logger.info("【{}】查询合同向下占用余额【{}】",DscmsLmtEnum.TRADE_CODE_CMISLMT0039.key, contBal);

        //查询分项向下无合同占用余额
        BigDecimal lmtBal =lmtContRelService.selecBizBalByLimitSubNos(apprSubSernos) ;
        logger.info("【{}】查询无合同向下占用余额【{}】",DscmsLmtEnum.TRADE_CODE_CMISLMT0039.key, lmtBal);

        return contBal.add(lmtBal) ;
    }

    /**
     * 获取一个或多个批复分项编号（以逗号隔开）的获取向下贷款总金额（不包含结清贷款）
     * @param apprSubSernos
     * @return
     */
    public BigDecimal getLoanTotalByApprSubSernos(String apprSubSernos){
        //根据分项编号
        BigDecimal contTotal = contAccRelService.selectAccTotalByLimitSubNos(apprSubSernos) ;
        logger.info("【{}】查询合同向下占用余额【{}】",DscmsLmtEnum.TRADE_CODE_CMISLMT0039.key, contTotal);

        //查询分项向下无合同占用余额
        BigDecimal lmtTotal =lmtContRelService.selecBizTotalByLimitSubNos(apprSubSernos) ;
        logger.info("【{}】查询无合同向下占用余额【{}】",DscmsLmtEnum.TRADE_CODE_CMISLMT0039.key, lmtTotal);

        return contTotal.add(lmtTotal) ;
    }

    /**
     * @作者:lizx
     * @方法名称: isExistsUnSettle
     * @方法描述:  判断分项向下是否存在未结清的业务
     * @参数与返回说明: true 存在  false 不存在
     * @算法描述: 无
     * @日期：2021/8/17 20:29
     * @param apprSubSernos:
     * @return: boolean
     * @算法描述: 无
    */
    public boolean isExistsUnSettle(String apprSubSernos){
        BigDecimal contBal = getBalanceByApprSubSernos(apprSubSernos) ;
        if(contBal.compareTo(BigDecimal.ZERO)!=0){
            return true ;
        }
        return false ;
    }

    /**
     * @作者:lizx
     * @方法名称: isExistsUnSettleTwo
     * @方法描述:  判断分项向下是否存在未结清的业务
     * @参数与返回说明: true 存在  false 不存在
     * @算法描述: 无
     * @日期：2021/8/17 20:29
     * @param apprSubSerno:
     * @param limitSubType:
     * @return: boolean
     * @算法描述: 无
     */
    public boolean isExistsUnSettleTwo(String apprSubSerno, String limitSubType) throws Exception {
        logger.info("判断是否存在未结清业务处理..........start");
        boolean result = false ;
        CmisLmt0047ReqDto cmisLmt0047ReqDto = new CmisLmt0047ReqDto() ;
        List<CmisLmt0047LmtSubDtoList> lmtSubDtoList = new ArrayList<>();

        CmisLmt0047LmtSubDtoList cmisLmt0047LmtSubDtoList = new CmisLmt0047LmtSubDtoList() ;
        cmisLmt0047LmtSubDtoList.setAccSubNo(apprSubSerno);

        lmtSubDtoList.add(cmisLmt0047LmtSubDtoList);
        cmisLmt0047ReqDto.setLmtSubDtoList(lmtSubDtoList);
        cmisLmt0047ReqDto.setQueryType(limitSubType);

        CmisLmt0047RespDto cmisLmt0047RespDto = cmisLmt0047Service.execute(cmisLmt0047ReqDto) ;
        if(Objects.nonNull(cmisLmt0047RespDto)){
            if(cmisLmt0047RespDto.getErrorCode().equals(SuccessEnum.SUCCESS.key)){
                List<CmisLmt0047ContRelDtoList> cmisLmt0047ContRelDtoLists = cmisLmt0047RespDto.getContRelDtoList() ;
                if(CollectionUtils.nonEmpty(cmisLmt0047ContRelDtoLists) && cmisLmt0047ContRelDtoLists.size()>0){
                    result = true ;
                }
            }
        }
        return result ;
    }

    /**
     * 判断一个或多个批复分项编号（以逗号隔开）的用信余额总和是否大于0
     * @param apprSubSernos
     * @return
     */
    public boolean hasBalance(String apprSubSernos){
        BigDecimal totalBal = getBalanceByApprSubSernos(apprSubSernos);
        return totalBal.compareTo(BigDecimal.ZERO)>0;
    }

    /**
     * @作者:lizx
     * @方法名称: cancelContAccRel
     * @方法描述:  台账撤销占用方法处理
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/15 17:25
     * @param contAccRel:
     * @param lmtContRel:
     * @return: void
     * @算法描述: 无
    */
    public void cancelContAccRel(ContAccRel contAccRel, LmtContRel lmtContRel, Map<String, String> resultMap) {
        //撤销敞口金额
        BigDecimal recoverSpacAmtCny = BigDecimal.ZERO ;
        BigDecimal recoverAmtCny = BigDecimal.ZERO ;
        //修改记录添加
        String status = CmisLmtConstants.STD_ZB_BIZ_STATUS_200 ;
        String bizAttr = lmtContRel.getBizAttr() ;
        if(CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
            recoverSpacAmtCny = lmtContRel.getBizSpacBalanceAmtCny() ;
            recoverAmtCny = lmtContRel.getBizTotalAmtCny() ;
            status = lmtContRel.getBizStatus() ;
        }else{
            recoverSpacAmtCny = contAccRel.getAccSpacBalanceAmtCny() ;
            recoverAmtCny = contAccRel.getAccTotalBalanceAmtCny() ;
            status = contAccRel.getStatus() ;
        }
        cancalOccExecute(contAccRel, lmtContRel, resultMap, recoverSpacAmtCny, recoverAmtCny, status);
    }
    /**
     * @作者:lizx
     * @方法名称: cancalOccExecute
     * @方法描述:  撤销占用执行处理
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/11/16 10:19
     * @param contAccRel:
     * @param lmtContRel:
     * @param resultMap:
     * @param recoverSpacAmtCny:
     * @param recoverAmtCny:
     * @param status:
     * @return: void
     * @算法描述: 无
    */
    public void cancalOccExecute(ContAccRel contAccRel, LmtContRel lmtContRel, Map<String, String> resultMap,
                                  BigDecimal recoverSpacAmtCny, BigDecimal recoverAmtCny, String status) {
        //额度类型
        String lmtType = lmtContRel.getLmtType() ;
        //分项编号
        String subSerno = lmtContRel.getLimitSubNo() ;
        String bizAttr = lmtContRel.getBizAttr() ;
        logger.info("【cancelContAccRel】台账撤销占用开始,撤销金额以台账中余额为准，撤销敞口金额{}，撤销总额{}---------->start", recoverSpacAmtCny, recoverAmtCny) ;
        //add by 2021-08-30 占用敞口余额，只有最高额合同/最高额授信协议，未到期的情况下，才不进行恢复，其余情况都恢复
        if((lmtContRel.getBizStatus()).equals(CmisLmtConstants.STD_ZB_BIZ_STATUS_500)){
            Map<String, BigDecimal> amtMap = new HashMap<>() ;
            amtMap.put("recoverAmtCny", recoverAmtCny) ;
            amtMap.put("recoverSpacAmtCny", recoverSpacAmtCny) ;
            //恢复合同金额
            recoverLmtcontRelBal(lmtContRel, amtMap, resultMap);
        }
        //合同类型
        logger.info("【cancelContAccRel】台账占用撤销----start,撤销额度类型【LmtType】{}, 合同类型{}", lmtType, bizAttr);
        //法人额度，获取风险分项信息，恢复分线该信息
        if(CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(lmtType) || CmisLmtConstants.STD_ZB_LMT_TYPE_07.equals(lmtType)){
            //查询分项信息
            ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(subSerno);
            Optional.ofNullable(apprLmtSubBasicInfo).orElseThrow(()->new YuspException(EclEnum.ECL070007.key,EclEnum.ECL070007.value));
            //已出账金额 = 已出账金额 - 本次恢复金额  && 已出账金额 = 已出账金额 - 本次恢复金额
            recoverAvlAndPvpOutstndAmt(apprLmtSubBasicInfo, recoverAmtCny, recoverSpacAmtCny) ;

            //状态为100-未生效的额度撤销，不要要撤销用信敞口余额和用信总额
            logger.info("【cancelContAccRel】台账撤销,撤销台账状态为【{}】 （100-不恢复用信余额和用信敞口余额）", status) ;
            if(CmisLmtConstants.STD_ZB_BIZ_STATUS_200.equals(status)){
                //用信敞口余额 = 用信敞口余额 - 本次恢复敞口金额
                BigDecimal loanSpacBalance = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanSpacBalance()) ;
                apprLmtSubBasicInfo.setLoanSpacBalance(loanSpacBalance.subtract(recoverSpacAmtCny));

                //用信余额 = 用信余额 - 本次恢复金额
                BigDecimal loanBalance = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanBalance()) ;
                apprLmtSubBasicInfo.setLoanBalance(loanBalance.subtract(recoverAmtCny));
                logger.info("【cancelContAccRel】撤销占用，撤销状态为【200-生效】恢复用信敞口余额【loanSpacBalance】"
                                +loanSpacBalance.subtract(recoverSpacAmtCny)+"和用信余额【loanBalance】"+apprLmtSubBasicInfo.getLoanBalance()
                    ) ;
            }
            //如果是台账
            if(CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr) ||
                    (CmisLmtConstants.STD_ZB_BIZ_STATUS_500.equals(lmtContRel.getBizStatus()) && CmisLmtConstants.YES_NO_Y.equals(apprLmtSubBasicInfo.getIsRevolv()))){
                logger.info("合同类型为台账或合同已到期且可循环额度，撤销占用时，进行占用余额恢复 【bizAttr：】{},合同状态{}", bizAttr, lmtContRel.getBizStatus());
                cancalApprAmt(recoverSpacAmtCny, recoverAmtCny, apprLmtSubBasicInfo);
            }

            //更新处理额度分项数据信息
            apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfo, resultMap) ;

            //最高额协议处理下级分项明细额度信息，非最高额协议合同，处理上级分项额度信息
            String dealBizType = lmtContRel.getDealBizType() ;
            //最高额协议
            if(CmisLmtConstants.DEAL_BIZ_TYPE_3.equals(dealBizType)){
                //获取最高协议下的分项明细的台账信息
               List<LmtContRel> lmtContRelList = lmtContRelMapper.selectLmtContRelByDealBizNo(contAccRel.getTranAccNo()) ;
                if(CollectionUtils.nonEmpty(lmtContRelList)){
                    LmtContRel contRel  = lmtContRelList.get(0);
                    //获取分项明细编号信息
                    String lmtDetailNo = contRel.getLimitSubNo() ;
                    logger.info("【cancelContAccRel】台账占用撤销-----,最高额协议撤销明细占用{},删除台账占用{}", lmtDetailNo, JSON.toJSONString(contRel));
                    //删除该台账占用信息
                    int delCont = lmtContRelMapper.deleteByIds(contRel.getPkId()) ;
                    logger.info("【cancelContAccRel】台账占用撤销-----,最高额协议删除合同台账信息，删除结果{}", JSON.toJSONString(contRel), delCont);

                    //获取分项明细信息根据额度状态，恢复额度信息
                    ApprLmtSubBasicInfo detailsInfo = apprLmtSubBasicInfoMapper.selectByApprSubSerno(lmtDetailNo) ;
                    if(Objects.nonNull(detailsInfo)){
                        //可出账金额 和 已出账金额处理
                        recoverAvlAndPvpOutstndAmt(detailsInfo, recoverAmtCny, recoverSpacAmtCny) ;
                        //已用总额
                        //修改前
                        BigDecimal bfDetailsOutstndAmt = BigDecimalUtil.replaceNull(detailsInfo.getOutstndAmt()) ;
                        //修改后
                        BigDecimal afDetailsOutstndAmt = bfDetailsOutstndAmt.subtract(recoverAmtCny) ;
                        detailsInfo.setOutstndAmt(afDetailsOutstndAmt);

                        //已用敞口总额
                        //修改前
                        BigDecimal bfDetailsSpacOutstndAmt = BigDecimalUtil.replaceNull(detailsInfo.getSpacOutstndAmt()) ;
                        //修改后
                        BigDecimal afDetailsSpacOutstndAmt = bfDetailsSpacOutstndAmt.subtract(recoverSpacAmtCny) ;
                        detailsInfo.setSpacOutstndAmt(afDetailsSpacOutstndAmt);
                        logger.info("【ApprLmtSubBasicInfo】恢复【"+ detailsInfo.getApprSubSerno()+"】,"
                                +",已用总额金额【OutstndAmt】更改为" + bfDetailsOutstndAmt + " --> "+afDetailsOutstndAmt
                                +",可出账金额【SpacOutstndAmt】更改为" + bfDetailsSpacOutstndAmt + " --> "+afDetailsSpacOutstndAmt
                        );
                        //判断状态，如果时200 则恢复用信金额
                        if(CmisLmtConstants.STD_ZB_BIZ_STATUS_200.equals(status)){
                            //用信敞口余额
                            //修改前
                            BigDecimal bfLoanSpacBalance = BigDecimalUtil.replaceNull(detailsInfo.getLoanSpacBalance()) ;
                            //修改后
                            BigDecimal afLoanSpacBalance = bfLoanSpacBalance.subtract(recoverSpacAmtCny) ;
                            detailsInfo.setLoanSpacBalance(afLoanSpacBalance);
                            //用信余额
                            //修改前
                            BigDecimal bfLoanBalance = BigDecimalUtil.replaceNull(detailsInfo.getLoanBalance()) ;
                            //修改后
                            BigDecimal afLoanBalance = bfLoanBalance.subtract(recoverAmtCny) ;
                            detailsInfo.setLoanBalance(afLoanBalance);
                            logger.info("【ApprLmtSubBasicInfo】恢复【"+ detailsInfo.getApprSubSerno()+"】,"
                                    +",用信余额【LoanBalance】更改为"+ bfLoanBalance + " --> "+afLoanBalance
                                    +",用信敞口余额【LoanSpacBalance】更改为" + bfLoanSpacBalance + " --> "+afLoanSpacBalance
                            ) ;
                        }
                        if(CmisLmtConstants.STD_ZB_BIZ_STATUS_500.equals(lmtContRel.getBizStatus())
                                && CmisLmtConstants.YES_NO_Y.equals(detailsInfo.getIsRevolv())){
                            logger.info("合同已到期且可循环额度，撤销占用时，进行占用余额恢复合同状态{},分项信息：", lmtContRel.getBizStatus(), JSON.toJSONString(detailsInfo));
                            cancalApprAmt(recoverSpacAmtCny, recoverAmtCny, detailsInfo);
                        }
                        apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(detailsInfo, resultMap) ;
                    }
                }
            }else{
                //上级分项信息
                String parantId = apprLmtSubBasicInfo.getParentId() ;
                //获取分项信息
                ApprLmtSubBasicInfo detailsInfo = apprLmtSubBasicInfoMapper.selectByApprSubSerno(parantId) ;
                if(Objects.nonNull(detailsInfo)){
                    //可出账金额 = 可出账金额 + 本次恢复金额 && 已出账金额 = 已出账金额 - 本次恢复金额
                    recoverAvlAndPvpOutstndAmt(detailsInfo, recoverAmtCny, recoverSpacAmtCny) ;
                    //判断状态，如果时200 则恢复用信金额
                    if(CmisLmtConstants.STD_ZB_BIZ_STATUS_200.equals(status)){
                        //用信敞口余额 = 用信敞口余额 - 本次恢复金额
                        BigDecimal loanSpacBalance = BigDecimalUtil.replaceNull(detailsInfo.getLoanSpacBalance()) ;
                        detailsInfo.setLoanSpacBalance(loanSpacBalance.subtract(recoverSpacAmtCny));

                        //用信余额 = 用信余额 - 本次恢复金额
                        BigDecimal loanBalance = BigDecimalUtil.replaceNull(detailsInfo.getLoanBalance()) ;
                        detailsInfo.setLoanBalance(loanBalance.subtract(recoverAmtCny) );
                    }
                    if(CmisLmtConstants.STD_ZB_BIZ_STATUS_500.equals(lmtContRel.getBizStatus())
                            && CmisLmtConstants.YES_NO_Y.equals(detailsInfo.getIsRevolv())){
                        logger.info("合同已到期且可循环额度，撤销占用时，进行占用余额恢复合同状态{},分项信息：", lmtContRel.getBizStatus(), JSON.toJSONString(detailsInfo));
                        cancalApprAmt(recoverSpacAmtCny, recoverAmtCny, detailsInfo);
                    }
                    apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(detailsInfo, resultMap) ;
                }
            }

            //判断是否需要恢复同业客户类额度信息
            if(!CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
                recoverLmtSigInfo(contAccRel, resultMap,CmisLmtConstants.STD_RECOVER_TYPE_06, null);
            }
        }else if(CmisLmtConstants.STD_ZB_LMT_TYPE_03.equals(lmtType)){
            if(!CmisLmtConstants.STD_ZB_BIZ_STATUS_100.equals(status)){
                //合作方额度，查询合作方分项信息，更新合作方额度金额
                ApprCoopSubInfo apprCoopSubInfo = apprCoopSubInfoService.selectAcsiInfoBySubSerno(subSerno) ;
                Optional.ofNullable(apprCoopSubInfo).orElseThrow(()->new YuspException(EclEnum.ECL070007.key,EclEnum.ECL070007.value));
                //修改前
                BigDecimal bfLoanBalance = BigDecimalUtil.replaceNull(apprCoopSubInfo.getLoanBalance()) ;
                //修改后
                BigDecimal afLoanBalance = bfLoanBalance.subtract(recoverAmtCny) ;
                apprCoopSubInfo.setLoanBalance(afLoanBalance);
                apprCoopSubInfoService.updateByPKeyInApprLmtChgDetails(apprCoopSubInfo, resultMap) ;
            }
        }
        logger.info("【cancelContAccRel】台账撤销占用开始---------->end") ;
    }

    private void cancalApprAmt(BigDecimal recoverSpacAmtCny, BigDecimal recoverAmtCny, ApprLmtSubBasicInfo apprLmtSubBasicInfo) {
        //已用总额 ---   修改前
        BigDecimal bfDetailsOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getOutstndAmt()) ;
        //修改后
        BigDecimal afDetailsOutstndAmt = bfDetailsOutstndAmt.subtract(recoverAmtCny) ;
        apprLmtSubBasicInfo.setOutstndAmt(afDetailsOutstndAmt);

        //已用敞口总额 -- 修改前
        BigDecimal bfDetailsSpacOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getSpacOutstndAmt()) ;
        //修改后
        BigDecimal afDetailsSpacOutstndAmt = bfDetailsSpacOutstndAmt.subtract(recoverSpacAmtCny) ;
        apprLmtSubBasicInfo.setSpacOutstndAmt(afDetailsSpacOutstndAmt);
    }

    /**
     * @作者:lizx
     * @方法名称: recoverLmtSigInfo
     * @方法描述:  恢复同业额度信息，代开银票，代开保函，占用客户自身额度的同时，占用代开银行额度信息，额度进行恢复时，只会推送客户的借据进行恢复，
     *            所以需要判断后，根据恢复类型进行是否需要恢复同业客户额度信息
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/10/5 17:31
     * @param contAccRel: 
     * @param resultMap: 
     * @return: void
     * @算法描述: 无
    */
    public void recoverLmtSigInfo(ContAccRel contAccRel, Map<String, String> resultMap, String recoverType, Map<String, BigDecimal> amtMap) {
        logger.info("恢复同业额度开始------------------------start");
        String tranAccNo = contAccRel.getTranAccNo() ;
        //查看该合同是否涉及占用同业类额度信息 类似代开银票等
        List<LmtContRel> lmtContRelSig = lmtContRelService.selectSigLmtByDealBizNo(tranAccNo) ;
        if(CollectionUtils.nonEmpty(lmtContRelSig)){
            for (LmtContRel contRel : lmtContRelSig) {
                String bizStatus = contRel.getBizStatus() ;
                if(CmisLmtConstants.STD_ZB_BIZ_STATUS_300.equals(bizStatus) || CmisLmtConstants.STD_ZB_BIZ_STATUS_400.equals(bizStatus)){
                    continue ;
                }
                String contBizAttr = contRel.getBizAttr() ;
                String conBizStatus = contRel.getBizStatus() ;
                if(CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(contBizAttr)){
                    String apprSubSerno = contRel.getLimitSubNo();
                    ApprLmtSubBasicInfo subBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(apprSubSerno) ;
                    //银行承兑汇票贴现，占用白名单额度，存在台账为lmt_cont_rel 的记录，此处应跳过
                    if(Objects.isNull(subBasicInfo)) return;

                    BigDecimal bizTotalBalanceAmtCny = NumberUtils.nullDefaultZero(contRel.getBizTotalBalanceAmtCny()) ;
                    BigDecimal bizSpacBalanceAmtCny = NumberUtils.nullDefaultZero(contRel.getBizSpacBalanceAmtCny()) ;
                    //撤销占用
                    if(CmisLmtConstants.STD_RECOVER_TYPE_06.equals(recoverType)){
                        //撤销占用无理由恢复额度
                        if(CmisLmtConstants.STD_ZB_BIZ_STATUS_200.equals(conBizStatus)){
                            subBasicInfo.setLoanBalance(subBasicInfo.getLoanBalance().subtract(bizTotalBalanceAmtCny));
                            subBasicInfo.setLoanSpacBalance(subBasicInfo.getLoanSpacBalance().subtract(bizSpacBalanceAmtCny));
                        }
                        subBasicInfo.setOutstndAmt(subBasicInfo.getOutstndAmt().subtract(bizTotalBalanceAmtCny));
                        subBasicInfo.setSpacOutstndAmt(subBasicInfo.getSpacOutstndAmt().subtract(bizSpacBalanceAmtCny));
                        subBasicInfo.setPvpOutstndAmt(subBasicInfo.getPvpOutstndAmt().subtract(bizSpacBalanceAmtCny));
                        subBasicInfo.setAvlOutstndAmt(subBasicInfo.getAvlOutstndAmt().add(bizSpacBalanceAmtCny));
                        apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(subBasicInfo, resultMap) ;

                        contRel.setBizSpacBalanceAmtCny(BigDecimal.ZERO);
                        contRel.setBizTotalBalanceAmtCny(BigDecimal.ZERO);
                        if(CmisLmtConstants.STD_RECOVER_TYPE_06.equals(recoverType)){
                            contRel.setOprType(CmisLmtConstants.OPR_TYPE_DELETE);
                        }
                        contRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_300);
                        lmtContRelService.updateByPKeyInApprLmtChgDetails(contRel, resultMap) ;
                    }else if((CmisLmtConstants.STD_RECOVER_TYPE_01.equals(recoverType))) {
                        //结清恢复
                        if(CmisLmtConstants.YES_NO_Y.equals(subBasicInfo.getIsRevolv())) {
                            subBasicInfo.setPvpOutstndAmt(subBasicInfo.getPvpOutstndAmt().subtract(bizSpacBalanceAmtCny));
                            subBasicInfo.setAvlOutstndAmt(subBasicInfo.getAvlOutstndAmt().add(bizSpacBalanceAmtCny));
                            subBasicInfo.setOutstndAmt(subBasicInfo.getOutstndAmt().subtract(bizTotalBalanceAmtCny));
                            subBasicInfo.setSpacOutstndAmt(subBasicInfo.getSpacOutstndAmt().subtract(bizSpacBalanceAmtCny));
                        }
                        if(CmisLmtConstants.STD_ZB_BIZ_STATUS_200.equals(conBizStatus)){
                            subBasicInfo.setLoanBalance(subBasicInfo.getLoanBalance().subtract(bizTotalBalanceAmtCny));
                            subBasicInfo.setLoanSpacBalance(subBasicInfo.getLoanSpacBalance().subtract(bizSpacBalanceAmtCny));
                        }
                        //更新额度分项
                        apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(subBasicInfo, resultMap) ;

                        contRel.setBizSpacBalanceAmtCny(BigDecimal.ZERO);
                        contRel.setBizTotalBalanceAmtCny(BigDecimal.ZERO);
                        contRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_300);
                        lmtContRelService.updateByPKeyInApprLmtChgDetails(contRel, resultMap) ;
                    }else{
                        bizTotalBalanceAmtCny = amtMap.get("recoverAmtCny") ;
                        bizSpacBalanceAmtCny = amtMap.get("recoverSpacAmtCny") ;
                        //结清恢复
                        if(CmisLmtConstants.STD_ZB_BIZ_STATUS_200.equals(conBizStatus)){
                            subBasicInfo.setLoanBalance(subBasicInfo.getLoanBalance().subtract(bizTotalBalanceAmtCny));
                            subBasicInfo.setLoanSpacBalance(subBasicInfo.getLoanSpacBalance().subtract(bizSpacBalanceAmtCny));
                        }
                        if(CmisLmtConstants.YES_NO_Y.equals(subBasicInfo.getIsRevolv())) {
                            subBasicInfo.setOutstndAmt(subBasicInfo.getOutstndAmt().subtract(bizTotalBalanceAmtCny));
                            subBasicInfo.setSpacOutstndAmt(subBasicInfo.getSpacOutstndAmt().subtract(bizSpacBalanceAmtCny));
                            subBasicInfo.setPvpOutstndAmt(subBasicInfo.getPvpOutstndAmt().subtract(bizSpacBalanceAmtCny));
                            subBasicInfo.setAvlOutstndAmt(subBasicInfo.getAvlOutstndAmt().add(bizSpacBalanceAmtCny));
                        }
                        //更新额度分项
                        apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(subBasicInfo, resultMap) ;

                        BigDecimal afBizSpacBalanceAmtCny = contRel.getBizSpacBalanceAmtCny().subtract(bizSpacBalanceAmtCny) ;
                        BigDecimal afBizTotalBalanceAmtCny = contRel.getBizTotalBalanceAmtCny().subtract(bizSpacBalanceAmtCny) ;
                        contRel.setBizSpacBalanceAmtCny(afBizSpacBalanceAmtCny);
                        contRel.setBizTotalBalanceAmtCny(afBizTotalBalanceAmtCny);
                        if(afBizSpacBalanceAmtCny.compareTo(BigDecimal.ZERO)==0 && afBizSpacBalanceAmtCny.compareTo(BigDecimal.ZERO)== 0) {
                            contRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_300);
                        }
                        lmtContRelService.updateByPKeyInApprLmtChgDetails(contRel, resultMap) ;
                    }
                }
            }
        }
        logger.info("恢复同业额度结束------------------------end");
    }

    /**
     * @作者:lizx
     * @方法名称: initApprLmtChgDetail
     * @方法描述:  初始化留痕记录表数据信息
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/7/16 16:24
     * @param paramHamp: 
     * @return: cn.com.yusys.yusp.domain.ApprLmtChgDetail
     * @算法描述: 无
    */
    public ApprLmtChgDetail initApprLmtChgDetail(Map<String,String> paramHamp) {
        ApprLmtChgDetail apprLmtChgDetail = new ApprLmtChgDetail() ;
        apprLmtChgDetail.setPkId(generatePkId());
        apprLmtChgDetail.setInputBrId(paramHamp.get("inputBrId"));
        apprLmtChgDetail.setInputId(paramHamp.get("inputId"));
        apprLmtChgDetail.setInputDate(paramHamp.get("inputDate"));
        apprLmtChgDetail.setCreateTime(getCurrrentDate());
        apprLmtChgDetail.setUpdateTime(getCurrrentDate());
        apprLmtChgDetail.setUpdDate(paramHamp.get("inputDate"));
        apprLmtChgDetail.setUpdId(paramHamp.get("inputId"));
        apprLmtChgDetail.setUpdBrId(paramHamp.get("inputBrId"));
        apprLmtChgDetail.setSerno(paramHamp.get("serno"));
        apprLmtChgDetail.setServiceCode(paramHamp.get("serviceCode"));
        return apprLmtChgDetail ;
    }

    /**
     * 通用当前是时间
     * @return
     */
    public Date getCurrrentDate(){
        return DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue());
    }

    /**
     * 根据大额行号获取对应客户号
     */
    public String queryCusIdByBankNo(String bankNo){
        String cusId = "";

        CmisCfg0003ReqDto cmisCfg0003ReqDto = new CmisCfg0003ReqDto();
        cmisCfg0003ReqDto.setBankNo(bankNo);
        CmisCfg0003RespDto cfgData = dscmsCfgQtClientService.cmisCfg0003(cmisCfg0003ReqDto).getData();
        if(SuccessEnum.SUCCESS.key.equals(cfgData.getErrorCode())){
            String superBankNo = cfgData.getSuperBankNo();

            CmisCus0023ReqDto cmisCus0023ReqDto = new CmisCus0023ReqDto();
            cmisCus0023ReqDto.setBankNo(superBankNo);
            CmisCus0023RespDto cusData = cmisCusClientService.cmiscus0023(cmisCus0023ReqDto).getData();

            if(SuccessEnum.SUCCESS.key.equals(cusData.getErrorCode())){
                cusId = cusData.getCusId();
            }
        }
        return cusId;
    }

    /**
     * @作者:lizx
     * @方法名称: judgeDealIsSuss
     * @方法描述: 判断交易已经成功，防止重复发送占用
     * @参数与返回说明:boolean  true 已经成功  false 未成功
     * @算法描述: 无
     * @日期：2021/8/9 9:43
     * @return: boolean
     * @算法描述: 无
    */
    public boolean judgeLmtDealIsSuss(String limitSubNo, String dealBizNo, String sysId){
        boolean result = false ;
        //LmtContRel lmtContRel = lmtContRelService.selectByLmtSubNoAndDealNo(limitSubNo, dealBizNo, sysId) ;
        LmtContRel lmtContRel = lmtContRelService.selectLCRelByDBizNoAngSubNo(dealBizNo, limitSubNo, sysId) ;
        if(Objects.nonNull(lmtContRel)){
            result =  true ;
        }
        logger.info("判断该【{}】交易是否已经成功，返回结果【{}】", dealBizNo, result);
        return result ;
    }

    /**
     * @作者:lizx
     * @方法名称: judgeDealIsSuss
     * @方法描述: 判断交易已经成功，防止重复发送占用
     * @参数与返回说明:boolean  true 已经成功  false 未成功
     * @算法描述: 无
     * @日期：2021/8/9 9:43
     * @return: boolean
     * @算法描述: 无
     */
    public boolean judgeContDealIsSuss(String tranAccNo, String dealBizNo,String sysId){
        boolean result = false ;
        ContAccRel contAccRel = contAccRelService.selectByTranAccNoAndDealNo(tranAccNo, dealBizNo,sysId) ;
        if(Objects.nonNull(contAccRel)){
            result =  true ;
        }
        logger.info("判断该【{}】交易是否已经成功，返回结果【{}】", tranAccNo, result);
        return result ;
    }


    /**
     * @作者:lizx
     * @方法名称: coopLmtBizTypePropCheck
     * @方法描述:  合作方校验授信产品类型是否允许通过
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/8/18 20:09
     * @param reqLmtBizTypeProp:
     * @param apprCoopSubInfo: 
     * @return: void
     * @算法描述: 无
    */
    public void coopLmtBizTypePropCheck(String reqLmtBizTypeProp, ApprCoopSubInfo apprCoopSubInfo) {
        //授信品种类型属性 分项中
        String subLmtBizTypeProp = apprCoopSubInfo.getLmtBizTypeProp() ;
        //占用合作方额度时，调整产品类型属性的字段控制，如果是专业担保公司额度，则使用产品类型属性进行匹配，如果匹配不到，类型为一般担保担保额度也允许占用；其余合作方额度，不控制产品类型属性字段
        if(StringUtils.isNotEmpty(subLmtBizTypeProp)){
            if(!IsEqualsTwoStr(subLmtBizTypeProp, reqLmtBizTypeProp)){
                logger.info("【{}】检验授信品种类型属性不相等，分项中属性值【{}】，接口中属性值【{}】", DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key,
                        subLmtBizTypeProp, reqLmtBizTypeProp);
                throw new YuspException(EclEnum.ECL070086.key, EclEnum.ECL070086.value);
            }
        }
    }


    /**
     * @作者:lizx
     * @方法名称: recoverAvlAndPvpOutstndAmt
     * @方法描述: 根据低风险字段，恢复可出账金额和已出账金额
     * @参数与返回说明: 如果是低风险，则可出账已处长金额，根据恢复金额字段进行处理，如果是非低风险，则可出账金额已出账金额根据敞口金额进行处理
     * @算法描述: 无
     * @日期：2021/8/26 21:40
     * @param apprLmtSubBasicInfo: 
     * @param recoverAmtCny: 
     * @param recoverSpacAmtCny: 
     * @return: void
     * @算法描述: 无
    */
    public void recoverAvlAndPvpOutstndAmt(ApprLmtSubBasicInfo apprLmtSubBasicInfo, BigDecimal recoverAmtCny, BigDecimal recoverSpacAmtCny){
        logger.info("处理可出账金额开始-----------------------start");
        //是否低风险
        String isLriskLmt = apprLmtSubBasicInfo.getIsLriskLmt() ;
        //修改前 -- 已出账金额
        BigDecimal bfPvpOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getPvpOutstndAmt()) ;
        //修改前 --可出账金额
        BigDecimal bfAvlOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getAvlOutstndAmt()) ;

        if(CmisLmtConstants.YES_NO_Y.equals(isLriskLmt)){
            apprLmtSubBasicInfo.setPvpOutstndAmt(bfPvpOutstndAmt.subtract(recoverAmtCny));
            apprLmtSubBasicInfo.setAvlOutstndAmt(bfAvlOutstndAmt.add(recoverAmtCny));
        }else{
            apprLmtSubBasicInfo.setPvpOutstndAmt(bfPvpOutstndAmt.subtract(recoverSpacAmtCny));
            apprLmtSubBasicInfo.setAvlOutstndAmt(bfAvlOutstndAmt.add(recoverSpacAmtCny));
        }
        logger.info("处理可出账金额结束，【bfPvpOutstndAmt】{}，【bfAvlOutstndAmt】{}-----------------------end", bfPvpOutstndAmt, bfAvlOutstndAmt);
    }

    /**
     * @作者:lizx
     * @方法名称: occAvlAndPvpOutstndAmt
     * @方法描述: 根据低风险字段，恢复可出账金额和已出账金额
     * @参数与返回说明: 如果是低风险，则可出账已处长金额，根据恢复金额字段进行处理，如果是非低风险，则可出账金额已出账金额根据敞口金额进行处理
     * @算法描述: 无
     * @日期：2021/8/26 21:40
     * @param apprLmtSubBasicInfo:
     * @param occAmtCny:
     * @param occSpacAmtCny:
     * @return: void
     * @算法描述: 无
     */
    public void occAvlAndPvpOutstndAmt(ApprLmtSubBasicInfo apprLmtSubBasicInfo, BigDecimal occAmtCny, BigDecimal occSpacAmtCny){
        logger.info("处理已出账金额开始【{}】------------start", apprLmtSubBasicInfo.getApprSubSerno());
        //是否低风险
        String isLriskLmt = apprLmtSubBasicInfo.getIsLriskLmt() ;
        //修改前 -- 已出账金额
        BigDecimal bfPvpOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getPvpOutstndAmt()) ;
        //修改前 --可出账金额
        BigDecimal bfAvlOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getAvlOutstndAmt()) ;

        if(CmisLmtConstants.YES_NO_Y.equals(isLriskLmt)){
            apprLmtSubBasicInfo.setPvpOutstndAmt(bfPvpOutstndAmt.add(occAmtCny));
            apprLmtSubBasicInfo.setAvlOutstndAmt(bfAvlOutstndAmt.subtract(occAmtCny));
        }else{
            apprLmtSubBasicInfo.setPvpOutstndAmt(bfPvpOutstndAmt.add(occSpacAmtCny));
            apprLmtSubBasicInfo.setAvlOutstndAmt(bfAvlOutstndAmt.subtract(occSpacAmtCny));
        }
        logger.info("处理已出账金额开始------------end");
    }

    /**
     * @作者:lizx
     * @方法名称: checkRecoverLmtContRelBal
     * @方法描述: 判断是否恢复合同余额
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/8/30 22:01

     * @return: boolean
     * @算法描述: 无
    */
    public boolean checkRecoverLmtContRelBal(String dealBizType, String bizStatus){
        //add by 2021-08-30 占用敞口余额，只有最高额合同/最高额授信协议，未到期的情况下，才不进行恢复，其余情况都恢复
        //最高额协议未到期
        if(CmisLmtConstants.STD_ZB_CONT_TYPE_3.equals(dealBizType) && !bizStatus.equals(CmisLmtConstants.STD_ZB_BIZ_STATUS_500)){
            logger.info("是否恢复合同信息结果:【false】");
            return false ;
        }
        //最高额合同未到期
        if(CmisLmtConstants.STD_ZB_CONT_TYPE_2.equals(dealBizType) && !bizStatus.equals(CmisLmtConstants.STD_ZB_BIZ_STATUS_500)){
            logger.info("是否恢复合同信息结果:【false】");
            return false ;
        }
        logger.info("是否恢复合同信息结果:【true】");
        return true ;
    }

    /**
     * @作者:lizx
     * @方法名称: checkRecoverLmtApprBal
     * @方法描述: 是否恢复额度金额校验(前提是分项可循环)
     * @参数与返回说明: true  恢复   false  不回复
     * @算法描述: 无
     * @日期：2021/8/30 22:15
     * @param dealBizType:
     * @param bizStatus:
     * @return: boolean
     * @算法描述: 无
    */
    public boolean checkRecoverLmtApprBal(String dealBizType, String bizStatus){
        //add by 2021-08-30 占用敞口余额，只有最高额合同/最高额授信协议，未到期的情况下，才不进行恢复，其余情况都恢复
        boolean result = false ;
        if(CmisLmtConstants.STD_ZB_CONT_TYPE_1.equals(dealBizType)){
            result = true ;
        }
        //最高额协议到期
        if(CmisLmtConstants.STD_ZB_CONT_TYPE_3.equals(dealBizType) && bizStatus.equals(CmisLmtConstants.STD_ZB_BIZ_STATUS_500)){
            result = true ;
        }
        //最高额合同到期
        if(CmisLmtConstants.STD_ZB_CONT_TYPE_2.equals(dealBizType) && bizStatus.equals(CmisLmtConstants.STD_ZB_BIZ_STATUS_500)){
            result = true ;
        }
        logger.info("是否恢复额度分项信息：{}", result);
        return result ;
    }

    /**
     * 产品编号和额度体系类型查询对应的额度产品编号
     * @param prdId
     * @param limitStrNo
     * @return
     */
    public String getLimitSubNoByPrdIdAndStrNo(String prdId, String limitStrNo){
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("prdId", prdId);
        queryModel.addCondition("limitStrNo", limitStrNo);
        List<String> limitSubNoList = lmtSubPrdMappConfService.selectLimitSubNoListByLimitStrNoAndPrdId(queryModel);
        String limitSubNo = "" ;
        if(org.apache.commons.collections.CollectionUtils.isNotEmpty(limitSubNoList)){
            limitSubNo = limitSubNoList.get(0) ;
        }
        return limitSubNo ;
    }

    /**
     * 产品编号和额度体系类型查询对应的额度产品编号
     * @param prdId
     * @param limitStrNo
     * @return
     */
    public String selectLimitSubNoListByParam(String prdId, String limitStrNo, String isLriskLmt){
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("prdId", prdId);
        queryModel.addCondition("limitStrNo", limitStrNo);
        queryModel.addCondition("isLriskLmt", isLriskLmt);
        List<String> limitSubNoList = lmtSubPrdMappConfService.selectLimitSubNoListByParam(queryModel);
        String limitSubNo = "" ;
        if(org.apache.commons.collections.CollectionUtils.isNotEmpty(limitSubNoList)){
            limitSubNo = limitSubNoList.get(0) ;
        }
        return limitSubNo ;
    }


    /**
     * 查询客户（综合授信+主体授信）敞口用信余额，不包含穿透化额度、承销额度
     * @param cusId,instuCde
     * @return
     * add by zhangjw 20210905
     */
    public BigDecimal selectLoanSpacBalanceAmtByCusId(String cusId,String instuCde){
        QueryModel model = new QueryModel();
        model.addCondition("cusId",cusId);
        model.addCondition("instuCde",instuCde);
        return apprStrMtableInfoService.selectLoanSpacBalanceAmtByCusId(model);
    }

    /**
     * 查询客户（综合授信+主体授信）敞口用信余额 及敞口金额，不包含穿透化额度、承销额度
     * @param cusIds,instuCde
     * @return
     * add by zhangjw 20210905
     */
    public Map<String,Object> calLmtAntLoanSpacBalanceAmt(String cusIds,String instuCde){
        QueryModel model = new QueryModel();
        model.addCondition("cusIds",cusIds);
        model.addCondition("instuCde",instuCde);
        return apprStrMtableInfoService.calLmtAntLoanSpacBalanceAmt(model);
    }

    /**
     * @作者:lizx
     * @方法名称: bizRevExecute
     * @方法描述: 合同提前终止
     *          判断合同向下是否存在未结清业务，
     *          如果存在，则将合同更改为500-到期未结清状态，则将合同对应业务余额更改为向下借据余额，并且更改占用分项的已用总额已用敞口金额
     *          如果不存在，且向下没有发生过业务，则将合同更改未400-结清未适用状态，反之则将合同更改未300-失效已结清状态，并且更改占用分项额度信息
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/9/26 23:08
     * @param origiDealBizNo:
     * @param resultMap:
     * @return: void
     * @算法描述: 无
    */
    public void bizRevExecute(String origiDealBizNo, Map<String, String> resultMap){
        //恢复合同金额 ，结清恢复，不需要处理分项已用总额和已用敞口总额
        List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelListByDealBizNo(origiDealBizNo, CmisLmtConstants.STD_ZB_BIZ_ATTR_1) ;
        logger.info("【{}】额度占用合同重签，合同信息{}",DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key ,JSON.toJSONString(lmtContRelList));
        if(org.apache.commons.collections4.CollectionUtils.isNotEmpty(lmtContRelList)){
            for (LmtContRel lmtContRel : lmtContRelList) {
                String lmtType = lmtContRel.getLmtType() ;
                String dealBizNoStr = lmtContRel.getDealBizNo() ;
                Map<String, BigDecimal> amtMap = contAccRelService.getContAccRelBal(dealBizNoStr,CmisLmtConstants.STD_ZB_BIZ_STATUS_STR) ;
                BigDecimal accAlance = amtMap.get("accalance") ;
                BigDecimal accSpacAlance = amtMap.get("accspacalance") ;
                //恢复金额
                BigDecimal bizRevAccAlbance = BigDecimalUtil.replaceNull(lmtContRel.getBizTotalBalanceAmtCny()).subtract(accAlance) ;
                BigDecimal bizRevAccSpacAlance = BigDecimalUtil.replaceNull(lmtContRel.getBizSpacBalanceAmtCny()).subtract(accSpacAlance) ;
                //对额度进行恢复操作
                String apprSubSerno = lmtContRel.getLimitSubNo() ;

                if(CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(lmtType)){
                    logger.info("法人额度合同提前终止处理---------------start");
                    earlyEndContInfo(origiDealBizNo, resultMap, lmtContRel);
                    //获取额度信息
                    ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(apprSubSerno) ;
                    if(Objects.isNull(apprLmtSubBasicInfo)){
                        throw new YuspException(EclEnum.ECL070007.key, "合同重签处理法人额度分项【"+apprSubSerno+"】" + EclEnum.ECL070007.value);
                    }
                    //如果额度是可循环的，则进行已用额度恢复恢复操作
                    if(CmisLmtConstants.YES_NO_Y.equals(apprLmtSubBasicInfo.getIsRevolv())){
                        logger.info("法人额度循环额度，更改已用额度信息");
                        apprLmtSubBasicInfo.setOutstndAmt(apprLmtSubBasicInfo.getOutstndAmt().subtract(bizRevAccAlbance));
                        apprLmtSubBasicInfo.setSpacOutstndAmt(apprLmtSubBasicInfo.getSpacOutstndAmt().subtract(bizRevAccSpacAlance));
                        apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfo, resultMap) ;

                        /** 处理上级分项额度数据 **/
                        String parentId = apprLmtSubBasicInfo.getParentId() ;
                        ApprLmtSubBasicInfo parentIdSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(parentId) ;
                        if(Objects.nonNull(parentIdSubBasicInfo)){
                            parentIdSubBasicInfo.setOutstndAmt(parentIdSubBasicInfo.getOutstndAmt().subtract(bizRevAccAlbance));
                            parentIdSubBasicInfo.setSpacOutstndAmt(parentIdSubBasicInfo.getSpacOutstndAmt().subtract(bizRevAccSpacAlance));
                            apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(parentIdSubBasicInfo, resultMap) ;
                        }
                    }
                    logger.info("法人额度合同提前终止处理---------------end");
                }else if(CmisLmtConstants.STD_ZB_LMT_TYPE_03.equals(lmtType)){
                    logger.info("合同额度合同提前终止处理---------------start");
                    //获取额度信息
                    ApprCoopSubInfo apprCoopSubInfo = apprCoopSubInfoService.selectBySubSerno(apprSubSerno) ;
                    if(Objects.isNull(apprCoopSubInfo)){
                        throw new YuspException(EclEnum.ECL070007.key, "合同重签处理合作方分项【"+apprSubSerno+"】" + EclEnum.ECL070007.value);
                    }
                    if(CmisLmtConstants.YES_NO_Y.equals(apprCoopSubInfo.getIsRevolv())){
                        logger.info("合作方循环额度，更改已用额度信息");
                        apprCoopSubInfo.setOutstndAmt(NumberUtils.nullDefaultZero(apprCoopSubInfo.getOutstndAmt().subtract(bizRevAccAlbance)));
                    }
                    earlyEndContInfo(origiDealBizNo, resultMap, lmtContRel);
                    logger.info("合同额度合同提前终止处理---------------start");
                }else{
                    logger.info(this.getClass().getName() + "不支持的额度类型，不进行处理---------------");
                }
            }
        }else{
            logger.info("【{}】额度占用合同重签，未查询你到信息，此处跳过不处理",DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key);
        }
    }

    /**
     * @作者:lizx
     * @方法名称: earlyEndContInfo
     * @方法描述:  合同提前终止，更改合同金额以及状态等信息
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/11/18 16:21
     * @param origiDealBizNo: 
     * @param resultMap: 
     * @param lmtContRel: 
     * @return: void
     * @算法描述: 无
    */
    private void earlyEndContInfo(String origiDealBizNo, Map<String, String> resultMap, LmtContRel lmtContRel) {
        //查询向下未结清的余额
        String dealBizNoStr = lmtContRel.getDealBizNo() ;
        Map<String, BigDecimal> amtMap = contAccRelService.getContAccRelBal(dealBizNoStr,CmisLmtConstants.STD_ZB_BIZ_STATUS_STR) ;
        BigDecimal accAlance = amtMap.get("accalance") ;
        BigDecimal accSpacAlance = amtMap.get("accspacalance") ;

        //向下不存在未结清业务，则更改状态为失效已结清
        if(accAlance.compareTo(BigDecimal.ZERO)==0 && accSpacAlance.compareTo(BigDecimal.ZERO)==0){
            lmtContRel.setBizTotalBalanceAmtCny(BigDecimal.ZERO);
            lmtContRel.setBizSpacBalanceAmtCny(BigDecimal.ZERO);
            if(contAccRelService.validCreditrecord(origiDealBizNo)){
                lmtContRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_300);
            }else{
                lmtContRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_400);
            }
        }else{
            //向下存在未结清业务，则更改状态为到期未结清
            lmtContRel.setBizTotalBalanceAmtCny(accAlance);
            lmtContRel.setBizSpacBalanceAmtCny(accSpacAlance);
            lmtContRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_500);
        }
        lmtContRelService.updateByPKeyInApprLmtChgDetails(lmtContRel, resultMap) ;
    }

    /**
     * 恢复批复主信息额度
     * @param amtMap
     * @param lmtContRel
     * @param amtMap
     */
    public void recoverApprLmtSubBasicInfo(ContAccRel contAccRel, LmtContRel lmtContRel, Map<String, BigDecimal> amtMap, Map<String, String> resultMap){
        logger.info("【recoverApprLmtSubBasicInfo】还款操作----------setart");
        //恢复总额
        BigDecimal recoverAmtCny = amtMap.get("recoverAmtCny") ;
        //恢复敞口金额
        BigDecimal recoverSpacAmtCny = amtMap.get("recoverSpacAmtCny");
        if(CmisLmtConstants.STD_ZB_LMT_TYPE_07.equals(lmtContRel.getLmtType())){
            if(BigDecimal.ZERO.compareTo(recoverSpacAmtCny)==0){
                recoverSpacAmtCny = recoverAmtCny ;
            }
        }
        //分项编号
        String subSerno = lmtContRel.getLimitSubNo();
        //如果分项编号为空，则跳过处理
        if(cn.com.yusys.yusp.commons.util.StringUtils.isBlank(subSerno)){
            throw new YuspException(EclEnum.ECL070126.key,EclEnum.ECL070126.value);
        }
        //查询分项信息
        ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(subSerno);
        Optional.ofNullable(apprLmtSubBasicInfo).orElseThrow(()->new YuspException(EclEnum.ECL070007.key,EclEnum.ECL070007.value));
        //交易业务类型    1-一般合同  2-最高额合同  3-最高额授信协议
        String dealBizType = lmtContRel.getDealBizType() ;
        String bizAttr = lmtContRel.getBizAttr() ;
        String bizStatus = lmtContRel.getBizStatus() ;

        //如果是最高协议，额度恢复时，恢复二级分项
        if(CmisLmtConstants.STD_ZB_CONT_TYPE_3.equals(dealBizType) && !CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
            logger.info("【recoverApprLmtSubBasicInfo】还款操作【3-最高协议】----------setart");

            //add by 2021-08-30 占用敞口余额，只有最高额合同/最高额授信协议，未到期的情况下，才不进行恢复，其余情况都恢复
            if(checkRecoverLmtContRelBal(dealBizType, bizStatus)){
                //恢复合同金额
                recoverLmtcontRelBal(lmtContRel, amtMap, resultMap);
            }
            //考虑到分项中是否可循环，是否低风险没有值，所以分项和分项明细一起恢复
            if(CmisLmtConstants.YES_NO_Y.equals(apprLmtSubBasicInfo.getIsRevolv())){
                if(checkRecoverLmtApprBal(dealBizType, bizStatus)){
                    //恢复合同台账金额
                /*    List<LmtContRel> lmtContRelList = lmtContRelMapper.selectLmtContRelByDealBizNo(contAccRel.getTranAccNo()) ;
                    if(org.apache.commons.collections4.CollectionUtils.isNotEmpty(lmtContRelList)){
                        LmtContRel contRel = lmtContRelList.get(0) ;
                        //恢复该合同金额
                        BigDecimal bfBizTotalBalanceAmtCny = BigDecimalUtil.replaceNull(contRel.getBizTotalBalanceAmtCny()) ;
                        //修改后
                        BigDecimal afBizTotalBalanceAmtCny = bfBizTotalBalanceAmtCny.subtract(recoverAmtCny) ;
                        lmtContRel.setBizTotalBalanceAmtCny(afBizTotalBalanceAmtCny);

                        //占用敞口余额（折人民币)
                        //修改前
                        BigDecimal bfBizSpacBalanceAmtCny = BigDecimalUtil.replaceNull(contRel.getBizSpacBalanceAmtCny()) ;
                        //修改后
                        BigDecimal afBizSpacBalanceAmtCny = bfBizSpacBalanceAmtCny.subtract(recoverSpacAmtCny) ;
                        contRel.setBizSpacBalanceAmtCny(afBizSpacBalanceAmtCny);
                        logger.info("【lmtContRel】恢复【"+ contRel.getDealBizNo()+"】,占用总余额（折人民币）更改为：" + bfBizTotalBalanceAmtCny + " --> " + afBizTotalBalanceAmtCny
                                +",占用敞口余额（折人民币)更改为：" + bfBizSpacBalanceAmtCny + " --> " + afBizSpacBalanceAmtCny
                        );
                        lmtContRelMapper.updateByPrimaryKey(contRel) ;
                    }*/

                    String isLriskLmt = apprLmtSubBasicInfo.getIsLriskLmt() ;
                    //修改前 （恢复分项信息）
                    BigDecimal bfSpacOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getSpacOutstndAmt()) ;
                    //修改后
                    BigDecimal afSpacOutstndAmt = bfSpacOutstndAmt.subtract(recoverSpacAmtCny) ;
                    //如果是非低风险，恢复敞口已用额度
                    if(CmisLmtConstants.YES_NO_N.equals(isLriskLmt)){
                        apprLmtSubBasicInfo.setSpacOutstndAmt(afSpacOutstndAmt);
                    }

                    //总额已用额度减少本次恢复合同总额金额
                    //修改前
                    BigDecimal bfOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getOutstndAmt()) ;
                    //修改后
                    BigDecimal afOutstndAmt = bfOutstndAmt.subtract(recoverAmtCny) ;
                    apprLmtSubBasicInfo.setOutstndAmt(afOutstndAmt);

                    logger.info("【ApprLmtSubBasicInfo】分项恢复恢复【"+apprLmtSubBasicInfo.getApprSubSerno()+"】,"
                            +"敞口已用额度【SpacOutstndAmt】更改为：" + bfSpacOutstndAmt + " --> " + afSpacOutstndAmt
                            +"敞口已用额度【OutstndAmt】更改为：" + bfOutstndAmt + " --> " + afOutstndAmt
                    );
                }

                //额度可循环才需要恢复可出账金额和已出账金额
                recoverAvlAndPvpOutstndAmt(apprLmtSubBasicInfo, recoverAmtCny, recoverSpacAmtCny) ;
            }
            String contStatus = contAccRel.getStatus() ;
            if(!CmisLmtConstants.STD_ZB_BIZ_STATUS_100.equals(contStatus)){
                updateLmtSubLoanBal(amtMap, apprLmtSubBasicInfo);
            }
            apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfo, resultMap) ;

            //处理向下分项明细以及额度占用关系中的台账信息，获取台账信息
            String tranAccNo = contAccRel.getTranAccNo() ;
            //根据用信编号获取额度占用的台账信息
            //List<LmtContRel> lmtContRelOcc = lmtContRelService.selectLmtContRelByDealBizNo(tranAccNo) ;
            List<LmtContRel> lmtContRelOcc = lmtContRelService.selectLmtContRelListByDealBizNo(tranAccNo, CmisLmtConstants.STD_ZB_BIZ_ATTR_2) ;
            for (LmtContRel contRel : lmtContRelOcc) {
                //台账敞口余额
                BigDecimal bizSpacBalanceAmtCny = BigDecimalUtil.replaceNull(contRel.getBizSpacBalanceAmtCny()) ;
                //台账总余额
                BigDecimal bizTotalBalanceAmtCny = BigDecimalUtil.replaceNull(contRel.getBizTotalBalanceAmtCny()) ;
                //恢复敞口余额后金额
                bizSpacBalanceAmtCny = bizSpacBalanceAmtCny.subtract(recoverSpacAmtCny) ;
                //恢复敞口总余额
                bizTotalBalanceAmtCny = bizTotalBalanceAmtCny.subtract(recoverAmtCny) ;
                if(bizSpacBalanceAmtCny.compareTo(BigDecimal.ZERO)==0 && bizTotalBalanceAmtCny.compareTo(BigDecimal.ZERO)==0){
                    contRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_300);
                }

                contRel.setBizSpacBalanceAmtCny(bizSpacBalanceAmtCny);
                contRel.setBizTotalBalanceAmtCny(bizTotalBalanceAmtCny);
                lmtContRelService.updateByPKeyInApprLmtChgDetails(contRel, resultMap) ;

                String apprSubSerno = contRel.getLimitSubNo() ;
                ApprLmtSubBasicInfo apprLmtSubBasicInfoDetails = apprLmtSubBasicInfoService.selectByApprSubSerno(apprSubSerno);//selectSubBasicInfoByParentId(subSerno ,prdId, CmisLmtConstants.STD_ZB_LMT_STR_MTABLE_CONF_01) ;
                if(Objects.isNull(apprLmtSubBasicInfoDetails)){
                    throw new YuspException(EclEnum.ECL070081.key,EclEnum.ECL070081.value);
                }

                apprLmtSubBasicInfoDetails.setSpacOutstndAmt(apprLmtSubBasicInfoDetails.getSpacOutstndAmt().subtract(recoverSpacAmtCny));
                apprLmtSubBasicInfoDetails.setOutstndAmt(apprLmtSubBasicInfoDetails.getOutstndAmt().subtract(recoverAmtCny));

                String isRevFlag = apprLmtSubBasicInfoDetails.getIsRevolv() ;
                if(CmisLmtConstants.YES_NO_Y.equals(isRevFlag)){
                    //额度可循环才需要恢复可出账金额和已出账金额
                    recoverAvlAndPvpOutstndAmt(apprLmtSubBasicInfoDetails, recoverAmtCny, recoverSpacAmtCny) ;
                }

                if(!CmisLmtConstants.STD_ZB_BIZ_STATUS_100.equals(contStatus)){
                    //updateLmtSubLoanBal(amtMap, apprLmtSubBasicInfoDetails, resultMap);
                    apprLmtSubBasicInfoDetails.setLoanSpacBalance(apprLmtSubBasicInfoDetails.getLoanSpacBalance().subtract(recoverSpacAmtCny));
                    apprLmtSubBasicInfoDetails.setLoanBalance(apprLmtSubBasicInfoDetails.getLoanBalance().subtract(recoverAmtCny));
                }
                apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfoDetails, resultMap) ;
            }
        }else{
            String parentId = apprLmtSubBasicInfo.getParentId() ;
            ApprLmtSubBasicInfo subBasicInfo = null ;
            if(cn.com.yusys.yusp.commons.util.StringUtils.nonBlank(parentId)){
                subBasicInfo = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(parentId);
            }

            //add by 2021-08-30 占用敞口余额，只有最高额合同/最高额授信协议，未到期的情况下，才不进行恢复，其余情况都恢复
            if(checkRecoverLmtContRelBal(dealBizType, bizStatus)){
                //恢复合同金额
                recoverLmtcontRelBal(lmtContRel, amtMap, resultMap);
            }

            if(CmisLmtConstants.YES_NO_Y.equals(apprLmtSubBasicInfo.getIsRevolv())){
                if(checkRecoverLmtApprBal(dealBizType, bizStatus)){
                    String isLriskLmt = apprLmtSubBasicInfo.getIsLriskLmt() ;
                    //敞口已用额度减少本次恢复合同总敞口金额
                    //修改前
                    BigDecimal bfSpacOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getSpacOutstndAmt()) ;
                    //修改后
                    BigDecimal afSpacOutstndAmt = bfSpacOutstndAmt.subtract(recoverSpacAmtCny) ;
                    if(CmisLmtConstants.YES_NO_N.equals(isLriskLmt)){
                        apprLmtSubBasicInfo.setSpacOutstndAmt(afSpacOutstndAmt);
                    }

                    //总额已用额度减少本次恢复合同总额金额 修改前
                    BigDecimal bfOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getOutstndAmt()) ;
                    //修改后
                    BigDecimal afOutstndAmt = bfOutstndAmt.subtract(recoverAmtCny) ;
                    apprLmtSubBasicInfo.setOutstndAmt(afOutstndAmt);

                    if(Objects.nonNull(subBasicInfo)){
                        //修改前
                        BigDecimal bfSubSpacOutstndAmt = BigDecimalUtil.replaceNull(subBasicInfo.getSpacOutstndAmt()) ;
                        //修改后
                        BigDecimal afSubSpacOutstndAmt = bfSubSpacOutstndAmt.subtract(recoverSpacAmtCny) ;

                        if(CmisLmtConstants.YES_NO_N.equals(isLriskLmt)){
                            subBasicInfo.setSpacOutstndAmt(afSubSpacOutstndAmt);
                        }

                        //修改前
                        BigDecimal bfSubOutstndAmt = BigDecimalUtil.replaceNull(subBasicInfo.getOutstndAmt()) ;
                        //修改后
                        BigDecimal afSubOutstndAmt = bfSubOutstndAmt.subtract(recoverAmtCny) ;
                        subBasicInfo.setOutstndAmt(afSubOutstndAmt);
                    }
                }

                /** 额度如果是循环的，则需要恢复可出账金额和已出账金额。 可出账金额=原可出账金额+本次归还金额  已出账金额=原已出账金额-本次恢复金额  （非低风险 可出账金额和已出账金额 指敞口金额） **/
                recoverAvlAndPvpOutstndAmt(apprLmtSubBasicInfo, recoverAmtCny, recoverSpacAmtCny) ;
                if(Objects.nonNull(subBasicInfo)){
                    //已出账金额 可出账金额
                    recoverAvlAndPvpOutstndAmt(subBasicInfo, recoverAmtCny, recoverSpacAmtCny) ;
                }
            }

            if((CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr) && !CmisLmtConstants.STD_ZB_BIZ_STATUS_100.equals(bizStatus))
                    || (Objects.nonNull(contAccRel) && !CmisLmtConstants.STD_ZB_BIZ_STATUS_100.equals(contAccRel.getStatus()))){
                //用信余额 修改前
                BigDecimal bfLoanBalance = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanBalance()) ;
                //修改后
                BigDecimal afLoanBalance = bfLoanBalance.subtract(recoverAmtCny) ;
                apprLmtSubBasicInfo.setLoanBalance(afLoanBalance);

                //用信敞口余额 修改前
                BigDecimal bfLoanSpacBalance = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanSpacBalance()) ;
                //修改后
                BigDecimal afLoanSpacBalance = bfLoanSpacBalance.subtract(recoverSpacAmtCny) ;
                apprLmtSubBasicInfo.setLoanSpacBalance(afLoanSpacBalance);
            }

            apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfo, resultMap) ;

            //-----------------------------处理上级分项信息--------------------------
            if(Objects.nonNull(subBasicInfo)){
                if((CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr) && !CmisLmtConstants.STD_ZB_BIZ_STATUS_100.equals(bizStatus))
                        || (Objects.nonNull(contAccRel) && !CmisLmtConstants.STD_ZB_BIZ_STATUS_100.equals(contAccRel.getStatus()))){
                    //用信余额 修改前
                    BigDecimal bfSubLoanBalance = BigDecimalUtil.replaceNull(subBasicInfo.getLoanBalance()) ;
                    //修改后
                    BigDecimal afSubLoanBalance = bfSubLoanBalance.subtract(recoverAmtCny) ;
                    subBasicInfo.setLoanBalance(afSubLoanBalance);

                    //用信敞口余额 修改前
                    BigDecimal bfSubLoanSpacBalance = BigDecimalUtil.replaceNull(subBasicInfo.getLoanSpacBalance()) ;
                    //修改后
                    BigDecimal afSubLoanSpacBalance = bfSubLoanSpacBalance.subtract(recoverSpacAmtCny) ;
                    subBasicInfo.setLoanSpacBalance(afSubLoanSpacBalance);
                }
                apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(subBasicInfo, resultMap) ;
            }
            //apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfo, resultMap) ;
        }
        logger.info("【recoverApprLmtSubBasicInfo】还款操作----------end");
    }

    public void recoverLmtcontRelBal(LmtContRel lmtContRel, Map<String, BigDecimal> amtMap, Map<String, String> resultMap) {
        //合同类型 1 一般合同 2 最高额合同 3 最高额协议
        //String dealBizType = lmtContRel.getDealBizType() ;
        //一般额合同，还款时，恢复额度金额  || 最高额合同，判断合同是否到期未结清，如果是，恢复最高额
        //恢复合同金额  占用总余额（折人民币)
        //修改前
        BigDecimal bfBizTotalBalanceAmtCny = BigDecimalUtil.replaceNull(lmtContRel.getBizTotalBalanceAmtCny()) ;
        //修改后
        BigDecimal afBizTotalBalanceAmtCny = bfBizTotalBalanceAmtCny.subtract(amtMap.get("recoverAmtCny")) ;
        lmtContRel.setBizTotalBalanceAmtCny(afBizTotalBalanceAmtCny);

        //占用敞口余额（折人民币)
        //修改前
        BigDecimal bfBizSpacBalanceAmtCny = BigDecimalUtil.replaceNull(lmtContRel.getBizSpacBalanceAmtCny()) ;
        //修改后
        BigDecimal afBizSpacBalanceAmtCny = bfBizSpacBalanceAmtCny.subtract(amtMap.get("recoverSpacAmtCny")) ;
        lmtContRel.setBizSpacBalanceAmtCny(afBizSpacBalanceAmtCny);

        //台账信息，如果已经结清，则将状态更改为结清已使用（注销）
        if(CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(lmtContRel.getBizAttr())){
            if(afBizTotalBalanceAmtCny.compareTo(BigDecimal.ZERO)==0 && afBizSpacBalanceAmtCny.compareTo(BigDecimal.ZERO)==0){
                lmtContRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_300);
            }
        }

        logger.info("【lmtContRel】恢复【"+ lmtContRel.getDealBizNo()+"】,占用总余额（折人民币）更改为：" + bfBizTotalBalanceAmtCny + " --> " + afBizTotalBalanceAmtCny
                +",占用敞口余额（折人民币)更改为：" + bfBizSpacBalanceAmtCny + " --> " + afBizSpacBalanceAmtCny
        );
        lmtContRelService.updateByPKeyInApprLmtChgDetails(lmtContRel, resultMap) ;
    }

    public void updateLmtSubLoanBal(Map<String, BigDecimal> amtMap, ApprLmtSubBasicInfo apprLmtSubBasicInfo) {
        //用信余额
        //修改前
        BigDecimal bfLoanBalance = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanBalance()) ;
        //修改后
        BigDecimal afLoanBalance = bfLoanBalance.subtract(amtMap.get("recoverAmtCny")) ;
        apprLmtSubBasicInfo.setLoanBalance(afLoanBalance);

        //用信敞口余额
        //修改前
        BigDecimal bfLoanSpacBalance = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanSpacBalance()) ;
        //修改后
        BigDecimal afLoanSpacBalance = bfLoanSpacBalance.subtract(amtMap.get("recoverSpacAmtCny")) ;
        apprLmtSubBasicInfo.setLoanSpacBalance(afLoanSpacBalance);
    }

    /**
     * 贴现业务出账成功后，根据登记机构和申请年月至LMT_DISC_ORG查找状态为生效的数据，将贴现金额累加至USE_AMT已使用额度上
     * 	查询出来的多条记录，判断若已用金额>=核准金额，则不处理；否则，判断若核准金额- 已用金额>=本次贴现申请总金额，
     * 	则将本次贴现申请金额累加至USE_AMT已使用额度并返回；若核准金额- 已用金额<本次贴现申请总金额，则将差额累加至USE_AMT已使用额度上，剩余贴现金额继续做累加.
     * 	贴现业务：prd_id = 052198 银行承兑汇票
     */
    public void lmtLastDiscOrgAddUseAmt(Map<String, String> resultMap, BigDecimal occBizAmtCny, String prdId, String inputBrId) {
        logger.info("贴现业务出账成功,贴现累加处理开始");
        //当前系统日期
        String currentDate = DateUtils.getCurrDateStr() ;
        String currYearMonth = currentDate.substring(0, currentDate.lastIndexOf("-")) ;
        logger.info("贴现业务出账成功产品{},机构{},日期{}", prdId, inputBrId, currYearMonth);
        if(CmisLmtConstants.STD_PJ_PRD_ID_052198.equals(prdId)){
            //查询LMT_DISC_ORG 表记录
            List<LmtDiscOrg> lmtDiscOrgs = lmtDiscOrgService.queryDetailByOrgAndYm(inputBrId, currYearMonth) ;
            // 如果是空，不处理
            if(org.apache.commons.collections4.CollectionUtils.isNotEmpty(lmtDiscOrgs)){
                LmtDiscOrg lmtLastDiscOrg = new LmtDiscOrg() ;
                for (LmtDiscOrg lmtDiscOrg : lmtDiscOrgs) {
                    lmtLastDiscOrg = lmtDiscOrg ;
                    if(occBizAmtCny.compareTo(BigDecimal.ZERO)<=0){
                        break ;
                    }
                    //核准金额
                    BigDecimal approveAmount = BigDecimalUtil.replaceNull(lmtDiscOrg.getApproveAmount()) ;
                    //已用金额
                    BigDecimal useAmt = BigDecimalUtil.replaceNull(lmtDiscOrg.getUseAmt()) ;
                    //核准金额 - 已用金额
                    BigDecimal subAmount = approveAmount.subtract(useAmt) ;
                    if(subAmount.compareTo(BigDecimal.ZERO)>0 && subAmount.compareTo(occBizAmtCny)>=0){
                        logger.info("贴现业务机构信息{},满足该占用，占用清0，本次更新贴现机构用额结束", JSON.toJSONString(lmtLastDiscOrg));
                        lmtDiscOrg.setUseAmt(useAmt.add(occBizAmtCny));
                        occBizAmtCny = BigDecimal.ZERO ;
                    }else if(subAmount.compareTo(BigDecimal.ZERO)>0 && subAmount.compareTo(occBizAmtCny)<0){
                        occBizAmtCny = occBizAmtCny.subtract(subAmount) ;
                        logger.info("贴现业务机构信息{},不满足该占用，更新占用后金额{}", JSON.toJSONString(lmtLastDiscOrg), occBizAmtCny);
                        lmtDiscOrg.setUseAmt(useAmt.add(subAmount));
                    }
                    lmtDiscOrgService.updateByPKeyInApprLmtChgDetails(lmtDiscOrg, resultMap) ;
                }

                if(occBizAmtCny.compareTo(BigDecimal.ZERO)>0){
                    logger.info("贴现业务机构信息{},不满足该占用，全部更新的到该信息上", JSON.toJSONString(lmtLastDiscOrg), occBizAmtCny);
                    //已用金额
                    BigDecimal useAmt = BigDecimalUtil.replaceNull(lmtLastDiscOrg.getUseAmt()) ;
                    lmtLastDiscOrg.setUseAmt(useAmt.add(occBizAmtCny));
                    lmtDiscOrgService.updateByPKeyInApprLmtChgDetails(lmtLastDiscOrg, resultMap) ;
                }
            }
        }
        logger.info("贴现业务出账成功,贴现累加处理结束");
    }


    /**
     * @作者:lizx
     * @方法名称: listToString
     * @方法描述:  字符串集合转化为字符串
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/9/17 14:04
     * @param list:
     * @return: java.lang.String
     * @算法描述: 无
    */
    public String listToString(List<String> list){
        String delimiter = "," ;
        StringBuilder sb = new StringBuilder((list.isEmpty() ? "":list.get(0))) ;
        for (int i = 1; i < list.size(); i++) {
            sb.append(delimiter).append(list.get(i)) ;
        }
        return sb.toString() ;
    }

    /**
     * @作者:lizx
     * @方法名称: checkStrInSourceStr
     * @方法描述: 判断
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/9/18 9:54
     * @param sourceStr: 
     * @param targetStr: 
     * @return: boolean
     * @算法描述: 无
    */
    public boolean checkStrInSourceStr(String sourceStr, String targetStr){
        String[] sourceArr = sourceStr.split(",") ;
        for (int i = 0; i < sourceArr.length; i++) {
            String s = sourceArr[i];
            if(s.equals(targetStr)){
                return true ;
            }
        }
        return false ;
    }


    /**
     * @作者:lizx
     * @方法名称: checkProGuarComRev
     * @方法描述:  检查担保公司是否重复占用
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/10/6 10:26
     * @return: boolean
     * @算法描述: 无
    */
    public boolean check09ProGuarComRev(List<CmisLmt0009OccRelListReqDto> object, String guarCusId){
        boolean result = true ;
        logger.info("判断是否担保公司重复占用--9 ------------ start");
        for (CmisLmt0009OccRelListReqDto objectBean : object) {
            String apprSubSerno = "" ;
            if(CmisLmtConstants.YES_NO_Y.equals(objectBean.getIsProGuarCom())
                    || CmisLmtConstants.STD_ZB_LMT_TYPE_03.equals(objectBean.getLmtType())){
                continue;
            }
            apprSubSerno = ( objectBean.getLmtSubNo());
            String cusId = apprCoopSubInfoService.selectCusIdByApprSubSerno(apprSubSerno) ;
            if(guarCusId.equals(cusId)){
                result = false ;
            }
        }
        logger.info("判断是否担保公司重复占用结果{}--9 ------------ end", result);
        return result ;
    }

    public boolean check11ProGuarComRev(List<CmisLmt0011OccRelListDto> object, String guarCusId){
        boolean result = true ;
        logger.info("判断是否担保公司重复占用--11------------ start");
        String apprSubSerno = "" ;
        for (CmisLmt0011OccRelListDto objectBean : object) {
            if(CmisLmtConstants.YES_NO_Y.equals(objectBean.getIsProGuarCom())
                    || CmisLmtConstants.STD_ZB_LMT_TYPE_03.equals(objectBean.getLmtType())){
                continue;
            }
            apprSubSerno = (objectBean.getLmtSubNo()) ;
            String cusId = apprCoopSubInfoService.selectCusIdByApprSubSerno(apprSubSerno) ;
            if(guarCusId.equals(cusId)){
                result = false ;
            }
        }
        logger.info("判断是否担保公司重复占用结果:{}--11 ------------ end", result);
        return result ;
    }

    /**
     * @作者:lizx
     * @方法名称: selectGrpNoByCusId
     * @方法描述:  根据客户号获取客户所在集团成员编号
     * @参数与返回说明: null  非集团客户   grpNo 集团客户编号
     * @算法描述: 无
     * @日期：2021/10/11 9:11
     * @param cusId:
     * @return: java.lang.String
     * @算法描述: 无
    */
    public String selectGrpNoByCusId(String cusId){
        return cusGrpMemberRelMapper.selectGrpNoByCusId(cusId) ;
    }

    /**
     * @作者:lizx
     * @方法名称: updateApprLmtGrpNo
     * @方法描述:  
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/10/11 21:27
     * @param cusId: 
 * @param grpNo:
     * @return: void
     * @算法描述: 无
    */
    public void updateApprLmtGrpNo(String cusId, String grpNo){
        logger.info("更新成员客户{}中，得集团客户编号{} ---------- start ", cusId, grpNo);
        Map params = new HashMap();
        params.put("cusId", cusId) ;
        params.put("grpNo", grpNo) ;
        //根据客户好更新主表信息
        apprStrMtableInfoService.updateGrpNoByCusId(params) ;
        apprLmtSubBasicInfoService.updateGrpNoByCusId(params) ;
        logger.info("更新成员客户中的集团客户编号 ---------- end ");
    }

    /**
     * @作者:lizx
     * @方法名称: getLimitStrNo
     * @方法描述:  根据客户类型，返回客户额度系统类型
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/10/14 17:45
     * @param cusType: 
     * @return: java.lang.String
     * @算法描述: 无
    */
    public String getLimitStrNo(String cusType){
        String limitStrNo = CmisLmtConstants.STD_ZB_LMT_STR_MTABLE_CONF_01 ;
        //个人客户
        if(CmisLmtConstants.STD_ZB_CUS_TYPE_1.equals(cusType)){
            limitStrNo = CmisLmtConstants.STD_ZB_LMT_STR_MTABLE_CONF_02 ;
        }else if(CmisLmtConstants.STD_ZB_CUS_TYPE_3.equals(cusType)){
            limitStrNo = CmisLmtConstants.STD_ZB_LMT_STR_MTABLE_CONF_03 ;
        }
        return limitStrNo ;
    }


    /**
     * 根据属性名称拷贝map中的属性值到目标对象中的属性，
     * 增加对（BigDecimal、Integer、Date.Long 判断）
     * @param source
     * @param target
     */
    public void mapToBean(Map source,Object target){
        source.forEach((key,val)->{
            setValByKey(target, (String) key,val);
        });
    }

    /**
     * 设置目标对象对应属性设置值
     * @param target  目标转换
     * @param key  属性名称
     * @param val  属性值
     */
    public void setValByKey(Object target, String key, Object val) {
        if (target == null){
            logger.error("target不能为null");
            return;
        }
        boolean isSuccess = false;
        BeanWrapper src = new BeanWrapperImpl(target);
        PropertyDescriptor[] propertyDescriptors = src.getPropertyDescriptors();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
            String name = propertyDescriptor.getName();

            if (name.equals(key)){
                String valTypeName = "";
                if (val != null){
                    valTypeName = val.getClass().getTypeName();
                }
                String typeName = propertyDescriptor.getPropertyType().getTypeName();
                //BigDecimal单独进行转换
                if(typeName.equals(BigDecimal.class.getTypeName())){
                    BigDecimal bigDecimal = changeValueToBigDecimal(val);
                    src.setPropertyValue(name,bigDecimal);
                }else if(typeName.equals(Integer.class.getTypeName())){
                    //Integer单独进行转换
                    Integer integer = changeValueToInteger(val);
                    src.setPropertyValue(name,integer);
                }else if(typeName.equals(Date.class.getTypeName()) && valTypeName.equals(Long.class.getTypeName())){
                    Date date = new Date((Long) val);
                    src.setPropertyValue(name,date);
                }else if(valTypeName.equals(typeName)){
                    //类型匹配直接赋值
                    src.setPropertyValue(name,val);
                }else{
                    src.setPropertyValue(name,null);
                    //类型无法匹配
                    //log.error("当前copy属性值：{}(目标所需类型：{} --- 提供属性类型：{} )不匹配，该属性copy失败，默认为null",name,typeName,valTypeName);
                }
                isSuccess = true;
            }
        }
        if (!isSuccess){
            //log.info("目标类（{}）不存在该属性（{}）。。",target.getClass().getSimpleName(),key);
        }
    }

    /**
     * 转换对象为BigDecimal （null、”“、"null"）默认值为BigDecimal.ZERO
     * @param target 需要转换的值
     * @return
     */
    public BigDecimal changeValueToBigDecimal(Object target){
        try {
            if (target ==null ||"".equals(target) || "null".equals(target)){
                return BigDecimal.ZERO;
            }else{
                String val = String.valueOf(target);
                return new BigDecimal(val);
            }
        } catch (Exception e) {
            logger.error("BigDecimal格式转化失败===》",e);
            throw BizException.error(null,EclEnum.LMT_SIG_INVESTAPP_UPDATE_FAILD.key,EclEnum.LMT_SIG_INVESTAPP_UPDATE_FAILD.value);
        }
    }
    public Integer changeValueToInteger(Map map,String key){
        Integer resultVal = 0;
        if (map!=null && map.containsKey(key)){
            try {
                if (map.get(key)==null || "".equals(map.get(key)) || "null".equals(map.get(key))){
                    map.put(key,resultVal);
                }else{
                    resultVal = Integer.parseInt(String.valueOf(map.get(key)));
                    map.put(key,resultVal);
                }
            } catch (Exception e) {
                logger.error("Integer格式转化失败===》",e);
                throw BizException.error(null,EclEnum.LMT_SIG_INVESTAPP_UPDATE_FAILD.key,EclEnum.LMT_SIG_INVESTAPP_UPDATE_FAILD.value);
            }
        }
        return resultVal;
    }

    public Integer changeValueToInteger(Object target) {
        try {
            if (target == null || "".equals(target) || "null".equals(target)) {
                return 0;
            } else {
                String val = String.valueOf(target);
                return Integer.parseInt(String.valueOf(val));
            }
        } catch (Exception e) {
            logger.error("Integer格式转化失败===》",e);
            throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_UPDATE_FAILD.key, EclEnum.LMT_SIG_INVESTAPP_UPDATE_FAILD.value);
        }
    }

    /**
     * @作者:lizx
     * @方法名称: 累加金额处理
     * @方法描述: cmislmt0011 占用分项额度调用
     *     描述：最高额协议：最高额协议占用得时候，只会占用到分项，不会占用到明细，所以如果是最高额协议，调用父类累加金额处理（?）
     *     如果是一般合同或者最高额合同，则进行明细和分项累加处理
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/10/20 19:34
     * @param apprSubSerno:
     * @param lmtType:
     * @param dealBizType:
     * @return: void
     * @算法描述: 无
     */
    public void lmtAddAmtExecute(String apprSubSerno, String lmtType, String dealBizType) {
        logger.info("---3 param 累加金额处理开始----------------------start");
        if (CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(lmtType) || CmisLmtConstants.STD_ZB_LMT_TYPE_07.equals(lmtType)) {
            if(CmisLmtConstants.DEAL_BIZ_TYPE_3.equals(dealBizType)){
                comm4LmtCalFormulaUtils.calParentIdLmtAmtAddExecute(apprSubSerno);
            }else{
                comm4LmtCalFormulaUtils.calLmtAmtAddExecute(apprSubSerno);
                ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(apprSubSerno);
                if(Objects.nonNull(apprLmtSubBasicInfoService)) {
                    String parentId = apprLmtSubBasicInfo.getParentId() ;
                    comm4LmtCalFormulaUtils.calParentIdLmtAmtAddExecute(parentId);
                }
            }
        }
        logger.info("---3 param 累加金额处理结束----------------------end");
    }

    /**
     * @作者:lizx
     * @方法名称: lmtAddAmtExecute
     * @方法描述:  额度累加处理：cmislmt0014 调用
     *      描述：如果是最高额协议，根据当前交易编号反向查到得额度是分项信息，需反向查找到对应得明细进行，进行累加金额处理
     *           如是一般或最高额合同，则进行分项和明细得累加金额处理
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/10/20 19:44
     * @param tranAccNo:
     * @param apprSubSerno:
     * @param lmtType:
     * @param dealBizType:
     * @return: void
     * @算法描述: 无
    */
    public void lmtAddAmtExecute(String tranAccNo, String apprSubSerno, String lmtType, String dealBizType) {
        logger.info("---4 param 累加金额处理开始----------------------start");
        if (CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(lmtType) || CmisLmtConstants.STD_ZB_LMT_TYPE_07.equals(lmtType)) {
            if(CmisLmtConstants.DEAL_BIZ_TYPE_3.equals(dealBizType)){
                //根据交易流水号反向查找明细 二级分项
                //List<LmtContRel> contRelList = lmtContRelService.selectLmtContRelByDealBizNo(tranAccNo) ;
                List<LmtContRel> contRelList = lmtContRelService.selectLmtContRelListByDealBizNo(tranAccNo, CmisLmtConstants.STD_ZB_BIZ_ATTR_2) ;
                if(CollectionUtils.nonEmpty(contRelList)){
                    LmtContRel contRel = contRelList.get(0) ;
                    if(Objects.nonNull(contRel))  {
                        String subSerno = contRel.getLimitSubNo() ;
                        ApprLmtSubBasicInfo subInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(subSerno) ;
                        if(Objects.nonNull(subInfo)) comm4LmtCalFormulaUtils.calLmtAmtAddExecute(subSerno);
                    }
                }
                //分项处理 （一级分项）
                comm4LmtCalFormulaUtils.calParentIdLmtAmtAddExecute(apprSubSerno);
            }else{
                comm4LmtCalFormulaUtils.calLmtAmtAddExecute(apprSubSerno);
                ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(apprSubSerno);
                if(Objects.nonNull(apprLmtSubBasicInfoService)) {
                    String parentId = apprLmtSubBasicInfo.getParentId() ;
                    comm4LmtCalFormulaUtils.calParentIdLmtAmtAddExecute(parentId);
                }
            }
        }
        logger.info("---4 param 累加金额处理结束----------------------end");
    }

    /**
     * @作者:lizx
     * @方法名称: formatDateStr
     * @方法描述:  日期转化为2021-01-01 格式，防止防止推送过来的日期格式为 2021-01-01 01:01:01 格式
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/10/27 15:12
     * @param dateStr:
     * @return: java.lang.String
     * @算法描述: 无
    */
    public String formatDateStr(String dateStr){
        if(StringUtils.isBlank(dateStr)) return dateStr ;
        Date date = DateUtils.parseDateTimeByDef(dateStr) ;
        String strDate = DateUtils.formatDateByDef(date);
        if(Objects.nonNull(strDate)) {
            return strDate ;
        } else if(dateStr.length()==8){
            return DateUtils.formatDate8To10(dateStr) ;
        }
        return dateStr ;
    }

    /**
     * 获取客户上年度销售总额和当年销售总额较大值（纳税申报表）
     * @param cusId
     * @return
     * 规则：客户批复累加授信总额+授信总额部分，不允许超过客户上年度销售总额和当年销售总额较大值（纳税申报表）的N倍，
     * 此处【补充保证金释放金额倍数】作为额度系统参数设置，初始值为【2】  客户销售总额查询
     */
    public BigDecimal getStatYearAmtMax(String cusId){
        CmisCus0005ReqDto cmisCus0005ReqDto = new CmisCus0005ReqDto() ;
        cmisCus0005ReqDto.setCusId(cusId);
        logger.info("调用【cmiscus0005】接口开始,请求报文：{}", JSONObject.toJSONString(cmisCus0005ReqDto)) ;
        ResultDto<CmisCus0005RespDto> resultDto = cmisCusClientService.cmiscus0005(cmisCus0005ReqDto) ;
        logger.info("调用【cmiscus0005】接口结束,响应报文：{}", JSONObject.toJSONString(resultDto)) ;
        BigDecimal statYearAmtMax;
        //接口调用成功
        if (resultDto != null && resultDto.getData() != null && EcbEnum.ECB010000.key.equals(resultDto.getData().getErrorCode())) {
            CmisCus0005RespDto cmisCus0005RespDto = resultDto.getData() ;
            statYearAmtMax = BigDecimalUtil.replaceNull(cmisCus0005RespDto.getStatYearAmtMax()) ;
        }else{
            throw new YuspException(EclEnum.ECL070080.key, EclEnum.ECL070080.value);
        }
        logger.info("调用【cmiscus0005】接口结束，客户上年度销售总额和当年销售总额较大值为"+statYearAmtMax) ;
        return statYearAmtMax ;
    }
}

