/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.vo.ManaBusiLmtHisVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ManaBusiLmtHis;
import cn.com.yusys.yusp.service.ManaBusiLmtHisService;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ManaBusiLmtHisResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-16 15:32:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/manabusilmthis")
public class ManaBusiLmtHisResource {
    @Autowired
    private ManaBusiLmtHisService manaBusiLmtHisService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<ManaBusiLmtHis>> query() {
        QueryModel queryModel = new QueryModel();
        List<ManaBusiLmtHis> list = manaBusiLmtHisService.selectAll(queryModel);
        return new ResultDto<List<ManaBusiLmtHis>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<ManaBusiLmtHis>> index(QueryModel queryModel) {
        List<ManaBusiLmtHis> list = manaBusiLmtHisService.selectByModel(queryModel);
        return new ResultDto<List<ManaBusiLmtHis>>(list);
    }

    /**
     * @函数名称:selectbymodel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<ManaBusiLmtHisVo>> selectbymodel(@RequestBody QueryModel queryModel) {
        List<ManaBusiLmtHisVo> list = manaBusiLmtHisService.selectListByModel(queryModel);
        return new ResultDto<List<ManaBusiLmtHisVo>>(list);
    }

    /**
     * 异步下载业务条线额度管控数据
     */
    @PostMapping("/exportmanabusilmthis")
    public ResultDto<ProgressDto> asyncExportManaBusiLmtHis(@RequestBody QueryModel model) {
        ProgressDto progressDto = manaBusiLmtHisService.asyncExportManaBusiLmtHis(model);
        return ResultDto.success(progressDto);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<ManaBusiLmtHis> create(@RequestBody ManaBusiLmtHis manaBusiLmtHis) throws URISyntaxException {
        manaBusiLmtHisService.insert(manaBusiLmtHis);
        return new ResultDto<ManaBusiLmtHis>(manaBusiLmtHis);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ManaBusiLmtHis manaBusiLmtHis) throws URISyntaxException {
        int result = manaBusiLmtHisService.update(manaBusiLmtHis);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String busiType) {
        int result = manaBusiLmtHisService.deleteByPrimaryKey(pkId, busiType);
        return new ResultDto<Integer>(result);
    }

}
