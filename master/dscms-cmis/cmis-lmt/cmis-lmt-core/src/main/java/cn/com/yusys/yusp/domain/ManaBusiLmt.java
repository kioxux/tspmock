/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ManaBusiLmt
 * @类描述: mana_busi_lmt数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-16 15:32:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "mana_busi_lmt")
public class ManaBusiLmt extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 条线类型 **/
	@Id
	@Column(name = "BUSI_TYPE")
	private String busiType;
	
	/** 机构区域类型 **/
	@Id
	@Column(name = "ORG_AREA_TYPE")
	private String orgAreaType;
	
	/** 上月末贷款余额 **/
	@Column(name = "LAST_MONTH_LOAN_BALANCE", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal lastMonthLoanBalance;
	
	/** 当月可净新增贷款投放金额 **/
	@Column(name = "CURR_MONTH_ALLOW_ADD_AMT", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal currMonthAllowAddAmt;
	
	/** 上一日贷款余额 **/
	@Column(name = "LAST_DAY_LOAN_BALANCE", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal lastDayLoanBalance;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param busiType
	 */
	public void setBusiType(String busiType) {
		this.busiType = busiType;
	}
	
    /**
     * @return busiType
     */
	public String getBusiType() {
		return this.busiType;
	}
	
	/**
	 * @param orgAreaType
	 */
	public void setOrgAreaType(String orgAreaType) {
		this.orgAreaType = orgAreaType;
	}
	
    /**
     * @return orgAreaType
     */
	public String getOrgAreaType() {
		return this.orgAreaType;
	}
	
	/**
	 * @param lastMonthLoanBalance
	 */
	public void setLastMonthLoanBalance(java.math.BigDecimal lastMonthLoanBalance) {
		this.lastMonthLoanBalance = lastMonthLoanBalance;
	}
	
    /**
     * @return lastMonthLoanBalance
     */
	public java.math.BigDecimal getLastMonthLoanBalance() {
		return this.lastMonthLoanBalance;
	}
	
	/**
	 * @param currMonthAllowAddAmt
	 */
	public void setCurrMonthAllowAddAmt(java.math.BigDecimal currMonthAllowAddAmt) {
		this.currMonthAllowAddAmt = currMonthAllowAddAmt;
	}
	
    /**
     * @return currMonthAllowAddAmt
     */
	public java.math.BigDecimal getCurrMonthAllowAddAmt() {
		return this.currMonthAllowAddAmt;
	}
	
	/**
	 * @param lastDayLoanBalance
	 */
	public void setLastDayLoanBalance(java.math.BigDecimal lastDayLoanBalance) {
		this.lastDayLoanBalance = lastDayLoanBalance;
	}
	
    /**
     * @return lastDayLoanBalance
     */
	public java.math.BigDecimal getLastDayLoanBalance() {
		return this.lastDayLoanBalance;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}