/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.ApprCoopSubInfo;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprCoopSubInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: lenovo
 * @创建时间: 2021-04-20 21:28:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface ApprCoopSubInfoMapper{

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    ApprCoopSubInfo selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<ApprCoopSubInfo> selectByModel(QueryModel model);

    /**
     * @作者:lizx
     * @方法名称: selectSubInfoByCusIdAndPrdId
     * @方法描述:  根据担保合同客户号，和担保合同产品号，获取担保合同对应的分项额度信息
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/7/5 14:00
     * @param model: 
     * @return: cn.com.yusys.yusp.domain.ApprCoopSubInfo
     * @算法描述: 无
    */
    ApprCoopSubInfo selectSubInfoByCusIdAndPrdId(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(ApprCoopSubInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(ApprCoopSubInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(ApprCoopSubInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(ApprCoopSubInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectByApprSerno
     * @方法描述: 根据批复流水号查询合作方授信台账调整申请信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<ApprCoopSubInfo> selectByApprSerno(@Param("apprSenro") String appSerno);

    /**
     * @方法名称: selectAcsiInfoBySerno
     * @方法描述: 条件列表查询
     * @参数与返回说明:

     * @算法描述: 无
     */
    List<ApprCoopSubInfo> selectAcsiInfoBySerno(QueryModel queryModel);

    /**
     * @方法名称: selectApprCoopSubInfoAndBasicBySerno
     * @方法描述: 关联合作方额度台账表，查询合作方额度分项信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<Map<String, Object>> selectApprCoopSubInfoAndBasicBySerno(QueryModel queryModel);
    /**
     * @方法名称: queryListByCusIdAndCopType
     * @方法描述: 合作方额度视图详情-分项明细
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<Map<String, Object>> queryListByCusIdAndCopType(QueryModel queryModel);

    /**
     * @方法名称: selectBySubSerno
     * @方法描述: 根据分项编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    ApprCoopSubInfo selectBySubSerno(@Param("subSerno") String subSerno);

    /**
     * @方法名称: updateBySerno
     * @方法描述: 根据批复流水号修改状态(冻结/解冻)
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByApprSerno(Map params);

    /**
     * @方法名称: updateBySubSerno
     * @方法描述: 根据批复分项流水号修改状态(冻结/解冻)
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByApprSubSerno(Map params);

    /**
     * @方法名称: getStatusByApprSubSerno
     * @方法描述: 根据批复流水号获得分项状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<ApprCoopSubInfo> getStatusByApprSerno(@Param("apprSerno") String apprSerno);

    /**
     * @方法名称: updateBySubSerno
     * @方法描述: 根据批复分项流水号修改状态(失效已结清/失效未结清)
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateStatusByApprSubSerno(Map params);

    /**
     * @方法名称: getAllByApprSubSerno
     * @方法描述: 根据批复流水号查询分项信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<ApprCoopSubInfo> getAllByApprSubSerno(@Param("apprSerno") String apprSerno);


    /**
     * @方法名称: selectInfoByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<ApprCoopSubInfo> selectInfoByModel(QueryModel model);


    /**
     * @方法名称: selectAccSubNoByArray
     * @方法描述: 根据批复分项编号查询表里有哪些分项已经存在
     * @参数与返回说明:
     * @算法描述: 无
     */
    String selectAccSubNoByArray(@Param("apprSubSernos") List<String> apprSubSernos);

    /**
     * @方法名称: selectAccSubNoByApprSerno
     * @方法描述: 根据批复编号查询批复向下分项编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    String selectAccSubNoByApprSerno(@Param("apprSerno") String apprSerno);

    /**
     * @方法名称: deleteByApprSubNo
     * @方法描述: 根据批复分项流水号删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByApprSubNo(String apprSubNo);

    /**
     * 根据授信分项编号修改合作方分项授信信息
     * @param
     * @return
     */
    int updateByApprSubSernoSelective(ApprCoopSubInfo record);

    /**
     *根据批复台账编号查询对应的授信总额之和和已用总额之和
     * @param apprSerno
     * @return
     */
    Map<String, BigDecimal> selectTotalAmtByApprSerno(@Param("apprSerno") String apprSerno);

    /**
     * @方法名称: selectCusIdByApprSubSerno
     * @方法描述: 根据分项编号，查询合作方客户号
     * @参数与返回说明:
     * @算法描述: 无
     */
    String selectCusIdByApprSubSerno(@Param("apprSerno") String apprSerno);
}