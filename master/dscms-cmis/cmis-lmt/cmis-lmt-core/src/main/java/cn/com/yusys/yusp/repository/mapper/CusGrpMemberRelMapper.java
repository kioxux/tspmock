/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.data.authority.annotation.IgnoredDataAuthority;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.CusGrpMemberRel;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: CusGrpMemberRelMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-29 20:42:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CusGrpMemberRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CusGrpMemberRel selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    @IgnoredDataAuthority
    List<CusGrpMemberRel> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CusGrpMemberRel record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CusGrpMemberRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CusGrpMemberRel record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CusGrpMemberRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     *
     * @param cusId
     * @return
     */
    int deleteByCusId(@Param("cusId") String cusId);

    /**
     * @方法名称: deleteByGrpNo
     * @方法描述: 根据集团编号删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByGrpNo(@Param("grpNo") String grpNo);

    /**
     * 根据集团编号查询集团成员客户编号
     * @param grpNo
     * @return
     */
    List<Map<String,Object>> selectCusIdsByGrpNo(@Param("grpNo") String grpNo);

    /**
     * 根据集团编号查询集团成员客户编号
     * @param cusId
     * @return
     */
    List<Map<String,String>> selectByCusIdQueryGrpNoCuss(@Param("cusId") String cusId);

    String selectGrpNoByCusId(@Param("cusId") String cusId) ;
}