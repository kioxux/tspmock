package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.ApprCoopInfo;
import cn.com.yusys.yusp.domain.ApprCoopSubInfo;
import cn.com.yusys.yusp.domain.CoopProFrozeUnfrozeApp;
import cn.com.yusys.yusp.domain.CoopProFrozeUnfrozeSub;
import cn.com.yusys.yusp.dto.CoopProAccInfoDto;
import cn.com.yusys.yusp.dto.CoopProFrozeUnfrozeSubDto;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.NextNodeInfoDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.repository.mapper.ApprCoopInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprCoopSubInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CoopProFrozeUnfrozeSubMapper;
import cn.com.yusys.yusp.service.CmisBizClientService;
import cn.com.yusys.yusp.service.CoopProFrozeUnfrozeAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/6/517:25
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class HZXM06BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(HZXM06BizService.class);

    @Autowired
    private CoopProFrozeUnfrozeAppService coopProFrozeUnfrozeAppService;

    @Autowired
    private CoopProFrozeUnfrozeSubMapper coopProFrozeUnfrozeSubMapper;

    @Autowired
    private ApprCoopInfoMapper apprCoopInfoMapper;

    @Autowired
    private ApprCoopSubInfoMapper apprCoopSubInfoMapper;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void bizOp(ResultInstanceDto resultInstanceDto) {

        resultInstanceDto.getBizType();

        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        log.info("后业务处理类型" + currentOpType);
        try {
            CoopProFrozeUnfrozeApp iqpContExt = coopProFrozeUnfrozeAppService.selectByPrimaryKey(serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("合作方额度冻结解冻申请-总行部门【{}】，流程发起操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                iqpContExt.setApprStatus(CmisBizConstants.APPLY_STATE_APP);
                coopProFrozeUnfrozeAppService.update(iqpContExt);
                log.info("合作方额度冻结解冻申请-总行部门【{}】，流程提交操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.JUMP.equals(currentOpType)) {
                iqpContExt.setApprStatus(CmisBizConstants.APPLY_STATE_APP);
                coopProFrozeUnfrozeAppService.update(iqpContExt);
                log.info("合作方额度冻结解冻申请-总行部门【{}】，流程跳转操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                iqpContExt.setApprStatus(CmisBizConstants.APPLY_STATE_PASS);
                coopProFrozeUnfrozeAppService.update(iqpContExt);
                QueryModel queryModel = new QueryModel();
                queryModel.getCondition().put("serno", iqpContExt.getSerno());
                List<CoopProFrozeUnfrozeSubDto> list = coopProFrozeUnfrozeSubMapper.selectByModel(queryModel);
                AtomicInteger sum = new AtomicInteger();
                list.stream().forEach(sub -> {
                    String apprSerno = sub.getApprSerno();
                    String apprSubSerno = sub.getApprSubSerno();
                    String limitSubNo = sub.getLimitSubNo();
                    QueryModel qm = new QueryModel();
                    qm.getCondition().put("apprSerno", apprSerno);
                    qm.getCondition().put("apprSubSerno", apprSubSerno);
                    qm.getCondition().put("limitSubNo", limitSubNo);
                    List<ApprCoopSubInfo> listInfo = apprCoopSubInfoMapper.selectByModel(qm);
                    listInfo.stream().forEach(coopInfo -> {
                        coopInfo.setStatus(sub.getAfterLmtStatus());
                        apprCoopSubInfoMapper.updateByPrimaryKey(coopInfo);
                    });
                    //同步更新项目信息[{"key":"0","value":"未生效"},{"key":"1","value":"正常"},{"key":"2","value":"失效"},{"key":"3","value":"冻结"}]
                    CoopProAccInfoDto coopProAccInfoDto = new CoopProAccInfoDto();
                    coopProAccInfoDto.setProNo(apprSubSerno);
                    if("1".equals(iqpContExt.getAppType())){//1-冻结2-解冻
                        coopProAccInfoDto.setProStatus("3");
                    }else if("2".equals(iqpContExt.getAppType())){
                        coopProAccInfoDto.setProStatus("1");
                    }
                    cmisBizClientService.updatestatusbyprono(coopProAccInfoDto);

                    if (("11").equals(sub.getAfterLmtStatus())) {
                        sum.set(sum.get() + 1);
                    }
                });
                if (list.size() == sum.get()) {
                    QueryModel qmi = new QueryModel();
                    qmi.getCondition().put("cusId", iqpContExt.getPartnerNo());
                    qmi.getCondition().put("copType", iqpContExt.getPartnerType());
                    List<ApprCoopInfo> apprInfo = apprCoopInfoMapper.selectByModel(qmi);
                    apprInfo.stream().forEach(cpInfo -> {
                        cpInfo.setApprStatus("11");
                        apprCoopInfoMapper.updateByPrimaryKey(cpInfo);
                    });
                }
                log.info("合作方额度冻结解冻申请-总行部门【{}】，流程结束操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("合作方额度冻结解冻申请-总行部门【{}】，流程退回操作，流程参数【{}】", serno, resultInstanceDto);
                List<NextNodeInfoDto> list = resultInstanceDto.getNextNodeInfos();
                list.stream().forEach(next->{
                    ResultDto<Boolean> flowResultDto = workflowCoreClient.isFirstNode(next.getNextNodeId());
                    if (flowResultDto.getData()) {
                        iqpContExt.setApprStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                        coopProFrozeUnfrozeAppService.update(iqpContExt);
                    }
                });
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("合作方额度冻结解冻申请-总行部门【{}】，流程打回操作，流程参数【{}】", serno, resultInstanceDto);
                List<NextNodeInfoDto> list = resultInstanceDto.getNextNodeInfos();
                list.stream().forEach(next->{
                    ResultDto<Boolean> flowResultDto = workflowCoreClient.isFirstNode(next.getNextNodeId());
                    if (flowResultDto.getData()) {
                        iqpContExt.setApprStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                        coopProFrozeUnfrozeAppService.update(iqpContExt);
                    }
                });
                //针对流程到办结节点，进行以下处理
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("合作方额度冻结解冻申请-总行部门【{}】，流程拿回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                iqpContExt.setApprStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                coopProFrozeUnfrozeAppService.update(iqpContExt);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("合作方额度冻结解冻申请-总行部门【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                iqpContExt.setApprStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                coopProFrozeUnfrozeAppService.update(iqpContExt);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("合作方额度冻结解冻申请-总行部门【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                iqpContExt.setApprStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                coopProFrozeUnfrozeAppService.update(iqpContExt);
            } else {
                log.info("合作方额度冻结解冻申请-总行部门【{}】，未知操作，流程参数【{}】", serno, resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("合作方额度冻结解冻申请-总行部门后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
            throw new MessageConversionException("合作方额度冻结解冻消息消费失败，触发事务回滚，添加异常队列，并将此消息移除队列！！！！！！");
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.HZXM06.equals(flowCode);
    }
}