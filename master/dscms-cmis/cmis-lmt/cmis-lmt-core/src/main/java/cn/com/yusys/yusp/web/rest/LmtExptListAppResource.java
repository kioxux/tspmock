/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtExptListApp;
import cn.com.yusys.yusp.service.LmtExptListAppService;
import cn.com.yusys.yusp.service.LmtExptListInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtExptListAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: lenovo
 * @创建时间: 2021-04-16 15:41:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtexptlistapp")
public class LmtExptListAppResource {
    @Autowired
    private LmtExptListAppService lmtExptListAppService;

    @Autowired
    private LmtExptListInfoService lmtExptListInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtExptListApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtExptListApp> list = lmtExptListAppService.selectAll(queryModel);
        return new ResultDto<List<LmtExptListApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtExptListApp>> index(QueryModel queryModel) {
        List<LmtExptListApp> list = lmtExptListAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtExptListApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtExptListApp>> selectByModel(@RequestBody QueryModel queryModel) {
        List<LmtExptListApp> list = lmtExptListAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtExptListApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtExptListApp> show(@PathVariable("pkId") String pkId) {
        LmtExptListApp lmtExptListApp = lmtExptListAppService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtExptListApp>(lmtExptListApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtExptListApp> create(@RequestBody LmtExptListApp lmtExptListApp) {
        lmtExptListAppService.insert(lmtExptListApp);
        return new ResultDto<LmtExptListApp>(lmtExptListApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertAppToInfo")
    protected ResultDto<Integer> insertAppToInfo(@RequestBody LmtExptListApp lmtExptListApp) {
        int result = lmtExptListInfoService.insertAppToInfo(lmtExptListApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtExptListApp lmtExptListApp) {
        int result = lmtExptListAppService.update(lmtExptListApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:insertAppToInfo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateAppToInfo")
    protected ResultDto<Integer> updateAppToInfo(@RequestBody LmtExptListApp lmtExptListInfoUpdate) {
        int result = lmtExptListAppService.updateAppToInfo(lmtExptListInfoUpdate);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtExptListAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtExptListAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:logicaldelete
     * @函数描述:根据流水号逻辑删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/logicaldelete")
    protected ResultDto<Integer> logicaldelete(@RequestBody LmtExptListApp record) {
        int result = lmtExptListAppService.logicaldelete(record);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateBySerno
     * @函数描述:根据流水号修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateBySerno")
    protected ResultDto<Integer> updateBySerno(@RequestBody LmtExptListApp record) {
        int result = lmtExptListAppService.updateBySerno(record);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:insert
     * @函数描述:新增数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insert")
    protected ResultDto<Integer> insert(@RequestBody LmtExptListApp lmtExptListAppAdd) {
        int result = lmtExptListAppService.insertSelective(lmtExptListAppAdd);
        return new ResultDto<Integer>(result);
    }



}
