package cn.com.yusys.yusp.web.server.cmislmt0024;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0024.req.CmisLmt0024ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0024.resp.CmisLmt0024RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0024.CmisLmt0024Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:承兑行白名单额度占用
 *
 * @author dumd 20210531
 * @version 1.0
 */
@Api(tags = "cmislmt0024:承兑行白名单额度占用")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0024Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0024Resource.class);

    @Autowired
    private CmisLmt0024Service cmisLmt0024Service;

    /**
     * 交易码：cmislmt0024
     * 交易描述：承兑行白名单额度占用
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("承兑行白名单额度占用")
    @PostMapping("/cmislmt0024")
    protected @ResponseBody
    ResultDto<CmisLmt0024RespDto> cmisLmt0024(@Validated @RequestBody CmisLmt0024ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0024.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0024.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0024RespDto> cmisLmt0024RespDtoResultDto = new ResultDto<>();
        CmisLmt0024RespDto cmisLmt0024RespDto = new CmisLmt0024RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0024RespDto = cmisLmt0024Service.execute(reqDto);
            cmisLmt0024RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0024RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0024.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0024.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0024.value, e.getMessage());
            // 封装cmisLmt0024RespDtoResultDto中异常返回码和返回信息
            cmisLmt0024RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0024RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0024RespDto到cmisLmt0024RespDtoResultDto中
        cmisLmt0024RespDtoResultDto.setData(cmisLmt0024RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0024.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0024.value, JSON.toJSONString(cmisLmt0024RespDtoResultDto));
        return cmisLmt0024RespDtoResultDto;
    }
}