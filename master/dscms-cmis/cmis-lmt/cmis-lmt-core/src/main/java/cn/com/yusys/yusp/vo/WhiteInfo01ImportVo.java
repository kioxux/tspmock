package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;
import java.time.LocalDate;

@ExcelCsv(namePrefix = "承兑行白名单导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class WhiteInfo01ImportVo {

    /*
    客户号
     */
    @ExcelField(title = "ECIF编号", viewLength = 20)
    private String cusId;

    /*
    总行行号
     */
    @ExcelField(title = "总行行号", viewLength = 20)
    private String aorgNo;

    /*
    客户名称
     */
    @ExcelField(title = "同业名称", viewLength = 20)
    private String cusName;

    /*
    客户类型
     */
    @ExcelField(title = "机构类型",dictCode = "STD_ZB_CUS_TYP", viewLength = 20)
    private String cusType;

    /*
    限额（万元）
     */
    @ExcelField(title = "限额（万元）", viewLength = 20)
    private BigDecimal sigAmt;

    /*
    到期日期
     */
    @ExcelField(title = "到期日", viewLength = 20)
    private String endDate;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getAorgNo() {
        return aorgNo;
    }

    public void setAorgNo(String aorgNo) {
        this.aorgNo = aorgNo;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public BigDecimal getSigAmt() {
        return sigAmt;
    }

    public void setSigAmt(BigDecimal sigAmt) {
        this.sigAmt = sigAmt;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

}
