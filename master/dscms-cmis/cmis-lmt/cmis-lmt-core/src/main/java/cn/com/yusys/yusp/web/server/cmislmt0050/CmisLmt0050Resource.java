package cn.com.yusys.yusp.web.server.cmislmt0050;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0050.req.CmisLmt0050ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0050.resp.CmisLmt0050RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0050.CmisLmt0050Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据条线部门和区域获取业务条线额度管控信息
 *
 * @author zhangjw 2021/7/14
 * @version 1.0
 */
@Api(tags = "cmislmt0050:根据条线部门和区域获取业务条线额度管控信息")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0050Resource {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0050Resource.class);

    @Autowired
    private CmisLmt0050Service cmisLmt0050Service;
    /**
     * 交易码：cmislmt0050
     * 交易描述：根据条线部门和区域获取业务条线额度管控信息
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("根据条线部门和区域获取业务条线额度管控信息")
    @PostMapping("/cmislmt0050")
    protected @ResponseBody
    ResultDto<CmisLmt0050RespDto> cmisLmt0050(@Validated @RequestBody CmisLmt0050ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0050.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0050.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0050RespDto> cmisLmt0050RespDtoResultDto = new ResultDto<>();
        CmisLmt0050RespDto cmisLmt0050RespDto = new CmisLmt0050RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0050RespDto = cmisLmt0050Service.execute(reqDto);
            cmisLmt0050RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0050RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);

        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0050.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0050.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0050.value, e.getMessage());
            // 封装xddb0050DataResultDto中异常返回码和返回信息
            cmisLmt0050RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0050RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0016RespDto到cmisLmt0016RespDtoResultDto中
        cmisLmt0050RespDtoResultDto.setData(cmisLmt0050RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0050.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0050.value, JSON.toJSONString(cmisLmt0050RespDtoResultDto));
        return cmisLmt0050RespDtoResultDto;
    }

}
