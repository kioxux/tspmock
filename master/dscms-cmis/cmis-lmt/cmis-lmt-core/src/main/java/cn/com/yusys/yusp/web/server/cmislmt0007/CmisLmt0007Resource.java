package cn.com.yusys.yusp.web.server.cmislmt0007;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0007.req.CmisLmt0007ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0007.resp.CmisLmt0007RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0007.CmisLmt0007Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:额度解冻
 *
 * @author jiangyl 20210506
 * @version 1.0
 */
@Api(tags = "cmislmt0007:额度解冻")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0007Resource.class);

    @Autowired
    private CmisLmt0007Service cmisLmt0007Service;
    /**
     * 交易码：cmislmt0007
     * 交易描述：额度冻结
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("额度解冻")
    @PostMapping("/cmislmt0007")
    protected @ResponseBody
    ResultDto<CmisLmt0007RespDto> cmisLmt0007(@Validated @RequestBody CmisLmt0007ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0007.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0007.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0007RespDto> cmisLmt0007RespDtoResultDto = new ResultDto<>();
        CmisLmt0007RespDto cmisLmt0007RespDto = new CmisLmt0007RespDto();
        // 调用对应的service层
        try {
            cmisLmt0007RespDto = cmisLmt0007Service.thaw(reqDto);
            cmisLmt0007RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0007RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0007.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0007.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0007.value, e.getMessage());
            // 封装xddb0001DataResultDto中异常返回码和返回信息
            cmisLmt0007RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0007RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        cmisLmt0007RespDtoResultDto.setData(cmisLmt0007RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0007.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0007.value, JSON.toJSONString(cmisLmt0007RespDtoResultDto));
        return cmisLmt0007RespDtoResultDto;
    }
}
