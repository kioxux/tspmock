/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.ApprLmtSubChgApp;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprLmtSubChgAppMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-20 20:39:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface ApprLmtSubChgAppMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    ApprLmtSubChgApp selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<ApprLmtSubChgApp> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(ApprLmtSubChgApp record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(ApprLmtSubChgApp record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(ApprLmtSubChgApp record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(ApprLmtSubChgApp record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: updateByAppSerno
     * @方法描述: 根据AppSerno逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByAppSerno(Map delMap);

    /**
     * @函数名称:querySubChgAppBySerno
     * @函数描述:根据调整申请流水号查询一级分项列表
     * @参数与返回说明:
     * @param model
     *            分页查询类
     * @算法描述:
     */
    List<Map<String, Object>> querySubChgAppBySerno(QueryModel model);
    /**
     * @函数名称:querySubDetailsChgAppBySerno
     * @函数描述:根据调整申请流水号和一级额度分项编号查询二级分项列表
     * @参数与返回说明:
     * @param model
     *            分页查询类
     * @算法描述:
     */
    List<Map<String, Object>> querySubDetailsChgAppBySerno(QueryModel model);

}