/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtContRel;
import cn.com.yusys.yusp.service.LmtContRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtContRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZR
 * @创建时间: 2021-04-20 10:38:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtcontrel")
public class LmtContRelResource {
    @Autowired
    private LmtContRelService lmtContRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtContRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtContRel> list = lmtContRelService.selectAll(queryModel);
        return new ResultDto<List<LmtContRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtContRel>> index(QueryModel queryModel) {
        List<LmtContRel> list = lmtContRelService.selectByModel(queryModel);
        return new ResultDto<List<LmtContRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtContRel> show(@PathVariable("pkId") String pkId) {
        LmtContRel lmtContRel = lmtContRelService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtContRel>(lmtContRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtContRel> create(@RequestBody LmtContRel lmtContRel) {
        lmtContRelService.insert(lmtContRel);
        return new ResultDto<LmtContRel>(lmtContRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtContRel lmtContRel) {
        int result = lmtContRelService.update(lmtContRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtContRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtContRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 额度视图，查询额度项下关联业务
     * @author zhangjw 2021-05-05
     * @return
     */
    @PostMapping("/queryListByLimitSubNo")
    protected ResultDto<List<Map>> queryListByLimitSubNo(@RequestBody QueryModel queryModel) {
        List<Map> list = lmtContRelService.queryListByLimitSubNo(queryModel);
        return new ResultDto<List<Map>>(list);
    }

    /**
     * @函数名称:
     * @函数描述:检查该分项编号是否存在
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectRecordsByApprSubNo")
    protected ResultDto<Integer> selectRecordsByApprSubNo(@RequestBody QueryModel queryModel) {
        String apprSubNo = (String) queryModel.getCondition().get("apprSubNo");
        int result = lmtContRelService.selectRecordsByApprSubNo(apprSubNo);
        return new ResultDto<Integer>(result);
    }

}
