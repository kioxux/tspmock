package cn.com.yusys.yusp.web.server.cmislmt0017;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.resp.CmisLmt0001RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0017.resp.CmisLmt0017RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0001.CmisLmt0001Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:单一客户额度同步
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "cmislmt0017:客户移交")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0017Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0017Resource.class);

    /**
     * 交易码：cmislmt0001
     * 交易描述：客户移交
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("客户移交")
    @PostMapping("/cmislmt0017")
    protected @ResponseBody
    ResultDto<CmisLmt0017RespDto> cmisLmt0017(@Validated @RequestBody CmisLmt0001ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0017.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0017.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0017RespDto> cmisLmt0017RespDtoResultDto = new ResultDto<>();
        CmisLmt0017RespDto cmisLmt0017RespDto = new CmisLmt0017RespDto() ;
        // 调用对应的service层
        try {
            //TODO:调用接口服务请求
            cmisLmt0017RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0017RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0017.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0017.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0017.value, e.getMessage());
            cmisLmt0017RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0017RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0017RespDto到cmisLmt0017RespDtoResultDto中
        cmisLmt0017RespDtoResultDto.setData(cmisLmt0017RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0017.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0017.value, JSON.toJSONString(cmisLmt0017RespDtoResultDto));
        return cmisLmt0017RespDtoResultDto;
    }
}
