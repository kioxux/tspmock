package cn.com.yusys.yusp.service.server.cmislmt0008;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.LmtApprExceptionDefEnums;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ApprListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ApprSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.resp.CmisLmt0008RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ReqDto;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ApprCoopInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprCoopSubInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprLmtSubBasicInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprStrMtableInfoMapper;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.LmtContRelService;
import cn.com.yusys.yusp.service.server.cmislmt0006.CmisLmt0006Service;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0008Service
 * @类描述: #对内服务类
 * @功能描述: 额度提前终止
 * @创建时间: 2021-05-07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0008Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0006Service.class);

    @Autowired
    private ApprCoopInfoMapper apprCoopInfoMapper;

    @Autowired
    private ApprCoopSubInfoMapper apprCoopSubInfoMapper;

    @Autowired
    private ApprStrMtableInfoMapper apprStrMtableInfoMapper;

    @Autowired
    private ApprLmtSubBasicInfoMapper apprLmtSubBasicInfoMapper;

    @Autowired
    private CmisLmt0006Service cmisLmt0006Service;

    @Autowired
    private Comm4Service comm4Service ;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService;

    @Transactional
    public CmisLmt0008RespDto terminate(CmisLmt0008ReqDto reqDto){
        logger.info(this.getClass().getName()+":额度提前终止接口接收数据开始............. ");
        CmisLmt0008RespDto respDto = new CmisLmt0008RespDto();
        try{
            //最新更新人
            String inputId = reqDto.getInputId();
            //最新更新机构
            String inputBrId = reqDto.getInputBrId();
            //最新更新日期
            String inputDate = reqDto.getInputDate();
            //授信类型
            String lmtType = reqDto.getLmtType() ;
            if(StringUtils.isBlank(lmtType)){
                respDto.setErrorCode(EclEnum.ECL070129.key);
                respDto.setErrorMsg(EclEnum.ECL070129.value);
                return respDto ;
            }

            if(reqDto.getApprList()==null || reqDto.getApprList().size()<=0){
                respDto.setErrorCode(EclEnum.ECL070106.key);
                respDto.setErrorMsg(EclEnum.ECL070106.value);
                return respDto ;
            }

            List<CmisLmt0008ApprListReqDto> apprList = reqDto.getApprList();
            //判断是否提前终止的额度列表
            if(apprList==null || apprList.size()<=0){
                respDto.setErrorCode(EclEnum.ECL070110.key);
                respDto.setErrorMsg(EclEnum.ECL070110.value);
                return respDto ;
            }

            //合作方额度提前终止
            if (CmisLmtConstants.STD_ZB_LMT_TYPE_03.equals(lmtType)){
                for (CmisLmt0008ApprListReqDto apprListRrqDto:apprList) {
                    //合作方额度全部提前终止
                    if (apprListRrqDto.getOptType().equals(CmisLmtConstants.STD_ZB_APPR_INFO_02)){
                        if(StringUtils.isBlank(apprListRrqDto.getApprSerno())){
                            respDto.setErrorCode(EclEnum.ECL070115.key);
                            respDto.setErrorMsg(EclEnum.ECL070115.value);
                            return respDto ;
                        }

                        List<ApprCoopSubInfo> subInfoList = apprCoopSubInfoMapper.getAllByApprSubSerno(apprListRrqDto.getApprSerno());

                        //判断是否有需要提前终止的额度批复台账列表，若没有则直接返回成功  因为合作方方案审批通过后，签订协议才推送额度，可能出现未签协议就进行合作方退出
                        if(subInfoList==null || subInfoList.size()<=0){
                            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
                            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
                            return respDto ;
                        }

                        for (ApprCoopSubInfo subInfo:subInfoList) {
                            //判断额度分项是否存在未结清的占用 -- 失效未结清
                            if(comm4Service.isExistsUnSettle(subInfo.getApprSubSerno())){
                                updatePartAoopSubStatus(subInfo.getApprSubSerno(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_90);
                            }else {//失效已结清
                                updatePartAoopSubStatus(subInfo.getApprSubSerno(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_99);
                            }
                        }
                    }
                }
                //合作方部分授信额度提前终止
                if (reqDto.getApprSubList()!=null && reqDto.getApprSubList().size()>0){
                    for (CmisLmt0008ApprSubListReqDto apprSubListReqDto : reqDto.getApprSubList()) {
                        //判断额度分项是否存在未结清的占用 失效未结清
                        if(comm4Service.isExistsUnSettle(apprSubListReqDto.getApprSubSerno())){
                            updatePartAoopSubStatus(apprSubListReqDto.getApprSubSerno(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_90);
                        }else {//失效已结清
                            updatePartAoopSubStatus(apprSubListReqDto.getApprSubSerno(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_99);
                        }
                    }
                }
                //判断合作方额度状态是否要修改
                for (CmisLmt0008ApprListReqDto apprListRrqDto:reqDto.getApprList()) {
                    List<ApprCoopSubInfo> statusList = apprCoopSubInfoMapper.getStatusByApprSerno(apprListRrqDto.getApprSerno());
                    boolean flag = true;
                    if(statusList!=null && statusList.size()>0) {
                        for (ApprCoopSubInfo apprCoopSubInfo : statusList) {
                            if (!apprCoopSubInfo.getStatus().equals(CmisLmtConstants.STD_ZB_APPR_ST_99)) {
                                flag = false;
                            }
                        }
                        //该合作方所有分项都是失效已结清
                        if (flag) {
                            //修改合作方状态为失效已结清
                            updateAoopStatus(apprListRrqDto.getApprSerno(), inputId, inputBrId, inputDate, CmisLmtConstants.STD_ZB_APPR_ST_99);
                        }
                    }
                }
            }else {
                //非合作方额度提前终止
                for (CmisLmt0008ApprListReqDto apprListRrqDto:apprList) {
                    if(StringUtils.isBlank(apprListRrqDto.getApprSerno())){
                        respDto.setErrorCode(EclEnum.ECL070115.key);
                        respDto.setErrorMsg(EclEnum.ECL070115.value);
                        return respDto ;
                    }

                    //全部提前终止
                    if (apprListRrqDto.getOptType().equals(CmisLmtConstants.STD_ZB_APPR_INFO_02)){
                        //获取批复分项编号下的所有分项批复流水号（非结清注销的，操作类型为新增的）
                        List<ApprLmtSubBasicInfo> apprSubSernoList = apprLmtSubBasicInfoMapper.getSubByFkPkid(apprListRrqDto.getApprSerno());
                        for (ApprLmtSubBasicInfo apprSub:apprSubSernoList) {
                            //判断额度分项是否未结清 失效未结清
                            //if(comm4Service.isExistsUnSettle(apprSub.getApprSubSerno())){
                            if(comm4Service.hasBalance(apprSub.getApprSubSerno())){
                                updatePartLmtSubBasicStatus(apprSub.getApprSubSerno(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_90);
                            }else {
                                //失效已结清
                                updatePartLmtSubBasicStatus(apprSub.getApprSubSerno(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_99);
                            }
                        }
                        //处理一级分项状态数据信息
                        List<ApprLmtSubBasicInfo> statusOneList = apprLmtSubBasicInfoMapper.selectOnbSubByApprSerno(apprListRrqDto.getApprSerno()) ;
                        if(CollectionUtils.isNotEmpty(statusOneList)){
                            for (ApprLmtSubBasicInfo apprLmtSubBasicInfo : statusOneList) {
                                boolean ontSubFlag = true ;
                                String parentId = apprLmtSubBasicInfo.getApprSubSerno() ;
                                String cusId = apprLmtSubBasicInfo.getCusId() ;
                                if(StringUtils.nonBlank(parentId)){
                                    //根据批复台账编号判断
                                    List<ApprLmtSubBasicInfo> statusList = apprLmtSubBasicInfoService.selectSbuListByParentId(cusId, parentId);
                                    if(CollectionUtils.isNotEmpty(statusList)){
                                        for (ApprLmtSubBasicInfo apprSubInfo:statusList) {
                                            if (!apprSubInfo.getStatus().equals(CmisLmtConstants.STD_ZB_APPR_ST_99)){
                                                ontSubFlag = false ;
                                            }
                                        }
                                        if(ontSubFlag){
                                            updatePartLmtSubBasicStatus(parentId,inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_99);
                                        }
                                        /*else{
                                            updatePartLmtSubBasicStatus(parentId,inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_90);
                                        }*/
                                    }
                                }
                            }
                        }
                    }
                }

                //单一用户分项部分提前终止
                List<CmisLmt0008ApprSubListReqDto> cmisLmt0008ApprSubListReqDtos = reqDto.getApprSubList() ;
                if(CollectionUtils.isNotEmpty(cmisLmt0008ApprSubListReqDtos)){
                    for (CmisLmt0008ApprSubListReqDto apprSubListReqDto : cmisLmt0008ApprSubListReqDtos) {
                        //判断额度分项是否未结清-失效未结清
                        //if(comm4Service.isExistsUnSettle(apprSubListReqDto.getApprSubSerno())){//此处暂且填充条件，等判断方法封装好时，再来调用
                        if(comm4Service.hasBalance(apprSubListReqDto.getApprSubSerno())){//此处暂且填充条件，等判断方法封装好时，再来调用
                            updatePartLmtSubBasicStatus(apprSubListReqDto.getApprSubSerno(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_90);
                        }else {
                            //失效已结清
                            updatePartLmtSubBasicStatus(apprSubListReqDto.getApprSubSerno(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_99);
                        }

                        ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoMapper.selectByApprSubSerno(apprSubListReqDto.getApprSubSerno()) ;
                        String parentId = apprLmtSubBasicInfo.getParentId() ;
                        String cusId = apprLmtSubBasicInfo.getCusId() ;
                        if(StringUtils.nonBlank(parentId)){
                            //根据批复台账编号判断
                            List<ApprLmtSubBasicInfo> statusList = apprLmtSubBasicInfoService.selectSbuListByParentId(cusId, parentId);
                            boolean ontSubFlag = true ;
                            if(CollectionUtils.isNotEmpty(statusList)){
                                for (ApprLmtSubBasicInfo apprSubInfo:statusList) {
                                    if (!apprSubInfo.getStatus().equals(CmisLmtConstants.STD_ZB_APPR_ST_99)){
                                        ontSubFlag = false ;
                                    }
                                }
                                if(ontSubFlag){
                                    updatePartLmtSubBasicStatus(parentId,inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_99);
                                }
                                /*else{
                                    updatePartLmtSubBasicStatus(parentId,inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_90);
                                }*/
                            }
                        }
                    }
                }
                //判断单一客户额度状态是否要修改
                for (CmisLmt0008ApprListReqDto apprListRrqDto:reqDto.getApprList()) {
                    //根据批复台账编号判断
                    List<ApprLmtSubBasicInfo> statusList = apprLmtSubBasicInfoMapper.getStatusByFkPkid(apprListRrqDto.getApprSerno());
                    boolean flag = true;
                    for (ApprLmtSubBasicInfo apprLmtSubBasicInfo:statusList) {
                        if (!apprLmtSubBasicInfo.getStatus().equals(CmisLmtConstants.STD_ZB_APPR_ST_99)){
                            flag = false;
                        }
                    }
                    //该单一客户所有分项都是失效已结清
                    if (flag){
                        //修改单一客户状态为失效已结清
                        updateStrMtableStatus(apprListRrqDto.getApprSerno(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_99);
                    }
                    cmisLmt0006Service.judgeUpdateGrp(apprListRrqDto.getCusId(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_99);
                }
            }
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        }catch (YuspException e) {
            logger.error("额度提前终止接口报错：", e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(this.getClass().getName()+":额度提前终止接口接收数据结束............. ");
        return respDto;
    }

    //合作方额度状态修改
    public void updateAoopStatus(String apprSerno,String inputId,String inputBrId,String inputDate,String status){
        if(StringUtils.isBlank(apprSerno)){
            throw new YuspException(EclEnum.ECL070133.key, "额度编号"+EclEnum.ECL070133.value);
        }
        Map params = new HashMap();
        params.put("updId", inputId);
        params.put("updBrId", inputBrId);
        params.put("updDate", inputDate);
        params.put("apprStatus", status);
        params.put("apprSerno", apprSerno);
        params.put("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        params.put("updateTime", DateUtils.getCurrTimestamp());
        int i = apprCoopInfoMapper.updateByApprSerNo(params);
    }

    //合作方部分分项额度状态修改
    public void updatePartAoopSubStatus(String apprSubSerno,String inputId,String inputBrId,String inputDate,String status){
        if(StringUtils.isBlank(apprSubSerno)){
            throw new YuspException(EclEnum.ECL070133.key, "额度分项编号"+EclEnum.ECL070133.value);
        }
        Map params = new HashMap();
        params.put("updId", inputId);
        params.put("updBrId", inputBrId);
        params.put("updDate", inputDate);
        params.put("status", status);
        params.put("apprSubSerno", apprSubSerno);
        params.put("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        params.put("updateTime", DateUtils.getCurrTimestamp());
        int k = apprCoopSubInfoMapper.updateStatusByApprSubSerno(params);
    }

    //单一客户额度状态修改
    public void updateStrMtableStatus(String apprSerno,String inputId,String inputBrId,String inputDate,String status){
        if(StringUtils.isBlank(apprSerno)){
            throw new YuspException(EclEnum.ECL070133.key, "额度编号"+EclEnum.ECL070133.value);
        }
        Map params = new HashMap();
        params.put("updId", inputId);
        params.put("updBrId", inputBrId);
        params.put("updDate", inputDate);
        params.put("apprStatus", status);
        params.put("apprSerno", apprSerno);
        params.put("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        params.put("updateTime", DateUtils.getCurrTimestamp());
        int i = apprStrMtableInfoMapper.updateStatusByApprSerno(params);
    }

    //单一客户部分分项状态修改
    public void updatePartLmtSubBasicStatus(String apprSubSerno,String inputId,String inputBrId,String inputDate,String status){
        if(StringUtils.isBlank(apprSubSerno)){
            throw new YuspException(EclEnum.ECL070133.key, "额度分项编号"+EclEnum.ECL070133.value);
        }
        Map params = new HashMap();
        params.put("updId", inputId);
        params.put("updBrId", inputBrId);
        params.put("updDate", inputDate);
        params.put("status", status);
        params.put("apprSubSerno", apprSubSerno);
        params.put("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        params.put("updateTime", DateUtils.getCurrTimestamp());
        int k = apprLmtSubBasicInfoMapper.updateStatusBySerno(params);
    }

}
