package cn.com.yusys.yusp.web.server.cmislmt0015;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0015.req.CmisLmt0015ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0015.CmisLmt0015Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:单一客户额度同步
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "cmislmt0015:客户额度查询")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0015Resource.class);

    @Autowired
    private CmisLmt0015Service cmisLmt0015Service;
    /**
     * 交易码：cmislmt0015
     * 交易描述：客户额度查询
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("客户额度查询")
    @PostMapping("/cmislmt0015")
    protected @ResponseBody
    ResultDto<CmisLmt0015RespDto> cmisLmt0015(@Validated @RequestBody CmisLmt0015ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0015.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0015.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0015RespDto> cmisLmt0015RespDtoResultDto = new ResultDto<>();
        CmisLmt0015RespDto cmisLmt0015RespDto = new CmisLmt0015RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0015RespDto = cmisLmt0015Service.execute(reqDto);
            cmisLmt0015RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0015RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0015.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0015.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0015.value, e.getMessage());
            // 封装xddb0015DataResultDto中异常返回码和返回信息
            cmisLmt0015RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0015RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0015RespDto到cmisLmt0015RespDtoResultDto中
        cmisLmt0015RespDtoResultDto.setData(cmisLmt0015RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0015.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0015.value, JSON.toJSONString(cmisLmt0015RespDtoResultDto));
        return cmisLmt0015RespDtoResultDto;
    }
}
