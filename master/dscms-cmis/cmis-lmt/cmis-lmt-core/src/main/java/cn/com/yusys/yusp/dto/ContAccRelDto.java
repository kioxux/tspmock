package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ContAccRel
 * @类描述: cont_acc_rel数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-04 17:03:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class ContAccRelDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	/** 交易台账编号 **/
	private String tranAccNo;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 交易业务编号 **/
	private String dealBizNo;
	
	/** 交易描述 **/
	private String tranDec;
	
	/** 系统编号 **/
	private String sysId;
	
	/** 产品编号 **/
	private String prdId;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 产品类型属性 **/
	private String prdTypeProp;
	
	/** 所属条线 **/
	private String belgLine;
	
	/** 担保方式 **/
	private String guarMode;
	
	/** 业务金额 **/
	private java.math.BigDecimal bizAmt;
	
	/** 交易台账状态 **/
	private String status;
	
	/** 台账占用总金额（折人民币） **/
	private java.math.BigDecimal accTotalAmtCny;
	
	/** 台账占用敞口金额（折人民币） **/
	private java.math.BigDecimal accSpacAmtCny;
	
	/** 台账占用总余额（折人民币） **/
	private java.math.BigDecimal accTotalBalanceAmtCny;
	
	/** 台账占用敞口余额（折人民币） **/
	private java.math.BigDecimal accSpacBalanceAmtCny;
	
	/** 业务保证金比例 **/
	private java.math.BigDecimal securityRate;
	
	/** 业务保证金金额 **/
	private java.math.BigDecimal securityAmt;
	
	/** 起始日期 **/
	private String startDate;
	
	/** 到期日期 **/
	private String endDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近更新人 **/
	private String updId;
	
	/** 最近更新机构 **/
	private String updBrId;
	
	/** 最近更新日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param tranAccNo
	 */
	public void setTranAccNo(String tranAccNo) {
		this.tranAccNo = tranAccNo == null ? null : tranAccNo.trim();
	}
	
    /**
     * @return TranAccNo
     */	
	public String getTranAccNo() {
		return this.tranAccNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param dealBizNo
	 */
	public void setDealBizNo(String dealBizNo) {
		this.dealBizNo = dealBizNo == null ? null : dealBizNo.trim();
	}
	
    /**
     * @return DealBizNo
     */	
	public String getDealBizNo() {
		return this.dealBizNo;
	}
	
	/**
	 * @param tranDec
	 */
	public void setTranDec(String tranDec) {
		this.tranDec = tranDec == null ? null : tranDec.trim();
	}
	
    /**
     * @return TranDec
     */	
	public String getTranDec() {
		return this.tranDec;
	}
	
	/**
	 * @param sysId
	 */
	public void setSysId(String sysId) {
		this.sysId = sysId == null ? null : sysId.trim();
	}
	
    /**
     * @return SysId
     */	
	public String getSysId() {
		return this.sysId;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}
	
    /**
     * @return PrdId
     */	
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp == null ? null : prdTypeProp.trim();
	}
	
    /**
     * @return PrdTypeProp
     */	
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}
	
	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine == null ? null : belgLine.trim();
	}
	
    /**
     * @return BelgLine
     */	
	public String getBelgLine() {
		return this.belgLine;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode == null ? null : guarMode.trim();
	}
	
    /**
     * @return GuarMode
     */	
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param bizAmt
	 */
	public void setBizAmt(java.math.BigDecimal bizAmt) {
		this.bizAmt = bizAmt;
	}
	
    /**
     * @return BizAmt
     */	
	public java.math.BigDecimal getBizAmt() {
		return this.bizAmt;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}
	
    /**
     * @return Status
     */	
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param accTotalAmtCny
	 */
	public void setAccTotalAmtCny(java.math.BigDecimal accTotalAmtCny) {
		this.accTotalAmtCny = accTotalAmtCny;
	}
	
    /**
     * @return AccTotalAmtCny
     */	
	public java.math.BigDecimal getAccTotalAmtCny() {
		return this.accTotalAmtCny;
	}
	
	/**
	 * @param accSpacAmtCny
	 */
	public void setAccSpacAmtCny(java.math.BigDecimal accSpacAmtCny) {
		this.accSpacAmtCny = accSpacAmtCny;
	}
	
    /**
     * @return AccSpacAmtCny
     */	
	public java.math.BigDecimal getAccSpacAmtCny() {
		return this.accSpacAmtCny;
	}
	
	/**
	 * @param accTotalBalanceAmtCny
	 */
	public void setAccTotalBalanceAmtCny(java.math.BigDecimal accTotalBalanceAmtCny) {
		this.accTotalBalanceAmtCny = accTotalBalanceAmtCny;
	}
	
    /**
     * @return AccTotalBalanceAmtCny
     */	
	public java.math.BigDecimal getAccTotalBalanceAmtCny() {
		return this.accTotalBalanceAmtCny;
	}
	
	/**
	 * @param accSpacBalanceAmtCny
	 */
	public void setAccSpacBalanceAmtCny(java.math.BigDecimal accSpacBalanceAmtCny) {
		this.accSpacBalanceAmtCny = accSpacBalanceAmtCny;
	}
	
    /**
     * @return AccSpacBalanceAmtCny
     */	
	public java.math.BigDecimal getAccSpacBalanceAmtCny() {
		return this.accSpacBalanceAmtCny;
	}
	
	/**
	 * @param securityRate
	 */
	public void setSecurityRate(java.math.BigDecimal securityRate) {
		this.securityRate = securityRate;
	}
	
    /**
     * @return SecurityRate
     */	
	public java.math.BigDecimal getSecurityRate() {
		return this.securityRate;
	}
	
	/**
	 * @param securityAmt
	 */
	public void setSecurityAmt(java.math.BigDecimal securityAmt) {
		this.securityAmt = securityAmt;
	}
	
    /**
     * @return SecurityAmt
     */	
	public java.math.BigDecimal getSecurityAmt() {
		return this.securityAmt;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}
	
    /**
     * @return StartDate
     */	
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return EndDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}