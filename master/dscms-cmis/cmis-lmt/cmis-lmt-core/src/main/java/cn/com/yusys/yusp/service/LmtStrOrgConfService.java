/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.dto.LmtConfStrMtableResDto;
import cn.com.yusys.yusp.dto.LmtStrOrgConfDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtStrOrgConf;
import cn.com.yusys.yusp.repository.mapper.LmtStrOrgConfMapper;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtStrOrgConfService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-15 15:36:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtStrOrgConfService {

    @Autowired
    private LmtStrOrgConfMapper lmtStrOrgConfMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public LmtStrOrgConf selectByPrimaryKey(String pkId) {
        return lmtStrOrgConfMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */
    @Transactional(readOnly=true)
    public List<LmtStrOrgConf> selectAll(QueryModel model) {
        List<LmtStrOrgConf> records = (List<LmtStrOrgConf>) lmtStrOrgConfMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtStrOrgConf> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtStrOrgConf> list = lmtStrOrgConfMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int insert(LmtStrOrgConf record) {
        return lmtStrOrgConfMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int insertSelective(LmtStrOrgConf record) {
        return lmtStrOrgConfMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int update(LmtStrOrgConf record) {
        return lmtStrOrgConfMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int updateSelective(LmtStrOrgConf record) {
        return lmtStrOrgConfMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return lmtStrOrgConfMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtStrOrgConfMapper.deleteByIds(ids);
    }

    /**
     * 获额度结构适用取机构信息
     * @param lmtStrMtableConfList
     * @return
     */
//	public List<LmtConfStrMtableResDto> selectDtoByQueryDto(List<LmtConfStrMtableResDto> lmtStrMtableConfList) {
//		if(lmtStrMtableConfList!=null&&lmtStrMtableConfList.size()>0) {
//			for(LmtConfStrMtableResDto lmtConfStrMtableResDto : lmtStrMtableConfList) {
//				List<LmtStrOrgConfDto> list  = lmtStrOrgConfMapper.selectByFkPkid(lmtConfStrMtableResDto.getPkId());
////				lmtConfStrMtableResDto.setOrgList(list);
//			}
//		}
//		return lmtStrMtableConfList;
//	}
}
