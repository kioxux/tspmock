package cn.com.yusys.yusp.service.server.cmislmt0022;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0022.req.CmisLmt0022CusListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0022.req.CmisLmt0022ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0022.resp.CmisLmt0022LmtListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0022.resp.CmisLmt0022RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.LmtContRelService;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0022Service
 * @类描述: #对内服务类
 * @功能描述: 客户额度分类查询
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0022Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0022Service.class);

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService;

    @Autowired
    private LmtContRelService lmtContRelService;

    /**
     * 客户分类额度查询
     *
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0022RespDto execute(CmisLmt0022ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0022.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0022.value);
        CmisLmt0022RespDto resqDto = new CmisLmt0022RespDto();
        List<CmisLmt0022LmtListRespDto> lmtListRespDtos = new ArrayList<>();

        try {
            List<CmisLmt0022CusListReqDto> cusListReqDto = reqDto.getCusList();

            List<String> cusIds = new ArrayList<>();

            for (CmisLmt0022CusListReqDto cusDto : cusListReqDto) {
                cusIds.add(cusDto.getCusId());
            }
            String instuCde = reqDto.getInstuCde();

            //查询条件
            Map queryMap = new HashMap();
            //客户号
            queryMap.put("cusIds", cusIds);
            //金融机构号
            queryMap.put("instuCde", instuCde);

            List<Map<String, Object>> maps = apprLmtSubBasicInfoService.selectLmtInfoByCusList(queryMap);

            if (maps!=null && maps.size()>0) {

                //额度分项编号列表
                List<String> lmtSubNoList = new ArrayList<>();

                for (Map<String, Object> lmtInfo : maps) {
                    CmisLmt0022LmtListRespDto lmtListRespDto = new CmisLmt0022LmtListRespDto();
                    // 客户号
                    lmtListRespDto.setCusId((String) lmtInfo.get("cusId"));
                    // 客户名称
                    lmtListRespDto.setCusName((String) lmtInfo.get("cusName"));
                    //获取额度统计类型
                    String lmtCatalog = null;

                    //客户类型
                    String cusType = (String) lmtInfo.get("cusType");
                    //是否低风险
                    String isLriskLmt = (String) lmtInfo.get("isLriskLmt");
                    // 授信类型
                    String lmtType = (String) lmtInfo.get("lmtType");
                    String lmtBizType = (String) lmtInfo.get("lmtBizType");
                    if (CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(lmtType) || CmisLmtConstants.STD_ZB_LMT_TYPE_07.equals(lmtType)){
                        //01-单一客户授信
                        if (CmisLmtConstants.YES_NO_Y.equals(isLriskLmt)) {
                            //是否低风险授信为是 低风险存量授信
                            lmtCatalog = CmisLmtConstants.STD_ZB_LMT_STATS_TYPE_22;
                        } else if (CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_300601.equals(lmtBizType)){
                            //资金业务存量授信  货币基金展示在资金业务存量授信下，modify by zhangjw 20210824
                            lmtCatalog = CmisLmtConstants.STD_ZB_LMT_STATS_TYPE_11;
                        }else{
                            //敞口存量授信
                            lmtCatalog = CmisLmtConstants.STD_ZB_LMT_STATS_TYPE_21;
                        }

                        if(CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(lmtType)){
                            String parentId = (String)lmtInfo.get("parentId");
                            if(!StringUtils.isBlank(parentId)){
                                continue;
                                //modify by zhangjw 20210825 根据蔡俊要求，无需要法人客户第二层级品种的额度
                            }
                        }

                    }else if (CmisLmtConstants.STD_ZB_LMT_TYPE_04.equals(lmtType) || CmisLmtConstants.STD_ZB_LMT_TYPE_05.equals(lmtType)){
                        //04--产品授信  05-主体授信 资金业务存量授信
                        lmtCatalog = CmisLmtConstants.STD_ZB_LMT_STATS_TYPE_11;
                    }
                    //额度统计类型
                    lmtListRespDto.setLmtCatalog(lmtCatalog);

                    //批复分项编号
                    String apprSubSerno = (String) lmtInfo.get("apprSubSerno");

                    if (CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4001.equals(lmtBizType)){
                        //4001--债券池,查询分项占用关系表
                        lmtSubNoList.add(apprSubSerno);
                    }
                    //授信类型
                    lmtListRespDto.setLmtType(lmtType);
                    // 额度分项流水号
                    lmtListRespDto.setApprSubSerno(apprSubSerno);
                    // 授信品种编号
                    lmtListRespDto.setLmtBizType((String) lmtInfo.get("lmtBizType"));
                    // 授信品种名称
                    lmtListRespDto.setLmtBizTypeName((String) lmtInfo.get("lmtBizTypeName"));
                    // 项目名称
                    lmtListRespDto.setProName((String) lmtInfo.get("proName"));
                    // 授信金额
                    lmtListRespDto.setLmtAmt(BigDecimalUtil.replaceNull(lmtInfo.get("lmtAmt")));
                    // 授信余额
                    lmtListRespDto.setLmtBalanceAmt(BigDecimalUtil.replaceNull(lmtInfo.get("lmtBalanceAmt")));
                    // 已用额度
                    lmtListRespDto.setOutstandAmt(BigDecimalUtil.replaceNull(lmtInfo.get("outstandAmt")));
                    // 担保方式
                    lmtListRespDto.setGuarMode((String) lmtInfo.get("guarMode"));
                    // 是否循环
                    lmtListRespDto.setIsRevolv((String) lmtInfo.get("isRevolv"));
                    // 起始日期
                    lmtListRespDto.setStartDate((String) lmtInfo.get("startDate"));
                    // 到期日期
                    lmtListRespDto.setEndDate((String) lmtInfo.get("endDate"));
                    //登记人，登记机构，登记日期
                    lmtListRespDto.setInputId((String) lmtInfo.get("inputId"));
                    lmtListRespDto.setInputBrId((String) lmtInfo.get("inputBrId"));
                    lmtListRespDto.setInputDate((String) lmtInfo.get("inputDate"));

                    lmtListRespDtos.add(lmtListRespDto);
                }

                //查询占用分项关系
                if (lmtSubNoList.size()>0){
                    List<Map<String, Object>> mapList = lmtContRelService.selectLmtInfoByLmtSubNoList(lmtSubNoList);

                    if (mapList!=null && maps.size()>0){
                        for (Map<String, Object> lmtInfo : mapList) {
                            CmisLmt0022LmtListRespDto lmtListRespDto = new CmisLmt0022LmtListRespDto();
                            lmtListRespDto.setCusId((String) lmtInfo.get("cusId"));// 客户号
                            lmtListRespDto.setCusName((String) lmtInfo.get("cusName"));// 客户名称
                            //获取额度统计类型
                            String apprSubSerno = (String) lmtInfo.get("apprSubSerno");
                            //额度统计类型 默认 13--债券投资存量业务
                            lmtListRespDto.setLmtCatalog(CmisLmtConstants.STD_ZB_LMT_STATS_TYPE_13);
                            //投资资产名称
                            lmtListRespDto.setInvestAssetName((String)lmtInfo.get("investAssetName"));// 投资资产名称

                            lmtListRespDto.setApprSubSerno(apprSubSerno);// 额度分项流水号
                            //授信品种编号 默认4001
                            lmtListRespDto.setLmtBizType("4001");
                            //授信品种名称 默认债券池
                            lmtListRespDto.setLmtBizTypeName("债券池");
                            //项目名称
                            lmtListRespDto.setProName("");
                            //授信金额
                            lmtListRespDto.setLmtAmt(BigDecimalUtil.replaceNull(lmtInfo.get("lmtAmt")));

                            //根据BUG14818修复：若占用余额<0，则不进行存量业务数据展示
                            BigDecimal lmtBalanceAmt = BigDecimalUtil.replaceNull(lmtInfo.get("lmtBalanceAmt"));
                            if(lmtBalanceAmt.compareTo(BigDecimal.ZERO) <= 0 ){
                                continue;
                            }

                            //授信余额
                            lmtListRespDto.setLmtBalanceAmt(lmtBalanceAmt);
                            //已用额度
                            lmtListRespDto.setOutstandAmt(BigDecimalUtil.replaceNull(lmtInfo.get("outstandAmt")));
                            //担保方式 默认为空
                            lmtListRespDto.setGuarMode("");
                            //是否循环 默认为空
                            lmtListRespDto.setIsRevolv("");
                            // 起始日期
                            lmtListRespDto.setStartDate((String) lmtInfo.get("startDate"));
                            // 到期日期
                            lmtListRespDto.setEndDate((String) lmtInfo.get("endDate"));
                            //登记人，登记机构，登记日期
                            lmtListRespDto.setInputId((String) lmtInfo.get("inputId"));
                            lmtListRespDto.setInputBrId((String) lmtInfo.get("inputBrId"));
                            lmtListRespDto.setInputDate((String) lmtInfo.get("inputDate"));

                            lmtListRespDtos.add(lmtListRespDto);
                        }
                    }
                }
            }

            //返回信息
            resqDto.setLmtList(lmtListRespDtos);
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("客户分类额度查询：" , e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0022.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0022.value);
        return resqDto;
    }
}