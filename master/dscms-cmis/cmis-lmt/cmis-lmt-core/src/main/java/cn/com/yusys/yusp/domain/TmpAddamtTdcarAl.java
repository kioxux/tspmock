/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: TmpAddamtTdcarAl
 * @类描述: tmp_addamt_tdcar_al数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-11-06 18:15:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_addamt_tdcar_al")
public class TmpAddamtTdcarAl extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 数值1 **/
	@Column(name = "AMT1", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal amt1;
	
	/** 数值2 **/
	@Column(name = "AMT2", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal amt2;
	
	/** 数值3 **/
	@Column(name = "AMT3", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal amt3;
	
	/** 数值4 **/
	@Column(name = "AMT4", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal amt4;
	
	/** 数值5 **/
	@Column(name = "AMT5", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal amt5;
	
	/** 字符型1 **/
	@Column(name = "FIELD1", unique = false, nullable = true, length = 80)
	private String field1;
	
	/** 字符型2 **/
	@Column(name = "FIELD2", unique = false, nullable = true, length = 80)
	private String field2;
	
	/** 字符型3 **/
	@Column(name = "FIELD3", unique = false, nullable = true, length = 80)
	private String field3;
	
	/** 字符型4 **/
	@Column(name = "FIELD4", unique = false, nullable = true, length = 80)
	private String field4;
	
	/** 字符型5 **/
	@Column(name = "FIELD5", unique = false, nullable = true, length = 80)
	private String field5;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param amt1
	 */
	public void setAmt1(java.math.BigDecimal amt1) {
		this.amt1 = amt1;
	}
	
    /**
     * @return amt1
     */
	public java.math.BigDecimal getAmt1() {
		return this.amt1;
	}
	
	/**
	 * @param amt2
	 */
	public void setAmt2(java.math.BigDecimal amt2) {
		this.amt2 = amt2;
	}
	
    /**
     * @return amt2
     */
	public java.math.BigDecimal getAmt2() {
		return this.amt2;
	}
	
	/**
	 * @param amt3
	 */
	public void setAmt3(java.math.BigDecimal amt3) {
		this.amt3 = amt3;
	}
	
    /**
     * @return amt3
     */
	public java.math.BigDecimal getAmt3() {
		return this.amt3;
	}
	
	/**
	 * @param amt4
	 */
	public void setAmt4(java.math.BigDecimal amt4) {
		this.amt4 = amt4;
	}
	
    /**
     * @return amt4
     */
	public java.math.BigDecimal getAmt4() {
		return this.amt4;
	}
	
	/**
	 * @param amt5
	 */
	public void setAmt5(java.math.BigDecimal amt5) {
		this.amt5 = amt5;
	}
	
    /**
     * @return amt5
     */
	public java.math.BigDecimal getAmt5() {
		return this.amt5;
	}
	
	/**
	 * @param field1
	 */
	public void setField1(String field1) {
		this.field1 = field1;
	}
	
    /**
     * @return field1
     */
	public String getField1() {
		return this.field1;
	}
	
	/**
	 * @param field2
	 */
	public void setField2(String field2) {
		this.field2 = field2;
	}
	
    /**
     * @return field2
     */
	public String getField2() {
		return this.field2;
	}
	
	/**
	 * @param field3
	 */
	public void setField3(String field3) {
		this.field3 = field3;
	}
	
    /**
     * @return field3
     */
	public String getField3() {
		return this.field3;
	}
	
	/**
	 * @param field4
	 */
	public void setField4(String field4) {
		this.field4 = field4;
	}
	
    /**
     * @return field4
     */
	public String getField4() {
		return this.field4;
	}
	
	/**
	 * @param field5
	 */
	public void setField5(String field5) {
		this.field5 = field5;
	}
	
    /**
     * @return field5
     */
	public String getField5() {
		return this.field5;
	}


}