/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprLmtSubChgApp
 * @类描述: appr_lmt_sub_chg_app数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-14 10:06:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "appr_lmt_sub_chg_app")
public class ApprLmtSubChgApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** FK_PKID **/
	@Column(name = "FK_PKID", unique = false, nullable = false, length = 40)
	private String fkPkid;
	
	/** 主申请流水号 **/
	@Column(name = "SUB_APP_SERNO", unique = false, nullable = false, length = 40)
	private String subAppSerno;

	/** 批复分项流水号 **/
	@Column(name = "APPR_SUB_SERNO", unique = false, nullable = false, length = 40)
	private String apprSubSerno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 集团编号 **/
	@Column(name = "GRP_NO", unique = false, nullable = true, length = 40)
	private String grpNo;
	
	/** 客户主体类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 2)
	private String cusType;
	
	/** 额度分项编号 **/
	@Column(name = "LIMIT_SUB_NO", unique = false, nullable = true, length = 32)
	private String limitSubNo;
	
	/** 额度分项名称 **/
	@Column(name = "LIMIT_SUB_NAME", unique = false, nullable = true, length = 255)
	private String limitSubName;
	
	/** 授信品种类型属性 **/
	@Column(name = "LMT_BIZ_TYPE_PROP", unique = false, nullable = true, length = 5)
	private String lmtBizTypeProp;
	
	/** 父节点 **/
	@Column(name = "PARENT_ID", unique = false, nullable = true, length = 32)
	private String parentId;
	
	/** 是否低风险授信 **/
	@Column(name = "IS_LRISK_LMT", unique = false, nullable = true, length = 1)
	private String isLriskLmt;
	
	/** 授信分项类型 **/
	@Column(name = "LMT_SUB_TYPE", unique = false, nullable = true, length = 2)
	private String lmtSubType;
	
	/** 是否循环 **/
	@Column(name = "IS_REVOLV", unique = false, nullable = true, length = 1)
	private String isRevolv;
	
	/** 额度类型 **/
	@Column(name = "LIMIT_TYPE", unique = false, nullable = true, length = 2)
	private String limitType;
	
	/** 适用担保方式 **/
	@Column(name = "SUIT_GUAR_WAY", unique = false, nullable = true, length = 2)
	private String suitGuarWay;
	
	/** 额度有效期 **/
	@Column(name = "LMT_DATE", unique = false, nullable = true, length = 20)
	private String lmtDate;
	
	/** 额度宽限期(月） **/
	@Column(name = "LMT_GRAPER", unique = false, nullable = true, length = 10)
	private Integer lmtGraper;
	
	/** 授信总额 **/
	@Column(name = "AVL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal avlAmt;
	
	/** 已用额度 **/
	@Column(name = "OUTSTND_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outstndAmt;
	
	/** 敞口金额 **/
	@Column(name = "SPAC_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal spacAmt;
	
	/** 敞口已用额度 **/
	@Column(name = "SPAC_OUTSTND_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal spacOutstndAmt;
	
	/** 贷款余额 **/
	@Column(name = "LOAN_BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanBalance;
	
	/** 状态 **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 2)
	private String status;
	
	/** 项目编号 **/
	@Column(name = "PRO_NO", unique = false, nullable = true, length = 40)
	private String proNo;
	
	/** 项目名称 **/
	@Column(name = "PRO_NAME", unique = false, nullable = true, length = 80)
	private String proName;
	
	/** 资产编号 **/
	@Column(name = "ASSET_NO", unique = false, nullable = true, length = 40)
	private String assetNo;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 保证金预留比例 **/
	@Column(name = "BAIL_PRE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailPreRate;
	
	/** 年利率 **/
	@Column(name = "RATE_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rateYear;
	
	/** 授信总额累加 **/
	@Column(name = "LMT_AMT_ADD", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtAmtAdd;
	
	/** 已出帐金额 **/
	@Column(name = "PVP_OUTSTND_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pvpOutstndAmt;
	
	/** 可出账金额 **/
	@Column(name = "AVL_OUTSTND_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal avlOutstndAmt;
	
	/** 是否预授信 **/
	@Column(name = "IS_PRE_CRD", unique = false, nullable = true, length = 5)
	private String isPreCrd;
	
	/** 是否涉及货币基金 **/
	@Column(name = "IS_IVL_MF", unique = false, nullable = true, length = 5)
	private String isIvlMf;
	
	/** 单只货币基金授信额度 **/
	@Column(name = "LMT_SINGLE_MF_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtSingleMfAmt;
	
	/** 起始日期 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 20)
	private String startDate;
	
	/** 期限 **/
	@Column(name = "TERM", unique = false, nullable = true, length = 10)
	private Integer term;
	
	/** 用信敞口余额 **/
	@Column(name = "LOAN_SPAC_BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanSpacBalance;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param fkPkid
	 */
	public void setFkPkid(String fkPkid) {
		this.fkPkid = fkPkid;
	}
	
    /**
     * @return fkPkid
     */
	public String getFkPkid() {
		return this.fkPkid;
	}
	
	/**
	 * @param subAppSerno
	 */
	public void setSubAppSerno(String subAppSerno) {
		this.subAppSerno = subAppSerno;
	}
	
    /**
     * @return subAppSerno
     */
	public String getSubAppSerno() {
		return this.subAppSerno;
	}
	
	/**
	 * @param apprSubSerno
	 */
	public void setApprSubSerno(String apprSubSerno) {
		this.apprSubSerno = apprSubSerno;
	}
	
    /**
     * @return apprSubSerno
     */
	public String getApprSubSerno() {
		return this.apprSubSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param grpNo
	 */
	public void setGrpNo(String grpNo) {
		this.grpNo = grpNo;
	}
	
    /**
     * @return grpNo
     */
	public String getGrpNo() {
		return this.grpNo;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param limitSubNo
	 */
	public void setLimitSubNo(String limitSubNo) {
		this.limitSubNo = limitSubNo;
	}
	
    /**
     * @return limitSubNo
     */
	public String getLimitSubNo() {
		return this.limitSubNo;
	}
	
	/**
	 * @param limitSubName
	 */
	public void setLimitSubName(String limitSubName) {
		this.limitSubName = limitSubName;
	}
	
    /**
     * @return limitSubName
     */
	public String getLimitSubName() {
		return this.limitSubName;
	}
	
	/**
	 * @param lmtBizTypeProp
	 */
	public void setLmtBizTypeProp(String lmtBizTypeProp) {
		this.lmtBizTypeProp = lmtBizTypeProp;
	}
	
    /**
     * @return lmtBizTypeProp
     */
	public String getLmtBizTypeProp() {
		return this.lmtBizTypeProp;
	}
	
	/**
	 * @param parentId
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
    /**
     * @return parentId
     */
	public String getParentId() {
		return this.parentId;
	}
	
	/**
	 * @param isLriskLmt
	 */
	public void setIsLriskLmt(String isLriskLmt) {
		this.isLriskLmt = isLriskLmt;
	}
	
    /**
     * @return isLriskLmt
     */
	public String getIsLriskLmt() {
		return this.isLriskLmt;
	}
	
	/**
	 * @param lmtSubType
	 */
	public void setLmtSubType(String lmtSubType) {
		this.lmtSubType = lmtSubType;
	}
	
    /**
     * @return lmtSubType
     */
	public String getLmtSubType() {
		return this.lmtSubType;
	}
	
	/**
	 * @param isRevolv
	 */
	public void setIsRevolv(String isRevolv) {
		this.isRevolv = isRevolv;
	}
	
    /**
     * @return isRevolv
     */
	public String getIsRevolv() {
		return this.isRevolv;
	}
	
	/**
	 * @param limitType
	 */
	public void setLimitType(String limitType) {
		this.limitType = limitType;
	}
	
    /**
     * @return limitType
     */
	public String getLimitType() {
		return this.limitType;
	}
	
	/**
	 * @param suitGuarWay
	 */
	public void setSuitGuarWay(String suitGuarWay) {
		this.suitGuarWay = suitGuarWay;
	}
	
    /**
     * @return suitGuarWay
     */
	public String getSuitGuarWay() {
		return this.suitGuarWay;
	}
	
	/**
	 * @param lmtDate
	 */
	public void setLmtDate(String lmtDate) {
		this.lmtDate = lmtDate;
	}
	
    /**
     * @return lmtDate
     */
	public String getLmtDate() {
		return this.lmtDate;
	}
	
	/**
	 * @param lmtGraper
	 */
	public void setLmtGraper(Integer lmtGraper) {
		this.lmtGraper = lmtGraper;
	}
	
    /**
     * @return lmtGraper
     */
	public Integer getLmtGraper() {
		return this.lmtGraper;
	}
	
	/**
	 * @param avlAmt
	 */
	public void setAvlAmt(java.math.BigDecimal avlAmt) {
		this.avlAmt = avlAmt;
	}
	
    /**
     * @return avlAmt
     */
	public java.math.BigDecimal getAvlAmt() {
		return this.avlAmt;
	}
	
	/**
	 * @param outstndAmt
	 */
	public void setOutstndAmt(java.math.BigDecimal outstndAmt) {
		this.outstndAmt = outstndAmt;
	}
	
    /**
     * @return outstndAmt
     */
	public java.math.BigDecimal getOutstndAmt() {
		return this.outstndAmt;
	}
	
	/**
	 * @param spacAmt
	 */
	public void setSpacAmt(java.math.BigDecimal spacAmt) {
		this.spacAmt = spacAmt;
	}
	
    /**
     * @return spacAmt
     */
	public java.math.BigDecimal getSpacAmt() {
		return this.spacAmt;
	}
	
	/**
	 * @param spacOutstndAmt
	 */
	public void setSpacOutstndAmt(java.math.BigDecimal spacOutstndAmt) {
		this.spacOutstndAmt = spacOutstndAmt;
	}
	
    /**
     * @return spacOutstndAmt
     */
	public java.math.BigDecimal getSpacOutstndAmt() {
		return this.spacOutstndAmt;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(java.math.BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return loanBalance
     */
	public java.math.BigDecimal getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo;
	}
	
    /**
     * @return proNo
     */
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	
    /**
     * @return proName
     */
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param assetNo
	 */
	public void setAssetNo(String assetNo) {
		this.assetNo = assetNo;
	}
	
    /**
     * @return assetNo
     */
	public String getAssetNo() {
		return this.assetNo;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param bailPreRate
	 */
	public void setBailPreRate(java.math.BigDecimal bailPreRate) {
		this.bailPreRate = bailPreRate;
	}
	
    /**
     * @return bailPreRate
     */
	public java.math.BigDecimal getBailPreRate() {
		return this.bailPreRate;
	}
	
	/**
	 * @param rateYear
	 */
	public void setRateYear(java.math.BigDecimal rateYear) {
		this.rateYear = rateYear;
	}
	
    /**
     * @return rateYear
     */
	public java.math.BigDecimal getRateYear() {
		return this.rateYear;
	}
	
	/**
	 * @param lmtAmtAdd
	 */
	public void setLmtAmtAdd(java.math.BigDecimal lmtAmtAdd) {
		this.lmtAmtAdd = lmtAmtAdd;
	}
	
    /**
     * @return lmtAmtAdd
     */
	public java.math.BigDecimal getLmtAmtAdd() {
		return this.lmtAmtAdd;
	}
	
	/**
	 * @param pvpOutstndAmt
	 */
	public void setPvpOutstndAmt(java.math.BigDecimal pvpOutstndAmt) {
		this.pvpOutstndAmt = pvpOutstndAmt;
	}
	
    /**
     * @return pvpOutstndAmt
     */
	public java.math.BigDecimal getPvpOutstndAmt() {
		return this.pvpOutstndAmt;
	}
	
	/**
	 * @param avlOutstndAmt
	 */
	public void setAvlOutstndAmt(java.math.BigDecimal avlOutstndAmt) {
		this.avlOutstndAmt = avlOutstndAmt;
	}
	
    /**
     * @return avlOutstndAmt
     */
	public java.math.BigDecimal getAvlOutstndAmt() {
		return this.avlOutstndAmt;
	}
	
	/**
	 * @param isPreCrd
	 */
	public void setIsPreCrd(String isPreCrd) {
		this.isPreCrd = isPreCrd;
	}
	
    /**
     * @return isPreCrd
     */
	public String getIsPreCrd() {
		return this.isPreCrd;
	}
	
	/**
	 * @param isIvlMf
	 */
	public void setIsIvlMf(String isIvlMf) {
		this.isIvlMf = isIvlMf;
	}
	
    /**
     * @return isIvlMf
     */
	public String getIsIvlMf() {
		return this.isIvlMf;
	}
	
	/**
	 * @param lmtSingleMfAmt
	 */
	public void setLmtSingleMfAmt(java.math.BigDecimal lmtSingleMfAmt) {
		this.lmtSingleMfAmt = lmtSingleMfAmt;
	}
	
    /**
     * @return lmtSingleMfAmt
     */
	public java.math.BigDecimal getLmtSingleMfAmt() {
		return this.lmtSingleMfAmt;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param term
	 */
	public void setTerm(Integer term) {
		this.term = term;
	}
	
    /**
     * @return term
     */
	public Integer getTerm() {
		return this.term;
	}
	
	/**
	 * @param loanSpacBalance
	 */
	public void setLoanSpacBalance(java.math.BigDecimal loanSpacBalance) {
		this.loanSpacBalance = loanSpacBalance;
	}
	
    /**
     * @return loanSpacBalance
     */
	public java.math.BigDecimal getLoanSpacBalance() {
		return this.loanSpacBalance;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}