/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtSubBasicConf
 * @类描述: lmt_sub_basic_conf数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-16 14:45:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_sub_basic_conf")
public class LmtSubBasicConf extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 额度结构编号 **/
	@Column(name = "LIMIT_STR_NO", unique = false, nullable = true, length = 20)
	private String limitStrNo;
	
	/** 结构分项编号 **/
	@Column(name = "LIMIT_SUB_NO", unique = false, nullable = true, length = 20)
	private String limitSubNo;
	
	/** 结构分项名称 **/
	@Column(name = "LIMIT_SUB_NAME", unique = false, nullable = true, length = 80)
	private String limitSubName;
	
	/** 父节点 **/
	@Column(name = "PARENT_ID", unique = false, nullable = true, length = 20)
	private String parentId;
	
	/** 是否低风险授信 **/
	@Column(name = "IS_LRISK_LMT", unique = false, nullable = true, length = 5)
	private String isLriskLmt;
	
	/** 授信分项类型 **/
	@Column(name = "LMT_SUB_TYPE", unique = false, nullable = true, length = 5)
	private String lmtSubType;
	
	/** 是否循环 **/
	@Column(name = "IS_REVOLV", unique = false, nullable = true, length = 5)
	private String isRevolv;
	
	/** 额度类型 **/
	@Column(name = "LIMIT_TYPE", unique = false, nullable = true, length = 5)
	private String limitType;
	
	/** 是否专项授信 **/
	@Column(name = "IS_EXPERT_LMT", unique = false, nullable = true, length = 5)
	private String isExpertLmt;
	
	/** 适用业务条线 **/
	@Column(name = "SUIT_BIZ_LINE", unique = false, nullable = true, length = 5)
	private String suitBizLine;
	
	/** 是否允许突破总体授信期限 **/
	@Column(name = "IS_OVER_LMT_TERM", unique = false, nullable = true, length = 5)
	private String isOverLmtTerm;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param limitStrNo
	 */
	public void setLimitStrNo(String limitStrNo) {
		this.limitStrNo = limitStrNo;
	}
	
    /**
     * @return limitStrNo
     */
	public String getLimitStrNo() {
		return this.limitStrNo;
	}
	
	/**
	 * @param limitSubNo
	 */
	public void setLimitSubNo(String limitSubNo) {
		this.limitSubNo = limitSubNo;
	}
	
    /**
     * @return limitSubNo
     */
	public String getLimitSubNo() {
		return this.limitSubNo;
	}
	
	/**
	 * @param limitSubName
	 */
	public void setLimitSubName(String limitSubName) {
		this.limitSubName = limitSubName;
	}
	
    /**
     * @return limitSubName
     */
	public String getLimitSubName() {
		return this.limitSubName;
	}
	
	/**
	 * @param parentId
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
    /**
     * @return parentId
     */
	public String getParentId() {
		return this.parentId;
	}
	
	/**
	 * @param isLriskLmt
	 */
	public void setIsLriskLmt(String isLriskLmt) {
		this.isLriskLmt = isLriskLmt;
	}
	
    /**
     * @return isLriskLmt
     */
	public String getIsLriskLmt() {
		return this.isLriskLmt;
	}
	
	/**
	 * @param lmtSubType
	 */
	public void setLmtSubType(String lmtSubType) {
		this.lmtSubType = lmtSubType;
	}
	
    /**
     * @return lmtSubType
     */
	public String getLmtSubType() {
		return this.lmtSubType;
	}
	
	/**
	 * @param isRevolv
	 */
	public void setIsRevolv(String isRevolv) {
		this.isRevolv = isRevolv;
	}
	
    /**
     * @return isRevolv
     */
	public String getIsRevolv() {
		return this.isRevolv;
	}
	
	/**
	 * @param limitType
	 */
	public void setLimitType(String limitType) {
		this.limitType = limitType;
	}
	
    /**
     * @return limitType
     */
	public String getLimitType() {
		return this.limitType;
	}
	
	/**
	 * @param isExpertLmt
	 */
	public void setIsExpertLmt(String isExpertLmt) {
		this.isExpertLmt = isExpertLmt;
	}
	
    /**
     * @return isExpertLmt
     */
	public String getIsExpertLmt() {
		return this.isExpertLmt;
	}
	
	/**
	 * @param suitBizLine
	 */
	public void setSuitBizLine(String suitBizLine) {
		this.suitBizLine = suitBizLine;
	}
	
    /**
     * @return suitBizLine
     */
	public String getSuitBizLine() {
		return this.suitBizLine;
	}
	
	/**
	 * @param isOverLmtTerm
	 */
	public void setIsOverLmtTerm(String isOverLmtTerm) {
		this.isOverLmtTerm = isOverLmtTerm;
	}
	
    /**
     * @return isOverLmtTerm
     */
	public String getIsOverLmtTerm() {
		return this.isOverLmtTerm;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}