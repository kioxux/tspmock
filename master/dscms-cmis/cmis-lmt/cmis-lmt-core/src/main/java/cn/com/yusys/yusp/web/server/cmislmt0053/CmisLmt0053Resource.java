package cn.com.yusys.yusp.web.server.cmislmt0053;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0053.req.CmisLmt0053ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0053.resp.CmisLmt0053RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0053.CmisLmt0053Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询多客户敞口金额和敞口余额
 *
 * @author zhangjw 20210905
 * @version 1.0
 */
@Api(tags = "cmislmt0053:查询多客户敞口金额和敞口余额")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0053Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0053Resource.class);

    @Autowired
    private CmisLmt0053Service cmisLmt0053Service;

    /**
     * 交易码：CmisLmt0053
     * 交易描述：查询多客户敞口金额和敞口余额
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("查询多客户敞口金额和敞口余额")
    @PostMapping("/cmislmt0053")
    protected @ResponseBody
    ResultDto<CmisLmt0053RespDto> CmisLmt0053(@Validated @RequestBody CmisLmt0053ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0053.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0053.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0053RespDto> CmisLmt0053RespDtoResultDto = new ResultDto<>();
        CmisLmt0053RespDto CmisLmt0053RespDto = new CmisLmt0053RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0053RespDto = cmisLmt0053Service.execute(reqDto);
            CmisLmt0053RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0053RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0053.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0053.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0053.value, e.getMessage());
            // 封装CmisLmt0053RespDtoResultDto中异常返回码和返回信息
            CmisLmt0053RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0053RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0053RespDto到CmisLmt0053RespDtoResultDto中
        CmisLmt0053RespDtoResultDto.setData(CmisLmt0053RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0053.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0053.value, JSON.toJSONString(CmisLmt0053RespDtoResultDto));
        return CmisLmt0053RespDtoResultDto;
    }
}