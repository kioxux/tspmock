/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: TmpAddamtAccAdddetails
 * @类描述: tmp_addamt_acc_adddetails数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-11-06 20:32:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_addamt_acc_adddetails")
public class TmpAddamtAccAdddetails extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 批复分项编号 **/
	@Id
	@Column(name = "APPR_SUB_SERNO")
	private String apprSubSerno;
	
	/** 业务流水号 **/
	@Id
	@Column(name = "cont_no")
	private String contNo;
	
	/** 占用总金额 **/
	@Column(name = "BIZ_TOTAL_AMT_CNY", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal bizTotalAmtCny;
	
	/** 占用敞口金额 **/
	@Column(name = "BIZ_SPAC_AMT_CNY", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal bizSpacAmtCny;
	
	/** 授信总额 **/
	@Column(name = "AVL_AMT", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal avlAmt;
	
	/** 授信敞口总额 **/
	@Column(name = "SPAC_AMT", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal spacAmt;
	
	
	/**
	 * @param apprSubSerno
	 */
	public void setApprSubSerno(String apprSubSerno) {
		this.apprSubSerno = apprSubSerno;
	}
	
    /**
     * @return apprSubSerno
     */
	public String getApprSubSerno() {
		return this.apprSubSerno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param bizTotalAmtCny
	 */
	public void setBizTotalAmtCny(java.math.BigDecimal bizTotalAmtCny) {
		this.bizTotalAmtCny = bizTotalAmtCny;
	}
	
    /**
     * @return bizTotalAmtCny
     */
	public java.math.BigDecimal getBizTotalAmtCny() {
		return this.bizTotalAmtCny;
	}
	
	/**
	 * @param bizSpacAmtCny
	 */
	public void setBizSpacAmtCny(java.math.BigDecimal bizSpacAmtCny) {
		this.bizSpacAmtCny = bizSpacAmtCny;
	}
	
    /**
     * @return bizSpacAmtCny
     */
	public java.math.BigDecimal getBizSpacAmtCny() {
		return this.bizSpacAmtCny;
	}
	
	/**
	 * @param avlAmt
	 */
	public void setAvlAmt(java.math.BigDecimal avlAmt) {
		this.avlAmt = avlAmt;
	}
	
    /**
     * @return avlAmt
     */
	public java.math.BigDecimal getAvlAmt() {
		return this.avlAmt;
	}
	
	/**
	 * @param spacAmt
	 */
	public void setSpacAmt(java.math.BigDecimal spacAmt) {
		this.spacAmt = spacAmt;
	}
	
    /**
     * @return spacAmt
     */
	public java.math.BigDecimal getSpacAmt() {
		return this.spacAmt;
	}


}