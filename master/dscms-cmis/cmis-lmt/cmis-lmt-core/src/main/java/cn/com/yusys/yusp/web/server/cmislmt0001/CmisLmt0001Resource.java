package cn.com.yusys.yusp.web.server.cmislmt0001;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.resp.CmisLmt0001RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0001.CmisLmt0001Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:单一客户额度同步
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "cmislmt0001:单一客户额度同步")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0001Resource.class);

    @Autowired
    private CmisLmt0001Service cmisLmt0001Service;
    /**
     * 交易码：cmislmt0001
     * 交易描述：单一客户额度同步
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("单一客户额度同步")
    @PostMapping("/cmislmt0001")
    protected @ResponseBody
    ResultDto<CmisLmt0001RespDto> cmisLmt0001(@Validated @RequestBody CmisLmt0001ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0001RespDto> cmisLmt0001RespDtoResultDto = new ResultDto<>();
        CmisLmt0001RespDto cmisLmt0001RespDto = new CmisLmt0001RespDto() ;
        try {
            // 调用对应的service层
            cmisLmt0001RespDto = cmisLmt0001Service.execute(reqDto);
            //设置陈工交易码值
            cmisLmt0001RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0001RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (YuspException e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0001.value, e);
            cmisLmt0001RespDto.setErrorCode(e.getCode());
            cmisLmt0001RespDto.setErrorMsg(e.getMsg());
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0001.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.value, e.getMessage());
            // 封装cmisLmt0001RespDtoResultDto中异常返回码和返回信息
            cmisLmt0001RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0001RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }

        // 封装cmisLmt0001RespDto到cmisLmt0001RespDtoResultDto中
        cmisLmt0001RespDtoResultDto.setData(cmisLmt0001RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.value, JSON.toJSONString(cmisLmt0001RespDtoResultDto));
        return cmisLmt0001RespDtoResultDto;
    }
}
