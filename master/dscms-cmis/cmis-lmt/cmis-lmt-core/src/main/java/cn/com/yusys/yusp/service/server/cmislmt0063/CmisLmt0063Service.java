package cn.com.yusys.yusp.service.server.cmislmt0063;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.dto.server.cmislmt0063.req.CmisLmt0063ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0063.resp.CmisLmt0063RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0063.resp.CmisLmt0063SubListRespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ApprLmtSubBasicInfoMapper;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0063Service
 * @类描述: #对内服务类
 * @功能描述: 根据客户号获取客户综合授信批复编号，到期日，起始日，期限等
 * @创建时间: 2021-09-30
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0063Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0063Service.class);

    @Autowired
    private ApprLmtSubBasicInfoMapper apprLmtSubBasicInfoMapper ;

    /**
     * 获取客户或分项明细用信综合总额和用信敞口余额（不包含合作方）
     * add by lizx 2021-09-09
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0063RespDto execute(CmisLmt0063ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0063.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0063.value);
        CmisLmt0063RespDto respDto = new CmisLmt0063RespDto();
        List<CmisLmt0063SubListRespDto> cmisLmt0063SubListRespDtoList = new ArrayList<>();
        try {
            //客户号
            String parentId = reqDto.getSubSerno();
            List<ApprLmtSubBasicInfo> apprLmtSubDetails = apprLmtSubBasicInfoMapper.selectByParentId(parentId) ;
            if(CollectionUtils.isNotEmpty(apprLmtSubDetails)){
                apprLmtSubDetails.forEach(apprLmtSubBasicInfo -> {
                    CmisLmt0063SubListRespDto cmisLmt0063SubListRespDto = new CmisLmt0063SubListRespDto();
                    cmisLmt0063SubListRespDto.setApprSubSerno(apprLmtSubBasicInfo.getApprSubSerno());
                    cmisLmt0063SubListRespDto.setLimitSubNo(apprLmtSubBasicInfo.getLimitSubNo());
                    cmisLmt0063SubListRespDto.setLimitSubName(apprLmtSubBasicInfo.getLimitSubName());
                    cmisLmt0063SubListRespDtoList.add(cmisLmt0063SubListRespDto);
                });
            }
            respDto.setCmisLmt0063SubListRespDtoList(cmisLmt0063SubListRespDtoList);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("【cmislmt0063】根据分项编号查询向下非失效状态的分项明细：" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0063.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0063.value);
        return respDto;
    }
}