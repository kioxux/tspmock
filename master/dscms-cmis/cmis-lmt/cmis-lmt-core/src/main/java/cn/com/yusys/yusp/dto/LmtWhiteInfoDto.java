package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtWhiteInfo
 * @类描述: lmt_white_info数据实体类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-05-03 10:31:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtWhiteInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** PK_ID **/
	private String pkId;
	
	/** 金融机构代码 **/
	private String instuCde;
	
	/** 额度分项编号 **/
	private String subAccNo;
	
	/** 白名单额度类型STD_ZB_LMT_WHITE_TYPE **/
	private String lmtWhiteType;
	
	/** 客户号 **/
	private String cusId;
	
	/** 总行行号 **/
	private String aorgNo;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 机构类型STD_ZB_INTB_SUB_TYPE **/
	private String cusType;
	
	/** 限额 **/
	private java.math.BigDecimal sigAmt;
	
	/** 单个产品户买入返售限额 **/
	private java.math.BigDecimal singleResaleQuota;
	
	/** 已用限额 **/
	private java.math.BigDecimal sigUseAmt;
	
	/** 生效日期 **/
	private String startDate;
	
	/** 到期日期 **/
	private String endDate;
	
	/** 操作类型STD_ZB_OPR_TYPE **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param instuCde
	 */
	public void setInstuCde(String instuCde) {
		this.instuCde = instuCde == null ? null : instuCde.trim();
	}
	
    /**
     * @return InstuCde
     */	
	public String getInstuCde() {
		return this.instuCde;
	}
	
	/**
	 * @param subAccNo
	 */
	public void setSubAccNo(String subAccNo) {
		this.subAccNo = subAccNo == null ? null : subAccNo.trim();
	}
	
    /**
     * @return SubAccNo
     */	
	public String getSubAccNo() {
		return this.subAccNo;
	}
	
	/**
	 * @param lmtWhiteType
	 */
	public void setLmtWhiteType(String lmtWhiteType) {
		this.lmtWhiteType = lmtWhiteType == null ? null : lmtWhiteType.trim();
	}
	
    /**
     * @return LmtWhiteType
     */	
	public String getLmtWhiteType() {
		return this.lmtWhiteType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param aorgNo
	 */
	public void setAorgNo(String aorgNo) {
		this.aorgNo = aorgNo == null ? null : aorgNo.trim();
	}
	
    /**
     * @return AorgNo
     */	
	public String getAorgNo() {
		return this.aorgNo;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType == null ? null : cusType.trim();
	}
	
    /**
     * @return CusType
     */	
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param sigAmt
	 */
	public void setSigAmt(java.math.BigDecimal sigAmt) {
		this.sigAmt = sigAmt;
	}
	
    /**
     * @return SigAmt
     */	
	public java.math.BigDecimal getSigAmt() {
		return this.sigAmt;
	}
	
	/**
	 * @param singleResaleQuota
	 */
	public void setSingleResaleQuota(java.math.BigDecimal singleResaleQuota) {
		this.singleResaleQuota = singleResaleQuota;
	}
	
    /**
     * @return SingleResaleQuota
     */	
	public java.math.BigDecimal getSingleResaleQuota() {
		return this.singleResaleQuota;
	}
	
	/**
	 * @param sigUseAmt
	 */
	public void setSigUseAmt(java.math.BigDecimal sigUseAmt) {
		this.sigUseAmt = sigUseAmt;
	}
	
    /**
     * @return SigUseAmt
     */	
	public java.math.BigDecimal getSigUseAmt() {
		return this.sigUseAmt;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}
	
    /**
     * @return StartDate
     */	
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return EndDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}