package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

@ExcelCsv(namePrefix = "单一指标风险暴露查询", fileType = ExcelCsv.ExportFileType.XLS)
public class RiskExpose02ExportVo {


    /**
     * 客户编号
     */
    @ExcelField(title = "客户编号", viewLength = 20)
    private String custId;

    /**
     * 客户名称
     */
    @ExcelField(title = "客户名称", viewLength = 20)
    private String custName;

    /**
     * 指标值
     */
    @ExcelField(title = "指标值（万元）", viewLength = 20, format = "#0.00")
    private BigDecimal zbLmt;

    /**
     * 授信总额（万元）
     */
    @ExcelField(title = "授信总额（万元）", viewLength = 20, format = "#0.00")
    private BigDecimal sumLmt;

    /**
     * 用信余额（万元）
     */
    @ExcelField(title = "用信余额（万元）", viewLength = 20, format = "#0.00")
    private BigDecimal sumUseLmt;

    /**
     * 指标日期
     */
    @ExcelField(title = "指标日期", viewLength = 20)
    private String zbDate;


    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public BigDecimal getZbLmt() {
        return zbLmt;
    }

    public void setZbLmt(BigDecimal zbLmt) {
        this.zbLmt = zbLmt;
    }

    public BigDecimal getSumLmt() {
        return sumLmt;
    }

    public void setSumLmt(BigDecimal sumLmt) {
        this.sumLmt = sumLmt;
    }

    public BigDecimal getSumUseLmt() {
        return sumUseLmt;
    }

    public void setSumUseLmt(BigDecimal sumUseLmt) {
        this.sumUseLmt = sumUseLmt;
    }

    public String getZbDate() {
        return zbDate;
    }

    public void setZbDate(String zbDate) {
        this.zbDate = zbDate;
    }
}
