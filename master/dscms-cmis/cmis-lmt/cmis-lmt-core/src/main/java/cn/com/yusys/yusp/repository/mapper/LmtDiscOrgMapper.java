/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtDiscOrg;
import cn.com.yusys.yusp.dto.server.cmislmt0044.resp.CmisLmt0044ListRespDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtDiscOrgMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-24 09:15:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtDiscOrgMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtDiscOrg selectByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtDiscOrg> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtDiscOrg record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtDiscOrg record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtDiscOrg record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtDiscOrg record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 根据queryModel查询集团客户的批复台账编号
     * @param queryModel
     * @return
     */
    List<CmisLmt0044ListRespDto> queryDetailByreqDto(QueryModel queryModel);


    /**
     * 根据queryModel查询集团客户的批复台账编号
     * @param queryModel
     * @return
     */
    List<LmtDiscOrg> queryDetailByOrgAndYm(QueryModel queryModel);

    /**
     * 获取贴现额度
     * @param mmyy
     * @param openDay
     * @param organno
     * @return
     */
    List<LmtDiscOrg> selectByCmisLmt0051(@Param("mmyy") String mmyy,@Param("openDay") String openDay, @Param("organno") String organno);
}