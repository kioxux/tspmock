/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.ManaOrgLmt;
import cn.com.yusys.yusp.dto.server.cmisbiz0003.req.CmisBiz0003ReqDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0003.resp.CmisBiz0003RespDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0003.resp.CurrentMonthAllowLoanBlanceDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ManaOrgLmtMapper;
import cn.com.yusys.yusp.vo.ManaOrgLmtVo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ManaOrgLmtService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-16 15:32:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ManaOrgLmtService {

    private static final Logger logger = LoggerFactory.getLogger(ManaOrgLmtService.class);

    @Autowired
    private ManaOrgLmtMapper manaOrgLmtMapper;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public ManaOrgLmt selectByPrimaryKey(String orgId) {
        return manaOrgLmtMapper.selectByPrimaryKey(orgId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<ManaOrgLmt> selectAll(QueryModel model) {
        List<ManaOrgLmt> records = (List<ManaOrgLmt>) manaOrgLmtMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<ManaOrgLmt> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ManaOrgLmt> list = manaOrgLmtMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    public List<Map> selectByModel2(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ManaOrgLmt> list = manaOrgLmtMapper.selectByModel(model);

        List<Map> pageList = new ArrayList<>();

        if (CollectionUtils.nonEmpty(list)){
            List<String> orgIdList = list.stream().map(ManaOrgLmt::getOrgId).collect(Collectors.toList());
            CmisBiz0003ReqDto cmisBiz0003ReqDto = new CmisBiz0003ReqDto();
            cmisBiz0003ReqDto.setOrgIdList(orgIdList);
            cmisBiz0003ReqDto.setBelgLine(CmisBizConstants.BELGLINE_DG_03);
            logger.info("cmisBiz0003--->reqDto===>{}",cmisBiz0003ReqDto);
            ResultDto<CmisBiz0003RespDto> cmisBiz0003RespDtoResultDto = cmisBizClientService.cmisBiz0003(cmisBiz0003ReqDto);
            logger.info("cmisBiz0003--->respDto===>{}",cmisBiz0003RespDtoResultDto);
            String code = cmisBiz0003RespDtoResultDto.getCode();
            if (!SuccessEnum.CMIS_SUCCSESS.key.equals(code)){
                throw BizException.error(null,cmisBiz0003RespDtoResultDto.getCode(),cmisBiz0003RespDtoResultDto.getMessage());
            }
            CmisBiz0003RespDto data = cmisBiz0003RespDtoResultDto.getData();
            if (!SuccessEnum.SUCCESS.key.equals(data.getErrorCode())){
                throw BizException.error(null,data.getErrorCode(),data.getErrorMsg());
            }
            Map<String, CurrentMonthAllowLoanBlanceDto> currMonthAllowLoanBalanceMap = data.getCurrMonthAllowLoanBalanceMap();
            for (ManaOrgLmt manaOrgLmt : list) {
                Map<String, Object> stringObjectMap = ManaOrgLmtService.BeanToMap(manaOrgLmt);
                String orgId = manaOrgLmt.getOrgId();
                CurrentMonthAllowLoanBlanceDto currentMonthAllowLoanBlanceDto = currMonthAllowLoanBalanceMap.get(orgId);
                if (!SuccessEnum.SUCCESS.key.equals(currentMonthAllowLoanBlanceDto.getErrorCode())){
                    stringObjectMap.put("currMonthAllowLoanBalance",currentMonthAllowLoanBlanceDto.getErrorCode());
                    continue;
                }
                BigDecimal currentMonthAllowLoanBlance = currentMonthAllowLoanBlanceDto.getCurrentMonthAllowLoanBlance().divide(BigDecimal.valueOf(10000));
                //当月剩余可投放对公贷款余额 = 上月末对公贷款余额+当月可净新增对公贷款投放金额-当前对公人民币贷款余额
                //0+1_0000_0000-400_0000 = 9600_0000
                BigDecimal subtract = manaOrgLmt.getLastMonthComLoanBalance().add(manaOrgLmt.getCurrMonthAllowComAddAmt()).subtract(currentMonthAllowLoanBlance);
                stringObjectMap.put("currMonthAllowLoanBalance",subtract);
                pageList.add(stringObjectMap);
            }
        }
        PageHelper.clearPage();
        return pageList;
    }

    /**
     * 异步导出分支机构额度管控数据
     * @return 导出进度信息
     */
    public ProgressDto asyncExportManaOrgLmt(QueryModel model) {
        model.getCondition().put("oprType", CmisBizConstants.OPR_TYPE_01);
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel)object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            List<ManaOrgLmt> manaOrgLmts = selectByModel(queryModeTemp);

            List<ManaOrgLmtVo> manaOrgLmtVoList = new ArrayList<>();
            if (CollectionUtils.nonEmpty(manaOrgLmts)){
                manaOrgLmts.stream().forEach(a->{
                    ManaOrgLmtVo manaOrgLmtVo = new ManaOrgLmtVo();
                    BeanUtils.copyProperties(a,manaOrgLmtVo);

                    //获取机构名称
                    manaOrgLmtVo.setOrgName(OcaTranslatorUtils.getOrgName(a.getOrgId()));

//                    //当月剩余可投放对公贷款余额
//                    BigDecimal currMonthAllowLoanBalance = a.getLastMonthComLoanBalance()
//                            .add(a.getCurrMonthAllowComAddAmt())
//                            .subtract(a.getLastDayComLoanBalance());
//                    manaOrgLmtVo.setCurrMonthAllowLoanBalance(currMonthAllowLoanBalance);

                    //月末对公贷款余额测算
                    BigDecimal curMonthComLoanBalancePlan = a.getLastMonthComLoanBalance()
                            .add(a.getCurrMonthAllowComAddAmt());
                    manaOrgLmtVo.setCurMonthComLoanBalancePlan(curMonthComLoanBalancePlan);

                    manaOrgLmtVoList.add(manaOrgLmtVo);
                });
            }

            return manaOrgLmtVoList;
        };
        ExportContext exportContext = ExportContext.of(ManaOrgLmtVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(ManaOrgLmt record) {
        return manaOrgLmtMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(ManaOrgLmt record) {
        return manaOrgLmtMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(ManaOrgLmt record) {
        return manaOrgLmtMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(ManaOrgLmt record) {
        return manaOrgLmtMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String orgId) {
        return manaOrgLmtMapper.deleteByPrimaryKey(orgId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return manaOrgLmtMapper.deleteByIds(ids);
    }


    /**
     *
     * @param obj
     * @return
     */
    public static Map<String,Object> BeanToMap(Object obj){
        if (obj != null){
            Map<String,Object> newMap = new HashMap<>();
            Map<String, Object> map = (Map<String, Object>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(obj);
            map.entrySet().forEach(a->{
                newMap.put(a.getKey(),a.getValue());
            });
            return newMap;
        }
        return null;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<Map<String,String>> queryOrgList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<String> list = manaOrgLmtMapper.queryOrgList(model);

        List<Map<String,String>> resultList = new ArrayList<>();

        if(list!=null && list.size()>0){

            for(String orgId : list ){
                String orgName = OcaTranslatorUtils.getOrgName(orgId);//
                Map<String,String> map = new HashMap<String,String>();
                map.put("orgId",orgId);
                map.put("orgName",orgName);
                resultList.add(map);
            }
        }
        PageHelper.clearPage();
        return resultList;
    }

    /**
     * @方法名称: selectOrgList
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<String> selectOrgList(QueryModel model) {
        List<String> list = manaOrgLmtMapper.queryOrgList(model);
        return list;
    }


}
