package cn.com.yusys.yusp.dto.appr;

import java.io.Serializable;
import java.util.List;

/**
 * 额度结构产品树
 * @author reny
 * create at 2021/4/16 9:02
 **/
public class ApprLmtSubPrdTreeDto implements Serializable {
    private static final long serialVersionUID = 4532789264068469642L;
    public static final String LMT = "lmt";
    public static final String PRD = "prd";
    /**
     * 当前节点id
     */
    private String id;
    /**
     * 父节点id
     */
    private String pid;
    /**
     * 节点类型：lmt-额度授信,prd-产品
     */
    private String nodeType;
    /**
     * 节点编号
     */
    private String nodeNo;
    /**
     * 节点名称
     */
    private String nodeName;
    /**
     * 是否为顶级，额度授信/产品有各自的顶级
     */
    private Boolean isHead;
    /**
     * 是否为叶子节点，额度授信/产品有各自的顶级
     */
    private Boolean isLeaf;
    /**
     * 子节点
     */
    private List<ApprLmtSubPrdTreeDto> children;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getNodeType() {
        return nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    public String getNodeNo() {
        return nodeNo;
    }

    public void setNodeNo(String nodeNo) {
        this.nodeNo = nodeNo;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public Boolean getIsHead() {
        return isHead;
    }

    public void setIsHead(Boolean head) {
        isHead = head;
    }

    public Boolean getIsLeaf() {
        return isLeaf;
    }

    public void setIsLeaf(Boolean leaf) {
        isLeaf = leaf;
    }

    public List<ApprLmtSubPrdTreeDto> getChildren() {
        return children;
    }

    public void setChildren(List<ApprLmtSubPrdTreeDto> children) {
        this.children = children;
    }
}
