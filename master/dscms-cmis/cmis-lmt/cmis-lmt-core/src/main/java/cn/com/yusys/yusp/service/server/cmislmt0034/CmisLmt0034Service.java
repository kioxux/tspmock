package cn.com.yusys.yusp.service.server.cmislmt0034;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtContRel;
import cn.com.yusys.yusp.domain.LmtSubPrdMappConf;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.req.CmisLmt0034DealBizListReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.req.CmisLmt0034OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.req.CmisLmt0034ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.resp.CmisLmt0034RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.resp.CmisLmt0013RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.LmtContRelService;
import cn.com.yusys.yusp.service.LmtSubPrdMappConfService;
import cn.com.yusys.yusp.service.LmtWhiteInfoService;
import cn.com.yusys.yusp.service.server.cmislmt0011.CmisLmt0011Service;
import cn.com.yusys.yusp.service.server.cmislmt0013.CmisLmt0013Service;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Service
public class CmisLmt0034Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0034Service.class);

    @Autowired
    private LmtSubPrdMappConfService lmtSubPrdMappConfService ;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;

    @Autowired
    private LmtWhiteInfoService lmtWhiteInfoService ;

    @Autowired
    private CmisLmt0011Service cmisLmt0011Service ;

    @Autowired
    private CmisLmt0013Service cmisLmt0013Service ;

    @Autowired
    private Comm4Service comm4Service;

    @Autowired
    private LmtContRelService lmtContRelService ;

    /**
     * 国结票据出账额度占用
     * add by dumd 20210623
     * @param reqDto
     * 描述：合同编号有值，台账明细列表组装数据，占用台账占用接口CmisLmt0013 接口 台账占用，占用额度列表（同业客户额度）有值，调用CmisLmt0011接口额度占用
     *      合同编号无值：根据额度列表（同业客户额度）有值 组装CmisLmt0011 额度占用
     * @return
     */
    @Transactional
    public CmisLmt0034RespDto execute(CmisLmt0034ReqDto reqDto) throws YuspException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0034.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0034.value);
        CmisLmt0034RespDto respDto = new CmisLmt0034RespDto();

        //合同编号
        String bizNo = reqDto.getBizNo();
        //合同编号不为空，占用台账信息
        if(StringUtils.isNotEmpty(bizNo)){
            initCmisLmt0013ReqData(reqDto) ;
        }

        //同业列表部位空，占用同业客户额度信息
        List<CmisLmt0034OccRelListReqDto> occRelList = reqDto.getOccRelList() ;
        if(!CollectionUtils.isEmpty(occRelList) && StringUtils.isNotEmpty(occRelList.get(0).getLmtCusId())){
            initCmisLmt0011ReqData(reqDto) ;
        }

        respDto.setErrorCode(SuccessEnum.SUCCESS.key);
        respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0034.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0034.value);
        return respDto;
    }


    /**
     * @作者:lizx
     * @方法名称: initCmisLmt0013ReqData
     * @方法描述:  根据0034的请求报文，组装0013 台账占用的请求报文，并且返回该请求dto
     * @参数与返回说明:返回CmisLmt0013请求dto
     * @算法描述: 无
     * @日期：2021/7/7 14:41
     * @param reqDto:
     * @return: cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDto
     * @算法描述: 无
    */
    public void initCmisLmt0013ReqData(CmisLmt0034ReqDto reqDto) throws Exception {
        CmisLmt0013ReqDto cmisLmt0013ReqDto =  new CmisLmt0013ReqDto() ;
        List<CmisLmt0013ReqDealBizListDto> cmisLmt0013ReqDealBizListDtos = new ArrayList<>() ;
        //数据拷贝
        BeanUtils.copyProperties(reqDto, cmisLmt0013ReqDto);
        //系统编号单独处理
        cmisLmt0013ReqDto.setSysId(reqDto.getSysNo());
        //集合单独处理
        List<CmisLmt0034DealBizListReqDto> dealBizList = reqDto.getDealBizList();
        Supplier<CmisLmt0013ReqDealBizListDto> cmis0013Sup = CmisLmt0013ReqDealBizListDto::new ;
        //合同编号
        String bizNo = reqDto.getBizNo() ;
        //根据合同编号，获取合同信息
        //List<LmtContRel> lmtContRelLsit = lmtContRelService.selectLmtContRelByDealBizNo(bizNo) ;
        List<LmtContRel> lmtContRelLsit = lmtContRelService.selectLmtContRelListByDealBizNo(bizNo, CmisLmtConstants.STD_ZB_BIZ_ATTR_1) ;
        if(CollectionUtils.isEmpty(lmtContRelLsit)){
            throw new YuspException(EclEnum.ECL070018.key, EclEnum.ECL070018.value);
        }
        //请求报文赋值到0013 集合中
        dealBizList.forEach(dealBiz -> {
            CmisLmt0013ReqDealBizListDto cmisLmt0013ReqDealBizListDto = cmis0013Sup.get() ;
/*            String cusId = dealBiz.getCusId() ;
            if(StringUtils.isEmpty(cusId)){
                throw new YuspException(EclEnum.ECL070088.key, EclEnum.ECL070088.value);
            }
            lmtContRelLsit.forEach(lmtContRel -> {
                if(!cusId.equals(lmtContRel.getCusId())){
                    throw new YuspException(EclEnum.ECL070089.key, EclEnum.ECL070089.value);
                }
            });*/
            //数据拷贝
            BeanUtils.copyProperties(dealBiz, cmisLmt0013ReqDealBizListDto);
            //数据类型转换
            String prdId = dealBiz.getPrdNo() ;
            cmisLmt0013ReqDealBizListDto.setPrdId(prdId);
            cmisLmt0013ReqDealBizListDtos.add(cmisLmt0013ReqDealBizListDto) ;
        });
        cmisLmt0013ReqDto.setCmisLmt0013ReqDealBizListDtos(cmisLmt0013ReqDealBizListDtos);
        //发送报文
        logger.info("CmisLmt0034发送报文到CmisLmt0013 请求报文内容【{}】", JSON.toJSONString(cmisLmt0013ReqDto));
        CmisLmt0013RespDto cmisLmt0013RespDto =  cmisLmt0013Service.execute(cmisLmt0013ReqDto) ;
        logger.info("CmisLmt0034发送报文到CmisLmt0013 响应报文内容【{}】", JSON.toJSONString(cmisLmt0013RespDto));
        if(Objects.nonNull(cmisLmt0013RespDto) && !Objects.equals(SuccessEnum.SUCCESS.key, cmisLmt0013RespDto.getErrorCode())){
            throw new YuspException(cmisLmt0013RespDto.getErrorCode(), cmisLmt0013RespDto.getErrorMsg());
        }
    }

    /**
     * @作者:lizx
     * @方法名称: initCmisLmt0011ReqData
     * @方法描述:  组长额度占用报文信息，占用额度数据
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/7 16:50
     * @param reqDto:
     * @return: void
     * @算法描述: 无
    */
    public void initCmisLmt0011ReqData(CmisLmt0034ReqDto reqDto) throws Exception {
        logger.info("CmisLmt0034发送报文到CmisLmt0011报文内容-------->start");
        Supplier<CmisLmt0011ReqDto> cmisLmt0011Supr = CmisLmt0011ReqDto::new ;
        List<CmisLmt0034OccRelListReqDto> occRelList = reqDto.getOccRelList() ;
        List<CmisLmt0034DealBizListReqDto> dealBizList = reqDto.getDealBizList() ;
        for (CmisLmt0034DealBizListReqDto dealBizInfo : dealBizList) {
            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<>() ;
            CmisLmt0011ReqDto cmisLmt0011ReqDto = cmisLmt0011Supr.get() ;
            //台账编号
            String dealBizNo = dealBizInfo.getDealBizNo() ;

            //CmisLmt0034DealBizListReqDto cmisLmt0034DealBizListReqDto = getDealBizInfoByDealBizNo(reqDto.getDealBizList(), dealBizNo) ;
            String prdId = dealBizInfo.getPrdNo() ;
            BigDecimal dealBizAmtCny = dealBizInfo.getDealBizAmtCny() ;

            //0034请求报文对比差距太大，此处用set方式赋值处理
            //处理0011 请求报文主体信息
            cmisLmt0011ReqDto.setSysId(reqDto.getSysNo());
            cmisLmt0011ReqDto.setInstuCde(reqDto.getInstuCde());
            //该合同编号从list中获取
            cmisLmt0011ReqDto.setDealBizNo(dealBizNo);
            cmisLmt0011ReqDto.setCusName(dealBizInfo.getCusName());
            cmisLmt0011ReqDto.setDealBizType(CmisLmtConstants.DEAL_BIZ_TYPE_1);
            cmisLmt0011ReqDto.setBizAttr(CmisLmtConstants.STD_ZB_BIZ_ATTR_2);
            cmisLmt0011ReqDto.setPrdId(prdId);
            cmisLmt0011ReqDto.setPrdName(dealBizInfo.getPrdName());
            cmisLmt0011ReqDto.setIsLriskBiz(CmisLmtConstants.YES_NO_N);
            cmisLmt0011ReqDto.setIsBizRev(CmisLmtConstants.YES_NO_N);
            cmisLmt0011ReqDto.setIsFollowBiz(CmisLmtConstants.YES_NO_N);
            cmisLmt0011ReqDto.setOrigiDealBizNo("");
            cmisLmt0011ReqDto.setOrigiDealBizStatus("");
            cmisLmt0011ReqDto.setOrigiRecoverType("");
            cmisLmt0011ReqDto.setOrigiBizAttr("");
            cmisLmt0011ReqDto.setDealBizAmt(dealBizInfo.getDealBizAmtCny());
            cmisLmt0011ReqDto.setDealBizBailPreRate(BigDecimal.ZERO);
            cmisLmt0011ReqDto.setDealBizBailPreAmt(BigDecimal.ZERO);
            cmisLmt0011ReqDto.setStartDate(dealBizInfo.getStartDate());
            cmisLmt0011ReqDto.setEndDate(dealBizInfo.getEndDate());
            cmisLmt0011ReqDto.setDealBizStatus(dealBizInfo.getDealBizStatus());
            cmisLmt0011ReqDto.setInputId(reqDto.getInputId());
            cmisLmt0011ReqDto.setInputBrId(reqDto.getInputBrId());
            cmisLmt0011ReqDto.setInputDate(reqDto.getInputDate());

            //额度类型
            String lmtType = "" ;
            if(CmisLmtConstants.STD_PJ_PRD_ID_052198.equals(dealBizInfo.getPrdNo())){
                lmtType = CmisLmtConstants.STD_ZB_LMT_TYPE_06 ;
            }else{
                lmtType = CmisLmtConstants.STD_ZB_LMT_TYPE_07 ;
            }

            List<CmisLmt0034OccRelListReqDto> occDealRelList = occRelList.stream().filter(occRel->occRel.getDealBizNo().equals(dealBizNo)).collect(Collectors.toList());
            for (CmisLmt0034OccRelListReqDto cmisLmt0034OccRelListReqDto : occDealRelList) {
                CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto() ;
                //同业客户号
                String lmtCusId = cmisLmt0034OccRelListReqDto.getLmtCusId() ;
                //如果是票据的交易，cusId传的是行号，需要转换为客户号
                if(CmisBizConstants.SYS_NO_PJP.equals(reqDto.getSysNo())){
                    logger.info("票据系统交易，将同业客户大额行号【"+lmtCusId+"】转换为客户号开始----->start");
                    lmtCusId = comm4Service.queryCusIdByBankNo(lmtCusId);
                    logger.info("票据系统交易，将同业客户大额行号转换为客户号【"+lmtCusId+"】结束----->start");

                    //票据类业务，转换为同业客户号信息，发送请求也为同业客户信息  TODO:此处是否考虑吧不进行转换，调用0058 额度释放是也不进行转换
                    cmisLmt0011ReqDto.setCusId(lmtCusId);
                }else{
                    cmisLmt0011ReqDto.setCusId(dealBizInfo.getCusId());
                }

                //占用敞口
                BigDecimal bizSpacAmtCny = cmisLmt0034OccRelListReqDto.getBizSpacAmtCny() ;
                //根据同业客户信息和产品信息反向推算分项信息，获取分项编号
                String lmtSubNo = getSubNoByCusIdAndPrdId(lmtCusId, lmtType, prdId) ;
                cmisLmt0011OccRelListDto.setLmtType(lmtType);
                cmisLmt0011OccRelListDto.setLmtSubNo(lmtSubNo);
                cmisLmt0011OccRelListDto.setBizTotalAmtCny(bizSpacAmtCny);
                cmisLmt0011OccRelListDto.setBizSpacAmtCny(bizSpacAmtCny);
                cmisLmt0011OccRelListDto.setPrdTypeProp(dealBizInfo.getPrdTypeProp());
                cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto) ;
                cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);
            }
            //发送报文
            logger.info("CmisLmt0034发送报文到CmisLmt0011报文内容【{}】", JSON.toJSONString(cmisLmt0011ReqDto));
            CmisLmt0011RespDto respDto = cmisLmt0011Service.execute(cmisLmt0011ReqDto) ;
            logger.info("CmisLmt0034发送报文到CmisLmt0011响应报文内容【{}】-------->", respDto);
            if(Objects.nonNull(respDto) && !Objects.equals(SuccessEnum.SUCCESS.key, respDto.getErrorCode())){
                throw new YuspException(respDto.getErrorCode(), respDto.getErrorMsg());
            }
        }
    }

    /**
     * @作者:lizx
     * @方法名称: getDealBizInfoByDealBizNo
     * @方法描述:  根据台账编号，查询台账明细列表中的台账编号为该编号的信息数据
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/7 15:29
     * @param dealBizList:
     * @param dealBizNo:
     * @return: cn.com.yusys.yusp.dto.server.cmisLmt0034.req.CmisLmt0034DealBizListReqDto
     * @算法描述: 无
    */
    public CmisLmt0034DealBizListReqDto getDealBizInfoByDealBizNo(List<CmisLmt0034DealBizListReqDto> dealBizList, String dealBizNo){
        for (CmisLmt0034DealBizListReqDto cmisLmt0034DealBizListReqDto : dealBizList) {
            if(dealBizNo.equals(cmisLmt0034DealBizListReqDto.getDealBizNo())){
                return cmisLmt0034DealBizListReqDto ;
            }
        }
        return null ;
    }


    /**
     * @作者:lizx
     * @方法名称: getSubNoByCusIdAndPrdId
     * @方法描述:  根据额度类型，同业客户号，用信产品，获取额度分项编号信息
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/7/7 16:42
     * @param lmtCusId: 
     * @param lmtType: 
     * @param prdId: 
     * @return: java.lang.String
     * @算法描述: 无
    */
    public String getSubNoByCusIdAndPrdId(String lmtCusId, String lmtType, String prdId){
        String lmtSubNo = "" ;
        //根据产品编号，反向推算额度产品编号
        //LmtSubPrdMappConf lmtSubPrdMappConf = lmtSubPrdMappConfService.selectByPrdMappByPrdId(prdId) ;
        //String limitPrdId = lmtSubPrdMappConf.getLimitSubNo() ;

        List<Map<String, String>> limitList = lmtSubPrdMappConfService.selectSubNoAndNameByStrNoAndPrdId(CmisLmtConstants.STD_ZB_LMT_STR_MTABLE_CONF_03, prdId) ;
        if(org.apache.commons.collections4.CollectionUtils.isEmpty(limitList)){
            throw new YuspException(EclEnum.ECL070140.key, EclEnum.ECL070140.value);
        }
        String limitPrdId = limitList.get(0).get("limitSubNo") ;
        //根据客户编号和产品编查询分项信息
        if(CmisLmtConstants.STD_ZB_LMT_TYPE_07.equals(lmtType)){
            //同业客户额度信息
            lmtSubNo = apprLmtSubBasicInfoService.selectApprSubSernoByCusIdAndLmtSubNo(lmtCusId, limitPrdId) ;
        }else{
            //白名单客户额度信息
            lmtSubNo = lmtWhiteInfoService.selectSubAccNoByCusId(lmtCusId) ;
        }
        logger.info("查询同业数据信息，获取分项额度编号为：-------------------->{}", lmtSubNo);
        return lmtSubNo ;
    }
}