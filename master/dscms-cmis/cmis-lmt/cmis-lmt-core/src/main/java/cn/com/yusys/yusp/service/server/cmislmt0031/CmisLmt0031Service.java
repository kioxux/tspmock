package cn.com.yusys.yusp.service.server.cmislmt0031;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ContAccRel;
import cn.com.yusys.yusp.domain.LmtContRel;
import cn.com.yusys.yusp.dto.server.cmisLmt0031.req.CmisLmt0031ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0031.resp.CmisLmt0031RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CusGrpMemberRelMapper;
import cn.com.yusys.yusp.service.ContAccRelService;
import cn.com.yusys.yusp.service.LmtContRelService;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class CmisLmt0031Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0031Service.class);

    @Autowired
    private Comm4Service comm4Service ;

    @Autowired
    private CusGrpMemberRelMapper cusGrpMemberRelMapper ;

    /**
     * 查询客户是否存在有效的非标额度
     * add by macm 20210615
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0031RespDto execute(CmisLmt0031ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0031.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0031.value);
        CmisLmt0031RespDto respDto = new CmisLmt0031RespDto();
        // 客户号
        String cusId = reqDto.getCusId();
        try {
             /*
                是否查询集团向下 1-是 0-否
                1：根据当前客户查找是所属集团向下的所有子公司，如果有有自动存在相关授信，则返回是 否则返回否
                0：不查询集团
             */
            String isQuryGrp = reqDto.getIsQuryGrp() ;
            //如果查询集团向下
            if(CmisLmtConstants.YES_NO_Y.equals(isQuryGrp)) {
                //根据改客户号查询该客户集团向下的客户信息
                List<Map<String, String>> cusMemList = cusGrpMemberRelMapper.selectByCusIdQueryGrpNoCuss(cusId);
                //空处理
                if (CollectionUtils.nonEmpty(cusMemList)) {
                    //遍历查询
                    for (Map<String, String> cusMap : cusMemList) {
                        String memCusId = cusMap.get("cusId") ;
                        //集团下可以等于当前客户跳过此循环
                        logger.info("{}:查询集团向下客户是否存在非标，成员客户信息{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0031.key, memCusId);
                        String isExists = extracted(memCusId);
                        logger.info("{}:查询集团向下客户是否存在非标，查询结果{} 【1-存在；2-不存在】", DscmsLmtEnum.TRADE_CODE_CMISLMT0031.key, isExists);
                        if(CmisLmtConstants.YES_NO_Y.equals(isExists)) {
                            respDto.setIsExists(CmisLmtConstants.YES_NO_Y);
                            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
                            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
                            return respDto ;
                        }
                    }
                }
            }else{
                logger.info("{}:查询客户是否存在非标，客户信息{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0031.key, cusId);
                String isExists = extracted(cusId);
                logger.info("{}:查询客户是否存在非标，查询结果{} 【1-存在；2-不存在】", DscmsLmtEnum.TRADE_CODE_CMISLMT0031.key, isExists);
                if(CmisLmtConstants.YES_NO_Y.equals(isExists)){
                    respDto.setIsExists(CmisLmtConstants.YES_NO_Y);
                    respDto.setErrorCode(SuccessEnum.SUCCESS.key);
                    respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
                    return respDto ;
                }
            }
            respDto.setIsExists(CmisLmtConstants.YES_NO_N);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("查询客户是否存在有效的非标额度" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0031.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0031.value);
        return respDto;
    }

    private String extracted(String cusId) {
        //客户有效非标资产授信（未到期）金额之和
        BigDecimal totalAssetLmtAmt = comm4Service.getTotalAssetLmtAmtNonStandardByCusId(cusId);
        //客户名下非标资产余额（已到期部分）
        BigDecimal totalAssetBalanceAmt = comm4Service.getTotalAssetBalanceAmtNonStandardByCusId(cusId);
        //客户非标资产授信余额 = 客户有效非标资产授信（未到期）金额之和 + 客户名下非标资产产余额（已到期部分）
        BigDecimal lmtBalanceAmt = totalAssetLmtAmt.add(totalAssetBalanceAmt);
        // 判断是否存在
        if(lmtBalanceAmt.compareTo(BigDecimal.ZERO) > 0){// 存在
            return CmisLmtConstants.YES_NO_Y ;
        }
        return CmisLmtConstants.YES_NO_N ;
    }
}