package cn.com.yusys.yusp.web.server.cmislmt0056;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0056.req.CmisLmt0056ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0056.resp.CmisLmt0056RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0056.CmisLmt0056Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:获取客户有效的低风险额度分项明细编号
 *
 * @author lizx 20210909
 * @version 1.0
 */
@Api(tags = "cmislmt0056:获取客户有效的低风险额度分项明细编号")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0056Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0056Resource.class);

    @Autowired
    private CmisLmt0056Service cmisLmt0056Service;

    /**
     * 交易码：CmisLmt0056
     * 交易描述：获取客户有效的低风险额度分项明细编号
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("获取客户有效的特定目的载体投资额度列表")
    @PostMapping("/cmislmt0056")
    protected @ResponseBody
    ResultDto<CmisLmt0056RespDto> CmisLmt0056(@Validated @RequestBody CmisLmt0056ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0056.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0056.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0056RespDto> CmisLmt0056RespDtoResultDto = new ResultDto<>();
        CmisLmt0056RespDto CmisLmt0056RespDto = new CmisLmt0056RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0056RespDto = cmisLmt0056Service.execute(reqDto);
            CmisLmt0056RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0056RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0056.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0056.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0056.value, e.getMessage());
            // 封装CmisLmt0056RespDtoResultDto中异常返回码和返回信息
            CmisLmt0056RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0056RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0056RespDto到CmisLmt0056RespDtoResultDto中
        CmisLmt0056RespDtoResultDto.setData(CmisLmt0056RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0056.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0056.value, JSON.toJSONString(CmisLmt0056RespDtoResultDto));
        return CmisLmt0056RespDtoResultDto;
    }
}