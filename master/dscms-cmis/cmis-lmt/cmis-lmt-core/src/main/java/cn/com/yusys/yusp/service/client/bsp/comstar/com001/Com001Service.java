package cn.com.yusys.yusp.service.client.bsp.comstar.com001;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.com001.req.Com001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.com001.resp.Com001RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2ComstarClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
@Transactional
public class Com001Service {
    private static final Logger logger = LoggerFactory.getLogger(Com001Service.class);

    // 1）注入：
    @Autowired
    private Dscms2ComstarClientService dscms2ComstarClientService;

    /**
     * 业务逻辑处理方法：对私客户创建及维护接口
     *
     * @param com001ReqDto
     * @return
     */
    @Transactional
    public Com001RespDto Com001(Com001ReqDto com001ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_COM001.key, EsbEnum.TRADE_CODE_COM001.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_COM001.key, EsbEnum.TRADE_CODE_COM001.value, JSON.toJSONString(com001ReqDto));
        //TODO 先不发comstar,统一返回成功
        ResultDto<Com001RespDto> com001ResultDto = dscms2ComstarClientService.com001(com001ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_COM001.key, EsbEnum.TRADE_CODE_COM001.value, JSON.toJSONString(com001ResultDto));

        String com001Code = Optional.ofNullable(com001ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String com001Meesage = Optional.ofNullable(com001ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Com001RespDto com001RespDto = new Com001RespDto() ;

        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, com001ResultDto.getCode())) {
            //  获取相关的值并解析
            com001RespDto = com001ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(com001Code, com001Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_COM001.key, EsbEnum.TRADE_CODE_COM001.value);
        com001RespDto.setErrorCode(SuccessEnum.SUCCESS.key);
        com001RespDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        return com001RespDto;
    }
}
