/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtStrMtableConf;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.service.LmtStrMtableConfService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtStrMtableConfResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 86188
 * @创建时间: 2021-05-11 10:36:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtstrmtableconf")
public class LmtStrMtableConfResource {
    @Autowired
    private LmtStrMtableConfService lmtStrMtableConfService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtStrMtableConf>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtStrMtableConf> list = lmtStrMtableConfService.selectAll(queryModel);
        return new ResultDto<List<LmtStrMtableConf>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtStrMtableConf>> index(QueryModel queryModel) {
        List<LmtStrMtableConf> list = lmtStrMtableConfService.selectByModel(queryModel);
        return new ResultDto<List<LmtStrMtableConf>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtStrMtableConf>> selectByModel(@RequestBody QueryModel queryModel) {
        List<LmtStrMtableConf> list = lmtStrMtableConfService.selectByModel(queryModel);
        return new ResultDto<List<LmtStrMtableConf>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{lmtCfgSerno}")
    protected ResultDto<LmtStrMtableConf> show(@PathVariable("lmtCfgSerno") String lmtCfgSerno) {
        LmtStrMtableConf lmtStrMtableConf = lmtStrMtableConfService.selectByPrimaryKey(lmtCfgSerno);
        return new ResultDto<LmtStrMtableConf>(lmtStrMtableConf);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtStrMtableConf> create(@RequestBody LmtStrMtableConf lmtStrMtableConf) {
        lmtStrMtableConfService.insert(lmtStrMtableConf);
        return new ResultDto<LmtStrMtableConf>(lmtStrMtableConf);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtStrMtableConf lmtStrMtableConf) {
        int result = lmtStrMtableConfService.update(lmtStrMtableConf);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{lmtCfgSerno}")
    protected ResultDto<Integer> delete(@PathVariable("lmtCfgSerno") String lmtCfgSerno) {
        int result = lmtStrMtableConfService.deleteByPrimaryKey(lmtCfgSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtStrMtableConfService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:logicalDelete
     * @函数描述:产品删除时，数据逻辑删除(既将操作类型更新为“02-删除”)，不进行物理删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/logicalDelete/{lmtCfgSerno}")
    protected ResultDto<Integer> logicalDelete(@PathVariable("lmtCfgSerno") String lmtCfgSerno ) {
        int result = lmtStrMtableConfService.logicDelete(lmtCfgSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:addLmtStrMtableConf
     * @函数描述:新增额度结构
     * @参数与返回说明:
     * @算法描述:
     * add by zhangjw 2021-05-07
     */
    @PostMapping("/addLmtStrMtableConf")
    protected ResultDto<LmtStrMtableConf> addLmtStrMtableConf(@RequestBody LmtStrMtableConf lmtStrMtableConf) {
        ResultDto dto = new ResultDto();
        Map<String, String> map = new HashMap<String, String>();
        String limitStrNo = lmtStrMtableConf.getLimitStrNo();
        if (StringUtils.isNotBlank(limitStrNo)) {
            map = lmtStrMtableConfService.valiLimitStrNoInWay(limitStrNo);
        }
        if (EclEnum.ECL070092.key.equals(map.get("rtnCode"))) {
            dto.setData(map.get("rtnMsg"));
            return dto;
        }
        lmtStrMtableConfService.addLmtStrMtableConf(lmtStrMtableConf);
        return new ResultDto<LmtStrMtableConf>(lmtStrMtableConf);
    }

    /**
     * @函数名称:deleteConfByLimitStrNo
     * @函数描述:根据额度结构编号删除额度结构（包含额度结构子节点）
     * @参数与返回说明:
     * @算法描述:
     * add by zhangjw 2021-05-07
     */
    @PostMapping("/deleteConfByLimitStrNo")
    protected ResultDto<Integer> deleteConfByLimitStrNo (@RequestBody String  limitStrNo) {
        int result = lmtStrMtableConfService.deleteConfByLimitStrNo(limitStrNo);
        return new ResultDto<Integer>(result);
    }

}
