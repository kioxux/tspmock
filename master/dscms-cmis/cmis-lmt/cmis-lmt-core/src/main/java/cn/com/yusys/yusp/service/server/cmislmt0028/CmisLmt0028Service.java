package cn.com.yusys.yusp.service.server.cmislmt0028;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.domain.ApprStrMtableInfo;
import cn.com.yusys.yusp.dto.server.cmisLmt0028.req.CmisLmt0028ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0028.resp.CmisLmt0028RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.ApprStrMtableInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0028Service
 * @类描述: #对内服务类
 * @功能描述: 删除额度分项数据
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0028Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0028Service.class);

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService;

    @Autowired
    private ApprStrMtableInfoService apprStrMtableInfoService;

    /**
     * 删除额度分项数据
     * add by dumd 20210615
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0028RespDto execute(CmisLmt0028ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0028.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0028.value);
        CmisLmt0028RespDto respDto = new CmisLmt0028RespDto();
        //分项编号
        String apprSubSerno = reqDto.getApprSubSerno();

        try {
            ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(apprSubSerno);
            if (apprLmtSubBasicInfo!=null){
                //将该笔记录逻辑删除
                apprLmtSubBasicInfo.setOprType(CmisLmtConstants.OPR_TYPE_DELETE);
                apprLmtSubBasicInfoService.update(apprLmtSubBasicInfo);

                //批复台账编号
                String fkPkid = apprLmtSubBasicInfo.getFkPkid();
                //查询批复台账编号对应的分项记录数
                int count = apprLmtSubBasicInfoService.countRecordByFkPkid(fkPkid);

                if (count==0){
                    //分项记录数为空，则删除该笔分项对应的批复主信息
                    ApprStrMtableInfo apprStrMtableInfo = apprStrMtableInfoService.selectByAppSerno(fkPkid);
                    apprStrMtableInfo.setOprType(CmisLmtConstants.OPR_TYPE_DELETE);
                    apprStrMtableInfoService.update(apprStrMtableInfo);
                }
            }
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("删除额度分项数据" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0028.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0028.value);
        return respDto;
    }
}