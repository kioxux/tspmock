package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

@ExcelCsv(namePrefix = "分支机构额度管控列表", fileType = ExcelCsv.ExportFileType.XLS)
public class ManaOrgLmtVo {

    /*
   机构号
    */
    @ExcelField(title = "机构号", viewLength = 20)
    private String orgId;

    /*
   分支机构
    */
    @ExcelField(title = "分支机构", viewLength = 20)
    private String orgName;

    /*
    上月末对公贷款余额
     */
    @ExcelField(title = "上月末对公贷款余额（万元）", viewLength = 20)
    private java.math.BigDecimal lastMonthComLoanBalance;

    /*
    当月可净新增对公贷款投放金额
     */
    @ExcelField(title = "当月可净新增对公贷款投放金额（万元）", viewLength = 20)
    private java.math.BigDecimal currMonthAllowComAddAmt;

//    /*
//    当月剩余可投放对公贷款余额
//     */
//    @ExcelField(title = "当月剩余可投放对公贷款余额", viewLength = 20 ,format = "#0.00")
//    private java.math.BigDecimal currMonthAllowLoanBalance;

    /*
    月末对公贷款余额测算
     */
    @ExcelField(title = "月末对公贷款余额测算（万元）", viewLength = 20 ,format = "#0.00")
    private java.math.BigDecimal curMonthComLoanBalancePlan;

    /*
    上一日对公贷款余额
     */
    @ExcelField(title = "上一日对公贷款余额（万元）", viewLength = 20)
    private java.math.BigDecimal lastDayComLoanBalance;


    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public BigDecimal getLastMonthComLoanBalance() {
        return lastMonthComLoanBalance;
    }

    public void setLastMonthComLoanBalance(BigDecimal lastMonthComLoanBalance) {
        this.lastMonthComLoanBalance = lastMonthComLoanBalance;
    }

    public BigDecimal getCurrMonthAllowComAddAmt() {
        return currMonthAllowComAddAmt;
    }

    public void setCurrMonthAllowComAddAmt(BigDecimal currMonthAllowComAddAmt) {
        this.currMonthAllowComAddAmt = currMonthAllowComAddAmt;
    }

//    public BigDecimal getCurrMonthAllowLoanBalance() {
//        return currMonthAllowLoanBalance;
//    }
//
//    public void setCurrMonthAllowLoanBalance(BigDecimal currMonthAllowLoanBalance) {
//        this.currMonthAllowLoanBalance = currMonthAllowLoanBalance;
//    }

    public BigDecimal getCurMonthComLoanBalancePlan() {
        return curMonthComLoanBalancePlan;
    }

    public void setCurMonthComLoanBalancePlan(BigDecimal curMonthComLoanBalancePlan) {
        this.curMonthComLoanBalancePlan = curMonthComLoanBalancePlan;
    }

    public BigDecimal getLastDayComLoanBalance() {
        return lastDayComLoanBalance;
    }

    public void setLastDayComLoanBalance(BigDecimal lastDayComLoanBalance) {
        this.lastDayComLoanBalance = lastDayComLoanBalance;
    }
}
