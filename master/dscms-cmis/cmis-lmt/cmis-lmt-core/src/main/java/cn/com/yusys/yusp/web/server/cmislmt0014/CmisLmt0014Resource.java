package cn.com.yusys.yusp.web.server.cmislmt0014;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0014.CmisLmt0014Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:单一客户额度同步
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "cmislmt0014:台账恢复")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0014Resource.class);

    @Autowired
    private CmisLmt0014Service cmisLmt0014Service;
    /**
     * 交易码：cmislmt0014
     * 交易描述：台账恢复
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("台账恢复")
    @PostMapping("/cmislmt0014")
    protected @ResponseBody
    ResultDto<CmisLmt0014RespDto> cmisLmt0014(@Validated @RequestBody CmisLmt0014ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = new ResultDto<>();
        CmisLmt0014RespDto cmisLmt0014RespDto = new CmisLmt0014RespDto();
        // 调用对应的service层
        try {
            cmisLmt0014RespDto = cmisLmt0014Service.execute(reqDto);
            cmisLmt0014RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0014RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0014.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.value, e.getMessage());
            // 封装xddb0014DataResultDto中异常返回码和返回信息
            cmisLmt0014RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0014RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0014RespDto到cmisLmt0014RespDtoResultDto中
        cmisLmt0014RespDtoResultDto.setData(cmisLmt0014RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.value, JSON.toJSONString(cmisLmt0014RespDtoResultDto));
        return cmisLmt0014RespDtoResultDto;
    }
}
