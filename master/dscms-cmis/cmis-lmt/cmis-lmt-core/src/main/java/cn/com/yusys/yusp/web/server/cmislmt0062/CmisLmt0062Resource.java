package cn.com.yusys.yusp.web.server.cmislmt0062;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0062.req.CmisLmt0062ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0062.resp.CmisLmt0062RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0062.CmisLmt0062Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Api(tags = "cmislmt0062:大额风险暴露风险拦截")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0062Resource {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0062Resource.class);

    @Autowired
    private CmisLmt0062Service cmisLmt0062Service;

    @ApiOperation("大额风险暴露风险拦截")
    @PostMapping("/cmislmt0062")
    protected @ResponseBody
    ResultDto<CmisLmt0062RespDto> CmisLmt0062(@Validated @RequestBody CmisLmt0062ReqDto reqDto) {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0062.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0062.value, JSON.toJSONString(reqDto));

        ResultDto<CmisLmt0062RespDto> CmisLmt0062RespDtoResultDto = new ResultDto<>();
        CmisLmt0062RespDto CmisLmt0062RespDto = new CmisLmt0062RespDto();

        // 调用对应的service层
        try {
            CmisLmt0062RespDto = cmisLmt0062Service.execute(reqDto);
            CmisLmt0062RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0062RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0062.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0062.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0062.value, e.getMessage());
            // 封装CmisLmt0061RespDtoResultDto中异常返回码和返回信息
            CmisLmt0062RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0062RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0061RespDto到CmisLmt0061RespDtoResultDto中
        CmisLmt0062RespDtoResultDto.setData(CmisLmt0062RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0062.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0062.value, JSON.toJSONString(CmisLmt0062RespDtoResultDto));
        return CmisLmt0062RespDtoResultDto;
    }

}
