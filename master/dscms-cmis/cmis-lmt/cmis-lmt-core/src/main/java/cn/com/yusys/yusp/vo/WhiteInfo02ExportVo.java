package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;
import java.util.Date;

@ExcelCsv(namePrefix = "产品户买入返售白名单导出模板", fileType = ExcelCsv.ExportFileType.XLS)
public class WhiteInfo02ExportVo {

    /*
    额度分项编号
     */
    @ExcelField(title = "额度分项编号", viewLength = 20)
    private String subAccNo;

    /*
    客户号
     */
    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /*
    总行行号
     */
    @ExcelField(title = "总行行号", viewLength = 20)
    private String aorgNo;

    /*
    客户名称
     */
    @ExcelField(title = "客户名称", viewLength = 20)
    private String cusName;

    /*
    限额（万元）
     */
    @ExcelField(title = "限额（万元）", viewLength = 20)
    private java.math.BigDecimal sigAmt;

    /*
    登记人
     */
    @ExcelField(title = "登记人", viewLength = 20)
    private String inputIdName;

    /*
    登记机构
     */
    @ExcelField(title = "登记机构", viewLength = 20)
    private String inputBrIdName;

    /*
    登记日期
     */
    @ExcelField(title = "登记日期", viewLength = 20)
    private String inputDate;

    /*
    创建时间
     */
    @ExcelField(title = "创建时间", viewLength = 20)
    private Date createTime;

    public String getSubAccNo() {
        return subAccNo;
    }

    public void setSubAccNo(String subAccNo) {
        this.subAccNo = subAccNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getAorgNo() {
        return aorgNo;
    }

    public void setAorgNo(String aorgNo) {
        this.aorgNo = aorgNo;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public BigDecimal getSigAmt() {
        return sigAmt;
    }

    public void setSigAmt(BigDecimal sigAmt) {
        this.sigAmt = sigAmt;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getInputIdName() {
        return inputIdName;
    }

    public void setInputIdName(String inputIdName) {
        this.inputIdName = inputIdName;
    }

    public String getInputBrIdName() {
        return inputBrIdName;
    }

    public void setInputBrIdName(String inputBrIdName) {
        this.inputBrIdName = inputBrIdName;
    }
}
