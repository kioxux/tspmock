package cn.com.yusys.yusp.dto;

public class CoopProFrozeUnfrozeSubDto {
    /**
     * 主键
     **/
    private String pkId;

    /**
     * 批复台账流水号
     **/
    private String serno;

    /**
     * 批复台账流水号
     **/
    private String apprSerno;

    /**
     * 批复分项流水号
     **/
    private String apprSubSerno;

    /**
     * 品种编号/项目编号
     **/
    private String limitSubNo;

    /**
     * 分项名称/项目名称
     **/
    private String limitSubName;

    /**
     * 授信总额
     **/
    private java.math.BigDecimal avlAmt;

    /**
     * 已用总额
     **/
    private java.math.BigDecimal outstndAmt;

    /**
     * 限额
     **/
    private java.math.BigDecimal sigAmt;

    /**
     * 单笔业务限额
     **/
    private java.math.BigDecimal sigBussAmt;

    /**
     * 贷款余额
     **/
    private java.math.BigDecimal loanBalance;

    /**
     * 是否循环
     **/
    private String isRevolv;

    /**
     * 币种
     **/
    private String curType;

    /**
     * 期限
     **/
    private Integer term;

    /**
     * 起始日期
     **/
    private String startDate;

    /**
     * 到期日期
     **/
    private String endDate;

    /**
     * 状态
     **/
    private String status;

    /**
     * 修改后额度状态
     **/
    private String afterLmtStatus;

    /**
     * 操作类型
     **/
    private String oprType;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 登记时间
     **/
    private String inputDate;

    /**
     * 最新更新人
     **/
    private String updId;

    /**
     * 最新更新机构
     **/
    private String updBrId;

    /**
     * 最新更新日期
     **/
    private String updDate;

    /**
     * 创建时间
     **/
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    private java.util.Date updateTime;

    private String instuCde;

    private String cusId;

    private String cusName;

    private String copType;

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param apprSerno
     */
    public void setApprSerno(String apprSerno) {
        this.apprSerno = apprSerno;
    }

    /**
     * @return apprSerno
     */
    public String getApprSerno() {
        return this.apprSerno;
    }

    /**
     * @param apprSubSerno
     */
    public void setApprSubSerno(String apprSubSerno) {
        this.apprSubSerno = apprSubSerno;
    }

    /**
     * @return apprSubSerno
     */
    public String getApprSubSerno() {
        return this.apprSubSerno;
    }

    /**
     * @param limitSubNo
     */
    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    /**
     * @return limitSubNo
     */
    public String getLimitSubNo() {
        return this.limitSubNo;
    }

    /**
     * @param limitSubName
     */
    public void setLimitSubName(String limitSubName) {
        this.limitSubName = limitSubName;
    }

    /**
     * @return limitSubName
     */
    public String getLimitSubName() {
        return this.limitSubName;
    }

    /**
     * @param avlAmt
     */
    public void setAvlAmt(java.math.BigDecimal avlAmt) {
        this.avlAmt = avlAmt;
    }

    /**
     * @return avlAmt
     */
    public java.math.BigDecimal getAvlAmt() {
        return this.avlAmt;
    }

    /**
     * @param outstndAmt
     */
    public void setOutstndAmt(java.math.BigDecimal outstndAmt) {
        this.outstndAmt = outstndAmt;
    }

    /**
     * @return outstndAmt
     */
    public java.math.BigDecimal getOutstndAmt() {
        return this.outstndAmt;
    }

    /**
     * @param sigAmt
     */
    public void setSigAmt(java.math.BigDecimal sigAmt) {
        this.sigAmt = sigAmt;
    }

    /**
     * @return sigAmt
     */
    public java.math.BigDecimal getSigAmt() {
        return this.sigAmt;
    }

    /**
     * @param sigBussAmt
     */
    public void setSigBussAmt(java.math.BigDecimal sigBussAmt) {
        this.sigBussAmt = sigBussAmt;
    }

    /**
     * @return sigBussAmt
     */
    public java.math.BigDecimal getSigBussAmt() {
        return this.sigBussAmt;
    }

    /**
     * @param loanBalance
     */
    public void setLoanBalance(java.math.BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    /**
     * @return loanBalance
     */
    public java.math.BigDecimal getLoanBalance() {
        return this.loanBalance;
    }

    /**
     * @param isRevolv
     */
    public void setIsRevolv(String isRevolv) {
        this.isRevolv = isRevolv;
    }

    /**
     * @return isRevolv
     */
    public String getIsRevolv() {
        return this.isRevolv;
    }

    /**
     * @param curType
     */
    public void setCurType(String curType) {
        this.curType = curType;
    }

    /**
     * @return curType
     */
    public String getCurType() {
        return this.curType;
    }

    /**
     * @param term
     */
    public void setTerm(Integer term) {
        this.term = term;
    }

    /**
     * @return term
     */
    public Integer getTerm() {
        return this.term;
    }

    /**
     * @param startDate
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return startDate
     */
    public String getStartDate() {
        return this.startDate;
    }

    /**
     * @param endDate
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * @return endDate
     */
    public String getEndDate() {
        return this.endDate;
    }

    /**
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return status
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * @param afterLmtStatus
     */
    public void setAfterLmtStatus(String afterLmtStatus) {
        this.afterLmtStatus = afterLmtStatus;
    }

    /**
     * @return afterLmtStatus
     */
    public String getAfterLmtStatus() {
        return this.afterLmtStatus;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCopType() {
        return copType;
    }

    public void setCopType(String copType) {
        this.copType = copType;
    }
}
