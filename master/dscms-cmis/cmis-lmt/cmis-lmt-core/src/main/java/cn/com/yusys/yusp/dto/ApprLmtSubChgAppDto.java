package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprLmtSubChgApp
 * @类描述: appr_lmt_sub_chg_app数据实体类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-05-03 10:32:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class ApprLmtSubChgAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** FK_PKID **/
	private String fkPkid;
	
	/** 主申请流水号 **/
	private String subAppSerno;

	/** 批复分项流水号 **/
	private String apprSubSerno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 集团编号 **/
	private String grpNo;
	
	/** 客户主体类型 **/
	private String cusType;
	
	/** 额度分项编号 **/
	private String limitSubNo;
	
	/** 额度分项名称 **/
	private String limitSubName;
	
	/** 授信品种类型属性 **/
	private String lmtBizTypeProp;
	
	/** 父节点 **/
	private String parentId;
	
	/** 是否低风险授信 **/
	private String isLriskLmt;
	
	/** 授信分项类型 **/
	private String lmtSubType;
	
	/** 是否循环 **/
	private String isRevolv;
	
	/** 额度类型 **/
	private String limitType;
	
	/** 适用担保方式 **/
	private String suitGuarWay;
	
	/** 额度有效期 **/
	private String lmtDate;
	
	/** 额度宽限期(月） **/
	private Integer lmtGraper;
	
	/** 授信总额 **/
	private BigDecimal avlAmt;
	
	/** 已用额度 **/
	private BigDecimal outstndAmt;
	
	/** 敞口金额 **/
	private BigDecimal spacAmt;
	
	/** 敞口已用额度 **/
	private BigDecimal spacOutstndAmt;
	
	/** 贷款余额 **/
	private BigDecimal loanBalance;
	
	/** 状态 **/
	private String status;
	
	/** 项目编号 **/
	private String proNo;
	
	/** 项目名称 **/
	private String proName;
	
	/** 资产编号 **/
	private String assetNo;
	
	/** 币种 **/
	private String curType;
	
	/** 保证金预留比例 **/
	private BigDecimal bailPreRate;
	
	/** 年利率 **/
	private BigDecimal rateYear;
	
	/** 授信总额累加 **/
	private BigDecimal lmtAmtAdd;
	
	/** 已出帐金额 **/
	private BigDecimal pvpOutstndAmt;
	
	/** 可出账金额 **/
	private BigDecimal avlOutstndAmt;
	
	/** 是否预授信 **/
	private String isPreCrd;
	
	/** 是否涉及货币基金 **/
	private String isIvlMf;
	
	/** 单只货币基金授信额度 **/
	private BigDecimal lmtSingleMfAmt;
	
	/** 起始日期 **/
	private String startDate;
	
	/** 期限 **/
	private Integer term;
	
	/** 用信敞口余额 **/
	private BigDecimal loanSpacBalance;
	
	/** 操作类型 **/
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param fkPkid
	 */
	public void setFkPkid(String fkPkid) {
		this.fkPkid = fkPkid == null ? null : fkPkid.trim();
	}
	
    /**
     * @return FkPkid
     */	
	public String getFkPkid() {
		return this.fkPkid;
	}
	
	/**
	 * @param subAppSerno
	 */
	public void setSubAppSerno(String subAppSerno) {
		this.subAppSerno = subAppSerno == null ? null : subAppSerno.trim();
	}
	
    /**
     * @return SubAppSerno
     */	
	public String getSubAppSerno() {
		return this.subAppSerno;
	}
	
	/**
	 * @param apprSubSerno
	 */
	public void setApprSubSerno(String apprSubSerno) {
		this.apprSubSerno = apprSubSerno == null ? null : apprSubSerno.trim();
	}
	
    /**
     * @return ApprSubSerno
     */	
	public String getApprSubSerno() {
		return this.apprSubSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param grpNo
	 */
	public void setGrpNo(String grpNo) {
		this.grpNo = grpNo == null ? null : grpNo.trim();
	}
	
    /**
     * @return GrpNo
     */	
	public String getGrpNo() {
		return this.grpNo;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType == null ? null : cusType.trim();
	}
	
    /**
     * @return CusType
     */	
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param limitSubNo
	 */
	public void setLimitSubNo(String limitSubNo) {
		this.limitSubNo = limitSubNo == null ? null : limitSubNo.trim();
	}
	
    /**
     * @return LimitSubNo
     */	
	public String getLimitSubNo() {
		return this.limitSubNo;
	}
	
	/**
	 * @param limitSubName
	 */
	public void setLimitSubName(String limitSubName) {
		this.limitSubName = limitSubName == null ? null : limitSubName.trim();
	}
	
    /**
     * @return LimitSubName
     */	
	public String getLimitSubName() {
		return this.limitSubName;
	}
	
	/**
	 * @param lmtBizTypeProp
	 */
	public void setLmtBizTypeProp(String lmtBizTypeProp) {
		this.lmtBizTypeProp = lmtBizTypeProp == null ? null : lmtBizTypeProp.trim();
	}
	
    /**
     * @return LmtBizTypeProp
     */	
	public String getLmtBizTypeProp() {
		return this.lmtBizTypeProp;
	}
	
	/**
	 * @param parentId
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId == null ? null : parentId.trim();
	}
	
    /**
     * @return ParentId
     */	
	public String getParentId() {
		return this.parentId;
	}
	
	/**
	 * @param isLriskLmt
	 */
	public void setIsLriskLmt(String isLriskLmt) {
		this.isLriskLmt = isLriskLmt == null ? null : isLriskLmt.trim();
	}
	
    /**
     * @return IsLriskLmt
     */	
	public String getIsLriskLmt() {
		return this.isLriskLmt;
	}
	
	/**
	 * @param lmtSubType
	 */
	public void setLmtSubType(String lmtSubType) {
		this.lmtSubType = lmtSubType == null ? null : lmtSubType.trim();
	}
	
    /**
     * @return LmtSubType
     */	
	public String getLmtSubType() {
		return this.lmtSubType;
	}
	
	/**
	 * @param isRevolv
	 */
	public void setIsRevolv(String isRevolv) {
		this.isRevolv = isRevolv == null ? null : isRevolv.trim();
	}
	
    /**
     * @return IsRevolv
     */	
	public String getIsRevolv() {
		return this.isRevolv;
	}
	
	/**
	 * @param limitType
	 */
	public void setLimitType(String limitType) {
		this.limitType = limitType == null ? null : limitType.trim();
	}
	
    /**
     * @return LimitType
     */	
	public String getLimitType() {
		return this.limitType;
	}
	
	/**
	 * @param suitGuarWay
	 */
	public void setSuitGuarWay(String suitGuarWay) {
		this.suitGuarWay = suitGuarWay == null ? null : suitGuarWay.trim();
	}
	
    /**
     * @return SuitGuarWay
     */	
	public String getSuitGuarWay() {
		return this.suitGuarWay;
	}
	
	/**
	 * @param lmtDate
	 */
	public void setLmtDate(String lmtDate) {
		this.lmtDate = lmtDate == null ? null : lmtDate.trim();
	}
	
    /**
     * @return LmtDate
     */	
	public String getLmtDate() {
		return this.lmtDate;
	}
	
	/**
	 * @param lmtGraper
	 */
	public void setLmtGraper(Integer lmtGraper) {
		this.lmtGraper = lmtGraper;
	}
	
    /**
     * @return LmtGraper
     */	
	public Integer getLmtGraper() {
		return this.lmtGraper;
	}
	
	/**
	 * @param avlAmt
	 */
	public void setAvlAmt(BigDecimal avlAmt) {
		this.avlAmt = avlAmt;
	}
	
    /**
     * @return AvlAmt
     */	
	public BigDecimal getAvlAmt() {
		return this.avlAmt;
	}
	
	/**
	 * @param outstndAmt
	 */
	public void setOutstndAmt(BigDecimal outstndAmt) {
		this.outstndAmt = outstndAmt;
	}
	
    /**
     * @return OutstndAmt
     */	
	public BigDecimal getOutstndAmt() {
		return this.outstndAmt;
	}
	
	/**
	 * @param spacAmt
	 */
	public void setSpacAmt(BigDecimal spacAmt) {
		this.spacAmt = spacAmt;
	}
	
    /**
     * @return SpacAmt
     */	
	public BigDecimal getSpacAmt() {
		return this.spacAmt;
	}
	
	/**
	 * @param spacOutstndAmt
	 */
	public void setSpacOutstndAmt(BigDecimal spacOutstndAmt) {
		this.spacOutstndAmt = spacOutstndAmt;
	}
	
    /**
     * @return SpacOutstndAmt
     */	
	public BigDecimal getSpacOutstndAmt() {
		return this.spacOutstndAmt;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return LoanBalance
     */	
	public BigDecimal getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}
	
    /**
     * @return Status
     */	
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo == null ? null : proNo.trim();
	}
	
    /**
     * @return ProNo
     */	
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName == null ? null : proName.trim();
	}
	
    /**
     * @return ProName
     */	
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param assetNo
	 */
	public void setAssetNo(String assetNo) {
		this.assetNo = assetNo == null ? null : assetNo.trim();
	}
	
    /**
     * @return AssetNo
     */	
	public String getAssetNo() {
		return this.assetNo;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param bailPreRate
	 */
	public void setBailPreRate(BigDecimal bailPreRate) {
		this.bailPreRate = bailPreRate;
	}
	
    /**
     * @return BailPreRate
     */	
	public BigDecimal getBailPreRate() {
		return this.bailPreRate;
	}
	
	/**
	 * @param rateYear
	 */
	public void setRateYear(BigDecimal rateYear) {
		this.rateYear = rateYear;
	}
	
    /**
     * @return RateYear
     */	
	public BigDecimal getRateYear() {
		return this.rateYear;
	}
	
	/**
	 * @param lmtAmtAdd
	 */
	public void setLmtAmtAdd(BigDecimal lmtAmtAdd) {
		this.lmtAmtAdd = lmtAmtAdd;
	}
	
    /**
     * @return LmtAmtAdd
     */	
	public BigDecimal getLmtAmtAdd() {
		return this.lmtAmtAdd;
	}
	
	/**
	 * @param pvpOutstndAmt
	 */
	public void setPvpOutstndAmt(BigDecimal pvpOutstndAmt) {
		this.pvpOutstndAmt = pvpOutstndAmt;
	}
	
    /**
     * @return PvpOutstndAmt
     */	
	public BigDecimal getPvpOutstndAmt() {
		return this.pvpOutstndAmt;
	}
	
	/**
	 * @param avlOutstndAmt
	 */
	public void setAvlOutstndAmt(BigDecimal avlOutstndAmt) {
		this.avlOutstndAmt = avlOutstndAmt;
	}
	
    /**
     * @return AvlOutstndAmt
     */	
	public BigDecimal getAvlOutstndAmt() {
		return this.avlOutstndAmt;
	}
	
	/**
	 * @param isPreCrd
	 */
	public void setIsPreCrd(String isPreCrd) {
		this.isPreCrd = isPreCrd == null ? null : isPreCrd.trim();
	}
	
    /**
     * @return IsPreCrd
     */	
	public String getIsPreCrd() {
		return this.isPreCrd;
	}
	
	/**
	 * @param isIvlMf
	 */
	public void setIsIvlMf(String isIvlMf) {
		this.isIvlMf = isIvlMf == null ? null : isIvlMf.trim();
	}
	
    /**
     * @return IsIvlMf
     */	
	public String getIsIvlMf() {
		return this.isIvlMf;
	}
	
	/**
	 * @param lmtSingleMfAmt
	 */
	public void setLmtSingleMfAmt(BigDecimal lmtSingleMfAmt) {
		this.lmtSingleMfAmt = lmtSingleMfAmt;
	}
	
    /**
     * @return LmtSingleMfAmt
     */	
	public BigDecimal getLmtSingleMfAmt() {
		return this.lmtSingleMfAmt;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}
	
    /**
     * @return StartDate
     */	
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param term
	 */
	public void setTerm(Integer term) {
		this.term = term;
	}
	
    /**
     * @return Term
     */	
	public Integer getTerm() {
		return this.term;
	}
	
	/**
	 * @param loanSpacBalance
	 */
	public void setLoanSpacBalance(BigDecimal loanSpacBalance) {
		this.loanSpacBalance = loanSpacBalance;
	}
	
    /**
     * @return LoanSpacBalance
     */	
	public BigDecimal getLoanSpacBalance() {
		return this.loanSpacBalance;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}