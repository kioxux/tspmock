package cn.com.yusys.yusp.service.server.cmislmt0065;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0065.req.CmisLmt0065ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0065.resp.CmisLmt0065RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0065Service
 * @类描述: #对内服务类
 * @功能描述: 获取客户指定品种或指定项目的授信余额
 * @创建时间: 2021-09-30
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0065Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0065Service.class);

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;


    @Transactional
    public CmisLmt0065RespDto execute(CmisLmt0065ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0065.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0065.value);
        CmisLmt0065RespDto respDto = new CmisLmt0065RespDto();
        try {
            //客户号
            String cusId = reqDto.getCusId();
            String limitSubNo = reqDto.getLimitSubNo();
            String proNo = reqDto.getProNo();

            if(StringUtils.isBlank(cusId)){
                throw new YuspException("90011","客户编号不允许为空");
            }
            if(StringUtils.isBlank(limitSubNo) && StringUtils.isBlank(proNo)){
                throw new YuspException("90011","分项品种编号和项目编号不允许都为空");
            }

            BigDecimal lmtAmt =  apprLmtSubBasicInfoService.queryLmtAmtBycusIdAndLimitSubNo(cusId,limitSubNo,proNo);
            String status = apprLmtSubBasicInfoService.queryLmtStatusByProNoOrZqc(cusId,limitSubNo,proNo);

            respDto.setLmtAmt(lmtAmt);
            respDto.setStatus(status);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
            logger.error("【cmislmt0065】查询结果：" , respDto.toString());
        } catch (YuspException e) {
            logger.error("【cmislmt0065】获取客户指定品种或指定项目的授信余额：" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0065.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0065.value);
        return respDto;
    }
}