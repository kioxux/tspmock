package cn.com.yusys.yusp.constant;

/**
 * 额度协议异常枚举类
 */
public enum LmtCtrExceptionDefEnums {
    //定义默认的成功以及异常返回信息
    LMT_CTR_DEF_SUCCESS("000000","成功"),
    LMT_CTR_DEF_EXCEPTION("999999","异常"),


    LMT_UPDATE_EXCEPTION("UPDATE000001","更新异常"),

    // 异常定义
    E_GETLMTNOS_PARAMS_EXCEPTION("GPE000001","未获取到授信协议编号！"),
    E_LMT_CTR_CANCEL_EXCEPTION1("CANCEL000001","个人授信协议注销操作异常！"),
    E_LMT_CTR_CANCEL_EXCEPTION2("CANCEL000002","该授信协议项下存在在途的业务申请！"),
    E_LMT_CTR_CANCEL_EXCEPTION3("CANCEL000003","该授信协议项下未找到额度分项信息！"),
    E_LMT_CTR_CANCEL_EXCEPTION4("CANCEL000004","该个人授信协议项下关联有未结清的贷款合同不能注销！"),
    E_LMT_CTR_CANCEL_EXCEPTION5("CANCEL000004","获取当前登录用户为空！"),
    
    E_LMT_FR_UFR_APP_FROZENAMT_EXCEPTION("FROZE000001","未获取到本次冻结解冻金额！"),
    E_LMT_FR_UFR_APP_LMTFROZENAMT_EXCEPTION("FROZE000002","授信冻结金额的计算异常！"),
    
    //获取额度协议数据异常定义
    LMTCTRLIST_CLIENT_PARAMS_EXCEPTION("LCLC000001","获取额度协议接口参数异常！"),
    LMTCTR_CLIENT_PARAMS_EXCEPTION("LCLC000002","该额度台账数据不存在"),
    LMTCTR_LMTBEGINAMT_EXCEPTION("FROZE000002","获取该额度台账数据异常！"),
    
    //保存额度协议和额度台账数据异常定义
    LMTCTR_HANDLE_PARAMS_EXCEPTION("LCH000001","保存额度协议和额度台账数据失败！未获取到保存的参数信息！"),
    LMTCTR_HANDLE_LMTCTRTRANS_EXCEPTION("LCH000002","转化额度协议数据异常！"),
    LMTCTR_HANDLE_LMTCTRSAVE_EXCEPTION("LCH000003","保存额度协议失败！"),
    LMTCTR_HANDLE_LMTACCTRANS_EXCEPTION("LCH000004","转化额度台账数据异常！"),
    LMTCTR_HANDLE_LMTACCTRANSNULL_EXCEPTION("LCH000005","转化额度台账数据信息为空！"),
    LMTCTR_HANDLE_LMTACCSAVE_EXCEPTION("LCH000006","保存额度台账数据失败！"),
    LMTCTR_HANDLE_LMTCTRUPDATE_EXCEPTION("LCH000007","更新额度协议数据失败！"),
    LMTCTR_HANDLE_LMTACCUPDATE_EXCEPTION("LCH000008","更新额度台账数据失败！"),
    LMTCTR_HANDLE_LMTCTRMORE_EXCEPTION("LCH000009","生成额度协议以及额度台账异常！异常原因：存在多条授信协议数据"),
    ;



    /** 异常编码 **/
    private String exceptionCode;

    /** 异常描述 **/
    private String exceptionDesc;

    LmtCtrExceptionDefEnums(String exceptionCode, String exceptionDesc){
        this.exceptionCode = exceptionCode;
        this.exceptionDesc = exceptionDesc;
    }

    public String getExceptionCode() {
        return exceptionCode;
    }



    public String getExceptionDesc() {
        return exceptionDesc;
    }
}
