package cn.com.yusys.yusp.web.server.cmislmt0026;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0026.req.CmisLmt0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.resp.CmisLmt0026RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0026.CmisLmt0026Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:额度分项信息查询
 *
 * @author dumd 20210609
 * @version 1.0
 */
@Api(tags = "cmislmt0026:额度分项信息查询")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0026Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0026Resource.class);

    @Autowired
    private CmisLmt0026Service cmisLmt0026Service;

    /**
     * 交易码：cmislmt0026
     * 交易描述：额度分项信息查询
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("额度分项信息查询")
    @PostMapping("/cmislmt0026")
    protected @ResponseBody
    ResultDto<CmisLmt0026RespDto> cmisLmt0026(@Validated @RequestBody CmisLmt0026ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0026.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0026.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0026RespDto> cmisLmt0026RespDtoResultDto = new ResultDto<>();
        CmisLmt0026RespDto cmisLmt0026RespDto = new CmisLmt0026RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0026RespDto = cmisLmt0026Service.execute(reqDto);
            cmisLmt0026RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0026RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0026.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0026.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0026.value, e.getMessage());
            // 封装cmisLmt0026RespDtoResultDto中异常返回码和返回信息
            cmisLmt0026RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0026RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0026RespDto到cmisLmt0026RespDtoResultDto中
        cmisLmt0026RespDtoResultDto.setData(cmisLmt0026RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0026.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0026.value, JSON.toJSONString(cmisLmt0026RespDtoResultDto));
        return cmisLmt0026RespDtoResultDto;
    }
}