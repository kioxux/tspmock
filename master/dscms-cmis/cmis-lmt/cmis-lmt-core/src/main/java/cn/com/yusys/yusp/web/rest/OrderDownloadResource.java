/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.OrderDownloadDto;
import cn.com.yusys.yusp.service.OrderDownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * 预约下载 cmis-biz 入口
 */
@RestController
@RequestMapping("/api/orderdownlad")
public class OrderDownloadResource {
    @Autowired
    private  OrderDownloadService orderDownloadService;
 

    /**
     * 预约下载启动
     * @param cfgOrderDownload
     * @return
     */
    @PostMapping("/start")
    protected ResultDto<String> start(@RequestBody OrderDownloadDto cfgOrderDownload) {
    	String result = orderDownloadService.start(cfgOrderDownload);
        return new ResultDto<String>(result);
    }

    /**
     * 预约下载终止
     * @param cfgOrderDownload
     * @return
     */
    @PostMapping("/end")
    protected ResultDto<String> end(@RequestBody OrderDownloadDto cfgOrderDownload) {
    	String result = orderDownloadService.end(cfgOrderDownload);
        return new ResultDto<String>(result);
    }

    @GetMapping("/download")
    protected void download(@RequestParam("pkId") String filePath,HttpServletResponse response) {
    	orderDownloadService.download(filePath,response);
    }
    
}
