package cn.com.yusys.yusp.service.server.cmislmt0024;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtContRel;
import cn.com.yusys.yusp.domain.LmtWhiteInfo;
import cn.com.yusys.yusp.dto.server.cmislmt0024.req.CmisLmt0024OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0024.req.CmisLmt0024ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0024.resp.CmisLmt0024RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.LmtContRelService;
import cn.com.yusys.yusp.service.LmtWhiteInfoService;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0024Service
 * @类描述: #对内服务类
 * @功能描述: 承兑行白名单额度占用
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0024Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0024Service.class);

    @Autowired
    private LmtWhiteInfoService lmtWhiteInfoService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private LmtContRelService lmtContRelService;

    @Autowired
    private Comm4Service comm4Service;

    /**
     * 承兑行白名单额度占用
     *
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0024RespDto execute(CmisLmt0024ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0024.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0024.value);
        CmisLmt0024RespDto resqDto = new CmisLmt0024RespDto();
        List<CmisLmt0024OccRelListReqDto> occRelListReqDtos = reqDto.getOccRelList();

        Map<String, String> paramtMap = (Map<String, String>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(reqDto);
        Map<String, String> resultMap = new HashMap<>() ;
        resultMap.putAll(paramtMap);
        resultMap.put("serno", reqDto.getSysNo()) ;
        resultMap.put("serviceCode", DscmsLmtEnum.TRADE_CODE_CMISLMT0024.key) ;

        //金融机构代码
        String instuCde = reqDto.getInstuCde();
        //登记人
        String inputId = reqDto.getInputId();
        //登记机构
        String inputBrId = reqDto.getInputBrId();
        //登记日期
        String inputDate = reqDto.getInputDate();
        //是否占用
        String isOccLmt = reqDto.getIsOccLmt() ;
        if(StringUtils.isBlank(isOccLmt)) isOccLmt = CmisLmtConstants.YES_NO_Y ;

        try {
            if(occRelListReqDtos!=null && occRelListReqDtos.size()>0){
                logger.info("{}是否占用操作标识{}【0-否 不占用  1-是 占用】", DscmsLmtEnum.TRADE_CODE_CMISLMT0024.key, isOccLmt);
                //检查信息是否合法
                List<Map<String, Object>> mapList = new ArrayList<>() ;
                mapList= checkInfoIsVaild(occRelListReqDtos, instuCde);

                //是否占用为 1-是 才进行占用操作
                if(CmisLmtConstants.YES_NO_Y.equals(isOccLmt)){
                    //key:承兑行客户号 value:对应的额度分项编号
                    Map<String,Object> orgCusIdAndSubAccNoMap = mapList.get(0);
                    //key:承兑行客户号 value:对应的已用金额
                    Map<String,Object> orgCusIdAndSigUseAmtMap = mapList.get(1);

                    for (CmisLmt0024OccRelListReqDto occRelList : occRelListReqDtos) {
                        //原交易业务恢复类型
                        String origiRecoverType = occRelList.getOrigiRecoverType();

                        if (CmisLmtConstants.STD_RECOVER_TYPE_06.equals(origiRecoverType)){
                            //如果是撤销占用，则将原来的物理删除
                            //原交易业务编号
                            String origiDealBizNo = occRelList.getOrigiDealBizNo();
                            lmtContRelService.deleteByDealBizNo(origiDealBizNo);
                        }

                        LmtContRel lmtContRel = new LmtContRel();
                        Map paramMap= new HashMap<>() ;
                        String pkId = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, paramMap);
                        lmtContRel.setPkId(pkId);
                        lmtContRel.setSysId(reqDto.getSysNo());
                        lmtContRel.setBizAttr(CmisLmtConstants.STD_ZB_BIZ_ATTR_2);
                        lmtContRel.setSecurityRate(BigDecimal.ZERO);
                        lmtContRel.setSecurityAmt(BigDecimal.ZERO);
                        //客户编号
                        lmtContRel.setCusId(occRelList.getCusId());
                        //客户名称
                        lmtContRel.setCusName(occRelList.getCusName());
                        //业务金额
                        lmtContRel.setBizAmt(occRelList.getBizTotalAmt());
                        //交易业务编号
                        lmtContRel.setDealBizNo(occRelList.getDealBizNo());
                        String orgCusId = occRelList.getOrgCusId();//在前面将同业大额行号转换成客户号时，已将list里面数据替换为客户号
                        //额度分项编号
                        lmtContRel.setLimitSubNo((String) orgCusIdAndSubAccNoMap.get(orgCusId));
                        //授信类型 默认白名单额度
                        lmtContRel.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_06);
                        //业务属性 默认最高额合同
                        lmtContRel.setDealBizType(CmisLmtConstants.STD_ZB_CONT_TYPE_2);
                        //产品编号
                        lmtContRel.setPrdId(occRelList.getPrdNo());
                        //产品名称
                        lmtContRel.setPrdName(occRelList.getPrdName());
                        //占用总金额（折人民币）
                        lmtContRel.setBizTotalAmtCny(occRelList.getBizTotalAmt());
                        //占用敞口金额（折人民币）
                        lmtContRel.setBizSpacAmtCny(occRelList.getBizTotalAmt());
                        //占用总余额（折人民币）
                        lmtContRel.setBizTotalBalanceAmtCny(occRelList.getBizTotalAmt());
                        //占用敞口余额（折人民币）
                        lmtContRel.setBizSpacBalanceAmtCny(occRelList.getBizTotalAmt());
                        //起始日
                        lmtContRel.setStartDate(occRelList.getStartDate());
                        //到期日
                        lmtContRel.setEndDate(occRelList.getEndDate());
                        //交易业务状态 默认有效
                        lmtContRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_200);
                        //操作类型 默认新增
                        lmtContRel.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
                        //登记人
                        lmtContRel.setInputId(inputId);
                        //登记机构
                        lmtContRel.setInputBrId(inputBrId);
                        //登记时间
                        lmtContRel.setInputDate(inputDate);
                        //最近更新人
                        lmtContRel.setUpdId(inputId);
                        //最近更新机构
                        lmtContRel.setUpdBrId(inputBrId);
                        //最近更新日期
                        lmtContRel.setUpdDate(inputDate);
                        //创建时间
                        lmtContRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                        //修改时间
                        lmtContRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                        lmtContRelService.insert(lmtContRel);
                    }

                    //更新白名单额度信息表里的已用限额
                    for(Map.Entry<String,Object> entry : orgCusIdAndSigUseAmtMap.entrySet()){
                        // update by lizx 20210928 方便日志打印，数据留存，此处更改，先查询后更改,防止改动太大，在原基础上修改
                       /* QueryModel queryModel = new QueryModel();
                        queryModel.addCondition("cusId",entry.getKey());
                        queryModel.addCondition("sigUseAmt",entry.getValue());
                        lmtWhiteInfoService.updateSigUseAmtByCusId(queryModel);*/

                        //承兑行白名单客户号
                        String cusId = entry.getKey() ;
                        //根据承兑行白名单获取到承兑行白名单信息
                        String accSubNo = (String) orgCusIdAndSubAccNoMap.get(cusId) ;
                        //查询白名单对象数据信息
                        LmtWhiteInfo lmtWhiteInfo = lmtWhiteInfoService.selectLmtWhiteInfoBySubNo(accSubNo) ;
                        if(Objects.nonNull(lmtWhiteInfo)){
                            lmtWhiteInfo.setSigUseAmt((BigDecimal) entry.getValue());
                            lmtWhiteInfoService.updateByPKeyInApprLmtChgDetails(lmtWhiteInfo, resultMap) ;
                        }
                    }
                }
            }

            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("承兑行白名单额度占用" , e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0024.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0024.value);
        return resqDto;
    }

    /**
     * 检查客户号对应的占用金额总和是否超过可用限额,合同到期日是否超过额度分项到期日,返回承兑行客户号对应额度分项编号以及承兑行客户号对应已用金额的List
     * @param occRelListReqDtos
     * @param instuCde
     * @return orgCusIdAndSubAccNoMap
     */
    public List<Map<String,Object>> checkInfoIsVaild(List<CmisLmt0024OccRelListReqDto> occRelListReqDtos,String instuCde){
        logger.info("{}占用前校验-----------------------------start", DscmsLmtEnum.TRADE_CODE_CMISLMT0024.key);
        //key:承兑行客户号 value:对应的占用金额总和
        Map<String,BigDecimal> orgCusIdAndAmtMap = new HashMap<>();
        //key:承兑行客户号 value:对应的最大到期日
        Map<String,String> orgCusIdAndMaxEndDateMap = new HashMap<>();
        //key:承兑行客户号 value:对应的额度分项编号
        Map<String,Object> orgCusIdAndSubAccNoMap = new HashMap<>();
        //key:承兑行客户号 value:对应的已用金额
        Map<String,Object> orgCusIdAndSigUseAmtMap = new HashMap<>();
        //用于保存承兑行客户号
        List<String> orgCusIdList = new ArrayList<>();
        //返回resultList
        List<Map<String,Object>> resultList = new ArrayList<>();

        for (CmisLmt0024OccRelListReqDto occRelList : occRelListReqDtos) {
            //承兑行总行行号
            String orgCusId = occRelList.getOrgCusId();
            //占用金额
            BigDecimal bizTotalAmt = occRelList.getBizTotalAmt();
            //到期日
            String endDate = occRelList.getEndDate();

            if(StringUtils.isNotBlank(orgCusId)){
                logger.info("承兑行白名单，将同业客户大额行号【"+orgCusId+"】转换为客户号开始----->start");
                orgCusId = comm4Service.queryCusIdByBankNo(orgCusId);
                occRelList.setOrgCusId(orgCusId);
                logger.info("承兑行白名单，将同业客户大额行号转换为客户号【"+orgCusId+"】结束----->start");
            }else{
                throw new YuspException(EclEnum.ECL070004.key,"承兑行客户行号不允许为空");
            }

            if (!orgCusIdList.contains(orgCusId)){
                //如果orgCusIdList没有该交易编号，则放入orgCusIdList
                orgCusIdList.add(orgCusId);
            }

            if (orgCusIdAndAmtMap.containsKey(orgCusId)){
                //如果bizNoAndAmtMap里有该交易编号，则占用金额累加之后存入bizNoAndAmtMap
                BigDecimal bizTotalAmtTotal = orgCusIdAndAmtMap.get(orgCusId).add(bizTotalAmt);
                orgCusIdAndAmtMap.put(orgCusId,bizTotalAmtTotal);

                //比较orgCusIdAndMaxEndDateMap里的maxEndDate和该endDate哪个最大，存入大的到期日
                if (StringUtils.isNotEmpty(endDate) && endDate.compareTo(orgCusIdAndMaxEndDateMap.get(orgCusId))>0){
                    orgCusIdAndMaxEndDateMap.put(orgCusId,endDate);
                }
            }else{
                //没有则直接存入bizNoAndAmtMap
                orgCusIdAndAmtMap.put(orgCusId,bizTotalAmt);
                if (StringUtils.isNotEmpty(endDate)){
                    orgCusIdAndMaxEndDateMap.put(orgCusId,endDate);
                }
            }
        }

        //查询承兑行客户号对应的可用限额、额度分项编号、已用限额
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("instuCde",instuCde);
        queryModel.addCondition("cusIdList",orgCusIdList);
        queryModel.addCondition("lmtWhiteType",CmisLmtConstants.STD_ZB_LMT_WHITE_TYPE_01);

        List<Map<String, Object>> maps = lmtWhiteInfoService.selectInfoByQueryModel(queryModel);
        if (CollectionUtils.nonEmpty(maps)){

            for (Map<String, Object> map : maps) {
                String orgCusId = (String) map.get("cusId");
                //存入客户号对应的额度分项编号
                orgCusIdAndSubAccNoMap.put( orgCusId,(String) map.get("subAccNo"));

                if (orgCusIdAndAmtMap.containsKey(orgCusId)){
                    BigDecimal balanceAmt = BigDecimalUtil.replaceNull(map.get("balanceAmt"));

                    if(orgCusIdAndAmtMap.get(orgCusId).compareTo(balanceAmt)>0){
                        //占用金额总和超过可用限额，抛异常
                        throw new YuspException(EclEnum.ECL070079.key,"客户编号"+orgCusId+"的"+EclEnum.ECL070079.value);
                    }
                    //已用限额 = 已用限额 + 占用金额总和
                    BigDecimal sigUseAmt = BigDecimalUtil.replaceNull(map.get("sigUseAmt")).add(orgCusIdAndAmtMap.get(orgCusId));

                    orgCusIdAndSigUseAmtMap.put(orgCusId,sigUseAmt);
                }

                //add by lizx 2021-10-30 额度到期日不校验 第二轮演练 ：20211030-00113
                /*if (orgCusIdAndMaxEndDateMap.containsKey(orgCusId)){
                    String endDate = (String) map.get("endDate");

                    if (orgCusIdAndMaxEndDateMap.get(orgCusId).compareTo(endDate)>0){
                        //请求里的到期日超过白名单额度表里的对应到期日，则抛异常
                        throw new YuspException(EclEnum.ECL070004.key,"客户编号"+orgCusId+"的"+EclEnum.ECL070004.value);
                    }
                }*/
            }
        }else{
            //未找到承兑行白名单额度
            throw new YuspException(EclEnum.ECL070009.key,"客户编号的"+EclEnum.ECL070009.value);
        }

        resultList.add(orgCusIdAndSubAccNoMap);
        resultList.add(orgCusIdAndSigUseAmtMap);
        logger.info("{}占用前校验-----------------------------end", DscmsLmtEnum.TRADE_CODE_CMISLMT0024.key);
        return resultList;
    }
}