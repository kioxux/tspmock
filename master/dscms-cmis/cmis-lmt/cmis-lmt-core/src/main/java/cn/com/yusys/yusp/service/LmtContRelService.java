/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtContRel;
import cn.com.yusys.yusp.repository.mapper.LmtContRelMapper;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtContRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZR
 * @创建时间: 2021-04-20 10:38:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtContRelService {

    @Autowired
    private LmtContRelMapper lmtContRelMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtContRel selectByPrimaryKey(String pkId) {
        return lmtContRelMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectLmtContRelByDealBizNo
     * @方法描述: 根据合同编号，查询非合作方额度信息数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtContRel> selectLmtContRelByDealBizNo(String dealBizNo) {
        return lmtContRelMapper.selectLmtContRelByDealBizNo(dealBizNo);
    }


    /**
     * @方法名称: selectSigLmtByDealBizNo
     * @方法描述: 根据合同编号，查询同业或白名单额度信息数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtContRel> selectSigLmtByDealBizNo(String dealBizNo) {
        return lmtContRelMapper.selectSigLmtByDealBizNo(dealBizNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public List<LmtContRel> selectAll(QueryModel model) {
        List<LmtContRel> records = lmtContRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtContRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtContRel> list = lmtContRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtContRel record) {
        return lmtContRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtContRel record) {
        return lmtContRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtContRel record) {
        return lmtContRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtContRel record) {
        return lmtContRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtContRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtContRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称:
     * @方法描述：根据分项额度编号，获取分项占用关系信息
     * @参数与返回说明:LimitSubNo 分项额度编号  返回：额度分项编号下的所有占用信息
     * @算法描述: 无
     */
    public List<LmtContRel> selectLmtContRelByLimitSubNo(String limitSubNo){
        if(StringUtils.isBlank(limitSubNo)) {
            throw new YuspException(EclEnum.ECL070133.key, "【分项编号】" + EclEnum.ECL070133.value) ;
        }
        //根据分项编号获取分项占用关系表信息 LmtContRel
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("limitSubNo", limitSubNo);
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        List<LmtContRel> lmtContRelList = lmtContRelMapper.selectByModel(queryModel) ;
        return lmtContRelList ;
    }

    /**
     * @方法名称:
     * @方法描述：根据分项额度编号，获取分项占用关系信息
     * @参数与返回说明:LimitSubNo 分项额度编号  返回：额度分项编号下的所有占用信息
     * @算法描述: 无
     */
    public List<LmtContRel> selectLmtContRelByLimitSubNo(String limitSubNo, String status){
        if(StringUtils.isBlank(limitSubNo)) return null ;
        //根据分项编号获取分项占用关系表信息 LmtContRel
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("limitSubNos", limitSubNo);
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        queryModel.addCondition("bizStatus", status);
        List<LmtContRel> lmtContRelList = lmtContRelMapper.selectLmtContRelListByParam(queryModel) ;
        return lmtContRelList ;
    }

    /**
     * @方法名称:
     * @方法描述：根据分项额度编号，获取分项占用关系信息
     * @参数与返回说明:LimitSubNo 分项额度编号  返回：额度分项编号下的所有占用信息
     * @算法描述: 无
     */
    public List<LmtContRel> selectLmtContRelByLimitSubNo(String limitSubNo, String status, String dealBizNo){
        if(StringUtils.isBlank(limitSubNo)) return null ;
        //根据分项编号获取分项占用关系表信息 LmtContRel
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("limitSubNos", limitSubNo);
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        queryModel.addCondition("bizStatus", status);
        queryModel.addCondition("dealBizNo", dealBizNo);
        List<LmtContRel> lmtContRelList = lmtContRelMapper.selectLmtContRelListByParam(queryModel) ;
        return lmtContRelList ;
    }


    /**
     * @方法名称:
     * @方法描述：根据分项额度编号，获取分项占用关系信息
     * @参数与返回说明:LimitSubNo 分项额度编号  返回：额度分项编号下的所有占用信息
     * @算法描述: 无  selectLmtBizTotal
     */
    public Map selectLmtBizTotal(String subSerno, String cusId){
        //根据分项编号获取分项占用关系表信息 LmtContRel
        QueryModel queryModel = new QueryModel() ;
        //分项编号
        queryModel.addCondition("limitSubNo", subSerno);
        //客户号
        queryModel.addCondition("cusId", cusId);
        Map lmtContRelList = lmtContRelMapper.selectLmtBizTotal(queryModel) ;
        return lmtContRelList ;
    }

    /**
     * @方法名称:
     * @方法描述：根据分项编号
     * @参数与返回说明:LimitSubNo 分项额度编号  返回：额度分项编号下的所有占用信息
     * @算法描述: 无  selectLmtBizTotal
     */
    public List<LmtContRel> selectOutStandingLoadCont(String subSerno){
        //根据分项编号获取分项占用关系表信息 LmtContRel
        QueryModel queryModel = new QueryModel() ;
        //分项编号
        queryModel.addCondition("limitSubNo", subSerno);
        List<LmtContRel> lmtContRelList = lmtContRelMapper.selectLmtContRelList(queryModel) ;
        return lmtContRelList ;
    }

    /**
     * @方法名称:getNorStatusLmtContCnyBal
     * @方法描述:获取分项下状态借款合同余额
     * @参数与返回说明 subSerno 分项编号  dealBizType 交易业务类型  交易业务状态bizStatus  biz_attr:业务属性
     * @算法描述: 无
     */
    public Map<String, BigDecimal> getNorStatusLmtContCnyBal(String subSerno, String dealBizType, String bizStatus, String bizAttr){
        //根据分项编号获取分项占用关系表信息 LmtContRel
        QueryModel queryModel = new QueryModel() ;
        //分项编号
        queryModel.addCondition("limitSubNo", subSerno);

        //交易业务类型 1-一般合同 2-最高额合同 3-最高额协议
        if(StringUtils.isNotEmpty(dealBizType)){
            queryModel.addCondition("dealBizType", dealBizType);
        }

        //交易业务状态  100-未生效 200-有效	300-结清已使用	400-结清未使用	500-到期未结清
        queryModel.addCondition("bizStatus", bizStatus);
        //业务属性1-合同  2-台账
        queryModel.addCondition("bizAttr", bizAttr);
        return lmtContRelMapper.getNorStatusLmtContCnyBal(queryModel) ;
    }

    /**
     * @作者：李召星
     * @方法名称: selectLCRelByDBizNoAngSubNo
     * @方法描述: 根据交易流水号，获取合同信息 DEAL_BIZ_NO	交易业务编号
     * @参数与返回说明:
     * @算法描述：交易业务编号对应的合同信息
     */
    public LmtContRel selectLCRelByDBizNoAngSubNo(String origiDealBizNo, String limitSubNo, String sysId){
        if(StringUtils.isBlank(origiDealBizNo)) {
            throw new YuspException(EclEnum.ECL070133.key, "【交易流水号】" + EclEnum.ECL070133.value) ;
        }
        //根据合同编号，获取合同信息
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("dealBizNo", origiDealBizNo);
        queryModel.addCondition("limitSubNo", limitSubNo);
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        queryModel.addCondition("sysId", sysId);
        List<LmtContRel> lmtContRels = lmtContRelMapper.selectByModel(queryModel) ;
        if(CollectionUtils.isNotEmpty(lmtContRels)){
            return lmtContRels.get(0) ;
        }else{
            return null ;
        }
    }

    /**
     * @作者：李召星
     * @方法名称: selectLmtContRelByDealBizNo
     * @方法描述: 根据交易流水号，获取合同信息 DEAL_BIZ_NO	交易业务编号
     * @参数与返回说明:
     * @算法描述：交易业务编号对应的合同信息
     */
/*    public LmtContRel selectLmtContRelByDealBizNo(String dealBizNo){
        //根据合同编号，获取合同信息
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("dealBizNo", dealBizNo);
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        List<LmtContRel> lmtContRels = lmtContRelMapper.selectByModel(queryModel) ;
        if(CollectionUtils.isNotEmpty(lmtContRels)){
            return lmtContRels.get(0) ;
        }else{
            return null ;
        }
    }*/

    /**
     * @作者：李召星
     * @方法名称: selectLmtContRelListByDealBizNo
     * @方法描述: 根据交易流水号，获取合同信息 DEAL_BIZ_NO	交易业务编号
     * @参数与返回说明:
     * @算法描述：交易业务编号对应的合同信息
     */
    public List<LmtContRel> selectLmtContRelListByDealBizNo(String origiDealBizNo, String bizAttrs){
        if(StringUtils.isBlank(origiDealBizNo)) {
            throw new YuspException(EclEnum.ECL070133.key, "【交易编号】" + EclEnum.ECL070133.value) ;
        }
        //根据合同编号，获取合同信息
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("dealBizNo", origiDealBizNo);
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        if(StringUtils.isNotBlank(bizAttrs)){
            queryModel.addCondition("bizAttrs", bizAttrs);
        }
        return lmtContRelMapper.selectByModel(queryModel) ;
    }

    /**
     * 额度视图，查询额度项下关联业务
     * @author zhangjw 2021-05-05
     * @return
     */
    public List<Map> queryListByLimitSubNo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map> list = lmtContRelMapper.queryListByLimitSubNoMap(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 检查该分项编号是否存在
     * @param apprSubNo
     * @return
     */
    public int selectRecordsByApprSubNo(String apprSubNo){
        return lmtContRelMapper.selectRecordsByApprSubNo(apprSubNo);
    }

    /**
     * 单一客户授信-获取占用额度分项编号
     * @param dealBizNo
     * @return
     */
    public List<Map<String, String >> selectLimitSubNoByDealBizNoDY(String dealBizNo){
        return lmtContRelMapper.selectLimitSubNoByDealBizNoDY(dealBizNo);
    }

    /**
     * 根据交易业务编号查询产品名称
     * @param dealBizNo
     * @return
     */
    public Map<String,String> selectPrdNameByDealBizNo(String dealBizNo){
        return lmtContRelMapper.selectPrdNameByDealBizNo(dealBizNo);
    }

    /**
     * 根据额度分项编号列表查询分项信息
     * @param lmtSubNoList
     * @return
     */
    public List<Map<String,Object>> selectLmtInfoByLmtSubNoList(List<String> lmtSubNoList){
        return lmtContRelMapper.selectLmtInfoByLmtSubNoList(lmtSubNoList);
    }

    /**
     * 根据交易业务编号删除
     * @param dealBizNo
     * @return
     */
    public int deleteByDealBizNo(String dealBizNo){
        return lmtContRelMapper.deleteByDealBizNo(dealBizNo);
    }


    /**
     * 根据分项编号，查询额度占用关系中的占用总余额
     * @param limitSubNo
     * @return 额度占用关系中的总余额
     * 注释：未完成
     */
    public BigDecimal selectLmtContRelOutstandBal(String limitSubNo){
        return lmtContRelMapper.selectLmtContRelOutstandBal(limitSubNo) ;
    }

    /**
     * @作者:lizx
     * @方法名称: selectLmtContRelByApprSubSernos
     * @方法描述:  根据分项编号集合查询分项下是否有有效的占用关系
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/7/1 20:27
     * @param apprSubSernos: 
     * @return: java.lang.Integer
     * @算法描述: 无
    */
    public Integer selectLmtContRelByApprSubSernos(List<String> apprSubSernos){
        return lmtContRelMapper.selectLmtContRelByApprSubSernos(apprSubSernos) ;
    }



    /**
     * @作者:lizx
     * @方法名称: selectLmtContRelByApprSubSernos
     * @方法描述: 根据分项字符串（分项1，分项2，分项3） 查询向下占用总余额之和
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/9 10:47
     * @param limitSubNos:
     * @return: java.lang.Integer
     * @算法描述: 无
    */
    public BigDecimal selecBizBalByLimitSubNos(String limitSubNos){
        return lmtContRelMapper.selecBizBalByLimitSubNos(limitSubNos) ;
    }

    /**
     * @作者:lizx
     * @方法名称: selectLmtContRelByApprSubSernos
     * @方法描述: 根据分项编号和合同编号查询该交易是否已经成功
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/9 10:47
     * @param limitSubNo:
     * @param dealBizNo:
     * @return: java.lang.Integer
     * @算法描述: 无
     */
    public LmtContRel selectByLmtSubNoAndDealNo(String limitSubNo, String dealBizNo, String sysId){
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("dealBizNo", dealBizNo);
        queryModel.addCondition("limitSubNo", limitSubNo);
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        queryModel.addCondition("sysId", sysId);
        return lmtContRelMapper.selectByLmtSubNoAndDealNo(queryModel) ;
    }

    /**
     * @作者:lizx
     * @方法名称: selectLmtContRelByApprSubSernos
     * @方法描述: 根据分项编号，业务属性，查询向下占用总余额
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/9 10:47
     * @param queryModel:
     * @return: java.lang.Integer
     * @算法描述: 无
     */
    /*public BigDecimal selecBizBalByLimitSubNosAndBizAttr(QueryModel queryModel){
        return lmtContRelMapper.selecBizBalByLimitSubNosAndBizAttr(queryModel) ;
    }*/

    /**
     * @作者:lizx
     * @方法名称: updateByPKeyInApprLmtChgDetails
     * @方法描述:  传入原始数据，最新数据，一级相关接口信息，跟数据的同时，调用留存记录，留存数据信息appr_lmt_chg_details
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/28 19:44
     * @param lmtContRel:
     * @param paramMap:
     * @return: int
     * @算法描述: 无
     */
    public int updateByPKeyInApprLmtChgDetails(LmtContRel lmtContRel, Map<String, String> paramMap){
        return lmtContRelMapper.updateByPrimaryKey(lmtContRel);
    }


    /**
     * @作者:lizx
     * @方法名称: selectByParamIsExistsBusinss
     * @方法描述: 根据分项编号，状态，以及合同类型，查询分项编号向下的合同信息
     * @参数与返回说明: String
     * @算法描述: 无
     * @日期：2021/7/28 19:44
     * @param dealBizType:
     * @param bizStatus:
     * @param dealBizType:
     * @return: int
     * @算法描述: 无
     */
    public int selectByParamIsExistsBusinss(String limitSubNos, String bizStatus, String dealBizType){
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("limitSubNos", limitSubNos);
        queryModel.addCondition("bizStatus", bizStatus);
        queryModel.addCondition("dealBizTypes", dealBizType);
        return lmtContRelMapper.selectByParamIsExistsBusinss(queryModel);
    }


    /**
     * @方法名称: selectUnCoopLCRByDealBizNo
     * @方法描述: 根据合同编号，查询非合作方额度信息数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtContRel> selectUnCoopLCRByDealBizNo(String dealBizNo) {
        return lmtContRelMapper.selectUnCoopLCRByDealBizNo(dealBizNo);
    }

    /**
     * @作者:lizx
     * @方法名称: selectLmtContRelByApprSubSernos
     * @方法描述: 根据分项字符串（分项1，分项2，分项3） 查询向下占用总余额之和
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/9 10:47
     * @param limitSubNos:
     * @return: java.lang.Integer
     * @算法描述: 无
     */
    public BigDecimal selecBizTotalByLimitSubNos(String limitSubNos){
        return lmtContRelMapper.selecBizTotalByLimitSubNos(limitSubNos) ;
    }

    /**
     * @方法名称: selecContInfoByCusId
     * @方法描述: 根据客户号，获取贴现业务台账信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtContRel> selecContInfoByCusId(String cusId) {
        return lmtContRelMapper.selecContInfoByCusId(cusId);
    }



    /**
     * @方法名称: selecContInfoByCusId
     * @方法描述: 根据客户号，获取贴现业务台账信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtContRel> select4002ByDealBizNo(String dealBizNo) {
        return lmtContRelMapper.select4002ByDealBizNo(dealBizNo);
    }
}
