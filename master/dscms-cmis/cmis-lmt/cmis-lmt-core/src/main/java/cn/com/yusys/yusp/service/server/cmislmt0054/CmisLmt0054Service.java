package cn.com.yusys.yusp.service.server.cmislmt0054;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0054.req.CmisLmt0054ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0054.resp.CmisLmt0054RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprStrMtableInfoService;
import com.alibaba.excel.util.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0054Service
 * @类描述: #对内服务类
 * @功能描述: 查询多客户量敞口金额和敞口余额
 * @创建时间: 2021-09-05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0054Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0054Service.class);

    @Autowired
    private ApprStrMtableInfoService apprStrMtableInfoService ;
    /**
     * 查询多客户量敞口金额和敞口余额
     * add by zhangjw 2021-09-05
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0054RespDto execute(CmisLmt0054ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0054.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0054.value);
        CmisLmt0054RespDto respDto = new CmisLmt0054RespDto();

        try {
            //金融组织代码
            String instuDde = reqDto.getInstuCde() ;
            //客户号
            String cusId = reqDto.getCusId() ;
            //类型 01-单一客户授信
            String lmtType = reqDto.getLmtType() ;

            QueryModel queryModel = new QueryModel() ;
            queryModel.addCondition("instuCde", instuDde);
            queryModel.addCondition("cusId", cusId);
            queryModel.addCondition("lmtType", lmtType);
            Map<String, BigDecimal> amtMap = apprStrMtableInfoService.selectAvlAmtByCusIdParam(queryModel) ;

            //授信总额
            BigDecimal avlAmt = amtMap.get("avlAmt") ;
            BigDecimal lmtAmtAdd = amtMap.get("lmtAmtAdd") ;

            BigDecimal returnAmt = avlAmt.add(lmtAmtAdd) ;
            respDto.setAvlAmt(returnAmt);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("【cmislmt0054】查询查询多客户量敞口金额和敞口余额：" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0054.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0054.value);
        return respDto;
    }
}