package cn.com.yusys.yusp.service.server.cmislmt0046;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.dto.server.cmislmt0046.req.CmisLmt0046ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0046.resp.CmisLmt0046RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.LmtSubPrdMappConfService;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0046Service
 * @类描述: #对内服务类
 * @功能描述: 根据分项编号和用信产品编号，查询分项向下是否存在适用该产品的明细
 * @修改备注: 2021/8/26     lizx   新增接口
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0046Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0046Service.class);
    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;
    @Autowired
    private Comm4Service comm4Service ;
    @Transactional
    public CmisLmt0046RespDto execute(CmisLmt0046ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0046.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0046.value);
        CmisLmt0046RespDto resqDto = new CmisLmt0046RespDto() ;

        try {
            String accSubNo = reqDto.getAccSubNo() ;
            String prdId = reqDto.getPrdId() ;
            if(StringUtils.isBlank(accSubNo)){
                throw new YuspException(EclEnum.ECL070133.key, "分项编号" + EclEnum.ECL070133.value);
            }
            if(StringUtils.isBlank(prdId)){
                throw new YuspException(EclEnum.ECL070133.key, "用信产品编号" + EclEnum.ECL070133.value);
            }

            String limitSubNo = comm4Service.getLimitSubNoByPrdIdAndStrNo(prdId, CmisLmtConstants.STD_ZB_LMT_STR_MTABLE_CONF_01) ;
            if(StringUtils.isBlank(limitSubNo)){
                limitSubNo = comm4Service.getLimitSubNoByPrdIdAndStrNo(prdId, CmisLmtConstants.STD_ZB_LMT_STR_MTABLE_CONF_02) ;
            }
            if(StringUtils.isBlank(limitSubNo)) throw new YuspException(EclEnum.ECL070082.key,EclEnum.ECL070082.value);

            ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectSubInfoByParentIdAndLSubNo(accSubNo, limitSubNo) ;
            if(Objects.nonNull(apprLmtSubBasicInfo)){
                resqDto.setHasBussFlag(CmisLmtConstants.YES_NO_Y);
                resqDto.setSubNo(apprLmtSubBasicInfo.getApprSubSerno());
            }else{
                resqDto.setHasBussFlag(CmisLmtConstants.YES_NO_N);
            }
            //将列表放入返回中
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("获取查询机构贴现限额报错：",e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0046.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0046.value);
        return resqDto;
    }

}