package cn.com.yusys.yusp.web.server.cmislmt0008;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.resp.CmisLmt0008RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0008.CmisLmt0008Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:额度提前终止
 *
 * @author jiangyl 20210507
 * @version 1.0
 */
@Api(tags = "cmislmt0008:额度提前终止")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0008Resource {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0008Resource.class);
    
    @Autowired
    private CmisLmt0008Service cmisLmt0008Service;

    /**
     * 交易码：cmislmt0008
     * 交易描述：额度提前终止
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("额度提前终止")
    @PostMapping("/cmislmt0008")
    protected @ResponseBody
    ResultDto<CmisLmt0008RespDto> cmisLmt0008(@Validated @RequestBody CmisLmt0008ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0008.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0008.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0008RespDto> cmisLmt0008RespDtoResultDto = new ResultDto<>();
        CmisLmt0008RespDto cmisLmt0008RespDto = new CmisLmt0008RespDto();
        // 调用对应的service层
        try {
            cmisLmt0008RespDto = cmisLmt0008Service.terminate(reqDto);
            cmisLmt0008RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0008RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0008.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0008.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0008.value, e.getMessage());
            // 封装xddb0001DataResultDto中异常返回码和返回信息
            cmisLmt0008RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0008RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        cmisLmt0008RespDtoResultDto.setData(cmisLmt0008RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0008.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0008.value, JSON.toJSONString(cmisLmt0008RespDtoResultDto));
        return cmisLmt0008RespDtoResultDto;
    }

}
