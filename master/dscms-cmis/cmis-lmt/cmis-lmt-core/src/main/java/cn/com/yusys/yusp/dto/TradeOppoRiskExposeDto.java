package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: TradeOppoRiskExpose
 * @类描述: trade_oppo_risk_expose数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-27 21:38:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class TradeOppoRiskExposeDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 客户编号 **/
	private String cusId;
	/** 产品名称 **/
	private String prdName;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 本金金额 **/
	private java.math.BigDecimal holdPosition;
	
	/** 不考虑缓释的风险暴露 **/
	private java.math.BigDecimal riskExposeNoslowRelease;
	
	/** 不可豁免的风险暴露 **/
	private java.math.BigDecimal riskExposeNoexampt;
	
	/** 可豁免的风险暴露 **/
	private java.math.BigDecimal riskExposeExampt;
	
	/** 风险缓释金额 **/
	private java.math.BigDecimal riskExposeAmt;
	
	/** 风险缓释客户号 **/
	private String guarCusId;
	
	/** 风险缓释客户名称 **/
	private String guarCusName;
	
	/** 业务日期 **/
	private String bussDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记时间 **/
	private String inputDate;
	
	/** 最近更新人 **/
	private String updId;
	
	/** 最近更新机构 **/
	private String updBrId;
	
	/** 最近更新日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param holdPosition
	 */
	public void setHoldPosition(java.math.BigDecimal holdPosition) {
		this.holdPosition = holdPosition;
	}
	
    /**
     * @return HoldPosition
     */	
	public java.math.BigDecimal getHoldPosition() {
		return this.holdPosition;
	}
	
	/**
	 * @param riskExposeNoslowRelease
	 */
	public void setRiskExposeNoslowRelease(java.math.BigDecimal riskExposeNoslowRelease) {
		this.riskExposeNoslowRelease = riskExposeNoslowRelease;
	}
	
    /**
     * @return RiskExposeNoslowRelease
     */	
	public java.math.BigDecimal getRiskExposeNoslowRelease() {
		return this.riskExposeNoslowRelease;
	}
	
	/**
	 * @param riskExposeNoexampt
	 */
	public void setRiskExposeNoexampt(java.math.BigDecimal riskExposeNoexampt) {
		this.riskExposeNoexampt = riskExposeNoexampt;
	}
	
    /**
     * @return RiskExposeNoexampt
     */	
	public java.math.BigDecimal getRiskExposeNoexampt() {
		return this.riskExposeNoexampt;
	}
	
	/**
	 * @param riskExposeExampt
	 */
	public void setRiskExposeExampt(java.math.BigDecimal riskExposeExampt) {
		this.riskExposeExampt = riskExposeExampt;
	}
	
    /**
     * @return RiskExposeExampt
     */	
	public java.math.BigDecimal getRiskExposeExampt() {
		return this.riskExposeExampt;
	}
	
	/**
	 * @param riskExposeAmt
	 */
	public void setRiskExposeAmt(java.math.BigDecimal riskExposeAmt) {
		this.riskExposeAmt = riskExposeAmt;
	}
	
    /**
     * @return RiskExposeAmt
     */	
	public java.math.BigDecimal getRiskExposeAmt() {
		return this.riskExposeAmt;
	}
	
	/**
	 * @param guarCusId
	 */
	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId == null ? null : guarCusId.trim();
	}
	
    /**
     * @return GuarCusId
     */	
	public String getGuarCusId() {
		return this.guarCusId;
	}
	
	/**
	 * @param guarCusName
	 */
	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName == null ? null : guarCusName.trim();
	}
	
    /**
     * @return GuarCusName
     */	
	public String getGuarCusName() {
		return this.guarCusName;
	}
	
	/**
	 * @param bussDate
	 */
	public void setBussDate(String bussDate) {
		this.bussDate = bussDate == null ? null : bussDate.trim();
	}
	
    /**
     * @return BussDate
     */	
	public String getBussDate() {
		return this.bussDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}