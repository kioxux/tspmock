package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;
import java.time.LocalDate;

@ExcelCsv(namePrefix = "贴现限额管控列表导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class LmtDiscOrgTempExportVo {

    /*
    机构号
     */
    @ExcelField(title = "机构号", viewLength = 20)
    private String organno;

    /*
    机构名称
     */
    @ExcelField(title = "机构名称", viewLength = 20)
    private String organname;

    /*
    申请额度
     */
    @ExcelField(title = "申请额度", viewLength = 20)
    private BigDecimal applyAmount;

    /*
    核准额度
     */
    @ExcelField(title = "核准额度", viewLength = 20)
    private BigDecimal approveAmount;

    /*
    生效日期
     */
    @ExcelField(title = "生效日期", viewLength = 20)
    private String appYm;

    public String getOrganno() {
        return organno;
    }

    public void setOrganno(String organno) {
        this.organno = organno;
    }

    public String getOrganname() {
        return organname;
    }

    public void setOrganname(String organname) {
        this.organname = organname;
    }

    public BigDecimal getApplyAmount() {
        return applyAmount;
    }

    public void setApplyAmount(BigDecimal applyAmount) {
        this.applyAmount = applyAmount;
    }

    public BigDecimal getApproveAmount() {
        return approveAmount;
    }

    public void setApproveAmount(BigDecimal approveAmount) {
        this.approveAmount = approveAmount;
    }

    public String getAppYm() {
        return appYm;
    }

    public void setAppYm(String appYm) {
        this.appYm = appYm;
    }
}
