package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: TmpAddamtTdcarAl
 * @类描述: tmp_addamt_tdcar_al数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-11-06 18:15:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class TmpAddamtTdcarAlDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String serno;
	
	/** 数值1 **/
	private java.math.BigDecimal amt1;
	
	/** 数值2 **/
	private java.math.BigDecimal amt2;
	
	/** 数值3 **/
	private java.math.BigDecimal amt3;
	
	/** 数值4 **/
	private java.math.BigDecimal amt4;
	
	/** 数值5 **/
	private java.math.BigDecimal amt5;
	
	/** 字符型1 **/
	private String field1;
	
	/** 字符型2 **/
	private String field2;
	
	/** 字符型3 **/
	private String field3;
	
	/** 字符型4 **/
	private String field4;
	
	/** 字符型5 **/
	private String field5;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param amt1
	 */
	public void setAmt1(java.math.BigDecimal amt1) {
		this.amt1 = amt1;
	}
	
    /**
     * @return Amt1
     */	
	public java.math.BigDecimal getAmt1() {
		return this.amt1;
	}
	
	/**
	 * @param amt2
	 */
	public void setAmt2(java.math.BigDecimal amt2) {
		this.amt2 = amt2;
	}
	
    /**
     * @return Amt2
     */	
	public java.math.BigDecimal getAmt2() {
		return this.amt2;
	}
	
	/**
	 * @param amt3
	 */
	public void setAmt3(java.math.BigDecimal amt3) {
		this.amt3 = amt3;
	}
	
    /**
     * @return Amt3
     */	
	public java.math.BigDecimal getAmt3() {
		return this.amt3;
	}
	
	/**
	 * @param amt4
	 */
	public void setAmt4(java.math.BigDecimal amt4) {
		this.amt4 = amt4;
	}
	
    /**
     * @return Amt4
     */	
	public java.math.BigDecimal getAmt4() {
		return this.amt4;
	}
	
	/**
	 * @param amt5
	 */
	public void setAmt5(java.math.BigDecimal amt5) {
		this.amt5 = amt5;
	}
	
    /**
     * @return Amt5
     */	
	public java.math.BigDecimal getAmt5() {
		return this.amt5;
	}
	
	/**
	 * @param field1
	 */
	public void setField1(String field1) {
		this.field1 = field1 == null ? null : field1.trim();
	}
	
    /**
     * @return Field1
     */	
	public String getField1() {
		return this.field1;
	}
	
	/**
	 * @param field2
	 */
	public void setField2(String field2) {
		this.field2 = field2 == null ? null : field2.trim();
	}
	
    /**
     * @return Field2
     */	
	public String getField2() {
		return this.field2;
	}
	
	/**
	 * @param field3
	 */
	public void setField3(String field3) {
		this.field3 = field3 == null ? null : field3.trim();
	}
	
    /**
     * @return Field3
     */	
	public String getField3() {
		return this.field3;
	}
	
	/**
	 * @param field4
	 */
	public void setField4(String field4) {
		this.field4 = field4 == null ? null : field4.trim();
	}
	
    /**
     * @return Field4
     */	
	public String getField4() {
		return this.field4;
	}
	
	/**
	 * @param field5
	 */
	public void setField5(String field5) {
		this.field5 = field5 == null ? null : field5.trim();
	}
	
    /**
     * @return Field5
     */	
	public String getField5() {
		return this.field5;
	}


}