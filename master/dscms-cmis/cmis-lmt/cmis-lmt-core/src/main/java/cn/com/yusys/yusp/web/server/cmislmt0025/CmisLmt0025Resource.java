package cn.com.yusys.yusp.web.server.cmislmt0025;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0025.req.CmisLmt0025ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0025.resp.CmisLmt0025RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0025.CmisLmt0025Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:承兑行白名单额度恢复
 *
 * @author dumd 20210608
 * @version 1.0
 */
@Api(tags = "cmislmt0025:承兑行白名单额度恢复")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0025Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0025Resource.class);

    @Autowired
    private CmisLmt0025Service cmisLmt0025Service;

    /**
     * 交易码：cmislmt0025
     * 交易描述：承兑行白名单额度恢复
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("承兑行白名单额度恢复")
    @PostMapping("/cmislmt0025")
    protected @ResponseBody
    ResultDto<CmisLmt0025RespDto> cmisLmt0025(@Validated @RequestBody CmisLmt0025ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0025.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0025RespDto> cmisLmt0025RespDtoResultDto = new ResultDto<>();
        CmisLmt0025RespDto cmisLmt0025RespDto = new CmisLmt0025RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0025RespDto = cmisLmt0025Service.execute(reqDto);
            cmisLmt0025RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0025RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0025.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0025.value, e.getMessage());
            // 封装cmisLmt0025RespDtoResultDto中异常返回码和返回信息
            cmisLmt0025RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0025RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0025RespDto到cmisLmt0025RespDtoResultDto中
        cmisLmt0025RespDtoResultDto.setData(cmisLmt0025RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.value, JSON.toJSONString(cmisLmt0025RespDtoResultDto));
        return cmisLmt0025RespDtoResultDto;
    }
}