/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.LmtApprExceptionDefEnums;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.ContAccRel;
import cn.com.yusys.yusp.domain.ContAccRelChgApp;
import cn.com.yusys.yusp.domain.LmtContRel;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.repository.mapper.ContAccRelChgAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ContAccRelChgAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-20 20:39:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ContAccRelChgAppService {

    @Autowired
    private ContAccRelChgAppMapper contAccRelChgAppMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
	@Autowired
    private ContAccRelService contAccRelService ;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public ContAccRelChgApp selectByPrimaryKey(String pkId) {
        return contAccRelChgAppMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<ContAccRelChgApp> selectAll(QueryModel model) {
        List<ContAccRelChgApp> records = (List<ContAccRelChgApp>) contAccRelChgAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<ContAccRelChgApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ContAccRelChgApp> list = contAccRelChgAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(ContAccRelChgApp record) {
        return contAccRelChgAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(ContAccRelChgApp record) {
        return contAccRelChgAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(ContAccRelChgApp record) {
        return contAccRelChgAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(ContAccRelChgApp record) {
        return contAccRelChgAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return contAccRelChgAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return contAccRelChgAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int logicDelete(String pkId) {
        //逻辑删除操作参数
        Map delMap = new HashMap();
        delMap.put("pkId",pkId);
        //获取业务标识位，通过标识位进行逻辑删除操作
        delMap.put("oprType",  CmisLmtConstants.OPR_TYPE_DELETE);
        return contAccRelChgAppMapper.updateByParams(delMap);
    }

    /**
     * @方法名称: createcontAccRelChgApp
     * @方法描述: 根据分项编号，生成分项占用关系信息
     * @参数与返回说明:limitSubNo 分项编号    返回：组装后的分项编号占用关系申请
     * @算法描述: 无
     */

    public List<ContAccRelChgApp> createContAccRelChgApp(String dealBizNo) {
        //定义返回类型
        List<ContAccRelChgApp> contAccRelChgAppList = new ArrayList<>();
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        //获取分项编号下的占用关系
        List<ContAccRel> contAccRelList = contAccRelService.selectContAccRelByDealBizNo(dealBizNo);
        String pkValue = "" ;
        //如果大于0，进行申请数据组装处理
        if (contAccRelList.size() > 0) {
            for (ContAccRel contAccRel : contAccRelList) {
                ContAccRelChgApp contAccRelChgApp = new ContAccRelChgApp();
                //生成主键，获取流水号
                pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, new HashMap<>());

                BeanUtils.copyProperties(contAccRel, contAccRelChgApp);
                //主键
                contAccRelChgApp.setPkId(pkValue);
                //关联主键
                contAccRelChgApp.setRelId(contAccRel.getPkId());
                //更新最近更新人
                contAccRelChgApp.setInputId(userInfo.getLoginCode());
                // // 申请机构
                contAccRelChgApp.setInputBrId(userInfo.getOrg().getCode());
                String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前日期
                // 申请时间
                contAccRelChgApp.setInputDate(openDay);
                // 更新人
                contAccRelChgApp.setUpdId(userInfo.getLoginCode());
                // 更新机构
                contAccRelChgApp.setUpdBrId(userInfo.getOrg().getCode());
                //更新日期
                contAccRelChgApp.setUpdDate(openDay);
                //修改时间
                contAccRelChgApp.setUpdateTime(new Date());
                // 创建时间
                contAccRelChgApp.setCreateTime(new Date());
                //操作状态
                contAccRelChgApp.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
                contAccRelChgAppList.add(contAccRelChgApp);
            }
        }
        return contAccRelChgAppList;
    }

    /**
     * @方法名称: selectByAppSerno
     * @方法描述: 根据调整主申请流水号获取
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<ContAccRelChgApp> selectByAppSerno(String appSerno) {
        List<ContAccRelChgApp> records = (List<ContAccRelChgApp>) contAccRelChgAppMapper.selectByAppSerno(appSerno);
        return records;
    }

    /**
     * @方法名称: handleBusinessDataAfterEnd
     * @方法描述: 额度调整申请审批通过后处理
     * @参数与返回说明:limitSubNo 主申请流水号
     * @算法描述: 无
     */

    public void handleBusinessDataAfterEnd(String appSerno) {
        //判断传入的主申请流水号是否为空
        if (StringUtils.isBlank(appSerno)) {
            throw new YuspException(EclEnum.ECL070041.key, EclEnum.ECL070041.value);
        }
        //获取调整申请流水号下的台账占用关系
        List<ContAccRelChgApp> contAccRelChgAppList = this.selectByAppSerno(appSerno);
        //如果大于0，进行申请数据组装处理
        if (contAccRelChgAppList!=null && contAccRelChgAppList.size() > 0) {
            for (ContAccRelChgApp contAccRelChgApp : contAccRelChgAppList) {
                //获取关联LmtContRel表的主键
                String relId = contAccRelChgApp.getRelId();
                if(StringUtils.isBlank(relId)){
                    ContAccRel contAccRel = new ContAccRel();
                    BeanUtils.copyProperties(contAccRelChgApp, contAccRel);
                    contAccRelService.insert(contAccRel);
                }else{
                    //根据关联主键获取分项占用关系表的对应数据
                    ContAccRel contAccRel= contAccRelService.selectByPrimaryKey(relId);
                    if(contAccRel==null || "".equals(contAccRel.getPkId())){
                        throw new YuspException(EclEnum.ECL070045.key, EclEnum.ECL070045.value);
                    }
                    BeanUtils.copyProperties(contAccRelChgApp, contAccRel);
                    contAccRelService.update(contAccRel);
                }
            }
        }
    }

    /**
     * 检查合同编号是否存在
     * @author zhangjw 2021-05-18
     * @return
     */
    public int checkSameDealBizNoIsExistAll(String dealBizNo) {
        int result = contAccRelService.checkSameDealBizNoIsExist(dealBizNo);
        result = result + contAccRelChgAppMapper.checkSameDealBizNoIsExistAll(dealBizNo);
        return result;
    }

    /**
     * 检查台账编号是否存在
     * @author
     * @return
     */
    public int checkSameTranAccNoIsExistAll(String tranAccNo) {
        int result = contAccRelChgAppMapper.checkSameTranAccNoIsExistAll(tranAccNo);
        return result;
    }
}
