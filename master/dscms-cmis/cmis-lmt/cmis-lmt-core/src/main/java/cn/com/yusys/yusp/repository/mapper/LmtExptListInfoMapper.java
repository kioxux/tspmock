/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtExptListInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtExptListInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: lenovo
 * @创建时间: 2021-04-16 16:27:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtExptListInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtExptListInfo selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    LmtExptListInfo selectBySerno(@Param("serno") String serno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtExptListInfo> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtExptListInfo record);

    /**
     * @方法名称: insertAppToInfo
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertAppToInfo(LmtExptListInfo record);


    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtExptListInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtExptListInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtExptListInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: updateAppToInfo
     * @方法描述: App表变更提交
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateAppToInfo(LmtExptListInfo record);

    /**
     * @方法名称: logicaldelete
     * @方法描述: 根据流水号逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int logicaldelete(LmtExptListInfo record);

}