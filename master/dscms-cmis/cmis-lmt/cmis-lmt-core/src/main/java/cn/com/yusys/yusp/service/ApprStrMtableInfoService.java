/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.ApprStrMtableInfo;
import cn.com.yusys.yusp.domain.CusGrpMemberRel;
import cn.com.yusys.yusp.dto.ApprStrMtableInfoDto;
import cn.com.yusys.yusp.dto.server.cmiscus0003.req.CmisCus0003ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0003.resp.CmisCus0003ComCusRelListRespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0003.resp.CmisCus0003RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0004.req.CmisCus0004ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0004.resp.CmisCus0004IndivCusRelListRespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0004.resp.CmisCus0004RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.req.CmisLmt0015ReqDto;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.repository.mapper.ApprStrMtableInfoMapper;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprStrMtableInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 86188
 * @创建时间: 2021-05-11 10:36:52
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ApprStrMtableInfoService {
    private static final Logger logger = LoggerFactory.getLogger(ApprStrMtableInfoService.class);

    @Autowired
    private ApprStrMtableInfoMapper apprStrMtableInfoMapper;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private CusGrpMemberRelService cusGrpMemberRelService;

    @Autowired
    private CusGrpService cusGrpService ;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ApprStrMtableInfo selectByPrimaryKey(String pkId) {
        return apprStrMtableInfoMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly=true)
    public List<ApprStrMtableInfo> selectAll(QueryModel model) {
        List<ApprStrMtableInfo> records = (List<ApprStrMtableInfo>) apprStrMtableInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<ApprStrMtableInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ApprStrMtableInfo> list = apprStrMtableInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(ApprStrMtableInfo record) {
        return apprStrMtableInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(ApprStrMtableInfo record) {
        return apprStrMtableInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(ApprStrMtableInfo record) {
        return apprStrMtableInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(ApprStrMtableInfo record) {
        return apprStrMtableInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return apprStrMtableInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return apprStrMtableInfoMapper.deleteByIds(ids);
    }

    /**
     * @param queryModel
     * @return List<Map<String,Object>>
     * @date 2021/4/19 15:25
     * @version 1.0.0
     * @desc    法人客户额度视图信息
     * @修改历史: zhangjw
     */
    public ResultDto selectStrInfoByList(QueryModel queryModel) {
        int pageCount = apprStrMtableInfoMapper.selectPageCountByParam(queryModel) ;

        PageHelper.startPage(queryModel.getPage(), queryModel.getSize(), false);
        List<String> cusList = apprStrMtableInfoMapper.selectCusIdInfoByList(queryModel);
        PageInfo<String> pageinfo = new PageInfo<>(cusList) ;
        int total = pageCount ;
        ResultDto resultDto = new ResultDto();
        String instuCde =  (String) queryModel.getCondition().get("instuCde") ;

        if(CollectionUtils.isNotEmpty(cusList)){
            QueryModel queryParam = new QueryModel() ;
            BeanUtils.copyProperties(queryModel, queryParam);
            queryParam.addCondition("cusList", cusList);
            if(StringUtils.isNotEmpty(instuCde)){
                queryParam.addCondition("instuCde", instuCde);
            }

            System.out.println("查询客户信息"+JSON.toJSONString(cusList));
            List<Map<String, Object>> mapList = apprStrMtableInfoMapper.selectStrInfoByList(queryParam);
            if (mapList.size() > 0) {
                for (Map<String, Object> map : mapList) {
                    BigDecimal lmtAmtAdd = BigDecimalUtil.replaceNull(map.get("lmtAmtAdd"));
                    BigDecimal avlAmt = BigDecimalUtil.replaceNull(map.get("avlAmt"));
                    // 授信总额
                    BigDecimal totalAmt = lmtAmtAdd.add(avlAmt);
                    map.put("totalAmt",totalAmt);
                    // 授信可用金额
                    BigDecimal totalValAmt = totalAmt.subtract(BigDecimalUtil.replaceNull(map.get("totalUseAmt")));
                    map.put("totalValAmt",totalValAmt);
                    // 敞口可用金额
                    BigDecimal totalSpacValAmt = BigDecimalUtil.replaceNull(map.get("totalSpacAmt")).subtract(BigDecimalUtil.replaceNull(map.get("totalSpacUseAmt")));
                    map.put("totalSpacValAmt",totalSpacValAmt);
                }
            }
            resultDto.setData(mapList);
        }
        resultDto.setTotal(total);
        PageHelper.clearPage();
        return resultDto;
    }

    /**
     * @param queryModel
     *      instuCde 金融机构号（Y）；支持cusId客户号、cusName客户名称查询
     * @return List<Map<String,Object>>
     * @date 2021/5/21
     * @version 1.0.0
     * @desc    同业客户额度视图信息
     * @修改历史: dumd
     */
    public ResultDto selectSigStrInfoByList(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String,String>> sigCusList = apprStrMtableInfoMapper.selectSigCusIdListByQueryModel(queryModel);
        PageHelper.clearPage();
        ResultDto sigCusListResult = new ResultDto<>(sigCusList);
        long total = sigCusListResult.getTotal();

        List<Map<String, Object>> mapList = new ArrayList<>();

        if (sigCusList!=null && sigCusList.size()>0){
            List<String> cusIdList = new ArrayList<>();

            for (Map<String, String> sigCus : sigCusList) {
                cusIdList.add(sigCus.get("cusId"));
            }

            mapList = apprStrMtableInfoMapper.selectSigStrInfoByList(cusIdList);
        }

        //将客户号相同的记录里的金额进行累加
        if (mapList!=null && mapList.size()>0){
            Map<String,Map<String, Object>> map = new HashMap<>();

            for (Map<String, Object> sigInfo : mapList) {
                String cusId = (String) sigInfo.get("cusId");

                if (map.containsKey(cusId)){
                    Map<String, Object> value = map.get(cusId);

                    BigDecimal totalAmt = new BigDecimal(value.get("totalAmt").toString());
                    BigDecimal totalUseAmt = new BigDecimal(value.get("totalUseAmt").toString());
                    BigDecimal totalAvlAmt = new BigDecimal(value.get("totalAvlAmt").toString());
                    BigDecimal totalPrdAmt = new BigDecimal(value.get("totalPrdAmt").toString());
                    BigDecimal totalPrdUseAmt = new BigDecimal(value.get("totalPrdUseAmt").toString());
                    BigDecimal totalPrdAvlAmt = new BigDecimal(value.get("totalPrdAvlAmt").toString());

                    totalAmt = totalAmt.add(new BigDecimal(sigInfo.get("totalAmt").toString()));
                    totalUseAmt = totalUseAmt.add(new BigDecimal(sigInfo.get("totalUseAmt").toString()));
                    totalAvlAmt = totalAvlAmt.add((new BigDecimal(sigInfo.get("totalAvlAmt").toString())));
                    totalPrdAmt = totalPrdAmt.add((new BigDecimal(sigInfo.get("totalPrdAmt").toString())));
                    totalPrdUseAmt = totalPrdUseAmt.add(new BigDecimal( sigInfo.get("totalPrdUseAmt").toString()));
                    totalPrdAvlAmt = totalPrdAvlAmt.add(new BigDecimal(sigInfo.get("totalPrdAvlAmt").toString()));

                    value.put("totalAmt",totalAmt);
                    value.put("totalUseAmt",totalUseAmt);
                    value.put("totalAvlAmt",totalAvlAmt);
                    value.put("totalPrdAmt",totalPrdAmt);
                    value.put("totalPrdUseAmt",totalPrdUseAmt);
                    value.put("totalPrdAvlAmt",totalPrdAvlAmt);
                    map.put(cusId,value);
                }else{
                    map.put(cusId,sigInfo);
                }
            }

            //清空mapList,将map里的value放入mapList里
            mapList.clear();
            Set<Map.Entry<String,Map<String, Object> >> entries = map.entrySet();

            for (Map.Entry<String, Map<String, Object>> entry : entries) {
                mapList.add(entry.getValue());
            }
        }

        ResultDto resultDto = new ResultDto();
        resultDto.setData(mapList);
        resultDto.setTotal(total);
        return resultDto;
    }



    /**
     * @param queryModel
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     * @date 2021/5/3
     * @version 1.0.0
     * @desc    集团客户额度视图列表信息
     * @修改历史: zhangjw
     */
    /*public ResultDto selectGrpStrInfoByList(QueryModel queryModel) {
        List<Map<String, Object>> grpNoList = apprStrMtableInfoMapper.selectGrpNosByQueryModel(queryModel);
        List<String> grpNosOntList = grpNoList.stream().map(e -> (String) e.get("grpNo")).distinct().collect(Collectors.toList());
        //grpNoList.stream().distinct().collect(Collectors.toList());
        int total = grpNosOntList.size() ;
        int startPage = (queryModel.getPage()-1) * queryModel.getSize() ;
        int endPage = queryModel.getPage() * queryModel.getSize() ;

        if(endPage>total) endPage = total ;
        List<String> grpNoTwoList = grpNosOntList.subList(startPage, endPage) ;

        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        PageInfo<String> pageinfo = new PageInfo<>(grpNoTwoList);
        //int total = (int)pageinfo.getTotal();
        PageHelper.clearPage();
        //查询集团客户编号列表
        logger.info("集团列表集团客户信息展示----{}", JSON.toJSONString(grpNoTwoList));
        List<Map<String, Object>> mapList = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(grpNoTwoList)){
            List<String> grpNos = grpNoTwoList ;//grpNoTwoList.stream().map(e -> (String) e.get("grpNo")).distinct().collect(Collectors.toList());
            mapList = apprStrMtableInfoMapper.selectGrpStrInfoByGrpNos(grpNos);
        }


        logger.info("集团列表集团视图列表展示----{}", JSON.toJSONString(mapList));
        ResultDto resultDto = new ResultDto();
        resultDto.setData(mapList);
        resultDto.setTotal(total);
        PageHelper.clearPage();
        return resultDto;
    }*/

    public ResultDto selectGrpStrInfoByList(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String, Object>> grpNoList = apprStrMtableInfoMapper.selectGrpNosByQueryModel(queryModel);
        PageInfo<Map<String, Object>> pageinfo = new PageInfo<>(grpNoList);
        int total = (int)pageinfo.getTotal();
        PageHelper.clearPage();
        //查询集团客户编号列表
        logger.info("集团列表集团客户信息展示----{}", JSON.toJSONString(grpNoList));
        List<Map<String, Object>> mapList = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(grpNoList)){
            List<String> grpNos = grpNoList.stream().map(e -> (String) e.get("grpNo")).distinct().collect(Collectors.toList());
            mapList = apprStrMtableInfoMapper.selectGrpStrInfoByGrpNos(grpNos);
            mapList.forEach(grpRecord->{
                grpRecord.put("grpName", cusGrpService.selectGrpNameByGrpNo((String) grpRecord.get("grpNo"))) ;
            });
        }

        ResultDto resultDto = new ResultDto();
        resultDto.setData(mapList);
        resultDto.setTotal(total);
        PageHelper.clearPage();
        return resultDto;
    }

    /**
     * @param queryModel
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     * @date 2021/5/3
     * @version 1.0.0
     * @desc    集团客户额度视图列表信息
     * @修改历史: zhangjw
     */
    public List<Map<String, Object>> queryStrChgAppBySerno(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String, Object>> mapList = apprStrMtableInfoMapper.queryStrChgAppBySerno(queryModel);
        PageHelper.clearPage();
        return mapList;
    }

    /**
     * @方法名称:
     * @方法描述: 更具批复编号，获取批复信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ApprStrMtableInfo getAppStrMTableInfoBySerno(String apprSerno) {
        QueryModel queryModel = new QueryModel() ;
        //批复编号
        queryModel.addCondition("apprSerno", apprSerno);
        //查询批复信息
        List<ApprStrMtableInfo> apprStrMtableInfoList =  apprStrMtableInfoMapper.selectByModel(queryModel) ;
        //批复是否为空
        if(CollectionUtils.isNotEmpty(apprStrMtableInfoList)){
            return apprStrMtableInfoList.get(0) ;
        }else{
            return null ;
        }
    }


    /**
     * @方法名称: valiApprIsExistsBySerno
     * @方法描述: 判断是否存在批复信息
     * @参数与返回说明: 传入批复编号，存在返回true 不存在返回false
     * @算法描述: 无
     */

    public boolean valiApprIsExistsBySerno(String serno) {
        QueryModel queryModel = new QueryModel() ;
        //批复编号
        queryModel.addCondition("apprSerno", serno);
        //查询批复信息
        List<ApprStrMtableInfo> apprStrMtableInfoList =  apprStrMtableInfoMapper.selectByModel(queryModel) ;
        //批复是否为空
        if(CollectionUtils.isNotEmpty(apprStrMtableInfoList)){
            return true ;
        }else{
            return false ;
        }
    }


    /**
     * @param queryModel
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @date 2021/4/25
     * @version 1.0.0
     * @desc	单一客户额度视图-关联人授信列表（关联个人/关联法人）
     * @修改历史: zhangjw creadit
     */
    public List<Map<String,Object>> selectRelCusApprLmtByCusId(QueryModel queryModel) {
        //结果列表
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>() ;
        //获取主客户类型（1对私/2对公）
        String cusType = (String)queryModel.getCondition().get("cusType");
        //获取关联目标客户类型（1对私/2对公）
        String relDiff = (String)queryModel.getCondition().get("relCusType");
        //用于存储关联客户汇总列表
        List<Map<String,Object>> relCusList = new ArrayList<>();

        String cusId = (String) queryModel.getCondition().get("cusId");
        //根据主客户类型判断调用接口类别
        if(CmisLmtConstants.STD_ZB_CUS_CATALOG2.equals(cusType)){
            //调用客户管理API，获取CUS_ID法人关联客户列表
            CmisCus0003ReqDto reqDto = new CmisCus0003ReqDto();
            reqDto.setCusId(cusId);
            ResultDto<CmisCus0003RespDto> resultDto = cmisCusClientService.cmiscus0003(reqDto);
            List<CmisCus0003ComCusRelListRespDto> comCusRelList = resultDto.getData().getComCusRelList();

            if(comCusRelList!=null && comCusRelList.size()>0){

                for (CmisCus0003ComCusRelListRespDto cusRel : comCusRelList) {
                    Map<String,Object> map = new HashMap<String,Object>();
                    map.put("cusId",cusRel.getCusIdRel());
                    map.put("relType",cusRel.getRelType());
                    map.put("relCusType",cusRel.getCusTypeRel());
                    relCusList.add(map);
                }
            }

        }else{
            //调用客户管理API，获取CUS_ID个人关联客户列表
            CmisCus0004ReqDto reqDto = new CmisCus0004ReqDto();
            reqDto.setCusId(cusId);
            ResultDto<CmisCus0004RespDto> resultDto = cmisCusClientService.cmiscus0004(reqDto);
            List<CmisCus0004IndivCusRelListRespDto> indivCusRelList = resultDto.getData().getIndivCusRelList();

            if (indivCusRelList!=null && indivCusRelList.size()>0){

                for (CmisCus0004IndivCusRelListRespDto cusRel : indivCusRelList) {
                    Map<String,Object> map = new HashMap<String,Object>();
                    map.put("cusId",cusRel.getCusIdRel());
                    map.put("relType",cusRel.getRelType());
                    map.put("relCusType",cusRel.getCusTypeRel());
                    relCusList.add(map);
                }
            }

        }

        //关联人列表逐条获取信息，并根据关联客户类型剔除不需要客户类型
        if(relCusList !=null && relCusList.size()>0){
            for(int i=0;i<relCusList.size();i++){
                Map<String,Object> relCusIndivMap = relCusList.get(i);
                String relCusType = (String)relCusIndivMap.get("relCusType");//关联人客户大类  1  2
                //若筛选关联人客户类型不为空，则判断是否一致，若不一致则不纳入获取范围
                //relDiff ：  2 --对公  1-- 对私    relCusType：1--对公 2--个人
                if(relDiff!=null && !relDiff.equals(relCusType)){
                    continue;
                }
                String relType = (String)relCusIndivMap.get("relType");//关联人类型

                QueryModel selectQueryModel = new QueryModel();
                selectQueryModel.addCondition("cusId",(String)relCusIndivMap.get("cusId"));
                selectQueryModel.addCondition("instuCde",queryModel.getCondition().get("instuCde"));
                List<Map<String, Object>> resultList = apprStrMtableInfoMapper.selectStrInfoByList(selectQueryModel);

                //计算授信总额、授信可用金额、敞口可用金额
                if (resultList.size() > 0) {
                    for (Map<String, Object> result : resultList) {
                        BigDecimal lmtAmtAdd = BigDecimalUtil.replaceNull(result.get("lmtAmtAdd"));
                        BigDecimal avlAmt = BigDecimalUtil.replaceNull(result.get("avlAmt"));
                        // 授信总额
                        BigDecimal totalAmt = lmtAmtAdd.add(avlAmt);
                        result.put("totalAmt",totalAmt);
                        // 授信可用金额
                        BigDecimal totalValAmt = totalAmt.subtract(BigDecimalUtil.replaceNull(result.get("totalUseAmt")));
                        result.put("totalValAmt",totalValAmt);
                        // 敞口可用金额
                        BigDecimal totalSpacValAmt = BigDecimalUtil.replaceNull(result.get("totalSpacAmt")).subtract(BigDecimalUtil.replaceNull(result.get("totalSpacUseAmt")));
                        result.put("totalSpacValAmt",totalSpacValAmt);
                    }
                }

                if(resultList!=null && resultList.size()>0){
                    Map<String,Object> resultMap = resultList.get(0);
                    resultMap.put("relType",relType);
                    mapList.add(resultMap);
                }
            }
        }
        return mapList;
    }

    /**
     * @param queryModel
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     * @date 2021/4/26 15:25
     * @version 1.0.0
     * @desc    根据当前金融机构号和客户号查询客户并表额度汇总列表
     * @修改历史: zhangjw credate
     */
    public List<Map<String, Object>> selectStuCdeCusLmtByCusId(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String, Object>> mapList = apprStrMtableInfoMapper.selectStuCdeCusLmtByCusId(queryModel);
        if (mapList.size() > 0) {
            for (Map<String, Object> map : mapList) {
                BigDecimal lmtAmtAdd = BigDecimalUtil.replaceNull(map.get("lmtAmtAdd"));
                BigDecimal avlAmt = BigDecimalUtil.replaceNull(map.get("avlAmt"));
                // 授信总额
                BigDecimal creditTotal = lmtAmtAdd.add(avlAmt);
                map.put("creditTotal",creditTotal);
                // 授信可用余额
                BigDecimal availableTotal = creditTotal.subtract(BigDecimalUtil.replaceNull(map.get("outstndAmt")));
                map.put("availableTotal",availableTotal);
                // 敞口可用余额
                BigDecimal spacTotal = BigDecimalUtil.replaceNull(map.get("spacAmt")).subtract(BigDecimalUtil.replaceNull(map.get("spacOutstndAmt")));
                map.put("spacTotal",spacTotal);
            }
        }
        PageHelper.clearPage();
        return mapList;
    }

    /**
     * @方法名称: selectApprStrMtableInfoBySerno
     * @方法描述: 根据批复编号获取同业批复信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ApprStrMtableInfo selectApprStrMtableInfoBySerno(String apprSerno) {
        QueryModel queryModel = new QueryModel() ;
        //批复编号
        queryModel.addCondition("apprSerno", apprSerno);
        //查询批复信息
        List<ApprStrMtableInfo> apprStrMtableInfoList = apprStrMtableInfoMapper.selectByModel(queryModel) ;
        //如果不为空，返回批复信息
        if(CollectionUtils.isNotEmpty(apprStrMtableInfoList)){
            return apprStrMtableInfoList.get(0) ;
        }
        return null ;
    }


    /**
     * @param queryModel
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @date 2021/4/23 10:00
     * @version 1.0.0
     * @desc	根据客户号查询客户额度视图详情汇总信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<Map<String,Object> >selectAssSumByCusId(QueryModel queryModel) {
        logger.info("************************** 查询条件：【{}】", JSON.toJSONString(queryModel));

        String cusId = (String) queryModel.getCondition().get("cusId");
        //根据主客户号获取主客户对应的额度汇总信息
        String cusType = (String)queryModel.getCondition().get("cusType");

        logger.info("获取客户"+cusId+"关联额度客户类型【cusType】{}汇总开始------------->start", cusType);

        List<Map<String,Object>> mapList;
        //queryModel（查询条件）： cusId、instuCde、cusType

        if(CmisLmtConstants.STD_ZB_CUS_CATALOG3.equals(cusType)) {
            //同业客户
            mapList = selectSigStrInfoNoPageByQueryModel(queryModel);
        }else if (CmisLmtConstants.STD_ZB_CUS_CATALOG4.equals(cusType)){
            //集团客户
            List<String> grpNos = new ArrayList<>();
            grpNos.add(cusId);
            mapList = apprStrMtableInfoMapper.selectGrpStrInfoByGrpNos(grpNos);
        }else {
            mapList = apprStrMtableInfoMapper.selectStrInfoByBbList(queryModel);
            //计算授信总额、授信可用金额、敞口可用金额
            if (mapList.size() > 0) {
                for (Map<String, Object> result : mapList) {
                    BigDecimal lmtAmtAdd = BigDecimalUtil.replaceNull(result.get("lmtAmtAdd"));
                    BigDecimal avlAmt = BigDecimalUtil.replaceNull(result.get("avlAmt"));
                    // 授信总额
                    BigDecimal totalAmt = lmtAmtAdd.add(avlAmt);
                    result.put("totalAmt",totalAmt);
                    // 授信可用金额
                    BigDecimal totalValAmt = totalAmt.subtract(BigDecimalUtil.replaceNull(result.get("totalUseAmt")));
                    result.put("totalValAmt",totalValAmt);
                    // 敞口可用金额
                    BigDecimal totalSpacValAmt = BigDecimalUtil.replaceNull(result.get("totalSpacAmt")).subtract(BigDecimalUtil.replaceNull(result.get("totalSpacUseAmt")));
                    result.put("totalSpacValAmt",totalSpacValAmt);
                }
            }
        }

        logger.info("额度主客户【"+cusId+"】额度信息mapList："+ mapList );

        if (mapList==null || mapList.size()<=0){
            return null;
        }

//        //获取额度视图授信主体客户类型  1-对私  2-对公  3-同业 4-集团
//        if(StringUtils.isBlank(cusType)){
//            cusType = mapList.get(0).get("cusType").toString() ;
//        }

        //根据主客户号/主客户类型获取关联客户列表
        List relCusList = new ArrayList();

        if(CmisLmtConstants.STD_ZB_CUS_CATALOG2.equals(cusType)){
            //获取法人客户关联客户汇总列表
            relCusList = getComCusRelList(relCusList,cusId);
        }else if(CmisLmtConstants.STD_ZB_CUS_CATALOG1.equals(cusType)){
            //获取个人客户关联客户汇总
            relCusList = getIndivCusRelList(relCusList,cusId);
        }else if (CmisLmtConstants.STD_ZB_CUS_CATALOG4.equals(cusType)){
            //获取集团成员信息 TODO
            List<Map<String, Object>> cusIdList = cusGrpMemberRelService.selectCusIdsByGrpNo(cusId);

            if(CollectionUtils.isNotEmpty(cusIdList)){
                for (Map<String, Object> cusIdMap : cusIdList) {
                    //客户大类
                    String cusCatalog = (String) cusIdMap.get("cusCatalog");

                    if(CmisLmtConstants.STD_ZB_CUS_CATALOG1.equals(cusCatalog)){
                        //获取个人客户关联客户汇总
                        relCusList = getIndivCusRelList(relCusList,(String) cusIdMap.get("cusId"));
                    }else if (CmisLmtConstants.STD_ZB_CUS_CATALOG2.equals(cusCatalog)){
                        //获取法人客户关联客户汇总列表
                        relCusList = getComCusRelList(relCusList,(String) cusIdMap.get("cusId"));
                    }
                }
            }
        }

        logger.info("***额度主客户【"+cusId+"】关联客户：{}", JSON.toJSONString(relCusList));

        BigDecimal relTotalAmt = BigDecimal.ZERO;//关联汇总：授信总额
        BigDecimal relTotalUseAmt = BigDecimal.ZERO;//关联汇总：授信总额已用
        BigDecimal relTotalValAmt = BigDecimal.ZERO;//关联汇总：授信总额可用

        BigDecimal relSpacAmt = BigDecimal.ZERO;//关联汇总：授信敞口
        BigDecimal relSpacUseAmt = BigDecimal.ZERO;//关联汇总：授信敞口已用
        BigDecimal relSpacValAmt = BigDecimal.ZERO;//关联汇总：授信敞口可用

        //循环获取关联客户额度信息
        if(relCusList!=null && relCusList.size()>0){
            for(int i=0;i<relCusList.size();i++){
                Map<String,String> relCusMap = (Map<String,String>)relCusList.get(i);
                String relCusId = relCusMap.get("relCusId");
                if(StringUtils.isBlank(relCusId)) continue;

                List<Map<String,Object>> relMapList;
                QueryModel relQueryModel = new QueryModel();
                relQueryModel.addCondition("cusId",relCusId);
                relQueryModel.addCondition("instuCde",queryModel.getCondition().get("instuCde"));
                relMapList = apprStrMtableInfoMapper.selectStrInfoByList(relQueryModel);

                //计算授信总额、授信可用金额、敞口可用金额
                if (relMapList.size() > 0) {
                    for (Map<String, Object> result : relMapList) {
                        BigDecimal lmtAmtAdd = BigDecimalUtil.replaceNull(result.get("lmtAmtAdd"));
                        BigDecimal avlAmt = BigDecimalUtil.replaceNull(result.get("avlAmt"));
                        // 授信总额
                        BigDecimal totalAmt = lmtAmtAdd.add(avlAmt);
                        result.put("totalAmt",totalAmt);
                        // 授信可用金额
                        BigDecimal totalValAmt = totalAmt.subtract(BigDecimalUtil.replaceNull(result.get("totalUseAmt")));
                        result.put("totalValAmt",totalValAmt);
                        // 敞口可用金额
                        BigDecimal totalSpacValAmt = BigDecimalUtil.replaceNull(result.get("totalSpacAmt")).subtract(BigDecimalUtil.replaceNull(result.get("totalSpacUseAmt")));
                        result.put("totalSpacValAmt",totalSpacValAmt);
                    }
                }
                logger.info("额度主客户【"+cusId+"】的关联客户【"+relCusId+"】额度信息："+ relMapList );

                Map<String,Object> relMap;
                if(relMapList!=null && relMapList.size()>0){
                    relMap = relMapList.get(0);
                    relTotalAmt = relTotalAmt.add((BigDecimal) relMap.get("totalAmt"));
                    relTotalUseAmt = relTotalUseAmt.add((BigDecimal) relMap.get("totalUseAmt"));
                    relTotalValAmt = relTotalValAmt.add((BigDecimal) relMap.get("totalValAmt"));
                    relSpacAmt = relSpacAmt.add((BigDecimal) relMap.get("totalSpacAmt"));
                    relSpacUseAmt = relSpacUseAmt.add((BigDecimal) relMap.get("totalSpacUseAmt"));
                    relSpacValAmt = relSpacValAmt.add((BigDecimal) relMap.get("totalSpacValAmt"));
                }
            }

            logger.info("额度主客户【"+cusId+"】的关联客户额度汇总信息：relTotalAmt【"+ relTotalAmt +"】,relTotalUseAmt【"+relTotalUseAmt+"】,relTotalValAmt【"+relTotalValAmt+"】" +
                    ",relSpacAmt【"+relSpacAmt+"】,relSpacUseAmt【"+relSpacUseAmt+"】,relSpacValAmt【"+relSpacValAmt+"】" );
        }

        //获取含关联客户额度汇总数据
        Map<String,Object> map = mapList.get(0);

        List<Map<String,Object>> mapLmtList = new ArrayList<>();
        // 授信总额
        BigDecimal totalamt = BigDecimalUtil.replaceNull(map.get("totalAmt"));
        // 授信总额已用
        BigDecimal totaluseamt = BigDecimalUtil.replaceNull(map.get("totalUseAmt"));
        // 授信总额可用
        BigDecimal totalvalamt;

        if(CmisLmtConstants.STD_ZB_CUS_CATALOG3.equals(cusType)){
            totalvalamt = BigDecimalUtil.replaceNull(map.get("totalAvlAmt"));
        }else{
            totalvalamt = BigDecimalUtil.replaceNull(map.get("totalValAmt"));
        }

        relTotalAmt = relTotalAmt.add(totalamt);
        relTotalUseAmt = relTotalUseAmt.add(totaluseamt);

        if(CmisLmtConstants.STD_ZB_CUS_CATALOG3.equals(cusType)){
            //同业客户授信敞口为授信总额，授信敞口已用为授信总额已用，授信敞口可用为授信总额可用
            relTotalValAmt = relTotalValAmt.add(totalvalamt);
            relSpacAmt = relSpacAmt.add(totalamt);
            relSpacUseAmt = relSpacUseAmt.add(totaluseamt);
            relSpacValAmt = relSpacValAmt.add(totalvalamt);
        }else{
            relTotalValAmt = relTotalValAmt.add(totalvalamt);
            relSpacAmt = relSpacAmt.add((BigDecimal) map.get("totalSpacAmt"));
            relSpacUseAmt = relSpacUseAmt.add((BigDecimal) map.get("totalSpacUseAmt"));
            relSpacValAmt = relSpacValAmt.add((BigDecimal) map.get("totalSpacValAmt"));
        }

        // 手动拼接需要返回的形式
        Map<String, Object> mapOut1 = getMap("授信总额", totalamt, relTotalAmt);
        Map<String, Object> mapOut2 = getMap("授信总额已用", totaluseamt, relTotalUseAmt);
        Map<String, Object> mapOut3 = getMap("授信总额可用", totalvalamt, relTotalValAmt);

        mapLmtList.add(mapOut1);
        mapLmtList.add(mapOut2);
        mapLmtList.add(mapOut3);

        if(!CmisLmtConstants.STD_ZB_CUS_CATALOG3.equals(cusType)){
            // 授信敞口
            BigDecimal totalspacamt = BigDecimalUtil.replaceNull(map.get("totalSpacAmt"));
            // 授信敞口已用
            BigDecimal totalspacuseamt = BigDecimalUtil.replaceNull(map.get("totalSpacUseAmt"));
            // 授信敞口可用
            BigDecimal totalspacvalamt = BigDecimalUtil.replaceNull(map.get("totalSpacValAmt"));

            Map<String, Object> mapOut4 = getMap("授信敞口", totalspacamt, relSpacAmt);
            Map<String, Object> mapOut5 = getMap("授信敞口已用", totalspacuseamt, relSpacUseAmt);
            Map<String, Object> mapOut6 = getMap("授信敞口可用", totalspacvalamt, relSpacValAmt);
            mapLmtList.add(mapOut4);
            mapLmtList.add(mapOut5);
            mapLmtList.add(mapOut6);
        }

        logger.info("额度主客户【"+cusId+"】的含关联客户额度汇总信息：" + mapLmtList);
        return mapLmtList;
    }


    private Map<String, Object> getMap(String name, BigDecimal value1, BigDecimal value2) {
        Map<String, Object> map = new HashMap<>();
        map.put("name",name);
        map.put("noAssSum",value1);
        map.put("assSum",value2);
        return map;
    }



    /**
     * @方法名称: selectByAppSerno
     * @方法描述: 根据批复台账查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ApprStrMtableInfo selectByAppSerno(String apprSerno) {
        return apprStrMtableInfoMapper.selectByAppSerno(apprSerno);
    }

    /**
     * @方法名称: selectCusInfoListByReqDto
     * @方法描述: 根据reqDto查询客户类型及客户编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<Map<String,String>> selectCusInfoListByReqDto(CmisLmt0015ReqDto reqDto) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("instuCde",reqDto.getInstuCde());
        queryModel.addCondition("cusId",reqDto.getCusId());
        queryModel.addCondition("cusName", reqDto.getCusName());
        queryModel.addCondition("cusType", reqDto.getCusType());
        queryModel.addCondition("startNum", reqDto.getStartNum());
        queryModel.addCondition("pageCount", reqDto.getPageCount());

        return apprStrMtableInfoMapper.selectCusInfoListByQueryModel(queryModel);
    }

    /**
     * @方法名称: selectGrpApprSernoInfoByReqDto
     * @方法描述: 根据ReqDto查询集团客户的批复台账编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<Map<String,String>> selectGrpApprSernoInfoByReqDto(CmisLmt0015ReqDto reqDto) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("instuCde",reqDto.getInstuCde());
        queryModel.addCondition("cusId",reqDto.getCusId());
        queryModel.addCondition("cusName", reqDto.getCusName());
        queryModel.addCondition("startNum", reqDto.getStartNum());
        queryModel.addCondition("pageCount", reqDto.getPageCount());
        return apprStrMtableInfoMapper.selectGrpApprSernoInfoByQueryModel(queryModel);
    }

    /**
     * 根据客户列表查询法人及个人客户额度台账信息
     * @param cusIdList
     * @return
     */
    public List<Map<String,Object>> selectLawAndPersonLmtAccListByCusIdList(List<String> cusIdList){
        return apprStrMtableInfoMapper.selectLawAndPersonLmtAccListByCusIdList(cusIdList);
    }

    /**
     * 根据客户查询，判断改客户是否数据集团
     * @param cusId
     * @return
     */
    public Map<String, String> selectCusIsGrpMemCus(String cusId){
        HashMap<String, String> resultMap = new HashMap();
        String rtnCode = EclEnum.LMT_CTR_DEF_SUCCESS.key;
        String rtnMsg = EclEnum.LMT_CTR_DEF_SUCCESS.value;
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("cusId", cusId);

        //查询该客户是否集团成员
        List<CusGrpMemberRel> cusGrpMemberRels = cusGrpMemberRelService.selectAll(queryModel);

        if(CollectionUtils.isNotEmpty(cusGrpMemberRels)){
            rtnCode = EclEnum.ECL070091.key;
            rtnMsg = EclEnum.ECL070091.value;
            resultMap.put("rtnCode", rtnCode);
            resultMap.put("rtnMsg", rtnMsg);
            return resultMap ;
        }

//        Map<String, String> selParameter = apprStrMtableInfoMapper.selectCusIsGrpMemCus(queryModel);
//
//        if(selParameter == null || StringUtils.isEmpty(selParameter.get("cusId"))){
//            return resultMap ;
//        }

        resultMap.put("rtnCode", rtnCode);
        resultMap.put("rtnMsg", rtnMsg);
        return resultMap ;
    }

    /**
     * 根据查询条件查询集团客户额度列表-有效的-对应的子成员客户编号
     * @param model
     * @return
     */
    public List<Map<String,Object>> queryGrpLmtMemberListStatus(QueryModel model){
        return apprStrMtableInfoMapper.queryGrpLmtMemberListStatus(model);
    }
    /**
     * @return List<Map<String,Object>>
     * @date 2021/5、15 15:25
     * @version 1.0.0
     * @desc    根据客户号和法人机构号获取客户额度视图信息
     * @修改历史: zhangjw
     */
    public List<Map<String, Object>> queryLmtInfoByCusId(QueryModel queryModel) {
        List<Map<String, Object>> mapList = apprStrMtableInfoMapper.queryLmtInfoByCusId(queryModel);
        return mapList;
    }

    /**
     * @方法名称: queryGrpMemberLmtInfoList
     * @方法描述: 根据集团编号和金融机构代码，查询集团成员额度视图列表
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<Map<String,Object>> queryGrpMemberLmtInfoList(QueryModel model) {
        List<Map<String,Object>> resultList = new ArrayList<>();
        //集团编号
        String grpNo = (String) model.getCondition().get("cusId");

        //获取集团额度批复项下对应的集团成员客户号
        List<Map<String,Object>> cusIdList = cusGrpMemberRelService.selectCusIdsByGrpNo(grpNo);

        //循环获取额度编号下的成员客户编号，获取集团客户额度信息
        if(CollectionUtils.isNotEmpty(cusIdList)){
            QueryModel memberModel = new QueryModel();

            List<String> cusIds = new ArrayList<>();

            for (Map<String, Object> cusIdMap : cusIdList) {
                cusIds.add((String) cusIdMap.get("cusId"));
            }

            memberModel.addCondition("cusIds",cusIds);
            memberModel.addCondition("instuCde",model.getCondition().get("instuCde"));

            resultList = queryLmtInfoByCusId(memberModel);
        }
        return resultList;
    }

    /**
     * 查询同业客户额度信息（不分页）
     * @param queryModel
     * @return
     */
    public List<Map<String,Object>> selectSigStrInfoNoPageByQueryModel(QueryModel queryModel){
        List<Map<String,String>> sigCusList = apprStrMtableInfoMapper.selectSigCusIdListByQueryModel(queryModel);
        List<Map<String, Object>> mapList = new ArrayList<>();

        if (sigCusList!=null && sigCusList.size()>0){
            List<String> cusIdList = new ArrayList<>();

            for (Map<String, String> sigCus : sigCusList) {
                cusIdList.add(sigCus.get("cusId"));
            }

            mapList = apprStrMtableInfoMapper.selectSigStrInfoByList(cusIdList);
        }

        //将客户号相同的记录里的金额进行累加
        if (mapList!=null && mapList.size()>0){
            Map<String,Map<String, Object>> map = new HashMap<>();

            for (Map<String, Object> sigInfo : mapList) {
                String cusId = (String) sigInfo.get("cusId");

                if (map.containsKey(cusId)){
                    Map<String, Object> value = map.get(cusId);

                    BigDecimal totalAmt = (BigDecimal) value.get("totalAmt");
                    BigDecimal totalUseAmt = (BigDecimal) value.get("totalUseAmt");
                    BigDecimal totalAvlAmt = (BigDecimal) value.get("totalAvlAmt");
                    BigDecimal totalPrdAmt = (BigDecimal) value.get("totalPrdAmt");
                    BigDecimal totalPrdUseAmt = (BigDecimal) value.get("totalPrdUseAmt");
                    BigDecimal totalPrdAvlAmt = (BigDecimal) value.get("totalPrdAvlAmt");

                    totalAmt = totalAmt.add((BigDecimal) sigInfo.get("totalAmt"));
                    totalUseAmt = totalUseAmt.add((BigDecimal) sigInfo.get("totalUseAmt"));
                    totalAvlAmt = totalAvlAmt.add((BigDecimal) sigInfo.get("totalAvlAmt"));
                    totalPrdAmt = totalPrdAmt.add((BigDecimal) sigInfo.get("totalPrdAmt"));
                    totalPrdUseAmt = totalPrdUseAmt.add((BigDecimal) sigInfo.get("totalPrdUseAmt"));
                    totalPrdAvlAmt = totalPrdAvlAmt.add((BigDecimal) sigInfo.get("totalPrdAvlAmt"));

                    value.put("totalAmt",totalAmt);
                    value.put("totalUseAmt",totalUseAmt);
                    value.put("totalAvlAmt",totalAvlAmt);
                    value.put("totalPrdAmt",totalPrdAmt);
                    value.put("totalPrdUseAmt",totalPrdUseAmt);
                    value.put("totalPrdAvlAmt",totalPrdAvlAmt);
                    map.put(cusId,value);
                }else{
                    map.put(cusId,sigInfo);
                }
            }

            //清空mapList,将map里的value放入mapList里
            mapList.clear();
            Set<Map.Entry<String,Map<String, Object> >> entries = map.entrySet();

            for (Map.Entry<String, Map<String, Object>> entry : entries) {
                mapList.add(entry.getValue());
            }
        }
        return mapList;
    }

    /**
     * 根据客户编号列表查询同业客户额度信息
     * @param cusIdList
     * @return
     */
    public List<Map<String,Object>> selectSigStrInfoNoPageByCusIdList(List<String> cusIdList){
        List<Map<String, Object>> mapList = apprStrMtableInfoMapper.selectSigStrInfoByList(cusIdList);

        //将客户号相同的记录里的金额进行累加
        if (mapList!=null && mapList.size()>0){
            Map<String,Map<String, Object>> map = new HashMap<>();

            for (Map<String, Object> sigInfo : mapList) {
                String cusId = (String) sigInfo.get("cusId");

                if (map.containsKey(cusId)){
                    Map<String, Object> value = map.get(cusId);

                    BigDecimal totalAmt = (BigDecimal) value.get("totalAmt");
                    BigDecimal totalUseAmt = (BigDecimal) value.get("totalUseAmt");
                    BigDecimal totalAvlAmt = (BigDecimal) value.get("totalAvlAmt");
                    BigDecimal totalPrdAmt = (BigDecimal) value.get("totalPrdAmt");
                    BigDecimal totalPrdUseAmt = (BigDecimal) value.get("totalPrdUseAmt");
                    BigDecimal totalPrdAvlAmt = (BigDecimal) value.get("totalPrdAvlAmt");

                    totalAmt = totalAmt.add((BigDecimal) sigInfo.get("totalAmt"));
                    totalUseAmt = totalUseAmt.add((BigDecimal) sigInfo.get("totalUseAmt"));
                    totalAvlAmt = totalAvlAmt.add((BigDecimal) sigInfo.get("totalAvlAmt"));
                    totalPrdAmt = totalPrdAmt.add((BigDecimal) sigInfo.get("totalPrdAmt"));
                    totalPrdUseAmt = totalPrdUseAmt.add((BigDecimal) sigInfo.get("totalPrdUseAmt"));
                    totalPrdAvlAmt = totalPrdAvlAmt.add((BigDecimal) sigInfo.get("totalPrdAvlAmt"));

                    value.put("avlAmt",totalAmt);
                    value.put("avlOutstndAmt",totalUseAmt);
                    value.put("avlAvailAmt",totalAvlAmt);
                    value.put("spacAmt",totalPrdAmt);
                    value.put("spacOutstndAmt",totalPrdUseAmt);
                    value.put("spacAvailAmt",totalPrdAvlAmt);
                    map.put(cusId,value);
                }else{
                    map.put(cusId,sigInfo);
                }
            }

            //清空mapList,将map里的value放入mapList里
            mapList.clear();
            Set<Map.Entry<String,Map<String, Object> >> entries = map.entrySet();

            for (Map.Entry<String, Map<String, Object>> entry : entries) {
                mapList.add(entry.getValue());
            }
        }
        return mapList;
    }

    /**
     * 查询客户名下有效标准化资产授信（未到期）金额之和
     * @param cusId
     * @return
     */
    public BigDecimal selectTotalAssetLmtAmtByCusId(String cusId){
        return apprStrMtableInfoMapper.selectTotalAssetLmtAmtByCusId(cusId);
    }

    /**
     * 查询客户名下标准化资产余额（已到期部分）
     * @param cusId
     * @return
     */
    public BigDecimal selectTotalAssetBalanceAmtByCusId(String cusId){
        return apprStrMtableInfoMapper.selectTotalAssetBalanceAmtByCusId(cusId);
    }

    /**
     * 查询客户名下有效非标资产授信（未到期）金额之和
     * @param cusId
     * @return
     */
    public BigDecimal selectTotalAssetLmtAmtNonStandardByCusId(String cusId){
        return apprStrMtableInfoMapper.selectTotalAssetLmtAmtNonStandardByCusId(cusId);
    }

    /**
     * 查询客户名下非标资产余额（已到期部分）
     * @param cusId
     * @return
     */
    public BigDecimal selectTotalAssetBalanceAmtNonStandardByCusId(String cusId){
        return apprStrMtableInfoMapper.selectTotalAssetBalanceAmtNonStandardByCusId(cusId);
    }

    /**
     * 查询客户（综合授信+主体授信）未到期的授信金额
     * @param model
     * @return
     */
    public BigDecimal selectTotalLmtAmtByCusId(QueryModel model){
        return apprStrMtableInfoMapper.selectTotalLmtAmtByCusId(model);
    }

    /**
     * 查询客户（综合授信+主体授信）已到期的业务余额
     * @param model
     * @return
     */
    public BigDecimal selectTotalBalanceAmtByCusId(QueryModel model){
        return apprStrMtableInfoMapper.selectTotalBalanceAmtByCusId(model);
    }

    /**
     * 查询客户 委托贷款 未到期的授信金额
     * @param model
     * @return
     */
    public BigDecimal selectTotalWtLmtAmtByCusId(QueryModel model){
        return apprStrMtableInfoMapper.selectTotalWtLmtAmtByCusId(model);
    }

    /**
     * 查询客户 委托贷款 已到期的用信余额
     * @param model
     * @return
     */
    public BigDecimal selectTotalWtBalanceAmtByCusId(QueryModel model){
        return apprStrMtableInfoMapper.selectTotalWtBalanceAmtByCusId(model);
    }

    /**
     * 根据批复台账编号列表更新集团批复台账编号字段
     * @param queryModel
     * @return
     */
    public int updateGrpApprSernoByApprSernos(QueryModel queryModel){
        return apprStrMtableInfoMapper.updateGrpApprSernoByApprSernos(queryModel);
    }

    /**
     * 查询客户的低风险授信总额
     * @param cusIdList
     * @return
     */
    public List<Map<String,Object>> selectLriskLmtByCusIdList(List<java.lang.String> cusIdList){
        return apprStrMtableInfoMapper.selectLriskLmtByCusIdList(cusIdList);
    }

    /**
     * @作者:lizx
     * @方法名称: selectApprSubSernoListByApprSerno
     * @方法描述:  根据批复分项编号，查询该批复向下分项信息
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/1 20:18
     * @param apprSerno:
     * @return: java.util.List<java.lang.String>
     * @算法描述: 无
    */
    public List<String> selectApprSubSernoListByApprSerno(String apprSerno){
        return apprStrMtableInfoMapper.selectApprSubSernoListByApprSerno(apprSerno);
    }

    /**
     * @方法名称:createNewAccExtracted
     * @方法描述:如果是 以新批复覆盖老批复isCreateAcc：如果是则原批复编号必须存在，批复状态跟更改为99 失效已结清，如果存在未结清业务，则原业务挂到新批复下
     * 如果是 否以新批复覆盖老批复isCreateAcc：
     * 根据批复台账判断该批复是否已经存在，如果存在，进行变更操作，如果不存在则新增批复
     * @参数与返回说明:
     * @算法描述: 无
     */
    public void createNewAccExtracted(String origiAccNo, String accNo) {
        //原批复信息
        ApprStrMtableInfo oldApprStrMtableInfo = selectByAppSerno(origiAccNo) ;
        if(oldApprStrMtableInfo == null){
            throw new YuspException(EclEnum.ECL070072.key, "【"+ accNo +"】，"+EclEnum.ECL070072.value);
        }
        //原批复状态更改为失效已结清
        oldApprStrMtableInfo.setApprStatus(CmisLmtConstants.STD_ZB_APPR_ST_99);
        apprStrMtableInfoMapper.updateByPrimaryKey(oldApprStrMtableInfo) ;
        logger.info("CmisLmt0001 生成新批复，原批复编号：【"+origiAccNo+"】信息，更新为【99】失效已结清！") ;
    }

    /**
     * 根据批复台账编号获取授信模式
     * @param apprSerno
     * @return
     */
    public String selectLmtModeByApprSerno(String apprSerno){
        return apprStrMtableInfoMapper.selectLmtModeByApprSerno(apprSerno);
    }

    /**
     * @作者:zhangjw
     * @方法名称: queryApprStrZHLmt
     * @方法描述:  获取客户当前有效的综合授信额度
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/12
     * @param cusId:
     * @return: java.util.List<java.lang.String>
     * @算法描述: 无
     */
    public List<ApprStrMtableInfo> queryApprStrZHLmt(String cusId){
        QueryModel model = new QueryModel();
        model.addCondition("cusId", cusId);
        return apprStrMtableInfoMapper.queryApprStrZHLmt(model);
    }

    /**
     * 根据客户号查询该客户是否存在授信
     *
     * @param cusId
     * @return
     */
    public Map<String, String> selectByCusId(String cusId) {
        Map rtnData = new HashMap();
        String rtnCode = EclEnum.LMT_APPR_DEF_SUCCESS.key;
        String rtnMsg = EclEnum.LMT_APPR_DEF_SUCCESS.value;
        try {
            if(StringUtils.isBlank(cusId)) throw new BizException(null, "", null, "客户号不允许为空！");
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("cusId",cusId);
            queryModel.addCondition("oprType",CmisLmtConstants.OPR_TYPE_ADD);

            List<ApprStrMtableInfo> apprStrMtableInfos = selectAll(queryModel);

            if (apprStrMtableInfos!=null && apprStrMtableInfos.size()>0){
                //存在授信记录
                rtnCode = EclEnum.ECL070112.key;
                rtnMsg = EclEnum.ECL070112.value;
            }
        } catch (YuspException e) {
            logger.error("集团新增核心客户业务失败！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * 根据客户编号获取该客户的综合授信批复台账信息
     * @param cusId
     * @return
     */
    public ApprStrMtableInfo selectByGrpNoAndCusId(String cusId, String grpApprSerno) throws Exception {

        if(StringUtils.isBlank(grpApprSerno)){
            throw new Exception("集团批复编号不允许为空");
        }

        if(StringUtils.isBlank(cusId)){
            throw new Exception("集团成员客户号不允许为空");
        }

        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId",cusId);
        queryModel.addCondition("grpApprSerno", grpApprSerno);
        queryModel.addCondition("lmtMode",CmisLmtConstants.STD_ZB_LMT_MODE_01);
        queryModel.addCondition("oprType",CmisLmtConstants.OPR_TYPE_ADD);
        queryModel.addCondition("apprStatus",CmisLmtConstants.STD_ZB_APPR_ST_01);

        List<ApprStrMtableInfo> apprStrMtableInfos = selectByModel(queryModel);

        if (apprStrMtableInfos!=null && apprStrMtableInfos.size()>0) {
            return apprStrMtableInfos.get(0);
        }

        return null;
    }

    /**
     * 获取法人关联客户列表放入relCusList
     * @param relCusList
     * @param cusId
     * @return
     */
    public List getComCusRelList(List relCusList,String cusId){
        //调用客户管理API，获取CUS_ID法人关联客户列表
        CmisCus0003ReqDto reqDto = new CmisCus0003ReqDto();
        reqDto.setCusId(cusId);
        logger.info("查询cmiscus0003请求报文{}", JSON.toJSONString(reqDto));
        ResultDto<CmisCus0003RespDto> resultDto = cmisCusClientService.cmiscus0003(reqDto);
        logger.info("查询cmiscus0003响应报文{}", JSON.toJSONString(resultDto));
        List<CmisCus0003ComCusRelListRespDto> comCusRelList = resultDto.getData().getComCusRelList();

        if (CollectionUtils.isNotEmpty(comCusRelList)){
            for (CmisCus0003ComCusRelListRespDto cusRel : comCusRelList) {
                Map<String,Object> map = new HashMap<>();
                map.put("relCusId",cusRel.getCusIdRel());
                relCusList.add(map);
            }
        }

        return relCusList;
    }

    /**
     * 获取个人关联客户列表放入relCusList
     * @param relCusList
     * @param cusId
     * @return
     */
    public List getIndivCusRelList(List relCusList,String cusId){
        //调用客户管理API，获取CUS_ID个人关联客户列表
        CmisCus0004ReqDto reqDto = new CmisCus0004ReqDto();
        reqDto.setCusId(cusId);
        ResultDto<CmisCus0004RespDto> resultDto = cmisCusClientService.cmiscus0004(reqDto);
        List<CmisCus0004IndivCusRelListRespDto> indivCusRelList = resultDto.getData().getIndivCusRelList();

        if (indivCusRelList!=null && indivCusRelList.size()>0){

            for (CmisCus0004IndivCusRelListRespDto cusRel : indivCusRelList) {
                Map<String,Object> map = new HashMap<String,Object>();
                map.put("relCusId",cusRel.getCusIdRel());
                relCusList.add(map);
            }
        }

        return relCusList;
    }

    /**
     * @param queryModel
     * @return List<Map<String,Object>>
     * @date 2021/4/19 15:25
     * @version 1.0.0
     * @desc    法人客户额度视图信息
     * @修改历史: zhangjw
     */
    public ResultDto queryLmtListBymodel(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        //請求參數：managerId 主管客户经理、 apprStatus额度状态、cusId客户号、cusType客户类型、instueCode金融机构代码、cusName客户名称、djjdFlag冻结解冻标识、isGrp是否集团
        List<Map<String, Object>> cusIdList = apprStrMtableInfoMapper.queryLmtCusIdByModel(queryModel);
        PageHelper.clearPage();

        ResultDto LmtListResult = new ResultDto<>(cusIdList);
        long total = LmtListResult.getTotal();

        List<Map<String, Object>> mapList = new ArrayList<>();
        QueryModel lmtListModel = new QueryModel();
        if(cusIdList!=null && cusIdList.size()>0){
            String cusId = "";
            for(Map<String, Object> cusIds : cusIdList){
                cusId = cusId + (String)cusIds.get("cusId") + ",";
            }
            lmtListModel.addCondition("cusIds",cusId);

            String instuCde = queryModel.getCondition().get("instuCde") == null ? null : (String)queryModel.getCondition().get("instuCde");
            if(StringUtils.isNotBlank(instuCde)){
                lmtListModel.addCondition("instuCde",instuCde);
            }

            String noInstuCde = queryModel.getCondition().get("noInstuCde") == null ? null : (String)queryModel.getCondition().get("noInstuCde");
            if(StringUtils.isNotBlank(noInstuCde)){
                lmtListModel.addCondition("noInstuCde",noInstuCde);
            }

            mapList = apprStrMtableInfoMapper.queryLmtListByCusId(lmtListModel);
            if (mapList.size() > 0) {
                for (Map<String, Object> map : mapList) {
                    BigDecimal lmtAmtAdd = BigDecimalUtil.replaceNull(map.get("lmtAmtAdd"));
                    BigDecimal avlAmt = BigDecimalUtil.replaceNull(map.get("avlAmt"));
                    // 授信总额
                    BigDecimal totalAmt = lmtAmtAdd.add(avlAmt);
                    map.put("totalAmt",totalAmt);
                    // 授信可用金额
                    BigDecimal totalValAmt = totalAmt.subtract(BigDecimalUtil.replaceNull(map.get("totalUseAmt")));
                    map.put("totalValAmt",totalValAmt);
                    // 敞口可用金额
                    BigDecimal totalSpacValAmt = BigDecimalUtil.replaceNull(map.get("totalSpacAmt")).subtract(BigDecimalUtil.replaceNull(map.get("totalSpacUseAmt")));
                    map.put("totalSpacValAmt",totalSpacValAmt);
                }
            }
        }
        ResultDto resultDto = new ResultDto();
        resultDto.setData(mapList);
        resultDto.setTotal(total);
        return resultDto;
    }

    /**
     * 投后业务查询（贷后）
     * @param model
     * @return
     * @创建人：周茂伟
     */
    public List<ApprStrMtableInfoDto> selectForPsp(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ApprStrMtableInfoDto> list = apprStrMtableInfoMapper.selectForPsp(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @param queryModel
     * @return List<Map<String,Object>>
     * @date 2021/9/3
     * @version 1.0.0
     * @desc    法人个人客户额度视图列表（优化查询性能）
     * @修改历史: zhangjw
     */
    public ResultDto queryListForComIndivLmtList(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<String> cusList = apprStrMtableInfoMapper.queryListForCus(queryModel);
        PageInfo<String> pageinfo = new PageInfo<>(cusList);
        int total = (int)pageinfo.getTotal();
        PageHelper.clearPage();

        queryModel.addCondition("cusList",cusList);
        List<Map<String, Object>> mapList = apprStrMtableInfoMapper.selectStrInfoByList(queryModel);
        if (mapList.size() > 0) {
            for (Map<String, Object> map : mapList) {
                BigDecimal lmtAmtAdd = BigDecimalUtil.replaceNull(map.get("lmtAmtAdd"));
                BigDecimal avlAmt = BigDecimalUtil.replaceNull(map.get("avlAmt"));
                // 授信总额
                BigDecimal totalAmt = lmtAmtAdd.add(avlAmt);
                map.put("totalAmt",totalAmt);
                // 授信可用金额
                BigDecimal totalValAmt = totalAmt.subtract(BigDecimalUtil.replaceNull(map.get("totalUseAmt")));
                map.put("totalValAmt",totalValAmt);
                // 敞口可用金额
                BigDecimal totalSpacValAmt = BigDecimalUtil.replaceNull(map.get("totalSpacAmt")).subtract(BigDecimalUtil.replaceNull(map.get("totalSpacUseAmt")));
                map.put("totalSpacValAmt",totalSpacValAmt);
            }
        }

        ResultDto resultDto = new ResultDto();
        resultDto.setData(mapList);
        resultDto.setTotal(total);
        return resultDto;
    }

    /**
     * @param queryModel
     * @return List<Map<String,Object>>
     * @date 2021/9/3
     * @version 1.0.0
     * @desc    法人个人客户额度视图列表
     * @修改历史:
     */
    public ResultDto selectStrInfoByCusId(QueryModel queryModel) {
        List<Map<String, Object>> mapList = apprStrMtableInfoMapper.selectStrInfoByBbList(queryModel);;
        if (mapList.size() > 0) {
            for (Map<String, Object> map : mapList) {
                BigDecimal lmtAmtAdd = BigDecimalUtil.replaceNull(map.get("lmtAmtAdd"));
                BigDecimal avlAmt = BigDecimalUtil.replaceNull(map.get("avlAmt"));
                // 授信总额
                BigDecimal totalAmt = lmtAmtAdd.add(avlAmt);
                map.put("totalAmt",totalAmt);
                // 授信可用金额
                BigDecimal totalValAmt = totalAmt.subtract(BigDecimalUtil.replaceNull(map.get("totalUseAmt")));
                map.put("totalValAmt",totalValAmt);
                // 敞口可用金额
                BigDecimal totalSpacValAmt = BigDecimalUtil.replaceNull(map.get("totalSpacAmt")).subtract(BigDecimalUtil.replaceNull(map.get("totalSpacUseAmt")));
                map.put("totalSpacValAmt",totalSpacValAmt);
            }
        }

        ResultDto resultDto = new ResultDto();
        resultDto.setData(mapList);
        return resultDto;
    }

    /**
     * 查询客户（综合授信+主体授信）敞口用信余额，不包含穿透化额度、承销额度
     * @param model
     * @return
     * add by zhangjw 20210905
     */
    public BigDecimal selectLoanSpacBalanceAmtByCusId(QueryModel model){
        return apprStrMtableInfoMapper.selectLoanSpacBalanceAmtByCusId(model);
    }

    /**
     * 查询客户（综合授信+主体授信）敞口用信余额，不包含穿透化额度、承销额度
     * @param model
     * @return
     * add by zhangjw 20210905
     */
    public Map<String,Object> calLmtAntLoanSpacBalanceAmt(QueryModel model){
        return apprStrMtableInfoMapper.calLmtAntLoanSpacBalanceAmt(model);
    }

    /**
     * 查询客户（综合授信+主体授信）敞口用信余额，不包含穿透化额度、承销额度
     * @param model
     * @return
     * add by zhangjw 20210905
     */
    public Map<String,BigDecimal> selectAvlAmtByCusIdParam(QueryModel model){
        return apprStrMtableInfoMapper.selectAvlAmtByCusIdParam(model);
    }

    /**
     * 根据客户号和额度品种编号查找额度分项
     * @param model
     * @return
     */
    public List<Map<String,Object>> selectApprSernoByCusIdAndLimitSubNo(QueryModel model){
        return apprStrMtableInfoMapper.selectApprSernoByCusIdAndLimitSubNo(model);
    }



    public List<String> selectCusIdInfoByList(QueryModel model){
        return apprStrMtableInfoMapper.selectCusIdInfoByList(model);
    }

    /**
     * 根据客户号和客户类型，额度类型，金融机构代码查询批复主表信息，客户号必输
     * @param model
     * @return
     * add by  20211007
     */
    public ApprStrMtableInfo selectByParamCusId(QueryModel model){
        return apprStrMtableInfoMapper.selectByParamCusId(model);
    }

    public Integer selectPageCountByParam(QueryModel model){
        return apprStrMtableInfoMapper.selectPageCountByParam(model) ;
    }

    /**
     * @作者:lizx
     * @方法名称: updateByCusId
     * @方法描述:  根据客户号跟新批复主表中集团客户号字段
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/10/11 21:23
     * @param params:
     * @return: void
     * @算法描述: 无
    */
    public int updateGrpNoByCusId(Map params){
        return apprStrMtableInfoMapper.updateGrpNoByCusId(params) ;
    }

    /**
     * @作者:lizx
     * @方法名称: updateByCusId
     * @方法描述:  根据客户号跟新批复主表中集团客户号字段
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/10/11 21:23
     * @param model:
     * @return: void
     * @算法描述: 无
     */
    public List<Map<String,Object>> selectByApprApp(QueryModel model){
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String,Object>> list = apprStrMtableInfoMapper.selectByApprApp(model) ;
        PageHelper.clearPage();
        return list;
    }
}