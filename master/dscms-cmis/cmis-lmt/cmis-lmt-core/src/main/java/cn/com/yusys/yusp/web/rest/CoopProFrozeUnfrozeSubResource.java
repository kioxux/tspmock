/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.CoopProFrozeUnfrozeSubDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CoopProFrozeUnfrozeSub;
import cn.com.yusys.yusp.service.CoopProFrozeUnfrozeSubService;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: CoopProFrozeUnfrozeSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-04 10:25:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/coopprofrozeunfrozesub")
public class CoopProFrozeUnfrozeSubResource {
    @Autowired
    private CoopProFrozeUnfrozeSubService coopProFrozeUnfrozeSubService;

	/**
     * 全表查询.
     *
     * @return
     */
    @PostMapping("/query/all")
    protected ResultDto<List<CoopProFrozeUnfrozeSubDto>> query() {
        QueryModel queryModel = new QueryModel();
        List<CoopProFrozeUnfrozeSubDto> list  = coopProFrozeUnfrozeSubService.selectAll(queryModel);
        return new ResultDto<List<CoopProFrozeUnfrozeSubDto>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<CoopProFrozeUnfrozeSubDto>> index(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        List<CoopProFrozeUnfrozeSubDto> list = coopProFrozeUnfrozeSubService.selectByModel(queryModel);
        return new ResultDto<List<CoopProFrozeUnfrozeSubDto>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{pkId}")
    protected ResultDto<CoopProFrozeUnfrozeSub> show(@PathVariable("pkId") String pkId) {
        CoopProFrozeUnfrozeSub coopProFrozeUnfrozeSub = coopProFrozeUnfrozeSubService.selectByPrimaryKey(pkId);
        return new ResultDto<CoopProFrozeUnfrozeSub>(coopProFrozeUnfrozeSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<CoopProFrozeUnfrozeSub> create(@RequestBody CoopProFrozeUnfrozeSub coopProFrozeUnfrozeSub) throws URISyntaxException {
        coopProFrozeUnfrozeSubService.insert(coopProFrozeUnfrozeSub);
        return new ResultDto<CoopProFrozeUnfrozeSub>(coopProFrozeUnfrozeSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CoopProFrozeUnfrozeSub coopProFrozeUnfrozeSub) throws URISyntaxException {
        int result = coopProFrozeUnfrozeSubService.update(coopProFrozeUnfrozeSub);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/batchUpdate")
    protected ResultDto<Integer> update(@RequestBody List<CoopProFrozeUnfrozeSub> lists) throws URISyntaxException {
        lists.forEach(item->{
            coopProFrozeUnfrozeSubService.updateSelective(item);
        });
        return new ResultDto<Integer>(0);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = coopProFrozeUnfrozeSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = coopProFrozeUnfrozeSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
