/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtWhiteInfo;
import cn.com.yusys.yusp.domain.LmtWhiteInfoHistory;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtWhiteInfoHistoryMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: dumingdi
 * @创建时间: 2021-05-22 11:08:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtWhiteInfoHistoryMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    LmtWhiteInfoHistory selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtWhiteInfoHistory> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtWhiteInfoHistory record);

    /**
     * 批量插入.
     * @param lmtWhiteInfoHistoryList 不宜贷款客户列表
     * @return 结果
     */
    int batchInsert(List<LmtWhiteInfoHistory> lmtWhiteInfoHistoryList);

    /**
     * @方法名称: selectByIds
     * @方法描述: 根据多个主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtWhiteInfoHistory> selectByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectTotalLmtAmtByCusId
     * @方法描述: 根据客户id查询限额总额、已用限额总额和可用总额
     * @参数与返回说明: cusId - 客户编号
     * @算法描述: 无
     */

    Map<String, BigDecimal> selectTotalLmtAmtByCusId(@Param("cusId") String cusId);

    /**
     * 根据客户号查询白名单记录数
     * @param cusId
     * @return
     */
    int selectRecordsByCusId(@Param("cusId") String cusId);

    /**
     * 根据客户号查询产品户买入返售（信用债）白名单记录数
     * @param cusId
     * @return
     */
    int selectResaleRecordsByCusId(@Param("cusId") String cusId);
}