package cn.com.yusys.yusp.service.server.cmislmt0059;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0059.req.CmisLmt0059ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0059.resp.CmisLmt0059RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0059Service
 * @类描述: #对内服务类
 * @功能描述: 获取客户或分项明细用信综合总额和用信敞口余额（不包含合作方）
 * @创建时间: 2021-09-09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0059Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0059Service.class);

    @Autowired
    private Comm4Service comm4Service ;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;

    @Autowired
    private ContAccRelService contAccRelService ;

    /**
     * 获取客户或分项明细用信综合总额和用信敞口余额（不包含合作方）
     * add by lizx 2021-09-09
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0059RespDto execute(CmisLmt0059ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0059.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0059.value);
        CmisLmt0059RespDto respDto = new CmisLmt0059RespDto();
        try {

            //金融机构代码
            String instuCde = reqDto.getInstuCde() ;
            //客户号
            String cusId = reqDto.getCusId() ;
            //分项明细编号
            String apprSubSerno = reqDto.getApprSubSerno() ;

            /********************************* 根据客户号获取分项对应的用信总额和用信余额*****************/
            //根据客户号获取客户想下业务余额  TODO: 最高额协议，额度占用关系占用台账的算不算，我感觉不应该算？
            BigDecimal loanTotal = BigDecimal.ZERO ;
            BigDecimal loanBal = BigDecimal.ZERO;
            BigDecimal spacLoanBal = BigDecimal.ZERO ;
            BigDecimal subSpacLoanBal = BigDecimal.ZERO ;
            if(StringUtils.nonBlank(cusId)){
                QueryModel queryModel = new QueryModel() ;
                queryModel.addCondition("cusId", cusId);
                queryModel.addCondition("instuCde", instuCde);
                List<String> appList = apprLmtSubBasicInfoService.selectSubNosByCusId(queryModel) ;
                //分项字符串不为空
                if(CollectionUtils.nonEmpty(appList)){
                    String apprSubSernos = comm4Service.listToString(appList) ;
                    loanBal = contAccRelService.selectAccTotalBalByLimitSubNo(apprSubSernos) ;
                    loanTotal = contAccRelService.selectAccTotalByLimitSubNos(apprSubSernos) ;
                    spacLoanBal = contAccRelService.selectSpacAccBalByLimitSubNos(apprSubSernos) ;
                }
            }
            respDto.setLoanTotal(loanTotal);
            respDto.setLoanBal(loanBal);
            respDto.setSpacLoanBal(spacLoanBal);

            /********************************* 根据分项获取分项对应的用信总额和用信余额*****************/
            //根据分项编号，获取分项向下业务总额和业务余额
            BigDecimal subLoanTotal = BigDecimal.ZERO ;
            //分项用信余额
            BigDecimal subLoanBal = BigDecimal.ZERO ;
            if(StringUtils.nonBlank(apprSubSerno)){
                //根据分项编号
                subLoanBal = contAccRelService.selectAccTotalBalByLimitSubNo(apprSubSerno) ;
                subLoanTotal = contAccRelService.selectAccTotalByLimitSubNos(apprSubSerno) ;
                subSpacLoanBal = contAccRelService.selectSpacAccBalByLimitSubNos(apprSubSerno) ;
            }
            respDto.setSubLoanBal(subLoanBal);
            respDto.setSubLoanTotal(subLoanTotal);
            respDto.setSubSpacLoanBal(subSpacLoanBal);

            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("【cmislmt0059】获取客户或分项明细用信综合总额和用信敞口余额（不包含合作方）：" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0059.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0059.value);
        return respDto;
    }
}