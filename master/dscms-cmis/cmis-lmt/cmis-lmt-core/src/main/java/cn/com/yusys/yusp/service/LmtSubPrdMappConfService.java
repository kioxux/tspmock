/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.*;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.dto.CfgPrdCatalogDto;
import cn.com.yusys.yusp.dto.LmtConfStrMtableResDto;
import cn.com.yusys.yusp.dto.LmtSubPrdMappConfDto;
import cn.com.yusys.yusp.dto.LmtSubPrdMappConfListDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.PageHelper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtSubPrdMappConf;
import cn.com.yusys.yusp.repository.mapper.LmtSubPrdMappConfMapper;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtSubPrdMappConfService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-02 17:08:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSubPrdMappConfService {

    @Autowired
    private LmtSubPrdMappConfMapper lmtSubPrdMappConfMapper;
	@Resource
    private ICmisCfgClientService iCmisCfgClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public LmtSubPrdMappConf selectByPrimaryKey(String pkId) {
        return lmtSubPrdMappConfMapper.selectByPrimaryKey(pkId);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */
    @Transactional(readOnly=true)
    public List<LmtSubPrdMappConf> selectAll(QueryModel model) {
        List<LmtSubPrdMappConf> records = (List<LmtSubPrdMappConf>) lmtSubPrdMappConfMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSubPrdMappConf> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSubPrdMappConf> list = lmtSubPrdMappConfMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int insert(LmtSubPrdMappConf record) {
        return lmtSubPrdMappConfMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int insertSelective(LmtSubPrdMappConf record) {
        return lmtSubPrdMappConfMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int update(LmtSubPrdMappConf record) {
        return lmtSubPrdMappConfMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int updateSelective(LmtSubPrdMappConf record) {
        return lmtSubPrdMappConfMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return lmtSubPrdMappConfMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtSubPrdMappConfMapper.deleteByIds(ids);
    }
    
	public List<LmtSubPrdMappConf> getLmtSubPrdByFkPkId(String id) {
		QueryModel model = new QueryModel();
		model.addCondition("fkPkid", id);
	    List<LmtSubPrdMappConf> records = (List<LmtSubPrdMappConf>) lmtSubPrdMappConfMapper.selectByModel(model);
	
	    return records;
	}

    public int save(String fkPkid,List<CfgPrdCatalogDto> cfgPrdCatalogDtos) {
        List<LmtSubPrdMappConfDto> lmtSubPrdMappConfDtos = iCmisCfgClientService.getLmtSubPrdMappConf(cfgPrdCatalogDtos);
        int count = 0;
        for (LmtSubPrdMappConfDto lmtSubPrdMappConfDto : lmtSubPrdMappConfDtos) {
            LmtSubPrdMappConf lmtSubPrdMappConf = new LmtSubPrdMappConf();
            BeanUtils.beanCopy(lmtSubPrdMappConfDto,lmtSubPrdMappConf);
            lmtSubPrdMappConf.setPkId(StringUtils.getUUID());
            lmtSubPrdMappConf.setLimitSubNo(fkPkid);
            lmtSubPrdMappConfMapper.insertSelective(lmtSubPrdMappConf);
            count++;
        }
        return count;
    }

	public List<LmtConfStrMtableResDto> selectDtoByQueryDto(List<LmtConfStrMtableResDto> lmtStrMtableConfList) {
		if(lmtStrMtableConfList!=null&&lmtStrMtableConfList.size()>0) {
			for(LmtConfStrMtableResDto lmtConfStrMtableResDto : lmtStrMtableConfList) {
				List<LmtSubPrdMappConf> list  = lmtSubPrdMappConfMapper.selectByLimitSubNo(lmtConfStrMtableResDto.getPkId());
				List<LmtSubPrdMappConfDto> dtoList = new ArrayList<LmtSubPrdMappConfDto>();
                dtoList = (List<LmtSubPrdMappConfDto>) BeanUtils.beansCopy(list, LmtSubPrdMappConfDto.class);
				lmtConfStrMtableResDto.setPrdMappList(dtoList);
			}
		}
		return lmtStrMtableConfList;
	}

	public List<LmtSubPrdMappConf> selectLmtSubPrdMappConfByLimitSubNo(String limitSubNo){
        return lmtSubPrdMappConfMapper.selectByLimitSubNo(limitSubNo);
    }

    /**
     * 保存额度结构树勾选的适用产品
     * add by zhangjw 20210507
     * @param lmtSubPrdMappConfDto
     * @return
     */
    public int saveLmtSubPrdMappConfList(LmtSubPrdMappConfDto lmtSubPrdMappConfDto) {
        int count = 0;
        String limitSubNo = lmtSubPrdMappConfDto.getLimitSubNo();
        lmtSubPrdMappConfMapper.deleteByLimitSubNo(limitSubNo);

        List<LmtSubPrdMappConfListDto> list = lmtSubPrdMappConfDto.getLmtSubPrdMappConfListDto();

        if(list!=null && list.size()>0 ){
            for (LmtSubPrdMappConfListDto item : list) {
                LmtSubPrdMappConf lmtSubPrdMappConf = new LmtSubPrdMappConf();
                lmtSubPrdMappConf.setPkId(StringUtils.getUUID());
                lmtSubPrdMappConf.setLimitSubNo(limitSubNo);
                lmtSubPrdMappConf.setPrdId(item.getPrdId());
                lmtSubPrdMappConf.setPrdName(item.getPrdName());
                lmtSubPrdMappConf.setOprType(CmisLmtConstants.OPR_TYPE_ADD);

                User userInfo = SessionUtils.getUserInformation();
                String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);

                lmtSubPrdMappConf.setInputId(userInfo.getLoginCode());
                lmtSubPrdMappConf.setInputBrId(userInfo.getOrg().getCode());
                lmtSubPrdMappConf.setInputDate(nowDate);
                lmtSubPrdMappConf.setUpdId(userInfo.getLoginCode());
                lmtSubPrdMappConf.setUpdDate(nowDate);
                lmtSubPrdMappConf.setUpdBrId(userInfo.getOrg().getCode());
                lmtSubPrdMappConf.setCreateTime(DateUtils.getCurrDate());
                lmtSubPrdMappConf.setUpdateTime(DateUtils.getCurrDate());

                lmtSubPrdMappConfMapper.insertSelective(lmtSubPrdMappConf);
                count++;
            }
        }

        return count;
    }

    /**
     * @方法名称: selectByPrdMappByPrdId
     * @方法描述: 通过用信产品编号，获取对应分项产品吗
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtSubPrdMappConf selectByPrdMappByPrdId(String prdId) {
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("prdId", prdId);
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        List<LmtSubPrdMappConf> lmtSubPrdMappConfs = lmtSubPrdMappConfMapper.selectByModel(queryModel) ;
        if(CollectionUtils.nonEmpty(lmtSubPrdMappConfs)){
            return lmtSubPrdMappConfs.get(0) ;
        }
        return null ;
    }

    /**
     * @方法名称: getLimitSubNoByPrdId
     * @方法描述: 通过用信产品编号，获取对应分项产品吗
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String getLimitSubNoByPrdId(String prdId) {
        LmtSubPrdMappConf lmtSubPrdMappConfs = selectByPrdMappByPrdId(prdId) ;
        if(lmtSubPrdMappConfs!=null){
            return lmtSubPrdMappConfs.getLimitSubNo() ;
        }
        return null ;
    }

    /**
     * @方法名称: selectPrdIdByLimitSubNo
     * @方法描述: 根据额度品种编号查询适用产品编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtSubPrdMappConf> selectPrdIdByLimitSubNo(String limitSubNo) {
        List<LmtSubPrdMappConf> list = lmtSubPrdMappConfMapper.selectPrdIdByLimitSubNo(limitSubNo);
        return list;
    }

    /**
     * @方法名称: selectPrdIdByLimitSubNo
     * @方法描述: 根据产品编号查询适用产品编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtSubPrdMappConfDto> selectLimitSubNoByPrdId(String prdId) {
        List<LmtSubPrdMappConfDto> list = lmtSubPrdMappConfMapper.selectLimitSubNoByPrdId(prdId);
        return list;
    }

    /**
     * @方法名称: selectPrdIdByLimitSubNo
     * @方法描述: 根据产品编号查询适用产品编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<String> selectLimitSubNoListByLimitStrNoAndPrdId(QueryModel queryModel) {
        List<String> list = lmtSubPrdMappConfMapper.selectLimitSubNoListByLimitStrNoAndPrdId(queryModel);
        return list;
    }

    /**
     * @方法名称: selectPrdIdByLimitSubNo
     * @方法描述: 根据产品编号查询适用产品编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<String> selectLimitSubNoListByParam(QueryModel queryModel) {
        List<String> list = lmtSubPrdMappConfMapper.selectLimitSubNoListByParam(queryModel);
        return list;
    }



    /**
     * @方法名称: selectPrdIdByLimitSubNo
     * @方法描述: 根据产品编号查询适用产品编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<Map<String, String>> selectSubNoAndNameByStrNoAndPrdId(String limitStrNo, String prdId) {
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("limitStrNo", limitStrNo);
        queryModel.addCondition("prdId", prdId);
        return lmtSubPrdMappConfMapper.selectSubNoAndNameByStrNoAndPrdId(queryModel);
    }
}
