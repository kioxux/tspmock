package cn.com.yusys.yusp.service.server.cmislmt0052;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0032.req.CmisLmt0032ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0032.resp.CmisLmt0032RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0052.req.CmisLmt0052ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0052.resp.CmisLmt0052RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CusGrpMemberRelMapper;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0052Service
 * @类描述: #对内服务类
 * @功能描述: 查询单一客户及所在集团存量敞口余额  查询客户（综合授信+主体授信）敞口用信余额，不包含穿透化额度、承销额度
 * @创建时间: 2021-06-18
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0052Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0052Service.class);

    @Autowired
    private Comm4Service comm4Service ;

    @Autowired
    private CusGrpMemberRelMapper cusGrpMemberRelMapper ;

    /**
     * 查询客户授信余额（不包含穿透化额度、承销额度、低风险额度）
     * add by dumd 20210617
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0052RespDto execute(CmisLmt0052ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0052.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0052.value);
        CmisLmt0052RespDto respDto = new CmisLmt0052RespDto();
        //客户号
        String cusId = reqDto.getCusId();

        try {
            /*
                是否查询集团向下 1-是 0-否
                1：根据当前客户查找是所属集团向下的所有子公司，如果有有自动存在相关授信，则返回是 否则返回否
                0：不查询集团
             */
            String isQuryGrp = reqDto.getIsQuryGrp() ;
            BigDecimal result = BigDecimal.ZERO;
            //如果查询集团向下
            if(CmisLmtConstants.YES_NO_Y.equals(isQuryGrp)) {
                //根据改客户号查询该客户集团向下的客户信息
                List<Map<String, String>> cusMemList = cusGrpMemberRelMapper.selectByCusIdQueryGrpNoCuss(cusId);
                //空处理
                if (CollectionUtils.nonEmpty(cusMemList)) {
                    //遍历查询
                    for (Map<String, String> cusMap : cusMemList) {
                        String memCusId = cusMap.get("cusId");
                        //集团下可以等于当前客户跳过此循环
                        logger.info("{}:查询集团项下不包含穿透化额度、承销额度，的敞口余额：集团成员{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0052.key, memCusId);
                        BigDecimal loanSpacBalance = this.calLoanSpacBalanceAmt(memCusId,reqDto.getInstuCde());
                        logger.info("{}:查询集团项下不包含穿透化额度、承销额度，的敞口余额:集团成员{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0052.key, result);

                        result = result.add(loanSpacBalance);
                    }
                    logger.info("{}:查询集团项下不包含穿透化额度、承销额度，的敞口余额，集团总敞口余额{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0052.key, result);
                }else{
                    logger.info("{}:查询客户项下不包含穿透化额度、承销额度，的敞口余额{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0052.key, cusId);
                    result = this.calLoanSpacBalanceAmt(cusId,reqDto.getInstuCde());
                    logger.info("{}:查询客户项下不包含穿透化额度、承销额度，的敞口余额{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0052.key, result);
                }
            }else{
                logger.info("{}:查询客户项下不包含穿透化额度、承销额度，的敞口余额{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0052.key, cusId);
                result = this.calLoanSpacBalanceAmt(cusId,reqDto.getInstuCde());
                logger.info("{}:查询客户项下不包含穿透化额度、承销额度，的敞口余额{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0052.key, result);
            }

            respDto.setSpcaBalanceAmt(result);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("【cmislmt0052】查询客户项下不包含穿透化额度、承销额度，的敞口余额：" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0052.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0052.value);
        return respDto;
    }

    private BigDecimal calLoanSpacBalanceAmt(String cusId,String instuCde) {
        //查询客户（综合授信+主体授信+同业授信+产品授信+同业授信+产品授信）用信余额
        BigDecimal loanSpacBalance = comm4Service.selectLoanSpacBalanceAmtByCusId(cusId,instuCde);
        return loanSpacBalance;
    }

}