package cn.com.yusys.yusp.service.server.cmislmt0039;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0039.req.CmisLmt0039ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0039.resp.CmisLmt0039RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprCoopSubInfoService;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.math.BigDecimal;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0039Service
 * @类描述: #对内服务类
 * @功能描述: 获取合作方额度台账项下总用信敞口余额
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0039Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0039Service.class);

    @Autowired
    private ApprCoopSubInfoService apprCoopSubInfoService ;

    @Autowired
    private Comm4Service comm4Service ;

    @Transactional
    public CmisLmt0039RespDto execute(CmisLmt0039ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0039.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0039.value);
        CmisLmt0039RespDto resqDto = new CmisLmt0039RespDto() ;

        try {
            //合作方批复编号
            String apprSerno = reqDto.getApprSerno() ;
            //根据合作方批复编号，查询合作方向下占用总额
            //1、根据合作方批复编号，获取合作方向下有效的分项编号
            String apprSubSernos = apprCoopSubInfoService.selectAccSubNoByApprSerno(apprSerno) ;
            logger.info("【{}】查询向下分项编号有【{}】",DscmsLmtEnum.TRADE_CODE_CMISLMT0039.key, apprSubSernos);

            //用信余额
            BigDecimal totalBal = BigDecimal.ZERO;
            //用信总额
            BigDecimal loanTotal = BigDecimal.ZERO;
            //已用额度
            BigDecimal outstndAmt = BigDecimal.ZERO ;
            if(!StringUtils.isBlank(apprSubSernos)){
                totalBal = comm4Service.getBalanceByApprSubSernos(apprSubSernos) ;
                loanTotal = comm4Service.getLoanTotalByApprSubSernos(apprSubSernos) ;

                //根据批复台账编号查询对应的分项的授信总额之和和已用总额之和
                Map<String, BigDecimal> amtMap = apprCoopSubInfoService.selectTotalAmtByApprSerno(apprSerno);
                //合作方授信台账可用总额
                outstndAmt = amtMap.get("outstndAmtTotal") ;
            }
            resqDto.setSumAccSpacBalanceAmtCny(totalBal);
            resqDto.setSumLoanTotalCny(loanTotal);
            resqDto.setOutstndAmt(outstndAmt);
            //将列表放入返回中
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("获取合作方额度台账项下总用信敞口余额接口报错：",e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0039.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0039.value);
        return resqDto;
    }
}