/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CoopProFrozeUnfrozeApp;
import cn.com.yusys.yusp.dto.CoopProFrozeUnfrozeAppSubDto;
import cn.com.yusys.yusp.service.CoopProFrozeUnfrozeAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt-core模块
 * @类名称: CoopProFrozeUnfrozeAppResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-04 10:25:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/coopprofrozeunfrozeapp")
public class CoopProFrozeUnfrozeAppResource {
    @Autowired
    private CoopProFrozeUnfrozeAppService coopProFrozeUnfrozeAppService;

    /**
     * 全表查询.
     *
     * @return
     */
    @PostMapping("/query/all")
    protected ResultDto<List<CoopProFrozeUnfrozeApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CoopProFrozeUnfrozeApp> list = coopProFrozeUnfrozeAppService.selectAll(queryModel);
        return new ResultDto<List<CoopProFrozeUnfrozeApp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<CoopProFrozeUnfrozeApp>> index(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno desc");
        List<CoopProFrozeUnfrozeApp> list = coopProFrozeUnfrozeAppService.selectByModel(queryModel);
        return new ResultDto<List<CoopProFrozeUnfrozeApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{serno}")
    protected ResultDto<CoopProFrozeUnfrozeApp> show(@PathVariable("serno") String serno) {
        CoopProFrozeUnfrozeApp coopProFrozeUnfrozeApp = coopProFrozeUnfrozeAppService.selectByPrimaryKey(serno);
        return new ResultDto<CoopProFrozeUnfrozeApp>(coopProFrozeUnfrozeApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<CoopProFrozeUnfrozeApp> create(@RequestBody CoopProFrozeUnfrozeApp coopProFrozeUnfrozeApp) throws URISyntaxException {
        coopProFrozeUnfrozeAppService.insertSelective(coopProFrozeUnfrozeApp);
        return new ResultDto<CoopProFrozeUnfrozeApp>(coopProFrozeUnfrozeApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CoopProFrozeUnfrozeApp coopProFrozeUnfrozeApp) throws URISyntaxException {
        int result = coopProFrozeUnfrozeAppService.updateSelective(coopProFrozeUnfrozeApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateCoopProApp
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateCoopProApp")
    protected ResultDto<Integer> updateCoopProApp(@RequestBody CoopProFrozeUnfrozeAppSubDto coopProFrozeUnfrozeAppSubDto) throws URISyntaxException {
        coopProFrozeUnfrozeAppService.updateAppSub(coopProFrozeUnfrozeAppSubDto);
        return new ResultDto<Integer>();
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = coopProFrozeUnfrozeAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = coopProFrozeUnfrozeAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
