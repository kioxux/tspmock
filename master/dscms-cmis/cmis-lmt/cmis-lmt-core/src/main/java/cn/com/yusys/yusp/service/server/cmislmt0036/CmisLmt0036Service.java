package cn.com.yusys.yusp.service.server.cmislmt0036;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.dto.server.cmisLmt0036.req.CmisLmt0036ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0036.resp.CmisLmt0036RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.LmtContRelService;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0036Service
 * @类描述: #对内服务类
 * @功能描述: 起始日到期日，期限等
 * @创建人: 李召星
 * @创建时间: 2021-05-07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0036Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0036Service.class);

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService;

    @Autowired
    private LmtContRelService lmtContRelService ;

    /**
     * 根据分项信息，查询起始日到期日期限
     * add by lizx 20210629
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0036RespDto execute(CmisLmt0036ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0036.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0036.value);
        CmisLmt0036RespDto respDto = new CmisLmt0036RespDto();

        //分项编号
        String accSubNo = reqDto.getAccSubNo();
        try {
            if(StringUtils.isBlank(accSubNo)){
                respDto.setErrorCode(EclEnum.ECL070085.key);
                respDto.setErrorMsg(EclEnum.ECL070085.value);
                return respDto;
            }
            //根据分项编号，获取分项信息
            ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(accSubNo) ;
            if(apprLmtSubBasicInfo!=null){
                //起始日期
                String startDate = apprLmtSubBasicInfo.getStartDate() ;
                //到日期
                String endDate = apprLmtSubBasicInfo.getLmtDate() ;
                //期限
                Integer term = apprLmtSubBasicInfo.getTerm() ;
                //组装相应报文
                BigDecimal bizTotalBalAmtCny = BigDecimalUtil.replaceNull(lmtContRelService.selectLmtContRelOutstandBal(accSubNo)) ;
                respDto.setStartDate(startDate);
                respDto.setEndDate(endDate);
                respDto.setTerm(term);
                respDto.setBizTotalBalAmtCny(bizTotalBalAmtCny);
                logger.info(this.getClass().getName()+": "+DscmsLmtEnum.TRADE_CODE_CMISLMT0036.value+"，起始日："
                        +startDate+";到期日："+endDate+";期限:"+term+";授信余总额："+bizTotalBalAmtCny);
                //返回结果
                respDto.setErrorCode(SuccessEnum.SUCCESS.key);
                respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
            }else{
                throw new YuspException(EclEnum.ECL070007.key, EclEnum.ECL070007.value);
            }
        } catch (YuspException e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0036.value, e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0036.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0036.value);
        return respDto;
    }
}