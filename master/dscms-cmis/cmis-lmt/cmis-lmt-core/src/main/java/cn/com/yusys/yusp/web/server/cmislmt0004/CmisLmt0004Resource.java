package cn.com.yusys.yusp.web.server.cmislmt0004;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0004.req.CmisLmt0004ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0004.resp.CmisLmt0004RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0004.CmisLmt0004Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:合作方额度同步
 *
 * @author dmd 20210507
 * @version 1.0
 */
@Api(tags = "cmislmt0004:合作方额度同步")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0004Resource.class);

    @Autowired
    private  CmisLmt0004Service cmisLmt0004Service;
    /**
     * 交易码：cmislmt0004
     * 交易描述：合作方额度同步
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("合作方额度同步")
    @PostMapping("/cmislmt0004")
    protected @ResponseBody
    ResultDto<CmisLmt0004RespDto> CmisLmt0004(@Validated @RequestBody CmisLmt0004ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0004.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0004.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0004RespDto> cmisLmt0004RespDtoResultDto = new ResultDto<>();
        CmisLmt0004RespDto cmisLmt0004RespDto = new CmisLmt0004RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0004RespDto = cmisLmt0004Service.execute(reqDto);
            cmisLmt0004RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0004RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0004.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0004.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0004.value, e.getMessage());
            // 封装CmisLmt0004RespDtoResultDto中异常返回码和返回信息
            cmisLmt0004RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0004RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }

        // 封装CmisLmt0004RespDto到CmisLmt0004RespDtoResultDto中
        cmisLmt0004RespDtoResultDto.setData(cmisLmt0004RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0004.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0004.value, JSON.toJSONString(cmisLmt0004RespDtoResultDto));
        return cmisLmt0004RespDtoResultDto;
    }
}
