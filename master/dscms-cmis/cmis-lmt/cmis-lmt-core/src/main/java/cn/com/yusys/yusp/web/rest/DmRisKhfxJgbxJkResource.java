/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.DmRisKhfxJgbxJk;
import cn.com.yusys.yusp.service.DmRisKhfxJgbxJkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: DmRisKhfxJgbxJkResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-25 15:50:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/dmriskhfxjgbxjk")
public class DmRisKhfxJgbxJkResource {
    @Autowired
    private DmRisKhfxJgbxJkService dmRisKhfxJgbxJkService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<DmRisKhfxJgbxJk>> query() {
        QueryModel queryModel = new QueryModel();
        List<DmRisKhfxJgbxJk> list = dmRisKhfxJgbxJkService.selectAll(queryModel);
        return new ResultDto<List<DmRisKhfxJgbxJk>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<DmRisKhfxJgbxJk>> selectByModel(@RequestBody QueryModel queryModel) {
        List<DmRisKhfxJgbxJk> list = dmRisKhfxJgbxJkService.selectByModel(queryModel);
        return new ResultDto<List<DmRisKhfxJgbxJk>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<DmRisKhfxJgbxJk> create(@RequestBody DmRisKhfxJgbxJk dmRisKhfxJgbxJk) throws URISyntaxException {
        dmRisKhfxJgbxJkService.insert(dmRisKhfxJgbxJk);
        return new ResultDto<DmRisKhfxJgbxJk>(dmRisKhfxJgbxJk);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody DmRisKhfxJgbxJk dmRisKhfxJgbxJk) throws URISyntaxException {
        int result = dmRisKhfxJgbxJkService.update(dmRisKhfxJgbxJk);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String dataDt, String custTypeId, String custId) {
        int result = dmRisKhfxJgbxJkService.deleteByPrimaryKey(dataDt, custTypeId, custId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 单一客户风险暴露查询
     * @param queryModel
     * @return
     */
    @PostMapping("/selectList")
    protected ResultDto<List<Map>> selectList(@RequestBody QueryModel queryModel) {
        List<Map> list = dmRisKhfxJgbxJkService.selectSingleCusList(queryModel);
        return new ResultDto<List<Map>>(list);
    }

    /**
     * 单一指标风险暴露查询
     * @param queryModel
     * @return
     */
    @PostMapping("/selectSingleZbList")
    protected ResultDto<List<Map>> selectSingleZbList(@RequestBody QueryModel queryModel) {
        ResultDto resultDto = dmRisKhfxJgbxJkService.selectSingleZbList(queryModel);
        return resultDto;
    }

    /**
     * 单一客户风险暴露查询xls导出
     */
    @PostMapping("/exportRiskExpose01")
    public ResultDto<ProgressDto> asyncExportRiskExpose01(@RequestBody QueryModel model) {
        ProgressDto progressDto = dmRisKhfxJgbxJkService.asyncExportRiskExpose01(model);
        return ResultDto.success(progressDto);
    }

    /**
     * 单一指标风险暴露查询xls导出
     */
    @PostMapping("/exportRiskExpose02")
    public ResultDto<ProgressDto> asyncExportRiskExpose02(@RequestBody QueryModel model) {
        ProgressDto progressDto = dmRisKhfxJgbxJkService.asyncExportRiskExpose02(model);
        return ResultDto.success(progressDto);
    }

}
