/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.DeRiskIndexCfg;
import cn.com.yusys.yusp.service.DeRiskIndexCfgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: DeRiskIndexCfgResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-16 13:42:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/deriskindexcfg")
public class DeRiskIndexCfgResource {
    @Autowired
    private DeRiskIndexCfgService deRiskIndexCfgService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<DeRiskIndexCfg>> query() {
        QueryModel queryModel = new QueryModel();
        List<DeRiskIndexCfg> list = deRiskIndexCfgService.selectAll(queryModel);
        return new ResultDto<List<DeRiskIndexCfg>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<DeRiskIndexCfg>> index(@RequestBody QueryModel queryModel) {
        List<DeRiskIndexCfg> list = deRiskIndexCfgService.selectByModel(queryModel);
        return new ResultDto<List<DeRiskIndexCfg>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{riskType}")
    protected ResultDto<DeRiskIndexCfg> show(@PathVariable("riskType") String riskType) {
        DeRiskIndexCfg deRiskIndexCfg = deRiskIndexCfgService.selectByPrimaryKey(riskType);
        return new ResultDto<DeRiskIndexCfg>(deRiskIndexCfg);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<DeRiskIndexCfg> create(@RequestBody DeRiskIndexCfg deRiskIndexCfg) throws URISyntaxException {
        deRiskIndexCfgService.insert(deRiskIndexCfg);
        return new ResultDto<DeRiskIndexCfg>(deRiskIndexCfg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody DeRiskIndexCfg deRiskIndexCfg) throws URISyntaxException {
        int result = deRiskIndexCfgService.update(deRiskIndexCfg);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{riskType}")
    protected ResultDto<Integer> delete(@PathVariable("riskType") String riskType) {
        int result = deRiskIndexCfgService.deleteByPrimaryKey(riskType);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = deRiskIndexCfgService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
