/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.DefaultDataHandle;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.domain.LmtWhiteInfo;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.service.LmtWhiteInfoService;
import cn.com.yusys.yusp.vo.WhiteInfo01ImportVo;
import cn.com.yusys.yusp.vo.WhiteInfo02ImportVo;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtWhiteInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: yuanz
 * @创建时间: 2021-04-17 11:08:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtwhiteinfo")
public class LmtWhiteInfoResource {
    private static final Logger log = LoggerFactory.getLogger(LmtWhiteInfoResource.class);
    @Autowired
    private LmtWhiteInfoService lmtWhiteInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtWhiteInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtWhiteInfo> list = lmtWhiteInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtWhiteInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/selectByModel")
    protected ResultDto<List<LmtWhiteInfo>> index(QueryModel queryModel) {
        List<LmtWhiteInfo> list = lmtWhiteInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtWhiteInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/querymodel")
    protected ResultDto<List<LmtWhiteInfo>> selectByModel(@RequestBody QueryModel queryModel) {
        List<LmtWhiteInfo> list = lmtWhiteInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtWhiteInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtWhiteInfo> show(@PathVariable("pkId") String pkId) {
        LmtWhiteInfo lmtWhiteInfo = lmtWhiteInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtWhiteInfo>(lmtWhiteInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtWhiteInfo> create(@RequestBody LmtWhiteInfo lmtWhiteInfo) {
        lmtWhiteInfoService.insert(lmtWhiteInfo);
        return new ResultDto<LmtWhiteInfo>(lmtWhiteInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtWhiteInfo lmtWhiteInfo) {
        int result = lmtWhiteInfoService.update(lmtWhiteInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtWhiteInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtWhiteInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:logicalDelete
     * @函数描述:产品删除时，数据逻辑删除(既将操作类型更新为“02-删除”)，不进行物理删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/logicalDelete/{pkId}")
    protected ResultDto<Integer> logicalDelete(@PathVariable("pkId") String pkId ) {
        int result = lmtWhiteInfoService.logicDelete(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 异步下载承兑行白名单模板
     */
    @PostMapping("/exportwhiteinfo01")
    public ResultDto<ProgressDto> asyncExportWhiteInfo01() {
        ProgressDto progressDto = lmtWhiteInfoService.asyncExportWhiteInfo01();
        return ResultDto.success(progressDto);
    }

    /**
     * 异步下载产品户买入返售白名单模板
     */
    @PostMapping("/exportwhiteinfo02")
    public ResultDto<ProgressDto> asyncExportWhiteInfo02() {
        ProgressDto progressDto = lmtWhiteInfoService.asyncExportWhiteInfo02();
        return ResultDto.success(progressDto);
    }

    /**
     * 异步下载承兑行白名单
     */
    @PostMapping("/exportlmtwhiteinfo01")
    public ResultDto<ProgressDto> asyncExportLmtWhiteInfo01(@RequestBody QueryModel model) {
        ProgressDto progressDto = lmtWhiteInfoService.asyncExportLmtWhiteInfo01(model);
        return ResultDto.success(progressDto);
    }

    /**
     * 异步下载产品户买入返售白名单
     */
    @PostMapping("/exportlmtwhiteinfo02")
    public ResultDto<ProgressDto> asyncExportLmtWhiteInfo02(@RequestBody  QueryModel model) {
        ProgressDto progressDto = lmtWhiteInfoService.asyncExportLmtWhiteInfo02(model);
        return ResultDto.success(progressDto);
    }



    /**
     * Excel数据导入
     * @param fileId Excel文件信息ID
     * @return
     */
    @PostMapping("/importwhiteinfo01")
    public ResultDto<ProgressDto> asyncImportwhiteinfo01(@RequestParam("fileId") String fileId) {
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = null;
        try {
            tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
            FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);

            // 将文件内容导入数据库，StudentScore为导入数据的类
//            ProgressDto progressDto = ExcelUtils.asyncImport(ImportContext.of(WhiteInfo01ImportVo.class)
//                    // 批量操作需要将batch设置为true
//                    .batch(true)
//                    .file(tempFile)
//                    .dataStorage(ExcelUtils.batchConsumer(lmtWhiteInfoService::insertWhiteInfo01)));
            log.info("开始执行异步导入，导入fileId为[{}];", fileId);

            //modify by zhangjw 20210805 异步改为同步
            ExcelUtils.syncImport(WhiteInfo01ImportVo.class, tempFile, new DefaultDataHandle(), ExcelUtils.batchConsumer(dataList -> {
                return lmtWhiteInfoService.insertWhiteInfo01(dataList);
            }), true);

        } catch (IOException e) {
            log.error(EclEnum.ECL070094.value,e);
            return ResultDto.error(EcnEnum.ECN069999.key, e.getMessage());
        }
        return ResultDto.success().message("导入成功！");
    }

    /**
     * Excel数据导入
     * @param fileId Excel文件信息ID
     * @return
     */
    @PostMapping("/importwhiteinfo02")
    public ResultDto<ProgressDto> asyncImportwhiteinfo02(@RequestParam("fileId") String fileId) {
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = null;
        try {
            tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
            FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
            // 将文件内容导入数据库，StudentScore为导入数据的类
//            progressDto = ExcelUtils.syncImport(ImportContext.of(WhiteInfo02ImportVo.class)
//                    // 批量操作需要将batch设置为true
//                    .batch(true)
//                    .file(tempFile)
//                    .dataStorage(ExcelUtils.batchConsumer(lmtWhiteInfoService::insertWhiteInfo02)));
            log.info("开始执行异步导入，导入fileId为[{}];",fileId);
            //modify by zhangjw 20210805 异步改为同步
            ExcelUtils.syncImport(WhiteInfo02ImportVo.class, tempFile, new DefaultDataHandle(), ExcelUtils.batchConsumer(dataList -> {
                return lmtWhiteInfoService.insertWhiteInfo02(dataList);
            }), true);

        } catch (BizException e) {
            log.error(EclEnum.ECL070095.value,e);
            return ResultDto.error(EcnEnum.ECN069999.key, e.getMessage());
        } catch (IOException e) {
            log.error(EclEnum.ECL070095.value,e);
            return ResultDto.error(EcnEnum.ECN069999.key, e.getMessage());
        }
        return ResultDto.success().message("导入成功！");
    }

    /**
     * @函数名称:
     * @函数描述:检查该客户是否已经有承兑行白名单记录
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkSameOrgCusIdIsExist")
    protected ResultDto<Integer> checkSameOrgCusIdIsExist(@RequestBody QueryModel queryModel) {
        String cusId = (String) queryModel.getCondition().get("cusId");
        int result = lmtWhiteInfoService.selectRecordsByCusId(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:
     * @函数描述:检查该客户是否已经有产品户买入返售（信用债）白名单记录
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkSameOrgResaleCusIdIsExist")
    protected ResultDto<Integer> checkSameOrgResaleCusIdIsExist(@RequestBody QueryModel queryModel) {
        String cusId = (String) queryModel.getCondition().get("cusId");
        int result = lmtWhiteInfoService.selectResaleRecordsByCusId(cusId);
        return new ResultDto<Integer>(result);
    }



}
