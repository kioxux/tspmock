/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.domain.LmtSubPrdMappConf;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.repository.mapper.ApprLmtSubBasicInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSubPrdMappConfMapper;
import cn.com.yusys.yusp.service.server.comm.Comm4LmtCalFormulaUtils;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import feign.QueryMap;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;


/**
 * @version 1.0.0
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprLmtSubBasicInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: LQC
 * @创建时间: 2021-04-01 20:40:32
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ApprLmtSubBasicInfoService {

    private static final Logger log = LoggerFactory.getLogger(ApprLmtSubBasicInfoService.class);

    @Autowired
    private ApprLmtSubBasicInfoMapper apprLmtSubBasicInfoMapper;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private LmtSubPrdMappConfMapper lmtSubPrdMappConfMapper;

    @Autowired
    private Comm4LmtCalFormulaUtils comm4LmtCalFormulaUtils ;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ApprLmtSubBasicInfo selectByPrimaryKey(String pkId) {
        return apprLmtSubBasicInfoMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<ApprLmtSubBasicInfo> selectAll(QueryModel model) {
        List<ApprLmtSubBasicInfo> records = (List<ApprLmtSubBasicInfo>) apprLmtSubBasicInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<ApprLmtSubBasicInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ApprLmtSubBasicInfo> list = apprLmtSubBasicInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(ApprLmtSubBasicInfo record) {
        return apprLmtSubBasicInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(ApprLmtSubBasicInfo record) {
        return apprLmtSubBasicInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(ApprLmtSubBasicInfo record) {
        return apprLmtSubBasicInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(ApprLmtSubBasicInfo record) {
        return apprLmtSubBasicInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return apprLmtSubBasicInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return apprLmtSubBasicInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectBylimitSubNo
     * @方法描述: 根据分项品种编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ApprLmtSubBasicInfo selectByApprSubSerno(String apprSubSerno) {
        return apprLmtSubBasicInfoMapper.selectByApprSubSerno(apprSubSerno);
    }

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据分项编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ApprLmtSubBasicInfo selectBySerno(String serno) {
        return apprLmtSubBasicInfoMapper.selectBySerno(serno);
    }

    public List<ApprLmtSubBasicInfo> getSubByFkPkidList(List<String> pkIdList) {
        List<ApprLmtSubBasicInfo> apprLmtSubBasicInfoList = new ArrayList<ApprLmtSubBasicInfo>();
        if (pkIdList != null & pkIdList.size() > 0) {
            apprLmtSubBasicInfoMapper.getSubByFkPkidList(pkIdList);
        }
        return apprLmtSubBasicInfoList;
    }

    /**
     * 获取客户额度视图列表
     * @param queryModel
     * @return
     */
    public List<Map<String, Object>> selectLmtCusInfoList(QueryModel queryModel) {
        log.info("请求数据信息************************** :{}", JSON.toJSONString(queryModel));
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String, Object>> mapList = apprLmtSubBasicInfoMapper.selectLmtCusInfoList(queryModel);
        if(CollectionUtils.isNotEmpty(mapList)){
            if (mapList.size() > 0) {
                for (Map<String, Object> map : mapList) {
                    if (CmisLmtConstants.STD_ZB_YES_NO_Y.equals(map.get("flag"))){
                        BigDecimal lmtAmtAdd = BigDecimalUtil.replaceNull(map.get("lmtAmtAdd"));
                        BigDecimal avlAmt = BigDecimalUtil.replaceNull(map.get("avlAmt"));
                        // 授信总额
                        BigDecimal creditTotal = lmtAmtAdd.add(avlAmt);
                        map.put("creditTotal", creditTotal);
                        // 授信可用余额
                        BigDecimal availableTotal = creditTotal.subtract(BigDecimalUtil.replaceNull(map.get("outstndAmt")));
                        map.put("availableTotal", availableTotal);
                        // 敞口可用余额
                        BigDecimal spacTotal = BigDecimalUtil.replaceNull(map.get("spacAmt")).subtract(BigDecimalUtil.replaceNull(map.get("spacOutstndAmt")));
                        map.put("spacTotal", spacTotal);
                        // 支持树状结构
                        map.put("hasChildren",true);
                    }else{
                        // 授信总额
                        BigDecimal creditTotal = BigDecimalUtil.replaceNull(map.get("avlAmt"));
                        map.put("creditTotal", creditTotal);
                        // 授信可用余额
                        BigDecimal availableTotal = creditTotal.subtract(BigDecimalUtil.replaceNull(map.get("outstndAmt")));
                        map.put("availableTotal", availableTotal);
                        // 敞口可用余额
                        map.put("spacTotal", BigDecimalUtil.replaceNull(map.get("avlAmt")));

                        //获取承兑行白名单额度对应的到期日，根据 白名单额度到期日 及 白名单额度已用金额 判断额度状态；
                        Date endDate = DateUtils.parseDate((String) map.get("lmtDate"), DateUtils.PATTERN_DEFAULT);
                        Date curDate = DateUtils.getCurrDate();
                        BigDecimal outstndAmt =  BigDecimalUtil.replaceNull(map.get("outstndAmt"));
                        String status = CmisLmtConstants.STD_ZB_APPR_ST_01;
                        if(DateUtils.compare(endDate,curDate) == -1 && outstndAmt.compareTo(BigDecimal.ZERO) > 0){ //endDate<curDate  && outstndAmt>0
                            status = CmisLmtConstants.STD_ZB_APPR_ST_90;
                        }else if (DateUtils.compare(endDate,curDate) == -1 && outstndAmt.compareTo(BigDecimal.ZERO) <= 0){
                            status = CmisLmtConstants.STD_ZB_APPR_ST_99;
                        }
                        map.put("status",status);
                        map.put("hasChildren",true);
                    }
                }
            }
        }
        PageHelper.clearPage();
        return mapList;
    }


    /**
     * 获取客户额度视图列表
     * @param queryModel
     * @return
     */
    public List<Map<String, Object>> selectLmtSigZhInfoList(QueryModel queryModel) {
        log.info("请求数据信息************************** :{}", JSON.toJSONString(queryModel));
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String, Object>> mapList = apprLmtSubBasicInfoMapper.selectLmtSigZhInfoList(queryModel);
        if(CollectionUtils.isNotEmpty(mapList)){
            if (mapList.size() > 0) {
                for (Map<String, Object> map : mapList) {
                    if (CmisLmtConstants.STD_ZB_YES_NO_Y.equals(map.get("flag"))){
                        BigDecimal lmtAmtAdd = BigDecimalUtil.replaceNull(map.get("lmtAmtAdd"));
                        BigDecimal avlAmt = BigDecimalUtil.replaceNull(map.get("avlAmt"));
                        // 授信总额
                        BigDecimal creditTotal = lmtAmtAdd.add(avlAmt);
                        map.put("creditTotal", creditTotal);
                        // 授信可用余额
                        BigDecimal availableTotal = creditTotal.subtract(BigDecimalUtil.replaceNull(map.get("outstndAmt")));
                        map.put("availableTotal", availableTotal);
                        // 敞口可用余额
                        BigDecimal spacTotal = BigDecimalUtil.replaceNull(map.get("spacAmt")).subtract(BigDecimalUtil.replaceNull(map.get("spacOutstndAmt")));
                        map.put("spacTotal", spacTotal);
                        // 支持树状结构
                        map.put("hasChildren",true);
                    }else{
                        // 授信总额
                        BigDecimal creditTotal = BigDecimalUtil.replaceNull(map.get("avlAmt"));
                        map.put("creditTotal", creditTotal);
                        // 授信可用余额
                        BigDecimal availableTotal = creditTotal.subtract(BigDecimalUtil.replaceNull(map.get("outstndAmt")));
                        map.put("availableTotal", availableTotal);
                        // 敞口可用余额
                        map.put("spacTotal", BigDecimalUtil.replaceNull(map.get("avlAmt")));

                        //获取承兑行白名单额度对应的到期日，根据 白名单额度到期日 及 白名单额度已用金额 判断额度状态；
                        Date endDate = DateUtils.parseDate((String) map.get("lmtDate"), DateUtils.PATTERN_DEFAULT);
                        Date curDate = DateUtils.getCurrDate();
                        BigDecimal outstndAmt =  BigDecimalUtil.replaceNull(map.get("outstndAmt"));
                        String status = CmisLmtConstants.STD_ZB_APPR_ST_01;
                        if(DateUtils.compare(endDate,curDate) == -1 && outstndAmt.compareTo(BigDecimal.ZERO) > 0){ //endDate<curDate  && outstndAmt>0
                            status = CmisLmtConstants.STD_ZB_APPR_ST_90;
                        }else if (DateUtils.compare(endDate,curDate) == -1 && outstndAmt.compareTo(BigDecimal.ZERO) <= 0){
                            status = CmisLmtConstants.STD_ZB_APPR_ST_99;
                        }
                        map.put("status",status);
                        map.put("hasChildren",true);
                    }
                }
            }
        }
        PageHelper.clearPage();
        return mapList;
    }

    /**
     * 获取额度分项弹框列表专用
     * @param queryModel
     * @return
     */
    public List<Map<String, Object>> selectLmtCusInfoListDialog(QueryModel queryModel) {
        List<Map<String, Object>> mapList = new ArrayList<>();
        String prdId = (String) queryModel.getCondition().get("prdId");
        if (!StringUtils.isBlank(prdId)) {
            List<String> lmtSubPrdlist = lmtSubPrdMappConfMapper.selectLimitSubNoListByPrdId(prdId);
            if (CollectionUtils.isNotEmpty(lmtSubPrdlist)) {
                mapList = selectLmtCusInfoSub(queryModel,lmtSubPrdlist);
            } else {
                mapList = selectLmtCusInfoSub(queryModel,null);
            }
        } else {
            mapList = selectLmtCusInfoSub(queryModel,null);
        }
        return mapList;
    }

    /**
     * 获取额度分项弹框列表专用子方法
     * @param queryModel
     * @return
     */
    public List<Map<String, Object>> selectLmtCusInfoSub(QueryModel queryModel, List lmtSubPrdlist) {

        List<Map<String, Object>> mapList ;
        if(lmtSubPrdlist!=null && lmtSubPrdlist.size()>0){
            //适用产品不为空，则返回适用品种，对应的额度
            queryModel.addCondition("limitSubNoList",lmtSubPrdlist);
            mapList =  apprLmtSubBasicInfoMapper.selectLmtCusInfoListByPrdIdList(queryModel);
        }else{
            // 返回客户全额度视图列表
            mapList =  apprLmtSubBasicInfoMapper.selectLmtCusInfoListByPrdIdList(queryModel);
        }

        List<Map<String, Object>> list = new ArrayList<>();
        if(mapList!=null && mapList.size() > 0){
            for (Map<String, Object> map : mapList) {
                if (CmisLmtConstants.STD_ZB_YES_NO_Y.equals(map.get("flag"))) {
                    BigDecimal lmtAmtAdd = BigDecimalUtil.replaceNull(map.get("lmtAmtAdd"));
                    BigDecimal avlAmt = BigDecimalUtil.replaceNull(map.get("avlAmt"));
                    // 授信总额
                    BigDecimal creditTotal = lmtAmtAdd.add(avlAmt);
                    map.put("creditTotal", creditTotal);
                    // 授信可用余额
                    BigDecimal availableTotal = creditTotal.subtract(BigDecimalUtil.replaceNull(map.get("outstndAmt")));
                    map.put("availableTotal", availableTotal);
                    // 敞口可用余额
                    BigDecimal spacTotal = BigDecimalUtil.replaceNull(map.get("spacAmt")).subtract(BigDecimalUtil.replaceNull(map.get("spacOutstndAmt")));
                    map.put("spacTotal", spacTotal);
                    QueryModel queryModelHasChild = new QueryModel();
                    queryModelHasChild.addCondition("parentId", map.get("apprSubSerno"));
                    if(lmtSubPrdlist!=null && lmtSubPrdlist.size()>0){
                        queryModelHasChild.addCondition("limitSubNoList",lmtSubPrdlist);
                    }
                    List<Map<String, Object>> subListChild = selectHasChildren(queryModelHasChild);
                    map.put("children", subListChild);
                    list.add(map);
                } else {
                    map.put("creditTotal", BigDecimalUtil.replaceNull(map.get("avlAmt")));
                    map.put("availableTotal", BigDecimalUtil.replaceNull(map.get("spacAmt")));
                    map.put("spacTotal", BigDecimalUtil.replaceNull(map.get("avlAmt")));

                    //获取承兑行白名单额度对应的到期日，根据 白名单额度到期日 及 白名单额度已用金额 判断额度状态；
                    Date endDate = DateUtils.parseDate((String) map.get("lmtDate"), DateUtils.PATTERN_DATE_SHORT);
                    Date curDate = DateUtils.getCurrDate();
                    BigDecimal outstndAmt = BigDecimalUtil.replaceNull(map.get("outstndAmt"));
                    String status = CmisLmtConstants.STD_ZB_APPR_ST_01;
                    if (DateUtils.compare(endDate, curDate) == -1 && outstndAmt.compareTo(BigDecimal.ZERO) > 0) { //endDate<curDate  && outstndAmt>0
                        status = CmisLmtConstants.STD_ZB_APPR_ST_90;
                    } else if (DateUtils.compare(endDate, curDate) == -1 && outstndAmt.compareTo(BigDecimal.ZERO) <= 0) {
                        status = CmisLmtConstants.STD_ZB_APPR_ST_99;
                    }
                    map.put("status", status);

                    QueryModel queryModelHasChild = new QueryModel();
                    queryModelHasChild.addCondition("apprSubSerno", map.get("apprSubSerno"));
                    if(lmtSubPrdlist!=null && lmtSubPrdlist.size()>0){
                        queryModelHasChild.addCondition("limitSubNoList",lmtSubPrdlist);
                    }
                    List<Map<String, Object>> subListChild = selectHasChildren(queryModelHasChild);
                    map.put("children", subListChild);
                    list.add(map);
                }
            }
        }

//        List<Map<String, Object>> mapList = apprLmtSubBasicInfoMapper.selectLmtCusInfoList(queryModel);
//        List<Map<String, Object>> list = new ArrayList<>();
//        if (mapList.size() > 0) {
//            for (Map<String, Object> map : mapList) {
//                if (CmisLmtConstants.STD_ZB_YES_NO_Y.equals(map.get("flag"))) {
//                    BigDecimal lmtAmtAdd = BigDecimalUtil.replaceNull(map.get("lmtAmtAdd"));
//                    BigDecimal avlAmt = BigDecimalUtil.replaceNull(map.get("avlAmt"));
//                    // 授信总额
//                    BigDecimal creditTotal = lmtAmtAdd.add(avlAmt);
//                    map.put("creditTotal", creditTotal);
//                    // 授信可用余额
//                    BigDecimal availableTotal = creditTotal.subtract(BigDecimalUtil.replaceNull(map.get("outstndAmt")));
//                    map.put("availableTotal", availableTotal);
//                    // 敞口可用余额
//                    BigDecimal spacTotal = BigDecimalUtil.replaceNull(map.get("spacAmt")).subtract(BigDecimalUtil.replaceNull(map.get("spacOutstndAmt")));
//                    map.put("spacTotal", spacTotal);
//                    QueryModel queryModelHasChild = new QueryModel();
//                    queryModelHasChild.addCondition("parentId", map.get("apprSubSerno"));
//                    if (!StringUtils.isBlank(limitSubNo)) {
//                        queryModelHasChild.addCondition("limitSubNo", map.get("limitSubNo"));
//                        List<Map<String, Object>> subListChild = selectHasChildren(queryModelHasChild);
//                        // 支持树状结构
//                        map.put("children", subListChild);
//                        if ((map.get("limitSubNo") == null && map.get("children") != null) || (map.get("limitSubNo")).equals(limitSubNo)) {
//                            list.add(map);
//                        }
//                    } else {
//                        List<Map<String, Object>> subListChild = selectHasChildren(queryModelHasChild);
//                        // 支持树状结构
//                        map.put("children", subListChild);
//                        list.add(map);
//                    }
////                        if (map.get("limitSubNo")==null||(map.get("limitSubNo")).equals(limitSubNo)) {
////                            list.add(map);
////                        }
//                } else {
//                    map.put("creditTotal", BigDecimalUtil.replaceNull(map.get("avlAmt")));
//                    map.put("availableTotal", BigDecimalUtil.replaceNull(map.get("spacAmt")));
//                    map.put("spacTotal", BigDecimalUtil.replaceNull(map.get("avlAmt")));
//
//                    //获取承兑行白名单额度对应的到期日，根据 白名单额度到期日 及 白名单额度已用金额 判断额度状态；
//                    Date endDate = DateUtils.parseDate((String) map.get("lmtDate"), DateUtils.PATTERN_DATE_SHORT);
//                    Date curDate = DateUtils.getCurrDate();
//                    BigDecimal outstndAmt = BigDecimalUtil.replaceNull(map.get("outstndAmt"));
//                    String status = CmisLmtConstants.STD_ZB_APPR_ST_01;
//                    if (DateUtils.compare(endDate, curDate) == -1 && outstndAmt.compareTo(BigDecimal.ZERO) > 0) { //endDate<curDate  && outstndAmt>0
//                        status = CmisLmtConstants.STD_ZB_APPR_ST_90;
//                    } else if (DateUtils.compare(endDate, curDate) == -1 && outstndAmt.compareTo(BigDecimal.ZERO) <= 0) {
//                        status = CmisLmtConstants.STD_ZB_APPR_ST_99;
//                    }
//                    map.put("status", status);
//
//                    QueryModel queryModelHasChild = new QueryModel();
//                    queryModelHasChild.addCondition("apprSubSerno", map.get("apprSubSerno"));
//                    List<Map<String, Object>> subListChild = selectHasChildren(queryModelHasChild);
//                    map.put("children", subListChild);
//                    list.add(map);
//                }
//            }
//        }
        return list;
    }



    /**
     * 查询同业客户
     * @param queryModel
     * @return
     */
    public List<Map<String, Object>> selectLmtCusPrdInfoList(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String, Object>> mapList = apprLmtSubBasicInfoMapper.selectLmtCusPrdInfoList(queryModel);
        PageHelper.clearPage();
        return mapList;
    }

    public List<ApprLmtSubBasicInfo> getByCusIdAndSubNo(String cusId, String limitSubNo) {
        QueryModel model = new QueryModel();
        model.addCondition("cusId", cusId);
        model.addCondition("limitSubNo", limitSubNo);
        model.addCondition("status", "01");
        return apprLmtSubBasicInfoMapper.selectByModel(model);
    }


    public int endSub(ApprLmtSubBasicInfo apprLmtSubBasicInfo) {
        apprLmtSubBasicInfo.setStatus("02"); // 01 启用 02 停用
        return apprLmtSubBasicInfoMapper.updateByPrimaryKeySelective(apprLmtSubBasicInfo);

    }



    /**
     * @方法名称: getAppLmtSubBasicInfoByParentId
     * @方法描述: 根据上级编号(批复编号或分项编号)获取所有分项信息，不区分状态
     * @参数与返回说明: 返回父级目录是：参数【parentId】 的所有记录
     * @算法描述: 无
     */
    public List<ApprLmtSubBasicInfo> getAppLmtSubBasicInfoByParentId(String parentId) {
        //根据父级编号，查询分项信息
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("parentId", parentId);
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        List<ApprLmtSubBasicInfo> apprLmtSubBasicInfoList = apprLmtSubBasicInfoMapper.selectByModel(queryModel);
        return apprLmtSubBasicInfoList;
    }


    private Map<String, Object> getMap(String name, BigDecimal value1, BigDecimal value2) {
        Map<String, Object> map = new HashMap<>();
        map.put("name",name);
        map.put("noAssSum",value1);
        map.put("assSum",value2);
        return map;
    }

    public List<Map<String, Object>> selectHasChildren(QueryModel queryModel) {
//        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String, Object>> mapList = apprLmtSubBasicInfoMapper.selectHasChildren(queryModel);
        if (mapList.size() > 0) {
            for (Map<String, Object> map : mapList) {
                BigDecimal lmtAmtAdd = BigDecimalUtil.replaceNull(map.get("lmtAmtAdd"));
                BigDecimal avlAmt = BigDecimalUtil.replaceNull(map.get("avlAmt"));
                // 授信总额
                BigDecimal creditTotal = lmtAmtAdd.add(avlAmt);
                map.put("creditTotal", creditTotal);
                // 授信可用余额
                BigDecimal availableTotal = creditTotal.subtract(BigDecimalUtil.replaceNull(map.get("outstndAmt")));
                map.put("availableTotal", availableTotal);
                // 敞口可用余额
                BigDecimal spacTotal = BigDecimalUtil.replaceNull(map.get("spacAmt")).subtract(BigDecimalUtil.replaceNull(map.get("spacOutstndAmt")));
                map.put("spacTotal", spacTotal);
            }
        }
//        PageHelper.clearPage();
        return mapList;
    }

    public List<Map<String, Object>> selectHasChildren4SameOrg(QueryModel queryModel) {
//        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());

        List<Map<String, Object>> mapList = apprLmtSubBasicInfoMapper.selectHasChildren4SameOrg(queryModel);
        if (mapList.size() > 0) {
            for (Map<String, Object> map : mapList) {
                BigDecimal lmtAmtAdd = BigDecimalUtil.replaceNull(map.get("lmtAmtAdd"));
                BigDecimal avlAmt = BigDecimalUtil.replaceNull(map.get("avlAmt"));
                // 授信总额
                BigDecimal creditTotal = lmtAmtAdd.add(avlAmt);
                map.put("creditTotal", creditTotal);
                // 授信可用余额
                BigDecimal availableTotal = creditTotal.subtract(BigDecimalUtil.replaceNull(map.get("outstndAmt")));
                map.put("availableTotal", availableTotal);
                // 敞口可用余额
                BigDecimal spacTotal = BigDecimalUtil.replaceNull(map.get("spacAmt")).subtract(BigDecimalUtil.replaceNull(map.get("spacOutstndAmt")));
                map.put("spacTotal", spacTotal);
            }
        }
//        PageHelper.clearPage();
        return mapList;
    }


    /**
     * @方法名称: selectAppLmtSubBasicInfoByLmtSubNo
     * @方法描述: 根据分项编号，获取分项批复信息，不考虑状态
     * @参数与返回说明: 存在：返回分项批复信息，不存在返回空
     * @算法描述: 无
     */
    public ApprLmtSubBasicInfo selectAppLmtSubBasicInfoByLmtSubNo(String lmtSubNo) {
        if(StringUtils.isBlank(lmtSubNo)){
            throw new YuspException(EclEnum.ECL070126.key, EclEnum.ECL070126.value) ;
        }
        //根据父级编号，查询分项信息
        QueryModel queryModel = new QueryModel();
        //分项编号
        queryModel.addCondition("apprSubSerno", lmtSubNo);
        //批复信息
        List<ApprLmtSubBasicInfo> apprLmtSubBasicInfoList = apprLmtSubBasicInfoMapper.selectByModel(queryModel);
        if(CollectionUtils.isNotEmpty(apprLmtSubBasicInfoList)){
            return apprLmtSubBasicInfoList.get(0) ;
        }
        return null ;
    }

    /**
     * @方法名称: selectSubInfoByParentIdAndLSubNo
     * @方法描述: 根据分项编号，获取分项批复信息，不考虑状态
     * @参数与返回说明: 存在：返回分项批复信息，不存在返回空
     * @算法描述: 无
     */
    public ApprLmtSubBasicInfo selectSubInfoByParentIdAndLSubNo(String parentId, String limitSubNo) {
        //根据父级编号，查询分项信息
        QueryModel queryModel = new QueryModel();
        //分项编号
        queryModel.addCondition("parentId", parentId);
        queryModel.addCondition("limitSubNo", limitSubNo);
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        queryModel.addCondition("status", CmisLmtConstants.STD_ZB_APPR_ST_01);
        //批复信息
        List<ApprLmtSubBasicInfo> apprLmtSubBasicInfoList = apprLmtSubBasicInfoMapper.selectByModel(queryModel);
        if(CollectionUtils.isNotEmpty(apprLmtSubBasicInfoList)){
            return apprLmtSubBasicInfoList.get(0) ;
        }
        return null ;
    }

    /**
     * @方法名称: selectSubInfoByParentIdAndLSubNo
     * @方法描述: 根据分项编号，额度产品编号，产品类型属性，查询分项明细
     * @参数与返回说明: 存在：返回分项批复信息，不存在返回空
     * @算法描述: 无
     */
    public ApprLmtSubBasicInfo selectByPrdAndPraParam(String parentId, String limitSubNo, String prdTypeProp) {
        //根据父级编号，查询分项信息
        QueryModel queryModel = new QueryModel();
        //分项编号
        queryModel.addCondition("parentId", parentId);
        queryModel.addCondition("limitSubNo", limitSubNo);
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        queryModel.addCondition("status", CmisLmtConstants.STD_ZB_APPR_ST_01);
        queryModel.addCondition("prdTypeProp", prdTypeProp);
        //批复信息
        List<ApprLmtSubBasicInfo> apprLmtSubBasicInfoList = apprLmtSubBasicInfoMapper.selectByPrdAndPraParam(queryModel);
        if(CollectionUtils.isNotEmpty(apprLmtSubBasicInfoList)){
            return apprLmtSubBasicInfoList.get(0) ;
        }
        return null ;
    }



    /**
     * @方法名称: selectLmtSubTotal
     * @方法描述: 根据根据批复编号，汇总分项总额，已用金额，累加金额，出账金额等
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map<String, BigDecimal> selectLmtSubTotal(String parentId) {
        //根据父级编号，查询分项信息
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("parentId", parentId);
        Map<String, BigDecimal> apprlmt = apprLmtSubBasicInfoMapper.selectLmtSubTotal(queryModel);
        return apprlmt ;
    }


    /**
     * @方法名称: selectApprNoBySubNo
     * @方法描述: 根据分项编号，获取批复编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String selectApprNoBySubNo(String subNo) {
        //根据父级编号，查询分项信息
        ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoMapper.selectByApprSubSerno(subNo) ;

        /*if(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_01.equals(apprLmtSubBasicInfo.getLmtSubType())){
            parentId =  apprLmtSubBasicInfo.getParentId() ;
        }else{
            String parentId = apprLmtSubBasicInfo.getParentId() ;
            return selectApprNoBySubNo(parentId) ;
        }*/

        return apprLmtSubBasicInfo.getFkPkid() ;
    }

    /**
     * @方法名称: selectAccSubNoByArray
     * @方法描述: 查询记录是否存在
     * @参数与返回说明: 流水号
     * @算法描述: 无
     */

    public String selectAccSubNoByArray(List<String> apprSubSernos) {
        return apprLmtSubBasicInfoMapper.selectAccSubNoByArray(apprSubSernos);
    }

    /**
     * @方法名称: selectLmtSubAccListByCusIdList
     * @方法描述: 根据客户号列表查询客户额度分项信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<Map<String,Object>> selectLmtSubAccListByCusIdList(List<String> cusIdList) {
        return apprLmtSubBasicInfoMapper.selectLmtSubAccListByCusIdList(cusIdList);
    }

    /**
     * @方法名称: selectLmtAmtByApprSernos
     * @方法描述: 根据批复台账编号查询授信总额等信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map<String, BigDecimal> selectLmtAmtByApprSernos(List<String> apprSernos) {
        return apprLmtSubBasicInfoMapper.selectLmtAmtByApprSernos(apprSernos);
    }

    /**
     * 根据客户编号查询批复分项信息
     * @param queryMap
     * @return
     */
    public List<Map<String,Object>> selectLmtInfoByCusList(Map queryMap){
        return apprLmtSubBasicInfoMapper.selectLmtInfoByCusList(queryMap);
    }

    /**
     * 查询个人新微贷额度信息
     * @param queryMode
     * @return
     */
    public List<Map<String,Object>> selectLmtInfoByQueryModel(QueryModel queryMode){
        return apprLmtSubBasicInfoMapper.selectLmtInfoByQueryModel(queryMode);
    }

    /**
     * 查询额度分项下的产品分项里的最小可用金额与最小敞口可用金额
     * @param parentId
     * @return
     */
    public Map<String,BigDecimal> selectMinAvailAndSpacAmt(String parentId){
        return apprLmtSubBasicInfoMapper.selectMinAvailAndSpacAmt(parentId);
    }

    /**
     * 根据外键查询分项记录数
     * @return
     */
    public int countRecordByFkPkid(String fkPkid){
        return apprLmtSubBasicInfoMapper.countRecordByFkPkid(fkPkid);
    }


    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<ApprLmtSubBasicInfo> selectSubBasicInfoByPool(QueryModel queryModel){
        return apprLmtSubBasicInfoMapper.selectSubBasicInfoByPool(queryModel) ;
    }

    /**
     * 根据批复台账编号查询名下的批复分项信息
     * @param fkPkid
     * @return
     */
    public List<ApprLmtSubBasicInfo> selectByFkPkid(String fkPkid){
        return apprLmtSubBasicInfoMapper.selectByFkPkid(fkPkid);
    }

    /**
     * 根据额度品种编号和客户号查询客户的额度分项编号
     * @param cusId limitSubNo
     * @return
     */
    public String selectApprSubSernoByCusIdAndLmtSubNo(String cusId, String limitSubNo){
        //根据额度品种编号和客户号查询该同业客户的额度分项编号
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId",cusId);
        queryModel.addCondition("limitSubNo",limitSubNo);
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        return apprLmtSubBasicInfoMapper.selectApprSubSernoByCusIdAndLmtSubNo(queryModel);
    }

    /**
     * @作者:lizx
     * @方法名称: updateByPKeyInApprLmtChgDetails
     * @方法描述:  传入原始数据，最新数据，一级相关接口信息，跟数据的同时，调用留存记录，留存数据信息appr_lmt_chg_details
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/28 19:44
    * @param apprLmtSubBasicInfo:
    * @param paramMap:
     * @return: int
     * @算法描述: 无
    */
    public int updateByPKeyInApprLmtChgDetails(ApprLmtSubBasicInfo apprLmtSubBasicInfo, Map<String, String> paramMap){
        return apprLmtSubBasicInfoMapper.updateByPrimaryKey(apprLmtSubBasicInfo);
    }

    /**
     * 根据额度品种编号和客户号查询客户的额度分项编号
     * @param cusId limitSubNo
     * @return
     */
    public BigDecimal selectLmtType04And05LmtBal(String cusId, String instuCde){
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("cusId", cusId);
        queryModel.addCondition("instuCde", instuCde);
        return apprLmtSubBasicInfoMapper.selectLmtType04And05LmtBal(queryModel);
    }

    public List<ApprLmtSubBasicInfo> selectSbuListByParentId(String cusId, String parentId){
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("cusId", cusId);
        queryModel.addCondition("parentId", parentId);
        return apprLmtSubBasicInfoMapper.selectSbuListByParentId(queryModel);
    }

    /**
     * 根据额度品种编号和客户号查询客户的额度分项编号
     * @param cusId limitSubNo
     * @return
     */
    public BigDecimal selectLmtType04And05UseBal(String cusId, String instuCde){
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("cusId", cusId);
        queryModel.addCondition("instuCde", instuCde);
        return apprLmtSubBasicInfoMapper.selectLmtType04And05UseBal(queryModel);
    }

    /**
     * @作者:lizx
     * @方法名称: selectLmtTotalByCusId
     * @方法描述:  根据客户号查，返回该客户下非失效已结清状态授信总额
     * @参数与返回说明: 授信总额、累加金额
     * @算法描述: 无
     * @日期：2021/8/28 19:01
     * @param cusId:
     * @param instuCde:
     * @return: java.lang.String
     * @算法描述: 无
     */
    public Map<String,BigDecimal> selectLmtTotalByCusId(String cusId, String instuCde){
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("cusId", cusId);
        queryModel.addCondition("instuCde", instuCde);
        return apprLmtSubBasicInfoMapper.selectLmtTotalByCusId(queryModel);
    }

    /**
     * @作者:lizx
     * @方法名称: selectApprSubSernoArrayByParentId
     * @方法描述:  根据父类编号获取向下分项编号，
     * @参数与返回说明:返回String  中间用","隔开
     * @算法描述: 无
     * @日期：2021/8/28 19:01
     * @param parentId:
     * @return: java.lang.String
     * @算法描述: 无
    */
    public String selectApprSubSernoArrayByParentId(String parentId){
        return apprLmtSubBasicInfoMapper.selectApprSubSernoArrayByParentId(parentId);
    }




    /**
     * 根据额度品种编号和客户号查询客户生效的低风险额度编号额度分项编号
     * @param queryModel
     * @return
     */
    public List<Map<String, String>> selectApprSubSerNoByParam(QueryModel queryModel){
        return apprLmtSubBasicInfoMapper.selectApprSubSerNoByParam(queryModel);
    }

    /**
     * 根据客户号查询客户生效的额度分项编号
     * @param queryModel
     * @return
     */
    public List<String> selectSubNosByCusId(QueryModel queryModel){
        return apprLmtSubBasicInfoMapper.selectSubNosByCusId(queryModel);
    }

    /**
     * @方法名称: batchUpdateAvlAmt
     * @方法描述: 批量处理分项总金额
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int batchUpdateAvlAmt() throws Exception {
        //List<ApprLmtSubBasicInfo> apprLmtSubBasicInfoList = apprLmtSubBasicInfoMapper.selectOneSubInfoByAll()  ;
        try{
            List<String> parentIdList = apprLmtSubBasicInfoMapper.selectParentIdByAll()  ;
            log.info("处理数条数：{}", parentIdList.size());
            for (String parentId : parentIdList) {
                ApprLmtSubBasicInfo parSubInfo = apprLmtSubBasicInfoMapper.selectByApprSubSerno(parentId) ;
                //一级分项总额
                BigDecimal avlAmt = BigDecimalUtil.replaceNull(parSubInfo.getSpacAmt()) ;
                //根据一级分项编号，获取向下二级分项信息
                List<ApprLmtSubBasicInfo> apprLmtSubDetails = apprLmtSubBasicInfoMapper.selectByParentId(parentId) ;
                if(CollectionUtils.isNotEmpty(apprLmtSubDetails)){
                    List<Map<String, Object>> subList = new ArrayList<>();
                    for (ApprLmtSubBasicInfo apprLmtSubDetail : apprLmtSubDetails) {
                        Map<String, Object> map = new HashMap<>();
                        map.put("bailPreRate", apprLmtSubDetail.getBailPreRate()) ;
                        map.put("AvlAmt", apprLmtSubDetail.getAvlAmt()) ;
                        map.put("apprSubSerno", apprLmtSubDetail.getApprSubSerno()) ;
                        subList.add(map);
                    }
                    BigDecimal calAvlAmt = comm4LmtCalFormulaUtils.calAvlLmtTotal(subList, avlAmt) ;
                    log.info("计算后的额度总额:{}", calAvlAmt);
                    parSubInfo.setAvlAmt(calAvlAmt); ;
                    update(parSubInfo) ;
                }
            }
        }catch(Exception e){
            throw new Exception(e) ;
        }
        return 1 ;
    }

    /**
     * 根据客户号查询客户生效的额度分项编号
     * @param
     * @return
     */
    public List<String> selectParentIdByAll(){
        return apprLmtSubBasicInfoMapper.selectParentIdByAll();
    }


    /**
     * 根据客户号查询客户生效的额度分项编号
     * @param
     * @return
     */
    public List<ApprLmtSubBasicInfo> select3003ByCusId(String cusId){
        return apprLmtSubBasicInfoMapper.select3003ByCusId(cusId);
    }

    /**
     * 根据客户号更新分项表中得集团客户号
     * @param
     * @return
     */
    public int updateGrpNoByCusId(Map paramMap){
        return apprLmtSubBasicInfoMapper.updateGrpNoByCusId(paramMap) ;
    }

    /**
     * 根据分项编号更新累加金额
     * @param
     * @return
     */
    public int updateLmtAmtAddBySubSerno(String apprSubSerno){
        return apprLmtSubBasicInfoMapper.updateLmtAmtAddBySubSerno(apprSubSerno) ;
    }


    /**
     * 根据分项编号更新累加金额
     * @param
     * @return
     */
    public int updateLmtAmtAddBySceneOne(String apprSubSerno){
        return apprLmtSubBasicInfoMapper.updateLmtAmtAddBySceneOne(apprSubSerno) ;
    }

    /**
     * 根据分项编号更新累加金额
     * @param
     * @return
     */
    public int updateLmtAmtAddBySceneTwo(String apprSubSerno){
        return apprLmtSubBasicInfoMapper.updateLmtAmtAddBySceneTwo(apprSubSerno) ;
    }

    /**
     * 关联交易计算个人客户余额
     * @param cusId
     * @return
     */
    public BigDecimal selectLmtAmtGeRenGljy(String cusId, String instuCde){
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("cusId", cusId);
        queryModel.addCondition("instuCde", instuCde);
        return apprLmtSubBasicInfoMapper.selectLmtAmtGeRenGljy(queryModel);
    }

    /**
     * 根据客户号和品种编号获取分项下用信余额
     * @param cusId
     * @param limitSubNo
     * @return
     */
    public BigDecimal queryLmtAmtBycusIdAndLimitSubNo(String cusId,String limitSubNo,String proNo){
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("cusId", cusId);
        queryModel.addCondition("limitSubNo", limitSubNo);
        queryModel.addCondition("proNo", proNo);
        return apprLmtSubBasicInfoMapper.queryLmtAmtBycusIdAndLimitSubNo(queryModel);
    }

    /**
     * 根据客户号和品种编号获取分项下状态
     * @param cusId
     * @param limitSubNo
     * @return
     */
    public String queryLmtStatusByProNoOrZqc(String cusId,String limitSubNo,String proNo){
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("cusId", cusId);
        queryModel.addCondition("limitSubNo", limitSubNo);
        queryModel.addCondition("proNo", proNo);
        return apprLmtSubBasicInfoMapper.queryLmtStatusByProNoOrZqc(queryModel);
    }

    /**
     * @作者:lizx
     * @方法名称: selectAvlOutstndAmtByCusId
     * @方法描述:  根据客户号和台账编号，查询向下可出账金额和已出账金额
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/11/22 21:17
     * @param cusId:
     * @param apprSerno:
     * @return: java.util.Map<java.lang.String,java.math.BigDecimal>
     * @算法描述: 无
    */
    public Map<String, BigDecimal> selectAvlOutstndAmtByCusId(String cusId, String apprSerno){
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("cusId", cusId);
        queryModel.addCondition("fkPkid", apprSerno);
        return apprLmtSubBasicInfoMapper.selectAvlOutstndAmtByCusId(queryModel);
    }
}
