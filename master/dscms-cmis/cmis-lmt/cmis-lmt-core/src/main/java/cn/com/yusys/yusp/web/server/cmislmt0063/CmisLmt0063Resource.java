package cn.com.yusys.yusp.web.server.cmislmt0063;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0063.req.CmisLmt0063ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0063.resp.CmisLmt0063RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0063.CmisLmt0063Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据分项编号查询向下非失效状态的分项明细
 *
 * @author lizx 20210909
 * @version 1.0
 */
@Api(tags = "cmislmt0063:根据分项编号查询向下非失效状态的分项明细")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0063Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0063Resource.class);

    @Autowired
    private CmisLmt0063Service cmisLmt0063Service;

    /**
     * 交易码：CmisLmt0063
     * 交易描述：根据分项编号查询向下非失效状态的分项明细
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("根据分项编号查询向下非失效状态的分项明细")
    @PostMapping("/cmislmt0063")
    protected @ResponseBody
    ResultDto<CmisLmt0063RespDto> CmisLmt0063(@Validated @RequestBody CmisLmt0063ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0063.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0063.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0063RespDto> CmisLmt0063RespDtoResultDto = new ResultDto<>();
        CmisLmt0063RespDto CmisLmt0063RespDto = new CmisLmt0063RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0063RespDto = cmisLmt0063Service.execute(reqDto);
            CmisLmt0063RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0063RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0063.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0063.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0063.value, e.getMessage());
            // 封装CmisLmt0063RespDtoResultDto中异常返回码和返回信息
            CmisLmt0063RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0063RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0063RespDto到CmisLmt0063RespDtoResultDto中
        CmisLmt0063RespDtoResultDto.setData(CmisLmt0063RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0063.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0063.value, JSON.toJSONString(CmisLmt0063RespDtoResultDto));
        return CmisLmt0063RespDtoResultDto;
    }
}