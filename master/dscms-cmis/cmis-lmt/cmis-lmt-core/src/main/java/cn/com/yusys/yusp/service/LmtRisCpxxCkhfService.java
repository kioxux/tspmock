/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtRisCpxxCkhf;
import cn.com.yusys.yusp.domain.LmtSubBasicConf;
import cn.com.yusys.yusp.repository.mapper.LmtRisCpxxCkhfMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtRisCpxxCkhfService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-10 08:44:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtRisCpxxCkhfService {

    @Autowired
    private LmtRisCpxxCkhfMapper lmtRisCpxxCkhfMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtRisCpxxCkhf selectByPrimaryKey(String dataDt, String productId) {
        return lmtRisCpxxCkhfMapper.selectByPrimaryKey(dataDt, productId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtRisCpxxCkhf> selectAll(QueryModel model) {
        List<LmtRisCpxxCkhf> records = (List<LmtRisCpxxCkhf>) lmtRisCpxxCkhfMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<Map<String,Object>> selectAllValue(QueryModel model) {
        List<LmtRisCpxxCkhf> records = (List<LmtRisCpxxCkhf>) lmtRisCpxxCkhfMapper.selectByModel(model);
        //返回字典列表
        return records.stream().map((item) -> {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("key", item.getProductId());
            map.put("value", item.getProdName());
            return map;
        }).collect(Collectors.toList());
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtRisCpxxCkhf> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtRisCpxxCkhf> list = lmtRisCpxxCkhfMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtRisCpxxCkhf record) {
        return lmtRisCpxxCkhfMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtRisCpxxCkhf record) {
        return lmtRisCpxxCkhfMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtRisCpxxCkhf record) {
        return lmtRisCpxxCkhfMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtRisCpxxCkhf record) {
        return lmtRisCpxxCkhfMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String dataDt, String productId) {
        return lmtRisCpxxCkhfMapper.deleteByPrimaryKey(dataDt, productId);
    }

}
