/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ManaOrgLmtHis;
import cn.com.yusys.yusp.service.ManaOrgLmtHisService;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ManaOrgLmtHisResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-16 15:32:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/manaorglmthis")
public class ManaOrgLmtHisResource {
    @Autowired
    private ManaOrgLmtHisService manaOrgLmtHisService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<ManaOrgLmtHis>> query() {
        QueryModel queryModel = new QueryModel();
        List<ManaOrgLmtHis> list = manaOrgLmtHisService.selectAll(queryModel);
        return new ResultDto<List<ManaOrgLmtHis>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<ManaOrgLmtHis>> index(QueryModel queryModel) {
        List<ManaOrgLmtHis> list = manaOrgLmtHisService.selectByModel(queryModel);
        return new ResultDto<List<ManaOrgLmtHis>>(list);
    }

    /**
     * @函数名称:selectbymodel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<ManaOrgLmtHis>> selectbymodel(@RequestBody QueryModel queryModel) {
        List<ManaOrgLmtHis> list = manaOrgLmtHisService.selectByModel(queryModel);
        return new ResultDto<List<ManaOrgLmtHis>>(list);
    }

    /**
     * 异步下载分支机构额度管控历史数据
     */
    @PostMapping("/exportmanaorglmthis")
    public ResultDto<ProgressDto> asyncExportManaOrgLmtHis(@RequestBody QueryModel model) {
        ProgressDto progressDto = manaOrgLmtHisService.asyncExportManaOrgLmtHis(model);
        return ResultDto.success(progressDto);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<ManaOrgLmtHis> create(@RequestBody ManaOrgLmtHis manaOrgLmtHis) throws URISyntaxException {
        manaOrgLmtHisService.insert(manaOrgLmtHis);
        return new ResultDto<ManaOrgLmtHis>(manaOrgLmtHis);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ManaOrgLmtHis manaOrgLmtHis) throws URISyntaxException {
        int result = manaOrgLmtHisService.update(manaOrgLmtHis);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String orgId) {
        int result = manaOrgLmtHisService.deleteByPrimaryKey(pkId, orgId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectOrgList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * @算法描述:
     */
    @PostMapping("/selectOrgList")
    protected ResultDto<List<String>> selectOrgList(@RequestBody QueryModel queryModel) {
        List<String> list = manaOrgLmtHisService.selectOrgList(queryModel);
        return new ResultDto<List<String>>(list);
    }

}
