package cn.com.yusys.yusp.service.server.cmislmt0004;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprCoopInfo;
import cn.com.yusys.yusp.domain.ApprCoopSubInfo;
import cn.com.yusys.yusp.domain.ApprStrOrgInfo;
import cn.com.yusys.yusp.domain.LmtContRel;
import cn.com.yusys.yusp.dto.server.cmislmt0004.req.CmisLmt0004LmtSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0004.req.CmisLmt0004ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0004.req.CmisLmt0004StrOrgListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0004.resp.CmisLmt0004RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ApprCoopInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprCoopSubInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprStrOrgInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtContRelMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Supplier;
/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0004Service
 * @类描述: #对内服务类
 * @功能描述: 合作方额度同步
 * @创建时间: 2021-05-07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0004Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0004Service.class);

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private ApprCoopInfoService apprCoopInfoService;

    @Autowired
    private ApprCoopInfoMapper apprCoopInfoMapper;

    @Autowired
    private ApprCoopSubInfoMapper apprCoopSubInfoMapper;

    @Autowired
    private ApprCoopSubInfoService apprCoopSubInfoService;

    @Autowired
    private LmtContRelService lmtContRelService ;

    @Autowired
    private ApprStrOrgInfoService apprStrOrgInfoService;

    @Autowired
    private LmtContRelMapper lmtContRelMapper;

    @Autowired
    private ApprStrOrgInfoMapper apprStrOrgInfoMapper;

    @Autowired
    private Comm4Service comm4Service ;

    @Transactional
    public CmisLmt0004RespDto execute(CmisLmt0004ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0004.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0004.value);
        CmisLmt0004RespDto resqDto = new CmisLmt0004RespDto() ;
        try {
            //批复编号
            String accNo = reqDto.getAccNo() ;

            Map<String, String> paramtMap = (Map<String, String>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(reqDto);
            Map<String, String> resultMap = new HashMap<>() ;
            resultMap.putAll(paramtMap);
            resultMap.put("serno", accNo) ;
            resultMap.put("serviceCode", CmisLmtConstants.CMIS_LMT0004_SERVICE_CODE) ;

            //判断是否生成新批复编号
            String isCreateAcc = reqDto.getIsCreateAcc() ;
            //新建批复主表对象信息
            ApprCoopInfo apprCoopInfo = new ApprCoopInfo() ;
            //拷贝数据
            BeanUtils.copyProperties(reqDto, apprCoopInfo);
            //批复编号
            apprCoopInfo.setApprSerno(reqDto.getAccNo());
            //状态
            apprCoopInfo.setApprStatus(reqDto.getAccStatus());
            //操作类型 默认 01- 新增
            apprCoopInfo.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
            //最新更新人
            apprCoopInfo.setUpdId(reqDto.getInputId());
            //最新更新机构
            apprCoopInfo.setUpdBrId(reqDto.getInputBrId());
            //最新更新日期
            apprCoopInfo.setUpdDate(comm4Service.formatDateStr(reqDto.getInputDate()));
            //修改时间
            apprCoopInfo.setUpdateTime(new Date());
            //批复主表主键
            String pkId = getPkId();
            //非全行适用，适用机构为空不进行拦截
            /*String isWholeBankSuit = reqDto.getIsWholeBankSuit() ;
            if(CmisLmtConstants.YES_NO_N.equals(isWholeBankSuit)){
                if(CollectionUtils.isEmpty(reqDto.getStrOrgList())){
                    throw new YuspException(EclEnum.ECL070138.key, EclEnum.ECL070138.value);
                }
            }*/

            //如果是新增批复
            if(CmisLmtConstants.YES_NO_Y.equals(isCreateAcc)){
                //1.检查数据是否合法
                checkInfoIsVaild(reqDto);
                //2.将原批复台账编号的批复台账状态改为失效未结清
                HashMap<String,Object> hashMap = new HashMap<>();
                hashMap.put("updateTime",new Date());
                hashMap.put("updId",reqDto.getInputId());
                hashMap.put("updBrId",reqDto.getInputBrId());
                hashMap.put("updDate",comm4Service.formatDateStr(reqDto.getInputDate()));
                hashMap.put("apprStatus",CmisLmtConstants.STD_ZB_APPR_ST_99);//失效已结清
                hashMap.put("oprType",CmisLmtConstants.OPR_TYPE_ADD);
                hashMap.put("apprSerNo",reqDto.getOrigiAccNo());
                apprCoopInfoMapper.updateApprCoopInfoByApprSerNo(hashMap);

                //3.插入批复主表信息
                //创建时间
                apprCoopInfo.setCreateTime(new Date());
                //登记人
                apprCoopInfo.setInputId(reqDto.getInputId());
                //登记机构
                apprCoopInfo.setInputBrId(reqDto.getInputBrId());
                //登记日期
                apprCoopInfo.setInputDate(comm4Service.formatDateStr(reqDto.getInputDate()));
                //添加主键
                apprCoopInfo.setPkId(pkId);
                apprCoopInfoMapper.insert(apprCoopInfo);
            }else {
                //查询新批复编号是否存在，存在则变更，不存在则新增
                if (apprCoopInfoService.valiApprIsExistsBySerno(accNo)){
                    //存在则变更
                    String oriPkId = apprCoopInfoService.selectACIBySerno(accNo).getPkId();
                    apprCoopInfo.setPkId(oriPkId);
                    apprCoopInfoMapper.updateByPrimaryKeySelective(apprCoopInfo) ;
                }else {
                    //新增
                    //创建时间
                    apprCoopInfo.setCreateTime(new Date());
                    //登记人
                    apprCoopInfo.setInputId(reqDto.getInputId());
                    //登记机构
                    apprCoopInfo.setInputBrId(reqDto.getInputBrId());
                    //登记日期
                    apprCoopInfo.setInputDate(comm4Service.formatDateStr(reqDto.getInputDate()));
                    //添加主键
                    apprCoopInfo.setPkId(pkId);
                    //插入批复主表信息
                    apprCoopInfoMapper.insert(apprCoopInfo) ;
                }
            }

            /**************处理分项信息********************/
            //利用工厂接口创建实例
            Supplier<ApprCoopSubInfo> supplier = ApprCoopSubInfo::new ;
            reqDto.getLmtSubList().forEach(lmtSub->{
                //新分项编号
                String accSubNo = lmtSub.getAccSubNo() ;
                //获取分项批复实例对象
                ApprCoopSubInfo apprCoopSubInfo = supplier.get() ;
                //拷贝数据
                BeanUtils.copyProperties(lmtSub, apprCoopSubInfo);
                apprCoopSubInfo.setApprSubSerno(lmtSub.getAccSubNo());
                apprCoopSubInfo.setTerm(lmtSub.getTerm());
                apprCoopSubInfo.setUpdateTime(new Date());
                apprCoopSubInfo.setUpdId(reqDto.getInputId());
                apprCoopSubInfo.setUpdBrId(reqDto.getInputBrId());
                apprCoopSubInfo.setUpdDate(comm4Service.formatDateStr(reqDto.getInputDate()));
                apprCoopSubInfo.setLmtBizTypeProp(lmtSub.getLmtBizTypeProp());
                apprCoopSubInfo.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);

                //如果是新增授信分项
                if(CmisLmtConstants.YES_NO_Y.equals(isCreateAcc)){
                    //获取原分项对象
                    ApprCoopSubInfo oriapprCoopSubInfo = apprCoopSubInfoService.selectApprCoopSubInfoByLmtSubNo(lmtSub.getOrigiAccSubNo()) ;

                    /*java.util.List<LmtContRel> lmtContRelList = lmtContRelService.selectOutStandingLoadCont(lmtSub.getOrigiAccSubNo()) ;
                    //原分项批复下的业务挂到新分项下
                    lmtContRelList.forEach(item->{
                        item.setLimitSubNo(lmtSub.getAccSubNo());
                        lmtContRelMapper.updateByPrimaryKey(item) ;
                    });*/
                    //查询原分项下业务，将未结清业务挂靠到新分项下
                    comm4Service.updateLmtContRelRelSubNo(lmtSub.getOrigiAccSubNo(), lmtSub.getAccSubNo());

                    //更改原批复状态为 99-失效已结清
                    oriapprCoopSubInfo.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_99);
                    apprCoopSubInfoService.update(oriapprCoopSubInfo) ;

                    //生成新批复分项信息
                    insertapprCoopSubInfo(apprCoopSubInfo,reqDto);
                }else {
                    ApprCoopSubInfo oldApprCoopSubInfo = apprCoopSubInfoService.selectApprCoopSubInfoByLmtSubNo(accSubNo) ;
                    if(oldApprCoopSubInfo!=null){
                        //变更或删除
                        if (CmisLmtConstants.OPR_TYPE_ADD.equals(lmtSub.getOprType())) {
                            //变更
                            apprCoopSubInfoService.updateBySubSernoInApprLmtChgDetails(apprCoopSubInfo, resultMap) ;
                        }else if (CmisLmtConstants.OPR_TYPE_DELETE.equals(lmtSub.getOprType())) {
                            //删除之前判断是否有占用
                            QueryModel queryModel = new QueryModel();
                            queryModel.addCondition("limitSubNo",lmtSub.getLimitSubNo());

                            if (lmtContRelMapper.selectLmtContRelList(queryModel).size()>0){
                                throw new YuspException(EclEnum.ECL070023.key, EclEnum.ECL070023.value);
                            }

                            apprCoopSubInfoMapper.deleteByApprSubNo(lmtSub.getLimitSubNo());
                        }
                    }else{
                        apprCoopSubInfo.setInputId(reqDto.getInputId());
                        apprCoopSubInfo.setInputBrId(reqDto.getInputBrId());
                        apprCoopSubInfo.setInputDate(comm4Service.formatDateStr(reqDto.getInputDate()));
                        apprCoopSubInfo.setCreateTime(comm4Service.getCurrrentDate());
                        insertapprCoopSubInfo(apprCoopSubInfo,reqDto);
                    }
                }
            }) ;

            /**************处理适用机构信息********************/
            //利用工厂接口创建实例
            Supplier<ApprStrOrgInfo> orgSupplier = ApprStrOrgInfo::new ;
            List<CmisLmt0004StrOrgListReqDto> strOrgList = reqDto.getStrOrgList();
            //先删除，后插入 根据批复流水号删除操作
            apprStrOrgInfoMapper.deleteByApprSerno(reqDto.getAccNo()) ;
            if(!CollectionUtils.isEmpty(strOrgList)){
                for (CmisLmt0004StrOrgListReqDto strOrg : strOrgList) {
                    ApprStrOrgInfo apprStrOrgInfo = orgSupplier.get() ;
                    String oprType = strOrg.getOprType()  ;
                    //先删除后插入，删除的操作不要了
                    if(CmisLmtConstants.OPR_TYPE_DELETE.equals(oprType)) continue ;
                    //操作类型
                    apprStrOrgInfo.setOprType(oprType);
                    //流水号
                    apprStrOrgInfo.setApprSerno(reqDto.getAccNo());
                    //机构编号
                    apprStrOrgInfo.setBchCde(strOrg.getBchCde());

                    //最新更新人
                    apprStrOrgInfo.setUpdId(reqDto.getInputId());
                    //最新更新机构
                    apprStrOrgInfo.setUpdBrId(reqDto.getInputBrId());
                    //最新更新日期
                    apprStrOrgInfo.setUpdDate(reqDto.getInputDate());
                    //修改时间
                    apprStrOrgInfo.setUpdateTime(new Date());

                    //生成主键
                    String orgPkId = getPkId();
                    //设置主键
                    apprStrOrgInfo.setPkId(orgPkId);
                    //登记人
                    apprStrOrgInfo.setInputId(reqDto.getInputId());
                    //登记机构
                    apprStrOrgInfo.setInputBrId(reqDto.getInputBrId());
                    //登记日期
                    apprStrOrgInfo.setInputDate(comm4Service.formatDateStr(reqDto.getInputDate()));
                    //创建时间
                    apprStrOrgInfo.setCreateTime(comm4Service.getCurrrentDate());
                    apprStrOrgInfoMapper.insert(apprStrOrgInfo);

                    //如果是新增适用机构
                /*if(CmisLmtConstants.YES_NO_Y.equals(isCreateAcc)){
                    //生成主键
                    orgPkId = getPkId();
                    //设置主键
                    apprStrOrgInfo.setPkId(orgPkId);
                    //登记人
                    apprStrOrgInfo.setInputId(reqDto.getInputId());
                    //登记机构
                    apprStrOrgInfo.setInputBrId(reqDto.getInputBrId());
                    //登记日期
                    apprStrOrgInfo.setInputDate(reqDto.getInputDate());
                    //创建时间
                    apprStrOrgInfo.setCreateTime(new Date());
                    apprStrOrgInfoMapper.insert(apprStrOrgInfo);
                }else {
                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("serno",accNo);
                    queryModel.addCondition("bchCde",apprStrOrgInfo.getBchCde());

                    orgPkId = apprStrOrgInfoService.selectByModel(queryModel).get(0).getPkId();
                    //新增则更新
                    if (CmisLmtConstants.OPR_TYPE_ADD.equals(apprStrOrgInfo.getOprType())){
                        apprStrOrgInfo.setPkId(orgPkId);
                        apprStrOrgInfoMapper.updateByPrimaryKeySelective(apprStrOrgInfo);
                    }else if (CmisLmtConstants.OPR_TYPE_DELETE.equals(apprStrOrgInfo.getOprType())){
                        //删除
                        apprStrOrgInfoService.deleteByPrimaryKey(orgPkId);
                    }
                }*/
                }
            }

            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("合作方额度同步接口报错：",e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0004.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0004.value);
        return resqDto;
    }

    /**
     * 1.检查原批复编号是否存在，不存在抛异常
     * 2.检查新批复编号是否存在，存在则抛异常
     * 3.检查新的批复分项编号是否存在，已存在则抛异常;检查原批复分项编号是否存在，不存在则抛异常
     * @param reqDto
     */
    private void checkInfoIsVaild(CmisLmt0004ReqDto reqDto){
        //1.检查新的合作方台账编号是否存在，已存在则抛异常
        if(apprCoopInfoService.valiApprIsExistsBySerno(reqDto.getAccNo())){
            throw new YuspException(EclEnum.ECL070021.key, EclEnum.ECL070021.value);
        }

        //2.检查原批复台账编号是否存在，不存在则抛异常
        if(!apprCoopInfoService.valiApprIsExistsBySerno(reqDto.getOrigiAccNo())){
            throw new YuspException(EclEnum.ECL070016.key, EclEnum.ECL070016.value);
        }

        //3.检查新的批复分项编号是否存在，已存在则抛异常;检查原批复分项编号是否存在，不存在则抛异常
        java.util.List<CmisLmt0004LmtSubListReqDto> lmtSubList = reqDto.getLmtSubList();

        if (!lmtSubList.isEmpty()){
            java.util.List<String> accSubNoList = new ArrayList<String>();
            java.util.List<String> origiAccSubNoList = new ArrayList<String>();

            for (CmisLmt0004LmtSubListReqDto lmtSub : lmtSubList) {
                accSubNoList.add(lmtSub.getAccSubNo());
                origiAccSubNoList.add(lmtSub.getOrigiAccSubNo());
            }

            String apprSubSernos = apprCoopSubInfoMapper.selectAccSubNoByArray(accSubNoList);

            if (StringUtils.isNotEmpty(apprSubSernos)){
                throw new YuspException(EclEnum.ECL070022.key, "额度同步，批复分项["+apprSubSernos+"]已经存在，不允许重新生成");
            }

            apprSubSernos = apprCoopSubInfoMapper.selectAccSubNoByArray(origiAccSubNoList);

            if (StringUtils.isEmpty(apprSubSernos)){
                throw new YuspException(EclEnum.ECL070007.key, EclEnum.ECL070007.value);
            }
        }
    }

    /**
     * 生成主键
     * @return 返回主键
     */
    private String getPkId(){
        Map paramMap= new HashMap<>() ;
        return sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, paramMap);
    }

    /**
     * 插入批复额度分项表
     * @param apprCoopSubInfo
     * @param reqDto
     */
    private void insertapprCoopSubInfo(ApprCoopSubInfo apprCoopSubInfo, CmisLmt0004ReqDto reqDto){
        //状态
        apprCoopSubInfo.setStatus(reqDto.getAccStatus());
        //操作类型 默认 01- 新增
        apprCoopSubInfo.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        //登记人
        apprCoopSubInfo.setInputId(reqDto.getInputId());
        //登记机构
        apprCoopSubInfo.setInputBrId(reqDto.getInputBrId());
        //登记时间
        apprCoopSubInfo.setInputDate(comm4Service.formatDateStr(reqDto.getInputDate()));
        //创建时间
        apprCoopSubInfo.setCreateTime(comm4Service.getCurrrentDate());
        //批复台账流水号
        apprCoopSubInfo.setApprSerno(reqDto.getAccNo());
        //贷款余额 默认为0
        apprCoopSubInfo.setLoanBalance(BigDecimal.ZERO);
        //已用总额 默认为0
        apprCoopSubInfo.setOutstndAmt(BigDecimal.ZERO);
        //生成主键
        String subPkId = getPkId();
        //设置主键
        apprCoopSubInfo.setPkId(subPkId);

        apprCoopSubInfoMapper.insert(apprCoopSubInfo);
    }
}