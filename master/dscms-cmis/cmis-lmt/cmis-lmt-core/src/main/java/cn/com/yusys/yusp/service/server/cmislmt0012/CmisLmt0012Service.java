package cn.com.yusys.yusp.service.server.cmislmt0012;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprLmtChgDetail;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.domain.LmtContRel;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.LmtContRelMapper;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.server.comm.Comm4LmtCalFormulaUtils;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0012Service
 * @类描述: #对内服务类
 * @功能描述: 额度占用
 * @创建人: 李召星
 * @创建时间: 2021-05-07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0012Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0012Service.class);

    @Autowired
    private LmtContRelService lmtContRelService ;
    @Autowired
    private ContAccRelService contAccRelService ;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;

    @Autowired
    private LmtContRelMapper lmtContRelMapper ;

    @Autowired
    private Comm4Service comm4Service ;

    @Autowired
    private Comm4LmtCalFormulaUtils comm4LmtCalFormulaUtils ;

    @Transactional
    public CmisLmt0012RespDto execute(CmisLmt0012ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0012.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0012.value);
        CmisLmt0012RespDto resqDto = new CmisLmt0012RespDto() ;
        try {
            Map<String, String> paramMap = (Map<String, String>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(reqDto);
            //合同编号
            String bizNo = reqDto.getBizNo() ;
            if(StringUtils.isBlank(bizNo)){
                throw new YuspException(EclEnum.ECL070135.key,EclEnum.ECL070135.value);
            }

            Map<String, String> resultMap = new HashMap<>() ;
            resultMap.putAll(paramMap);
            resultMap.put("serno", bizNo) ;
            resultMap.put("serviceCode", CmisLmtConstants.CMIS_LMT0012_SERVICE_CODE) ;

            //恢复类型
            String recoverType = reqDto.getRecoverType() ;
            //是否恢复合作方额度
            String isRecoverCoop = reqDto.getIsRecoverCoop() ;
            if(StringUtils.isBlank(isRecoverCoop)) isRecoverCoop = CmisLmtConstants.YES_NO_Y ;
            //如果是空，则默认未是，恢复合作方额度
            //根据合同获取合同编号
            List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelListByDealBizNo(bizNo, CmisLmtConstants.STD_ZB_BIZ_ATTR_1);
            if(lmtContRelList==null || lmtContRelList.size()<=0){
                throw new YuspException(EclEnum.ECL070018.key,EclEnum.ECL070018.value);
            }

            for (LmtContRel lmtContRel : lmtContRelList) {
                logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0012.key+"恢复处理，对应合同信息：【{}】", JSON.toJSONString(lmtContRel)) ;
                String lmtType = lmtContRel.getLmtType() ;
                //合作方额度 是否恢复合作方额度类型未0-否  不恢复，跳过
                if(CmisLmtConstants.STD_ZB_LMT_TYPE_03.equals(lmtType) && CmisLmtConstants.YES_NO_N.equals(isRecoverCoop)){
                    continue;
                }
                //分项编号
                String apprSubSerno = lmtContRel.getLimitSubNo() ;
                //贷款还清后，进行合同注销操作为结清恢复，此处不回复额度分项金额，因为还款的时候，会根据情况对额度金额进行恢复
                if(CmisLmtConstants.STD_RECOVER_TYPE_01.equals(recoverType)){
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0012.key+"结清恢复开始") ;
                    //判断合同项下是否存在未结清业务，如存在，则提示不允许注销合同
                    String flag = contAccRelService.checkDealBizNoHasBussBalanceFlag(bizNo);
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0012.key+"判断交易业务编号项下是否存在未结清的业务（结清恢复）：flag"+flag) ;
                    if(!"0".equals(flag)){
                        throw new YuspException(EclEnum.ECL070134.key,EclEnum.ECL070134.value);
                    }
                    //恢复合同金额 ，结清恢复，不需要处理分项已用总额和已用敞口总额
                    comm4Service.settleRecover(lmtContRel, resultMap, isRecoverCoop) ;
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0012.key+"结清结束开始") ;
                    comm4LmtCalFormulaUtils.calLmtAmtAddExecute(apprSubSerno);
                }else if(CmisLmtConstants.STD_RECOVER_TYPE_02.equals(recoverType)){
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0012.key+"未用注销开始") ;
                    //未用注销，合同未经使用，注销合同，合同状态更改为结清未使用，恢复分项相关金额，不考虑分项是否可循环

                    //判断合同项下是否存在未结清业务，如存在，则提示不允许注销合同
                    String flag = contAccRelService.checkDealBizNoHasBussBalanceFlag(bizNo);
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0012.key+"判断交易业务编号项下是否存在未结清的业务（未用注销）：flag"+flag) ;
                    if(!"0".equals(flag)){
                        throw new YuspException(EclEnum.ECL070134.key,EclEnum.ECL070134.value);
                    }
                    comm4Service.updateSubInfoAmt(apprSubSerno, lmtContRel, resultMap) ;
                    extractLmtContRel(resultMap, lmtContRel, CmisLmtConstants.STD_ZB_BIZ_STATUS_400);
                    //更新批复分项记录里的授信总额累加的值
                    comm4LmtCalFormulaUtils.calLmtAmtAddExecute(apprSubSerno);
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0012.key+"未用注销结束") ;
                }else if(CmisLmtConstants.STD_RECOVER_TYPE_06.equals(recoverType)){
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0012.key+"撤销占用开始") ;
                    //撤销占用，删除合同信息，操作类型更改为02 删除，恢复分项金额，合同不考虑
                    comm4Service.updateSubInfoAmt(apprSubSerno, lmtContRel, resultMap) ;
                    lmtContRel.setBizTotalBalanceAmtCny(BigDecimal.ZERO);
                    lmtContRel.setBizSpacBalanceAmtCny(BigDecimal.ZERO);
                    lmtContRel.setOprType(CmisLmtConstants.OPR_TYPE_DELETE);
                    lmtContRelService.updateByPKeyInApprLmtChgDetails(lmtContRel, resultMap) ;
                    comm4LmtCalFormulaUtils.calLmtAmtAddExecute(apprSubSerno);
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0012.key+"撤销占用结束") ;
                }else if(CmisLmtConstants.STD_RECOVER_TYPE_05.equals(recoverType)) {
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0012.key+"押品出库开始") ;
                    //押品出库，恢复白名单额度，合同更改为结清已使用
                    lmtContRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_300);
                    lmtContRelMapper.updateByPrimaryKey(lmtContRel);
                    comm4Service.recoverWhiteInfo(apprSubSerno, lmtContRel, resultMap) ;
                    comm4LmtCalFormulaUtils.calLmtAmtAddExecute(apprSubSerno);
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0012.value+"押品出库结束") ;
                }else if(CmisLmtConstants.STD_RECOVER_TYPE_08.equals(recoverType)){
                    //合同提前终止
                    logger.info("合同提前终止【{}】----------开始处理" ,bizNo);
                    comm4Service.bizRevExecute(bizNo, resultMap);
                    logger.info("合同提前终止【{}】----------结束处理" ,bizNo);
                }else{
                    logger.info("未知的恢复类型状态，不给予处理！！");
                }
            }
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("合同恢复接口报错：", e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0012.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0012.value);
        return resqDto;
    }

    private void extractLmtContRel(Map<String, String> resultMap, LmtContRel lmtContRel, String BizStatus) {
        //更改合同状态为 ：结清未使用
        lmtContRel.setBizTotalBalanceAmtCny(BigDecimal.ZERO);
        lmtContRel.setBizSpacBalanceAmtCny(BigDecimal.ZERO);
        lmtContRel.setBizStatus(BizStatus);
        lmtContRelService.updateByPKeyInApprLmtChgDetails(lmtContRel, resultMap) ;
    }
}
