package cn.com.yusys.yusp.web.server.cmislmt0029;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.req.CmisLmt0029ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.resp.CmisLmt0029RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0029.CmisLmt0029Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:额度分项信息查询
 *
 * @author dumd 20210615
 * @version 1.0
 */
@Api(tags = "cmislmt0029:更新台账编号")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0029Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0029Resource.class);

    @Autowired
    private CmisLmt0029Service cmisLmt0029Service;

    /**
     * 交易码：CmisLmt0029
     * 交易描述：更新台账编号
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("更新台账编号")
    @PostMapping("/cmislmt0029")
    protected @ResponseBody
    ResultDto<CmisLmt0029RespDto> CmisLmt0029(@Validated @RequestBody CmisLmt0029ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0029.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0029RespDto> CmisLmt0029RespDtoResultDto = new ResultDto<>();
        CmisLmt0029RespDto CmisLmt0029RespDto = new CmisLmt0029RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0029RespDto = cmisLmt0029Service.execute(reqDto);
            CmisLmt0029RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0029RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (YuspException yuspe) {
            logger.error("更新台账编号" , yuspe);
            CmisLmt0029RespDto.setErrorCode(yuspe.getCode());
            CmisLmt0029RespDto.setErrorMsg(yuspe.getMsg());
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0029.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0029.value, e.getMessage());
            // 封装CmisLmt0029RespDtoResultDto中异常返回码和返回信息
            CmisLmt0029RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0029RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0029RespDto到CmisLmt0029RespDtoResultDto中
        CmisLmt0029RespDtoResultDto.setData(CmisLmt0029RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.value, JSON.toJSONString(CmisLmt0029RespDtoResultDto));
        return CmisLmt0029RespDtoResultDto;
    }
}