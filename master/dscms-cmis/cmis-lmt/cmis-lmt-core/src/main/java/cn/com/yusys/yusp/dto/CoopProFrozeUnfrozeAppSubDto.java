package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.CoopProFrozeUnfrozeApp;
import cn.com.yusys.yusp.domain.CoopProFrozeUnfrozeSub;

import java.util.List;

public class CoopProFrozeUnfrozeAppSubDto {

    private CoopProFrozeUnfrozeApp coopProFrozeUnfrozeApp;
    private List<CoopProFrozeUnfrozeSub> coopProFrozeUnfrozeSub;


    public CoopProFrozeUnfrozeApp getCoopProFrozeUnfrozeApp() {
        return coopProFrozeUnfrozeApp;
    }

    public void setCoopProFrozeUnfrozeApp(CoopProFrozeUnfrozeApp coopProFrozeUnfrozeApp) {
        this.coopProFrozeUnfrozeApp = coopProFrozeUnfrozeApp;
    }

    public List<CoopProFrozeUnfrozeSub> getCoopProFrozeUnfrozeSub() {
        return coopProFrozeUnfrozeSub;
    }

    public void setCoopProFrozeUnfrozeSub(List<CoopProFrozeUnfrozeSub> coopProFrozeUnfrozeSub) {
        this.coopProFrozeUnfrozeSub = coopProFrozeUnfrozeSub;
    }
}
