package cn.com.yusys.yusp.web.server.cmislmt0049;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0049.req.CmisLmt0049ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0049.resp.CmisLmt0049RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0049.CmisLmt0049Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据机构编号获取分支机构额度管控信息
 *
 * @author zhangjw 2021/7/14
 * @version 1.0
 */
@Api(tags = "cmislmt0049:根据机构编号获取分支机构额度管控信息")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0049Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0049Resource.class);

    @Autowired
    private CmisLmt0049Service cmisLmt0049Service;
    /**
     * 交易码：cmislmt0049
     * 交易描述：根据机构编号获取分支机构额度管控信息
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("根据机构编号获取分支机构额度管控信息")
    @PostMapping("/cmislmt0049")
    protected @ResponseBody
    ResultDto<CmisLmt0049RespDto> cmisLmt0049(@Validated @RequestBody CmisLmt0049ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0049.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0049.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0049RespDto> cmisLmt0049RespDtoResultDto = new ResultDto<>();
        CmisLmt0049RespDto cmisLmt0049RespDto = new CmisLmt0049RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0049RespDto = cmisLmt0049Service.execute(reqDto);
            cmisLmt0049RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0049RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);

        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0049.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0049.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0049.value, e.getMessage());
            // 封装xddb0049DataResultDto中异常返回码和返回信息
            cmisLmt0049RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0049RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0016RespDto到cmisLmt0016RespDtoResultDto中
        cmisLmt0049RespDtoResultDto.setData(cmisLmt0049RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0049.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0049.value, JSON.toJSONString(cmisLmt0049RespDtoResultDto));
        return cmisLmt0049RespDtoResultDto;
    }
}
