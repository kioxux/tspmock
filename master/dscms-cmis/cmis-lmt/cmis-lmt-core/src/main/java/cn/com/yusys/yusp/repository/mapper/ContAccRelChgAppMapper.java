/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.ContAccRelChgApp;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ContAccRelChgAppMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-20 20:39:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface ContAccRelChgAppMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    ContAccRelChgApp selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<ContAccRelChgApp> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(ContAccRelChgApp record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(ContAccRelChgApp record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(ContAccRelChgApp record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(ContAccRelChgApp record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: updateByParams
     * @方法描述: 根据主键逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByParams(Map delMap);

    /**
     * @方法名称: updateByAppSerno
     * @方法描述: 根据AppSerno逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByAppSerno(Map delMap);

    /**
     * @方法名称: selectByAppSerno
     * @方法描述: 根据调整主申请流水号获取
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<ContAccRelChgApp> selectByAppSerno(String appSerno);
    /**
     * 检查合同编号是否存在
     * @param dealBizNo
     * @return
     */
    int checkSameDealBizNoIsExistAll(@Param("dealBizNo") String dealBizNo);

    /**
     * 检查台账编号是否存在
     * @param tranAccNo
     * @return
     */
    int checkSameTranAccNoIsExistAll(@Param("tranAccNo") String tranAccNo);
}