/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.dto.ApprLmtSubBasicInfoDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprLmtSubBasicInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: LQC
 * @创建时间: 2021-04-01 20:40:32
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface ApprLmtSubBasicInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    ApprLmtSubBasicInfo selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<ApprLmtSubBasicInfo> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insert(ApprLmtSubBasicInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(ApprLmtSubBasicInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(ApprLmtSubBasicInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(ApprLmtSubBasicInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @作者：李召星
     * @方法名称: selectBylimitSubNo
     * @方法描述: 根据分项品种编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    ApprLmtSubBasicInfo selectByApprSubSerno(@Param("apprSubSerno") String apprSubSerno);

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据分项编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    ApprLmtSubBasicInfo selectBySerno(@Param("serno") String serno);
    
    List<ApprLmtSubBasicInfo> getSubByFkPkidList(@Param("list") List<String> pkIdList);

    List<ApprLmtSubBasicInfo> getSubByFkPkid(@Param("fkPkid") String fkPkid);

    List<ApprLmtSubBasicInfoDto> selectByRecord(ApprLmtSubBasicInfoDto apprLmtSubBasicInfoDto);//查询额度分项信息

    /**
     * 获取客户额度视图列表
     * @param queryModel
     * @return
     */
    List<Map<String, Object>> selectLmtCusInfoList(QueryModel queryModel);

    /**
     * 获取客户额度视图列表
     * @param queryModel
     * @return
     */
    List<Map<String, Object>> selectLmtSigZhInfoList(QueryModel queryModel);

    /**
     * 查询同业客户产品授信列表
     * @param queryModel
     * @return
     */
    List<Map<String, Object>> selectLmtCusPrdInfoList(QueryModel queryModel);

    List<Map<String, Object>> selectHasChildren(QueryModel queryModel);

    List<Map<String, Object>> selectHasChildren4SameOrg(QueryModel queryModel);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    Map<String, BigDecimal> selectLmtSubTotal(QueryModel queryModel);


    List<ApprLmtSubBasicInfoDto> selectSumAmtByArray(@Param("list") List<String> cusIds);

    /**
     * @方法名称: selectAccSubNoByArray
     * @方法描述: 根据批复分项编号查询表里有哪些分项已经存在
     * @参数与返回说明:
     * @算法描述: 无
     */
    String selectAccSubNoByArray(@Param("apprSubSernos") List<String> apprSubSernos);


    /**
     * @方法名称: updateBySerno
     * @方法描述: 根据客户编号修改状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByFkPkid(Map params);

    /**
     * @方法名称: updateBySerno
     * @方法描述: 根据授信分项编号修改状态(失效已结清/失效未结清)
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateBySerno(Map params);

    /**
     * @方法名称: getStatusByFkPkid
     * @方法描述: 根据批复台账编号查询分项状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<ApprLmtSubBasicInfo> getStatusByFkPkid(@Param("fkPkid") String fkPkid);

    /**
     * @方法名称: updateBySerno
     * @方法描述: 根据授信分项编号修改状态(失效已结清/失效未结清)
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateStatusBySerno(Map params);

    /**
     * @方法名称: getApprSubSernoByCusId
     * @方法描述: 根据客户编号查询分项状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<ApprLmtSubBasicInfo> getApprSubSernoByCusId(@Param("cusId") String cusId);

    /**
     * @方法名称: updateByApprSubSerno
     * @方法描述: 根据授信分项编号修改信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByApprSubSerno(ApprLmtSubBasicInfo record);


    /**
     * @方法名称: deleteByApprSubNo
     * @方法描述: 根据授信分项编号删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByApprSubNo(String apprSubNo);

    /**
     * 根据客户号列表查询客户额度分项信息
     * @param cusIdList
     * @return
     */
    List<Map<String, Object>> selectLmtSubAccListByCusIdList(@Param("cusIdList") List<String> cusIdList);

    /**
     * @方法名称: updateOprTypeByApprSubSerno
     * @方法描述: 根据授信分项编号逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateOprTypeByApprSubSerno(String apprSubNo);


    /**
     * 根据批复台账编号列表查询授信总额等信息
     * @param apprSernos
     * @return
     */
    Map<String, BigDecimal> selectLmtAmtByApprSernos(@Param("apprSernos") List<String> apprSernos);

    /**
     * 根据客户编号查询批复分项信息
     * @param queryMap
     * @return
     */
    List<Map<String,Object>> selectLmtInfoByCusList(Map queryMap);

    /**
     * 查询个人新微贷额度信息
     * @param queryModel
     * @return
     */
    List<Map<String,Object>> selectLmtInfoByQueryModel(QueryModel queryModel);

    /**
     * 查询额度分项下的产品分项里的最小可用金额与最小敞口可用金额
     * @param parentId
     * @return
     */
    Map<String,BigDecimal> selectMinAvailAndSpacAmt(@Param("parentId") String parentId);

    /**
     * 根据外键查询分项记录数
     * @param fkPkid
     * @return
     */
    int countRecordByFkPkid(@Param("fkPkid") String fkPkid);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<ApprLmtSubBasicInfo> selectSubBasicInfoByPool(QueryModel queryModel) ;

    /**
     * 根据额度品种编号和客户号查询客户的额度分项编号
     * @param queryModel
     * @return
     */
    String selectApprSubSernoByCusIdAndLmtSubNo(QueryModel queryModel);

    /**
     * 根据批复台账编号查询名下的批复分项信息
     * @param fkPkid
     * @return
     */
    List<ApprLmtSubBasicInfo> selectByFkPkid(@Param("fkPkid") String fkPkid);

    /**
     * 根据批复台账编号将批复分项的状态由生效改为冻结
     * @param fkPkid
     * @return
     */
    int updateStatusByFkPkid(@Param("fkPkid") String fkPkid);

    /**
     * @方法名称: selectSbuListByParentId
     * @方法描述: 根据父级编号，查询父级向下分项明细
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<ApprLmtSubBasicInfo> selectSbuListByParentId(QueryModel queryModel) ;


    /**
     * 根据批复台账编号将批复分项的状态由生效改为冻结
     * @param queryModel
     * @return
     */
    BigDecimal selectLmtType04And05LmtBal(QueryModel queryModel) ;

    /**
     * 根据批复台账编号将批复分项的状态由生效改为冻结
     * @param queryModel
     * @return
     */
    BigDecimal selectLmtType04And05UseBal(QueryModel queryModel) ;

    /**
     * 根据批复台账编号将批复分项的状态由生效改为冻结
     * @param queryModel
     * @return
     */
    int countApprLmtSubBasicInfoByQueryModel(QueryModel queryModel);

    /**
     * @方法名称: selectSubUnInvalidByParentId
     * @方法描述: 根据父级编号，查询向下是否存在失效未结清的业务
     * @参数与返回说明:
     * @算法描述: 无
     */
    int selectSubUnInvalidByParentId(@Param("parentId") String parentId) ;

    /**
     * @方法名称: selectSubUnInvalidByParentId
     * @方法描述: 根据父级编号，查询向下是否存在失效未结清的业务
     * @参数与返回说明:
     * @算法描述: 无
     */
    int selectSubUnInvalidByFkPkid(@Param("parentId") String parentId) ;

    /**
     * 根据产品编号获取客户额度视图列表
     * @param
     * @return
     */
    List<Map<String, Object>> selectLmtCusInfoListByPrdIdList(QueryModel queryModel);

    /**
     * @方法名称: selectApprSubSernoArrayByParentId
     * @方法描述:
     * @参数与返回说明:
     * @算法描述: 无
     */
    String selectApprSubSernoArrayByParentId(@Param("parentId") String parentId);

    /**
     * 根据客户号查，返回该客户下非失效已结清状态授信总额
     * @param queryModel
     * @return
     */
    Map<String,BigDecimal> selectLmtTotalByCusId(QueryModel queryModel) ;

    /**
     * @方法名称: updateFApprSubStatus
     * @方法描述: 分项状态为什么则更新为什么
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateApprSubStatus(Map params);




    /**
     * @方法名称: selectOnbSubByApprSerno
     * @方法描述: 根据批复台账，查询向下分项信息（不包含分项明细）
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<ApprLmtSubBasicInfo> selectOnbSubByApprSerno(@Param("fkPkid") String fkPkid) ;



    /**
     * 根据额度品种编号和客户号查询客户生效的低风险额度编号额度分项编号
     * @param queryModel
     * @return
     */
    List<Map<String, String>> selectApprSubSerNoByParam(QueryModel queryModel) ;




    /**
     * 根据客户号查询客户生效的额度分项编号
     * @param queryModel
     * @return
     */
    List<String> selectSubNosByCusId(QueryModel queryModel) ;

    /**
     * 根据客户号查询客户生效的额度分项编号
     * @return
     */
    List<ApprLmtSubBasicInfo> selectOneSubInfoByAll() ;

    /**
     * 根据客户号查询客户生效的额度分项编号
     * @return
     */
    List<ApprLmtSubBasicInfo> selectByParentId(@Param("parentId") String parentId) ;


    /**
     * 根据客户号查询客户生效的额度分项编号
     * @param
     * @return
     */
    List<String> selectParentIdByAll() ;


    /**
     * 查询同业票据类额度信息
     * @return
     */
    List<ApprLmtSubBasicInfo> select3003ByCusId(@Param("cusId") String cusId) ;

    /**
     * 根据客户号更新分项表中得集团客户号
     * @return
     */
    int updateGrpNoByCusId(Map param);

    /**
     * @方法名称: selectByPrdAndPraParam
     * @方法描述: 根据分项编号，额度产品编号，产品类型属性，查询分项明细
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<ApprLmtSubBasicInfo> selectByPrdAndPraParam(QueryModel model);

    /**
     * @方法名称: updateLmtAmtAddBySubSerno
     * @方法描述: 根据分项编号，初始化分项累加金额
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateLmtAmtAddBySubSerno(@Param("apprSubSerno") String apprSubSerno) ;

    /**
     * @方法名称: updateLmtAmtAddBySubSerno
     * @方法描述: 根据分项编号，按照场景1 ，更新累加金额
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateLmtAmtAddBySceneOne(@Param("apprSubSerno") String apprSubSerno) ;

    /**
     * @方法名称: updateLmtAmtAddBySubSerno
     * @方法描述: 根据分项编号，按照场景2 ，更新累加金额
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateLmtAmtAddBySceneTwo(@Param("apprSubSerno") String apprSubSerno) ;

    /**
     * 关联交易计算个人客户余额
     * @param queryModel
     * @return
     */
    BigDecimal selectLmtAmtGeRenGljy(QueryModel queryModel) ;

    /**
     * 根据客户号和品种编号获取分项下用信余额
     * @param queryModel
     * @return
     */
    BigDecimal queryLmtAmtBycusIdAndLimitSubNo(QueryModel queryModel) ;

    /**
     * 根据客户号和品种编号获取分项状态
     * @param queryModel
     * @return
     */
    String queryLmtStatusByProNoOrZqc(QueryModel queryModel) ;


    /**
     * 根据客户号和台账编号，查询向下可出账金额和已出账金额
     * @return
     */
    Map<String, BigDecimal> selectAvlOutstndAmtByCusId(QueryModel queryModel) ;
}