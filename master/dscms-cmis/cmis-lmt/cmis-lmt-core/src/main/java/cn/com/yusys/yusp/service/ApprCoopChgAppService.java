/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprCoopChgAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: yuanz
 * @创建时间: 2021-04-20 22:35:33
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ApprCoopChgAppService {

    private static final Logger log = LoggerFactory.getLogger(ApprCoopChgAppService.class);

    @Autowired
    private ApprCoopChgAppMapper apprCoopChgAppMapper;

    @Autowired
    private ApprCoopInfoService apprCoopInfoService;//申请主表

    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private ApprCoopSubChgAppService apprCoopSubChgAppService;

    @Autowired
    private ApprCoopSubChgAppMapper apprCoopSubChgAppMapper;

    @Autowired
    private LmtContRelChgAppService lmtContRelChgAppService;

    @Autowired
    private LmtContRelChgAppMapper lmtContRelChgAppMapper;

    @Autowired
    private ContAccRelChgAppService contAccRelChgAppService;

    @Autowired
    private ContAccRelChgAppMapper contAccRelChgAppMapper;

    @Autowired
    private ApprCoopInfoMapper apprCoopInfoMapper;

    @Autowired
    private ApprCoopSubInfoService apprCoopSubInfoService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ApprCoopChgApp selectByPrimaryKey(String pkId) {
        return apprCoopChgAppMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<ApprCoopChgApp> selectAll(QueryModel model) {
        List<ApprCoopChgApp> records = (List<ApprCoopChgApp>) apprCoopChgAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<ApprCoopChgApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ApprCoopChgApp> list = apprCoopChgAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(ApprCoopChgApp record) {
        return apprCoopChgAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(ApprCoopChgApp record) {
        return apprCoopChgAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(ApprCoopChgApp record) {
        return apprCoopChgAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(ApprCoopChgApp record) {
        return apprCoopChgAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return apprCoopChgAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return apprCoopChgAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int logicDelete(String pkId) {
        //逻辑删除操作参数
        Map delMap = new HashMap();
        delMap.put("pkId", pkId);
        //获取业务标识位，通过标识位进行逻辑删除操作
        delMap.put("oprType", CmisLmtConstants.OPR_TYPE_DELETE);
        ApprCoopChgApp record = apprCoopChgAppMapper.selectByPrimaryKey(pkId);
        delMap.put("appSerno", record.getAppSerno());
        QueryModel queryModel = new QueryModel();
        //批复编号
        queryModel.addCondition("appSerno", record.getAppSerno());
        //查询批复信息
        List<ApprCoopSubChgApp> apprCoopSubChgAppList = apprCoopSubChgAppMapper.selectByModel(queryModel);
        List<LmtContRelChgApp> lmtContRelChgAppList = lmtContRelChgAppMapper.selectByModel(queryModel);
        List<ContAccRelChgApp> contAccRelChgAppList = contAccRelChgAppMapper.selectByModel(queryModel);
        //批复是否为空
        if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(apprCoopSubChgAppList)) {
            apprCoopSubChgAppMapper.updateByAppSerno(delMap);
        }
        //批复是否为空
        if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(lmtContRelChgAppList)) {
            lmtContRelChgAppMapper.updateByAppSerno(delMap);
        }
        //批复是否为空
        if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(contAccRelChgAppList)) {
            contAccRelChgAppMapper.updateByAppSerno(delMap);
        }
        return apprCoopChgAppMapper.updateByParams(delMap);
    }

    /**
     * @方法名称: valiLmtChgAppInWay
     * @方法描述: 根据批复编号，查询批复是否存在在途申请
     * @参数与返回说明: 如果返回记录大于1 则存在在途记录，否则不存在
     * @算法描述: 无
     */

    public int valiLmtChgAppInWay(String serno) {
        ApprCoopChgApp apprCoopChgApp = new ApprCoopChgApp();
        //设置批复编号
        apprCoopChgApp.setApprSerno(serno);
        return apprCoopChgAppMapper.valiLmtChgAppInWay(apprCoopChgApp);
    }

    /**
     * 额度项下业务修改保存
     * 保存合作方授信台账信息
     *
     * @param params
     */
    @Transactional(rollbackFor = Exception.class)
    public Map updateApprCoopChgApp(Map params) {
        Map rtnData = new HashMap();
        String rtnCode = SuccessEnum.SUCCESS.key;
        String rtnMsg = SuccessEnum.SUCCESS.value;
        ApprCoopChgApp apprCoopChgApp = new ApprCoopChgApp();
        try {
            log.info("修改数据信息开始！获取请求入参数据");
            String lmtSerno = (String) params.get("lmtSerno");
            //获取申请主表主键，通过主键查询是否已经存在，若是已经存在则抛出异常
            log.info("保存合作方授信台账调整申请" + lmtSerno + "-查询数据是否已存在");
            if (this.selectByPrimaryKey(lmtSerno) != null) {
                rtnCode = EclEnum.ECL070031.key;
                rtnMsg = EclEnum.ECL070031.value;
                return rtnData;
            }
            ApprCoopInfo apprCoopInfo = apprCoopInfoService.selectBySerno(lmtSerno);
            Map apprCoopInfoMap = JSONObject.parseObject(JSON.toJSONString(apprCoopInfo, SerializerFeature.WriteMapNullValue), Map.class);
            Map apprCoopChgAppMap = new HashMap();
            if (apprCoopInfoMap != null) {
                Set<String> keySet = apprCoopInfoMap.keySet();
                for (String key : keySet) {
                    if (params.containsKey(key)) {
                        apprCoopChgAppMap.put(key, params.get(key));
                    }
                }
                apprCoopChgApp = JSONObject.parseObject(JSON.toJSONString(apprCoopChgAppMap), ApprCoopChgApp.class);
            }


            log.info("修改" + lmtSerno + "额度项下申请信息-获取当前登录用户数据");
            // User userInfo = SessionUtils.getUserInformation();
            // if(userInfo==null){
            //     throw new YuspException(EclEnum.ECL070032.key, EclEnum.ECL070032.value);
            // }else{
            //     apprCoopChgApp.setUpdId(userInfo.getLoginCode());
            //     apprCoopChgApp.setUpdBrId(userInfo.getOrg().getCode());
            //     apprCoopChgApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            // }

            int apprCoopChgAppCount = 0;
            apprCoopChgAppCount = this.insertSelective(apprCoopChgApp);
            if (apprCoopChgAppCount < 0) {
                throw new YuspException(EclEnum.ECL070033.key, EclEnum.ECL070033.value);
            }

            log.info("修改" + lmtSerno + "合作方授信台账调整申请！");

        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }


    /**
     * @return
     * @方法名称: createApprCoopChgApp
     * @方法描述: 获取批复数据，根据批复台账主表信息，生成批复调整申请信息
     * @参数与返回说明:返回影响数据记录，等于0 操作失败
     * @算法描述: 无
     */

    public ApprCoopChgApp createApprCoopChgApp(ApprCoopInfo apprCoopInfo) {
        //获取批复编号
        String apprSerno = apprCoopInfo.getApprSerno();
        //根据额度编号
        ApprCoopInfo newapprCoopInfo = apprCoopInfoMapper.selectBySerno(apprSerno);
        Optional.ofNullable(newapprCoopInfo).orElseThrow(()->new YuspException(EclEnum.ECL070016.key,EclEnum.ECL070016.value));
        //生成主键
        HashMap<String, String> param = new HashMap<>();
        String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, new HashMap<>());
        //生成流水号
        String appSerno = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.LMT_SERNO, new HashMap<>());

        ApprCoopChgApp apprCoopChgApp = new ApprCoopChgApp();

        //将批复台账中的数据拷贝到批复调整申请中
        BeanUtils.copyProperties(newapprCoopInfo, apprCoopChgApp);
        //设置主键
        apprCoopChgApp.setPkId(pkValue);
        //设置新的申请流水号
        apprCoopChgApp.setAppSerno(appSerno);
        //设置批复编号
        apprCoopChgApp.setApprSerno(apprSerno);
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        // 申请人
        apprCoopChgApp.setInputId(userInfo.getLoginCode());
        // 申请机构
        apprCoopChgApp.setInputBrId(userInfo.getOrg().getCode());
        // 申请时间
        apprCoopChgApp.setInputDate(DateUtils.getCurrDateStr());
        // 申请时间
        apprCoopChgApp.setInputDate(DateUtils.getCurrDateStr());
        // 更新人
        apprCoopChgApp.setUpdId(userInfo.getLoginCode());
        // 更新机构
        apprCoopChgApp.setUpdBrId(userInfo.getOrg().getCode());
        //更新日期
        apprCoopChgApp.setUpdDate(DateUtils.getCurrDateStr());
        //修改时间
        apprCoopChgApp.setUpdateTime(new Date());
        // 创建时间
        apprCoopChgApp.setCreateTime(new Date());
        // 审批状态
        apprCoopChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
        return apprCoopChgApp;
    }

    /**
     * @方法名称: initApprCoopChgAppInfo
     * @方法描述: 初始化额度调账变更信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map initApprCoopChgAppInfo(ApprCoopInfo apprCoopInfo) {
        HashMap<String, String> resultMap = new HashMap();
        ApprCoopChgApp apprCoopChgApp = createApprCoopChgApp(apprCoopInfo);
        String apprSerno = apprCoopChgApp.getApprSerno();
        String pkId = apprCoopChgApp.getPkId();
        String appSerno = apprCoopChgApp.getAppSerno();
        //数据插入操作
        apprCoopChgAppMapper.insert(apprCoopChgApp);
        //初始化批复批复变更分项信息
        List<ApprCoopSubChgApp> apprCoopSubChgApps = apprCoopSubChgAppService.createApprCoopSubChgApp(apprSerno, pkId);
        //遍历处理
        for (ApprCoopSubChgApp apprCoopSubChgApp : apprCoopSubChgApps) {
            //初始申请信息
            apprCoopSubChgApp.setSubAppSerno(appSerno);
            //数据落表处理
            apprCoopSubChgAppMapper.insert(apprCoopSubChgApp);
            //获取合作方分项编号
            String limitSubNo = apprCoopSubChgApp.getApprSubSerno();
            if (!StringUtils.isEmpty(limitSubNo)) {
                //根据分项编号处理分项占用信息  以及  合同占用关系
                lmtContRelChgAppService.createLmtcontRelChgApp(limitSubNo,appSerno);
            }
        }
        resultMap.put("appSerno", appSerno);
        return resultMap ;
    }

    /**
     * 合作方客户额度调整申请  审批通过后处理
     * 1、更新合作方额度主信息不变 最近更新人  最近更新机构  最近更新时间  修改时间
     * 2、更新合作方额度分项信息已用金额等金额 最近更新人  最近更新机构  最近更新时间  修改时间
     * 3、更新合作方分项占用关系数据
     * 4、更新合作方台账占用关系数据
     *
     * @param appSerno credit by zhangjw 2021-04-26
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterEnd(String appSerno) {
        try {

            log.info("审批通过更新合作方客户额度主信息：获取合作方客户额度调整申请" + appSerno + "主表信息");
            ApprCoopChgApp apprCoopChgApp = apprCoopChgAppMapper.selectByAppSerno(appSerno);
            if (apprCoopChgApp == null) {
                throw new YuspException(EclEnum.ECL070042.key, EclEnum.ECL070042.value);
            }
            String apprSerno = apprCoopChgApp.getApprSerno();//批复台账编号
            Map params = new HashMap();
            params.put("updId", apprCoopChgApp.getUpdId());
            params.put("updBrId", apprCoopChgApp.getUpdBrId());
            params.put("updDate", apprCoopChgApp.getUpdDate());
            params.put("updBrId", apprCoopChgApp.getUpdBrId());
            params.put("updateTime", DateUtils.getCurrTimestamp());

            log.info("审批通过更新合作方客户额度主信息，批复台账编号【" + apprSerno + "】");
            int apprStrMtableInfo = apprCoopInfoMapper.updateApprCoopInfoByParams(params);
            if (apprStrMtableInfo < 0) {
                throw new YuspException(EclEnum.ECL070043.key, EclEnum.ECL070043.value);
            }
            log.info("审批通过更新合作方客户额度分项信息_获取额度分项调整信息" + appSerno);
            Map rtnData = new HashMap();
            rtnData.put("subAppSerno", appSerno);

            //根据主申请流水号获取批复额度分项调整申请信息
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("subAppSerno", appSerno);
            List<ApprCoopSubChgApp> apprCoopSubChgAppList = apprCoopSubChgAppService.selectAll(queryModel);

            /*分项调整申请信息不为空时，循环更新批复额度分项基础信息*/
            if (CollectionUtils.nonEmpty(apprCoopSubChgAppList)) {
                for (ApprCoopSubChgApp apprCoopSubChgApp : apprCoopSubChgAppList) {

                    String apprSubSerno = apprCoopSubChgApp.getApprSubSerno();//获取分项调整申请的批复分项编号

                    //根据合作方分项调整申请的批复分项编号 获取 合作方额度分项基础信息   更新合作方额度分项基础信息
                    ApprCoopSubInfo apprCoopSubInfo = apprCoopSubInfoService.selectBySubSerno(apprSubSerno);

                    if (apprCoopSubInfo == null || "".equals(apprCoopSubInfo.getPkId())) {
                        throw new YuspException(EclEnum.ECL070044.key, EclEnum.ECL070044.value);
                    }

                    apprCoopSubInfo.setOutstndAmt(apprCoopSubChgApp.getOutstndAmt());
                    apprCoopSubInfo.setLoanBalance(apprCoopSubChgApp.getLoanBalance());
                    apprCoopSubInfoService.update(apprCoopSubInfo);
                }
            }

            /*处理分项占用关系数据*/
            lmtContRelChgAppService.handleBusinessDataAfterEnd(appSerno);

            /*处理合同占用关系数据*/
            contAccRelChgAppService.handleBusinessDataAfterEnd(appSerno);

            log.info("审批通过更新合作方客户额度调整申请" + appSerno + "审批状态为【997】-审批通过");
            apprCoopChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
            apprCoopChgAppMapper.updateApproveStatus(apprCoopChgApp);
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("流程审批通过业务处理发生异常！", e);
        }
    }

    /**
     * @方法名称: updateApproveStatus
     * @方法描述: 流程申请状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateApproveStatus(ApprCoopChgApp apprCoopChgApp) {
        return apprCoopChgAppMapper.updateApproveStatus(apprCoopChgApp);
    }

    /**
     * @param
     * @函数名称:querySubChgAppBySerno
     * @函数描述:根据调整申请流水号和合作方批复台账编号加工展示合作方调整申请主信息
     * @参数与返回说明:
     * @算法描述:
     */
    public List<Map<String, Object>> querySubChgAppBySerno(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String, Object>> list = apprCoopChgAppMapper.querySubChgAppBySerno(model);
        PageHelper.clearPage();
        return list;
    }
}
