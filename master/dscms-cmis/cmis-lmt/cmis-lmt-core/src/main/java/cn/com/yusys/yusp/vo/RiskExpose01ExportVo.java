package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

@ExcelCsv(namePrefix = "单一客户风险暴露查询", fileType = ExcelCsv.ExportFileType.XLS)
public class RiskExpose01ExportVo {

    /**
     * 指标名称
     */
    @ExcelField(title = "指标名称", viewLength = 20 ,dictCode = "STD_DE_RISK_TYPE")
    private String typeName;

    /**
     * 指标限额要求（%）
     */
    @ExcelField(title = "指标限额要求（%）", viewLength = 20)
    private BigDecimal riskIndexReq;

    /**
     * 指标值
     */
    @ExcelField(title = "指标值（万元）", viewLength = 20, format = "#0.00")
    private BigDecimal zbLmt;

    /**
     * 授信总额（万元）
     */
    @ExcelField(title = "授信总额（万元）", viewLength = 20, format = "#0.00")
    private BigDecimal sumSxLmt;

    /**
     * 用信余额（万元）
     */
    @ExcelField(title = "用信余额（万元）", viewLength = 20, format = "#0.00")
    private BigDecimal sumYxLmt;

    /**
     * 指标日期
     */
    @ExcelField(title = "指标日期", viewLength = 20)
    private String zbDate;

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public BigDecimal getRiskIndexReq() {
        return riskIndexReq;
    }

    public void setRiskIndexReq(BigDecimal riskIndexReq) {
        this.riskIndexReq = riskIndexReq;
    }

    public BigDecimal getZbLmt() {
        return zbLmt;
    }

    public void setZbLmt(BigDecimal zbLmt) {
        this.zbLmt = zbLmt;
    }

    public BigDecimal getSumYxLmt() {
        return sumYxLmt;
    }

    public void setSumYxLmt(BigDecimal sumYxLmt) {
        this.sumYxLmt = sumYxLmt;
    }

    public BigDecimal getSumSxLmt() {
        return sumSxLmt;
    }

    public void setSumSxLmt(BigDecimal sumSxLmt) {
        this.sumSxLmt = sumSxLmt;
    }

    public String getZbDate() {
        return zbDate;
    }

    public void setZbDate(String zbDate) {
        this.zbDate = zbDate;
    }
}
