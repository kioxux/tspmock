/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtSubBasicConf;
import cn.com.yusys.yusp.domain.LmtSubPrdMappConf;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoTranDto;
import cn.com.yusys.yusp.enums.online.DscmsCfgEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSubBasicConfMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSubPrdMappConfMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.server.cmiscfg0002.req.CmisCfg0002ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0002.resp.CmisCfg0002ListRespDto;
import cn.com.yusys.yusp.server.cmiscfg0002.resp.CmisCfg0002RespDto;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtSubBasicConfService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-02 17:08:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSubBasicConfService {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtSubBasicConfService.class);

    @Autowired
    private LmtSubBasicConfMapper lmtSubBasicConfMapper;

    @Autowired
    private LmtSubPrdMappConfService lmtSubPrdMappConfService;
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private DscmsCfgQtClientService dscmsCfgQtClientService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public LmtSubBasicConf selectByPrimaryKey(String pkId) {
        return lmtSubBasicConfMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */
    @Transactional(readOnly=true)
    public List<LmtSubBasicConf> selectAll(QueryModel model) {
        List<LmtSubBasicConf> records = (List<LmtSubBasicConf>) lmtSubBasicConfMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSubBasicConf> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSubBasicConf> list = lmtSubBasicConfMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int insert(LmtSubBasicConf record) {
        return lmtSubBasicConfMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int insertSelective(LmtSubBasicConf record) {
        return lmtSubBasicConfMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int update(LmtSubBasicConf record) {
        return lmtSubBasicConfMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int updateSelective(LmtSubBasicConf record) {
        return lmtSubBasicConfMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return lmtSubBasicConfMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtSubBasicConfMapper.deleteByIds(ids);
    }



    /**
     * @param root, all]
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     * @author tangxun
     * @date 2021/5/7
     * @version 1.0.0
     * @desc 组装前端要求的格式
     * @修改历史: zhangjw
     */
    private List<Map<String,Object>> getTree(String root, Map<String, ArrayList<LmtSubBasicConf>> all) {
        if(all.containsKey(root)){
            return all.get(root).stream().map(item->{
                Map<String,Object> map = new LinkedHashMap<>();
                map.put("label", item.getLimitSubName());
                map.put("id", item.getLimitSubNo());
                map.put("parentId",item.getParentId());
                map.put("lmtSubType",item.getLmtSubType());//额度类型：01-额度分项、02-产品分项
                List list = getTree(item.getLimitSubNo(),all);
                if(!list.isEmpty()){
                    map.put("children",list);
                }
                return map;
            }).collect(Collectors.toList());
        }else{
            return new ArrayList<Map<String,Object>>();
        }


    }

    /**
     * 获取额度结构树
     * add by zhangjw 2021-05-07
     * */
    public List<Map<String,Object>> selectTreeList(Map map) {
        String limitStrNo = (String) map.get("limitStrNo");
        QueryModel model = new QueryModel();
        model.addCondition("limitStrNo",limitStrNo);
        model.addCondition("oprType",CmisLmtConstants.OPR_TYPE_ADD);
        List<LmtSubBasicConf> list = lmtSubBasicConfMapper.selectByModel(model);
        //将所有对象放到map中 格式是 <pid,List<Node>>
        Map<String, ArrayList<LmtSubBasicConf>> allMap = new HashMap<>(16);

        String rootLimitSubNo = "";
        String rootLimitSubName = "";

        for (LmtSubBasicConf item:list) {
            if(allMap.containsKey(item.getParentId())){
                allMap.get(item.getParentId()).add(item);
            }else{
                ArrayList<LmtSubBasicConf> sublist = new ArrayList<>();
                sublist.add(item);
                allMap.put(item.getParentId(),sublist);
            }
            if("".equals(item.getParentId())){
                rootLimitSubNo = item.getLimitSubNo();
                rootLimitSubName = item.getLimitSubName();
            }
        }
        //补充根节点：
//        List<Map<String,Object>> resList = new ArrayList<>();
//        Map<String,Object> rootMap = new HashMap<>();
//        rootMap.put("id",rootLimitSubNo);
//        rootMap.put("label",rootLimitSubName);
//        rootMap.put("parentId","");
//        rootMap.put("children",getTree(limitStrNo,allMap));
//        resList.add(rootMap);
        return getTree("",allMap);
    }

    /**
     * 选取框获取额度结构树专用
     *
     * */
    public List<Map<String,Object>> selectLmtsubTree(Map map) {
        String limitStrNo = (String) map.get("limitStrNo");
        QueryModel model = new QueryModel();
        model.addCondition("limitStrNo",limitStrNo);
        model.addCondition("oprType",CmisLmtConstants.OPR_TYPE_ADD);
        List<LmtSubBasicConf> list = lmtSubBasicConfMapper.selectByModel(model);
        //将所有对象放到map中 格式是 <pid,List<Node>>
        Map<String, ArrayList<LmtSubBasicConf>> allMap = new HashMap<>(16);

        for (LmtSubBasicConf item:list) {
            if(allMap.containsKey(item.getParentId())){
                if (Objects.isNull(map.get("limitType"))) {
                    allMap.get(item.getParentId()).add(item);
                } else if (map.get("limitType").equals(item.getLimitType())) {
                    allMap.get(item.getParentId()).add(item);
                }
            }else{
                ArrayList<LmtSubBasicConf> sublist = new ArrayList<>();
                sublist.add(item);
                if (Objects.isNull(map.get("limitType"))) {
                    allMap.put(item.getParentId(),sublist);
                } else if (StringUtil.isEmpty(item.getParentId())) {
                    allMap.put(item.getParentId(),sublist);
                } else if (map.get("limitType").equals(item.getLimitType())) {
                    allMap.put(item.getParentId(),sublist);
                }
            }
        }
        return getTree("",allMap);
    }

    /**
     * @函数名称:queryConfInfoByLimitSubNo
     * @函数描述:根据额度品种编号查询额度品种基础信息
     * @参数与返回说明:
     * @param map
     * zhangjw
     */
    public List<LmtSubBasicConf> queryConfInfoByLimitSubNo(Map map) {
        QueryModel model = new QueryModel();
        model.addCondition("limitSubNo",map.get("limitSubNo"));
        List<LmtSubBasicConf> list = lmtSubBasicConfMapper.selectByModel(model);
        return list;
    }

    /**
     * @函数名称:deleteConfInfoByLimitSubNo
     * @函数描述:根据额度品种编号删除额度品种（包含下级节点删除）
     * @参数与返回说明:
     * @param map
     * zhangjw  2021-05-07
     */
    public int deleteConfInfoByLimitSubNo(Map map) {
        QueryModel model = new QueryModel();
        int reuslt = 0;
        String limitSubNo = (String)map.get("limitSubNo");
        //实现递归删除
        model.addCondition("limitSubNo",limitSubNo);
        model.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        List<LmtSubBasicConf> list = lmtSubBasicConfMapper.selectByModel(model);

        //保存子节点数据
        ArrayList<LmtSubBasicConf> childList = new ArrayList<>();
        if(list!=null && list.size()>0){
            childList = queryListByParentId(limitSubNo);
            list.addAll(childList);

            String limitSubNos = " ,";
            for(LmtSubBasicConf item: list ){
                String id = item.getLimitSubNo();
                limitSubNos = limitSubNos +","+ id ;
            }
            reuslt = lmtSubBasicConfMapper.updateConfInfoByLimitSubNo(limitSubNos);
        }
        return reuslt;
    }

    /**
     * 根据额度品种编号查询该额度品种编号下子节点数据
     * @param parentId
     * @return
     */
    public ArrayList<LmtSubBasicConf> queryListByParentId(String parentId){
        QueryModel model = new QueryModel();
        model.addCondition("parentId",parentId);
        model.addCondition("oprType","01");
        List<LmtSubBasicConf> list = lmtSubBasicConfMapper.selectByModel(model);

        //保存结果数据
        ArrayList<LmtSubBasicConf> allList = new ArrayList<>(16);

        if(list!=null && list.size()>0){
            allList.addAll(list);
            for(LmtSubBasicConf item:list){
                parentId = item.getLimitSubNo();
                ArrayList<LmtSubBasicConf> childList = queryListByParentId(parentId);
                allList.addAll(childList);
            }
        }
        return allList;
    }

    /**
     * @函数名称:createConfInfoByLimitSubNo
     * @函数描述:新增节点
     * @参数与返回说明:
     * @param map
     * zhangjw  2021-05-07
     */
    public int createConfInfoByLimitSubNo(Map map) {
        LmtSubBasicConf lmtSubBasicConf = new LmtSubBasicConf();
        User userInfo = SessionUtils.getUserInformation();

        lmtSubBasicConf.setPkId(sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, new HashMap<>()));
        lmtSubBasicConf.setLimitStrNo((String) map.get("limitStrNo"));
        lmtSubBasicConf.setLimitSubNo((String) map.get("limitSubNo"));
        lmtSubBasicConf.setLimitSubName((String) map.get("limitSubName"));
        lmtSubBasicConf.setParentId((String) map.get("parentId"));
        lmtSubBasicConf.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        lmtSubBasicConf.setInputId(userInfo.getLoginCode());
        lmtSubBasicConf.setInputBrId(userInfo.getOrg().getCode());
        lmtSubBasicConf.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        lmtSubBasicConf.setUpdId(userInfo.getLoginCode());
        lmtSubBasicConf.setUpdBrId(userInfo.getOrg().getCode());
        lmtSubBasicConf.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        lmtSubBasicConf.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME),
                DateFormatEnum.DATETIME.getValue()));
        lmtSubBasicConf.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));

        int reuslt = lmtSubBasicConfMapper.insert(lmtSubBasicConf);
        return reuslt;
    }

    /**
     * @函数名称:updateConfInfoByLimitSubNo
     * @函数描述:更新节点
     * @参数与返回说明:
     * @param
     */
    public int updateConfInfoByLimitSubNo(Map map) {
        String limitSubNo = (String)map.get("limitSubNo");
        String limitStrNo = (String)map.get("limitStrNo");
        String parentId = (String)map.get("parentId");

        QueryModel model = new QueryModel();
        model.addCondition("limitSubNo",limitSubNo);
        model.addCondition("oprType",CmisLmtConstants.OPR_TYPE_ADD);
        model.addCondition("limitStrNo",limitStrNo);
        model.addCondition("parentId",parentId);
        List<LmtSubBasicConf> list = lmtSubBasicConfMapper.selectByModel(model);

        if(list!=null && list.size()>0){
            LmtSubBasicConf lmtSubBasicConf = list.get(0);
            User userInfo = SessionUtils.getUserInformation();

            lmtSubBasicConf.setLimitSubName((String) map.get("limitSubName"));
            lmtSubBasicConf.setParentId(parentId);
            lmtSubBasicConf.setIsLriskLmt((String) map.get("isLriskLmt"));
            lmtSubBasicConf.setLmtSubType((String) map.get("lmtSubType"));
            lmtSubBasicConf.setIsRevolv((String) map.get("isRevolv"));
            lmtSubBasicConf.setLimitType((String) map.get("limitType"));
            lmtSubBasicConf.setIsExpertLmt((String) map.get("isExpertLmt"));
            lmtSubBasicConf.setSuitBizLine((String) map.get("suitBizLine"));
            lmtSubBasicConf.setIsOverLmtTerm((String)map.get("isOverLmtTerm"));

            lmtSubBasicConf.setUpdId(userInfo.getLoginCode());
            lmtSubBasicConf.setUpdBrId(userInfo.getOrg().getCode());
            lmtSubBasicConf.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtSubBasicConf.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));

            int reuslt = lmtSubBasicConfMapper.updateByPrimaryKey(lmtSubBasicConf);
            return reuslt;
        }
        return 0;
    }

    @Autowired
    private LmtSubPrdMappConfMapper lmtSubPrdMappConfMapper;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    /***
     * @param limitSubNo 额度编号
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     * @author tangxun
     * @date 2021/5/8 1:14 下午
     * @version 1.0.0
     * @desc 查询已选择的产品
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<CfgPrdBasicinfoTranDto> selectCheckTreeList(String limitSubNo) {
        QueryModel model = new QueryModel();
        model.addCondition("limitSubNo", limitSubNo);
        Set<String> prdIdset = lmtSubPrdMappConfMapper.selectByModel(model)
                .stream().map(LmtSubPrdMappConf::getPrdId).collect(Collectors.toSet());
        Map<String, String> param = new HashMap<>(3);
        ResultDto<List<CfgPrdBasicinfoTranDto>> res = iCmisCfgClientService.queryPrdTree(param);
        List<CfgPrdBasicinfoTranDto> list = res.getData();
        //设置选中
        setCheck(list, prdIdset);
        return list;
    }

    /***
     * @param list
     * @param prdIdset
     * @return void
     * @author tangxun
     * @date 2021/5/8 1:56 下午
     * @version 1.0.0
     * @desc 遍历返回的对象 设置选中状态
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private void setCheck(List<CfgPrdBasicinfoTranDto> list, Set<String> prdIdset) {
        for (CfgPrdBasicinfoTranDto cfgPrdBasicinfoTranDto : list) {
            if (prdIdset.contains(cfgPrdBasicinfoTranDto.getId())) {
                cfgPrdBasicinfoTranDto.setCheck("true");
            }
            setCheck(Optional.ofNullable(cfgPrdBasicinfoTranDto.getChildren()).orElse(new ArrayList<>()), prdIdset);
        }
    }

    /**
     * 根据查询条件获取额度结构树
     * add by zhangjw 2021--07-14
     * */
    public List<Map<String,Object>> selectTreeListBySelect(Map map) {
        QueryModel model = new QueryModel();
        model.setCondition(JSON.toJSONString(map));
        model.addCondition("oprType",CmisLmtConstants.OPR_TYPE_ADD);
        List<LmtSubBasicConf> list = lmtSubBasicConfMapper.selectByModel(model);
        //将所有对象放到map中 格式是 <pid,List<Node>>
        Map<String, ArrayList<LmtSubBasicConf>> allMap = new HashMap<>(16);

        String rootLimitSubNo = "";
        String rootLimitSubName = "";

        for (LmtSubBasicConf item:list) {
            if(allMap.containsKey(item.getParentId())){
                allMap.get(item.getParentId()).add(item);
            }else{
                ArrayList<LmtSubBasicConf> sublist = new ArrayList<>();
                sublist.add(item);
                allMap.put(item.getParentId(),sublist);
            }
            if("".equals(item.getParentId())){
                rootLimitSubNo = item.getLimitSubNo();
                rootLimitSubName = item.getLimitSubName();
            }
        }
        return getTree("",allMap);
    }

    /***
     * @param limitSubNo 额度编号
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     * @author tangxun
     * @date 2021/5/8 1:14 下午
     * @version 1.0.0
     * @desc 查询已选择的产品
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<CmisCfg0002ListRespDto> queryDetailBylimitSubNo(String limitSubNo) {

        Set<String> prdIdset = lmtSubPrdMappConfMapper.selectPrdIdByLimitSubNo(limitSubNo)
                .stream().map(LmtSubPrdMappConf::getPrdId).collect(Collectors.toSet());

        List<CmisCfg0002ListRespDto> list = null ;
        if(!prdIdset.isEmpty()){
            for(String prdId : prdIdset){
                CmisCfg0002ReqDto reqDto = new CmisCfg0002ReqDto();
                reqDto.setPrdId(prdId);

                log.info("根据产品编号【{}】获取产品扩展属性！", prdId);
                ResultDto<CmisCfg0002RespDto> res = dscmsCfgQtClientService.cmisCfg0002(reqDto);
                log.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0002.key,
                        DscmsCfgEnum.TRADE_CODE_CMISCFG0002.value, JSON.toJSONString(res));

                CmisCfg0002RespDto resDate = res.getData();
                if (res != null && SuccessEnum.SUCCESS.key.equals(res.getData().getErrorCode())) {
                    log.info("根据产品编号【{}】获取产品扩展属性成功！", prdId);
                    if(resDate.getCfgPrdTypePropertiesList()!=null && resDate.getCfgPrdTypePropertiesList().size()>0){
                        list = resDate.getCfgPrdTypePropertiesList();
                    }

                } else {
                    log.info("根据产品编号【{}】获取产品扩展属性失败！", prdId);
                }
            }
        }
        return list;
    }

}
