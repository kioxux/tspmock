/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtWhiteInfo
 * @类描述: lmt_white_info数据实体类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-05-03 10:31:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_white_info")
public class LmtWhiteInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** PK_ID **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 金融机构代码 **/
	@Column(name = "INSTU_CDE", unique = false, nullable = true, length = 10)
	private String instuCde;
	
	/** 额度分项编号 **/
	@Column(name = "SUB_ACC_NO", unique = false, nullable = true, length = 40)
	private String subAccNo;
	
	/** 白名单额度类型STD_ZB_LMT_WHITE_TYPE **/
	@Column(name = "LMT_WHITE_TYPE", unique = false, nullable = true, length = 5)
	private String lmtWhiteType;
	
	/** 客户号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 总行行号 **/
	@Column(name = "AORG_NO", unique = false, nullable = true, length = 40)
	private String aorgNo;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 机构类型STD_ZB_INTB_SUB_TYPE **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 10)
	private String cusType;
	
	/** 限额 **/
	@Column(name = "SIG_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sigAmt;
	
	/** 单个产品户买入返售限额 **/
	@Column(name = "SINGLE_RESALE_QUOTA", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal singleResaleQuota;
	
	/** 已用限额 **/
	@Column(name = "SIG_USE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sigUseAmt;
	
	/** 生效日期 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 20)
	private String startDate;
	
	/** 到期日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 20)
	private String endDate;
	
	/** 操作类型STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param instuCde
	 */
	public void setInstuCde(String instuCde) {
		this.instuCde = instuCde;
	}
	
    /**
     * @return instuCde
     */
	public String getInstuCde() {
		return this.instuCde;
	}
	
	/**
	 * @param subAccNo
	 */
	public void setSubAccNo(String subAccNo) {
		this.subAccNo = subAccNo;
	}
	
    /**
     * @return subAccNo
     */
	public String getSubAccNo() {
		return this.subAccNo;
	}
	
	/**
	 * @param lmtWhiteType
	 */
	public void setLmtWhiteType(String lmtWhiteType) {
		this.lmtWhiteType = lmtWhiteType;
	}
	
    /**
     * @return lmtWhiteType
     */
	public String getLmtWhiteType() {
		return this.lmtWhiteType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param aorgNo
	 */
	public void setAorgNo(String aorgNo) {
		this.aorgNo = aorgNo;
	}
	
    /**
     * @return aorgNo
     */
	public String getAorgNo() {
		return this.aorgNo;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param sigAmt
	 */
	public void setSigAmt(java.math.BigDecimal sigAmt) {
		this.sigAmt = sigAmt;
	}
	
    /**
     * @return sigAmt
     */
	public java.math.BigDecimal getSigAmt() {
		return this.sigAmt;
	}
	
	/**
	 * @param singleResaleQuota
	 */
	public void setSingleResaleQuota(java.math.BigDecimal singleResaleQuota) {
		this.singleResaleQuota = singleResaleQuota;
	}
	
    /**
     * @return singleResaleQuota
     */
	public java.math.BigDecimal getSingleResaleQuota() {
		return this.singleResaleQuota;
	}
	
	/**
	 * @param sigUseAmt
	 */
	public void setSigUseAmt(java.math.BigDecimal sigUseAmt) {
		this.sigUseAmt = sigUseAmt;
	}
	
    /**
     * @return sigUseAmt
     */
	public java.math.BigDecimal getSigUseAmt() {
		return this.sigUseAmt;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}