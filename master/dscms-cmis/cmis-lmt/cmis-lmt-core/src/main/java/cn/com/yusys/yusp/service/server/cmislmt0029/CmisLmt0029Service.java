package cn.com.yusys.yusp.service.server.cmislmt0029;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.ApprLmtChgFiledListDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.req.CmisLmt0029ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.resp.CmisLmt0029RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ApprCoopSubInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprLmtSubBasicInfoMapper;
import cn.com.yusys.yusp.service.ApprCoopSubInfoService;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.ContAccRelService;
import cn.com.yusys.yusp.service.LmtContRelService;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

@Service
public class CmisLmt0029Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0029Service.class);

    @Autowired
    private LmtContRelService lmtContRelService;
    @Autowired
    private ContAccRelService contAccRelService;
    @Autowired
    private Comm4Service comm4Service;
    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;
    @Autowired
    private ApprCoopSubInfoMapper apprCoopSubInfoMapper ;
    @Autowired
    private ApprCoopSubInfoService apprCoopSubInfoService ;

    /**
     * 更新台账编号
     * add by dumd 20210615
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0029RespDto execute(CmisLmt0029ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.value);
        CmisLmt0029RespDto respDto = new CmisLmt0029RespDto();
        Map<String, String> paramMap = (Map<String, String>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(reqDto);
        Map<String, String> resultMap = new HashMap<>() ;
        resultMap.putAll(paramMap);
        resultMap.put("serviceCode", CmisLmtConstants.CMIS_LMT0029_SERVICE_CODE) ;

        //交易流水号
        String dealBizNo = reqDto.getDealBizNo();
        //原台账编号
        String originAccNo = reqDto.getOriginAccNo();
        //新台账编号
        String newAccNo = reqDto.getNewAccNo();
        //起始日期
        String startDate = reqDto.getStartDate() ;
        //到期日
        String endDate = reqDto.getEndDate() ;
        //是否出账成功通知
        String isPvpSucs = reqDto.getIsPvpSucs() ;

        resultMap.put("serno", dealBizNo) ;
        //无论什么情况，调用该接口，原编号肯定不为空
        if(StringUtils.isEmpty(originAccNo)){
            throw new YuspException(EclEnum.ECL070123.key, EclEnum.ECL070123.value) ;
        }

        ContAccRel contAccRel = contAccRelService.selectContAccRelByTranAccNo(originAccNo);
        if (Objects.nonNull(contAccRel)){
            //新台账编号
            if(StringUtils.isNotEmpty(newAccNo)) contAccRel.setTranAccNo(newAccNo);
            //新起始日期
            if(StringUtils.isNotEmpty(startDate)) contAccRel.setStartDate(startDate);
            //新到期日
            if(StringUtils.isNotEmpty(endDate)) contAccRel.setEndDate(endDate);
            //最近更新日期
            contAccRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            //最近更新人
            contAccRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            //台账状态 100
            String bizStatus = contAccRel.getStatus() ;
            logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0029.key + "出账通知接口，是否出账通知{}【1-是 0-否】,台账状态{}【100-未生效，200-已生效】", isPvpSucs, bizStatus);
            //是否出账成功通知 是否
            if(CmisLmtConstants.YES_NO_Y.equals(isPvpSucs) && CmisLmtConstants.STD_ZB_BIZ_STATUS_100.equals(bizStatus)){
                resultMap.put("inputId", contAccRel.getInputId()) ;
                resultMap.put("inputBrId", contAccRel.getInputBrId()) ;
                resultMap.put("inputDate", DateUtils.getCurrentDate(DateFormatEnum.DATETIME)) ;
                //占用状态更改为生效状态
                contAccRel.setStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_200);
                contAccRelService.update(contAccRel) ;
                //用信产品编号
                String prdId = contAccRel.getPrdId() ;
                //合同编号
                String bizNo = contAccRel.getDealBizNo() ;
                //占用总额
                BigDecimal dealBizAmtCny = BigDecimalUtil.replaceNull(contAccRel.getAccTotalAmtCny()) ;
                //占用敞口金额
                BigDecimal dealBizSpacAmtCny = BigDecimalUtil.replaceNull(contAccRel.getAccSpacAmtCny()) ;
                //贴现限额累加处理
                comm4Service.lmtLastDiscOrgAddUseAmt(resultMap, dealBizAmtCny, prdId, contAccRel.getInputBrId());
                //根据合同编号，查询合同信息
                List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelByDealBizNo(bizNo);
                if(CollectionUtils.isNotEmpty(lmtContRelList)){
                    for (LmtContRel lmtContRel : lmtContRelList) {
                        //进行台账占用，更改台账对应相关分项额度信息
                        String lmtType = lmtContRel.getLmtType() ;
                        //交易业务类型    1-一般合同  2-最高额合同  3-最高额授信协议
                        String dealBizType = lmtContRel.getDealBizType() ;
                        //进行台账占用，更改台账对应相关分项额度信息
                        String limitSubNo = lmtContRel.getLimitSubNo() ;
                        //获取分项信息
                        //如果是法人额度占用，更改分项中的已经出账金额，可出账金额，用信余额，用信敞口余额
                        if(CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(lmtType)){
                            ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(limitSubNo) ;
                            //用信余额
                            //修改前
                            BigDecimal bfLoanBalance = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanBalance());
                            //修改后
                            BigDecimal afLoanBalance = bfLoanBalance.add(dealBizAmtCny);
                            apprLmtSubBasicInfo.setLoanBalance(afLoanBalance);

                            //用信敞口余额
                            //修改前
                            BigDecimal bfLoanSpacBalance = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanSpacBalance());
                            //修改后
                            BigDecimal afLoanSpacBalance = bfLoanSpacBalance.add(dealBizSpacAmtCny) ;
                            apprLmtSubBasicInfo.setLoanSpacBalance(afLoanSpacBalance);

                            apprLmtSubBasicInfo.setUpdateTime(DateUtils.getCurrDate());
                            apprLmtSubBasicInfo.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));

                            logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key+":分项【"+apprLmtSubBasicInfo.getApprSubSerno()+"】,"
                                    +"用信敞口余额更改为："+bfLoanSpacBalance+" --> "+ afLoanSpacBalance
                                    +"用信余额更改为："+bfLoanBalance+" --> "+ afLoanBalance);
                            apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfo, resultMap) ;

                            //上级目录处理
                            String parentId = apprLmtSubBasicInfo.getParentId() ;
                            //根据上级目录，查询一级分项信息
                            if(!com.alibaba.excel.util.StringUtils.isEmpty(parentId)){
                                ApprLmtSubBasicInfo oldApprLmtSubBasicInfoOne = apprLmtSubBasicInfoService.selectByApprSubSerno(parentId) ;
                                //克隆原始数据
                                ApprLmtSubBasicInfo apprLmtSubBasicInfoOne = new ApprLmtSubBasicInfo() ;
                                //初始化最新数据
                                if(oldApprLmtSubBasicInfoOne != null){
                                    BeanUtils.copyProperties(oldApprLmtSubBasicInfoOne, apprLmtSubBasicInfoOne);
                                    apprLmtSubBasicInfoOne.setLoanBalance(dealBizAmtCny.add(apprLmtSubBasicInfoOne.getLoanBalance()));
                                    apprLmtSubBasicInfoOne.setLoanSpacBalance(dealBizSpacAmtCny.add(apprLmtSubBasicInfoOne.getLoanSpacBalance()));
                                    //调用更新代码，并且对比原始数据，流程金额更改记录
                                    apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfoOne, resultMap) ;
                                }
                            }
                        }else if(CmisLmtConstants.STD_ZB_LMT_TYPE_03.equals(lmtType)){
                            ApprCoopSubInfo apprCoopSubInfo = apprCoopSubInfoMapper.selectBySubSerno(limitSubNo) ;
                            //修改前
                            BigDecimal bfLoanBalance = BigDecimalUtil.replaceNull(apprCoopSubInfo.getLoanBalance()) ;
                            //修改后
                            BigDecimal afLoanBalance = bfLoanBalance.add(dealBizAmtCny) ;
                            logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0029.key+": 合作方【"+apprCoopSubInfo.getApprSubSerno()+"】," +
                                    "用信总余额更改为：" + bfLoanBalance + " --> "+ afLoanBalance);
                            apprCoopSubInfo.setLoanBalance(afLoanBalance);
                            apprCoopSubInfoService.updateByPKeyInApprLmtChgDetails (apprCoopSubInfo, resultMap) ;
                        }
                    }
                }
            }
            contAccRelService.deleteByPrimaryKey(contAccRel.getPkId());
            logger.info("因为shardkey原因，删除记录，重新插入{}", contAccRel);
            contAccRelService.insert(contAccRel) ;
        }

        List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelListByDealBizNo(originAccNo, null);
        //判空处理 update by dumd
        if (CollectionUtils.isNotEmpty(lmtContRelList)){
            for (LmtContRel lmtContRel : lmtContRelList) {
                resultMap.put("inputId", lmtContRel.getInputId()) ;
                resultMap.put("inputBrId", lmtContRel.getInputBrId()) ;
                resultMap.put("inputDate", DateUtils.getCurrentDate(DateFormatEnum.DATETIME)) ;
                //新的台账编号
                if(StringUtils.isNotEmpty(newAccNo)) lmtContRel.setDealBizNo(newAccNo);
                //新的起始日期
                if(StringUtils.isNotEmpty(startDate)) lmtContRel.setStartDate(startDate);
                //新的到期日期
                if(StringUtils.isNotEmpty(endDate)) lmtContRel.setEndDate(endDate);
                //最近更新日期
                lmtContRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                //最近更新时间
                lmtContRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                //更新合同对应的额度用信金额
                String lmtType = lmtContRel.getLmtType() ;
                String bizAttr = lmtContRel.getBizAttr() ;
                //合同占用总额
                BigDecimal bizTotalAmtCny = BigDecimalUtil.replaceNull(lmtContRel.getBizTotalAmtCny()) ;
                BigDecimal bizSpacAmtCny = BigDecimalUtil.replaceNull(lmtContRel.getBizSpacAmtCny()) ;

                String bizStatus = lmtContRel.getBizStatus() ;
                //如果单一法人额度
                if(CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr) && CmisLmtConstants.STD_ZB_BIZ_STATUS_100.equals(bizStatus)){
                    if(CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(lmtType) || CmisLmtConstants.STD_ZB_LMT_TYPE_07.equals(lmtType)){
                        logger.info("cmislmt0029 通知出账，额度占用关系为台账，处理对应额度中的用信余额开始......start");
                        ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(lmtContRel.getLimitSubNo()) ;
                        if(Objects.isNull(apprLmtSubBasicInfo)){
                            throw new YuspException(EclEnum.ECL070007.key, EclEnum.ECL070007.value);
                        }
                        apprLmtSubBasicInfo.setLoanBalance(bizTotalAmtCny.add(BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanBalance())));
                        apprLmtSubBasicInfo.setLoanSpacBalance(bizSpacAmtCny.add(BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanSpacBalance())));
                        apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfo, resultMap) ;
                        logger.info("cmislmt0029 通知出账，额度占用关系为台账，处理对应额度中的用信余额结束......end");
                    }
                }
                //状态更新为有效
                lmtContRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_200);
                lmtContRelService.deleteByPrimaryKey(lmtContRel.getPkId()) ;
                lmtContRelService.insert(lmtContRel);
            }
        }
        respDto.setErrorCode(SuccessEnum.SUCCESS.key);
        respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.value);
        return respDto;
    }
}