package cn.com.yusys.yusp.web.server.cmislmt0031;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0031.req.CmisLmt0031ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0031.resp.CmisLmt0031RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0031.CmisLmt0031Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询客户是否存在有效的非标额度
 *
 * @author macm 20210615
 * @version 1.0
 */
@Api(tags = "cmislmt0031:查询客户是否存在有效的非标额度")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0031Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0031Resource.class);

    @Autowired
    private CmisLmt0031Service cmisLmt0031Service;

    /**
     * 交易码：CmisLmt0031
     * 交易描述：查询客户是否存在有效的非标额度
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("查询客户是否存在有效的非标额度")
    @PostMapping("/cmislmt0031")
    protected @ResponseBody
    ResultDto<CmisLmt0031RespDto> CmisLmt0031(@Validated @RequestBody CmisLmt0031ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0031.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0031.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0031RespDto> CmisLmt0031RespDtoResultDto = new ResultDto<>();
        CmisLmt0031RespDto CmisLmt0031RespDto = new CmisLmt0031RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0031RespDto = cmisLmt0031Service.execute(reqDto);
            CmisLmt0031RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0031RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0031.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0031.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0031.value, e.getMessage());
            // 封装CmisLmt0031RespDtoResultDto中异常返回码和返回信息
            CmisLmt0031RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0031RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0031RespDto到CmisLmt0031RespDtoResultDto中
        CmisLmt0031RespDtoResultDto.setData(CmisLmt0031RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0031.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0031.value, JSON.toJSONString(CmisLmt0031RespDtoResultDto));
        return CmisLmt0031RespDtoResultDto;
    }
}