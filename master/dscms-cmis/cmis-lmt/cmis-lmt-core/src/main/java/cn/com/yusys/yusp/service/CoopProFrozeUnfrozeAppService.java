/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.DictTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.domain.ApprCoopInfo;
import cn.com.yusys.yusp.domain.ApprCoopSubInfo;
import cn.com.yusys.yusp.domain.CoopProFrozeUnfrozeApp;
import cn.com.yusys.yusp.domain.CoopProFrozeUnfrozeSub;
import cn.com.yusys.yusp.dto.CoopProFrozeUnfrozeAppSubDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.ApprCoopInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprCoopSubInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CoopProFrozeUnfrozeAppMapper;
import cn.com.yusys.yusp.repository.mapper.CoopProFrozeUnfrozeSubMapper;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt-core模块
 * @类名称: CoopProFrozeUnfrozeAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-04 10:25:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CoopProFrozeUnfrozeAppService {

    @Autowired
    private CoopProFrozeUnfrozeAppMapper coopProFrozeUnfrozeAppMapper;

    @Autowired
    private ApprCoopInfoMapper apprCoopInfoMapper;
    @Autowired
    private ApprCoopSubInfoMapper apprCoopSubInfoMapper;
    @Autowired
    private CoopProFrozeUnfrozeSubMapper coopProFrozeUnfrozeSubMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CoopProFrozeUnfrozeApp selectByPrimaryKey(String serno) {
        return coopProFrozeUnfrozeAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CoopProFrozeUnfrozeApp> selectAll(QueryModel model) {
        List<CoopProFrozeUnfrozeApp> records = (List<CoopProFrozeUnfrozeApp>) coopProFrozeUnfrozeAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CoopProFrozeUnfrozeApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CoopProFrozeUnfrozeApp> list = coopProFrozeUnfrozeAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CoopProFrozeUnfrozeApp record) {
        return coopProFrozeUnfrozeAppMapper.insert(record);
    }


    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CoopProFrozeUnfrozeApp record) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("partnerNo",record.getPartnerNo());
        queryModel.addCondition("apprStatusDefault","1");//('000','111','992') 待发起、审批中、退回状态不允许在新增
        List<CoopProFrozeUnfrozeApp> listCoopProFrozeUnfrozeApp = coopProFrozeUnfrozeAppMapper.selectByModel(queryModel);
        if(!CollectionUtils.isEmpty(listCoopProFrozeUnfrozeApp)){
            throw BizException.error(null, null, "该合作方有在途合作方冻结/解冻申请！");
        }
/*        queryModel = new QueryModel();
        queryModel.addCondition("partnerNo",record.getPartnerNo());
        queryModel.addCondition("apprStatus","997");
        queryModel.addCondition("diffAppType",record.getAppType());
        listCoopProFrozeUnfrozeApp = coopProFrozeUnfrozeAppMapper.selectByModel(queryModel);
        if(!CollectionUtils.isEmpty(listCoopProFrozeUnfrozeApp)){
            if(record.getAppType().equals(listCoopProFrozeUnfrozeApp.get(0).getAppType())){
                throw BizException.error(null, null, "该合作方有在途合作方冻结/解冻申请！");
            }
        }*/
        String appType = record.getAppType();
        QueryModel model = new QueryModel();
        model.addCondition("apprSerno", record.getCoopPlanNo());
        //[{"key":"1","value":"额度冻结"},{"key":"2","value":"额度解冻"}]
        List<ApprCoopInfo> list = apprCoopInfoMapper.selectByModel(model);
        if (list.isEmpty()) {
            //额度台账不存在
            throw BizException.error(null, EcbEnum.E_IQP_LMT_QUERY_FAILED.key, EcbEnum.E_IQP_LMT_QUERY_FAILED.value);
        }
        //申请主表信息
        ApprCoopInfo apprCoopInfo = list.get(0);
        record.setPartnerNo(apprCoopInfo.getCusId());
        record.setPartnerName(apprCoopInfo.getCusName());
        record.setPartnerType(apprCoopInfo.getCopType());
        record.setCoopPlanNo(apprCoopInfo.getApprSerno());
        record.setCoopStartDate(apprCoopInfo.getStartDate());
        record.setCoopEndDate(apprCoopInfo.getEndDate());
        record.setLmtAmt(apprCoopInfo.getLmtAmt());
        record.setApprStatus("000");
        coopProFrozeUnfrozeAppMapper.insertSelective(record);
        //申请主表流水号,需要存入从表
        String serno = record.getSerno();
        QueryModel modelsub = new QueryModel();
        modelsub.addCondition("apprSerno", apprCoopInfo.getApprSerno());

        //[{"key":"1","value":"额度冻结"},{"key":"2","value":"额度解冻"}]
        if("1".equals(appType)){
            modelsub.addCondition("status", "01");
        }else {
            modelsub.addCondition("status", "11");
        }
        List<ApprCoopSubInfo> apprCoopInfos = apprCoopSubInfoMapper.selectByModel(modelsub);
        apprCoopInfos.forEach(apprCoopSubInfo -> {
            CoopProFrozeUnfrozeSub coopProFrozeUnfrozeSub = new CoopProFrozeUnfrozeSub();
            coopProFrozeUnfrozeSub.setSerno(serno);
            coopProFrozeUnfrozeSub.setApprSerno(apprCoopSubInfo.getApprSerno());
            coopProFrozeUnfrozeSub.setApprSubSerno(apprCoopSubInfo.getApprSubSerno());
            coopProFrozeUnfrozeSub.setLimitSubNo(apprCoopSubInfo.getLimitSubNo());
            String lmtBizTypeProp = apprCoopSubInfo.getLmtBizTypeProp() ;
            String lmtBizTypePropName;
            if(StringUtils.isNotEmpty(lmtBizTypeProp)){
                lmtBizTypePropName= DictTranslatorUtils.findValueByDictKey("STD_PRD_TYPE_PROP", lmtBizTypeProp);
                coopProFrozeUnfrozeSub.setLimitSubName(lmtBizTypePropName);
            }else{
                coopProFrozeUnfrozeSub.setLimitSubName(apprCoopSubInfo.getLimitSubName());
            }
            coopProFrozeUnfrozeSub.setAvlAmt(apprCoopSubInfo.getAvlAmt());
            coopProFrozeUnfrozeSub.setOutstndAmt(apprCoopSubInfo.getOutstndAmt());
            coopProFrozeUnfrozeSub.setSigAmt(apprCoopSubInfo.getSigAmt());
            coopProFrozeUnfrozeSub.setSigBussAmt(apprCoopSubInfo.getSigBussAmt());
            coopProFrozeUnfrozeSub.setLoanBalance(apprCoopSubInfo.getLoanBalance());
            coopProFrozeUnfrozeSub.setIsRevolv(apprCoopSubInfo.getIsRevolv());
            coopProFrozeUnfrozeSub.setCurType(apprCoopSubInfo.getCurType());
            coopProFrozeUnfrozeSub.setTerm(apprCoopSubInfo.getTerm());
            coopProFrozeUnfrozeSub.setStartDate(apprCoopSubInfo.getStartDate());
            coopProFrozeUnfrozeSub.setEndDate(apprCoopSubInfo.getEndDate());
            coopProFrozeUnfrozeSub.setStatus(apprCoopSubInfo.getStatus());
            coopProFrozeUnfrozeSubMapper.insertSelective(coopProFrozeUnfrozeSub);
        });
        return 1;
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CoopProFrozeUnfrozeApp record) {
        return coopProFrozeUnfrozeAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CoopProFrozeUnfrozeApp record) {
        return coopProFrozeUnfrozeAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return coopProFrozeUnfrozeAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return coopProFrozeUnfrozeAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: updateAppSub
     * @方法描述: 更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateAppSub(CoopProFrozeUnfrozeAppSubDto coopProFrozeUnfrozeAppSubDto) {
        CoopProFrozeUnfrozeApp coopProFrozeUnfrozeApp = coopProFrozeUnfrozeAppSubDto.getCoopProFrozeUnfrozeApp();
        List<CoopProFrozeUnfrozeSub> list = coopProFrozeUnfrozeAppSubDto.getCoopProFrozeUnfrozeSub();
        coopProFrozeUnfrozeAppMapper.updateByPrimaryKey(coopProFrozeUnfrozeApp);
        list.stream().forEach(sub -> {
            if(sub.getAfterLmtStatus() == null || "".equals(sub.getAfterLmtStatus())){
                throw BizException.error(null, null, "请选择修改后额度状态");
            }
            coopProFrozeUnfrozeSubMapper.updateByPrimaryKey(sub);
        });
    }
}
