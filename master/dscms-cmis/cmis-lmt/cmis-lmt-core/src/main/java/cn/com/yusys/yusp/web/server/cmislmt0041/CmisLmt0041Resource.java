package cn.com.yusys.yusp.web.server.cmislmt0041;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0040.req.CmisLmt0040ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0040.resp.CmisLmt0040RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0041.req.CmisLmt0041ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0041.resp.CmisLmt0041RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0040.CmisLmt0040Service;
import cn.com.yusys.yusp.service.server.cmislmt0041.CmisLmt0041Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据额度品种编号查找适用产品编号
 *
 * @author zhangjw 2021/7/6
 * @version 1.0
 */
@Api(tags = "cmislmt0041:低风险额度反向生成")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0041Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0041Resource.class);

    @Autowired
    private CmisLmt0041Service cmisLmt0041Service;
    /**
     * 交易码：cmislmt0041
     * 交易描述：集团成员退出或成员解散
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("低风险额度反向生成")
    @PostMapping("/cmislmt0041")
    protected @ResponseBody
    ResultDto<CmisLmt0041RespDto> cmisLmt0041(@Validated @RequestBody CmisLmt0041ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0041.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0041.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0041RespDto> cmisLmt0041RespDtoResultDto = new ResultDto<>();
        CmisLmt0041RespDto cmisLmt0041RespDto = new CmisLmt0041RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0041RespDto = cmisLmt0041Service.execute(reqDto);
            cmisLmt0041RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0041RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0041.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0041.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0041.value, e.getMessage());
            // 封装xddb0041DataResultDto中异常返回码和返回信息
            cmisLmt0041RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0041RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        cmisLmt0041RespDtoResultDto.setData(cmisLmt0041RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0041.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0041.value, JSON.toJSONString(cmisLmt0041RespDtoResultDto));
        return cmisLmt0041RespDtoResultDto;
    }
}
