package cn.com.yusys.yusp.web.server.cmislmt0028;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0028.req.CmisLmt0028ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0028.resp.CmisLmt0028RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0028.CmisLmt0028Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:额度分项信息查询
 *
 * @author dumd 20210615
 * @version 1.0
 */
@Api(tags = "cmislmt0028:删除额度分项数据")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0028Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0028Resource.class);

    @Autowired
    private CmisLmt0028Service cmisLmt0028Service;

    /**
     * 交易码：CmisLmt0028
     * 交易描述：删除额度分项数据
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("删除额度分项数据")
    @PostMapping("/cmislmt0028")
    protected @ResponseBody
    ResultDto<CmisLmt0028RespDto> CmisLmt0028(@Validated @RequestBody CmisLmt0028ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0028.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0028.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0028RespDto> CmisLmt0028RespDtoResultDto = new ResultDto<>();
        CmisLmt0028RespDto CmisLmt0028RespDto = new CmisLmt0028RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0028RespDto = cmisLmt0028Service.execute(reqDto);
            CmisLmt0028RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0028RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0028.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0028.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0028.value, e.getMessage());
            // 封装CmisLmt0028RespDtoResultDto中异常返回码和返回信息
            CmisLmt0028RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0028RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0028RespDto到CmisLmt0028RespDtoResultDto中
        CmisLmt0028RespDtoResultDto.setData(CmisLmt0028RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0028.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0028.value, JSON.toJSONString(CmisLmt0028RespDtoResultDto));
        return CmisLmt0028RespDtoResultDto;
    }
}