/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSubBasicConf;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoTranDto;
import cn.com.yusys.yusp.dto.CfgPrdTypePropertiesDto;
import cn.com.yusys.yusp.server.cmiscfg0002.resp.CmisCfg0002ListRespDto;
import cn.com.yusys.yusp.service.LmtSubBasicConfService;
import cn.com.yusys.yusp.service.LmtSubPrdMappConfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtSubBasicConfResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-05-06 09:43:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsubbasicconf")
public class LmtSubBasicConfResource {
    @Autowired
    private LmtSubBasicConfService lmtSubBasicConfService;

    @Autowired
    private LmtSubPrdMappConfService lmtSubPrdMappConfService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSubBasicConf>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSubBasicConf> list = lmtSubBasicConfService.selectAll(queryModel);
        return new ResultDto<List<LmtSubBasicConf>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtSubBasicConf>> index(QueryModel queryModel) {
        List<LmtSubBasicConf> list = lmtSubBasicConfService.selectByModel(queryModel);
        return new ResultDto<List<LmtSubBasicConf>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSubBasicConf> show(@PathVariable("pkId") String pkId) {
        LmtSubBasicConf lmtSubBasicConf = lmtSubBasicConfService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSubBasicConf>(lmtSubBasicConf);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<LmtSubBasicConf> create(@RequestBody LmtSubBasicConf lmtSubBasicConf) {
        lmtSubBasicConfService.insert(lmtSubBasicConf);
        return new ResultDto<LmtSubBasicConf>(lmtSubBasicConf);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSubBasicConf lmtSubBasicConf) {
        int result = lmtSubBasicConfService.update(lmtSubBasicConf);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtSubBasicConfService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSubBasicConfService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param map
     * @return
     * @date 2021/05/07
     * @version 1.0.0
     * @desc    根据额度结构编号查询额度结构树信息
     * @修改历史: zhangjw
     */
    @PostMapping("/selectTreeList")
    protected ResultDto<List<Map<String,Object>>> selectTreeList(@RequestBody Map map) {
        List<Map<String,Object>> list = lmtSubBasicConfService.selectTreeList(map);
        return new ResultDto<>(list);
    }

    /**
     * @param model
     * @return
     * @date 2021/07/14
     * @version 1.0.0
     * @desc    根据查询条件获取额度结构树信息
     *           额度结构编号   LIMIT_STR_NO
     *           额度类型    LIMIT_TYPE
     *           分项类型   LMT_SUB_TYPE
     *           适用业务条线   SUIT_BIZ_LINE
     * @修改历史: zhangjw
     */
    @PostMapping("/selectTreeListBySelect")
    protected ResultDto<List<Map<String,Object>>> selectTreeListBySelect(@RequestBody Map map) {
        List<Map<String,Object>> list = lmtSubBasicConfService.selectTreeListBySelect(map);
        return new ResultDto<>(list);
    }

    /**
     * @param map
     * @return
     * @date 2021/06/22
     * @version 1.0.0
     * @desc    根据额度结构编号查询额度结构树信息
     * @修改历史:
     */
    @PostMapping("/selectlmtsubtree")
    protected ResultDto<List<Map<String,Object>>> selectLmtsubTree(@RequestBody Map map) {
        List<Map<String,Object>> list = lmtSubBasicConfService.selectLmtsubTree(map);
        return new ResultDto<>(list);
    }

    /***
     * @param limitSubNoDto 额度编号
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List < java.util.Map < java.lang.String, java.lang.Object>>>
     * @author tangxun
     * @date 2021/5/8 1:11 下午
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectCheckTreeList")
    protected ResultDto<List<CfgPrdBasicinfoTranDto>> selectCheckTreeList(@RequestBody LimitSubNoDto limitSubNoDto) {
        String limitSubNo = limitSubNoDto.getLimitSubNo();
        List<CfgPrdBasicinfoTranDto> list = lmtSubBasicConfService.selectCheckTreeList(limitSubNo);
        return new ResultDto<>(list);
    }

    static class LimitSubNoDto {
        private String limitSubNo;

        public String getLimitSubNo() {
            return limitSubNo;
        }

        public void setLimitSubNo(String limitSubNo) {
            this.limitSubNo = limitSubNo;
        }

        public LimitSubNoDto() {
        }
    }

    /**
     * @函数名称:queryConfInfoByLimitSubNo
     * @函数描述:根据额度品种编号查询额度品种基础信息
     * @参数与返回说明:
     * @param map
     * zhangjw
     */
    @PostMapping("/queryConfInfoByLimitSubNo")
    protected ResultDto<List<LmtSubBasicConf>> queryConfInfoByLimitSubNo(@RequestBody Map map) {
        List<LmtSubBasicConf> list = lmtSubBasicConfService.queryConfInfoByLimitSubNo(map);
        return new ResultDto<List<LmtSubBasicConf>>(list);
    }

    /**
     * @函数名称:deleteConfInfoByLimitSubNo
     * @函数描述:根据额度品种编号删除额度品种(下级节点也删除)
     * @参数与返回说明:selectSigStrInfoByList
     * @param map
     * zhangjw
     */
    @PostMapping("/deleteConfInfoByLimitSubNo")
    protected ResultDto<Integer> deleteConfInfoByLimitSubNo(@RequestBody Map map) {
        int result = lmtSubBasicConfService.deleteConfInfoByLimitSubNo(map);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param map: limitStrNo    limitSubNo  limitSubName  parentId
     *             zhangjw
     * @函数名称:createConfInfoByLimitSubNo
     * @函数描述:新增节点
     * @参数与返回说明:
     */
    @PostMapping("/createConfInfoByLimitSubNo")
    protected ResultDto<Integer> createConfInfoByLimitSubNo(@RequestBody Map map) {
        int result = lmtSubBasicConfService.createConfInfoByLimitSubNo(map);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param map: limitStrNo    limitSubNo  limitSubName  parentId
     *             zhangjw
     * @函数名称:updateConfInfoByLimitSubNo
     * @函数描述:更新基础节点信息
     * @参数与返回说明:
     */
    @PostMapping("/updateConfInfoByLimitSubNo")
    protected ResultDto<Integer> updateConfInfoByLimitSubNo(@RequestBody Map map) {
        int result = lmtSubBasicConfService.updateConfInfoByLimitSubNo(map);
        return new ResultDto<Integer>(result);
    }


    /**
     * @param limitSubNo
     * @return
     * @date 2021/07/14
     * @version 1.0.0
     * 根据额度品种编号获取对应的适用产品，根据适用产品获取产品扩展属性
     * @修改历史: zhangjw
     */
    @PostMapping("/queryDetailBylimitSubNo")
    protected ResultDto<List<CmisCfg0002ListRespDto>> queryDetailBylimitSubNo(@RequestBody String limitSubNo) {
        List<CmisCfg0002ListRespDto> list = lmtSubBasicConfService.queryDetailBylimitSubNo(limitSubNo);
        return new ResultDto<>(list);
    }
}
