
package cn.com.yusys.yusp.service.server.comm;

import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.TmpAccAdddetailsService;
import cn.com.yusys.yusp.service.TmpAddamtAccAdddetailsService;
import cn.com.yusys.yusp.service.TmpAddamtTdcarAlService;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: Comm4LmtCalFormulaUtils
 * @类描述: #对内服务类
 * @功能描述: TODO
 * @创建时间: 2021/9/26 9:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Comm4LmtCalFormulaUtils {

    private static final Logger log = LoggerFactory.getLogger(Comm4LmtCalFormulaUtils.class);

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;

    @Autowired
    private TmpAddamtTdcarAlService tmpAddAmtTdcarAlService ;

    @Autowired
    private TmpAddamtAccAdddetailsService tmpAddamtAccAdddetailsService;

    /**
     * @作者:lizx
     * @方法名称: getLoanBalance
     * @方法描述:  用于续作，或者复议的时候，重新生成新额度分项，原额度分项下的余额挂靠到新额度分项下，用信与同步更新，
     * @参数与返回说明: 返回新的用信余额
     * @算法描述: 无
     * @日期：2021/9/26 9:43
     * @param oriBean: 
 * @param newBean: 
     * @return: java.math.BigDecimal
     * @算法描述: 无
    */
    public BigDecimal getLoanBalance(ApprLmtSubBasicInfo oriBean, ApprLmtSubBasicInfo newBean){
        //用信余额
        BigDecimal oriLoanBalance = BigDecimalUtil.replaceNull(oriBean.getLoanBalance()) ;
        return oriLoanBalance.add(BigDecimalUtil.replaceNull(newBean.getLoanBalance())) ;
    }

    /**
     * @作者:lizx
     * @方法名称: getLoanBalance
     * @方法描述:  用于续作，或者复议的时候，重新生成新额度分项，原额度分项下的余额挂靠到新额度分项下，用信与同步更新，
     * @参数与返回说明: 返回新的用信敞口余额
     * @算法描述: 无
     * @日期：2021/9/26 9:43
     * @param oriBean:
     * @param newBean:
     * @return: java.math.BigDecimal
     * @算法描述: 无
     */
    public BigDecimal getloanSpacBalance(ApprLmtSubBasicInfo oriBean, ApprLmtSubBasicInfo newBean){
        //用信余额
        BigDecimal oriLoanSpacBalance = BigDecimalUtil.replaceNull(oriBean.getLoanSpacBalance()) ;
        return oriLoanSpacBalance.add(BigDecimalUtil.replaceNull(newBean.getLoanSpacBalance())) ;
    }

    /**
     * @作者:lizx
     * @方法名称: getLoanBalance
     * @方法描述:  用于续作，或者复议的时候，重新生成新额度分项，原额度分项下的余额挂靠到新额度分项下，用信与同步更新，
     * @参数与返回说明: 返回新的已出账金额
     * @算法描述: 无
     * @日期：2021/9/26 9:43
     * @param oriBean:
     * @param newBean:
     * @return: java.math.BigDecimal
     * @算法描述: 无
     */
    public BigDecimal getPvpOutstndAmt(ApprLmtSubBasicInfo oriBean, ApprLmtSubBasicInfo newBean){
        //用信余额
        BigDecimal oriPvpOutstndAmt = BigDecimalUtil.replaceNull(oriBean.getPvpOutstndAmt()) ;
        return oriPvpOutstndAmt.add(BigDecimalUtil.replaceNull(newBean.getPvpOutstndAmt())) ;
    }

    /**
     * @作者:lizx
     * @方法名称: getLoanBalance
     * @方法描述:  用于续作，或者复议的时候，重新生成新额度分项，原额度分项下的余额挂靠到新额度分项下，用信与同步更新，
     * @参数与返回说明: 返回新的已出账金额
     * @算法描述: 无
     * @日期：2021/9/26 9:43
     * @param oriBean:
     * @param newBean:
     * @return: java.math.BigDecimal
     * @算法描述: 无
     */
    public BigDecimal getAvlOutstndAmt(ApprLmtSubBasicInfo oriBean, ApprLmtSubBasicInfo newBean){
        //已出账余额
        BigDecimal oriPvpOutstndAmt = BigDecimalUtil.replaceNull(oriBean.getPvpOutstndAmt()) ;
        //可出账金额
        BigDecimal avlOutstndAmt = BigDecimalUtil.replaceNull(newBean.getAvlOutstndAmt()) ;
        return getAvlOutstndAmt(oriPvpOutstndAmt, avlOutstndAmt) ;
    }

    /**
     * @作者:lizx
     * @方法名称: getAvlOutstndAmt
     * @方法描述:  可出账长金额计算
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/9/26 14:15
     * @param pvpOutstndAmt:
     * @param avlTotal:
     * @return: java.math.BigDecimal
     * @算法描述: 无
    */
    public BigDecimal getAvlOutstndAmt(BigDecimal pvpOutstndAmt, BigDecimal avlTotal){
        //可出账金额=授信总额 - 已出账金额
        return avlTotal.subtract(pvpOutstndAmt) ;
    }


    /**
     * @作者:lizx
     * @方法名称: getLmtMfAmt
     * @方法描述:  获取新旧金额之间的差额
     * @参数与返回说明: 返回新的货币基金金额
     * @算法描述: 无
     * @日期：2021/9/26 9:43
     * @return: java.math.BigDecimal
     * @算法描述: 无
     */
    public BigDecimal getDiffAmt(BigDecimal newAmt, BigDecimal oldAmt){
        return newAmt.subtract(oldAmt)  ;
    }

    /**
     * @作者:lizx
     * @方法名称: calAvlLmtTotal
     * @方法描述:  一级分项下存在多个二级分项，根据二级分项反算授信总额
     *           算法描述：二级分项根据保证金比例进行从高到底排序，排序后
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/9/29 16:06
     * @return: java.math.BigDecimal
     * @算法描述: 无
    */
    public BigDecimal calAvlLmtTotal(List<Map<String, Object>> objectList, BigDecimal lmtAmt){
        BigDecimal subAmt = BigDecimal.ZERO ;
        BigDecimal subTotal = lmtAmt ;
        //根据保证金比例进行排序
        Collections.sort(objectList, new Comparator<Map<String, Object>>() {
            @Override
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                BigDecimal bailPreReateOne = (BigDecimal) o1.get("bailPreRate");
                BigDecimal bailPreReateTow = (BigDecimal) o2.get("bailPreRate");
                if(bailPreReateTow==null) bailPreReateTow = BigDecimal.ZERO ;
                if(bailPreReateOne==null) bailPreReateOne = BigDecimal.ZERO ;
                return bailPreReateTow.compareTo(bailPreReateOne);
            }
        });
        log.info("处理分项计算总额列表：{}", JSON.toJSONString(objectList)) ;
        int count =1 ;
        for (Map<String, Object> stringObjectMap : objectList) {
            log.info("处理第"+count+"个分项分项信息：{}", JSON.toJSONString(stringObjectMap));
            count++ ;
            //当前分项授信总额
            BigDecimal curAvlamt = BigDecimalUtil.replaceNull(stringObjectMap.get("AvlAmt"));
            BigDecimal bailPreRate = BigDecimalUtil.replaceNull(stringObjectMap.get("bailPreRate")) ;
            //遍历二级分项数据，计算实际敞口金额，计算规则：
            if(lmtAmt.compareTo(BigDecimal.ZERO)>0){
                if(lmtAmt.compareTo(curAvlamt)>=0){
                    //是否低风险
                    //获取保证金比例计算后的金额
                    BigDecimal preRateAmt = curAvlamt.divide(BigDecimal.ONE.subtract(bailPreRate), 0, BigDecimal.ROUND_HALF_UP) ;
                    subAmt = subAmt.add(preRateAmt) ;
                    lmtAmt = lmtAmt.subtract(curAvlamt) ;
                    log.info("额度分项下二级分项【"+stringObjectMap.get("apprSubSerno")+"】，保证金比例【"+bailPreRate+"】," +
                            "保证金比例计算后的金额:"+preRateAmt+",计算后的总金额：{}", subAmt) ;
                }else{
                    BigDecimal preRateAmt = lmtAmt.divide(BigDecimal.ONE.subtract(bailPreRate), 0, BigDecimal.ROUND_HALF_UP) ;
                    subAmt = subAmt.add(preRateAmt) ;
                    lmtAmt = lmtAmt.subtract(curAvlamt) ;
                    log.info("额度分项下二级分项【"+stringObjectMap.get("apprSubSerno")+"】，保证金比例【\"+bailPreRate+\"】," +
                            "保证金比例计算后的金额:"+preRateAmt+",计算后的总金额：{}", subAmt) ;
                }
            }else{
                break ;
            }
        }
        if(subTotal.compareTo(subAmt)>0) subAmt = subTotal ;
        return subAmt ;
    }

    /**
     * @作者:lizx
     * @方法名称: calDetailAvlLmtTotal
     * @方法描述:  明细（二级分项总额）的总额极端
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/9/30 0:21
     * @param paramMap:
     * @return: java.math.BigDecimal
     * @算法描述: 无
    */
    public BigDecimal calDetailAvlLmtTotal(Map<String, Object> paramMap) {
        BigDecimal bailPreRate = BigDecimalUtil.replaceNull(paramMap.get("bailPreRate"));
        BigDecimal lmtAmt = BigDecimalUtil.replaceNull(paramMap.get("AvlAmt"));
        if (bailPreRate == null) bailPreRate = BigDecimal.ZERO;
        BigDecimal avlAmt = lmtAmt.divide(BigDecimal.ONE.subtract(bailPreRate), 0, BigDecimal.ROUND_HALF_UP);
        log.info("##3 二级分项【" + paramMap.get("apprSubSerno") + "】分项可用总额计算后的可用总额：" + avlAmt);
        return avlAmt;
    }


    /**
     * @作者:lizx
     * @方法名称: apprLmtAmtAddExecute
     * @方法描述:
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/10/12 21:57
     * @param apprSubSerno:
     * @return: void
     * @算法描述: 无
     */
    public void calLmtAmtAddExecute(String apprSubSerno){
        log.info("{}：累加金额处理-----------------start", apprSubSerno);
        /**
         * 初始化表处理
         */
        log.info("初始化累加金额和中间表-----------------start");
        //初始化分项中的累加金额
        apprLmtSubBasicInfoService.updateLmtAmtAddBySubSerno(apprSubSerno) ;
        //清空中间表 【tmp_tdcar_al】
        tmpAddAmtTdcarAlService.truncateTalbeData() ;

        //清空中间表 【tmp_acc_adddetails】
        tmpAddamtAccAdddetailsService.truncateTalbeData() ;
        log.info("初始化累加金额和中间表-----------------end");

        /**
         * 场景 1
         * -- 判断合同占用是否存在超分项金额的，若有，则更新授信总额累加
         * -- 此处更新授信总额累加规则为：
         * --    1、若合同占用分项，敞口占用未超批复分项敞口金额：
         * --					则 授信总额已用 - 授信总额  作为 授信总额累加
         * --    2、若合同占用分项，敞口占用超批复分项敞口金额：
         * --	        则 （授信总额已用 - 授信敞口已用） - （授信总额 - 授信敞口）作为 授信总额累加
         */
        log.info("累加金额场景1处理-----------------start");
        // 插入中间表【tmp_tdcar_al】 符合场景1的数据
        int count = tmpAddAmtTdcarAlService.insertTmpLmtAmt(apprSubSerno);

        log.info("1.插入【tmpTdcarAl】数据记录数{}",count);
        //更新额度分项表中数据
        count = apprLmtSubBasicInfoService.updateLmtAmtAddBySceneOne(apprSubSerno) ;
        log.info("1.修改【apprLmtSubBasicInfo】数据记录数{}",count);
        log.info("累加金额场景1处理-----------------end");

        /**
         * 场景2
         * -- 判断合同下台账占用是否超合同占用的，若有，则需更新授信总额累加：
         * -- 此处更新累加规则为：
         * -- A、若合同占用分项，敞口占用未超批复分项敞口金额  且  合同占用总额 <= 授信分项总额：
         * --     则：台账占用合同总已用 - 授信分项总额  作为 需增加的授信总额累加
         * -- B、若合同占用分项，敞口占用未超批复分项敞口金额  且  合同占用总额 > 授信分项总额：
         * --     则：台账占用合同总已用 - 合同占用总额  作为 需增加的授信总额累加
         * -- C、若合同占用分项，敞口占用超批复分项敞口金额  且  合同占用总额 <= 授信分项总额：
         * --		 则：（台账占用合同总已用 - 台账占用合同敞口已用） - （分项总额 - 分项总敞口）  作为 需增加的授信总额累加;
         * -- D、若合同占用分项，敞口占用超批复分项敞口金额  且  合同占用总额 > 授信分项总额：
         * --		 则：（台账占用合同总已用 - 台账占用合同敞口已用） - 分项已用金额  作为 需增加的授信总额累加;
         */
        log.info("累加金额场景2处理-----------------start");
        //插入台账低风险总额累加明细
        count = tmpAddamtAccAdddetailsService.insertSceneTwo(apprSubSerno);
        log.info("插入台账低风险总额累加明细 数据记录数{}",count);

        //清空中间表 【tmp_tdcar_al】
        tmpAddAmtTdcarAlService.truncateTalbeData() ;
        /**
         * A、若合同占用分项，敞口占用未超批复分项敞口金额  且  合同占用总额 <= 授信分项总额：
         *      则：台账占用合同总已用 - 授信分项总额  作为 需增加的授信总额累加
         */
        count = tmpAddAmtTdcarAlService.insertSceneTwoA(apprSubSerno);
        log.info("A、若合同占用分项，敞口占用未超批复分项敞口金额------ 数据记录数{}",count);

        /**
         * -- B、若合同占用分项，敞口占用未超批复分项敞口金额  且  合同占用总额 > 授信分项总额：
         *    则：台账占用合同总已用 - 合同占用总额  作为 需增加的授信总额累加
         */
        count = tmpAddAmtTdcarAlService.insertSceneTwoB(apprSubSerno);
        log.info("B、若合同占用分项，敞口占用未超批复分项敞口金额------ 数据记录数{}",count);

        /**
         * -- C、若合同占用分项，敞口占用超批复分项敞口金额  且  合同占用总额 <= 授信分项总额：
         *    则：（台账占用合同总已用 - 台账占用合同敞口已用） - （分项总额 - 分项总敞口）  作为 需增加的授信总额累加;
         */
        count = tmpAddAmtTdcarAlService.insertSceneTwoC(apprSubSerno);
        log.info("C、若合同占用分项，敞口占用超批复分项敞口金额------ 数据记录数{}",count);

        /**
         * --  D、若合同占用分项，敞口占用超批复分项敞口金额  且  合同占用总额 > 授信分项总额：
         *    则：（台账占用合同总已用 - 台账占用合同敞口已用） - 分项已用金额  作为 需增加的授信总额累加;
         */
        count = tmpAddAmtTdcarAlService.insertSceneTwoD(apprSubSerno);
        log.info("D、若合同占用分项，敞口占用超批复分项敞口金额------ 数据记录数{}",count);
        //更新额度分项表中数据
        count = apprLmtSubBasicInfoService.updateLmtAmtAddBySceneTwo(apprSubSerno) ;
        log.info("更新额度分项表中数据------ 数据记录数{}",count);
        log.info("累加金额场景2处理-----------------end");
        log.info("{}：累加金额处理-----------------end", apprSubSerno);
    }


    /**
     * @作者:lizx
     * @方法名称: calParentIdLmtAmtAddExecute
     * @方法描述:  一级分项累加金额处理
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/10/19 9:42
     * @return: void
     * @算法描述: 无
    */
    public void calParentIdLmtAmtAddExecute(String apprSubSerno){
        log.info("{}：累加一级分项金额处理-----------------start", apprSubSerno);
        //清空中间表 【tmp_tdcar_al】
        tmpAddAmtTdcarAlService.truncateTalbeData() ;

        //将父级分项编号插入累加金额中
        tmpAddAmtTdcarAlService.insertParentIdTmpTdcarAl(apprSubSerno) ;

        //更新额度分项表中数据
        int count = apprLmtSubBasicInfoService.updateLmtAmtAddBySceneOne(apprSubSerno) ;
        log.info("1.修改【apprLmtSubBasicInfo】数据记录数{}",count);
        log.info("{}：累加一级分项金额处理-----------------end", apprSubSerno);
    }

}
