package cn.com.yusys.yusp.web.server.cmislmt0038;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0038.req.CmisLmt0038ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0038.resp.CmisLmt0038RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmisLmt0038.CmisLmt0038Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据额度品种编号查找适用产品编号
 *
 * @author zhangjw 2021/7/6
 * @version 1.0
 */
@Api(tags = "cmislmt0038:根据额度品种编号查找适用产品编号")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0038Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0038Resource.class);

    @Autowired
    private CmisLmt0038Service cmisLmt0038Service;
    /**
     * 交易码：cmislmt0038
     * 交易描述：根据额度品种编号查找适用产品编号
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("根据额度品种编号查找适用产品编号")
    @PostMapping("/cmislmt0038")
    protected @ResponseBody
    ResultDto<CmisLmt0038RespDto> cmisLmt0038(@Validated @RequestBody CmisLmt0038ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0038.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0038.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0038RespDto> cmisLmt0038RespDtoResultDto = new ResultDto<>();
        CmisLmt0038RespDto cmisLmt0038RespDto = new CmisLmt0038RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0038RespDto = cmisLmt0038Service.execute(reqDto);
            cmisLmt0038RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0038RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0038.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0038.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0038.value, e.getMessage());
            // 封装xddb0038DataResultDto中异常返回码和返回信息
            cmisLmt0038RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0038RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0038RespDto到cmisLmt0038RespDtoResultDto中
        cmisLmt0038RespDtoResultDto.setData(cmisLmt0038RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0038.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0038.value, JSON.toJSONString(cmisLmt0038RespDtoResultDto));
        return cmisLmt0038RespDtoResultDto;
    }
}
