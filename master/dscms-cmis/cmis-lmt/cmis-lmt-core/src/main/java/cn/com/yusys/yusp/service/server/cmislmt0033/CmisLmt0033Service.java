package cn.com.yusys.yusp.service.server.cmislmt0033;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtContRel;
import cn.com.yusys.yusp.dto.LmtSubPrdMappConfDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.req.CmisLmt0033DealBizListReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.req.CmisLmt0033OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.req.CmisLmt0033ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.resp.CmisLmt0033RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.resp.CmisLmt0009RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.resp.CmisLmt0010RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.LmtContRelService;
import cn.com.yusys.yusp.service.LmtSubPrdMappConfService;
import cn.com.yusys.yusp.service.LmtWhiteInfoService;
import cn.com.yusys.yusp.service.server.cmislmt0009.CmisLmt0009Service;
import cn.com.yusys.yusp.service.server.cmislmt0010.CmisLmt0010Service;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import com.alibaba.fastjson.JSON;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.checkerframework.checker.units.qual.A;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

@Service
public class CmisLmt0033Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0033Service.class);

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService;

    @Autowired
    private LmtSubPrdMappConfService lmtSubPrdMappConfService;

    @Autowired
    private CmisLmt0009Service cmisLmt0009Service;

    @Autowired
    private CmisLmt0010Service cmisLmt0010Service;

    @Autowired
    private LmtWhiteInfoService lmtWhiteInfoService;

    @Autowired
    private Comm4Service comm4Service;

    @Autowired
    private LmtContRelService lmtContRelService ;

    /**
     * 国结票据出账额度校验
     * add by dumd 20210618
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0033RespDto execute(CmisLmt0033ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0033.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0033.value);
        CmisLmt0033RespDto respDto = new CmisLmt0033RespDto();

        //台账明细列表
        List<CmisLmt0033DealBizListReqDto> dealBizList = reqDto.getDealBizList();
        //占用额度列表(同业客户额度)
        List<CmisLmt0033OccRelListReqDto> occRelList = reqDto.getOccRelList();
        //合同编号
        String bizNo = reqDto.getBizNo();

        try {

            if (StringUtils.isNotEmpty(bizNo)){
                //如果合同编号不为空，则调cmislmt0010接口进行台账校验
                executeCmisLmt0010Service(reqDto);
            }

            if(occRelList!=null && occRelList.size()>0){
                //如果占用额度列表有数据，则遍历调cmislmt0009接口进行额度校验
                //key：台账编号，value：台账编号和key一致的CmisLmt0033DealBizListReqDto对象
                Map<String,CmisLmt0033DealBizListReqDto> dealBizMap = getDealBizMap(dealBizList);
                //key：台账编号，value：台账编号和key一致的CmisLmt0033OccRelListReqDto对象列表
                Map<String,List<CmisLmt0033OccRelListReqDto>> occRelMap = getOccRelMap(occRelList);

                Set<Map.Entry<String, List<CmisLmt0033OccRelListReqDto>>> entries = occRelMap.entrySet();

                for (Map.Entry<String, List<CmisLmt0033OccRelListReqDto>> entry : entries) {
                    //台账编号
                    String dealBizNo = entry.getKey();
                    List<CmisLmt0033OccRelListReqDto> occRelListReqDto = entry.getValue();

                    CmisLmt0033DealBizListReqDto cmisLmt0033DealBizListReqDto = dealBizMap.get(dealBizNo);

                    //组织请求报文调cmislmt0009接口进行额度校验
                    executeCmisLmt0009Service(reqDto,occRelListReqDto,cmisLmt0033DealBizListReqDto);
                }
            }

            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("国结票据出账额度校验" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0033.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0033.value);
        return respDto;
    }

    /**
     * 调cmislmt0010台账校验接口进行台账校验
     * @param reqDto
     * @throws Exception
     */
    public void executeCmisLmt0010Service(CmisLmt0033ReqDto reqDto) throws Exception {
        //台账明细
        List<CmisLmt0033DealBizListReqDto> dealBizList = reqDto.getDealBizList();
        //组装信息调cmislmt0010service进行台账校验
        CmisLmt0010ReqDto cmisLmt0010ReqDto = new CmisLmt0010ReqDto();
        //台账明细
        List<CmisLmt0010ReqDealBizListDto> cmisLmt0010ReqDealBizListDtos = new ArrayList<>();

        //金融机构代码
        cmisLmt0010ReqDto.setInstuCde(reqDto.getInstuCde());
        //系统编号
        cmisLmt0010ReqDto.setSysId(reqDto.getSysNo());
        //合同编号
        String bizNo = reqDto.getBizNo();
        //List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelByDealBizNo(bizNo) ;
        List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelListByDealBizNo(bizNo, CmisLmtConstants.STD_ZB_BIZ_ATTR_1) ;
        if(CollectionUtils.isEmpty(lmtContRelList)){
            throw new YuspException(EclEnum.ECL070018.key, EclEnum.ECL070018.value);
        }
        cmisLmt0010ReqDto.setBizNo(bizNo) ;

        if (dealBizList != null && dealBizList.size() > 0) {

            for (CmisLmt0033DealBizListReqDto cmisLmt0033DealBizListReqDto : dealBizList) {

                CmisLmt0010ReqDealBizListDto dealBizListDto = new CmisLmt0010ReqDealBizListDto();
                /*String cusID = cmisLmt0033DealBizListReqDto.getCusId() ;
                if(StringUtils.isEmpty(cusID)){
                    throw new YuspException(EclEnum.ECL070088.key, EclEnum.ECL070088.value);
                }
                lmtContRelList.forEach(lmtContRel -> {
                    if(!cusID.equals(lmtContRel.getCusId())){
                        throw new YuspException(EclEnum.ECL070089.key, EclEnum.ECL070089.value);
                    }
                });*/
                //台账编号
                dealBizListDto.setDealBizNo(cmisLmt0033DealBizListReqDto.getDealBizNo());
                //是否无缝衔接
                dealBizListDto.setIsFollowBiz(cmisLmt0033DealBizListReqDto.getIsFollowBiz());
                //原交易业务编号
                dealBizListDto.setOrigiDealBizNo(cmisLmt0033DealBizListReqDto.getOrigiDealBizNo());
                //原交易业务恢复类型
                dealBizListDto.setOrigiRecoverType(cmisLmt0033DealBizListReqDto.getOrigiRecoverType());
                //原交易业务状态
//              dealBizListDto.setOrigiDealBizStatus();
                //原交易属性 默认 2--台账
                dealBizListDto.setOrigiBizAttr(CmisLmtConstants.STD_ZB_BIZ_ATTR_2);
                //产品编号	prdNo
                dealBizListDto.setPrdId(cmisLmt0033DealBizListReqDto.getPrdNo());
                //产品名称
                dealBizListDto.setPrdName(cmisLmt0033DealBizListReqDto.getPrdName());
                //台账占用总额
                dealBizListDto.setDealBizAmt(cmisLmt0033DealBizListReqDto.getDealBizAmtCny());
                //台账占用敞口
                dealBizListDto.setDealBizSpacAmt(cmisLmt0033DealBizListReqDto.getDealBizSpacAmtCny());
                //保证金比例
                dealBizListDto.setDealBizBailPreRate(BigDecimal.ZERO);
                //保证金金额
                dealBizListDto.setDealBizBailPreAmt(BigDecimal.ZERO);
                //台账到期日	endDate
                dealBizListDto.setEndDate(cmisLmt0033DealBizListReqDto.getEndDate());
                //客户名称
                dealBizListDto.setCusId(cmisLmt0033DealBizListReqDto.getCusId());
                //客户编号
                dealBizListDto.setCusName(cmisLmt0033DealBizListReqDto.getCusName());
                //产品类型属性
                dealBizListDto.setPrdTypeProp(cmisLmt0033DealBizListReqDto.getPrdTypeProp());
                //将dealBizListDto放入cmisLmt0010ReqDealBizListDtos
                cmisLmt0010ReqDealBizListDtos.add(dealBizListDto);
            }

            cmisLmt0010ReqDto.setCmisLmt0010ReqDealBizListDtoList(cmisLmt0010ReqDealBizListDtos);

            //调cmislmt0010台账校验接口
            logger.info("【{}】根据调用cmisLmt0010,请求报文【{}】开始！", DscmsLmtEnum.TRADE_CODE_CMISLMT0033.key, JSON.toJSONString(cmisLmt0010ReqDto));
            CmisLmt0010RespDto cmisLmt0010RespDto = cmisLmt0010Service.execute(cmisLmt0010ReqDto);
            logger.info("【{}】根据调用cmisLmt0010,响应报文【{}】开始！", DscmsLmtEnum.TRADE_CODE_CMISLMT0033.key, JSON.toJSONString(cmisLmt0010RespDto));
            if(cmisLmt0010RespDto != null && SuccessEnum.SUCCESS.key.equals(cmisLmt0010RespDto.getErrorCode())){
                logger.info("根据合同编号【{}】,进行cmislmt0010台账校验成功！", bizNo);
            }else{
                String errorMsg = "";
                if (cmisLmt0010RespDto!=null){
                    errorMsg = cmisLmt0010RespDto.getErrorMsg();
                }
                logger.info("根据合同编号【{}】,进行cmislmt0010台账校验失败！", bizNo);
                throw new YuspException(cmisLmt0010RespDto.getErrorCode(),errorMsg);
            }
        }
    }

    /**
     * 遍历台账明细列表，将台账编号作为key，CmisLmt0033DealBizListReqDto对象作为value存入hashMap
     * @param dealBizList
     * @return
     */
    public Map<String,CmisLmt0033DealBizListReqDto> getDealBizMap(List<CmisLmt0033DealBizListReqDto> dealBizList){
        Map<String,CmisLmt0033DealBizListReqDto> dealBizMap = new HashMap<>();

        for (CmisLmt0033DealBizListReqDto cmisLmt0033DealBizListReqDto : dealBizList) {
            dealBizMap.put(cmisLmt0033DealBizListReqDto.getDealBizNo(),cmisLmt0033DealBizListReqDto);
        }

        return dealBizMap;
    }

    /**
     * 遍历占用额度列表，将台账编号作为key，List<CmisLmt0033OccRelListReqDto>作为value存入hashMap
     * @param occRelList
     * @return
     */
    public Map<String,List<CmisLmt0033OccRelListReqDto>> getOccRelMap(List<CmisLmt0033OccRelListReqDto> occRelList){
        Map<String,List<CmisLmt0033OccRelListReqDto>> occRelMap = new HashMap<>();

        for (CmisLmt0033OccRelListReqDto cmisLmt0033OccRelListReqDto : occRelList) {
            //台账编号
            String dealBizNo = cmisLmt0033OccRelListReqDto.getDealBizNo();
            //创建占用额度列表
            List<CmisLmt0033OccRelListReqDto> occRelListReqDto = new ArrayList<>();

            if (occRelMap.containsKey(dealBizNo)){
                //如果occRelMap里有该台账编号，则占用额度列表从occRelMap里取
                occRelListReqDto = occRelMap.get(dealBizNo);
            }

            occRelListReqDto.add(cmisLmt0033OccRelListReqDto);
            occRelMap.put(dealBizNo,occRelListReqDto);
        }
        return occRelMap;
    }

    /**
     * 调cmislmt0009接口进行额度校验
     * @param reqDto
     * @param occRelList
     * @param dealBizListReqDto
     * @throws Exception
     */
    public void executeCmisLmt0009Service(CmisLmt0033ReqDto reqDto,List<CmisLmt0033OccRelListReqDto> occRelList,CmisLmt0033DealBizListReqDto dealBizListReqDto) throws Exception {
        CmisLmt0009ReqDto cmisLmt0009ReqDto = new CmisLmt0009ReqDto();
        //客户号
        String cusId = occRelList.get(0).getLmtCusId();

        //如果是票据的交易，cusId传的是行号，需要转换为客户号
        if(CmisBizConstants.SYS_NO_PJP.equals(reqDto.getSysNo())){
            logger.info("票据系统交易，将同业客户大额行号【"+cusId+"】转换为客户号开始----->start");
            cusId = comm4Service.queryCusIdByBankNo(cusId);
            logger.info("票据系统交易，将同业客户大额行号转换为客户号【"+cusId+"】结束----->start");
        }

        //产品编号
        String prdNo = dealBizListReqDto.getPrdNo();
        //额度分项编号
        String apprSubSerno = null;
        //额度类型 默认 同业额度
        String lmtType = CmisLmtConstants.STD_ZB_LMT_TYPE_07;

        if (CmisLmtConstants.STD_PJ_PRD_ID_052198.equals(prdNo) || CmisLmtConstants.STD_PJ_PRD_ID_052199.equals(prdNo)){
            //如果产品编号是贴现，则根据客户号从承兑行白名单表查询额度分项编号
            //052198	银行承兑汇票贴现
            //052199	商业汇票贴现
            apprSubSerno = lmtWhiteInfoService.selectSubAccNoByCusId(cusId);

            if(StringUtils.isBlank(apprSubSerno)){
                throw new YuspException("99999","客户【"+occRelList.get(0).getLmtCusId()+"】不存在承兑行白名单额度");
            }

            lmtType = CmisLmtConstants.STD_ZB_LMT_TYPE_06;
        }else{
            //根据产品编号找到对应要占用的额度品种编号
            String limitSubNo = comm4Service.getLimitSubNoByPrdIdAndStrNo(prdNo, CmisLmtConstants.STD_ZB_LMT_STR_MTABLE_CONF_03) ;
            if(StringUtils.isBlank(limitSubNo)) throw new YuspException(EclEnum.ECL070082.key,EclEnum.ECL070082.value);
            //额度分项编号
            apprSubSerno = apprLmtSubBasicInfoService.selectApprSubSernoByCusIdAndLmtSubNo(cusId, limitSubNo);
            if(StringUtils.isBlank(apprSubSerno)){
                throw new YuspException(EclEnum.ECL070007.key,EclEnum.ECL070007.value);
            }
        }

        //系统编号
        cmisLmt0009ReqDto.setSysId(reqDto.getSysNo());
        //金融机构代码
        cmisLmt0009ReqDto.setInstuCde(reqDto.getInstuCde());
        //交易业务编号
        String dealBizNo = dealBizListReqDto.getDealBizNo();
        cmisLmt0009ReqDto.setDealBizNo(dealBizNo);
        //客户编号
        cmisLmt0009ReqDto.setCusId(cusId);
        //交易业务类型 默认 1--一般合同
        cmisLmt0009ReqDto.setDealBizType(CmisLmtConstants.DEAL_BIZ_TYPE_1);
        //产品编号
        cmisLmt0009ReqDto.setPrdId(prdNo);
        //产品名称
        cmisLmt0009ReqDto.setPrdName(dealBizListReqDto.getPrdName());
        //是否低风险业务 默认 0--否
        cmisLmt0009ReqDto.setIsLriskBiz(CmisLmtConstants.YES_NO_N);
        //是否无缝衔接
        cmisLmt0009ReqDto.setIsFollowBiz(dealBizListReqDto.getIsFollowBiz());
        //是否合同重签 默认 0--否
        cmisLmt0009ReqDto.setIsBizRev(CmisLmtConstants.YES_NO_N);
        //交易属性 默认 2--台账
        cmisLmt0009ReqDto.setBizAttr(CmisLmtConstants.STD_ZB_BIZ_ATTR_2);
        //原交易业务编号
        cmisLmt0009ReqDto.setOrigiDealBizNo(dealBizListReqDto.getOrigiDealBizNo());
        //原交易业务恢复类型
        cmisLmt0009ReqDto.setOrigiRecoverType(dealBizListReqDto.getOrigiRecoverType());
        //原交易业务状态 默认 06--撤销占用
        cmisLmt0009ReqDto.setOrigiDealBizStatus(CmisLmtConstants.STD_RECOVER_TYPE_06);
        //原交易属性 默认 2--台账
        cmisLmt0009ReqDto.setOrigiBizAttr(CmisLmtConstants.STD_ZB_BIZ_ATTR_2);
        //交易业务金额
        cmisLmt0009ReqDto.setDealBizAmt(dealBizListReqDto.getDealBizAmtCny());
        //保证金比例
        cmisLmt0009ReqDto.setDealBizBailPreRate(BigDecimal.ZERO);
        //保证金金额
        cmisLmt0009ReqDto.setDealBizBailPreAmt(BigDecimal.ZERO);
        //合同起始日
        cmisLmt0009ReqDto.setStartDate(dealBizListReqDto.getStartDate());
        //合同到期日
        cmisLmt0009ReqDto.setEndDate(dealBizListReqDto.getEndDate());

        //占用额度列表
        List<CmisLmt0009OccRelListReqDto> cmisLmt0009OccRelListReqDtos = new ArrayList<>();

        for (CmisLmt0033OccRelListReqDto cmisLmt0033OccRelListReqDto : occRelList) {
            CmisLmt0009OccRelListReqDto cmisLmt0009OccRelListReqDto = new CmisLmt0009OccRelListReqDto();
            //额度类型 默认 07--同业额度
            cmisLmt0009OccRelListReqDto.setLmtType(lmtType);
            //额度分项编号
            cmisLmt0009OccRelListReqDto.setLmtSubNo(apprSubSerno);
            //占用总额取占用敞口
            cmisLmt0009OccRelListReqDto.setBizTotalAmt(cmisLmt0033OccRelListReqDto.getBizSpacAmtCny());
            //占用敞口取占用敞口
            cmisLmt0009OccRelListReqDto.setBizSpacAmt(cmisLmt0033OccRelListReqDto.getBizSpacAmtCny());
            //产品类型属性
            cmisLmt0009OccRelListReqDto.setPrdTypeProp(dealBizListReqDto.getPrdTypeProp());

            cmisLmt0009OccRelListReqDtos.add(cmisLmt0009OccRelListReqDto);
        }

        cmisLmt0009ReqDto.setCmisLmt0009OccRelListReqDtoList(cmisLmt0009OccRelListReqDtos);

        //调cmislmt0009接口进行额度校验
        logger.info("【{}】根据调用cmisLmt0009,请求报文【{}】开始！", DscmsLmtEnum.TRADE_CODE_CMISLMT0033.key, JSON.toJSONString(cmisLmt0009ReqDto));
        CmisLmt0009RespDto cmisLmt0009RespDto = cmisLmt0009Service.execute(cmisLmt0009ReqDto);
        logger.info("【{}】根据调用cmisLmt0009,响应报文【{}】结束！", DscmsLmtEnum.TRADE_CODE_CMISLMT0033.key, JSON.toJSONString(cmisLmt0009RespDto));
        if(cmisLmt0009RespDto != null && SuccessEnum.SUCCESS.key.equals(cmisLmt0009RespDto.getErrorCode())){
            logger.info("根据台账编号【{}】,进行cmislmt0009额度校验成功！", dealBizNo);
        }else{
            String errorMsg = "";
            if (cmisLmt0009RespDto!=null){
                errorMsg = cmisLmt0009RespDto.getErrorMsg();
            }
            logger.info("根据台账编号【{}】,进行cmislmt0009额度校验失败！", dealBizNo);
            throw new YuspException("额度校验失败",errorMsg);
        }
    }
}