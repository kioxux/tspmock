/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: DeRiskIndexCfg
 * @类描述: de_risk_index_cfg数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-16 13:42:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "de_risk_index_cfg")
public class DeRiskIndexCfg extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 指标类型 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "RISK_TYPE")
	private String riskType;
	
	/** 指标限额要求 **/
	@Column(name = "RISK_INDEX_REQ", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal riskIndexReq;
	
	/** 黄区阈值 **/
	@Column(name = "RISK_YELLOW_REQ", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal riskYellowReq;
	
	/** 红区阈值 **/
	@Column(name = "RISK_RED_REQ", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal riskRedReq;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param riskType
	 */
	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}
	
    /**
     * @return riskType
     */
	public String getRiskType() {
		return this.riskType;
	}
	
	/**
	 * @param riskIndexReq
	 */
	public void setRiskIndexReq(java.math.BigDecimal riskIndexReq) {
		this.riskIndexReq = riskIndexReq;
	}
	
    /**
     * @return riskIndexReq
     */
	public java.math.BigDecimal getRiskIndexReq() {
		return this.riskIndexReq;
	}
	
	/**
	 * @param riskYellowReq
	 */
	public void setRiskYellowReq(java.math.BigDecimal riskYellowReq) {
		this.riskYellowReq = riskYellowReq;
	}
	
    /**
     * @return riskYellowReq
     */
	public java.math.BigDecimal getRiskYellowReq() {
		return this.riskYellowReq;
	}
	
	/**
	 * @param riskRedReq
	 */
	public void setRiskRedReq(java.math.BigDecimal riskRedReq) {
		this.riskRedReq = riskRedReq;
	}
	
    /**
     * @return riskRedReq
     */
	public java.math.BigDecimal getRiskRedReq() {
		return this.riskRedReq;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}