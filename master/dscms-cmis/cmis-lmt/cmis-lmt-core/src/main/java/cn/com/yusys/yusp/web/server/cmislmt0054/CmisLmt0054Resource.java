package cn.com.yusys.yusp.web.server.cmislmt0054;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0054.req.CmisLmt0054ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0054.resp.CmisLmt0054RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0054.CmisLmt0054Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询客户授信总额
 *
 * @author lizx 20210908
 * @version 1.0
 */
@Api(tags = "cmislmt0054:查询多客户敞口金额和敞口余额")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0054Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0054Resource.class);

    @Autowired
    private CmisLmt0054Service cmisLmt0054Service;

    /**
     * 交易码：CmisLmt0054
     * 交易描述：查询客户授信总额
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("查询客户授信总额")
    @PostMapping("/cmislmt0054")
    protected @ResponseBody
    ResultDto<CmisLmt0054RespDto> CmisLmt0054(@Validated @RequestBody CmisLmt0054ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0054.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0054.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0054RespDto> CmisLmt0054RespDtoResultDto = new ResultDto<>();
        CmisLmt0054RespDto CmisLmt0054RespDto = new CmisLmt0054RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0054RespDto = cmisLmt0054Service.execute(reqDto);
            CmisLmt0054RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0054RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0054.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0054.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0054.value, e.getMessage());
            // 封装CmisLmt0054RespDtoResultDto中异常返回码和返回信息
            CmisLmt0054RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0054RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0054RespDto到CmisLmt0054RespDtoResultDto中
        CmisLmt0054RespDtoResultDto.setData(CmisLmt0054RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0054.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0054.value, JSON.toJSONString(CmisLmt0054RespDtoResultDto));
        return CmisLmt0054RespDtoResultDto;
    }
}