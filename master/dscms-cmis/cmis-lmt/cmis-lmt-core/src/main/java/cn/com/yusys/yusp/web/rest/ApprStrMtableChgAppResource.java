/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ApprStrMtableChgApp;
import cn.com.yusys.yusp.service.ApprStrMtableChgAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprStrMtableChgAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 86188
 * @创建时间: 2021-05-11 10:36:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/apprstrmtablechgapp")
public class ApprStrMtableChgAppResource {
    @Autowired
    private ApprStrMtableChgAppService apprStrMtableChgAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<ApprStrMtableChgApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<ApprStrMtableChgApp> list = apprStrMtableChgAppService.selectAll(queryModel);
        return new ResultDto<List<ApprStrMtableChgApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<ApprStrMtableChgApp>> index(QueryModel queryModel) {
        List<ApprStrMtableChgApp> list = apprStrMtableChgAppService.selectByModel(queryModel);
        return new ResultDto<List<ApprStrMtableChgApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<ApprStrMtableChgApp> show(@PathVariable("pkId") String pkId) {
        ApprStrMtableChgApp apprStrMtableChgApp = apprStrMtableChgAppService.selectByPrimaryKey(pkId);
        return new ResultDto<ApprStrMtableChgApp>(apprStrMtableChgApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<ApprStrMtableChgApp> create(@RequestBody ApprStrMtableChgApp apprStrMtableChgApp) {
        apprStrMtableChgAppService.insert(apprStrMtableChgApp);
        return new ResultDto<ApprStrMtableChgApp>(apprStrMtableChgApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ApprStrMtableChgApp apprStrMtableChgApp) {
        int result = apprStrMtableChgAppService.update(apprStrMtableChgApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = apprStrMtableChgAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = apprStrMtableChgAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:logicalDelete
     * @函数描述:产品删除时，数据逻辑删除(既将操作类型更新为“02-删除”)，不进行物理删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/logicalDelete/{pkId}")
    protected ResultDto<Integer> logicalDelete(@PathVariable("pkId") String pkId) {
        int result = apprStrMtableChgAppService.logicDelete(pkId);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/valilmtchgappinway/{serno}")
    protected ResultDto<Integer> valiLmtChgAppInWay(@Validated @PathVariable String serno) {
        int result = apprStrMtableChgAppService.valiLmtChgAppInWay(serno);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/initapprstrmtablechgappinfo")
    protected ResultDto<Map> initApprStrMTableChgAppInfo(@Validated @RequestBody ApprStrMtableChgApp apprStrMtableChgApp) {
        Map result = apprStrMtableChgAppService.initApprStrMTableChgAppInfo(apprStrMtableChgApp);
        return new ResultDto<Map>(result);
    }

    /**
     * @函数名称:queryStrChgAppBySerno
     * @函数描述:根据调整申请流水号和主批复台账编号加工展示主调整申请主信息
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryStrChgAppBySerno")
    protected ResultDto<List<Map<String,Object>>> queryStrChgAppBySerno(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprStrMtableChgAppService.queryStrChgAppBySerno(queryModel);
        return new ResultDto<>(list);
    }
}
