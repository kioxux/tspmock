package cn.com.yusys.yusp.service.server.cmislmt0049;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ManaOrgLmt;
import cn.com.yusys.yusp.dto.server.cmislmt0049.req.CmisLmt0049ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0049.resp.CmisLmt0049RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ManaOrgLmtService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CmisLmt0049Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0049Service.class);

    @Autowired
    private ManaOrgLmtService manaOrgLmtService;

    /**
     * 根据机构编号获取分支机构额度管控信息
     * @param reqDto
     * @return
     * @throws Exception
     */
    @Transactional
    public CmisLmt0049RespDto execute(CmisLmt0049ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0049.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0049.value);
        CmisLmt0049RespDto resqDto = new CmisLmt0049RespDto() ;

        try {
            String orgId = reqDto.getOrgId();
            if(StringUtils.isBlank(orgId)){
                throw new YuspException(EclEnum.ECL070133.key, "机构编号" + EclEnum.ECL070133.value);
            }
            ManaOrgLmt manaOrgLmt = manaOrgLmtService.selectByPrimaryKey(orgId);
            if (manaOrgLmt != null){
                BeanUtils.copyProperties(manaOrgLmt,resqDto);
                //将列表放入返回中
                resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
                resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
            }else{
                resqDto.setErrorCode(EpbEnum.EPB090004.key);
                resqDto.setErrorMsg(EpbEnum.EPB090004.value);
            }
        } catch (YuspException e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0049.value, e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0049.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0049.value);
        return resqDto;
    }
    
    
}
