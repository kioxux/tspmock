package cn.com.yusys.yusp.web.server.cmislmt0037;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0037.req.CmisLmt0037ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0037.resp.CmisLmt0037RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.cmislmt0037.CmisLmt0037Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 根据分项信息，查询起始日到期日期限
 * add by lizx 20210701
 */
@Api(tags = "cmislmt0037:根据分项信息，查询起始日到期日期限")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0037Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0037Resource.class);

    @Autowired
    private CmisLmt0037Service cmisLmt0037Service;

    /**
     * 交易码：CmisLmt0037
     * 交易描述：根据分项信息，查询起始日到期日期限
     * @param reqDto
     */

    @ApiOperation("根据分项信息，查询起始日到期日期限")
    @PostMapping("/cmislmt0037")
    protected @ResponseBody
    ResultDto<CmisLmt0037RespDto> CmisLmt0037(@Validated @RequestBody CmisLmt0037ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0037.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0037.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0037RespDto> CmisLmt0037RespDtoResultDto = new ResultDto<>();
        CmisLmt0037RespDto CmisLmt0037RespDto = new CmisLmt0037RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0037RespDto = cmisLmt0037Service.execute(reqDto);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0037.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0037.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0037.value, e.getMessage());
            // 封装CmisLmt0037RespDtoResultDto中异常返回码和返回信息
            CmisLmt0037RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0037RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0037RespDto到CmisLmt0037RespDtoResultDto中
        CmisLmt0037RespDtoResultDto.setData(CmisLmt0037RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0037.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0037.value, JSON.toJSONString(CmisLmt0037RespDtoResultDto));
        return CmisLmt0037RespDtoResultDto;
    }
}