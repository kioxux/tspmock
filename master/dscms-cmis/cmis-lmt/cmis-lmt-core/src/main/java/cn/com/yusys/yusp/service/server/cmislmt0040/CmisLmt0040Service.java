package cn.com.yusys.yusp.service.server.cmislmt0040;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.domain.ApprStrMtableInfo;
import cn.com.yusys.yusp.domain.CusGrp;
import cn.com.yusys.yusp.domain.CusGrpMemberRel;
import cn.com.yusys.yusp.dto.server.cmislmt0040.req.CmisLmt0040MemberListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0040.req.CmisLmt0040ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0040.resp.CmisLmt0040RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ApprLmtSubBasicInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprStrMtableInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CusGrpMapper;
import cn.com.yusys.yusp.repository.mapper.CusGrpMemberRelMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.ApprStrMtableInfoService;
import cn.com.yusys.yusp.service.CusGrpMemberRelService;
import cn.com.yusys.yusp.service.CusGrpService;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import com.alibaba.fastjson.JSON;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.checkerframework.checker.units.qual.A;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0040Service
 * @类描述: #对内服务类
 * @功能描述: 集团认定或集团成员退出或集团解散
 * @修改备注: 2021/7/12     zhangjw   新增接口
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0040Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0040Service.class);

    @Autowired
    private ApprStrMtableInfoService apprStrMtableInfoService;

    @Autowired
    private ApprStrMtableInfoMapper apprStrMtableInfoMapper;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService;

    @Autowired
    private ApprLmtSubBasicInfoMapper apprLmtSubBasicInfoMapper;

    @Autowired
    private Comm4Service comm4Service;

    @Autowired
    private CusGrpMapper cusGrpMapper;

    @Autowired
    private CusGrpMemberRelMapper cusGrpMemberRelMapper;

    @Autowired
    private CusGrpMemberRelService cusGrpMemberRelService;

    @Transactional
    public CmisLmt0040RespDto execute(CmisLmt0040ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0040.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0040.value);
        CmisLmt0040RespDto resqDto = new CmisLmt0040RespDto() ;

        //集团客户编号
        String grpNo = reqDto.getGrpNo();
        //集团客户名称
        String grpName = reqDto.getGrpName();
        //集团成员列表
        List<CmisLmt0040MemberListReqDto> memberList = reqDto.getMemberList();

        try {
            logger.info("集团认定或集团成员退出或集团解散",DscmsLmtEnum.TRADE_CODE_CMISLMT0040.key);

            //集团申请类型
            String grpAppType = reqDto.getGrpAppType();

            if(grpAppType.equals(CmisLmtConstants.STD_GRP_APP_TYPE_01)){
                //如果是集团客户认定
                logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团客户认定开始");

                CusGrp cusGrp = new CusGrp();
                //拷贝数据
                BeanUtils.copyProperties(reqDto, cusGrp);
                //新增集团客户信息
                cusGrpMapper.insert(cusGrp);

                //遍历集团成员列表，插入集团成员信息表
                for (CmisLmt0040MemberListReqDto cmisLmt0040MemberListReqDto : memberList) {
                    String cusId = cmisLmt0040MemberListReqDto.getGrpMemberNo() ;
                    CusGrpMemberRel cusGrpMemberRel = new CusGrpMemberRel();
                    cusGrpMemberRel.setCusId(cusId);
                    cusGrpMemberRel.setGrpNo(grpNo);
                    cusGrpMemberRel.setGrpName(grpName);
                    cusGrpMemberRel.setManagerId(cmisLmt0040MemberListReqDto.getManagerId());
                    cusGrpMemberRel.setManagerBrId(cmisLmt0040MemberListReqDto.getManagerBrId());
                    cusGrpMemberRel.setCusCatalog(cmisLmt0040MemberListReqDto.getCusCatalog());
                    //新增集团成员信息
                    cusGrpMemberRelMapper.insert(cusGrpMemberRel);
                    //集团插入，跟新成员对应得额度信息
                    comm4Service.updateApprLmtGrpNo(cusId, grpNo) ;
                }

                logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团客户认定结束");
            }else if(grpAppType.equals(CmisLmtConstants.STD_GRP_APP_TYPE_02)){
                //如果是集团客户变更
                //遍历集团成员列表memberList，如果本次变更类型是03--本次退出且是否保留额度为0--否，
                //则将该客户的批复台账以及分项的状态由生效改为冻结
                logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团客户变更开始");

                List<ApprStrMtableInfo> grpApprStrMtableList = apprStrMtableInfoService.queryApprStrZHLmt(grpNo);
                String grpApprSerno = "";
                if(CollectionUtils.isNotEmpty(grpApprStrMtableList)){
                    ApprStrMtableInfo grpApprStrMtable = grpApprStrMtableList.get(0) ;
                    if (grpApprStrMtable!=null){
                        grpApprSerno = grpApprStrMtable.getApprSerno();
                    }
                }else{
                    logger.info("---------------未找到集团客户有效授信额度信息------------------");
                }

                for (CmisLmt0040MemberListReqDto cmisLmt0040MemberListReqDto : memberList) {
                    //集团成员编号
                    String grpMemberNo = cmisLmt0040MemberListReqDto.getGrpMemberNo();
                    //本次变更类型
                    String cusModifyType = cmisLmt0040MemberListReqDto.getCusModifyType();
                    //是否保留额度
                    String isRetainLmt = cmisLmt0040MemberListReqDto.getIsRetainLmt();

                    if (CmisCusConstants.STD_CUS_MODIFY_TYPE_03.equals(cusModifyType)){
                        logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团子成员客户编号【"+grpMemberNo+"】,本次变更类型为本次退出，不保留额度");

                        //删除集团成员信息
                        cusGrpMemberRelMapper.deleteByCusId(grpMemberNo);

                        //本次退出，成员客户中得集团记录更新为空
                        comm4Service.updateApprLmtGrpNo(grpMemberNo, "") ;

                        if (CmisLmtConstants.YES_NO_N.equals(isRetainLmt)){
                            ApprStrMtableInfo apprStrMtableInfo = apprStrMtableInfoService.selectByGrpNoAndCusId(grpMemberNo, grpApprSerno);
                            logger.info("查询集团成员数据信息：{}", JSON.toJSONString(apprStrMtableInfo));
                            //集团成员的批复台账记录里的集团批复台账编号需要与集团客户的批复台账编号一致
                            if (apprStrMtableInfo!=null && StringUtils.isNotEmpty(apprStrMtableInfo.getGrpApprSerno()) && apprStrMtableInfo.getGrpApprSerno().equals(grpApprSerno)){
                                //批复状态
                                String apprStatus = apprStrMtableInfo.getApprStatus();

                                if (CmisLmtConstants.STD_ZB_APPR_ST_01.equals(apprStatus)){
                                    //如果批复状态是生效，则需要改成冻结
                                    apprStrMtableInfo.setApprStatus(CmisLmtConstants.STD_ZB_APPR_ST_11);
                                    apprStrMtableInfoMapper.updateByPrimaryKey(apprStrMtableInfo);

                                    //批复台账编号
                                    String apprSerno = apprStrMtableInfo.getApprSerno();
                                    //将该批复台账名下的批复分项的批复状态由生效改为冻结
                                    apprLmtSubBasicInfoMapper.updateStatusByFkPkid(apprSerno);
                                }
                            }
                        }
                    }else if (CmisCusConstants.STD_CUS_MODIFY_TYPE_02.equals(cusModifyType)){
                        logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团子成员客户编号【"+grpMemberNo+"】,本次变更类型为本次新增");
                        //将该客户加入到集团成员列表里
                        CusGrpMemberRel cusGrpMemberRel = new CusGrpMemberRel();
                        cusGrpMemberRel.setCusId(grpMemberNo);
                        cusGrpMemberRel.setGrpNo(grpNo);
                        cusGrpMemberRel.setGrpName(grpName);
                        cusGrpMemberRel.setManagerId(cmisLmt0040MemberListReqDto.getManagerId());
                        cusGrpMemberRel.setManagerBrId(cmisLmt0040MemberListReqDto.getManagerBrId());
                        cusGrpMemberRel.setCusCatalog(cmisLmt0040MemberListReqDto.getCusCatalog());
                        //新增集团成员信息
                        cusGrpMemberRelMapper.insert(cusGrpMemberRel);
                        //本次退出，成员客户中得集团客户编号更新为集团客户
                        comm4Service.updateApprLmtGrpNo(grpMemberNo, grpNo) ;
                    }
                }
                logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团客户变更结束");
            }else{
                //如果是集团客户解散
                //将该集团客户的批复台账及分项的状态由生效改为失效未结清或失效已结清
                //将该集团客户子成员的综合授信批复台账及分项的状态由生效改为失效未结清或失效已结清
                this.dealRemoveGrp(grpNo);
            }

            //将列表放入返回中
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("集团认定或集团成员退出或集团解散：",e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0040.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0040.value);
        return resqDto;
    }

    /**
     * @方法名称: dealRemoveGrp
     * @方法描述: 集团解散
     * @参数与返回说明:
     * @算法描述: 无
     */
    public void dealRemoveGrp(String grpNo){
        //如果是集团客户解散
        logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团客户解散开始");
        //删除集团客户信息、集团与成员关系表相关数据
        cusGrpMapper.deleteByPrimaryKey(grpNo);

        List<Map<String, Object>> cusIdList = cusGrpMemberRelService.selectCusIdsByGrpNo(grpNo);

        if (CollectionUtils.isEmpty(cusIdList)){
            logger.info("cmislmt0040 集团客户编号【"+grpNo+"】没有集团成员");
            return;
        }

        //删除集团成员关系表相关数据
        cusGrpMemberRelMapper.deleteByGrpNo(grpNo);

        //查询该集团客户的综合授信
        List<ApprStrMtableInfo> grpApprStrMtableList = apprStrMtableInfoService.queryApprStrZHLmt(grpNo);
        ApprStrMtableInfo grpApprStrMtable = new ApprStrMtableInfo() ;

        if (CollectionUtils.isEmpty(grpApprStrMtableList)){
            logger.info("cmislmt0040 集团客户编号【"+grpNo+"】没有综合授信的批复台账");
            return;
        }else{
            grpApprStrMtable = grpApprStrMtableList.get(0) ;
        }
        //集团客户批复编号
        String apprSernoGrp = grpApprStrMtable.getApprSerno();
        //集团客户综合授信是否还有用信余额，默认为否
        boolean hasBalance = false;

        for (Map<String, Object> cusIdMap : cusIdList) {
            //集团成员客户号
            String cusId = (String) cusIdMap.get("cusId");

            //本次退出，成员客户中得集团客户编号更新为集团客户
            comm4Service.updateApprLmtGrpNo(cusId, "") ;

            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("cusId",cusId);
            //集团批复台账编号
            queryModel.addCondition("grpApprSerno",apprSernoGrp);
            queryModel.addCondition("oprType",CmisLmtConstants.OPR_TYPE_ADD);

            //根据查询其集团成员的批复台账
            List<ApprStrMtableInfo> apprStrMtableInfos = apprStrMtableInfoService.selectAll(queryModel);

            if (CollectionUtils.isEmpty(apprStrMtableInfos)){
                continue;
            }

            ApprStrMtableInfo apprStrMtableInfo = apprStrMtableInfos.get(0);

            //批复台账编号
            String apprSerno = apprStrMtableInfo.getApprSerno();
            //批复状态
            String apprStatus = apprStrMtableInfo.getApprStatus();

            if (CmisLmtConstants.STD_ZB_APPR_ST_01.equals(apprStatus) || CmisLmtConstants.STD_ZB_APPR_ST_11.equals(apprStatus)){
                logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团子成员客户编号【"+cusId+"】,其批复台账状态是生效或冻结");
                //如果批复台账的批复状态是生效或冻结，则需要根据其名下的批复分项的批复状态将批复台账的批复状态改为失效未结清或失效已结清

                //查询批复台账下的一级批复分项
                List<ApprLmtSubBasicInfo> apprLmtSubBasicInfos = apprLmtSubBasicInfoService.selectByFkPkid(apprSerno);

                Set<String> setList = new LinkedHashSet<>() ;
                if (CollectionUtils.isNotEmpty(apprLmtSubBasicInfos)){
                    //遍历批复分项
                    for (ApprLmtSubBasicInfo apprLmtSubBasicInfo : apprLmtSubBasicInfos) {
                        String lmtSubType = apprLmtSubBasicInfo.getLmtSubType() ;
                        if(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_01.equals(lmtSubType)){
                            String subSerno = apprLmtSubBasicInfo.getApprSubSerno();
                            setList.add(subSerno) ;
                            continue ;
                        }
                        //状态
                        String status = apprLmtSubBasicInfo.getStatus();
                        //批复分项编号
                        String apprSubSerno = apprLmtSubBasicInfo.getApprSubSerno();
                        logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团子成员客户编号【"+cusId+"】,批复台账编号【"+apprSerno+"】,批复分项编号【"+apprSubSerno+"】");

                        if (CmisLmtConstants.STD_ZB_APPR_ST_01.equals(status) || CmisLmtConstants.STD_ZB_APPR_ST_11.equals(status)){
                            logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团子成员客户编号【"+cusId+"】,批复台账编号【"+apprSerno+"】,批复分项编号【"+apprSubSerno+"】的批复状态是生效或冻结");
                            //如果批复分项的批复状态是生效或冻结，则需要根据用信余额将批复状态更改为失效未结清或失效已结清
                            if (comm4Service.hasBalance(apprSubSerno)){
                                logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团子成员客户编号【"+cusId+"】,批复台账编号【"+apprSerno+"】,批复分项编号【"+apprSubSerno+"】的用信余额大于0");
                                //该批复分项有用信余额，则批复状态更新为失效未结清，批复台账状态改为失效未结清
                                status = CmisLmtConstants.STD_ZB_APPR_ST_90;
                                logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团子成员客户编号【"+cusId+"】,批复台账编号【"+apprSerno+"】,批复分项编号【"+apprSubSerno+"】的批复状态改为失效未结清");
                                apprStatus = CmisLmtConstants.STD_ZB_APPR_ST_90;
                                logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团子成员客户编号【"+cusId+"】,批复台账编号【"+apprSerno+"】的批复状态改为失效未结清");

                                //如果集团子成员的批复状态是失效未结清，则集团客户综合授信的用信余额大于0
                                hasBalance = true;
                                logger.info("cmislmt0040 集团客户编号【"+grpNo+"】的综合授信的用信余额大于0");
                            }else{
                                logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团子成员客户编号【"+cusId+"】,批复台账编号【"+apprSerno+"】,批复分项编号【"+apprSubSerno+"】的用信余额为0，将其批复状态更新为失效已结清");
                                //该批复分项无用信余额，则批复状态更新为失效已结清
                                status = CmisLmtConstants.STD_ZB_APPR_ST_99;
                            }
                        }else if (CmisLmtConstants.STD_ZB_APPR_ST_90.equals(status)){
                            logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团子成员客户编号【"+cusId+"】,批复台账编号【"+apprSerno+"】,批复分项编号【"+apprSubSerno+"】的批复状态是失效未结清");
                            //如果有一个分项是失效未结清则批复台账状态也改成失效未结清
                            logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团子成员客户编号【"+cusId+"】,批复台账编号【"+apprSerno+"】的批复状态改为失效未结清");
                            apprStatus = CmisLmtConstants.STD_ZB_APPR_ST_90;
                            //如果集团子成员的批复状态是失效未结清，则集团客户综合授信的用信余额大于0
                            logger.info("cmislmt0040 集团客户编号【"+grpNo+"】的综合授信的用信余额大于0");
                            hasBalance = true;
                        }
                        //更新批复分项的批复状态
                        logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团子成员客户编号【"+cusId+"】,批复台账编号【"+apprSerno+"】,批复分项编号【"+apprSubSerno+"】的批复状态改为"+status);
                        apprLmtSubBasicInfo.setStatus(status);
                        apprLmtSubBasicInfoMapper.updateByPrimaryKey(apprLmtSubBasicInfo);
                    }
                }

                for (String parentId : setList) {
                    int count = apprLmtSubBasicInfoMapper.selectSubUnInvalidByParentId(parentId) ;
                    ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoMapper.selectByApprSubSerno(parentId) ;
                    if(count >0){
                        apprLmtSubBasicInfo.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_90);
                        apprLmtSubBasicInfoMapper.updateByPrimaryKey(apprLmtSubBasicInfo) ;
                    }else{
                        apprLmtSubBasicInfo.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_99);
                        apprLmtSubBasicInfoMapper.updateByPrimaryKey(apprLmtSubBasicInfo) ;
                    }
                }
                int count = apprLmtSubBasicInfoMapper.selectSubUnInvalidByFkPkid(apprSerno) ;
                if(count > 0){
                    apprStatus = CmisLmtConstants.STD_ZB_APPR_ST_90 ;
                }else{
                    apprStatus = CmisLmtConstants.STD_ZB_APPR_ST_99 ;
                }

                //更新批复台账的批复状态
                logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团子成员客户编号【"+cusId+"】,批复台账编号【"+apprSerno+"】的批复状态改为"+apprStatus);
                apprStrMtableInfo.setApprStatus(apprStatus);
                apprStrMtableInfoMapper.updateByPrimaryKey(apprStrMtableInfo);

            }else if (CmisLmtConstants.STD_ZB_APPR_ST_90.equals(apprStatus)){
                //如果集团子成员的批复状态是失效未结清，则集团客户综合授信的用信余额大于0
                logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团子成员客户编号【"+cusId+"】,批复台账编号【"+apprSerno+"】的批复状态是失效未结清");
                hasBalance = true;
                logger.info("cmislmt0040 集团客户编号【"+grpNo+"】的综合授信的用信余额大于0");
            }
        }

        //批复台账的批复状态
        String apprStatus = grpApprStrMtable.getApprStatus();
        logger.info("cmislmt0040 集团客户编号【"+grpNo+"】的批复台账编号【"+apprSernoGrp+"】,批复状态是"+apprStatus);

        if (CmisLmtConstants.STD_ZB_APPR_ST_01.equals(apprStatus) || CmisLmtConstants.STD_ZB_APPR_ST_11.equals(apprStatus)){
            //如果批复台账的批复状态是生效或冻结，则需要根据集团客户综合授信的用信余额将批复台账的批复状态改为失效未结清或失效已结清
            if (hasBalance){
                logger.info("cmislmt0040 集团客户编号【"+grpNo+"】的批复台账编号【"+apprSernoGrp+"】,批复状态改为失效未结清");
                //集团客户综合授信的用信余额大于0,将批复状态改为失效未结清
                apprStatus = CmisLmtConstants.STD_ZB_APPR_ST_90;
            }else{
                //集团客户综合授信的用信余额为0,将批复状态改为失效已结清
                logger.info("cmislmt0040 集团客户编号【"+grpNo+"】的批复台账编号【"+apprSernoGrp+"】,批复状态改为失效已结清");
                apprStatus = CmisLmtConstants.STD_ZB_APPR_ST_99;
            }
        }

        //更新集团客户的批复台账的批复状态
        grpApprStrMtable.setApprStatus(apprStatus);
        apprStrMtableInfoMapper.updateByPrimaryKey(grpApprStrMtable);
        logger.info("cmislmt0040 集团客户编号【"+grpNo+"】,集团客户解散结束");
    }
}