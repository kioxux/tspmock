/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtExptListInfo;
import cn.com.yusys.yusp.service.LmtExptListInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtExptListInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: lenovo
 * @创建时间: 2021-04-16 16:27:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtexptlistinfo")
public class LmtExptListInfoResource {
    @Autowired
    private LmtExptListInfoService lmtExptListInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtExptListInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtExptListInfo> list = lmtExptListInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtExptListInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtExptListInfo>> index(QueryModel queryModel) {
        List<LmtExptListInfo> list = lmtExptListInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtExptListInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtExptListInfo>> selectByModel(@RequestBody QueryModel queryModel) {
        List<LmtExptListInfo> list = lmtExptListInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtExptListInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtExptListInfo> show(@PathVariable("pkId") String pkId) {
        LmtExptListInfo lmtExptListInfo = lmtExptListInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtExptListInfo>(lmtExptListInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtExptListInfo> create(@RequestBody LmtExptListInfo lmtExptListInfo) {
        lmtExptListInfoService.insert(lmtExptListInfo);
        return new ResultDto<LmtExptListInfo>(lmtExptListInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtExptListInfo lmtExptListInfo) {
        int result = lmtExptListInfoService.update(lmtExptListInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtExptListInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtExptListInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:logicaldelete
     * @函数描述:根据流水号逻辑删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/logicaldelete")
    protected ResultDto<Integer> logicaldelete(@RequestBody LmtExptListInfo record) {
        int result = lmtExptListInfoService.logicaldelete(record);
        return new ResultDto<Integer>(result);
    }

}
