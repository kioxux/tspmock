package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtCoopCtr
 * @类描述: lmt_coop_ctr数据实体类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2020-12-11 10:30:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtCoopCtrDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 授信协议编号 **/
	private String lmtCtrNo;

	/** 合作方授信协议名称 **/
	private String lmtCtrName;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 合作方客户编号 **/
	private String coopCusId;
	
	/** 合作方客户名称 **/
	private String coopCusName;
	
	/** 证件类型 STD_ZB_CERT_TYP **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 合作方类型 STD_ZB_COOP_TYP **/
	private String coopType;
	
	/** 产品编号 **/
	private String prdId;

	/** 授信期限类型 STD_ZB_TERM_TYP **/
	private String lmtTermType;

	/** 授信期限 **/
	private java.math.BigDecimal lmtTerm;
	
	/** 共享范围 STD_ZB_SHR_SCOPE **/
	private String sharedScope;
	
	/** 币种 STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 授信额度 **/
	private java.math.BigDecimal lmtAmt;
	
	/** 单户限额 **/
	private java.math.BigDecimal sigAmt;
	
	/** 授信起始日期 **/
	private String lmtStarDate;
	
	/** 授信到期日期 **/
	private String lmtEndDate;
	
	/** 注销日期 **/
	private String logoutDate;
	
	/** 争议借据方式选项 STD_ZB_DISPUPE_OPT **/
	private String billDispupeOpt;
	
	/** 仲裁机构 **/
	private String arbitrateBch;
	
	/** 合同份数 **/
	private java.math.BigDecimal contQnt;
	
	/** 甲方执合同份数 **/
	private java.math.BigDecimal contQntOwner;
	
	/** 乙方执合同份数 **/
	private java.math.BigDecimal contQntPartyB;
	
	/** 其他约定事项 **/
	private String otherAgreedEvent;
	
	/** 主办人 **/
	private String managerId;
	
	/** 主办机构 **/
	private String managerBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 协议状态 STD_ZB_CTR_ST **/
	private String agrStatus;
	

	/**
	 * @param lmtCtrNo
	 */
	public void setLmtCtrNo(String lmtCtrNo) {
		this.lmtCtrNo = lmtCtrNo == null ? null : lmtCtrNo.trim();
	}
	
    /**
     * @return LmtCtrNo
     */	
	public String getLmtCtrNo() {
		return this.lmtCtrNo;
	}

	/**
	 * @param lmtCtrName
	 */
	public void setLmtCtrName(String lmtCtrName) {
		this.lmtCtrName = lmtCtrName == null ? null : lmtCtrName.trim();
	}

	/**
	 * @return LmtCtrName
	 */
	public String getLmtCtrName() {
		return this.lmtCtrName;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param coopCusId
	 */
	public void setCoopCusId(String coopCusId) {
		this.coopCusId = coopCusId == null ? null : coopCusId.trim();
	}
	
    /**
     * @return CoopCusId
     */	
	public String getCoopCusId() {
		return this.coopCusId;
	}
	
	/**
	 * @param coopCusName
	 */
	public void setCoopCusName(String coopCusName) {
		this.coopCusName = coopCusName == null ? null : coopCusName.trim();
	}
	
    /**
     * @return CoopCusName
     */	
	public String getCoopCusName() {
		return this.coopCusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param coopType
	 */
	public void setCoopType(String coopType) {
		this.coopType = coopType == null ? null : coopType.trim();
	}
	
    /**
     * @return CoopType
     */	
	public String getCoopType() {
		return this.coopType;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}
	
    /**
     * @return PrdId
     */	
	public String getPrdId() {
		return this.prdId;
	}

	/**
	 * @return LmtTermType
	 */
	public String getLmtTermType(){
		return this.lmtTermType;
	}

	/**
	 * @return LmtTerm
	 */
	public java.math.BigDecimal getLmtTerm(){
		return this.lmtTerm;
	}
	
	/**
	 * @param sharedScope
	 */
	public void setSharedScope(String sharedScope) {
		this.sharedScope = sharedScope == null ? null : sharedScope.trim();
	}
	
    /**
     * @return SharedScope
     */	
	public String getSharedScope() {
		return this.sharedScope;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return LmtAmt
     */	
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param sigAmt
	 */
	public void setSigAmt(java.math.BigDecimal sigAmt) {
		this.sigAmt = sigAmt;
	}
	
    /**
     * @return SigAmt
     */	
	public java.math.BigDecimal getSigAmt() {
		return this.sigAmt;
	}
	
	/**
	 * @param lmtStarDate
	 */
	public void setLmtStarDate(String lmtStarDate) {
		this.lmtStarDate = lmtStarDate == null ? null : lmtStarDate.trim();
	}
	
    /**
     * @return LmtStarDate
     */	
	public String getLmtStarDate() {
		return this.lmtStarDate;
	}
	
	/**
	 * @param lmtEndDate
	 */
	public void setLmtEndDate(String lmtEndDate) {
		this.lmtEndDate = lmtEndDate == null ? null : lmtEndDate.trim();
	}
	
    /**
     * @return LmtEndDate
     */	
	public String getLmtEndDate() {
		return this.lmtEndDate;
	}
	
	/**
	 * @param logoutDate
	 */
	public void setLogoutDate(String logoutDate) {
		this.logoutDate = logoutDate == null ? null : logoutDate.trim();
	}
	
    /**
     * @return LogoutDate
     */	
	public String getLogoutDate() {
		return this.logoutDate;
	}
	
	/**
	 * @param billDispupeOpt
	 */
	public void setBillDispupeOpt(String billDispupeOpt) {
		this.billDispupeOpt = billDispupeOpt == null ? null : billDispupeOpt.trim();
	}
	
    /**
     * @return BillDispupeOpt
     */	
	public String getBillDispupeOpt() {
		return this.billDispupeOpt;
	}
	
	/**
	 * @param arbitrateBch
	 */
	public void setArbitrateBch(String arbitrateBch) {
		this.arbitrateBch = arbitrateBch == null ? null : arbitrateBch.trim();
	}
	
    /**
     * @return ArbitrateBch
     */	
	public String getArbitrateBch() {
		return this.arbitrateBch;
	}
	
	/**
	 * @param contQnt
	 */
	public void setContQnt(java.math.BigDecimal contQnt) {
		this.contQnt = contQnt;
	}
	
    /**
     * @return ContQnt
     */	
	public java.math.BigDecimal getContQnt() {
		return this.contQnt;
	}
	
	/**
	 * @param contQntOwner
	 */
	public void setContQntOwner(java.math.BigDecimal contQntOwner) {
		this.contQntOwner = contQntOwner;
	}
	
    /**
     * @return ContQntOwner
     */	
	public java.math.BigDecimal getContQntOwner() {
		return this.contQntOwner;
	}
	
	/**
	 * @param contQntPartyB
	 */
	public void setContQntPartyB(java.math.BigDecimal contQntPartyB) {
		this.contQntPartyB = contQntPartyB;
	}
	
    /**
     * @return ContQntPartyB
     */	
	public java.math.BigDecimal getContQntPartyB() {
		return this.contQntPartyB;
	}
	
	/**
	 * @param otherAgreedEvent
	 */
	public void setOtherAgreedEvent(String otherAgreedEvent) {
		this.otherAgreedEvent = otherAgreedEvent == null ? null : otherAgreedEvent.trim();
	}
	
    /**
     * @return OtherAgreedEvent
     */	
	public String getOtherAgreedEvent() {
		return this.otherAgreedEvent;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param agrStatus
	 */
	public void setAgrStatus(String agrStatus) {
		this.agrStatus = agrStatus == null ? null : agrStatus.trim();
	}
	
    /**
     * @return AgrStatus
     */	
	public String getAgrStatus() {
		return this.agrStatus;
	}
	

}