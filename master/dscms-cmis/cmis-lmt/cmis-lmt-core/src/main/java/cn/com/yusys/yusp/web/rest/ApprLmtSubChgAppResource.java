/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ApprLmtSubChgApp;
import cn.com.yusys.yusp.service.ApprLmtSubChgAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprLmtSubChgAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-20 20:39:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/apprlmtsubchgapp")
public class ApprLmtSubChgAppResource {
    @Autowired
    private ApprLmtSubChgAppService apprLmtSubChgAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<ApprLmtSubChgApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<ApprLmtSubChgApp> list = apprLmtSubChgAppService.selectAll(queryModel);
        return new ResultDto<List<ApprLmtSubChgApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<ApprLmtSubChgApp>> index(QueryModel queryModel) {
        List<ApprLmtSubChgApp> list = apprLmtSubChgAppService.selectByModel(queryModel);
        return new ResultDto<List<ApprLmtSubChgApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<ApprLmtSubChgApp> show(@PathVariable("pkId") String pkId) {
        ApprLmtSubChgApp apprLmtSubChgApp = apprLmtSubChgAppService.selectByPrimaryKey(pkId);
        return new ResultDto<ApprLmtSubChgApp>(apprLmtSubChgApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<ApprLmtSubChgApp> create(@RequestBody ApprLmtSubChgApp apprLmtSubChgApp) {
        apprLmtSubChgAppService.insert(apprLmtSubChgApp);
        return new ResultDto<ApprLmtSubChgApp>(apprLmtSubChgApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ApprLmtSubChgApp apprLmtSubChgApp) {
        int result = apprLmtSubChgAppService.update(apprLmtSubChgApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = apprLmtSubChgAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = apprLmtSubChgAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:querySubChgAppBySerno
     * @函数描述:根据调整申请流水号查询一级分项列表
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/querySubChgAppBySerno")
    protected ResultDto<List<Map<String,Object>>> querySubChgAppBySerno(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprLmtSubChgAppService.querySubChgAppBySerno(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:querySubDetailsChgAppBySerno
     * @函数描述:根据批复分项编号及调整申请流水号查询二级分项列表
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/querySubDetailsChgAppBySerno")
    protected ResultDto<List<Map<String,Object>>> querySubDetailsChgAppBySerno(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprLmtSubChgAppService.querySubDetailsChgAppBySerno(queryModel);
        return new ResultDto<>(list);
    }
}
