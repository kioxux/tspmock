package cn.com.yusys.yusp.service.server.cmislmt0014;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqdealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.server.comm.Comm4LmtCalFormulaUtils;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import com.alibaba.fastjson.JSON;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.math.BigDecimal;
import java.sql.SQLOutput;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0014Service
 * @类描述: #对内服务类
 * @功能描述: 台账恢复
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0014Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0014Service.class);

    @Autowired
    private ContAccRelService contAccRelService ;

    @Autowired
    private ContAccRelMapper contAccRelMapper ;

    @Autowired
    private LmtContRelService lmtContRelService ;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;

    @Autowired
    private ApprLmtSubBasicInfoMapper apprLmtSubBasicInfoMapper ;

    @Autowired
    private ApprCoopSubInfoService apprCoopSubInfoService ;

    @Autowired
    private LmtWhiteInfoService lmtWhiteInfoService ;

    @Autowired
    private Comm4Service comm4Service ;

    @Autowired
    private CmisBizClientService cmisBizClientService ;

    @Transactional
    public CmisLmt0014RespDto execute(CmisLmt0014ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.value);
        CmisLmt0014RespDto resqDto = new CmisLmt0014RespDto() ;
        try {
            List<CmisLmt0014ReqdealBizListDto> cmisLmt0014ReqdealBizListDtos = reqDto.getDealBizList() ;
            if(CollectionUtils.isNotEmpty(cmisLmt0014ReqdealBizListDtos)){
                for (CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto : cmisLmt0014ReqdealBizListDtos) {
                    Map<String, String> paramMap = (Map<String, String>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(reqDto);
                    Map<String, String> resultMap = new HashMap<>() ;
                    resultMap.putAll(paramMap);
                    resultMap.put("serviceCode", CmisLmtConstants.CMIS_LMT0014_SERVICE_CODE) ;

                    //恢复标准值 01-余额  02-差额
                    String recoverStd = cmisLmt0014ReqdealBizListDto.getRecoverStd() ;
                    //台账总余额（人民币）
                    BigDecimal balanceAmtCny = BigDecimalUtil.replaceNull(cmisLmt0014ReqdealBizListDto.getBalanceAmtCny()) ;
                    //台账敞口余额（人民币）
                    BigDecimal spacBalanceAmtCny = BigDecimalUtil.replaceNull(cmisLmt0014ReqdealBizListDto.getSpacBalanceAmtCny()) ;
                    //恢复类型 01-结清恢复  03-还款 04-追加保证金  06-撤销占用
                    String  recoverType = cmisLmt0014ReqdealBizListDto.getRecoverType() ;
                    //恢复敞口金额(人民币)
                    BigDecimal recoverSpacAmtCny = BigDecimalUtil.replaceNull(cmisLmt0014ReqdealBizListDto.getRecoverSpacAmtCny()) ;
                    //恢复总额(人民币)
                    BigDecimal recoverAmtCny = BigDecimalUtil.replaceNull(cmisLmt0014ReqdealBizListDto.getRecoverAmtCny()) ;
                    //台账编号--接口中推送的台账编号
                    String dealBizNo = cmisLmt0014ReqdealBizListDto.getDealBizNo() ;

                    if(StringUtils.nonBlank(cmisBizClientService.selectAccCvrsBillNo(dealBizNo))){
                        dealBizNo = cmisBizClientService.selectAccCvrsBillNo(dealBizNo) ;
                        cmisLmt0014ReqdealBizListDto.setDealBizNo(dealBizNo);
                    }else{
                        if(StringUtils.nonBlank(cmisBizClientService.selectAccTfLocBillNo(dealBizNo))){
                            dealBizNo = cmisBizClientService.selectAccTfLocBillNo(dealBizNo) ;
                            cmisLmt0014ReqdealBizListDto.setDealBizNo(dealBizNo);
                        }
                    }

                    if(StringUtils.isBlank(dealBizNo)){
                        logger.info("CmisLmt0014接口交易，请求字段dealBizNo为空，报错------>continue");
                        throw new Exception("台账编号不允许为空，请检查");
                    }
                    //获取台账信息
                    ContAccRel contAccRel = contAccRelService.selectContAccRelByTranAccNo(dealBizNo) ;
                    Map<String, BigDecimal> amtMap = getRecoverAmtMap(recoverAmtCny, recoverSpacAmtCny) ;
                    if(CmisLmtConstants.STD_RECOVER_TYPE_01.equals(recoverType)){
                        if(Objects.nonNull(contAccRel)){
                            amtMap = getRecoverAmtMap(contAccRel.getAccTotalBalanceAmtCny(), contAccRel.getAccSpacBalanceAmtCny()) ;
                        }else{
                            //List<LmtContRel> contRelList = lmtContRelService.selectLmtContRelByDealBizNo(dealBizNo) ;
                            List<LmtContRel> contRelList = lmtContRelService.selectLmtContRelListByDealBizNo(dealBizNo, CmisLmtConstants.STD_ZB_BIZ_ATTR_2) ;
                            if(CollectionUtils.isNotEmpty(contRelList)){
                                LmtContRel contRel = contRelList.get(0) ;
                                amtMap = getRecoverAmtMap(contRel.getBizTotalBalanceAmtCny(), contRel.getBizSpacBalanceAmtCny()) ;
                            }
                        }
                    }


                    /**
                     * 台账记录为空，即 lmt_cont_rel 做台账，恢复这种类型的还款，例如：代开银票等业务;
                     * 排除最高额协议的情况
                     */
                    if(Objects.isNull(contAccRel)){
                        logger.info("CmisLmt0014台账记录为空，即 lmt_cont_rel 做台账，恢复这种类型的还款{}", JSON.toJSONString(amtMap));
                        recoverLmtContRelLoan(resultMap, cmisLmt0014ReqdealBizListDto, amtMap);
                    }else{
                        /**
                         * 台账记录不为空，处理情况包括最高额协议，一般担保合同，最高额度担保合同等情况处理
                         */
                        logger.info("CmisLmt0014台账记录不为空，处理情况包括最高额协议，一般担保合同，最高额度担保合同等情况处理还款{}", JSON.toJSONString(amtMap));
                        String contStatus = contAccRel.getStatus() ;
                        if(CmisLmtConstants.STD_ZB_BIZ_STATUS_300.equals(contStatus) || CmisLmtConstants.STD_ZB_BIZ_STATUS_400.equals(contStatus)){
                            logger.info("台账编号{}，状态{}【300-结清已使用，400-结清未使用】, 已结清合同，不进行恢复操作", contAccRel.getTranAccNo(), contStatus);
                            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
                            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
                            return resqDto ;
                        }
                        //获取交易业务编号 （分项占用关系）
                        String lmtContNo = contAccRel.getDealBizNo() ;
                        List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelListByDealBizNo(lmtContNo, CmisLmtConstants.STD_ZB_BIZ_ATTR_1) ;
                        if(CollectionUtils.isEmpty(lmtContRelList)){
                            throw new YuspException(EclEnum.ECL070018.key,EclEnum.ECL070018.value);
                        }

                        //如果是交易撤销，则撤销金额以台账的占用总额和敞口为准
                        if(CmisLmtConstants.STD_RECOVER_TYPE_06.equals(recoverType)){
                            recoverSpacAmtCny = contAccRel.getAccSpacAmtCny();
                            recoverAmtCny = contAccRel.getAccTotalAmtCny();
                        }

                        //根据余额恢复
                        if(CmisLmtConstants.STD_ZB_RECOVER_STD_01.equals(recoverStd)){
                            BigDecimal bizTotalBalanceAmtCny = BigDecimalUtil.replaceNull(contAccRel.getAccTotalBalanceAmtCny()) ;
                            BigDecimal bizSpacBalanceAmtCny = BigDecimalUtil.replaceNull(contAccRel.getAccSpacBalanceAmtCny()) ;

                            recoverSpacAmtCny = bizSpacBalanceAmtCny.subtract(spacBalanceAmtCny);
                            recoverAmtCny = bizTotalBalanceAmtCny.subtract(balanceAmtCny);
                            //获取恢复金额
                            amtMap = getRecoverAmtMap(recoverAmtCny, recoverSpacAmtCny) ;
                            /**
                             * add by 2021-11-19
                             * 部分未用退回，按余额恢复，考虑到场景:
                             *      eg:    台账总额1000万  敞口1000万 还款100万  剩余900万  此时进行额度未用部分退回300万  退回后的额度处理
                             *      按照余额恢复 推送总额700万 敞口总额700万  总余额600万 总敞口余额600万
                             *      撤销占用300万  额度撤销占用300万
                             *      公式：
                             *      额度恢复总额=台账总额【AccTotalAmtCny】 - 推送总额【creditTotal】 = 1000-700=300万
                             *      额度恢复敞口=台账敞口额【AccSpacAmtCny】 - 推送总额【creditSpacTotal】 = 1000-700=300万
                             */
                            if(CmisLmtConstants.STD_RECOVER_TYPE_08.equals(recoverType)){
                                BigDecimal accTotalAmtCny = BigDecimalUtil.replaceNull(contAccRel.getAccTotalAmtCny()) ;
                                BigDecimal accSpacAmtCny = BigDecimalUtil.replaceNull(contAccRel.getAccSpacAmtCny()) ;
                                recoverAmtCny = accTotalAmtCny.subtract(NumberUtils.nullDefaultZero(cmisLmt0014ReqdealBizListDto.getCreditTotal()));
                                recoverSpacAmtCny = accSpacAmtCny.subtract(NumberUtils.nullDefaultZero(cmisLmt0014ReqdealBizListDto.getCreditSpacTotal()));
                                amtMap = getRecoverAmtMap(recoverAmtCny, recoverSpacAmtCny) ;
                                logger.info("银票部分未用退回，处理恢复额度金额{}", JSON.toJSONString(amtMap));
                            }
                        }

                        //add by lizx 2021-11-19 担保公司作为担保，进行额度恢复处理，记录原始额度数据信息，用于计算是否恢复担保公司额度
                        for (LmtContRel contRel : lmtContRelList) {
                            if(CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(contRel.getLmtType())){
                                amtMap.put("dbOccRelAmt", contRel.getBizSpacBalanceAmtCny()) ;
                            }
                        }
                        logger.info("CmisLmt0014 根据余额恢复开始1 ，恢复金额数据{}", JSON.toJSONString(amtMap));
                        List<Map<String, String>> lmtAddList = new ArrayList<>();
                        for (LmtContRel lmtContRel : lmtContRelList) {
                            logger.info("CmisLmt0014 处理合同数据信息{}", JSON.toJSONString(lmtContRel));
                            //分项编号
                            String subSerno = lmtContRel.getLimitSubNo() ;
                            //授信类型
                            String lmtType = lmtContRel.getLmtType() ;
                            //合同类型 1 一般合同 2 最高额合同 3 最高额协议
                            String dealBizType = lmtContRel.getDealBizType() ;
                            //若台账关联合同关联的授信分项存在授信总额累加：授信总额累加 = if(授信总额累加 - 本次恢复<0,0,授信总额累加 - 本次恢复)，
                            ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(subSerno);
                            //恢复台账信息 还款操作
                            if(CmisLmtConstants.STD_RECOVER_TYPE_03.equals(recoverType)){
                                //一般还款
                                exceRecoverType03(resultMap, contAccRel, amtMap, lmtContRel, lmtType, dealBizType, CmisLmtConstants.STD_RECOVER_TYPE_03);
                            }else if(CmisLmtConstants.STD_RECOVER_TYPE_01.equals(recoverType)){
                                //结清恢复
                                settleContAccRel(lmtContRel, amtMap, contAccRel, resultMap) ;
                            }else if(CmisLmtConstants.STD_RECOVER_TYPE_06.equals(recoverType)){
                                //撤销占用
                                comm4Service.cancelContAccRel(contAccRel, lmtContRel, resultMap);
                            }else if(CmisLmtConstants.STD_RECOVER_TYPE_07.equals(recoverType)){
                                //信用证修改
                                creditLmtExecute(cmisLmt0014ReqdealBizListDto, resultMap, contAccRel, lmtContRel);
                            }else if(CmisLmtConstants.STD_RECOVER_TYPE_08.equals(recoverType)){
                                //未用退回
                                unUsedReturnOcc(lmtContRel, amtMap, contAccRel, resultMap) ;
                            }else{
                                //追加办证金额
                                comm4Service.recoverApprLmtSubBasicInfo(contAccRel, lmtContRel, amtMap, resultMap) ;
                                //追加保证金
                                //marginCall(resultMap, recoverSpacAmtCny, contAccRel, lmtContRel);
                            }
                            Map addAmtMap = new HashMap();
                            addAmtMap.put("apprSubSerno", subSerno) ;
                            addAmtMap.put("lmtType", lmtType) ;
                            addAmtMap.put("dealBizType", dealBizType) ;
                            lmtAddList.add(addAmtMap) ;
                        }

                        //国界票据出账，恢复额度的同时，恢复白名单额度，应为白名单额度时lmtcontrel 台账占用，单独处理
                        List<LmtContRel> contRelList = lmtContRelService.selectLmtContRelListByDealBizNo(dealBizNo, CmisLmtConstants.STD_ZB_BIZ_ATTR_2) ;
                        if(CollectionUtils.isNotEmpty(contRelList)){
                            logger.info("国界出账操作");
                            LmtContRel contRel = contRelList.get(0) ;
                            //属性  2 台账  1 一般合同
                            String bizAttr = contRel.getBizAttr() ;
                            // 06 白名单额度
                            String lmtType = contRel.getLmtType() ;
                            if(CmisLmtConstants.STD_ZB_LMT_TYPE_06.equals(lmtType) && CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
                                recoverWhiteInfo(contRel, amtMap, resultMap) ;
                                if(CmisLmtConstants.STD_RECOVER_TYPE_06.equals(recoverType)){
                                    lmtContRelService.deleteByPrimaryKey(contRel.getPkId()) ;
                                }
                            }
                        }

                        if(!CmisLmtConstants.STD_RECOVER_TYPE_07.equals(recoverType)){
                            //根据恢复类型，进行台账恢复操作 Map<String, BigDecimal> amtMap  recoverSpacAmtCny, recoverAmtCny
                            recoverContAccRel(recoverType, contAccRel, amtMap, resultMap) ;
                        }

                        System.out.println(JSON.toJSONString(lmtAddList));
                        // 累加金额最后处理
                        if(CollectionUtils.isNotEmpty(lmtAddList)){
                            for (Map<String, String> map : lmtAddList) {
                                comm4Service.lmtAddAmtExecute(dealBizNo, map.get("apprSubSerno"), map.get("lmtType"), map.get("dealBizType"));
                            }
                        }
                    }
                }
            }
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("台账恢复接口报错：", e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.value);
        return resqDto;
    }

    /**
     * @作者:lizx
     * @方法名称: 还款操作处理
     * @方法描述:  
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/11/16 9:46
     * @param resultMap: 
     * @param contAccRel:
     * @param amtMap:
     * @param lmtContRel:
     * @param lmtType:
     * @param dealBizType:
     * @param stdRecoverType03:
     * @return: void
     * @算法描述: 无
    */
    private void exceRecoverType03(Map<String, String> resultMap, ContAccRel contAccRel, Map<String, BigDecimal> amtMap, LmtContRel lmtContRel, String lmtType, String dealBizType, String stdRecoverType03) {
        if (CmisLmtConstants.STD_ZB_LMT_TYPE_03.equals(lmtType)) {
            //一般额合同，还款时，恢复额度金额  || 最高额合同，判断合同是否到期未结清，如果是，恢复最高额
            recoverCoopSubInfo(lmtContRel, amtMap, resultMap);
        } else if (CmisLmtConstants.STD_ZB_LMT_TYPE_06.equals(lmtType)) {
            //白名单额度，查询白名单对应额度信息，更新白名单额度金额
            //一般额合同，还款时，恢复额度金额  || 最高额合同，判断合同是否到期未结清，如果是，恢复最高额
            if (CmisLmtConstants.STD_ZB_CONT_TYPE_1.equals(dealBizType) || (CmisLmtConstants.STD_ZB_CONT_TYPE_2.equals(dealBizType) &&
                    lmtContRel.getBizStatus().equals(CmisLmtConstants.STD_ZB_BIZ_STATUS_500))) {
                recoverWhiteInfo(lmtContRel, amtMap, resultMap);
            }
        } else {
            comm4Service.recoverApprLmtSubBasicInfo(contAccRel, lmtContRel, amtMap, resultMap);
            if (Objects.nonNull(contAccRel)) {
                comm4Service.recoverLmtSigInfo(contAccRel, resultMap, stdRecoverType03, amtMap);
            }
        }
    }

    /**
     * @作者:lizx
     * @方法名称: creditLmtExecute
     * @方法描述:  信用证修改：信用证修改针对额度
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/10/18 17:18
     * @param cmisLmt0014ReqdealBizListDto:
     * @param resultMap:
     * @param contAccRel:
     * @param lmtContRel:
     * @return: void
     * @算法描述: 无
    */
    private void creditLmtExecute(CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto, Map<String, String> resultMap, ContAccRel contAccRel, LmtContRel lmtContRel) {
        logger.info("信用证修改开始{}-----------start", JSON.toJSONString(cmisLmt0014ReqdealBizListDto));
        //台账编号--接口中推送的台账编号
        String dealBizNo = cmisLmt0014ReqdealBizListDto.getDealBizNo() ;
        //合同类型 1 一般合同 2 最高额合同 3 最高额协议
        String dealBizType = lmtContRel.getDealBizType() ;
        //台账总余额（人民币）
        BigDecimal balanceAmtCny = BigDecimalUtil.replaceNull(cmisLmt0014ReqdealBizListDto.getBalanceAmtCny()) ;
        //台账敞口余额（人民币）
        BigDecimal spacBalanceAmtCny = BigDecimalUtil.replaceNull(cmisLmt0014ReqdealBizListDto.getSpacBalanceAmtCny()) ;
        BigDecimal creditTotal = NumberUtils.nullDefaultZero(cmisLmt0014ReqdealBizListDto.getCreditTotal()) ;
        BigDecimal creditSpacTotal = NumberUtils.nullDefaultZero(cmisLmt0014ReqdealBizListDto.getCreditSpacTotal()) ;

        BigDecimal accTotalAmtCny = NumberUtils.nullDefaultZero(contAccRel.getAccTotalAmtCny()) ;
        BigDecimal accTotalBalanceAmtCny = NumberUtils.nullDefaultZero(contAccRel.getAccTotalBalanceAmtCny()) ;
        BigDecimal accSpacAmtCny = NumberUtils.nullDefaultZero(contAccRel.getAccSpacAmtCny()) ;
        BigDecimal accSpacBalanceAmtCny = NumberUtils.nullDefaultZero(contAccRel.getAccSpacBalanceAmtCny()) ;

        BigDecimal afAccTotalAmtCny = creditTotal.subtract(accTotalAmtCny) ;
        BigDecimal afAccTotalBalanceAmtCny = balanceAmtCny.subtract(accTotalBalanceAmtCny) ;
        BigDecimal afAccSpacAmtCny = creditSpacTotal.subtract(accSpacAmtCny) ;
        BigDecimal afAccSpacBalanceAmtCny = spacBalanceAmtCny.subtract(accSpacBalanceAmtCny) ;

        //信用证修改 一般担保合同 和 最高担保合同，到期后
        //if(CmisLmtConstants.STD_ZB_CONT_TYPE_1.equals(dealBizType) || (CmisLmtConstants.STD_ZB_CONT_TYPE_2.equals(dealBizType) &&
        if(lmtContRel.getBizStatus().equals(CmisLmtConstants.STD_ZB_BIZ_STATUS_500)){
            lmtContRel.setBizTotalBalanceAmtCny(lmtContRel.getBizTotalBalanceAmtCny().add(afAccTotalBalanceAmtCny));
            lmtContRel.setBizSpacBalanceAmtCny(lmtContRel.getBizTotalBalanceAmtCny().add(afAccSpacBalanceAmtCny)) ;
            lmtContRelService.updateByPKeyInApprLmtChgDetails(lmtContRel, resultMap) ;
        }
        //一般合同活最高额合同
        if(CmisLmtConstants.STD_ZB_CONT_TYPE_1.equals(dealBizType) || (CmisLmtConstants.STD_ZB_CONT_TYPE_2.equals(dealBizType))){
            logger.info("信用证修改一般合同 or 最高额合同-----------start");
            String apprSubSerno = lmtContRel.getLimitSubNo() ;
            //获取额度分项
            ApprLmtSubBasicInfo subBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(apprSubSerno) ;
            updateCreditApprLmt(resultMap, afAccTotalAmtCny, afAccTotalBalanceAmtCny, afAccSpacAmtCny, afAccSpacBalanceAmtCny, subBasicInfo);
            //处理上级分项信息
            String parentId = subBasicInfo.getParentId() ;
            if(StringUtils.nonBlank(parentId)){
                ApprLmtSubBasicInfo parSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(parentId) ;
                updateCreditApprLmt(resultMap, afAccTotalAmtCny, afAccTotalBalanceAmtCny, afAccSpacAmtCny, afAccSpacBalanceAmtCny, parSubBasicInfo);
            }
            logger.info("信用证修改一般合同 or 最高额合同-----------end");
        }else if(CmisLmtConstants.STD_ZB_CONT_TYPE_3.equals(dealBizType)){
            logger.info("信用证修改最高额协议处理-----------start");
            String apprSubSerno = lmtContRel.getLimitSubNo() ;
            //获取额度分项
            ApprLmtSubBasicInfo subBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(apprSubSerno) ;
            updateCreditApprLmt(resultMap, afAccTotalAmtCny, afAccTotalBalanceAmtCny, afAccSpacAmtCny, afAccSpacBalanceAmtCny, subBasicInfo);
            //处理分项明细
            List<LmtContRel> detailsLmtContRel = lmtContRelService.selectLmtContRelByDealBizNo(dealBizNo) ;
            if(CollectionUtils.isNotEmpty(detailsLmtContRel)){
                for (LmtContRel contRel : detailsLmtContRel) {
                    //更改台账金额
                    contRel.setBizTotalAmtCny(creditTotal);
                    contRel.setBizTotalBalanceAmtCny(balanceAmtCny);
                    contRel.setBizSpacAmtCny(creditSpacTotal);
                    contRel.setBizSpacBalanceAmtCny(spacBalanceAmtCny);
                    lmtContRelService.updateByPKeyInApprLmtChgDetails(contRel, resultMap);
                    String subInfoSerno = contRel.getLimitSubNo() ;
                    if(StringUtils.nonBlank(subInfoSerno)){
                        ApprLmtSubBasicInfo lmtSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(subInfoSerno) ;
                        updateCreditApprLmt(resultMap, afAccTotalAmtCny, afAccTotalBalanceAmtCny, afAccSpacAmtCny, afAccSpacBalanceAmtCny, lmtSubBasicInfo);
                    }
                }
            }
            logger.info("信用证修改最高额协议处理-----------end");
        }
        //更改台账金额
        contAccRel.setAccTotalAmtCny(creditTotal);
        contAccRel.setAccTotalBalanceAmtCny(balanceAmtCny);
        contAccRel.setAccSpacAmtCny(creditSpacTotal);
        contAccRel.setAccSpacBalanceAmtCny(spacBalanceAmtCny);
        contAccRelService.updateByPKeyInApprLmtChgDetails(contAccRel, resultMap);
        logger.info("信用证修改结束-----------end");
    }

    /**
     * @作者:lizx
     * @方法名称: updateCreditApprLmt
     * @方法描述:  信用证修改，修改额度分项用到的方法
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/10/18 17:19
     * @param resultMap:
     * @param afAccTotalAmtCny:
     * @param afAccTotalBalanceAmtCny:
     * @param afAccSpacAmtCny:
     * @param afAccSpacBalanceAmtCny:
     * @param lmtSubBasicInfo:
     * @return: void
     * @算法描述: 无
    */
    private void updateCreditApprLmt(Map<String, String> resultMap, BigDecimal afAccTotalAmtCny, BigDecimal afAccTotalBalanceAmtCny,
                                     BigDecimal afAccSpacAmtCny, BigDecimal afAccSpacBalanceAmtCny, ApprLmtSubBasicInfo lmtSubBasicInfo) {
        logger.info("信用证修改处理额度信息-----------start");
        if(CmisLmtConstants.YES_NO_Y.equals(lmtSubBasicInfo.getIsRevolv())){
            lmtSubBasicInfo.setLoanBalance(NumberUtils.nullDefaultZero(lmtSubBasicInfo.getLoanBalance()).add(afAccTotalBalanceAmtCny));
            lmtSubBasicInfo.setLoanSpacBalance(NumberUtils.nullDefaultZero(lmtSubBasicInfo.getLoanSpacBalance()).add(afAccSpacBalanceAmtCny));

            if(CmisLmtConstants.YES_NO_Y.equals(lmtSubBasicInfo.getIsLriskLmt())){
                lmtSubBasicInfo.setPvpOutstndAmt(NumberUtils.nullDefaultZero(lmtSubBasicInfo.getPvpOutstndAmt()).add(afAccTotalBalanceAmtCny));
                lmtSubBasicInfo.setAvlOutstndAmt(NumberUtils.nullDefaultZero(lmtSubBasicInfo.getAvlOutstndAmt()).subtract(afAccTotalBalanceAmtCny));
            }else{
                lmtSubBasicInfo.setPvpOutstndAmt(NumberUtils.nullDefaultZero(lmtSubBasicInfo.getPvpOutstndAmt()).add(afAccSpacBalanceAmtCny));
                lmtSubBasicInfo.setAvlOutstndAmt(NumberUtils.nullDefaultZero(lmtSubBasicInfo.getAvlOutstndAmt()).subtract(afAccSpacBalanceAmtCny));
            }
        }else {
            //不可循环额度处理
            lmtSubBasicInfo.setLoanBalance(NumberUtils.nullDefaultZero(lmtSubBasicInfo.getLoanBalance()).add(afAccTotalBalanceAmtCny));
            lmtSubBasicInfo.setLoanSpacBalance(NumberUtils.nullDefaultZero(lmtSubBasicInfo.getLoanSpacBalance()).add(afAccSpacBalanceAmtCny));

            if (CmisLmtConstants.YES_NO_Y.equals(lmtSubBasicInfo.getIsLriskLmt())) {
                lmtSubBasicInfo.setPvpOutstndAmt(NumberUtils.nullDefaultZero(lmtSubBasicInfo.getPvpOutstndAmt()).add(afAccTotalAmtCny));
                lmtSubBasicInfo.setAvlOutstndAmt(NumberUtils.nullDefaultZero(lmtSubBasicInfo.getAvlOutstndAmt()).subtract(afAccTotalAmtCny));
            } else {
                lmtSubBasicInfo.setPvpOutstndAmt(NumberUtils.nullDefaultZero(lmtSubBasicInfo.getPvpOutstndAmt()).add(afAccSpacAmtCny));
                lmtSubBasicInfo.setAvlOutstndAmt(NumberUtils.nullDefaultZero(lmtSubBasicInfo.getAvlOutstndAmt()).subtract(afAccSpacAmtCny));
            }
        }
        apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(lmtSubBasicInfo, resultMap) ;
        logger.info("信用证修改处理额度信息-----------end");
    }

    /**
     * @作者:lizx
     * @方法名称: recoverLmtContRelLoan
     * @方法描述:  lmt_cont_rel 做为台账，进行恢复造作，例如代开银票，转贴现等业务（不包含最高额意义的情况，因为最高额协议，向下会挂一条台账
     * 【cont_acc_rel】 记录）
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/10/12 19:44
     * @param resultMap:
     * @param cmisLmt0014ReqdealBizListDto:
     * @param amtMap:
     * @return: void
     * @算法描述: 无
    */
    private void recoverLmtContRelLoan(Map<String, String> resultMap, CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto,
                                       Map<String, BigDecimal> amtMap) {
        //恢复标准值 01-余额  02-差额
        String recoverStd = cmisLmt0014ReqdealBizListDto.getRecoverStd() ;
        //台账总余额（人民币）
        BigDecimal balanceAmtCny = BigDecimalUtil.replaceNull(cmisLmt0014ReqdealBizListDto.getBalanceAmtCny()) ;
        //台账敞口余额（人民币）
        BigDecimal spacBalanceAmtCny = BigDecimalUtil.replaceNull(cmisLmt0014ReqdealBizListDto.getSpacBalanceAmtCny()) ;
        //台账编号--接口中推送的台账编号
        String dealBizNo = cmisLmt0014ReqdealBizListDto.getDealBizNo() ;
        //恢复类型 01-结清恢复  03-还款 04-追加保证金  06-撤销占用
        String  recoverType = cmisLmt0014ReqdealBizListDto.getRecoverType() ;
        //恢复敞口金额(人民币)
        BigDecimal recoverSpacAmtCny = BigDecimalUtil.replaceNull(cmisLmt0014ReqdealBizListDto.getRecoverSpacAmtCny()) ;
        //恢复总额(人民币)
        BigDecimal recoverAmtCny = BigDecimalUtil.replaceNull(cmisLmt0014ReqdealBizListDto.getRecoverAmtCny()) ;
        //用台账编号查询额度占用关系中信息
        List<LmtContRel> contRelList =  lmtContRelService.selectLmtContRelListByDealBizNo(dealBizNo, CmisLmtConstants.STD_ZB_BIZ_ATTR_2) ;
        if(CollectionUtils.isNotEmpty(contRelList)){
            for (LmtContRel contRel : contRelList) {
                String lmtType = contRel.getLmtType();
                if(CmisLmtConstants.STD_ZB_RECOVER_STD_01.equals(recoverStd)){
                    BigDecimal bizTotalBalanceAmtCny = BigDecimalUtil.replaceNull(contRel.getBizTotalBalanceAmtCny()) ;
                    BigDecimal bizSpacBalanceAmtCny = BigDecimalUtil.replaceNull(contRel.getBizSpacBalanceAmtCny()) ;
                    //获取恢复金额
                    amtMap = getRecoverAmtMap(bizTotalBalanceAmtCny.subtract(balanceAmtCny), bizSpacBalanceAmtCny.subtract(spacBalanceAmtCny)) ;
                }
                //合同类型 1 一般合同 2 最高额合同 3 最高额协议
                String dealBizType = contRel.getDealBizType();
                //恢复台账信息 还款操作
                if (CmisLmtConstants.STD_RECOVER_TYPE_03.equals(recoverType)) {
                    if (CmisLmtConstants.STD_ZB_LMT_TYPE_03.equals(lmtType)) {
                        //一般额合同，还款时，恢复额度金额  || 最高额合同，判断合同是否到期未结清，如果是，恢复最高额
                        recoverCoopSubInfo(contRel, amtMap, resultMap);
                    } else if (CmisLmtConstants.STD_ZB_LMT_TYPE_06.equals(lmtType)) {
                        //白名单额度，更新白名单额度金额，一般额合同，还款时，恢复额度金额  || 最高额合同，判断合同是否到期未结清，如果是，恢复最高额
                        recoverWhiteInfo(contRel, amtMap, resultMap);
                    } else {
                        comm4Service.recoverApprLmtSubBasicInfo(null, contRel, amtMap, resultMap);
                    }
                } else if (CmisLmtConstants.STD_RECOVER_TYPE_01.equals(recoverType)) {
                    //结清恢复
                    settleContAccRel(contRel, amtMap, null, resultMap);
                } else if (CmisLmtConstants.STD_RECOVER_TYPE_06.equals(recoverType)) {
                    comm4Service.cancelContAccRel(null, contRel, resultMap);
                }

                //恢复台账金额
                if (CmisLmtConstants.STD_RECOVER_TYPE_03.equals(recoverType)) {
                    //敞口余额
                    BigDecimal BizSpacBalanceAmtCny = BigDecimalUtil.replaceNull(contRel.getBizSpacBalanceAmtCny()) ;
                    BigDecimal afBizSpacBalanceAmtCny =BizSpacBalanceAmtCny.subtract(recoverSpacAmtCny) ;

                    //总额余额
                    BigDecimal BizTotalBalAmtCny = BigDecimalUtil.replaceNull(contRel.getBizTotalBalanceAmtCny()) ;
                    BigDecimal afBizTotalBalAmtCny = BizTotalBalAmtCny.subtract(recoverAmtCny) ;

                    contRel.setBizSpacBalanceAmtCny(afBizSpacBalanceAmtCny);
                    contRel.setBizTotalBalanceAmtCny(afBizTotalBalAmtCny);
                    if(afBizSpacBalanceAmtCny.compareTo(BigDecimal.ZERO) == 0 && afBizTotalBalAmtCny.compareTo(BigDecimal.ZERO) == 0){
                        logger.info("该信息已结清，更改状态为【300-结清已使用】");
                        contRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_300);
                    }
                    lmtContRelService.updateByPKeyInApprLmtChgDetails(contRel, resultMap) ;
                }else if (CmisLmtConstants.STD_RECOVER_TYPE_01.equals(recoverType)) {
                    contRel.setBizSpacBalanceAmtCny(BigDecimal.ZERO);
                    contRel.setBizTotalBalanceAmtCny(BigDecimal.ZERO);
                    logger.info("该信息已结清，更改状态为【300-结清已使用】");
                    contRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_300);
                    lmtContRelService.updateByPKeyInApprLmtChgDetails(contRel, resultMap) ;
                }else if (CmisLmtConstants.STD_RECOVER_TYPE_06.equals(recoverType)) {
                    contRel.setBizSpacBalanceAmtCny(BigDecimal.ZERO);
                    contRel.setBizTotalBalanceAmtCny(BigDecimal.ZERO);
                    logger.info("该信息撤销占用，删除操作");
                    contRel.setOprType(CmisLmtConstants.OPR_TYPE_DELETE);
                    lmtContRelService.updateByPKeyInApprLmtChgDetails(contRel, resultMap) ;
                }
                //累加金额处理
                //分项编号
                String apprSubSerno = contRel.getLimitSubNo() ;
                comm4Service.lmtAddAmtExecute(dealBizNo, apprSubSerno, lmtType, dealBizType);
            }
        }else{
            throw new YuspException(EclEnum.ECL070124.key,"【"+ dealBizNo +"】" + EclEnum.ECL070124.value);
        }
    }

    /**
     * @作者:lizx
     * @方法名称:
     * @方法描述: 追加保证金还款
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/9/18 14:17
     * @param resultMap:
     * @param recoverSpacAmtCny:
     * @param contAccRel:
     * @param lmtContRel:
     * @return: void
     * @算法描述: 无
    */
    private void marginCall(Map<String, String> resultMap, BigDecimal recoverSpacAmtCny, ContAccRel contAccRel, LmtContRel lmtContRel) {
        if(Objects.nonNull(contAccRel)){
            //追加保证金，恢复台账敞口
            //修改前
            BigDecimal bfAccSpacBalanceAmtCny = contAccRel.getAccSpacBalanceAmtCny() ;
            //修改后
            BigDecimal afAccSpacBalanceAmtCny = bfAccSpacBalanceAmtCny.subtract(recoverSpacAmtCny) ;
            contAccRel.setAccSpacBalanceAmtCny(afAccSpacBalanceAmtCny);
            //保证金额金额更新
            contAccRel.setSecurityAmt(BigDecimalUtil.replaceNull(contAccRel.getSecurityAmt()).add(recoverSpacAmtCny));
            logger.info("【contaccrel】追加保证金恢复【"+ contAccRel.getTranAccNo()+"】,"
                    +",占用敞口余额（折人民币) 【AccSpacBalanceAmtCny】更改为"+bfAccSpacBalanceAmtCny + " --> " + afAccSpacBalanceAmtCny
            );
            contAccRelService.updateByPKeyInApprLmtChgDetails(contAccRel, resultMap) ;
        }

        //交易业务类型    1-一般合同  2-最高额合同  3-最高额授信协议
        String bizAttr = lmtContRel.getBizAttr() ;
        String bizStatus = lmtContRel.getBizStatus() ;
        //恢复额度信息 交易业务类型
        String dealBizType = lmtContRel.getDealBizType() ;
        Map<String, BigDecimal> amtMap = new HashMap<>();
        amtMap.put("recoverAmtCny", BigDecimal.ZERO) ;
        amtMap.put("recoverSpacAmtCny", recoverSpacAmtCny) ;
        //add by 2021-08-30 占用敞口余额，只有最高额合同/最高额授信协议，未到期的情况下，才不进行恢复，其余情况都恢复
        if(comm4Service.checkRecoverLmtContRelBal(dealBizType, bizStatus)){
            //恢复合同金额
            comm4Service.recoverLmtcontRelBal(lmtContRel, amtMap, resultMap);
        }

        //分项额度编号
        String apprSubSerno = lmtContRel.getLimitSubNo() ;
        //一般担保合同或者最高额担保合同，恢复额度分项信息
        if(CmisLmtConstants.DEAL_BIZ_TYPE_1.equals(dealBizType) || CmisLmtConstants.DEAL_BIZ_TYPE_2.equals(dealBizType)){
            ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(apprSubSerno) ;
            //恢复敞口已用额度，敞口用信余额，已出账金额，可出账金额
            marginCallUpdateLmt(resultMap, recoverSpacAmtCny, apprLmtSubBasicInfo);
            String parentId = apprLmtSubBasicInfo.getParentId() ;
            if(StringUtils.nonBlank(parentId)){
                apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(parentId) ;
                if(Objects.nonNull(apprLmtSubBasicInfo)){
                    marginCallUpdateLmt(resultMap, recoverSpacAmtCny, apprLmtSubBasicInfo);
                }
            }
        }else if(CmisLmtConstants.DEAL_BIZ_TYPE_3.equals(dealBizType)){
            //最高额协议信息
            ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(apprSubSerno) ;
            marginCallUpdateLmt(resultMap, recoverSpacAmtCny, apprLmtSubBasicInfo);

            //处理向下分项明细以及额度占用关系中的台账信息，获取台账信息
            String tranAccNo = contAccRel.getTranAccNo() ;
            //根据用信编号获取额度占用的台账信息
            //List<LmtContRel> lmtContRelOcc = lmtContRelService.selectLmtContRelByDealBizNo(tranAccNo) ;
            List<LmtContRel> lmtContRelOcc = lmtContRelService.selectLmtContRelListByDealBizNo(tranAccNo, CmisLmtConstants.STD_ZB_BIZ_ATTR_2) ;
            for (LmtContRel contRel : lmtContRelOcc) {
                //台账敞口余额
                BigDecimal bizSpacBalanceAmtCny = BigDecimalUtil.replaceNull(contRel.getBizSpacBalanceAmtCny()) ;
                //恢复敞口余额后金额
                bizSpacBalanceAmtCny = bizSpacBalanceAmtCny.subtract(recoverSpacAmtCny) ;

                contRel.setBizSpacBalanceAmtCny(bizSpacBalanceAmtCny);
                lmtContRelService.updateByPKeyInApprLmtChgDetails(contRel, resultMap) ;

                ApprLmtSubBasicInfo apprLmtSubBasicInfoDetails = apprLmtSubBasicInfoService.selectByApprSubSerno(contRel.getLimitSubNo());
                marginCallUpdateLmt(resultMap, recoverSpacAmtCny, apprLmtSubBasicInfoDetails);
            }
        }
    }

    /**
     * @作者:lizx
     * @方法名称: marginCallUpdateLmt
     * @方法描述:  追加担保，更新追加对应额度信息
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/10/6 20:12
     * @param resultMap:
     * @param recoverSpacAmtCny:
     * @param apprLmtSubBasicInfo:
     * @return: void
     * @算法描述: 无
    */
    private void marginCallUpdateLmt(Map<String, String> resultMap, BigDecimal recoverSpacAmtCny, ApprLmtSubBasicInfo apprLmtSubBasicInfo) {
        apprLmtSubBasicInfo.setSpacOutstndAmt(apprLmtSubBasicInfo.getSpacOutstndAmt().subtract(recoverSpacAmtCny));
        apprLmtSubBasicInfo.setLoanSpacBalance(apprLmtSubBasicInfo.getLoanSpacBalance().subtract(recoverSpacAmtCny));
        apprLmtSubBasicInfo.setPvpOutstndAmt(apprLmtSubBasicInfo.getPvpOutstndAmt().subtract(recoverSpacAmtCny));
        apprLmtSubBasicInfo.setAvlOutstndAmt(apprLmtSubBasicInfo.getAvlOutstndAmt().add(recoverSpacAmtCny));
        apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfo, resultMap) ;
    }

    /**
     * 恢复批复主信息额度
     * @param subSerno
     * @param lmtContRel
     * @param amtMap
     */
    public void recoverAddToDepositSubBasicInfo(String subSerno, LmtContRel lmtContRel, Map<String, BigDecimal> amtMap){
        //查询分项信息
        ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(subSerno);
        Optional.ofNullable(apprLmtSubBasicInfo).orElseThrow(()->new YuspException(EclEnum.ECL070007.key,EclEnum.ECL070007.value));

        //已用总额
        apprLmtSubBasicInfo.setOutstndAmt(apprLmtSubBasicInfo.getOutstndAmt().subtract(amtMap.get("recoverAmtCny")));
        //敞口已用额度减少本次恢复合同总敞口金额
        apprLmtSubBasicInfo.setSpacOutstndAmt(apprLmtSubBasicInfo.getSpacOutstndAmt().subtract(amtMap.get("recoverSpacAmt")));
        //用信余额
        apprLmtSubBasicInfo.setLoanBalance(apprLmtSubBasicInfo.getLoanBalance().subtract(amtMap.get("recoverAmtCny")));
        //用信敞口余额
        apprLmtSubBasicInfo.setLoanSpacBalance(apprLmtSubBasicInfo.getLoanSpacBalance().subtract(amtMap.get("recoverAmtCny")));
        //已出账金额 = 已出账金额 - 本次恢复金额  && 已出账金额 = 已出账金额 - 本次恢复金额
        comm4Service.recoverAvlAndPvpOutstndAmt(apprLmtSubBasicInfo, amtMap.get("recoverAmtCny"), amtMap.get("recoverSpacAmt")) ;

        apprLmtSubBasicInfoMapper.updateByPrimaryKey(apprLmtSubBasicInfo) ;
        if(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_02.equals(apprLmtSubBasicInfo.getLmtSubType())){
            recoverAddToDepositSubBasicInfo(apprLmtSubBasicInfo.getLimitSubNo(), lmtContRel, amtMap) ;
        }else{
            return ;
        }
    }

    /**
     * 恢复合作方额度
     * @param lmtContRel
     * @param amtMap
     */
    public void recoverCoopSubInfo(LmtContRel lmtContRel, Map<String, BigDecimal> amtMap, Map<String, String> resultMap){
        logger.info("恢复合作方额度{}", JSON.toJSONString(lmtContRel));
        Map<String, BigDecimal> amtDBMap = new HashMap<>() ;
        amtDBMap.putAll(amtMap);
        //合作方额度，查询合作方分项信息，更新合作方额度金额
        ApprCoopSubInfo apprCoopSubInfo = apprCoopSubInfoService.selectAcsiInfoBySubSerno(lmtContRel.getLimitSubNo()) ;
        Optional.ofNullable(apprCoopSubInfo).orElseThrow(()->new YuspException(EclEnum.ECL070007.key,EclEnum.ECL070007.value));
        //恢复敞口金额
        BigDecimal recoverSpacAmtCny = NumberUtils.nullDefaultZero(amtMap.get("recoverSpacAmtCny")) ;
        //恢复总额
        BigDecimal recoverAmtCny = NumberUtils.nullDefaultZero(amtMap.get("recoverAmtCny")) ;

        String dealBizType = lmtContRel.getDealBizType() ;
        String bizStatus = lmtContRel.getBizStatus() ;
        //add by 2021-08-30 占用敞口余额，只有最高额合同/最高额授信协议，未到期的情况下，才不进行恢复，其余情况都恢复
        if(comm4Service.checkRecoverLmtContRelBal(dealBizType, bizStatus)){
            //恢复合同金额
            /**
             * add by 2021-08-30
             *对于将担保公司作为追加担保，同时所关联的主合同金额大于担保额度的（如主合同金额100，担保额度80），需要在合同生效时点全部占用担保公司额度，恢复时
             *按合同余额小于等于担保额度时，在根据还款金额恢复担保额度，即发生还款时，恢复的担保额度=Max(0,担保金额-合同金额)
             * eg: 担保该公司追加担保 80 万  自己担保100万  放款 100 万
             *                        还款金额      贷款余额     自己担保合同余额（一般）   担保公司余额
             *         第一次还款：       10万        90万          90万             80万      担保公司合同余额计算：10-100+80 = -10  Max(0,-10)=0
             *         第二次还款：       10万        80万          80万             80万      担保公司合同余额计算：10-90+80 = 0  Max(0,0)=0
             *         第三次还款：       10万        70万          70万             70万      担保公司合同余额计算：10-80+80 = 10  Max(0,10)=10
             *             .
             *             .
             *             .
             */
            if(Objects.nonNull(apprCoopSubInfo)){
                String limitPrdNo = apprCoopSubInfo.getLimitSubNo() ;
                //判断是否担保公司担保
                if(CmisLmtConstants.STD_ZB_PARTNER_TYPE_2.equals(limitPrdNo)){
                    logger.info("担保公司作为追加担保，恢复合同时根据公式恢复的担保额度=Max(0,担保金额-合同金额)");
                    //获取担保公司需恢复金额，首先查询主合同金额
                    BigDecimal contSpaceBal = BigDecimalUtil.replaceNull(lmtContRel.getBizSpacBalanceAmtCny()) ;
                    //获取主合同总额
                    BigDecimal contLoanBal = BigDecimalUtil.replaceNull(lmtContRel.getBizTotalBalanceAmtCny()) ;
                    //获取担保合同敞口余额
                    BigDecimal dbSpaceBal = NumberUtils.nullDefaultZero(amtMap.get("dbOccRelAmt")) ;
                    //担保合同本次恢复金额   本次恢复金额-法人合同余额+担保合同金额  10-80+80 = 10
                    BigDecimal dbRecoverSpacBal = (recoverSpacAmtCny.subtract(dbSpaceBal)).add(contSpaceBal) ;
                    BigDecimal dbRecoverBizTotalBal = (recoverAmtCny.subtract(dbSpaceBal)).add(contLoanBal) ;
                    //如果恢复的担保额度=Max(0,担保金额-合同金额)=0 ,不进行恢复操作
                    if(dbRecoverSpacBal.compareTo(BigDecimal.ZERO)<=0){
                        dbRecoverSpacBal = BigDecimal.ZERO ;
                    }
                    if(dbRecoverBizTotalBal.compareTo(BigDecimal.ZERO)<=0){
                        dbRecoverBizTotalBal = BigDecimal.ZERO ;
                    }
                    logger.info("担保公司作为追加担保，更改后恢复敞口金额{}， 恢复总额{}", dbRecoverBizTotalBal, dbRecoverSpacBal);
                    amtDBMap.put("recoverSpacAmtCny", dbRecoverSpacBal) ;
                    amtDBMap.put("recoverAmtCny", dbRecoverBizTotalBal) ;
                }
            }
            //恢复合同金额
            comm4Service.recoverLmtcontRelBal(lmtContRel, amtDBMap, resultMap);
        }
        //判断是否可循环，是则恢复
        if(CmisLmtConstants.YES_NO_Y.equals(apprCoopSubInfo.getIsRevolv())){
            if(comm4Service.checkRecoverLmtApprBal(dealBizType, bizStatus)) {
                //合作方已用额度 = 合作方已用额度-本次合同金额
                //修改前
                BigDecimal bfOutstndAmt = BigDecimalUtil.replaceNull(apprCoopSubInfo.getOutstndAmt()) ;
                //修改后
                BigDecimal afOutstndAmt = bfOutstndAmt.subtract(amtDBMap.get("recoverAmtCny")) ;
                apprCoopSubInfo.setOutstndAmt(afOutstndAmt);
                logger.info("【ApprCoopSubInfo】恢复【"+apprCoopSubInfo.getApprSubSerno()+"】," +
                        "已用额度【OutstndAmt】更改为："+ bfOutstndAmt + " --> " + afOutstndAmt);
            }
        }

        //用信余额 = 用信余额 - 本次恢复金额  修改前
        BigDecimal bfLoanBalance = BigDecimalUtil.replaceNull(apprCoopSubInfo.getLoanBalance()) ;
        //修改后
        BigDecimal afLoanBalance = bfLoanBalance.subtract(amtDBMap.get("recoverAmtCny")) ;
        apprCoopSubInfo.setLoanBalance(afLoanBalance);
        logger.info("【ApprCoopSubInfo】恢复【"+apprCoopSubInfo.getApprSubSerno()+"】," +
                "已用额度【LoanBalance】更改为："+ bfLoanBalance + " --> " + afLoanBalance);
        apprCoopSubInfoService.updateByPKeyInApprLmtChgDetails(apprCoopSubInfo, resultMap) ;
        logger.info("恢复合作方额度---------------end");
    }

    /**
     * 恢复白名单额度
     * @param lmtContRel
     * @param amtMap
     */
    public void recoverWhiteInfo(LmtContRel lmtContRel, Map<String, BigDecimal> amtMap, Map<String, String>resultMap){
        //承兑行白名单额度恢复，敞口金额和恢复金额保持一致
        amtMap.put("recoverSpacAmtCny", amtMap.get("recoverAmtCny")) ;

        String dealBizType = lmtContRel.getDealBizType() ;
        String bizStatus = lmtContRel.getBizStatus() ;
        if(comm4Service.checkRecoverLmtContRelBal(dealBizType, bizStatus)){
            //恢复额度占用关系中余额信息
            comm4Service.recoverLmtcontRelBal(lmtContRel, amtMap, resultMap);
        }

        //白名单额度，查询白名单对应额度信息，更新白名单额度金额
        if (comm4Service.checkRecoverLmtApprBal(dealBizType, bizStatus)) {

            LmtWhiteInfo lmtWhiteInfo = lmtWhiteInfoService.selectLmtWhiteInfoBySubNo(lmtContRel.getLimitSubNo());
            Optional.ofNullable(lmtWhiteInfo).orElseThrow(() -> new YuspException(EclEnum.ECL070009.key, EclEnum.ECL070009.value));

            //白名单已用限额-合同金额
            //修改前
            BigDecimal bfSigUseAmt = BigDecimalUtil.replaceNull(lmtWhiteInfo.getSigUseAmt()) ;
            //修改后
            BigDecimal afSigUseAmt = bfSigUseAmt.subtract(amtMap.get("recoverAmtCny")) ;
            lmtWhiteInfo.setSigUseAmt(afSigUseAmt);
            logger.info("【LmtWhiteInfo】恢复【"+lmtWhiteInfo.getSubAccNo()+"】,已用限额【SigUseAmt】更改为：" + bfSigUseAmt + " --> " + afSigUseAmt);
            lmtWhiteInfoService.updateByPKeyInApprLmtChgDetails(lmtWhiteInfo, resultMap) ;
        }
    }

    /**
     * 结清恢复
     * @param lmtContRel
     * @param lmtContRel
     * @param amtMap
     */
    public void settleContAccRel(LmtContRel lmtContRel, Map<String, BigDecimal> amtMap, ContAccRel contAccRel, Map<String, String> resultMap){
        logger.info("cmislmt0014 结清恢复--------------start");
        //授信类型
        String lmtType = lmtContRel.getLmtType() ;
        //合同类型 1 一般合同 2 最高额合同 3 最高额协议
        String dealBizType = lmtContRel.getDealBizType() ;
        //判断是否非合作方或白名单额度，
        exceRecoverType03(resultMap, contAccRel, amtMap, lmtContRel, lmtType, dealBizType, CmisLmtConstants.STD_RECOVER_TYPE_01);
        logger.info("cmislmt0014 结清恢复--------------end");
    }

    /**
     * 未用退回处理
     * @param lmtContRel
     * @param amtMap
     * @param contAccRel
     * @param resultMap
     */
    public void unUsedReturnOcc(LmtContRel lmtContRel, Map<String, BigDecimal> amtMap, ContAccRel contAccRel, Map<String, String> resultMap){
        logger.info("cmislmt0014 未用退回--------------start");
        //撤销敞口金额
        BigDecimal recoverSpacAmtCny = BigDecimal.ZERO ;
        BigDecimal recoverAmtCny = BigDecimal.ZERO ;
        //修改记录添加
        String status = CmisLmtConstants.STD_ZB_BIZ_STATUS_200 ;
        String bizAttr = lmtContRel.getBizAttr() ;

        if(CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
            status = lmtContRel.getBizStatus() ;
        }else{
            status = contAccRel.getStatus() ;
        }
        recoverSpacAmtCny = amtMap.get("recoverSpacAmtCny") ;
        recoverAmtCny = amtMap.get("recoverAmtCny") ;
        comm4Service.cancalOccExecute(contAccRel, lmtContRel, resultMap, recoverSpacAmtCny, recoverAmtCny, status);
        logger.info("cmislmt0014 未用退回--------------start");
    }


    /**
     * 台账占用撤销
     * @param contaccrel
     * @param apprLmtSubBasicInfo
     * @param cmisLmt0014ReqdealBizListDto
     * @param lmtType
     */
    public void revokeContAccRel(ContAccRel contaccrel, ApprLmtSubBasicInfo apprLmtSubBasicInfo, CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto, String lmtType){
        //恢复敞口金额(人民币)
        BigDecimal recoverSpacAmtCny = cmisLmt0014ReqdealBizListDto.getRecoverSpacAmtCny() ;
        //恢复总额(人民币)
        BigDecimal recoverAmtCny = cmisLmt0014ReqdealBizListDto.getRecoverAmtCny() ;

        //撤销占用，删除台账信息，恢复分项可出账金额，已出账金额，用信余额等
        contaccrel.setOprType(CmisLmtConstants.OPR_TYPE_DELETE);
        contAccRelMapper.updateByPrimaryKey(contaccrel) ;
        //法人额度，获取风险分项信息，恢复分线该信息
        if(CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(lmtType)){
            //已出账金额 = 已出账金额 - 本次恢复金额  && 已出账金额 = 已出账金额 - 本次恢复金额
            comm4Service.recoverAvlAndPvpOutstndAmt(apprLmtSubBasicInfo, recoverAmtCny, recoverSpacAmtCny) ;
            //用信敞口余额
            apprLmtSubBasicInfo.setLoanSpacBalance(apprLmtSubBasicInfo.getLoanSpacBalance().subtract(recoverSpacAmtCny));
            //用信余额
            apprLmtSubBasicInfo.setLoanBalance(apprLmtSubBasicInfo.getLoanBalance().subtract(recoverAmtCny));
            apprLmtSubBasicInfoMapper.updateByPrimaryKey(apprLmtSubBasicInfo) ;
        }
    }

    /**
     * 组装恢复金额字段
     * @param recoverAmtCny
     * @param recoverSpacAmtCny
     */
    public Map<String, BigDecimal> getRecoverAmtMap(BigDecimal recoverAmtCny, BigDecimal recoverSpacAmtCny) {
        Map<String, BigDecimal> amtMap = new HashMap<>();
        //恢复敞口金额(人民币)
        amtMap.put("recoverSpacAmtCny", recoverSpacAmtCny) ;
        //恢复总额(人民币)
        amtMap.put("recoverAmtCny", recoverAmtCny) ;
        return amtMap ;
    }

    /**
     * @作者:lizx
     * @方法名称: recoverContAccRel
     * @方法描述:  额度恢复台账余额信息恢复处理
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/11/19 17:05
     * @param recoverType:
     * @param contAccRel:
     * @param amtMap:
     * @param resultMap:
     * @return: void
     * @算法描述: 无
     */
    public void recoverContAccRel(String recoverType, ContAccRel contAccRel, Map<String, BigDecimal> amtMap, Map<String, String> resultMap){
        logger.info("CmisLmt0014 台账信息数据-----------------start {}", JSON.toJSONString(contAccRel));
        BigDecimal recoverSpacAmtCny = amtMap.get("recoverSpacAmtCny");
        BigDecimal recoverAmtCny = amtMap.get("recoverAmtCny") ;
        if(CmisLmtConstants.STD_RECOVER_TYPE_03.equals(recoverType)){
            rePayRecoverContAccRel(contAccRel, recoverSpacAmtCny, recoverAmtCny, resultMap);
        }else if(CmisLmtConstants.STD_RECOVER_TYPE_01.equals(recoverType)){
            contAccRel.setAccTotalBalanceAmtCny(BigDecimal.ZERO);
            contAccRel.setAccSpacBalanceAmtCny(BigDecimal.ZERO);
            contAccRel.setStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_300);
            contAccRelService.updateByPKeyInApprLmtChgDetails(contAccRel, resultMap) ;
        }else if(CmisLmtConstants.STD_RECOVER_TYPE_06.equals(recoverType)){
            //撤销占用，删除台账信息，恢复分项可出账金额，已出账金额，用信余额等
            logger.info("撤销占用删除台账信息：{}", JSON.toJSONString(contAccRel)) ;
            contAccRelMapper.deleteByPrimaryKey(contAccRel.getPkId()) ;
        }else if(CmisLmtConstants.STD_RECOVER_TYPE_08.equals(recoverType)){
            //未用撤回
            unUsedRetrunContAcc(contAccRel, resultMap, recoverSpacAmtCny, recoverAmtCny);
        }else{
            //追加保证金，恢复台账敞口
            //修改前
            BigDecimal bfAccSpacBalanceAmtCny = contAccRel.getAccSpacBalanceAmtCny() ;
            //修改后
            BigDecimal afAccSpacBalanceAmtCny = bfAccSpacBalanceAmtCny.subtract(recoverSpacAmtCny) ;
            contAccRel.setAccSpacBalanceAmtCny(afAccSpacBalanceAmtCny);
            //保证金额金额更新
            contAccRel.setSecurityAmt(BigDecimalUtil.replaceNull(contAccRel.getSecurityAmt()).add(recoverSpacAmtCny));
            logger.info("【contaccrel】追加保证金恢复【"+ contAccRel.getTranAccNo()+"】,"
                    +",占用敞口余额（折人民币) 【AccSpacBalanceAmtCny】更改为"+bfAccSpacBalanceAmtCny + " --> " + afAccSpacBalanceAmtCny
            );
            contAccRelService.updateByPKeyInApprLmtChgDetails(contAccRel, resultMap) ;
        }
        logger.info("CmisLmt0014 台账信息数据-----------------end {}", JSON.toJSONString(contAccRel));
    }

    /**
     * @作者:lizx
     * @方法名称: unUsedRetrunContAcc
     * @方法描述:  未用撤销，处理台账信息数据
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/11/16 10:58
     * @param contAccRel:
     * @param resultMap:
     * @param recoverSpacAmtCny:
     * @param recoverAmtCny:
     * @return: void
     * @算法描述: 无
    */
    private void unUsedRetrunContAcc(ContAccRel contAccRel, Map<String, String> resultMap, BigDecimal recoverSpacAmtCny, BigDecimal recoverAmtCny) {
        logger.info("未用注销更改台账相关信息");
        BigDecimal accSpacBalanceAmtCny = NumberUtils.nullDefaultZero(contAccRel.getAccSpacBalanceAmtCny());
        BigDecimal accTotalBalanceAmtCny = NumberUtils.nullDefaultZero(contAccRel.getAccTotalBalanceAmtCny()) ;

        BigDecimal afAccSpacBalanceAmtCny = accSpacBalanceAmtCny.subtract(recoverSpacAmtCny);
        BigDecimal afAccTotalBalanceAmtCny = accTotalBalanceAmtCny.subtract(recoverAmtCny) ;

        BigDecimal accTotalAmtCny = NumberUtils.nullDefaultZero(contAccRel.getAccTotalAmtCny()).subtract(recoverAmtCny);
        BigDecimal accSpacAmtCny = NumberUtils.nullDefaultZero(contAccRel.getAccSpacAmtCny()).subtract(recoverSpacAmtCny) ;

        contAccRel.setAccTotalBalanceAmtCny(afAccTotalBalanceAmtCny);
        contAccRel.setAccSpacBalanceAmtCny(afAccSpacBalanceAmtCny);
        contAccRel.setAccSpacAmtCny(accSpacAmtCny);
        contAccRel.setAccTotalAmtCny(accTotalAmtCny);

        if(afAccSpacBalanceAmtCny.compareTo(BigDecimal.ZERO)==0 && afAccTotalBalanceAmtCny.compareTo(BigDecimal.ZERO)==0){
            logger.info("【lmtContRel】恢复【"+ contAccRel.getDealBizNo()+"】,还款结清，更改额度信息{}：", CmisLmtConstants.STD_ZB_BIZ_STATUS_300);
            contAccRel.setStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_400);
        }
        contAccRelService.updateByPKeyInApprLmtChgDetails(contAccRel, resultMap) ;
    }

    public void rePayRecoverContAccRel(ContAccRel contAccRel, BigDecimal recoverSpacAmtCny, BigDecimal recoverAmtCny, Map<String, String> resultMap){
        //还款操作，台账信息中余额减去本次还款金额
        //修改前
        BigDecimal bfAccTotalBalanceAmtCny = BigDecimalUtil.replaceNull(contAccRel.getAccTotalBalanceAmtCny()) ;
        //修改后
        BigDecimal afAccTotalBalanceAmtCny = bfAccTotalBalanceAmtCny.subtract(recoverAmtCny) ;
        contAccRel.setAccTotalBalanceAmtCny(afAccTotalBalanceAmtCny);

        //修改前
        BigDecimal bfAccSpacBalanceAmtCny = BigDecimalUtil.replaceNull(contAccRel.getAccSpacBalanceAmtCny()) ;
        //修改后
        BigDecimal afAccSpacBalanceAmtCny = bfAccSpacBalanceAmtCny.subtract(recoverSpacAmtCny) ;
        contAccRel.setAccSpacBalanceAmtCny(afAccSpacBalanceAmtCny);

        logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0014.key+":【ContAccRel】恢复【"+contAccRel.getDealBizNo()+"】" +
                ",占用总余额（折人民币）更改为：" + bfAccTotalBalanceAmtCny + " --> "+ afAccTotalBalanceAmtCny
                +",占用敞口余额（折人民币)更改为" + bfAccSpacBalanceAmtCny + " --> "+ afAccSpacBalanceAmtCny
        );

        if(afAccSpacBalanceAmtCny.compareTo(BigDecimal.ZERO)==0 && afAccTotalBalanceAmtCny.compareTo(BigDecimal.ZERO)==0){
            logger.info("【lmtContRel】恢复【"+ contAccRel.getDealBizNo()+"】,还款结清，更改额度信息{}：", CmisLmtConstants.STD_ZB_BIZ_STATUS_300);
            contAccRel.setStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_300);
        }
        contAccRelService.updateByPKeyInApprLmtChgDetails(contAccRel, resultMap) ;
    }
}
