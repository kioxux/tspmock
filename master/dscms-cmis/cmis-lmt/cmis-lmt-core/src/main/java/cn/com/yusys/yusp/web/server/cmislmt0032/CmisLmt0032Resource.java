package cn.com.yusys.yusp.web.server.cmislmt0032;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0032.req.CmisLmt0032ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0032.resp.CmisLmt0032RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0032.CmisLmt0032Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询客户授信余额（不包含穿透化额度）
 *
 * @author dumd 20210617
 * @version 1.0
 */
@Api(tags = "cmislmt0032:查询客户授信余额（不包含穿透化额度）")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0032Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0032Resource.class);

    @Autowired
    private CmisLmt0032Service cmisLmt0032Service;

    /**
     * 交易码：CmisLmt0032
     * 交易描述：查询客户授信余额（不包含穿透化额度）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("查询客户授信余额（不包含穿透化额度）")
    @PostMapping("/cmislmt0032")
    protected @ResponseBody
    ResultDto<CmisLmt0032RespDto> CmisLmt0032(@Validated @RequestBody CmisLmt0032ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0032.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0032.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0032RespDto> CmisLmt0032RespDtoResultDto = new ResultDto<>();
        CmisLmt0032RespDto CmisLmt0032RespDto = new CmisLmt0032RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0032RespDto = cmisLmt0032Service.execute(reqDto);
            CmisLmt0032RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0032RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0032.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0032.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0032.value, e.getMessage());
            // 封装CmisLmt0032RespDtoResultDto中异常返回码和返回信息
            CmisLmt0032RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0032RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0032RespDto到CmisLmt0032RespDtoResultDto中
        CmisLmt0032RespDtoResultDto.setData(CmisLmt0032RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0032.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0032.value, JSON.toJSONString(CmisLmt0032RespDtoResultDto));
        return CmisLmt0032RespDtoResultDto;
    }
}