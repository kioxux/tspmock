package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

@ExcelCsv(namePrefix = "业务条线额度管控历史列表", fileType = ExcelCsv.ExportFileType.XLS)
public class ManaBusiLmtHisVo {

    /*
   条线部门
    */
    @ExcelField(title = "条线部门", viewLength = 20 ,dictCode = "STD_ZB_SUIT_BIZ_LINE")
    private String busiType;

    /*
   分支机构
    */
    @ExcelField(title = "分支机构", viewLength = 20 ,dictCode = "STD_ORG_AREA_TYPE")
    private String orgAreaType;

    /*
    上月末对公贷款余额
     */
    @ExcelField(title = "上月末贷款余额（万元）", viewLength = 20)
    private java.math.BigDecimal lastMonthLoanBalance;

    /*
    当月可净新增对公贷款投放金额
     */
    @ExcelField(title = "当月可净新增对款投放金额（万元）", viewLength = 20)
    private java.math.BigDecimal currMonthAllowAddAmt;

//    /*
//    当月剩余可投放对公贷款余额
//     */
//    @ExcelField(title = "当月剩余可投放贷款余额", viewLength = 20)
//    private java.math.BigDecimal currMonthAllowLoanBalance;

    /*
    月末对公贷款余额测算
     */
    @ExcelField(title = "月末贷款余额测算（万元）", viewLength = 20 ,format = "#0.00")
    private java.math.BigDecimal curMonthLoanBalancePlan;

    /*
    上一日对公贷款余额
     */
    @ExcelField(title = "上一日贷款余额（万元）", viewLength = 20)
    private java.math.BigDecimal lastDayLoanBalance;

    /*
    最近更新日期
     */
    @ExcelField(title = "更新日期", viewLength = 20)
    private String updDate;

    public String getBusiType() {
        return busiType;
    }

    public void setBusiType(String busiType) {
        this.busiType = busiType;
    }

    public String getOrgAreaType() {
        return orgAreaType;
    }

    public void setOrgAreaType(String orgAreaType) {
        this.orgAreaType = orgAreaType;
    }

//    public BigDecimal getCurrMonthAllowLoanBalance() {
//        return currMonthAllowLoanBalance;
//    }
//
//    public void setCurrMonthAllowLoanBalance(BigDecimal currMonthAllowLoanBalance) {
//        this.currMonthAllowLoanBalance = currMonthAllowLoanBalance;
//    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public BigDecimal getLastMonthLoanBalance() {
        return lastMonthLoanBalance;
    }

    public void setLastMonthLoanBalance(BigDecimal lastMonthLoanBalance) {
        this.lastMonthLoanBalance = lastMonthLoanBalance;
    }

    public BigDecimal getCurrMonthAllowAddAmt() {
        return currMonthAllowAddAmt;
    }

    public void setCurrMonthAllowAddAmt(BigDecimal currMonthAllowAddAmt) {
        this.currMonthAllowAddAmt = currMonthAllowAddAmt;
    }

    public BigDecimal getCurMonthLoanBalancePlan() {
        return curMonthLoanBalancePlan;
    }

    public void setCurMonthLoanBalancePlan(BigDecimal curMonthLoanBalancePlan) {
        this.curMonthLoanBalancePlan = curMonthLoanBalancePlan;
    }

    public BigDecimal getLastDayLoanBalance() {
        return lastDayLoanBalance;
    }

    public void setLastDayLoanBalance(BigDecimal lastDayLoanBalance) {
        this.lastDayLoanBalance = lastDayLoanBalance;
    }
}
