/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.TrnFlowInfo;
import cn.com.yusys.yusp.service.TrnFlowInfoService;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: TrnFlowInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-05-04 16:27:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/trnflowinfo")
public class TrnFlowInfoResource {
    @Autowired
    private TrnFlowInfoService trnFlowInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<TrnFlowInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<TrnFlowInfo> list = trnFlowInfoService.selectAll(queryModel);
        return new ResultDto<List<TrnFlowInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<TrnFlowInfo>> index(QueryModel queryModel) {
        List<TrnFlowInfo> list = trnFlowInfoService.selectByModel(queryModel);
        return new ResultDto<List<TrnFlowInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<TrnFlowInfo> show(@PathVariable("pkId") String pkId) {
        TrnFlowInfo trnFlowInfo = trnFlowInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<TrnFlowInfo>(trnFlowInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<TrnFlowInfo> create(@RequestBody TrnFlowInfo trnFlowInfo) {
        trnFlowInfoService.insert(trnFlowInfo);
        return new ResultDto<TrnFlowInfo>(trnFlowInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody TrnFlowInfo trnFlowInfo) {
        int result = trnFlowInfoService.update(trnFlowInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = trnFlowInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = trnFlowInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
