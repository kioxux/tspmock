package cn.com.yusys.yusp.web.server.cmislmt0035;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0035.req.CmisLmt0035ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0035.resp.CmisLmt0035RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.cmislmt0035.CmisLmt0035Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 用信到期日变更通知
 * add by lizx 20210707
 */
@Api(tags = "cmislmt0035:用信到期日变更通知限")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0035Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0035Resource.class);

    @Autowired
    private CmisLmt0035Service cmisLmt0035Service;

    /**
     * 交易码：CmisLmt0035
     * 交易描述：用信到期日变更通知
     * @param reqDto
     */

    @ApiOperation("用信到期日变更通知")
    @PostMapping("/cmislmt0035")
    protected @ResponseBody
    ResultDto<CmisLmt0035RespDto> CmisLmt0035(@Validated @RequestBody CmisLmt0035ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0035.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0035.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0035RespDto> CmisLmt0035RespDtoResultDto = new ResultDto<>();
        CmisLmt0035RespDto CmisLmt0035RespDto = new CmisLmt0035RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0035RespDto = cmisLmt0035Service.execute(reqDto);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0035.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0035.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0035.value, e.getMessage());
            // 封装CmisLmt0035RespDtoResultDto中异常返回码和返回信息
            CmisLmt0035RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0035RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0035RespDto到CmisLmt0035RespDtoResultDto中
        CmisLmt0035RespDtoResultDto.setData(CmisLmt0035RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0035.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0035.value, JSON.toJSONString(CmisLmt0035RespDtoResultDto));
        return CmisLmt0035RespDtoResultDto;
    }
}