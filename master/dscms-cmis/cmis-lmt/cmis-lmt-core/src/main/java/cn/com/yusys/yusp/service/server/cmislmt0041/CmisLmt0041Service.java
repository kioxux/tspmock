package cn.com.yusys.yusp.service.server.cmislmt0041;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.domain.ApprStrMtableInfo;
import cn.com.yusys.yusp.dto.server.cmislmt0041.req.CmisLmt0041ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0041.resp.CmisLmt0041RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.ApprStrMtableInfoService;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0040Service
 * @类描述: #对内服务类
 * @功能描述: 低风险额度反向生成
 * @修改备注: 2021/7/12     zhangjw   新增接口
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0041Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0041Service.class);

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;

    @Autowired
    private ApprStrMtableInfoService apprStrMtableInfoService ;

    @Autowired
    private Comm4Service comm4Service ;

    @Transactional
    public CmisLmt0041RespDto execute(CmisLmt0041ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0041.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0041.value);
        CmisLmt0041RespDto resqDto = new CmisLmt0041RespDto() ;

        try {
            logger.info("低风险额度反向生成",DscmsLmtEnum.TRADE_CODE_CMISLMT0041.key,reqDto.getSerno());

            //判断客户是否存在有效的综合授信
            List<ApprStrMtableInfo>  list =  apprStrMtableInfoService.queryApprStrZHLmt(reqDto.getCusId());
            if(list==null || list.size()<=0){
                throw new YuspException(EclEnum.ECL070111.key, EclEnum.ECL070111.value);
            }

            String cusType = list.get(0) != null ? list.get(0).getCusType() : "";

            ApprLmtSubBasicInfo apprLmtSubBasicInfo = new ApprLmtSubBasicInfo();
            //主键
            apprLmtSubBasicInfo.setPkId(comm4Service.generatePkId());
            //外键
            apprLmtSubBasicInfo.setFkPkid(reqDto.getApprSerno());
            //分项批复编号
            apprLmtSubBasicInfo.setApprSubSerno(reqDto.getParent());
            //客户号
            apprLmtSubBasicInfo.setCusId(reqDto.getCusId());
            //客户名称
            apprLmtSubBasicInfo.setCusName(reqDto.getCusName());
            //客户类型
            apprLmtSubBasicInfo.setCusType(cusType);
            //授信品种编号
            apprLmtSubBasicInfo.setLimitSubNo(null);
            //授信品种名称
            apprLmtSubBasicInfo.setLimitSubName("低风险额度");
            //父节点
            apprLmtSubBasicInfo.setParentId(null);
            //父节点
            apprLmtSubBasicInfo.setIsLriskLmt(CmisLmtConstants.STD_ZB_YES_NO_Y);
            //授信分项类型
            apprLmtSubBasicInfo.setLmtSubType(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_01);
            //是否循环
            apprLmtSubBasicInfo.setIsRevolv(CmisLmtConstants.STD_ZB_YES_NO_Y);
            //额度类型
            apprLmtSubBasicInfo.setLimitType(CmisLmtConstants.STD_ZB_LIMIT_TYPE_01);
            //适用担保方式
            apprLmtSubBasicInfo.setSuitGuarWay(reqDto.getSuitGuarWay());
            //额度有效期
            apprLmtSubBasicInfo.setLmtDate(reqDto.getEndDate());
            //额度宽限期（月)
            apprLmtSubBasicInfo.setLmtGraper(0);
            //授信总额
            apprLmtSubBasicInfo.setAvlAmt(reqDto.getLmtAmt());
            //已用总额
            apprLmtSubBasicInfo.setOutstndAmt(BigDecimal.ZERO);
            //敞口金额
            apprLmtSubBasicInfo.setSpacAmt(BigDecimal.ZERO);
            //敞口已用额度
            apprLmtSubBasicInfo.setSpacOutstndAmt(BigDecimal.ZERO);
            //用信余额
            apprLmtSubBasicInfo.setLoanBalance(BigDecimal.ZERO);
            //状态
            apprLmtSubBasicInfo.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
            //币种
            apprLmtSubBasicInfo.setCurType("CNY");
            //保证金预留比例
            apprLmtSubBasicInfo.setBailPreRate(null);
            //年利率
            apprLmtSubBasicInfo.setRateYear(null);
            //到日期
            apprLmtSubBasicInfo.setLmtDate(reqDto.getEndDate());
            //台账状态
            apprLmtSubBasicInfo.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
            //授信总额累加
            apprLmtSubBasicInfo.setLmtAmtAdd(BigDecimal.ZERO);
            //已出帐金额
            apprLmtSubBasicInfo.setPvpOutstndAmt(BigDecimal.ZERO);
            //可出账金额
            apprLmtSubBasicInfo.setAvlOutstndAmt(reqDto.getLmtAmt());
            //是否预授信标识
            apprLmtSubBasicInfo.setIsPreCrd(CmisLmtConstants.STD_ZB_YES_NO_N);
            //是否涉及货币基金
            apprLmtSubBasicInfo.setIsIvlMf(CmisLmtConstants.STD_ZB_YES_NO_N);
            //起始日期
            apprLmtSubBasicInfo.setStartDate(reqDto.getStartDate());
            //期限
            apprLmtSubBasicInfo.setTerm(reqDto.getTerm());
            //用信敞口余额
            apprLmtSubBasicInfo.setLoanSpacBalance(BigDecimal.ZERO);
            //操作类型
            apprLmtSubBasicInfo.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
            //登记人
            apprLmtSubBasicInfo.setInputId(reqDto.getInputId());
            //登记机构
            apprLmtSubBasicInfo.setInputBrId(reqDto.getInputBrId());
            //登记日期
            apprLmtSubBasicInfo.setInputDate(reqDto.getInputDate());
            //更新人
            apprLmtSubBasicInfo.setUpdId(reqDto.getInputId());
            //更新机构
            apprLmtSubBasicInfo.setUpdBrId(reqDto.getInputBrId());
            //更新日期
            apprLmtSubBasicInfo.setUpdDate(reqDto.getInputDate());
            //创建时间
            apprLmtSubBasicInfo.setCreateTime(DateUtils.getCurrTimestamp());
            //修改时间
            apprLmtSubBasicInfo.setUpdateTime(DateUtils.getCurrTimestamp());
            //宽限期
            apprLmtSubBasicInfo.setLmtGraper(reqDto.getLmtGraper());
            apprLmtSubBasicInfoService.insert(apprLmtSubBasicInfo);

            /**
             * 插入二级分项
             */
            ApprLmtSubBasicInfo apprLmtSubBasicInfoPrd = new ApprLmtSubBasicInfo();
            //主键
            apprLmtSubBasicInfoPrd.setPkId(comm4Service.generatePkId());
            //外键
            apprLmtSubBasicInfoPrd.setFkPkid(reqDto.getApprSerno());
            //分项批复编号
            apprLmtSubBasicInfoPrd.setApprSubSerno(reqDto.getApprSubSerno());
            //客户号
            apprLmtSubBasicInfoPrd.setCusId(reqDto.getCusId());
            //客户名称
            apprLmtSubBasicInfoPrd.setCusName(reqDto.getCusName());
            //客户类型
            apprLmtSubBasicInfoPrd.setCusType(cusType);
            //授信品种编号
            apprLmtSubBasicInfoPrd.setLimitSubNo(reqDto.getLimitSubNo());
            //授信品种名称
            apprLmtSubBasicInfoPrd.setLimitSubName(reqDto.getLimitSubName());
            //父节点
            apprLmtSubBasicInfoPrd.setParentId(reqDto.getParent());
            //是否低风险授信
            apprLmtSubBasicInfoPrd.setIsLriskLmt(CmisLmtConstants.STD_ZB_YES_NO_Y);
            //授信分项类型
            apprLmtSubBasicInfoPrd.setLmtSubType(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_02);
            //是否循环
            apprLmtSubBasicInfoPrd.setIsRevolv(CmisLmtConstants.STD_ZB_YES_NO_Y);
            //额度类型
            apprLmtSubBasicInfoPrd.setLimitType(CmisLmtConstants.STD_ZB_LIMIT_TYPE_01);
            //适用担保方式
            apprLmtSubBasicInfoPrd.setSuitGuarWay(reqDto.getSuitGuarWay());
            //额度有效期
            apprLmtSubBasicInfoPrd.setLmtDate(reqDto.getEndDate());
            //额度宽限期（月)
            apprLmtSubBasicInfoPrd.setLmtGraper(0);
            //授信总额
            apprLmtSubBasicInfoPrd.setAvlAmt(reqDto.getLmtAmt());
            //已用总额
            apprLmtSubBasicInfoPrd.setOutstndAmt(BigDecimal.ZERO);
            //敞口金额
            apprLmtSubBasicInfoPrd.setSpacAmt(BigDecimal.ZERO);
            //敞口已用额度
            apprLmtSubBasicInfoPrd.setSpacOutstndAmt(BigDecimal.ZERO);
            //用信余额
            apprLmtSubBasicInfoPrd.setLoanBalance(BigDecimal.ZERO);
            //状态
            apprLmtSubBasicInfoPrd.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
            //币种
            apprLmtSubBasicInfoPrd.setCurType("CNY");
            //保证金预留比例
            apprLmtSubBasicInfoPrd.setBailPreRate(reqDto.getBailPreRate());
            //年利率
            apprLmtSubBasicInfoPrd.setRateYear(reqDto.getRateYear());
            //到日期
            apprLmtSubBasicInfoPrd.setLmtDate(reqDto.getEndDate());
            //台账状态
            apprLmtSubBasicInfoPrd.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
            //授信总额累加
            apprLmtSubBasicInfoPrd.setLmtAmtAdd(BigDecimal.ZERO);
            //已出帐金额
            apprLmtSubBasicInfoPrd.setPvpOutstndAmt(BigDecimal.ZERO);
            //可出账金额
            apprLmtSubBasicInfoPrd.setAvlOutstndAmt(reqDto.getLmtAmt());
            //是否预授信标识
            apprLmtSubBasicInfoPrd.setIsPreCrd(CmisLmtConstants.STD_ZB_YES_NO_N);
            //是否涉及货币基金
            apprLmtSubBasicInfoPrd.setIsIvlMf(CmisLmtConstants.STD_ZB_YES_NO_N);
            //起始日期
            apprLmtSubBasicInfoPrd.setStartDate(reqDto.getStartDate());
            //期限
            apprLmtSubBasicInfoPrd.setTerm(reqDto.getTerm());
            //用信敞口余额
            apprLmtSubBasicInfoPrd.setLoanSpacBalance(BigDecimal.ZERO);
            //操作类型
            apprLmtSubBasicInfoPrd.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
            //登记人
            apprLmtSubBasicInfoPrd.setInputId(reqDto.getInputId());
            //登记机构
            apprLmtSubBasicInfoPrd.setInputBrId(reqDto.getInputBrId());
            //登记日期
            apprLmtSubBasicInfoPrd.setInputDate(reqDto.getInputDate());
            //更新人
            apprLmtSubBasicInfoPrd.setUpdId(reqDto.getInputId());
            //更新机构
            apprLmtSubBasicInfoPrd.setUpdBrId(reqDto.getInputBrId());
            //更新日期
            apprLmtSubBasicInfoPrd.setUpdDate(reqDto.getInputDate());
            //创建时间
            apprLmtSubBasicInfoPrd.setCreateTime(DateUtils.getCurrTimestamp());
            //修改时间
            apprLmtSubBasicInfoPrd.setUpdateTime(DateUtils.getCurrTimestamp());
            //宽限期
            apprLmtSubBasicInfoPrd.setLmtGraper(reqDto.getLmtGraper());
            apprLmtSubBasicInfoService.insert(apprLmtSubBasicInfoPrd);

            //将列表放入返回中
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("反向生成低风险额度报错：",e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0041.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0041.value);
        return resqDto;
    }

}