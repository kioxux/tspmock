package cn.com.yusys.yusp.dto;

import java.math.BigDecimal;
import java.util.List;

public class QuotaViewDto {
    private String name;//编号名称
    private BigDecimal value;//金额
    private Integer level;//级别
    private Integer type;//0.树,1.列表
    private Integer state;//0.查询
    private ApprLmtSubBasicInfoDto data;
    private List<QuotaViewDto> children;

    public QuotaViewDto(String name, BigDecimal value, Integer level, Integer type,Integer state) {
        this.name = name;
        this.value = value;
        this.level = level;
        this.type = type;
        this.state=state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public List<QuotaViewDto> getChildren() {
        return children;
    }

    public void setChildren(List<QuotaViewDto> children) {
        this.children = children;
    }

    public ApprLmtSubBasicInfoDto getData() {
        return data;
    }

    public void setData(ApprLmtSubBasicInfoDto data) {
        this.data = data;
    }
}
