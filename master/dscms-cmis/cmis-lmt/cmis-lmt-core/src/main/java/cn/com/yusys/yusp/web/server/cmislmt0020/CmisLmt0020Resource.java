package cn.com.yusys.yusp.web.server.cmislmt0020;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0020.req.CmisLmt0020ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0020.resp.CmisLmt0020RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0020.CmisLmt0020Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:承兑行白名单额度查询
 *
 * @author dmd 20210508
 * @version 1.0
 */
@Api(tags = "cmislmt0020:承兑行白名单额度查询")
@RestController
@RequestMapping("/api/cmislmt0020resource")
public class CmisLmt0020Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0020Resource.class);

    @Autowired
    private CmisLmt0020Service cmisLmt0020Service;
    /**
     * 交易码：cmislmt0020
     * 交易描述：承兑行白名单额度查询
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("承兑行白名单额度查询")
    @PostMapping("/cmisLmt0020")
    protected @ResponseBody
    ResultDto<CmisLmt0020RespDto> cmisLmt0020(@Validated @RequestBody CmisLmt0020ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0020.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0020.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0020RespDto> cmisLmt0020RespDtoResultDto = new ResultDto<>();
        CmisLmt0020RespDto cmisLmt0020RespDto = new CmisLmt0020RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0020RespDto = cmisLmt0020Service.execute(reqDto);
            cmisLmt0020RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0020RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0020.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0020.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0020.value, e.getMessage());
            // 封装xddb0001DataResultDto中异常返回码和返回信息
            cmisLmt0020RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0020RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0020RespDto到cmisLmt0020RespDtoResultDto中
        cmisLmt0020RespDtoResultDto.setData(cmisLmt0020RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0020.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0020.value, JSON.toJSONString(cmisLmt0020RespDtoResultDto));
        return cmisLmt0020RespDtoResultDto;
    }
}
