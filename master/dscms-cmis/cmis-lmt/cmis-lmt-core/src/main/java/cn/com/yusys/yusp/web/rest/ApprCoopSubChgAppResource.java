/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ApprCoopChgApp;
import cn.com.yusys.yusp.domain.ApprCoopSubChgApp;
import cn.com.yusys.yusp.service.ApprCoopSubChgAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprCoopSubChgAppResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-20 20:38:47
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/apprcoopsubchgapp")
public class ApprCoopSubChgAppResource {
    @Autowired
    private ApprCoopSubChgAppService apprCoopSubChgAppService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<ApprCoopSubChgApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<ApprCoopSubChgApp> list = apprCoopSubChgAppService.selectAll(queryModel);
        return new ResultDto<List<ApprCoopSubChgApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<ApprCoopSubChgApp>> index(QueryModel queryModel) {
        List<ApprCoopSubChgApp> list = apprCoopSubChgAppService.selectByModel(queryModel);
        return new ResultDto<List<ApprCoopSubChgApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<ApprCoopSubChgApp>> selectByModel(@RequestBody QueryModel queryModel) {
        List<ApprCoopSubChgApp> list = apprCoopSubChgAppService.selectByModel(queryModel);
        return new ResultDto<List<ApprCoopSubChgApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<ApprCoopSubChgApp> show(@PathVariable("pkId") String pkId) {
        ApprCoopSubChgApp apprCoopSubChgApp = apprCoopSubChgAppService.selectByPrimaryKey(pkId);
        return new ResultDto<ApprCoopSubChgApp>(apprCoopSubChgApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<ApprCoopSubChgApp> create(@RequestBody ApprCoopSubChgApp apprCoopSubChgApp) {
        apprCoopSubChgAppService.insert(apprCoopSubChgApp);
        return new ResultDto<ApprCoopSubChgApp>(apprCoopSubChgApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ApprCoopSubChgApp apprCoopSubChgApp){
        int result = apprCoopSubChgAppService.update(apprCoopSubChgApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = apprCoopSubChgAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = apprCoopSubChgAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:insertInfoToApp
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertInfoToApp")
    protected void insertInfoToApp(@RequestBody ApprCoopChgApp apprCoopChgApp) {
        apprCoopSubChgAppService.insertInfoToApp(apprCoopChgApp);
    }

}
