package cn.com.yusys.yusp.web.server.cmislmt0005;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0005.req.CmisLmt0005ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0005.resp.CmisLmt0005RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0005.CmisLmt0005Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:资金业务额度同步
 *
 * @author dmd 202105010
 * @version 1.0
 */
@Api(tags = "cmislmt0005:资金业务额度同步")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0005Resource.class);

    @Autowired
    private  CmisLmt0005Service cmisLmt0005Service;
    /**
     * 交易码：cmislmt0005
     * 交易描述：资金业务额度同步
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("资金业务额度同步")
    @PostMapping("/cmislmt0005")
    protected @ResponseBody
    ResultDto<CmisLmt0005RespDto> CmisLmt0005(@Validated @RequestBody CmisLmt0005ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0005.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0005.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0005RespDto> cmisLmt0005RespDtoResultDto = new ResultDto<>();
        CmisLmt0005RespDto cmisLmt0005RespDto = new CmisLmt0005RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0005RespDto = cmisLmt0005Service.execute(reqDto);
            cmisLmt0005RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0005RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (YuspException yuspe) {
            logger.error("资金业务额度同步接口报错：",yuspe);
            cmisLmt0005RespDto.setErrorCode(yuspe.getCode());
            cmisLmt0005RespDto.setErrorMsg(yuspe.getMsg());
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0005.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0005.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0005.value, e.getMessage());
            // 封装CmisLmt0005RespDtoResultDto中异常返回码和返回信息
            cmisLmt0005RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0005RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }

        // 封装CmisLmt0005RespDto到CmisLmt0005RespDtoResultDto中
        cmisLmt0005RespDtoResultDto.setData(cmisLmt0005RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0005.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0005.value, JSON.toJSONString(cmisLmt0005RespDtoResultDto));
        return cmisLmt0005RespDtoResultDto;
    }
}
