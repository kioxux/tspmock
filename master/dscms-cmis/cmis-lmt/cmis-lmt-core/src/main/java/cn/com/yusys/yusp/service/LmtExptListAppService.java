/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.LmtExptListApp;
import cn.com.yusys.yusp.domain.LmtExptListInfo;
import cn.com.yusys.yusp.repository.mapper.LmtExptListAppMapper;
import cn.com.yusys.yusp.repository.mapper.LmtExptListInfoMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtExptListAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: lenovo
 * @创建时间: 2021-04-16 15:41:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtExptListAppService {
    private static final Logger log = LoggerFactory.getLogger(LmtExptListAppService.class);
    @Autowired
    private LmtExptListAppMapper lmtExptListAppMapper;

    @Autowired
    private LmtExptListInfoMapper lmtExptListInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtExptListApp selectByPrimaryKey(String pkId) {
        return lmtExptListAppMapper.selectByPrimaryKey(pkId);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtExptListApp> selectAll(QueryModel model) {
        List<LmtExptListApp> records = (List<LmtExptListApp>) lmtExptListAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtExptListApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtExptListApp> list = lmtExptListAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtExptListApp record) {
        return lmtExptListAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtExptListApp lmtExptListAppAdd) {
        //创建实体类
        LmtExptListApp lmtExptListApp = new LmtExptListApp();

        //获取当前时间
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //新增导入数据
        lmtExptListApp.setPkId(lmtExptListAppAdd.getPkId());
        lmtExptListApp.setSerno(lmtExptListAppAdd.getSerno());
        lmtExptListApp.setInstuCde(lmtExptListAppAdd.getInstuCde());
        lmtExptListApp.setBussNo(lmtExptListAppAdd.getBussNo());
        lmtExptListApp.setSysId(lmtExptListAppAdd.getSysId());
        lmtExptListApp.setCusId(lmtExptListAppAdd.getCusId());
        lmtExptListApp.setCusName(lmtExptListAppAdd.getCusName());
        lmtExptListApp.setExptType(lmtExptListAppAdd.getExptType());
        lmtExptListApp.setExptResn(lmtExptListAppAdd.getExptResn());
        lmtExptListApp.setInputId(lmtExptListAppAdd.getInputId());
        lmtExptListApp.setInputBrId(lmtExptListAppAdd.getInputBrId());
        lmtExptListApp.setInputDate(sdf.format(date).toString());
        lmtExptListApp.setApproveStatus(lmtExptListAppAdd.getApproveStatus());
        lmtExptListApp.setApproveStatus(CmisLmtConstants.APP_STATUS_111);
//        BeanUtils.copyProperties(lmtExptListAppAdd,lmtExptListApp);
        lmtExptListApp.setOprType(CmisLmtConstants.LMT_APP_TYPE_01);
        lmtExptListApp.setInputDate(sdf.format(date).toString());

        return lmtExptListAppMapper.insertSelective(lmtExptListApp);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtExptListApp record) {
        return lmtExptListAppMapper.updateBySerno(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtExptListAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtExptListAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: logicaldelete
     * @方法描述: 根据流水号逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int logicaldelete(LmtExptListApp record){

        LmtExptListApp lmtExptListApp = new LmtExptListApp();

        //获取当前时间
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //变更为删除状态
        lmtExptListApp.setSerno(record.getSerno());
        lmtExptListApp.setOprType(CmisLmtConstants.LMT_APP_TYPE_02);
        lmtExptListApp.setUpdId(record.getUpdId());
        lmtExptListApp.setUpdBrId(record.getUpdBrId());
        lmtExptListApp.setUpdDate(sdf.format(date).toString());
        lmtExptListApp.setUpdateTime(date);

        return lmtExptListAppMapper.logicaldelete(lmtExptListApp);
    };

    /**
     * @方法名称: updateBySerno
     * @方法描述: 根据流水号修改，点击保存
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateBySerno(LmtExptListApp lmtExptListAppUpdate){

        //获取当前时间
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //设置修改信息
//        lmtExptListAppUpdate.setUpdId();
//        lmtExptListAppUpdate.setUpdBrId();
        lmtExptListAppUpdate.setUpdDate(sdf.format(date).toString());

        return lmtExptListAppMapper.updateBySerno(lmtExptListAppUpdate);
    };

    /**
     * @方法名称: updateAppToInfo
     * @方法描述: 根据流水号修改，点击提交
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateAppToInfo(LmtExptListApp lmtExptListInfoUpdate){

        //判断状态是否提交
        if(lmtExptListInfoUpdate.getApproveStatus() == "111") {
            return 1;
        }else{
            //创建实体类
            LmtExptListInfo lmtExptListInfo = new LmtExptListInfo();

            //获取当前时间
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            //判断info表是否已经存在信息
            //调用info表查询接口
            lmtExptListInfo = lmtExptListInfoMapper.selectBySerno(lmtExptListInfoUpdate.getSerno());

            //进入判断（info没有信息调用新增借口，存在信息则调用修改接口）
            if (lmtExptListInfo.getSerno() != null) {

                //设置修改信息
                lmtExptListInfoUpdate.setApproveStatus("997");
                lmtExptListInfoUpdate.setUpdDate(sdf.format(date).toString());
                BeanUtils.copyProperties(lmtExptListInfoUpdate, lmtExptListInfo);

                //调用info表
                lmtExptListInfoMapper.updateAppToInfo(lmtExptListInfo);

            } else {
                insertSelective(lmtExptListInfoUpdate);
            }
        }

        return lmtExptListAppMapper.updateBySerno(lmtExptListInfoUpdate);
    };

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据申请流水号查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtExptListApp selectBySerno(String serno) {
        return lmtExptListAppMapper.selectBySerno(serno);
    }
    /**
     * @方法名称: updateApproveStatus
     * @方法描述: 流程申请状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateApproveStatus(LmtExptListApp lmtExptListApp) {
        return lmtExptListAppMapper.updateApproveStatus(lmtExptListApp);
    }

    /**
     * 单一客户额度调整申请  审批通过后处理
     * 1、更新额度主信息不变 最近更新人  最近更新机构  最近更新时间  修改时间
     * 2、更新额度分项信息已用金额等金额 最近更新人  最近更新机构  最近更新时间  修改时间
     * 3、更新分项占用关系数据
     * 4、更新台账占用关系数据
     * @param serno
     * credit by zhangjw 2021-04-26
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessDataAfterEnd(String serno) {
        try {
            log.info("审批通过移入额度管控信息表：获取额度管控白名单申请" + serno + "信息");
            LmtExptListApp lmtExptListApp = selectBySerno(serno);

            //将申请表移入正式表
            LmtExptListInfo lmtExptListInfo = new LmtExptListInfo();
            BeanUtils.copyProperties(lmtExptListApp, lmtExptListInfo);

            lmtExptListInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME),
                    DateFormatEnum.DATETIME.getValue()));
            lmtExptListInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME),
                    DateFormatEnum.DATETIME.getValue()));

            //设置登记日期、最近更新日期
            lmtExptListInfo.setInputDate(DateUtils.getCurrDateStr());
            lmtExptListInfo.setUpdDate(DateUtils.getCurrDateStr());

            //设置最近更新人、最近更新机构
            lmtExptListInfo.setUpdId(lmtExptListApp.getInputId());
            lmtExptListInfo.setUpdBrId(lmtExptListApp.getInputBrId());
            //保存数据
            lmtExptListInfoMapper.insert(lmtExptListInfo);

            log.info("审批通过更新额度管控白名单申请" + serno + "审批状态为【997】-审批通过");
            lmtExptListApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
            lmtExptListAppMapper.updateApproveStatus(lmtExptListApp);
        } catch(YuspException e){
            throw e;
        } catch (Exception e) {
            log.error("流程审批通过业务处理发生异常！", e);
        }
    }
}
