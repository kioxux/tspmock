/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: TrnFlowInfo
 * @类描述: trn_flow_info数据实体类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-05-04 16:27:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "trn_flow_info")
public class TrnFlowInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 交易流水号 **/
	@Column(name = "TRAN_SERNO", unique = false, nullable = true, length = 40)
	private String tranSerno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 交易业务编号 **/
	@Column(name = "TRAN_BIZ_NO", unique = false, nullable = true, length = 40)
	private String tranBizNo;
	
	/** 批复分项流水号 **/
	@Column(name = "APPR_SUB_SERNO", unique = false, nullable = true, length = 40)
	private String apprSubSerno;
	
	/** 交易类型STD_ZB_TRAN_TYPE **/
	@Column(name = "TRAN_TYPE", unique = false, nullable = true, length = 5)
	private String tranType;
	
	/** 修改数据表 **/
	@Column(name = "CHG_TABLE_NAME", unique = false, nullable = true, length = 40)
	private String chgTableName;
	
	/** 更改字段名 **/
	@Column(name = "CHG_FIELD_NAME", unique = false, nullable = true, length = 40)
	private String chgFieldName;
	
	/** 更改字段值 **/
	@Column(name = "CHG_FIELD_VALUE", unique = false, nullable = true, length = 40)
	private String chgFieldValue;
	
	/** 操作类型STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 2)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param tranSerno
	 */
	public void setTranSerno(String tranSerno) {
		this.tranSerno = tranSerno;
	}
	
    /**
     * @return tranSerno
     */
	public String getTranSerno() {
		return this.tranSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param tranBizNo
	 */
	public void setTranBizNo(String tranBizNo) {
		this.tranBizNo = tranBizNo;
	}
	
    /**
     * @return tranBizNo
     */
	public String getTranBizNo() {
		return this.tranBizNo;
	}
	
	/**
	 * @param apprSubSerno
	 */
	public void setApprSubSerno(String apprSubSerno) {
		this.apprSubSerno = apprSubSerno;
	}
	
    /**
     * @return apprSubSerno
     */
	public String getApprSubSerno() {
		return this.apprSubSerno;
	}
	
	/**
	 * @param tranType
	 */
	public void setTranType(String tranType) {
		this.tranType = tranType;
	}
	
    /**
     * @return tranType
     */
	public String getTranType() {
		return this.tranType;
	}
	
	/**
	 * @param chgTableName
	 */
	public void setChgTableName(String chgTableName) {
		this.chgTableName = chgTableName;
	}
	
    /**
     * @return chgTableName
     */
	public String getChgTableName() {
		return this.chgTableName;
	}
	
	/**
	 * @param chgFieldName
	 */
	public void setChgFieldName(String chgFieldName) {
		this.chgFieldName = chgFieldName;
	}
	
    /**
     * @return chgFieldName
     */
	public String getChgFieldName() {
		return this.chgFieldName;
	}
	
	/**
	 * @param chgFieldValue
	 */
	public void setChgFieldValue(String chgFieldValue) {
		this.chgFieldValue = chgFieldValue;
	}
	
    /**
     * @return chgFieldValue
     */
	public String getChgFieldValue() {
		return this.chgFieldValue;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}


}