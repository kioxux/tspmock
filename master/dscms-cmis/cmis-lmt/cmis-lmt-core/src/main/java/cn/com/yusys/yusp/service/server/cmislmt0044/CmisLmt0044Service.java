package cn.com.yusys.yusp.service.server.cmislmt0044;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtDiscOrg;
import cn.com.yusys.yusp.dto.server.cmislmt0044.req.CmisLmt0044ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0044.resp.CmisLmt0044ListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0044.resp.CmisLmt0044RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.cmiscfg0002.resp.CmisCfg0002ListRespDto;
import cn.com.yusys.yusp.service.LmtDiscOrgService;
import cn.com.yusys.yusp.service.LmtSubBasicConfService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0044Service
 * @类描述: #对内服务类
 * @功能描述: 根据额度品种编号获取产品扩展属性
 * @修改备注: 2021/7/12     zhangjw   新增接口
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0044Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0044Service.class);

    @Autowired
    private LmtDiscOrgService lmtDiscOrgService ;

    @Transactional
    public CmisLmt0044RespDto execute(CmisLmt0044ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0044.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0044.value);
        CmisLmt0044RespDto resqDto = new CmisLmt0044RespDto() ;

        try {
            logger.info("获取查询机构贴现限额",DscmsLmtEnum.TRADE_CODE_CMISLMT0044.key);

            List<CmisLmt0044ListRespDto> cmisLmt0044ListRespDtoList = lmtDiscOrgService.queryDetailByreqDto(reqDto);

            //将列表放入返回中
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
            resqDto.setLmtDiscOrgList(cmisLmt0044ListRespDtoList);

        } catch (YuspException e) {
            logger.error("获取查询机构贴现限额报错：",e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0044.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0044.value);
        return resqDto;
    }

}