package cn.com.yusys.yusp.service.server.cmislmt0043;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0043.req.req.CmisLmt0043ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0043.req.resp.CmisLmt0043RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.LmtSubBasicConfService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0043Service
 * @类描述: #对内服务类
 * @功能描述: 获取客户及其集团成员的标和非标业务授信余额（除资管计划、净值型产品以及承销额度）
 *          获取客户单笔投资业务余额即获取主体授信和产品授信的授信余额，当分项状态为冻结或正常状态时，取该分项的敞口授信金额
 *          当分项状态为失效未结清时，取该分项的敞口用信余额
 * @修改备注: 2021/08/10     lizx   新增接口
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0043Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0043Service.class);

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;

    @Transactional
    public CmisLmt0043RespDto execute(CmisLmt0043ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0043.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0043.value);
        CmisLmt0043RespDto resqDto = new CmisLmt0043RespDto() ;

        try {
            String instuCde = reqDto.getInstuCde();
            //客户号
            String cusId = reqDto.getCusId();
            //根据客户号查询 额度状态为正常或者冻结的授信余额 (标和非标业务)
            BigDecimal lmtBal = apprLmtSubBasicInfoService.selectLmtType04And05LmtBal(cusId, instuCde) ;
            //查询状态为失效未结清的授信余额
            BigDecimal useBal = apprLmtSubBasicInfoService.selectLmtType04And05UseBal(cusId, instuCde) ;
            //最终余额
            BigDecimal resultBal = lmtBal.add(useBal) ;

            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0043.key+"查询客户授信余额{},结果为{}", cusId, useBal);
            //组装返回报文
            resqDto.setLmtBal(resultBal);
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("根据额度品种编号获取产品扩展属性报错：",e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0043.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0043.value);
        return resqDto;
    }

}