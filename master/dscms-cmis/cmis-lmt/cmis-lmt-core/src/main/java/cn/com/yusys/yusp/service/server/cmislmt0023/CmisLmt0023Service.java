package cn.com.yusys.yusp.service.server.cmislmt0023;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0023.req.CmisLmt0023ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0023.resp.CmisLmt0023LmtListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0023.resp.CmisLmt0023RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0023Service
 * @类描述: #对内服务类
 * @功能描述: 个人额度查询（新微贷）
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0023Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0023Service.class);

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService;

    /**
     * 个人额度查询（新微贷）
     *
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0023RespDto execute(CmisLmt0023ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0023.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0023.value);
        CmisLmt0023RespDto resqDto = new CmisLmt0023RespDto();
        List<CmisLmt0023LmtListRespDto> lmtListRespDtos = new ArrayList<>();

        String cusId = reqDto.getCusId();
        //额度品种编号
        String lmtSubNo = reqDto.getLmtSubNo();

        if (StringUtils.isEmpty(lmtSubNo)){
            //如果额度品种编号为空，则查询所有小微贷额度品种
            lmtSubNo = CmisLmtConstants.XWD_LMT_SUB_NO;
        }

        //总授信额度
        BigDecimal lmtAmt = BigDecimal.ZERO;
        //总授信额度已用
        BigDecimal totalOutstandAmt = BigDecimal.ZERO;

        try {
            List<String> lmtSubNos = Arrays.asList(lmtSubNo.split(","));
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("cusId",cusId);
            queryModel.addCondition("limitSubNos",lmtSubNos);

            List<Map<String, Object>> lmtInfoList = apprLmtSubBasicInfoService.selectLmtInfoByQueryModel(queryModel);

            if (lmtInfoList!=null && lmtInfoList.size()>0){
                for (Map<String, Object> lmtInfo : lmtInfoList) {
                    CmisLmt0023LmtListRespDto lmtListRespDto = new CmisLmt0023LmtListRespDto();
                    //客户号
                    lmtListRespDto.setCusId((String) lmtInfo.get("cusId"));
                    //客户名称
                    lmtListRespDto.setCusName((String) lmtInfo.get("cusName"));
                    //额度分项流水号
                    lmtListRespDto.setApprSubSerno((String) lmtInfo.get("apprSubSerno"));
                    //授信品种编号
                    lmtListRespDto.setLmtBizType((String) lmtInfo.get("limitSubNo"));
                    //授信品种名称
                    lmtListRespDto.setLmtBizTypeName((String) lmtInfo.get("limitSubName"));
                    //授信敞口金额
                    lmtListRespDto.setSpacAmt(BigDecimalUtil.replaceNull(lmtInfo.get("spacAmt")));
                    //授信敞口已用
                    lmtListRespDto.setSpacOutstandAmt(BigDecimalUtil.replaceNull(lmtInfo.get("spacOutstndAmt")));
                    //担保方式
                    lmtListRespDto.setGuarMode((String) lmtInfo.get("suitGuarWay"));
                    //授信总额
                    BigDecimal avlAmt = BigDecimalUtil.replaceNull(lmtInfo.get("avlAmt"));
                    lmtListRespDto.setAvlAmt(avlAmt);
                    //总授信额度 = 总授信额度+授信总额
                    lmtAmt = lmtAmt.add(avlAmt);

                    //已用额度
                    BigDecimal outstandAmt = BigDecimalUtil.replaceNull(lmtInfo.get("outstndAmt"));
                    //授信总额已用
                    lmtListRespDto.setOutstandAmt(outstandAmt);
                    //总授信额度已用 = 总授信额度已用+授信总额已用
                    totalOutstandAmt = totalOutstandAmt.add(outstandAmt);

                    //是否循环
                    lmtListRespDto.setIsRevolv((String) lmtInfo.get("isRevolv"));
                    //起始日期
                    lmtListRespDto.setStartDate((String) lmtInfo.get("startDate"));
                    //到期日期
                    lmtListRespDto.setEndDate((String) lmtInfo.get("lmtDate"));
                    //额度批复台账编号
                    lmtListRespDto.setApprSerno((String) lmtInfo.get("fkPkid"));
                    //主管客户经理
                    lmtListRespDto.setManagerId((String) lmtInfo.get("managerId"));
                    //主管机构
                    lmtListRespDto.setManagerBrId((String) lmtInfo.get("managerBrId"));

                    lmtListRespDtos.add(lmtListRespDto);
                }
            }

            //返回信息
            resqDto.setLmtAmt(lmtAmt);
            resqDto.setLmtValAmt(lmtAmt.subtract(totalOutstandAmt));
            resqDto.setLmtList(lmtListRespDtos);
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("个人额度查询（新微贷）：" , e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0023.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0023.value);
        return resqDto;
    }
}