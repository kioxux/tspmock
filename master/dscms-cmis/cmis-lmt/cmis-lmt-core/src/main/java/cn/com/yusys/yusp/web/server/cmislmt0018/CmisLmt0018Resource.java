package cn.com.yusys.yusp.web.server.cmislmt0018;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0018.req.CmisLmt0018ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0018.resp.CmisLmt0018RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0018.CmisLmt0018Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:单一客户额度同步
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "cmislmt0018:额度结构查询")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0018Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0018Resource.class);

    @Autowired
    private CmisLmt0018Service cmisLmt0018Service;
    /**
     * 交易码：cmislmt0018
     * 交易描述：额度结构查询
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("额度结构查询")
    @PostMapping("/cmislmt0018")
    protected @ResponseBody
    ResultDto<CmisLmt0018RespDto> cmisLmt0018(@Validated @RequestBody CmisLmt0018ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0018.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0018.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0018RespDto> cmisLmt0018RespDtoResultDto = new ResultDto<>();
        CmisLmt0018RespDto cmisLmt0018RespDto = new CmisLmt0018RespDto() ;
        // 调用对应的service层
        try {
            //TODO:调用Service 服务处理接口请求
            cmisLmt0018RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0018RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0018.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0018.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0018.value, e.getMessage());
            // 封装cmisLmt0018RespDtoResultDto中异常返回码和返回信息
            cmisLmt0018RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0018RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0018RespDto到cmisLmt0018RespDtoResultDto中
        cmisLmt0018RespDtoResultDto.setData(cmisLmt0018RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0018.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0018.value, JSON.toJSONString(cmisLmt0018RespDtoResultDto));
        return cmisLmt0018RespDtoResultDto;
    }
}
