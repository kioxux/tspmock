package cn.com.yusys.yusp.web.server.cmislmt0046;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0046.req.CmisLmt0046ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0046.resp.CmisLmt0046RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0046.CmisLmt0046Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据分项编号和用信产品编号，查询分项向下是否存在适用改产品的明
 *
 * @author zhangjw 2021/7/14
 * @version 1.0
 */
@Api(tags = "cmislmt0046:根据分项编号和用信产品编号，查询分项向下是否存在适用改产品的明")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0046Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0046Resource.class);

    @Autowired
    private CmisLmt0046Service cmisLmt0046Service;
    /**
     * 交易码：cmislmt0046
     * 交易描述：根据分项编号和用信产品编号，查询分项向下是否存在适用改产品的明
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("根据分项编号和用信产品编号，查询分项向下是否存在适用改产品的明")
    @PostMapping("/cmislmt0046")
    protected @ResponseBody
    ResultDto<CmisLmt0046RespDto> cmisLmt0046(@Validated @RequestBody CmisLmt0046ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0046.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0046.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0046RespDto> cmisLmt0046RespDtoResultDto = new ResultDto<>();
        CmisLmt0046RespDto cmisLmt0046RespDto = new CmisLmt0046RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0046RespDto = cmisLmt0046Service.execute(reqDto);
            cmisLmt0046RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0046RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);

        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0046.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0046.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0046.value, e.getMessage());
            // 封装xddb0046DataResultDto中异常返回码和返回信息
            cmisLmt0046RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0046RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0016RespDto到cmisLmt0016RespDtoResultDto中
        cmisLmt0046RespDtoResultDto.setData(cmisLmt0046RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0046.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0046.value, JSON.toJSONString(cmisLmt0046RespDtoResultDto));
        return cmisLmt0046RespDtoResultDto;
    }
}
