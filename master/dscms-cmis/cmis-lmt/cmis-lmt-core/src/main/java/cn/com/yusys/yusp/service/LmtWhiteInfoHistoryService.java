/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtWhiteInfoHistory;
import cn.com.yusys.yusp.repository.mapper.LmtWhiteInfoHistoryMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtWhiteInfoHistoryService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: dumd
 * @创建时间: 2021-05-22 11:22:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtWhiteInfoHistoryService {

    @Autowired
    private LmtWhiteInfoHistoryMapper lmtWhiteInfoHistoryMapper;

    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtWhiteInfoHistory selectByPrimaryKey(String pkId) {
        return lmtWhiteInfoHistoryMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtWhiteInfoHistory> selectAll(QueryModel model) {
        List<LmtWhiteInfoHistory> records = (List<LmtWhiteInfoHistory>) lmtWhiteInfoHistoryMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtWhiteInfoHistory> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtWhiteInfoHistory> list = lmtWhiteInfoHistoryMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(LmtWhiteInfoHistory record) {
        return lmtWhiteInfoHistoryMapper.insert(record);
    }

    /**
     * 批量插入.
     * @param lmtWhiteInfoHistoryList 白名单管理
     * @return 结果
     */
    public int batchInsert(List<LmtWhiteInfoHistory> lmtWhiteInfoHistoryList) {
        int count = lmtWhiteInfoHistoryList.size() != 0 ? lmtWhiteInfoHistoryMapper.batchInsert(lmtWhiteInfoHistoryList) : 0;
        return count;
    }
}


