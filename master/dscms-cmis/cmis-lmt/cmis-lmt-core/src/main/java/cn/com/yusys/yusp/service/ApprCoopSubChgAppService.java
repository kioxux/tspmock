/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.ApprCoopChgApp;
import cn.com.yusys.yusp.domain.ApprCoopSubChgApp;
import cn.com.yusys.yusp.domain.ApprCoopSubInfo;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.repository.mapper.ApprCoopSubChgAppMapper;
import cn.com.yusys.yusp.repository.mapper.ApprCoopSubInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Supplier;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprCoopSubChgAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-20 20:38:47
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ApprCoopSubChgAppService {

    @Autowired
    private ApprCoopSubChgAppMapper apprCoopSubChgAppMapper;

    @Autowired
    private ApprCoopSubInfoMapper apprCoopSubInfoMapper;

    @Autowired
    private ApprCoopSubInfoService apprCoopSubInfoService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ApprCoopSubChgApp selectByPrimaryKey(String pkId) {
        return apprCoopSubChgAppMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<ApprCoopSubChgApp> selectAll(QueryModel model) {
        List<ApprCoopSubChgApp> records = (List<ApprCoopSubChgApp>) apprCoopSubChgAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<ApprCoopSubChgApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ApprCoopSubChgApp> list = apprCoopSubChgAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(ApprCoopSubChgApp record) {
        return apprCoopSubChgAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(ApprCoopSubChgApp record) {
        return apprCoopSubChgAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(ApprCoopSubChgApp record) {
        return apprCoopSubChgAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(ApprCoopSubChgApp record) {
        return apprCoopSubChgAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return apprCoopSubChgAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return apprCoopSubChgAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: insertInfoToApp
     * @方法描述: 将info表内容传入app表
     * @参数与返回说明:
     * @算法描述: 无
     */

    public void insertInfoToApp(ApprCoopChgApp apprCoopChgApp){

        // User userInfo = SessionUtils.getUserInformation();
        String apprSenro = apprCoopChgApp.getApprSerno();//获取合作方批复台账编号

        List<ApprCoopSubInfo> apprCoopSubInfos = apprCoopSubInfoMapper.selectByApprSerno(apprSenro);

        String pkid = StringUtils.getUUID();
        String fkPkid = apprCoopChgApp.getPkId();
        String subAppSerno = apprCoopChgApp.getAppSerno();
        // String inputId = userInfo.getLoginCode();
        // String inputBrId = userInfo.getOrg().getCode();

        //获取当前时间
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dayTime = sdf.format(date).toString();

        for (int i = 0; i < apprCoopSubInfos.size(); i++) {
            //创建App表对象
            ApprCoopSubChgApp apprCoopSubChgApp = new ApprCoopSubChgApp();
            //将前端获取info表对象导入app表对象
            apprCoopSubChgApp.setPkId(apprCoopSubInfos.get(i).getPkId());
            apprCoopSubChgApp.setApprSerno(apprCoopSubInfos.get(i).getApprSerno());
            apprCoopSubChgApp.setApprSubSerno(apprCoopSubInfos.get(i).getApprSubSerno());
            apprCoopSubChgApp.setLimitSubNo(apprCoopSubInfos.get(i).getLimitSubNo());
            apprCoopSubChgApp.setLimitSubName(apprCoopSubInfos.get(i).getLimitSubName());
            apprCoopSubChgApp.setAvlAmt(apprCoopSubInfos.get(i).getAvlAmt());
            apprCoopSubChgApp.setOutstndAmt(apprCoopSubInfos.get(i).getOutstndAmt());
            apprCoopSubChgApp.setIsRevolv(apprCoopSubInfos.get(i).getIsRevolv());
            apprCoopSubChgApp.setCurType(apprCoopSubInfos.get(i).getCurType());
            apprCoopSubChgApp.setTerm(apprCoopSubInfos.get(i).getTerm());
            apprCoopSubChgApp.setStartDate(apprCoopSubInfos.get(i).getStartDate());
            apprCoopSubChgApp.setEndDate(apprCoopSubInfos.get(i).getEndDate());
            apprCoopSubChgApp.setIsRevolv(apprCoopSubInfos.get(i).getIsRevolv());
            apprCoopSubChgApp.setIsRevolv(apprCoopSubInfos.get(i).getIsRevolv());
            apprCoopSubChgApp.setOprType(CmisLmtConstants.LMT_APP_TYPE_01);
            apprCoopSubChgApp.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
            apprCoopSubChgApp.setFkPkid(fkPkid);
            apprCoopSubChgApp.setSubAppSerno(subAppSerno);
            apprCoopSubChgApp.setInputDate(dayTime);
            // apprCoopSubChgApp.setInputId(inputId);
            // apprCoopSubChgApp.setInputBrId(inputBrId);
            apprCoopSubChgAppMapper.insert(apprCoopSubChgApp);
        }
    }

    /**
     * @方法名称: createApprCoopSubChgApp
     * @方法描述: 根据多个主键获取分项记录
     * @参数与返回说明:【serno】本次批复变更申请流水号 【pkId】 主键的流水号
     * @算法描述: 无
     */
    public List<ApprCoopSubChgApp> createApprCoopSubChgApp(String apprSerno, String pkId) {
        //判断传入的父级编号是否为空
        if (StringUtils.isBlank(apprSerno)) {
            throw BizException.error(null,EclEnum.ECL070041.key, EclEnum.ECL070041.value);
        }

        List<ApprCoopSubChgApp> apprCoopSubChgApps = new ArrayList<>() ;
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        //根据父级编号，查询分项信息
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", apprSerno);
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        List<ApprCoopSubInfo> apprCoopSubInfoList =  apprCoopSubInfoService.selectAcsiInfoBySerno(queryModel);
        //生成主键，获取流水号
        HashMap<String, String> param = new HashMap<>();
        Supplier<ApprCoopSubChgApp> supplier = ApprCoopSubChgApp::new ;
        //判断获取的分项记录不为空，遍历处理
        if(apprCoopSubInfoList.size()>0){
            for(ApprCoopSubInfo apprCoopSubInfo:apprCoopSubInfoList){
                //获取初始化对象
                ApprCoopSubChgApp apprCoopSubChgApp = supplier.get() ;
                BeanUtils.copyProperties(apprCoopSubInfo, apprCoopSubChgApp);
                //生成主键，获取流水号
                String pkValue = StringUtils.getUUID();//sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.PK_VALUE, param);
                //生成分项调整申请流水号
                String appSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, param);
                //主申请流水号
                apprCoopSubChgApp.setSubAppSerno(appSerno);
                //主键
                apprCoopSubChgApp.setPkId(pkValue);
                //外健关联主键（感觉很多余）
                apprCoopSubChgApp.setFkPkid(pkId);
                //数据导入
                apprCoopSubChgApps.add(apprCoopSubChgApp) ;
            }
        }
        return apprCoopSubChgApps ;
    }

}
