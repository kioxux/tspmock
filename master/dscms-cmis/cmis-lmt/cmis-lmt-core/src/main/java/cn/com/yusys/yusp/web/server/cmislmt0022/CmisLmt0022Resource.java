package cn.com.yusys.yusp.web.server.cmislmt0022;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0022.req.CmisLmt0022ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0022.resp.CmisLmt0022RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0022.CmisLmt0022Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:客户分类额度查询
 *
 * @author dumd 20210524
 * @version 1.0
 */
@Api(tags = "cmislmt0022:客户分类额度查询")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0022Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0022Resource.class);

    @Autowired
    private CmisLmt0022Service cmisLmt0022Service;

    /**
     * 交易码：cmislmt0022
     * 交易描述：客户分类额度查询
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("客户分类额度查询")
    @PostMapping("/cmislmt0022")
    protected @ResponseBody
    ResultDto<CmisLmt0022RespDto> cmisLmt0022(@Validated @RequestBody CmisLmt0022ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0022.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0022.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0022RespDto> cmisLmt0022RespDtoResultDto = new ResultDto<>();
        CmisLmt0022RespDto cmisLmt0022RespDto = new CmisLmt0022RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0022RespDto = cmisLmt0022Service.execute(reqDto);
            cmisLmt0022RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0022RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0022.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0022.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0022.value, e.getMessage());
            // 封装cmisLmt0022RespDtoResultDto中异常返回码和返回信息
            cmisLmt0022RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0022RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0022RespDto到cmisLmt0022RespDtoResultDto中
        cmisLmt0022RespDtoResultDto.setData(cmisLmt0022RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0022.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0022.value, JSON.toJSONString(cmisLmt0022RespDtoResultDto));
        return cmisLmt0022RespDtoResultDto;
    }
}