/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ContAccRel;
import cn.com.yusys.yusp.service.ContAccRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ContAccRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZR
 * @创建时间: 2021-04-20 10:46:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/contaccrel")
public class ContAccRelResource {
    @Autowired
    private ContAccRelService contAccRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<ContAccRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<ContAccRel> list = contAccRelService.selectAll(queryModel);
        return new ResultDto<List<ContAccRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<ContAccRel>> index(QueryModel queryModel) {
        List<ContAccRel> list = contAccRelService.selectByModel(queryModel);
        return new ResultDto<List<ContAccRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<ContAccRel> show(@PathVariable("pkId") String pkId) {
        ContAccRel contAccRel = contAccRelService.selectByPrimaryKey(pkId);
        return new ResultDto<ContAccRel>(contAccRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<ContAccRel> create(@RequestBody ContAccRel contAccRel) {
        contAccRelService.insert(contAccRel);
        return new ResultDto<ContAccRel>(contAccRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ContAccRel contAccRel) {
        int result = contAccRelService.update(contAccRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = contAccRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = contAccRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 额度视图列表，根据合同号查询合同号下台账列表
     * @author zhangjw 2021-05-05
     * @return
     */
    @PostMapping("/queryListByDealBizNo")
    protected ResultDto<List<ContAccRel>> queryListByDealBizNo(@RequestBody QueryModel queryModel) {
        List<ContAccRel> list = contAccRelService.queryListByDealBizNo(queryModel);
        return new ResultDto<List<ContAccRel>>(list);
    }

    /**
     * @函数名称:
     * @函数描述:检查合同编号是否存在
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkSameDealBizNoIsExist")
    protected ResultDto<Integer> checkSameDealBizNoIsExist(@RequestBody QueryModel queryModel) {
        String dealBizNo = (String) queryModel.getCondition().get("dealBizNo");
        int result = contAccRelService.checkSameDealBizNoIsExist(dealBizNo);
        return new ResultDto<Integer>(result);
    }

}
