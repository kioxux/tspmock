package cn.com.yusys.yusp.web.server.cmislmt0064;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0064.req.CmisLmt0064ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0064.resp.CmisLmt0064RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0064.CmisLmt0064Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:关联交易计算个人客户余额
 *
 * @author zhangjw 20211021
 * @version 1.0
 */
@Api(tags = "cmislmt0064:关联交易计算个人客户余额")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0064Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0064Resource.class);

    @Autowired
    private CmisLmt0064Service cmisLmt0064Service;

    /**
     * 交易码：CmisLmt0064
     * 交易描述：根据分项编号查询向下非失效状态的分项明细
     * 1、如果授信为循环类且为生效或冻结状态则取授信敞口
     * 2、如果授信为循环类且为到期未结清状态则取用信余额
     * 3、如果授信为非循环类则取用信余额
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("关联交易计算个人客户余额")
    @PostMapping("/cmislmt0064")
    protected @ResponseBody
    ResultDto<CmisLmt0064RespDto> CmisLmt0064(@Validated @RequestBody CmisLmt0064ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0064.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0064.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0064RespDto> CmisLmt0064RespDtoResultDto = new ResultDto<>();
        CmisLmt0064RespDto CmisLmt0064RespDto = new CmisLmt0064RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0064RespDto = cmisLmt0064Service.execute(reqDto);
            CmisLmt0064RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0064RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0064.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0064.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0064.value, e.getMessage());
            // 封装CmisLmt0064RespDtoResultDto中异常返回码和返回信息
            CmisLmt0064RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0064RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0064RespDto到CmisLmt0064RespDtoResultDto中
        CmisLmt0064RespDtoResultDto.setData(CmisLmt0064RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0064.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0064.value, JSON.toJSONString(CmisLmt0064RespDtoResultDto));
        return CmisLmt0064RespDtoResultDto;
    }
}