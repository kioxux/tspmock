package cn.com.yusys.yusp.service.server.cmislmt0025;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtContRel;
import cn.com.yusys.yusp.domain.LmtWhiteInfo;
import cn.com.yusys.yusp.dto.server.cmislmt0025.req.CmisLmt0025OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0025.req.CmisLmt0025ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0025.resp.CmisLmt0025RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.LmtContRelService;
import cn.com.yusys.yusp.service.LmtWhiteInfoService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0025Service
 * @类描述: #对内服务类
 * @功能描述: 承兑行白名单额度恢复
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0025Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0025Service.class);

    @Autowired
    private LmtWhiteInfoService lmtWhiteInfoService;

    @Autowired
    private LmtContRelService lmtContRelService;

    /**
     * 承兑行白名单额度恢复
     *
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0025RespDto execute(CmisLmt0025ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.value);
        CmisLmt0025RespDto resqDto = new CmisLmt0025RespDto();

        Map<String, String> paramMap = (Map<String, String>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(reqDto);
        Map<String, String> resultMap = new HashMap<>() ;
        resultMap.putAll(paramMap);
        resultMap.put("serviceCode", CmisLmtConstants.CMIS_LMT0014_SERVICE_CODE) ;


        //占用额度列表
        List<CmisLmt0025OccRelListReqDto> occRelListReqDtos = reqDto.getOccRelList();

        //金融机构代码
        String instuCde = reqDto.getInstuCde();
        //登记人	inputId
        String inputId = reqDto.getInputId();
        //登记机构
        String inputBrId = reqDto.getInputBrId();
        //登记日期
        String inputDate = reqDto.getInputDate();

        try {
            //key: 额度分项编号  value:占用总余额（折人民币）
            Map<String,BigDecimal> subNoAndAmtMap = new HashMap<>();

            for (CmisLmt0025OccRelListReqDto occRelList : occRelListReqDtos) {
                //交易业务编号
                String dealBizNo = occRelList.getDealBizNo();
                resultMap.put("serno", dealBizNo) ;
                List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelListByDealBizNo(dealBizNo, CmisLmtConstants.STD_ZB_BIZ_ATTR_2);
                for (LmtContRel lmtContRel : lmtContRelList) {
                    logger.info("【{}】 接口承兑行白名单额度恢复，恢复额度信息：【{}】",DscmsLmtEnum.TRADE_CODE_CMISLMT0025.key , JSON.toJSONString(lmtContRel) );
                    //额度分项编号
                    String limitSubNo = lmtContRel.getLimitSubNo();
                    //占用总余额（折人民币）
                    BigDecimal bizTotalBalanceAmtCny = lmtContRel.getBizTotalBalanceAmtCny();

                    if (subNoAndAmtMap.containsKey(limitSubNo)){
                        //如果subNoAndAmtMap有该额度分项编号，则占用总金额（折人民币）累加
                        bizTotalBalanceAmtCny = bizTotalBalanceAmtCny.add(subNoAndAmtMap.get(limitSubNo));
                        subNoAndAmtMap.put(limitSubNo,bizTotalBalanceAmtCny);
                    }else{
                        //没有则放入subNoAndAmtMap
                        subNoAndAmtMap.put(limitSubNo,bizTotalBalanceAmtCny);
                    }

                    //占用总余额（折人民币）改为0
                    lmtContRel.setBizTotalBalanceAmtCny(BigDecimal.ZERO);
                    //占用敞口余额（折人民币）改为0
                    lmtContRel.setBizSpacBalanceAmtCny(BigDecimal.ZERO);
                    //交易业务状态改为结清已使用
                    lmtContRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_300);
                    //最近更新人
                    lmtContRel.setUpdId(inputId);
                    //最近更新机构
                    lmtContRel.setUpdBrId(inputBrId);
                    //最近更新日期
                    lmtContRel.setUpdDate(inputDate);
                    //修改时间
                    lmtContRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));

                    //更新该笔分项占用关系记录
                    lmtContRelService.updateByPKeyInApprLmtChgDetails(lmtContRel, resultMap);
                }
            }
            //更新白名单额度信息表里的已用限额
            for(Map.Entry<String,BigDecimal> entry : subNoAndAmtMap.entrySet()){

                String limitSubNo = entry.getKey();
                LmtWhiteInfo lmtWhiteInfo = lmtWhiteInfoService.selectLmtWhiteInfoBySubNo(limitSubNo);
                if(lmtWhiteInfo != null){
                    //已用限额
                    BigDecimal sigUseAmt = lmtWhiteInfo.getSigUseAmt();
                    logger.info("【{}】 接口承兑行白名单额度恢复，恢复前金额：【{}】",DscmsLmtEnum.TRADE_CODE_CMISLMT0025.key , sigUseAmt);
                    sigUseAmt = sigUseAmt.subtract(entry.getValue());
                    logger.info("【{}】 接口承兑行白名单额度恢复，恢复后金额：【{}】",DscmsLmtEnum.TRADE_CODE_CMISLMT0025.key , sigUseAmt);
                    lmtWhiteInfo.setSigUseAmt(sigUseAmt);
                    lmtWhiteInfoService.updateByPKeyInApprLmtChgDetails(lmtWhiteInfo,resultMap);
                }
            }

            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("承兑行白名单额度恢复" , e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.value);
        return resqDto;
    }
}