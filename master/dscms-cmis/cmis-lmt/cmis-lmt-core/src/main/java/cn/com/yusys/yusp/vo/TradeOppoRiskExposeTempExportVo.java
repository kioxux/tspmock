package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;
import java.time.LocalDate;

@ExcelCsv(namePrefix = "交易对手风险暴露列表导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class TradeOppoRiskExposeTempExportVo {

    /*
    客户编号
     */
    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /*
    客户名称
     */
    @ExcelField(title = "客户名称", viewLength = 20)
    private String cusName;

    /*
    产品名称
     */
    @ExcelField(title = "产品名称", viewLength = 20)
    private String prdName;

    /*
    本金金额
     */
    @ExcelField(title = "本金金额", viewLength = 20)
    private BigDecimal holdPosition;

    /*
    不考虑缓释的风险暴露
     */
    @ExcelField(title = "不考虑缓释的风险暴露", viewLength = 20)
    private BigDecimal riskExposeNoslowRelease;

    /*
    不可豁免的风险暴露
     */
    @ExcelField(title = "不可豁免的风险暴露", viewLength = 20)
    private BigDecimal riskExposeNoexampt;

    /*
    可豁免的风险暴露
     */
    @ExcelField(title = "可豁免的风险暴露", viewLength = 20)
    private BigDecimal riskExposeExampt;

    /*
    风险缓释金额
     */
    @ExcelField(title = "风险缓释金额", viewLength = 20)
    private BigDecimal riskExposeAmt;

    /*
    风险缓释客户号
     */
    @ExcelField(title = "风险缓释客户号", viewLength = 20)
    private String guarCusId;
    /*
    风险缓释客户名称
     */
    @ExcelField(title = "风险缓释客户名称", viewLength = 20)
    private String guarCusName;
    /*
    业务日期
     */
    @ExcelField(title = "业务日期", viewLength = 20)
    private String bussDate;

    public String getBussDate() {
        return bussDate;
    }

    public void setBussDate(String bussDate) {
        this.bussDate = bussDate;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public BigDecimal getHoldPosition() {
        return holdPosition;
    }

    public void setHoldPosition(BigDecimal holdPosition) {
        this.holdPosition = holdPosition;
    }

    public BigDecimal getRiskExposeNoslowRelease() {
        return riskExposeNoslowRelease;
    }

    public void setRiskExposeNoslowRelease(BigDecimal riskExposeNoslowRelease) {
        this.riskExposeNoslowRelease = riskExposeNoslowRelease;
    }

    public BigDecimal getRiskExposeNoexampt() {
        return riskExposeNoexampt;
    }

    public void setRiskExposeNoexampt(BigDecimal riskExposeNoexampt) {
        this.riskExposeNoexampt = riskExposeNoexampt;
    }

    public BigDecimal getRiskExposeExampt() {
        return riskExposeExampt;
    }

    public void setRiskExposeExampt(BigDecimal riskExposeExampt) {
        this.riskExposeExampt = riskExposeExampt;
    }

    public BigDecimal getRiskExposeAmt() {
        return riskExposeAmt;
    }

    public void setRiskExposeAmt(BigDecimal riskExposeAmt) {
        this.riskExposeAmt = riskExposeAmt;
    }

    public String getGuarCusId() {
        return guarCusId;
    }

    public void setGuarCusId(String guarCusId) {
        this.guarCusId = guarCusId;
    }

    public String getGuarCusName() {
        return guarCusName;
    }

    public void setGuarCusName(String guarCusName) {
        this.guarCusName = guarCusName;
    }
}
