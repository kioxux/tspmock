package cn.com.yusys.yusp.web.server.cmislmt0040;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0040.req.CmisLmt0040ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0040.resp.CmisLmt0040RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0040.CmisLmt0040Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据额度品种编号查找适用产品编号
 *
 * @author zhangjw 2021/7/6
 * @version 1.0
 */
@Api(tags = "cmislmt0040:集团成员退出或成员解散")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0040Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0040Resource.class);

    @Autowired
    private CmisLmt0040Service cmisLmt0040Service;
    /**
     * 交易码：cmislmt0040
     * 交易描述：集团成员退出或成员解散
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("集团成员退出或成员解散")
    @PostMapping("/cmislmt0040")
    protected @ResponseBody
    ResultDto<CmisLmt0040RespDto> cmisLmt0040(@Validated @RequestBody CmisLmt0040ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0040.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0040.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0040RespDto> cmisLmt0040RespDtoResultDto = new ResultDto<>();
        CmisLmt0040RespDto cmisLmt0040RespDto = new CmisLmt0040RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0040RespDto = cmisLmt0040Service.execute(reqDto);
            cmisLmt0040RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0040RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0040.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0040.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0040.value, e.getMessage());
            // 封装xddb0040DataResultDto中异常返回码和返回信息
            cmisLmt0040RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0040RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        cmisLmt0040RespDtoResultDto.setData(cmisLmt0040RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0040.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0040.value, JSON.toJSONString(cmisLmt0040RespDtoResultDto));
        return cmisLmt0040RespDtoResultDto;
    }
}
