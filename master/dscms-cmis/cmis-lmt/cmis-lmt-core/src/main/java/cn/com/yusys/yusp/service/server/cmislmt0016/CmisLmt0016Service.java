package cn.com.yusys.yusp.service.server.cmislmt0016;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprCoopSubInfo;
import cn.com.yusys.yusp.dto.server.cmislmt0016.req.CmisLmt0016ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0016.resp.CmisLmt0016RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0016.resp.Cmislmt0016LmtCopSubAccListRespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ApprCoopSubInfoMapper;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0016Service
 * @类描述: #对内服务类
 * @功能描述: 合作方额度查询
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0016Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0016Service.class);

    @Autowired
    private ApprCoopSubInfoMapper apprCoopSubInfoMapper;

    @Transactional
    public CmisLmt0016RespDto execute(CmisLmt0016ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0016.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0016.value);
        CmisLmt0016RespDto resqDto = new CmisLmt0016RespDto() ;
        try {

            String instuCde = reqDto.getInstuCde();//金融机构代码
            String cusId = reqDto.getCusId();//合作方编号
            String cudName = reqDto.getCudName();//合作方名称
            String copType = reqDto.getCopType();//合作方额度类型
            String accNo = reqDto.getAccNo();//批复编号
            String subAccNo = reqDto.getSubAccNo();//额度分项/项目编号
            String apprStatus = reqDto.getApprStatus();//批复状态
            Integer startNum = reqDto.getStartNum();//起始记录数
            Integer pageCount = reqDto.getPageCount();//本次查询条数

            QueryModel model = new QueryModel();
            model.addCondition("instuCde",instuCde);
            if(!StringUtils.isBlank(cusId)){
                model.addCondition("cusId",cusId);
            }
            if(!StringUtils.isBlank(cudName)){
                model.addCondition("cudName",cudName);
            }
            if(!StringUtils.isBlank(copType)){
                model.addCondition("copType",copType);
            }
            if(!StringUtils.isBlank(accNo)){
                model.addCondition("apprSerno",accNo);
            }
            if(!StringUtils.isBlank(subAccNo)){
                model.addCondition("apprSubSerno",subAccNo);
            }
            if(!StringUtils.isBlank(apprStatus)){
                model.addCondition("apprStatus",apprStatus);
            }else{
                model.addCondition("noApprStatus", CmisLmtConstants.STD_ZB_LMT_STATE_03);
            }
            model.addCondition("startNum",startNum);
            model.addCondition("pageCount",pageCount);

            List<ApprCoopSubInfo> apprCoopSubInfoList = apprCoopSubInfoMapper.selectInfoByModel(model);

            List<Cmislmt0016LmtCopSubAccListRespDto> lmtCopSubAccList = new ArrayList<>();

            for(int i=0;i<apprCoopSubInfoList.size();i++){
                Cmislmt0016LmtCopSubAccListRespDto lmtCopSubAccListRespDto = new Cmislmt0016LmtCopSubAccListRespDto();

                ApprCoopSubInfo apprCoopSubInfo = apprCoopSubInfoList.get(i);

                //批复台账编号
                lmtCopSubAccListRespDto.setAccNo(apprCoopSubInfo.getApprSerno());
                //分项编号
                lmtCopSubAccListRespDto.setSubAccNo(apprCoopSubInfo.getApprSubSerno());
                //品种编号/项目编号
                lmtCopSubAccListRespDto.setLimitSubNo(apprCoopSubInfo.getLimitSubNo());
                //分项名称/项目名称
                lmtCopSubAccListRespDto.setLimitSubName(apprCoopSubInfo.getLimitSubName());
                //总合作额度
                lmtCopSubAccListRespDto.setAvlAmt(apprCoopSubInfo.getAvlAmt());
                //总合作额度已用
                lmtCopSubAccListRespDto.setAvlOutstndAmt(apprCoopSubInfo.getOutstndAmt());
                //总合作额度可用 = 总合作额度 - 总合作额度已用
                lmtCopSubAccListRespDto.setAvlAvailAmt(NumberUtils.sub(BigDecimalUtil.replaceNull(apprCoopSubInfo.getAvlAmt()),
                        BigDecimalUtil.replaceNull(apprCoopSubInfo.getOutstndAmt())));

                //币种
                lmtCopSubAccListRespDto.setCny(apprCoopSubInfo.getCurType());
                //期限
                lmtCopSubAccListRespDto.setTerm(apprCoopSubInfo.getTerm());
                //授信起始日
                lmtCopSubAccListRespDto.setStartDate(apprCoopSubInfo.getStartDate());
                //授信到期日
                lmtCopSubAccListRespDto.setEndDate(apprCoopSubInfo.getEndDate());
                //是否可循环
                lmtCopSubAccListRespDto.setIsRevolv(apprCoopSubInfo.getIsRevolv());
                //额度状态
                lmtCopSubAccListRespDto.setStatus(apprCoopSubInfo.getStatus());
                //单户限额
                lmtCopSubAccListRespDto.setSigAmt(apprCoopSubInfo.getSigAmt());
                //单笔业务限额
                lmtCopSubAccListRespDto.setSigBussAmt(apprCoopSubInfo.getSigBussAmt());

                lmtCopSubAccList.add(lmtCopSubAccListRespDto);
            }
            resqDto.setCount(apprCoopSubInfoList.size());
            resqDto.setLmtCopSubAccList(lmtCopSubAccList);
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("合作方额度查询接口报错：", e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0016.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0016.value);
        return resqDto;
    }
}