/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.DefaultDataHandle;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.vo.LmtDiscOrgTempExportVo;
import cn.com.yusys.yusp.vo.WhiteInfo01ImportVo;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtDiscOrg;
import cn.com.yusys.yusp.service.LmtDiscOrgService;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtDiscOrgResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-24 09:15:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtdiscorg")
public class LmtDiscOrgResource {
    private static final Logger log = LoggerFactory.getLogger(LmtDiscOrgResource.class);
    @Autowired
    private LmtDiscOrgService lmtDiscOrgService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtDiscOrg>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtDiscOrg> list = lmtDiscOrgService.selectAll(queryModel);
        return new ResultDto<List<LmtDiscOrg>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtDiscOrg>> index(QueryModel queryModel) {
        List<LmtDiscOrg> list = lmtDiscOrgService.selectByModel(queryModel);
        return new ResultDto<List<LmtDiscOrg>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<LmtDiscOrg>> selectbymodel(@RequestBody QueryModel queryModel) {
        List<LmtDiscOrg> list = lmtDiscOrgService.selectByModel(queryModel);
        return new ResultDto<List<LmtDiscOrg>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<LmtDiscOrg> show(@PathVariable("serno") String serno) {
        LmtDiscOrg lmtDiscOrg = lmtDiscOrgService.selectByPrimaryKey(serno);
        return new ResultDto<LmtDiscOrg>(lmtDiscOrg);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtDiscOrg> create(@RequestBody LmtDiscOrg lmtDiscOrg) throws URISyntaxException {
        lmtDiscOrgService.insert(lmtDiscOrg);
        return new ResultDto<LmtDiscOrg>(lmtDiscOrg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtDiscOrg lmtDiscOrg) throws URISyntaxException {
        int result = lmtDiscOrgService.update(lmtDiscOrg);
        return new ResultDto<Integer>(result);
    }

    /**
     * 异步下载贴现限额管控模板
     */
    @PostMapping("/exportlmtdiscorgtemp")
    public ResultDto<ProgressDto> asyncExportLmtDiscOrgTemp() {
        ProgressDto progressDto = lmtDiscOrgService.asyncExportLmtDiscOrgTemp();
        return ResultDto.success(progressDto);
    }

    /**
     * Excel数据导入
     * @param fileId Excel文件信息ID
     * @return
     */
    @PostMapping("/importlmtdiscorg")
    public ResultDto<ProgressDto> asyncImportLmtDiscOrg(@RequestParam("fileId") String fileId) {
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = null;
        try {
            tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
            FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);

            log.info("开始执行异步导入，导入fileId为[{}];", fileId);

            //异步改为同步
            ExcelUtils.syncImport(LmtDiscOrgTempExportVo.class, tempFile, new DefaultDataHandle(), ExcelUtils.batchConsumer(dataList -> {
                return lmtDiscOrgService.importLmtDiscOrg(dataList);
            }), true);

        } catch (IOException e) {
            log.error(EclEnum.ECL070094.value,e);
            return ResultDto.error(EcnEnum.ECN069999.key, e.getMessage());
        }
        return ResultDto.success().message("导入成功！");
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = lmtDiscOrgService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtDiscOrgService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
