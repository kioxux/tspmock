package cn.com.yusys.yusp.service.server.cmislmt0003;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.domain.ApprStrMtableInfo;
import cn.com.yusys.yusp.domain.ContAccRel;
import cn.com.yusys.yusp.domain.LmtContRel;
import cn.com.yusys.yusp.dto.client.esb.com001.req.Com001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.com001.req.Data;
import cn.com.yusys.yusp.dto.client.esb.com001.req.List;
import cn.com.yusys.yusp.dto.client.esb.com001.resp.Com001RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.req.CmisCus0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CmisCus0010RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0003.req.CmisLmt0003LmtSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0003.req.CmisLmt0003ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0003.resp.CmisLmt0003RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ApprLmtSubBasicInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprStrMtableInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtContRelMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.ApprStrMtableInfoService;
import cn.com.yusys.yusp.service.CmisCusClientService;
import cn.com.yusys.yusp.service.LmtContRelService;
import cn.com.yusys.yusp.service.client.bsp.comstar.com001.Com001Service;
import cn.com.yusys.yusp.service.server.comm.Comm4LmtCalFormulaUtils;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;
/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0003Service
 * @类描述: #对内服务类
 * @功能描述: 同业额度同步
 * @创建时间: 2021-05-07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0003Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0003Service.class);

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private ApprStrMtableInfoService apprStrMtableInfoService;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService;

    @Autowired
    private ApprLmtSubBasicInfoMapper apprLmtSubBasicInfoMapper;

    @Autowired
    private ApprStrMtableInfoMapper apprStrMtableInfoMapper;

    @Autowired
    private LmtContRelMapper lmtContRelMapper;

    @Autowired
    private LmtContRelService lmtContRelService;

    @Autowired
    private Com001Service com001Service;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private Comm4Service comm4Service ;

    @Autowired
    private Comm4LmtCalFormulaUtils comm4LmtCalFormulaUtils ;

    @Transactional
    public CmisLmt0003RespDto execute(CmisLmt0003ReqDto reqDto) throws YuspException, Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0003.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0003.value);
        CmisLmt0003RespDto resqDto = new CmisLmt0003RespDto() ;

        Map<String, String> paramtMap = (Map<String, String>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(reqDto);
        Map<String, String> resultMap = new HashMap<>() ;
        resultMap.putAll(paramtMap);
        resultMap.put("serno", reqDto.getAccNo()) ;
        resultMap.put("serviceCode", DscmsLmtEnum.TRADE_CODE_CMISLMT0003.key) ;

        //批复编号
        String accNo = reqDto.getAccNo() ;
        //判断是否生成新批复编号
        String isCreateAcc = reqDto.getIsCreateAcc() ;
        //新建批复主表对象信息
        ApprStrMtableInfo apprStrMtableInfo = new ApprStrMtableInfo() ;
        //批复主表主键
        String pkId = getPkId();
        //原批复台账编号
        String origiAccNo = reqDto.getOrigiAccNo();

        //如果是新增批复
        if(CmisLmtConstants.YES_NO_Y.equals(isCreateAcc)){
            logger.info("CmisLmt0003 批复台账编号【"+accNo+"】，是新批复覆盖老批复，校验数据是否合法开始");
            checkInfoIsVaild(reqDto);
            logger.info("CmisLmt0003 批复台账编号【"+accNo+"】，是新批复覆盖老批复，校验数据是否合法结束");
            //拷贝数据
            BeanUtils.copyProperties(reqDto, apprStrMtableInfo);

            //将原批复台账编号的批复台账状态改为失效未结清
            HashMap<String,Object> hashMap = new HashMap<>();
            hashMap.put("apprSerno",origiAccNo);
            hashMap.put("updateTime",DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            hashMap.put("updId",reqDto.getInputId());
            hashMap.put("updBrId",reqDto.getInputBrId());
            hashMap.put("updDate",reqDto.getInputDate());
            hashMap.put("apprStatus",CmisLmtConstants.STD_ZB_APPR_ST_99);//失效已结清
            hashMap.put("oprType",CmisLmtConstants.OPR_TYPE_ADD);

            logger.info("CmisLmt0003 将原批复台账编号【"+origiAccNo+"】台账状态更新为失效已结清");

            apprStrMtableInfoMapper.updateApprStrMtableInfoByApprSerno(hashMap);
        }

        //授信类型 默认07--同业额度
        apprStrMtableInfo.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_07);
        //授信模式 默认 01--综合授信
        apprStrMtableInfo.setLmtMode(CmisLmtConstants.STD_ZB_LMT_MODE_01);
        //集团客户号
        apprStrMtableInfo.setGrpNo(comm4Service.selectGrpNoByCusId(reqDto.getCusId()));
        //批复编号转换
        apprStrMtableInfo.setApprSerno(accNo);
        //状态
        apprStrMtableInfo.setApprStatus(reqDto.getAccStatus());
        //更新人
        apprStrMtableInfo.setUpdId(reqDto.getInputId());
        //更新机构
        apprStrMtableInfo.setUpdBrId(reqDto.getInputBrId());
        //更新日期
        apprStrMtableInfo.setUpdDate(reqDto.getInputDate());
        //金融机构代码
        apprStrMtableInfo.setInstuCde(reqDto.getInstuCde());
        //更新时间
        apprStrMtableInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //期限(月)
        apprStrMtableInfo.setTerm(reqDto.getTerm());
        //起始日期
        apprStrMtableInfo.setStartDate(reqDto.getStartDate());
        //到期日期
        apprStrMtableInfo.setEndDate(reqDto.getEndDate());
        //责任人
        apprStrMtableInfo.setManagerId(reqDto.getManagerId());
        //责任机构
        apprStrMtableInfo.setManagerBrId(reqDto.getManagerBrId());
        //操作类型 默认新增
        apprStrMtableInfo.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        //客户号
        apprStrMtableInfo.setCusId(reqDto.getCusId());
        //客户名称
        apprStrMtableInfo.setCusName(reqDto.getCusName());
        //客户主体类型
        apprStrMtableInfo.setCusType(reqDto.getCusType());

        if(CmisLmtConstants.YES_NO_Y.equals(isCreateAcc)){
            //创建时间
            apprStrMtableInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            //登记人
            apprStrMtableInfo.setInputId(reqDto.getInputId());
            //登记机构
            apprStrMtableInfo.setInputBrId(reqDto.getInputBrId());
            //登记日期
            apprStrMtableInfo.setInputDate(reqDto.getInputDate());
            //添加主键
            apprStrMtableInfo.setPkId(pkId);
            //插入批复主表信息
            logger.info("CmisLmt0003 批复台账编号【"+accNo+"】，是新批复覆盖老批复，插入批复主信息");
            apprStrMtableInfoMapper.insert(apprStrMtableInfo) ;
        }else{
            //查询新批复编号是否存在，存在则变更，不存在则新增
            if (apprStrMtableInfoService.valiApprIsExistsBySerno(accNo)){
                //存在则变更
                String oriPkId = apprStrMtableInfoService.selectApprStrMtableInfoBySerno(accNo).getPkId();
                apprStrMtableInfo.setPkId(oriPkId);
                logger.info("CmisLmt0003 批复台账编号【"+accNo+"】，不是新批复覆盖老批复，更新原批复主信息");
                apprStrMtableInfoMapper.updateByPrimaryKeySelective(apprStrMtableInfo);
            }else {
                //新增
                //创建时间
                apprStrMtableInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                //登记人
                apprStrMtableInfo.setInputId(reqDto.getInputId());
                //登记机构
                apprStrMtableInfo.setInputBrId(reqDto.getInputBrId());
                //登记日期
                apprStrMtableInfo.setInputDate(reqDto.getInputDate());
                //添加主键
                apprStrMtableInfo.setPkId(pkId);
                //插入批复主表信息
                logger.info("CmisLmt0003 批复台账编号【"+accNo+"】，不是新批复覆盖老批复，不存在原批复主信息，新增批复主信息");
                apprStrMtableInfoMapper.insert(apprStrMtableInfo) ;
            }
        }

        /**************处理分项信息********************/
        //利用工厂接口创建实例
        Supplier<ApprLmtSubBasicInfo> supplier = ApprLmtSubBasicInfo::new ;

        Com001ReqDto com001ReqDto = new Com001ReqDto();
        Data com001ReqDtoData = new Data();
        java.util.List<List> com001ReqLists = new ArrayList<>();

        //计数
        int count = 0;

        java.util.List<CmisLmt0003LmtSubListReqDto> lmtSubList =  reqDto.getLmtSubList();

        for (CmisLmt0003LmtSubListReqDto lmtSub: lmtSubList) {
            //新分项编号
            String accSubNo = lmtSub.getAccSubNo();
            //获取分项批复实例对象
            ApprLmtSubBasicInfo apprLmtSubBasicInfo = supplier.get();
            //拷贝数据
            BeanUtils.copyProperties(lmtSub, apprLmtSubBasicInfo);
            apprLmtSubBasicInfo.setGrpNo(comm4Service.selectGrpNoByCusId(reqDto.getCusId()));
            //更新人
            apprLmtSubBasicInfo.setUpdId(reqDto.getInputId());
            //更新机构
            apprLmtSubBasicInfo.setUpdBrId(reqDto.getInputBrId());
            //更新时间
            apprLmtSubBasicInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            //更新日期
            apprLmtSubBasicInfo.setUpdDate(reqDto.getInputDate());
            //到期日处理
            apprLmtSubBasicInfo.setLmtDate(lmtSub.getEndDate());

            //如果是新增授信分项
            //原批复分项编号
            String origiAccSubNo = lmtSub.getOrigiAccSubNo();
            //新批复分项编号
            String newAccSubNo = lmtSub.getAccSubNo();
            if(StringUtils.isNotEmpty(origiAccSubNo)) {
                //如果是同业管理类额度，则将同业管理类额度下 货币基金的占用关系  挂到新分项项下
                if (CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_3006.equals(lmtSub.getLimitSubNo()) &&
                        CmisLmtConstants.STD_ZB_YES_NO_Y.equals(lmtSub.getIsIvlMf())) {
                    //原货币基金额度编号
                    String oriApprSubSerno = origiAccSubNo + "01" ;
                    String newApprSubSerno = newAccSubNo + "01" ;
                    logger.info("CmisLmt0003 额度续作或额度复议批复台账编号【"+accNo+"】，原批复分项编号【"+origiAccSubNo+"】，将同业管理类额度下货币基金的占用关系挂到新分项项下");

                    //货币基金额度生成原额度向下业务挂靠到新业务向下，需要重新计算分项已用额度，可用额度，用信余额，用信总额，可处长金额，出账金额
                    createIvlMfLmtInfo(apprLmtSubBasicInfo, lmtSub, reqDto);
                    logger.info("CmisLmt0003 批复台账编号【"+accNo+"】，原批复分项编号【"+origiAccSubNo+"】，将同业管理类额度下货币基金的占用关系挂到新分项项下");
                    //查询原分项下未结清的业务，将未结清业务挂靠到新的分项下
                    java.util.List<LmtContRel> lmtContRelList = lmtContRelService.selectOutStandingLoadCont(origiAccSubNo) ;
                    //原分项批复下的业务挂到新分项下
                    if(CollectionUtils.nonEmpty(lmtContRelList)){
                        for (LmtContRel lmtContRel : lmtContRelList) {
                            comm4Service.updateContAccRelLmtCont(lmtContRel.getDealBizNo(), newApprSubSerno);
                            //失效向下占用关系
                            lmtContRel.setBizTotalBalanceAmtCny(BigDecimal.ZERO);
                            lmtContRel.setBizSpacBalanceAmtCny(BigDecimal.ZERO);
                            lmtContRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_300);
                            lmtContRelService.update(lmtContRel) ;
                        }
                    }

                    //获取原额度对象，更改为失效已结清
                    ApprStrMtableInfo apprSMInfo = apprStrMtableInfoService.selectByAppSerno(oriApprSubSerno) ;
                    apprSMInfo.setApprStatus(CmisLmtConstants.STD_ZB_APPR_ST_99);
                    apprStrMtableInfoService.update(apprSMInfo) ;
                    //获取原分项对象
                    ApprLmtSubBasicInfo oriApprIvlMf = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(oriApprSubSerno) ;
                    if( oriApprIvlMf!=null ){
                        //获取新的额度分项信息
                        ApprLmtSubBasicInfo apprIvlMf = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(oriApprSubSerno) ;
                        //用信余额
                        BigDecimal loanBalance = comm4LmtCalFormulaUtils.getLoanBalance(oriApprIvlMf, apprIvlMf) ;
                        //
                        BigDecimal loanSpacBalance = comm4LmtCalFormulaUtils.getloanSpacBalance(oriApprIvlMf, apprIvlMf) ;
                        BigDecimal pvpOutstndAmt = comm4LmtCalFormulaUtils.getPvpOutstndAmt(oriApprIvlMf, apprIvlMf) ;
                        BigDecimal avlOutstndAmt = comm4LmtCalFormulaUtils.getAvlOutstndAmt(oriApprIvlMf, apprIvlMf) ;
                        apprIvlMf.setLoanBalance(loanBalance);
                        apprIvlMf.setLoanSpacBalance(loanSpacBalance);
                        apprIvlMf.setPvpOutstndAmt(pvpOutstndAmt);
                        apprIvlMf.setAvlOutstndAmt(avlOutstndAmt);
                        apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfo, resultMap) ;

                        //更改原批复状态为 99-失效已结清
                        oriApprIvlMf.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_99);
                        logger.info("CmisLmt0003 批复台账编号【"+accNo+"】，原批复分项编号【"+origiAccSubNo+"】，将批复分项编号【"+oriApprSubSerno+"】的批复状态为 99-失效已结清");
                        apprLmtSubBasicInfoService.update(oriApprIvlMf) ;
                    }
                }

                logger.info("CmisLmt0003 批复台账编号【"+accNo+"】，获取原批复分项编号【"+origiAccSubNo+"】的批复分项信息");
                //获取原分项对象
                ApprLmtSubBasicInfo oriApprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(origiAccSubNo) ;
                //查询原分项下未结清的业务，将未结清业务挂靠到新的分项下
                comm4Service.updateLmtContRelRelSubNo(origiAccSubNo, newAccSubNo);

                //更改原批复状态为 99-失效已结清
                oriApprLmtSubBasicInfo.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_99);
                logger.info("CmisLmt0003 批复台账编号【"+accNo+"】，将原批复分项编号【"+origiAccSubNo+"】的批复状态为 99-失效已结清");
                apprLmtSubBasicInfoService.update(oriApprLmtSubBasicInfo) ;

                //生成新批复分项信息
                logger.info("CmisLmt0003 批复台账编号【"+accNo+"】，原批复分项编号【"+origiAccSubNo+"】，生成新批复分项信息开始");
                initApprLmtSubBasicInfo(apprLmtSubBasicInfo,lmtSub,reqDto);
                apprLmtSubBasicInfo.setOutstndAmt(oriApprLmtSubBasicInfo.getOutstndAmt());
                apprLmtSubBasicInfo.setSpacOutstndAmt(oriApprLmtSubBasicInfo.getSpacOutstndAmt());
                apprLmtSubBasicInfo.setLoanSpacBalance(oriApprLmtSubBasicInfo.getLoanSpacBalance());
                apprLmtSubBasicInfo.setLoanSpacBalance(oriApprLmtSubBasicInfo.getLoanSpacBalance());
                apprLmtSubBasicInfo.setPvpOutstndAmt(oriApprLmtSubBasicInfo.getPvpOutstndAmt());
                apprLmtSubBasicInfo.setAvlOutstndAmt(NumberUtils.nullDefaultZero(lmtSub.getAvlamt()).subtract(
                        NumberUtils.nullDefaultZero(oriApprLmtSubBasicInfo.getPvpOutstndAmt())));
                apprLmtSubBasicInfoService.insert(apprLmtSubBasicInfo) ;
                logger.info("CmisLmt0003 批复台账编号【"+accNo+"】，原批复分项编号【"+origiAccSubNo+"】，生成新批复分项信息结束");
            }else {
                ApprLmtSubBasicInfo apprLmtSubBasicInfoOld = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(accSubNo) ;
                if(apprLmtSubBasicInfoOld!=null){
                    logger.info("CmisLmt0003 批复台账编号【"+accNo+"】，根据批复分项编号【"+accSubNo+"】查询到了批复分项信息");
                    //变更或删除
                    if (CmisLmtConstants.OPR_TYPE_ADD.equals(lmtSub.getOprType())) {
                        //变更
                        apprLmtSubBasicInfoOld.setStatus(lmtSub.getAccSubStatus());
                        apprLmtSubBasicInfoOld.setLmtDate(lmtSub.getEndDate());
                        apprLmtSubBasicInfoOld.setAvlAmt(lmtSub.getAvlamt());
                        apprLmtSubBasicInfoOld.setStartDate(lmtSub.getStartDate());
                        apprLmtSubBasicInfoOld.setTerm(lmtSub.getTerm());
                        apprLmtSubBasicInfoOld.setCurType(lmtSub.getCurType());
                        apprLmtSubBasicInfoOld.setIsRevolv(lmtSub.getIsRevolv());
                        apprLmtSubBasicInfoOld.setGrpNo(comm4Service.selectGrpNoByCusId(reqDto.getCusId()));
                        //更新人
                        apprLmtSubBasicInfoOld.setUpdId(reqDto.getInputId());
                        //更新机构
                        apprLmtSubBasicInfoOld.setUpdBrId(reqDto.getInputBrId());
                        //更新时间
                        apprLmtSubBasicInfoOld.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                        //更新日期
                        apprLmtSubBasicInfoOld.setUpdDate(reqDto.getInputDate());

                        if (CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_3006.equals(lmtSub.getLimitSubNo()) &&
                                CmisLmtConstants.STD_ZB_YES_NO_Y.equals(lmtSub.getIsIvlMf())) {
                            BigDecimal lmtMfAmt = BigDecimalUtil.replaceNull(lmtSub.getLmtMfAmt()) ;
                            BigDecimal lmtSingleMfAmt = BigDecimalUtil.replaceNull(lmtSub.getLmtSingleMfAmt()) ;

                            //获取原货币基金额度
                            ApprLmtSubBasicInfo lmtMfInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(newAccSubNo + "01") ;
                            //原货币基金金额
                            BigDecimal oriLmtMfAmt = BigDecimalUtil.replaceNull(lmtMfInfo.getAvlAmt()) ;
                            //可出账金额计算
                            BigDecimal pvpOutstndAmt = BigDecimalUtil.replaceNull(lmtMfInfo.getPvpOutstndAmt()) ;
                            BigDecimal avlOutstndAmt = comm4LmtCalFormulaUtils.getAvlOutstndAmt(pvpOutstndAmt, lmtMfAmt) ;
                            //获取原货币基金额度
                            lmtMfInfo.setGrpNo(comm4Service.selectGrpNoByCusId(reqDto.getCusId()));
                            lmtMfInfo.setAvlAmt(lmtMfAmt);
                            lmtMfInfo.setSpacAmt(lmtMfAmt);
                            lmtMfInfo.setLmtSingleMfAmt(lmtSingleMfAmt);
                            lmtMfInfo.setAvlOutstndAmt(avlOutstndAmt);
                            lmtMfInfo.setLmtDate(lmtSub.getEndDate());
                            lmtMfInfo.setStartDate(lmtSub.getStartDate());
                            lmtMfInfo.setTerm(lmtSub.getTerm());
                            lmtMfInfo.setCurType(lmtSub.getCurType());
                            lmtMfInfo.setIsRevolv(lmtSub.getIsRevolv());
                            //更新人
                            lmtMfInfo.setUpdId(reqDto.getInputId());
                            //更新机构
                            lmtMfInfo.setUpdBrId(reqDto.getInputBrId());
                            //更新时间
                            lmtMfInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                            //更新日期
                            lmtMfInfo.setUpdDate(reqDto.getInputDate());
                            apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(lmtMfInfo, resultMap) ;

                            //跟新额度占用关系
                            java.util.List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelByDealBizNo(newAccSubNo + "01") ;
                            if(CollectionUtils.nonEmpty(lmtContRelList)){
                                LmtContRel lmtContRel = lmtContRelList.get(0) ;
                                if(lmtMfAmt.compareTo(lmtContRel.getBizSpacAmtCny())!=0){
                                    lmtContRel.setBizAmt(lmtMfAmt);
                                    lmtContRel.setBizSpacAmtCny(lmtMfAmt);
                                    lmtContRel.setBizSpacBalanceAmtCny(lmtMfAmt);
                                    lmtContRel.setBizTotalAmtCny(lmtMfAmt);
                                    lmtContRel.setBizTotalBalanceAmtCny(lmtMfAmt);
                                    lmtContRelService.updateByPKeyInApprLmtChgDetails(lmtContRel, resultMap) ;
                                }
                            }

                            //计算新老货币基金之间的差额
                            BigDecimal diffAmt = comm4LmtCalFormulaUtils.getDiffAmt(lmtMfAmt, oriLmtMfAmt) ;
                            apprLmtSubBasicInfoOld.setSpacOutstndAmt(apprLmtSubBasicInfoOld.getSpacOutstndAmt().add(diffAmt));
                            apprLmtSubBasicInfoOld.setOutstndAmt(apprLmtSubBasicInfoOld.getOutstndAmt().add(diffAmt));
                        }
                        logger.info("CmisLmt0003 批复台账编号【"+accNo+"】，更新批复分项编号【"+accSubNo+"】的批复分项信息");
                        apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfoOld, resultMap);
                    }else if (CmisLmtConstants.OPR_TYPE_DELETE.equals(lmtSub.getOprType())) {
                        //删除之前判断是否有占用
                        QueryModel queryModel = new QueryModel();
                        queryModel.addCondition("limitSubNo",lmtSub.getAccSubNo());

                        if (lmtContRelMapper.selectLmtContRelList(queryModel).size()>0){
                            throw new YuspException(EclEnum.ECL070023.key, EclEnum.ECL070023.value);
                        }
                        //对要删除的额度分项进行逻辑删除
                        logger.info("CmisLmt0003 批复台账编号【"+accNo+"】，逻辑删除批复分项编号【"+accSubNo+"】的批复分项信息");
                        apprLmtSubBasicInfoMapper.updateOprTypeByApprSubSerno(lmtSub.getAccSubNo());
                    }
                }else{
                    logger.info("CmisLmt0003 批复台账编号【"+accNo+"】，根据批复分项编号【"+accSubNo+"】未查询到批复分项信息，新增批复分项信息");
                    if (CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_3006.equals(lmtSub.getLimitSubNo()) &&
                            CmisLmtConstants.STD_ZB_YES_NO_Y.equals(lmtSub.getIsIvlMf())) {
                        createIvlMfLmtInfo(apprLmtSubBasicInfo, lmtSub, reqDto);
                    }
                    initApprLmtSubBasicInfo(apprLmtSubBasicInfo,lmtSub,reqDto);
                    apprLmtSubBasicInfoService.insert(apprLmtSubBasicInfo) ;
                }
            }

            /**
             * 处理发送ComStar交易报文：
             * 3001--同业融资类额度 3005-同业交易类额度 3006-同业管理类额度
             *判断同业管理类额度，是否涉及货币基金额度，若涉及则将货币基金额度推送至ComStar，不涉及货币基金，则无需推送
             *单只货币基金授信额度	lmtSingleMfAmt   货币基金总授信额度lmtMfAmt
             *
             * 同业融资类额度和同业交易类额度需要扣减掉 项下产品授信占用余额后，推送至ComStar
             *modify by zhangjw 20210519
             */
            if (CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_3001.equals(lmtSub.getLimitSubNo()) ||
                    CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_3005.equals(lmtSub.getLimitSubNo()) ||
                    (CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_3006.equals(lmtSub.getLimitSubNo()) && CmisLmtConstants.STD_ZB_YES_NO_Y.equals(lmtSub.getIsIvlMf()))
            ) {

                logger.info("CmisLmt0003 批复台账编号【"+accNo+"】，批复分项编号【"+accSubNo+"】，组织报文发生comstar");

                List com001ReqDtoList = new List();
                count++;
                //序号
                com001ReqDtoList.setNumber(String.valueOf(count));
                //授信批复编号
                com001ReqDtoList.setOrigiAccNo(reqDto.getOrigiAccNo());
                //授信分项ID
                com001ReqDtoList.setLmtSubNo(lmtSub.getLimitSubNo());
                //Ecif客户号
                com001ReqDtoList.setCusId(reqDto.getCusId());

                logger.info("CmisLmt0003 批复台账编号【"+accNo+"】，批复分项编号【"+accSubNo+"】，调用客户管理API，获取同业客户信息");
                CmisCus0010ReqDto cmisCus0010ReqDto = new CmisCus0010ReqDto();
                cmisCus0010ReqDto.setCusId(reqDto.getCusId());
                ResultDto<CmisCus0010RespDto> cmisCus0010RespDtoResultDto = cmisCusClientService.cmiscus0010(cmisCus0010ReqDto);
                CmisCus0010RespDto cus0010RespDto = cmisCus0010RespDtoResultDto.getData();

                if (cus0010RespDto!=null){
                    //授信主体证件类型	 certType
                    com001ReqDtoList.setCertType(cus0010RespDto.getCertType());
                    //授信主体证件号码	certCode
                    com001ReqDtoList.setCertCode(cus0010RespDto.getCertCode());
                }

                //授信主体客户名称	cusName
                com001ReqDtoList.setCusName(reqDto.getCusName());
                //额度到期日
                String endDate = lmtSub.getEndDate() ;
                if(StringUtils.isNotEmpty(endDate)){
                    endDate = endDate.replace("-","") ;
                }
                com001ReqDtoList.setEndDate(endDate);
                if(CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_3006.equals(apprLmtSubBasicInfo.getLimitSubNo())
                        && CmisLmtConstants.STD_ZB_YES_NO_Y.equals(lmtSub.getIsIvlMf())){
                    //额度品种编号
                    com001ReqDtoList.setLimitSubNo(CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_300601);
                    //额度品种名称
                    com001ReqDtoList.setLimitSubName(CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_NAME_300601);
                }else{
                    //额度品种编号
                    com001ReqDtoList.setLimitSubNo(lmtSub.getLimitSubNo());
                    //额度品种名称
                    com001ReqDtoList.setLimitSubName(lmtSub.getLimitSubName());
                }

                //额度类型	lmtType 默认 01-同业综合授信额度
                com001ReqDtoList.setLmtType(CmisLmtConstants.COMSTAR_LMT_TYPE_01);
                //操作类型	optType 默认 01-调整
                com001ReqDtoList.setOptType(CmisLmtConstants.COMSTAR_OPT_TYPE_01);

                if(CmisLmtConstants.OPR_TYPE_ADD.equals(lmtSub.getOprType())){
                    //货币基金直接将货币基金金额同步
                    if(CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_3006.equals(lmtSub.getLimitSubNo()) ){
                        com001ReqDtoList.setLmtAmt(lmtSub.getLmtMfAmt());//货币基金授信总金额 为 货币基金分项金额
                        com001ReqDtoList.setLmtSingleMfAmt(lmtSub.getLmtSingleMfAmt());//单只货币基金金额
                    }else{
                        //计算授信总额，原授信总额-资金占用分项额度
                        QueryModel queryModel = new QueryModel();
                        queryModel.addCondition("limitSubNo", lmtSub.getAccSubNo());
                        //资金占用分项额度
                        BigDecimal sumBizTotalAmtCny = lmtContRelMapper.getSumBizTotalAmtCnyByLimitSubNo(queryModel).get("bizTotalBalanceAmtCny");
                        com001ReqDtoList.setLmtAmt(lmtSub.getAvlamt().subtract(sumBizTotalAmtCny));
                    }
                }else{
                    //删除额度时，授信金额默认为0
                    com001ReqDtoList.setLmtAmt(BigDecimal.ZERO);
                }
                com001ReqLists.add(com001ReqDtoList);
            }
        }

        com001ReqDtoData.setList(com001ReqLists);
        com001ReqDto.setData(com001ReqDtoData);

        //同业额度信息发comstar
        if (!com001ReqLists.isEmpty()){
            //交易流水号	serno
            Map seqMap = new HashMap();
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, seqMap);
            com001ReqDto.setSerno(serno);
            //金融机构代码
            com001ReqDto.setInstuCde(reqDto.getInstuCde());
            //发comstar
            logger.info("CmisLmt0003Service 调用Com001接口，同步额度信息到Comstar:"+JSON.toJSONString(com001ReqDto));
            Com001RespDto com001RespDto = com001Service.Com001(com001ReqDto);
            logger.info(this.getClass().getName()+",调用Com001接口，同步额度信息到Comstar结束------>:"+ JSON.toJSONString(com001RespDto));

            if (!SuccessEnum.SUCCESS.key.equals(com001RespDto.getErrorCode())){
                throw new YuspException(EclEnum.ECL070075.key, EclEnum.ECL070075.value+",错误信息："+com001RespDto.getErrorMsg());
            }
        }
        resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
        resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0003.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0003.value);
        return resqDto;
    }

    /**
     * 1.检查原批复编号是否存在，不存在抛异常
     * 2.检查新批复编号是否存在，存在则抛异常
     * 3.
     * @param reqDto
     */
    private void checkInfoIsVaild(CmisLmt0003ReqDto reqDto){
        //1.检查新的批复台账编号是否存在，已存在则抛异常
        if(apprStrMtableInfoService.valiApprIsExistsBySerno(reqDto.getAccNo())){
            throw new YuspException(EclEnum.ECL070021.key, EclEnum.ECL070021.value);
        }

        //2.检查原批复台账编号是否存在，不存在则抛异常
        if(!apprStrMtableInfoService.valiApprIsExistsBySerno(reqDto.getOrigiAccNo())){
            throw new YuspException(EclEnum.ECL070016.key, EclEnum.ECL070016.value);
        }

        //3.检查新的批复分项编号是否存在，已存在则抛异常;检查原批复分项编号是否存在，不存在则抛异常
        java.util.List<CmisLmt0003LmtSubListReqDto> lmtSubList = reqDto.getLmtSubList();

        if (!lmtSubList.isEmpty()){
            java.util.List<String> accSubNoList = new ArrayList<String>();
            java.util.List<String> origiAccSubNoList = new ArrayList<String>();

            for (CmisLmt0003LmtSubListReqDto lmtSub : lmtSubList) {
                accSubNoList.add(lmtSub.getAccSubNo());
                origiAccSubNoList.add(lmtSub.getOrigiAccSubNo());
            }

            String apprSubSernos = apprLmtSubBasicInfoService.selectAccSubNoByArray(accSubNoList);

            if (StringUtils.isNotEmpty(apprSubSernos)){
                throw new YuspException(EclEnum.ECL070022.key, "额度同步，批复分项["+apprSubSernos+"]已经存在，不允许重新生成");
            }

//            apprSubSernos = apprLmtSubBasicInfoMapper.selectAccSubNoByArray(origiAccSubNoList);
//
//            if (StringUtils.isEmpty(apprSubSernos)){
//                throw new YuspException(EclEnum.ECL070007.key, "未找到额度分项["+apprSubSernos+"]的信息");
//            }
        }
    }

    /**
     * 初始化批复额度分项数信息
     * @param apprLmtSubBasicInfo
     * @param lmtSub
     * @param reqDto
     */
    private void initApprLmtSubBasicInfo(ApprLmtSubBasicInfo apprLmtSubBasicInfo,CmisLmt0003LmtSubListReqDto lmtSub,CmisLmt0003ReqDto reqDto){
        logger.info("CmisLmt0003 初始化批复分项编号的批复分项信息---------- 开始");
        //批复台账编号
        String accSubNo = lmtSub.getAccSubNo();
        apprLmtSubBasicInfo.setApprSubSerno(accSubNo) ;
        apprLmtSubBasicInfo.setCusId(reqDto.getCusId()) ;
        apprLmtSubBasicInfo.setCusName(reqDto.getCusName()) ;
        apprLmtSubBasicInfo.setCusType(reqDto.getCusType()) ;
        //可用总额取授信总额
        apprLmtSubBasicInfo.setAvlAmt(lmtSub.getAvlamt());
        //敞口可用额度取授信总额
        apprLmtSubBasicInfo.setSpacAmt(lmtSub.getAvlamt());
        //用信余额默认为0
        apprLmtSubBasicInfo.setLoanBalance(BigDecimal.ZERO);
        //已用总额默认为0
        apprLmtSubBasicInfo.setOutstndAmt(BigDecimal.ZERO);
        //敞口已用额度默认为0
        apprLmtSubBasicInfo.setSpacOutstndAmt(BigDecimal.ZERO);
        //是否低风险授信 默认 N-否
        apprLmtSubBasicInfo.setIsLriskLmt(CmisLmtConstants.YES_NO_N);
        //授信分项类型 默认 01-额度分项
        apprLmtSubBasicInfo.setLmtSubType(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_01);
        //额度类型 默认 01-综合
        apprLmtSubBasicInfo.setLimitType(CmisLmtConstants.STD_ZB_LIMIT_TYPE_01);
        //已出帐金额 默认为0
        apprLmtSubBasicInfo.setPvpOutstndAmt(BigDecimal.ZERO);
        //可出账金额 默认为授信总额
        apprLmtSubBasicInfo.setAvlOutstndAmt(lmtSub.getAvlamt());
        //是否预授信 默认为 N-否
        apprLmtSubBasicInfo.setIsPreCrd(CmisLmtConstants.YES_NO_N);
        //授信总额累加 默认为0
        apprLmtSubBasicInfo.setLmtAmtAdd(BigDecimal.ZERO);
        //用信敞口余额 默认为授信总额
        apprLmtSubBasicInfo.setLoanSpacBalance(BigDecimal.ZERO);
        //登记人
        apprLmtSubBasicInfo.setInputId(reqDto.getInputId());
        //登记机构
        apprLmtSubBasicInfo.setInputBrId(reqDto.getInputBrId());
        //登记时间
        apprLmtSubBasicInfo.setInputDate(reqDto.getInputDate());
        //创建时间
        apprLmtSubBasicInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //生成主键
        String subPkId = getPkId();
        //设置主键
        apprLmtSubBasicInfo.setPkId(subPkId);
        //外键取批复台账编号
        apprLmtSubBasicInfo.setFkPkid(reqDto.getAccNo());
        //到期日
        apprLmtSubBasicInfo.setLmtDate(lmtSub.getEndDate());
        //状态
        apprLmtSubBasicInfo.setStatus(lmtSub.getAccSubStatus());

        /**
         * add by zhangjw 20210519
         * 增加逻辑：
         * 判断是否为同业管理类额度，并且同业管理类额度是否包含货币基金额度
         * 若包含，则生成同业管理类额度项下二级 货币基金分项 额度品种编号300601  分项台账编号为同业管理类额度分项编号+“01”
         * update by lizx 202109925
         * 修改以上逻辑：
         * 判断是同业管理类额度，并且包含货币基金，若包含，则从原先的二级分项更改为自动生成一套货币基金额度，并且占用同业管理类额度
         * 即:1.同业管理额度分项下，新增一条lmt_cont_rel 记录，业务属性 3-授信 ，合同类型为1-一般合同。更新同业管理类已用金额和敞口金额
         *    2.生成货币基金额度，即生成 appr_str_mtable_info appr_lmt_sub_basic_info 数据信息
         */
        String limitSubNo = apprLmtSubBasicInfo.getLimitSubNo();
        String isIvlMf = lmtSub.getIsIvlMf();
        if(CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_3006.equals(limitSubNo) && CmisLmtConstants.STD_ZB_YES_NO_Y.equals(isIvlMf)){
            //更新同业管理类已出账金额，和可出账金额
            apprLmtSubBasicInfo.setOutstndAmt(apprLmtSubBasicInfo.getOutstndAmt().add(lmtSub.getLmtMfAmt()));
            apprLmtSubBasicInfo.setSpacOutstndAmt(apprLmtSubBasicInfo.getSpacOutstndAmt().add(lmtSub.getLmtMfAmt()));
        }
        logger.info("CmisLmt0003 初始化批复分项编号【"+accSubNo+"】的批复分项信息---------- 结束");
    }

    /**
     * @作者:lizx
     * @方法名称: createIvlMfLmtInfo
     * @方法描述: 同业管理类额度设计到货币基金，生成货币金额额度信息，以及占用同业管理类额度
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/9/25 15:40
     * @param apprLmtSubBasicInfo:
     * @param lmtSub:
     * @param reqDto:
     * @return: void
     * @算法描述: 无
    */
    private void createIvlMfLmtInfo(ApprLmtSubBasicInfo apprLmtSubBasicInfo, CmisLmt0003LmtSubListReqDto lmtSub, CmisLmt0003ReqDto reqDto) {
        String accSubNo = lmtSub.getAccSubNo();
        //分项编号，台账编号，额度占用交易流水号
        String dealBizNo = accSubNo + "01" ;
        //处理主表数据信息
        ApprStrMtableInfo apprStrMtableInfo = apprStrMtableInfoService.selectByAppSerno(reqDto.getAccNo()) ;
        ApprStrMtableInfo apprStrIvlMf = new ApprStrMtableInfo() ;
        BeanUtils.copyProperties(apprStrMtableInfo,apprStrIvlMf);
        apprStrIvlMf.setPkId(getPkId());
        apprStrIvlMf.setApprSerno(dealBizNo);
        apprStrIvlMf.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_04);
        apprStrIvlMf.setLmtMode(CmisLmtConstants.STD_ZB_LMT_MODE_02);
        //插入货币基金主额度信息
        apprStrMtableInfoService.insert(apprStrIvlMf) ;

        //处理分项信息
        ApprLmtSubBasicInfo apprIvlMf = new ApprLmtSubBasicInfo();
        BeanUtils.copyProperties(lmtSub,apprIvlMf);
        apprIvlMf.setPkId(getPkId());//设置主键
        apprIvlMf.setApprSubSerno(dealBizNo);
        //额度产品
        apprIvlMf.setLimitSubNo(CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_300601);
        apprIvlMf.setLimitSubName(CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_NAME_300601);
        //上级分项默认为空
        apprIvlMf.setParentId("");
        apprIvlMf.setIsIvlMf(CmisLmtConstants.STD_ZB_YES_NO_N);
        //货币基金默认专项
        apprIvlMf.setLimitType(CmisLmtConstants.STD_ZB_LIMIT_TYPE_02);
        //货币基金单独处理
        apprIvlMf.setAvlAmt(lmtSub.getLmtMfAmt());
        apprIvlMf.setSpacAmt(lmtSub.getLmtMfAmt());
        apprIvlMf.setLmtSingleMfAmt(lmtSub.getLmtSingleMfAmt());
        apprIvlMf.setAvlOutstndAmt(lmtSub.getLmtMfAmt());
        apprIvlMf.setFkPkid(dealBizNo);
        logger.info("CmisLmt0003 批复分项编号【"+ accSubNo +"】,生成同业管理类额度项下货币基金分项");
        apprIvlMf.setCusId(reqDto.getCusId());
        apprIvlMf.setCusName(reqDto.getCusName());
        apprIvlMf.setCusType(reqDto.getCusType());
        apprIvlMf.setStartDate(reqDto.getStartDate());
        apprIvlMf.setLmtDate(reqDto.getEndDate());
        apprIvlMf.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
        apprIvlMf.setIsIvlMf(lmtSub.getIsIvlMf());
        apprIvlMf.setIsLriskLmt(CmisLmtConstants.YES_NO_N);
        apprIvlMf.setIsRevolv(lmtSub.getIsRevolv());
        apprIvlMf.setLimitType(CmisLmtConstants.STD_ZB_LIMIT_TYPE_02);
        apprIvlMf.setLmtSubType(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_01);
        apprIvlMf.setSpacOutstndAmt(BigDecimal.ZERO);
        apprIvlMf.setOutstndAmt(BigDecimal.ZERO);
        apprIvlMf.setLoanBalance(BigDecimal.ZERO);
        apprIvlMf.setLoanSpacBalance(BigDecimal.ZERO);
        logger.info("CmisLmt0003 批复分项编号【"+ accSubNo +"】,生成同业管理类额度项下货币基金分项");
        apprLmtSubBasicInfoService.insert(apprIvlMf);

        /**************************货币基金占用同业管理类额度*********************************/
        logger.info("{}初始化合同信息------------------start", DscmsLmtEnum.TRADE_CODE_CMISLMT0003.key);
        LmtContRel lmtContRel = new LmtContRel() ;
        //主键
        lmtContRel.setPkId(comm4Service.generatePkId());
        //客户编号
        lmtContRel.setCusId(reqDto.getCusId());
        //客户名称
        lmtContRel.setCusName(reqDto.getCusName());
        //交易业务编号
        lmtContRel.setDealBizNo(dealBizNo);
        //额度分项编号
        lmtContRel.setLimitSubNo(accSubNo);
        //系统编号
        lmtContRel.setSysId(reqDto.getSysId());
        //授信类型
        lmtContRel.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_04);
        //资产编号
        lmtContRel.setAssetNo("");
        //交易业务类型
        lmtContRel.setDealBizType(CmisLmtConstants.DEAL_BIZ_TYPE_1);
        //业务属性
        lmtContRel.setBizAttr(CmisLmtConstants.STD_ZB_BIZ_ATTR_3);
        //产品编号
        lmtContRel.setPrdId(CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_300601);
        //产品名称
        lmtContRel.setPrdName(CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_NAME_300601);
        //业务金额
        BigDecimal dealBizAmt = BigDecimalUtil.replaceNull(lmtSub.getLmtMfAmt()) ;
        lmtContRel.setBizAmt(dealBizAmt);
        //占用总金额（折人民币）
        lmtContRel.setBizTotalAmtCny(dealBizAmt);
        //占用总余额（折人民币）
        lmtContRel.setBizTotalBalanceAmtCny(dealBizAmt);
        //占用敞口余额（折人民币）
        lmtContRel.setBizSpacAmtCny(dealBizAmt);
        //占用敞口金额（折人民币）
        lmtContRel.setBizSpacBalanceAmtCny(dealBizAmt);
        //业务保证金比例
        lmtContRel.setSecurityAmt(BigDecimal.ZERO);
        //业务保证金金额
        lmtContRel.setSecurityRate(BigDecimal.ZERO);
        //合同起始日
        lmtContRel.setStartDate(apprLmtSubBasicInfo.getStartDate());
        //合同到期日
        lmtContRel.setEndDate(apprLmtSubBasicInfo.getLmtDate());
        //交易业务状态
        lmtContRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_200);
        //操作类型
        lmtContRel.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        //登记人
        lmtContRel.setInputId(reqDto.getInputId());
        //登记机构
        lmtContRel.setInputBrId(reqDto.getInputBrId());
        //登记时间
        lmtContRel.setInputDate(reqDto.getInputDate());
        //最近更新人
        lmtContRel.setUpdId(reqDto.getInputId());
        //最近更新机构
        lmtContRel.setUpdBrId(reqDto.getInputBrId());
        //最近更新日期
        lmtContRel.setUpdDate(reqDto.getInputDate());
        //创建时间
        lmtContRel.setCreateTime(DateUtils.getCurrTimestamp());
        //修改时间
        lmtContRel.setUpdateTime(DateUtils.getCurrTimestamp());
        lmtContRelService.insert(lmtContRel);
        logger.info("{}初始合同信息------------------end,合同信息-{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0003.key, JSON.toJSONString(lmtContRel));
    }

    /**
     * 生成主键
     * @return 返回主键
     */
    private String getPkId(){
        Map paramMap= new HashMap<>() ;
        return sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, paramMap);
    }
}