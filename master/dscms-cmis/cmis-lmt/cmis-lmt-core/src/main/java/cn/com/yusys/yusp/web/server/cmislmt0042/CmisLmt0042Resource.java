package cn.com.yusys.yusp.web.server.cmislmt0042;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0042.req.CmisLmt0042ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0042.resp.CmisLmt0042RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0042.CmisLmt0042Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据额度品种编号获取产品扩展属性
 *
 * @author zhangjw 2021/7/14
 * @version 1.0
 */
@Api(tags = "cmislmt0042:根据额度品种编号获取产品扩展属性")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0042Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0042Resource.class);

    @Autowired
    private CmisLmt0042Service cmisLmt0042Service;
    /**
     * 交易码：cmislmt0042
     * 交易描述：根据额度品种编号获取产品扩展属性
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("根据额度品种编号获取产品扩展属性")
    @PostMapping("/cmislmt0042")
    protected @ResponseBody
    ResultDto<CmisLmt0042RespDto> cmisLmt0042(@Validated @RequestBody CmisLmt0042ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0042.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0042.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0042RespDto> cmisLmt0042RespDtoResultDto = new ResultDto<>();
        CmisLmt0042RespDto cmisLmt0042RespDto = new CmisLmt0042RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0042RespDto = cmisLmt0042Service.execute(reqDto);
            cmisLmt0042RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0042RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);

        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0042.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0042.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0042.value, e.getMessage());
            // 封装xddb0042DataResultDto中异常返回码和返回信息
            cmisLmt0042RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0042RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0016RespDto到cmisLmt0016RespDtoResultDto中
        cmisLmt0042RespDtoResultDto.setData(cmisLmt0042RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0042.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0042.value, JSON.toJSONString(cmisLmt0042RespDtoResultDto));
        return cmisLmt0042RespDtoResultDto;
    }
}
