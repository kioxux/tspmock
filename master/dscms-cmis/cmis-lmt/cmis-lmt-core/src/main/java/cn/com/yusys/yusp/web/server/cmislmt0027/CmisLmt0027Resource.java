package cn.com.yusys.yusp.web.server.cmislmt0027;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0027.req.CmisLmt0027ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0027.resp.CmisLmt0027RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0027.CmisLmt0027Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:额度分项信息查询
 *
 * @author dumd 20210609
 * @version 1.0
 */
@Api(tags = "cmislmt0027:合作方客户额度查询")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0027Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0027Resource.class);

    @Autowired
    private CmisLmt0027Service cmisLmt0027Service;

    /**
     * 交易码：CmisLmt0027
     * 交易描述：合作方客户额度查询
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("合作方客户额度查询")
    @PostMapping("/cmislmt0027")
    protected @ResponseBody
    ResultDto<CmisLmt0027RespDto> CmisLmt0027(@Validated @RequestBody CmisLmt0027ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0027.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0027.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0027RespDto> CmisLmt0027RespDtoResultDto = new ResultDto<>();
        CmisLmt0027RespDto CmisLmt0027RespDto = new CmisLmt0027RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0027RespDto = cmisLmt0027Service.execute(reqDto);
            CmisLmt0027RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0027RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0027.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0027.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0027.value, e.getMessage());
            // 封装CmisLmt0027RespDtoResultDto中异常返回码和返回信息
            CmisLmt0027RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0027RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0027RespDto到CmisLmt0027RespDtoResultDto中
        CmisLmt0027RespDtoResultDto.setData(CmisLmt0027RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0027.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0027.value, JSON.toJSONString(CmisLmt0027RespDtoResultDto));
        return CmisLmt0027RespDtoResultDto;
    }
}