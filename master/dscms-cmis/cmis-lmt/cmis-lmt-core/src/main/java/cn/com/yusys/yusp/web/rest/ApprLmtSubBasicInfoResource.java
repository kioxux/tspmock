/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.dto.CusGrpMemberTreeClientDto;
import cn.com.yusys.yusp.service.ICusClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprLmtSubBasicInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: LQC
 * @创建时间: 2021-04-01 20:40:32
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/apprlmtsubbasicinfo")
public class ApprLmtSubBasicInfoResource {
    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService;

    @Autowired
    private ICusClientService iCusClientService;
    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<ApprLmtSubBasicInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<ApprLmtSubBasicInfo> list = apprLmtSubBasicInfoService.selectAll(queryModel);
        return new ResultDto<List<ApprLmtSubBasicInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<ApprLmtSubBasicInfo>> index(QueryModel queryModel) {
        List<ApprLmtSubBasicInfo> list = apprLmtSubBasicInfoService.selectByModel(queryModel);
        return new ResultDto<List<ApprLmtSubBasicInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<ApprLmtSubBasicInfo> show(@PathVariable("pkId") String pkId) {
        ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<ApprLmtSubBasicInfo>(apprLmtSubBasicInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<ApprLmtSubBasicInfo> create(@RequestBody ApprLmtSubBasicInfo apprLmtSubBasicInfo) {
        apprLmtSubBasicInfoService.insert(apprLmtSubBasicInfo);
        return new ResultDto<ApprLmtSubBasicInfo>(apprLmtSubBasicInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ApprLmtSubBasicInfo apprLmtSubBasicInfo) {
        int result = apprLmtSubBasicInfoService.update(apprLmtSubBasicInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = apprLmtSubBasicInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = apprLmtSubBasicInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 集团成员信息
     * @param cusGrpMemberTreeClientDto
     * @return
     */
    @PostMapping("/groupMember")
    public ResultDto<List<CusGrpMemberTreeClientDto>> getGroupMemberList(@RequestBody CusGrpMemberTreeClientDto cusGrpMemberTreeClientDto) {
        List<CusGrpMemberTreeClientDto> list= iCusClientService.getCusGrpMemberTree(cusGrpMemberTreeClientDto);
        return new ResultDto<>(list);
    }

    /**
     * @param queryModel
     * @return
     * @date 2021/4/19 22:18
     * @version 1.0.0
     * @desc    查询客户一级额度明细列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectLmtCusInfoList")
    protected ResultDto<List<Map<String,Object>>> selectLmtCusInfoList(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprLmtSubBasicInfoService.selectLmtCusInfoList(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @param queryModel
     * @return
     * @date 2021/7/24
     * @version 1.0.0
     * @desc    查询客户一级额度明细列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectLmtCusInfoListDialog")
    protected ResultDto<List<Map<String,Object>>> selectLmtCusInfoListDialog(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprLmtSubBasicInfoService.selectLmtCusInfoListDialog(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @param queryModel
     * @return
     * @date 2021/4/19 22:18
     * @version 1.0.0
     * @desc    查询同业客户列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectLmtCusPrdInfoList")
    protected ResultDto<List<Map<String,Object>>> selectLmtCusPrdInfoList(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprLmtSubBasicInfoService.selectLmtCusPrdInfoList(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<java.util.Map<java.lang.String,java.lang.Object>>>
     * @date 2021/4/19 22:18
     * @version 1.0.0
     * @desc    查询客户二级额度明细列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectHasChildren")
    protected ResultDto<List<Map<String,Object>>> selectHasChildren(@RequestBody QueryModel queryModel) {
        Object limitSubNo = queryModel.getCondition().get("limitSubNo");
        List<Map<String,Object>> list;

        if (limitSubNo!=null && CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_3006.equals(limitSubNo.toString())){
            list = apprLmtSubBasicInfoService.selectHasChildren4SameOrg(queryModel);
        }else{
            list = apprLmtSubBasicInfoService.selectHasChildren(queryModel);
        }

        return new ResultDto<>(list);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batch/upateAvlAmt")
    protected ResultDto<Integer> upateAvlAmt(){
        ResultDto<Integer> resultDto = new ResultDto<>();
        int result = 0;
        try {
            result = apprLmtSubBasicInfoService.batchUpdateAvlAmt();
        } catch (Exception e) {
            resultDto.setCode("9999");
            resultDto.setMessage("更新报错：" + e.getMessage());
            resultDto.setData(result);
        }
        resultDto.setData(result);
        return resultDto;
    }

    /**
     * @param queryModel
     * @return
     * @date 2021/4/19 22:18
     * @version 1.0.0
     * @desc    查询客户一级额度明细列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectlmtsigzhinfolist")
    protected ResultDto<List<Map<String,Object>>> selectLmtSigZhInfoList(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprLmtSubBasicInfoService.selectLmtSigZhInfoList(queryModel);
        return new ResultDto<>(list);
    }

}