/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.TmpAddamtTdcarAl;
import cn.com.yusys.yusp.repository.mapper.TmpAddamtTdcarAlMapper;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: TmpAddamtTdcarAlService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-11-06 18:15:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class TmpAddamtTdcarAlService {

    @Autowired
    private TmpAddamtTdcarAlMapper tmpAddamtTdcarAlMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public TmpAddamtTdcarAl selectByPrimaryKey(String serno) {
        return tmpAddamtTdcarAlMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<TmpAddamtTdcarAl> selectAll(QueryModel model) {
        List<TmpAddamtTdcarAl> records = (List<TmpAddamtTdcarAl>) tmpAddamtTdcarAlMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<TmpAddamtTdcarAl> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<TmpAddamtTdcarAl> list = tmpAddamtTdcarAlMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(TmpAddamtTdcarAl record) {
        return tmpAddamtTdcarAlMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(TmpAddamtTdcarAl record) {
        return tmpAddamtTdcarAlMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(TmpAddamtTdcarAl record) {
        return tmpAddamtTdcarAlMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(TmpAddamtTdcarAl record) {
        return tmpAddamtTdcarAlMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return tmpAddamtTdcarAlMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return tmpAddamtTdcarAlMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: truncateTalbeDate
     * @方法描述: 清空中间表处理
     * @参数与返回说明:
     * @算法描述: 无
     */
    public void truncateTalbeData() {
        tmpAddamtTdcarAlMapper.truncateTalbeData();
    }

    public int insertTmpLmtAmt(String apprSubSerno) {
        return tmpAddamtTdcarAlMapper.insertTmpLmtAmt(apprSubSerno);
    }

    /**
     * A、若合同占用分项，敞口占用未超批复分项敞口金额  且  合同占用总额 <= 授信分项总额：
     *      则：台账占用合同总已用 - 授信分项总额  作为 需增加的授信总额累加
     */
    public int insertSceneTwoA(String apprSubSerno) {
        return tmpAddamtTdcarAlMapper.insertSceneTwoA(apprSubSerno);
    }

    /**
     * -- B、若合同占用分项，敞口占用未超批复分项敞口金额  且  合同占用总额 > 授信分项总额：
     *    则：台账占用合同总已用 - 合同占用总额  作为 需增加的授信总额累加
     */
    public int insertSceneTwoB(String apprSubSerno) {
        return tmpAddamtTdcarAlMapper.insertSceneTwoB(apprSubSerno);
    }

    /**
     * -- C、若合同占用分项，敞口占用超批复分项敞口金额  且  合同占用总额 <= 授信分项总额：
     *    则：（台账占用合同总已用 - 台账占用合同敞口已用） - （分项总额 - 分项总敞口）  作为 需增加的授信总额累加;
     */
    public int insertSceneTwoC(String apprSubSerno) {
        return tmpAddamtTdcarAlMapper.insertSceneTwoC(apprSubSerno);
    }

    /**
     * --  D、若合同占用分项，敞口占用超批复分项敞口金额  且  合同占用总额 > 授信分项总额：
     *    则：（台账占用合同总已用 - 台账占用合同敞口已用） - 分项已用金额  作为 需增加的授信总额累加;
     */
    public int insertSceneTwoD(String apprSubSerno) {
        return tmpAddamtTdcarAlMapper.insertSceneTwoD(apprSubSerno);
    }

    public int insertParentIdTmpTdcarAl(String apprSubSerno){
        return tmpAddamtTdcarAlMapper.insertParentIdTmpTdcarAl(apprSubSerno);
    }
}
