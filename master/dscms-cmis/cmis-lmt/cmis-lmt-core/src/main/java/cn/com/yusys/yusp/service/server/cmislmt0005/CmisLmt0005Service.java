package cn.com.yusys.yusp.service.server.cmislmt0005;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.domain.ApprStrMtableInfo;
import cn.com.yusys.yusp.domain.LmtContRel;
import cn.com.yusys.yusp.dto.client.esb.com001.req.Com001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.com001.req.Data;
import cn.com.yusys.yusp.dto.client.esb.com001.req.List;
import cn.com.yusys.yusp.dto.client.esb.com001.resp.Com001RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.req.CmisCus0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CmisCus0010RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0005.req.CmisLmt0005LmtDetailsListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0005.req.CmisLmt0005ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0005.resp.CmisLmt0005RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ApprLmtSubBasicInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprStrMtableInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtContRelMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.ApprStrMtableInfoService;
import cn.com.yusys.yusp.service.CmisCusClientService;
import cn.com.yusys.yusp.service.LmtContRelService;
import cn.com.yusys.yusp.service.client.bsp.comstar.com001.Com001Service;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import com.alibaba.fastjson.JSON;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0005Service
 * @类描述: #对内服务类
 * @功能描述: 资金业务额度同步
 * @创建时间: 2021-05-07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0005Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0005Service.class);

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private ApprStrMtableInfoMapper apprStrMtableInfoMapper;

    @Autowired
    private ApprLmtSubBasicInfoMapper apprLmtSubBasicInfoMapper;

    @Autowired
    private ApprStrMtableInfoService apprStrMtableInfoService;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService;

    @Autowired
    private Com001Service com001Service;

    @Autowired
    private LmtContRelMapper lmtContRelMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate ;

    @Autowired
    private Comm4Service comm4Service ;

    @Autowired
    private LmtContRelService lmtContRelService;

    @Autowired
    private CmisCusClientService cmisCusClientService ;

    @Transactional
    public CmisLmt0005RespDto execute(CmisLmt0005ReqDto reqDto) throws YuspException,Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0005.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0005.value);
        CmisLmt0005RespDto resqDto = new CmisLmt0005RespDto() ;
        Map<String, String> paramMap = (Map<String, String>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(reqDto);
        //批复台账编号
        String accNo = reqDto.getAccNo() ;

        Map<String, String> resultMap = new HashMap<>() ;
        resultMap.putAll(paramMap);
        resultMap.put("serno", accNo) ;
        resultMap.put("serviceCode", CmisLmtConstants.CMIS_LMT0005_SERVICE_CODE) ;

        //原批复编号
        String origiAccNo = reqDto.getOrigiAccNo() ;
        //额度品种编号
        String limitSubNo = reqDto.getLimitSubNo();
        //授信金额
        BigDecimal lmtAmt = BigDecimalUtil.replaceNull(reqDto.getLmtAmt());
        //自营金额
        BigDecimal sobsAmt = BigDecimalUtil.replaceNull(reqDto.getSobsAmt());
        //资管金额
        BigDecimal assetManaAmt = BigDecimalUtil.replaceNull(reqDto.getAssetManaAmt());
        //资产编号
        String assetNo = reqDto.getAssetNo() ;

        //是否覆盖原批复台账
        String isCreateAcc = reqDto.getIsCreateAcc();

        //获取批复主表对象
        ApprStrMtableInfo apprStrMtableInfo = new ApprStrMtableInfo();
        //将reqDto的数据拷贝到apprStrMtableInfo
        BeanUtils.copyProperties(reqDto,apprStrMtableInfo);

        //额度类型
        String lmtType = reqDto.getLmtType();
        if (CmisLmtConstants.STD_ZB_LMT_TYPE_04.equals(lmtType)){
            //如果授信类型是产品授信，客户类型为3--同业
            apprStrMtableInfo.setCusType(CmisLmtConstants.STD_ZB_CUS_CATALOG3);
        }else if (CmisLmtConstants.STD_ZB_LMT_TYPE_05.equals(lmtType) || CmisLmtConstants.STD_ZB_LMT_TYPE_08.equals(lmtType)){
            //如果授信类型是主体授信，客户类型为2--对公  20210821与蔡俊确认承销额度只会给法人客户做
            apprStrMtableInfo.setCusType(CmisLmtConstants.STD_ZB_CUS_CATALOG2);
        }
        //集团客户编号
        apprStrMtableInfo.setGrpNo(comm4Service.selectGrpNoByCusId(reqDto.getCusId()));
        //批复台账编号
        apprStrMtableInfo.setApprSerno(reqDto.getAccNo());
        //额度状态
        apprStrMtableInfo.setApprStatus(reqDto.getAccStatus());
        //最新更新人
        apprStrMtableInfo.setUpdId(reqDto.getInputId());
        //最新更新机构
        apprStrMtableInfo.setUpdBrId(reqDto.getInputBrId());
        //最新更新日期
        apprStrMtableInfo.setUpdDate(reqDto.getInputDate());
        //修改时间
        apprStrMtableInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));

        //获取批复分项表对象
        ApprLmtSubBasicInfo apprLmtSubBasicInfo = new ApprLmtSubBasicInfo();

        BeanUtils.copyProperties(reqDto,apprLmtSubBasicInfo);

        //可用总额取授信总额
        apprLmtSubBasicInfo.setAvlAmt(lmtAmt);

        /**
         * 如果是 以新批复覆盖老批复isCreateAcc：如果是则原批复编号必须存在，批复状态跟更改为99 失效已结清，如果存在未结清业务，则原业务挂到新批复下
         * 如果是 否以新批复覆盖老批复isCreateAcc：
         *          根据批复台账判断该批复是否已经存在，如果存在，进行变更操作，如果不存在则新增批复
         */

        if(CmisLmtConstants.YES_NO_Y.equals(isCreateAcc)){
            //原批复信息
            ApprStrMtableInfo oldApprStrMtableInfo = apprStrMtableInfoService.selectByAppSerno(origiAccNo) ;

            if(oldApprStrMtableInfo == null){
                resqDto.setErrorCode(EclEnum.ECL070072.key);
                resqDto.setErrorMsg(EclEnum.ECL070072.value);
                return resqDto ;
            }
            //原批复状态更改为失效已结清
            oldApprStrMtableInfo.setApprStatus(CmisLmtConstants.STD_ZB_APPR_ST_99);
            apprStrMtableInfoMapper.updateByPrimaryKey(oldApprStrMtableInfo) ;
            logger.info("CmisLmt0005 生成新批复，原批复编号：【"+origiAccNo+"】信息，更新为【99】失效已结清！") ;
        }

       //如果该批复编号已存在，则是变更
        ApprStrMtableInfo apprStrMtable = apprStrMtableInfoService.selectApprStrMtableInfoBySerno(accNo);

        //判断是否债券池额度，如果是透支，查看该客户向下是否存在符合条件债券投资，如果存在，将该额度业务挂靠到债券池向下
        BigDecimal outAmt = BigDecimal.ZERO ;
        BigDecimal outLoanBal = BigDecimal.ZERO ;
        Map<String, Object> result = null ;
        if(CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4001.equals(limitSubNo)){
            result = this.generateSigInvestToPool(reqDto, resultMap) ;
            outAmt = (BigDecimal) result.get("outTotal");
            outLoanBal = (BigDecimal) result.get("outLoanBal");
        }

        if(apprStrMtable==null || CmisLmtConstants.YES_NO_Y.equals(isCreateAcc)){
            logger.info("CmisLmt0005 批复编号【"+accNo+"】不存在，新增批复主信息开始。。。");

            //新增
            //生成主键
            String pkValue = getPkId();
            //添加主键
            apprStrMtableInfo.setPkId(pkValue);
            //登记人
            apprStrMtableInfo.setInputId(reqDto.getInputId());
            //登记机构
            apprStrMtableInfo.setInputBrId(reqDto.getInputBrId());
            //登记时间
            apprStrMtableInfo.setInputDate(reqDto.getInputDate());
            //创建时间
            apprStrMtableInfo.setCreateTime(new Date());
            //操作类型 默认 01- 新增
            apprStrMtableInfo.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
            //授信模式，默认 02--单笔单批
            apprStrMtableInfo.setLmtMode(CmisLmtConstants.STD_ZB_LMT_MODE_02);

            //插入批复主表信息
            apprStrMtableInfoMapper.insert(apprStrMtableInfo);

            logger.info("CmisLmt0005 批复编号【"+accNo+"】不存在，新增批复主信息结束。。。");

            //已出账金额
            BigDecimal pvpOutstndAmt = BigDecimal.ZERO;

            if(CmisLmtConstants.YES_NO_Y.equals(isCreateAcc)){

                logger.info("CmisLmt0005 需要生成新批复分项覆盖老批复分项。。。");

                //是否生成新批复为是，则将原分项下的业务挂到新分项下，然后把原分项状态改为失效已结清
                ApprLmtSubBasicInfo oldApprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(origiAccNo);

                Optional.ofNullable(oldApprLmtSubBasicInfo).orElseThrow(()->new YuspException(EclEnum.ECL070007.key,EclEnum.ECL070007.value));
                //已出账金额取原分项的已出账金额
                pvpOutstndAmt = BigDecimalUtil.replaceNull(oldApprLmtSubBasicInfo.getPvpOutstndAmt());
                //原分项下已出账金额+债券投资的用信余额=总的已出账金额
                pvpOutstndAmt = pvpOutstndAmt.add(outLoanBal) ;

                BigDecimal oldLoanBalance = BigDecimalUtil.replaceNull(oldApprLmtSubBasicInfo.getLoanBalance()) ;
                outLoanBal = outLoanBal.add(oldLoanBalance) ;

                BigDecimal oldOutAmt = BigDecimalUtil.replaceNull(oldApprLmtSubBasicInfo.getOutstndAmt()) ;
                outAmt = outAmt.add(oldOutAmt) ;

                //查询原分项下业务，将未结清业务挂靠到新分项下
                comm4Service.updateLmtContRelRelSubNo(origiAccNo, accNo);

                //原批复状态更改为失效已结清
                oldApprLmtSubBasicInfo.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_99);
                apprLmtSubBasicInfoService.update(oldApprLmtSubBasicInfo) ;
                logger.info("CmisLmt0005 生成新批复分项，原批复分项编号：【"+origiAccNo+"】信息，更新为【99】失效已结清！") ;

                //续作时，生成新批复，原批复如果穿透到底层，则需要将占用底层额度进行恢复操作
                java.util.List<LmtContRel> lmtContRelOld = lmtContRelService.selectLmtContRelListByDealBizNo(origiAccNo, null) ;
                if(!CollectionUtils.isEmpty(lmtContRelOld)){
                    //遍历底层占用关系，进行额度恢复操作
                    for (LmtContRel lmtContRel : lmtContRelOld) {
                        String apprSubSerno = lmtContRel.getLimitSubNo() ;
                        //撤销占用，删除合同信息，操作类型更改为02 删除，恢复分项金额，合同不考虑
                        lmtContRel.setOprType(CmisLmtConstants.OPR_TYPE_DELETE);
                        lmtContRelMapper.updateByPrimaryKey(lmtContRel);
                        comm4Service.updateSubInfoAmt(apprSubSerno, lmtContRel, resultMap) ;
                    }
                }
            }

            apprLmtSubBasicInfo.setGrpNo(comm4Service.selectGrpNoByCusId(reqDto.getCusId()));
            //外键
            apprLmtSubBasicInfo.setFkPkid(reqDto.getAccNo());
            //操作类型 默认 01- 新增
            apprLmtSubBasicInfo.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
            //额度台账状态 默认 01- 生效
            apprLmtSubBasicInfo.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
            //授信总额
            apprLmtSubBasicInfo.setAvlAmt(lmtAmt);
            //敞口可用额度取授信总额
            apprLmtSubBasicInfo.setSpacAmt(lmtAmt);
            //用信余额默认为0
            apprLmtSubBasicInfo.setLoanBalance(BigDecimal.ZERO);
            //已用总额
            apprLmtSubBasicInfo.setOutstndAmt(outAmt);
            //敞口已用额度
            apprLmtSubBasicInfo.setSpacOutstndAmt(outAmt);
            //是否低风险授信 默认 N-否
            apprLmtSubBasicInfo.setIsLriskLmt(CmisLmtConstants.YES_NO_N);
            //授信分项类型 默认 01-额度分项
            apprLmtSubBasicInfo.setLmtSubType(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_01);
            //额度类型 默认 01-综合
            apprLmtSubBasicInfo.setLimitType(CmisLmtConstants.STD_ZB_LIMIT_TYPE_01);
            //已出帐金额 默认为0
            apprLmtSubBasicInfo.setPvpOutstndAmt(pvpOutstndAmt);
            //可出账金额 敞口可用-已出帐金额
            apprLmtSubBasicInfo.setAvlOutstndAmt(lmtAmt.subtract(pvpOutstndAmt));
            //是否预授信 默认为 N-否
            apprLmtSubBasicInfo.setIsPreCrd(CmisLmtConstants.YES_NO_N);
            //授信总额累加 默认为0
            apprLmtSubBasicInfo.setLmtAmtAdd(BigDecimal.ZERO);
            //用信敞口余额 默认为授信总额
            apprLmtSubBasicInfo.setLoanSpacBalance(outLoanBal);
            //到期日
            apprLmtSubBasicInfo.setLmtDate(reqDto.getEndDate());
            if (CmisLmtConstants.STD_ZB_LMT_TYPE_04.equals(lmtType)){
                //如果授信类型是产品授信，客户类型为3--同业
                apprLmtSubBasicInfo.setCusType(CmisLmtConstants.STD_ZB_CUS_CATALOG3);
            }else if (CmisLmtConstants.STD_ZB_LMT_TYPE_05.equals(lmtType) || CmisLmtConstants.STD_ZB_LMT_TYPE_08.equals(lmtType)){
                //如果授信类型是主体授信，客户类型为2--对公  20210821与蔡俊确认承销额度只会给法人客户做
                apprLmtSubBasicInfo.setCusType(CmisLmtConstants.STD_ZB_CUS_CATALOG2);
            }
            //生成主键
            String subPkId = getPkId();
            //设置主键
            apprLmtSubBasicInfo.setPkId(subPkId);
            //批复分项流水号取批复编号
            apprLmtSubBasicInfo.setApprSubSerno(reqDto.getAccNo());

            //插入批复额度分项基础信息表
            logger.info("CmisLmt0005 批复编号【"+accNo+"】不存在，新增批复分项信息。。。");
            apprLmtSubBasicInfoService.insert(apprLmtSubBasicInfo);

            //如果自营金额、资管金额有值，则生成对应的批复分项记录  TODO:此段代码已废除，无用，暂时留存
            if(sobsAmt.compareTo(BigDecimal.ZERO)>0){
                logger.info("CmisLmt0005 批复编号【"+accNo+"】不存在，自营金额有值，生成对应的批复分项记录开始。。。");
                insertApprLmtSubBasicInfo4SobsAndAssetMana(isCreateAcc,apprLmtSubBasicInfo,origiAccNo,sobsAmt,accNo,CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_400101,CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_400101_NAME);
                logger.info("CmisLmt0005 批复编号【"+accNo+"】不存在，自营金额有值，生成对应的批复分项记录结束。。。");
            }

            //TODO:此段代码已废除，无用，暂时留存
            if (assetManaAmt.compareTo(BigDecimal.ZERO)>0){
                logger.info("CmisLmt0005 批复编号【"+accNo+"】不存在，资管金额有值，生成对应的批复分项记录开始。。。");
                insertApprLmtSubBasicInfo4SobsAndAssetMana(isCreateAcc,apprLmtSubBasicInfo,origiAccNo,assetManaAmt,accNo,CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_400102,CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_400102_NAME);
                logger.info("CmisLmt0005 批复编号【"+accNo+"】不存在，资管金额有值，生成对应的批复分项记录结束。。。");
            }
        }else {
            logger.info("CmisLmt0005 批复编号【"+accNo+"】存在，更新批复主信息和批复分项信息。。。");
            //变更
            apprStrMtableInfoMapper.updateByApprSernoSelective(apprStrMtableInfo);

            apprLmtSubBasicInfo.setGrpNo(comm4Service.selectGrpNoByCusId(reqDto.getCusId()));
            apprLmtSubBasicInfo.setApprSubSerno(reqDto.getAccNo());
            apprLmtSubBasicInfo.setSpacAmt(lmtAmt);
            apprLmtSubBasicInfo.setAvlOutstndAmt(lmtAmt);
            apprLmtSubBasicInfo.setUpdateTime(comm4Service.getCurrrentDate());
            apprLmtSubBasicInfo.setUpdDate(DateUtils.getCurrDateStr());
            apprLmtSubBasicInfo.setLmtDate(reqDto.getEndDate());
            apprLmtSubBasicInfo.setStatus(reqDto.getAccStatus());
            if (CmisLmtConstants.STD_ZB_LMT_TYPE_04.equals(lmtType)){
                //如果授信类型是产品授信，客户类型为3--同业
                apprLmtSubBasicInfo.setCusType(CmisLmtConstants.STD_ZB_CUS_CATALOG3);
            }else if (CmisLmtConstants.STD_ZB_LMT_TYPE_05.equals(lmtType) || CmisLmtConstants.STD_ZB_LMT_TYPE_08.equals(lmtType)){
                //如果授信类型是主体授信，客户类型为2--对公  20210821与蔡俊确认承销额度只会给法人客户做
                apprLmtSubBasicInfo.setCusType(CmisLmtConstants.STD_ZB_CUS_CATALOG2);
            }
            apprLmtSubBasicInfoMapper.updateByApprSubSerno(apprLmtSubBasicInfo);

            //如果自营金额、资管金额有值，则更新对应的批复分项的授信总额
            if(sobsAmt.compareTo(BigDecimal.ZERO)>0){
                logger.info("CmisLmt0005 批复编号【"+accNo+"】存在，自营金额有值，更新对应的批复分项记录开始。。。");
                updateAvlAmt4SobsAndAssetMana(origiAccNo,CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_400101,sobsAmt);
                logger.info("CmisLmt0005 批复编号【"+accNo+"】存在，自营金额有值，更新对应的批复分项记录结束。。。");
            }

            if (assetManaAmt.compareTo(BigDecimal.ZERO)>0){
                logger.info("CmisLmt0005 批复编号【"+accNo+"】存在，资管金额有值，更新对应的批复分项记录开始。。。");
                updateAvlAmt4SobsAndAssetMana(origiAccNo,CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_400102,assetManaAmt);
                logger.info("CmisLmt0005 批复编号【"+accNo+"】存在，资管金额有值，更新对应的批复分项记录结束。。。");
            }
       }

        //如果是产品授信则需要占用同业投资类或同业管理类额度
        if(CmisLmtConstants.STD_ZB_LMT_TYPE_04.equals(lmtType)){
            logger.info("CmisLmt0005 批复编号【"+accNo+"】，占用同业投资类或同业管理类额度开始。。。");
            generateLmtContRel4SameOrg(reqDto,limitSubNo,isCreateAcc);
            logger.info("CmisLmt0005 批复编号【"+accNo+"】，占用同业投资类或同业管理类额度结束。。。");
        }

        //遍历底层融资人分项列表，根据底层融资人批复分项编号查询分项占用关系表,
        //如果存在就更新否则新增
        // 如果是生成穿透化额度，则再生成批复主信息和分项信息记录
        java.util.List<CmisLmt0005LmtDetailsListReqDto> lmtDetailsList = reqDto.getLmtDetailsList();

        for (CmisLmt0005LmtDetailsListReqDto lmtDetails:lmtDetailsList) {

            //底层融资人批复分项编号
            String basicAccSubNo = lmtDetails.getBasicAccSubNo();
            //金融组织代码
            String sysId = reqDto.getSysId() ;
            LmtContRel lmtContRel = lmtContRelService.selectByLmtSubNoAndDealNo(basicAccSubNo, reqDto.getAccNo(), sysId) ;
            //java.util.List<LmtContRel> lmtContRels = lmtContRelMapper.selectLmtContRelByDealBizNo(reqDto.getAccNo());

            if (Objects.isNull(lmtContRel)){
                //新增
                logger.info("CmisLmt0005 批复编号【"+accNo+"】，新增分项占用关系，占用底层融资人批复分项编号【"+basicAccSubNo+"】开始");
                insertLmtContRel(reqDto,lmtDetails);
                logger.info("CmisLmt0005 批复编号【"+accNo+"】，新增分项占用关系，占用底层融资人批复分项编号【"+basicAccSubNo+"】结束");
            }else {
                //变更
                logger.info("CmisLmt0005 批复编号【"+accNo+"】，更新分项占用关系，占用底层融资人批复分项编号【"+basicAccSubNo+"】开始");
                updateLmtContRel(lmtDetails, lmtContRel);
                logger.info("CmisLmt0005 批复编号【"+accNo+"】，更新分项占用关系，占用底层融资人批复分项编号【"+basicAccSubNo+"】结束");
            }

            //是否生成穿透化额度
            String isCreateLmt = lmtDetails.getIsCreateLmt();
            if (CmisLmtConstants.YES_NO_Y.equals(isCreateLmt)){
                ApprStrMtableInfo apprStrMtableInfo2 = apprStrMtableInfoService.selectApprStrMtableInfoBySerno(lmtDetails.getBasicAccNo());

                if (apprStrMtableInfo2==null){
                    //新增批复主信息
                    insertApprStrMtableInfo(reqDto,lmtDetails);
                    //新增批复分项信息
                    insertApprLmtSubBasicInfo(reqDto,lmtDetails);
                }else{
                    //更新批复主信息
                    updateApprStrMtableInfo(apprStrMtableInfo2,reqDto,lmtDetails);
                    //更新批复分项信息
                    updateApprLmtSubBasicInfo(lmtDetails);
                }
            }else{
                //不生成穿透化额度，查询分项信息，判断是否同业交易类额度、同业融资类额度或同业管理类额度涉及货币基金的，如果是，重新推送comstart 系统
                ApprLmtSubBasicInfo apprLmtSubBasicInfo1 = apprLmtSubBasicInfoService.selectByApprSubSerno(basicAccSubNo) ;
                if(apprLmtSubBasicInfo1!=null){
                    //3001--同业融资类额度 3005-同业交易类额度 3006-同业管理类额度 同业管理类额度，是否涉及货币基金额度
                    if (CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_3001.equals(apprLmtSubBasicInfo1.getLimitSubNo()) ||
                            CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_3005.equals(apprLmtSubBasicInfo1.getLimitSubNo()) ||
                            (CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_3006.equals(apprLmtSubBasicInfo1.getLimitSubNo())
                                    && CmisLmtConstants.STD_ZB_YES_NO_Y.equals(apprLmtSubBasicInfo1.getIsIvlMf())) ){
                        lmtIntendComstar(apprLmtSubBasicInfo1, lmtDetails) ;
                    }
                }
            }
        }

        /**
         * 以下产品需要推送资产编号，资产编号为空的时候，不推送
         * 4002-债券投资
         * 4003-资产-其他
         * 4004-资产-债权融资计划、理财直融工具
         * 4005-净值型产品
         * 4006-资产证券化产品（标准）
         * 4007-资产证券化产品（非标）
         * 4008-其他标准化债权投资
         * 4009-债务融资工具（投资）
         * 4010-理财直融工具（投资）
         * 4011-结构化融资
         * 4012-其他非标债权投资
         * 4001-债券池 不需要推送资产编号
         * 承销类额度无需推送ComStar
         */
        if((CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4001.equals(limitSubNo) || StringUtils.isNotEmpty(assetNo)) && !CmisLmtConstants.STD_ZB_LMT_TYPE_08.equals(lmtType) ){
            logger.info("CmisLmt0005 批复编号【"+accNo+"】，发送comstar开始。。。");
            sendComstar(reqDto,limitSubNo,lmtAmt,sobsAmt,assetManaAmt,result);
            logger.info("CmisLmt0005 批复编号【"+accNo+"】，发送comstar结束。。。");
        }

        resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
        resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0005.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0005.value);
        return resqDto;
    }

    /**
     * 往分项占用关系表里插入一条记录
     * @param lmtDetails
     */
    private void insertLmtContRel(CmisLmt0005ReqDto reqDto,CmisLmt0005LmtDetailsListReqDto lmtDetails){
        LmtContRel lmtContRel = new LmtContRel();
        //分项编号
        String basicAccSubNo = lmtDetails.getBasicAccSubNo() ;
        BigDecimal useamt = BigDecimalUtil.replaceNull(lmtDetails.getUseamt()) ;
        //PK_ID	主键
        Map seqMap = new HashMap();
        String pkId = sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ, seqMap);

        BeanUtils.copyProperties(lmtDetails,lmtContRel);

        lmtContRel.setPkId(pkId);
        //系统编号录入
        lmtContRel.setSysId(reqDto.getSysId());
        //客户编号
        lmtContRel.setCusId(reqDto.getCusId());
        //客户名称
        lmtContRel.setCusName(reqDto.getCusName());
        //交易业务编号取批复台账编号
        lmtContRel.setDealBizNo(reqDto.getAccNo());
        //额度分项编号取底层融资人批复台账编号
        lmtContRel.setLimitSubNo(basicAccSubNo);
        //交易业务类型  默认1-一般合同
        lmtContRel.setDealBizType(CmisLmtConstants.STD_ZB_CONT_TYPE_1);
        //业务属性 默认3--授信
        lmtContRel.setBizAttr(CmisLmtConstants.STD_ZB_BIZ_ATTR_3);
        //产品编号 默认额度品种编号
        lmtContRel.setPrdId(reqDto.getProNo());
        //PRD_NAME	产品名称  额度品种名称
        lmtContRel.setPrdName(reqDto.getProName());
        //占用总金额（折人民币）取本次占用金额
        lmtContRel.setBizTotalAmtCny(useamt);
        //合同金额（折人民币）取本次占用金额
        lmtContRel.setBizAmt(useamt);
        //占用敞口金额（折人民币）取本次占用金额
        lmtContRel.setBizSpacAmtCny(useamt);
        //占用总余额（折人民币）取本次占用金额
        lmtContRel.setBizTotalBalanceAmtCny(useamt);
        //占用敞口余额（折人民币）取本次占用金额
        lmtContRel.setBizSpacBalanceAmtCny(useamt);
        //SECURITY_RATE	业务保证金比例
        lmtContRel.setSecurityRate(BigDecimal.ZERO);
        //SECURITY_AMT	业务保证金金额
        lmtContRel.setSecurityAmt(BigDecimal.ZERO);
        //交易业务状态 默认200--有效
        lmtContRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_200);
        //操作类型 默认01--新增
        lmtContRel.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        //最近更新人
        lmtContRel.setUpdId(lmtDetails.getInputId());
        //最近更新机构
        lmtContRel.setUpdBrId(lmtDetails.getInputBrId());
        //最近更新日期
        lmtContRel.setUpdDate(lmtDetails.getInputDate());
        //创建时间
        lmtContRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //修改时间
        lmtContRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //投资资产名称
        lmtContRel.setInvestAssetName(reqDto.getProName());
        //起始日期
        lmtContRel.setStartDate(reqDto.getStartDate());
        //到日期
        lmtContRel.setEndDate(reqDto.getEndDate());
        //授信模式
        lmtContRel.setLmtType(reqDto.getLmtType());

        //插入分项占用关系表
        lmtContRelMapper.insert(lmtContRel);

        ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(basicAccSubNo) ;
        if(apprLmtSubBasicInfo!=null){
            //更新批复分项信息里的已用金额、敞口已用金额
            apprLmtSubBasicInfo.setOutstndAmt(useamt);
            apprLmtSubBasicInfo.setSpacOutstndAmt(useamt);
            apprLmtSubBasicInfo.setUpdId(reqDto.getInputId());
            apprLmtSubBasicInfo.setUpdBrId(reqDto.getInputBrId());
            apprLmtSubBasicInfo.setUpdDate(reqDto.getInputDate());
            apprLmtSubBasicInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            apprLmtSubBasicInfoMapper.updateByPrimaryKey(apprLmtSubBasicInfo);
        }
    }

    /**
     * 更新分项占用关系表数据
     * @param lmtDetails
     * @param lmtContRel
     * @ 算法：更新分项分项总额已用以及敞口已用：取原额度占用关系中的总额已用和敞口已用 ，分项中总额已用先减去原分项中的总额已用加本次更新后的总额已用，敞口已用相同
     */
    private void updateLmtContRel(CmisLmt0005LmtDetailsListReqDto lmtDetails, LmtContRel lmtContRel){
        //for (LmtContRel lmtContRel:lmtContRelList) {
        //获取原额度占用关系中的总额已用以及敞口已用
        BigDecimal useamt =  BigDecimalUtil.replaceNull(lmtDetails.getUseamt()) ;
        //占用总金额（折人民币）取本次占用金额
        lmtContRel.setBizTotalAmtCny(useamt);
        //占用敞口金额（折人民币）取本次占用金额
        lmtContRel.setBizSpacAmtCny(useamt);
        //占用总余额（折人民币）取本次占用金额
        lmtContRel.setBizTotalBalanceAmtCny(useamt);
        //占用敞口余额（折人民币）取本次占用金额
        lmtContRel.setBizSpacBalanceAmtCny(useamt);

        //最近更新人
        lmtContRel.setUpdId(lmtDetails.getInputId());
        //最近更新机构
        lmtContRel.setUpdBrId(lmtDetails.getInputBrId());
        //最近更新日期
        lmtContRel.setUpdDate(lmtDetails.getInputDate());
        //修改时间
        lmtContRel.setUpdateTime(new Date());
        //更新分项占用关系表
        lmtContRelMapper.updateByPrimaryKey(lmtContRel);
        //分项编号
        String apprSubSerno = lmtContRel.getLimitSubNo() ;
        ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(apprSubSerno) ;
        if(apprLmtSubBasicInfo!=null){
            //更新批复分项信息里的已用金额、敞口已用金额
            apprLmtSubBasicInfo.setOutstndAmt(useamt);
            apprLmtSubBasicInfo.setSpacOutstndAmt(useamt);
            apprLmtSubBasicInfo.setUpdId(lmtDetails.getInputId());
            apprLmtSubBasicInfo.setUpdBrId(lmtDetails.getInputBrId());
            apprLmtSubBasicInfo.setUpdDate(lmtDetails.getInputDate());
            apprLmtSubBasicInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            apprLmtSubBasicInfoMapper.updateByPrimaryKey(apprLmtSubBasicInfo);
        }
        //}
    }

    /**
     * 往批复主信息表里插入一条数据,返回主键
     * @param reqDto
     * @param lmtDetails
     * @return 主键
     */
    private void insertApprStrMtableInfo(CmisLmt0005ReqDto reqDto,CmisLmt0005LmtDetailsListReqDto lmtDetails){
        //初始化批复主表对象
        ApprStrMtableInfo apprStrMtableInfo = new ApprStrMtableInfo();

        BeanUtils.copyProperties(lmtDetails,apprStrMtableInfo);

        apprStrMtableInfo.setPkId(getPkId());
        apprStrMtableInfo.setStartDate(reqDto.getStartDate());
        apprStrMtableInfo.setEndDate(reqDto.getEndDate());
        apprStrMtableInfo.setTerm(reqDto.getTerm());
        apprStrMtableInfo.setGrpNo(comm4Service.selectGrpNoByCusId(reqDto.getCusId()));
        //金融机构代码
        apprStrMtableInfo.setInstuCde(reqDto.getInstuCde());
        //客户编号
        apprStrMtableInfo.setCusId(lmtDetails.getBasicCusId());
        //客户名称
        apprStrMtableInfo.setCusName(lmtDetails.getBasicCusName());
        //客户类型
        apprStrMtableInfo.setCusType(lmtDetails.getbasicCusCatalog());
        //额度类型
        apprStrMtableInfo.setLmtType(reqDto.getLmtType());
        //批复台账编号
        apprStrMtableInfo.setApprSerno(lmtDetails.getBasicAccNo());
        //期限
        apprStrMtableInfo.setTerm(reqDto.getTerm());
        //授信起始日
        apprStrMtableInfo.setStartDate(reqDto.getStartDate());
        //授信到期日
        apprStrMtableInfo.setEndDate(reqDto.getEndDate());
        //额度状态
        apprStrMtableInfo.setApprStatus(lmtDetails.getAccStatus());

        //操作类型 默认 01- 新增
        apprStrMtableInfo.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        //最新更新人
        apprStrMtableInfo.setUpdId(lmtDetails.getInputId());
        //最新更新机构
        apprStrMtableInfo.setUpdBrId(lmtDetails.getInputBrId());
        //最新更新日期
        apprStrMtableInfo.setUpdDate(lmtDetails.getInputDate());
        //创建时间
        apprStrMtableInfo.setCreateTime(new Date());
        //修改时间
        apprStrMtableInfo.setUpdateTime(new Date());
        //授信类型
        apprStrMtableInfo.setLmtMode(CmisLmtConstants.STD_ZB_LMT_MODE_02);
        //插入批复主信息表
        apprStrMtableInfoMapper.insert(apprStrMtableInfo);
    }

    /**
     * 更新该笔批复主信息数据
     * @param reqDto
     * @param lmtDetails
     * @return 主键
     */
    private void updateApprStrMtableInfo(ApprStrMtableInfo apprStrMtableInfo,CmisLmt0005ReqDto reqDto,CmisLmt0005LmtDetailsListReqDto lmtDetails){
        //金融机构代码
        apprStrMtableInfo.setInstuCde(reqDto.getInstuCde());
        //客户编号
        apprStrMtableInfo.setCusId(lmtDetails.getBasicCusId());
        //客户名称
        apprStrMtableInfo.setCusName(lmtDetails.getBasicCusName());
        //客户类型
        apprStrMtableInfo.setCusType(lmtDetails.getbasicCusCatalog());
        //额度类型
        apprStrMtableInfo.setLmtType(reqDto.getLmtType());
        //批复台账编号
        apprStrMtableInfo.setApprSerno(lmtDetails.getBasicAccNo());

        //期限
        apprStrMtableInfo.setTerm(reqDto.getTerm());
        //授信起始日
        apprStrMtableInfo.setStartDate(reqDto.getStartDate());
        //授信到期日
        apprStrMtableInfo.setEndDate(reqDto.getEndDate());
        //额度状态
        apprStrMtableInfo.setApprStatus(lmtDetails.getAccStatus());
        //责任人
        apprStrMtableInfo.setManagerId(lmtDetails.getManagerId());
        //责任机构
        apprStrMtableInfo.setManagerBrId(lmtDetails.getManagerBrId());
        //操作类型 默认 01- 新增
        apprStrMtableInfo.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        //最新更新人
        apprStrMtableInfo.setUpdId(lmtDetails.getInputId());
        //最新更新机构
        apprStrMtableInfo.setUpdBrId(lmtDetails.getInputBrId());
        //最新更新日期
        apprStrMtableInfo.setUpdDate(lmtDetails.getInputDate());
        //修改时间
        apprStrMtableInfo.setUpdateTime(new Date());
        //插入批复主信息表
        apprStrMtableInfoMapper.updateByPrimaryKeySelective(apprStrMtableInfo);
    }

    /**
     * 往批复分项表里插入一条数据
     * @param reqDto
     * @param lmtDetails
     */
    private void insertApprLmtSubBasicInfo(CmisLmt0005ReqDto reqDto,CmisLmt0005LmtDetailsListReqDto lmtDetails){
        //初始化批复分项表对象
        ApprLmtSubBasicInfo apprLmtSubBasicInfo = new ApprLmtSubBasicInfo();

        BeanUtils.copyProperties(lmtDetails,apprLmtSubBasicInfo);

        //设置主键
        apprLmtSubBasicInfo.setPkId(getPkId());

        apprLmtSubBasicInfo.setGrpNo(comm4Service.selectGrpNoByCusId(reqDto.getCusId()));
        //设置外键
        apprLmtSubBasicInfo.setFkPkid(lmtDetails.getBasicAccNo());
        //批复分项流水号取批复编号
        apprLmtSubBasicInfo.setApprSubSerno(lmtDetails.getBasicAccSubNo());
        //客户编号
        apprLmtSubBasicInfo.setCusId(lmtDetails.getBasicCusId());
        //客户名称
        apprLmtSubBasicInfo.setCusName(lmtDetails.getBasicCusName());
        //客户主体类型
        apprLmtSubBasicInfo.setCusType(lmtDetails.getbasicCusCatalog());

        //授信金额取本次占用金额
        apprLmtSubBasicInfo.setAvlAmt(lmtDetails.getUseamt());
        //敞口可用额度取本次占用金额
        apprLmtSubBasicInfo.setSpacAmt(lmtDetails.getUseamt());
        //用信余额默认为0
        apprLmtSubBasicInfo.setLoanBalance(BigDecimal.ZERO);
        //已用总额默认为0
        apprLmtSubBasicInfo.setOutstndAmt(lmtDetails.getUseamt());
        //敞口已用额度默认为0
        apprLmtSubBasicInfo.setSpacOutstndAmt(lmtDetails.getUseamt());
        //是否低风险授信 默认 N-否
        apprLmtSubBasicInfo.setIsLriskLmt(CmisLmtConstants.YES_NO_N);
        //授信分项类型 默认 01-额度分项
        apprLmtSubBasicInfo.setLmtSubType(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_01);
        //额度类型 默认 01-综合
        apprLmtSubBasicInfo.setLimitType(CmisLmtConstants.STD_ZB_LIMIT_TYPE_01);
        //已出帐金额 默认为0
        apprLmtSubBasicInfo.setPvpOutstndAmt(BigDecimal.ZERO);
        //可出账金额 默认为本次占用金额
        apprLmtSubBasicInfo.setAvlOutstndAmt(BigDecimal.ZERO);
        //是否预授信 默认为 N-否
        apprLmtSubBasicInfo.setIsPreCrd(CmisLmtConstants.YES_NO_N);
        //授信总额累加 默认为0
        apprLmtSubBasicInfo.setLmtAmtAdd(BigDecimal.ZERO);
        //用信敞口余额 默认为本次占用金额
        apprLmtSubBasicInfo.setLoanSpacBalance(BigDecimal.ZERO);
        //最近更新人
        apprLmtSubBasicInfo.setUpdId(lmtDetails.getInputId());
        //最近更新机构
        apprLmtSubBasicInfo.setUpdBrId(lmtDetails.getInputBrId());
        //最近更新日期
        apprLmtSubBasicInfo.setUpdDate(lmtDetails.getInputDate());
        //创建时间
        apprLmtSubBasicInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //修改时间
        apprLmtSubBasicInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //是否可循环
        apprLmtSubBasicInfo.setIsRevolv(reqDto.getIsRevolv());
        //币种
        apprLmtSubBasicInfo.setCurType(reqDto.getCurType());
        //项目编号
        apprLmtSubBasicInfo.setProNo(reqDto.getProNo());
        //项目名称
        apprLmtSubBasicInfo.setProName(reqDto.getProName());
        //资产编号
        apprLmtSubBasicInfo.setAssetNo(reqDto.getAssetNo());
        //操作类型 默认 01- 新增
        apprLmtSubBasicInfo.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        //到期日
        apprLmtSubBasicInfo.setLmtDate(reqDto.getEndDate());
        //起始日
        apprLmtSubBasicInfo.setStartDate(reqDto.getStartDate());
        //状态 01-生效
        apprLmtSubBasicInfo.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
        //宽限期
        apprLmtSubBasicInfo.setLmtGraper(0);
        //插入数据
        apprLmtSubBasicInfoMapper.insert(apprLmtSubBasicInfo);
    }

    /**
     * 更新该笔批复分项信息
     * @param lmtDetails
     */
    private void updateApprLmtSubBasicInfo(CmisLmt0005LmtDetailsListReqDto lmtDetails){
        ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(lmtDetails.getBasicAccSubNo());
        apprLmtSubBasicInfo.setGrpNo(comm4Service.selectGrpNoByCusId(lmtDetails.getBasicCusId()));
        //客户编号
        apprLmtSubBasicInfo.setCusId(lmtDetails.getBasicCusId());
        //客户名称
        apprLmtSubBasicInfo.setCusName(lmtDetails.getBasicCusName());
        //客户主体类型
        apprLmtSubBasicInfo.setCusType(lmtDetails.getbasicCusCatalog());
        //适用担保方式
        apprLmtSubBasicInfo.setSuitGuarWay(lmtDetails.getSuitGuarWay());
        //授信金额取本次占用金额
        apprLmtSubBasicInfo.setAvlAmt(lmtDetails.getUseamt());
        //敞口可用额度为0
        apprLmtSubBasicInfo.setSpacAmt(lmtDetails.getUseamt());
        //敞口已用额度取本次占用金额
        apprLmtSubBasicInfo.setSpacOutstndAmt(lmtDetails.getUseamt());
        //最近更新人
        apprLmtSubBasicInfo.setUpdId(lmtDetails.getInputId());
        //最近更新机构
        apprLmtSubBasicInfo.setUpdBrId(lmtDetails.getInputBrId());
        //最近更新日期
        apprLmtSubBasicInfo.setUpdDate(lmtDetails.getInputDate());
        //修改时间
        apprLmtSubBasicInfo.setUpdateTime(new Date());
        //额度品种编号
        apprLmtSubBasicInfo.setLimitSubNo(lmtDetails.getLimitSubNo());
        //额度品种名称
        apprLmtSubBasicInfo.setLimitSubName(lmtDetails.getLimitSubName());
        //操作类型 默认 01- 新增
        apprLmtSubBasicInfo.setOprType(CmisLmtConstants.OPR_TYPE_ADD);

        //插入数据
        apprLmtSubBasicInfoMapper.updateByPrimaryKeySelective(apprLmtSubBasicInfo);
    }

    /**
     * 生成主键
     * @return
     */
    private String getPkId(){
        Map paramMap= new HashMap<>();
        return sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, paramMap);
    }

    /**
     *
     * @方法名称：generateSigInvestToPool
     * @描述：
     * 检查同一客户不允许发起多笔债券池授信
     * 如该客户同时存在满足条件的债券投资授信，系统自动将其额度纳入本次债券池授信额度，债券池授信金额以投资经理录入为准，审批通过后原债券投资授信额度自动失效。满足条件的单笔债券投资指：
     * ①债券投资的标准化 投资类型为：企业债、公司债、中期票据债、PPN、短期融资券、超短期融资券、项目收益券、项目收益票据、绿色债务融资工具；
     * ②债券投资的额度未到期
     * 在债券池额度审批通过后，将符合条件的债券投资额度状态调整为“失效已结清”，原债券投资额度项下的未结清的业务关联至新审批通过的债券池额度项下。
     * 同时通知ComStar系统将单笔债券投资产品授信金额调整为0，新增债券池额度。
     * 授信主体客户有债券池授信后，允许发起全类型的单笔债券投资授信额度，并与债券池授信额度无关。后续客户再次新增或调整债券池授信额度时，将符合条件的单笔债券投资额度纳入债券池授信额度中。
     * 4	企业债
     * E	公司债
     * N	中期票据
     * N1	PPN
     * 6	短期融资券
     * O	超短期融资券
     * G	项目收益债券
     * J	绿色债务融资工具
     * @返回值 挂在债券池下的未结清业务总额
     */
    public Map<String, Object> generateSigInvestToPool(CmisLmt0005ReqDto reqDto,  Map<String, String> resultMap){
        logger.info(this.getClass().getName()+"债券投资需挂靠到债券池，处理逻辑开始---------------> start ");
        Map<String, Object> result = new HashedMap() ;
        //获取客户信息
        String cusId = reqDto.getCusId() ;
        String accNo = reqDto.getAccNo() ;
        //债券投资授信查询：①债券投资的标准化 投资类型为：企业债、公司债、中期票据债、PPN、短期融资券、超短期融资券、项目收益券、项目收益票据、绿色债务融资工具；
        //     ②债券投资的额度未到期
        // update by lizx 2021-11-03 ②债券投资的额度未到期 该条件去除
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("cusId", cusId);
        //单笔投资业务类型  4002--债券投资
        queryModel.addCondition("limitSubNo", CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4002);
        //4企业债  E 公司债 N 中期票据 N1 PPN 6 短期融资券 O	超短期融资券 G 项目收益债券 J 绿色债务融资工具
        queryModel.addCondition("investType", CmisLmtConstants.STD_ZB_NORM_INVEST_TYPE_DMD) ;
        java.util.List<ApprLmtSubBasicInfo> apprLmtSubBasicInfoList = apprLmtSubBasicInfoService.selectSubBasicInfoByPool(queryModel) ;
        BigDecimal outTotal = BigDecimal.ZERO ;
        //用信余额
        BigDecimal outLoanBal = BigDecimal.ZERO ;

        //遍历符合条件的分项信息
        for (ApprLmtSubBasicInfo apprLmtSubBasicInfo : apprLmtSubBasicInfoList) {
            //判断分项是否存在未结清的业务  分项编号
            String lmtSubNo = apprLmtSubBasicInfo.getApprSubSerno() ;
            //将分项未结清的业务挂到债券池下
            java.util.List<LmtContRel> lmtContRels =lmtContRelMapper.selectUnSettleLmtContRel(lmtSubNo) ;
            //非空判断
            if(!CollectionUtils.isEmpty(lmtContRels)){
                //遍历额度占用分项信息处理
                for (LmtContRel lmtContRel : lmtContRels) {
                    outTotal = outTotal.add(BigDecimalUtil.replaceNull(lmtContRel.getBizTotalBalanceAmtCny())) ;
                    //额度占用关系挂在
                    lmtContRel.setLimitSubNo(accNo);
                    lmtContRelMapper.updateByPrimaryKey(lmtContRel) ;
                }
            }
            outLoanBal = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanBalance()) ;
            apprLmtSubBasicInfo.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_99);
            apprLmtSubBasicInfoMapper.updateByPrimaryKey(apprLmtSubBasicInfo) ;

            //获取主授信台账编号
            String fkId = apprLmtSubBasicInfo.getFkPkid() ;
            ApprStrMtableInfo apprStrMtableInfo = apprStrMtableInfoService.getAppStrMTableInfoBySerno(fkId) ;
            apprStrMtableInfo.setApprStatus(CmisLmtConstants.STD_ZB_APPR_ST_99);
            apprStrMtableInfoMapper.updateByPrimaryKey(apprStrMtableInfo) ;

            //查看债券透支是否占用同业融资类额度，如果占用进行恢复操作
            String dealBizNo = fkId ;
            //查询占用同业融资类额度信息
            java.util.List<LmtContRel> lmtContRelList = lmtContRelService.select4002ByDealBizNo(dealBizNo) ;
            if(!CollectionUtils.isEmpty(lmtContRelList)){
                for (LmtContRel lmtContRel : lmtContRelList) {
                    logger.info(this.getClass().getName()+"债券投资需挂靠到债券池，恢复同业融资类额度{}---------------> start ", lmtContRel.getLimitSubNo());
                    //获取同业融资额度分项编号
                    String apprSubSenro = lmtContRel.getLimitSubNo() ;
                    BigDecimal recoverBal = NumberUtils.nullDefaultZero(lmtContRel.getBizTotalBalanceAmtCny()) ;
                    BigDecimal recoverSpacBal = NumberUtils.nullDefaultZero(lmtContRel.getBizSpacBalanceAmtCny()) ;
                    //获取同业额度信息
                    ApprLmtSubBasicInfo apprBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(apprSubSenro) ;
                    //恢复额度
                    if(Objects.nonNull(apprBasicInfo)){
                        apprBasicInfo.setOutstndAmt(apprBasicInfo.getOutstndAmt().subtract(recoverBal));
                        apprBasicInfo.setSpacOutstndAmt(apprBasicInfo.getSpacOutstndAmt().subtract(recoverSpacBal));
                        apprBasicInfo.setUpdDate(reqDto.getInputDate());
                        apprBasicInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                        apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprBasicInfo, resultMap) ;
                    }
                    lmtContRel.setBizTotalBalanceAmtCny(BigDecimal.ZERO);
                    lmtContRel.setBizSpacBalanceAmtCny(BigDecimal.ZERO);
                    lmtContRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_300);
                    lmtContRel.setUpdDate(reqDto.getInputDate());
                    lmtContRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    lmtContRelService.updateByPKeyInApprLmtChgDetails(lmtContRel, resultMap) ;
                    logger.info(this.getClass().getName()+"债券投资需挂靠到债券池，恢复同业融资类额度{}---------------> end ", lmtContRel.getLimitSubNo());
                }
            }
        }
        result.put("outTotal",outTotal) ;
        result.put("outLoanBal",outLoanBal) ;
        result.put("apprLmtSubBasicInfoList",apprLmtSubBasicInfoList) ;
        logger.info(this.getClass().getName()+"债券投资需挂靠到债券池，处理逻辑结束---------------> end", JSON.toJSONString(result));
        return result ;
    }

    /**
     * 债券投资标准额度失效组装发送comstart 报文
     * @param com001ReqLists
     * @param count
     * @param apprLmtSubBasicInfoList
     */
    public void generateCom001ReqList(java.util.List<List> com001ReqLists, int count, java.util.List<ApprLmtSubBasicInfo> apprLmtSubBasicInfoList){
        if(!CollectionUtils.isEmpty(apprLmtSubBasicInfoList)){
            for (ApprLmtSubBasicInfo apprLmtSubBasicInfo : apprLmtSubBasicInfoList) {
                List com001ReqDtoList = new List();
                String assetNo = apprLmtSubBasicInfo.getAssetNo() ;
                if(!StringUtils.isEmpty(assetNo)){
                    count++ ;
                    //序号
                    com001ReqDtoList.setNumber(String.valueOf(count));
                    //授信批复编号
                    com001ReqDtoList.setOrigiAccNo(apprLmtSubBasicInfo.getApprSubSerno());
                    //授信分项ID
                    com001ReqDtoList.setLmtSubNo(apprLmtSubBasicInfo.getLimitSubNo());
                    //Ecif客户号
                    com001ReqDtoList.setCusId(apprLmtSubBasicInfo.getCusId());
                    //授信主体客户名称
                    com001ReqDtoList.setCusName(apprLmtSubBasicInfo.getCusName());
                    //额度到期日
                    com001ReqDtoList.setEndDate(apprLmtSubBasicInfo.getLmtDate());
                    //额度品种编号
                    com001ReqDtoList.setLimitSubNo(apprLmtSubBasicInfo.getLimitSubNo());
                    //额度品种名称
                    com001ReqDtoList.setLimitSubName(apprLmtSubBasicInfo.getLimitSubName());
                    //操作类型 默认 01-调整
                    com001ReqDtoList.setOptType(CmisLmtConstants.COMSTAR_OPT_TYPE_01);
                    com001ReqDtoList.setDeleteFlag(CmisLmtConstants.YES_NO_Y);
                    com001ReqDtoList.setLmtAmt(BigDecimal.ZERO);
                    com001ReqDtoList.setAssetNo(apprLmtSubBasicInfo.getAssetNo());
                    com001ReqDtoList.setLmtType(CmisLmtConstants.COMSTAR_LMT_TYPE_02);
                    logger.info(this.getClass().getName()+"纳入进债券池的债券投资额度List------>:"+ JSON.toJSONString(com001ReqDtoList));
                    com001ReqLists.add(com001ReqDtoList) ;
                }
            }
        }
    }

    /**
     * 占用同业管理类或投资类额度
     * @param reqDto
     * @param apprSubSerno
     */
    private void insertLmtContRel4SameOrg(CmisLmt0005ReqDto reqDto,String apprSubSerno){
        LmtContRel lmtContRel = new LmtContRel();
        //主键
        Map seqMap = new HashMap();
        String pkId = sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ, seqMap);
        lmtContRel.setPkId(pkId);
        //客户编号
        lmtContRel.setCusId(reqDto.getCusId());
        //客户名称
        lmtContRel.setCusName(reqDto.getCusName());
        //交易业务编号取批复台账编号
        lmtContRel.setDealBizNo(reqDto.getAccNo());
        //额度分项编号
        lmtContRel.setLimitSubNo(apprSubSerno);
        //系统编号
        lmtContRel.setSysId(reqDto.getSysId());
        //授信类型
        lmtContRel.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_07);
        //资产编号
        lmtContRel.setAssetNo(reqDto.getAssetNo());
        //交易业务类型  默认1-一般合同
        lmtContRel.setDealBizType(CmisLmtConstants.STD_ZB_CONT_TYPE_1);
        //业务属性 默认3--授信
        lmtContRel.setBizAttr(CmisLmtConstants.STD_ZB_BIZ_ATTR_3);
        //产品编号 默认额度品种编号
        lmtContRel.setPrdId(reqDto.getLimitSubNo());
        //产品名称  额度品种名称
        lmtContRel.setPrdName(reqDto.getLimitSubName());
        //合同金额取本次占用金额
        lmtContRel.setBizAmt(reqDto.getLmtAmt());
        //占用总金额（折人民币）取本次占用金额
        lmtContRel.setBizTotalAmtCny(reqDto.getLmtAmt());
        //占用敞口金额（折人民币）取本次占用金额
        lmtContRel.setBizSpacAmtCny(reqDto.getLmtAmt());
        //占用总余额（折人民币）取本次占用金额
        lmtContRel.setBizTotalBalanceAmtCny(reqDto.getLmtAmt());
        //占用敞口余额（折人民币）取本次占用金额
        lmtContRel.setBizSpacBalanceAmtCny(reqDto.getLmtAmt());
        //业务保证金比例
        lmtContRel.setSecurityRate(BigDecimal.ZERO);
        //业务保证金金额
        lmtContRel.setSecurityAmt(BigDecimal.ZERO);
        //合同起始日取授信起始日
        lmtContRel.setStartDate(reqDto.getStartDate());
        //合同到期日取授信到期日
        lmtContRel.setEndDate(reqDto.getEndDate());
        //交易业务状态 默认200--有效
        lmtContRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_200);
        //操作类型 默认01--新增
        lmtContRel.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        //登记人
        lmtContRel.setInputId(reqDto.getInputId());
        //登记机构
        lmtContRel.setInputBrId(reqDto.getInputBrId());
        //登记时间
        lmtContRel.setInputDate(reqDto.getInputDate());
        //最近更新人
        lmtContRel.setUpdId(reqDto.getInputId());
        //最近更新机构
        lmtContRel.setUpdBrId(reqDto.getInputBrId());
        //最近更新日期
        lmtContRel.setUpdDate(reqDto.getInputDate());
        //创建时间
        lmtContRel.setCreateTime(new Date());
        //修改时间
        lmtContRel.setUpdateTime(new Date());
        lmtContRel.setInvestAssetName(reqDto.getProName());

        //插入分项占用关系表
        lmtContRelMapper.insert(lmtContRel);
    }

    /**
     * 往批复分项表里插入关于自营金额和资管金额的记录
     * @param apprLmtSubBasicInfo
     * @param sobsOrAssetManaAmt
     * @param accNo
     * @param limitSubNo
     * @param limitSubName
     */
    private void insertApprLmtSubBasicInfo4SobsAndAssetMana(String isCreateAcc,ApprLmtSubBasicInfo apprLmtSubBasicInfo,String origiAccNo,BigDecimal sobsOrAssetManaAmt,String accNo,String limitSubNo,String limitSubName){
        //自营、资管新分项的已出账金额 默认为0
        BigDecimal pvpOutstndAmt = BigDecimal.ZERO;
        //新批复分项编号
        String newApprSubSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_ACC_DETAILS, new HashMap<>());

        if (CmisLmtConstants.YES_NO_Y.equals(isCreateAcc)) {
            //将原自营和资管批复分项下的未结清合同挂到新自营和资管批复分项下
            //1.查询原批复分项下的自营和资管批复分项
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("parentId", origiAccNo);
            queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
            queryModel.addCondition("limitSubNo", limitSubNo);
            queryModel.addCondition("status", CmisLmtConstants.STD_ZB_APPR_ST_01);

            java.util.List<ApprLmtSubBasicInfo> apprLmtSubBasicInfos = apprLmtSubBasicInfoService.selectByModel(queryModel);

            if (apprLmtSubBasicInfos != null && apprLmtSubBasicInfos.size() > 0) {
                //2.如果有，则先将自营和资管分项对应的未结清业务挂到新自营和资管分项下，然后将原自营和资管分项状态改为失效已结清
                ApprLmtSubBasicInfo oldApprLmtSubBasicInfo = apprLmtSubBasicInfos.get(0);

                String apprSubSerno = oldApprLmtSubBasicInfo.getApprSubSerno();

                //新分项已出账金额取原分项的已出账金额
                pvpOutstndAmt = oldApprLmtSubBasicInfo.getPvpOutstndAmt();

                /*java.util.List<LmtContRel> lmtContRelList = lmtContRelService.selectOutStandingLoadCont(apprSubSerno);
                //原分项批复下的业务挂到新分项下
                for (LmtContRel lmtContRel : lmtContRelList) {
                    lmtContRel.setLimitSubNo(newApprSubSerno);
                    lmtContRelMapper.updateByPrimaryKey(lmtContRel);
                }*/
                //查询原分项下业务，将未结清业务挂靠到新分项下
                comm4Service.updateLmtContRelRelSubNo(apprSubSerno, newApprSubSerno);

                //原批复状态更改为失效已结清
                oldApprLmtSubBasicInfo.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_99);
                apprLmtSubBasicInfoService.update(oldApprLmtSubBasicInfo);
                logger.info("CmisLmt0005 生成新批复分项，原批复分项编号：【" + oldApprLmtSubBasicInfo.getApprSubSerno() + "】信息，更新为【99】失效已结清！");
            }
        }

        apprLmtSubBasicInfo.setPkId(getPkId());
        apprLmtSubBasicInfo.setParentId(accNo);
        //授信总额
        apprLmtSubBasicInfo.setAvlAmt(sobsOrAssetManaAmt);
        //敞口可用额度
        apprLmtSubBasicInfo.setSpacAmt(sobsOrAssetManaAmt);
        //授信分项类型 默认 02-产品分项
        apprLmtSubBasicInfo.setLmtSubType(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_02);
        //已用总额默认为0
        apprLmtSubBasicInfo.setOutstndAmt(BigDecimal.ZERO);
        //敞口已用额度默认为0
        apprLmtSubBasicInfo.setSpacOutstndAmt(BigDecimal.ZERO);
        //已出帐金额 默认为0
        apprLmtSubBasicInfo.setPvpOutstndAmt(pvpOutstndAmt);
        //可出账金额
        apprLmtSubBasicInfo.setAvlOutstndAmt(sobsOrAssetManaAmt.subtract(pvpOutstndAmt));
        //用信敞口余额 默认为授信总额
        apprLmtSubBasicInfo.setLoanSpacBalance(sobsOrAssetManaAmt);
        //额度品种编号
        apprLmtSubBasicInfo.setLimitSubNo(limitSubNo);
        //额度品种名称
        apprLmtSubBasicInfo.setLimitSubName(limitSubName);

        //批复分项编号 需要生成
        apprLmtSubBasicInfo.setApprSubSerno(newApprSubSerno);

        apprLmtSubBasicInfoMapper.insert(apprLmtSubBasicInfo);
    }

    /**
     * 更新自营、资管分项的授信总额
     * @param origiAccNo
     * @param limitSubNo
     * @param sobsOrAssetManaAmt
     */
    private void updateAvlAmt4SobsAndAssetMana(String origiAccNo,String limitSubNo,BigDecimal sobsOrAssetManaAmt){
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("parentId", origiAccNo);
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        queryModel.addCondition("limitSubNo", limitSubNo);
        queryModel.addCondition("status", CmisLmtConstants.STD_ZB_APPR_ST_01);

        java.util.List<ApprLmtSubBasicInfo> apprLmtSubBasicInfos = apprLmtSubBasicInfoService.selectByModel(queryModel);

        if (apprLmtSubBasicInfos != null && apprLmtSubBasicInfos.size() > 0) {

            ApprLmtSubBasicInfo oldApprLmtSubBasicInfo = apprLmtSubBasicInfos.get(0);
            oldApprLmtSubBasicInfo.setAvlAmt(sobsOrAssetManaAmt);

            apprLmtSubBasicInfoMapper.updateByPrimaryKey(oldApprLmtSubBasicInfo);
        }
    }

    /**
     * 组织请求报文发送comstar
     * @param reqDto
     * @param limitSubNo
     * @param lmtAmt
     * @param sobsAmt
     * @param assetManaAmt
     */
    private void sendComstar(CmisLmt0005ReqDto reqDto,String limitSubNo,BigDecimal lmtAmt,BigDecimal sobsAmt,BigDecimal assetManaAmt,Map<String, Object> result){
        Com001ReqDto com001ReqDto = new Com001ReqDto();
        Data com001ReqDtoData = new Data();
        java.util.List<List> com001ReqLists = new ArrayList<>();
        List com001ReqDtoList = new List();
        int count = 1;

        //序号
        com001ReqDtoList.setNumber(String.valueOf(count));
        //授信批复编号
        com001ReqDtoList.setOrigiAccNo(reqDto.getAccNo());
        //授信分项ID
        com001ReqDtoList.setLmtSubNo(reqDto.getLimitSubNo());
        //Ecif客户号
        com001ReqDtoList.setCusId(reqDto.getCusId());
        //TODO 授信主体证件类型	 certType
        //com001ReqDtoList.setCertType();
        //TODO 授信主体证件号码	certCode
        //com001ReqDtoList.setCertCode();
        //授信主体客户名称
        com001ReqDtoList.setCusName(reqDto.getCusName());
        //额度到期日  Comstart日期 八位
        String endDate = reqDto.getEndDate() ;
        if(org.apache.commons.lang.StringUtils.isNotEmpty(endDate)){
            endDate = endDate.replace("-","") ;
        }
        com001ReqDtoList.setEndDate(endDate);
        //额度品种编号
        com001ReqDtoList.setLimitSubNo(limitSubNo);
        //额度品种名称
        com001ReqDtoList.setLimitSubName(reqDto.getLimitSubName());
        //操作类型 默认 01-调整
        com001ReqDtoList.setOptType(CmisLmtConstants.COMSTAR_OPT_TYPE_01);
        com001ReqDtoList.setLmtAmt(lmtAmt);

        com001ReqDtoList.setAssetNo(reqDto.getAssetNo());

        if (CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4001.equals(limitSubNo)) {
            //如果额度品种名称是债券池，则额度类型为01-同业综合授信额度
            com001ReqDtoList.setLmtType(CmisLmtConstants.COMSTAR_LMT_TYPE_01);
            com001ReqLists.add(com001ReqDtoList);

            if (sobsAmt !=null && sobsAmt.compareTo(BigDecimal.ZERO)>0) {
                count++;
                List com001ReqDtoSobsList = new List();
                BeanUtils.copyProperties(com001ReqDtoList, com001ReqDtoSobsList);
                //自营金额大于0，授信金额取自营金额，额度品种名称取400101-债券池自营额度
                com001ReqDtoSobsList.setNumber(String.valueOf(count));
                com001ReqDtoSobsList.setLmtAmt(sobsAmt);
                com001ReqDtoSobsList.setLimitSubNo(CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_400101);
                com001ReqDtoSobsList.setLimitSubName(CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_400101_NAME);
                com001ReqDtoSobsList.setLmtSubNo(CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_400101);
                com001ReqLists.add(com001ReqDtoSobsList);
            }

            if (assetManaAmt !=null && assetManaAmt.compareTo(BigDecimal.ZERO)>0) {
                count++;
                List com001ReqDtoAssetList = new List();
                BeanUtils.copyProperties(com001ReqDtoList, com001ReqDtoAssetList);
                //自营金额大于0，授信金额取自营金额，额度品种名称取400102-债券池资管额度
                com001ReqDtoAssetList.setNumber(String.valueOf(count));
                com001ReqDtoAssetList.setLmtAmt(assetManaAmt);
                com001ReqDtoAssetList.setLimitSubNo(CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_400102);
                com001ReqDtoAssetList.setLmtSubNo(CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_400102);
                com001ReqDtoAssetList.setLimitSubName(CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_400102_NAME);
                com001ReqLists.add(com001ReqDtoAssetList);
            }
            logger.info(this.getClass().getName()+"com001ReqLists------>:"+ JSON.toJSONString(com001ReqLists));

            //债券池额度，失效债券投资标准化额度
            if(!CollectionUtils.isEmpty(result)){
                java.util.List<ApprLmtSubBasicInfo> apprLmtSubBasicInfoList =
                        (java.util.List<ApprLmtSubBasicInfo>) result.get("apprLmtSubBasicInfoList");
                generateCom001ReqList(com001ReqLists,count,apprLmtSubBasicInfoList) ;
                logger.info(this.getClass().getName()+"增加纳入进债券池的债券投资额度后的com001ReqLists------>:"+ JSON.toJSONString(com001ReqLists));
            }
        }else {
            //额度类型 02-单笔投资业务额度
            com001ReqDtoList.setLmtType(CmisLmtConstants.COMSTAR_LMT_TYPE_02);
            com001ReqLists.add(com001ReqDtoList);
            logger.info(this.getClass().getName()+"com001ReqLists------>:"+ JSON.toJSONString(com001ReqLists));
        }

        //同业额度信息发给comstar
        if (com001ReqLists.size()>0){
            com001ReqDtoData.setList(com001ReqLists);
            com001ReqDto.setData(com001ReqDtoData);
            //交易流水号	serno
            Map seqMap = new HashMap();
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, seqMap);
            com001ReqDto.setSerno(serno);
            //金融机构代码
            com001ReqDto.setInstuCde(reqDto.getInstuCde());
            logger.info(this.getClass().getName()+",调用Com001接口，同步额度信息到Comstar开始------>:"+ JSON.toJSONString(com001ReqDto));
            Com001RespDto com001RespDto = com001Service.Com001(com001ReqDto);
            logger.info(this.getClass().getName()+",调用Com001接口，同步额度信息到Comstar结束------>:"+ JSON.toJSONString(com001RespDto));

            if (!SuccessEnum.SUCCESS.key.equals(com001RespDto.getErrorCode())){
                throw new YuspException(EclEnum.ECL070075.key, EclEnum.ECL070075.value+",错误信息："+com001RespDto.getErrorMsg());
            }
        }
    }

    /**
     * 产品授信需要占用同业投资类或同业管理类额度
     * @param reqDto
     * @param limitSubNo
     */
    private void generateLmtContRel4SameOrg(CmisLmt0005ReqDto reqDto,String limitSubNo,String isCreateAcc){
        //同业额度类型
        String lmtBizType ;
        //批复编号
        String accNo = reqDto.getAccNo();

        if (CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4003.equals(limitSubNo) || CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4004.equals(limitSubNo)
                || CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4005.equals(limitSubNo) || CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4006.equals(limitSubNo)
                || CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4007.equals(limitSubNo)) {
            //资产-其他、资产-债权融资计划、理财直融工具、净值型产品、资产证券化产品（标准）、资产证券化产品（非标）需要占用同业管理类额度
            lmtBizType = CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_3006;
        }else if (CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4001.equals(limitSubNo) || CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4002.equals(limitSubNo)
                ||CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4008.equals(limitSubNo)) {
            //债券池、其他标准化债权投资类需要占用同业投资类额度
            lmtBizType = CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_3002;
        }else{
            //不能匹配到要占用的额度，则不处理
            logger.info("CmisLmt0005 批复编号【"+accNo+"】，lmtBizType未匹配到要占用的额度。。。");
            return;
        }

        //获取要占用的同业客户的同业投资类额度/同业管理类额度分项
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId",reqDto.getCusId());
        queryModel.addCondition("limitSubNo",lmtBizType);
        queryModel.addCondition("oprType",CmisLmtConstants.OPR_TYPE_ADD);
        queryModel.addCondition("status",CmisLmtConstants.STD_ZB_APPR_ST_01);

        logger.info("CmisLmt0005 批复编号【"+accNo+"】，获取要占用的同业客户的同业投资类额度/同业管理类额度分项。。。");

        java.util.List<ApprLmtSubBasicInfo> apprLmtSubBasicInfos = apprLmtSubBasicInfoService.selectAll(queryModel);

        if (apprLmtSubBasicInfos==null || apprLmtSubBasicInfos.size()==0){
            throw new YuspException(EclEnum.ECL070103.key,EclEnum.ECL070103.value);
        }

        ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfos.get(0);

        //敞口金额
        BigDecimal spaceAmt = apprLmtSubBasicInfo.getSpacAmt();
        //敞口已用额度
        BigDecimal spacOutstndAmt = apprLmtSubBasicInfo.getSpacOutstndAmt();
        //批复分项编号
        String apprSubSerno = apprLmtSubBasicInfo.getApprSubSerno();
        //授信总额
        BigDecimal lmtAmt = reqDto.getLmtAmt();

        logger.info("CmisLmt0005 批复编号【"+accNo+"】，获取要占用的同业客户的同业投资类额度/同业管理类额度分项，该分项的敞口金额："+spaceAmt+"," +
                "敞口已用额度:"+spacOutstndAmt+",批复分项编号:"+apprSubSerno+"");

        /**
         * 获取产品授信占用同业客户额度的占用关系
         * 1、若结果为空，则证明该笔产品授信为授信新增，不存在占用同业客户额度
         * 2、若结果不为空，则存在需要恢复老产品授信占用同业客户额度，以新推送产品授信金额占用
         */
        QueryModel model = new QueryModel();
        model.addCondition("oprType",CmisLmtConstants.OPR_TYPE_ADD);
        model.addCondition("limitSubNo",apprSubSerno);
        //如果是以新批复覆盖老批复，则获取老批复台账占用同业客户额度的占用关系
        if(CmisLmtConstants.YES_NO_Y.equals(isCreateAcc)){
            model.addCondition("dealBizNo",reqDto.getOrigiAccNo());
        }else{
            //如果是授信新增/变更，则根据本次新推送批复台账编号查询是否存在占用同业客户额度信息
            model.addCondition("dealBizNo",reqDto.getAccNo());
        }
        java.util.List<LmtContRel> lmtContRels = lmtContRelService.selectAll(model);

        /**
         * 处理老产品授信与新产品授信占用同业客户额度关系
         * 1、若为新批复覆盖老批复：
         *      判断老产品授信占用同业客户余额恢复后，同业客户额度是否可足额给产品授信占用；
         *      若足额够新产品授信占用，则老产品授信批复占用同业客户额度的占用关系余额置为0，占用关系结清；
         *      创建新产品授信与同业客户额度的占用关系；
         *      更新同业客户已用总额、已用敞口；
         * 2、若不是新批复覆盖老批复，分为两种情况：
         *       情况一：纯新增的产品授信
         *          直接判断同业客户额度是否可足额给产品授信占用；
         *          若足额，则创建新产品授信与同业客户额度的占用关系；
         *          更新同业客户已用总额、已用敞口；
         *       情况二：更新原有产品授信
         *          判断原产品授信占用同业客户余额恢复后，同业客户额度是否可足额给更新后产品授信占用；
         *          若足额够新产品授信占用，更新产品授信与同业客户额度的占用关系：占用总额、占用总余额、占用敞口、占用敞口余额；
         *          更新同业客户已用总额、已用敞口；
         */
        /**
         * 情况一处理
         */
        logger.info("CmisLmt0005 批复编号【"+accNo+"】，纯新增的产品授信");
        if(lmtContRels==null || lmtContRels.size()<=0){
            /**
             * 第一步：判断同业客户额度是否可足额给产品授信占用；
             */
            //获取占用同业客户额度分项，分项的可用额度
            BigDecimal spacValAmt = spaceAmt.subtract(spacOutstndAmt);
            //如果授信总额超过批复分项敞口可用金额，则无法占用
            if (lmtAmt.compareTo(spacValAmt)>0){
                logger.info("CmisLmt0005 批复编号【"+accNo+"】，授信总额："+lmtAmt+"超过批复分项敞口可用金额:"+spacValAmt);
                throw new YuspException(EclEnum.ECL070102.key, EclEnum.ECL070102.value);
            }
            /**
             * 第二步：若足额，则创建新产品授信与同业客户额度的占用关系；
             */
            logger.info("CmisLmt0005 批复编号【"+accNo+"】，创建新产品授信与同业客户额度的占用关系开始");
            insertLmtContRel4SameOrg(reqDto,apprSubSerno);
            logger.info("CmisLmt0005 批复编号【"+accNo+"】，创建新产品授信与同业客户额度的占用关系结束");
            /**
             * 第三步：更新同业客户已用总额、已用敞口；
             */
            //已用金额 = 已用金额 + 占用总金额（折人民币）
            BigDecimal outstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getOutstndAmt()).add(lmtAmt);
            //敞口已用额度 = 敞口已用额度 + 占用敞口金额（折人民币）
            spacOutstndAmt = spacOutstndAmt.add(lmtAmt);
            //更新批复分项信息里的已用金额、敞口已用金额
            apprLmtSubBasicInfo.setOutstndAmt(outstndAmt);
            apprLmtSubBasicInfo.setSpacOutstndAmt(spacOutstndAmt);
            apprLmtSubBasicInfo.setUpdId(reqDto.getInputId());
            apprLmtSubBasicInfo.setUpdBrId(reqDto.getInputBrId());
            apprLmtSubBasicInfo.setUpdDate(reqDto.getInputDate());
            apprLmtSubBasicInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            apprLmtSubBasicInfoMapper.updateByPrimaryKey(apprLmtSubBasicInfo);
            logger.info("CmisLmt0005 批复编号【"+accNo+"】，更新批复分项信息里的已用金额、敞口已用金额成功");

        }else{// 情况二处理

            /**
             * 第一步：判断原产品授信占用同业客户余额恢复后，同业客户额度是否可足额给更新后产品授信占用；
             */
            //获取老产品授信占用同业客户额度分项占用余额
            LmtContRel lmtContRel = lmtContRels.get(0);
            BigDecimal olddBizSpacBalanceAmtCny = lmtContRel.getBizSpacBalanceAmtCny().compareTo(BigDecimal.ZERO)>0 ? lmtContRel.getBizSpacBalanceAmtCny() : BigDecimal.ZERO ;
            BigDecimal spacValAmt = spaceAmt.subtract(spacOutstndAmt);
            //如果授信总额超过批复分项敞口可用金额，则无法占用
            if (lmtAmt.compareTo(spacValAmt.add(olddBizSpacBalanceAmtCny))>0){
                logger.info("CmisLmt0005 批复编号【"+accNo+"】，授信总额："+lmtAmt+"超过spacValAmt:"+spacValAmt+"加olddBizSpacBalanceAmtCny："+olddBizSpacBalanceAmtCny);
                throw new YuspException(EclEnum.ECL070102.key, EclEnum.ECL070102.value);
            }
            /**
             * 第二步：若足额够新产品授信占用，更新产品授信与同业客户额度的占用关系：占用总额、占用总余额、占用敞口、占用敞口余额；
             */
            lmtContRel.setBizTotalAmtCny(lmtAmt);
            lmtContRel.setBizTotalBalanceAmtCny(lmtAmt);
            lmtContRel.setBizSpacAmtCny(lmtAmt);
            lmtContRel.setBizSpacBalanceAmtCny(lmtAmt);
            lmtContRel.setUpdId(reqDto.getInputId());
            lmtContRel.setUpdBrId(reqDto.getInputBrId());
            lmtContRel.setUpdDate(reqDto.getInputDate());
            lmtContRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            lmtContRelMapper.updateByPrimaryKey(lmtContRel);
            logger.info("CmisLmt0005 批复编号【"+accNo+"】，足额够新产品授信占用,更新产品授信与同业客户额度的占用关系");
            /**
             * 第三步：更新同业客户已用总额、已用敞口；
             */
            //已用金额 = 已用金额 + 占用总金额（折人民币）-原占用余额
            BigDecimal outstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getOutstndAmt()).add(lmtAmt).subtract(olddBizSpacBalanceAmtCny);
            //敞口已用额度 = 敞口已用额度 + 占用敞口金额（折人民币）
            spacOutstndAmt = spacOutstndAmt.add(lmtAmt).subtract(olddBizSpacBalanceAmtCny);
            //更新批复分项信息里的已用金额、敞口已用金额-原占用余额
            apprLmtSubBasicInfo.setOutstndAmt(outstndAmt);
            apprLmtSubBasicInfo.setSpacOutstndAmt(spacOutstndAmt);
            apprLmtSubBasicInfo.setUpdId(reqDto.getInputId());
            apprLmtSubBasicInfo.setUpdBrId(reqDto.getInputBrId());
            apprLmtSubBasicInfo.setUpdDate(reqDto.getInputDate());
            apprLmtSubBasicInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            apprLmtSubBasicInfoMapper.updateByPrimaryKey(apprLmtSubBasicInfo);
            logger.info("CmisLmt0005 批复编号【"+accNo+"】，更新同业客户已用总额、已用敞口；");
        }
    }
    /**
     * @作者:lizx
     * @方法名称: lmtIntendComstar
     * @方法描述:  同步资金同业，底层占用同业管理类额度（涉及货币基金），融资类额度和交易类额度，重新发送comstart 系统
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/7/10 11:17
     * @param apprLmtSubBasicInfo: 
     * @param lmtDetails: 
     * @return: void
     * @算法描述: 无
    */
    private void lmtIntendComstar(ApprLmtSubBasicInfo apprLmtSubBasicInfo, CmisLmt0005LmtDetailsListReqDto lmtDetails){
        logger.info("穿透化底层占用同业客户额度，分项编号【{}】，重新同步额度到comstart开始...", apprLmtSubBasicInfo.getApprSubSerno()) ;
        Com001ReqDto com001ReqDto = new Com001ReqDto();
        Data com001ReqDtoData = new Data();
        java.util.List<List> com001ReqLists = new ArrayList<>();
        List com001ReqDtoList = new List();
        //底层融资人批复分项编号
        String basicAccSubNo = lmtDetails.getBasicAccSubNo();
        int count = 1;
        //序号
        com001ReqDtoList.setNumber(String.valueOf(count));
        //授信批复编号
        com001ReqDtoList.setOrigiAccNo(apprLmtSubBasicInfo.getFkPkid());
        //授信分项ID
        com001ReqDtoList.setLmtSubNo(apprLmtSubBasicInfo.getApprSubSerno());
        //Ecif客户号
        com001ReqDtoList.setCusId(apprLmtSubBasicInfo.getCusId());

        CmisCus0010ReqDto cmisCus0010ReqDto = new CmisCus0010ReqDto();
        cmisCus0010ReqDto.setCusId(apprLmtSubBasicInfo.getCusId());
        ResultDto<CmisCus0010RespDto> cmisCus0010RespDtoResultDto = cmisCusClientService.cmiscus0010(cmisCus0010ReqDto);
        CmisCus0010RespDto cus0010RespDto = cmisCus0010RespDtoResultDto.getData();
        if (cus0010RespDto!=null){
            //授信主体证件类型	 certType
            com001ReqDtoList.setCertType(cus0010RespDto.getCertType());
            //授信主体证件号码	certCode
            com001ReqDtoList.setCertCode(cus0010RespDto.getCertCode());
        }

        //授信主体客户名称	cusName
        com001ReqDtoList.setCusName(apprLmtSubBasicInfo.getCusName());
        //额度到期日
        String endDate = apprLmtSubBasicInfo.getLmtDate() ;
        if(StringUtils.isNotEmpty(endDate)){
            endDate = endDate.replace("-","") ;
        }
        com001ReqDtoList.setEndDate(endDate);
        if(CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_3006.equals(apprLmtSubBasicInfo.getLimitSubNo())
                && CmisLmtConstants.STD_ZB_YES_NO_Y.equals(apprLmtSubBasicInfo.getIsIvlMf())){
            //额度品种编号
            com001ReqDtoList.setLimitSubNo(CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_300601);
            //额度品种名称
            com001ReqDtoList.setLimitSubName(CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_NAME_300601);
        }else{
            //额度品种编号
            com001ReqDtoList.setLimitSubNo(apprLmtSubBasicInfo.getLimitSubNo());
            //额度品种名称
            com001ReqDtoList.setLimitSubName(apprLmtSubBasicInfo.getLimitSubName());
        }
        //额度类型	lmtType 默认 01-同业综合授信额度
        com001ReqDtoList.setLmtType(CmisLmtConstants.COMSTAR_LMT_TYPE_01);
        //操作类型	optType 默认 01-调整
        com001ReqDtoList.setOptType(CmisLmtConstants.COMSTAR_OPT_TYPE_01);
        //查询分项向下，授信占用分项额度信息金额
        QueryModel queryModel1 = new QueryModel();
        queryModel1.addCondition("limitSubNo", basicAccSubNo);
        //资金占用分项额度
        BigDecimal sumBizTotalAmtCny = lmtContRelMapper.getSumBizTotalAmtCnyByLimitSubNo(queryModel1).get("bizTotalBalanceAmtCny");
        //穿透底层占用额度前分项金额
        BigDecimal subAvlAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getAvlAmt());
        BigDecimal subAfAvlAmt = subAvlAmt.subtract(sumBizTotalAmtCny) ;
        logger.info("穿透话底层占用同业客户额度，重新同步额度到comstart,占用前额度金额【{}】，占用后额度金额【{}】", subAvlAmt, subAfAvlAmt) ;
        com001ReqDtoList.setLmtAmt(subAfAvlAmt);
        com001ReqLists.add(com001ReqDtoList);

        //同业额度信息发给comstar
        if (com001ReqLists.size()>0){
            com001ReqDtoData.setList(com001ReqLists);
            com001ReqDto.setData(com001ReqDtoData);
            //交易流水号	serno
            Map seqMap = new HashMap();
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, seqMap);
            com001ReqDto.setSerno(serno);
            //金融机构代码
            com001ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);
            logger.info(this.getClass().getName()+",调用Com001接口，同步额度信息到Comstar开始------>:"+ JSON.toJSONString(com001ReqDto));
            Com001RespDto com001RespDto = com001Service.Com001(com001ReqDto);
            logger.info(this.getClass().getName()+",调用Com001接口，同步额度信息到Comstar结束------>:"+ JSON.toJSONString(com001RespDto));

            if (!SuccessEnum.SUCCESS.key.equals(com001RespDto.getErrorCode())){
                throw new YuspException(EclEnum.ECL070075.key, EclEnum.ECL070075.value+",错误信息："+com001RespDto.getErrorMsg());
            }
        }
    }
}