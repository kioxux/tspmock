/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtStrOrgConf;
import cn.com.yusys.yusp.service.LmtStrOrgConfService;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtStrOrgConfResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-05-06 09:48:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtstrorgconf")
public class LmtStrOrgConfResource {
    @Autowired
    private LmtStrOrgConfService lmtStrOrgConfService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtStrOrgConf>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtStrOrgConf> list = lmtStrOrgConfService.selectAll(queryModel);
        return new ResultDto<List<LmtStrOrgConf>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtStrOrgConf>> index(QueryModel queryModel) {
        List<LmtStrOrgConf> list = lmtStrOrgConfService.selectByModel(queryModel);
        return new ResultDto<List<LmtStrOrgConf>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtStrOrgConf> show(@PathVariable("pkId") String pkId) {
        LmtStrOrgConf lmtStrOrgConf = lmtStrOrgConfService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtStrOrgConf>(lmtStrOrgConf);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtStrOrgConf> create(@RequestBody LmtStrOrgConf lmtStrOrgConf) {
        lmtStrOrgConfService.insert(lmtStrOrgConf);
        return new ResultDto<LmtStrOrgConf>(lmtStrOrgConf);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtStrOrgConf lmtStrOrgConf) {
        int result = lmtStrOrgConfService.update(lmtStrOrgConf);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:insertSelective
     * @函数描述:对象新增，只插入非空字段
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertSelective")
    protected ResultDto<Integer> insertSelective(@RequestBody LmtStrOrgConf lmtStrOrgConf) {
        int result = lmtStrOrgConfService.insertSelective(lmtStrOrgConf);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody LmtStrOrgConf lmtStrOrgConf) {
        int result = lmtStrOrgConfService.updateSelective(lmtStrOrgConf);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtStrOrgConfService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtStrOrgConfService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
