package cn.com.yusys.yusp.service.server.cmislmt0010;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.resp.CmisLmt0010RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import com.alibaba.fastjson.JSON;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0010Service
 * @类描述: #对内服务类
 * @功能描述:台账校验 :台账占用前校验操作
 * @创建人: 李召星
 * @创建时间: 2021-05-07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */

@Service
public class CmisLmt0010Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0010Service.class);

    @Autowired
    private LmtContRelService lmtContRelService ;

    @Autowired
    private ApprCoopSubInfoService apprCoopSubInfoService ;

    @Autowired
    private Comm4Service comm4Service ;

    @Autowired
    private LmtWhiteInfoService lmtWhiteInfoService ;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;

    @Autowired
    private ContAccRelService contAccRelService ;

    @Autowired
    private ApprCoopInfoService apprCoopInfoService ;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /***
     * 台账校验：台账校验分为合作方、白名单、单一法人
     *          合作方：
     *          白名单：
     *          单一法人：
     * @param reqDto
     * @return
     */

    @Transactional
    public CmisLmt0010RespDto execute(CmisLmt0010ReqDto reqDto)  throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value);
        CmisLmt0010RespDto resqDto = new CmisLmt0010RespDto() ;
        try {
            //合同编号
            String bizNo = reqDto.getBizNo() ;
            //非空校验
            ifNotFiledCheck(reqDto) ;

            //通过合同编号，获取合同信息  TODO:没法标识查询的是台账还是合同，因为cmislmt0033 同业台账占用校验用的也是它
            List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelByDealBizNo(bizNo);
            if(CollectionUtils.isNotEmpty(lmtContRelList)){
                logger.info("台账校验开始，占用合同数据信息为：{}", JSON.toJSONString(lmtContRelList));
                for (LmtContRel lmtContRel : lmtContRelList) {
                    Optional.ofNullable(lmtContRel).orElseThrow(()->new YuspException(EclEnum.ECL070018.key,EclEnum.ECL070018.value));
                    //校验合同状态
                    if(!CmisLmtConstants.STD_ZB_BIZ_STATUS_200.equals(lmtContRel.getBizStatus())){
                        throw new YuspException(EclEnum.ECL070019.key, EclEnum.ECL070019.value);
                    }
                    //额度类型
                    String lmtType = lmtContRel.getLmtType() ;
                    //交易业务类型    1-一般合同  2-最高额合同  3-最高额授信协议
                    String dealBizType = lmtContRel.getDealBizType() ;
                    //分项编号
                    String lmtSubno = lmtContRel.getLimitSubNo() ;
                    //合作方
                    if(CmisLmtConstants.STD_ZB_LMT_TYPE_03.equals(lmtType)){
                        this.checkApprCoopSubInfo(reqDto, lmtSubno);
                    }else if(CmisLmtConstants.STD_ZB_LMT_TYPE_06.equals(lmtType)){
                        //白名单额度
                        checkLmtWhiteInfo(reqDto, lmtSubno);
                    }else{
                        checkApprLmtSubBasicInfo(reqDto,lmtType,dealBizType,lmtSubno) ;
                    }
                }
            }else{
                throw new YuspException(EclEnum.ECL070018.key,"【"+bizNo+"】"+EclEnum.ECL070018.value) ;
            }
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("台账校验接口报错：", e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value);
        return resqDto;
    }

    /**
     * 合作方额度校验
     * @param reqDto
     * @param lmtSubno
     */
    public void checkApprCoopSubInfo(CmisLmt0010ReqDto reqDto,String lmtSubno) {
        logger.info(this.getClass().getName()+":"+DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value+"，合作方额度校验。");
        //配置交易流水号
        String cfgTranAccNo = comm4Service.getSysParameterByName(CmisLmtConstants.TRAN_ACC_NO) ;
        //合同编号
        String bizNo = reqDto.getBizNo() ;
        //系统编号
        String sysId = reqDto.getSysId() ;
        //遍历台账明细
        for (CmisLmt0010ReqDealBizListDto dealBizInfo : reqDto.getCmisLmt0010ReqDealBizListDtoList()) {
            //交易流水号
            String tranAccNo = dealBizInfo.getDealBizNo() ;
            //产品类型属性
            String prdTypeProp = dealBizInfo.getPrdTypeProp() ;
            //判断改交易是否配置了，无条件通过
            if(comm4Service.checkStrInSourceStr(cfgTranAccNo, tranAccNo)){
                continue;
            }
            if(com.alibaba.excel.util.StringUtils.isEmpty(tranAccNo)){
                throw new YuspException(EclEnum.ECL070133.key, "台账编号" + EclEnum.ECL070133.value);
            }
            //获取台账到期日
            String endDate = dealBizInfo.getEndDate() ;
            //获取台账的起始日期
            String startDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT) ;
            //合作方分项信息
            ApprCoopSubInfo apprCoopSubInfo = apprCoopSubInfoService.selectAcsiInfoBySubSerno(lmtSubno);
            //获取合作放分项信息
            if (apprCoopSubInfo == null) {
                throw new YuspException(EclEnum.ECL070007.key, EclEnum.ECL070007.value);
            }

            //校验分项状态
            if (!CmisLmtConstants.STD_ZB_APPR_ST_01.equals(apprCoopSubInfo.getStatus())) {
                throw new YuspException(EclEnum.ECL070003.key, EclEnum.ECL070003.value);
            }

            if (comm4Service.checkLmtEndDate(startDate, endDate)) {
                throw new YuspException(EclEnum.ECL070127.key, EclEnum.ECL070127.value);
            }

            //根据批复台账编号，获取合作方批复台账信息
            ApprCoopInfo apprCoopInfo = apprCoopInfoService.selectACIBySerno(apprCoopSubInfo.getApprSerno()) ;
            String cusId = "" ;
            if(Objects.nonNull(apprCoopInfo)) {
                cusId = apprCoopInfo.getCusId() ;
            }

            /** 张家港市农业融资担保有限公司（客户号：8001092178，专业担保公司），
             * 信保贷产品使用该合作方担保额度时不做超额校验控制 ，出账时不做超额校验控制
             * 江苏扬帆广睿国际贸易有限公司（客户号：8000749823，集群贷市场方），合同占用合作方额度是，
             * 不做超额校验控制 ，出账时做超额校验控制**/
            if (comm4Service.checkSpecialCoopCus(apprCoopInfo.getCusId(), prdTypeProp, apprCoopSubInfo.getLimitSubNo())) return;

            /**************** 额度校验开始 *******************/
            //原交易业务编号
            String isRecolv = apprCoopSubInfo.getIsRevolv() ;
            BigDecimal loanSpacBal = BigDecimal.ZERO ;

            QueryModel model =  new QueryModel() ;
            model.addCondition("status", CmisLmtConstants.STD_ZB_BIZ_STATUS_STR);
            model.addCondition("limitSubNos", lmtSubno);
            //根据交易流水查选是否已经占用成功
            if(CmisLmtConstants.YES_NO_Y.equals(isRecolv)){
                loanSpacBal = contAccRelService.selectRevolvSpceBalByLimitSubNo(model) ;
            }else{
                loanSpacBal = contAccRelService.selectUnRevolvSpceBalByLimitSubNo(model) ;
            }

            BigDecimal recoverAmt = BigDecimal.ZERO ;
            if(comm4Service.judgeContDealIsSuss(tranAccNo, null,sysId)){
                //根据原业务编号，获取原业务信息
                ContAccRel contAccRel = contAccRelService.selectByTranAccNoAndDealNo(tranAccNo, null,sysId) ;
                if(Objects.nonNull(contAccRel)){
                    recoverAmt = NumberUtils.nullDefaultZero(contAccRel.getAccTotalAmtCny()) ;
                    //可出账金额+本次无缝衔接恢复金额
                    loanSpacBal = loanSpacBal.subtract(recoverAmt) ;
                    logger.info("台账占用总余额【dealBizAmtTotal】:"+loanSpacBal+" 重新出账，恢复已出账金额:"+contAccRel.getAccTotalAmtCny());
                }
            }
            //单笔限额，单户限额校验
            sigAndSigBussCheck(dealBizInfo, cusId, apprCoopSubInfo, apprCoopInfo, recoverAmt);

            //本次出账金额 + 已出账金额
            BigDecimal loanBalTotal = loanSpacBal.add(BigDecimalUtil.replaceNull(dealBizInfo.getDealBizAmt())) ;
            if(loanBalTotal.compareTo(apprCoopSubInfo.getAvlAmt())>0){
                throw new YuspException(EclEnum.ECL070006.key, "合作方：" + EclEnum.ECL070006.value);
            }
            /**************** 额度校验结束 *******************/
        }
    }
    /**
     * @作者:lizx
     * @方法名称: sigAndSigBussCheck
     * @方法描述:  单户限额，单笔限额校验
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/11/16 20:36
     * @param dealBizInfo:
     * @param cusId:
     * @param apprCoopSubInfo:
     * @param apprCoopInfo:
     * @param recoverAmt:
     * @return: void
     * @算法描述: 无
    */
    private void sigAndSigBussCheck(CmisLmt0010ReqDealBizListDto dealBizInfo, String cusId, ApprCoopSubInfo apprCoopSubInfo, ApprCoopInfo apprCoopInfo, BigDecimal recoverAmt) {
        //合作方类别 1-房地产开发商 不进行校验单核单笔限额
        String copType = apprCoopInfo.getCopType() ;
        //获取批复分项单户限额
        BigDecimal sigAmt = NumberUtils.nullDefaultZero(apprCoopSubInfo.getSigAmt()) ;
        //获取批复分项单笔限额
        BigDecimal sigBussAmt = NumberUtils.nullDefaultZero(apprCoopSubInfo.getSigBussAmt()) ;
        //占用总金额
        BigDecimal dealBizAmt = NumberUtils.nullDefaultZero(dealBizInfo.getDealBizAmt()) ;
        //占用敞口金额
        BigDecimal dealBizSpacAmt = NumberUtils.nullDefaultZero(dealBizInfo.getDealBizSpacAmt()) ;

        if(!CmisLmtConstants.STD_ZB_PARTNER_TYPE_1.equals(copType)){
            //单笔业务限额： 本次占用总额>= 单笔业务限额
            if(apprCoopSubInfo.getSigBussAmt()==null){//单笔业务限额如果为空，则不进行校验
                logger.info(EclEnum.ECL070108.value+",合作方批复台账编号：【"+ apprCoopInfo +"】");
            }else if(dealBizAmt.compareTo(sigBussAmt)>0){
                logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0010.key+":"+DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value+":" +
                        "本次占用金额【BizTotalAmt】："+ dealBizAmt+",大于单笔业务限额【sigBussAmt】："+ sigBussAmt);
                throw new YuspException(EclEnum.ECL070013.key, "合作方"+ EclEnum.ECL070013.value);
            }

            //判断单户限额如果为空，则不进行单户限额
            if(apprCoopSubInfo.getSigAmt()==null){
                logger.info(EclEnum.ECL070109.value+",合作方批复台账编号：【"+ apprCoopInfo +"】");
            }else{
                //单户限额：该客户合同占用总额LMT_CONT_REL 中占用总金额+本次占用 <= 单户限额
                //根据额度分项信息和客户信息，查询该客户分项占用总额 LMT_CONT_REL
                Map<String, BigDecimal> mapbiz = lmtContRelService.selectLmtBizTotal(apprCoopSubInfo.getApprSubSerno(), cusId) ;
                //获取批复下所有分项已用额度之和 occSubRel.getBizTotalAmt()
                BigDecimal bizTotal = mapbiz.get("bizTotal") ; // TODO 大小写转换问题遗留
                //已用额度之和 - 本次恢复金额
                bizTotal = bizTotal.subtract(recoverAmt) ;
                if((bizTotal.add(dealBizAmt)).compareTo(sigAmt)>0){
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0010.key+":"+DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value+":" +
                            "批复下所有分项该客户已用额度之和【bizTotal】："+bizTotal+"+本次占用金额【BizTotalAmt】"+ dealBizAmt+",大于单户限额【sigAmt】："+ sigAmt);
                    throw new YuspException(EclEnum.ECL070015.key, "合作方"+ EclEnum.ECL070015.value);
                }
            }
        }
    }

    /**
     * 单一法人额度校验
     * @param reqDto
     * @param lmtSubno
     */
    public void checkApprLmtSubBasicInfo(CmisLmt0010ReqDto reqDto, String lmtType, String dealBizType, String lmtSubno){
        logger.info(this.getClass().getName()+":"+DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value+"单一法人额度校验");
        //原交易业务编号
        String origiDealBizNo =  "" ;
        //法人分项信息
        ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(lmtSubno) ;
        if(apprLmtSubBasicInfo==null) throw new YuspException(EclEnum.ECL070007.key, EclEnum.ECL070007.value+"【"+lmtSubno+"】");
        //恢复总金额
        BigDecimal dealBizAmtTotal = BigDecimal.ZERO ;
        //恢复敞口金额
        BigDecimal dealBizSpacTotal = BigDecimal.ZERO ;
        //可出账金额
        BigDecimal avlOutstndAmt = apprLmtSubBasicInfo.getAvlOutstndAmt() ;
        //是否低风险
        String isLriskLmt =  apprLmtSubBasicInfo.getIsLriskLmt() ;
        //遍历台账明细
        List<CmisLmt0010ReqDealBizListDto> cmisLmt0010ReqDealBizListDtoList = reqDto.getCmisLmt0010ReqDealBizListDtoList() ;

        //状态
        String status = apprLmtSubBasicInfo.getStatus() ;
        if(!CmisLmtConstants.STD_ZB_APPR_ST_01.equals(status)){
            throw new YuspException(EclEnum.ECL070003.key, EclEnum.ECL070003.value);
        }

        //配置交易流水号
        String cfgTranAccNo = comm4Service.getSysParameterByName(CmisLmtConstants.TRAN_ACC_NO) ;

        for(CmisLmt0010ReqDealBizListDto dealBizInfo:cmisLmt0010ReqDealBizListDtoList){
            logger.info("额度校验循环处理开始--------------------------------start");

            BigDecimal avlRecoverAmt = BigDecimal.ZERO ;

            //交易流水号
            String tranAccNo = dealBizInfo.getDealBizNo() ;
            //判断改交易是否配置了，无条件通过
            if(comm4Service.checkStrInSourceStr(cfgTranAccNo, tranAccNo)){
                logger.info("该笔交易已配置参数{}，无条件通过处理", JSON.toJSONString(cfgTranAccNo));
                return ;
            }
            //合同编号
            String bizNo = reqDto.getBizNo() ;
            //系统编号
            String sysId = reqDto.getSysId() ;
            if(com.alibaba.excel.util.StringUtils.isEmpty(tranAccNo)){
                throw new YuspException(EclEnum.ECL070133.key, "台账编号" + EclEnum.ECL070133.value);
            }

            //产品编号 prdNo TODO：产品编号是否需要根据额度体系进行校验对应额度中是否有该产品？
            String prdNo = dealBizInfo.getPrdId() ;
            //获取台账到期日
            String endDate = dealBizInfo.getEndDate() ;
            //台账占用总金额
            dealBizAmtTotal = BigDecimalUtil.replaceNull(dealBizAmtTotal.add(dealBizInfo.getDealBizAmt())) ;
            //台账占用敞口金额
            dealBizSpacTotal = BigDecimalUtil.replaceNull(dealBizSpacTotal.add(dealBizInfo.getDealBizSpacAmt())) ;
            logger.info("台账占用总金额【dealBizAmtTotal】:"+dealBizAmtTotal+"台账占用敞口金额【dealBizSpacTotal】:"+dealBizSpacTotal);

            //白名单 || 最高额合同  || 最高额协议 不校验到期日
            //配置产品
            String prdId = dealBizInfo.getPrdId() ;
            String cfgPrdId = comm4Service.getSysParameterByName(CmisLmtConstants.PRD_ID_CONT_UNCHECK) ;
            if(!(CmisLmtConstants.STD_ZB_LMT_TYPE_06.equals(lmtType)
                    || CmisLmtConstants.STD_ZB_CONT_TYPE_2.equals(dealBizType) || CmisLmtConstants.STD_ZB_CONT_TYPE_3.equals(dealBizType) ||
                    comm4Service.checkStrInSourceStr(cfgPrdId, prdId)
            )){
                // 法人分项信息 分项实际到期日
                if (null != apprLmtSubBasicInfo){
                    Integer lmtGraper = apprLmtSubBasicInfo.getLmtGraper() ;
                    if(lmtGraper == null){
                        lmtGraper = 0 ;
                    }

                    String  lmtDate = apprLmtSubBasicInfo.getLmtDate();
                    if(StringUtils.isBlank(lmtDate)){
                        throw new YuspException(EclEnum.ECL070128.key,EclEnum.ECL070128.value);
                    }

                    String actualEndDae =comm4Service.getEndDateAddGaraperDate(apprLmtSubBasicInfo.getLmtDate(), lmtGraper) ;
                    //校验分项到期日
                    if(StringUtils.isNotEmpty(endDate) && comm4Service.checkLmtEndDate(endDate, actualEndDae)){
                        throw new YuspException(EclEnum.ECL070020.key, EclEnum.ECL070020.value);
                    }
                }
            }else{
                logger.info("额度类型{},合同类型{},产品{} 不校验到期日处理--------------------", lmtType, dealBizType, prdId);
            }

            //低风险处理
            if(CmisLmtConstants.YES_NO_Y.equals(isLriskLmt)){
                /**
                 * 法人客户进行校验
                 * 规则：客户批复累加授信总额+授信总额部分，不允许超过客户上年度销售总额和当年销售总额较大值（纳税申报表）的N倍，
                 * 此处【补充保证金释放金额倍数】作为额度系统参数设置，初始值为【2】  客户销售总额查询  FY_BAIL_REL_RATE
                 */
                logger.info("低风险校验额度处理--------------------start");
                String checkTotalSales = comm4Service.getSysParameterByName(CmisLmtConstants.CHECK_TOTAL_SALES) ;
                //是否校验授信总额参数配置  1-校验 0-不校验
                logger.info("是否校验授信总额参数配置{},【1-校验 0-不校验】--------------------", JSON.toJSONString(checkTotalSales));
                if(CmisLmtConstants.YES_NO_Y.equals(checkTotalSales)){
                    if(CmisLmtConstants.STD_ZB_CUS_TYPE_2.equals(apprLmtSubBasicInfo.getCusType())){
                        logger.info("校验授信总额n倍--------------------start");
                        BigDecimal bailRate = BigDecimalUtil.replaceNull(comm4Service.getSysParameterByName(CmisLmtConstants.FY_BAIL_REL_RATE)) ;
                        BigDecimal statYearAmtMax = comm4Service.getStatYearAmtMax(dealBizInfo.getCusId()) ;

                        Map<String, BigDecimal> amtMap = apprLmtSubBasicInfoService.selectLmtTotalByCusId(dealBizInfo.getCusId(), reqDto.getInstuCde()) ;
                        //授信累加
                        BigDecimal avlAmt = amtMap.get("avlAmt") ;
                        //授信总额
                        BigDecimal lmtAmtAdd = amtMap.get("lmtAmtAdd") ;
                        BigDecimal lmtTotal = avlAmt.add(lmtAmtAdd).add(dealBizAmtTotal) ;
                        if(lmtTotal.compareTo((statYearAmtMax.multiply(bailRate)))>0){
                            logger.info("cmislmt0010 额度分项编号【"+apprLmtSubBasicInfo.getApprSubSerno()+"】，" +
                                    "授信总额【avlAmt】:"+avlAmt+"+授信累加【lmtAmtAdd】:"+lmtAmtAdd+
                                    "大于客户上年度销售总额和当年销售总额较大值【statYearAmtMax】"+statYearAmtMax+"*补充保证金释放金额倍数【bailRate】："+bailRate);
                            throw new YuspException(EclEnum.ECL070078.key, EclEnum.ECL070078.value+"【"+bailRate+"】倍");
                        }
                    }
                }
                logger.info("校验授信总额n倍--------------------end");
                return ;
            }

            //原交易业务编号
            origiDealBizNo = dealBizInfo.getOrigiDealBizNo() ;
            //根据交易流水查选是否已经占用成功
            if(comm4Service.judgeContDealIsSuss(tranAccNo, null,sysId)){
                //根据原业务编号，获取原业务信息
                ContAccRel contAccRel = contAccRelService.selectByTranAccNoAndDealNo(tranAccNo, null,sysId) ;
                if(Objects.nonNull(contAccRel)){
                    //可出账金额+已出账金额
                    /*dealBizSpacTotal = dealBizSpacTotal.subtract(contAccRel.getAccSpacAmtCny()) ;
                    logger.info("无缝衔接后，台账占用总金额【dealBizAmtTotal】:"+dealBizAmtTotal+"台账占用敞口金额【dealBizSpacTotal】:"+dealBizSpacTotal
                            +"可出账金额【avlOutstndAmt】："+avlOutstndAmt);*/
                    avlRecoverAmt = NumberUtils.nullDefaultZero(contAccRel.getAccSpacAmtCny()) ;
                    logger.info("该笔出账已占用一次，需恢复可出账金额为：{}", avlRecoverAmt );
                }
            }

            //是否无缝衔接
            String isFollowBiz = dealBizInfo.getIsFollowBiz() ;
            /**是无缝衔接 :
             * 根据原交易流水获取到，原台账信息，获取台账余额 bal
             * 更具分项判断分项是否可循环，如果是可循环，无论是一般合同、最高额合同和最高额协议，全部恢复可出账金额
             * **/
            if(CmisLmtConstants.YES_NO_Y.equals(isFollowBiz)){
                logger.info("无缝衔接处理（不校验额度）-------------------");
                //根据原业务编号，获取原业务信息
                ContAccRel contAccRel = contAccRelService.selectContAccRelByTranAccNo(origiDealBizNo) ;
                if(Objects.nonNull(contAccRel)){
                    //可出账金额+本次无缝衔接恢复金额
                   /* dealBizSpacTotal=dealBizSpacTotal.subtract(contAccRel.getAccSpacAmtCny()) ;
                    logger.info("无缝衔接后，台账占用总金额【dealBizAmtTotal】:"+dealBizAmtTotal+"台账占用敞口金额【dealBizSpacTotal】:"+dealBizSpacTotal
                            +"可出账金额【avlOutstndAmt】："+avlOutstndAmt);*/
                    //后期如需要放开无缝衔接，需考虑分项额度是否可循环，可循环取余额，不可循环取总额
                    avlRecoverAmt = NumberUtils.nullDefaultZero(contAccRel.getAccSpacBalanceAmtCny()) ;
                    logger.info("无缝衔接，需恢复可出账金额为：{}", avlRecoverAmt );
                }
                //无缝衔接，不校验额度信息数据
                break ;
            }

            //如果是最高额协议
            if(CmisLmtConstants.STD_ZB_CONT_TYPE_3.equals(dealBizType)){
                logger.info("最高额协议处理开始-------------------start");
                //分项信息
                String parentId = "";
                if(null != apprLmtSubBasicInfo){
                    parentId = apprLmtSubBasicInfo.getApprSubSerno() ;
                }else{
                    throw new RuntimeException("未查询到批复额度分项基础信息！");
                }
                String cusType = apprLmtSubBasicInfo.getCusType() ;
                String limitStrNo = comm4Service.getLimitStrNo(cusType) ;
                String limitSubNo = comm4Service.getLimitSubNoByPrdIdAndStrNo(prdNo, limitStrNo) ;
                //产品类型属性
                String prdTypeProp = dealBizInfo.getPrdTypeProp() ;
                if(StringUtils.isBlank(prdTypeProp)) prdTypeProp = "" ;
                //法人分项信息
                apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectByPrdAndPraParam(parentId, limitSubNo, prdTypeProp) ;
                //校验获取的二级分项是否为空
                Optional.ofNullable(apprLmtSubBasicInfo).orElseThrow(()->new YuspException(EclEnum.ECL070081.key,EclEnum.ECL070081.value));
                //单一法人校验，校验分项可出账金额>=台账明细占用总额之和 TODO:此处是否更改为 本次额度占用金额<= 可出账金额即可
                if(CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(lmtType)){
                    BigDecimal avlOutstndAmtDetails = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getAvlOutstndAmt()).add(avlRecoverAmt) ;
                    if(avlOutstndAmtDetails.compareTo(dealBizSpacTotal)<0){
                        logger.info("最高额明细可出账金额:"+avlOutstndAmtDetails+"，小于本次占用金额之和【dealBizAmtTotal】"+dealBizSpacTotal);
                        throw new YuspException(EclEnum.ECL070071.key, "最高额明细可出账金额:"+avlOutstndAmtDetails+"，小于本次占用金额"+dealBizSpacTotal);
                    }
                }
                logger.info("最高额协议处理开始-------------------end");
            }else{
                logger.info("台账校验，校验分项层次数据信息--------start");
                //不是最高额协议，先校验一级分项，最后校验二级分项
                String parentId = apprLmtSubBasicInfo.getParentId() ;
                //法人分项信息
                ApprLmtSubBasicInfo lmtSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(parentId) ;
                if(Objects.nonNull(lmtSubBasicInfo)){
                    //分项可出账
                    if(CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(lmtType)){
                        BigDecimal oneAvlOutstndAmt = NumberUtils.nullDefaultZero(lmtSubBasicInfo.getAvlOutstndAmt()).add(avlRecoverAmt) ;
                        if(oneAvlOutstndAmt.compareTo(dealBizSpacTotal)<0){
                            logger.info("分项可出账金额:"+oneAvlOutstndAmt+"，小于本次占用金额之和【dealBizAmtTotal】"+dealBizSpacTotal);
                            throw new YuspException(EclEnum.ECL070071.key, "分项可出账金额:"+oneAvlOutstndAmt+"，小于本次占用金额"+dealBizSpacTotal);
                        }
                    }
                }
                logger.info("台账校验，校验分项层次数据信息--------end");
            }

            //单一法人校验，校验分项可出账金额>=台账明细占用总额之和 TODO:此处是否更改为 本次额度占用金额<= 可出账金额即可
            if(CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(lmtType)){
                avlOutstndAmt = avlOutstndAmt.add(avlRecoverAmt) ;
                if(avlOutstndAmt.compareTo(dealBizSpacTotal)<0){
                    logger.info("分项明细可出账金额:"+avlOutstndAmt+"，小于本次占用金额【dealBizSpacTotal】"+dealBizSpacTotal);
                    throw new YuspException(EclEnum.ECL070071.key, "分项明细可出账金额:"+avlOutstndAmt+"，小于本次占用金额"+dealBizSpacTotal);
                }

                // 校验客户层级额度是否足额处理   获取客户向下分项层可出账金额
                logger.info("校验客户级别是否足额-------------------start");
                String cusId = apprLmtSubBasicInfo.getCusId() ;
                String fkPkId = apprLmtSubBasicInfo.getFkPkid() ;
                Map<String, BigDecimal> avlAmtMap = apprLmtSubBasicInfoService.selectAvlOutstndAmtByCusId(cusId, fkPkId) ;
                BigDecimal avlAmtCusId = avlAmtMap.get("avlOutstndAmt").add(avlRecoverAmt) ; ;
                if(avlAmtCusId.compareTo(dealBizSpacTotal)<0){
                    logger.info("客户可出账金额:"+avlAmtCusId+"，小于本次占用金额之和【dealBizSpacTotal】"+dealBizSpacTotal);
                    throw new YuspException(EclEnum.ECL070071.key, "客户可出账金额:"+avlAmtCusId+"，小于本次占用金额"+dealBizSpacTotal);
                }
                logger.info("校验客户级别是否足额-------------------end");
            }
        }
    }

    /**
     *白名单额度校验，单一法人额度，台账占用校验，校验到日期
     * @param lmtSubno
     * @param reqDto
     */
    public void checkLmtWhiteInfo(CmisLmt0010ReqDto reqDto, String lmtSubno){
        logger.info(this.getClass().getName()+":"+DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value + "白名单额度校验。" );
        LmtWhiteInfo lmtWhiteInfo = lmtWhiteInfoService.selectLmtWhiteInfoBySubNo(lmtSubno);
        //配置交易流水号
        String cfgTranAccNo = comm4Service.getSysParameterByName(CmisLmtConstants.TRAN_ACC_NO) ;
        //遍历台账明细
        for(CmisLmt0010ReqDealBizListDto dealBizInfo:reqDto.getCmisLmt0010ReqDealBizListDtoList()){
            //交易流水号
            String tranAccNo = dealBizInfo.getDealBizNo() ;
            //判断改交易是否配置了，无条件通过
            if(comm4Service.checkStrInSourceStr(cfgTranAccNo, tranAccNo)){
                continue;
            }
            //获取台账到期日
            String endDate = dealBizInfo.getEndDate() ;
            if(com.alibaba.excel.util.StringUtils.isEmpty(tranAccNo)){
                throw new YuspException(EclEnum.ECL070133.key, "台账编号" + EclEnum.ECL070133.value);
            }

            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            logger.info("签订日期{}, 白名单到期日{}", openDay, lmtWhiteInfo.getEndDate());
            //白名单额度 校验分项到期日
            if(lmtWhiteInfo!=null && StringUtils.isNotEmpty(lmtWhiteInfo.getEndDate()) && comm4Service.checkLmtEndDate(openDay, lmtWhiteInfo.getEndDate())){
                throw new YuspException(EclEnum.ECL070146.key, EclEnum.ECL070146.value);
            }
        }
    }

    /**
     * @作者:lizx
     * @方法名称: ifNotFiledCheck
     * @方法描述:  必输项目校验
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/9/30 0:13
     * @param reqDto:
     * @return: void
     * @算法描述: 无
     */
    public void ifNotFiledCheck(CmisLmt0010ReqDto reqDto){
        //系统编号不允许为空
        if(StringUtils.isEmpty(reqDto.getSysId())) throw new YuspException(EclEnum.ECL070133.key, "系统编号" + EclEnum.ECL070133.value) ;
        if(StringUtils.isEmpty(reqDto.getInstuCde())) throw new YuspException(EclEnum.ECL070115.key, "金融机构代码不能为空" + EclEnum.ECL070133.value) ;
        if(StringUtils.isEmpty(reqDto.getBizNo())) throw new YuspException(EclEnum.ECL070133.key, "合同编号" + EclEnum.ECL070133.value) ;
        //校验占用列表不允许为空
        List<CmisLmt0010ReqDealBizListDto> cmisLmt0010ReqDealBizListDtoList = reqDto.getCmisLmt0010ReqDealBizListDtoList() ;
        //DealBizList 台账明细 不允许为空
        if(CollectionUtils.isEmpty(cmisLmt0010ReqDealBizListDtoList)){
            throw new YuspException(EclEnum.ECL070122.key, EclEnum.ECL070122.value);
        }
        //占用额度列表非空校验
        for(CmisLmt0010ReqDealBizListDto occRelListReqDto : cmisLmt0010ReqDealBizListDtoList){
            if(StringUtils.isEmpty(occRelListReqDto.getDealBizNo())) throw new YuspException(EclEnum.ECL070133.key, "台账编号" + EclEnum.ECL070133.value) ;
            //if(StringUtils.isEmpty(occRelListReqDto.getCusId())) throw new YuspException(EclEnum.ECL070133.key, "客户编号" + EclEnum.ECL070133.value) ;
            if(StringUtils.isEmpty(occRelListReqDto.getPrdId())) throw new YuspException(EclEnum.ECL070133.key, "产品编号" + EclEnum.ECL070133.value) ;
        }
    }
}
