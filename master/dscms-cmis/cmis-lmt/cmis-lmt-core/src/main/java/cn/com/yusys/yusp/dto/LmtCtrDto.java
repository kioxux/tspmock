package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtCtr
 * @类描述: lmt_ctr数据实体类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-02-01 16:40:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtCtrDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 授信协议编号 **/
	private String lmtCtrNo;
	
	/** 全局流水号 **/
	private String serno;
	
	/** 授信申请流水号 **/
	private String lmtSerno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 授信类型 STD_ZB_LMT_TYP **/
	private String lmtType;
	
	/** 原授信协议编号 **/
	private String oldLmtCtrNo;
	
	/** 币种 STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 授信总额度 **/
	private java.math.BigDecimal lmtTotlAmt;
	
	/** 循环额度 **/
	private java.math.BigDecimal revolvAmt;
	
	/** 一次性额度 **/
	private java.math.BigDecimal oneAmt;
	
	/** 低风险业务类型  STD_ZB_LOW_RISK_TYP **/
	private String lowRiskType;
	
	/** 是否曾被拒绝 **/
	private String isHasRefused;
	
	/** 授信冻结额度 **/
	private java.math.BigDecimal lmtFrozeAmt;
	
	/** 授信起始日期 **/
	private String lmtStarDate;
	
	/** 授信到期日期 **/
	private String lmtEndDate;
	
	/** 授信期限类型 STD_ZB_CHAGE_TYP **/
	private String lmtTermType;
	
	/** 授信期限 **/
	private Integer lmtTerm;
	
	/** 最近冻结日期 **/
	private String frozeDate;
	
	/** 最近解冻日期 **/
	private String unfrozeDate;
	
	/** 注销日期 **/
	private String logoutDate;
	
	/** 渠道来源 STD_ZB_CHNL_SOUR **/
	private String chnlSour;
	
	/** 备注 **/
	private String remark;
	
	/** 授信审批通知书有效期 **/
	private String lmtDocIdate;
	
	/** 主办机构 **/
	private String managerBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 协议状态 STD_ZB_CTR_ST **/
	private String agrStatus;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param lmtSerno
	 */
	public void setLmtSerno(String lmtSerno) {
		this.lmtSerno = lmtSerno == null ? null : lmtSerno.trim();
	}
	
    /**
     * @return LmtSerno
     */	
	public String getLmtSerno() {
		return this.lmtSerno;
	}
	
	/**
	 * @param lmtCtrNo
	 */
	public void setLmtCtrNo(String lmtCtrNo) {
		this.lmtCtrNo = lmtCtrNo == null ? null : lmtCtrNo.trim();
	}
	
    /**
     * @return LmtCtrNo
     */	
	public String getLmtCtrNo() {
		return this.lmtCtrNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param lmtType
	 */
	public void setLmtType(String lmtType) {
		this.lmtType = lmtType == null ? null : lmtType.trim();
	}
	
    /**
     * @return LmtType
     */	
	public String getLmtType() {
		return this.lmtType;
	}
	
	/**
	 * @param oldLmtCtrNo
	 */
	public void setOldLmtCtrNo(String oldLmtCtrNo) {
		this.oldLmtCtrNo = oldLmtCtrNo == null ? null : oldLmtCtrNo.trim();
	}
	
    /**
     * @return OldLmtCtrNo
     */	
	public String getOldLmtCtrNo() {
		return this.oldLmtCtrNo;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param lmtTotlAmt
	 */
	public void setLmtTotlAmt(java.math.BigDecimal lmtTotlAmt) {
		this.lmtTotlAmt = lmtTotlAmt;
	}
	
    /**
     * @return LmtTotlAmt
     */	
	public java.math.BigDecimal getLmtTotlAmt() {
		return this.lmtTotlAmt;
	}
	
	/**
	 * @param revolvAmt
	 */
	public void setRevolvAmt(java.math.BigDecimal revolvAmt) {
		this.revolvAmt = revolvAmt;
	}
	
    /**
     * @return RevolvAmt
     */	
	public java.math.BigDecimal getRevolvAmt() {
		return this.revolvAmt;
	}
	
	/**
	 * @param oneAmt
	 */
	public void setOneAmt(java.math.BigDecimal oneAmt) {
		this.oneAmt = oneAmt;
	}
	
    /**
     * @return OneAmt
     */	
	public java.math.BigDecimal getOneAmt() {
		return this.oneAmt;
	}
	
	/**
	 * @param lowRiskType
	 */
	public void setLowRiskType(String lowRiskType) {
		this.lowRiskType = lowRiskType == null ? null : lowRiskType.trim();
	}
	
    /**
     * @return LowRiskType
     */	
	public String getLowRiskType() {
		return this.lowRiskType;
	}
	
	/**
	 * @param isHasRefused
	 */
	public void setIsHasRefused(String isHasRefused) {
		this.isHasRefused = isHasRefused == null ? null : isHasRefused.trim();
	}
	
    /**
     * @return IsHasRefused
     */	
	public String getIsHasRefused() {
		return this.isHasRefused;
	}
	
	/**
	 * @param lmtFrozeAmt
	 */
	public void setLmtFrozeAmt(java.math.BigDecimal lmtFrozeAmt) {
		this.lmtFrozeAmt = lmtFrozeAmt;
	}
	
    /**
     * @return LmtFrozeAmt
     */	
	public java.math.BigDecimal getLmtFrozeAmt() {
		return this.lmtFrozeAmt;
	}
	
	/**
	 * @param lmtStarDate
	 */
	public void setLmtStarDate(String lmtStarDate) {
		this.lmtStarDate = lmtStarDate == null ? null : lmtStarDate.trim();
	}
	
    /**
     * @return LmtStarDate
     */	
	public String getLmtStarDate() {
		return this.lmtStarDate;
	}
	
	/**
	 * @param lmtEndDate
	 */
	public void setLmtEndDate(String lmtEndDate) {
		this.lmtEndDate = lmtEndDate == null ? null : lmtEndDate.trim();
	}
	
    /**
     * @return LmtEndDate
     */	
	public String getLmtEndDate() {
		return this.lmtEndDate;
	}
	
	/**
	 * @param lmtTermType
	 */
	public void setLmtTermType(String lmtTermType) {
		this.lmtTermType = lmtTermType;
	}

	/**
     * @return lmtTermType
     */
	public String getLmtTermType() {
		return lmtTermType;
	}

	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}
	
    /**
     * @return LmtTerm
     */	
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param frozeDate
	 */
	public void setFrozeDate(String frozeDate) {
		this.frozeDate = frozeDate == null ? null : frozeDate.trim();
	}
	
    /**
     * @return FrozeDate
     */	
	public String getFrozeDate() {
		return this.frozeDate;
	}
	
	/**
	 * @param unfrozeDate
	 */
	public void setUnfrozeDate(String unfrozeDate) {
		this.unfrozeDate = unfrozeDate == null ? null : unfrozeDate.trim();
	}
	
    /**
     * @return UnfrozeDate
     */	
	public String getUnfrozeDate() {
		return this.unfrozeDate;
	}
	
	/**
	 * @param logoutDate
	 */
	public void setLogoutDate(String logoutDate) {
		this.logoutDate = logoutDate == null ? null : logoutDate.trim();
	}
	
    /**
     * @return LogoutDate
     */	
	public String getLogoutDate() {
		return this.logoutDate;
	}
	
	/**
	 * @param chnlSour
	 */
	public void setChnlSour(String chnlSour) {
		this.chnlSour = chnlSour == null ? null : chnlSour.trim();
	}
	
    /**
     * @return ChnlSour
     */	
	public String getChnlSour() {
		return this.chnlSour;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param lmtDocIdate
	 */
	public void setLmtDocIdate(String lmtDocIdate) {
		this.lmtDocIdate = lmtDocIdate == null ? null : lmtDocIdate.trim();
	}
	
    /**
     * @return LmtDocIdate
     */	
	public String getLmtDocIdate() {
		return this.lmtDocIdate;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param agrStatus
	 */
	public void setAgrStatus(String agrStatus) {
		this.agrStatus = agrStatus == null ? null : agrStatus.trim();
	}
	
    /**
     * @return AgrStatus
     */	
	public String getAgrStatus() {
		return this.agrStatus;
	}


}