/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtContRel;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtContRelMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZR
 * @创建时间: 2021-04-20 10:38:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtContRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtContRel selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectLmtContRelByDealBizNo
     * @方法描述: 根据合同编号，查询非合作方额度信息数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtContRel> selectLmtContRelByDealBizNo(@Param("dealBizNo") String dealBizNo);



    /**
     * @方法名称: selectSigLmtByDealBizNo
     * @方法描述: 根据合同编号，查询同业或白名单额度信息数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtContRel> selectSigLmtByDealBizNo(@Param("dealBizNo") String dealBizNo);


    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtContRel> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtContRel record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtContRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtContRel record);


    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtContRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);


    /**
     * @方法名称: selectLmtBizTotal
     * @方法描述: 根据客户号，分项编号获取该客户的占用分项总额
     * @参数与返回说明:
     * @算法描述: 无
     */

    Map<String, BigDecimal> selectLmtBizTotal(QueryModel queryModel);

    /**
     * @方法名称: getNorStatusLmtContCnyBal
     * @方法描述: 获取合同下总余额
     * @参数与返回说明:
     * @算法描述: 无
     */

    Map<String, BigDecimal> getNorStatusLmtContCnyBal(QueryModel queryModel);


    /**
     * @方法名称: selectLmtContRelList
     * @方法描述: 查询借款合同下，总业务余额不为0 的数据，即业务未结清的数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtContRel> selectLmtContRelList(QueryModel model);

    /**
     * 额度视图，查询额度项下关联业务
     * @author zhangjw 2021-05-05
     * @return
     */
    List<LmtContRel> queryListByLimitSubNo(QueryModel model);

    /**
     * 额度视图，查询额度项下关联业务
     * @author zhangjw 2021-05-05
     * @return
     */
    List<Map> queryListByLimitSubNoMap(QueryModel model);
    /**
     * @方法名称: getSumBizTotalAmtCnyByLimitSubNo
     * @方法描述: 根据额度分项编号查询占用总金额（折人民币）
     * @参数与返回说明:
     * @算法描述: 无
     */
    Map<String, BigDecimal> getSumBizTotalAmtCnyByLimitSubNo(QueryModel queryModel);


    /**
     * @方法名称: updateByLimitSubNo
     * @方法描述: 根据额度分项编号更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByLimitSubNo(LmtContRel record);
    /**
     * 检查该分项编号是否存在
     * @param apprSubNo
     * @return
     */
    int selectRecordsByApprSubNo(@Param("apprSubNo") String apprSubNo);

    /**
     * 单一客户授信-获取占用额度分项编号
     * @param dealBizNo
     * @return
     */
    List<Map<String, String >> selectLimitSubNoByDealBizNoDY(@Param("dealBizNo") String dealBizNo);

    /**
     *根据交易业务编号查询产品名称
     * @param dealBizNo
     * @return
     */
    Map<String,String> selectPrdNameByDealBizNo(@Param("dealBizNo") String dealBizNo);

    /**
     * 根据额度分项编号列表查询分项信息
     * @param lmtSubNoList
     * @return
     */
    List<Map<String,Object>> selectLmtInfoByLmtSubNoList(@Param("lmtSubNoList") List<String> lmtSubNoList);

    /**
     * 根据交易业务编号删除
     * @param dealBizNo
     * @return
     */
    int deleteByDealBizNo(@Param("dealBizNo") String dealBizNo);

    /**
     * 根据分项编号，查询额度占用关系中的占用总余额
     * @param limitSubNo
     * @return
     */
    BigDecimal selectLmtContRelOutstandBal(@Param("limitSubNo") String limitSubNo) ;


    List<LmtContRel> selectUnSettleLmtContRel(@Param("limitSubNo") String limitSubNo) ;

    /**
     * @作者:lizx
     * @方法名称: selectLmtContRelByApprSubSernos
     * @方法描述:  根据分项编号集合查询分项下是否有有效的占用关系
     * @日期：2021/7/1 20:25
     * @算法描述: 无
    */
    Integer selectLmtContRelByApprSubSernos(@Param("apprSubSernos") List<String> apprSubSernos);

    /**
     * 根据分项编号，查询额度占用关系中的占用总余额
     * @param limitSubNos
     * @return
     */
    BigDecimal selecBizBalByLimitSubNos(@Param("limitSubNos") String limitSubNos) ;

    /**
     * 根据分项编号，类型等，查询额度占用关系中的占用总余额
     * @param queryModel
     * @return
     */
    LmtContRel selectByLmtSubNoAndDealNo(QueryModel queryModel) ;

    /**
     * @方法名称: selectLmtBizTotal
     * @方法描述: 根据客户号，分项编号获取该客户的占用分项总额
     * @参数与返回说明:
     * @算法描述: 无
     */

    int selectByParamIsExistsBusinss(QueryModel queryModel);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtContRel> selectLmtContRelListByParam(QueryModel model);

    /**
     * @方法名称: selectUnCoopLCRByDealBizNo
     * @方法描述: 根据合同编号，查询非合作方额度信息数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtContRel> selectUnCoopLCRByDealBizNo(@Param("dealBizNo") String dealBizNo);

    /**
     * 根据分项编号，查询额度占用关系中的总额
     * @param limitSubNos
     * @return
     */
    BigDecimal selecBizTotalByLimitSubNos(@Param("limitSubNos") String limitSubNos) ;




    /**
     * @方法名称: selecContInfoByCusId
     * @方法描述: 根据客户号，获取贴现业务台账信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtContRel> selecContInfoByCusId(@Param("cusId") String cusId);

    /**
     * @方法名称: selectSigLmtByDealBizNo
     * @方法描述: 根据合同编号，查询同业或白名单额度信息数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtContRel> select4002ByDealBizNo(@Param("dealBizNo") String dealBizNo);
}