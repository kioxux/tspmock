package cn.com.yusys.yusp.service.server.cmislmt0015;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0015.req.CmisLmt0015ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015GrpLmtAccListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015LmtAccListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015LmtSubAccListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ApprStrMtableInfoMapper;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.ApprStrMtableInfoService;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0015Service
 * @类描述: #对内服务类
 * @功能描述: 客户额度查询
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0015Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0015Service.class);

    @Autowired
    private ApprStrMtableInfoService apprStrMtableInfoService;

    @Autowired
    private ApprStrMtableInfoMapper apprStrMtableInfoMapper;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService;

    @Transactional
    public CmisLmt0015RespDto execute(CmisLmt0015ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0015.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0015.value);
        CmisLmt0015RespDto resqDto = new CmisLmt0015RespDto() ;
        //集团额度台账列表
        List<CmisLmt0015GrpLmtAccListRespDto> grpLmtAccRespDtoList = new ArrayList<>();
        //客户额度台账列表
        List<CmisLmt0015LmtAccListRespDto> lmtAccRespDtoList = new ArrayList<>();
        //客户额度分项列表
        List<CmisLmt0015LmtSubAccListRespDto> lmtSubAccRespDtoList = new ArrayList<>();

        try {
            String queryType = reqDto.getQueryType();

            if ("02".equals(queryType)){
                logger.info("{}集团客户授信查询------------>start", DscmsLmtEnum.TRADE_CODE_CMISLMT0015.key);
                //集团客户授信额度
                //1.查询集团编号对应的客户号和批复台账编号
                List<Map<String, String>> mapList = apprStrMtableInfoService.selectGrpApprSernoInfoByReqDto(reqDto);

                for (Map<String, String> grpLmtAccMap : mapList) {
                    //集团额度台账
                    CmisLmt0015GrpLmtAccListRespDto grpLmtAccListRespDto = new CmisLmt0015GrpLmtAccListRespDto();

                    //集团编号
                    String grpCusNo = grpLmtAccMap.get("grpCusId");
                    grpLmtAccListRespDto.setGrpCusNo(grpCusNo);
                    //集团名称
                    grpLmtAccListRespDto.setGrpCusName(grpLmtAccMap.get("grpCusName"));
                    //集团对应的所有批复台账编号
                    String apprSernos = grpLmtAccMap.get("apprSernos");
                    //集团对应的所有客户号
                    String cusIds = grpLmtAccMap.get("cusIds");

                    if (StringUtils.isNotEmpty(apprSernos)){
                        //有批复编号
                        //1.将批复编号信息变成list
                        List<String> apprSernoList = transformString2List(apprSernos);

                        //根据该集团客户的所有批复台账编号查询授信总额等字段
                        Map<String, BigDecimal> amtMap = apprLmtSubBasicInfoService.selectLmtAmtByApprSernos(apprSernoList);

                        //授信总额
                        grpLmtAccListRespDto.setAvlAmt(amtMap.get("avlAmt"));
                        //授信总额已用
                        grpLmtAccListRespDto.setAvlOutstndAmt(amtMap.get("avlOutstndAmt"));
                        //授信总额可用
                        grpLmtAccListRespDto.setAvlAvailAmt(amtMap.get("avlAvailAmt"));
                        //授信敞口
                        grpLmtAccListRespDto.setSpacAmt(amtMap.get("spacAmt"));
                        //授信敞口已用
                        grpLmtAccListRespDto.setSpacOutstndAmt(amtMap.get("spacOutstndAmt"));
                        //授信敞口可用
                        grpLmtAccListRespDto.setSpacAvailAmt(amtMap.get("spacAvailAmt"));
                    }

                    grpLmtAccRespDtoList.add(grpLmtAccListRespDto);

                    if (StringUtils.isNotEmpty(cusIds)) {
                        //1.将客户编号信息变成list
                        List<String> cusIdList = transformString2List(cusIds);

                        //2.根据集团客户里的所有客户编号查询客户额度台账信息
                        List<Map<String,Object>> maps = apprStrMtableInfoService.selectLawAndPersonLmtAccListByCusIdList(cusIdList);
                        //3.获取客户号及其低风险授信总额的map
                        Map<String, BigDecimal> cusAndLriskMap = getCusAndLriskMap(cusIdList);
                        //4.将查询到的客户额度台账信息放入lmtAccRespDtoList里
                        lmtAccRespDtoList = getLmtAccListRespDto(maps,lmtAccRespDtoList,grpCusNo,cusAndLriskMap);
                        //5.根据集团客户里的所有客户编号查询客户额度分项信息，放入lmtSubAccRespDtoList里
                        lmtSubAccRespDtoList = getLmtSubAccRespDtoList(cusIdList,lmtSubAccRespDtoList);
                    }
                }
                logger.info("{}集团客户授信查询------------>end, 查询结果{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0015.key, JSON.toJSONString(lmtSubAccRespDtoList));
            }else {
                logger.info("{}非集团类额度查询------------>start", DscmsLmtEnum.TRADE_CODE_CMISLMT0015.key);
                //01--单一客户授信额度
                //1.根据查询条件查询客户额度台账列表
                List<Map<String,String>> cusInfoList = apprStrMtableInfoService.selectCusInfoListByReqDto(reqDto);
                //法人及个人客户编号列表
                List<String> lawAndPersonCusIdList = new ArrayList<>();
                //同业客户编号列表
                List<String> sameOrgCusIdList = new ArrayList<>();
                //客户编号列表
                List<String> cusIdList = new ArrayList<>();

                for (Map<String,String> cusInfo : cusInfoList) {
                    String cusType = cusInfo.get("cusType");
                    String cusId = cusInfo.get("cusId");
                    cusIdList.add(cusId);

                    if (CmisLmtConstants.STD_ZB_CUS_CATALOG3.equals(cusType)){
                        sameOrgCusIdList.add(cusId);
                    }else {
                        lawAndPersonCusIdList.add(cusId);
                    }
                }

                if (!lawAndPersonCusIdList.isEmpty()){
                    //获取客户号及其低风险授信总额的map
                    Map<String, BigDecimal> cusAndLriskMap = getCusAndLriskMap(lawAndPersonCusIdList);

                    //根据客户列表查询法人及个人客户额度台账信息
                    List<Map<String,Object>> mapList = apprStrMtableInfoService.selectLawAndPersonLmtAccListByCusIdList(lawAndPersonCusIdList);
                    lmtAccRespDtoList = getLmtAccListRespDto(mapList,lmtAccRespDtoList,null,cusAndLriskMap);
                }

                if (!sameOrgCusIdList.isEmpty()){
                    //获取客户号及其低风险授信总额的map
                    Map<String, BigDecimal> cusAndLriskMap = getCusAndLriskMap(sameOrgCusIdList);

                    List<Map<String,Object>> mapList = apprStrMtableInfoService.selectSigStrInfoNoPageByCusIdList(sameOrgCusIdList);
                    lmtAccRespDtoList = getLmtAccListRespDto(mapList,lmtAccRespDtoList,null,cusAndLriskMap);
                }

                //2.根据客户编号查询客户额度分项信息，然后放入客户额度分项RespDto列表里
                lmtSubAccRespDtoList = getLmtSubAccRespDtoList(cusIdList,lmtSubAccRespDtoList);
                logger.info("{}非集团类额度查询------------>end,查询结果{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0015.key, JSON.toJSONString(lmtSubAccRespDtoList));
            }

            //将集团额度台账列表、客户额度台账列表、客户额度分项列表放入返回的Dto里
            resqDto.setGrpLmtAccList(grpLmtAccRespDtoList);
            resqDto.setLmtAccList(lmtAccRespDtoList);
            resqDto.setLmtSubAccList(lmtSubAccRespDtoList);
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("客户额度查询接口报错：",e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0015.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0015.value);
        return resqDto;
    }

    /**
     * 将查询出来的客户额度台账信息放入客户额度台账RespDto里
     * @param maps
     * @param lmtAccRespDtoList
     * @return
     */
    private List<CmisLmt0015LmtAccListRespDto> getLmtAccListRespDto(List<Map<String, Object>> maps,
        List<CmisLmt0015LmtAccListRespDto> lmtAccRespDtoList,String grpCusNo,Map<String,BigDecimal> cusAndLriskMap){

        for (Map<String, Object> map : maps) {
            CmisLmt0015LmtAccListRespDto lmtAccListRespDto = new CmisLmt0015LmtAccListRespDto();
            lmtAccListRespDto.setGrpCusNo(grpCusNo);
            String cusId = (String) map.get("cusId");
            lmtAccListRespDto.setCusId(cusId);
            lmtAccListRespDto.setCusName((String) map.get("cusName"));
            lmtAccListRespDto.setAvlAmt((BigDecimal) map.get("avlAmt"));
            lmtAccListRespDto.setAvlOutstndAmt((BigDecimal) map.get("avlOutstndAmt"));
            lmtAccListRespDto.setAvlAvailAmt((BigDecimal) map.get("avlAvailAmt"));
            lmtAccListRespDto.setSpacAmt((BigDecimal) map.get("spacAmt"));
            lmtAccListRespDto.setSpacOutstndAmt((BigDecimal) map.get("spacOutstndAmt"));
            lmtAccListRespDto.setSpacAvailAmt((BigDecimal) map.get("spacAvailAmt"));

            BigDecimal lriskLmt = BigDecimal.ZERO;

            if (cusAndLriskMap!=null && cusAndLriskMap.containsKey(cusId)){
                lriskLmt = cusAndLriskMap.get(cusId);
            }
            //授信低风险金额
            lmtAccListRespDto.setLriskLmt(lriskLmt);
            lmtAccRespDtoList.add(lmtAccListRespDto);
        }

        return lmtAccRespDtoList;
    }

    /**
     * 根据客户编号查询客户额度分项信息，然后放入客户额度分项RespDto列表里
     * @param cusIdList
     * @param lmtSubAccRespDtoList
     * @return
     */
    private List<CmisLmt0015LmtSubAccListRespDto> getLmtSubAccRespDtoList(List<String> cusIdList,List<CmisLmt0015LmtSubAccListRespDto> lmtSubAccRespDtoList){
        if (!cusIdList.isEmpty()) {
            List<Map<String, Object>> lmtSubAccList = apprLmtSubBasicInfoService.selectLmtSubAccListByCusIdList(cusIdList);

            for (Map<String, Object> lmtSubAcc : lmtSubAccList) {
                CmisLmt0015LmtSubAccListRespDto lmtSubAccListRespDto = new CmisLmt0015LmtSubAccListRespDto();

                String accNo = (String) lmtSubAcc.get("accNo");
                lmtSubAccListRespDto.setCusId((String) lmtSubAcc.get("cusId"));
                lmtSubAccListRespDto.setAccNo(accNo);
                lmtSubAccListRespDto.setParentId((String) lmtSubAcc.get("parentId"));
                lmtSubAccListRespDto.setSubSerno((String) lmtSubAcc.get("apprSubSerno"));
                lmtSubAccListRespDto.setLimitSubNo((String) lmtSubAcc.get("limitSubNo"));
                lmtSubAccListRespDto.setLimitSubName((String) lmtSubAcc.get("limitSubName"));
                lmtSubAccListRespDto.setIsLriskLmt((String) lmtSubAcc.get("isLriskLmt"));
                lmtSubAccListRespDto.setSuitGuarWay((String) lmtSubAcc.get("suitGuarWay"));
                lmtSubAccListRespDto.setCny((String) lmtSubAcc.get("cny"));
                lmtSubAccListRespDto.setIsRevolv((String) lmtSubAcc.get("isRevolv"));
                lmtSubAccListRespDto.setIsPreCrd((String) lmtSubAcc.get("isPreCrd"));
                lmtSubAccListRespDto.setAvlAmt((BigDecimal) lmtSubAcc.get("avlAmt"));
                lmtSubAccListRespDto.setAvlOutstndAmt((BigDecimal) lmtSubAcc.get("avlOutstndAmt"));
                lmtSubAccListRespDto.setAvlAvailAmt((BigDecimal) lmtSubAcc.get("avlAvailAmt"));
                lmtSubAccListRespDto.setSpacAmt((BigDecimal) lmtSubAcc.get("spacAmt"));
                lmtSubAccListRespDto.setSpacOutstndAmt((BigDecimal) lmtSubAcc.get("spacOutstndAmt"));
                lmtSubAccListRespDto.setSpacAvailAmt((BigDecimal) lmtSubAcc.get("spacAvailAmt"));
                lmtSubAccListRespDto.setStartDate((String) lmtSubAcc.get("startDate"));
                lmtSubAccListRespDto.setEndDate((String) lmtSubAcc.get("endDate"));
                lmtSubAccListRespDto.setTerm((Integer) lmtSubAcc.get("term"));
                lmtSubAccListRespDto.setStatus((String) lmtSubAcc.get("status"));
                lmtSubAccListRespDto.setLoanBalance((BigDecimal) lmtSubAcc.get("loanBalance"));
                lmtSubAccListRespDto.setLoanSpacBalance((BigDecimal) lmtSubAcc.get("loanSpacBalance"));
                lmtSubAccListRespDto.setRateYear((BigDecimal) lmtSubAcc.get("rateYear"));
                lmtSubAccListRespDto.setLimitType((String) lmtSubAcc.get("limitType"));
                lmtSubAccListRespDto.setDealBizBailPreRate((BigDecimal) lmtSubAcc.get("dealBizBailPreRate"));

                //根据批复台账编号查询授信模式
                String lmtMode = apprStrMtableInfoService.selectLmtModeByApprSerno(accNo);
                lmtSubAccListRespDto.setLmtMode(lmtMode);

                lmtSubAccRespDtoList.add(lmtSubAccListRespDto);
            }
        }
        return lmtSubAccRespDtoList;
    }

    /**
     * 将形如“123，456，57”或“123”的字符串转换成List集合
     * @param str
     * @return
     */
    private List<String> transformString2List(String str){
        List<String> result = new ArrayList<>();

        if (str.contains(",")){
            String[] strArr = str.split(",");
            result = Arrays.asList(strArr);
        }else {
            result.add(str);
        }
        return result;
    }

    /**
     * 获取客户号及其低风险授信总额的map
     * @param lawAndPersonCusIdList
     * @return
     */
    public Map<String,BigDecimal> getCusAndLriskMap(List<String> lawAndPersonCusIdList){
        //查询客户的低风险授信总额列表
        List<Map<String, Object>> cusIdAndLriskLmtList = apprStrMtableInfoService.selectLriskLmtByCusIdList(lawAndPersonCusIdList);
        //key:客户号 value：低风险授信总额
        Map<String,BigDecimal> cusAndLriskMap = new HashMap<>();

        if (cusIdAndLriskLmtList!=null && cusIdAndLriskLmtList.size()>0){
            for (Map<String, Object> cusIdAndLriskLmtMap : cusIdAndLriskLmtList) {
                //遍历低风险授信总额列表，将客户号和对应的低风险授信总额放入map
                cusAndLriskMap.put((String) cusIdAndLriskLmtMap.get("cusId"), BigDecimalUtil.replaceNull(cusIdAndLriskLmtMap.get("lriskLmt")));
            }
        }
        return cusAndLriskMap;
    }
}