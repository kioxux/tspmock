package cn.com.yusys.yusp.web.server.cmislmt0043;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0043.req.req.CmisLmt0043ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0043.req.resp.CmisLmt0043RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0043.CmisLmt0043Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:获取客户及其集团成员的标和非标业务授信余额（除资管计划、净值型产品以及承销额度）
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "cmislmt0043:获取客户及其集团成员的标和非标业务授信余额（除资管计划、净值型产品以及承销额度）")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0043Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0043Resource.class);

    @Autowired
    private CmisLmt0043Service cmisLmt0043Service;
    /**
     * 交易码：cmislmt0043
     * 交易描述：单一客户额度同步
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("获取客户及其集团成员的标和非标业务授信余额（除资管计划、净值型产品以及承销额度）")
    @PostMapping("/cmislmt0043")
    protected @ResponseBody
    ResultDto<CmisLmt0043RespDto> cmisLmt0043(@Validated @RequestBody CmisLmt0043ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0043.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0043.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0043RespDto> cmisLmt0043RespDtoResultDto = new ResultDto<>();
        CmisLmt0043RespDto cmisLmt0043RespDto = new CmisLmt0043RespDto() ;
        try {
            // 调用对应的service层
            cmisLmt0043RespDto = cmisLmt0043Service.execute(reqDto);
            //设置陈工交易码值
            cmisLmt0043RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0043RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (YuspException e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0043.value, e);
            cmisLmt0043RespDto.setErrorCode(e.getCode());
            cmisLmt0043RespDto.setErrorMsg(e.getMsg());
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0043.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0043.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0043.value, e.getMessage());
            // 封装cmisLmt0043RespDtoResultDto中异常返回码和返回信息
            cmisLmt0043RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0043RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }

        // 封装cmisLmt0043RespDto到cmisLmt0043RespDtoResultDto中
        cmisLmt0043RespDtoResultDto.setData(cmisLmt0043RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0043.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0043.value, JSON.toJSONString(cmisLmt0043RespDtoResultDto));
        return cmisLmt0043RespDtoResultDto;
    }
}
