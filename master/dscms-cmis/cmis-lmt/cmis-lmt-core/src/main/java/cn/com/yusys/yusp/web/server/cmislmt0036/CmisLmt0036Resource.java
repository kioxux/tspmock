package cn.com.yusys.yusp.web.server.cmislmt0036;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0036.req.CmisLmt0036ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0036.resp.CmisLmt0036RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.cmislmt0036.CmisLmt0036Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 根据分项信息，查询起始日到期日期限
 * add by lizx 20210629
 */
@Api(tags = "cmislmt0036:根据分项编号，查询起始日到期日期限")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0036Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0036Resource.class);

    @Autowired
    private CmisLmt0036Service cmisLmt0036Service;

    /**
     * 交易码：CmisLmt0036
     * 交易描述：根据分项信息，查询起始日到期日期限
     * @param reqDto
     */

    @ApiOperation("根据分项信息，查询起始日到期日期限")
    @PostMapping("/cmislmt0036")
    protected @ResponseBody
    ResultDto<CmisLmt0036RespDto> CmisLmt0036(@Validated @RequestBody CmisLmt0036ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0036.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0036.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0036RespDto> CmisLmt0036RespDtoResultDto = new ResultDto<>();
        CmisLmt0036RespDto CmisLmt0036RespDto = new CmisLmt0036RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0036RespDto = cmisLmt0036Service.execute(reqDto);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0036.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0036.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0036.value, e.getMessage());
            // 封装CmisLmt0036RespDtoResultDto中异常返回码和返回信息
            CmisLmt0036RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0036RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0036RespDto到CmisLmt0036RespDtoResultDto中
        CmisLmt0036RespDtoResultDto.setData(CmisLmt0036RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0036.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0036.value, JSON.toJSONString(CmisLmt0036RespDtoResultDto));
        return CmisLmt0036RespDtoResultDto;
    }
}