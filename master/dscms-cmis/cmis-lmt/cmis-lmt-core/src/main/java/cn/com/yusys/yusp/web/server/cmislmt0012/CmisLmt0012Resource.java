package cn.com.yusys.yusp.web.server.cmislmt0012;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0012.CmisLmt0012Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:合同恢复
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "cmislmt0012:合同恢复")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0012Resource.class);

    @Autowired
    private CmisLmt0012Service cmisLmt0012Service;
    /**
     * 交易码：cmislmt0012
     * 交易描述：合同恢复
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("合同恢复")
    @PostMapping("/cmislmt0012")
    protected @ResponseBody
    ResultDto<CmisLmt0012RespDto> cmisLmt0012(@Validated @RequestBody CmisLmt0012ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0012.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0012.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0012RespDto> cmisLmt0012RespDtoResultDto = new ResultDto<>();
        CmisLmt0012RespDto cmisLmt0012RespDto = new CmisLmt0012RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0012RespDto = cmisLmt0012Service.execute(reqDto);
            cmisLmt0012RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0012RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0012.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0012.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0012.value, e.getMessage());
            // 封装xddb0012DataResultDto中异常返回码和返回信息
            cmisLmt0012RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0012RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0012RespDto到cmisLmt0012RespDtoResultDto中
        cmisLmt0012RespDtoResultDto.setData(cmisLmt0012RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0012.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0012.value, JSON.toJSONString(cmisLmt0012RespDtoResultDto));
        return cmisLmt0012RespDtoResultDto;
    }
}
