package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.ApprCoopChgApp;
import cn.com.yusys.yusp.domain.ApprStrMtableChgApp;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.ApprCoopChgAppService;
import cn.com.yusys.yusp.service.ApprStrMtableChgAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @className EDGL02BizService
 * @Description 额度调整申请  审批后处理
 * @Date 2021/06/21
 */
@Service
public class EDGL02BizService implements ClientBizInterface {
	private final Logger log = LoggerFactory.getLogger(EDGL02BizService.class);

	@Autowired
	private ApprStrMtableChgAppService apprStrMtableChgAppService;

	@Autowired
	private ApprCoopChgAppService apprCoopChgAppService;

	@Override
	public void bizOp(ResultInstanceDto resultInstanceDto) {
		String bizType = resultInstanceDto.getBizType();

		if (CmisFlowConstants.ED001.equals(bizType)){
			//ED001--单一客户额度调整申请
			bizOp4ED001(resultInstanceDto);
		}else if (CmisFlowConstants.ED003.equals(bizType)){
			//ED003--合作方客户额度调整申请
			bizOp4ED003(resultInstanceDto);
		}
	}

	/**
	 * 单一客户额度调整申请审批后业务处理
	 * @param resultInstanceDto
	 */
	private void bizOp4ED001(ResultInstanceDto resultInstanceDto) {
		String currentOpType = resultInstanceDto.getCurrentOpType();
		String serno = resultInstanceDto.getBizId();
		Map<String, Object> paramMap = resultInstanceDto.getParam();
		log.info("单一客户额度调整申请审批后业务处理类型" + currentOpType);
		try {
			String pkId = (String)paramMap.get("bizPkId") ;
			ApprStrMtableChgApp apprStrMtableChgApp = apprStrMtableChgAppService.selectByPrimaryKey(pkId);
			if (OpType.STRAT.equals(currentOpType)) {
				log.info("单一客户额度调整申请" + serno + "流程发起操作，流程参数" + resultInstanceDto);
			} else if (OpType.RUN.equals(currentOpType)) {
				log.info("单一客户额度调整申请" + serno + "流程提交操作，流程参数" + resultInstanceDto);
				apprStrMtableChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
				apprStrMtableChgAppService.updateApproveStatus(apprStrMtableChgApp);
			} else if (OpType.JUMP.equals(currentOpType)) {
				log.info("单一客户额度调整申请" + serno + "流程跳转操作，流程参数" + resultInstanceDto);
			} else if (OpType.END.equals(currentOpType)) {
				log.info("单一客户额度调整申请" + serno + "流程结束操作，流程参数" + resultInstanceDto);
				//针对流程到办结节点，进行以下处理
				//额度主信息不变  额度分项信息更新已用金额  分项占用关系数据更新  台账占用关系更新
				apprStrMtableChgAppService.handleBusinessDataAfterEnd(serno);
			} else if (OpType.RETURN_BACK.equals(currentOpType)) {
				log.info("单一客户额度调整申请"+serno+"退回操作，流程参数："+ resultInstanceDto.toString());
				//判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
				if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)){
					apprStrMtableChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
					apprStrMtableChgAppService.updateApproveStatus(apprStrMtableChgApp);
				}
			} else if (OpType.CALL_BACK.equals(currentOpType)) {
				log.info("单一客户额度调整申请"+serno+"打回操作，流程参数："+ resultInstanceDto.toString());
				//判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
				if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
					apprStrMtableChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
					apprStrMtableChgAppService.updateApproveStatus(apprStrMtableChgApp);
				}
			} else if (OpType.TACK_BACK.equals(currentOpType)) {
				log.info("单一客户额度调整申请"+serno+"拿回操作，流程参数："+ resultInstanceDto.toString());
				//判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
				if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
					apprStrMtableChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
					apprStrMtableChgAppService.updateApproveStatus(apprStrMtableChgApp);
				}
			} else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
				log.info("单一客户额度调整申请" + serno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
				//流程拿回到第一个节点，申请主表的业务
				apprStrMtableChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
				apprStrMtableChgAppService.updateApproveStatus(apprStrMtableChgApp);
			} else if (OpType.REFUSE.equals(currentOpType)) {
				log.info("单一客户额度调整申请" + serno + "否决操作，流程参数：" + resultInstanceDto);
				// 否决改变标志 审批中 111-> 审批不通过 998
				apprStrMtableChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
				apprStrMtableChgAppService.updateApproveStatus(apprStrMtableChgApp);
			} else {
				log.warn("单一客户额度调整申请" + serno + "未知操作:" + resultInstanceDto);
			}
		} catch (Exception e) {
			log.error("后业务处理失败", e);
			try {
				BizCommonUtils bizCommonUtils = new BizCommonUtils();
				bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
			} catch (Exception e1) {
				log.error("发送异常消息失败", e1);
			}
		}
	}

	/**
	 * 合作方客户额度调整申请审批后业务处理
	 * @param resultInstanceDto
	 */
	private void bizOp4ED003(ResultInstanceDto resultInstanceDto) {
		String currentOpType = resultInstanceDto.getCurrentOpType();
		String serno = resultInstanceDto.getBizId();
		Map<String, Object> paramMap = resultInstanceDto.getParam();
		log.info("合作方客户额度调整申请审批后业务处理类型" + currentOpType);
		try {
			String pkId = (String)paramMap.get("bizPkId") ;
			ApprCoopChgApp apprCoopChgApp = apprCoopChgAppService.selectByPrimaryKey(pkId);
			if (OpType.STRAT.equals(currentOpType)) {
				log.info("合作方客户额度调整申请" + serno + "流程发起操作，流程参数" + resultInstanceDto);
			} else if (OpType.RUN.equals(currentOpType)) {
				log.info("合作方客户额度调整申请" + serno + "流程提交操作，流程参数" + resultInstanceDto);
				apprCoopChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
				apprCoopChgAppService.updateApproveStatus(apprCoopChgApp);
			} else if (OpType.JUMP.equals(currentOpType)) {
				log.info("合作方客户额度调整申请" + serno + "流程跳转操作，流程参数" + resultInstanceDto);
			} else if (OpType.END.equals(currentOpType)) {
				log.info("合作方客户额度调整申请" + serno + "流程结束操作，流程参数" + resultInstanceDto);
				//针对流程到办结节点，进行以下处理
				//额度主信息不变  额度分项信息更新已用金额  分项占用关系数据更新  台账占用关系更新
				apprCoopChgAppService.handleBusinessDataAfterEnd(serno);
			} else if (OpType.RETURN_BACK.equals(currentOpType)) {
				//判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
				if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)){
					apprCoopChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
					apprCoopChgAppService.updateApproveStatus(apprCoopChgApp);
				}
			} else if (OpType.CALL_BACK.equals(currentOpType)) {
				log.info("合作方客户额度调整申请"+serno+"打回操作，流程参数："+ resultInstanceDto.toString());
				//判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
				if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
					apprCoopChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
					apprCoopChgAppService.updateApproveStatus(apprCoopChgApp);
				}
			} else if (OpType.TACK_BACK.equals(currentOpType)) {
				log.info("合作方客户额度调整申请"+serno+"拿回操作，流程参数："+ resultInstanceDto.toString());
				//判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
				if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
					apprCoopChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
					apprCoopChgAppService.updateApproveStatus(apprCoopChgApp);
				}
			} else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
				log.info("合作方客户额度调整申请" + serno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
				//流程拿回到第一个节点，申请主表的业务
				apprCoopChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
				apprCoopChgAppService.updateApproveStatus(apprCoopChgApp);
			} else if (OpType.REFUSE.equals(currentOpType)) {
				log.info("合作方客户额度调整申请" + serno + "否决操作，流程参数：" + resultInstanceDto);
				// 否决改变标志 审批中 111-> 审批不通过 998
				apprCoopChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
				apprCoopChgAppService.updateApproveStatus(apprCoopChgApp);
			} else {
				log.warn("合作方客户额度调整申请" + serno + "未知操作:" + resultInstanceDto);
			}
		} catch (Exception e) {
			log.error("后业务处理失败", e);
			try {
				BizCommonUtils bizCommonUtils = new BizCommonUtils();
				bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
			} catch (Exception e1) {
				log.error("发送异常消息失败", e1);
			}
		}
	}

	// 判定流程能否进行业务处理
	@Override
	public boolean should(ResultInstanceDto resultInstanceDto) {
		String flowCode = resultInstanceDto.getFlowCode();
		return CmisFlowConstants.EDGL02.equals(flowCode);
	}
}