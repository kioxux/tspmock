package cn.com.yusys.yusp.web.server.cmislmt0002;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0002.req.CmisLmt0002ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0002.resp.CmisLmt0002RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0002.CmisLmt0002Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:集团客户额度同步
 *
 * @author zhangjw 20210505
 * @version 1.0
 */
@Api(tags = "cmislmt0002:集团客户额度同步")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0002Resource.class);

    @Autowired
    private CmisLmt0002Service cmisLmt0002Service;
    /**
     * 交易码：cmislmt0002
     * 交易描述：单一客户额度同步
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("集团客户额度同步")
    @PostMapping("/cmislmt0002")
    protected @ResponseBody
    ResultDto<CmisLmt0002RespDto> cmisLmt0002(@Validated @RequestBody CmisLmt0002ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0002.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0002.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0002RespDto> cmisLmt0002RespDtoResultDto = new ResultDto<>();
        CmisLmt0002RespDto cmisLmt0002RespDto = new CmisLmt0002RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0002RespDto = cmisLmt0002Service.execute(reqDto);
            cmisLmt0002RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0002RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (YuspException e) {
            logger.error("集团额度同步接口报错：", e);
            cmisLmt0002RespDto.setErrorCode(e.getCode());
            cmisLmt0002RespDto.setErrorMsg(e.getMsg());
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0002.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0002.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0002.value, e.getMessage());
            // 封装cmisLmt0002RespDtoResultDto中异常返回码和返回信息
            cmisLmt0002RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0002RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }

        // 封装cmisLmt0002RespDto到cmisLmt0002RespDtoResultDto中
        cmisLmt0002RespDtoResultDto.setData(cmisLmt0002RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0002.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0002.value, JSON.toJSONString(cmisLmt0002RespDtoResultDto));
        return cmisLmt0002RespDtoResultDto;
    }
}
