/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.LmtWhiteInfo;
import cn.com.yusys.yusp.domain.LmtWhiteInfoHistory;
import cn.com.yusys.yusp.dto.server.cmiscus0010.req.CmisCus0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CmisCus0010RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.req.CmisLmt0015ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0044.req.CmisLmt0044ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0044.resp.CmisLmt0044ListRespDto;
import cn.com.yusys.yusp.repository.mapper.LmtWhiteInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.server.cmiscfg0002.resp.CmisCfg0002ListRespDto;
import cn.com.yusys.yusp.vo.LmtDiscOrgTempExportVo;
import cn.com.yusys.yusp.vo.WhiteInfo01ImportVo;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtDiscOrg;
import cn.com.yusys.yusp.repository.mapper.LmtDiscOrgMapper;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtDiscOrgService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-24 09:15:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtDiscOrgService {

    @Autowired
    private LmtDiscOrgMapper lmtDiscOrgMapper;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    private SequenceTemplateService sequenceTemplateClient;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtDiscOrg selectByPrimaryKey(String serno) {
        return lmtDiscOrgMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtDiscOrg> selectAll(QueryModel model) {
        List<LmtDiscOrg> records = (List<LmtDiscOrg>) lmtDiscOrgMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtDiscOrg> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtDiscOrg> list = lmtDiscOrgMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtDiscOrg record) {
        QueryModel query = new QueryModel() ;
        query.addCondition("appYm", record.getAppYm());
        query.addCondition("organno", record.getOrganno());
        query.addCondition("statusList", CmisLmtConstants.STD_LMT_DISC_ORG_STATUS_01 + CmisLmtConstants.COMMON_SPLIT_COMMA
                + CmisLmtConstants.STD_LMT_DISC_ORG_STATUS_02 + CmisLmtConstants.COMMON_SPLIT_COMMA + CmisLmtConstants.STD_LMT_DISC_ORG_STATUS_03);
        query.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        List<LmtDiscOrg> lmtDiscOrgs = lmtDiscOrgMapper.selectByModel(query) ;
        //数据库中已存在
        if (CollectionUtils.isNotEmpty(lmtDiscOrgs)){
            throw new BizException(null, "9999", null, record.getOrganname()+"机构已存在生效日为"+record.getAppYm()+"的贴现限额申请，请核实");
        }

        return lmtDiscOrgMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtDiscOrg record) {
        return lmtDiscOrgMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtDiscOrg record) {
        return lmtDiscOrgMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtDiscOrg record) {
        return lmtDiscOrgMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * 异步下载贴现限额管控模板
     * @return 导出进度信息
     */
    public ProgressDto asyncExportLmtDiscOrgTemp() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {

                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(LmtDiscOrgTempExportVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 批量插入贴现限额管控
     *
     * @param perLmtDiscOrgList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int importLmtDiscOrg(List<Object> perLmtDiscOrgList) {
        List<LmtDiscOrg> discOrgList = (List<LmtDiscOrg>) BeanUtils.beansCopy(perLmtDiscOrgList, LmtDiscOrg.class);
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        User userInfo = SessionUtils.getUserInformation();
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            LmtDiscOrgMapper lmtDiscOrgMapper = sqlSession.getMapper(LmtDiscOrgMapper.class);
            for (LmtDiscOrg discOrg : discOrgList) {
                QueryModel query = new QueryModel() ;
                query.addCondition("appYm", discOrg.getAppYm());
                query.addCondition("organno", discOrg.getOrganno());
                query.addCondition("statusList", CmisLmtConstants.STD_LMT_DISC_ORG_STATUS_01 + CmisLmtConstants.COMMON_SPLIT_COMMA
                        + CmisLmtConstants.STD_LMT_DISC_ORG_STATUS_02 + CmisLmtConstants.COMMON_SPLIT_COMMA + CmisLmtConstants.STD_LMT_DISC_ORG_STATUS_03);
                query.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
                List<LmtDiscOrg> lmtDiscOrgs = lmtDiscOrgMapper.selectByModel(query) ;
                //数据库中已存在
                if (CollectionUtils.isNotEmpty(lmtDiscOrgs)){
                    throw new BizException(null, "9999", null, discOrg.getOrganname()+"机构已存在生效日为"+discOrg.getAppYm()+"的贴现限额申请，请核实");

                }//数据库中不存在，新增信息
                else{
                    //生成主键
                    Map paramMap= new HashMap<>() ;
                    String serno = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.LMT_SERNO, paramMap);
                    discOrg.setSerno(serno);
                    discOrg.setUseAmt(BigDecimal.ZERO);
                    discOrg.setStatus(CmisLmtConstants.STD_LMT_DISC_ORG_STATUS_03);

                    //到期日转换
                    String appYm = discOrg.getAppYm();
                    try{
                        appYm = LocalDate.parse(appYm,dateTimeFormatter).toString();
                    }catch (Exception e){
                        throw new BizException(null, "9999", null, "到期日格式录入错误，请使用YYYY-MM-DD格式");
                    }
                    //到期日期
                    discOrg.setAppYm(appYm);
                    //操作类型
//                    discOrg.setOprType(CmisLmtConstants.OPR_TYPE_ADD);

                    lmtDiscOrgMapper.insertSelective(discOrg);

                }
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        return perLmtDiscOrgList.size();
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return lmtDiscOrgMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtDiscOrgMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryDetailByreqDto
     * @方法描述: 获取查询机构贴现限额
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CmisLmt0044ListRespDto> queryDetailByreqDto(CmisLmt0044ReqDto reqDto) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("organno",reqDto.getOrganno());
        queryModel.addCondition("appYm",reqDto.getMmyy());
        queryModel.addCondition("inputDate", reqDto.getOpenDay());
        return lmtDiscOrgMapper.queryDetailByreqDto(queryModel);
    }

    /**
     * @方法名称: queryDetailByreqDto
     * @方法描述: 获取查询机构贴现限额
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtDiscOrg> queryDetailByOrgAndYm(String organno, String appYm) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("organno", organno);
        queryModel.addCondition("appYm", appYm);
        return lmtDiscOrgMapper.queryDetailByOrgAndYm(queryModel);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateByPKeyInApprLmtChgDetails(LmtDiscOrg record, Map<String, String> paramMap){
        return lmtDiscOrgMapper.updateByPrimaryKey(record);
    }
}
