/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: DmRisKhfxJgbxJk
 * @类描述: dm_ris_khfx_jgbx_jk数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-25 15:50:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "dm_ris_khfx_jgbx_jk")
public class DmRisKhfxJgbxJk extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 数据日期 **/
	@Id
	@Column(name = "data_dt")
	private String dataDt;
	
	/** 客户类型代码 **/
	@Id
	@Column(name = "cust_type_id")
	private String custTypeId;
	
	/** 客户编号 **/
	@Id
	@Column(name = "cust_id")
	private String custId;
	
	/** 客户名称 **/
	@Column(name = "cust_name", unique = false, nullable = true, length = 100)
	private String custName;
	
	/** 非同业单一客户贷款余额 **/
	@Column(name = "cont_lon_bal", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal contLonBal;
	
	/** 非同业单一客户风险暴露 **/
	@Column(name = "ftydy_khmx_expo", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal ftydyKhmxExpo;
	
	/** 非同业关联客户风险暴露 **/
	@Column(name = "ftygl_khmx_expo", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal ftyglKhmxExpo;
	
	/** 同业单一客户风险暴露 **/
	@Column(name = "tydy_khmx_expo", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal tydyKhmxExpo;
	
	/** 同业关联客户风险暴露 **/
	@Column(name = "tygl_khmx_expo", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal tyglKhmxExpo;
	
	/** 匿名客户风险暴露 **/
	@Column(name = "mmkh_khmx_expo", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal mmkhKhmxExpo;
	
	/** 一级资本净额 **/
	@Column(name = "yjzbje_amt", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal yjzbjeAmt;
	
	/** 资本净额 **/
	@Column(name = "zbje_amt", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal zbjeAmt;
	
	/** 总资产 **/
	@Column(name = "zzc_amt", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal zzcAmt;
	
	/** 授信金额 **/
	@Column(name = "acct_balz_sx", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal acctBalzSx;
	
	/** 用信金额 **/
	@Column(name = "acct_balz_yx", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal acctBalzYx;
	
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param custTypeId
	 */
	public void setCustTypeId(String custTypeId) {
		this.custTypeId = custTypeId;
	}
	
    /**
     * @return custTypeId
     */
	public String getCustTypeId() {
		return this.custTypeId;
	}
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
    /**
     * @return custId
     */
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
    /**
     * @return custName
     */
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param contLonBal
	 */
	public void setContLonBal(java.math.BigDecimal contLonBal) {
		this.contLonBal = contLonBal;
	}
	
    /**
     * @return contLonBal
     */
	public java.math.BigDecimal getContLonBal() {
		return this.contLonBal;
	}
	
	/**
	 * @param ftydyKhmxExpo
	 */
	public void setFtydyKhmxExpo(java.math.BigDecimal ftydyKhmxExpo) {
		this.ftydyKhmxExpo = ftydyKhmxExpo;
	}
	
    /**
     * @return ftydyKhmxExpo
     */
	public java.math.BigDecimal getFtydyKhmxExpo() {
		return this.ftydyKhmxExpo;
	}
	
	/**
	 * @param ftyglKhmxExpo
	 */
	public void setFtyglKhmxExpo(java.math.BigDecimal ftyglKhmxExpo) {
		this.ftyglKhmxExpo = ftyglKhmxExpo;
	}
	
    /**
     * @return ftyglKhmxExpo
     */
	public java.math.BigDecimal getFtyglKhmxExpo() {
		return this.ftyglKhmxExpo;
	}
	
	/**
	 * @param tydyKhmxExpo
	 */
	public void setTydyKhmxExpo(java.math.BigDecimal tydyKhmxExpo) {
		this.tydyKhmxExpo = tydyKhmxExpo;
	}
	
    /**
     * @return tydyKhmxExpo
     */
	public java.math.BigDecimal getTydyKhmxExpo() {
		return this.tydyKhmxExpo;
	}
	
	/**
	 * @param tyglKhmxExpo
	 */
	public void setTyglKhmxExpo(java.math.BigDecimal tyglKhmxExpo) {
		this.tyglKhmxExpo = tyglKhmxExpo;
	}
	
    /**
     * @return tyglKhmxExpo
     */
	public java.math.BigDecimal getTyglKhmxExpo() {
		return this.tyglKhmxExpo;
	}
	
	/**
	 * @param mmkhKhmxExpo
	 */
	public void setMmkhKhmxExpo(java.math.BigDecimal mmkhKhmxExpo) {
		this.mmkhKhmxExpo = mmkhKhmxExpo;
	}
	
    /**
     * @return mmkhKhmxExpo
     */
	public java.math.BigDecimal getMmkhKhmxExpo() {
		return this.mmkhKhmxExpo;
	}
	
	/**
	 * @param yjzbjeAmt
	 */
	public void setYjzbjeAmt(java.math.BigDecimal yjzbjeAmt) {
		this.yjzbjeAmt = yjzbjeAmt;
	}
	
    /**
     * @return yjzbjeAmt
     */
	public java.math.BigDecimal getYjzbjeAmt() {
		return this.yjzbjeAmt;
	}
	
	/**
	 * @param zbjeAmt
	 */
	public void setZbjeAmt(java.math.BigDecimal zbjeAmt) {
		this.zbjeAmt = zbjeAmt;
	}
	
    /**
     * @return zbjeAmt
     */
	public java.math.BigDecimal getZbjeAmt() {
		return this.zbjeAmt;
	}
	
	/**
	 * @param zzcAmt
	 */
	public void setZzcAmt(java.math.BigDecimal zzcAmt) {
		this.zzcAmt = zzcAmt;
	}
	
    /**
     * @return zzcAmt
     */
	public java.math.BigDecimal getZzcAmt() {
		return this.zzcAmt;
	}
	
	/**
	 * @param acctBalzSx
	 */
	public void setAcctBalzSx(java.math.BigDecimal acctBalzSx) {
		this.acctBalzSx = acctBalzSx;
	}
	
    /**
     * @return acctBalzSx
     */
	public java.math.BigDecimal getAcctBalzSx() {
		return this.acctBalzSx;
	}
	
	/**
	 * @param acctBalzYx
	 */
	public void setAcctBalzYx(java.math.BigDecimal acctBalzYx) {
		this.acctBalzYx = acctBalzYx;
	}
	
    /**
     * @return acctBalzYx
     */
	public java.math.BigDecimal getAcctBalzYx() {
		return this.acctBalzYx;
	}


}