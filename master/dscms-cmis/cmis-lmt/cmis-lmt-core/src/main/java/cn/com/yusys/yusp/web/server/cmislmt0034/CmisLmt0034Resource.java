package cn.com.yusys.yusp.web.server.cmislmt0034;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.req.CmisLmt0034ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.resp.CmisLmt0034RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0034.CmisLmt0034Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:国结票据出账额度占用
 *
 * @author dumd 20210623
 * @version 1.0
 */
@Api(tags = "cmislmt0034:国结票据出账额度占用")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0034Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0034Resource.class);

    @Autowired
    private CmisLmt0034Service cmisLmt0034Service;

    /**
     * 交易码：CmisLmt0034
     * 交易描述：国结票据出账额度占用
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("国结票据出账额度占用")
    @PostMapping("/cmislmt0034")
    protected @ResponseBody
    ResultDto<CmisLmt0034RespDto> CmisLmt0034(@Validated @RequestBody CmisLmt0034ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0034.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0034.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0034RespDto> CmisLmt0034RespDtoResultDto = new ResultDto<>();
        CmisLmt0034RespDto CmisLmt0034RespDto = new CmisLmt0034RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0034RespDto = cmisLmt0034Service.execute(reqDto);
            CmisLmt0034RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0034RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (YuspException e) {
            logger.error("国结票据出账额度占用" , e);
            CmisLmt0034RespDto.setErrorCode(e.getCode());
            CmisLmt0034RespDto.setErrorMsg(e.getMsg());
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0034.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0034.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0034.value, e.getMessage());
            // 封装CmisLmt0034RespDtoResultDto中异常返回码和返回信息
            CmisLmt0034RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0034RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0034RespDto到CmisLmt0034RespDtoResultDto中
        CmisLmt0034RespDtoResultDto.setData(CmisLmt0034RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0034.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0034.value, JSON.toJSONString(CmisLmt0034RespDtoResultDto));
        return CmisLmt0034RespDtoResultDto;
    }
}