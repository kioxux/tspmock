package cn.com.yusys.yusp.service.server.cmislmt0021;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0021.req.CmisLmt0021ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0021.resp.CmisLmt0021RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ApprStrMtableInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0021Service
 * @类描述: #对内服务类
 * @功能描述: 客户额度分类查询
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0021Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0021Service.class);

    @Autowired
    private ApprStrMtableInfoMapper apprStrMtableInfoMapper;

    /**
     * 对公授信信息查询
     *
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0021RespDto execute(CmisLmt0021ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0021.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0021.value);
        CmisLmt0021RespDto resqDto = new CmisLmt0021RespDto();
        try {

            //客户编号
            String cusId = reqDto.getCusId();
            //查询对公客户授信信息
            resqDto = apprStrMtableInfoMapper.selectCusComLmtDetailsByCusId(cusId);
            //返回信息
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("对公授信信息查询：", e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0021.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0021.value);
        return resqDto;
    }
}
