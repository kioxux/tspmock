package cn.com.yusys.yusp.constant;

/**
 * 授信台账异常枚举类
 */
public enum LmtAccExceptionDefEnums {
    //定义默认的成功以及异常返回信息
    LMT_ACC_DEF_SUCCESS("000000","成功"),
    LMT_ACC_DEF_EXCEPTION("999999","异常"),

    E_LMT_ACC_NOT_EXISTS_FAILED("LAE000001","授信台账不存在！"),
    E_LMT_ACC_CAL_AMT_FAILED("LAE000002","计算可用额度失败！"),
    E_LMT_ACC_NOT_FROZEAMT_FAILED("LAE000001","额度台账项下冻结金额为空！！"),

    E_TH_LMT_ACC_PARAMS_FAILED("TLAE000001","请求入参数据不存在！"),
    E_TH_LMT_ACC_QUERY_FAILED("TLAE000002","查询第三方额度信息不存在！"),

    E_LMTQUERY_PARAMS_EXCEPTION("LMTQUERY00001","未获取到需要查询的额度台账/第三方额度台账入参信息！"),
    E_LMTQUERY_LMT_CTR_NOT_EXISTS_FAILED("LMTQUERY00002","授信协议不存在！"),

    E_LMT_ACC_EXISTS("LMTQUERY00003","新增数据失败！该数据已存在"),
    E_LMT_ACC_GERUSER_FAILED("LMTQUERY00004","获取当前登录用户信息异常"),
    E_LMT_ACC_INSERT_FAILED("IQPIE0004","新增数据异常！"),


    E_LMTACCLISTQUERY_PARAMS_EXCEPTION("LALQ000001","获取额度协议及额度台账集合-参数异常！"),
    E_LMTACCLISTQUERY_LMTCTRNULL_EXCEPTION("LALQ000002","获取额度协议及额度台账集合-额度协议不存在！"),
    E_LMTACCLISTQUERY_LMTACCLISTNULL_EXCEPTION("LALQ000003","获取额度协议及额度台账集合-额度台账列表为空！"),


    //getLmtAccNosByLmtCtrNo 异常定义
    E_GETLMTNOS_PARAMS_EXCEPTION("GPE000001","未获取到额度协议编号！"),

    // 计算授信余额 异常定义
    E_GETLMTBAL_EXCEPTION_1("GLB000001","计算授信余额异常：该额度编号项下额度类型为空，请确认数据完整性！"),
    E_GETLMTBAL_EXCEPTION_2("GLB000001","计算授信余额异常：该额度编号项下启用额度为空，请确认数据完整性！"),
    ;



    /** 异常编码 **/
    private String exceptionCode;

    /** 异常描述 **/
    private String exceptionDesc;

    LmtAccExceptionDefEnums(String exceptionCode,String exceptionDesc){
        this.exceptionCode = exceptionCode;
        this.exceptionDesc = exceptionDesc;
    }

    public String getExceptionCode() {
        return exceptionCode;
    }



    public String getExceptionDesc() {
        return exceptionDesc;
    }
}
