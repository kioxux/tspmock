package cn.com.yusys.yusp.service.server.cmislmt0045;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0045.req.CmisLmt0045ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0045.resp.CmisLmt0045RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ContAccRelService;
import cn.com.yusys.yusp.service.LmtDiscOrgService;
import okhttp3.internal.http2.ErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0045Service
 * @类描述: #对内服务类
 * @功能描述: 校验客户合同项下是否存在未结清业务
 * @修改备注: 2021/7/12     zhangjw   新增接口
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0045Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0045Service.class);

    @Autowired
    private LmtDiscOrgService lmtDiscOrgService ;
    @Autowired
    private ContAccRelService contAccRelService ;

    @Transactional
    public CmisLmt0045RespDto execute(CmisLmt0045ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0045.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0045.value);
        CmisLmt0045RespDto resqDto = new CmisLmt0045RespDto() ;

        try {
            logger.info("校验客户合同项下是否存在未结清业务",DscmsLmtEnum.TRADE_CODE_CMISLMT0045.key);
            if(StringUtils.isBlank(reqDto.getDealBizNo())){
                //将列表放入返回中
                throw new Exception("合同编号不允许为空");
            }

            String flag = contAccRelService.checkDealBizNoHasBussBalanceFlag(reqDto.getDealBizNo());
            if("0".equals(flag)){
                flag = CmisCommonConstants.YES_NO_0;//为0不存在未结清业务
            }else{
                flag = CmisCommonConstants.YES_NO_1;
            }

            //将列表放入返回中
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
            resqDto.setFlag(flag);

        } catch (YuspException e) {
            logger.error("校验客户合同项下是否存在未结清业务：",e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0045.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0045.value);
        return resqDto;
    }

}