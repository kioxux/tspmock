/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtWhiteInfo;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtWhiteInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: yuanz
 * @创建时间: 2021-04-17 11:08:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtWhiteInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtWhiteInfo selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtWhiteInfo> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtWhiteInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtWhiteInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtWhiteInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtWhiteInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: updateByParams
     * @方法描述: 根据主键逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByParams(Map delMap);

    /**
     * 批量插入.
     * @param LmtWhiteInfo 不宜贷款客户列表
     * @return 结果
     */
    int batchInsert(List<LmtWhiteInfo> LmtWhiteInfo);

    /**
     * @方法名称: selectByIds
     * @方法描述: 根据多个主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtWhiteInfo> selectByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectTotalLmtAmtByCusId
     * @方法描述: 根据客户id查询限额总额、已用限额总额和可用总额
     * @参数与返回说明: cusId - 客户编号
     * @算法描述: 无
     */

    Map<String, BigDecimal> selectTotalLmtAmtByCusId(@Param("cusId") String cusId);

    /**
     * 根据客户号查询白名单记录数
     * @param cusId
     * @return
     */
    int selectRecordsByCusId(@Param("cusId") String cusId);

    /**
     * 根据客户号查询产品户买入返售（信用债）白名单记录数
     * @param cusId
     * @return
     */
    int selectResaleRecordsByCusId(@Param("cusId") String cusId);

    /**
     * 根据QueryModel查询白名单额度信息
     * @param queryModel
     * @return
     */
    List<Map<String,Object>> selectInfoByQueryModel(QueryModel queryModel);

    /**
     * 根据客户号更新已用限额
     * @param queryModel
     * @return
     */
    int updateSigUseAmtByCusId(QueryModel queryModel);

    /**
     * 根据额度分项编号更新已用限额
     * @param queryModel
     * @return
     */
    int updateSigUseAmtByLimitSubNo(QueryModel queryModel);

    /**
     * 根据客户号查询额度分项编号
     * @return
     */
    String selectSubAccNoByCusId(@Param("cusId") String cusId);

    /**
     * 根据承兑行行号查询额度分项编号
     * @return
     */
    String selectLmtWhiteInfoByAorgNo(@Param("aorgNo") String aorgNo);

    /**
     * 根据客户号查询额度分项编号
     * @return
     */
    String selectLmtWhiteInfoByCusId(@Param("cusId") String cusId);
}