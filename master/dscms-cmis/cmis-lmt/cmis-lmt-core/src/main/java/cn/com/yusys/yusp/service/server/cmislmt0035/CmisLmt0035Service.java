package cn.com.yusys.yusp.service.server.cmislmt0035;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ContAccRel;
import cn.com.yusys.yusp.domain.LmtContRel;
import cn.com.yusys.yusp.dto.server.cmisLmt0035.req.CmisLmt0035ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0035.resp.CmisLmt0035RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.ContAccRelService;
import cn.com.yusys.yusp.service.LmtContRelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CmisLmt0035Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0035Service.class);

    @Autowired
    private LmtContRelService lmtContRelService ;

    @Autowired
    private ContAccRelService contAccRelService ;

    /**
     * 根据用信台账以及推送的到期日，更新台账以及额度占用关系中的到期日
     * add by lizx 20210707
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0035RespDto execute(CmisLmt0035ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0035.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0035.value);
        CmisLmt0035RespDto respDto = new CmisLmt0035RespDto();

        //台账编号
        String tranAccNo = reqDto.getDealBizNo();
        try {
            //根据分项编号，获取分项信息
            ContAccRel contAccRel = contAccRelService.selectContAccRelByTranAccNo(tranAccNo) ;
            if(contAccRel !=null){
                //到日期
                String endDate = reqDto.getEndDate() ;
                //修改人
                String inputId = reqDto.getInputId() ;
                //修改机构
                String inputBrId =  reqDto.getInputBrId() ;
                //修改时间
                String inputDate = reqDto.getInputDate() ;
                //更新到期日，更新日期机构等
                logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0035.key+"，用信信台账【{}】到期日由【{}】更改为【{}】", tranAccNo , contAccRel.getEndDate(), endDate);
                contAccRel.setEndDate(endDate);
                contAccRel.setUpdDate(inputDate);
                contAccRel.setUpdId(inputId);
                contAccRel.setUpdBrId(inputBrId);
                //更新操作
                contAccRelService.update(contAccRel) ;

                //更新用信对应的额度占用信息
                String dealBizNo = contAccRel.getDealBizNo() ;
                //根据额度占用交易流水号，查询额度占用信息
                List<LmtContRel> lmtcontRelList = lmtContRelService.selectLmtContRelListByDealBizNo(dealBizNo, null) ;
                for (LmtContRel lmtContRel : lmtcontRelList) {
                    if(lmtContRel != null){
                        logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0035.key+"，额度占用【{}】到期日由【{}】更改为【{}】", dealBizNo , lmtContRel.getEndDate(), endDate);
                        //更新到期日，更新日期机构等
                        lmtContRel.setEndDate(endDate);
                        lmtContRel.setUpdDate(inputDate);
                        lmtContRel.setUpdId(inputId);
                        lmtContRel.setUpdBrId(inputBrId);
                        lmtContRelService.update(lmtContRel) ;
                    }
                }
                //返回结果
                respDto.setErrorCode(SuccessEnum.SUCCESS.key);
                respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
            }else{
                throw new YuspException(EclEnum.ECL070060.key, EclEnum.ECL070060.value);
            }
        } catch (YuspException e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0035.value, e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0035.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0035.value);
        return respDto;
    }
}