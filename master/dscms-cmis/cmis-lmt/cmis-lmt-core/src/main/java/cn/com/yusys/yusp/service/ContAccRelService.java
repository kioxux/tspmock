/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.ContAccRel;
import cn.com.yusys.yusp.repository.mapper.ContAccRelMapper;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ContAccRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZR
 * @创建时间: 2021-04-20 10:46:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ContAccRelService {

    @Autowired
    private ContAccRelMapper contAccRelMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public ContAccRel selectByPrimaryKey(String pkId) {
        return contAccRelMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ContAccRel selectByTranAccNo(String tranAccNo) {
        return contAccRelMapper.selectByTranAccNo(tranAccNo);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<ContAccRel> selectAll(QueryModel model) {
        List<ContAccRel> records = (List<ContAccRel>) contAccRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<ContAccRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ContAccRel> list = contAccRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(ContAccRel record) {
        return contAccRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(ContAccRel record) {
        return contAccRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(ContAccRel record) {
        return contAccRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(ContAccRel record) {
        return contAccRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return contAccRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return contAccRelMapper.deleteByIds(ids);
    }


    /**
     * @方法名称:
     * @方法描述：根据交易编号（合同编号），获取合同占用关系信息
     * @参数与返回说明:LimitSubNo 分项额度编号  返回：额度分项编号下的所有占用信息
     * @算法描述: 无
     */
    public List<ContAccRel> selectContAccRelByDealBizNo(String dealBizNo){
        return queryUnsettleListByDealBizNo(dealBizNo) ;
    }


    /**
     * @方法名称:getContAccRelBal
     * @方法描述：根据交易编号，获取合同下业务余额以及占用总额
     * @参数与返回说明:返回未结清金额
     * @算法描述: 无
     */
    public Map<String, BigDecimal> getContAccRelBal(String dealBizNo, String status){
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("dealBizNo", dealBizNo);
        queryModel.addCondition("status", status);
        return contAccRelMapper.getContAccRelBal(queryModel) ;
    }


    /**
     * @方法名称:getContAccRelBal
     * @方法描述：根据交易编号，获取合同下业务余额以及占用总额
     * @参数与返回说明:返回未结清金额
     * @算法描述: 无
     */
    public boolean validCreditrecord(String dealBizNo){
        QueryModel model = new QueryModel();
        model.addCondition("dealBizNo", dealBizNo);
        List<ContAccRel> contAccRelList = queryListByDealBizNo(model) ;
        if(CollectionUtils.isNotEmpty(contAccRelList)){
            return true ;
        }else{
            return false ;
        }
    }

    /**
     * @方法名称:selectContAccRelByTranAccNo
     * @方法描述：根据台账编号，获取台账信息（合同占用关系表）
     * @参数与返回说明:返回未结清金额
     * @算法描述: 无
     */
    public ContAccRel selectContAccRelByTranAccNo(String tranAccNo){
        return selectByTranAccNo(tranAccNo) ;
    }

    /**
     * 额度视图列表，根据合同号查询合同号下台账列表
     * @author zhangjw 2021-05-05
     * @return
     */
    public List<ContAccRel> queryListByDealBizNo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ContAccRel> records = (List<ContAccRel>) contAccRelMapper.queryListByDealBizNo(model);
        PageHelper.clearPage();
        return records;
    }


    /**
     * 根据合同编号，查询合同向下是否存在未结清的业务
     * @author zhangjw 2021-05-18
     * @return
     */
    public List<ContAccRel> queryUnsettleListByDealBizNo(String dealBizNo) {
        return contAccRelMapper.queryUnsettleListByDealBizNo(dealBizNo);
    }


    /**
     * 检查合同编号是否存在
     * @author zhangjw 2021-05-18
     * @return
     */
    public int checkSameDealBizNoIsExist(String dealBizNo) {
        return contAccRelMapper.checkSameDealBizNoIsExist(dealBizNo);
    }

    /**
     * 根据合同编号查询项下台账总余额
     * @param dealBizNo
     * @return
     */
    public BigDecimal getTotalAccTotalBalanceAmtCnyByDealBizNo(String dealBizNo){
        return contAccRelMapper.getTotalAccTotalBalanceAmtCnyByDealBizNo(dealBizNo);
    }


    /**
     * @方法名称: selectAccTotalBalByLimitSubNo
     * @方法描述:
     * @参数与返回说明:
     * @算法描述: 无
     */
    public BigDecimal selectAccTotalBalByLimitSubNo(String limitSubNos) {
        return contAccRelMapper.selectAccTotalBalByLimitSubNo(limitSubNos);
    };


    /**
     * @作者:lizx
     * @方法名称: selectByTranAccNoAndDealNo
     * @方法描述: 根据交易流水号，和合同编号，差选该台账是否已经占用成功
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/9 10:47
     * @param tranAccNo:
     * @param dealBizNo:
     * @return: java.lang.Integer
     * @算法描述: 无
     */
    public ContAccRel selectByTranAccNoAndDealNo(String tranAccNo, String dealBizNo,String sysId){
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("tranAccNo", tranAccNo);
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        if(StringUtils.nonEmpty(dealBizNo)){
            queryModel.addCondition("dealBizNo", dealBizNo);
        }
        if(StringUtils.nonEmpty(sysId)){
            queryModel.addCondition("sysId", sysId);
        }
        return contAccRelMapper.selectByTranAccNoAndDealNo(queryModel) ;
    }

    /**
     * @作者:lizx
     * @方法名称: updateByPKeyInApprLmtChgDetails
     * @方法描述:  传入原始数据，最新数据，一级相关接口信息，跟数据的同时，调用留存记录，留存数据信息appr_lmt_chg_details
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/28 19:44
     * @param record:
     * @param paramMap:
     * @return: int
     * @算法描述: 无
     */
    public int updateByPKeyInApprLmtChgDetails(ContAccRel record, Map<String, String> paramMap){
        return contAccRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @作者:zhangjw
     * @方法名称: checkDealBizNoHasBussBalanceFlag
     * @方法描述: 判断合同编号项下是否存在未结清的业务
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/8/26
     * @param dealBizNo:
     * @return: java.lang.String
     * @算法描述: 无
     */
    public String checkDealBizNoHasBussBalanceFlag(String dealBizNo){
        return contAccRelMapper.checkDealBizNoHasBussBalanceFlag(dealBizNo) ;
    }

    /**
     * @方法名称: selectAccTotalBalByLimitSubNo
     * @方法描述:
     * @参数与返回说明:
     * @算法描述: 无
     */
    public BigDecimal selectAccTotalByLimitSubNos(String limitSubNos) {
        return contAccRelMapper.selectAccTotalByLimitSubNos(limitSubNos);
    }
    /**
     * @方法名称: selectAccTotalBalByLimitSubNo
     * @方法描述:
     * @参数与返回说明:
     * @算法描述: 无
     */
    public BigDecimal selectSpacAccBalByLimitSubNos(String limitSubNos) {
        return contAccRelMapper.selectSpacAccBalByLimitSubNos(limitSubNos);
    }

    /**
     * @方法名称: selectContUseAmtByDealBizNo
     * @方法描述: 根据交易业务编号，获取合同项下台账总占用
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map selectContUseAmtByDealBizNo(String dealBizNo, String tranAccNo) {
        QueryModel paramQuery = new QueryModel() ;
        paramQuery.addCondition("dealBizNo", dealBizNo);
        if(StringUtils.nonBlank(tranAccNo)){
            paramQuery.addCondition("tranAccNo", tranAccNo);
        }
        return contAccRelMapper.selectContUseAmtByDealBizNo(paramQuery);
    }

    /**
     * @方法名称: selectRevolvSpceBalByLimitSubNo
     * @方法描述:
     * @参数与返回说明:
     * @算法描述: 无
     */
    public BigDecimal selectRevolvSpceBalByLimitSubNo(QueryModel model) {
        return contAccRelMapper.selectRevolvSpceBalByLimitSubNo(model);
    }

    /**
     * @方法名称: selectUnRevolvSpceBalByLimitSubNo
     * @方法描述:
     * @参数与返回说明:
     * @算法描述: 无
     */
    public BigDecimal selectUnRevolvSpceBalByLimitSubNo(QueryModel model) {
        return contAccRelMapper.selectUnRevolvSpceBalByLimitSubNo(model);
    }

}
