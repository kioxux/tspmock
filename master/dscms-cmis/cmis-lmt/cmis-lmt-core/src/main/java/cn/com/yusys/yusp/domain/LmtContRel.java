/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtContRel
 * @类描述: lmt_cont_rel数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-04 16:52:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_cont_rel")
public class LmtContRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 交易业务编号 **/
	@Id
	@Column(name = "DEAL_BIZ_NO")
	private String dealBizNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 额度分项编号 **/
	@Column(name = "LIMIT_SUB_NO", unique = false, nullable = true, length = 40)
	private String limitSubNo;
	
	/** 系统编号 **/
	@Column(name = "SYS_ID", unique = false, nullable = true, length = 5)
	private String sysId;
	
	/** 授信类型 **/
	@Column(name = "LMT_TYPE", unique = false, nullable = true, length = 5)
	private String lmtType;
	
	/** 资产编号 **/
	@Column(name = "ASSET_NO", unique = false, nullable = true, length = 40)
	private String assetNo;
	
	/** 交易业务类型 **/
	@Column(name = "DEAL_BIZ_TYPE", unique = false, nullable = true, length = 5)
	private String dealBizType;
	
	/** 业务属性 **/
	@Column(name = "BIZ_ATTR", unique = false, nullable = true, length = 5)
	private String bizAttr;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;
	
	/** 产品类型属性 **/
	@Column(name = "PRD_TYPE_PROP", unique = false, nullable = true, length = 5)
	private String prdTypeProp;
	
	/** 所属条线 **/
	@Column(name = "BELG_LINE", unique = false, nullable = true, length = 65535)
	private String belgLine;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 业务金额 **/
	@Column(name = "BIZ_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bizAmt;
	
	/** 占用总金额（折人民币） **/
	@Column(name = "BIZ_TOTAL_AMT_CNY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bizTotalAmtCny;
	
	/** 占用敞口金额（折人民币） **/
	@Column(name = "BIZ_SPAC_AMT_CNY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bizSpacAmtCny;
	
	/** 占用总余额（折人民币） **/
	@Column(name = "BIZ_TOTAL_BALANCE_AMT_CNY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bizTotalBalanceAmtCny;
	
	/** 占用敞口余额（折人民币） **/
	@Column(name = "BIZ_SPAC_BALANCE_AMT_CNY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bizSpacBalanceAmtCny;
	
	/** 业务保证金比例 **/
	@Column(name = "SECURITY_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal securityRate;
	
	/** 业务保证金金额 **/
	@Column(name = "SECURITY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal securityAmt;
	
	/** 投资资产名称字段 **/
	@Column(name = "INVEST_ASSET_NAME", unique = false, nullable = true, length = 200)
	private String investAssetName;
	
	/** 合同起始日 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 20)
	private String startDate;
	
	/** 合同到期日 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 20)
	private String endDate;
	
	/** 交易业务状态 **/
	@Column(name = "BIZ_STATUS", unique = false, nullable = true, length = 5)
	private String bizStatus;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param dealBizNo
	 */
	public void setDealBizNo(String dealBizNo) {
		this.dealBizNo = dealBizNo;
	}
	
    /**
     * @return dealBizNo
     */
	public String getDealBizNo() {
		return this.dealBizNo;
	}
	
	/**
	 * @param limitSubNo
	 */
	public void setLimitSubNo(String limitSubNo) {
		this.limitSubNo = limitSubNo;
	}
	
    /**
     * @return limitSubNo
     */
	public String getLimitSubNo() {
		return this.limitSubNo;
	}
	
	/**
	 * @param sysId
	 */
	public void setSysId(String sysId) {
		this.sysId = sysId;
	}
	
    /**
     * @return sysId
     */
	public String getSysId() {
		return this.sysId;
	}
	
	/**
	 * @param lmtType
	 */
	public void setLmtType(String lmtType) {
		this.lmtType = lmtType;
	}
	
    /**
     * @return lmtType
     */
	public String getLmtType() {
		return this.lmtType;
	}
	
	/**
	 * @param assetNo
	 */
	public void setAssetNo(String assetNo) {
		this.assetNo = assetNo;
	}
	
    /**
     * @return assetNo
     */
	public String getAssetNo() {
		return this.assetNo;
	}
	
	/**
	 * @param dealBizType
	 */
	public void setDealBizType(String dealBizType) {
		this.dealBizType = dealBizType;
	}
	
    /**
     * @return dealBizType
     */
	public String getDealBizType() {
		return this.dealBizType;
	}
	
	/**
	 * @param bizAttr
	 */
	public void setBizAttr(String bizAttr) {
		this.bizAttr = bizAttr;
	}
	
    /**
     * @return bizAttr
     */
	public String getBizAttr() {
		return this.bizAttr;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}
	
    /**
     * @return prdTypeProp
     */
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}
	
	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}
	
    /**
     * @return belgLine
     */
	public String getBelgLine() {
		return this.belgLine;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param bizAmt
	 */
	public void setBizAmt(java.math.BigDecimal bizAmt) {
		this.bizAmt = bizAmt;
	}
	
    /**
     * @return bizAmt
     */
	public java.math.BigDecimal getBizAmt() {
		return this.bizAmt;
	}
	
	/**
	 * @param bizTotalAmtCny
	 */
	public void setBizTotalAmtCny(java.math.BigDecimal bizTotalAmtCny) {
		this.bizTotalAmtCny = bizTotalAmtCny;
	}
	
    /**
     * @return bizTotalAmtCny
     */
	public java.math.BigDecimal getBizTotalAmtCny() {
		return this.bizTotalAmtCny;
	}
	
	/**
	 * @param bizSpacAmtCny
	 */
	public void setBizSpacAmtCny(java.math.BigDecimal bizSpacAmtCny) {
		this.bizSpacAmtCny = bizSpacAmtCny;
	}
	
    /**
     * @return bizSpacAmtCny
     */
	public java.math.BigDecimal getBizSpacAmtCny() {
		return this.bizSpacAmtCny;
	}
	
	/**
	 * @param bizTotalBalanceAmtCny
	 */
	public void setBizTotalBalanceAmtCny(java.math.BigDecimal bizTotalBalanceAmtCny) {
		this.bizTotalBalanceAmtCny = bizTotalBalanceAmtCny;
	}
	
    /**
     * @return bizTotalBalanceAmtCny
     */
	public java.math.BigDecimal getBizTotalBalanceAmtCny() {
		return this.bizTotalBalanceAmtCny;
	}
	
	/**
	 * @param bizSpacBalanceAmtCny
	 */
	public void setBizSpacBalanceAmtCny(java.math.BigDecimal bizSpacBalanceAmtCny) {
		this.bizSpacBalanceAmtCny = bizSpacBalanceAmtCny;
	}
	
    /**
     * @return bizSpacBalanceAmtCny
     */
	public java.math.BigDecimal getBizSpacBalanceAmtCny() {
		return this.bizSpacBalanceAmtCny;
	}
	
	/**
	 * @param securityRate
	 */
	public void setSecurityRate(java.math.BigDecimal securityRate) {
		this.securityRate = securityRate;
	}
	
    /**
     * @return securityRate
     */
	public java.math.BigDecimal getSecurityRate() {
		return this.securityRate;
	}
	
	/**
	 * @param securityAmt
	 */
	public void setSecurityAmt(java.math.BigDecimal securityAmt) {
		this.securityAmt = securityAmt;
	}
	
    /**
     * @return securityAmt
     */
	public java.math.BigDecimal getSecurityAmt() {
		return this.securityAmt;
	}
	
	/**
	 * @param investAssetName
	 */
	public void setInvestAssetName(String investAssetName) {
		this.investAssetName = investAssetName;
	}
	
    /**
     * @return investAssetName
     */
	public String getInvestAssetName() {
		return this.investAssetName;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param bizStatus
	 */
	public void setBizStatus(String bizStatus) {
		this.bizStatus = bizStatus;
	}
	
    /**
     * @return bizStatus
     */
	public String getBizStatus() {
		return this.bizStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}