package cn.com.yusys.yusp.service.server.cmislmt0057;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.domain.ApprStrMtableInfo;
import cn.com.yusys.yusp.domain.ContAccRel;
import cn.com.yusys.yusp.domain.LmtContRel;
import cn.com.yusys.yusp.dto.server.cmislmt0057.req.CmisLmt0057ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0057.resp.CmisLmt0057RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import com.alibaba.fastjson.JSON;
import org.apache.commons.collections4.CollectionUtils;
import org.checkerframework.checker.units.qual.A;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0057Service
 * @类描述: #对内服务类
 * @功能描述: 线上产品自动生成合同台账
 * @创建时间: 2021-09-09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0057Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0057Service.class);

    @Autowired
    private Comm4Service comm4Service ;

    @Autowired
    private ApprStrMtableInfoService apprStrMtableInfoService ;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;

    @Autowired
    private LmtContRelService lmtContRelService ;

    @Autowired
    private ContAccRelService contAccRelService ;

    @Autowired
    private LmtSubPrdMappConfService lmtSubPrdMappConfService ;

    /**
     * 线上产品自动生成合同台账
     * 描述：综合接口，小微系统单笔单批调用，同时生成额度，合同，台账信息
     * add by lizx 2021-09-09
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0057RespDto execute(CmisLmt0057ReqDto reqDto) throws YuspException,Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0057.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0057.value);
        CmisLmt0057RespDto respDto = new CmisLmt0057RespDto();
        logger.info("{}初始化批复台账信息------------------start", DscmsLmtEnum.TRADE_CODE_CMISLMT0057.key);
        //生成额度信息
        ApprStrMtableInfo apprStrMtableInfo = new ApprStrMtableInfo() ;
        //主键
        apprStrMtableInfo.setPkId(comm4Service.generatePkId());
        //批复台账编号
        apprStrMtableInfo.setApprSerno(reqDto.getAccNo());
        //客户编号
        apprStrMtableInfo.setCusId(reqDto.getCusId());
        //客户名称
        apprStrMtableInfo.setCusName(reqDto.getCusName());
        //客户主体类型
        apprStrMtableInfo.setCusType(CmisLmtConstants.STD_ZB_CUS_CATALOG1);
        //状态
        apprStrMtableInfo.setApprStatus(CmisLmtConstants.STD_REPLY_STATUS_01);
        //金融机构代码
        apprStrMtableInfo.setInstuCde(reqDto.getInstuCde());
        //集团批复台账编号
        apprStrMtableInfo.setGrpApprSerno("");
        //期限(月) TODO:期限月放空
        apprStrMtableInfo.setTerm(0);
        //起始日期
        apprStrMtableInfo.setStartDate(reqDto.getLmtStartDate());
        //到期日期
        apprStrMtableInfo.setEndDate(reqDto.getLmtEndDate());
        //授信类型
        apprStrMtableInfo.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_01);
        //授信模式
        apprStrMtableInfo.setLmtMode(CmisLmtConstants.STD_ZB_LMT_MODE_02);
        //责任人
        apprStrMtableInfo.setManagerBrId(reqDto.getInputBrId());
        //责任机构
        apprStrMtableInfo.setManagerId(reqDto.getInputId());
        //操作类型
        apprStrMtableInfo.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        //登记人
        apprStrMtableInfo.setInputId(reqDto.getInputId());
        //登记机构
        apprStrMtableInfo.setInputBrId(reqDto.getInputBrId());
        //登记日期
        apprStrMtableInfo.setInputDate(reqDto.getInputDate());
        //更新人
        apprStrMtableInfo.setUpdId(reqDto.getInputId());
        //更新机构
        apprStrMtableInfo.setUpdBrId(reqDto.getInputId());
        //最近更新日期
        apprStrMtableInfo.setUpdDate(reqDto.getInputDate());
        //创建时间
        apprStrMtableInfo.setCreateTime(DateUtils.getCurrTimestamp());
        //更新时间
        apprStrMtableInfo.setUpdateTime(DateUtils.getCurrTimestamp());
        apprStrMtableInfoService.insert(apprStrMtableInfo);
        logger.info("{}初始化批复台账信息------------------end,台账信息{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0057.key, JSON.toJSONString(apprStrMtableInfo));

        /*********************新增分项额度信息***********************/
        logger.info("{}初始化批复台账分项信息------------------start", DscmsLmtEnum.TRADE_CODE_CMISLMT0057.key);
        //产品编号
        String prdId = reqDto.getPrdNo() ;
        List<Map<String, String>> limitList = lmtSubPrdMappConfService.selectSubNoAndNameByStrNoAndPrdId(CmisLmtConstants.STD_ZB_LMT_STR_MTABLE_CONF_02, prdId) ;
        if(CollectionUtils.isEmpty(limitList)){
            throw new YuspException(EclEnum.ECL070140.key, EclEnum.ECL070140.value);
        }
        String limitPrdNo = limitList.get(0).get("limitSubNo") ;
        String limitPrdName = limitList.get(0).get("limitSubName") ;
        //是否低风险
        String isLriskLmt = reqDto.getIsLriskLmt() ;
        ApprLmtSubBasicInfo apprLmtSubBasicInfo = new ApprLmtSubBasicInfo() ;
        //主键
        apprLmtSubBasicInfo.setPkId(comm4Service.generatePkId());
        //FK_PKID
        apprLmtSubBasicInfo.setFkPkid(reqDto.getAccNo());
        //批复分项流水号
        apprLmtSubBasicInfo.setApprSubSerno(reqDto.getAccSubNo());
        //客户编号
        apprLmtSubBasicInfo.setCusId(reqDto.getCusId());
        //客户名称
        apprLmtSubBasicInfo.setCusName(reqDto.getCusName());
        //客户主体类型
        apprLmtSubBasicInfo.setCusType(CmisLmtConstants.STD_ZB_CUS_CATALOG1);
        //额度分项编号
        apprLmtSubBasicInfo.setLimitSubNo(limitPrdNo);
        //额度分项名称
        apprLmtSubBasicInfo.setLimitSubName(limitPrdName);
        //父节点
        apprLmtSubBasicInfo.setParentId("");
        //是否低风险授信
        apprLmtSubBasicInfo.setIsLriskLmt(reqDto.getIsLriskLmt());
        //授信分项类型
        apprLmtSubBasicInfo.setLmtSubType(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_02);
        //是否循环
        apprLmtSubBasicInfo.setIsRevolv(reqDto.getIsRevolv());
        //额度类型
        apprLmtSubBasicInfo.setLimitType(CmisLmtConstants.STD_ZB_LIMIT_TYPE_02);
        //适用担保方式
        apprLmtSubBasicInfo.setSuitGuarWay(reqDto.getSuitGuarWay());
        //额度宽限期（月）
        apprLmtSubBasicInfo.setLmtGraper(0);
        //授信总额
        apprLmtSubBasicInfo.setAvlAmt(reqDto.getAvlAmt());
        //已用总额
        apprLmtSubBasicInfo.setOutstndAmt(reqDto.getDealBizAmt());
        if(CmisLmtConstants.YES_NO_Y.equals(isLriskLmt)){
            //敞口金额
            apprLmtSubBasicInfo.setSpacAmt(BigDecimal.ZERO);
            //敞口已用额度
            apprLmtSubBasicInfo.setSpacOutstndAmt(BigDecimal.ZERO);
            //已出帐金额
            apprLmtSubBasicInfo.setPvpOutstndAmt(reqDto.getDealBizAmtCny());
            BigDecimal avlOutstndAmt = BigDecimalUtil.replaceNull(reqDto.getAvlAmt()).subtract(reqDto.getDealBizAmtCny()) ;
            //可出账金额
            apprLmtSubBasicInfo.setAvlOutstndAmt(avlOutstndAmt);
        }else{
            //敞口金额
            apprLmtSubBasicInfo.setSpacAmt(reqDto.getAvlAmt());
            //敞口已用额度
            apprLmtSubBasicInfo.setSpacOutstndAmt(reqDto.getDealBizAmt());
            //用信敞口余额
            apprLmtSubBasicInfo.setLoanSpacBalance(reqDto.getDealBizSpacAmtCny());
            //已出帐金额
            apprLmtSubBasicInfo.setPvpOutstndAmt(reqDto.getDealBizSpacAmtCny());
            BigDecimal avlOutstndAmt = BigDecimalUtil.replaceNull(reqDto.getAvlAmt()).subtract(reqDto.getDealBizSpacAmtCny()) ;
            //可出账金额
            apprLmtSubBasicInfo.setAvlOutstndAmt(avlOutstndAmt);
        }
        //贷款余额
        apprLmtSubBasicInfo.setLoanBalance(reqDto.getDealBizAmtCny());
        //状态
        apprLmtSubBasicInfo.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
        //币种
        apprLmtSubBasicInfo.setCurType(CmisBizConstants.STD_ZB_CUR_TYP_CNY);
        //保证金预留比例
        apprLmtSubBasicInfo.setBailPreRate(BigDecimal.ZERO);
        //年利率
        //授信总额累加
        apprLmtSubBasicInfo.setLmtAmtAdd(BigDecimal.ZERO);
        //是否预授信
        apprLmtSubBasicInfo.setIsPreCrd(CmisBizConstants.STD_ZB_YES_NO_N);
        //额度起始日
        apprLmtSubBasicInfo.setStartDate(reqDto.getLmtStartDate());
        //额度有效期
        apprLmtSubBasicInfo.setLmtDate(reqDto.getLmtEndDate());
        //期限
        apprLmtSubBasicInfo.setTerm(0);
        //操作类型
        apprLmtSubBasicInfo.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        //登记人
        apprLmtSubBasicInfo.setInputId(reqDto.getInputId());
        //登记机构
        apprLmtSubBasicInfo.setInputBrId(reqDto.getInputBrId());
        //登记时间
        apprLmtSubBasicInfo.setInputDate(reqDto.getInputDate());
        //最近更新人
        apprLmtSubBasicInfo.setUpdId(reqDto.getInputId());
        //最近更新机构
        apprLmtSubBasicInfo.setUpdBrId(reqDto.getInputBrId());
        //最近更新日期
        apprLmtSubBasicInfo.setUpdDate(reqDto.getInputDate());
        //创建时间
        apprLmtSubBasicInfo.setCreateTime(DateUtils.getCurrTimestamp());
        //修改时间
        apprLmtSubBasicInfo.setUpdateTime(DateUtils.getCurrTimestamp());
        apprLmtSubBasicInfoService.insert(apprLmtSubBasicInfo) ;
        logger.info("{}初始化批复台账分项信息------------------end,台账信息{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0057.key, JSON.toJSONString(apprLmtSubBasicInfo));


        /********************* 生成合同 ***********************/
        logger.info("{}初始化合同信息------------------start", DscmsLmtEnum.TRADE_CODE_CMISLMT0057.key);
        LmtContRel lmtContRel = new LmtContRel() ;
        //主键
        lmtContRel.setPkId(comm4Service.generatePkId());
        //客户编号
        lmtContRel.setCusId(reqDto.getCusId());
        //客户名称
        lmtContRel.setCusName(reqDto.getCusName());
        //交易业务编号
        lmtContRel.setDealBizNo(reqDto.getBizNo());
        //额度分项编号
        lmtContRel.setLimitSubNo(reqDto.getAccSubNo());
        //系统编号
        lmtContRel.setSysId(reqDto.getSysNo());
        //授信类型
        lmtContRel.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_01);
        //资产编号
        lmtContRel.setAssetNo("");
        //交易业务类型
        lmtContRel.setDealBizType(reqDto.getDealBizType());
        //业务属性
        lmtContRel.setBizAttr(CmisLmtConstants.STD_ZB_BIZ_ATTR_1);
        //产品编号
        lmtContRel.setPrdId(prdId);
        //产品名称
        lmtContRel.setPrdName(reqDto.getPrdName());
        //业务金额
        BigDecimal dealBizAmt = BigDecimalUtil.replaceNull(reqDto.getDealBizAmt()) ;
        lmtContRel.setBizAmt(dealBizAmt);
        //占用总金额（折人民币）
        lmtContRel.setBizTotalAmtCny(dealBizAmt);
        //占用总余额（折人民币）
        lmtContRel.setBizTotalBalanceAmtCny(dealBizAmt);
        if(CmisLmtConstants.YES_NO_Y.equals(isLriskLmt)){
            //占用敞口余额（折人民币）
            lmtContRel.setBizSpacAmtCny(BigDecimal.ZERO);
            //占用敞口金额（折人民币）
            lmtContRel.setBizSpacBalanceAmtCny(BigDecimal.ZERO);
        }else{
            //占用敞口余额（折人民币）
            lmtContRel.setBizSpacAmtCny(dealBizAmt);
            //占用敞口金额（折人民币）
            lmtContRel.setBizSpacBalanceAmtCny(dealBizAmt);
        }
        //业务保证金比例
        lmtContRel.setSecurityAmt(BigDecimal.ZERO);
        //业务保证金金额
        lmtContRel.setSecurityRate(BigDecimal.ZERO);
        //合同起始日
        lmtContRel.setStartDate(reqDto.getBizStartDate());
        //合同到期日
        lmtContRel.setEndDate(reqDto.getBizEndDate());
        //交易业务状态
        lmtContRel.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_200);
        //操作类型
        lmtContRel.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        //登记人
        lmtContRel.setInputId(reqDto.getInputId());
        //登记机构
        lmtContRel.setInputBrId(reqDto.getInputBrId());
        //登记时间
        lmtContRel.setInputDate(reqDto.getInputDate());
        //最近更新人
        lmtContRel.setUpdId(reqDto.getInputId());
        //最近更新机构
        lmtContRel.setUpdBrId(reqDto.getInputBrId());
        //最近更新日期
        lmtContRel.setUpdDate(reqDto.getInputDate());
        //创建时间
        lmtContRel.setCreateTime(DateUtils.getCurrTimestamp());
        //修改时间
        lmtContRel.setUpdateTime(DateUtils.getCurrTimestamp());
        lmtContRelService.insert(lmtContRel);
        logger.info("{}初始合同信息------------------end,合同信息-{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0057.key, JSON.toJSONString(lmtContRel));

        /********************* 生成台账信息 ***********************/
        logger.info("{}初始台账信息------------------end,台账信息-{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0057.key);
        ContAccRel contAccRel = new ContAccRel() ;
        //主键
        contAccRel.setPkId(comm4Service.generatePkId());
        //交易台账编号
        contAccRel.setTranAccNo(reqDto.getDealBizNo());
        //客户编号
        contAccRel.setCusId(reqDto.getCusId());
        //客户名称
        contAccRel.setCusName(reqDto.getCusName());
        //交易业务编号
        contAccRel.setDealBizNo(reqDto.getBizNo());
        //系统编号
        contAccRel.setSysId(reqDto.getSysNo());
        //产品编号
        contAccRel.setPrdId(prdId);
        //产品名称
        contAccRel.setPrdName(reqDto.getPrdName());
        //交易台账状态
        contAccRel.setStatus(CmisLmtConstants.STD_ZB_CONT_STATUS_200);
        //业务金额
        contAccRel.setBizAmt(reqDto.getDealBizAmt());
        //台账占用总金额（折人民币）
        contAccRel.setAccTotalAmtCny(reqDto.getDealBizAmtCny());
        //台账占用总余额（折人民币）
        contAccRel.setAccTotalBalanceAmtCny(reqDto.getDealBizAmtCny());
        if(CmisLmtConstants.YES_NO_Y.equals(isLriskLmt)){
            //台账占用敞口金额（折人民币）
            contAccRel.setAccSpacAmtCny(BigDecimal.ZERO);
            //台账占用敞口余额（折人民币）
            contAccRel.setAccSpacBalanceAmtCny(BigDecimal.ZERO);
        }else{
            //台账占用敞口金额（折人民币）
            contAccRel.setAccSpacAmtCny(reqDto.getDealBizSpacAmtCny());
            //台账占用敞口余额（折人民币）
            contAccRel.setAccSpacBalanceAmtCny(reqDto.getDealBizSpacAmtCny());
        }
        //业务保证金比例
        contAccRel.setSecurityAmt(BigDecimal.ZERO);
        //业务保证金金额
        contAccRel.setSecurityRate(BigDecimal.ZERO);
        //合同起始日
        contAccRel.setStartDate(reqDto.getAccStartDate());
        //合同到期日
        contAccRel.setEndDate(reqDto.getAccEndDate());
        //操作类型
        contAccRel.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        //登记人
        contAccRel.setInputId(reqDto.getInputId());
        //登记机构
        contAccRel.setInputBrId(reqDto.getInputBrId());
        //登记时间
        contAccRel.setInputDate(reqDto.getInputDate());
        //最近更新人
        contAccRel.setUpdId(reqDto.getInputId());
        //最近更新机构
        contAccRel.setUpdBrId(reqDto.getInputBrId());
        //最近更新日期
        contAccRel.setUpdDate(reqDto.getInputDate());
        //创建时间
        contAccRel.setCreateTime(DateUtils.getCurrTimestamp());
        //修改时间
        contAccRel.setUpdateTime(DateUtils.getCurrTimestamp());
        contAccRelService.insert(contAccRel);
        logger.info("{}初始台账信息------------------end,台账信息-{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0057.key, JSON.toJSONString(contAccRel));

        respDto.setErrorCode(SuccessEnum.SUCCESS.key);
        respDto.setErrorMsg(SuccessEnum.SUCCESS.value);

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0057.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0057.value);
        return respDto;
    }
}