package cn.com.yusys.yusp.service.server.cmislmt0066;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0066.req.CmisLmt0066ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0066.resp.CmisLmt0066RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.LmtContRelService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0066Service
 * @类描述: #对内服务类
 * @功能描述: 获取合同占用分项编号-单一客户
 * @创建时间: 2021-09-30
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0066Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0066Service.class);

    @Autowired
    private LmtContRelService lmtContRelService ;


    @Transactional
    public CmisLmt0066RespDto execute(CmisLmt0066ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0066.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0066.value);
        CmisLmt0066RespDto respDto = new CmisLmt0066RespDto();
        try {
            String dealBizNO = reqDto.getDealBizNo();

            String apprSubserno = "" ;
            String parentId = "" ;
            String limitSubNo = "" ;

            if(StringUtils.isBlank(dealBizNO)){
                throw new YuspException("90011","合同号不允许为空");
            }
            List<Map<String, String>> list=  lmtContRelService.selectLimitSubNoByDealBizNoDY(dealBizNO);

            if(CollectionUtils.isNotEmpty(list)){
                for (Map<String, String> map : list) {
                    limitSubNo = map.get("limitSubNo") ;
                    if(CmisLmtConstants.STD_PRD_SPBT_10020201.equals(limitSubNo)){
                        break ;
                    }
                    apprSubserno = map.get("apprSubSerno") ;
                    parentId = map.get("parentId") ;
                }
            }
            respDto.setLimitSubNo(apprSubserno);
            respDto.setParentId(parentId);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("【cmislmt0066】获取合同占用分项编号-单一客户：" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0066.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0066.value);
        return respDto;
    }
}