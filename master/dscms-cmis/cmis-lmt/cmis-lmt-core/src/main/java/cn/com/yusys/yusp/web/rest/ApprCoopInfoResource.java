/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ApprCoopInfo;
import cn.com.yusys.yusp.service.ApprCoopInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprCoopInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: yuanz
 * @创建时间: 2021-04-20 22:36:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/apprcoopinfo")
public class ApprCoopInfoResource {
    @Autowired
    private ApprCoopInfoService apprCoopInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<ApprCoopInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<ApprCoopInfo> list = apprCoopInfoService.selectAll(queryModel);
        return new ResultDto<List<ApprCoopInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<ApprCoopInfo>> index(QueryModel queryModel) {
        List<ApprCoopInfo> list = apprCoopInfoService.selectByModel(queryModel);
        return new ResultDto<List<ApprCoopInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<ApprCoopInfo> show(@PathVariable("pkId") String pkId) {
        ApprCoopInfo apprCoopInfo = apprCoopInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<ApprCoopInfo>(apprCoopInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<ApprCoopInfo> create(@RequestBody ApprCoopInfo apprCoopInfo) {
        apprCoopInfoService.insert(apprCoopInfo);
        return new ResultDto<ApprCoopInfo>(apprCoopInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ApprCoopInfo apprCoopInfo) {
        int result = apprCoopInfoService.update(apprCoopInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = apprCoopInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = apprCoopInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param queryModel
     * @return
     * @date 2021/4/22 15:09
     * @version 1.0.0
     * @desc    根据客户号和状态查询合作方台账信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectByCusIdAndStatus")
    protected ResultDto<List<Map<String,Object>>> selectByCusIdAndStatus(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprCoopInfoService.selectByCusIdAndStatus(queryModel);
        return new ResultDto<List<Map<String,Object>>>(list);
    }

    /**
     * @param queryModel
     * @desc 合作方额度视图列表（支持查并表额度）：
     *      instuCde金融机构号;notInstuCde不为金融机构号；cusId客户号；cusName客户名称；copType合作方类型
     * @date 2021/5/6
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    @PostMapping("/queryListByInstuCde")
    protected ResultDto<List<Map<String,Object>>> queryListByInstuCde(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprCoopInfoService.queryListByInstuCde(queryModel);
        return new ResultDto<List<Map<String,Object>>>(list);
    }

    /**
     * @param queryModel
     * @return
     * @date 2021/4/22 15:09
     * @version 1.0.0
     * @desc    根据客户号和状态查询合作方台账信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/queryApprCoopBySerno")
    protected ResultDto<List<Map<String,Object>>> queryApprCoopBySerno(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprCoopInfoService.queryApprCoopBySerno(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @param queryModel
     * @desc 1、根据合作方客户号查找客户合作方额度项下关联合同列表
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    @PostMapping("/selectCoopLmtContRelByCusId")
    protected ResultDto<List<Map<String,Object>>> selectCoopLmtContRelByCusId(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprCoopInfoService.selectCoopLmtContRelByCusId(queryModel);
        return new ResultDto<List<Map<String,Object>>>(list);
    }

    /**
     * @param queryModel
     * @desc 5、根据合作方客户号查找客户合作方额度项下关联合同汇总
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    @PostMapping("/selectTotalCoopLmtContRelByCusId")
    protected ResultDto<Map<String,Object>> selectTotalCoopLmtContRelByCusId(@RequestBody QueryModel queryModel) {
        Map<String,Object> resultMap = apprCoopInfoService.selectTotalCoopLmtContRelByCusId(queryModel);
        return new ResultDto<Map<String,Object>>(resultMap);
    }

    /**
     * @param queryModel
     * @desc 2、根据合作方额度分项编号查找客户合作方额度项下关联合同列表
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    @PostMapping("/selectCoopLmtContRelByApprSubSerno")
    protected ResultDto<List<Map<String,Object>>> selectCoopLmtContRelByApprSubSerno(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprCoopInfoService.selectCoopLmtContRelByApprSubSerno(queryModel);
        return new ResultDto<List<Map<String,Object>>>(list);
    }

    /**
     * @param queryModel
     * @desc 6、根据合作方额度分项编号查找客户合作方额度项下关联合同 汇总
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    @PostMapping("/selectTotalCoopLmtContRelByApprSubSerno")
    protected ResultDto<Map<String,Object>> selectTotalCoopLmtContRelByApprSubSerno(@RequestBody QueryModel queryModel) {
        Map<String,Object> resultMap = apprCoopInfoService.selectTotalCoopLmtContRelByApprSubSerno(queryModel);
        return new ResultDto<Map<String,Object>>(resultMap);
    }

    /**
     * @param queryModel
     * @desc 3、根据合作方客户号查找客户合作方额度项下关联合同下关联的台账列表
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    @PostMapping("/selectCoopAccContRelByCusId")
    protected ResultDto<List<Map<String,Object>>> selectCoopAccContRelByCusId(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprCoopInfoService.selectCoopAccContRelByCusId(queryModel);
        return new ResultDto<List<Map<String,Object>>>(list);
    }

    /**
     * @param queryModel
     * @desc 7、根据合作方客户号查找客户合作方额度项下关联合同下关联的台账汇总
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    @PostMapping("/selectTotalCoopAccContRelByCusId")
    protected ResultDto<Map<String,Object>> selectTotalCoopAccContRelByCusId(@RequestBody QueryModel queryModel) {
        Map<String,Object> resultMap = apprCoopInfoService.selectTotalCoopAccContRelByCusId(queryModel);
        return new ResultDto<Map<String,Object>>(resultMap);
    }

    /**
     * @param queryModel
     * @desc 4、根据合作方额度分项编号查找客户合作方额度项下关联合同下关联的台账列表
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    @PostMapping("/selectCoopAccContRelByApprSubSerno")
    protected ResultDto<List<Map<String,Object>>> selectCoopAccContRelByApprSubSerno(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprCoopInfoService.selectCoopAccContRelByApprSubSerno(queryModel);
        return new ResultDto<List<Map<String,Object>>>(list);
    }

    /**
     * @param queryModel
     * @desc 8、根据合作方额度分项编号查找客户合作方额度项下关联合同下关联的台账汇总
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    @PostMapping("/selectTotalCoopAccContRelByApprSubSerno")
    protected ResultDto<Map<String,Object>> selectTotalCoopAccContRelByApprSubSerno(@RequestBody QueryModel queryModel) {
        Map<String,Object> resultMap = apprCoopInfoService.selectTotalCoopAccContRelByApprSubSerno(queryModel);
        return new ResultDto<Map<String,Object>>(resultMap);
    }

    /**
     * @param queryModel
     * @desc 合作方额度列表，以批复为维度（冻结解冻使用）：
     *      instuCde金融机构号;notInstuCde不为金融机构号；cusId客户号；cusName客户名称；copType合作方类型
     * @date 2021/9/29
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    @PostMapping("/queryListByInstuCdeForDj")
    protected ResultDto<List<Map<String,Object>>> queryListByInstuCdeForDj(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprCoopInfoService.queryListByInstuCdeForDj(queryModel);
        return new ResultDto<List<Map<String,Object>>>(list);
    }
}
