/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.vo.ManaBusiLmtVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ManaBusiLmt;
import cn.com.yusys.yusp.service.ManaBusiLmtService;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ManaBusiLmtResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-16 15:32:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/manabusilmt")
public class ManaBusiLmtResource {
    @Autowired
    private ManaBusiLmtService manaBusiLmtService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<ManaBusiLmt>> query() {
        QueryModel queryModel = new QueryModel();
        List<ManaBusiLmt> list = manaBusiLmtService.selectAll(queryModel);
        return new ResultDto<List<ManaBusiLmt>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<ManaBusiLmt>> index(QueryModel queryModel) {
        List<ManaBusiLmt> list = manaBusiLmtService.selectByModel(queryModel);
        return new ResultDto<List<ManaBusiLmt>>(list);
    }

    /**
     * @函数名称:selectbymodel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<Map>> selectbymodel(@RequestBody QueryModel queryModel) {
        List<Map> list = manaBusiLmtService.selectListByModel(queryModel);
        return new ResultDto<List<Map>>(list);
    }

    /**
     * 异步下载业务条线额度管控数据
     */
    @PostMapping("/exportmanabusilmt")
    public ResultDto<ProgressDto> asyncExportManaBusiLmt(@RequestBody QueryModel model) {
        ProgressDto progressDto = manaBusiLmtService.asyncExportManaBusiLmt(model);
        return ResultDto.success(progressDto);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<ManaBusiLmt> create(@RequestBody ManaBusiLmt manaBusiLmt) throws URISyntaxException {
        manaBusiLmtService.insert(manaBusiLmt);
        return new ResultDto<ManaBusiLmt>(manaBusiLmt);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ManaBusiLmt manaBusiLmt) throws URISyntaxException {
        int result = manaBusiLmtService.update(manaBusiLmt);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String busiType, String orgAreaType) {
        int result = manaBusiLmtService.deleteByPrimaryKey(busiType, orgAreaType);
        return new ResultDto<Integer>(result);
    }

}
