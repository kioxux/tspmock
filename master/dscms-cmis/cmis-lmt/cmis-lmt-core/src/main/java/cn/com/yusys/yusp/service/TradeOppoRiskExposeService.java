/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.LmtDiscOrg;
import cn.com.yusys.yusp.domain.ManaOrgLmt;
import cn.com.yusys.yusp.repository.mapper.LmtDiscOrgMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.vo.LmtDiscOrgTempExportVo;
import cn.com.yusys.yusp.vo.ManaOrgLmtVo;
import cn.com.yusys.yusp.vo.TradeOppoRiskExposeTempExportVo;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.TradeOppoRiskExpose;
import cn.com.yusys.yusp.repository.mapper.TradeOppoRiskExposeMapper;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: TradeOppoRiskExposeService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-23 15:37:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class TradeOppoRiskExposeService {

    @Autowired
    private TradeOppoRiskExposeMapper tradeOppoRiskExposeMapper;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    private SequenceTemplateService sequenceTemplateClient;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public TradeOppoRiskExpose selectByPrimaryKey(String cusId, String prdName) {
        return tradeOppoRiskExposeMapper.selectByPrimaryKey(cusId, prdName);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<TradeOppoRiskExpose> selectAll(QueryModel model) {
        List<TradeOppoRiskExpose> records = (List<TradeOppoRiskExpose>) tradeOppoRiskExposeMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<TradeOppoRiskExpose> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<TradeOppoRiskExpose> list = tradeOppoRiskExposeMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(TradeOppoRiskExpose record) {
        return tradeOppoRiskExposeMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(TradeOppoRiskExpose record) {
        return tradeOppoRiskExposeMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(TradeOppoRiskExpose record) {
        return tradeOppoRiskExposeMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(TradeOppoRiskExpose record) {
        return tradeOppoRiskExposeMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * 异步下载交易对手风险暴露模板
     * @return 导出进度信息
     */
    public ProgressDto asyncExportTradeOppoRiskExposeTemp() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {
                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(TradeOppoRiskExposeTempExportVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 批量插入交易对手风险暴露
     *
     * @param perTradeOppoRiskExposeList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int importTradeopporiskexpose(List<Object> perTradeOppoRiskExposeList) {
        List<TradeOppoRiskExpose> tradeOppoRiskExposeList = (List<TradeOppoRiskExpose>) BeanUtils.beansCopy(perTradeOppoRiskExposeList, TradeOppoRiskExpose.class);
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            TradeOppoRiskExposeMapper tradeOppoRiskExposeMapper = sqlSession.getMapper(TradeOppoRiskExposeMapper.class);
            for (TradeOppoRiskExpose tradeOppoRiskExpose : tradeOppoRiskExposeList) {
                QueryModel query = new QueryModel() ;
                query.addCondition("cusId", tradeOppoRiskExpose.getCusId());
                query.addCondition("prdName", tradeOppoRiskExpose.getPrdName());
                query.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
                List<TradeOppoRiskExpose> tradeOppoRiskExposes = tradeOppoRiskExposeMapper.selectByModel(query) ;
                //数据库中已存在
                if (CollectionUtils.isNotEmpty(tradeOppoRiskExposes)){
                    tradeOppoRiskExposeMapper.updateByPrimaryKeySelective(tradeOppoRiskExpose);
                }//数据库中不存在，新增信息
                else{
                    //操作类型
                    tradeOppoRiskExpose.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
                    tradeOppoRiskExposeMapper.insertSelective(tradeOppoRiskExpose);
                }
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        return perTradeOppoRiskExposeList.size();
    }

    /**
     * 异步导出交易对手风险暴露数据
     * @return 导出进度信息
     */
    public ProgressDto asyncExportTradeopporiskexpose(QueryModel model) {
        model.getCondition().put("oprType", CmisBizConstants.OPR_TYPE_01);
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel)object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            List<TradeOppoRiskExpose> tradeOppoRiskExposes = selectByModel(queryModeTemp);
            return tradeOppoRiskExposes;
        };
        ExportContext exportContext = ExportContext.of(TradeOppoRiskExposeTempExportVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String cusId, String prdName) {
        return tradeOppoRiskExposeMapper.deleteByPrimaryKey(cusId, prdName);
    }

}
