package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: TranAbnFlowInfo
 * @类描述: tran_abn_flow_info数据实体类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-05-03 10:17:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class TranAbnFlowInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 流水号 **/
	private String serno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 客户主体类型 **/
	private String cusType;
	
	/** 证件类型STD_ZB_CERT_TYP **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 交易业务编号 **/
	private String dealBizNo;
	
	/** 交易类型STD_ZB_DEAL_TYP **/
	private String dealTyp;
	
	/** 额度结构编号 **/
	private String limitStrNo;
	
	/** 额度结构名称 **/
	private String limitStrName;
	
	/** 版本号 **/
	private String version;
	
	/** 额度分项编号 **/
	private String limitSubNo;
	
	/** 额度分项名称 **/
	private String limitSubName;
	
	/** 交易币种STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 交易汇率 **/
	private java.math.BigDecimal tranExchangeRate;
	
	/** 交易描述 **/
	private String tranDec;
	
	/** 交易时间 **/
	private String tranTime;
	
	/** 交易金额 **/
	private java.math.BigDecimal tranAmt;
	
	/** 系统编号STD_ZB_SYS_ID **/
	private String sysId;
	
	/** 状态 **/
	private String status;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType == null ? null : cusType.trim();
	}
	
    /**
     * @return CusType
     */	
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param dealBizNo
	 */
	public void setDealBizNo(String dealBizNo) {
		this.dealBizNo = dealBizNo == null ? null : dealBizNo.trim();
	}
	
    /**
     * @return DealBizNo
     */	
	public String getDealBizNo() {
		return this.dealBizNo;
	}
	
	/**
	 * @param dealTyp
	 */
	public void setDealTyp(String dealTyp) {
		this.dealTyp = dealTyp == null ? null : dealTyp.trim();
	}
	
    /**
     * @return DealTyp
     */	
	public String getDealTyp() {
		return this.dealTyp;
	}
	
	/**
	 * @param limitStrNo
	 */
	public void setLimitStrNo(String limitStrNo) {
		this.limitStrNo = limitStrNo == null ? null : limitStrNo.trim();
	}
	
    /**
     * @return LimitStrNo
     */	
	public String getLimitStrNo() {
		return this.limitStrNo;
	}
	
	/**
	 * @param limitStrName
	 */
	public void setLimitStrName(String limitStrName) {
		this.limitStrName = limitStrName == null ? null : limitStrName.trim();
	}
	
    /**
     * @return LimitStrName
     */	
	public String getLimitStrName() {
		return this.limitStrName;
	}
	
	/**
	 * @param version
	 */
	public void setVersion(String version) {
		this.version = version == null ? null : version.trim();
	}
	
    /**
     * @return Version
     */	
	public String getVersion() {
		return this.version;
	}
	
	/**
	 * @param limitSubNo
	 */
	public void setLimitSubNo(String limitSubNo) {
		this.limitSubNo = limitSubNo == null ? null : limitSubNo.trim();
	}
	
    /**
     * @return LimitSubNo
     */	
	public String getLimitSubNo() {
		return this.limitSubNo;
	}
	
	/**
	 * @param limitSubName
	 */
	public void setLimitSubName(String limitSubName) {
		this.limitSubName = limitSubName == null ? null : limitSubName.trim();
	}
	
    /**
     * @return LimitSubName
     */	
	public String getLimitSubName() {
		return this.limitSubName;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param tranExchangeRate
	 */
	public void setTranExchangeRate(java.math.BigDecimal tranExchangeRate) {
		this.tranExchangeRate = tranExchangeRate;
	}
	
    /**
     * @return TranExchangeRate
     */	
	public java.math.BigDecimal getTranExchangeRate() {
		return this.tranExchangeRate;
	}
	
	/**
	 * @param tranDec
	 */
	public void setTranDec(String tranDec) {
		this.tranDec = tranDec == null ? null : tranDec.trim();
	}
	
    /**
     * @return TranDec
     */	
	public String getTranDec() {
		return this.tranDec;
	}
	
	/**
	 * @param tranTime
	 */
	public void setTranTime(String tranTime) {
		this.tranTime = tranTime == null ? null : tranTime.trim();
	}
	
    /**
     * @return TranTime
     */	
	public String getTranTime() {
		return this.tranTime;
	}
	
	/**
	 * @param tranAmt
	 */
	public void setTranAmt(java.math.BigDecimal tranAmt) {
		this.tranAmt = tranAmt;
	}
	
    /**
     * @return TranAmt
     */	
	public java.math.BigDecimal getTranAmt() {
		return this.tranAmt;
	}
	
	/**
	 * @param sysId
	 */
	public void setSysId(String sysId) {
		this.sysId = sysId == null ? null : sysId.trim();
	}
	
    /**
     * @return SysId
     */	
	public String getSysId() {
		return this.sysId;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}
	
    /**
     * @return Status
     */	
	public String getStatus() {
		return this.status;
	}


}