package cn.com.yusys.yusp.web.server.cmislmt0061;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0061.req.CmisLmt0061ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0061.resp.CmisLmt0061RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0061.CmisLmt0061Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据客户号获取客户综合授信批复编号，到期日，起始日，期限等
 *
 * @author lizx 20210909
 * @version 1.0
 */
@Api(tags = "cmislmt0061:根据客户号获取客户综合授信批复编号，到期日，起始日，期限等")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0061Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0061Resource.class);

    @Autowired
    private CmisLmt0061Service cmisLmt0061Service;

    /**
     * 交易码：CmisLmt0061
     * 交易描述：根据客户号获取客户综合授信批复编号，到期日，起始日，期限等
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("根据客户号获取客户综合授信批复编号，到期日，起始日，期限等")
    @PostMapping("/cmislmt0061")
    protected @ResponseBody
    ResultDto<CmisLmt0061RespDto> CmisLmt0061(@Validated @RequestBody CmisLmt0061ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0061.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0061.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0061RespDto> CmisLmt0061RespDtoResultDto = new ResultDto<>();
        CmisLmt0061RespDto CmisLmt0061RespDto = new CmisLmt0061RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0061RespDto = cmisLmt0061Service.execute(reqDto);
            CmisLmt0061RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0061RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0061.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0061.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0061.value, e.getMessage());
            // 封装CmisLmt0061RespDtoResultDto中异常返回码和返回信息
            CmisLmt0061RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0061RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0061RespDto到CmisLmt0061RespDtoResultDto中
        CmisLmt0061RespDtoResultDto.setData(CmisLmt0061RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0061.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0061.value, JSON.toJSONString(CmisLmt0061RespDtoResultDto));
        return CmisLmt0061RespDtoResultDto;
    }
}