package cn.com.yusys.yusp.web.server.cmislmt0066;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0066.req.CmisLmt0066ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0066.resp.CmisLmt0066RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0066.CmisLmt0066Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:获取客户指定品种或指定项目的授信余额
 *
 * @author zhangjw 20211021
 * @version 1.0
 */
@Api(tags = "cmislmt0066:获取合同占用分项编号-单一客户")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0066Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0066Resource.class);

    @Autowired
    private CmisLmt0066Service cmisLmt0066Service;

    @ApiOperation("获取合同占用分项编号-单一客户")
    @PostMapping("/cmislmt0066")
    protected @ResponseBody
    ResultDto<CmisLmt0066RespDto> CmisLmt0066(@Validated @RequestBody CmisLmt0066ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0066.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0066.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0066RespDto> CmisLmt0066RespDtoResultDto = new ResultDto<>();
        CmisLmt0066RespDto CmisLmt0066RespDto = new CmisLmt0066RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0066RespDto = cmisLmt0066Service.execute(reqDto);
            CmisLmt0066RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0066RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0066.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0066.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0066.value, e.getMessage());
            // 封装CmisLmt0066RespDtoResultDto中异常返回码和返回信息
            CmisLmt0066RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0066RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0066RespDto到CmisLmt0066RespDtoResultDto中
        CmisLmt0066RespDtoResultDto.setData(CmisLmt0066RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0066.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0066.value, JSON.toJSONString(CmisLmt0066RespDtoResultDto));
        return CmisLmt0066RespDtoResultDto;
    }
}