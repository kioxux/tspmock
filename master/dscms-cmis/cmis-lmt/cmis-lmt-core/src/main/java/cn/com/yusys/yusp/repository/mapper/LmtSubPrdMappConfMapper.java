/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtSubPrdMappConf;
import cn.com.yusys.yusp.dto.LmtSubPrdMappConfDto;
import cn.com.yusys.yusp.dto.LmtSubPrdMappConfListDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtSubPrdMappConfMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: yuanz
 * @创建时间: 2021-05-08 13:50:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtSubPrdMappConfMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtSubPrdMappConf selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtSubPrdMappConf> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtSubPrdMappConf record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtSubPrdMappConf record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtSubPrdMappConf record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtSubPrdMappConf record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 根据额度品种编号删除
     * @param limitSubNo
     * @return
     */
    int deleteByLimitSubNo(@Param("limitSubNo") String limitSubNo);

    /**
     * 获取产品映射
     * @param limitSubNo
     * @return
     */
    List<LmtSubPrdMappConf> selectByLimitSubNo(@Param("limitSubNo") String limitSubNo);

    /**
     * @方法名称: selectPrdIdByLimitSubNo
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtSubPrdMappConf> selectPrdIdByLimitSubNo(@Param("limitSubNo") String limitSubNo);

    /**
     * @方法名称: selectLimitSubNoByPrdId
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtSubPrdMappConfDto> selectLimitSubNoByPrdId(String prdId);

    /**
     * @方法名称: selectLimitSubNoListByPrdId
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<String> selectLimitSubNoListByPrdId(String prdId);

    /**
     * @方法名称: selectLimitSubNoListByLimitStrNoAndPrdId
     * @方法描述: 根据用信产品和类型，获取额度产品
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<String> selectLimitSubNoListByLimitStrNoAndPrdId(QueryModel model);

    /**
     * @方法名称: selectLimitSubNoListByLimitStrNoAndPrdId
     * @方法描述: 根据用信产品和类型，获取额度产品
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<String> selectLimitSubNoListByParam(QueryModel model);


    /**
     * @方法名称: selectSubNoAndNameByStrNoAndPrdId
     * @方法描述: 根据用信产品和类型，获取额度产品
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<Map<String, String >> selectSubNoAndNameByStrNoAndPrdId(QueryModel model);
}