/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.LmtExptListApp;
import cn.com.yusys.yusp.domain.LmtExptListInfo;
import cn.com.yusys.yusp.repository.mapper.LmtExptListAppMapper;
import cn.com.yusys.yusp.repository.mapper.LmtExptListInfoMapper;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtExptListInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: lenovo
 * @创建时间: 2021-04-16 16:27:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtExptListInfoService {

    @Autowired
    private LmtExptListInfoMapper lmtExptListInfoMapper;

    @Autowired
    private LmtExptListAppMapper lmtExptListAppMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtExptListInfo selectByPrimaryKey(String pkId) {
        return lmtExptListInfoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtExptListInfo> selectAll(QueryModel model) {
        List<LmtExptListInfo> records = (List<LmtExptListInfo>) lmtExptListInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtExptListInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtExptListInfo> list = lmtExptListInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtExptListInfo record) {
        return lmtExptListInfoMapper.insert(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtExptListInfo record) {
        return lmtExptListInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtExptListInfo record) {
        return lmtExptListInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtExptListInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtExptListInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: insertAppToInfo
     * @方法描述: App界面提交到Info界面
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertAppToInfo(LmtExptListApp lmtExptListAppAdd) {

        //创建实体类
        LmtExptListInfo lmtExptListInfo = new LmtExptListInfo();

        //获取当前时间
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //新增导入数据
        lmtExptListInfo.setPkId(lmtExptListAppAdd.getPkId());
        lmtExptListInfo.setSerno(lmtExptListAppAdd.getSerno());
        lmtExptListInfo.setInstuCde(lmtExptListAppAdd.getInstuCde());
        lmtExptListInfo.setBussNo(lmtExptListAppAdd.getBussNo());
        lmtExptListInfo.setSysId(lmtExptListAppAdd.getSysId());
        lmtExptListInfo.setCusId(lmtExptListAppAdd.getCusId());
        lmtExptListInfo.setCusName(lmtExptListAppAdd.getCusName());
        lmtExptListInfo.setExptType(lmtExptListAppAdd.getExptType());
        lmtExptListInfo.setExptResn(lmtExptListAppAdd.getExptResn());
        lmtExptListInfo.setUpdId(lmtExptListAppAdd.getUpdId());
        lmtExptListInfo.setUpdBrId(lmtExptListAppAdd.getUpdBrId());
        lmtExptListInfo.setUpdDate(sdf.format(date).toString());

        //将信息存入申请表
        lmtExptListAppAdd.setOprType(CmisLmtConstants.LMT_APP_TYPE_01);
        lmtExptListAppAdd.setInputDate(sdf.format(date).toString());
        lmtExptListAppMapper.insertSelective(lmtExptListAppAdd);

        //将信息存入信息表
        lmtExptListInfo.setInputDate(sdf.format(date).toString());

        return lmtExptListInfoMapper.insertAppToInfo(lmtExptListInfo);
    }

    /**
     * @方法名称: logicaldelete
     * @方法描述: 根据流水号逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int logicaldelete(LmtExptListInfo record){

        LmtExptListInfo lmtExptListInfo = new LmtExptListInfo();

        //获取当前时间
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //变更为删除状态
        lmtExptListInfo.setSerno(record.getSerno());
        lmtExptListInfo.setOprType(CmisLmtConstants.LMT_APP_TYPE_02);
        lmtExptListInfo.setUpdId(record.getUpdId());
        lmtExptListInfo.setUpdBrId(record.getUpdBrId());
        lmtExptListInfo.setUpdDate(sdf.format(date).toString());

        return lmtExptListInfoMapper.logicaldelete(lmtExptListInfo);
    };

    /**
     * @方法名称: selectLmtExptListInfoByBussNo
     * @方法描述: 根据交易业务编号，查询客户是否存在额度调整白名单信息
     * @参数与返回说明: 存在返回白名单信息，不存在返回null
     * @算法描述: 无
     */
    public LmtExptListInfo selectLmtExptListInfoByBussNo(String bussNo){
        QueryModel queryModel = new QueryModel() ;
        //业务交易
        queryModel.addCondition("bussNo", bussNo);
        //操作类型 01 新增 02 删除
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        //获取业务交易流水号
        List<LmtExptListInfo> list = lmtExptListInfoMapper.selectByModel(queryModel);
        //判断列表是否为空，不为空返回白名单信息
        if(CollectionUtils.isNotEmpty(list)){
            return list.get(0) ;
        }
        return null;
    }
}
