/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: TradeOppoRiskExpose
 * @类描述: trade_oppo_risk_expose数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-25 20:56:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "trade_oppo_risk_expose")
public class TradeOppoRiskExpose extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 客户编号 **/
	@Id
	@Column(name = "CUS_ID")
	private String cusId;
	
	/** 产品名称 **/
	@Id
	@Column(name = "PRD_NAME")
	private String prdName;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = false, length = 80)
	private String cusName;
	
	/** 本金金额 **/
	@Column(name = "HOLD_POSITION", unique = false, nullable = false, length = 24)
	private java.math.BigDecimal holdPosition;
	
	/** 不考虑缓释的风险暴露 **/
	@Column(name = "RISK_EXPOSE_NOSLOW_RELEASE", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal riskExposeNoslowRelease;
	
	/** 不可豁免的风险暴露 **/
	@Column(name = "RISK_EXPOSE_NOEXAMPT", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal riskExposeNoexampt;
	
	/** 可豁免的风险暴露 **/
	@Column(name = "RISK_EXPOSE_EXAMPT", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal riskExposeExampt;
	
	/** 风险缓释金额 **/
	@Column(name = "RISK_EXPOSE_AMT", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal riskExposeAmt;
	
	/** 风险缓释客户号 **/
	@Column(name = "GUAR_CUS_ID", unique = false, nullable = true, length = 40)
	private String guarCusId;
	
	/** 风险缓释客户名称 **/
	@Column(name = "GUAR_CUS_NAME", unique = false, nullable = true, length = 80)
	private String guarCusName;
	
	/** 业务日期 **/
	@Column(name = "BUSS_DATE", unique = false, nullable = true, length = 10)
	private String bussDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param holdPosition
	 */
	public void setHoldPosition(java.math.BigDecimal holdPosition) {
		this.holdPosition = holdPosition;
	}
	
    /**
     * @return holdPosition
     */
	public java.math.BigDecimal getHoldPosition() {
		return this.holdPosition;
	}
	
	/**
	 * @param riskExposeNoslowRelease
	 */
	public void setRiskExposeNoslowRelease(java.math.BigDecimal riskExposeNoslowRelease) {
		this.riskExposeNoslowRelease = riskExposeNoslowRelease;
	}
	
    /**
     * @return riskExposeNoslowRelease
     */
	public java.math.BigDecimal getRiskExposeNoslowRelease() {
		return this.riskExposeNoslowRelease;
	}
	
	/**
	 * @param riskExposeNoexampt
	 */
	public void setRiskExposeNoexampt(java.math.BigDecimal riskExposeNoexampt) {
		this.riskExposeNoexampt = riskExposeNoexampt;
	}
	
    /**
     * @return riskExposeNoexampt
     */
	public java.math.BigDecimal getRiskExposeNoexampt() {
		return this.riskExposeNoexampt;
	}
	
	/**
	 * @param riskExposeExampt
	 */
	public void setRiskExposeExampt(java.math.BigDecimal riskExposeExampt) {
		this.riskExposeExampt = riskExposeExampt;
	}
	
    /**
     * @return riskExposeExampt
     */
	public java.math.BigDecimal getRiskExposeExampt() {
		return this.riskExposeExampt;
	}
	
	/**
	 * @param riskExposeAmt
	 */
	public void setRiskExposeAmt(java.math.BigDecimal riskExposeAmt) {
		this.riskExposeAmt = riskExposeAmt;
	}
	
    /**
     * @return riskExposeAmt
     */
	public java.math.BigDecimal getRiskExposeAmt() {
		return this.riskExposeAmt;
	}
	
	/**
	 * @param guarCusId
	 */
	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId;
	}
	
    /**
     * @return guarCusId
     */
	public String getGuarCusId() {
		return this.guarCusId;
	}
	
	/**
	 * @param guarCusName
	 */
	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName;
	}
	
    /**
     * @return guarCusName
     */
	public String getGuarCusName() {
		return this.guarCusName;
	}
	
	/**
	 * @param bussDate
	 */
	public void setBussDate(String bussDate) {
		this.bussDate = bussDate;
	}
	
    /**
     * @return bussDate
     */
	public String getBussDate() {
		return this.bussDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}