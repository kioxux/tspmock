package cn.com.yusys.yusp.web.server.cmislmt0009;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.resp.CmisLmt0009RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0009.CmisLmt0009Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:合同下出账申请校验
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "cmislmt0009:合同校验")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0009Resource.class);

    @Autowired
    private CmisLmt0009Service cmisLmt0009Service;
    /**
     * 交易码：cmislmt0009
     * 交易描述：合同下出账申请校验
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("合同下出账申请校验")
    @PostMapping("/cmislmt0009")
    protected @ResponseBody
    ResultDto<CmisLmt0009RespDto> cmisLmt0009(@Validated @RequestBody CmisLmt0009ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0009.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0009RespDto> cmisLmt0009RespDtoResultDto = new ResultDto<>();
        CmisLmt0009RespDto cmisLmt0009RespDto = new CmisLmt0009RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0009RespDto = cmisLmt0009Service.execute(reqDto);
            //设置成功报文
            cmisLmt0009RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0009RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0009.value, e.getMessage());
            // 封装xddb0009DataResultDto中异常返回码和返回信息
            cmisLmt0009RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0009RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }

        // 封装cmisLmt0009RespDto到cmisLmt0009RespDtoResultDto中
        cmisLmt0009RespDtoResultDto.setData(cmisLmt0009RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0009.value, JSON.toJSONString(cmisLmt0009RespDtoResultDto));
        return cmisLmt0009RespDtoResultDto;
    }
}
