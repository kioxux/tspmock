package cn.com.yusys.yusp.web.server.cmislmt0065;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0065.req.CmisLmt0065ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0065.resp.CmisLmt0065RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0065.CmisLmt0065Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:获取客户指定品种或指定项目的授信余额
 *
 * @author zhangjw 20211021
 * @version 1.0
 */
@Api(tags = "cmislmt0065:获取客户指定品种或指定项目的授信余额")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0065Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0065Resource.class);

    @Autowired
    private CmisLmt0065Service cmisLmt0065Service;

    @ApiOperation("获取客户指定品种或指定项目的授信余额")
    @PostMapping("/cmislmt0065")
    protected @ResponseBody
    ResultDto<CmisLmt0065RespDto> CmisLmt0065(@Validated @RequestBody CmisLmt0065ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0065.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0065.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0065RespDto> CmisLmt0065RespDtoResultDto = new ResultDto<>();
        CmisLmt0065RespDto CmisLmt0065RespDto = new CmisLmt0065RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0065RespDto = cmisLmt0065Service.execute(reqDto);
            CmisLmt0065RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0065RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0065.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0065.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0065.value, e.getMessage());
            // 封装CmisLmt0065RespDtoResultDto中异常返回码和返回信息
            CmisLmt0065RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0065RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0065RespDto到CmisLmt0065RespDtoResultDto中
        CmisLmt0065RespDtoResultDto.setData(CmisLmt0065RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0065.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0065.value, JSON.toJSONString(CmisLmt0065RespDtoResultDto));
        return CmisLmt0065RespDtoResultDto;
    }
}