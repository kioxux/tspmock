package cn.com.yusys.yusp.web.server.cmislmt0019;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0019.req.CmisLmt0019ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0019.resp.CmisLmt0019RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0019.CmisLmt0019Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:综合授信额度校验
 *
 * @author jiangyl 20210506
 * @version 1.0
 */
@Api(tags = "cmislmt0019:综合授信额度校验(查询客户是否有有效的贷款或者非标业务)")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0019Resource.class);

    @Autowired
    private CmisLmt0019Service cmisLmt0019Service;
    /**
     * 交易码：cmislmt0019
     * 交易描述：综合授信额度校验
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("综合授信额度校验(查询客户是否有有效的贷款或者非标业务)")
    @PostMapping("/cmislmt0019")
    protected @ResponseBody
    ResultDto<CmisLmt0019RespDto> cmisLmt0019(@Validated @RequestBody CmisLmt0019ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0019.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0019.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0019RespDto> cmisLmt0019RespDtoResultDto = new ResultDto<>();
        CmisLmt0019RespDto cmisLmt0019RespDto = new CmisLmt0019RespDto();
        // 调用对应的service层
        try {
            cmisLmt0019RespDto = cmisLmt0019Service.checkLmtMode(reqDto);
            cmisLmt0019RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0019RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0019.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0019.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0019.value, e.getMessage());
            // 封装xddb0001DataResultDto中异常返回码和返回信息
            cmisLmt0019RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0019RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        cmisLmt0019RespDtoResultDto.setData(cmisLmt0019RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0019.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0019.value, JSON.toJSONString(cmisLmt0019RespDtoResultDto));
        return cmisLmt0019RespDtoResultDto;
    }
}
