package cn.com.yusys.yusp.service.server.cmislmt0037;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0037.req.CmisLmt0037ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0037.resp.CmisLmt0037RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.ApprStrMtableInfoService;
import cn.com.yusys.yusp.service.LmtContRelService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0037Service
 * @类描述: #对内服务类
 * @功能描述: 判断批复向下是否发生占用，
 * 1.根据接口传来来的类型，如果是1：批复编号，则根据批复编号查询该批复向下是否存在有效的占用关系信息
 * 2.如果是2：分项编号，则只查询该分项下是否存在有效的占用关系信息
 * @创建时间: 2021-07-01
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CmisLmt0037Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0037Service.class);

    @Autowired
    private ApprStrMtableInfoService apprStrMtableInfoService ;

    @Autowired
    private LmtContRelService lmtContRelService ;

    /**
     * 根据分项信息，查询起始日到期日期限
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0037RespDto execute(CmisLmt0037ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0037.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0037.value);
        CmisLmt0037RespDto respDto = new CmisLmt0037RespDto();
        String result = CmisLmtConstants.YES_NO_N ;
        //查询类型
        String queryType = reqDto.getQueryType();
        //分项编号
        String accSubNo = reqDto.getAccSubNo();
        //批复编号
        String accNo = reqDto.getAccNo();
        try {
            //..1.如果查询类型为批复编号，查询该批复编号向下是否存在操作类型为01 状态的占用信息
            if(CmisLmtConstants.CMISLMT0037_QUERY_TYPE_1.equals(queryType)){
                if(StringUtils.isEmpty(accNo)){
                    throw new YuspException(EclEnum.ECL070084.key, EclEnum.ECL070084.value);
                }
                List<String> apprSubSernoList = apprStrMtableInfoService.selectApprSubSernoListByApprSerno(accNo) ;
                //根据分项编号查询分项占用关系信息
                int count = lmtContRelService.selectLmtContRelByApprSubSernos(apprSubSernoList) ;
                if(count>0) {
                    result = CmisLmtConstants.YES_NO_Y ;
                }
            }else{
                //..2.如果查询类型为分项编号，查询该分项编号向下是否存在操作类型为01 状态的占用信息
                if(StringUtils.isEmpty(accSubNo)){
                    throw new YuspException(EclEnum.ECL070085.key, EclEnum.ECL070085.value);
                }
                List<String> apprSubSernoList = new ArrayList<>();
                apprSubSernoList.add(accSubNo) ;
                int count = lmtContRelService.selectLmtContRelByApprSubSernos(apprSubSernoList) ;
                if(count>0) {
                    result = CmisLmtConstants.YES_NO_Y ;
                }
            }
            logger.error(this.getClass().getName()+":接口查询【{}】,是否存在有效占用关系【opr_type=01】的数据【{}】",DscmsLmtEnum.TRADE_CODE_CMISLMT0037.value,result);
            //返回结果
            respDto.setIsExists(result);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
        } catch (YuspException e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0037.value, e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0037.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0037.value);
        return respDto;
    }
}