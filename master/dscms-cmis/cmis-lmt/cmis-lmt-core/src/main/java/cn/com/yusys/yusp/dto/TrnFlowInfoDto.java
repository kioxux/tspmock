package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: TrnFlowInfo
 * @类描述: trn_flow_info数据实体类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-05-04 16:27:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class TrnFlowInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 交易流水号 **/
	private String tranSerno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 交易业务编号 **/
	private String tranBizNo;
	
	/** 批复分项流水号 **/
	private String apprSubSerno;
	
	/** 交易类型STD_ZB_TRAN_TYPE **/
	private String tranType;
	
	/** 修改数据表 **/
	private String chgTableName;
	
	/** 更改字段名 **/
	private String chgFieldName;
	
	/** 更改字段值 **/
	private String chgFieldValue;
	
	/** 操作类型STD_ZB_OPR_TYPE **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param tranSerno
	 */
	public void setTranSerno(String tranSerno) {
		this.tranSerno = tranSerno == null ? null : tranSerno.trim();
	}
	
    /**
     * @return TranSerno
     */	
	public String getTranSerno() {
		return this.tranSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param tranBizNo
	 */
	public void setTranBizNo(String tranBizNo) {
		this.tranBizNo = tranBizNo == null ? null : tranBizNo.trim();
	}
	
    /**
     * @return TranBizNo
     */	
	public String getTranBizNo() {
		return this.tranBizNo;
	}
	
	/**
	 * @param apprSubSerno
	 */
	public void setApprSubSerno(String apprSubSerno) {
		this.apprSubSerno = apprSubSerno == null ? null : apprSubSerno.trim();
	}
	
    /**
     * @return ApprSubSerno
     */	
	public String getApprSubSerno() {
		return this.apprSubSerno;
	}
	
	/**
	 * @param tranType
	 */
	public void setTranType(String tranType) {
		this.tranType = tranType == null ? null : tranType.trim();
	}
	
    /**
     * @return TranType
     */	
	public String getTranType() {
		return this.tranType;
	}
	
	/**
	 * @param chgTableName
	 */
	public void setChgTableName(String chgTableName) {
		this.chgTableName = chgTableName == null ? null : chgTableName.trim();
	}
	
    /**
     * @return ChgTableName
     */	
	public String getChgTableName() {
		return this.chgTableName;
	}
	
	/**
	 * @param chgFieldName
	 */
	public void setChgFieldName(String chgFieldName) {
		this.chgFieldName = chgFieldName == null ? null : chgFieldName.trim();
	}
	
    /**
     * @return ChgFieldName
     */	
	public String getChgFieldName() {
		return this.chgFieldName;
	}
	
	/**
	 * @param chgFieldValue
	 */
	public void setChgFieldValue(String chgFieldValue) {
		this.chgFieldValue = chgFieldValue == null ? null : chgFieldValue.trim();
	}
	
    /**
     * @return ChgFieldValue
     */	
	public String getChgFieldValue() {
		return this.chgFieldValue;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}


}