package cn.com.yusys.yusp.web.server.cmislmt0058;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0058.req.CmisLmt0058ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0058.resp.CmisLmt0058RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0058.CmisLmt0058Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:线上产品自动生成合同台账
 *
 * @author lizx 20210909
 * @version 1.0
 */
@Api(tags = "cmislmt0058:转贴现业务额度恢复")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0058Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0058Resource.class);

    @Autowired
    private CmisLmt0058Service cmisLmt0058Service;

    /**
     * 交易码：CmisLmt0058
     * 交易描述：转贴现业务额度恢复
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("转贴现业务额度恢复")
    @PostMapping("/cmislmt0058")
    protected @ResponseBody
    ResultDto<CmisLmt0058RespDto> CmisLmt0058(@Validated @RequestBody CmisLmt0058ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0058.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0058.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0058RespDto> CmisLmt0058RespDtoResultDto = new ResultDto<>();
        CmisLmt0058RespDto CmisLmt0058RespDto = new CmisLmt0058RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0058RespDto = cmisLmt0058Service.execute(reqDto);
            CmisLmt0058RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0058RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0058.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0058.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0058.value, e.getMessage());
            // 封装CmisLmt0058RespDtoResultDto中异常返回码和返回信息
            CmisLmt0058RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0058RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0058RespDto到CmisLmt0058RespDtoResultDto中
        CmisLmt0058RespDtoResultDto.setData(CmisLmt0058RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0058.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0058.value, JSON.toJSONString(CmisLmt0058RespDtoResultDto));
        return CmisLmt0058RespDtoResultDto;
    }
}