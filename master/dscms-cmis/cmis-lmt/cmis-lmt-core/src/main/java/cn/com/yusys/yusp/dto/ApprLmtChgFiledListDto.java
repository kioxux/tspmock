package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: ApprLmtChgFiledListDto
 * @类描述: #对内服务类
 * @功能描述: TODO
 * @创建时间: 2021/7/17 15:45
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class ApprLmtChgFiledListDto  implements Serializable {

    private static final long serialVersionUID = 1L;
    /** 字段名1 **/
    private String filed;

    /** 修改前字段值1 **/
    private BigDecimal updbfFiled;

    /** 修改后字段值1 **/
    private BigDecimal updafFiled;

    public String getFiled() {
        return filed;
    }

    public void setFiled(String filed) {
        this.filed = filed;
    }

    public BigDecimal getUpdbfFiled() {
        return updbfFiled;
    }

    public void setUpdbfFiled(BigDecimal updbfFiled) {
        this.updbfFiled = updbfFiled;
    }

    public BigDecimal getUpdafFiled() {
        return updafFiled;
    }

    public void setUpdafFiled(BigDecimal updafFiled) {
        this.updafFiled = updafFiled;
    }

    @Override
    public String toString() {
        return "ApprLmtChgFiledListDto{" +
                "filed='" + filed + '\'' +
                ", updbfFiled=" + updbfFiled +
                ", updafFiled=" + updafFiled +
                '}';
    }
}
