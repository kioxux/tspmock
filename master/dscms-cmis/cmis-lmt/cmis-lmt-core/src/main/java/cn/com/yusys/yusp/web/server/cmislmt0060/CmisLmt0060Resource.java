package cn.com.yusys.yusp.web.server.cmislmt0060;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0060.req.CmisLmt0060ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0060.resp.CmisLmt0060RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0060.CmisLmt0060Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:获取客户或分项明细用信综合总额和用信敞口余额（不包含合作方）
 *
 * @author lizx 20210909
 * @version 1.0
 */
@Api(tags = "cmislmt0060:获取合同总已用及敞口已用金额")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0060Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0060Resource.class);

    @Autowired
    private CmisLmt0060Service cmisLmt0060Service;

    /**
     * 交易码：CmisLmt0060
     * 交易描述：转贴现业务额度恢复
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("获取合同总已用及敞口已用金额")
    @PostMapping("/cmislmt0060")
    protected @ResponseBody
    ResultDto<CmisLmt0060RespDto> CmisLmt0060(@Validated @RequestBody CmisLmt0060ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0060.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0060.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0060RespDto> CmisLmt0060RespDtoResultDto = new ResultDto<>();
        CmisLmt0060RespDto CmisLmt0060RespDto = new CmisLmt0060RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0060RespDto = cmisLmt0060Service.execute(reqDto);
            CmisLmt0060RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0060RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0060.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0060.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0060.value, e.getMessage());
            // 封装CmisLmt0060RespDtoResultDto中异常返回码和返回信息
            CmisLmt0060RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0060RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0060RespDto到CmisLmt0060RespDtoResultDto中
        CmisLmt0060RespDtoResultDto.setData(CmisLmt0060RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0060.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0060.value, JSON.toJSONString(CmisLmt0060RespDtoResultDto));
        return CmisLmt0060RespDtoResultDto;
    }
}