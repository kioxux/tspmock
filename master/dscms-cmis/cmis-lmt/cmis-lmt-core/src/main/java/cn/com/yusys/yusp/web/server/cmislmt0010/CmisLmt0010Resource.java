package cn.com.yusys.yusp.web.server.cmislmt0010;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.resp.CmisLmt0010RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0010.CmisLmt0010Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:合同下出账申请校验
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "cmislmt0010:合同下出账申请校验")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0010Resource.class);

    @Autowired
    private CmisLmt0010Service cmisLmt0010Service;
    /**
     * 交易码：cmislmt0010
     * 交易描述：合同下出账申请校验
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("合同下出账申请校验")
    @PostMapping("/cmislmt0010")
    protected @ResponseBody
    ResultDto<CmisLmt0010RespDto> cmisLmt0010(@Validated @RequestBody CmisLmt0010ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0010RespDto> cmisLmt0010RespDtoResultDto = new ResultDto<>();
        CmisLmt0010RespDto cmisLmt0010RespDto = new CmisLmt0010RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0010RespDto = cmisLmt0010Service.execute(reqDto);
            //设置成功报文
            cmisLmt0010RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0010RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0008.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value, e.getMessage());
            // 封装xddb0010DataResultDto中异常返回码和返回信息
            cmisLmt0010RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0010RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }

        // 封装cmisLmt0010RespDto到cmisLmt0010RespDtoResultDto中
        cmisLmt0010RespDtoResultDto.setData(cmisLmt0010RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0010.value, JSON.toJSONString(cmisLmt0010RespDtoResultDto));
        return cmisLmt0010RespDtoResultDto;
    }
}
