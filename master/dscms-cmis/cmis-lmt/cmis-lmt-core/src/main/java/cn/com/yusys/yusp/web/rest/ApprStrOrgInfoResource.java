/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ApprStrOrgInfo;
import cn.com.yusys.yusp.service.ApprStrOrgInfoService;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprStrOrgInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 26612
 * @创建时间: 2021-04-27 20:20:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/apprstrorginfo")
public class ApprStrOrgInfoResource {
    @Autowired
    private ApprStrOrgInfoService apprStrOrgInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<ApprStrOrgInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<ApprStrOrgInfo> list = apprStrOrgInfoService.selectAll(queryModel);
        return new ResultDto<List<ApprStrOrgInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<ApprStrOrgInfo>> index(QueryModel queryModel) {
        List<ApprStrOrgInfo> list = apprStrOrgInfoService.selectByModel(queryModel);
        return new ResultDto<List<ApprStrOrgInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<ApprStrOrgInfo> show(@PathVariable("pkId") String pkId) {
        ApprStrOrgInfo apprStrOrgInfo = apprStrOrgInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<ApprStrOrgInfo>(apprStrOrgInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<ApprStrOrgInfo> create(@RequestBody ApprStrOrgInfo apprStrOrgInfo) {
        apprStrOrgInfoService.insert(apprStrOrgInfo);
        return new ResultDto<ApprStrOrgInfo>(apprStrOrgInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ApprStrOrgInfo apprStrOrgInfo) {
        int result = apprStrOrgInfoService.update(apprStrOrgInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = apprStrOrgInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = apprStrOrgInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
