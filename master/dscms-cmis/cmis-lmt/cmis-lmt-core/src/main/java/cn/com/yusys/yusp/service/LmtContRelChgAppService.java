/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.ContAccRelChgApp;
import cn.com.yusys.yusp.domain.LmtContRel;
import cn.com.yusys.yusp.domain.LmtContRelChgApp;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.repository.mapper.LmtContRelChgAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtContRelChgAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-20 20:38:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtContRelChgAppService {

    @Autowired
    private LmtContRelChgAppMapper lmtContRelChgAppMapper;
    @Autowired
    private LmtContRelService lmtContRelService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private LmtContRelChgAppService lmtContRelChgAppService;
    @Autowired
    private ContAccRelChgAppService contAccRelChgAppService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtContRelChgApp selectByPrimaryKey(String pkId) {
        return lmtContRelChgAppMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtContRelChgApp> selectAll(QueryModel model) {
        List<LmtContRelChgApp> records = (List<LmtContRelChgApp>) lmtContRelChgAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByAppSerno
     * @方法描述: 根据调整主申请流水号获取
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtContRelChgApp> selectByAppSerno(String appSerno) {
        List<LmtContRelChgApp> records = (List<LmtContRelChgApp>) lmtContRelChgAppMapper.selectByAppSerno(appSerno);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtContRelChgApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtContRelChgApp> list = lmtContRelChgAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(LmtContRelChgApp record) {
        return lmtContRelChgAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtContRelChgApp record) {
        return lmtContRelChgAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtContRelChgApp record) {
        return lmtContRelChgAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtContRelChgApp record) {
        return lmtContRelChgAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtContRelChgAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtContRelChgAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: createLmtcontRelChgApp
     * @方法描述: 根据分项编号，生成分项占用关系信息
     * @参数与返回说明:limitSubNo 分项编号    返回：组装后的分项编号占用关系申请
     * @算法描述: 无
     */

    public List<LmtContRelChgApp> createLmtcontRelChgApp(String limitSubNo) {
        //定义返回类型
        List<LmtContRelChgApp> lmtContRelChgAppList = new ArrayList<>();
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        //获取分项编号下的占用关系
        List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelByLimitSubNo(limitSubNo);
        //如果大于0，进行申请数据组装处理
        if (CollectionUtils.isNotEmpty(lmtContRelList)) {
            for (LmtContRel lmtContRel : lmtContRelList) {
                LmtContRelChgApp lmtContRelChgApp = new LmtContRelChgApp();
                //生成主键，获取流水号
                String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, new HashMap<>());
                BeanUtils.copyProperties(lmtContRel, lmtContRelChgApp);
                //主键
                lmtContRelChgApp.setPkId(pkValue);
                //关联主键
                lmtContRelChgApp.setRelId(lmtContRel.getPkId());
                //更新最近更新人
                lmtContRelChgApp.setInputId(userInfo.getLoginCode());
                //申请机构
                lmtContRelChgApp.setInputBrId(userInfo.getOrg().getCode());
                //申请时间
                lmtContRelChgApp.setInputDate(DateUtils.getCurrDateStr());
                // 创建时间
                lmtContRelChgApp.setCreateTime(new Date());
                //更新人
                lmtContRelChgApp.setUpdId(userInfo.getLoginCode());
                //更新机构
                lmtContRelChgApp.setUpdBrId(userInfo.getOrg().getCode());
                //更新日期
                lmtContRelChgApp.setUpdDate(DateUtils.getCurrDateStr());
                //修改时间
                lmtContRelChgApp.setUpdateTime(new Date());
                //操作状态
                lmtContRelChgApp.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
                lmtContRelChgAppList.add(lmtContRelChgApp);
            }
        }
        return lmtContRelChgAppList;
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int logicDelete(String pkId) {
        //逻辑删除操作参数
        Map delMap = new HashMap();
        delMap.put("pkId", pkId);
        //获取业务标识位，通过标识位进行逻辑删除操作
        delMap.put("oprType", CmisLmtConstants.OPR_TYPE_DELETE);
        return lmtContRelChgAppMapper.updateByParams(delMap);
    }

    /**
     * @方法名称: handleBusinessDataAfterEnd
     * @方法描述: 额度调整申请审批通过后处理
     * @参数与返回说明:limitSubNo 主申请流水号
     * @算法描述: 无
     */

    public void handleBusinessDataAfterEnd(String appSerno) {
        //判断传入的主申请流水号是否为空
        if (StringUtils.isBlank(appSerno)) {
            throw BizException.error(null, EclEnum.ECL070041.key, EclEnum.ECL070041.value);
        }
        //获取调整申请流水号下的占用关系
        List<LmtContRelChgApp> lmtContRelChgAppList = this.selectByAppSerno(appSerno);
        //如果大于0，进行申请数据组装处理
        if (lmtContRelChgAppList.size() > 0) {
            for (LmtContRelChgApp lmtContRelChgApp : lmtContRelChgAppList) {
                //获取关联LmtContRel表的主键
                String relId = lmtContRelChgApp.getRelId();

                if (StringUtils.isBlank(relId)) {
                    LmtContRel lmtContRel = new LmtContRel();
                    BeanUtils.copyProperties(lmtContRelChgApp, lmtContRel);
                    lmtContRelService.insert(lmtContRel);
                } else {
                    //根据关联主键获取分项占用关系表的对应数据
                    LmtContRel lmtContRel = lmtContRelService.selectByPrimaryKey(relId);
                    if (lmtContRel == null || "".equals(lmtContRel.getPkId())) {
                        throw new YuspException(EclEnum.ECL070045.key, EclEnum.ECL070045.value);
                    }
                    /** 交易业务编号 **/
                    lmtContRel.setDealBizNo(lmtContRelChgApp.getDealBizNo());

                    /** 资产编号 **/
                    lmtContRel.setAssetNo(lmtContRelChgApp.getAssetNo());

                    /** 交易业务类型 **/
                    lmtContRel.setDealBizType(lmtContRelChgApp.getDealBizType());

                    /** 业务属性 **/
                    lmtContRel.setBizAttr(lmtContRelChgApp.getBizAttr());

                    /** 产品编号 **/
                    lmtContRel.setPrdId(lmtContRelChgApp.getPrdId());

                    /** 产品名称 **/
                    lmtContRel.setPrdName(lmtContRelChgApp.getPrdName());

                    /** 占用总金额（折人民币) **/
                    lmtContRel.setBizTotalAmtCny(lmtContRelChgApp.getBizTotalAmtCny());

                    /** 占用敞口金额（折人民币) **/
                    lmtContRel.setBizSpacAmtCny(lmtContRelChgApp.getBizSpacAmtCny());

                    /** 占用总余额（折人民币) **/
                    lmtContRel.setBizTotalBalanceAmtCny(lmtContRelChgApp.getBizTotalBalanceAmtCny());

                    /** 占用敞口余额（折人民币) **/
                    lmtContRel.setBizSpacBalanceAmtCny(lmtContRelChgApp.getBizSpacBalanceAmtCny());

                    /** 业务保证金比例 **/
                    lmtContRel.setSecurityRate(lmtContRelChgApp.getSecurityRate());

                    /** 业务保证金金额 **/
                    lmtContRel.setSecurityAmt(lmtContRelChgApp.getSecurityAmt());

                    /** 投资资产名称字段 **/
                    lmtContRel.setInvestAssetName(lmtContRelChgApp.getInvestAssetName());

                    /** 起始日期 **/
                    lmtContRel.setStartDate(lmtContRelChgApp.getStartDate());

                    /** 到期日期 **/
                    lmtContRel.setEndDate(lmtContRelChgApp.getEndDate());

                    /** 交易业务状态 **/
                    lmtContRel.setBizStatus(lmtContRelChgApp.getBizStatus());

                    /** 最近更新人 **/
                    lmtContRel.setUpdId(lmtContRelChgApp.getUpdId());

                    /** 最近更新机构 **/
                    lmtContRel.setUpdBrId(lmtContRelChgApp.getUpdBrId());

                    /** 最近更新日期 **/
                    lmtContRel.setUpdDate(lmtContRelChgApp.getUpdDate());

                    /** 修改时间 **/
                    lmtContRel.setUpdateTime(lmtContRelChgApp.getUpdateTime());

                    lmtContRelService.update(lmtContRel);
                }
            }
        }
    }

    /**
     * @方法名称: CreateLmtcontRelChgApp
     * @方法描述: 额度调整申请分项占用关系初始化
     * @参数与返回说明:limitSubNo 主申请流水号
     * @算法描述: 无
     */
    public void createLmtcontRelChgApp(String limitSubNo, String appSerno) {
        //判断传入的主申请流水号是否为空
        if (StringUtils.isBlank(limitSubNo)) {
            throw new YuspException(EclEnum.ECL070041.key, EclEnum.ECL070041.value);
        }
        //根据分项编号处理分项占用信息
        List<LmtContRelChgApp> lmtContRelChgAppList = lmtContRelChgAppService.createLmtcontRelChgApp(limitSubNo);
        //如果分项占用信息存在记录
        if (CollectionUtils.isNotEmpty(lmtContRelChgAppList)) {
            for (LmtContRelChgApp lmtContRelChgApp : lmtContRelChgAppList) {
                lmtContRelChgApp.setAppSerno(appSerno);
                lmtContRelChgAppService.insert(lmtContRelChgApp);
                //获取交易业务编号
                String dealBizNo = lmtContRelChgApp.getDealBizNo();
                if (!StringUtils.isEmpty(dealBizNo)) {
                    //初始化合同占用关系调整申请表
                    List<ContAccRelChgApp> contAccRelChgAppList = contAccRelChgAppService.createContAccRelChgApp(dealBizNo);
                    //如果分项占用信息存在记录
                    if (contAccRelChgAppList.size() > 0) {
                        for (ContAccRelChgApp contAccRelChgApp : contAccRelChgAppList) {
                            contAccRelChgApp.setAppSerno(appSerno);
                            contAccRelChgAppService.insert(contAccRelChgApp);
                        }
                    }
                }
            }
        }
    }

    /**
     * 检查分项占用关系表里的交易业务编号是否存在
     * @param dealBizNo
     * @return
     */
    public boolean checkDealBizNoIsExist(String dealBizNo) {
        int count = lmtContRelChgAppMapper.selectRecordsByDealBizNo(dealBizNo);
        return count!=0;
    }
}
