package cn.com.yusys.yusp.util;

import java.math.BigDecimal;

/**
 * @version 1.0.0
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class BigDecimalUtil {
    private static int DIVSCALE = 4;

    BigDecimalUtil(){

    }

    public static BigDecimal replaceNull(Object obj){
        if(obj == null || "".equals(obj) || "null".equals(obj)){
            return new BigDecimal(0);
        }else{
            return new BigDecimal(obj.toString());
        }
    }

    public static BigDecimal div(BigDecimal a,BigDecimal b){
        if(b.compareTo(new BigDecimal(0)) == 0){
            return new BigDecimal(0);
        }else{
            return  a.divide(b,BigDecimalUtil.DIVSCALE,BigDecimal.ROUND_HALF_UP);
        }
    }

    public static BigDecimal add(BigDecimal a,BigDecimal b){
        return a.add(b);
    }


    public static BigDecimal add(BigDecimal... list){
        BigDecimal ans = new BigDecimal(0);
        for(int i=0;i<list.length;i++){
            ans = ans.add(list[i]);
        }
        return ans;
    }
}
