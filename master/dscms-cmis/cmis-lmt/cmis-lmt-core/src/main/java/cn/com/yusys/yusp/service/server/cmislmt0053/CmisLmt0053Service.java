package cn.com.yusys.yusp.service.server.cmislmt0053;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0053.req.CmisLmt0053ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0053.req.CusListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0053.resp.CmisLmt0053RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0053Service
 * @类描述: #对内服务类
 * @功能描述: 查询多客户量敞口金额和敞口余额
 * @创建时间: 2021-09-05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0053Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0053Service.class);

    @Autowired
    private Comm4Service comm4Service ;

    /**
     * 查询多客户量敞口金额和敞口余额
     * add by zhangjw 2021-09-05
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0053RespDto execute(CmisLmt0053ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0053.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0053.value);
        CmisLmt0053RespDto respDto = new CmisLmt0053RespDto();
        //客户号
        List<CusListDto> cusList = reqDto.getCusListDtoList();

        try {

            if(cusList==null || cusList.size()<=0){
                respDto.setSpacLmtAmt(BigDecimal.ZERO);
                respDto.setSpacBalanceAmt(BigDecimal.ZERO);
                respDto.setErrorCode(SuccessEnum.SUCCESS.key);
                respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
                logger.info("{}:查询查询多客户量敞口金额和敞口余额，客户号列表为空，直接返回成功，金额为0，respDto：{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0053.key, respDto);
                return respDto;
            }

            //获取list 中的客户号，转化未字符串， ”，“ 号分割
            String cusIds = cusList.stream().map(cusListDto->cusListDto.getCusId()).collect(Collectors.joining(","));

            logger.info("{}:查询查询多客户量敞口金额和敞口余额，客户号列表为 cusIds：{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0053.key, cusIds);

            Map<String,Object> result = new HashMap();
            result = this.calLmtAntLoanSpacBalanceAmt(cusIds,reqDto.getInstuCde());
            logger.info("{}:查询查询多客户量敞口金额和敞口余额，result：{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0053.key, result);

            respDto.setSpacLmtAmt((BigDecimal) result.get("lmtSpacAmt"));
            respDto.setSpacBalanceAmt((BigDecimal)result.get("loanSpacBalance"));
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("【cmislmt0053】查询查询多客户量敞口金额和敞口余额：" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0053.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0053.value);
        return respDto;
    }

    private  Map<String,Object> calLmtAntLoanSpacBalanceAmt( String cusIds ,String instuCde) {
        //查询客户（综合授信+主体授信）未到期的授信金额
        Map<String,Object> result  = comm4Service.calLmtAntLoanSpacBalanceAmt(cusIds,instuCde);
        return result;
    }

}