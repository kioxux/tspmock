package cn.com.yusys.yusp.web.server.cmislmt0016;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0016.req.CmisLmt0016ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0016.resp.CmisLmt0016RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0016.CmisLmt0016Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:合作方客户额度查询
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "cmislmt0016:合作方客户额度查询")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0016Resource.class);

    @Autowired
    private CmisLmt0016Service cmisLmt0016Service;

    /**
     * 交易码：cmislmt0016
     * 交易描述：合作方客户额度查询
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("合作方客户额度查询")
    @PostMapping("/cmislmt0016")
    protected @ResponseBody
    ResultDto<CmisLmt0016RespDto> cmisLmt0016(@Validated @RequestBody CmisLmt0016ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0016.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0016.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0016RespDto> cmisLmt0016RespDtoResultDto = new ResultDto<>();
        CmisLmt0016RespDto cmisLmt0016RespDto = new CmisLmt0016RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0016RespDto = cmisLmt0016Service.execute(reqDto);
            cmisLmt0016RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0016RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0016.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0016.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0016.value, e.getMessage());
            // 封装xddb0016DataResultDto中异常返回码和返回信息
            cmisLmt0016RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0016RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0016RespDto到cmisLmt0016RespDtoResultDto中
        cmisLmt0016RespDtoResultDto.setData(cmisLmt0016RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0016.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0016.value, JSON.toJSONString(cmisLmt0016RespDtoResultDto));
        return cmisLmt0016RespDtoResultDto;
    }
}
