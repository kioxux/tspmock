package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;
/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtAccTotalDto
 * @类描述: lmt_acc数据实体类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-02-22 21:28:25
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */

public class LmtAccTotalDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 客户授信总额度(元) **/
    private BigDecimal lmtTotalAmt;

    /** 已用授信额度(元) **/
    private BigDecimal usedLmtTotalAmt;

    /** 可用授信额度(元) **/
    private BigDecimal availableLmtTotalAmt;

    /** 额度项下授信协议编号集合拼接成的字符串 **/
    private String   LmtLimitNos;



    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public BigDecimal getLmtTotalAmt() {
        return lmtTotalAmt;
    }

    public void setLmtTotalAmt(BigDecimal lmtTotalAmt) {
        this.lmtTotalAmt = lmtTotalAmt;
    }

    public BigDecimal getUsedLmtTotalAmt() {
        return usedLmtTotalAmt;
    }

    public void setUsedLmtTotalAmt(BigDecimal usedLmtTotalAmt) {
        this.usedLmtTotalAmt = usedLmtTotalAmt;
    }

    public BigDecimal getAvailableLmtTotalAmt() {
        return availableLmtTotalAmt;
    }

    public void setAvailableLmtTotalAmt(BigDecimal availableLmtTotalAmt) {
        this.availableLmtTotalAmt = availableLmtTotalAmt;
    }

    public String getLmtLimitNos() {
        return LmtLimitNos;
    }

    public void setLmtLimitNos(String lmtLimitNos) {
        LmtLimitNos = lmtLimitNos;
    }



}
