/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtContRelChgApp;
import cn.com.yusys.yusp.service.LmtContRelChgAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtContRelChgAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-21 14:45:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtcontrelchgapp")
public class LmtContRelChgAppResource {
    @Autowired
    private LmtContRelChgAppService lmtContRelChgAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtContRelChgApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtContRelChgApp> list = lmtContRelChgAppService.selectAll(queryModel);
        return new ResultDto<List<LmtContRelChgApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtContRelChgApp>> index(QueryModel queryModel) {
        List<LmtContRelChgApp> list = lmtContRelChgAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtContRelChgApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtContRelChgApp>> selectByModel(@RequestBody QueryModel queryModel) {
        List<LmtContRelChgApp> list = lmtContRelChgAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtContRelChgApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtContRelChgApp> show(@PathVariable("pkId") String pkId) {
        LmtContRelChgApp lmtContRelChgApp = lmtContRelChgAppService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtContRelChgApp>(lmtContRelChgApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtContRelChgApp> create(@RequestBody LmtContRelChgApp lmtContRelChgApp) {
        lmtContRelChgAppService.insert(lmtContRelChgApp);
        return new ResultDto<LmtContRelChgApp>(lmtContRelChgApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtContRelChgApp lmtContRelChgApp) {
        int result = lmtContRelChgAppService.update(lmtContRelChgApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtContRelChgAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtContRelChgAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:logicalDelete
     * @函数描述:产品删除时，数据逻辑删除(既将操作类型更新为“02-删除”)，不进行物理删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/logicalDelete/{pkId}")
    protected ResultDto<Integer> logicalDelete(@PathVariable("pkId") String pkId ) {
        int result = lmtContRelChgAppService.logicDelete(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:
     * @函数描述:检查该交易业务编号是否存在
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectDealBizNoIsExist")
    protected ResultDto<Boolean> selectDealBizNoIsExist(@RequestBody QueryModel queryModel) {
        String dealBizNo = (String) queryModel.getCondition().get("dealBizNo");
        boolean isExist = lmtContRelChgAppService.checkDealBizNoIsExist(dealBizNo);
        return new ResultDto<Boolean>(isExist);
    }
}
