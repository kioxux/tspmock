/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtRisKhfxJgbx
 * @类描述: lmt_ris_khfx_jgbx数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-25 15:03:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_ris_khfx_jgbx")
public class LmtRisKhfxJgbx extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 数据日期 **/
	@Id
	@Column(name = "data_dt")
	private String dataDt;
	
	/** 客户类型代码 **/
	@Id
	@Column(name = "cust_type_id")
	private String custTypeId;
	
	/** 客户编号 **/
	@Id
	@Column(name = "cust_id")
	private String custId;
	
	/** 客户名称 **/
	@Column(name = "cust_name", unique = false, nullable = true, length = 100)
	private String custName;
	
	/** 资产余额 **/
	@Column(name = "balance", unique = false, nullable = true, length = 26)
	private java.math.BigDecimal balance;
	
	/** 应收利息 **/
	@Column(name = "receivable_interest", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal receivableInterest;
	
	/** 一般风险暴露 **/
	@Column(name = "normal_risk_expo", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal normalRiskExpo;
	
	/** 特定风险暴露 **/
	@Column(name = "special_risk_expo", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal specialRiskExpo;
	
	/** 交易对手风险暴露 **/
	@Column(name = "counterparty_expo", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal counterpartyExpo;
	
	/** 潜在风险暴露 **/
	@Column(name = "potential_expo", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal potentialExpo;
	
	/** 缓释金额 **/
	@Column(name = "coll_amt", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal collAmt;
	
	/** 减值准备 **/
	@Column(name = "reserve", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal reserve;
	
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param custTypeId
	 */
	public void setCustTypeId(String custTypeId) {
		this.custTypeId = custTypeId;
	}
	
    /**
     * @return custTypeId
     */
	public String getCustTypeId() {
		return this.custTypeId;
	}
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
    /**
     * @return custId
     */
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
    /**
     * @return custName
     */
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param balance
	 */
	public void setBalance(java.math.BigDecimal balance) {
		this.balance = balance;
	}
	
    /**
     * @return balance
     */
	public java.math.BigDecimal getBalance() {
		return this.balance;
	}
	
	/**
	 * @param receivableInterest
	 */
	public void setReceivableInterest(java.math.BigDecimal receivableInterest) {
		this.receivableInterest = receivableInterest;
	}
	
    /**
     * @return receivableInterest
     */
	public java.math.BigDecimal getReceivableInterest() {
		return this.receivableInterest;
	}
	
	/**
	 * @param normalRiskExpo
	 */
	public void setNormalRiskExpo(java.math.BigDecimal normalRiskExpo) {
		this.normalRiskExpo = normalRiskExpo;
	}
	
    /**
     * @return normalRiskExpo
     */
	public java.math.BigDecimal getNormalRiskExpo() {
		return this.normalRiskExpo;
	}
	
	/**
	 * @param specialRiskExpo
	 */
	public void setSpecialRiskExpo(java.math.BigDecimal specialRiskExpo) {
		this.specialRiskExpo = specialRiskExpo;
	}
	
    /**
     * @return specialRiskExpo
     */
	public java.math.BigDecimal getSpecialRiskExpo() {
		return this.specialRiskExpo;
	}
	
	/**
	 * @param counterpartyExpo
	 */
	public void setCounterpartyExpo(java.math.BigDecimal counterpartyExpo) {
		this.counterpartyExpo = counterpartyExpo;
	}
	
    /**
     * @return counterpartyExpo
     */
	public java.math.BigDecimal getCounterpartyExpo() {
		return this.counterpartyExpo;
	}
	
	/**
	 * @param potentialExpo
	 */
	public void setPotentialExpo(java.math.BigDecimal potentialExpo) {
		this.potentialExpo = potentialExpo;
	}
	
    /**
     * @return potentialExpo
     */
	public java.math.BigDecimal getPotentialExpo() {
		return this.potentialExpo;
	}
	
	/**
	 * @param collAmt
	 */
	public void setCollAmt(java.math.BigDecimal collAmt) {
		this.collAmt = collAmt;
	}
	
    /**
     * @return collAmt
     */
	public java.math.BigDecimal getCollAmt() {
		return this.collAmt;
	}
	
	/**
	 * @param reserve
	 */
	public void setReserve(java.math.BigDecimal reserve) {
		this.reserve = reserve;
	}
	
    /**
     * @return reserve
     */
	public java.math.BigDecimal getReserve() {
		return this.reserve;
	}


}