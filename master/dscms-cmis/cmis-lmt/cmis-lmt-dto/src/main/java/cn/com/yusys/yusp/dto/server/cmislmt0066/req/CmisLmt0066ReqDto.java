package cn.com.yusys.yusp.dto.server.cmislmt0066.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0066ReqDto implements Serializable {
    private static final long serialVersionUID = 5461506169208969818L;

    //客户编号
    @JsonProperty(value = "dealBizNo")
    private String dealBizNo;

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    @Override
    public String toString() {
        return "CmisLmt0066ReqDto{" +
                "dealBizNo='" + dealBizNo + '\'' +
                '}';
    }
}
