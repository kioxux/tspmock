package cn.com.yusys.yusp.dto.server.cmislmt0054.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0054RespDto implements Serializable {

    private static final long serialVersionUID = 4257512024639182666L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息
    @JsonProperty(value = "avlAmt")
    private BigDecimal avlAmt;//授信总额

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public BigDecimal getAvlAmt() {
        return avlAmt;
    }

    public void setAvlAmt(BigDecimal avlAmt) {
        this.avlAmt = avlAmt;
    }

    @Override
    public String toString() {
        return "CmisLmt0054RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", avlAmt=" + avlAmt +
                '}';
    }
}
