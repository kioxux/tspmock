package cn.com.yusys.yusp.dto.server.cmisLmt0037.req;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0037ReqDto
 * @类描述: #对内服务类
 * @功能描述:
 * @创建时间: 2021-07-01
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CmisLmt0037ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "queryType")
    private String queryType;//查询类型

    @JsonProperty(value = "accNo")
    private String accNo;//批复台账编号

    @JsonProperty(value = "accSubNo")
    private String accSubNo;//分项编号

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getAccSubNo() {
        return accSubNo;
    }

    public void setAccSubNo(String accSubNo) {
        this.accSubNo = accSubNo;
    }

    @Override
    public String toString() {
        return "CmisLmt0037ReqDto{" +
                "queryType='" + queryType + '\'' +
                ", accNo='" + accNo + '\'' +
                ", accSubNo='" + accSubNo + '\'' +
                '}';
    }
}
