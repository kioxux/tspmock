package cn.com.yusys.yusp.dto.server.cmislmt0063.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0063ReqDto implements Serializable {
    private static final long serialVersionUID = 5461506169208969818L;

    //分项编号
    @JsonProperty(value = "subSerno")
    private String subSerno;

    public String getSubSerno() {
        return subSerno;
    }

    public void setSubSerno(String subSerno) {
        this.subSerno = subSerno;
    }

    @Override
    public String toString() {
        return "CmisLmt0063ReqDto{" +
                "subSerno='" + subSerno + '\'' +
                '}';
    }
}
