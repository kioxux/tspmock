package cn.com.yusys.yusp.dto.server.cmislmt0046.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CmisLmt0046RespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息
    @JsonProperty(value = "hasBussFlag")
    private String hasBussFlag;//是否存在
    @JsonProperty(value = "subNo")
    private String subNo;//分项明细编号

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getHasBussFlag() {
        return hasBussFlag;
    }

    public void setHasBussFlag(String hasBussFlag) {
        this.hasBussFlag = hasBussFlag;
    }

    public String getSubNo() {
        return subNo;
    }

    public void setSubNo(String subNo) {
        this.subNo = subNo;
    }

    @Override
    public String toString() {
        return "CmisLmt0046RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", hasBussFlag='" + hasBussFlag + '\'' +
                ", subNo='" + subNo + '\'' +
                '}';
    }
}
