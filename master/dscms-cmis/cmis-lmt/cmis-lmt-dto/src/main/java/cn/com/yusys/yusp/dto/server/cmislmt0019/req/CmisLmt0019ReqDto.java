package cn.com.yusys.yusp.dto.server.cmislmt0019.req;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 请求Dto：综合授信额度校验
 */
public class CmisLmt0019ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "isQuryGrp")
    private String isQuryGrp;//是否查询集团向下
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getIsQuryGrp() {
        return isQuryGrp;
    }

    public void setIsQuryGrp(String isQuryGrp) {
        this.isQuryGrp = isQuryGrp;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    @Override
    public String toString() {
        return "CmisLmt0019ReqDto{" +
                "instuCde='" + instuCde + '\'' +
                ", cusId='" + cusId + '\'' +
                ", isQuryGrp='" + isQuryGrp + '\'' +
                ", queryType='" + queryType + '\'' +
                '}';
    }
}
