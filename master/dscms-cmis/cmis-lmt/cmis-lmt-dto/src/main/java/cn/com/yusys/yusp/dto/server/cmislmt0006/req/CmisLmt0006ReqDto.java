package cn.com.yusys.yusp.dto.server.cmislmt0006.req;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

/**
 * 请求Dto：冻结额度接口
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0006ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "sysId")
    private String sysId;//系统编号
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期
    /**
     *授信类型
     *  01-单一客户授信
     *  03-合作方授信
     */
    @JsonProperty(value = "lmtType")
    private String lmtType;

    private List<CmisLmt0006ApprListReqDto> apprList;

    private List<CmisLmt0006ApprSubListReqDto> apprSubList;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public List<CmisLmt0006ApprListReqDto> getApprList() {
        return apprList;
    }

    public void setApprList(List<CmisLmt0006ApprListReqDto> apprList) {
        this.apprList = apprList;
    }

    public List<CmisLmt0006ApprSubListReqDto> getApprSubList() {
        return apprSubList;
    }

    public void setApprSubList(List<CmisLmt0006ApprSubListReqDto> apprSubList) {
        this.apprSubList = apprSubList;
    }

    public String getLmtType() {
        return lmtType;
    }

    public void setLmtType(String lmtType) {
        this.lmtType = lmtType;
    }

    @Override
    public String toString() {
        return "CmisLmt0006ReqDto{" +
                "sysId='" + sysId + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", lmtType='" + lmtType + '\'' +
                '}';
    }
}
