package cn.com.yusys.yusp.dto.server.cmisLmt0027.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * 合作方客户额度查询 接口
 * add by zhangjw 20210612
 */
public class CmisLmt0027RespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode;
    @JsonProperty(value = "errorMsg")
    private String errorMsg;
    @JsonProperty(value = "lmtAmt")
    private BigDecimal lmtAmt;
    @JsonProperty(value = "outstandAmt")
    private BigDecimal outstandAmt;
    @JsonProperty(value = "valAmt")
    private BigDecimal valAmt;

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public BigDecimal getOutstandAmt() {
        return outstandAmt;
    }

    public BigDecimal getValAmt() {
        return valAmt;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    public void setOutstandAmt(BigDecimal outstandAmt) {
        this.outstandAmt = outstandAmt;
    }

    public void setValAmt(BigDecimal valAmt) {
        this.valAmt = valAmt;
    }

    @Override
    public String toString() {
        return "CmisLmt0027RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", lmtAmt=" + lmtAmt +
                ", outstandAmt=" + outstandAmt +
                ", valAmt=" + valAmt +
                '}';
    }
}
