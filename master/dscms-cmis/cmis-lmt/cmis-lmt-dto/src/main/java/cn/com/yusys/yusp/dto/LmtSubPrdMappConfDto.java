package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtSubPrdMappConf
 * @类描述: lmt_sub_prd_mapp_conf数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-15 09:27:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtSubPrdMappConfDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** LIMIT_SUB_NO **/
	private String limitSubNo;
	
	private List<LmtSubPrdMappConfListDto> lmtSubPrdMappConfListDto;

	public String getLimitSubNo() {
		return limitSubNo;
	}

	public List<LmtSubPrdMappConfListDto> getLmtSubPrdMappConfListDto() {
		return lmtSubPrdMappConfListDto;
	}

	public void setLimitSubNo(String limitSubNo) {
		this.limitSubNo = limitSubNo;
	}

	public void setLmtSubPrdMappConfListDto(List<LmtSubPrdMappConfListDto> lmtSubPrdMappConfListDto) {
		this.lmtSubPrdMappConfListDto = lmtSubPrdMappConfListDto;
	}

	public LmtSubPrdMappConfDto(String limitSubNo, List<LmtSubPrdMappConfListDto> lmtSubPrdMappConfListDto) {
		this.limitSubNo = limitSubNo;
		this.lmtSubPrdMappConfListDto = lmtSubPrdMappConfListDto;
	}

	public LmtSubPrdMappConfDto() {
	}
}