package cn.com.yusys.yusp.dto.server.cmislmt0020.resp;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

/**
 * 响应Dto：承兑行白名单额度查询
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0020RespDto {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * 承兑行白名单额度列表
     */
    private List<CmisLmt0020LmtListRespDto> lmtList;

    public List<CmisLmt0020LmtListRespDto> getLmtList() {
        return lmtList;
    }

    public void setLmtList(List<CmisLmt0020LmtListRespDto> lmtList) {
        this.lmtList = lmtList;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        return "CmisLmt0020RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                '}';
    }
}
