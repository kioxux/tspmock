package cn.com.yusys.yusp.dto.server.cmislmt0041.req;

import cn.com.yusys.yusp.dto.server.cmislmt0040.req.CmisLmt0040MemberListReqDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;

public class CmisLmt0041ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")           //交易流水号
    private String serno;
    @JsonProperty(value = "instuCde")    //金融机构代码
    private String instuCde;
    @JsonProperty(value = "cusId")       //客户编号
    private String cusId;
    @JsonProperty(value = "cusName")     //客户名称
    private String cusName;
    @JsonProperty(value = "apprSerno")   //批复台账编号
    private String apprSerno;
    @JsonProperty(value = "parent")      //批复分项编号
    private String parent;
    @JsonProperty(value = "apprSubSerno")//适用产品编号
    private String apprSubSerno;
    @JsonProperty(value = "term")        //期限
    private Integer term;
    @JsonProperty(value = "startDate")   //起始日期
    private String startDate;
    @JsonProperty(value = "endDate")     //到期日期
    private String endDate;
    @JsonProperty(value = "limitSubNo")  //授信品种编号
    private String limitSubNo;
    @JsonProperty(value = "limitSubName")//授信品种名称
    private String limitSubName;
    @JsonProperty(value = "suitGuarWay") //担保方式
    private String suitGuarWay;
    @JsonProperty(value = "lmtAmt")      //授信金额
    private BigDecimal lmtAmt;
    @JsonProperty(value = "bailPreRate") //保证金比例
    private BigDecimal bailPreRate;
    @JsonProperty(value = "rateYear") //年利率
    private BigDecimal rateYear;
    @JsonProperty(value = "inputId") //登记人
    private String inputId;
    @JsonProperty(value = "inputBrId") //登记机构
    private String inputBrId;
    @JsonProperty(value = "inputDate") //登记日期
    private String inputDate;
    @JsonProperty(value = "lmtGraper") //宽限期
    private Integer lmtGraper;

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getApprSerno() {
        return apprSerno;
    }

    public void setApprSerno(String apprSerno) {
        this.apprSerno = apprSerno;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getApprSubSerno() {
        return apprSubSerno;
    }

    public void setApprSubSerno(String apprSubSerno) {
        this.apprSubSerno = apprSubSerno;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    public String getLimitSubName() {
        return limitSubName;
    }

    public void setLimitSubName(String limitSubName) {
        this.limitSubName = limitSubName;
    }

    public String getSuitGuarWay() {
        return suitGuarWay;
    }

    public void setSuitGuarWay(String suitGuarWay) {
        this.suitGuarWay = suitGuarWay;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    public BigDecimal getBailPreRate() {
        return bailPreRate;
    }

    public void setBailPreRate(BigDecimal bailPreRate) {
        this.bailPreRate = bailPreRate;
    }

    public BigDecimal getRateYear() {
        return rateYear;
    }

    public void setRateYear(BigDecimal rateYear) {
        this.rateYear = rateYear;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public Integer getLmtGraper() {
        return lmtGraper;
    }

    public void setLmtGraper(Integer lmtGraper) {
        this.lmtGraper = lmtGraper;
    }

    @Override
    public String toString() {
        return "CmisLmt0041ReqDto{" +
                "serno='" + serno + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", apprSerno='" + apprSerno + '\'' +
                ", parent='" + parent + '\'' +
                ", apprSubSerno='" + apprSubSerno + '\'' +
                ", term=" + term +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", limitSubNo='" + limitSubNo + '\'' +
                ", limitSubName='" + limitSubName + '\'' +
                ", suitGuarWay='" + suitGuarWay + '\'' +
                ", lmtAmt=" + lmtAmt +
                ", bailPreRate=" + bailPreRate +
                ", rateYear=" + rateYear +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", lmtGraper=" + lmtGraper +
                '}';
    }
}
