package cn.com.yusys.yusp.dto.server.cmislmt0016.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：合作方额度查询
 * @author xull
 * @version 1.0             
 */             
@JsonPropertyOrder(alphabetic = true)
public class Cmislmt0016LmtCopSubAccListRespDto implements Serializable {
	   private static final long serialVersionUID = 1L;
	 @JsonProperty(value = "accNo")
	private String accNo;//批复台账编号
	 @JsonProperty(value = "subAccNo")
	private String subAccNo;//分项编号
	 @JsonProperty(value = "limitSubNo")
	private String limitSubNo;//品种编号/项目编号
	 @JsonProperty(value = "limitSubName")
	private String limitSubName;//分项名称/项目名称
	 @JsonProperty(value = "avlAmt")
	private BigDecimal avlAmt;//总合作额度
	 @JsonProperty(value = "avlOutstndAmt")
	private BigDecimal avlOutstndAmt;//总合作额度已用
	 @JsonProperty(value = "avlAvailAmt")
	private BigDecimal avlAvailAmt;//总合作额度可用
	 @JsonProperty(value = "cny")
	private String cny;//币种
	 @JsonProperty(value = "term")
	private Integer term;//期限
	 @JsonProperty(value = "startDate")
	private String startDate;//授信起始日
	 @JsonProperty(value = "endDate")
	private String endDate;//授信到期日
	 @JsonProperty(value = "isRevolv")
	private String isRevolv;//是否可循环
	 @JsonProperty(value = "status")
	private String status;//额度状态
	 @JsonProperty(value = "sigAmt")
	private BigDecimal sigAmt;//单户限额
	 @JsonProperty(value = "sigBussAmt")
	private BigDecimal sigBussAmt;//单笔业务限额
	public String  getAccNo() { return accNo; }
	public void setAccNo(String accNo ) { this.accNo = accNo;}
	public String  getSubAccNo() { return subAccNo; }
	public void setSubAccNo(String subAccNo ) { this.subAccNo = subAccNo;}
	public String  getLimitSubNo() { return limitSubNo; }
	public void setLimitSubNo(String limitSubNo ) { this.limitSubNo = limitSubNo;}
	public String  getLimitSubName() { return limitSubName; }
	public void setLimitSubName(String limitSubName ) { this.limitSubName = limitSubName;}
	public BigDecimal  getAvlAmt() { return avlAmt; }
	public void setAvlAmt(BigDecimal avlAmt ) { this.avlAmt = avlAmt;}
	public BigDecimal  getAvlOutstndAmt() { return avlOutstndAmt; }
	public void setAvlOutstndAmt(BigDecimal avlOutstndAmt ) { this.avlOutstndAmt = avlOutstndAmt;}
	public BigDecimal  getAvlAvailAmt() { return avlAvailAmt; }
	public void setAvlAvailAmt(BigDecimal avlAvailAmt ) { this.avlAvailAmt = avlAvailAmt;}
	public String  getCny() { return cny; }
	public void setCny(String cny ) { this.cny = cny;}
	public Integer  getTerm() { return term; }
	public void setTerm(Integer term ) { this.term = term;}
	public String  getStartDate() { return startDate; }
	public void setStartDate(String startDate ) { this.startDate = startDate;}
	public String  getEndDate() { return endDate; }
	public void setEndDate(String endDate ) { this.endDate = endDate;}
	public String  getIsRevolv() { return isRevolv; }
	public void setIsRevolv(String isRevolv ) { this.isRevolv = isRevolv;}
	public String  getStatus() { return status; }
	public void setStatus(String status ) { this.status = status;}
	public BigDecimal  getSigAmt() { return sigAmt; }
	public void setSigAmt(BigDecimal sigAmt ) { this.sigAmt = sigAmt;}
	public BigDecimal  getSigBussAmt() { return sigBussAmt; }
	public void setSigBussAmt(BigDecimal sigBussAmt ) { this.sigBussAmt = sigBussAmt;}

	@Override
	public String toString() {
		return "Cmislmt0016DataRespDto{" +
				"accNo='" + accNo + '\'' +
				", subAccNo='" + subAccNo + '\'' +
				", limitSubNo='" + limitSubNo + '\'' +
				", limitSubName='" + limitSubName + '\'' +
				", avlAmt=" + avlAmt +
				", avlOutstndAmt=" + avlOutstndAmt +
				", avlAvailAmt=" + avlAvailAmt +
				", cny='" + cny + '\'' +
				", term=" + term +
				", startDate='" + startDate + '\'' +
				", endDate='" + endDate + '\'' +
				", isRevolv='" + isRevolv + '\'' +
				", status='" + status + '\'' +
				", sigAmt=" + sigAmt +
				", sigBussAmt=" + sigBussAmt +
				'}';
	}
}
