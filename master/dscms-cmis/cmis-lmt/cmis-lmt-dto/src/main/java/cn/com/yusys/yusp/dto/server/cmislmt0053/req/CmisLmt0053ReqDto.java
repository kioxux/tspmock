package cn.com.yusys.yusp.dto.server.cmislmt0053.req;

import cn.com.yusys.yusp.dto.server.cmislmt0051.resp.LmtDiscOrgDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0053ReqDto implements Serializable {
    private static final long serialVersionUID = 5461506169208969818L;

    @JsonProperty(value = "dealBizNo")
    private String dealBizNo;// 交易流水号
    @JsonProperty(value = "instuCde")
    private String instuCde;// 金融机构代码
    @JsonProperty(value = "cusListDtoList")
    private List<CusListDto> cusListDtoList;

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public List<CusListDto> getCusListDtoList() {
        return cusListDtoList;
    }

    public void setCusListDtoList(List<CusListDto> cusListDtoList) {
        this.cusListDtoList = cusListDtoList;
    }

    @Override
    public String toString() {
        return "CmisLmt0053ReqDto{" +
                "dealBizNo='" + dealBizNo + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", CusListDtoList=" + cusListDtoList +
                '}';
    }
}
