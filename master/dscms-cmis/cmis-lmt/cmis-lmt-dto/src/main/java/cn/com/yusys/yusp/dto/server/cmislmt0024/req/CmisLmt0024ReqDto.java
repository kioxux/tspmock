package cn.com.yusys.yusp.dto.server.cmislmt0024.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * 请求Dto：承兑行白名单额度占用
 */
public class CmisLmt0024ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "sysNo")
    private String sysNo;//系统编号
    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码

    //是否占用
    @JsonProperty(value = "isOccLmt")
    private String isOccLmt;

    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期

    /**
     * 占用额度列表
     */
    private List<CmisLmt0024OccRelListReqDto> occRelList;

    public List<CmisLmt0024OccRelListReqDto> getOccRelList(){
        return occRelList;
    }

    public void setOccRelList(List<CmisLmt0024OccRelListReqDto> occRelList){
        this.occRelList = occRelList;
    }

    public String getSysNo() {
        return sysNo;
    }

    public void setSysNo(String sysNo) {
        this.sysNo = sysNo;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getIsOccLmt() {
        return isOccLmt;
    }

    public void setIsOccLmt(String isOccLmt) {
        this.isOccLmt = isOccLmt;
    }

    @Override
    public String toString() {
        return "CmisLmt0024ReqDto{" +
                "sysNo='" + sysNo + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", isOccLmt='" + isOccLmt + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", occRelList=" + occRelList +
                '}';
    }
}