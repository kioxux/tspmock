package cn.com.yusys.yusp.dto.server.cmisLmt0036.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CmisLmt0036ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "accSubNo")
    private String accSubNo;//分项编号

    public String getAccSubNo() {
        return accSubNo;
    }

    public void setAccSubNo(String accSubNo) {
        this.accSubNo = accSubNo;
    }

    @Override
    public String toString() {
        return "CmisLmt0036ReqDto{" +
                "accSubNo='" + accSubNo + '\'' +
                '}';
    }
}
