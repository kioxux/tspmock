package cn.com.yusys.yusp.dto.server.cmislmt0015.resp;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0015LmtAccListRespDto {

    /**
     * 集团编号
     */
    @JsonProperty(value = "grpCusNo")
    private String grpCusNo;

    /**
     * 客户编号
     */
    @JsonProperty(value = "cusId")
    private String cusId;

    /**
     * 客户名称
     */
    @JsonProperty(value = "cusName")
    private String cusName;

    /**
     * 授信总额
     */
    @JsonProperty(value = "avlAmt")
    private BigDecimal avlAmt;

    /**
     * 授信总额已用
     */
    @JsonProperty(value = "avlOutstndAmt")
    private BigDecimal avlOutstndAmt;

    /**
     * 授信总额可用
     */
    @JsonProperty(value = "avlAvailAmt")
    private BigDecimal avlAvailAmt;

    /**
     * 授信敞口
     */
    @JsonProperty(value = "spacAmt")
    private BigDecimal spacAmt;

    /**
     * 授信敞口已用
     */
    @JsonProperty(value = "spacOutstndAmt")
    private BigDecimal spacOutstndAmt;

    /**
     * 授信敞口可用
     */
    @JsonProperty(value = "spacAvailAmt")
    private BigDecimal spacAvailAmt;

    /**
     * 授信低风险金额
     */
    @JsonProperty(value = "lriskLmt")
    private BigDecimal lriskLmt;


    public String getGrpCusNo() {
        return grpCusNo;
    }

    public void setGrpCusNo(String grpCusNo) {
        this.grpCusNo = grpCusNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public BigDecimal getAvlAmt() {
        return avlAmt;
    }

    public void setAvlAmt(BigDecimal avlAmt) {
        this.avlAmt = avlAmt;
    }

    public BigDecimal getAvlOutstndAmt() {
        return avlOutstndAmt;
    }

    public void setAvlOutstndAmt(BigDecimal avlOutstndAmt) {
        this.avlOutstndAmt = avlOutstndAmt;
    }

    public BigDecimal getAvlAvailAmt() {
        return avlAvailAmt;
    }

    public void setAvlAvailAmt(BigDecimal avlAvailAmt) {
        this.avlAvailAmt = avlAvailAmt;
    }

    public BigDecimal getSpacAmt() {
        return spacAmt;
    }

    public void setSpacAmt(BigDecimal spacAmt) {
        this.spacAmt = spacAmt;
    }

    public BigDecimal getSpacOutstndAmt() {
        return spacOutstndAmt;
    }

    public void setSpacOutstndAmt(BigDecimal spacOutstndAmt) {
        this.spacOutstndAmt = spacOutstndAmt;
    }

    public BigDecimal getSpacAvailAmt() {
        return spacAvailAmt;
    }

    public void setSpacAvailAmt(BigDecimal spacAvailAmt) {
        this.spacAvailAmt = spacAvailAmt;
    }

    public BigDecimal getLriskLmt() {
        return lriskLmt;
    }

    public void setLriskLmt(BigDecimal lriskLmt) {
        this.lriskLmt = lriskLmt;
    }

    @Override
    public String toString() {
        return "CmisLmt0015LmtAccListRespDto{" +
                "grpCusNo='" + grpCusNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", avlAmt=" + avlAmt +
                ", avlOutstndAmt=" + avlOutstndAmt +
                ", avlAvailAmt=" + avlAvailAmt +
                ", spacAmt=" + spacAmt +
                ", spacOutstndAmt=" + spacOutstndAmt +
                ", spacAvailAmt=" + spacAvailAmt +
                ", lriskLmt=" + lriskLmt +
                '}';
    }
}
