package cn.com.yusys.yusp.dto.server.cmisLmt0027.req;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 合作方客户额度查询 接口
 * add by zhangjw 20210612
 */
public class CmisLmt0027ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "instuCde")
    private String instuCde;
    @JsonProperty(value = "cusId")
    private String cusId;

    public String getInstuCde() {
        return instuCde;
    }

    public String getCusId() {
        return cusId;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "CmisLmt0027ReqDto{" +
                "instuCde='" + instuCde + '\'' +
                ", cusId='" + cusId + '\'' +
                '}';
    }
}
