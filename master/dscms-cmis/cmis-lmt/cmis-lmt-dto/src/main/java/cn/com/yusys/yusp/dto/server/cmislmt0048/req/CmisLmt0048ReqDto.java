package cn.com.yusys.yusp.dto.server.cmislmt0048.req;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CmisLmt0048ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "accSubNo")//分项编号
    private String accSubNo;

    @JsonProperty(value = "isHighLmt") //是否最高额协议
    private String isHighLmt;

    public String getAccSubNo() {
        return accSubNo;
    }

    public void setAccSubNo(String accSubNo) {
        this.accSubNo = accSubNo;
    }

    public String getIsHighLmt() {
        return isHighLmt;
    }

    public void setIsHighLmt(String isHighLmt) {
        this.isHighLmt = isHighLmt;
    }

    @Override
    public String toString() {
        return "CmisLmt0048ReqDto{" +
                "accSubNo='" + accSubNo + '\'' +
                ", isHighLmt='" + isHighLmt + '\'' +
                '}';
    }
}
