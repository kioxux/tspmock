package cn.com.yusys.yusp.dto.server.cmislmt0049.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0049ReqDto implements Serializable {
    private static final long serialVersionUID = -5537027977464464494L;

    @JsonProperty(value = "orgId",required = true)
    private String orgId;

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    @Override
    public String toString() {
        return "CmisLmt0049ReqDto{" +
                "orgId='" + orgId + '\'' +
                '}';
    }
}
