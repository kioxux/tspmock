package cn.com.yusys.yusp.dto.server.cmislmt0026.req;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 请求Dto：额度分项信息查询
 */
public class CmisLmt0026ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码
    @JsonProperty(value = "subSerno")
    private String subSerno;//分项编号
    @JsonProperty(value = "queryType")
    private String queryType;//分项类型

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getSubSerno() {
        return subSerno;
    }

    public void setSubSerno(String subSerno) {
        this.subSerno = subSerno;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    @Override
    public String toString() {
        return "CmisLmt0026ReqDto{" +
                "instuCde='" + instuCde + '\'' +
                ", subSerno='" + subSerno + '\'' +
                ", queryType='" + queryType + '\'' +
                '}';
    }
}