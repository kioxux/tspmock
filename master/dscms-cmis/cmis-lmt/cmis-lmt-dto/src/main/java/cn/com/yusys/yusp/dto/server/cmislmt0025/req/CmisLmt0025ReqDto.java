package cn.com.yusys.yusp.dto.server.cmislmt0025.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * 请求Dto：承兑行白名单额度恢复
 */
public class CmisLmt0025ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "sysNo")
    private String sysNo;//系统编号
    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码

    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期

    private List<CmisLmt0025OccRelListReqDto> occRelList;

    public String getSysNo() {
        return sysNo;
    }

    public void setSysNo(String sysNo) {
        this.sysNo = sysNo;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public List<CmisLmt0025OccRelListReqDto> getOccRelList() {
        return occRelList;
    }

    public void setOccRelList(List<CmisLmt0025OccRelListReqDto> occRelList) {
        this.occRelList = occRelList;
    }

    @Override
    public String toString() {
        return "CmisLmt0025ReqDto{" +
                "sysNo='" + sysNo + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", occRelList=" + occRelList +
                '}';
    }
}