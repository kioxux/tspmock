package cn.com.yusys.yusp.dto.server.cmislmt0063.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0063SubListRespDto implements Serializable {

    private static final long serialVersionUID = 4257512024639182666L;

    //明细分项编号
    @JsonProperty(value = "apprSubSerno")
    private String apprSubSerno;

    //额度品种编号
    @JsonProperty(value = "limitSubNo")
    private String limitSubNo;

    //额度品种名称
    @JsonProperty(value = "limitSubName")
    private String limitSubName;

    public String getApprSubSerno() {
        return apprSubSerno;
    }

    public void setApprSubSerno(String apprSubSerno) {
        this.apprSubSerno = apprSubSerno;
    }

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    public String getLimitSubName() {
        return limitSubName;
    }

    public void setLimitSubName(String limitSubName) {
        this.limitSubName = limitSubName;
    }

    @Override
    public String toString() {
        return "CmisLmt0063SubListRespDto{" +
                "apprSubSerno='" + apprSubSerno + '\'' +
                ", limitSubNo='" + limitSubNo + '\'' +
                ", limitSubName='" + limitSubName + '\'' +
                '}';
    }
}
