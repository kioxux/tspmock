package cn.com.yusys.yusp.dto.server.cmislmt0042.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CmisLmt0042ListRespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "prdTypePropNo")
    private String prdTypePropNo ;//产品类型属性配置编号
    @JsonProperty(value = "prdId")
    private String prdId         ;//产品编号
    @JsonProperty(value = "typePropNo")
    private String typePropNo    ;//产品类型属性编号
    @JsonProperty(value = "inputId")
    private String inputId       ;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId     ;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate     ;//登记日期
    @JsonProperty(value = "updId")
    private String updId         ;//更新人
    @JsonProperty(value = "updBrId")
    private String updBrId       ;//更新机构
    @JsonProperty(value = "updDate")
    private String updDate       ;//更新日期
    @JsonProperty(value = "oprType")
    private String oprType       ;//操作类型

    public String getPrdTypePropNo() {
        return prdTypePropNo;
    }

    public void setPrdTypePropNo(String prdTypePropNo) {
        this.prdTypePropNo = prdTypePropNo;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getTypePropNo() {
        return typePropNo;
    }

    public void setTypePropNo(String typePropNo) {
        this.typePropNo = typePropNo;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    @Override
    public String toString() {
        return "CmisLmt0042ListRespDto{" +
                "prdTypePropNo='" + prdTypePropNo + '\'' +
                ", prdId='" + prdId + '\'' +
                ", typePropNo='" + typePropNo + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", oprType='" + oprType + '\'' +
                '}';
    }
}
