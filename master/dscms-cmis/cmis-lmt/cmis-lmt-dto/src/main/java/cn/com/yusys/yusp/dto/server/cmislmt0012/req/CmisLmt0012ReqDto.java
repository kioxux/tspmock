package cn.com.yusys.yusp.dto.server.cmislmt0012.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

/**
 * 请求Dto：合同占用接口
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0012ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "sysNo")
    private String sysId;//系统编号

    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码

    @JsonProperty(value = "bizNo")
    private String bizNo;//合同编号

    @JsonProperty(value = "bizStatus")
    private String bizStatus;//业务状态

    @JsonProperty(value = "recoverType")
    private String recoverType;//恢复类型

    @JsonProperty(value = "recoverAmtCny")
    private BigDecimal recoverAmtCny;//恢复金额(人民币)

    @JsonProperty(value = "recoverSpaceAmtCny")
    private BigDecimal recoverSpacAmtCny;//恢复敞口金额(人民币)

    @JsonProperty(value = "inputId")
    private String inputId;//登记人

    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构

    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期

    @JsonProperty(value = "isRecoverCoop")
    private String isRecoverCoop;//是否恢复合作方额度

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getBizNo() {
        return bizNo;
    }

    public void setBizNo(String bizNo) {
        this.bizNo = bizNo;
    }

    public String getBizStatus() {
        return bizStatus;
    }

    public void setBizStatus(String bizStatus) {
        this.bizStatus = bizStatus;
    }

    public String getRecoverType() {
        return recoverType;
    }

    public void setRecoverType(String recoverType) {
        this.recoverType = recoverType;
    }

    public BigDecimal getRecoverAmtCny() {
        return recoverAmtCny;
    }

    public void setRecoverAmtCny(BigDecimal recoverAmtCny) {
        this.recoverAmtCny = recoverAmtCny;
    }

    public BigDecimal getRecoverSpacAmtCny() {
        return recoverSpacAmtCny;
    }

    public void setRecoverSpacAmtCny(BigDecimal recoverSpacAmtCny) {
        this.recoverSpacAmtCny = recoverSpacAmtCny;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getIsRecoverCoop() {
        return isRecoverCoop;
    }

    public void setIsRecoverCoop(String isRecoverCoop) {
        this.isRecoverCoop = isRecoverCoop;
    }

    @Override
    public String toString() {
        return "CmisLmt0012ReqDto{" +
                "sysId='" + sysId + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", bizNo='" + bizNo + '\'' +
                ", bizStatus='" + bizStatus + '\'' +
                ", recoverType='" + recoverType + '\'' +
                ", recoverAmtCny=" + recoverAmtCny +
                ", recoverSpacAmtCny=" + recoverSpacAmtCny +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", isRecoverCoop='" + isRecoverCoop + '\'' +
                '}';
    }
}
