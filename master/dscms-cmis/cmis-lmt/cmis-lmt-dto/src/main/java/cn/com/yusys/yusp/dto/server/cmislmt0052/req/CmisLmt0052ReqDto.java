package cn.com.yusys.yusp.dto.server.cmislmt0052.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0052ReqDto implements Serializable {
    private static final long serialVersionUID = 5461506169208969818L;

    @JsonProperty(value = "dealBizNo")
    private String dealBizNo;// 交易流水号
    @JsonProperty(value = "cusId")
    private String cusId;// 客户号
    @JsonProperty(value = "isQuryGrp")
    private String isQuryGrp;// 是否查询集团项下
    @JsonProperty(value = "instuCde")
    private String instuCde;// 金融机构代码

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getIsQuryGrp() {
        return isQuryGrp;
    }

    public void setIsQuryGrp(String isQuryGrp) {
        this.isQuryGrp = isQuryGrp;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    @Override
    public String toString() {
        return "CmisLmt0052ReqDto{" +
                "dealBizNo='" + dealBizNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", isQuryGrp='" + isQuryGrp + '\'' +
                ", instuCde='" + instuCde + '\'' +
                '}';
    }
}
