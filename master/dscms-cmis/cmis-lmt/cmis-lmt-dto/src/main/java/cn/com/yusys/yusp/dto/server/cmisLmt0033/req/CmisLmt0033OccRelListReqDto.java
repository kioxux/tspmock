package cn.com.yusys.yusp.dto.server.cmisLmt0033.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * 国结票据出账额度校验
 * add by dumd 20210618
 */
public class CmisLmt0033OccRelListReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "dealBizNo")
    private String dealBizNo;//台账编号
    @JsonProperty(value = "lmtCusId")
    private String lmtCusId;//同业客户号
    @JsonProperty(value = "bizSpacAmtCny")
    private BigDecimal bizSpacAmtCny;//占用敞口

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    public String getLmtCusId() {
        return lmtCusId;
    }

    public void setLmtCusId(String lmtCusId) {
        this.lmtCusId = lmtCusId;
    }

    public BigDecimal getBizSpacAmtCny() {
        return bizSpacAmtCny;
    }

    public void setBizSpacAmtCny(BigDecimal bizSpacAmtCny) {
        this.bizSpacAmtCny = bizSpacAmtCny;
    }

    @Override
    public String toString() {
        return "CmisLmt0033OccRelListReqDto{" +
                "dealBizNo='" + dealBizNo + '\'' +
                ", lmtCusId='" + lmtCusId + '\'' +
                ", bizSpacAmtCny=" + bizSpacAmtCny +
                '}';
    }
}
