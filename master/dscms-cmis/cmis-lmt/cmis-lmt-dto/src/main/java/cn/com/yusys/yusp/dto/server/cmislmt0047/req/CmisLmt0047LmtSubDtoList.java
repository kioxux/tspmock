package cn.com.yusys.yusp.dto.server.cmislmt0047.req;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: cmisLmt0047LmtSubDtoList
 * @类描述: #对内服务类
 * @功能描述: TODO
 * @创建时间: 2021/8/28 10:10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CmisLmt0047LmtSubDtoList {
    private static final long serialVersionUID = 1L;
    //分项或品种编号
    @JsonProperty(value = "accSubNo")
    private String accSubNo;

    public String getAccSubNo() {
        return accSubNo;
    }

    public void setAccSubNo(String accSubNo) {
        this.accSubNo = accSubNo;
    }

    @Override
    public String toString() {
        return "CmisLmt0047LmtSubDtoList{" +
                "accSubNo='" + accSubNo + '\'' +
                '}';
    }
}
