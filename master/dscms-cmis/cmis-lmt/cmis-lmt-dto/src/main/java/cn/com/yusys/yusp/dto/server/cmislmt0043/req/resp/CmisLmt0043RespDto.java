package cn.com.yusys.yusp.dto.server.cmislmt0043.req.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class CmisLmt0043RespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息
    @JsonProperty(value = "lmtBal")
    private BigDecimal lmtBal;//授信余额

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public BigDecimal getLmtBal() {
        return lmtBal;
    }

    public void setLmtBal(BigDecimal lmtBal) {
        this.lmtBal = lmtBal;
    }

    @Override
    public String toString() {
        return "CmisLmt0043RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", lmtBal=" + lmtBal +
                '}';
    }
}
