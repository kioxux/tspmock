package cn.com.yusys.yusp.dto.server.cmislmt0004.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：合作方额度同步
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0004ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "sysId")
    private String sysId;//系统编号
    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码
    @JsonProperty(value = "accNo")
    private String accNo;//批复台账编号
    @JsonProperty(value = "origiAccNo")
    private String origiAccNo;//原批复台账编号
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "cusType")
    private String cusType;//客户主体类型
    @JsonProperty(value = "copType")
    private String copType;//合作方类型
    @JsonProperty(value = "isCreateAcc")
    private String isCreateAcc;//是否生成新批复台账
    @JsonProperty(value = "lmtAmt")
    private BigDecimal lmtAmt;//授信金额
    @JsonProperty(value = "term")
    private Integer term;//授信期限
    @JsonProperty(value = "startDate")
    private String startDate;//起始日
    @JsonProperty(value = "endDate")
    private String endDate;//到期日
    @JsonProperty(value = "curType")
    private String curType;//币种
    @JsonProperty(value = "accStatus")
    private String accStatus;//批复台账状态
    @JsonProperty(value = "managerId")
    private String managerId;//责任人
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//责任机构
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期
    @JsonProperty(value = "isWholeBankSuit")
    private String isWholeBankSuit;//是否全行适用

    public String getIsWholeBankSuit() {
        return isWholeBankSuit;
    }

    public void setIsWholeBankSuit(String isWholeBankSuit) {
        this.isWholeBankSuit = isWholeBankSuit;
    }

    /**
     * 授信分项列表
     */
    private List<CmisLmt0004LmtSubListReqDto> lmtSubList;

    public List<CmisLmt0004LmtSubListReqDto> getLmtSubList() {
        return lmtSubList;
    }

    public void setLmtSubList(List<CmisLmt0004LmtSubListReqDto> lmtSubList) {
        this.lmtSubList = lmtSubList;
    }

    /**
     * 适用机构列表
     */
    private List<CmisLmt0004StrOrgListReqDto> strOrgList;

    public List<CmisLmt0004StrOrgListReqDto> getStrOrgList() {
        return strOrgList;
    }

    public void setStrOrgList(List<CmisLmt0004StrOrgListReqDto> strOrgList) {
        this.strOrgList = strOrgList;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getCopType() {
        return copType;
    }

    public void setCopType(String copType) {
        this.copType = copType;
    }

    public String getIsCreateAcc() {
        return isCreateAcc;
    }

    public void setIsCreateAcc(String isCreateAcc) {
        this.isCreateAcc = isCreateAcc;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getOrigiAccNo() {
        return origiAccNo;
    }

    public void setOrigiAccNo(String origiAccNo) {
        this.origiAccNo = origiAccNo;
    }

    @Override
    public String toString() {
        return "Cmislmt0004ReqDto{" +
                "sysId='" + sysId + '\'' +
                "instuCde='" + instuCde + '\'' +
                "accNo='" + accNo + '\'' +
                "cusId='" + cusId + '\'' +
                "origiAccNo='" + origiAccNo + '\'' +
                "cusName='" + cusName + '\'' +
                "cusType='" + cusType + '\'' +
                "copType='" + copType + '\'' +
                "isCreateAcc='" + isCreateAcc + '\'' +
                "lmtAmt='" + lmtAmt + '\'' +
                "term='" + term + '\'' +
                "startDate='" + startDate + '\'' +
                "endDate='" + endDate + '\'' +
                "curType='" + curType + '\'' +
                "accStatus='" + accStatus + '\'' +
                "managerId='" + managerId + '\'' +
                "managerBrId='" + managerBrId + '\'' +
                "inputId='" + inputId + '\'' +
                "inputBrId='" + inputBrId + '\'' +
                "inputDate='" + inputDate + '\'' +
                "isWholeBankSuit='" + isWholeBankSuit + '\'' +
                '}';
    }
}

