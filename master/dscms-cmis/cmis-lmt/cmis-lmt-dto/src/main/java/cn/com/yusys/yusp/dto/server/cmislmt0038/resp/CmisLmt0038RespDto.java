package cn.com.yusys.yusp.dto.server.cmislmt0038.resp;

import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015GrpLmtAccListRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CmisLmt0038RespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    private List<CmisLmt0038PrdListRespDto> cmisLmt0038PrdListRespDtoList;

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public List<CmisLmt0038PrdListRespDto> getCmisLmt0038PrdListRespDtoList() {
        return cmisLmt0038PrdListRespDtoList;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public void setCmisLmt0038PrdListRespDtoList(List<CmisLmt0038PrdListRespDto> cmisLmt0038PrdListRespDtoList) {
        this.cmisLmt0038PrdListRespDtoList = cmisLmt0038PrdListRespDtoList;
    }

    @Override
    public String toString() {
        return "CmisLmt0038RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", cmisLmt0038PrdListRespDtoList=" + cmisLmt0038PrdListRespDtoList +
                '}';
    }
}
