package cn.com.yusys.yusp.dto.server.cmislmt0016.resp;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

/**
 * 响应Dto：合作方额度查询
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0016RespDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息
    @JsonProperty(value = "count")
    private Integer count;//合作方额度总数

    /**
     * 合作方额度批复分项信息列表
     */
    private List<Cmislmt0016LmtCopSubAccListRespDto> lmtCopSubAccList;

    public List<Cmislmt0016LmtCopSubAccListRespDto> getLmtCopSubAccList() {
        return lmtCopSubAccList;
    }

    public void setLmtCopSubAccList(List<Cmislmt0016LmtCopSubAccListRespDto> lmtCopSubAccList) {
        this.lmtCopSubAccList = lmtCopSubAccList;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "CmisLmt0016RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", count='" + count + '\'' +
                '}';
    }
}
