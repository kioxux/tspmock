package cn.com.yusys.yusp.dto.server.cmislmt0042.req;

import cn.com.yusys.yusp.dto.server.cmislmt0040.req.CmisLmt0040MemberListReqDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;

public class CmisLmt0042ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "limitSubNo")           //额度品种编号
    private String limitSubNo;

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    @Override
    public String toString() {
        return "CmisLmt0042ReqDto{" +
                "limitSubNo='" + limitSubNo + '\'' +
                '}';
    }
}
