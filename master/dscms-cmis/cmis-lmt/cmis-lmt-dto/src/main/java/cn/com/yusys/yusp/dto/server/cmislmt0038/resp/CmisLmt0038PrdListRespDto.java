package cn.com.yusys.yusp.dto.server.cmislmt0038.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CmisLmt0038PrdListRespDto {
    /**
     * 产品编号
     */
    @JsonProperty(value = "prdId")
    private String prdId;

    /**
     * 产品名称
     */
    @JsonProperty(value = "prdName")
    private String prdName;

    public String getPrdId() {
        return prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    @Override
    public String toString() {
        return "CmisLmt0015PrdListDto{" +
                "prdId='" + prdId + '\'' +
                ", prdName='" + prdName + '\'' +
                '}';
    }
}
