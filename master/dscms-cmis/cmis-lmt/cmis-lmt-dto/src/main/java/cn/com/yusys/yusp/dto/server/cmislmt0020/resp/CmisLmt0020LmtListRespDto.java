package cn.com.yusys.yusp.dto.server.cmislmt0020.resp;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

/**
 * 响应Dto：承兑行白名单额度查询
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0020LmtListRespDto {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "subAccNo")
    private String subAccNo;//额度分项编号
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "sigAmt")
    private BigDecimal sigAmt;//限额
    @JsonProperty(value = "sigUseAmt")
    private BigDecimal sigUseAmt;//已用限额
    @JsonProperty(value = "sigValAmt")
    private BigDecimal sigValAmt;//可用限额
    @JsonProperty(value = "startDate")
    private String startDate;//生效日期
    @JsonProperty(value = "endDate")
    private String endDate;//到期日期

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }


    public String getSubAccNo() {
        return subAccNo;
    }

    public void setSubAccNo(String subAccNo) {
        this.subAccNo = subAccNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public BigDecimal getSigAmt() {
        return sigAmt;
    }

    public void setSigAmt(BigDecimal sigAmt) {
        this.sigAmt = sigAmt;
    }

    public BigDecimal getSigUseAmt() {
        return sigUseAmt;
    }

    public void setSigUseAmt(BigDecimal sigUseAmt) {
        this.sigUseAmt = sigUseAmt;
    }

    public BigDecimal getSigValAmt() {
        return sigValAmt;
    }

    public void setSigValAmt(BigDecimal sigValAmt) {
        this.sigValAmt = sigValAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "CmisLmt0020RespDto{" +
                ", subAccNo='" + subAccNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", sigAmt=" + sigAmt +
                ", sigUseAmt=" + sigUseAmt +
                ", sigValAmt=" + sigValAmt +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }
}
