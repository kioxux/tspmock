package cn.com.yusys.yusp.dto.server.cmislmt0060.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0060ReqDto implements Serializable {
    private static final long serialVersionUID = 5461506169208969818L;

    //合同号
    @JsonProperty(value = "dealBizNo")
    private String dealBizNo;

    //出账流水号
    @JsonProperty(value = "tranAccNo")
    private String tranAccNo;

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    public String getTranAccNo() {
        return tranAccNo;
    }

    public void setTranAccNo(String tranAccNo) {
        this.tranAccNo = tranAccNo;
    }

    @Override
    public String toString() {
        return "CmisLmt0060ReqDto{" +
                "dealBizNo='" + dealBizNo + '\'' +
                ", tranAccNo='" + tranAccNo + '\'' +
                '}';
    }
}
