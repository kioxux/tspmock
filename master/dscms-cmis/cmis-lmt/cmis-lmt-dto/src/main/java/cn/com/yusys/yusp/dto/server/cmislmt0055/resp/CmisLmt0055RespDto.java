package cn.com.yusys.yusp.dto.server.cmislmt0055.resp;

import cn.com.yusys.yusp.dto.server.cmislmt0051.resp.LmtDiscOrgDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0055RespDto implements Serializable {

    private static final long serialVersionUID = 4257512024639182666L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息
    @JsonProperty(value = "lmtDtoList")
    private List<LmtDto> lmtDtoList;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<LmtDto> getLmtDtoList() {
        return lmtDtoList;
    }

    public void setLmtDtoList(List<LmtDto> lmtDtoList) {
        this.lmtDtoList = lmtDtoList;
    }

    @Override
    public String toString() {
        return "CmisLmt0055RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", lmtDtoList=" + lmtDtoList +
                '}';
    }
}
