package cn.com.yusys.yusp.dto.server.cmislmt0010.resp;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * 响应Dto：合同校验接口
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0010RespDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        return "Cmislmt0010RespDto{" +
                "errorCode='" + errorCode + '\'' +
                "errorMsg='" + errorMsg + '\'' +
                '}';
    }
}
