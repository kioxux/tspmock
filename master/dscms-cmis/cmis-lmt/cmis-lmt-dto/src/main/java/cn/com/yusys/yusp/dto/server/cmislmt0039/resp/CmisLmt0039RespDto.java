package cn.com.yusys.yusp.dto.server.cmislmt0039.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class CmisLmt0039RespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息
    @JsonProperty(value = "sumAccSpacBalanceAmtCny")
    private BigDecimal sumAccSpacBalanceAmtCny;//总用信敞口余额
    @JsonProperty(value = "sumLoanTotalCny")
    private BigDecimal sumLoanTotalCny;//用信总额
    @JsonProperty(value = "outstndAmt")
    private BigDecimal outstndAmt;//已用额度

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public BigDecimal getSumAccSpacBalanceAmtCny() {
        return sumAccSpacBalanceAmtCny;
    }

    public void setSumAccSpacBalanceAmtCny(BigDecimal sumAccSpacBalanceAmtCny) {
        this.sumAccSpacBalanceAmtCny = sumAccSpacBalanceAmtCny;
    }

    public BigDecimal getSumLoanTotalCny() {
        return sumLoanTotalCny;
    }

    public void setSumLoanTotalCny(BigDecimal sumLoanTotalCny) {
        this.sumLoanTotalCny = sumLoanTotalCny;
    }

    public BigDecimal getOutstndAmt() {
        return outstndAmt;
    }

    public void setOutstndAmt(BigDecimal outstndAmt) {
        this.outstndAmt = outstndAmt;
    }

    @Override
    public String toString() {
        return "CmisLmt0039RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", sumAccSpacBalanceAmtCny=" + sumAccSpacBalanceAmtCny +
                ", sumLoanTotalCny=" + sumLoanTotalCny +
                ", outstndAmt=" + outstndAmt +
                '}';
    }
}
