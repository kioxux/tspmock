package cn.com.yusys.yusp.dto.server.cmislmt0040.req;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CmisLmt0040MemberListReqDto {
    /**
     * 集团成员编号
     */
    @JsonProperty(value = "grpMemberNo")
    private String grpMemberNo;

    /**
     * 集团成员客户经理
     */
    @JsonProperty(value = "managerId")
    private String managerId;

    /**
     * 主管机构
     */
    @JsonProperty(value = "managerBrId")
    private String managerBrId;

    /**
     * 本次变更类型
     */
    @JsonProperty(value = "cusModifyType")
    private String cusModifyType;

    /**
     * 是否保留额度
     */
    @JsonProperty(value = "isRetainLmt")
    private String isRetainLmt;

    /**
     * 客户大类
     */
    @JsonProperty(value = "cusCatalog")
    private String cusCatalog;

    public String getGrpMemberNo() {
        return grpMemberNo;
    }

    public void setGrpMemberNo(String grpMemberNo) {
        this.grpMemberNo = grpMemberNo;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getCusModifyType() {
        return cusModifyType;
    }

    public void setCusModifyType(String cusModifyType) {
        this.cusModifyType = cusModifyType;
    }

    public String getIsRetainLmt() {
        return isRetainLmt;
    }

    public void setIsRetainLmt(String isRetainLmt) {
        this.isRetainLmt = isRetainLmt;
    }

    public String getCusCatalog() {
        return cusCatalog;
    }

    public void setCusCatalog(String cusCatalog) {
        this.cusCatalog = cusCatalog;
    }

    @Override
    public String toString() {
        return "CmisLmt0040MemberListReqDto{" +
                "grpMemberNo='" + grpMemberNo + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", cusModifyType='" + cusModifyType + '\'' +
                ", isRetainLmt='" + isRetainLmt + '\'' +
                ", cusCatalog='" + cusCatalog + '\'' +
                '}';
    }
}
