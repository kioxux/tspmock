package cn.com.yusys.yusp.dto.server.cmislmt0007.req;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CmisLmt0007ApprSubListReqDto {

    /**
     *客户编号
     */
    @JsonProperty(value = "cusId")
    private String cusId;

    /**
     *分项编号
     */
    @JsonProperty(value = "apprSubSerno")
    private String apprSubSerno;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getApprSubSerno() {
        return apprSubSerno;
    }

    public void setApprSubSerno(String apprSubSerno) {
        this.apprSubSerno = apprSubSerno;
    }

    @Override
    public String toString() {
        return "CmisLmt0007ApprSubListReqDto{" +
                "cusId='" + cusId + '\'' +
                ", apprSubSerno='" + apprSubSerno + '\'' +
                '}';
    }
}
