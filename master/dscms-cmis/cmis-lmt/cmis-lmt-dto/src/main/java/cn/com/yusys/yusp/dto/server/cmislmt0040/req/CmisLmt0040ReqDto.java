package cn.com.yusys.yusp.dto.server.cmislmt0040.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CmisLmt0040ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "serno")
    private String serno;//交易流水号

    @JsonProperty(value = "grpAppType")
    private String grpAppType;//集团申请类型

    @JsonProperty(value = "grpNo")
    private String grpNo;//集团客户编号

    @JsonProperty(value = "grpName")
    private String grpName;//集团客户名称

    @JsonProperty(value = "managerId")
    private String managerId;//集团主管客户经理

    @JsonProperty(value = "managerBrId")
    private String managerBrId;//主管机构

    private List<CmisLmt0040MemberListReqDto> memberList;//集团客户列表

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getGrpAppType() {
        return grpAppType;
    }

    public void setGrpAppType(String grpAppType) {
        this.grpAppType = grpAppType;
    }

    public String getGrpNo() {
        return grpNo;
    }

    public void setGrpNo(String grpNo) {
        this.grpNo = grpNo;
    }

    public String getGrpName() {
        return grpName;
    }

    public void setGrpName(String grpName) {
        this.grpName = grpName;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public List<CmisLmt0040MemberListReqDto> getMemberList() {
        return memberList;
    }

    public void setMemberList(List<CmisLmt0040MemberListReqDto> memberList) {
        this.memberList = memberList;
    }

    @Override
    public String toString() {
        return "CmisLmt0040ReqDto{" +
                "serno='" + serno + '\'' +
                ", grpAppType='" + grpAppType + '\'' +
                ", grpNo='" + grpNo + '\'' +
                ", grpName='" + grpName + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", memberList=" + memberList +
                '}';
    }
}
