package cn.com.yusys.yusp.dto.server.cmislmt0052.resp;

import cn.com.yusys.yusp.dto.server.cmislmt0051.resp.LmtDiscOrgDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0052RespDto implements Serializable {

    private static final long serialVersionUID = 4257512024639182666L;
    @JsonProperty(value = "errorCode")
    private String errorCode;
    @JsonProperty(value = "errorMsg")
    private String errorMsg;
    @JsonProperty(value = "spcaBalanceAmt")
    private BigDecimal spcaBalanceAmt;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public BigDecimal getSpcaBalanceAmt() {
        return spcaBalanceAmt;
    }

    public void setSpcaBalanceAmt(BigDecimal spcaBalanceAmt) {
        this.spcaBalanceAmt = spcaBalanceAmt;
    }

    @Override
    public String toString() {
        return "CmisLmt0052RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", spcaBalanceAmt=" + spcaBalanceAmt +
                '}';
    }
}
