package cn.com.yusys.yusp.dto.server.cmislmt0050.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0050ReqDto implements Serializable {
    private static final long serialVersionUID = -3305734318359217L;

    /**
     * 条线部门
     */
    @JsonProperty(value = "belgLine",required = true)
    private String belgLine;

    /**
     * 区域
     */
    @JsonProperty(value = "orgAreaType",required = true)
    private String orgAreaType;


    public String getBelgLine() {
        return belgLine;
    }

    public void setBelgLine(String belgLine) {
        this.belgLine = belgLine;
    }

    public String getOrgAreaType() {
        return orgAreaType;
    }

    public void setOrgAreaType(String orgAreaType) {
        this.orgAreaType = orgAreaType;
    }

    @Override
    public String toString() {
        return "CmisLmt0050ReqDto{" +
                "belgLine='" + belgLine + '\'' +
                ", orgAreaType='" + orgAreaType + '\'' +
                '}';
    }
}
