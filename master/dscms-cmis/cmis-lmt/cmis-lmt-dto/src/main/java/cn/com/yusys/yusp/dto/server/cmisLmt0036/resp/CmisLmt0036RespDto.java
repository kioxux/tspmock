package cn.com.yusys.yusp.dto.server.cmisLmt0036.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * 国结票据出账额度校验
 * add by dumd 20210618
 */
public class CmisLmt0036RespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode;

    @JsonProperty(value = "errorMsg")
    private String errorMsg;

    @JsonProperty(value = "startDate")
    private String startDate;

    @JsonProperty(value = "endDate")
    private String endDate;

    @JsonProperty(value = "term")
    private Integer term;

    @JsonProperty(value = "bizTotalBalAmtCny")
    private BigDecimal bizTotalBalAmtCny ;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public BigDecimal getBizTotalBalAmtCny() {
        return bizTotalBalAmtCny;
    }

    public void setBizTotalBalAmtCny(BigDecimal bizTotalBalAmtCny) {
        this.bizTotalBalAmtCny = bizTotalBalAmtCny;
    }

    @Override
    public String toString() {
        return "CmisLmt0036RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", term='" + term + '\'' +
                ", bizTotalBalAmtCny='" + bizTotalBalAmtCny + '\'' +
                '}';
    }
}