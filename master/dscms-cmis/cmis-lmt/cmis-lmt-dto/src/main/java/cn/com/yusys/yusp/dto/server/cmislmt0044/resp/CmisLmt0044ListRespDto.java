package cn.com.yusys.yusp.dto.server.cmislmt0044.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Date;

public class CmisLmt0044ListRespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "serno")
    private String serno;//申请流水号
    @JsonProperty(value = "appYm")
    private String appYm;//申请年月
    @JsonProperty(value = "organno")
    private String organno;//机构号
    @JsonProperty(value = "organname")
    private String organname;//机构名称
    @JsonProperty(value = "applyAmount")
    private BigDecimal applyAmount;//申请额度
    @JsonProperty(value = "approveAmount")
    private BigDecimal approveAmount;//核准额度
    @JsonProperty(value = "useAmt")
    private BigDecimal useAmt;//已使用额度
    @JsonProperty(value = "status")
    private String status;//状态
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期
    @JsonProperty(value = "updId")
    private String updId;//更新人
    @JsonProperty(value = "updBrId")
    private String updBrId;//更新机构
    @JsonProperty(value = "updDate")
    private String updDate;//更新日期
    @JsonProperty(value = "createTime")
    private java.util.Date createTime;//创建时间
    @JsonProperty(value = "upddateTime")
    private java.util.Date upddateTime;//修改时间

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getAppYm() {
        return appYm;
    }

    public void setAppYm(String appYm) {
        this.appYm = appYm;
    }

    public String getOrganno() {
        return organno;
    }

    public void setOrganno(String organno) {
        this.organno = organno;
    }

    public String getOrganname() {
        return organname;
    }

    public void setOrganname(String organname) {
        this.organname = organname;
    }

    public BigDecimal getApplyAmount() {
        return applyAmount;
    }

    public void setApplyAmount(BigDecimal applyAmount) {
        this.applyAmount = applyAmount;
    }

    public BigDecimal getApproveAmount() {
        return approveAmount;
    }

    public void setApproveAmount(BigDecimal approveAmount) {
        this.approveAmount = approveAmount;
    }

    public BigDecimal getUseAmt() {
        return useAmt;
    }

    public void setUseAmt(BigDecimal useAmt) {
        this.useAmt = useAmt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpddateTime() {
        return upddateTime;
    }

    public void setUpddateTime(Date upddateTime) {
        this.upddateTime = upddateTime;
    }

    @Override
    public String toString() {
        return "CmisLmt0044ListRespDto{" +
                "serno='" + serno + '\'' +
                ", appYm='" + appYm + '\'' +
                ", organno='" + organno + '\'' +
                ", organname='" + organname + '\'' +
                ", applyAmount='" + applyAmount + '\'' +
                ", approveAmount='" + approveAmount + '\'' +
                ", useAmt='" + useAmt + '\'' +
                ", status='" + status + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", createTime='" + createTime + '\'' +
                ", upddateTime='" + upddateTime + '\'' +
                '}';
    }
}
