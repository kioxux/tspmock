package cn.com.yusys.yusp.dto.server.cmislmt0002.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Dto：集团客户额度同步
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0002ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "sysId")
    private String sysId;//系统编号
    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码
    @JsonProperty(value = "grpNo")
    private String grpNo;//集团客户编号
    @JsonProperty(value = "grpName")
    private String grpName;//集团客户名称
    @JsonProperty(value = "grpAccNo")
    private String grpAccNo;//批复台账编号
    @JsonProperty(value = "origiGrpAccNo")
    private String origiGrpAccNo;//原批复台账编号
    @JsonProperty(value = "isCreateAcc")
    private String isCreateAcc;//是否生成新批复台账
    @JsonProperty(value = "term")
    private Integer term;//期限
    @JsonProperty(value = "startDate")
    private String startDate;//授信起始日
    @JsonProperty(value = "endDate")
    private String endDate;//授信到期日
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期

    //集团成员额度批复列表
    private List<CmisLmt0002LmtListReqDto> lmtList;

    //授信分项列表
    private List<CmisLmt0002LmtSubListReqDto> lmSubList;

    public List<CmisLmt0002LmtListReqDto> getLmtList() {
        return lmtList;
    }

    public void setLmtList(List<CmisLmt0002LmtListReqDto> lmtList) {
        this.lmtList = lmtList;
    }

    public List<CmisLmt0002LmtSubListReqDto> getLmSubList() {
        return lmSubList;
    }

    public void setLmSubList(List<CmisLmt0002LmtSubListReqDto> lmSubList) {
        this.lmSubList = lmSubList;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getGrpNo() {
        return grpNo;
    }

    public void setGrpNo(String grpNo) {
        this.grpNo = grpNo;
    }

    public String getGrpName() {
        return grpName;
    }

    public void setGrpName(String grpName) {
        this.grpName = grpName;
    }

    public String getGrpAccNo() {
        return grpAccNo;
    }

    public void setGrpAccNo(String grpAccNo) {
        this.grpAccNo = grpAccNo;
    }

    public String getOrigiGrpAccNo() {
        return origiGrpAccNo;
    }

    public void setOrigiGrpAccNo(String origiGrpAccNo) {
        this.origiGrpAccNo = origiGrpAccNo;
    }

    public String getIsCreateAcc() {
        return isCreateAcc;
    }

    public void setIsCreateAcc(String isCreateAcc) {
        this.isCreateAcc = isCreateAcc;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    @Override
    public String toString() {
        return "Cmislmt0002ReqDto{" +
                "sysId='" + sysId + '\'' +
                "instuCde='" + instuCde + '\'' +
                "grpNo='" + grpNo + '\'' +
                "grpName='" + grpName + '\'' +
                "grpAccNo='" + grpAccNo + '\'' +
                "origiGrpAccNo='" + origiGrpAccNo + '\'' +
                "isCreateAcc='" + isCreateAcc + '\'' +
                "term='" + term + '\'' +
                "startDate='" + startDate + '\'' +
                "endDate='" + endDate + '\'' +
                "inputId='" + inputId + '\'' +
                "inputBrId='" + inputBrId + '\'' +
                "inputDate='" + inputDate + '\'' +
                '}';
    }
}

