package cn.com.yusys.yusp.dto.server.cmislmt0045.req;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CmisLmt0045ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dealBizNo")           //交易业务编号
    private String dealBizNo;

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    @Override
    public String toString() {
        return "CmisLmt0045ReqDto{" +
                "dealBizNo='" + dealBizNo + '\'' +
                '}';
    }
}
