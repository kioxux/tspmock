package cn.com.yusys.yusp.dto.server.cmislmt0055.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0055ReqDto implements Serializable {
    private static final long serialVersionUID = 5461506169208969818L;

    @JsonProperty(value = "cusId")
    private String cusId;// 客户编号

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "CmisLmt0055ReqDto{" +
                "cusId='" + cusId + '\'' +
                '}';
    }
}
