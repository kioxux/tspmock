package cn.com.yusys.yusp.dto.server.cmislmt0043.req.req;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CmisLmt0043ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "instuCde")           //金融机构代码
    private String instuCde;

    @JsonProperty(value = "cusId")           //客户号码
    private String cusId;

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }


    @Override
    public String toString() {
        return "CmisLmt0043ReqDto{" +
                "instuCde='" + instuCde + '\'' +
                ", cusId='" + cusId + '\'' +
                '}';
    }
}
