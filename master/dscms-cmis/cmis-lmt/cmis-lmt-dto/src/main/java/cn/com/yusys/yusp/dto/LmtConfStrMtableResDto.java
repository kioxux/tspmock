package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtStrMtableConf
 * @类描述: lmt_str_mtable_conf数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-14 18:51:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtConfStrMtableResDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** PKID **/
	private String pkId;
	
	/** 额度结构编号 **/
	private String limitStrNo;
	
	/** 额度结构名称 **/
	private String limitStrName;
	
	/** 版本号 **/
	private String version;
	
	/** 版本控制方式 **/
	private String verCtrlMod;
	
	/** 适用机构类型 **/
	private String suitOrgType;
	
	/** 适用机构 **/
	private String suitOrg;
	
	/** 适用其他法人机构 **/
	private String suitOtherOrg;
	
	/** 适用业务条线 **/
	private String suitApplicableLine;
	
	/** 是否公开授信 **/
	private String isOpenLmt;
	
	/** 是否唯有有效 **/
	private String isVld;
	
	/** 是否覆盖类额度 **/
	private String isCoverAmt;
	
	/** 是否启用 **/
	private String isBegin;
	
	/** 额度管控类型 **/
	private String limitControlType;
	
	/** 额度测算规则编号 **/
	private String limitMeasureRuleNo;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 更新人 **/
	private String updId;
	
	/** 更新机构 **/
	private String updBrId;
	
	/** 更新日期 **/
	private String updDate;
	
	/** 金融机构代码 **/
	private String instuCde;
	
	/** 父节点 **/
	private String parentId;
	
	/** 当前节点 **/
	private String childId;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 序号 **/
	private String seqNo;

	/** 产品映射  **/
	private List<LmtSubPrdMappConfDto> prdMappList;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param limitStrNo
	 */
	public void setLimitStrNo(String limitStrNo) {
		this.limitStrNo = limitStrNo == null ? null : limitStrNo.trim();
	}
	
    /**
     * @return LimitStrNo
     */	
	public String getLimitStrNo() {
		return this.limitStrNo;
	}
	
	/**
	 * @param limitStrName
	 */
	public void setLimitStrName(String limitStrName) {
		this.limitStrName = limitStrName == null ? null : limitStrName.trim();
	}
	
    /**
     * @return LimitStrName
     */	
	public String getLimitStrName() {
		return this.limitStrName;
	}
	
	/**
	 * @param version
	 */
	public void setVersion(String version) {
		this.version = version == null ? null : version.trim();
	}
	
    /**
     * @return Version
     */	
	public String getVersion() {
		return this.version;
	}
	
	/**
	 * @param verCtrlMod
	 */
	public void setVerCtrlMod(String verCtrlMod) {
		this.verCtrlMod = verCtrlMod == null ? null : verCtrlMod.trim();
	}
	
    /**
     * @return VerCtrlMod
     */	
	public String getVerCtrlMod() {
		return this.verCtrlMod;
	}
	
	/**
	 * @param suitOrgType
	 */
	public void setSuitOrgType(String suitOrgType) {
		this.suitOrgType = suitOrgType == null ? null : suitOrgType.trim();
	}
	
    /**
     * @return SuitOrgType
     */	
	public String getSuitOrgType() {
		return this.suitOrgType;
	}
	
	/**
	 * @param suitOrg
	 */
	public void setSuitOrg(String suitOrg) {
		this.suitOrg = suitOrg == null ? null : suitOrg.trim();
	}
	
    /**
     * @return SuitOrg
     */	
	public String getSuitOrg() {
		return this.suitOrg;
	}
	
	/**
	 * @param suitOtherOrg
	 */
	public void setSuitOtherOrg(String suitOtherOrg) {
		this.suitOtherOrg = suitOtherOrg == null ? null : suitOtherOrg.trim();
	}
	
    /**
     * @return SuitOtherOrg
     */	
	public String getSuitOtherOrg() {
		return this.suitOtherOrg;
	}
	
	/**
	 * @param suitApplicableLine
	 */
	public void setSuitApplicableLine(String suitApplicableLine) {
		this.suitApplicableLine = suitApplicableLine == null ? null : suitApplicableLine.trim();
	}
	
    /**
     * @return SuitApplicableLine
     */	
	public String getSuitApplicableLine() {
		return this.suitApplicableLine;
	}
	
	/**
	 * @param isOpenLmt
	 */
	public void setIsOpenLmt(String isOpenLmt) {
		this.isOpenLmt = isOpenLmt == null ? null : isOpenLmt.trim();
	}
	
    /**
     * @return IsOpenLmt
     */	
	public String getIsOpenLmt() {
		return this.isOpenLmt;
	}
	
	/**
	 * @param isVld
	 */
	public void setIsVld(String isVld) {
		this.isVld = isVld == null ? null : isVld.trim();
	}
	
    /**
     * @return IsVld
     */	
	public String getIsVld() {
		return this.isVld;
	}
	
	/**
	 * @param isCoverAmt
	 */
	public void setIsCoverAmt(String isCoverAmt) {
		this.isCoverAmt = isCoverAmt == null ? null : isCoverAmt.trim();
	}
	
    /**
     * @return IsCoverAmt
     */	
	public String getIsCoverAmt() {
		return this.isCoverAmt;
	}
	
	/**
	 * @param isBegin
	 */
	public void setIsBegin(String isBegin) {
		this.isBegin = isBegin == null ? null : isBegin.trim();
	}
	
    /**
     * @return IsBegin
     */	
	public String getIsBegin() {
		return this.isBegin;
	}
	
	/**
	 * @param limitControlType
	 */
	public void setLimitControlType(String limitControlType) {
		this.limitControlType = limitControlType == null ? null : limitControlType.trim();
	}
	
    /**
     * @return LimitControlType
     */	
	public String getLimitControlType() {
		return this.limitControlType;
	}
	
	/**
	 * @param limitMeasureRuleNo
	 */
	public void setLimitMeasureRuleNo(String limitMeasureRuleNo) {
		this.limitMeasureRuleNo = limitMeasureRuleNo == null ? null : limitMeasureRuleNo.trim();
	}
	
    /**
     * @return LimitMeasureRuleNo
     */	
	public String getLimitMeasureRuleNo() {
		return this.limitMeasureRuleNo;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param instuCde
	 */
	public void setInstuCde(String instuCde) {
		this.instuCde = instuCde == null ? null : instuCde.trim();
	}
	
    /**
     * @return InstuCde
     */	
	public String getInstuCde() {
		return this.instuCde;
	}
	
	/**
	 * @param parentId
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId == null ? null : parentId.trim();
	}
	
    /**
     * @return ParentId
     */	
	public String getParentId() {
		return this.parentId;
	}
	
	/**
	 * @param childId
	 */
	public void setChildId(String childId) {
		this.childId = childId == null ? null : childId.trim();
	}
	
    /**
     * @return ChildId
     */	
	public String getChildId() {
		return this.childId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param seqNo
	 */
	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo == null ? null : seqNo.trim();
	}
	
    /**
     * @return SeqNo
     */	
	public String getSeqNo() {
		return this.seqNo;
	}


	public List<LmtSubPrdMappConfDto> getPrdMappList() {
		return prdMappList;
	}

	public void setPrdMappList(List<LmtSubPrdMappConfDto> prdMappList) {
		this.prdMappList = prdMappList;
	}


}