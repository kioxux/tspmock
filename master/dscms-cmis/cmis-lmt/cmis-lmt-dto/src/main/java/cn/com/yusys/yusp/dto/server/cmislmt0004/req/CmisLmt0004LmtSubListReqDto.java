package cn.com.yusys.yusp.dto.server.cmislmt0004.req;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0004LmtSubListReqDto {

    /**
     * 授信分项编号
     */
    @JsonProperty(value = "accSubNo")
    private String accSubNo;

    /**
     * 父节点
     */
    @JsonProperty(value = "parentId")
    private String parentId;

    /**
     * 授信品种编号
     */
    @JsonProperty(value = "limitSubNo")
    private String limitSubNo;

    /**
     * 授信品种名称
     */
    @JsonProperty(value = "limitSubName")
    private String limitSubName;

    /**
     * 原授信分项编号
     */
    @JsonProperty(value = "origiAccSubNo")
    private String origiAccSubNo;
    /**
     * 授信金额
     */
    @JsonProperty(value = "avlAmt")
    private BigDecimal avlAmt;
    /**
     * 单户限额
     */
    @JsonProperty(value = "sigAmt")
    private BigDecimal sigAmt;
    /**
     * 单笔业务限额
     */
    @JsonProperty(value = "sigBussAmt")
    private BigDecimal sigBussAmt;

    /**
     * 币种
     */
    @JsonProperty(value = "curType")
    private String curType;

    /**
     * 授信期限
     */
    @JsonProperty(value = "term")
    private Integer term;

    /**
     * 额度起始日
     */
    @JsonProperty(value = "startDate")
    private String startDate;

    /**
     * 额度到期日
     */
    @JsonProperty(value = "endDate")
    private String endDate;


    /**
     * 是否可循环
     */
    @JsonProperty(value = "isRevolv")
    private String isRevolv;

    /**
     * 操作类型
     */
    @JsonProperty(value = "oprType")
    private String oprType;

    /** 授信品种类型属性 **/
    @JsonProperty(value = "lmtBizTypeProp")
    private String lmtBizTypeProp;

    public String getAccSubNo() {
        return accSubNo;
    }

    public void setAccSubNo(String accSubNo) {
        this.accSubNo = accSubNo;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    public String getLimitSubName() {
        return limitSubName;
    }

    public void setLimitSubName(String limitSubName) {
        this.limitSubName = limitSubName;
    }

    public BigDecimal getAvlAmt() {
        return avlAmt;
    }

    public void setAvlAmt(BigDecimal avlAmt) {
        this.avlAmt = avlAmt;
    }

    public BigDecimal getSigAmt() { return sigAmt; }

    public void setSigAmt(BigDecimal sigAmt) { this.sigAmt = sigAmt; }

    public BigDecimal getSigBussAmt() { return sigBussAmt; }

    public void setSigBussAmt(BigDecimal sigBussAmt) { this.sigBussAmt = sigBussAmt; }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getIsRevolv() {
        return isRevolv;
    }

    public void setIsRevolv(String isRevolv) {
        this.isRevolv = isRevolv;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getOrigiAccSubNo() {
        return origiAccSubNo;
    }

    public void setOrigiAccSubNo(String origiAccSubNo) {
        this.origiAccSubNo = origiAccSubNo;
    }

    public String getLmtBizTypeProp() {
        return lmtBizTypeProp;
    }

    public void setLmtBizTypeProp(String lmtBizTypeProp) {
        this.lmtBizTypeProp = lmtBizTypeProp;
    }

    @Override
    public String toString() {
        return "CmisLmt0004LmtSubListReqDto{" +
                "accSubNo='" + accSubNo + '\'' +
                ", parentId='" + parentId + '\'' +
                ", limitSubNo='" + limitSubNo + '\'' +
                ", limitSubName='" + limitSubName + '\'' +
                ", origiAccSubNo='" + origiAccSubNo + '\'' +
                ", avlAmt=" + avlAmt +
                ", sigAmt=" + sigAmt +
                ", sigBussAmt=" + sigBussAmt +
                ", curType='" + curType + '\'' +
                ", term=" + term +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", isRevolv='" + isRevolv + '\'' +
                ", oprType='" + oprType + '\'' +
                ", lmtBizTypeProp='" + lmtBizTypeProp + '\'' +
                '}';
    }
}
