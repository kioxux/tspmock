package cn.com.yusys.yusp.dto.server.cmislmt0018.req;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * 请求Dto：额度结构查询
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0018ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//交易流水号
    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码
    @JsonProperty(value = "suitApplicableLine")
    private String suitApplicableLine;//适用业务条线
    @JsonProperty(value = "strType")
    private String strType;//结构主体类型

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getSuitApplicableLine() {
        return suitApplicableLine;
    }

    public void setSuitApplicableLine(String suitApplicableLine) {
        this.suitApplicableLine = suitApplicableLine;
    }

    public String getStrType() {
        return strType;
    }

    public void setStrType(String strType) {
        this.strType = strType;
    }

    @Override
    public String toString() {
        return "Cmislmt0018ReqDto{" +
                "serno='" + serno + '\'' +
                "instuCde='" + instuCde + '\'' +
                "suitApplicableLine='" + suitApplicableLine + '\'' +
                "strType='" + strType + '\'' +
                '}';
    }

}
