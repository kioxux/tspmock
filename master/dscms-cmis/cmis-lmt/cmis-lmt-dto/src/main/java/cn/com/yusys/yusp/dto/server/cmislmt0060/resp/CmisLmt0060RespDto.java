package cn.com.yusys.yusp.dto.server.cmislmt0060.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0060RespDto implements Serializable {

    private static final long serialVersionUID = 4257512024639182666L;
    //结果代码
    @JsonProperty(value = "errorCode")
    private String errorCode;

    //结果信息
    @JsonProperty(value = "errorMsg")
    private String errorMsg;

    //客户用信总额
    @JsonProperty(value = "totalCny")
    private BigDecimal totalCny;

    //客户用信余额
    @JsonProperty(value = "spacCny")
    private BigDecimal spacCny;

    //客户用信总额
    @JsonProperty(value = "totalBalanceCny")
    private BigDecimal totalBalanceCny;

    //明细用信余额
    @JsonProperty(value = "spacBalanceCny")
    private BigDecimal spacBalanceCny;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public BigDecimal getTotalCny() {
        return totalCny;
    }

    public void setTotalCny(BigDecimal totalCny) {
        this.totalCny = totalCny;
    }

    public BigDecimal getSpacCny() {
        return spacCny;
    }

    public void setSpacCny(BigDecimal spacCny) {
        this.spacCny = spacCny;
    }

    public BigDecimal getTotalBalanceCny() {
        return totalBalanceCny;
    }

    public void setTotalBalanceCny(BigDecimal totalBalanceCny) {
        this.totalBalanceCny = totalBalanceCny;
    }

    public BigDecimal getSpacBalanceCny() {
        return spacBalanceCny;
    }

    public void setSpacBalanceCny(BigDecimal spacBalanceCny) {
        this.spacBalanceCny = spacBalanceCny;
    }

    @Override
    public String toString() {
        return "CmisLmt0060RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", totalCny=" + totalCny +
                ", spacCny=" + spacCny +
                ", totalBalanceCny=" + totalBalanceCny +
                ", spacBalanceCny=" + spacBalanceCny +
                '}';
    }
}
