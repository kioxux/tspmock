package cn.com.yusys.yusp.dto.server.cmislmt0056.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0056RespDto implements Serializable {

    private static final long serialVersionUID = 4257512024639182666L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    //分项明细编号
    @JsonProperty(value = "apprSubSerno")
    private String apprSubSerno;

    //担保方式
    @JsonProperty(value = "suitGuarWay")
    private String suitGuarWay;

    //产品类型属性
    @JsonProperty(value = "lmtBizTypeProp")
    private String lmtBizTypeProp;

    //分项编号
    @JsonProperty(value = "subSerno")
    private String subSerno;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getApprSubSerno() {
        return apprSubSerno;
    }

    public void setApprSubSerno(String apprSubSerno) {
        this.apprSubSerno = apprSubSerno;
    }

    public String getSuitGuarWay() {
        return suitGuarWay;
    }

    public void setSuitGuarWay(String suitGuarWay) {
        this.suitGuarWay = suitGuarWay;
    }

    public String getLmtBizTypeProp() {
        return lmtBizTypeProp;
    }

    public void setLmtBizTypeProp(String lmtBizTypeProp) {
        this.lmtBizTypeProp = lmtBizTypeProp;
    }

    public String getSubSerno() {
        return subSerno;
    }

    public void setSubSerno(String subSerno) {
        this.subSerno = subSerno;
    }

    @Override
    public String toString() {
        return "CmisLmt0056RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", apprSubSerno='" + apprSubSerno + '\'' +
                ", suitGuarWay='" + suitGuarWay + '\'' +
                ", lmtBizTypeProp='" + lmtBizTypeProp + '\'' +
                ", subSerno='" + subSerno + '\'' +
                '}';
    }
}
