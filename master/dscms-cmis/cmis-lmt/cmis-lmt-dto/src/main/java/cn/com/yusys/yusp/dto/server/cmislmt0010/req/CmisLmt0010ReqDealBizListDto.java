package cn.com.yusys.yusp.dto.server.cmislmt0010.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

/**
 * 请求Dto：合同校验接口
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0010ReqDealBizListDto {
    private static final long serialVersionUID = 1L;
    //系统编号
    @JsonProperty(value = "dealBizNo")
    private String dealBizNo;

    //客户编号
    @JsonProperty(value = "cusId")
    private String cusId;

    //客户名称
    @JsonProperty(value = "cusName")
    private String cusName;

    //产品编号
    @JsonProperty(value = "prdId")
    private String prdId;

    //产品名称
    @JsonProperty(value = "prdName")
    private String prdName;

    //台账占用总额
    @JsonProperty(value = "dealBizAmt")
    private BigDecimal dealBizAmt;

    //台账占用敞口
    @JsonProperty(value = "dealBizSpacAmt")
    private BigDecimal dealBizSpacAmt;

    //保证金比例
    @JsonProperty(value = "dealBizBailPreRate")
    private BigDecimal dealBizBailPreRate;

    //保证金金额
    @JsonProperty(value = "dealBizBailPreAmt")
    private BigDecimal dealBizBailPreAmt;

    //台账到期日
    @JsonProperty(value = "endDate")
    private String endDate;

    //是否无缝衔接
    @JsonProperty(value = "isFollowBiz")
    private String isFollowBiz;

    //原交易业务编号
    @JsonProperty(value = "origiDealBizNo")
    private String origiDealBizNo;

    //原交易业务恢复类型
    @JsonProperty(value = "origiRecoverType")
    private String origiRecoverType;

    //原交易业务状态
    @JsonProperty(value = "origiDealBizStatus")
    private String origiDealBizStatus;

    //原交易属性
    @JsonProperty(value = "origiBizAttr")
    private String origiBizAttr;

    //授信品种类型
    @JsonProperty(value = "prdTypeProp")
    private String prdTypeProp;

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public BigDecimal getDealBizAmt() {
        return dealBizAmt;
    }

    public void setDealBizAmt(BigDecimal dealBizAmt) {
        this.dealBizAmt = dealBizAmt;
    }

    public BigDecimal getDealBizSpacAmt() {
        return dealBizSpacAmt;
    }

    public void setDealBizSpacAmt(BigDecimal dealBizSpacAmt) {
        this.dealBizSpacAmt = dealBizSpacAmt;
    }

    public BigDecimal getDealBizBailPreRate() {
        return dealBizBailPreRate;
    }

    public void setDealBizBailPreRate(BigDecimal dealBizBailPreRate) {
        this.dealBizBailPreRate = dealBizBailPreRate;
    }

    public BigDecimal getDealBizBailPreAmt() {
        return dealBizBailPreAmt;
    }

    public void setDealBizBailPreAmt(BigDecimal dealBizBailPreAmt) {
        this.dealBizBailPreAmt = dealBizBailPreAmt;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getIsFollowBiz() {
        return isFollowBiz;
    }

    public void setIsFollowBiz(String isFollowBiz) {
        this.isFollowBiz = isFollowBiz;
    }

    public String getOrigiDealBizNo() {
        return origiDealBizNo;
    }

    public void setOrigiDealBizNo(String origiDealBizNo) {
        this.origiDealBizNo = origiDealBizNo;
    }

    public String getOrigiRecoverType() {
        return origiRecoverType;
    }

    public void setOrigiRecoverType(String origiRecoverType) {
        this.origiRecoverType = origiRecoverType;
    }

    public String getOrigiDealBizStatus() {
        return origiDealBizStatus;
    }

    public void setOrigiDealBizStatus(String origiDealBizStatus) {
        this.origiDealBizStatus = origiDealBizStatus;
    }

    public String getOrigiBizAttr() {
        return origiBizAttr;
    }

    public void setOrigiBizAttr(String origiBizAttr) {
        this.origiBizAttr = origiBizAttr;
    }

    public String getPrdTypeProp() {
        return prdTypeProp;
    }

    public void setPrdTypeProp(String prdTypeProp) {
        this.prdTypeProp = prdTypeProp;
    }

    @Override
    public String toString() {
        return "CmisLmt0010ReqDealBizListDto{" +
                "dealBizNo='" + dealBizNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", prdId='" + prdId + '\'' +
                ", prdName='" + prdName + '\'' +
                ", dealBizAmt=" + dealBizAmt +
                ", dealBizSpacAmt=" + dealBizSpacAmt +
                ", dealBizBailPreRate=" + dealBizBailPreRate +
                ", dealBizBailPreAmt=" + dealBizBailPreAmt +
                ", endDate='" + endDate + '\'' +
                ", isFollowBiz='" + isFollowBiz + '\'' +
                ", origiDealBizNo='" + origiDealBizNo + '\'' +
                ", origiRecoverType='" + origiRecoverType + '\'' +
                ", origiDealBizStatus='" + origiDealBizStatus + '\'' +
                ", origiBizAttr='" + origiBizAttr + '\'' +
                ", prdTypeProp='" + prdTypeProp + '\'' +
                '}';
    }
}
