package cn.com.yusys.yusp.dto.server.cmisLmt0032.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * 查询客户授信余额（不包含穿透化额度）
 * add by dumd 20210617
 */
public class CmisLmt0032RespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode;
    @JsonProperty(value = "errorMsg")
    private String errorMsg;
    @JsonProperty(value = "lmtBalanceAmt")
    private BigDecimal lmtBalanceAmt;
    @JsonProperty(value = "lmtAmt")
    private BigDecimal lmtAmt;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public BigDecimal getLmtBalanceAmt() {
        return lmtBalanceAmt;
    }

    public void setLmtBalanceAmt(BigDecimal lmtBalanceAmt) {
        this.lmtBalanceAmt = lmtBalanceAmt;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    @Override
    public String toString() {
        return "CmisLmt0032RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", lmtBalanceAmt=" + lmtBalanceAmt +
                ", lmtAmt=" + lmtAmt +
                '}';
    }
}