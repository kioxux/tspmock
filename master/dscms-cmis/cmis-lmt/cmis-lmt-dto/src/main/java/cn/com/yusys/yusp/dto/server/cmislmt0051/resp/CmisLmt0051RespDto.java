package cn.com.yusys.yusp.dto.server.cmislmt0051.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0051RespDto implements Serializable {

    private static final long serialVersionUID = 4257512024639182666L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息
    @JsonProperty(value = "lmtDiscOrgDtoList")
    private List<LmtDiscOrgDto> lmtDiscOrgDtoList;

    public List<LmtDiscOrgDto> getLmtDiscOrgDtoList() {
        return lmtDiscOrgDtoList;
    }

    public void setLmtDiscOrgDtoList(List<LmtDiscOrgDto> lmtDiscOrgDtoList) {
        this.lmtDiscOrgDtoList = lmtDiscOrgDtoList;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        return "CmisLmt0051RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", lmtDiscOrgDtoList=" + lmtDiscOrgDtoList +
                '}';
    }
}
