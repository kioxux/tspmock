package cn.com.yusys.yusp.dto.server.cmislmt0011.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class CmisLmt0011OccRelListDto {
    private static final long serialVersionUID = 1L;
    //额度类型
    @JsonProperty(value = "lmtType")
    private String lmtType;

    //额度分项编号
    @JsonProperty(value = "lmtSubNo")
    private String lmtSubNo;

    //占用总额(折人民币)
    @JsonProperty(value = "bizTotalAmtCny")
    private BigDecimal bizTotalAmtCny;

    //占用敞口(折人民币)
    @JsonProperty(value = "bizSpacAmtCny")
    private BigDecimal bizSpacAmtCny;

    //占用总额(原币)
    @JsonProperty(value = "bizTotalAmt")
    private BigDecimal bizTotalAmt;

    //占用敞口(原币)
    @JsonProperty(value = "bizSpacAmt")
    private BigDecimal bizSpacAmt;

    //是否专业担保公司
    @JsonProperty(value = "isProGuarCom")
    private String isProGuarCom;

    //专业担保公司客户号
    @JsonProperty(value = "guarCusid")
    private String guarCusid;

    /** 用信品种类型属性 **/
    @JsonProperty(value = "prdTypeProp")
    private String prdTypeProp;

    public String getLmtType() {
        return lmtType;
    }

    public void setLmtType(String lmtType) {
        this.lmtType = lmtType;
    }

    public String getLmtSubNo() {
        return lmtSubNo;
    }

    public void setLmtSubNo(String lmtSubNo) {
        this.lmtSubNo = lmtSubNo;
    }

    public BigDecimal getBizTotalAmtCny() {
        return bizTotalAmtCny;
    }

    public void setBizTotalAmtCny(BigDecimal bizTotalAmtCny) {
        this.bizTotalAmtCny = bizTotalAmtCny;
    }

    public BigDecimal getBizSpacAmtCny() {
        return bizSpacAmtCny;
    }

    public void setBizSpacAmtCny(BigDecimal bizSpacAmtCny) {
        this.bizSpacAmtCny = bizSpacAmtCny;
    }

    public BigDecimal getBizTotalAmt() {
        return bizTotalAmt;
    }

    public void setBizTotalAmt(BigDecimal bizTotalAmt) {
        this.bizTotalAmt = bizTotalAmt;
    }

    public BigDecimal getBizSpacAmt() {
        return bizSpacAmt;
    }

    public void setBizSpacAmt(BigDecimal bizSpacAmt) {
        this.bizSpacAmt = bizSpacAmt;
    }

    public String getIsProGuarCom() {
        return isProGuarCom;
    }

    public void setIsProGuarCom(String isProGuarCom) {
        this.isProGuarCom = isProGuarCom;
    }

    public String getGuarCusid() {
        return guarCusid;
    }

    public void setGuarCusid(String guarCusid) {
        this.guarCusid = guarCusid;
    }

    public String getPrdTypeProp() {
        return prdTypeProp;
    }

    public void setPrdTypeProp(String prdTypeProp) {
        this.prdTypeProp = prdTypeProp;
    }

    @Override
    public String toString() {
        return "CmisLmt0011OccRelListDto{" +
                "lmtType='" + lmtType + '\'' +
                ", lmtSubNo='" + lmtSubNo + '\'' +
                ", bizTotalAmtCny=" + bizTotalAmtCny +
                ", bizSpacAmtCny=" + bizSpacAmtCny +
                ", bizTotalAmt=" + bizTotalAmt +
                ", bizSpacAmt=" + bizSpacAmt +
                ", isProGuarCom='" + isProGuarCom + '\'' +
                ", guarCusid='" + guarCusid + '\'' +
                ", prdTypeProp='" + prdTypeProp + '\'' +
                '}';
    }
}
