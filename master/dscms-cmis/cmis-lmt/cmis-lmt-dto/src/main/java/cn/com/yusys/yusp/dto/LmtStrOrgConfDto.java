package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtStrOrgConf
 * @类描述: lmt_str_org_conf数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-15 13:44:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtStrOrgConfDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** FK_PKID **/
	private String fkPkid;
	
	/** 上级机构编号 **/
	private String bchSupCde;
	
	/** 版本号 **/
	private String version;
	
	/** 机构名称 **/
	private String bchDesc;
	
	/** 机构编号 **/
	private String bchCde;
	
	/** 金融机构代码 **/
	private String instuCde;
	
	/** 金融机构名称 **/
	private String instuName;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param fkPkid
	 */
	public void setFkPkid(String fkPkid) {
		this.fkPkid = fkPkid == null ? null : fkPkid.trim();
	}
	
    /**
     * @return FkPkid
     */	
	public String getFkPkid() {
		return this.fkPkid;
	}
	
	/**
	 * @param bchSupCde
	 */
	public void setBchSupCde(String bchSupCde) {
		this.bchSupCde = bchSupCde == null ? null : bchSupCde.trim();
	}
	
    /**
     * @return BchSupCde
     */	
	public String getBchSupCde() {
		return this.bchSupCde;
	}
	
	/**
	 * @param version
	 */
	public void setVersion(String version) {
		this.version = version == null ? null : version.trim();
	}
	
    /**
     * @return Version
     */	
	public String getVersion() {
		return this.version;
	}
	
	/**
	 * @param bchDesc
	 */
	public void setBchDesc(String bchDesc) {
		this.bchDesc = bchDesc == null ? null : bchDesc.trim();
	}
	
    /**
     * @return BchDesc
     */	
	public String getBchDesc() {
		return this.bchDesc;
	}
	
	/**
	 * @param bchCde
	 */
	public void setBchCde(String bchCde) {
		this.bchCde = bchCde == null ? null : bchCde.trim();
	}
	
    /**
     * @return BchCde
     */	
	public String getBchCde() {
		return this.bchCde;
	}
	
	/**
	 * @param instuCde
	 */
	public void setInstuCde(String instuCde) {
		this.instuCde = instuCde == null ? null : instuCde.trim();
	}
	
    /**
     * @return InstuCde
     */	
	public String getInstuCde() {
		return this.instuCde;
	}
	
	/**
	 * @param instuName
	 */
	public void setInstuName(String instuName) {
		this.instuName = instuName == null ? null : instuName.trim();
	}
	
    /**
     * @return InstuName
     */	
	public String getInstuName() {
		return this.instuName;
	}


}