package cn.com.yusys.yusp.dto.server.cmislmt0018.resp;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0018StrListRespDto {

    /**
     * 额度结构编号
     */
    @JsonProperty(value = "limitStrNo")
    private String limitStrNo;

    /**
     * 额度结构名称
     */
    @JsonProperty(value = "limitStrName")
    private String limitStrName;

    /**
     * 额度品种编号
     */
    @JsonProperty(value = "limitSubNo")
    private String limitSubNo;

    /**
     * 额度品种名称
     */
    @JsonProperty(value = "limitSubName")
    private String limitSubName;

    /**
     * 父节点
     */
    @JsonProperty(value = "parentId")
    private String parentId;

    /**
     * 是否低风险授信
     */
    @JsonProperty(value = "isLriskLmt")
    private String isLriskLmt;

    /**
     * 授信分项类型
     */
    @JsonProperty(value = "lmtSubType")
    private String lmtSubType;

    /**
     * 是否低风险授信
     */
    @JsonProperty(value = "isExpertLmt")
    private String isExpertLmt;


    /**
     * 额度类型
     */
    @JsonProperty(value = "limitType")
    private String limitType;

    public String getLimitStrNo() {
        return limitStrNo;
    }

    public void setLimitStrNo(String limitStrNo) {
        this.limitStrNo = limitStrNo;
    }

    public String getLimitStrName() {
        return limitStrName;
    }

    public void setLimitStrName(String limitStrName) {
        this.limitStrName = limitStrName;
    }

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    public String getLimitSubName() {
        return limitSubName;
    }

    public void setLimitSubName(String limitSubName) {
        this.limitSubName = limitSubName;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getIsLriskLmt() {
        return isLriskLmt;
    }

    public void setIsLriskLmt(String isLriskLmt) {
        this.isLriskLmt = isLriskLmt;
    }

    public String getLmtSubType() {
        return lmtSubType;
    }

    public void setLmtSubType(String lmtSubType) {
        this.lmtSubType = lmtSubType;
    }

    public String getLimitType() {
        return limitType;
    }

    public void setLimitType(String limitType) {
        this.limitType = limitType;
    }

    public String getIsExpertLmt() {
        return isExpertLmt;
    }

    public void setIsExpertLmt(String isExpertLmt) {
        this.isExpertLmt = isExpertLmt;
    }

    @Override
    public String toString() {
        return "CmisLmt0018StrListRespDto{" +
                "limitStrNo='" + limitStrNo + '\'' +
                ", limitStrName='" + limitStrName + '\'' +
                ", limitSubNo='" + limitSubNo + '\'' +
                ", limitSubName='" + limitSubName + '\'' +
                ", parentId='" + parentId + '\'' +
                ", isLriskLmt='" + isLriskLmt + '\'' +
                ", lmtSubType='" + lmtSubType + '\'' +
                ", isExpertLmt='" + isExpertLmt + '\'' +
                ", limitType='" + limitType + '\'' +
                '}';
    }
}
