package cn.com.yusys.yusp.dto.server.cmislmt0011.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：合同占用接口
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0011ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "sysId")
    private String sysId;//系统编号
    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码
    @JsonProperty(value = "dealBizNo")
    private String dealBizNo;//合同编号
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "dealBizType")
    private String dealBizType;//交易业务类型
    @JsonProperty(value = "bizAttr")
    private String bizAttr;//交易属性
    @JsonProperty(value = "prdId")
    private String prdId;//产品编号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "isLriskBiz")
    private String isLriskBiz;//是否低风险
    @JsonProperty(value = "isFollowBiz")
    private String isFollowBiz;//是否无缝衔接
    @JsonProperty(value = "isBizRev")
    private String isBizRev;//是否合同重签
    @JsonProperty(value = "origiDealBizNo")
    private String origiDealBizNo;//原交易业务编号
    @JsonProperty(value = "origiDealBizStatus")
    private String origiDealBizStatus;//原交易业务状态
    @JsonProperty(value = "dealBizAmt")
    private BigDecimal dealBizAmt;//交易业务金额
    @JsonProperty(value = "dealBizBailPreRate")
    private BigDecimal dealBizBailPreRate;//保证金比例
    @JsonProperty(value = "dealBizBailPreAmt")
    private BigDecimal dealBizBailPreAmt;//保证金金额
    @JsonProperty(value = "startDate")
    private String startDate;//合同起始日
    @JsonProperty(value = "endDate")
    private String endDate;//合同到期日
    @JsonProperty(value = "dealBizStatus")
    private String dealBizStatus;//合同状态
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期
    @JsonProperty(value = "origiRecoverType")
    private String origiRecoverType;//原交易业务恢复类型
    @JsonProperty(value = "origiBizAttr")
    private String origiBizAttr;//原交易属性
    //业务条线
    @JsonProperty(value = "belgLine")
    private String belgLine;
    //业务阶段类型
    @JsonProperty(value = "bussStageType")
    private String bussStageType;

    //占用额度列表
    @JsonProperty(value = "cmisLmt0011OccRelListDtoList")
    private List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList ;

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getDealBizType() {
        return dealBizType;
    }

    public void setDealBizType(String dealBizType) {
        this.dealBizType = dealBizType;
    }

    public String getBizAttr() {
        return bizAttr;
    }

    public void setBizAttr(String bizAttr) {
        this.bizAttr = bizAttr;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getIsLriskBiz() {
        return isLriskBiz;
    }

    public void setIsLriskBiz(String isLriskBiz) {
        this.isLriskBiz = isLriskBiz;
    }

    public String getIsFollowBiz() {
        return isFollowBiz;
    }

    public void setIsFollowBiz(String isFollowBiz) {
        this.isFollowBiz = isFollowBiz;
    }

    public String getIsBizRev() {
        return isBizRev;
    }

    public void setIsBizRev(String isBizRev) {
        this.isBizRev = isBizRev;
    }

    public String getOrigiDealBizNo() {
        return origiDealBizNo;
    }

    public void setOrigiDealBizNo(String origiDealBizNo) {
        this.origiDealBizNo = origiDealBizNo;
    }

    public String getOrigiDealBizStatus() {
        return origiDealBizStatus;
    }

    public void setOrigiDealBizStatus(String origiDealBizStatus) {
        this.origiDealBizStatus = origiDealBizStatus;
    }

    public BigDecimal getDealBizAmt() {
        return dealBizAmt;
    }

    public void setDealBizAmt(BigDecimal dealBizAmt) {
        this.dealBizAmt = dealBizAmt;
    }

    public BigDecimal getDealBizBailPreRate() {
        return dealBizBailPreRate;
    }

    public void setDealBizBailPreRate(BigDecimal dealBizBailPreRate) {
        this.dealBizBailPreRate = dealBizBailPreRate;
    }

    public BigDecimal getDealBizBailPreAmt() {
        return dealBizBailPreAmt;
    }

    public void setDealBizBailPreAmt(BigDecimal dealBizBailPreAmt) {
        this.dealBizBailPreAmt = dealBizBailPreAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDealBizStatus() {
        return dealBizStatus;
    }

    public void setDealBizStatus(String dealBizStatus) {
        this.dealBizStatus = dealBizStatus;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public List<CmisLmt0011OccRelListDto> getCmisLmt0011OccRelListDtoList() {
        return cmisLmt0011OccRelListDtoList;
    }

    public void setCmisLmt0011OccRelListDtoList(List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList) {
        this.cmisLmt0011OccRelListDtoList = cmisLmt0011OccRelListDtoList;
    }

    public String getOrigiRecoverType() {
        return origiRecoverType;
    }

    public void setOrigiRecoverType(String origiRecoverType) {
        this.origiRecoverType = origiRecoverType;
    }

    public String getOrigiBizAttr() {
        return origiBizAttr;
    }

    public void setOrigiBizAttr(String origiBizAttr) {
        this.origiBizAttr = origiBizAttr;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getBelgLine() {
        return belgLine;
    }

    public void setBelgLine(String belgLine) {
        this.belgLine = belgLine;
    }

    public String getBussStageType() {
        return bussStageType;
    }

    public void setBussStageType(String bussStageType) {
        this.bussStageType = bussStageType;
    }

    @Override
    public String toString() {
        return "CmisLmt0011ReqDto{" +
                "sysId='" + sysId + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", dealBizNo='" + dealBizNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", dealBizType='" + dealBizType + '\'' +
                ", bizAttr='" + bizAttr + '\'' +
                ", prdId='" + prdId + '\'' +
                ", prdName='" + prdName + '\'' +
                ", isLriskBiz='" + isLriskBiz + '\'' +
                ", isFollowBiz='" + isFollowBiz + '\'' +
                ", isBizRev='" + isBizRev + '\'' +
                ", origiDealBizNo='" + origiDealBizNo + '\'' +
                ", origiDealBizStatus='" + origiDealBizStatus + '\'' +
                ", dealBizAmt=" + dealBizAmt +
                ", dealBizBailPreRate=" + dealBizBailPreRate +
                ", dealBizBailPreAmt=" + dealBizBailPreAmt +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", dealBizStatus='" + dealBizStatus + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", origiRecoverType='" + origiRecoverType + '\'' +
                ", origiBizAttr='" + origiBizAttr + '\'' +
                ", belgLine='" + belgLine + '\'' +
                ", bussStageType='" + bussStageType + '\'' +
                ", cmisLmt0011OccRelListDtoList=" + cmisLmt0011OccRelListDtoList +
                '}';
    }
}
