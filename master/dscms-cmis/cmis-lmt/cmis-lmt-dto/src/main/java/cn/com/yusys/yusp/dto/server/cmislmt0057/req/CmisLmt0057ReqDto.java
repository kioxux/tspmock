package cn.com.yusys.yusp.dto.server.cmislmt0057.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0057ReqDto implements Serializable {
    private static final long serialVersionUID = 5461506169208969818L;

    //系统编号
    @JsonProperty(value = "sysNo")
    private String sysNo;
    //金融机构代码
    @JsonProperty(value = "instuCde")
    private String instuCde;
    //所属条线
    @JsonProperty(value = "belgLine")
    private String belgLine;
    //交易流水号
    @JsonProperty(value = "serno")
    private String serno;
    //批复台账编号
    @JsonProperty(value = "accNo")
    private String accNo;
    //批复分项编号
    @JsonProperty(value = "accSubNo")
    private String accSubNo;
    //合同编号
    @JsonProperty(value = "bizNo")
    private String bizNo;
    //交易业务类型
    @JsonProperty(value = "dealBizType")
    private String dealBizType;
    //交易属性
    @JsonProperty(value = "bizAttr")
    private String bizAttr;
    //交易业务金额
    @JsonProperty(value = "dealBizAmt")
    private BigDecimal dealBizAmt;
    //合同起始日
    @JsonProperty(value = "bizStartDate")
    private String bizStartDate;
    //合同到期日
    @JsonProperty(value = "bizEndDate")
    private String bizEndDate;
    //登记人
    @JsonProperty(value = "inputId")
    private String inputId;
    //登记机构
    @JsonProperty(value = "inputBrId")
    private String inputBrId;
    //登记日期
    @JsonProperty(value = "inputDate")
    private String inputDate;
    //业务台账编号
    @JsonProperty(value = "dealBizNo")
    private String dealBizNo;
    //客户编号
    @JsonProperty(value = "cusId")
    private String cusId;
    //客户名称
    @JsonProperty(value = "cusName")
    private String cusName;
    //产品编号
    @JsonProperty(value = "prdNo")
    private String prdNo;
    //产品名称
    @JsonProperty(value = "prdName")
    private String prdName;
    //授信品种类型
    @JsonProperty(value = "prdTypeProp")
    private String prdTypeProp;
    //台账占用总额(人民币)
    @JsonProperty(value = "dealBizAmtCny")
    private BigDecimal dealBizAmtCny;
    //台账占用敞口(人民币)
    @JsonProperty(value = "dealBizSpacAmtCny")
    private BigDecimal dealBizSpacAmtCny;
    //保证金比例
    @JsonProperty(value = "dealBizBailPreRate")
    private BigDecimal dealBizBailPreRate;
    //保证金金额
    @JsonProperty(value = "dealBizBailPreAmt")
    private BigDecimal dealBizBailPreAmt;
    //台账起始日
    @JsonProperty(value = "accStartDate")
    private String accStartDate;
    //台账到期日
    @JsonProperty(value = "accEndDate")
    private String accEndDate;
    //是否循环
    @JsonProperty(value = "IsRevolv")
    private String IsRevolv;
    //担保方式
    @JsonProperty(value = "suitGuarWay")
    private String suitGuarWay;
    //授信总额
    @JsonProperty(value = "avlAmt")
    private BigDecimal avlAmt;
    //额度起始日期
    @JsonProperty(value = "lmtStartDate")
    private String lmtStartDate;
    //额度到期日期
    @JsonProperty(value = "lmtEndDate")
    private String lmtEndDate;
    //授信品种编号
    @JsonProperty(value = "limitSubNo")
    private String limitSubNo;
    //授信品种名称
    @JsonProperty(value = "limitSubName")
    private String limitSubName;
    //是否低风险
    @JsonProperty(value = "isLriskLmt")
    private String isLriskLmt;

    public String getSysNo() {
        return sysNo;
    }

    public void setSysNo(String sysNo) {
        this.sysNo = sysNo;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getBelgLine() {
        return belgLine;
    }

    public void setBelgLine(String belgLine) {
        this.belgLine = belgLine;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getAccSubNo() {
        return accSubNo;
    }

    public void setAccSubNo(String accSubNo) {
        this.accSubNo = accSubNo;
    }

    public String getBizNo() {
        return bizNo;
    }

    public void setBizNo(String bizNo) {
        this.bizNo = bizNo;
    }

    public String getDealBizType() {
        return dealBizType;
    }

    public void setDealBizType(String dealBizType) {
        this.dealBizType = dealBizType;
    }

    public String getBizAttr() {
        return bizAttr;
    }

    public void setBizAttr(String bizAttr) {
        this.bizAttr = bizAttr;
    }

    public BigDecimal getDealBizAmt() {
        return dealBizAmt;
    }

    public void setDealBizAmt(BigDecimal dealBizAmt) {
        this.dealBizAmt = dealBizAmt;
    }

    public String getBizStartDate() {
        return bizStartDate;
    }

    public void setBizStartDate(String bizStartDate) {
        this.bizStartDate = bizStartDate;
    }

    public String getBizEndDate() {
        return bizEndDate;
    }

    public void setBizEndDate(String bizEndDate) {
        this.bizEndDate = bizEndDate;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrdNo() {
        return prdNo;
    }

    public void setPrdNo(String prdNo) {
        this.prdNo = prdNo;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getPrdTypeProp() {
        return prdTypeProp;
    }

    public void setPrdTypeProp(String prdTypeProp) {
        this.prdTypeProp = prdTypeProp;
    }

    public BigDecimal getDealBizAmtCny() {
        return dealBizAmtCny;
    }

    public void setDealBizAmtCny(BigDecimal dealBizAmtCny) {
        this.dealBizAmtCny = dealBizAmtCny;
    }

    public BigDecimal getDealBizSpacAmtCny() {
        return dealBizSpacAmtCny;
    }

    public void setDealBizSpacAmtCny(BigDecimal dealBizSpacAmtCny) {
        this.dealBizSpacAmtCny = dealBizSpacAmtCny;
    }

    public BigDecimal getDealBizBailPreRate() {
        return dealBizBailPreRate;
    }

    public void setDealBizBailPreRate(BigDecimal dealBizBailPreRate) {
        this.dealBizBailPreRate = dealBizBailPreRate;
    }

    public BigDecimal getDealBizBailPreAmt() {
        return dealBizBailPreAmt;
    }

    public void setDealBizBailPreAmt(BigDecimal dealBizBailPreAmt) {
        this.dealBizBailPreAmt = dealBizBailPreAmt;
    }

    public String getAccStartDate() {
        return accStartDate;
    }

    public void setAccStartDate(String accStartDate) {
        this.accStartDate = accStartDate;
    }

    public String getAccEndDate() {
        return accEndDate;
    }

    public void setAccEndDate(String accEndDate) {
        this.accEndDate = accEndDate;
    }

    public String getIsRevolv() {
        return IsRevolv;
    }

    public void setIsRevolv(String isRevolv) {
        IsRevolv = isRevolv;
    }

    public String getSuitGuarWay() {
        return suitGuarWay;
    }

    public void setSuitGuarWay(String suitGuarWay) {
        this.suitGuarWay = suitGuarWay;
    }

    public BigDecimal getAvlAmt() {
        return avlAmt;
    }

    public void setAvlAmt(BigDecimal avlAmt) {
        this.avlAmt = avlAmt;
    }

    public String getLmtStartDate() {
        return lmtStartDate;
    }

    public void setLmtStartDate(String lmtStartDate) {
        this.lmtStartDate = lmtStartDate;
    }

    public String getLmtEndDate() {
        return lmtEndDate;
    }

    public void setLmtEndDate(String lmtEndDate) {
        this.lmtEndDate = lmtEndDate;
    }

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    public String getLimitSubName() {
        return limitSubName;
    }

    public void setLimitSubName(String limitSubName) {
        this.limitSubName = limitSubName;
    }

    public String getIsLriskLmt() {
        return isLriskLmt;
    }

    public void setIsLriskLmt(String isLriskLmt) {
        this.isLriskLmt = isLriskLmt;
    }

    @Override
    public String toString() {
        return "CmisLmt0057ReqDto{" +
                "sysNo='" + sysNo + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", belgLine='" + belgLine + '\'' +
                ", serno='" + serno + '\'' +
                ", accNo='" + accNo + '\'' +
                ", accSubNo='" + accSubNo + '\'' +
                ", bizNo='" + bizNo + '\'' +
                ", dealBizType='" + dealBizType + '\'' +
                ", bizAttr='" + bizAttr + '\'' +
                ", dealBizAmt=" + dealBizAmt +
                ", bizStartDate='" + bizStartDate + '\'' +
                ", bizEndDate='" + bizEndDate + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", dealBizNo='" + dealBizNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", prdNo='" + prdNo + '\'' +
                ", prdName='" + prdName + '\'' +
                ", prdTypeProp='" + prdTypeProp + '\'' +
                ", dealBizAmtCny=" + dealBizAmtCny +
                ", dealBizSpacAmtCny=" + dealBizSpacAmtCny +
                ", dealBizBailPreRate=" + dealBizBailPreRate +
                ", dealBizBailPreAmt=" + dealBizBailPreAmt +
                ", accStartDate='" + accStartDate + '\'' +
                ", accEndDate='" + accEndDate + '\'' +
                ", IsRevolv='" + IsRevolv + '\'' +
                ", suitGuarWay='" + suitGuarWay + '\'' +
                ", avlAmt=" + avlAmt +
                ", lmtStartDate='" + lmtStartDate + '\'' +
                ", lmtEndDate='" + lmtEndDate + '\'' +
                ", limitSubNo='" + limitSubNo + '\'' +
                ", limitSubName='" + limitSubName + '\'' +
                ", isLriskLmt='" + isLriskLmt + '\'' +
                '}';
    }
}
