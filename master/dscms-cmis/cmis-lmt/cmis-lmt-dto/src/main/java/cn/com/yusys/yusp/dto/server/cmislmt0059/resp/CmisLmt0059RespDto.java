package cn.com.yusys.yusp.dto.server.cmislmt0059.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0059RespDto implements Serializable {

    private static final long serialVersionUID = 4257512024639182666L;
    //结果代码
    @JsonProperty(value = "errorCode")
    private String errorCode;

    //结果信息
    @JsonProperty(value = "errorMsg")
    private String errorMsg;

    //客户用信总额
    @JsonProperty(value = "loanTotal")
    private BigDecimal loanTotal;

    //客户用信余额
    @JsonProperty(value = "loanBal")
    private BigDecimal loanBal;

    //客户用信总额
    @JsonProperty(value = "subLoanTotal")
    private BigDecimal subLoanTotal;

    //明细用信余额
    @JsonProperty(value = "subLoanBal")
    private BigDecimal subLoanBal;

    //客户用信余额
    @JsonProperty(value = "spacLoanBal")
    private BigDecimal spacLoanBal;

    //明细敞口用信余额
    @JsonProperty(value = "subSpacLoanBal")
    private BigDecimal subSpacLoanBal;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public BigDecimal getLoanTotal() {
        return loanTotal;
    }

    public void setLoanTotal(BigDecimal loanTotal) {
        this.loanTotal = loanTotal;
    }

    public BigDecimal getLoanBal() {
        return loanBal;
    }

    public void setLoanBal(BigDecimal loanBal) {
        this.loanBal = loanBal;
    }

    public BigDecimal getSubLoanTotal() {
        return subLoanTotal;
    }

    public void setSubLoanTotal(BigDecimal subLoanTotal) {
        this.subLoanTotal = subLoanTotal;
    }

    public BigDecimal getSubLoanBal() {
        return subLoanBal;
    }

    public void setSubLoanBal(BigDecimal subLoanBal) {
        this.subLoanBal = subLoanBal;
    }

    public BigDecimal getSpacLoanBal() {
        return spacLoanBal;
    }

    public void setSpacLoanBal(BigDecimal spacLoanBal) {
        this.spacLoanBal = spacLoanBal;
    }

    public BigDecimal getSubSpacLoanBal() {
        return subSpacLoanBal;
    }

    public void setSubSpacLoanBal(BigDecimal subSpacLoanBal) {
        this.subSpacLoanBal = subSpacLoanBal;
    }

    @Override
    public String toString() {
        return "CmisLmt0059RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", loanTotal=" + loanTotal +
                ", loanBal=" + loanBal +
                ", subLoanTotal=" + subLoanTotal +
                ", subLoanBal=" + subLoanBal +
                ", spacLoanBal=" + spacLoanBal +
                ", subSpacLoanBal=" + subSpacLoanBal +
                '}';
    }
}
