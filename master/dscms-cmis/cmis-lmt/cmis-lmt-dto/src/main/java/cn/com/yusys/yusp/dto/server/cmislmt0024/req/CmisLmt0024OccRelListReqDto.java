package cn.com.yusys.yusp.dto.server.cmislmt0024.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * 响应Dto：承兑行白名单额度占用
 */
public class CmisLmt0024OccRelListReqDto {
    @JsonProperty(value = "dealBizNo")
    private String dealBizNo;//交易业务编号
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "prdNo")
    private String prdNo;//产品编号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "origiDealBizNo")
    private String origiDealBizNo;//原交易业务编号
    @JsonProperty(value = "origiRecoverType")
    private String origiRecoverType;//原交易业务恢复类型
    @JsonProperty(value = "startDate")
    private String startDate;//起始日
    @JsonProperty(value = "endDate")
    private String endDate;//到期日
    @JsonProperty(value = "orgCusId")
    private String orgCusId;//承兑行客户号
    @JsonProperty(value = "bizTotalAmt")
    private BigDecimal bizTotalAmt;//占用金额

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrdNo() {
        return prdNo;
    }

    public void setPrdNo(String prdNo) {
        this.prdNo = prdNo;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getOrigiDealBizNo() {
        return origiDealBizNo;
    }

    public void setOrigiDealBizNo(String origiDealBizNo) {
        this.origiDealBizNo = origiDealBizNo;
    }

    public String getOrigiRecoverType() {
        return origiRecoverType;
    }

    public void setOrigiRecoverType(String origiRecoverType) {
        this.origiRecoverType = origiRecoverType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getOrgCusId() {
        return orgCusId;
    }

    public void setOrgCusId(String orgCusId) {
        this.orgCusId = orgCusId;
    }

    public BigDecimal getBizTotalAmt() {
        return bizTotalAmt;
    }

    public void setBizTotalAmt(BigDecimal bizTotalAmt) {
        this.bizTotalAmt = bizTotalAmt;
    }

    @Override
    public String toString() {
        return "CmisLmt0024OccRelListReqDto{" +
                "dealBizNo='" + dealBizNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", prdNo='" + prdNo + '\'' +
                ", prdName='" + prdName + '\'' +
                ", origiDealBizNo='" + origiDealBizNo + '\'' +
                ", origiRecoverType='" + origiRecoverType + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", orgCusId='" + orgCusId + '\'' +
                ", bizTotalAmt=" + bizTotalAmt +
                '}';
    }
}
