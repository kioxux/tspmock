package cn.com.yusys.yusp.dto.server.cmislmt0065.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0065RespDto implements Serializable {

    private static final long serialVersionUID = 4257512024639182666L;
    //结果代码
    @JsonProperty(value = "errorCode")
    private String errorCode;

    //结果信息
    @JsonProperty(value = "errorMsg")
    private String errorMsg;

    //用信余额
    @JsonProperty(value = "lmtAmt")
    private BigDecimal lmtAmt;

    //额度状态
    @JsonProperty(value = "status")
    private String status;


    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "CmisLmt0065RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", lmtAmt=" + lmtAmt +
                ", status='" + status + '\'' +
                '}';
    }
}
