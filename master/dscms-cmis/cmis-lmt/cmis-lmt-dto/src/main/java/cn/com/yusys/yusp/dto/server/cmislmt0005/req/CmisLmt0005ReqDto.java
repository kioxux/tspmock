package cn.com.yusys.yusp.dto.server.cmislmt0005.req;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：资金业务额度同步
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0005ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "sysId")
    private String sysId;//系统编号
    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "lmtType")
    private String lmtType;//额度类型
    @JsonProperty(value = "isCreateAcc")
    private String isCreateAcc;//是否生成新批复台账
    @JsonProperty(value = "accNo")
    private String accNo;//批复台账编号
    @JsonProperty(value = "origiAccNo")
    private String origiAccNo;//原批复台账编号
    @JsonProperty(value = "proNo")
    private String proNo;//项目编号
    @JsonProperty(value = "lmtAmt")
    private BigDecimal lmtAmt;//授信金额
    @JsonProperty(value = "sobsAmt")
    private BigDecimal sobsAmt;//自营金额
    @JsonProperty(value = "assetManaAmt")
    private BigDecimal assetManaAmt;//资管金额
    @JsonProperty(value = "proName")
    private String proName;//项目名称
    @JsonProperty(value = "assetNo")
    private String assetNo;//资产编号
    @JsonProperty(value = "limitSubNo")
    private String limitSubNo;//额度品种编号
    @JsonProperty(value = "limitSubName")
    private String limitSubName;//额度品种名称
    @JsonProperty(value = "term")
    private Integer term;//期限
    @JsonProperty(value = "startDate")
    private String startDate;//授信起始日
    @JsonProperty(value = "endDate")
    private String endDate;//授信到期日
    @JsonProperty(value = "isRevolv")
    private String isRevolv;//是否可循环
    @JsonProperty(value = "curType")
    private String curType;//币种
    @JsonProperty(value = "accStatus")
    private String accStatus;//额度状态
    @JsonProperty(value = "managerId")
    private String managerId;//责任人
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//责任机构
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记时间

    @JsonProperty(value = "investType")
    private String investType;//投资类型

    /**
     * 底层融资人分项列表
     */
    private List<CmisLmt0005LmtDetailsListReqDto> lmtDetailsList;

    public List<CmisLmt0005LmtDetailsListReqDto> getLmtDetailsList() {
        return lmtDetailsList;
    }

    public void setLmtDetailsList(List<CmisLmt0005LmtDetailsListReqDto> lmtDetailsList) {
        this.lmtDetailsList = lmtDetailsList;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getLmtType() {
        return lmtType;
    }

    public void setLmtType(String lmtType) {
        this.lmtType = lmtType;
    }

    public String getIsCreateAcc() {
        return isCreateAcc;
    }

    public void setIsCreateAcc(String isCreateAcc) {
        this.isCreateAcc = isCreateAcc;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getOrigiAccNo() {
        return origiAccNo;
    }

    public void setOrigiAccNo(String origiAccNo) {
        this.origiAccNo = origiAccNo;
    }

    public String getProNo() {
        return proNo;
    }

    public void setProNo(String proNo) {
        this.proNo = proNo;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    public BigDecimal getSobsAmt() {
        return sobsAmt;
    }

    public void setSobsAmt(BigDecimal sobsAmt) {
        this.sobsAmt = sobsAmt;
    }

    public BigDecimal getAssetManaAmt() {
        return assetManaAmt;
    }

    public void setAssetManaAmt(BigDecimal assetManaAmt) {
        this.assetManaAmt = assetManaAmt;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    public String getLimitSubName() {
        return limitSubName;
    }

    public void setLimitSubName(String limitSubName) {
        this.limitSubName = limitSubName;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getIsRevolv() {
        return isRevolv;
    }

    public void setIsRevolv(String isRevolv) {
        this.isRevolv = isRevolv;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getInvestType() {
        return investType;
    }

    public void setInvestType(String investType) {
        this.investType = investType;
    }

    @Override
    public String toString() {
        return "CmisLmt0005ReqDto{" +
                "sysId='" + sysId + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", lmtType='" + lmtType + '\'' +
                ", isCreateAcc='" + isCreateAcc + '\'' +
                ", accNo='" + accNo + '\'' +
                ", origiAccNo='" + origiAccNo + '\'' +
                ", proNo='" + proNo + '\'' +
                ", lmtAmt=" + lmtAmt +
                ", sobsAmt=" + sobsAmt +
                ", assetManaAmt=" + assetManaAmt +
                ", proName='" + proName + '\'' +
                ", assetNo='" + assetNo + '\'' +
                ", limitSubNo='" + limitSubNo + '\'' +
                ", limitSubName='" + limitSubName + '\'' +
                ", term=" + term +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", isRevolv='" + isRevolv + '\'' +
                ", curType='" + curType + '\'' +
                ", accStatus='" + accStatus + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", investType='" + investType + '\'' +
                ", lmtDetailsList=" + lmtDetailsList +
                '}';
    }
}
