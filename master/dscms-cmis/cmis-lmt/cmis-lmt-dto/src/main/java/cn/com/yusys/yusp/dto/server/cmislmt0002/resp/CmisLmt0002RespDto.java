package cn.com.yusys.yusp.dto.server.cmislmt0002.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：集团客户额度同步
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0002RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        return "Cmislmt0003RespDto{" +
                "errorCode='" + errorCode + '\'' +
                "errorMsg='" + errorMsg + '\'' +
                '}';
    }
}
