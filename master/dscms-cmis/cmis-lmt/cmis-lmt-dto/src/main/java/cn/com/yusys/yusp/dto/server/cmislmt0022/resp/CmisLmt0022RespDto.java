package cn.com.yusys.yusp.dto.server.cmislmt0022.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * 响应Dto：客户分类额度查询
 */
public class CmisLmt0022RespDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    /**
     * 客户额度列表
     */
    private List<CmisLmt0022LmtListRespDto> lmtList;

    public List<CmisLmt0022LmtListRespDto> getLmtList() {
        return lmtList;
    }

    public void setLmtList(List<CmisLmt0022LmtListRespDto> lmtList) {
        this.lmtList = lmtList;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        return "CmisLmt0022RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                '}';
    }
}