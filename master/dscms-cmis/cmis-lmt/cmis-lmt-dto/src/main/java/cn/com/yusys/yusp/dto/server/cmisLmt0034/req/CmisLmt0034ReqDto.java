package cn.com.yusys.yusp.dto.server.cmisLmt0034.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * 国结票据出账额度占用
 * add by dumd 20210623
 */
public class CmisLmt0034ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "sysNo")
    private String sysNo;//系统编号
    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码
    @JsonProperty(value = "serno")
    private String serno;//交易流水号
    @JsonProperty(value = "bizNo")
    private String bizNo;//合同编号
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期

    /**
     * 台账明细
     */
    private List<CmisLmt0034DealBizListReqDto> dealBizList;

    /**
     * 占用额度列表(同业客户额度)
     */
    private List<CmisLmt0034OccRelListReqDto> occRelList;

    public List<CmisLmt0034DealBizListReqDto> getDealBizList() {
        return dealBizList;
    }

    public void setDealBizList(List<CmisLmt0034DealBizListReqDto> dealBizList) {
        this.dealBizList = dealBizList;
    }

    public List<CmisLmt0034OccRelListReqDto> getOccRelList() {
        return occRelList;
    }

    public void setOccRelList(List<CmisLmt0034OccRelListReqDto> occRelList) {
        this.occRelList = occRelList;
    }

    public String getSysNo() {
        return sysNo;
    }

    public void setSysNo(String sysNo) {
        this.sysNo = sysNo;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getBizNo() {
        return bizNo;
    }

    public void setBizNo(String bizNo) {
        this.bizNo = bizNo;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    @Override
    public String toString() {
        return "CmisLmt0034ReqDto{" +
                "sysNo='" + sysNo + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", serno='" + serno + '\'' +
                ", bizNo='" + bizNo + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", dealBizList=" + dealBizList +
                ", occRelList=" + occRelList +
                '}';
    }
}
