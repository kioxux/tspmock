package cn.com.yusys.yusp.dto.server.cmislmt0013.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

/**
 * 请求Dto：台账占用接口
 */             
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0013ReqDto {
	   private static final long serialVersionUID = 1L;
	 @JsonProperty(value = "sysId")
	private String sysId;//系统编号
	 @JsonProperty(value = "instuCde")
	private String instuCde;//金融机构代码
	 @JsonProperty(value = "serno")
	private String serno;//交易流水号
	 @JsonProperty(value = "bizNo")
	private String bizNo;//合同编号
	 @JsonProperty(value = "inputId")
	private String inputId;//登记人
	 @JsonProperty(value = "inputBrId")
	private String inputBrId;//登记机构
	 @JsonProperty(value = "inputDate")
	private String inputDate;//登记日期
	//业务条线
	@JsonProperty(value = "belgLine")
	private String belgLine;

	private List<CmisLmt0013ReqDealBizListDto> cmisLmt0013ReqDealBizListDtos;

	public String getSysId() {
		return sysId;
	}

	public void setSysId(String sysId) {
		this.sysId = sysId;
	}

	public String  getInstuCde() { return instuCde; }
	public void setInstuCde(String instuCde ) { this.instuCde = instuCde;}
	public String  getSerno() { return serno; }
	public void setSerno(String serno ) { this.serno = serno;}
	public String  getBizNo() { return bizNo; }
	public void setBizNo(String bizNo ) { this.bizNo = bizNo;}
	public String  getInputId() { return inputId; }
	public void setInputId(String inputId ) { this.inputId = inputId;}
	public String  getInputBrId() { return inputBrId; }
	public void setInputBrId(String inputBrId ) { this.inputBrId = inputBrId;}
	public String  getInputDate() { return inputDate; }
	public void setInputDate(String inputDate ) { this.inputDate = inputDate;}

	public String getBelgLine() {
		return belgLine;
	}

	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}

	public List<CmisLmt0013ReqDealBizListDto> getCmisLmt0013ReqDealBizListDtos() {
		return cmisLmt0013ReqDealBizListDtos;
	}

	public void setCmisLmt0013ReqDealBizListDtos(List<CmisLmt0013ReqDealBizListDto> cmisLmt0013ReqDealBizListDtos) {
		this.cmisLmt0013ReqDealBizListDtos = cmisLmt0013ReqDealBizListDtos;
	}

	@Override
	public String toString() {
		return "CmisLmt0013ReqDto{" +
				"sysId='" + sysId + '\'' +
				", instuCde='" + instuCde + '\'' +
				", serno='" + serno + '\'' +
				", bizNo='" + bizNo + '\'' +
				", inputId='" + inputId + '\'' +
				", inputBrId='" + inputBrId + '\'' +
				", inputDate='" + inputDate + '\'' +
				", belgLine='" + belgLine + '\'' +
				", cmisLmt0013ReqDealBizListDtos=" + cmisLmt0013ReqDealBizListDtos +
				'}';
	}
}
