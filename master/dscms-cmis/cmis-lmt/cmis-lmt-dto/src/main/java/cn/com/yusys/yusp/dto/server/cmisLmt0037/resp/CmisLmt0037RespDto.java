package cn.com.yusys.yusp.dto.server.cmisLmt0037.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * 国结票据出账额度校验
 * add by dumd 20210618
 */
public class CmisLmt0037RespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode; //结果代码

    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    @JsonProperty(value = "isExists")
    private String isExists; //是否存在s

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getIsExists() {
        return isExists;
    }

    public void setIsExists(String isExists) {
        this.isExists = isExists;
    }

    @Override
    public String toString() {
        return "CmisLmt0037RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", isExists='" + isExists + '\'' +
                '}';
    }
}