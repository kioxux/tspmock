package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtSubPrdMappConf
 * @类描述: lmt_sub_prd_mapp_conf数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-15 09:27:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtSubPrdMappConfListDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** PRD_ID **/
	private String prdId;
	/** PRD_NAME **/
	private String prdName;

	public String getPrdId() {
		return prdId;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	@Override
	public String toString() {
		return "LmtSubPrdMappConfListDto{" +
				"prdId='" + prdId + '\'' +
				", prdName='" + prdName + '\'' +
				'}';
	}

	public LmtSubPrdMappConfListDto() {
	}

	public LmtSubPrdMappConfListDto(String prdId, String prdName) {
		this.prdId = prdId;
		this.prdName = prdName;
	}
}