package cn.com.yusys.yusp.dto.server.cmislmt0042.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CmisLmt0042RespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    private List<CmisLmt0042ListRespDto> cfgPrdTypePropertiesList;//产品配置列表

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<CmisLmt0042ListRespDto> getCfgPrdTypePropertiesList() {
        return cfgPrdTypePropertiesList;
    }

    public void setCfgPrdTypePropertiesList(List<CmisLmt0042ListRespDto> cfgPrdTypePropertiesList) {
        this.cfgPrdTypePropertiesList = cfgPrdTypePropertiesList;
    }

    @Override
    public String toString() {
        return "CmisLmt0042RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", cfgPrdTypePropertiesList=" + cfgPrdTypePropertiesList +
                '}';
    }
}
