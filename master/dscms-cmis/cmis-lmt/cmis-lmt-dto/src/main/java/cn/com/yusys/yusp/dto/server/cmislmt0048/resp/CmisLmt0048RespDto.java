package cn.com.yusys.yusp.dto.server.cmislmt0048.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CmisLmt0048RespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息
    @JsonProperty(value = "hasOtherBuss")
    private String hasOtherBuss;//是否存在其他业务

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getHasOtherBuss() {
        return hasOtherBuss;
    }

    public void setHasOtherBuss(String hasOtherBuss) {
        this.hasOtherBuss = hasOtherBuss;
    }

    @Override
    public String toString() {
        return "CmisLmt0048RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", hasOtherBuss='" + hasOtherBuss + '\'' +
                '}';
    }
}
