package cn.com.yusys.yusp.dto.server.cmislmt0062.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class CmisLmt0062RespDto {

    /**
     * 大额风险暴露值
     */
    @JsonProperty("deRiskExpoAmt")
    private BigDecimal deRiskExpoAmt;

    /**
     * 指标限额要求（监管限额）
     */
    @JsonProperty("riskIndexReqAmt")
    private BigDecimal riskIndexReqAmt;


    /**
     * 黄色阈值
     */
    @JsonProperty("riskYellowReqAmt")
    private BigDecimal riskYellowReqAmt;

    /**
     * 红色阈值
     */
    @JsonProperty("riskRedReqAmt")
    private BigDecimal riskRedReqAmt;

    @JsonProperty("errorCode")
    private String errorCode;

    @JsonProperty("errorMsg")
    private String errorMsg;

    public BigDecimal getDeRiskExpoAmt() {
        return deRiskExpoAmt;
    }

    public void setDeRiskExpoAmt(BigDecimal deRiskExpoAmt) {
        this.deRiskExpoAmt = deRiskExpoAmt;
    }

    public BigDecimal getRiskIndexReqAmt() {
        return riskIndexReqAmt;
    }

    public void setRiskIndexReqAmt(BigDecimal riskIndexReqAmt) {
        this.riskIndexReqAmt = riskIndexReqAmt;
    }

    public BigDecimal getRiskYellowReqAmt() {
        return riskYellowReqAmt;
    }

    public void setRiskYellowReqAmt(BigDecimal riskYellowReqAmt) {
        this.riskYellowReqAmt = riskYellowReqAmt;
    }

    public BigDecimal getRiskRedReqAmt() {
        return riskRedReqAmt;
    }

    public void setRiskRedReqAmt(BigDecimal riskRedReqAmt) {
        this.riskRedReqAmt = riskRedReqAmt;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        return "CmisLmt0062RespDto{" +
                "deRiskExpoAmt=" + deRiskExpoAmt +
                ", riskIndexReqAmt=" + riskIndexReqAmt +
                ", riskYellowReqAmt=" + riskYellowReqAmt +
                ", riskRedReqAmt=" + riskRedReqAmt +
                ", errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                '}';
    }
}
