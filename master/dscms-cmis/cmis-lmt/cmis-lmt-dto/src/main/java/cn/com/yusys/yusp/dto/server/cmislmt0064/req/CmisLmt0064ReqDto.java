package cn.com.yusys.yusp.dto.server.cmislmt0064.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0064ReqDto implements Serializable {
    private static final long serialVersionUID = 5461506169208969818L;

    //客户编号
    @JsonProperty(value = "cusId")
    private String cusId;

    //金融机构代码
    @JsonProperty(value = "instuCde")
    private String instuCde;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    @Override
    public String toString() {
        return "CmisLmt0064ReqDto{" +
                "cusId='" + cusId + '\'' +
                ", instuCde='" + instuCde + '\'' +
                '}';
    }
}
