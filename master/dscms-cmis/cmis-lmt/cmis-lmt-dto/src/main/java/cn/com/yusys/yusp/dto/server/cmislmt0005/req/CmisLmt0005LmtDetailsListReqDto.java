package cn.com.yusys.yusp.dto.server.cmislmt0005.req;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

/**
 * 请求Dto：资金业务额度同步 底层融资人分项列表
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0005LmtDetailsListReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "basicCusId")
    private String basicCusId;//底层融资人客户编号
    @JsonProperty(value = "basicCusName")
    private String basicCusName;//底层融资人客户名称
    @JsonProperty(value = "isCreateLmt")
    private String isCreateLmt;//是否生成穿透化额度
    @JsonProperty(value = "basicCusCatalog")
    private String basicCusCatalog;//底层融资人客户类型
    @JsonProperty(value = "basicAccNo")
    private String basicAccNo;//底层融资人批复台账编号
    @JsonProperty(value = "basicAccSubNo")
    private String basicAccSubNo;//底层融资人批复分项编号
    @JsonProperty(value = "limitSubNo")
    private String limitSubNo;//额度品种编号
    @JsonProperty(value = "limitSubName")
    private String limitSubName;//额度品种名称
    @JsonProperty(value = "useamt")
    private BigDecimal useamt;//本次占用金额
    @JsonProperty(value = "suitGuarWay")
    private String suitGuarWay;//担保方式
    @JsonProperty(value = "accStatus")
    private String accStatus;//额度状态
    @JsonProperty(value = "oprType")
    private String oprType;//操作类型
    @JsonProperty(value = "managerId")
    private String managerId;//责任人
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//责任机构
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期

    public String getBasicCusId() {
        return basicCusId;
    }

    public void setBasicCusId(String basicCusId) {
        this.basicCusId = basicCusId;
    }

    public String getBasicCusName() {
        return basicCusName;
    }

    public void setBasicCusName(String basicCusName) {
        this.basicCusName = basicCusName;
    }

    public String getIsCreateLmt() {
        return isCreateLmt;
    }

    public void setIsCreateLmt(String isCreateLmt) {
        this.isCreateLmt = isCreateLmt;
    }

    public String getbasicCusCatalog() {
        return basicCusCatalog;
    }

    public void setbasicCusCatalog(String basicCusCatalog) {
        this.basicCusCatalog = basicCusCatalog;
    }

    public String getBasicAccNo() {
        return basicAccNo;
    }

    public void setBasicAccNo(String basicAccNo) {
        this.basicAccNo = basicAccNo;
    }

    public String getBasicAccSubNo() {
        return basicAccSubNo;
    }

    public void setBasicAccSubNo(String basicAccSubNo) {
        this.basicAccSubNo = basicAccSubNo;
    }

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    public String getLimitSubName() {
        return limitSubName;
    }

    public void setLimitSubName(String limitSubName) {
        this.limitSubName = limitSubName;
    }

    public BigDecimal getUseamt() {
        return useamt;
    }

    public void setUseamt(BigDecimal useamt) {
        this.useamt = useamt;
    }

    public String getSuitGuarWay() {
        return suitGuarWay;
    }

    public void setSuitGuarWay(String suitGuarWay) {
        this.suitGuarWay = suitGuarWay;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    @Override
    public String toString() {
        return "CmisLmt0005LmtDetailsListReqDto{" +
                "basicCusId='" + basicCusId + '\'' +
                ", basicCusName='" + basicCusName + '\'' +
                ", isCreateLmt='" + isCreateLmt + '\'' +
                ", basicCusCatalog='" + basicCusCatalog + '\'' +
                ", basicAccNo='" + basicAccNo + '\'' +
                ", basicAccSubNo='" + basicAccSubNo + '\'' +
                ", limitSubNo='" + limitSubNo + '\'' +
                ", limitSubName='" + limitSubName + '\'' +
                ", useamt=" + useamt +
                ", suitGuarWay='" + suitGuarWay + '\'' +
                ", accStatus='" + accStatus + '\'' +
                ", oprType='" + oprType + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                '}';
    }
}
