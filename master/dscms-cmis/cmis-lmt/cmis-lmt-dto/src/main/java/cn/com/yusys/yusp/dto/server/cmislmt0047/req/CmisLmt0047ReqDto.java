package cn.com.yusys.yusp.dto.server.cmislmt0047.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CmisLmt0047ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "queryType")//查询类型
    private String queryType;

    //占用额度列表
    @JsonProperty(value = "CmisLmt0047ContRelDtoList")
    private List<CmisLmt0047LmtSubDtoList> lmtSubDtoList ;

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public List<CmisLmt0047LmtSubDtoList> getLmtSubDtoList() {
        return lmtSubDtoList;
    }

    public void setLmtSubDtoList(List<CmisLmt0047LmtSubDtoList> lmtSubDtoList) {
        this.lmtSubDtoList = lmtSubDtoList;
    }

    @Override
    public String toString() {
        return "CmisLmt0047ReqDto{" +
                "queryType='" + queryType + '\'' +
                ", lmtSubDtoList=" + lmtSubDtoList +
                '}';
    }
}
