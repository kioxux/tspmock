package cn.com.yusys.yusp.dto.server.cmislmt0020.req;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 请求Dto：承兑行白名单额度查询
 */
public class CmisLmt0020ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//交易流水号
    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "CmisLmt0020ReqDto{" +
                "serno='" + serno + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", cusId='" + cusId + '\'' +
                '}';
    }
}
