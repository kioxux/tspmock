package cn.com.yusys.yusp.dto.server.cmislmt0003.req;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0003LmtSubListReqDto {

    /**
     * 授信分项编号
     */
    @JsonProperty(value = "accSubNo")
    private String accSubNo;

    /**
     * 原授信分项编号
     */
    @JsonProperty(value = "origiAccSubNo")
    private String origiAccSubNo;

    /**
     * 父节点
     */
    @JsonProperty(value = "parentId")
    private String parentId;

    /**
     * 授信品种编号
     */
    @JsonProperty(value = "limitSubNo")
    private String limitSubNo;

    /**
     * 授信品种名称
     */
    @JsonProperty(value = "limitSubName")
    private String limitSubName;

    /**
     * 授信金额
     */
    @JsonProperty(value = "avlamt")
    private BigDecimal avlamt;

    /**
     * 币种
     */
    @JsonProperty(value = "curType")
    private String curType;

    /**
     * 授信期限
     */
    @JsonProperty(value = "term")
    private Integer term;

    /**
     * 额度起始日
     */
    @JsonProperty(value = "startDate")
    private String startDate;

    /**
     * 额度到期日
     */
    @JsonProperty(value = "endDate")
    private String endDate;

    /**
     * 是否涉及货币基金
     */
    @JsonProperty(value = "isIvlMf")
    private String isIvlMf;

    /**
     * 货币基金总授信额度
     */
    @JsonProperty(value = "lmtMfAmt")
    private BigDecimal lmtMfAmt;

    /**
     * 单只货币基金授信额度
     */
    @JsonProperty(value = "lmtSingleMfAmt")
    private BigDecimal lmtSingleMfAmt;

    /**
     * 是否可循环
     */
    @JsonProperty(value = "isRevolv")
    private String isRevolv;

    /**
     * 批复分项状态
     */
    @JsonProperty(value = "accSubStatus")
    private String accSubStatus;

    public String getAccSubNo() {
        return accSubNo;
    }

    public void setAccSubNo(String accSubNo) {
        this.accSubNo = accSubNo;
    }

    public String getOrigiAccSubNo() {
        return origiAccSubNo;
    }

    public void setOrigiAccSubNo(String origiAccSubNo) {
        this.origiAccSubNo = origiAccSubNo;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    public String getLimitSubName() {
        return limitSubName;
    }

    public void setLimitSubName(String limitSubName) {
        this.limitSubName = limitSubName;
    }

    public BigDecimal getAvlamt() {
        return avlamt;
    }

    public void setAvlamt(BigDecimal avlamt) {
        this.avlamt = avlamt;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getIsIvlMf() {
        return isIvlMf;
    }

    public void setIsIvlMf(String isIvlMf) {
        this.isIvlMf = isIvlMf;
    }

    public BigDecimal getLmtMfAmt() {
        return lmtMfAmt;
    }

    public void setLmtMfAmt(BigDecimal lmtMfAmt) {
        this.lmtMfAmt = lmtMfAmt;
    }

    public BigDecimal getLmtSingleMfAmt() {
        return lmtSingleMfAmt;
    }

    public void setLmtSingleMfAmt(BigDecimal lmtSingleMfAmt) {
        this.lmtSingleMfAmt = lmtSingleMfAmt;
    }

    public String getIsRevolv() {
        return isRevolv;
    }

    public void setIsRevolv(String isRevolv) {
        this.isRevolv = isRevolv;
    }

    public String getAccSubStatus() {
        return accSubStatus;
    }

    public void setAccSubStatus(String accSubStatus) {
        this.accSubStatus = accSubStatus;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    /**
     * 操作类型
     */
    @JsonProperty(value = "oprType")
    private String oprType;




    @Override
    public String toString() {
        return "CmisLmt0003LmtSubListReqDto{" +
                "accSubNo='" + accSubNo + '\'' +
                ", parentId='" + parentId + '\'' +
                ", limitSubNo='" + limitSubNo + '\'' +
                ", limitSubName='" + limitSubName + '\'' +
                ", avlamt=" + avlamt +
                ", curType='" + curType + '\'' +
                ", term=" + term +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", isIvlMf='" + isIvlMf + '\'' +
                ", lmtMfAmt=" + lmtMfAmt +
                ", lmtSingleMfAmt=" + lmtSingleMfAmt +
                ", isRevolv='" + isRevolv + '\'' +
                ", accSubStatus='" + accSubStatus + '\'' +
                ", oprType='" + oprType + '\'' +
                '}';
    }
}
