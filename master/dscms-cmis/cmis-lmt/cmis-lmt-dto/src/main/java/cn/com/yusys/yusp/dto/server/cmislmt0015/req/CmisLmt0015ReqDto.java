package cn.com.yusys.yusp.dto.server.cmislmt0015.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * 请求Dto：客户额度查询接口
 */             
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0015ReqDto {
	   private static final long serialVersionUID = 1L;
	 @JsonProperty(value = "serno")
	private String serno;//交易流水号
	 @JsonProperty(value = "instuCde")
	private String instuCde;//金融机构代码
	 @JsonProperty(value = "cusId")
	private String cusId;//客户编号
	 @JsonProperty(value = "cusName")
	private String cusName;//客户名称
	 @JsonProperty(value = "queryType")
	private String queryType;//查询类型
	 @JsonProperty(value = "cusType")
	private String cusType;//客户类型
	 @JsonProperty(value = "startNum")
	private Integer startNum;//起始记录数
	 @JsonProperty(value = "pageCount")
	private Integer pageCount;//本次查询条数

	public String getSerno() {
		return serno;
	}

	public void setSerno(String serno) {
		this.serno = serno;
	}

	public String getInstuCde() {
		return instuCde;
	}

	public void setInstuCde(String instuCde) {
		this.instuCde = instuCde;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getQueryType() {
		return queryType;
	}

	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}

	public String getCusType() {
		return cusType;
	}

	public void setCusType(String cusType) {
		this.cusType = cusType;
	}

	public Integer getStartNum() {
		return startNum;
	}

	public void setStartNum(Integer startNum) {
		this.startNum = startNum;
	}

	public Integer getPageCount() {
		return pageCount;
	}

	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	@Override
	public String toString() {
		return "CmisLmt0015ReqDto{" +
				"serno='" + serno + '\'' +
				", instuCde='" + instuCde + '\'' +
				", cusId='" + cusId + '\'' +
				", cusName='" + cusName + '\'' +
				", queryType='" + queryType + '\'' +
				", cusType='" + cusType + '\'' +
				", startNum=" + startNum +
				", pageCount=" + pageCount +
				'}';
	}
}
