package cn.com.yusys.yusp.dto.server.cmislmt0002.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * 集团成员额度批复列表
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0002LmtListReqDto {

    /**
     * 批复台账编号
     */
    @JsonProperty(value = "accNo")
    private String accNo;

    /**
     * 原批复台账编号
     */
    @JsonProperty(value = "origiAccNo")
    private String origiAccNo;

    /**
     * 客户编号
     */
    @JsonProperty(value = "cusId")
    private String cusId;

    /**
     * 客户名称
     */
    @JsonProperty(value = "cusName")
    private String cusName;

    /**
     * 客户主体类型
     */
    @JsonProperty(value = "cusType")
    private String cusType;

    /**
     * 是否生成新批复台账
     */
    @JsonProperty(value = "isCreateAcc")
    private String isCreateAcc;

    /**
     * 授信金额
     */
    @JsonProperty(value = "lmtAmt")
    private BigDecimal lmtAmt;

    /**
     * 授信期限
     */
    @JsonProperty(value = "term")
    private Integer term;

    /**
     * 起始日
     */
    @JsonProperty(value = "startDate")
    private String startDate;

    /**
     * 到期日
     */
    @JsonProperty(value = "endDate")
    private String endDate;

    /**
     * 币种
     */
    @JsonProperty(value = "curType")
    private String curType;

    /**
     * 批复台账状态
     */
    @JsonProperty(value = "accStatus")
    private String accStatus;

    /**
     * 授信模式
     */
    @JsonProperty(value = "lmtMode")
    private String lmtMode;

    /**
     * 责任人
     */
    @JsonProperty(value = "managerId")
    private String managerId;

    /**
     * 责任机构
     */
    @JsonProperty(value = "managerBrId")
    private String managerBrId;

    /**
     * 登记人
     */
    @JsonProperty(value = "inputId")
    private String inputId;

    /**
     * 登记机构
     */
    @JsonProperty(value = "inputBrId")
    private String inputBrId;

    /**
     * 登记日期
     */
    @JsonProperty(value = "inputDate")
    private String inputDate;

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getOrigiAccNo() {
        return origiAccNo;
    }

    public void setOrigiAccNo(String origiAccNo) {
        this.origiAccNo = origiAccNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getIsCreateAcc() {
        return isCreateAcc;
    }

    public void setIsCreateAcc(String isCreateAcc) {
        this.isCreateAcc = isCreateAcc;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getLmtMode() {
        return lmtMode;
    }

    public void setLmtMode(String lmtMode) {
        this.lmtMode = lmtMode;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    @Override
    public String toString() {
        return "CmisLmt0002LmtListReqDto{" +
                "accNo='" + accNo + '\'' +
                ", origiAccNo='" + origiAccNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", cusType='" + cusType + '\'' +
                ", isCreateAcc='" + isCreateAcc + '\'' +
                ", lmtAmt=" + lmtAmt +
                ", term='" + term + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", curType='" + curType + '\'' +
                ", accStatus='" + accStatus + '\'' +
                ", lmtMode='" + lmtMode + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                '}';
    }
}
