package cn.com.yusys.yusp.dto.server.cmislmt0013.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

/**
 * 请求Dto：台账占用接口
 */             
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0013ReqDealBizListDto {
	private static final long serialVersionUID = 1L;

	//台账编号
	@JsonProperty(value = "dealBizNo")
	private String dealBizNo ;

	//客户编号
	@JsonProperty(value = "cusId")
	private String cusId ;

	//客户名称
	@JsonProperty(value = "cusName")
	private String cusName ;

	//产品编号
	@JsonProperty(value = "prdId")
	private String prdId ;

	//产品名称
	@JsonProperty(value = "prdName")
	private String prdName ;

	//台账占用总额(原币)
	@JsonProperty(value = "dealBizAmt")
	private BigDecimal dealBizAmt ;

    //台账占用敞口（原币）
	@JsonProperty(value = "dealBizSpacAmt")
	private BigDecimal dealBizSpacAmt ;

	//台账占用总额（人民币）
	@JsonProperty(value = "dealBizAmtCny")
	private BigDecimal dealBizAmtCny ;

	//台账占用敞口（人民币）
	@JsonProperty(value = "dealBizSpacAmtCny")
	private BigDecimal dealBizSpacAmtCny ;

	//保证金比例
	@JsonProperty(value = "dealBizBailPreRate")
	private BigDecimal dealBizBailPreRate ;

	//保证金金额
	@JsonProperty(value = "dealBizBailPreAmt")
	private BigDecimal dealBizBailPreAmt ;

	//台账起始日
	@JsonProperty(value = "startDate")
	private String startDate ;

	//台账到期日
	@JsonProperty(value = "endDate")
	private String endDate ;

	//台账状态
	@JsonProperty(value = "dealBizStatus")
	private String dealBizStatus ;

	//是否无缝衔接
	@JsonProperty(value = "isFollowBiz")
	private String isFollowBiz;

	//原交易业务编号
	@JsonProperty(value = "origiDealBizNo")
	private String origiDealBizNo;

	//原交易业务恢复类型
	@JsonProperty(value = "origiRecoverType")
	private String origiRecoverType;

	//原交易业务状态
	@JsonProperty(value = "origiDealBizStatus")
	private String origiDealBizStatus;

	//原交易属性
	@JsonProperty(value = "origiBizAttr")
	private String origiBizAttr;

	/** 用信品种类型属性 **/
	@JsonProperty(value = "prdTypeProp")
	private String prdTypeProp;


	public String getDealBizNo() {
		return dealBizNo;
	}

	public void setDealBizNo(String dealBizNo) {
		this.dealBizNo = dealBizNo;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getPrdId() {
		return prdId;
	}

	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public BigDecimal getDealBizSpacAmt() {
		return dealBizSpacAmt;
	}

	public BigDecimal getDealBizBailPreRate() {
		return dealBizBailPreRate;
	}

	public void setDealBizBailPreRate(BigDecimal dealBizBailPreRate) {
		this.dealBizBailPreRate = dealBizBailPreRate;
	}

	public BigDecimal getDealBizBailPreAmt() {
		return dealBizBailPreAmt;
	}

	public void setDealBizBailPreAmt(BigDecimal dealBizBailPreAmt) {
		this.dealBizBailPreAmt = dealBizBailPreAmt;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDealBizStatus() {
		return dealBizStatus;
	}

	public void setDealBizStatus(String dealBizStatus) {
		this.dealBizStatus = dealBizStatus;
	}

	public BigDecimal getDealBizAmtCny() {
		return dealBizAmtCny;
	}

	public void setDealBizAmtCny(BigDecimal dealBizAmtCny) {
		this.dealBizAmtCny = dealBizAmtCny;
	}

	public BigDecimal getDealBizSpacAmtCny() {
		return dealBizSpacAmtCny;
	}

	public void setDealBizSpacAmtCny(BigDecimal dealBizSpacAmtCny) {
		this.dealBizSpacAmtCny = dealBizSpacAmtCny;
	}

	public String getIsFollowBiz() {
		return isFollowBiz;
	}

	public void setIsFollowBiz(String isFollowBiz) {
		this.isFollowBiz = isFollowBiz;
	}

	public String getOrigiDealBizNo() {
		return origiDealBizNo;
	}

	public void setOrigiDealBizNo(String origiDealBizNo) {
		this.origiDealBizNo = origiDealBizNo;
	}

	public String getOrigiRecoverType() {
		return origiRecoverType;
	}

	public void setOrigiRecoverType(String origiRecoverType) {
		this.origiRecoverType = origiRecoverType;
	}

	public String getOrigiDealBizStatus() {
		return origiDealBizStatus;
	}

	public void setOrigiDealBizStatus(String origiDealBizStatus) {
		this.origiDealBizStatus = origiDealBizStatus;
	}

	public String getOrigiBizAttr() {
		return origiBizAttr;
	}

	public void setOrigiBizAttr(String origiBizAttr) {
		this.origiBizAttr = origiBizAttr;
	}

	public String getPrdTypeProp() {
		return prdTypeProp;
	}

	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}

	@Override
	public String toString() {
		return "CmisLmt0013ReqDealBizListDto{" +
				"dealBizNo='" + dealBizNo + '\'' +
				", cusId='" + cusId + '\'' +
				", cusName='" + cusName + '\'' +
				", prdId='" + prdId + '\'' +
				", prdName='" + prdName + '\'' +
				", dealBizAmt=" + dealBizAmt +
				", dealBizSpacAmt=" + dealBizSpacAmt +
				", dealBizAmtCny=" + dealBizAmtCny +
				", dealBizSpacAmtCny=" + dealBizSpacAmtCny +
				", dealBizBailPreRate=" + dealBizBailPreRate +
				", dealBizBailPreAmt=" + dealBizBailPreAmt +
				", startDate='" + startDate + '\'' +
				", endDate='" + endDate + '\'' +
				", dealBizStatus='" + dealBizStatus + '\'' +
				", isFollowBiz='" + isFollowBiz + '\'' +
				", origiDealBizNo='" + origiDealBizNo + '\'' +
				", origiRecoverType='" + origiRecoverType + '\'' +
				", origiDealBizStatus='" + origiDealBizStatus + '\'' +
				", origiBizAttr='" + origiBizAttr + '\'' +
				", prdTypeProp='" + prdTypeProp + '\'' +
				'}';
	}
}
