package cn.com.yusys.yusp.dto.server.cmislmt0023.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：客户分类额度查询
 */
public class CmisLmt0023RespDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息
    @JsonProperty(value = "lmtAmt")
    private BigDecimal lmtAmt;//总授信额度
    @JsonProperty(value = "lmtValAmt")
    private BigDecimal lmtValAmt;//总授信可用额度

    /**
     * 授信额度列表
     */
    private List<CmisLmt0023LmtListRespDto> lmtList;

    public List<CmisLmt0023LmtListRespDto> getLmtList() {
        return lmtList;
    }

    public void setLmtList(List<CmisLmt0023LmtListRespDto> lmtList) {
        this.lmtList = lmtList;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    public BigDecimal getLmtValAmt() {
        return lmtValAmt;
    }

    public void setLmtValAmt(BigDecimal lmtValAmt) {
        this.lmtValAmt = lmtValAmt;
    }

    @Override
    public String toString() {
        return "CmisLmt0023RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", lmtAmt=" + lmtAmt +
                ", lmtValAmt=" + lmtValAmt +
                ", lmtList=" + lmtList +
                '}';
    }
}