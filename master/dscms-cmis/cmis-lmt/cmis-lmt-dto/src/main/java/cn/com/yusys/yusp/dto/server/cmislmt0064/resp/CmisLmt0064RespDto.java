package cn.com.yusys.yusp.dto.server.cmislmt0064.resp;

import cn.com.yusys.yusp.dto.server.cmislmt0063.resp.CmisLmt0063SubListRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0064RespDto implements Serializable {

    private static final long serialVersionUID = 4257512024639182666L;
    //结果代码
    @JsonProperty(value = "errorCode")
    private String errorCode;

    //结果信息
    @JsonProperty(value = "errorMsg")
    private String errorMsg;

    //授信余额
    @JsonProperty(value = "lmtAmt")
    private BigDecimal lmtAmt;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    @Override
    public String toString() {
        return "CmisLmt0064RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", lmtAmt='" + lmtAmt + '\'' +
                '}';
    }
}
