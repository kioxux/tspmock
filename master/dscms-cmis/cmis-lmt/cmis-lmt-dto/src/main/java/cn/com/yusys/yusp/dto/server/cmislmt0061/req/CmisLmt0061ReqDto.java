package cn.com.yusys.yusp.dto.server.cmislmt0061.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0061ReqDto implements Serializable {
    private static final long serialVersionUID = 5461506169208969818L;

    //金融组织代码
    @JsonProperty(value = "instuCde")
    private String instuCde;

    //查询类型
    @JsonProperty(value = "queryType")
    private String queryType;

    //客户号
    @JsonProperty(value = "cusId")
    private String cusId;

    //客户类型
    @JsonProperty(value = "cusType")
    private String cusType;

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }


    @Override
    public String toString() {
        return "CmisLmt0061ReqDto{" +
                "instuCde='" + instuCde + '\'' +
                ", queryType='" + queryType + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusType='" + cusType + '\'' +
                '}';
    }
}
