package cn.com.yusys.yusp.dto.server.cmislmt0065.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0065ReqDto implements Serializable {
    private static final long serialVersionUID = 5461506169208969818L;

    //客户编号
    @JsonProperty(value = "cusId")
    private String cusId;

    //品种编号
    @JsonProperty(value = "limitSubNo")
    private String limitSubNo;

    //品种编号
    @JsonProperty(value = "proNo")
    private String proNo;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    public String getProNo() {
        return proNo;
    }

    public void setProNo(String proNo) {
        this.proNo = proNo;
    }

    @Override
    public String toString() {
        return "CmisLmt0065ReqDto{" +
                "cusId='" + cusId + '\'' +
                ", limitSubNo='" + limitSubNo + '\'' +
                ", proNo='" + proNo + '\'' +
                '}';
    }
}
