package cn.com.yusys.yusp.dto.server.cmislmt0054.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0054ReqDto implements Serializable {
    private static final long serialVersionUID = 5461506169208969818L;

    @JsonProperty(value = "lmtType")
    private String lmtType;// 额度类型
    @JsonProperty(value = "instuCde")
    private String instuCde;// 金融机构代码
    @JsonProperty(value = "cusId")
    private String cusId;// 客户编号

    public String getLmtType() {
        return lmtType;
    }

    public void setLmtType(String lmtType) {
        this.lmtType = lmtType;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "CmisLmt0054ReqDto{" +
                "lmtType='" + lmtType + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", cusId='" + cusId + '\'' +
                '}';
    }
}
