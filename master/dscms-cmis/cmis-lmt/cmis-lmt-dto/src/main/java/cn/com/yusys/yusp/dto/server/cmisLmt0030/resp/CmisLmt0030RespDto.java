package cn.com.yusys.yusp.dto.server.cmisLmt0030.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * 查询客户标准资产授信余额 接口
 * add by dumd 20210616
 */
public class CmisLmt0030RespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode;
    @JsonProperty(value = "errorMsg")
    private String errorMsg;
    @JsonProperty(value = "lmtBalanceAmt")
    private BigDecimal lmtBalanceAmt;//授信余额

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public BigDecimal getLmtBalanceAmt() {
        return lmtBalanceAmt;
    }

    public void setLmtBalanceAmt(BigDecimal lmtBalanceAmt) {
        this.lmtBalanceAmt = lmtBalanceAmt;
    }

    @Override
    public String toString() {
        return "CmisLmt0030RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", lmtBalanceAmt=" + lmtBalanceAmt +
                '}';
    }
}