package cn.com.yusys.yusp.dto.server.cmislmt0009.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * 请求体中占用关系列表
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0009OccRelListReqDto {
    private static final long serialVersionUID = 1L;

    //额度类型
    @JsonProperty(value = "lmtType")
    @NotBlank(message = "额度类型不能为空")
    private String lmtType;

    //额度分项编号
    @JsonProperty(value = "lmtSubNo")
    @NotBlank(message = "额度分项编号不能为空")
    private String lmtSubNo;

    //占用总额
    @JsonProperty(value = "bizTotalAmt")
    @NotBlank(message = "占用总额不能为空")
    private BigDecimal bizTotalAmt;

    //占用敞口
    @JsonProperty(value = "bizSpacAmt")
    @NotBlank(message = "占用敞口不能为空")
    private BigDecimal bizSpacAmt;

    //是否专业担保公司
    @JsonProperty(value = "isProGuarCom")
    private String isProGuarCom;

    //专业担保公司客户号
    @JsonProperty(value = "guarCusid")
    private String guarCusid;

    /** 用信品种类型属性 **/
    @JsonProperty(value = "prdTypeProp")
    private String prdTypeProp;

    public String getLmtType() {
        return lmtType;
    }

    public void setLmtType(String lmtType) {
        this.lmtType = lmtType;
    }

    public String getLmtSubNo() {
        return lmtSubNo;
    }

    public void setLmtSubNo(String lmtSubNo) {
        this.lmtSubNo = lmtSubNo;
    }

    public BigDecimal getBizTotalAmt() {
        return bizTotalAmt;
    }

    public void setBizTotalAmt(BigDecimal bizTotalAmt) {
        this.bizTotalAmt = bizTotalAmt;
    }

    public BigDecimal getBizSpacAmt() {
        return bizSpacAmt;
    }

    public void setBizSpacAmt(BigDecimal bizSpacAmt) {
        this.bizSpacAmt = bizSpacAmt;
    }

    public String getIsProGuarCom() {
        return isProGuarCom;
    }

    public void setIsProGuarCom(String isProGuarCom) {
        this.isProGuarCom = isProGuarCom;
    }

    public String getGuarCusid() {
        return guarCusid;
    }

    public void setGuarCusid(String guarCusid) {
        this.guarCusid = guarCusid;
    }

    public String getPrdTypeProp() {
        return prdTypeProp;
    }

    public void setPrdTypeProp(String prdTypeProp) {
        this.prdTypeProp = prdTypeProp;
    }

    @Override
    public String toString() {
        return "CmisLmt0009OccRelListReqDto{" +
                "lmtType='" + lmtType + '\'' +
                ", lmtSubNo='" + lmtSubNo + '\'' +
                ", bizTotalAmt=" + bizTotalAmt +
                ", bizSpacAmt=" + bizSpacAmt +
                ", isProGuarCom='" + isProGuarCom + '\'' +
                ", guarCusid='" + guarCusid + '\'' +
                ", prdTypeProp='" + prdTypeProp + '\'' +
                '}';
    }
}
