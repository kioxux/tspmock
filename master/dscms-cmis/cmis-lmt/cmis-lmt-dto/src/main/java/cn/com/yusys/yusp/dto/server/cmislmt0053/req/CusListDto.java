package cn.com.yusys.yusp.dto.server.cmislmt0053.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class CusListDto implements Serializable {

    private static final long serialVersionUID = -5548726905751854655L;

    @JsonProperty(value = "cusId")
    private String cusId;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "CusListDto{" +
                "cusId='" + cusId + '\'' +
                '}';
    }
}
