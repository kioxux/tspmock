package cn.com.yusys.yusp.dto.server.cmisLmt0035.req;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CmisLmt0035ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "sysNo")
    private String sysNo;//系统编号

    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码

    @JsonProperty(value = "dealBizNo")
    private String dealBizNo;//台账编号

    @JsonProperty(value = "endDate")
    private String endDate;//到日期

    @JsonProperty(value = "inputId")
    private String inputId;//登记人

    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构

    @JsonProperty(value = "inputDate")
    private String inputDate;//等级日期

    public String getSysNo() {
        return sysNo;
    }

    public void setSysNo(String sysNo) {
        this.sysNo = sysNo;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    @Override
    public String toString() {
        return "CmisLmt0035ReqDto{" +
                "sysNo='" + sysNo + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", dealBizNo='" + dealBizNo + '\'' +
                ", endDate='" + endDate + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                '}';
    }
}
