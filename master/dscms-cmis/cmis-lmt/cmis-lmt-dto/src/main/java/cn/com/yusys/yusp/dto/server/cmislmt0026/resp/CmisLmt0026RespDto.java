package cn.com.yusys.yusp.dto.server.cmislmt0026.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * 响应Dto：额度分项信息查询
 */
public class CmisLmt0026RespDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "accNo")
    private String accNo;//批复编号
    @JsonProperty(value = "subSerno")
    private String subSerno;//分项编号
    @JsonProperty(value = "limitSubNo")
    private String limitSubNo;//授信品种编号
    @JsonProperty(value = "limitSubName")
    private String limitSubName;//授信品种名称
    @JsonProperty(value = "suitGuarWay")
    private String suitGuarWay;//担保方式
    @JsonProperty(value = "isLriskLmt")
    private String isLriskLmt;//是否低风险授信
    @JsonProperty(value = "cny")
    private String cny;//币种
    @JsonProperty(value = "isRevolv")
    private String isRevolv;//是否循环
    @JsonProperty(value = "isPreCrd")
    private String isPreCrd;//是否预授信
    @JsonProperty(value = "avlAmt")
    private BigDecimal avlAmt;//授信总额
    @JsonProperty(value = "avlOutstndAmt")
    private BigDecimal avlOutstndAmt;//授信总额已用
    @JsonProperty(value = "avlAvailAmt")
    private BigDecimal avlAvailAmt;//授信总额可用
    @JsonProperty(value = "spacAmt")
    private BigDecimal spacAmt;//授信敞口
    @JsonProperty(value = "spacOutstndAmt")
    private BigDecimal spacOutstndAmt;//授信敞口已用
    @JsonProperty(value = "spacAvailAmt")
    private BigDecimal spacAvailAmt;//授信敞口可用
    @JsonProperty(value = "startDate")
    private String startDate;//额度起始日
    @JsonProperty(value = "endDate")
    private String endDate;//额度到期日
    @JsonProperty(value = "term")
    private Integer term;//期限
    @JsonProperty(value = "status")
    private String status;//额度状态
    @JsonProperty(value = "rateYear")
    private BigDecimal rateYear;//年利率
    @JsonProperty(value = "bailPreRate")
    private BigDecimal bailPreRate;//保证金比例
    @JsonProperty(value = "valPvpAmt")
    private BigDecimal valPvpAmt;//可出账金额
    @JsonProperty(value = "lmtBizTypeProp")
    private String lmtBizTypeProp;//产品类型属性

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getSubSerno() {
        return subSerno;
    }

    public void setSubSerno(String subSerno) {
        this.subSerno = subSerno;
    }

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    public String getLimitSubName() {
        return limitSubName;
    }

    public void setLimitSubName(String limitSubName) {
        this.limitSubName = limitSubName;
    }

    public String getSuitGuarWay() {
        return suitGuarWay;
    }

    public void setSuitGuarWay(String suitGuarWay) {
        this.suitGuarWay = suitGuarWay;
    }

    public String getIsLriskLmt() {
        return isLriskLmt;
    }

    public void setIsLriskLmt(String isLriskLmt) {
        this.isLriskLmt = isLriskLmt;
    }

    public String getCny() {
        return cny;
    }

    public void setCny(String cny) {
        this.cny = cny;
    }

    public String getIsRevolv() {
        return isRevolv;
    }

    public void setIsRevolv(String isRevolv) {
        this.isRevolv = isRevolv;
    }

    public String getIsPreCrd() {
        return isPreCrd;
    }

    public void setIsPreCrd(String isPreCrd) {
        this.isPreCrd = isPreCrd;
    }

    public BigDecimal getAvlAmt() {
        return avlAmt;
    }

    public void setAvlAmt(BigDecimal avlAmt) {
        this.avlAmt = avlAmt;
    }

    public BigDecimal getAvlOutstndAmt() {
        return avlOutstndAmt;
    }

    public void setAvlOutstndAmt(BigDecimal avlOutstndAmt) {
        this.avlOutstndAmt = avlOutstndAmt;
    }

    public BigDecimal getAvlAvailAmt() {
        return avlAvailAmt;
    }

    public void setAvlAvailAmt(BigDecimal avlAvailAmt) {
        this.avlAvailAmt = avlAvailAmt;
    }

    public BigDecimal getSpacAmt() {
        return spacAmt;
    }

    public void setSpacAmt(BigDecimal spacAmt) {
        this.spacAmt = spacAmt;
    }

    public BigDecimal getSpacOutstndAmt() {
        return spacOutstndAmt;
    }

    public void setSpacOutstndAmt(BigDecimal spacOutstndAmt) {
        this.spacOutstndAmt = spacOutstndAmt;
    }

    public BigDecimal getSpacAvailAmt() {
        return spacAvailAmt;
    }

    public void setSpacAvailAmt(BigDecimal spacAvailAmt) {
        this.spacAvailAmt = spacAvailAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getRateYear() {
        return rateYear;
    }

    public void setRateYear(BigDecimal rateYear) {
        this.rateYear = rateYear;
    }

    public BigDecimal getBailPreRate() {
        return bailPreRate;
    }

    public void setBailPreRate(BigDecimal bailPreRate) {
        this.bailPreRate = bailPreRate;
    }

    public BigDecimal getValPvpAmt() {
        return valPvpAmt;
    }

    public void setValPvpAmt(BigDecimal valPvpAmt) {
        this.valPvpAmt = valPvpAmt;
    }

    public String getLmtBizTypeProp() {
        return lmtBizTypeProp;
    }

    public void setLmtBizTypeProp(String lmtBizTypeProp) {
        this.lmtBizTypeProp = lmtBizTypeProp;
    }

    @Override
    public String toString() {
        return "CmisLmt0026RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", cusId='" + cusId + '\'' +
                ", accNo='" + accNo + '\'' +
                ", subSerno='" + subSerno + '\'' +
                ", limitSubNo='" + limitSubNo + '\'' +
                ", limitSubName='" + limitSubName + '\'' +
                ", suitGuarWay='" + suitGuarWay + '\'' +
                ", isLriskLmt='" + isLriskLmt + '\'' +
                ", cny='" + cny + '\'' +
                ", isRevolv='" + isRevolv + '\'' +
                ", isPreCrd='" + isPreCrd + '\'' +
                ", avlAmt=" + avlAmt +
                ", avlOutstndAmt=" + avlOutstndAmt +
                ", avlAvailAmt=" + avlAvailAmt +
                ", spacAmt=" + spacAmt +
                ", spacOutstndAmt=" + spacOutstndAmt +
                ", spacAvailAmt=" + spacAvailAmt +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", term=" + term +
                ", status='" + status + '\'' +
                ", rateYear=" + rateYear +
                ", bailPreRate=" + bailPreRate +
                ", valPvpAmt=" + valPvpAmt +
                ", lmtBizTypeProp='" + lmtBizTypeProp + '\'' +
                '}';
    }
}