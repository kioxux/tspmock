package cn.com.yusys.yusp.dto.server.cmislmt0016.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * 请求Dto：客户额度查询接口
 */             
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0016ReqDto {
	   private static final long serialVersionUID = 1L;
	@JsonProperty(value = "serno")
	private String serno;//交易流水号
	@JsonProperty(value = "instuCde")
	private String instuCde;//金融机构代码
	@JsonProperty(value = "cusId")
	private String cusId;//合作方编号
	@JsonProperty(value = "cudName")
	private String cudName;//合作方名称
	@JsonProperty(value = "copType")
	private String copType;//合作方额度类型
	@JsonProperty(value = "accNo")
	private String accNo;//批复编号
	@JsonProperty(value = "subAccNo")
	private String subAccNo;//额度分项/项目编号
	@JsonProperty(value = "apprStatus")
	private String apprStatus;//批复状态
	@JsonProperty(value = "startNum")
	private Integer startNum;//起始记录数
	@JsonProperty(value = "pageCount")
	private Integer pageCount;//本次查询条数
	public String  getSerno() { return serno; }
	public void setSerno(String serno ) { this.serno = serno;}
	public String  getInstuCde() { return instuCde; }
	public void setInstuCde(String instuCde ) { this.instuCde = instuCde;}
	public String  getCusId() { return cusId; }
	public void setCusId(String cusId ) { this.cusId = cusId;}
	public String  getCudName() { return cudName; }
	public void setCudName(String cudName ) { this.cudName = cudName;}
	public String  getCopType() { return copType; }
	public void setCopType(String copType ) { this.copType = copType;}
	public String  getAccNo() { return accNo; }
	public void setAccNo(String accNo ) { this.accNo = accNo;}
	public String  getSubAccNo() { return subAccNo; }
	public void setSubAccNo(String subAccNo ) { this.subAccNo = subAccNo;}
	public String  getApprStatus() { return apprStatus; }
	public void setApprStatus(String apprStatus ) { this.apprStatus = apprStatus;}
	public Integer  getStartNum() { return startNum; }
	public void setStartNum(Integer startNum ) { this.startNum = startNum;}
	public Integer  getPageCount() { return pageCount; }
	public void setPageCount(Integer pageCount ) { this.pageCount = pageCount;}

	@Override
	public String toString() {
		return "CmisLmt0016ReqDto{" +
				"serno='" + serno + '\'' +
				", instuCde='" + instuCde + '\'' +
				", cusId='" + cusId + '\'' +
				", cudName='" + cudName + '\'' +
				", copType='" + copType + '\'' +
				", accNo='" + accNo + '\'' +
				", subAccNo='" + subAccNo + '\'' +
				", apprStatus='" + apprStatus + '\'' +
				", startNum=" + startNum +
				", pageCount=" + pageCount +
				'}';
	}
}
