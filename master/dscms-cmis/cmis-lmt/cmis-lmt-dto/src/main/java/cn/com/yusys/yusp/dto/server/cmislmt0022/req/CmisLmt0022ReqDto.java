package cn.com.yusys.yusp.dto.server.cmislmt0022.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * 请求Dto：客户分类额度查询
 */
public class CmisLmt0022ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "instuCde")
    //金融机构号
    private String instuCde;

    /**
     * 客户编号列表
     */
    private List<CmisLmt0022CusListReqDto> cusList;

    public List<CmisLmt0022CusListReqDto> getCusList() {
        return cusList;
    }

    public void setCusList(List<CmisLmt0022CusListReqDto> cusList) {
        this.cusList = cusList;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    @Override
    public String toString() {
        return "CmisLmt0022ReqDto{" +
                "instuCde='" + instuCde + '\'' +
                ", cusList='" + cusList + '\'' +
                '}';
    }
}