package cn.com.yusys.yusp.dto.server.cmislmt0045.resp;

import cn.com.yusys.yusp.dto.server.cmislmt0044.resp.CmisLmt0044ListRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CmisLmt0045RespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息
    @JsonProperty(value = "flag")
    private String flag;//是否存在未结清业务


    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "CmisLmt0045RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", flag='" + flag + '\'' +
                '}';
    }
}
