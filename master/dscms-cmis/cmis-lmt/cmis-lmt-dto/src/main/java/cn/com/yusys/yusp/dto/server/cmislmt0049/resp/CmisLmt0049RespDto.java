package cn.com.yusys.yusp.dto.server.cmislmt0049.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class CmisLmt0049RespDto implements Serializable {
    private static final long serialVersionUID = 4054524313598358964L;

    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    @JsonProperty(value = "orgId")
    private String orgId;

    /** 上月末对公贷款余额 **/
    @JsonProperty(value = "lastMonthComLoanBalance")
    private BigDecimal lastMonthComLoanBalance;

    /** 当月可净新增对公贷款投放金额 **/
    @JsonProperty(value = "currMonthAllowComAddAmt")
    private BigDecimal currMonthAllowComAddAmt;

    /** 上一日对公贷款余额 **/
    @JsonProperty(value = "lastDayComLoanBalance")
    private BigDecimal lastDayComLoanBalance;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public BigDecimal getLastMonthComLoanBalance() {
        return lastMonthComLoanBalance;
    }

    public void setLastMonthComLoanBalance(BigDecimal lastMonthComLoanBalance) {
        this.lastMonthComLoanBalance = lastMonthComLoanBalance;
    }

    public BigDecimal getCurrMonthAllowComAddAmt() {
        return currMonthAllowComAddAmt;
    }

    public void setCurrMonthAllowComAddAmt(BigDecimal currMonthAllowComAddAmt) {
        this.currMonthAllowComAddAmt = currMonthAllowComAddAmt;
    }

    public BigDecimal getLastDayComLoanBalance() {
        return lastDayComLoanBalance;
    }

    public void setLastDayComLoanBalance(BigDecimal lastDayComLoanBalance) {
        this.lastDayComLoanBalance = lastDayComLoanBalance;
    }

    @Override
    public String toString() {
        return "CmisLmt0049RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", orgId='" + orgId + '\'' +
                ", lastMonthComLoanBalance=" + lastMonthComLoanBalance +
                ", currMonthAllowComAddAmt=" + currMonthAllowComAddAmt +
                ", lastDayComLoanBalance=" + lastDayComLoanBalance +
                '}';
    }
}
