package cn.com.yusys.yusp.dto.server.cmislmt0009.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：合同校验接口
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0009ReqDto {
    private static final long serialVersionUID = 1L;

    //系统编号
    @JsonProperty(value = "sysId")
    @NotBlank(message = "系统编号不能为空")
    private String sysId;

    //金融机构代码
    @JsonProperty(value = "instuCde")
    @NotBlank(message = "金融机构代码不能为空")
    private String instuCde;

    //交易业务编号
    @JsonProperty(value = "dealBizNo")
    @NotBlank(message = "交易业务编号不能为空")
    private String dealBizNo;

    //客户编号
    @JsonProperty(value = "cusId")
    @NotBlank(message = "客户编号不能为空")
    private String cusId;

    //客户名称
    @JsonProperty(value = "cusName")
    @NotBlank(message = "客户名称不能为空")
    private String cusName;

    //交易业务类型
    @JsonProperty(value = "dealBizType")
    @NotBlank(message = "交易业务类型不能为空")
    private String dealBizType;

    //产品编号
    @JsonProperty(value = "prdId")
    private String prdId;

    //产品名称
    @JsonProperty(value = "prdName")
    private String prdName;

    //是否低风险业务
    @JsonProperty(value = "isLriskBiz")
    @NotBlank(message = "是否低风险业务不能为空")
    private String isLriskBiz;

    //是否无缝衔接
    @JsonProperty(value = "isFollowBiz")
    @NotBlank(message = "是否无缝衔接不能为空")
    private String isFollowBiz;

    //是否合同重签
    @JsonProperty(value = "isBizRev")
    @NotBlank(message = "是否合同重签不能为空")
    private String isBizRev;

    //交易属性
    @JsonProperty(value = "bizAttr")
    @NotBlank(message = "交易属性不能为空")
    private String bizAttr;

    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期

    //原交易业务编号
    @JsonProperty(value = "origiDealBizNo")
    private String origiDealBizNo;

    //原交易业务状态
    @JsonProperty(value = "origiDealBizStatus")
    private String origiDealBizStatus;

    //交易业务金额
    @JsonProperty(value = "dealBizAmt")
//    @NotBlank(message = "交易业务金额不能为空")
    private BigDecimal dealBizAmt;

    //保证金比例
    @JsonProperty(value = "dealBizBailPreRate")
    private BigDecimal dealBizBailPreRate;

    //保证金金额
    @JsonProperty(value = "dealBizBailPreAmt")
    private BigDecimal dealBizBailPreAmt;

    //合同起始日
    @JsonProperty(value = "startDate")
    private String startDate;

    //合同到期日
    @JsonProperty(value = "endDate")
    private String endDate;

    //占用额度列表
    @JsonProperty(value = "occRelList")
    private List<CmisLmt0009OccRelListReqDto> cmisLmt0009OccRelListReqDtoList;

    //原交易业务恢复类型
    @JsonProperty(value = "origiRecoverType")
    private String origiRecoverType;

    //原交易属性
    @JsonProperty(value = "origiBizAttr")
    private String origiBizAttr;

    //业务条线
    @JsonProperty(value = "belgLine")
    private String belgLine;

    //业务阶段类型
    @JsonProperty(value = "bussStageType")
    private String bussStageType;


    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getDealBizType() {
        return dealBizType;
    }

    public void setDealBizType(String dealBizType) {
        this.dealBizType = dealBizType;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getIsLriskBiz() {
        return isLriskBiz;
    }

    public void setIsLriskBiz(String isLriskBiz) {
        this.isLriskBiz = isLriskBiz;
    }

    public String getIsFollowBiz() {
        return isFollowBiz;
    }

    public void setIsFollowBiz(String isFollowBiz) {
        this.isFollowBiz = isFollowBiz;
    }

    public String getIsBizRev() {
        return isBizRev;
    }

    public void setIsBizRev(String isBizRev) {
        this.isBizRev = isBizRev;
    }

    public String getBizAttr() {
        return bizAttr;
    }

    public void setBizAttr(String bizAttr) {
        this.bizAttr = bizAttr;
    }

    public String getOrigiDealBizNo() {
        return origiDealBizNo;
    }

    public void setOrigiDealBizNo(String origiDealBizNo) {
        this.origiDealBizNo = origiDealBizNo;
    }

    public String getOrigiDealBizStatus() {
        return origiDealBizStatus;
    }

    public void setOrigiDealBizStatus(String origiDealBizStatus) {
        this.origiDealBizStatus = origiDealBizStatus;
    }

    public BigDecimal getDealBizAmt() {
        return dealBizAmt;
    }

    public void setDealBizAmt(BigDecimal dealBizAmt) {
        this.dealBizAmt = dealBizAmt;
    }

    public BigDecimal getDealBizBailPreRate() {
        return dealBizBailPreRate;
    }

    public void setDealBizBailPreRate(BigDecimal dealBizBailPreRate) {
        this.dealBizBailPreRate = dealBizBailPreRate;
    }

    public BigDecimal getDealBizBailPreAmt() {
        return dealBizBailPreAmt;
    }

    public void setDealBizBailPreAmt(BigDecimal dealBizBailPreAmt) {
        this.dealBizBailPreAmt = dealBizBailPreAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<CmisLmt0009OccRelListReqDto> getCmisLmt0009OccRelListReqDtoList() {
        return cmisLmt0009OccRelListReqDtoList;
    }

    public void setCmisLmt0009OccRelListReqDtoList(List<CmisLmt0009OccRelListReqDto> cmisLmt0009OccRelListReqDtoList) {
        this.cmisLmt0009OccRelListReqDtoList = cmisLmt0009OccRelListReqDtoList;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getOrigiRecoverType() {
        return origiRecoverType;
    }

    public void setOrigiRecoverType(String origiRecoverType) {
        this.origiRecoverType = origiRecoverType;
    }

    public String getOrigiBizAttr() {
        return origiBizAttr;
    }

    public void setOrigiBizAttr(String origiBizAttr) {
        this.origiBizAttr = origiBizAttr;
    }

    public String getBelgLine() {
        return belgLine;
    }

    public void setBelgLine(String belgLine) {
        this.belgLine = belgLine;
    }

    public String getBussStageType() {
        return bussStageType;
    }

    public void setBussStageType(String bussStageType) {
        this.bussStageType = bussStageType;
    }

    @Override
    public String toString() {
        return "CmisLmt0009ReqDto{" +
                "sysId='" + sysId + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", dealBizNo='" + dealBizNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", dealBizType='" + dealBizType + '\'' +
                ", prdId='" + prdId + '\'' +
                ", prdName='" + prdName + '\'' +
                ", isLriskBiz='" + isLriskBiz + '\'' +
                ", isFollowBiz='" + isFollowBiz + '\'' +
                ", isBizRev='" + isBizRev + '\'' +
                ", bizAttr='" + bizAttr + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", origiDealBizNo='" + origiDealBizNo + '\'' +
                ", origiDealBizStatus='" + origiDealBizStatus + '\'' +
                ", dealBizAmt=" + dealBizAmt +
                ", dealBizBailPreRate=" + dealBizBailPreRate +
                ", dealBizBailPreAmt=" + dealBizBailPreAmt +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", cmisLmt0009OccRelListReqDtoList=" + cmisLmt0009OccRelListReqDtoList +
                ", origiRecoverType='" + origiRecoverType + '\'' +
                ", origiBizAttr='" + origiBizAttr + '\'' +
                ", belgLine='" + belgLine + '\'' +
                ", bussStageType='" + bussStageType + '\'' +
                '}';
    }
}
