package cn.com.yusys.yusp.dto.server.cmislmt0038.req;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CmisLmt0038ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "limitSubNo")
    private String limitSubNo;//授信品种编号

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    @Override
    public String toString() {
        return "CmisLmt0038ReqDto{" +
                "limitSubNo='" + limitSubNo + '\'' +
                '}';
    }
}
