package cn.com.yusys.yusp.dto.server.cmislmt0051.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class LmtDiscOrgDto implements Serializable {

    private static final long serialVersionUID = -5548726905751854655L;

    @JsonProperty(value = "serno")
    private String serno;

    /** 申请年月 **/
    @JsonProperty(value = "appYm")
    private String appYm;

    /** 机构号 **/
    @JsonProperty(value = "organno")
    private String organno;

    /** 机构名称 **/
    @JsonProperty(value = "organname")
    private String organname;

    /** 申请额度 **/
    @JsonProperty(value = "applyAmount")
    private java.math.BigDecimal applyAmount;

    /** 核准额度 **/
    @JsonProperty(value = "approveAmount")
    private java.math.BigDecimal approveAmount;

    /** 已使用额度 **/
    @JsonProperty(value = "useAmt")
    private java.math.BigDecimal useAmt;

    /** 状态 **/
    @JsonProperty(value = "status")
    private String status;


    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getAppYm() {
        return appYm;
    }

    public void setAppYm(String appYm) {
        this.appYm = appYm;
    }

    public String getOrganno() {
        return organno;
    }

    public void setOrganno(String organno) {
        this.organno = organno;
    }

    public String getOrganname() {
        return organname;
    }

    public void setOrganname(String organname) {
        this.organname = organname;
    }

    public BigDecimal getApplyAmount() {
        return applyAmount;
    }

    public void setApplyAmount(BigDecimal applyAmount) {
        this.applyAmount = applyAmount;
    }

    public BigDecimal getApproveAmount() {
        return approveAmount;
    }

    public void setApproveAmount(BigDecimal approveAmount) {
        this.approveAmount = approveAmount;
    }

    public BigDecimal getUseAmt() {
        return useAmt;
    }

    public void setUseAmt(BigDecimal useAmt) {
        this.useAmt = useAmt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "LmtDiscOrgDto{" +
                "serno='" + serno + '\'' +
                ", appYm='" + appYm + '\'' +
                ", organno='" + organno + '\'' +
                ", organname='" + organname + '\'' +
                ", applyAmount=" + applyAmount +
                ", approveAmount=" + approveAmount +
                ", useAmt=" + useAmt +
                ", status='" + status + '\'' +
                '}';
    }
}
