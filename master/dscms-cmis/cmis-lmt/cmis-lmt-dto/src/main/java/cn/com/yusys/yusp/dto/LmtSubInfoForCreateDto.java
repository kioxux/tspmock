package cn.com.yusys.yusp.dto;

import java.math.BigDecimal;

public class LmtSubInfoForCreateDto {
	
	
	/** 额度结构编号 **/
	private String limitStrNo;
	
	/** 额度结构名称 **/
	private String limitStrName;
	
	
	private String pkId;
	
	private String fkPkId; //申请信息的话既然树都选了这些应该都是可以取到的
	
	
	
	private String limitSubNo;
	
	private String lmtSerno;
	
	private String lmtItemSerno;
	
	private String lmtUpItemSerno;
	
	private String lmtPrdCode;
	
	private String lmtPrdName;
	
	private String isRecycle;
	
	private String riskType;
	
	private String curType;
	
	private BigDecimal lmtAmt;
	
	private BigDecimal secRate;
	
	private String mainGuarWay;
	
	/**期限类型 **/
	private String lmtTermType;
	
	/**期限 **/
	private int lmtTerm;
	
	/**宽限期限类型 **/
	private String lmtExtendTermType;
	
	/**宽限期限 **/
	private int lmtExtendTerm;
	

	/** 额度占有类型  **/
	private String occupyType;
	
	/** 备注 **/
	private String remarks;

	/** 额度状态 **/
	private String status;

	public String getLmtSerno() {
		return lmtSerno;
	}

	public void setLmtSerno(String lmtSerno) {
		this.lmtSerno = lmtSerno;
	}

	public String getLmtItemSerno() {
		return lmtItemSerno;
	}

	public void setLmtItemSerno(String lmtItemSerno) {
		this.lmtItemSerno = lmtItemSerno;
	}

	public String getLmtUpItemSerno() {
		return lmtUpItemSerno;
	}

	public void setLmtUpItemSerno(String lmtUpItemSerno) {
		this.lmtUpItemSerno = lmtUpItemSerno;
	}

	public String getLmtPrdCode() {
		return lmtPrdCode;
	}

	public void setLmtPrdCode(String lmtPrdCode) {
		this.lmtPrdCode = lmtPrdCode;
	}

	public String getLmtPrdName() {
		return lmtPrdName;
	}

	public void setLmtPrdName(String lmtPrdName) {
		this.lmtPrdName = lmtPrdName;
	}

	public String getIsRecycle() {
		return isRecycle;
	}

	public void setIsRecycle(String isRecycle) {
		this.isRecycle = isRecycle;
	}

	public String getRiskType() {
		return riskType;
	}

	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}

	public String getCurType() {
		return curType;
	}

	public void setCurType(String curType) {
		this.curType = curType;
	}

	public BigDecimal getLmtAmt() {
		return lmtAmt;
	}

	public void setLmtAmt(BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}

	public BigDecimal getSecRate() {
		return secRate;
	}

	public void setSecRate(BigDecimal secRate) {
		this.secRate = secRate;
	}

	public String getMainGuarWay() {
		return mainGuarWay;
	}

	public void setMainGuarWay(String mainGuarWay) {
		this.mainGuarWay = mainGuarWay;
	}

	public String getLmtTermType() {
		return lmtTermType;
	}

	public void setLmtTermType(String lmtTermType) {
		this.lmtTermType = lmtTermType;
	}

	public int getLmtTerm() {
		return lmtTerm;
	}

	public void setLmtTerm(int lmtTerm) {
		this.lmtTerm = lmtTerm;
	}

	public String getLmtExtendTermType() {
		return lmtExtendTermType;
	}

	public void setLmtExtendTermType(String lmtExtendTermType) {
		this.lmtExtendTermType = lmtExtendTermType;
	}

	public int getLmtExtendTerm() {
		return lmtExtendTerm;
	}

	public void setLmtExtendTerm(int lmtExtendTerm) {
		this.lmtExtendTerm = lmtExtendTerm;
	}

	public String getOccupyType() {
		return occupyType;
	}

	public void setOccupyType(String occupyType) {
		this.occupyType = occupyType;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPkId() {
		return pkId;
	}

	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	public String getFkPkId() {
		return fkPkId;
	}

	public void setFkPkId(String fkPkId) {
		this.fkPkId = fkPkId;
	}

	public String getLimitStrNo() {
		return limitStrNo;
	}

	public void setLimitStrNo(String limitStrNo) {
		this.limitStrNo = limitStrNo;
	}

	public String getLimitStrName() {
		return limitStrName;
	}

	public void setLimitStrName(String limitStrName) {
		this.limitStrName = limitStrName;
	}

	public String getLimitSubNo() {
		return limitSubNo;
	}

	public void setLimitSubNo(String limitSubNo) {
		this.limitSubNo = limitSubNo;
	}
}
