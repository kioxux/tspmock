package cn.com.yusys.yusp.dto.server.cmislmt0050.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0050RespDto implements Serializable {
    private static final long serialVersionUID = 1774529983234642226L;

    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    @JsonProperty(value = "busiType")
    private String busiType;

    /** 机构区域类型 **/
    @JsonProperty(value = "orgAreaType")
    private String orgAreaType;

    /** 上月末贷款余额 **/
    @JsonProperty(value = "lastMonthLoanBalance")
    private BigDecimal lastMonthLoanBalance;

    /** 当月可净新增贷款投放金额 **/
    @JsonProperty(value = "currMonthAllowAddAmt")
    private BigDecimal currMonthAllowAddAmt;

    /** 上一日贷款余额 **/
    @JsonProperty(value = "lastDayLoanBalance")
    private BigDecimal lastDayLoanBalance;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getBusiType() {
        return busiType;
    }

    public void setBusiType(String busiType) {
        this.busiType = busiType;
    }

    public String getOrgAreaType() {
        return orgAreaType;
    }

    public void setOrgAreaType(String orgAreaType) {
        this.orgAreaType = orgAreaType;
    }

    public BigDecimal getLastMonthLoanBalance() {
        return lastMonthLoanBalance;
    }

    public void setLastMonthLoanBalance(BigDecimal lastMonthLoanBalance) {
        this.lastMonthLoanBalance = lastMonthLoanBalance;
    }

    public BigDecimal getCurrMonthAllowAddAmt() {
        return currMonthAllowAddAmt;
    }

    public void setCurrMonthAllowAddAmt(BigDecimal currMonthAllowAddAmt) {
        this.currMonthAllowAddAmt = currMonthAllowAddAmt;
    }

    public BigDecimal getLastDayLoanBalance() {
        return lastDayLoanBalance;
    }

    public void setLastDayLoanBalance(BigDecimal lastDayLoanBalance) {
        this.lastDayLoanBalance = lastDayLoanBalance;
    }

    @Override
    public String toString() {
        return "CmisLmt0050RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", busiType='" + busiType + '\'' +
                ", orgAreaType='" + orgAreaType + '\'' +
                ", lastMonthLoanBalance=" + lastMonthLoanBalance +
                ", currMonthAllowAddAmt=" + currMonthAllowAddAmt +
                ", lastDayLoanBalance=" + lastDayLoanBalance +
                '}';
    }
}
