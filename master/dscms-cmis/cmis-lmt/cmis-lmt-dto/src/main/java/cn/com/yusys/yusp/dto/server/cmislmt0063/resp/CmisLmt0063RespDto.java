package cn.com.yusys.yusp.dto.server.cmislmt0063.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0063RespDto implements Serializable {

    private static final long serialVersionUID = 4257512024639182666L;
    //结果代码
    @JsonProperty(value = "errorCode")
    private String errorCode;

    //结果信息
    @JsonProperty(value = "errorMsg")
    private String errorMsg;

    //分项列表
    @JsonProperty(value = "subList")
    private List<CmisLmt0063SubListRespDto> cmisLmt0063SubListRespDtoList;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<CmisLmt0063SubListRespDto> getCmisLmt0063SubListRespDtoList() {
        return cmisLmt0063SubListRespDtoList;
    }

    public void setCmisLmt0063SubListRespDtoList(List<CmisLmt0063SubListRespDto> cmisLmt0063SubListRespDtoList) {
        this.cmisLmt0063SubListRespDtoList = cmisLmt0063SubListRespDtoList;
    }

    @Override
    public String toString() {
        return "CmisLmt0063RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", cmisLmt0063SubListRespDtoList=" + cmisLmt0063SubListRespDtoList +
                '}';
    }
}
