package cn.com.yusys.yusp.dto.server.cmislmt0044.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CmisLmt0044RespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    private List<CmisLmt0044ListRespDto> lmtDiscOrgList;//机构贴现限额列表

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<CmisLmt0044ListRespDto> getLmtDiscOrgList() {
        return lmtDiscOrgList;
    }

    public void setLmtDiscOrgList(List<CmisLmt0044ListRespDto> lmtDiscOrgList) {
        this.lmtDiscOrgList = lmtDiscOrgList;
    }

    @Override
    public String toString() {
        return "CmisLmt0042RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", lmtDiscOrgList=" + lmtDiscOrgList +
                '}';
    }
}
