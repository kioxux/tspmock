package cn.com.yusys.yusp.dto.server.cmisLmt0029.req;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 更新台账编号 接口
 * add by dumd 20210615
 */
public class CmisLmt0029ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "dealBizNo")
    private String dealBizNo;//交易流水号
    @JsonProperty(value = "originAccNo")
    private String originAccNo;//原台账编号
    @JsonProperty(value = "newAccNo")
    private String newAccNo;//新台账编号
    @JsonProperty(value = "startDate")
    private String startDate;//合同起始日
    @JsonProperty(value = "endDate")
    private String endDate;//合同到期日
    @JsonProperty(value = "isPvpSucs")
    private String isPvpSucs;//是否出账成功通知

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    public String getOriginAccNo() {
        return originAccNo;
    }

    public void setOriginAccNo(String originAccNo) {
        this.originAccNo = originAccNo;
    }

    public String getNewAccNo() {
        return newAccNo;
    }

    public void setNewAccNo(String newAccNo) {
        this.newAccNo = newAccNo;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getIsPvpSucs() {
        return isPvpSucs;
    }

    public void setIsPvpSucs(String isPvpSucs) {
        this.isPvpSucs = isPvpSucs;
    }

    @Override
    public String toString() {
        return "CmisLmt0029ReqDto{" +
                "dealBizNo='" + dealBizNo + '\'' +
                ", originAccNo='" + originAccNo + '\'' +
                ", newAccNo='" + newAccNo + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", isPvpSucs='" + isPvpSucs + '\'' +
                '}';
    }
}
