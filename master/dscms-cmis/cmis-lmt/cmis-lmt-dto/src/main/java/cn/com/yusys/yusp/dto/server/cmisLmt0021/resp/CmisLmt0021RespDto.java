package cn.com.yusys.yusp.dto.server.cmisLmt0021.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * 响应Dto：查询对公客户授信信息
 */
public class CmisLmt0021RespDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "sumCrd")
    private BigDecimal sumCrd;//总额度
    @JsonProperty(value = "lmtStatus")
    private String lmtStatus;//额度状态
    @JsonProperty(value = "managerId")
    private String managerId;//管户经理
    @JsonProperty(value = "startDate")
    private String startDate;//额度起始日期
    @JsonProperty(value = "overDate")
    private String overDate;//额度到期日

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {this.cusId = cusId; }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {this.cusName = cusName; }

    public BigDecimal getSumCrd() {
        return sumCrd;
    }

    public void setSumCrd(BigDecimal sumCrd) {this.sumCrd = sumCrd; }

    public String getLmtStatus() {
        return lmtStatus;
    }

    public void setLmtStatus(String lmtStatus) {this.lmtStatus = lmtStatus; }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {this.managerId = managerId; }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {this.startDate = startDate; }

    public String getOverDate() {
        return overDate;
    }

    public void setOverDate(String overDate) {this.overDate = overDate; }

    @Override
    public String toString() {
        return "CmisLmt0021RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", sumCrd='" + sumCrd + '\'' +
                ", lmtStatus='" + lmtStatus + '\'' +
                ", managerId='" + managerId + '\'' +
                ", startDate='" + startDate + '\'' +
                ", overDate='" + overDate + '\'' +
                '}';
    }
}
