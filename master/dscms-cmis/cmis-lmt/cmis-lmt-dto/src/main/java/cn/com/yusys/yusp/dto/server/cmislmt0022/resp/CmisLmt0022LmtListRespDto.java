package cn.com.yusys.yusp.dto.server.cmislmt0022.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * 响应Dto：客户分类额度查询
 */
public class CmisLmt0022LmtListRespDto {
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "lmtCatalog")
    private String lmtCatalog;//额度统计类型
    @JsonProperty(value = "apprSubSerno")
    private String apprSubSerno;//额度分项流水号
    @JsonProperty(value = "lmtBizType")
    private String lmtBizType;//授信品种编号
    @JsonProperty(value = "lmtBizTypeName")
    private String lmtBizTypeName;//授信品种名称
    @JsonProperty(value = "ProName")
    private String ProName;//项目名称
    @JsonProperty(value = "lmtAmt")
    private BigDecimal lmtAmt;//授信金额
    @JsonProperty(value = "lmtBalanceAmt")
    private BigDecimal lmtBalanceAmt;//授信余额
    @JsonProperty(value = "outstandAmt")
    private BigDecimal outstandAmt;//已用额度
    @JsonProperty(value = "guarMode")
    private String guarMode;//担保方式
    @JsonProperty(value = "isRevolv")
    private String isRevolv;//是否循环
    @JsonProperty(value = "startDate")
    private String startDate;//起始日期
    @JsonProperty(value = "endDate")
    private String endDate;//到期日期
    @JsonProperty(value = "investAssetName")
    private String investAssetName;//投资资产名称
    @JsonProperty(value = "lmtType")
    private String lmtType;//授信类型
    //登记人
    @JsonProperty(value = "inputId")
    private String inputId;

    //登记机构
    @JsonProperty(value = "inputBrId")
    private String inputBrId;

    //登记日期
    @JsonProperty(value = "inputDate")
    private String inputDate;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getLmtCatalog() {
        return lmtCatalog;
    }

    public void setLmtCatalog(String lmtCatalog) {
        this.lmtCatalog = lmtCatalog;
    }

    public String getApprSubSerno() {
        return apprSubSerno;
    }

    public void setApprSubSerno(String apprSubSerno) {
        this.apprSubSerno = apprSubSerno;
    }

    public String getLmtBizType() {
        return lmtBizType;
    }

    public void setLmtBizType(String lmtBizType) {
        this.lmtBizType = lmtBizType;
    }

    public String getLmtBizTypeName() {
        return lmtBizTypeName;
    }

    public void setLmtBizTypeName(String lmtBizTypeName) {
        this.lmtBizTypeName = lmtBizTypeName;
    }

    public String getProName() {
        return ProName;
    }

    public void setProName(String proName) {
        ProName = proName;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    public BigDecimal getLmtBalanceAmt() {
        return lmtBalanceAmt;
    }

    public void setLmtBalanceAmt(BigDecimal lmtBalanceAmt) {
        this.lmtBalanceAmt = lmtBalanceAmt;
    }

    public BigDecimal getOutstandAmt() {
        return outstandAmt;
    }

    public void setOutstandAmt(BigDecimal outstandAmt) {
        this.outstandAmt = outstandAmt;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getIsRevolv() {
        return isRevolv;
    }

    public void setIsRevolv(String isRevolv) {
        this.isRevolv = isRevolv;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getInvestAssetName() {
        return investAssetName;
    }

    public void setInvestAssetName(String investAssetName) {
        this.investAssetName = investAssetName;
    }

    public String getLmtType() {
        return lmtType;
    }

    public void setLmtType(String lmtType) {
        this.lmtType = lmtType;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    @Override
    public String toString() {
        return "CmisLmt0022LmtListRespDto{" +
                "cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", lmtCatalog='" + lmtCatalog + '\'' +
                ", apprSubSerno='" + apprSubSerno + '\'' +
                ", lmtBizType='" + lmtBizType + '\'' +
                ", lmtBizTypeName='" + lmtBizTypeName + '\'' +
                ", ProName='" + ProName + '\'' +
                ", lmtAmt=" + lmtAmt +
                ", lmtBalanceAmt=" + lmtBalanceAmt +
                ", outstandAmt=" + outstandAmt +
                ", guarMode='" + guarMode + '\'' +
                ", isRevolv='" + isRevolv + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", investAssetName='" + investAssetName + '\'' +
                ", lmtType='" + lmtType + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                '}';
    }
}