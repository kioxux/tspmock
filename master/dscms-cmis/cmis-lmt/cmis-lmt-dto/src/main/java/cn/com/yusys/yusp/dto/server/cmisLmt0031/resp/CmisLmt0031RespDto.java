package cn.com.yusys.yusp.dto.server.cmisLmt0031.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 查询客户是否存在有效的非标额度 接口
 * add by macm 20210616
 */
public class CmisLmt0031RespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode;
    @JsonProperty(value = "errorMsg")
    private String errorMsg;
    @JsonProperty(value = "isExists")
    private String isExists;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getIsExists() {
        return isExists;
    }

    public void setIsExists(String isExists) {
        this.isExists = isExists;
    }

    @Override
    public String toString() {
        return "CmisLmt0031RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", isExists='" + isExists + '\'' +
                '}';
    }
}