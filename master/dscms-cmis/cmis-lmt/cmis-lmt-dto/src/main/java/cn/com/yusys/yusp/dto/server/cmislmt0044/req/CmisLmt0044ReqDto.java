package cn.com.yusys.yusp.dto.server.cmislmt0044.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;

public class CmisLmt0044ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "organno")           //机构号
    private String organno;

    @JsonProperty(value = "mmyy")           //年月
    private String mmyy;

    @JsonProperty(value = "openDay")           //申请日期
    private String openDay;

    public String getOrganno() {
        return organno;
    }

    public void setOrganno(String organno) {
        this.organno = organno;
    }

    public String getMmyy() {
        return mmyy;
    }

    public void setMmyy(String mmyy) {
        this.mmyy = mmyy;
    }

    public String getOpenDay() {
        return openDay;
    }

    public void setOpenDay(String openDay) {
        this.openDay = openDay;
    }

    @Override
    public String toString() {
        return "CmisLmt0044ReqDto{" +
                "organno='" + organno + '\'' +
                ", mmyy='" + mmyy + '\'' +
                ", openDay='" + openDay + '\'' +
                '}';
    }
}
