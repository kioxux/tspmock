package cn.com.yusys.yusp.dto.server.cmislmt0015.resp;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0015LmtSubAccListRespDto {

    /**
     * 客户编号
     */
    @JsonProperty(value = "cusId")
    private String cusId;

    /**
     * 批复台账编号
     */
    @JsonProperty(value = "accNo")
    private String accNo;

    /**
     * 父节点
     */
    @JsonProperty(value = "parentId")
    private String parentId;

    /**
     * 分项编号
     */
    @JsonProperty(value = "subSerno")
    private String subSerno;

    /**
     * 授信品种编号
     */
    @JsonProperty(value = "limitSubNo")
    private String limitSubNo;

    /**
     * 授信品种名称
     */
    @JsonProperty(value = "limitSubName")
    private String limitSubName;

    /**
     * 是否低风险授信
     */
    @JsonProperty(value = "isLriskLmt")
    private String isLriskLmt;

    /**
     * 币种
     */
    @JsonProperty(value = "cny")
    private String cny;

    /**
     * 是否循环
     */
    @JsonProperty(value = "isRevolv")
    private String isRevolv;

    /**
     * 是否预授信
     */
    @JsonProperty(value = "isPreCrd")
    private String isPreCrd;

    /**
     * 授信总额
     */
    @JsonProperty(value = "avlAmt")
    private BigDecimal avlAmt;

    /**
     * 授信总额已用
     */
    @JsonProperty(value = "avlOutstndAmt")
    private BigDecimal avlOutstndAmt;

    /**
     * 授信总额可用
     */
    @JsonProperty(value = "avlAvailAmt")
    private BigDecimal avlAvailAmt;

    /**
     * 授信敞口
     */
    @JsonProperty(value = "spacAmt")
    private BigDecimal spacAmt;

    /**
     * 授信敞口已用
     */
    @JsonProperty(value = "spacOutstndAmt")
    private BigDecimal spacOutstndAmt;

    /**
     * 授信敞口可用
     */
    @JsonProperty(value = "spacAvailAmt")
    private BigDecimal spacAvailAmt;

    /**
     * 额度起始日
     */
    @JsonProperty(value = "startDate")
    private String startDate;

    /**
     * 额度到期日
     */
    @JsonProperty(value = "endDate")
    private String endDate;


    /**
     * 期限
     */
    @JsonProperty(value = "term")
    private Integer term;

    /**
     * 额度状态
     */
    @JsonProperty(value = "status")
    private String status;

    /**
     * 用信余额
     */
    @JsonProperty(value = "loanBalance")
    private BigDecimal loanBalance;

    /**
     * 用信敞口余额
     */
    @JsonProperty(value = "loanSpacBalance")
    private BigDecimal loanSpacBalance;

    /**
     * 担保方式
     */
    @JsonProperty(value = "suitGuarWay")
    private String suitGuarWay;

    /**
     * 年利率
     */
    @JsonProperty(value = "rateYear")
    private BigDecimal rateYear;

    /**
     * 额度类型
     */
    @JsonProperty(value = "limitType")
    private String limitType;

    /**
     * 保证金比例
     */
    @JsonProperty(value = "dealBizBailPreRate")
    private BigDecimal dealBizBailPreRate;

    /**
     * 授信模式
     */
    @JsonProperty(value = "lmtMode")
    private String lmtMode;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getIsLriskLmt() {
        return isLriskLmt;
    }

    public void setIsLriskLmt(String isLriskLmt) {
        this.isLriskLmt = isLriskLmt;
    }

    public String getSuitGuarWay() {
        return suitGuarWay;
    }

    public void setSuitGuarWay(String suitGuarWay) {
        this.suitGuarWay = suitGuarWay;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getSubSerno() {
        return subSerno;
    }

    public void setSubSerno(String subSerno) {
        this.subSerno = subSerno;
    }

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    public String getLimitSubName() {
        return limitSubName;
    }

    public void setLimitSubName(String limitSubName) {
        this.limitSubName = limitSubName;
    }

    public String getCny() {
        return cny;
    }

    public void setCny(String cny) {
        this.cny = cny;
    }

    public String getIsRevolv() {
        return isRevolv;
    }

    public void setIsRevolv(String isRevolv) {
        this.isRevolv = isRevolv;
    }

    public String getIsPreCrd() {
        return isPreCrd;
    }

    public void setIsPreCrd(String isPreCrd) {
        this.isPreCrd = isPreCrd;
    }

    public BigDecimal getAvlAmt() {
        return avlAmt;
    }

    public void setAvlAmt(BigDecimal avlAmt) {
        this.avlAmt = avlAmt;
    }

    public BigDecimal getAvlOutstndAmt() {
        return avlOutstndAmt;
    }

    public void setAvlOutstndAmt(BigDecimal avlOutstndAmt) {
        this.avlOutstndAmt = avlOutstndAmt;
    }

    public BigDecimal getAvlAvailAmt() {
        return avlAvailAmt;
    }

    public void setAvlAvailAmt(BigDecimal avlAvailAmt) {
        this.avlAvailAmt = avlAvailAmt;
    }

    public BigDecimal getSpacAmt() {
        return spacAmt;
    }

    public void setSpacAmt(BigDecimal spacAmt) {
        this.spacAmt = spacAmt;
    }

    public BigDecimal getSpacOutstndAmt() {
        return spacOutstndAmt;
    }

    public void setSpacOutstndAmt(BigDecimal spacOutstndAmt) {
        this.spacOutstndAmt = spacOutstndAmt;
    }

    public BigDecimal getSpacAvailAmt() {
        return spacAvailAmt;
    }

    public void setSpacAvailAmt(BigDecimal spacAvailAmt) {
        this.spacAvailAmt = spacAvailAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public BigDecimal getLoanSpacBalance() {
        return loanSpacBalance;
    }

    public void setLoanSpacBalance(BigDecimal loanSpacBalance) {
        this.loanSpacBalance = loanSpacBalance;
    }

    public BigDecimal getRateYear() {
        return rateYear;
    }

    public void setRateYear(BigDecimal rateYear) {
        this.rateYear = rateYear;
    }

    public String getLimitType() {
        return limitType;
    }

    public void setLimitType(String limitType) {
        this.limitType = limitType;
    }

    public BigDecimal getDealBizBailPreRate() {
        return dealBizBailPreRate;
    }

    public void setDealBizBailPreRate(BigDecimal dealBizBailPreRate) {
        this.dealBizBailPreRate = dealBizBailPreRate;
    }

    public String getLmtMode() {
        return lmtMode;
    }

    public void setLmtMode(String lmtMode) {
        this.lmtMode = lmtMode;
    }

    @Override
    public String toString() {
        return "CmisLmt0015LmtSubAccListRespDto{" +
                "cusId='" + cusId + '\'' +
                ", accNo='" + accNo + '\'' +
                ", parentId='" + parentId + '\'' +
                ", subSerno='" + subSerno + '\'' +
                ", limitSubNo='" + limitSubNo + '\'' +
                ", limitSubName='" + limitSubName + '\'' +
                ", isLriskLmt='" + isLriskLmt + '\'' +
                ", cny='" + cny + '\'' +
                ", isRevolv='" + isRevolv + '\'' +
                ", isPreCrd='" + isPreCrd + '\'' +
                ", avlAmt=" + avlAmt +
                ", avlOutstndAmt=" + avlOutstndAmt +
                ", avlAvailAmt=" + avlAvailAmt +
                ", spacAmt=" + spacAmt +
                ", spacOutstndAmt=" + spacOutstndAmt +
                ", spacAvailAmt=" + spacAvailAmt +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", term=" + term +
                ", status='" + status + '\'' +
                ", loanBalance=" + loanBalance +
                ", loanSpacBalance=" + loanSpacBalance +
                ", suitGuarWay='" + suitGuarWay + '\'' +
                ", rateYear=" + rateYear +
                ", limitType='" + limitType + '\'' +
                ", dealBizBailPreRate=" + dealBizBailPreRate +
                ", lmtMode='" + lmtMode + '\'' +
                '}';
    }
}
