package cn.com.yusys.yusp.dto.server.cmislmt0047.resp;

import cn.com.yusys.yusp.dto.server.cmislmt0047.req.CmisLmt0047LmtSubDtoList;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CmisLmt0047RespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    //占用额度列表
    @JsonProperty(value = "CmisLmt0047ContRelDtoList")
    private List<CmisLmt0047ContRelDtoList> contRelDtoList ;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<CmisLmt0047ContRelDtoList> getContRelDtoList() {
        return contRelDtoList;
    }

    public void setContRelDtoList(List<CmisLmt0047ContRelDtoList> contRelDtoList) {
        this.contRelDtoList = contRelDtoList;
    }

    @Override
    public String toString() {
        return "CmisLmt0047RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", contRelDtoList=" + contRelDtoList +
                '}';
    }
}
