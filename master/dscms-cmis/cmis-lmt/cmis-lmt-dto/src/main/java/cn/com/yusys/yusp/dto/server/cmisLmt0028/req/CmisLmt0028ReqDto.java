package cn.com.yusys.yusp.dto.server.cmisLmt0028.req;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 删除额度分项数据 接口
 * add by dumd 20210615
 */
public class CmisLmt0028ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "apprSubSerno")
    private String apprSubSerno;//分项编号

    public String getApprSubSerno() {
        return apprSubSerno;
    }

    public void setApprSubSerno(String apprSubSerno) {
        this.apprSubSerno = apprSubSerno;
    }

    @Override
    public String toString() {
        return "CmisLmt0028ReqDto{" +
                "apprSubSerno='" + apprSubSerno + '\'' +
                '}';
    }
}
