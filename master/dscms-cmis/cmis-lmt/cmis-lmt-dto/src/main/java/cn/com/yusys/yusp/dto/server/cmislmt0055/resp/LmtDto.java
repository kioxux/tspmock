package cn.com.yusys.yusp.dto.server.cmislmt0055.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class LmtDto implements Serializable {

    private static final long serialVersionUID = -5548726905751854655L;

    @JsonProperty(value = "apprSerno")
    private String apprSerno;

    public String getApprSerno() {
        return apprSerno;
    }

    public void setApprSerno(String apprSerno) {
        this.apprSerno = apprSerno;
    }

    @Override
    public String toString() {
        return "LmtDto{" +
                "apprSerno='" + apprSerno + '\'' +
                '}';
    }
}
