package cn.com.yusys.yusp.dto.server.cmislmt0059.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0059ReqDto implements Serializable {
    private static final long serialVersionUID = 5461506169208969818L;

    //金融组织代码
    @JsonProperty(value = "instuCde")
    private String instuCde;

    //客户号
    @JsonProperty(value = "cusId")
    private String cusId;

    //分项明细编号
    @JsonProperty(value = "apprSubSerno")
    private String apprSubSerno;

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getApprSubSerno() {
        return apprSubSerno;
    }

    public void setApprSubSerno(String apprSubSerno) {
        this.apprSubSerno = apprSubSerno;
    }

    @Override
    public String toString() {
        return "CmisLmt0059ReqDto{" +
                "instuCde='" + instuCde + '\'' +
                ", cusId='" + cusId + '\'' +
                ", apprSubSerno='" + apprSubSerno + '\'' +
                '}';
    }
}
