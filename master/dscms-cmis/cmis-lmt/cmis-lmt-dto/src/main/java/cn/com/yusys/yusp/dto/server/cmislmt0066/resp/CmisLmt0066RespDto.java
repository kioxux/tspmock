package cn.com.yusys.yusp.dto.server.cmislmt0066.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0066RespDto implements Serializable {

    private static final long serialVersionUID = 4257512024639182666L;
    //结果代码
    @JsonProperty(value = "errorCode")
    private String errorCode;

    //结果信息
    @JsonProperty(value = "errorMsg")
    private String errorMsg;

    //明细编号
    @JsonProperty(value = "limitSubNo")
    private String limitSubNo;

    //分项编号
    @JsonProperty(value = "parentId")
    private String parentId;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }


    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        return "CmisLmt0066RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", limitSubNo='" + limitSubNo + '\'' +
                ", parentId='" + parentId + '\'' +
                '}';
    }
}
