package cn.com.yusys.yusp.dto.server.cmislmt0046.req;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CmisLmt0046ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "accSubNo")//分项编号
    private String accSubNo;

    @JsonProperty(value = "prdId") //用信产品编号
    private String prdId;

    public String getAccSubNo() {
        return accSubNo;
    }

    public void setAccSubNo(String accSubNo) {
        this.accSubNo = accSubNo;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    @Override
    public String toString() {
        return "CmisLmt0046ReqDto{" +
                "accSubNo='" + accSubNo + '\'' +
                ", prdId='" + prdId + '\'' +
                '}';
    }
}
