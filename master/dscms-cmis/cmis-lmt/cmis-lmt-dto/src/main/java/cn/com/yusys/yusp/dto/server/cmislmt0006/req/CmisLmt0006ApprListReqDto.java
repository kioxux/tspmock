package cn.com.yusys.yusp.dto.server.cmislmt0006.req;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * 单一额度批复列表
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0006ApprListReqDto {

    /**
     *客户编号
     */
    @JsonProperty(value = "cusId")
    private String cusId;

    /**
     *操作范围
     * 01-部分
     * 02-全部
     */
    @JsonProperty(value = "optType")
    private String optType;

    /**
     *批复台账编号
     */
    @JsonProperty(value = "apprSerno")
    private String apprSerno;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getOptType() {
        return optType;
    }

    public void setOptType(String optType) {
        this.optType = optType;
    }

    public String getApprSerno() {
        return apprSerno;
    }

    public void setApprSerno(String apprSerno) {
        this.apprSerno = apprSerno;
    }



    @Override
    public String toString() {
        return "CmisLmt0006ApprListReqDto{" +
                "cusId='" + cusId + '\'' +
                ", optType='" + optType + '\'' +
                ", apprSerno='" + apprSerno + '\'' +
                '}';
    }
}
