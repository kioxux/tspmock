package cn.com.yusys.yusp.dto.server.cmislmt0039.req;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CmisLmt0039ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "apprSerno")
    private String apprSerno;//授信品种编号

    public String getApprSerno() {
        return apprSerno;
    }

    public void setApprSerno(String apprSerno) {
        this.apprSerno = apprSerno;
    }

    @Override
    public String toString() {
        return "CmisLmt0039ReqDto{" +
                "apprSerno='" + apprSerno + '\'' +
                '}';
    }
}
