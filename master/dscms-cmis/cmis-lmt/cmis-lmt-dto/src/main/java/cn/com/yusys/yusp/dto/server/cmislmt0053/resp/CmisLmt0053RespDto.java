package cn.com.yusys.yusp.dto.server.cmislmt0053.resp;

import cn.com.yusys.yusp.dto.server.cmislmt0051.resp.LmtDiscOrgDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0053RespDto implements Serializable {

    private static final long serialVersionUID = 4257512024639182666L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息
    @JsonProperty(value = "spacBalanceAmt")
    private BigDecimal spacBalanceAmt;//敞口余额
    @JsonProperty(value = "spacLmtAmt")
    private BigDecimal spacLmtAmt;//敞口金额

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public BigDecimal getSpacBalanceAmt() {
        return spacBalanceAmt;
    }

    public void setSpacBalanceAmt(BigDecimal spacBalanceAmt) {
        this.spacBalanceAmt = spacBalanceAmt;
    }

    public BigDecimal getSpacLmtAmt() {
        return spacLmtAmt;
    }

    public void setSpacLmtAmt(BigDecimal spacLmtAmt) {
        this.spacLmtAmt = spacLmtAmt;
    }

    @Override
    public String toString() {
        return "CmisLmt0053RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", spacBalanceAmt=" + spacBalanceAmt +
                ", spacLmtAmt=" + spacLmtAmt +
                '}';
    }
}
