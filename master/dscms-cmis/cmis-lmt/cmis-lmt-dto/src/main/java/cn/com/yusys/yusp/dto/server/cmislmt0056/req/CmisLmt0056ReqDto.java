package cn.com.yusys.yusp.dto.server.cmislmt0056.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0056ReqDto implements Serializable {
    private static final long serialVersionUID = 5461506169208969818L;

    @JsonProperty(value = "cusId")
    private String cusId;// 客户编号

    @JsonProperty(value = "instuCde")
    private String instuCde;// 金融机构代码

    @JsonProperty(value = "prdId")
    private String prdId;// 用信产品编号

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    @Override
    public String toString() {
        return "CmisLmt0056ReqDto{" +
                "cusId='" + cusId + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", prdId='" + prdId + '\'' +
                '}';
    }
}
