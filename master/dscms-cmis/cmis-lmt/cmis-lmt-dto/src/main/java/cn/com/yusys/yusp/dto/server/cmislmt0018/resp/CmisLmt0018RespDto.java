package cn.com.yusys.yusp.dto.server.cmislmt0018.resp;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

/**
 * 响应Dto：额度结构查询
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0018RespDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息
    @JsonProperty(value = "cmisLmt0018StrListRespDto")
    private List<CmisLmt0018StrListRespDto> cmisLmt0018StrList;

    /**
     * 额度结构信息
     */
    private CmisLmt0018StrListRespDto[] supList;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<CmisLmt0018StrListRespDto> getCmisLmt0018StrList() {
        return cmisLmt0018StrList;
    }

    public void setCmisLmt0018StrList(List<CmisLmt0018StrListRespDto> cmisLmt0018StrList) {
        this.cmisLmt0018StrList = cmisLmt0018StrList;
    }

    @Override
    public String toString() {
        return "CmisLmt0018RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", cmisLmt0018StrList=" + cmisLmt0018StrList +
                '}';
    }
}
