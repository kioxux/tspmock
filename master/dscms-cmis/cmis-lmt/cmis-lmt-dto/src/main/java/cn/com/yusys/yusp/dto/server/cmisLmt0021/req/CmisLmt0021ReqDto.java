package cn.com.yusys.yusp.dto.server.cmisLmt0021.req;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 请求Dto：查询对公客户授信信息
 */
public class CmisLmt0021ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")
    private String cusId;//客户编号

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "CmisLmt0021ReqDto{" +
                ", cusId='" + cusId + '\'' +
                '}';
    }
}
