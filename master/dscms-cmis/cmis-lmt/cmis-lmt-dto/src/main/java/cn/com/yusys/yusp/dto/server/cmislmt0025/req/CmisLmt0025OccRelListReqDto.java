package cn.com.yusys.yusp.dto.server.cmislmt0025.req;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 响应Dto：占用额度列表
 */
public class CmisLmt0025OccRelListReqDto {
    @JsonProperty(value = "dealBizNo")
    private String dealBizNo;//交易业务编号

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    @Override
    public String toString() {
        return "CmisLmt0025OccRelListReqDto{" +
                "dealBizNo='" + dealBizNo + '\'' +
                '}';
    }
}
