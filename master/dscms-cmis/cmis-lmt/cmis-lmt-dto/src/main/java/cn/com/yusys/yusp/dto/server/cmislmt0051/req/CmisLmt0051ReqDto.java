package cn.com.yusys.yusp.dto.server.cmislmt0051.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0051ReqDto implements Serializable {
    private static final long serialVersionUID = 5461506169208969818L;

    @JsonProperty(value = "mmyy",required = true)
    private String mmyy;

    @JsonProperty(value = "organno",required = true)
    private String organno;

    @JsonProperty(value = "openDay",required = true)
    private String openDay;


    public String getMmyy() {
        return mmyy;
    }

    public void setMmyy(String mmyy) {
        this.mmyy = mmyy;
    }

    public String getOrganno() {
        return organno;
    }

    public void setOrganno(String organno) {
        this.organno = organno;
    }

    public String getOpenDay() {
        return openDay;
    }

    public void setOpenDay(String openDay) {
        this.openDay = openDay;
    }

    @Override
    public String toString() {
        return "CmisLmt0051ReqDto{" +
                "mmyy='" + mmyy + '\'' +
                ", organno='" + organno + '\'' +
                ", openDay='" + openDay + '\'' +
                '}';
    }
}
