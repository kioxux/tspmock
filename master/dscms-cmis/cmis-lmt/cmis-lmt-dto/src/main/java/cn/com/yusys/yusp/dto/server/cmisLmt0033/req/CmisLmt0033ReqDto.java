package cn.com.yusys.yusp.dto.server.cmisLmt0033.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * 国结票据出账额度校验
 * add by dumd 20210618
 */
public class CmisLmt0033ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "sysNo")
    private String sysNo;//系统编号
    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码
    @JsonProperty(value = "serno")
    private String serno;//交易流水号
    @JsonProperty(value = "bizNo")
    private String bizNo;//合同编号

    /**
     * 台账明细
     */
    private List<CmisLmt0033DealBizListReqDto> dealBizList;

    /**
     * 占用额度列表(同业客户额度)
     */
    private List<CmisLmt0033OccRelListReqDto> occRelList;

    public List<CmisLmt0033DealBizListReqDto> getDealBizList() {
        return dealBizList;
    }

    public void setDealBizList(List<CmisLmt0033DealBizListReqDto> dealBizList) {
        this.dealBizList = dealBizList;
    }

    public List<CmisLmt0033OccRelListReqDto> getOccRelList() {
        return occRelList;
    }

    public void setOccRelList(List<CmisLmt0033OccRelListReqDto> occRelList) {
        this.occRelList = occRelList;
    }

    public String getSysNo() {
        return sysNo;
    }

    public void setSysNo(String sysNo) {
        this.sysNo = sysNo;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getBizNo() {
        return bizNo;
    }

    public void setBizNo(String bizNo) {
        this.bizNo = bizNo;
    }

    @Override
    public String toString() {
        return "CmisLmt0033ReqDto{" +
                "sysNo='" + sysNo + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", serno='" + serno + '\'' +
                ", bizNo='" + bizNo + '\'' +
                '}';
    }
}
