package cn.com.yusys.yusp.dto.server.cmislmt0004.req;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0004StrOrgListReqDto {

    /**
     * 批复台账编号
     */
    @JsonProperty(value = "accNo")
    private String accNo;


    /**
     * 适用机构编号
     */
    @JsonProperty(value = "bchCde")
    private String bchCde;

    /**
     * 适用机构名称
     */
    @JsonProperty(value = "bchDesc")
    private String bchDesc;

    /**
     * 上级机构编号
     */
    @JsonProperty(value = "bchSupCde")
    private String bchSupCde;

    /**
     * 操作类型
     */
    @JsonProperty(value = "oprType")
    private String oprType;

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getBchCde() {
        return bchCde;
    }

    public void setBchCde(String bchCde) {
        this.bchCde = bchCde;
    }

    public String getBchDesc() {
        return bchDesc;
    }

    public void setBchDesc(String bchDesc) {
        this.bchDesc = bchDesc;
    }

    public String getBchSupCde() {
        return bchSupCde;
    }

    public void setBchSupCde(String bchSupCde) {
        this.bchSupCde = bchSupCde;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    @Override
    public String toString() {
        return "CmisLmt0004BchListReqDto{" +
                "accNo='" + accNo + '\'' +
                ", bchCde='" + bchCde + '\'' +
                ", bchDesc='" + bchDesc + '\'' +
                ", bchSupCde='" + bchSupCde + '\'' +
                ", oprType='" + oprType + '\'' +
                '}';
    }
}
