package cn.com.yusys.yusp.dto.server.cmisLmt0031.req;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 查询客户是否存在有效的非标额度 接口
 * add by macm 20210616
 */
public class CmisLmt0031ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "dealBizNo")
    private String dealBizNo;// 交易流水号
    @JsonProperty(value = "cusId")
    private String cusId;// 客户号
    @JsonProperty(value = "isQuryGrp")
    private String isQuryGrp;// 是否查询集团向下

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getIsQuryGrp() {
        return isQuryGrp;
    }

    public void setIsQuryGrp(String isQuryGrp) {
        this.isQuryGrp = isQuryGrp;
    }

    @Override
    public String toString() {
        return "CmisLmt0031ReqDto{" +
                "dealBizNo='" + dealBizNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", isQuryGrp='" + isQuryGrp + '\'' +
                '}';
    }
}
