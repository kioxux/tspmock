package cn.com.yusys.yusp.dto.server.cmislmt0023.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * 请求Dto：个人额度查询（新微贷）
 */
public class CmisLmt0023ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    //客户编号
    private String cusId;
    @JsonProperty(value = "lmtSubNo")
    //额度品种编号
    private String lmtSubNo;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getLmtSubNo() {
        return lmtSubNo;
    }

    public void setLmtSubNo(String lmtSubNo) {
        this.lmtSubNo = lmtSubNo;
    }

    @Override
    public String toString() {
        return "CmisLmt0023ReqDto{" +
                "cusId='" + cusId + '\'' +
                ", lmtSubNo='" + lmtSubNo + '\'' +
                '}';
    }
}