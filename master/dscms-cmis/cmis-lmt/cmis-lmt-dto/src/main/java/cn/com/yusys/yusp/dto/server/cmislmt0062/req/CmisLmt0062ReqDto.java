package cn.com.yusys.yusp.dto.server.cmislmt0062.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class CmisLmt0062ReqDto {

    @JsonProperty("custId")
    private String custId;

    @JsonProperty("riskType")
    private String riskType;

    @JsonProperty("dateDt")
    private String dateDt;

    @JsonProperty("lmtAmt")
    private BigDecimal lmtAmt;

    /**
     * 属于授信
     */
    @JsonProperty("belongSx")
    private String belongSx;

    /**
     * 属于同业授信
     */
    @JsonProperty("belongTysx")
    private String belongTysx;

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getRiskType() {
        return riskType;
    }

    public void setRiskType(String riskType) {
        this.riskType = riskType;
    }

    public String getDateDt() {
        return dateDt;
    }

    public void setDateDt(String dateDt) {
        this.dateDt = dateDt;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    public String getBelongSx() {
        return belongSx;
    }

    public void setBelongSx(String belongSx) {
        this.belongSx = belongSx;
    }

    public String getBelongTysx() {
        return belongTysx;
    }

    public void setBelongTysx(String belongTysx) {
        this.belongTysx = belongTysx;
    }

    @Override
    public String toString() {
        return "CmisLmt0062ReqDto{" +
                "custId='" + custId + '\'' +
                ", riskType='" + riskType + '\'' +
                ", dateDt='" + dateDt + '\'' +
                ", lmtAmt=" + lmtAmt +
                ", belongSx=" + belongSx +
                ", belongTysx=" + belongTysx +
                '}';
    }
}
