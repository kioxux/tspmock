package cn.com.yusys.yusp.dto.server.cmislmt0015.resp;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

/**
 * 响应Dto：客户额度查询接口
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0015RespDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    /**
     * 集团额度台账列表
     */
    private List<CmisLmt0015GrpLmtAccListRespDto> grpLmtAccList;

    public List<CmisLmt0015GrpLmtAccListRespDto> getGrpLmtAccList() {
        return grpLmtAccList;
    }

    public void setGrpLmtAccList(List<CmisLmt0015GrpLmtAccListRespDto> grpLmtAccList) {
        this.grpLmtAccList = grpLmtAccList;
    }

    /**
     * 客户额度台账列表
     */
    private List<CmisLmt0015LmtAccListRespDto> lmtAccList;

    public List<CmisLmt0015LmtAccListRespDto> getLmtAccList() {
        return lmtAccList;
    }

    public void setLmtAccList(List<CmisLmt0015LmtAccListRespDto> lmtAccList) {
        this.lmtAccList = lmtAccList;
    }

    /**
     * 客户额度分项列表
     */
    private List<CmisLmt0015LmtSubAccListRespDto> lmtSubAccList;

    public List<CmisLmt0015LmtSubAccListRespDto> getLmtSubAccList() {
        return lmtSubAccList;
    }

    public void setLmtSubAccList(List<CmisLmt0015LmtSubAccListRespDto> lmtSubAccList) {
        this.lmtSubAccList = lmtSubAccList;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        return "Cmislmt0015RespDto{" +
                "errorCode='" + errorCode + '\'' +
                "errorMsg='" + errorMsg + '\'' +
                '}';
    }
}
