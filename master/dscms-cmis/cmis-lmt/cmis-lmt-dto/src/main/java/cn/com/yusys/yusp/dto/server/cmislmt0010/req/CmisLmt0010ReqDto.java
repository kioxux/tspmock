package cn.com.yusys.yusp.dto.server.cmislmt0010.req;

import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDealBizListDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

/**
 * 请求Dto：合同校验接口
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0010ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "sysId")
    private String sysId;//系统编号
    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码
    @JsonProperty(value = "bizNo")
    private String bizNo;//合同编号
    //业务条线
    @JsonProperty(value = "belgLine")
    private String belgLine;

    private List<CmisLmt0010ReqDealBizListDto> cmisLmt0010ReqDealBizListDtoList ;

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getBizNo() {
        return bizNo;
    }

    public void setBizNo(String bizNo) {
        this.bizNo = bizNo;
    }

    public List<CmisLmt0010ReqDealBizListDto> getCmisLmt0010ReqDealBizListDtoList() {
        return cmisLmt0010ReqDealBizListDtoList;
    }

    public void setCmisLmt0010ReqDealBizListDtoList(List<CmisLmt0010ReqDealBizListDto> cmisLmt0010ReqDealBizListDtoList) {
        this.cmisLmt0010ReqDealBizListDtoList = cmisLmt0010ReqDealBizListDtoList;
    }

    public String getBelgLine() {
        return belgLine;
    }

    public void setBelgLine(String belgLine) {
        this.belgLine = belgLine;
    }

    @Override
    public String toString() {
        return "CmisLmt0010ReqDto{" +
                "sysId='" + sysId + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", bizNo='" + bizNo + '\'' +
                ", belgLine='" + belgLine + '\'' +
                ", cmisLmt0010ReqDealBizListDtoList=" + cmisLmt0010ReqDealBizListDtoList +
                '}';
    }
}
