package cn.com.yusys.yusp.dto.server.cmisLmt0032.req;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 查询客户授信余额（不包含穿透化额度）
 * add by dumd 20210617
 */
public class CmisLmt0032ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "dealBizNo")
    private String dealBizNo;// 交易流水号
    @JsonProperty(value = "cusId")
    private String cusId;// 客户号
    @JsonProperty(value = "isQuryGrp")
    private String isQuryGrp;// 是否查询集团项下
    @JsonProperty(value = "isQueryCth")
    private String isQueryCth;// 是否包含穿透化额度
    @JsonProperty(value = "isQueryDfx")
    private String isQueryDfx;// 是否包含低风险额度
    @JsonProperty(value = "isQueryWt")
    private String isQueryWt;// 是否包含委托贷款额度
    @JsonProperty(value = "instuCde")
    private String instuCde;// 金融机构代码

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getIsQuryGrp() {
        return isQuryGrp;
    }

    public void setIsQuryGrp(String isQuryGrp) {
        this.isQuryGrp = isQuryGrp;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getIsQueryCth() {
        return isQueryCth;
    }

    public void setIsQueryCth(String isQueryCth) {
        this.isQueryCth = isQueryCth;
    }

    public String getIsQueryDfx() {
        return isQueryDfx;
    }

    public void setIsQueryDfx(String isQueryDfx) {
        this.isQueryDfx = isQueryDfx;
    }

    public String getIsQueryWt() {
        return isQueryWt;
    }

    public void setIsQueryWt(String isQueryWt) {
        this.isQueryWt = isQueryWt;
    }

    @Override
    public String toString() {
        return "CmisLmt0032ReqDto{" +
                "dealBizNo='" + dealBizNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", isQuryGrp='" + isQuryGrp + '\'' +
                ", isQueryCth='" + isQueryCth + '\'' +
                ", isQueryDfx='" + isQueryDfx + '\'' +
                ", isQueryWt='" + isQueryWt + '\'' +
                ", instuCde='" + instuCde + '\'' +
                '}';
    }
}

