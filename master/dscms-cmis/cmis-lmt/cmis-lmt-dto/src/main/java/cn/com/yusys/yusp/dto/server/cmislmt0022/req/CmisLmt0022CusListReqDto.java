package cn.com.yusys.yusp.dto.server.cmislmt0022.req;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 请求Dto：客户列表
 */
public class CmisLmt0022CusListReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    //客户编号
    private String cusId;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "CmisLmt0022CusListReqDto{" +
                "cusId='" + cusId + '\'' +
                '}';
    }
}