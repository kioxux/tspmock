package cn.com.yusys.yusp.dto.server.cmislmt0007.req;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CmisLmt0007ApprListReqDto {

    /**
     *客户编号
     */
    @JsonProperty(value = "cusId")
    private String cusId;

    /**
     *操作范围
     * 01-部分
     * 02-全部
     */
    @JsonProperty(value = "optType")
    private String optType;

    /**
     *批复台账编号
     */
    @JsonProperty(value = "apprSerno")
    private String apprSerno;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getOptType() {
        return optType;
    }

    public void setOptType(String optType) {
        this.optType = optType;
    }

    public String getApprSerno() {
        return apprSerno;
    }

    public void setApprSerno(String apprSerno) {
        this.apprSerno = apprSerno;
    }



    @Override
    public String toString() {
        return "CmisLmt0007ApprListReqDto{" +
                "cusId='" + cusId + '\'' +
                ", optType='" + optType + '\'' +
                ", apprSerno='" + apprSerno + '\'' +
                '}';
    }
}
