package cn.com.yusys.yusp.service;


import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.LmtSubPrdMappConfDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0021.req.CmisLmt0021ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0021.resp.CmisLmt0021RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0027.req.CmisLmt0027ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0027.resp.CmisLmt0027RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0028.req.CmisLmt0028ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0028.resp.CmisLmt0028RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.req.CmisLmt0029ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.resp.CmisLmt0029RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0030.req.CmisLmt0030ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0030.resp.CmisLmt0030RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0031.req.CmisLmt0031ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0031.resp.CmisLmt0031RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0032.req.CmisLmt0032ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0032.resp.CmisLmt0032RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.req.CmisLmt0033ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.resp.CmisLmt0033RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.req.CmisLmt0034ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.resp.CmisLmt0034RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0035.req.CmisLmt0035ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0035.resp.CmisLmt0035RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0036.req.CmisLmt0036ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0036.resp.CmisLmt0036RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0037.req.CmisLmt0037ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0037.resp.CmisLmt0037RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.resp.CmisLmt0001RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0002.req.CmisLmt0002ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0002.resp.CmisLmt0002RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0003.req.CmisLmt0003ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0003.resp.CmisLmt0003RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0004.req.CmisLmt0004ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0004.resp.CmisLmt0004RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0005.req.CmisLmt0005ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0005.resp.CmisLmt0005RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0006.req.CmisLmt0006ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0006.resp.CmisLmt0006RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0007.req.CmisLmt0007ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0007.resp.CmisLmt0007RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.resp.CmisLmt0008RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.resp.CmisLmt0009RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.resp.CmisLmt0010RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.resp.CmisLmt0013RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.req.CmisLmt0015ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0016.req.CmisLmt0016ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0016.resp.CmisLmt0016RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0017.req.CmisLmt0017ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0017.resp.CmisLmt0017RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0018.req.CmisLmt0018ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0018.resp.CmisLmt0018RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0019.req.CmisLmt0019ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0019.resp.CmisLmt0019RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0020.req.CmisLmt0020ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0020.resp.CmisLmt0020RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0022.req.CmisLmt0022ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0022.resp.CmisLmt0022RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0023.req.CmisLmt0023ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0023.resp.CmisLmt0023RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0024.req.CmisLmt0024ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0024.resp.CmisLmt0024RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0025.req.CmisLmt0025ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0025.resp.CmisLmt0025RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.req.CmisLmt0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.resp.CmisLmt0026RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0038.req.CmisLmt0038ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0038.resp.CmisLmt0038RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0039.req.CmisLmt0039ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0039.resp.CmisLmt0039RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0040.req.CmisLmt0040ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0040.resp.CmisLmt0040RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0041.req.CmisLmt0041ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0041.resp.CmisLmt0041RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0042.req.CmisLmt0042ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0042.resp.CmisLmt0042RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0043.req.req.CmisLmt0043ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0043.req.resp.CmisLmt0043RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0044.req.CmisLmt0044ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0044.resp.CmisLmt0044RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0045.req.CmisLmt0045ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0045.resp.CmisLmt0045RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0046.req.CmisLmt0046ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0046.resp.CmisLmt0046RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0047.req.CmisLmt0047ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0047.resp.CmisLmt0047RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0048.req.CmisLmt0048ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0048.resp.CmisLmt0048RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0049.req.CmisLmt0049ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0049.resp.CmisLmt0049RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0050.req.CmisLmt0050ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0050.resp.CmisLmt0050RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0051.req.CmisLmt0051ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0051.resp.CmisLmt0051RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0052.req.CmisLmt0052ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0052.resp.CmisLmt0052RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0053.req.CmisLmt0053ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0053.resp.CmisLmt0053RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0054.req.CmisLmt0054ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0054.resp.CmisLmt0054RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0055.req.CmisLmt0055ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0055.resp.CmisLmt0055RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0056.req.CmisLmt0056ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0056.resp.CmisLmt0056RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0057.req.CmisLmt0057ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0057.resp.CmisLmt0057RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0058.req.CmisLmt0058ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0058.resp.CmisLmt0058RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0059.req.CmisLmt0059ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0059.resp.CmisLmt0059RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0060.req.CmisLmt0060ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0060.resp.CmisLmt0060RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0061.req.CmisLmt0061ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0061.resp.CmisLmt0061RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0062.req.CmisLmt0062ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0062.resp.CmisLmt0062RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0063.req.CmisLmt0063ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0063.resp.CmisLmt0063RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0064.req.CmisLmt0064ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0064.resp.CmisLmt0064RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0065.req.CmisLmt0065ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0065.resp.CmisLmt0065RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0066.req.CmisLmt0066ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0066.resp.CmisLmt0066RespDto;
import cn.com.yusys.yusp.service.impl.CmisLmtClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

@FeignClient(name = "cmis-lmt", path = "/api", fallback = CmisLmtClientServiceImpl.class)
public interface CmisLmtClientService {

    /**
     * 交易码：cmislmt0001
     * 交易描述：单一客户额度同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0001")
    public ResultDto<CmisLmt0001RespDto> cmisLmt0001(CmisLmt0001ReqDto reqDto);

    /**
     * 交易码：cmislmt0002
     * 交易描述：集团额度同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0002")
    public ResultDto<CmisLmt0002RespDto> cmisLmt0002(CmisLmt0002ReqDto reqDto);

    /**
     * 交易码：cmislmt0003
     * 交易描述：同业额度同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0003")
    public ResultDto<CmisLmt0003RespDto> cmisLmt0003(CmisLmt0003ReqDto reqDto);

    /**
     * 交易码：cmislmt0004
     * 交易描述：合作方额度同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0004")
    public ResultDto<CmisLmt0004RespDto> cmisLmt0004(CmisLmt0004ReqDto reqDto);

    /**
     * 交易码：cmislmt0005
     * 交易描述：资金业务额度同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0005")
    public ResultDto<CmisLmt0005RespDto> cmisLmt0005(CmisLmt0005ReqDto reqDto);

    /**
     * 交易码：cmislmt0006
     * 交易描述：额度冻结
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0006")
    public ResultDto<CmisLmt0006RespDto> cmisLmt0006(CmisLmt0006ReqDto reqDto);

    /**
     * 交易码：cmislmt0007
     * 交易描述：额度解冻
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0007")
    public ResultDto<CmisLmt0007RespDto> cmisLmt0007(CmisLmt0007ReqDto reqDto);

    /**
     * 交易码：cmislmt0008
     * 交易描述：额度提前终止
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0008")
    public ResultDto<CmisLmt0008RespDto> cmisLmt0008(CmisLmt0008ReqDto reqDto);


    /**
     * 交易码：cmislmt0009
     * 交易描述：占用分项校验
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0009")
    public ResultDto<CmisLmt0009RespDto> cmisLmt0009(CmisLmt0009ReqDto reqDto);

    /**
     * 交易码：cmislmt0010
     * 交易描述：合同下出账申请校验
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0010")
    public ResultDto<CmisLmt0010RespDto> cmisLmt0010(CmisLmt0010ReqDto reqDto);

    /**
     * 交易码：cmislmt0011
     * 交易描述：占用分项交易
     *
     * @param reqDto
     * @return
     */

    @PostMapping("/lmt4inner/cmislmt0011")
    public ResultDto<CmisLmt0011RespDto> cmisLmt0011(CmisLmt0011ReqDto reqDto);

    /**
     * 交易码：cmislmt0012
     * 交易描述：合同恢复
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0012")
    public ResultDto<CmisLmt0012RespDto> cmisLmt0012(CmisLmt0012ReqDto reqDto);

    /**
     * 交易码：cmislmt0013
     * 交易描述：合同下出账交易
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0013")
    public ResultDto<CmisLmt0013RespDto> cmisLmt0013(CmisLmt0013ReqDto reqDto);

    /**
     * 交易码：cmislmt0014
     * 交易描述：台账恢复
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0014")
    public ResultDto<CmisLmt0014RespDto> cmisLmt0014(CmisLmt0014ReqDto reqDto);

    /**
     * 交易码：cmislmt0015
     * 交易描述：客户额度查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0015")
    public ResultDto<CmisLmt0015RespDto> cmisLmt0015(CmisLmt0015ReqDto reqDto);

    /**
     * 交易码：cmislmt0016
     * 交易描述：合作方客户额度分项查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0016")
    public ResultDto<CmisLmt0016RespDto> cmisLmt0016(CmisLmt0016ReqDto reqDto);

    /**
     * 交易码：cmislmt0017
     * 交易描述：客户移交
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0017")
    public ResultDto<CmisLmt0017RespDto> cmisLmt0017(CmisLmt0017ReqDto reqDto);

    /**
     * 交易码：cmislmt0018
     * 交易描述：额度结构查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0018")
    public ResultDto<CmisLmt0018RespDto> cmisLmt0018(CmisLmt0018ReqDto reqDto);

    /**
     * 交易码：cmislmt0019
     * 交易描述：校验客户是否存在有效综合授信额度
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0019")
    public ResultDto<CmisLmt0019RespDto> cmisLmt0019(CmisLmt0019ReqDto reqDto);

    /**
     * 交易码：cmislmt0020
     * 交易描述：承兑行白名单额度查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0020")
    public ResultDto<CmisLmt0020RespDto> cmisLmt0020(CmisLmt0020ReqDto reqDto);

    /**
     * 交易码：cmislmt0021
     * 交易描述：查询对公客户授信信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0021")
    public ResultDto<CmisLmt0021RespDto> cmisLmt0021(CmisLmt0021ReqDto reqDto);

    /**
     * 交易码：cmislmt0022
     * 交易描述：客户分类额度查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0022")
    public ResultDto<CmisLmt0022RespDto> cmisLmt0022(CmisLmt0022ReqDto reqDto);

    /**
     * 交易码：cmislmt0023
     * 交易描述：个人额度查询（新微贷）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0023")
    public ResultDto<CmisLmt0023RespDto> cmislmt0023(CmisLmt0023ReqDto reqDto);

    /**
     * 交易码：cmislmt0024
     * 交易描述：承兑行白名单额度占用
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0024")
    public ResultDto<CmisLmt0024RespDto> cmislmt0024(CmisLmt0024ReqDto reqDto);

    /**
     * 交易码：cmislmt0025
     * 交易描述：承兑行白名单额度恢复
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0025")
    public ResultDto<CmisLmt0025RespDto> cmislmt0025(CmisLmt0025ReqDto reqDto);

    /**
     * 交易码：cmislmt0026
     * 交易描述：额度分项信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0026")
    public ResultDto<CmisLmt0026RespDto> cmislmt0026(CmisLmt0026ReqDto reqDto);

    /**
     * 交易码：cmislmt0027
     * 交易描述：合作方客户额度查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0027")
    public ResultDto<CmisLmt0027RespDto> cmislmt0027(CmisLmt0027ReqDto reqDto);

    /**
     * 交易码：cmislmt0028
     * 交易描述：删除额度分项数据
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0028")
    public ResultDto<CmisLmt0028RespDto> cmislmt0028(CmisLmt0028ReqDto reqDto);

    /**
     * 交易码：cmislmt0029
     * 交易描述：更新台账编号
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0029")
    public ResultDto<CmisLmt0029RespDto> cmislmt0029(CmisLmt0029ReqDto reqDto);

    /**
     * 交易码：cmislmt0030
     * 交易描述：查询客户标准资产授信余额
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0030")
    public ResultDto<CmisLmt0030RespDto> cmislmt0030(CmisLmt0030ReqDto reqDto);

    /**
     * 交易码：cmislmt0031
     * 交易描述：查询客户是否存在有效的非标额度
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0031")
    public ResultDto<CmisLmt0031RespDto> cmislmt0031(CmisLmt0031ReqDto reqDto);

    /**
     * 交易码：cmislmt0032
     * 交易描述：查询客户授信余额（不包含穿透化额度）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0032")
    public ResultDto<CmisLmt0032RespDto> cmislmt0032(CmisLmt0032ReqDto reqDto);

    /**
     * 交易码：cmislmt0033
     * 交易描述：国结票据出账额度校验
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0033")
    public ResultDto<CmisLmt0033RespDto> cmislmt0033(CmisLmt0033ReqDto reqDto);

    /**
     * 交易码：cmislmt0034
     * 交易描述：国结票据出账额度占用
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0034")
    public ResultDto<CmisLmt0034RespDto> cmislmt0034(CmisLmt0034ReqDto reqDto);

    /**
     * 交易码：cmislmt0035
     * 交易描述：用信到期日变更通知
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0035")
    public ResultDto<CmisLmt0035RespDto> cmislmt0035(CmisLmt0035ReqDto reqDto);

    /**
     * 交易码：cmislmt0036
     * 交易描述：根据分项信息，查询起始日到期日期限
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0036")
    public ResultDto<CmisLmt0036RespDto> cmislmt0036(CmisLmt0036ReqDto reqDto);

    /**
     * 交易码：cmislmt0037
     * 交易描述：根据分项信息，查询起始日到期日期限
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0037")
    public ResultDto<CmisLmt0037RespDto> cmislmt0037(CmisLmt0037ReqDto reqDto);
    /**
     * 交易码：cmislmt0038
     * 交易描述：新增接口【cmisLmt0038】根据额度品种编号查找适用产品编号
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0038")
    public ResultDto<CmisLmt0038RespDto> cmislmt0038(CmisLmt0038ReqDto reqDto);

    /**
     * 交易码：cmislmt0039
     * 交易描述：新增接口【cmisLmt0039】获取合作方额度台账项下总用信敞口余额
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0039")
    public ResultDto<CmisLmt0039RespDto> cmislmt0039(CmisLmt0039ReqDto reqDto);

    /**
     * 交易码：cmislmt0040
     * 交易描述：新增接口【cmisLmt0040】集团成员退出或集团解散
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0040")
    public ResultDto<CmisLmt0040RespDto> cmislmt0040(CmisLmt0040ReqDto reqDto);
    /**
     * 交易码：cmislmt0041
     * 交易描述：新增接口【cmisLmt0041】低风险额度反向生成
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0041")
    public ResultDto<CmisLmt0041RespDto> cmislmt0041(CmisLmt0041ReqDto reqDto);
    /**
     * 交易码：cmislmt0042
     * 交易描述：根据额度品种编号获取产品扩展属性
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0042")
    public ResultDto<CmisLmt0042RespDto> cmislmt0042(CmisLmt0042ReqDto reqDto);

    /**
     * 交易码：cmislmt0043
     * 交易描述：获取客户及其集团成员的单笔投资业务授信余额
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0043")
    public ResultDto<CmisLmt0043RespDto> cmislmt0043(CmisLmt0043ReqDto reqDto);

    /**
     * 交易码：cmislmt0044
     * 交易描述：查询机构贴现限额
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0044")
    public ResultDto<CmisLmt0044RespDto> cmislmt0044(CmisLmt0044ReqDto reqDto);

    /**
     * 交易码：selectPrdIdByLimitSubNo
     * 交易描述：根据产品编号查询适用额度品种编号
     * @param prdId
     * @return
     */
    @PostMapping("/lmtsubprdmappconf/selectLimitSubNoByPrdId")
    public ResultDto<List<LmtSubPrdMappConfDto>> selectLimitSubNoByPrdId(String prdId);

    /**
     * 交易码：cmislmt0045
     * 交易描述：校验客户合同项下是否存在未结清业务
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0045")
    public ResultDto<CmisLmt0045RespDto> cmislmt0045(CmisLmt0045ReqDto reqDto);

    /**
     * 交易码：cmislmt0046
     * 交易描述：根据分项编号和用信产品编号，查询分项向下是否存在适用改产品的明
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0046")
    public ResultDto<CmisLmt0046RespDto> cmislmt0046(CmisLmt0046ReqDto reqDto);

    /**
     * 交易码：cmislmt0046
     * 交易描述：根据分项编号，查询分项向下未结清的业务
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0047")
    public ResultDto<CmisLmt0047RespDto> cmislmt0047(CmisLmt0047ReqDto reqDto);

    /**
     * 交易码：cmislmt0048
     * 交易描述：根据分项编号，判断是否可以做最高额协议或一般最高额合同
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0048")
    public ResultDto<CmisLmt0048RespDto> cmislmt0048(CmisLmt0048ReqDto reqDto);


    /**
     * 交易码：cmislmt0049
     * 交易描述：根据机构编号获取分支机构额度管控信息
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0049")
    public ResultDto<CmisLmt0049RespDto> cmislmt0049(CmisLmt0049ReqDto reqDto);

    /**
     * 交易码：cmislmt0050
     * 交易描述：根据条线部门和区域获取业务条线额度管控信息
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0050")
    public ResultDto<CmisLmt0050RespDto> cmislmt0050(CmisLmt0050ReqDto reqDto);

    /**
     * 交易码：cmislmt0051
     * 交易描述：获取机构贴现限额管控信息
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0051")
    public ResultDto<CmisLmt0051RespDto> cmislmt0051(CmisLmt0051ReqDto reqDto);

    /**
     * 交易码：cmislmt0052
     * 交易描述：查询单一客户及所在集团存量敞口余额  （综合授信+主体授信）敞口用信余额，不包含穿透化额度、承销额度
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0052")
    public ResultDto<CmisLmt0052RespDto> cmislmt0052(CmisLmt0052ReqDto reqDto);

    /**
     * 交易码：cmislmt0053
     * 交易描述：查询多客户量敞口金额和敞口余额
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0053")
    public ResultDto<CmisLmt0053RespDto> cmislmt0053(CmisLmt0053ReqDto reqDto);

    /**
     * 交易码：cmislmt0054
     * 交易描述：查询客户授信总金额(不包含合作方)
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0054")
    public ResultDto<CmisLmt0054RespDto> cmislmt0054(CmisLmt0054ReqDto reqDto);

    /**
     * 交易码：cmislmt0055
     * 交易描述：获取客户有效的特定目的载体投资额度列表
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0055")
    public ResultDto<CmisLmt0055RespDto> cmislmt0055(CmisLmt0055ReqDto reqDto);

    /**
     * 交易码：cmislmt0056
     * 交易描述：获取客户有效的低风险额度分项明细编号
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0056")
    public ResultDto<CmisLmt0056RespDto> cmislmt0056(CmisLmt0056ReqDto reqDto);

    /**
     * 交易码：cmislmt0057
     * 交易描述：线上产品自动生成合同台账
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0057")
    public ResultDto<CmisLmt0057RespDto> cmislmt0057(CmisLmt0057ReqDto reqDto);

    /**
     * 交易码：cmislmt0058
     * 交易描述：线上产品自动生成合同台账
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0058")
    public ResultDto<CmisLmt0058RespDto> cmislmt0058(CmisLmt0058ReqDto reqDto);
    
    /**
     * 交易码：cmislmt0059
     * 交易描述：获取客户或分项明细用信综合总额和用信敞口余额（不包含合作方）
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0059")
    public ResultDto<CmisLmt0059RespDto> cmislmt0059(CmisLmt0059ReqDto reqDto);


    /**
     * 查询合作方额度视图
     * @param queryModel
     * @return
     */
    @PostMapping("/apprcoopinfo/queryListByInstuCde")
    public ResultDto<List<Map<String,Object>>> queryListByInstuCde(@RequestBody QueryModel queryModel);

    /**
     * 交易码：cmislmt0060
     * 交易描述：获取合同总已用及敞口已用金额
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0060")
    public ResultDto<CmisLmt0060RespDto> cmislmt0060(CmisLmt0060ReqDto reqDto);

    /**
     * 交易码：cmislmt0061
     * 交易描述：根据客户号获取客户综合授信批复编号，到期日，起始日，期限等
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0061")
    public ResultDto<CmisLmt0061RespDto> cmislmt0061(CmisLmt0061ReqDto reqDto);

    /**
     * 根据客户编号获取最新大额风险值
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0062")
    public ResultDto<CmisLmt0062RespDto> cmislmt0062(CmisLmt0062ReqDto reqDto);

    /**
     * 根据客户编号获取最新大额风险值
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0063")
    public ResultDto<CmisLmt0063RespDto> cmislmt0063(CmisLmt0063ReqDto reqDto);

    /**
     * 关联交易计算个人客户余额
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0064")
    public ResultDto<CmisLmt0064RespDto> cmislmt0064(CmisLmt0064ReqDto reqDto);

    /**
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0065")
    public ResultDto<CmisLmt0065RespDto> cmislmt0065(CmisLmt0065ReqDto reqDto);

    /**
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/lmt4inner/cmislmt0066")
    public ResultDto<CmisLmt0066RespDto> cmislmt0066(CmisLmt0066ReqDto reqDto);
}

