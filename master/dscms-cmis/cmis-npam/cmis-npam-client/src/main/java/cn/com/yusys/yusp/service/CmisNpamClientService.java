package cn.com.yusys.yusp.service;

import org.springframework.cloud.openfeign.FeignClient;

/**
 * cmis-npam服务对外提供服务接口
 *
 * @author monchi
 * @date 2020-12-19 11:50:00
 */
@FeignClient(name = "cmis-npam", path = "/api", fallback = CmisNpamClientServiceImpl.class)
public interface CmisNpamClientService {


}
