package cn.com.yusys.yusp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * cmis-npam服务对外提供服务接口实现类
 *
 * @author monchi
 * @date 2020-12-19 11:50:00
 */
@Component
public class CmisNpamClientServiceImpl implements CmisNpamClientService {

    private static final Logger logger = LoggerFactory.getLogger(CmisNpamClientService.class);


}
