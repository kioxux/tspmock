/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawCaseInfo
 * @类描述: pla_law_case_info数据实体类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-03 21:57:56
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_law_case_info")
public class PlaLawCaseInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 案件流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "CASE_SERNO")
    private String caseSerno;

    /**
     * 借款人客户编号
     **/
    @Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
    private String cusId;

    /**
     * 借款人客户名称
     **/
    @Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
    private String cusName;

    /**
     * 标的币种
     **/
    @Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
    private String curType;

    /**
     * 本金金额
     **/
    @Column(name = "CAP_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal capAmt;

    /**
     * 标的金额（元)
     **/
    @Column(name = "TOTAL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalAmt;

    /**
     * 拖欠利息总额（元)
     **/
    @Column(name = "TOTAL_TQLX_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalTqlxAmt;

    /**
     * 案由
     **/
    @Column(name = "CASE_REASON", unique = false, nullable = true, length = 5)
    private String caseReason;

    /**
     * 其他案由说明
     **/
    @Column(name = "OTHER_CASE_EXT", unique = false, nullable = true, length = 2000)
    private String otherCaseExt;

    /**
     * 保全情况
     **/
    @Column(name = "PRESERVE_CASE", unique = false, nullable = true, length = 5)
    private String preserveCase;

    /**
     * 保全日期
     **/
    @Column(name = "PRESERVE_DATE", unique = false, nullable = true, length = 10)
    private String preserveDate;

    /**
     * 立案日期
     **/
    @Column(name = "FILING_DATE", unique = false, nullable = true, length = 5)
    private String filingDate;

    /**
     * 案件编号
     **/
    @Column(name = "CASE_NO", unique = false, nullable = true, length = 40)
    private String caseNo;

    /**
     * 案件进程
     **/
    @Column(name = "CASE_PROCESS", unique = false, nullable = true, length = 5)
    private String caseProcess;

    /**
     * 我行身份
     **/
    @Column(name = "BANK_ROLE", unique = false, nullable = true, length = 5)
    private String bankRole;

    /**
     * 诉讼类型
     **/
    @Column(name = "LAWSUIT_TYPE", unique = false, nullable = true, length = 5)
    private String lawsuitType;

    /**
     * 我行涉诉机构
     **/
    @Column(name = "BANK_IVL_LAW_ORG", unique = false, nullable = true, length = 20)
    @RedisCacheTranslator(redisCacheKey = "orgName",refFieldName = "bankIvlLawOrgName")
    private String bankIvlLawOrg;

    /**
     * 当事人是否有其他涉诉
     **/
    @Column(name = "IS_PARTY_IVL_OTH_LAW", unique = false, nullable = true, length = 5)
    private String isPartyIvlOthLaw;

    /**
     * 诉讼请求
     **/
    @Column(name = "LAWSUIT_REQ", unique = false, nullable = true, length = 2000)
    private String lawsuitReq;

    /**
     * 案件基本情况
     **/
    @Column(name = "CASE_BASE", unique = false, nullable = true, length = 2000)
    private String caseBase;

    /**
     * 备注
     **/
    @Column(name = "MEMO", unique = false, nullable = true, length = 2000)
    private String memo;

    /**
     * 是否作废
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    /**
     * 责任人
     **/
    @Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
    private String managerId;

    /**
     * 责任机构
     **/
    @Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
    private String managerBrId;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaLawCaseInfo() {
        // default implementation ignored
    }

    /**
     * @param caseSerno
     */
    public void setCaseSerno(String caseSerno) {
        this.caseSerno = caseSerno;
    }

    /**
     * @return caseSerno
     */
    public String getCaseSerno() {
        return this.caseSerno;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param curType
     */
    public void setCurType(String curType) {
        this.curType = curType;
    }

    /**
     * @return curType
     */
    public String getCurType() {
        return this.curType;
    }

    /**
     * @param capAmt
     */
    public void setCapAmt(java.math.BigDecimal capAmt) {
        this.capAmt = capAmt;
    }

    /**
     * @return capAmt
     */
    public java.math.BigDecimal getCapAmt() {
        return this.capAmt;
    }

    /**
     * @param totalAmt
     */
    public void setTotalAmt(java.math.BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }

    /**
     * @return totalAmt
     */
    public java.math.BigDecimal getTotalAmt() {
        return this.totalAmt;
    }

    /**
     * @param totalTqlxAmt
     */
    public void setTotalTqlxAmt(java.math.BigDecimal totalTqlxAmt) {
        this.totalTqlxAmt = totalTqlxAmt;
    }

    /**
     * @return totalTqlxAmt
     */
    public java.math.BigDecimal getTotalTqlxAmt() {
        return this.totalTqlxAmt;
    }

    /**
     * @param caseReason
     */
    public void setCaseReason(String caseReason) {
        this.caseReason = caseReason;
    }

    /**
     * @return caseReason
     */
    public String getCaseReason() {
        return this.caseReason;
    }

    /**
     * @param otherCaseExt
     */
    public void setOtherCaseExt(String otherCaseExt) {
        this.otherCaseExt = otherCaseExt;
    }

    /**
     * @return otherCaseExt
     */
    public String getOtherCaseExt() {
        return this.otherCaseExt;
    }

    /**
     * @param preserveCase
     */
    public void setPreserveCase(String preserveCase) {
        this.preserveCase = preserveCase;
    }

    /**
     * @return preserveCase
     */
    public String getPreserveCase() {
        return this.preserveCase;
    }

    /**
     * @param preserveDate
     */
    public void setPreserveDate(String preserveDate) {
        this.preserveDate = preserveDate;
    }

    /**
     * @return preserveDate
     */
    public String getPreserveDate() {
        return this.preserveDate;
    }

    /**
     * @param filingDate
     */
    public void setFilingDate(String filingDate) {
        this.filingDate = filingDate;
    }

    /**
     * @return filingDate
     */
    public String getFilingDate() {
        return this.filingDate;
    }

    /**
     * @param caseNo
     */
    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    /**
     * @return caseNo
     */
    public String getCaseNo() {
        return this.caseNo;
    }

    /**
     * @param caseProcess
     */
    public void setCaseProcess(String caseProcess) {
        this.caseProcess = caseProcess;
    }

    /**
     * @return caseProcess
     */
    public String getCaseProcess() {
        return this.caseProcess;
    }

    /**
     * @param bankRole
     */
    public void setBankRole(String bankRole) {
        this.bankRole = bankRole;
    }

    /**
     * @return bankRole
     */
    public String getBankRole() {
        return this.bankRole;
    }

    /**
     * @param lawsuitType
     */
    public void setLawsuitType(String lawsuitType) {
        this.lawsuitType = lawsuitType;
    }

    /**
     * @return lawsuitType
     */
    public String getLawsuitType() {
        return this.lawsuitType;
    }

    /**
     * @param bankIvlLawOrg
     */
    public void setBankIvlLawOrg(String bankIvlLawOrg) {
        this.bankIvlLawOrg = bankIvlLawOrg;
    }

    /**
     * @return bankIvlLawOrg
     */
    public String getBankIvlLawOrg() {
        return this.bankIvlLawOrg;
    }

    /**
     * @param isPartyIvlOthLaw
     */
    public void setIsPartyIvlOthLaw(String isPartyIvlOthLaw) {
        this.isPartyIvlOthLaw = isPartyIvlOthLaw;
    }

    /**
     * @return isPartyIvlOthLaw
     */
    public String getIsPartyIvlOthLaw() {
        return this.isPartyIvlOthLaw;
    }

    /**
     * @param lawsuitReq
     */
    public void setLawsuitReq(String lawsuitReq) {
        this.lawsuitReq = lawsuitReq;
    }

    /**
     * @return lawsuitReq
     */
    public String getLawsuitReq() {
        return this.lawsuitReq;
    }

    /**
     * @param caseBase
     */
    public void setCaseBase(String caseBase) {
        this.caseBase = caseBase;
    }

    /**
     * @return caseBase
     */
    public String getCaseBase() {
        return this.caseBase;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    /**
     * @return managerId
     */
    public String getManagerId() {
        return this.managerId;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    /**
     * @return managerBrId
     */
    public String getManagerBrId() {
        return this.managerBrId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }
}