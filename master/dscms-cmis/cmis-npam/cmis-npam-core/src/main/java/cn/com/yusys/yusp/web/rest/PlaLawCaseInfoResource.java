package cn.com.yusys.yusp.web.rest;

import java.util.List;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.domain.PlaLawBillRel;
import cn.com.yusys.yusp.dto.LawCaseInfoDto;
import cn.com.yusys.yusp.dto.PlaLawRecoverInfoDto;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaLawCaseInfo;
import cn.com.yusys.yusp.service.PlaLawCaseInfoService;

@RestController
@RequestMapping("/api/plalawcaseinfo")
public class PlaLawCaseInfoResource {

    private static final Logger log = LoggerFactory.getLogger(PlaLawCaseInfoResource.class);
    @Autowired
    private PlaLawCaseInfoService plaLawCaseInfoService;


    @GetMapping("/")
    protected ResultDto<List<PlaLawCaseInfo>> index(QueryModel queryModel) {
        List<PlaLawCaseInfo> list = plaLawCaseInfoService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }


    @GetMapping("/{caseSerno}")
    protected ResultDto<PlaLawCaseInfo> show(@PathVariable("caseSerno") String caseSerno) {
        PlaLawCaseInfo caseInfo = plaLawCaseInfoService.selectByPrimaryKey(caseSerno);
        return new ResultDto<>(caseInfo);
    }


    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaLawCaseInfo plaLawCaseInfo) {
        int result = plaLawCaseInfoService.updateSelective(plaLawCaseInfo);
        return new ResultDto<>(result);
    }


    @PostMapping("/delete/{caseSerno}")
    protected ResultDto<Integer> delete(@PathVariable("caseSerno") String caseSerno) {
        PlaLawCaseInfo caseInfo = new PlaLawCaseInfo();
        caseInfo.setCaseSerno(caseSerno);
        //是否作废 1是0否
        caseInfo.setOprType("1");
        int result = plaLawCaseInfoService.updateSelective(caseInfo);
        return new ResultDto<>(result);
    }

    @PostMapping("/getBills")
    protected ResultDto<List<PlaLawBillRel>> getBills(@RequestBody QueryModel queryModel) {
        List<PlaLawBillRel> list = plaLawCaseInfoService.selectBillByModel(queryModel);
        return new ResultDto<>(list);
    }

    @PostMapping("/bathInsert/{caseSerno}")
    protected ResultDto<Integer> bathInsert(@PathVariable("caseSerno") String caseSerno, @RequestBody List<PlaLawBillRel> list) {
        plaLawCaseInfoService.bathInsert(caseSerno, list);
        return new ResultDto<>();
    }

    /**
     * 案件查询列表
     *
     * @param model
     * @return
     */
    @PostMapping("/queryCaseList")
    protected ResultDto<List<LawCaseInfoDto>> queryCaseList(@RequestBody QueryModel model) {
        List<LawCaseInfoDto> list = plaLawCaseInfoService.queryCaseList(model);
        return new ResultDto<>(list);
    }
    /**
     * 案件查询列表
     * @param queryModel
     * @return
     */
    @ApiOperation(value = "案件信息列表查询")
    @PostMapping("/queryPlaLawCaseInfoList")
    protected ResultDto<List<PlaLawCaseInfo>> queryPlaLawCaseInfoList(@RequestBody QueryModel queryModel) {
        List<PlaLawCaseInfo> list = plaLawCaseInfoService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }
    /**
     * 案件详细信息查询
     * @param
     * @return
     */
    @ApiOperation(value = "案件信息查询")
    @PostMapping("/queryPlaLawCaseInfo")
    protected ResultDto<PlaLawCaseInfo> queryPlaLawCaseInfo(@RequestBody PlaLawCaseInfo plaLawCaseInfo) {
        PlaLawCaseInfo caseInfo = plaLawCaseInfoService.selectByPrimaryKey(plaLawCaseInfo.getCaseSerno());
        return new ResultDto<>(caseInfo);
    }



    /**
     * 异步下载诉讼申请列表
     */
    @PostMapping("/exportPlaLawCase")
    public ResultDto<ProgressDto> asyncExportPlaLawCase(@RequestBody QueryModel model) {
        ProgressDto progressDto = plaLawCaseInfoService.asyncExportPlaLawCase(model);
        return ResultDto.success(progressDto);
    }

    /**
     * 查询回收情况
     * @param
     * @return
     */
    @ApiOperation(value = "查询回收情况")
    @PostMapping("/querRecoveryInfo")
    protected ResultDto<PlaLawRecoverInfoDto> querRecoveryInfo(@RequestBody QueryModel model) {
        PlaLawRecoverInfoDto plaLawRecoverInfoDto = plaLawCaseInfoService.querRecoveryInfo(model);
        return new ResultDto<PlaLawRecoverInfoDto>(plaLawRecoverInfoDto);
    }

}
