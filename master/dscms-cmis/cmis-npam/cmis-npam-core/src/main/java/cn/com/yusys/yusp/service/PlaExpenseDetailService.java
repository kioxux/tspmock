/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.PlaExpenseRegiInfo;
import cn.com.yusys.yusp.dto.PlaExpenseRegiInfoDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Count;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaExpenseDetail;
import cn.com.yusys.yusp.repository.mapper.PlaExpenseDetailMapper;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaExpenseDetailService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 15:20:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaExpenseDetailService {

    @Autowired
    private PlaExpenseDetailMapper plaExpenseDetailMapper;
    @Autowired
    private PlaExpenseRegiInfoService plaExpenseRegiInfoService;

	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PlaExpenseDetail selectByPrimaryKey(String pedSerno) {
        return plaExpenseDetailMapper.selectByPrimaryKey(pedSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaExpenseDetail> selectAll(QueryModel model) {
        List<PlaExpenseDetail> records = (List<PlaExpenseDetail>) plaExpenseDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PlaExpenseDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaExpenseDetail> list = plaExpenseDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PlaExpenseDetail record) {
        return plaExpenseDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PlaExpenseDetail record) {
        return plaExpenseDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PlaExpenseDetail record) {
        return plaExpenseDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PlaExpenseDetail record) {
        return plaExpenseDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pedSerno) {
        return plaExpenseDetailMapper.deleteByPrimaryKey(pedSerno);
    }


    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaExpenseDetailMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByPeriSerno
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaExpenseDetail> selectByPeriSerno(String periSerno) {
        List<PlaExpenseDetail> records = (List<PlaExpenseDetail>) plaExpenseDetailMapper.selectByPeriSerno(periSerno);
        return records;
    }

    /**
     * @方法名称: deleteByExpenseDetail
     * @方法描述: 删除更新金额
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByExpenseDetail(PlaExpenseDetail plaExpenseDetail) {
        int count = 0;

        if (plaExpenseDetail != null){
            count =  plaExpenseDetailMapper.deleteByPrimaryKey(plaExpenseDetail.getPedSerno());

            List<PlaExpenseDetail> plaExpenseDetailList = plaExpenseDetailMapper.selectByPeriSerno(plaExpenseDetail.getPeriSerno());
            // 费用总额
            BigDecimal expenseTotalAmt = new BigDecimal("0.00");
            // 列支总额
            BigDecimal rankAmt = new BigDecimal("0.00");
            // 垫支总额
            BigDecimal advanceExpenAmt = new BigDecimal("0.00");
            //垫支总额
            advanceExpenAmt=plaExpenseDetailList.stream().filter(plaExpenseDetails->{return "01".equals(plaExpenseDetails.getExpenseType());
            }).reduce(BigDecimal.ZERO,(sum,a)-> sum.add(a.getExpenseAmt()),BigDecimal::add);
            //列支总额
            rankAmt=plaExpenseDetailList.stream().filter(plaExpenseDetails->{return "02".equals(plaExpenseDetails.getExpenseType());
            }).reduce(BigDecimal.ZERO,(sum,a)-> sum.add(a.getExpenseAmt()),BigDecimal::add);
            expenseTotalAmt=rankAmt.add(advanceExpenAmt);
            PlaExpenseRegiInfo plaExpenseRegiInfo = new PlaExpenseRegiInfo();
            BeanUtils.copyProperties(plaExpenseDetail, plaExpenseRegiInfo);
            plaExpenseRegiInfo.setRankAmt(rankAmt);
            plaExpenseRegiInfo.setAdvanceExpenAmt(advanceExpenAmt);
            plaExpenseRegiInfo.setExpenseTotalAmt(expenseTotalAmt);

            count = plaExpenseRegiInfoService.updateSelective(plaExpenseRegiInfo);
        }


        return count;
    }
}
