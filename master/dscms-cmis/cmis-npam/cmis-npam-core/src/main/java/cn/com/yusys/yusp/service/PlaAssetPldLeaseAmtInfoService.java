/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.PlaAssetPldLeaseAmtInfo;
import cn.com.yusys.yusp.repository.mapper.PlaAssetPldLeaseAmtInfoMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldLeaseAmtInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-09 15:13:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaAssetPldLeaseAmtInfoService {

    @Autowired
    private PlaAssetPldLeaseAmtInfoMapper plaAssetPldLeaseAmtInfoMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaAssetPldLeaseAmtInfo selectByPrimaryKey(String paplaiSerno) {
        return plaAssetPldLeaseAmtInfoMapper.selectByPrimaryKey(paplaiSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaAssetPldLeaseAmtInfo> selectAll(QueryModel model) {
        List<PlaAssetPldLeaseAmtInfo> records = (List<PlaAssetPldLeaseAmtInfo>) plaAssetPldLeaseAmtInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaAssetPldLeaseAmtInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaAssetPldLeaseAmtInfo> list = plaAssetPldLeaseAmtInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaAssetPldLeaseAmtInfo record) {
        return plaAssetPldLeaseAmtInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaAssetPldLeaseAmtInfo record) {
        return plaAssetPldLeaseAmtInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaAssetPldLeaseAmtInfo record) {
        return plaAssetPldLeaseAmtInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaAssetPldLeaseAmtInfo record) {
        return plaAssetPldLeaseAmtInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String paplaiSerno) {
        return plaAssetPldLeaseAmtInfoMapper.deleteByPrimaryKey(paplaiSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaAssetPldLeaseAmtInfoMapper.deleteByIds(ids);
    }

    /**
     * 根据出租流水号获取租金收取情况信息
     *
     * @author jijian_yx
     * @date 2021/6/10 14:29
     **/
    public List<PlaAssetPldLeaseAmtInfo> queryAmtInfo(QueryModel model) {
        return plaAssetPldLeaseAmtInfoMapper.queryAmtInfo(model);
    }

    /**
     * 根据出租流水号删除租金信息
     *
     * @author jijian_yx
     * @date 2021/6/11 11:15
     **/
    public int deleteByPapliSerno(String papliSerno) {
        return plaAssetPldLeaseAmtInfoMapper.deleteByPapliSerno(papliSerno);
    }

    /**
     * 保存租金收取信息
     *
     * @author jijian_yx
     * @date 2021/6/11 13:28
     **/
    public int insertAmtInfoList(List<PlaAssetPldLeaseAmtInfo> amtInfoList) {
        int result = 0;
        for (PlaAssetPldLeaseAmtInfo amtInfo : amtInfoList) {
            // 判断当前行是否已存在
            PlaAssetPldLeaseAmtInfo leaseAmtInfo = selectByPrimaryKey(amtInfo.getPaplaiSerno());
            if (leaseAmtInfo != null) {
                // 更新操作
                result = updateSelective(amtInfo);
            } else {
                // 新增操作
                if (StringUtils.isEmpty(amtInfo.getPaplaiSerno())) {
                    // 关联表流水号
                    amtInfo.setPaplaiSerno(StringUtils.getUUID());
                }
                amtInfo.setInputDate(DateUtils.getCurrDateStr());
                result = insertSelective(amtInfo);
            }
        }
        return result;
    }
}
