/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgPlaBcmParam;
import cn.com.yusys.yusp.service.CfgPlaBcmParamService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: CfgPlaBcmParamResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 20:09:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "催收规则参数配表")
@RestController
@RequestMapping("/api/cfgplabcmparam")
public class CfgPlaBcmParamResource {
    @Autowired
    private CfgPlaBcmParamService cfgPlaBcmParamService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgPlaBcmParam>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgPlaBcmParam> list = cfgPlaBcmParamService.selectAll(queryModel);
        return new ResultDto<List<CfgPlaBcmParam>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgPlaBcmParam>> index(QueryModel queryModel) {
        List<CfgPlaBcmParam> list = cfgPlaBcmParamService.selectByModel(queryModel);
        return new ResultDto<List<CfgPlaBcmParam>>(list);
    }

    /**
     * @函数名称:queryByRuleId
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            根据规则编号分页查询全表数据
     * @算法描述:
     * 刘权
     */
    @ApiOperation("根据规则编号查询")
    @PostMapping("/queryByRuleId")
    protected ResultDto<List<CfgPlaBcmParam>> queryByRuleId(@RequestBody QueryModel queryModel) {
        List<CfgPlaBcmParam> list = cfgPlaBcmParamService.queryByRuleId(queryModel);
        return new ResultDto<List<CfgPlaBcmParam>>(list);
    }


    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{cpbpSerno}")
    protected ResultDto<CfgPlaBcmParam> show(@PathVariable("cpbpSerno") String cpbpSerno) {
        CfgPlaBcmParam cfgPlaBcmParam = cfgPlaBcmParamService.selectByPrimaryKey(cpbpSerno);
        return new ResultDto<CfgPlaBcmParam>(cfgPlaBcmParam);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgPlaBcmParam> create(@RequestBody CfgPlaBcmParam cfgPlaBcmParam) throws URISyntaxException {
        cfgPlaBcmParamService.insert(cfgPlaBcmParam);
        return new ResultDto<CfgPlaBcmParam>(cfgPlaBcmParam);
    }

    /**
     * @函数名称:insert
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * 催收规则参数配表新增
     * 刘权
     */
    @PostMapping("/insert")
    protected ResultDto<CfgPlaBcmParam> insert(@RequestBody CfgPlaBcmParam cfgPlaBcmParam) throws URISyntaxException {
        cfgPlaBcmParamService.insert(cfgPlaBcmParam);
        return new ResultDto<CfgPlaBcmParam>(cfgPlaBcmParam);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgPlaBcmParam cfgPlaBcmParam) throws URISyntaxException {
        int result = cfgPlaBcmParamService.update(cfgPlaBcmParam);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{cpbpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("cpbpSerno") String cpbpSerno) {
        int result = cfgPlaBcmParamService.deleteByPrimaryKey(cpbpSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleteByRuleId
     * @函数描述:根据规则编号删除
     * @参数与返回说明:
     * @算法描述:
     * 刘权
     */
    @PostMapping("/deleteByRuleId/{ruleId}")
    protected ResultDto<Integer> deleteByRuleId(@PathVariable("ruleId") String ruleId) {
        int result = cfgPlaBcmParamService.deleteByRuleId(ruleId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgPlaBcmParamService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
