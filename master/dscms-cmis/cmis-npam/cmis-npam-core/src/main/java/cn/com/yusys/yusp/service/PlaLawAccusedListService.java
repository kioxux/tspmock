/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.List;

import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.domain.PlaLawPersonInfo;
import com.alibaba.excel.util.DateUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaLawAccusedList;
import cn.com.yusys.yusp.repository.mapper.PlaLawAccusedListMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawAccusedListService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-05 17:45:58
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaLawAccusedListService {

    @Autowired
    private PlaLawAccusedListMapper plaLawAccusedListMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaLawAccusedList selectByPrimaryKey(String plalSerno) {
        return plaLawAccusedListMapper.selectByPrimaryKey(plalSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaLawAccusedList> selectAll(QueryModel model) {
        List<PlaLawAccusedList> records = (List<PlaLawAccusedList>) plaLawAccusedListMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaLawAccusedList> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaLawAccusedList> list = plaLawAccusedListMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaLawAccusedList record) {
        return plaLawAccusedListMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaLawAccusedList record) {
        return plaLawAccusedListMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaLawAccusedList record) {
        return plaLawAccusedListMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaLawAccusedList record) {
        return plaLawAccusedListMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String plalSerno) {
        return plaLawAccusedListMapper.deleteByPrimaryKey(plalSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaLawAccusedListMapper.deleteByIds(ids);
    }


    @Transactional(rollbackFor = Throwable.class)
    public void batchInsert(List<PlaLawAccusedList> list) {
        Date inputDate = new Date();
        for (PlaLawAccusedList accusedList : list) {
            accusedList.setUpdId(SessionUtils.getUserInformation().getLoginCode());
            accusedList.setUpdBrId(SessionUtils.getUserInformation().getOrg().getCode());
            accusedList.setUpdDate(DateUtils.format(inputDate, "yyyyMMdd"));
            accusedList.setCreateTime(inputDate);
            accusedList.setUpdateTime(inputDate);
            if (accusedList.getPlalSerno() == null || "".equals(accusedList.getPlalSerno())) {
                accusedList.setInputId(SessionUtils.getUserInformation().getLoginCode());
                accusedList.setInputBrId(SessionUtils.getUserInformation().getOrg().getCode());
                accusedList.setInputDate(DateUtils.format(inputDate, "yyyyMMdd"));
                plaLawAccusedListMapper.insert(accusedList);
            } else {
                plaLawAccusedListMapper.updateByPrimaryKey(accusedList);
            }
        }
    }

    /**
     * 批量更新
     * @param plaLawAccusedList
     * @return
     */

    public int batchUpdate(List<PlaLawAccusedList> plaLawAccusedList) {
        for (PlaLawAccusedList accusedList : plaLawAccusedList) {
            if (StringUtils.isEmpty(accusedList.getPlalSerno())) {
                plaLawAccusedListMapper.insertSelective(accusedList);
            } else {
                plaLawAccusedListMapper.updateByPrimaryKeySelective(accusedList);
            }
        }
        return plaLawAccusedList.size();
    }
}
