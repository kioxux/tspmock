/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBcmRel
 * @类描述: pla_bcm_rel数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-04 11:12:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_bcm_rel")
public class PlaBcmRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 催收业务流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PBRC_SERNO")
    private String pbrcSerno;

    /**
     * 任务编号
     **/
    @Column(name = "TASK_NO", unique = false, nullable = true, length = 40)
    private String taskNo;

    /**
     * 借据编号
     **/
    @Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
    private String billNo;

    /**
     * 合同编号
     **/
    @Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
    private String contNo;

    /**
     * 产品名称
     **/
    @Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
    private String prdName;

    /**
     * 正常本金
     **/
    @Column(name = "ZCBJ_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal zcbjAmt;

    /**
     * 逾期本金
     **/
    @Column(name = "OVERDUE_CAP_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal overdueCapAmt;

    /**
     * 拖欠利息总额
     **/
    @Column(name = "TOTAL_TQLX_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalTqlxAmt;

    /**
     * 欠息
     **/
    @Column(name = "DEBIT_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal debitInt;

    /**
     * 罚息
     **/
    @Column(name = "PENAL_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal penalInt;

    /**
     * 复息
     **/
    @Column(name = "COMPOUND_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal compoundInt;

    /**
     * 逾期天数
     **/
    @Column(name = "OVERDUE_DAY", unique = false, nullable = true, length = 10)
    private String overdueDay;

    /**
     * 五级分类
     **/
    @Column(name = "FIVE_CLASS", unique = false, nullable = true, length = 5)
    private String fiveClass;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaBcmRel() {
        // default implementation ignored
    }

    /**
     * @param pbrcSerno
     */
    public void setPbrcSerno(String pbrcSerno) {
        this.pbrcSerno = pbrcSerno;
    }

    /**
     * @return pbrcSerno
     */
    public String getPbrcSerno() {
        return this.pbrcSerno;
    }

    /**
     * @param taskNo
     */
    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    /**
     * @return taskNo
     */
    public String getTaskNo() {
        return this.taskNo;
    }

    /**
     * @param billNo
     */
    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    /**
     * @return billNo
     */
    public String getBillNo() {
        return this.billNo;
    }

    /**
     * @param contNo
     */
    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    /**
     * @return contNo
     */
    public String getContNo() {
        return this.contNo;
    }

    /**
     * @param prdName
     */
    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    /**
     * @return prdName
     */
    public String getPrdName() {
        return this.prdName;
    }

    /**
     * @param zcbjAmt
     */
    public void setZcbjAmt(java.math.BigDecimal zcbjAmt) {
        this.zcbjAmt = zcbjAmt;
    }

    /**
     * @return zcbjAmt
     */
    public java.math.BigDecimal getZcbjAmt() {
        return this.zcbjAmt;
    }

    /**
     * @param overdueCapAmt
     */
    public void setOverdueCapAmt(java.math.BigDecimal overdueCapAmt) {
        this.overdueCapAmt = overdueCapAmt;
    }

    /**
     * @return overdueCapAmt
     */
    public java.math.BigDecimal getOverdueCapAmt() {
        return this.overdueCapAmt;
    }

    /**
     * @param totalTqlxAmt
     */
    public void setTotalTqlxAmt(java.math.BigDecimal totalTqlxAmt) {
        this.totalTqlxAmt = totalTqlxAmt;
    }

    /**
     * @return totalTqlxAmt
     */
    public java.math.BigDecimal getTotalTqlxAmt() {
        return this.totalTqlxAmt;
    }

    /**
     * @param debitInt
     */
    public void setDebitInt(java.math.BigDecimal debitInt) {
        this.debitInt = debitInt;
    }

    /**
     * @return debitInt
     */
    public java.math.BigDecimal getDebitInt() {
        return this.debitInt;
    }

    /**
     * @param penalInt
     */
    public void setPenalInt(java.math.BigDecimal penalInt) {
        this.penalInt = penalInt;
    }

    /**
     * @return penalInt
     */
    public java.math.BigDecimal getPenalInt() {
        return this.penalInt;
    }

    /**
     * @param compoundInt
     */
    public void setCompoundInt(java.math.BigDecimal compoundInt) {
        this.compoundInt = compoundInt;
    }

    /**
     * @return compoundInt
     */
    public java.math.BigDecimal getCompoundInt() {
        return this.compoundInt;
    }

    /**
     * @param overdueDay
     */
    public void setOverdueDay(String overdueDay) {
        this.overdueDay = overdueDay;
    }

    /**
     * @return overdueDay
     */
    public String getOverdueDay() {
        return this.overdueDay;
    }

    /**
     * @param fiveClass
     */
    public void setFiveClass(String fiveClass) {
        this.fiveClass = fiveClass;
    }

    /**
     * @return fiveClass
     */
    public String getFiveClass() {
        return this.fiveClass;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}