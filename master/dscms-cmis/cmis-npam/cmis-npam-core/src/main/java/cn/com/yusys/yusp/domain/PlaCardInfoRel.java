/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaCardInfoRel
 * @类描述: pla_card_info_rel数据实体类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-11 13:51:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_card_info_rel")
public class PlaCardInfoRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 信用卡信息流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PBDWAB_SERNO")
    private String pbdwabSerno;

    /**
     * 信用卡核销申请流水号
     **/
    @Column(name = "PCWA_SERNO", unique = false, nullable = true, length = 40)
    private String pcwaSerno;

    /**
     * 账户编号
     **/
    @Column(name = "ACCNO", unique = false, nullable = true, length = 40)
    private String accno;

    /**
     * 客户名称
     **/
    @Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
    private String cusName;

    /**
     * 身份证号码
     **/
    @Column(name = "CERT_CODE", unique = false, nullable = true, length = 20)
    private String certCode;

    /**
     * 最新介质信用卡卡号
     **/
    @Column(name = "CARD_NO", unique = false, nullable = true, length = 40)
    private String cardNo;

    /**
     * 最早开户日期
     **/
    @Column(name = "OPEN_DATE", unique = false, nullable = true, length = 20)
    private String openDate;

    /**
     * 授信额度
     **/
    @Column(name = "LMT_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal lmtAmt;

    /**
     * 逾期天数
     **/
    @Column(name = "OVERDUE_DAY", unique = false, nullable = true, length = 10)
    private String overdueDay;

    /**
     * 五级分类
     **/
    @Column(name = "FIVE_CLASS", unique = false, nullable = true, length = 5)
    private String fiveClass;

    /**
     * 透支余额
     **/
    @Column(name = "OVERDRAFT_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal overdraftAmt;

    /**
     * 利息合计
     **/
    @Column(name = "TOTAL_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalInt;

    /**
     * 费用合计
     **/
    @Column(name = "TOTAL_COST_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalCostAmt;

    /**
     * 核销类型
     **/
    @Column(name = "WRITEOFF_TYPE", unique = false, nullable = true, length = 5)
    private String writeoffType;

    /**
     * 核销本金
     **/
    @Column(name = "WRITEOFF_CAP", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal writeoffCap;

    /**
     * 核销利息
     **/
    @Column(name = "WRITEOFF_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal writeoffInt;

    /**
     * 核销费用
     **/
    @Column(name = "WRITEOFF_COST", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal writeoffCost;

    /**
     * 核销总金额
     **/
    @Column(name = "TOTAL_WRITEOFF_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalWriteoffAmt;

    /**
     * 核销状态
     **/
    @Column(name = "WRITEOFF_STATUS", unique = false, nullable = true, length = 5)
    private String writeoffStatus;

    /**
     * 核销日期
     **/
    @Column(name = "WRITEOFF_DATE", unique = false, nullable = true, length = 10)
    private String writeoffDate;

    /**
     * 是否诉讼
     **/
    @Column(name = "IS_LAW", unique = false, nullable = true, length = 5)
    private String isLaw;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaCardInfoRel() {
        // default implementation ignored
    }

    /**
     * @param pbdwabSerno
     */
    public void setPbdwabSerno(String pbdwabSerno) {
        this.pbdwabSerno = pbdwabSerno;
    }

    /**
     * @return pbdwabSerno
     */
    public String getPbdwabSerno() {
        return this.pbdwabSerno;
    }

    /**
     * @param pcwaSerno
     */
    public void setPcwaSerno(String pcwaSerno) {
        this.pcwaSerno = pcwaSerno;
    }

    /**
     * @return pcwaSerno
     */
    public String getPcwaSerno() {
        return this.pcwaSerno;
    }

    /**
     * @param accno
     */
    public void setAccno(String accno) {
        this.accno = accno;
    }

    /**
     * @return accno
     */
    public String getAccno() {
        return this.accno;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param certCode
     */
    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    /**
     * @return certCode
     */
    public String getCertCode() {
        return this.certCode;
    }

    /**
     * @param cardNo
     */
    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    /**
     * @return cardNo
     */
    public String getCardNo() {
        return this.cardNo;
    }

    /**
     * @param openDate
     */
    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    /**
     * @return openDate
     */
    public String getOpenDate() {
        return this.openDate;
    }

    /**
     * @param lmtAmt
     */
    public void setLmtAmt(java.math.BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    /**
     * @return lmtAmt
     */
    public java.math.BigDecimal getLmtAmt() {
        return this.lmtAmt;
    }

    /**
     * @param overdueDay
     */
    public void setOverdueDay(String overdueDay) {
        this.overdueDay = overdueDay;
    }

    /**
     * @return overdueDay
     */
    public String getOverdueDay() {
        return this.overdueDay;
    }

    /**
     * @param fiveClass
     */
    public void setFiveClass(String fiveClass) {
        this.fiveClass = fiveClass;
    }

    /**
     * @return fiveClass
     */
    public String getFiveClass() {
        return this.fiveClass;
    }

    /**
     * @param overdraftAmt
     */
    public void setOverdraftAmt(java.math.BigDecimal overdraftAmt) {
        this.overdraftAmt = overdraftAmt;
    }

    /**
     * @return overdraftAmt
     */
    public java.math.BigDecimal getOverdraftAmt() {
        return this.overdraftAmt;
    }

    /**
     * @param totalInt
     */
    public void setTotalInt(java.math.BigDecimal totalInt) {
        this.totalInt = totalInt;
    }

    /**
     * @return totalInt
     */
    public java.math.BigDecimal getTotalInt() {
        return this.totalInt;
    }

    /**
     * @param totalCostAmt
     */
    public void setTotalCostAmt(java.math.BigDecimal totalCostAmt) {
        this.totalCostAmt = totalCostAmt;
    }

    /**
     * @return totalCostAmt
     */
    public java.math.BigDecimal getTotalCostAmt() {
        return this.totalCostAmt;
    }

    /**
     * @param writeoffType
     */
    public void setWriteoffType(String writeoffType) {
        this.writeoffType = writeoffType;
    }

    /**
     * @return writeoffType
     */
    public String getWriteoffType() {
        return this.writeoffType;
    }

    /**
     * @param writeoffCap
     */
    public void setWriteoffCap(java.math.BigDecimal writeoffCap) {
        this.writeoffCap = writeoffCap;
    }

    /**
     * @return writeoffCap
     */
    public java.math.BigDecimal getWriteoffCap() {
        return this.writeoffCap;
    }

    /**
     * @param writeoffInt
     */
    public void setWriteoffInt(java.math.BigDecimal writeoffInt) {
        this.writeoffInt = writeoffInt;
    }

    /**
     * @return writeoffInt
     */
    public java.math.BigDecimal getWriteoffInt() {
        return this.writeoffInt;
    }

    /**
     * @param writeoffCost
     */
    public void setWriteoffCost(java.math.BigDecimal writeoffCost) {
        this.writeoffCost = writeoffCost;
    }

    /**
     * @return writeoffCost
     */
    public java.math.BigDecimal getWriteoffCost() {
        return this.writeoffCost;
    }

    /**
     * @param totalWriteoffAmt
     */
    public void setTotalWriteoffAmt(java.math.BigDecimal totalWriteoffAmt) {
        this.totalWriteoffAmt = totalWriteoffAmt;
    }

    /**
     * @return totalWriteoffAmt
     */
    public java.math.BigDecimal getTotalWriteoffAmt() {
        return this.totalWriteoffAmt;
    }

    /**
     * @param writeoffStatus
     */
    public void setWriteoffStatus(String writeoffStatus) {
        this.writeoffStatus = writeoffStatus;
    }

    /**
     * @return writeoffStatus
     */
    public String getWriteoffStatus() {
        return this.writeoffStatus;
    }

    /**
     * @param writeoffDate
     */
    public void setWriteoffDate(String writeoffDate) {
        this.writeoffDate = writeoffDate;
    }

    /**
     * @return writeoffDate
     */
    public String getWriteoffDate() {
        return this.writeoffDate;
    }

    /**
     * @param isLaw
     */
    public void setIsLaw(String isLaw) {
        this.isLaw = isLaw;
    }

    /**
     * @return isLaw
     */
    public String getIsLaw() {
        return this.isLaw;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}