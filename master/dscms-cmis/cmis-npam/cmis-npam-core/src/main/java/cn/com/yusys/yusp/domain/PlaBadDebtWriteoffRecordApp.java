/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBadDebtWriteoffRecordApp
 * @类描述: pla_bad_debt_writeoff_record_app数据实体类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-09 09:34:55
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_bad_debt_writeoff_record_app")
public class PlaBadDebtWriteoffRecordApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 记账申请流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PBDWRA_SERNO")
    private String pbdwraSerno;

    /**
     * 核销总户数
     **/
    @Column(name = "TOTAL_WRITEOFF_CUS", unique = false, nullable = true, length = 20)
    private String totalWriteoffCus;

    /**
     * 核销总笔数
     **/
    @Column(name = "WRITEOFF_COUNT", unique = false, nullable = true, length = 20)
    private String writeoffCount;

    /**
     * 核销总金额
     **/
    @Column(name = "TOTAL_WRITEOFF_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalWriteoffAmt;

    /**
     * 核销总本金
     **/
    @Column(name = "TOTAL_WRITEOFF_CAP", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalWriteoffCap;

    /**
     * 核销总利息
     **/
    @Column(name = "TOTAL_WRITEOFF_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalWriteoffInt;

    /**
     * 审批状态
     **/
    @Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
    private String approveStatus;

    /**
     * 记账状态
     **/
    @Column(name = "RECORD_STATUS", unique = false, nullable = true, length = 5)
    private String recordStatus;

    /**
     * 核销状态
     **/
    @Column(name = "WRITEOFF_STATUS", unique = false, nullable = true, length = 5)
    private String writeoffStatus;

    /**
     * 记账日期
     **/
    @Column(name = "RECORD_DATE", unique = false, nullable = true, length = 10)
    private String recordDate;

    /**
     * 核销日期
     **/
    @Column(name = "WRITEOFF_DATE", unique = false, nullable = true, length = 10)
    private String writeoffDate;

    /**
     * 备注
     **/
    @Column(name = "MEMO", unique = false, nullable = true, length = 2000)
    private String memo;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    /**
     * 核销申请流水号
     **/
    @Column(name = "PBDWAS_SERNO", unique = false, nullable = true, length = 40)
    private String pbdwasSerno;

    public PlaBadDebtWriteoffRecordApp() {
        // default implementation ignored
    }

    /**
     * @param pbdwraSerno
     */
    public void setPbdwraSerno(String pbdwraSerno) {
        this.pbdwraSerno = pbdwraSerno;
    }

    /**
     * @return pbdwraSerno
     */
    public String getPbdwraSerno() {
        return this.pbdwraSerno;
    }

    /**
     * @param totalWriteoffCus
     */
    public void setTotalWriteoffCus(String totalWriteoffCus) {
        this.totalWriteoffCus = totalWriteoffCus;
    }

    /**
     * @return totalWriteoffCus
     */
    public String getTotalWriteoffCus() {
        return this.totalWriteoffCus;
    }

    /**
     * @param writeoffCount
     */
    public void setWriteoffCount(String writeoffCount) {
        this.writeoffCount = writeoffCount;
    }

    /**
     * @return writeoffCount
     */
    public String getWriteoffCount() {
        return this.writeoffCount;
    }

    /**
     * @param totalWriteoffAmt
     */
    public void setTotalWriteoffAmt(java.math.BigDecimal totalWriteoffAmt) {
        this.totalWriteoffAmt = totalWriteoffAmt;
    }

    /**
     * @return totalWriteoffAmt
     */
    public java.math.BigDecimal getTotalWriteoffAmt() {
        return this.totalWriteoffAmt;
    }

    /**
     * @param totalWriteoffCap
     */
    public void setTotalWriteoffCap(java.math.BigDecimal totalWriteoffCap) {
        this.totalWriteoffCap = totalWriteoffCap;
    }

    /**
     * @return totalWriteoffCap
     */
    public java.math.BigDecimal getTotalWriteoffCap() {
        return this.totalWriteoffCap;
    }

    /**
     * @param totalWriteoffInt
     */
    public void setTotalWriteoffInt(java.math.BigDecimal totalWriteoffInt) {
        this.totalWriteoffInt = totalWriteoffInt;
    }

    /**
     * @return totalWriteoffInt
     */
    public java.math.BigDecimal getTotalWriteoffInt() {
        return this.totalWriteoffInt;
    }

    /**
     * @param approveStatus
     */
    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    /**
     * @return approveStatus
     */
    public String getApproveStatus() {
        return this.approveStatus;
    }

    /**
     * @param recordStatus
     */
    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    /**
     * @return recordStatus
     */
    public String getRecordStatus() {
        return this.recordStatus;
    }

    /**
     * @param writeoffStatus
     */
    public void setWriteoffStatus(String writeoffStatus) {
        this.writeoffStatus = writeoffStatus;
    }

    /**
     * @return writeoffStatus
     */
    public String getWriteoffStatus() {
        return this.writeoffStatus;
    }

    /**
     * @param recordDate
     */
    public void setRecordDate(String recordDate) {
        this.recordDate = recordDate;
    }

    /**
     * @return recordDate
     */
    public String getRecordDate() {
        return this.recordDate;
    }

    /**
     * @param writeoffDate
     */
    public void setWriteoffDate(String writeoffDate) {
        this.writeoffDate = writeoffDate;
    }

    /**
     * @return writeoffDate
     */
    public String getWriteoffDate() {
        return this.writeoffDate;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    public String getPbdwasSerno() {
        return pbdwasSerno;
    }

    public void setPbdwasSerno(String pbdwasSerno) {
        this.pbdwasSerno = pbdwasSerno;
    }
}