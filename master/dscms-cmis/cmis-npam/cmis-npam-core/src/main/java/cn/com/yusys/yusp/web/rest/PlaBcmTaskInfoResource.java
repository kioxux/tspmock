/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaBcmTaskInfo;
import cn.com.yusys.yusp.service.PlaBcmTaskInfoService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBcmTaskInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-03 16:26:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "待登记催收任务列表")
@RestController
@RequestMapping("/api/plabcmtaskinfo")
public class PlaBcmTaskInfoResource {
    @Autowired
    private PlaBcmTaskInfoService plaBcmTaskInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaBcmTaskInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaBcmTaskInfo> list = plaBcmTaskInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaBcmTaskInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaBcmTaskInfo>> index(QueryModel queryModel) {
        List<PlaBcmTaskInfo> list = plaBcmTaskInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaBcmTaskInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("催收任务信息分页查询")
    @PostMapping("/queryList")
    protected ResultDto<List<PlaBcmTaskInfo>> queryList(@RequestBody QueryModel queryModel) {
        List<PlaBcmTaskInfo> list = plaBcmTaskInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaBcmTaskInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{taskNo}")
    protected ResultDto<PlaBcmTaskInfo> show(@PathVariable("taskNo") String taskNo) {
        PlaBcmTaskInfo plaBcmTaskInfo = plaBcmTaskInfoService.selectByPrimaryKey(taskNo);
        return new ResultDto<PlaBcmTaskInfo>(plaBcmTaskInfo);
    }


    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * 刘权
     */
    @ApiOperation("待登记催收任务列表查看")
    @PostMapping("/showBytaskNo")
    protected ResultDto<PlaBcmTaskInfo> showBytaskNo(@RequestBody PlaBcmTaskInfo plaBcmTaskInfo) {
        return new ResultDto<PlaBcmTaskInfo>(plaBcmTaskInfoService.selectByPrimaryKey(plaBcmTaskInfo.getTaskNo()));
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaBcmTaskInfo> create(@RequestBody PlaBcmTaskInfo plaBcmTaskInfo) throws URISyntaxException {
        plaBcmTaskInfoService.insert(plaBcmTaskInfo);
        return new ResultDto<PlaBcmTaskInfo>(plaBcmTaskInfo);
    }



    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaBcmTaskInfo plaBcmTaskInfo) throws URISyntaxException {
        int result = plaBcmTaskInfoService.update(plaBcmTaskInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{taskNo}")
    protected ResultDto<Integer> delete(@PathVariable("taskNo") String taskNo) {
        int result = plaBcmTaskInfoService.deleteByPrimaryKey(taskNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaBcmTaskInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectNumByInputId
     * @函数描述:根据客户经理工号查询待处理数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    @ApiOperation("根据客户经理工号查询待处理数据")
    @PostMapping("/selectNumByInputId")
    protected ResultDto<List<Map<String, Object>>> selectNumByInputId(@RequestBody QueryModel queryModel){
        List<Map<String, Object>> list = plaBcmTaskInfoService.selectNumByInputId(queryModel);
        return new ResultDto<List<Map<String, Object>>>(list);
    }
}
