/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldLeaseInfo
 * @类描述: pla_asset_pld_lease_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_asset_pld_lease_info")
public class PlaAssetPldLeaseInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 出租信息流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PAPLI_SERNO")
    private String papliSerno;

    /**
     * 抵债资产处置申请流水号
     **/
    @Column(name = "PAPAI_SERNO", unique = false, nullable = true, length = 40)
    private String papaiSerno;

    /**
     * 抵债资产编号
     **/
    @Column(name = "PLDIMN_NO", unique = false, nullable = true, length = 40)
    private String pldimnNo;

    /**
     * 抵债资产名称
     **/
    @Column(name = "PLDIMN_NAME", unique = false, nullable = true, length = 80)
    private String pldimnName;

    /**
     * 抵债资产类型
     **/
    @Column(name = "PLDIMN_TYPE", unique = false, nullable = true, length = 5)
    private String pldimnType;

    /**
     * 租金
     **/
    @Column(name = "RENT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal rent;

    /**
     * 首付租金
     **/
    @Column(name = "FIRSTPAY_RENT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal firstpayRent;

    /**
     * 承租人名称
     **/
    @Column(name = "LESSEE_NAME", unique = false, nullable = true, length = 80)
    private String lesseeName;

    /**
     * 承租人电话
     **/
    @Column(name = "LESSEE_PHONE", unique = false, nullable = true, length = 20)
    private String lesseePhone;

    /**
     * 承租人证件类型
     **/
    @Column(name = "CERT_TYPE", unique = false, nullable = true, length = 5)
    private String certType;

    /**
     * 承租人证件号码
     **/
    @Column(name = "CERT_CODE", unique = false, nullable = true, length = 40)
    private String certCode;

    /**
     * 承租人地址
     **/
    @Column(name = "LESSEE_ADDR", unique = false, nullable = true, length = 200)
    private String lesseeAddr;

    /**
     * 起始日期
     **/
    @Column(name = "START_DATE", unique = false, nullable = true, length = 10)
    private String startDate;

    /**
     * 到期日期
     **/
    @Column(name = "END_DATE", unique = false, nullable = true, length = 10)
    private String endDate;

    /**
     * 备注
     **/
    @Column(name = "MEMO", unique = false, nullable = true, length = 2000)
    private String memo;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    /**
     * 催收规则参数配表
     **/
    @JsonProperty(value = "leaseAmtInfoParam")
    private List<PlaAssetPldLeaseAmtInfo> leaseAmtInfoParam;

    public PlaAssetPldLeaseInfo() {
        // default implementation ignored
    }

    /**
     * @param papliSerno
     */
    public void setPapliSerno(String papliSerno) {
        this.papliSerno = papliSerno;
    }

    /**
     * @return papliSerno
     */
    public String getPapliSerno() {
        return this.papliSerno;
    }

    /**
     * @param papaiSerno
     */
    public void setPapaiSerno(String papaiSerno) {
        this.papaiSerno = papaiSerno;
    }

    /**
     * @return papaiSerno
     */
    public String getPapaiSerno() {
        return this.papaiSerno;
    }

    /**
     * @param pldimnNo
     */
    public void setPldimnNo(String pldimnNo) {
        this.pldimnNo = pldimnNo;
    }

    /**
     * @return pldimnNo
     */
    public String getPldimnNo() {
        return this.pldimnNo;
    }

    /**
     * @param pldimnName
     */
    public void setPldimnName(String pldimnName) {
        this.pldimnName = pldimnName;
    }

    /**
     * @return pldimnName
     */
    public String getPldimnName() {
        return this.pldimnName;
    }

    /**
     * @param pldimnType
     */
    public void setPldimnType(String pldimnType) {
        this.pldimnType = pldimnType;
    }

    /**
     * @return pldimnType
     */
    public String getPldimnType() {
        return this.pldimnType;
    }

    /**
     * @param rent
     */
    public void setRent(java.math.BigDecimal rent) {
        this.rent = rent;
    }

    /**
     * @return rent
     */
    public java.math.BigDecimal getRent() {
        return this.rent;
    }

    /**
     * @param firstpayRent
     */
    public void setFirstpayRent(java.math.BigDecimal firstpayRent) {
        this.firstpayRent = firstpayRent;
    }

    /**
     * @return firstpayRent
     */
    public java.math.BigDecimal getFirstpayRent() {
        return this.firstpayRent;
    }

    /**
     * @param lesseeName
     */
    public void setLesseeName(String lesseeName) {
        this.lesseeName = lesseeName;
    }

    /**
     * @return lesseeName
     */
    public String getLesseeName() {
        return this.lesseeName;
    }

    /**
     * @param lesseePhone
     */
    public void setLesseePhone(String lesseePhone) {
        this.lesseePhone = lesseePhone;
    }

    /**
     * @return lesseePhone
     */
    public String getLesseePhone() {
        return this.lesseePhone;
    }

    /**
     * @param certType
     */
    public void setCertType(String certType) {
        this.certType = certType;
    }

    /**
     * @return certType
     */
    public String getCertType() {
        return this.certType;
    }

    /**
     * @param certCode
     */
    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    /**
     * @return certCode
     */
    public String getCertCode() {
        return this.certCode;
    }

    /**
     * @param lesseeAddr
     */
    public void setLesseeAddr(String lesseeAddr) {
        this.lesseeAddr = lesseeAddr;
    }

    /**
     * @return lesseeAddr
     */
    public String getLesseeAddr() {
        return this.lesseeAddr;
    }

    /**
     * @param startDate
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return startDate
     */
    public String getStartDate() {
        return this.startDate;
    }

    /**
     * @param endDate
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * @return endDate
     */
    public String getEndDate() {
        return this.endDate;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    public List<PlaAssetPldLeaseAmtInfo> getLeaseAmtInfoParam() {
        return leaseAmtInfoParam;
    }

    public void setLeaseAmtInfoParam(List<PlaAssetPldLeaseAmtInfo> leaseAmtInfoParam) {
        this.leaseAmtInfoParam = leaseAmtInfoParam;
    }
}