/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.PlaBcmTaskInfo;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.service.AdminSmUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaBcmAssignTask;
import cn.com.yusys.yusp.service.PlaBcmAssignTaskService;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBcmAssignTaskResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-01 10:07:42
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "催收任务人工指派")
@RestController
@RequestMapping("/api/plabcmassigntask")
public class PlaBcmAssignTaskResource {
    @Autowired
    private PlaBcmAssignTaskService plaBcmAssignTaskService;
    @Autowired
    private AdminSmUserService adminSmUserService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaBcmAssignTask>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaBcmAssignTask> list = plaBcmAssignTaskService.selectAll(queryModel);
        return new ResultDto<List<PlaBcmAssignTask>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaBcmAssignTask>> index(QueryModel queryModel) {
        List<PlaBcmAssignTask> list = plaBcmAssignTaskService.selectByModel(queryModel);
        return new ResultDto<List<PlaBcmAssignTask>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述: 刘权
     */
    @ApiOperation("催收任务人工指派分页查询")
    @PostMapping("/queryPlaBcmAssignTaskList")
    protected ResultDto<List<PlaBcmAssignTask>> queryPlaBcmAssignTaskList(@RequestBody QueryModel queryModel) {
        List<PlaBcmAssignTask> list = plaBcmAssignTaskService.selectByModel(queryModel);
        return new ResultDto<List<PlaBcmAssignTask>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{taskNo}")
    protected ResultDto<PlaBcmAssignTask> show(@PathVariable("taskNo") String taskNo) {
        PlaBcmAssignTask plaBcmAssignTask = plaBcmAssignTaskService.selectByPrimaryKey(taskNo);
        return new ResultDto<PlaBcmAssignTask>(plaBcmAssignTask);
    }

    /**
     * @函数名称:show
     * @函数描述: 点击修改, 查看时，查询单个对象回显数据
     * @参数与返回说明:
     * @算法描述: 刘权
     */
    @ApiOperation(value = "催收任务人工指派根据主键查询数据")
    @PostMapping("/showByTaskNo")
    protected ResultDto<PlaBcmAssignTask> showByTaskNo(@RequestBody PlaBcmAssignTask plaBcmAssignTask) {
        return new ResultDto<PlaBcmAssignTask>(plaBcmAssignTaskService.selectByPrimaryKey(plaBcmAssignTask.getTaskNo()));
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaBcmAssignTask> create(@RequestBody PlaBcmAssignTask plaBcmAssignTask) throws URISyntaxException {
        plaBcmAssignTaskService.insert(plaBcmAssignTask);
        return new ResultDto<PlaBcmAssignTask>(plaBcmAssignTask);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "催收任务人工指派新增")
    @PostMapping("/save")
    protected ResultDto<PlaBcmAssignTask> save(@RequestBody PlaBcmAssignTask plaBcmAssignTask) throws URISyntaxException {
        PlaBcmAssignTask plaBcmAssignTasks= plaBcmAssignTaskService.save(plaBcmAssignTask);
        return new ResultDto<PlaBcmAssignTask>(plaBcmAssignTasks);
    }


    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaBcmAssignTask plaBcmAssignTask) throws URISyntaxException {
        int result = plaBcmAssignTaskService.update(plaBcmAssignTask);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:update
     * @函数描述: 根据主键更新非空字段
     * @参数与返回说明:
     * @算法描述: 刘权
     */
    @ApiOperation(value = "催收任务人工指派修改")
    @PostMapping("/updateByTaskNo")
    protected ResultDto<Integer> updateByTaskNo(@RequestBody PlaBcmAssignTask plaBcmAssignTask) throws URISyntaxException {
        int result = plaBcmAssignTaskService.updateSelective(plaBcmAssignTask);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "催收任务人工指派删除")
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody PlaBcmAssignTask plaBcmAssignTask) {
        return new ResultDto<Integer>(plaBcmAssignTaskService.deleteByPrimaryKey(plaBcmAssignTask.getTaskNo()));
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaBcmAssignTaskService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:create
     * @函数描述: 催收任务人工指派 分配按钮
     * @参数与返回说明:
     * @算法描述: 刘权
     */
    @ApiOperation(value = "催收任务人工指派分配")
    @PostMapping("/doAllocation")
    protected ResultDto<PlaBcmAssignTask> insert(@RequestBody PlaBcmAssignTask plaBcmAssignTask) throws URISyntaxException {
        plaBcmAssignTaskService.insertBydoAllocation(plaBcmAssignTask);
        return new ResultDto<PlaBcmAssignTask>(plaBcmAssignTask);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "催收任务人工指派查询责任人")
    @PostMapping("/getUserList")
    protected ResultDto<List<AdminSmUserDto>> getUserList(@RequestBody GetUserInfoByOrgCodeDto getUserInfoByOrgCodeDto) throws URISyntaxException {
        ResultDto<List<AdminSmUserDto>> userInfo = adminSmUserService.getUserList(getUserInfoByOrgCodeDto);
        return userInfo;
    }

    /**
     * @函数名称: selectByBcmStatus
     * @函数描述: 根据催收状态查询数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByBcmStatus")
    protected ResultDto<Integer> selectByBcmStatus(@RequestBody PlaBcmAssignTask plaBcmAssignTask) {
        int result = plaBcmAssignTaskService.selectByBcmStatus(plaBcmAssignTask);
        return new ResultDto<Integer>(result);
    }
}