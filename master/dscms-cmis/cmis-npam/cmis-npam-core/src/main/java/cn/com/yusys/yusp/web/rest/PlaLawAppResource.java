/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaLawApp;
import cn.com.yusys.yusp.service.PlaLawAppService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 周茂伟
 * @创建时间: 2021-05-28 20:00:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "诉讼基本信息")
@RestController
    @RequestMapping("/api/plalawapp")
public class PlaLawAppResource {
    @Autowired
    private PlaLawAppService plaLawAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaLawApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaLawApp> list = plaLawAppService.selectAll(queryModel);
        return new ResultDto<List<PlaLawApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaLawApp>> index(QueryModel queryModel) {
        List<PlaLawApp> list = plaLawAppService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{appSerno}")
    protected ResultDto<PlaLawApp> show(@PathVariable("appSerno") String appSerno) {
        PlaLawApp plaLawApp = plaLawAppService.selectByPrimaryKey(appSerno);
        return new ResultDto<PlaLawApp>(plaLawApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaLawApp> create(@RequestBody PlaLawApp plaLawApp) throws URISyntaxException {
        plaLawAppService.savePlaLawApp(plaLawApp);
        return new ResultDto<PlaLawApp>(plaLawApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "诉讼申请详细信息修改")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaLawApp plaLawApp) throws URISyntaxException {
        int result = plaLawAppService.updateSelective(plaLawApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "诉讼申请删除")
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody PlaLawApp plaLawApp) {
        int result = plaLawAppService.delete(plaLawApp.getAppSerno());
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaLawAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryPlaLawAppList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "诉讼申请列表查询")
    @PostMapping("/queryPlaLawAppList")
    protected ResultDto<List<PlaLawApp>> queryPlaLawAppList(@RequestBody QueryModel queryModel) {

        List<PlaLawApp> list = plaLawAppService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "诉讼申请详细信息查询")
    @PostMapping("/queryPlaLawAppInfo")
    protected ResultDto<PlaLawApp> queryPlaLawAppInfo(@Validated @RequestBody PlaLawApp plaLawApp) {
        PlaLawApp plaLawApps = plaLawAppService.selectByPrimaryKey(plaLawApp.getAppSerno());
        return new ResultDto<PlaLawApp>(plaLawApps);
    }

    /**
     * @函数名称:savePlaLawApp
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:有则更新无责新增
     * @创建人：周茂伟
     */
    @ApiOperation(value = "诉讼申请信息保存")
    @PostMapping("/savePlaLawApp")
    protected ResultDto<PlaLawApp> savePlaLawApp(@RequestBody PlaLawApp plaLawApp) throws URISyntaxException {
        PlaLawApp plaLawApps =plaLawAppService.savePlaLawApp(plaLawApp);
        return new ResultDto<PlaLawApp>(plaLawApps);
    }
}
