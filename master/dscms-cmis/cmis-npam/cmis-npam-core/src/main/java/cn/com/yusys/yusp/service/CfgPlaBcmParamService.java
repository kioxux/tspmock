/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgPlaBcmParam;
import cn.com.yusys.yusp.repository.mapper.CfgPlaBcmParamMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: CfgPlaBcmParamService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-01 20:09:23
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgPlaBcmParamService {

    @Autowired
    private CfgPlaBcmParamMapper cfgPlaBcmParamMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CfgPlaBcmParam selectByPrimaryKey(String cpbpSerno) {
        return cfgPlaBcmParamMapper.selectByPrimaryKey(cpbpSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<CfgPlaBcmParam> selectAll(QueryModel model) {
        return cfgPlaBcmParamMapper.selectByModel(model);
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CfgPlaBcmParam> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgPlaBcmParam> list = cfgPlaBcmParamMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryByRuleId
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CfgPlaBcmParam> queryByRuleId(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgPlaBcmParam> list = cfgPlaBcmParamMapper.queryByRuleId(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(CfgPlaBcmParam record) {
        return cfgPlaBcmParamMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(CfgPlaBcmParam record) {
        return cfgPlaBcmParamMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(CfgPlaBcmParam record) {
        return cfgPlaBcmParamMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(CfgPlaBcmParam record) {
        return cfgPlaBcmParamMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String cpbpSerno) {
        return cfgPlaBcmParamMapper.deleteByPrimaryKey(cpbpSerno);
    }


    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据规则编号删除
     * @参数与返回说明:
     * @算法描述: 无
     * 刘权
     */
    public int deleteByRuleId(String ruleId) {
        return cfgPlaBcmParamMapper.deleteByRuleId(ruleId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return cfgPlaBcmParamMapper.deleteByIds(ids);
    }
}