package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.CfgPlaBcmParam;
import cn.com.yusys.yusp.domain.PlaLoanRat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLoanRatDto
 * @类描述:
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-03 19:52:47
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PlaLoanRatDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 利息试算 **/
	private List<PlaLoanRat> plaLoanRatList;

	/** 计算时间点 */
	private String beginDate;

	public List<PlaLoanRat> getPlaLoanRatList() {
		return plaLoanRatList;
	}

	public String getBeginDate() {
		return beginDate;
	}

	public void setPlaLoanRatList(List<PlaLoanRat> plaLoanRatList) {
		this.plaLoanRatList = plaLoanRatList;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}
}