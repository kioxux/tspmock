/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.PlaLawAccusedList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaLawPersonInfo;
import cn.com.yusys.yusp.service.PlaLawPersonInfoService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawPersonInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-07 19:18:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plalawpersoninfo")
public class PlaLawPersonInfoResource {
    @Autowired
    private PlaLawPersonInfoService plaLawPersonInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaLawPersonInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaLawPersonInfo> list = plaLawPersonInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaLawPersonInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaLawPersonInfo>> index(QueryModel queryModel) {
        List<PlaLawPersonInfo> list = plaLawPersonInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawPersonInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{plpiSerno}")
    protected ResultDto<PlaLawPersonInfo> show(@PathVariable("plpiSerno") String plpiSerno) {
        PlaLawPersonInfo plaLawPersonInfo = plaLawPersonInfoService.selectByPrimaryKey(plpiSerno);
        return new ResultDto<PlaLawPersonInfo>(plaLawPersonInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaLawPersonInfo> create(@RequestBody PlaLawPersonInfo plaLawPersonInfo) throws URISyntaxException {
        plaLawPersonInfoService.insert(plaLawPersonInfo);
        return new ResultDto<PlaLawPersonInfo>(plaLawPersonInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaLawPersonInfo plaLawPersonInfo) throws URISyntaxException {
        int result = plaLawPersonInfoService.update(plaLawPersonInfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchUpdate")
    protected ResultDto<Integer> batchUpdate(@RequestBody List<PlaLawPersonInfo> plaLawPersonInfo) throws URISyntaxException {
        int result = plaLawPersonInfoService.batchUpdate(plaLawPersonInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{plpiSerno}")
    protected ResultDto<Integer> delete(@PathVariable("plpiSerno") String plpiSerno) {
        int result = plaLawPersonInfoService.deleteByPrimaryKey(plpiSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaLawPersonInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insert")
    protected ResultDto<Integer> insert(@RequestBody PlaLawPersonInfo plaLawPersonInfo) throws URISyntaxException {
        return plaLawPersonInfoService.insertByAccusedList(plaLawPersonInfo);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryByCaseSerno")
    protected ResultDto<List<PlaLawPersonInfo>> queryPlaLawPersonInfo(@RequestBody QueryModel queryModel) {
        List<PlaLawPersonInfo> list = plaLawPersonInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawPersonInfo>>(list);
    }
}
