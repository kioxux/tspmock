/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.List;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaBadDebtWriteoffAppBatch;
import cn.com.yusys.yusp.repository.mapper.PlaBadDebtWriteoffAppBatchMapper;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBadDebtWriteoffAppBatchService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-06-09 09:34:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaBadDebtWriteoffAppBatchService {

    @Autowired
    private PlaBadDebtWriteoffAppBatchMapper plaBadDebtWriteoffAppBatchMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PlaBadDebtWriteoffAppBatch selectByPrimaryKey(String pbdwabSerno) {
        return plaBadDebtWriteoffAppBatchMapper.selectByPrimaryKey(pbdwabSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaBadDebtWriteoffAppBatch> selectAll(QueryModel model) {
        List<PlaBadDebtWriteoffAppBatch> records = (List<PlaBadDebtWriteoffAppBatch>) plaBadDebtWriteoffAppBatchMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PlaBadDebtWriteoffAppBatch> selectByModel(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PlaBadDebtWriteoffAppBatch> list = plaBadDebtWriteoffAppBatchMapper.selectByModel(queryModel);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PlaBadDebtWriteoffAppBatch record) {
        return plaBadDebtWriteoffAppBatchMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PlaBadDebtWriteoffAppBatch record) {
        return plaBadDebtWriteoffAppBatchMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PlaBadDebtWriteoffAppBatch record) {
        int count=0;
        // 申请人
        String inputId = "";
        // 申请机构
        String inputBrId = "";
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        // 创建时间
        Date createTime = DateUtils.getCurrTimestamp();
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if(userInfo != null){
            // 申请人
            inputId = userInfo.getLoginCode();
            // 申请机构
            inputBrId = userInfo.getOrg().getCode();
        }
        record.setUpdId(inputId);
        record.setUpdBrId(inputBrId);
        record.setUpdDate(openDay);
        record.setUpdateTime(createTime);
        count=plaBadDebtWriteoffAppBatchMapper.updateByPrimaryKeySelective(record);
        return count ;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PlaBadDebtWriteoffAppBatch record) {
        return plaBadDebtWriteoffAppBatchMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pbdwabSerno) {
        return plaBadDebtWriteoffAppBatchMapper.deleteByPrimaryKey(pbdwabSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaBadDebtWriteoffAppBatchMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: save
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    public int save(PlaBadDebtWriteoffAppBatch record) {
        int count=0;
        if(record !=null){
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if(userInfo != null){
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();

            }
            record.setInputId(inputId);
            record.setInputBrId(inputBrId);
            record.setInputDate(openDay);
            record.setCreateTime(createTime);
            record.setUpdId(inputId);
            record.setUpdBrId(inputBrId);
            record.setUpdDate(openDay);
            record.setUpdateTime(createTime);
            record.setApproveStatus("000");
            // 流水号
            // String serno= StringUtils.getUUID();//sequenceTemplateService.getSequenceTemplate("", new HashMap<>());
            // record.setPbdwasSerno(serno);
            count=plaBadDebtWriteoffAppBatchMapper.insert(record);
        }
        return count;
    }

}
