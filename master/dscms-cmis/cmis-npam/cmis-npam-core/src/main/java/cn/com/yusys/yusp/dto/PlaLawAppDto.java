package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawApp
 * @类描述: pla_law_app数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-28 20:00:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PlaLawAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 诉讼申请流水号 **/
	private String appSerno;
	
	/** 诉讼方案名称 **/
	private String planName;
	
	/** 申请事项 **/
	private String appEven;
	
	/** 当事人客户编号 **/
	private String cusId;
	
	/** 当事人客户名称 **/
	private String cusName;
	
	/** 当事人身份 **/
	private String partyRole;
	
	/** 客户联系电话 **/
	private String linkPhone;
	
	/** 币种 **/
	private String curType;
	
	/** 标的金额 **/
	private java.math.BigDecimal totalAmt;
	
	/** 本金总额（元) **/
	private java.math.BigDecimal capAmt;
	
	/** 拖欠利息总额 **/
	private java.math.BigDecimal totalTqlxAmt;
	
	/** 五级分类 **/
	private String fiveClass;
	
	/** 代理方式 **/
	private String agtMode;
	
	/** 代理类型 **/
	private String agtType;
	
	/** 案由 **/
	private String caseReason;
	
	/** 债务人基本情况 **/
	private String debtorBasCase;
	
	/** 担保人基本情况 **/
	private String guarBasCase;
	
	/** 支行处置情况 **/
	private String dispCase;
	
	/** 起诉理由和意见 **/
	private String resnAdvice;
	
	/** 备注 **/
	private String memo;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 责任人 **/
	private String managerId;
	
	/** 责任机构 **/
	private String managerBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param appSerno
	 */
	public void setAppSerno(String appSerno) {
		this.appSerno = appSerno == null ? null : appSerno.trim();
	}
	
    /**
     * @return AppSerno
     */	
	public String getAppSerno() {
		return this.appSerno;
	}
	
	/**
	 * @param planName
	 */
	public void setPlanName(String planName) {
		this.planName = planName == null ? null : planName.trim();
	}
	
    /**
     * @return PlanName
     */	
	public String getPlanName() {
		return this.planName;
	}
	
	/**
	 * @param appEven
	 */
	public void setAppEven(String appEven) {
		this.appEven = appEven == null ? null : appEven.trim();
	}
	
    /**
     * @return AppEven
     */	
	public String getAppEven() {
		return this.appEven;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param partyRole
	 */
	public void setPartyRole(String partyRole) {
		this.partyRole = partyRole == null ? null : partyRole.trim();
	}
	
    /**
     * @return PartyRole
     */	
	public String getPartyRole() {
		return this.partyRole;
	}
	
	/**
	 * @param linkPhone
	 */
	public void setLinkPhone(String linkPhone) {
		this.linkPhone = linkPhone == null ? null : linkPhone.trim();
	}
	
    /**
     * @return LinkPhone
     */	
	public String getLinkPhone() {
		return this.linkPhone;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param totalAmt
	 */
	public void setTotalAmt(java.math.BigDecimal totalAmt) {
		this.totalAmt = totalAmt;
	}
	
    /**
     * @return TotalAmt
     */	
	public java.math.BigDecimal getTotalAmt() {
		return this.totalAmt;
	}
	
	/**
	 * @param capAmt
	 */
	public void setCapAmt(java.math.BigDecimal capAmt) {
		this.capAmt = capAmt;
	}
	
    /**
     * @return CapAmt
     */	
	public java.math.BigDecimal getCapAmt() {
		return this.capAmt;
	}
	
	/**
	 * @param totalTqlxAmt
	 */
	public void setTotalTqlxAmt(java.math.BigDecimal totalTqlxAmt) {
		this.totalTqlxAmt = totalTqlxAmt;
	}
	
    /**
     * @return TotalTqlxAmt
     */	
	public java.math.BigDecimal getTotalTqlxAmt() {
		return this.totalTqlxAmt;
	}
	
	/**
	 * @param fiveClass
	 */
	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass == null ? null : fiveClass.trim();
	}
	
    /**
     * @return FiveClass
     */	
	public String getFiveClass() {
		return this.fiveClass;
	}
	
	/**
	 * @param agtMode
	 */
	public void setAgtMode(String agtMode) {
		this.agtMode = agtMode == null ? null : agtMode.trim();
	}
	
    /**
     * @return AgtMode
     */	
	public String getAgtMode() {
		return this.agtMode;
	}
	
	/**
	 * @param agtType
	 */
	public void setAgtType(String agtType) {
		this.agtType = agtType == null ? null : agtType.trim();
	}
	
    /**
     * @return AgtType
     */	
	public String getAgtType() {
		return this.agtType;
	}
	
	/**
	 * @param caseReason
	 */
	public void setCaseReason(String caseReason) {
		this.caseReason = caseReason == null ? null : caseReason.trim();
	}
	
    /**
     * @return CaseReason
     */	
	public String getCaseReason() {
		return this.caseReason;
	}
	
	/**
	 * @param debtorBasCase
	 */
	public void setDebtorBasCase(String debtorBasCase) {
		this.debtorBasCase = debtorBasCase == null ? null : debtorBasCase.trim();
	}
	
    /**
     * @return DebtorBasCase
     */	
	public String getDebtorBasCase() {
		return this.debtorBasCase;
	}
	
	/**
	 * @param guarBasCase
	 */
	public void setGuarBasCase(String guarBasCase) {
		this.guarBasCase = guarBasCase == null ? null : guarBasCase.trim();
	}
	
    /**
     * @return GuarBasCase
     */	
	public String getGuarBasCase() {
		return this.guarBasCase;
	}
	
	/**
	 * @param dispCase
	 */
	public void setDispCase(String dispCase) {
		this.dispCase = dispCase == null ? null : dispCase.trim();
	}
	
    /**
     * @return DispCase
     */	
	public String getDispCase() {
		return this.dispCase;
	}
	
	/**
	 * @param resnAdvice
	 */
	public void setResnAdvice(String resnAdvice) {
		this.resnAdvice = resnAdvice == null ? null : resnAdvice.trim();
	}
	
    /**
     * @return ResnAdvice
     */	
	public String getResnAdvice() {
		return this.resnAdvice;
	}
	
	/**
	 * @param memo
	 */
	public void setMemo(String memo) {
		this.memo = memo == null ? null : memo.trim();
	}
	
    /**
     * @return Memo
     */	
	public String getMemo() {
		return this.memo;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}