/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.List;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.PlaLawyerInfoMapper;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawyerInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 13:49:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaLawyerInfoService {

    @Autowired
    private PlaLawyerInfoMapper plaLawyerInfoMapper;
    @Autowired
    private PlaLawCaseFstBaseInfoService plaLawCaseFstBaseInfoService;
    @Autowired
    private PlaLawCaseSecdBaseInfoService plaLawCaseSecdBaseInfoService;
    @Autowired
    private PlaLawRetrialBaseInfoService plaLawRetrialBaseInfoService;
    @Autowired
    private PlaLawPerformCaseInfoService plaLawPerformCaseInfoService;
    @Autowired
    private PlaLawBrokeCaseInfoService plaLawBrokeCaseInfoService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PlaLawyerInfo selectByPrimaryKey(String lawyerNo) {
        return plaLawyerInfoMapper.selectByPrimaryKey(lawyerNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaLawyerInfo> selectAll(QueryModel model) {
        List<PlaLawyerInfo> records = (List<PlaLawyerInfo>) plaLawyerInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PlaLawyerInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaLawyerInfo> list = plaLawyerInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }


    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaLawyerInfo record) {
        return plaLawyerInfoMapper.insert(record);
    }
	
    /**
     * @方法名称: save
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     * 刘权
     */

    public int save(PlaLawyerInfo record) {
        int count = 0 ;
        if (record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();

                record.setInputId(inputId);
                record.setInputBrId(inputBrId);
                record.setInputDate(openDay);
                record.setCreateTime(createTime);
                record.setUpdId(inputId);
                record.setUpdBrId(inputBrId);
                record.setUpdDate(openDay);
                record.setUpdateTime(createTime);
                return plaLawyerInfoMapper.insert(record);
            }
        }
        return count;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PlaLawyerInfo record) {
        return plaLawyerInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PlaLawyerInfo record) {
        return plaLawyerInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PlaLawyerInfo record) {
        // 申请人
        String inputId = "";
        // 申请机构
        String inputBrId = "";
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        // 创建时间
        Date createTime = DateUtils.getCurrTimestamp();
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            inputId = userInfo.getLoginCode();
            // 申请机构
            inputBrId = userInfo.getOrg().getCode();
            record.setUpdId(inputId);
            record.setUpdBrId(inputBrId);
            record.setUpdDate(openDay);
            record.setUpdateTime(createTime);
        }
        return plaLawyerInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String lawyerNo) {
        return plaLawyerInfoMapper.deleteByPrimaryKey(lawyerNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaLawyerInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键删除  更新律师状态
     * @参数与返回说明:
     * @算法描述: 无
     * 刘权
     */

    public PlaLawyerInfo updateByLawyerStatus(String lawyerNo) {
        PlaLawyerInfo plaLawyerInfo = plaLawyerInfoMapper.selectByPrimaryKey(lawyerNo);
        String lawyerStatus = plaLawyerInfo.getLawyerStatus();
        if ("01".equals(lawyerStatus)){
            plaLawyerInfo.setLawyerStatus("02");
        }else {
            plaLawyerInfo.setLawyerStatus("01");
        }
        plaLawyerInfoMapper.updateByPrimaryKeySelective(plaLawyerInfo);

        return plaLawyerInfoMapper.selectByPrimaryKey(lawyerNo);
    }

    /**
     * @方法名称: selectByLawyerName
     * @方法描述: 根据律师姓名查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ResultDto<Integer> selectByLawyerName(PlaLawyerInfo plaLawyerInfo) {
        ResultDto<Integer> resultDto = new ResultDto<Integer>();
        int lawCaseFst = plaLawCaseFstBaseInfoService.selectByPlaLawCaseFstBaseInfo(plaLawyerInfo.getLawyerName());
        int lawCaseSecd = plaLawCaseSecdBaseInfoService.selectByPlaLawCaseSecdBaseInfo(plaLawyerInfo.getLawyerName());
        int lawRetrial = plaLawRetrialBaseInfoService.selectByPlaLawRetrialBaseInfo(plaLawyerInfo.getLawyerName());
        int lawPerform = plaLawPerformCaseInfoService.selectByPlaLawPerformCaseInfo(plaLawyerInfo.getLawyerName());
        int lawBroke = plaLawBrokeCaseInfoService.selectByPlaLawBrokeCaseInfo(plaLawyerInfo.getLawyerName());
        int result = 0;
        if (lawCaseFst >0 || lawCaseSecd >0 || lawRetrial >0 || lawPerform >0 || lawBroke >0){
            resultDto.setCode("1");
            resultDto.setMessage("此律师信息已被引用，无法删除");
        }else {
            result = plaLawyerInfoMapper.deleteByPrimaryKey(plaLawyerInfo.getLawyerNo());
            resultDto.setMessage("删除成功");
        }

        return resultDto;
    }
}
