package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaExpenseDetail
 * @类描述: pla_expense_detail数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 15:20:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PlaExpenseDetailDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 关联表流水号 **/
	private String pedSerno;
	
	/** 费用登记流水号 **/
	private String periSerno;
	
	/** 费用发生类型 **/
	private String expenseHppType;
	
	/** 费用发生阶段 **/
	private String expenseHppPhase;
	
	/** 费用金额 **/
	private java.math.BigDecimal expenseAmt;
	
	/** 费用发生日期 **/
	private String expenseHppDate;
	
	/** 费用类型 **/
	private String expenseType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pedSerno
	 */
	public void setPedSerno(String pedSerno) {
		this.pedSerno = pedSerno == null ? null : pedSerno.trim();
	}
	
    /**
     * @return PedSerno
     */	
	public String getPedSerno() {
		return this.pedSerno;
	}
	
	/**
	 * @param periSerno
	 */
	public void setPeriSerno(String periSerno) {
		this.periSerno = periSerno == null ? null : periSerno.trim();
	}
	
    /**
     * @return PeriSerno
     */	
	public String getPeriSerno() {
		return this.periSerno;
	}
	
	/**
	 * @param expenseHppType
	 */
	public void setExpenseHppType(String expenseHppType) {
		this.expenseHppType = expenseHppType == null ? null : expenseHppType.trim();
	}
	
    /**
     * @return ExpenseHppType
     */	
	public String getExpenseHppType() {
		return this.expenseHppType;
	}
	
	/**
	 * @param expenseHppPhase
	 */
	public void setExpenseHppPhase(String expenseHppPhase) {
		this.expenseHppPhase = expenseHppPhase == null ? null : expenseHppPhase.trim();
	}
	
    /**
     * @return ExpenseHppPhase
     */	
	public String getExpenseHppPhase() {
		return this.expenseHppPhase;
	}
	
	/**
	 * @param expenseAmt
	 */
	public void setExpenseAmt(java.math.BigDecimal expenseAmt) {
		this.expenseAmt = expenseAmt;
	}
	
    /**
     * @return ExpenseAmt
     */	
	public java.math.BigDecimal getExpenseAmt() {
		return this.expenseAmt;
	}
	
	/**
	 * @param expenseHppDate
	 */
	public void setExpenseHppDate(String expenseHppDate) {
		this.expenseHppDate = expenseHppDate == null ? null : expenseHppDate.trim();
	}
	
    /**
     * @return ExpenseHppDate
     */	
	public String getExpenseHppDate() {
		return this.expenseHppDate;
	}
	
	/**
	 * @param expenseType
	 */
	public void setExpenseType(String expenseType) {
		this.expenseType = expenseType == null ? null : expenseType.trim();
	}
	
    /**
     * @return ExpenseType
     */	
	public String getExpenseType() {
		return this.expenseType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}