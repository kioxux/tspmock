/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawCaseSecdBaseInfo
 * @类描述: pla_law_case_secd_base_info数据实体类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-05 17:45:58
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_law_case_secd_base_info")
public class PlaLawCaseSecdBaseInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 二审流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PLCSBI_NO")
    private String plcsbiNo;

    /**
     * 案件编号
     **/
    @Column(name = "CASE_NO", unique = false, nullable = true, length = 40)
    private String caseNo;

    /**
     * 案件流水号
     **/
    @Column(name = "CASE_SERNO", unique = false, nullable = true, length = 40)
    private String caseSerno;

    /**
     * 上诉人
     **/
    @Column(name = "APPELLOR", unique = false, nullable = true, length = 80)
    private String appellor;

    /**
     * 上诉理由
     **/
    @Column(name = "APPEAL_REASON", unique = false, nullable = true, length = 2000)
    private String appealReason;

    /**
     * 二审法院
     **/
    @Column(name = "ACCEPT_COURT", unique = false, nullable = true, length = 80)
    private String acceptCourt;

    /**
     * 二审承办法官
     **/
    @Column(name = "JUDGE", unique = false, nullable = true, length = 80)
    private String judge;

    /**
     * 支持我行诉讼请求
     **/
    @Column(name = "ISUPPORT_TYPE", unique = false, nullable = true, length = 5)
    private String isupportType;

    /**
     * 代理方式
     **/
    @Column(name = "AGCY_MODE", unique = false, nullable = true, length = 5)
    private String agcyMode;

    /**
     * 律师姓名
     **/
    @Column(name = "LAWYER_NAME", unique = false, nullable = true, length = 80)
    private String lawyerName;

    /**
     * 律师联系方式
     **/
    @Column(name = "LAWYER_TEL_NO", unique = false, nullable = true, length = 20)
    private String lawyerTelNo;

    /**
     * 所属律师事务所名称
     **/
    @Column(name = "LAW_OFFICE_NAME", unique = false, nullable = true, length = 80)
    private String lawOfficeName;

    /**
     * 内部代理人姓名
     **/
    @Column(name = "AGCY_NAME", unique = false, nullable = true, length = 80)
    private String agcyName;

    /**
     * 内部代理人联系方式
     **/
    @Column(name = "AGCY_TEL_NO", unique = false, nullable = true, length = 20)
    private String agcyTelNo;

    /**
     * 二审受理日期
     **/
    @Column(name = "ACCEPT_DATE", unique = false, nullable = true, length = 10)
    private String acceptDate;

    /**
     * 结案方式
     **/
    @Column(name = "CLOSE_CASE_TYPE", unique = false, nullable = true, length = 5)
    private String closeCaseType;

    /**
     * 结案日期
     **/
    @Column(name = "CLOSE_CASE_DATE", unique = false, nullable = true, length = 5)
    private String closeCaseDate;

    /**
     * 是否转破产
     **/
    @Column(name = "IS_CONV_BROKE", unique = false, nullable = true, length = 5)
    private String isConvBroke;

    /**
     * 是否胜诉
     **/
    @Column(name = "IS_WIN_LAW", unique = false, nullable = true, length = 5)
    private String isWinLaw;

    /**
     * 法律文书编号
     **/
    @Column(name = "LEGAL_DCMNTS_NO", unique = false, nullable = true, length = 40)
    private String legalDcmntsNo;

    /**
     * 法律文书名称
     **/
    @Column(name = "LEGAL_DCMNTS_NAME", unique = false, nullable = true, length = 80)
    private String legalDcmntsName;

    /**
     * 文书生效日期
     **/
    @Column(name = "DCMNTS_INURE_DATE", unique = false, nullable = true, length = 10)
    private String dcmntsInureDate;

    /**
     * 文书落款日期
     **/
    @Column(name = "DCMNTS_INSCRIBE_DATE", unique = false, nullable = true, length = 10)
    private String dcmntsInscribeDate;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaLawCaseSecdBaseInfo() {
        // default implementation ignored
    }

    public String getCloseCaseDate() {
        return closeCaseDate;
    }

    public void setCloseCaseDate(String closeCaseDate) {
        this.closeCaseDate = closeCaseDate;
    }

    public String getIsConvBroke() {
        return isConvBroke;
    }

    public void setIsConvBroke(String isConvBroke) {
        this.isConvBroke = isConvBroke;
    }

    public String getIsWinLaw() {
        return isWinLaw;
    }

    public void setIsWinLaw(String isWinLaw) {
        this.isWinLaw = isWinLaw;
    }

    /**
     * @param plcsbiNo
     */
    public void setPlcsbiNo(String plcsbiNo) {
        this.plcsbiNo = plcsbiNo;
    }

    /**
     * @return plcsbiNo
     */
    public String getPlcsbiNo() {
        return this.plcsbiNo;
    }

    /**
     * @param caseNo
     */
    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    /**
     * @return caseNo
     */
    public String getCaseNo() {
        return this.caseNo;
    }

    /**
     * @param caseSerno
     */
    public void setCaseSerno(String caseSerno) {
        this.caseSerno = caseSerno;
    }

    /**
     * @return caseSerno
     */
    public String getCaseSerno() {
        return this.caseSerno;
    }

    /**
     * @param appellor
     */
    public void setAppellor(String appellor) {
        this.appellor = appellor;
    }

    /**
     * @return appellor
     */
    public String getAppellor() {
        return this.appellor;
    }

    /**
     * @param appealReason
     */
    public void setAppealReason(String appealReason) {
        this.appealReason = appealReason;
    }

    /**
     * @return appealReason
     */
    public String getAppealReason() {
        return this.appealReason;
    }

    /**
     * @param acceptCourt
     */
    public void setAcceptCourt(String acceptCourt) {
        this.acceptCourt = acceptCourt;
    }

    /**
     * @return acceptCourt
     */
    public String getAcceptCourt() {
        return this.acceptCourt;
    }

    /**
     * @param judge
     */
    public void setJudge(String judge) {
        this.judge = judge;
    }

    /**
     * @return judge
     */
    public String getJudge() {
        return this.judge;
    }

    /**
     * @param isupportType
     */
    public void setIsupportType(String isupportType) {
        this.isupportType = isupportType;
    }

    /**
     * @return isupportType
     */
    public String getIsupportType() {
        return this.isupportType;
    }

    /**
     * @param agcyMode
     */
    public void setAgcyMode(String agcyMode) {
        this.agcyMode = agcyMode;
    }

    /**
     * @return agcyMode
     */
    public String getAgcyMode() {
        return this.agcyMode;
    }

    /**
     * @param lawyerName
     */
    public void setLawyerName(String lawyerName) {
        this.lawyerName = lawyerName;
    }

    /**
     * @return lawyerName
     */
    public String getLawyerName() {
        return this.lawyerName;
    }

    /**
     * @param lawyerTelNo
     */
    public void setLawyerTelNo(String lawyerTelNo) {
        this.lawyerTelNo = lawyerTelNo;
    }

    /**
     * @return lawyerTelNo
     */
    public String getLawyerTelNo() {
        return this.lawyerTelNo;
    }

    /**
     * @param lawOfficeName
     */
    public void setLawOfficeName(String lawOfficeName) {
        this.lawOfficeName = lawOfficeName;
    }

    /**
     * @return lawOfficeName
     */
    public String getLawOfficeName() {
        return this.lawOfficeName;
    }

    /**
     * @param agcyName
     */
    public void setAgcyName(String agcyName) {
        this.agcyName = agcyName;
    }

    /**
     * @return agcyName
     */
    public String getAgcyName() {
        return this.agcyName;
    }

    /**
     * @param agcyTelNo
     */
    public void setAgcyTelNo(String agcyTelNo) {
        this.agcyTelNo = agcyTelNo;
    }

    /**
     * @return agcyTelNo
     */
    public String getAgcyTelNo() {
        return this.agcyTelNo;
    }

    /**
     * @param acceptDate
     */
    public void setAcceptDate(String acceptDate) {
        this.acceptDate = acceptDate;
    }

    /**
     * @return acceptDate
     */
    public String getAcceptDate() {
        return this.acceptDate;
    }

    /**
     * @param closeCaseType
     */
    public void setCloseCaseType(String closeCaseType) {
        this.closeCaseType = closeCaseType;
    }

    /**
     * @return closeCaseType
     */
    public String getCloseCaseType() {
        return this.closeCaseType;
    }

    /**
     * @param legalDcmntsNo
     */
    public void setLegalDcmntsNo(String legalDcmntsNo) {
        this.legalDcmntsNo = legalDcmntsNo;
    }

    /**
     * @return legalDcmntsNo
     */
    public String getLegalDcmntsNo() {
        return this.legalDcmntsNo;
    }

    /**
     * @param legalDcmntsName
     */
    public void setLegalDcmntsName(String legalDcmntsName) {
        this.legalDcmntsName = legalDcmntsName;
    }

    /**
     * @return legalDcmntsName
     */
    public String getLegalDcmntsName() {
        return this.legalDcmntsName;
    }

    /**
     * @param dcmntsInureDate
     */
    public void setDcmntsInureDate(String dcmntsInureDate) {
        this.dcmntsInureDate = dcmntsInureDate;
    }

    /**
     * @return dcmntsInureDate
     */
    public String getDcmntsInureDate() {
        return this.dcmntsInureDate;
    }

    /**
     * @param dcmntsInscribeDate
     */
    public void setDcmntsInscribeDate(String dcmntsInscribeDate) {
        this.dcmntsInscribeDate = dcmntsInscribeDate;
    }

    /**
     * @return dcmntsInscribeDate
     */
    public String getDcmntsInscribeDate() {
        return this.dcmntsInscribeDate;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}