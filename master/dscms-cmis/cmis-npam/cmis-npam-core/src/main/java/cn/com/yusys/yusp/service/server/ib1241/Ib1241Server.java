package cn.com.yusys.yusp.service.server.ib1241;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.PlaBadDebtWriteoffRecordBillRel;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2CoreIbClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @version 1.0.0
 * @项目名称: cmis-psp模块
 * @类名称: Xddh0017Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZMW
 * @创建时间: 2021-08-30 09:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Ib1241Server {
    private static final Logger log = LoggerFactory.getLogger(Ib1241Server.class);
    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;
    /***
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241RespDto>
     * @desc 冲正交易
     * 1、获取出账授权通知信息的”原前置日期、原前置流水“
     * 2、调用冲正交易
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto<Ib1241RespDto> sendIb1241(Ib1241ReqDto ib1241ReqDto) throws Exception {
        // 1、拼装请求报文
        // 原前置日期
        ib1241ReqDto.setYqzhriqi("");
        // 原前置流水
        ib1241ReqDto.setYqzhlshu("");
        // 产品码
        ib1241ReqDto.setChanpnma(StringUtils.EMPTY);
        // 前置日期
        ib1241ReqDto.setQianzhrq("");
        // 前置流水
        ib1241ReqDto.setQianzhls("");
        //部门号
        ib1241ReqDto.setBrchno(EsbEnum.BRCHNO_CORE.key);
        log.info("开始进行冲正" + ib1241ReqDto);
        ResultDto<Ib1241RespDto> ib1241RespDtoResultDto = dscms2CoreIbClientService.ib1241(ib1241ReqDto);
        log.info("冲正结束 响应信息" + ib1241RespDtoResultDto);
        return ib1241RespDtoResultDto;
    }
}

