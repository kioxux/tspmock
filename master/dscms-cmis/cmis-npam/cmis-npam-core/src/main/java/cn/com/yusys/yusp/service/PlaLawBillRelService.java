/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.PlaLawApp;
import cn.com.yusys.yusp.domain.PlaLawGrtRel;
import cn.com.yusys.yusp.dto.AccLoanDto;
import cn.com.yusys.yusp.dto.BizAccLoanDto;
import cn.com.yusys.yusp.dto.GrtGuarContClientDto;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.repository.mapper.PlaLawGrtRelMapper;
import feign.QueryMap;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaLawBillRel;
import cn.com.yusys.yusp.repository.mapper.PlaLawBillRelMapper;

import javax.management.Query;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawBillRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-29 10:54:50
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaLawBillRelService {

    @Autowired
    private PlaLawBillRelMapper plaLawBillRelMapper;

    @Autowired
    private PlaLawGrtRelMapper plaLawGrtRelMapper;
    @Autowired
    private CmisBizClientService cmisBizClientService;
    @Autowired
    private PlaLawGrtRelService plaLawGrtRelService;

    @Autowired
    private PlaLawAppService plaLawAppService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaLawBillRel selectByPrimaryKey(String plbrSerno) {
        return plaLawBillRelMapper.selectByPrimaryKey(plbrSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaLawBillRel> selectAll(QueryModel model) {
        List<PlaLawBillRel> records = (List<PlaLawBillRel>) plaLawBillRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaLawBillRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaLawBillRel> list = plaLawBillRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaLawBillRel record) {
        return plaLawBillRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaLawBillRel record) {
        return plaLawBillRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaLawBillRel record) {
        return plaLawBillRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaLawBillRel record) {
        return plaLawBillRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(PlaLawBillRel plaLawBillRel) {
        int count =0;
        count =plaLawBillRelMapper.deleteByPrimaryKey(plaLawBillRel.getPlbrSerno());
        if(count>0){
            // 引入成功后，自动更新标的金额，本金金额等信息
            // 标的金额
            BigDecimal totalAmt = new BigDecimal("0.00");
            // 本金总额
            BigDecimal capAmt = new BigDecimal("0.00");
            // 拖欠利息总额
            BigDecimal totalTqlxAmt = new BigDecimal("0.00");
            PlaLawApp plaLawApp = new PlaLawApp();
            Map amtMap= plaLawBillRelMapper.selectBySerno(plaLawBillRel.getAppSerno());
            if(CollectionUtils.isEmpty(amtMap)){
                plaLawApp.setAppSerno(plaLawBillRel.getAppSerno());
                plaLawApp.setTotalAmt(totalAmt);
                plaLawApp.setCapAmt(capAmt);
                plaLawApp.setTotalTqlxAmt(totalTqlxAmt);
                plaLawApp.setFiveClass(StringUtils.EMPTY);
                plaLawAppService.updateSelective(plaLawApp);
                return count;
            }
            if(amtMap.containsKey("totalamt")){
                totalAmt= new BigDecimal(amtMap.get("totalamt").toString());
            }
            if(amtMap.containsKey("capamt")){
                capAmt= new BigDecimal(amtMap.get("capamt").toString());
            }
            if(amtMap.containsKey("totaltqlxamt")){
                totalTqlxAmt= new BigDecimal(amtMap.get("totaltqlxamt").toString());
            }
            plaLawApp.setAppSerno(plaLawBillRel.getAppSerno());
            plaLawApp.setTotalAmt(totalAmt);
            plaLawApp.setCapAmt(capAmt);
            plaLawApp.setTotalTqlxAmt(totalTqlxAmt);
            plaLawAppService.updateSelective(plaLawApp);
            // 当引入合同下借据全部删除时，同时删除合同关联担保合同信息
            int num = plaLawBillRelMapper.selectCount(plaLawBillRel.getAppSerno(),plaLawBillRel.getContNo());
            if(num == 0 ){
                plaLawGrtRelService.deleteByCountNo(plaLawBillRel.getAppSerno(),plaLawBillRel.getContNo());
            }
        }
        return count;
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaLawBillRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteByAppSerno
     * @方法描述: 根据诉讼流水号删除
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */

    public int deleteByAppSerno(String appSerno) {
        return plaLawBillRelMapper.deleteByAppSerno(appSerno);
    }

    /**
     * 批量引入合同借据信息
     *
     * @param ctrLoanCont
     * @return
     */
    public Integer saveBatchContNo(Map ctrLoanCont) {
        int count = 0;
        User userInfo = SessionUtils.getUserInformation();
        String openDay =stringRedisTemplate.opsForValue().get("openDay");
        String appSerno= ctrLoanCont.get("appSerno").toString();
        List<Map<String,String>> ctrLoanContList= (List<Map<String,String>>)ctrLoanCont.get("ctrLoanContList");
        List<String> contList = new ArrayList<>();
        for (Map item : ctrLoanContList) {
            contList.add((String) item.get("contNo"));
        }
        //根据合同号查询借据信息
        ResultDto<List<AccLoanDto>> loanMap = cmisBizClientService.queryAccLoanByContNoForNpam(contList);
        List<AccLoanDto> loanList = new ArrayList<>();
        if("0".equals(loanMap.getCode())){
            loanList  = loanMap.getData();
        }
        // 五级分类
        String fiveClass = null;
        // 币种
        String curType = null;
        //拖欠利息总额
        BigDecimal TotalTqlxAmt =BigDecimal.ZERO;
        //欠息
        BigDecimal debitInt =BigDecimal.ZERO;
        //罚息
        BigDecimal penalInt =BigDecimal.ZERO;
        //复利
        BigDecimal compoundInt =BigDecimal.ZERO;
        if(loanList.size()>0){
            for (Object map: loanList) {
                PlaLawBillRel plaLawBillRel = ObjectMapperUtils.instance().convertValue(map, PlaLawBillRel.class);
                // 判断该笔借据是否存在在途或者审批通过数据
                int counts = plaLawBillRelMapper.selectCountByBillNo(plaLawBillRel.getBillNo());
                if(counts > 0 ){
                    throw BizException.error(null, EcnEnum.ECN060003.key,EcnEnum.ECN060003.value+plaLawBillRel.getBillNo()+EcnEnum.ECN060024.value);
                }
                plaLawBillRel.setAppSerno(appSerno);
                plaLawBillRel.setPlbrSerno(StringUtils.getUUID());
                if(((LinkedHashMap) map).containsKey("contCurType")&&((LinkedHashMap) map).get("contCurType") !=null){
                    curType = ((LinkedHashMap) map).get("contCurType").toString();
                }
                if(((LinkedHashMap) map).containsKey("fiveClass")&&((LinkedHashMap) map).get("fiveClass") !=null){
                    fiveClass = ((LinkedHashMap) map).get("fiveClass").toString();
                }
                if(((LinkedHashMap) map).containsKey("debitInt")&&((LinkedHashMap) map).get("debitInt") !=null){
                    debitInt = new BigDecimal(((LinkedHashMap) map).get("debitInt").toString());
                }
                if(((LinkedHashMap) map).containsKey("penalInt")&&((LinkedHashMap) map).get("penalInt") !=null){
                    penalInt =  new BigDecimal(((LinkedHashMap) map).get("penalInt").toString());
                }
                if(((LinkedHashMap) map).containsKey("compoundInt")&&((LinkedHashMap) map).get("compoundInt") !=null){
                    compoundInt =  new BigDecimal(((LinkedHashMap) map).get("compoundInt").toString());
                }
                TotalTqlxAmt=debitInt.add(debitInt).add(penalInt).add(compoundInt);
                if (Objects.nonNull(userInfo)) {
                    plaLawBillRel.setCurType(curType);
                    plaLawBillRel.setTotalTqlxAmt(TotalTqlxAmt);
                    plaLawBillRel.setInputId(userInfo.getLoginCode());
                    plaLawBillRel.setInputBrId(userInfo.getOrg().getCode());
                    plaLawBillRel.setInputDate(openDay);
                    plaLawBillRel.setCreateTime(DateUtils.getCurrTimestamp());
                    plaLawBillRel.setUpdId(userInfo.getLoginCode());
                    plaLawBillRel.setUpdBrId(userInfo.getOrg().getCode());
                    plaLawBillRel.setUpdDate(openDay);
                    plaLawBillRel.setUpdateTime(DateUtils.getCurrTimestamp());
                }
                QueryModel model = new QueryModel();
                model.addCondition("appSerno",appSerno);
                model.addCondition("billNo",plaLawBillRel.getBillNo());
                List<PlaLawBillRel> plaLawBillRelList = plaLawBillRelMapper.selectByModel(model);
                if(CollectionUtils.isEmpty(plaLawBillRelList)){
                    count = plaLawBillRelMapper.insertSelective(plaLawBillRel);
                }
            }
        }

        ResultDto<List<GrtGuarContClientDto>> grtMap = cmisBizClientService.queryGrtGuarContByContNo(contList);
        List<GrtGuarContClientDto> grtList = new ArrayList<>();
        if("0".equals(grtMap.getCode())){
            grtList  = grtMap.getData();
        }
        if(grtList.size()>0){
            for (Object guarMap: grtList) {
                PlaLawGrtRel plaLawGrtRel = ObjectMapperUtils.instance().convertValue(guarMap, PlaLawGrtRel.class);
                plaLawGrtRel.setAppSerno(appSerno);
                plaLawGrtRel.setPlgrSerno(StringUtils.getUUID());
                if (Objects.nonNull(userInfo)) {
                    plaLawGrtRel.setInputId(userInfo.getLoginCode());
                    plaLawGrtRel.setInputBrId(userInfo.getOrg().getCode());
                    plaLawGrtRel.setInputDate(openDay);
                    plaLawGrtRel.setCreateTime(DateUtils.getCurrTimestamp());
                    plaLawGrtRel.setUpdId(userInfo.getLoginCode());
                    plaLawGrtRel.setUpdBrId(userInfo.getOrg().getCode());
                    plaLawGrtRel.setUpdDate(openDay);
                    plaLawGrtRel.setUpdateTime(DateUtils.getCurrTimestamp());
                }
                QueryModel model = new QueryModel();
                model.addCondition("appSerno",appSerno);
                model.addCondition("guarContNo",plaLawGrtRel.getGuarContNo());
                List<PlaLawGrtRel> plaLawGrtRelList = plaLawGrtRelService.selectByModel(model);
                if(CollectionUtils.isEmpty(plaLawGrtRelList)){
                    plaLawGrtRelService.insertSelective(plaLawGrtRel);
                }
            }
        }
        // 引入成功后，自动更新标的金额，本金金额等信息
        // 标的金额
        BigDecimal totalAmt = new BigDecimal("0.00");
        // 本金总额
        BigDecimal capAmt = new BigDecimal("0.00");
        // 拖欠利息总额
        BigDecimal totalTqlxAmt = new BigDecimal("0.00");
        PlaLawApp plaLawApp = new PlaLawApp();
        Map amtMap= plaLawBillRelMapper.selectBySerno(appSerno);
        if(CollectionUtils.nonEmpty(amtMap)){
            if(amtMap.containsKey("totalamt")){
                totalAmt= new BigDecimal(amtMap.get("totalamt").toString());
            }
            if(amtMap.containsKey("capamt")){
                capAmt= new BigDecimal(amtMap.get("capamt").toString());
            }
            if(amtMap.containsKey("totaltqlxamt")){
                totalTqlxAmt= new BigDecimal(amtMap.get("totaltqlxamt").toString());
            }
            plaLawApp.setAppSerno(appSerno);
            plaLawApp.setTotalAmt(totalAmt);
            plaLawApp.setCapAmt(capAmt);
            plaLawApp.setTotalTqlxAmt(totalTqlxAmt);
            plaLawApp.setFiveClass(fiveClass);
            plaLawAppService.updateSelective(plaLawApp);
        }
        return count;
    }

}
