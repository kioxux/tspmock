/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaTakeoverBillRelHis
 * @类描述: pla_takeover_bill_rel_his数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-07-29 16:14:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_takeover_bill_rel_his")
public class PlaTakeoverBillRelHis extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 关联表流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PTBRH_SERNO")
    private String ptbrhSerno;

    /**
     * 转让业务流水号
     **/
    @Column(name = "PTAI_SERNO", unique = false, nullable = true, length = 40)
    private String ptaiSerno;

    /**
     * 合同编号
     **/
    @Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
    private String contNo;

    /**
     * 借据编号
     **/
    @Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
    private String billNo;

    /**
     * 客户编号
     **/
    @Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
    private String cusId;

    /**
     * 客户名称
     **/
    @Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
    private String cusName;

    /**
     * 产品名称
     **/
    @Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
    private String prdName;

    /**
     * 币种
     **/
    @Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
    private String curType;

    /**
     * 贷款金额
     **/
    @Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal loanAmt;

    /**
     * 贷款余额
     **/
    @Column(name = "LOAN_BALANCE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal loanBalance;

    /**
     * 拖欠利息总额
     **/
    @Column(name = "TOTAL_TQLX_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalTqlxAmt;

    /**
     * 贷款起始日
     **/
    @Column(name = "LOAN_START_DATE", unique = false, nullable = true, length = 20)
    private String loanStartDate;

    /**
     * 贷款到期日
     **/
    @Column(name = "LOAN_END_DATE", unique = false, nullable = true, length = 20)
    private String loanEndDate;

    /**
     * 执行年利率
     **/
    @Column(name = "EXEC_RATE_YEAR", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal execRateYear;

    /**
     * 五级分类
     **/
    @Column(name = "FIVE_CLASS", unique = false, nullable = true, length = 5)
    private String fiveClass;

    /**
     * 担保方式
     **/
    @Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
    private String guarMode;

    /**
     * 封包金额
     **/
    @Column(name = "PACKAGE_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal packageAmt;

    /**
     * 转让对价金额
     **/
    @Column(name = "TAKEOVER_PRICE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal takeoverPrice;

    /**
     * 是否转让代理资产
     **/
    @Column(name = "IS_TRANS_AGCY_ASSET", unique = false, nullable = true, length = 5)
    private String isTransAgcyAsset;

    /**
     * 变更状态
     **/
    @Column(name = "CHANGE_STATUS", unique = false, nullable = true, length = 5)
    private String changeStatus;

    /**
     * 登记状态
     **/
    @Column(name = "REGI_STATUS", unique = false, nullable = true, length = 5)
    private String regiStatus;

    /**
     * 记账状态
     **/
    @Column(name = "RECORD_STATUS", unique = false, nullable = true, length = 5)
    private String recordStatus;

    /**
     * 变更日期
     **/
    @Column(name = "MODIFY_DATE", unique = false, nullable = true, length = 20)
    private String modifyDate;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaTakeoverBillRelHis() {
        // default implementation ignored
    }

    /**
     * @param ptbrhSerno
     */
    public void setPtbrhSerno(String ptbrhSerno) {
        this.ptbrhSerno = ptbrhSerno;
    }

    /**
     * @return ptbrhSerno
     */
    public String getPtbrhSerno() {
        return this.ptbrhSerno;
    }

    /**
     * @param ptaiSerno
     */
    public void setPtaiSerno(String ptaiSerno) {
        this.ptaiSerno = ptaiSerno;
    }

    /**
     * @return ptaiSerno
     */
    public String getPtaiSerno() {
        return this.ptaiSerno;
    }

    /**
     * @param contNo
     */
    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    /**
     * @return contNo
     */
    public String getContNo() {
        return this.contNo;
    }

    /**
     * @param billNo
     */
    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    /**
     * @return billNo
     */
    public String getBillNo() {
        return this.billNo;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param prdName
     */
    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    /**
     * @return prdName
     */
    public String getPrdName() {
        return this.prdName;
    }

    /**
     * @param curType
     */
    public void setCurType(String curType) {
        this.curType = curType;
    }

    /**
     * @return curType
     */
    public String getCurType() {
        return this.curType;
    }

    /**
     * @param loanAmt
     */
    public void setLoanAmt(java.math.BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    /**
     * @return loanAmt
     */
    public java.math.BigDecimal getLoanAmt() {
        return this.loanAmt;
    }

    /**
     * @param loanBalance
     */
    public void setLoanBalance(java.math.BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    /**
     * @return loanBalance
     */
    public java.math.BigDecimal getLoanBalance() {
        return this.loanBalance;
    }

    /**
     * @param totalTqlxAmt
     */
    public void setTotalTqlxAmt(java.math.BigDecimal totalTqlxAmt) {
        this.totalTqlxAmt = totalTqlxAmt;
    }

    /**
     * @return totalTqlxAmt
     */
    public java.math.BigDecimal getTotalTqlxAmt() {
        return this.totalTqlxAmt;
    }

    /**
     * @param loanStartDate
     */
    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    /**
     * @return loanStartDate
     */
    public String getLoanStartDate() {
        return this.loanStartDate;
    }

    /**
     * @param loanEndDate
     */
    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    /**
     * @return loanEndDate
     */
    public String getLoanEndDate() {
        return this.loanEndDate;
    }

    /**
     * @param execRateYear
     */
    public void setExecRateYear(java.math.BigDecimal execRateYear) {
        this.execRateYear = execRateYear;
    }

    /**
     * @return execRateYear
     */
    public java.math.BigDecimal getExecRateYear() {
        return this.execRateYear;
    }

    /**
     * @param fiveClass
     */
    public void setFiveClass(String fiveClass) {
        this.fiveClass = fiveClass;
    }

    /**
     * @return fiveClass
     */
    public String getFiveClass() {
        return this.fiveClass;
    }

    /**
     * @param guarMode
     */
    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    /**
     * @return guarMode
     */
    public String getGuarMode() {
        return this.guarMode;
    }

    /**
     * @param packageAmt
     */
    public void setPackageAmt(java.math.BigDecimal packageAmt) {
        this.packageAmt = packageAmt;
    }

    /**
     * @return packageAmt
     */
    public java.math.BigDecimal getPackageAmt() {
        return this.packageAmt;
    }

    /**
     * @param takeoverPrice
     */
    public void setTakeoverPrice(java.math.BigDecimal takeoverPrice) {
        this.takeoverPrice = takeoverPrice;
    }

    /**
     * @return takeoverPrice
     */
    public java.math.BigDecimal getTakeoverPrice() {
        return this.takeoverPrice;
    }

    /**
     * @param isTransAgcyAsset
     */
    public void setIsTransAgcyAsset(String isTransAgcyAsset) {
        this.isTransAgcyAsset = isTransAgcyAsset;
    }

    /**
     * @return isTransAgcyAsset
     */
    public String getIsTransAgcyAsset() {
        return this.isTransAgcyAsset;
    }

    /**
     * @param changeStatus
     */
    public void setChangeStatus(String changeStatus) {
        this.changeStatus = changeStatus;
    }

    /**
     * @return changeStatus
     */
    public String getChangeStatus() {
        return this.changeStatus;
    }

    /**
     * @param regiStatus
     */
    public void setRegiStatus(String regiStatus) {
        this.regiStatus = regiStatus;
    }

    /**
     * @return regiStatus
     */
    public String getRegiStatus() {
        return this.regiStatus;
    }

    /**
     * @param recordStatus
     */
    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    /**
     * @return recordStatus
     */
    public String getRecordStatus() {
        return this.recordStatus;
    }

    /**
     * @param modifyDate
     */
    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }

    /**
     * @return modifyDate
     */
    public String getModifyDate() {
        return this.modifyDate;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}