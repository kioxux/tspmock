package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAdvanceExpenRecoverDetail
 * @类描述: pla_advance_expen_recover_detail数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:52:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PlaAdvanceExpenRecoverDetailDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 关联表流水号 **/
	private String paerdSerno;
	
	/** 费用明细流水号 **/
	private String periSerno;
	
	/** 回收金额 **/
	private java.math.BigDecimal recoverAmt;
	
	/** 回收日期 **/
	private String recoverDate;
	
	/** 回收后剩余垫支金额 **/
	private java.math.BigDecimal reclaimAdvanceExpenAmt;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;

	
	/**
	 * @param paerdSerno
	 */
	public void setPaerdSerno(String paerdSerno) {
		this.paerdSerno = paerdSerno == null ? null : paerdSerno.trim();
	}
	
    /**
     * @return PaerdSerno
     */	
	public String getPaerdSerno() {
		return this.paerdSerno;
	}
	
	/**
	 * @param periSerno
	 */
	public void setPeriSerno(String periSerno) {
		this.periSerno = periSerno == null ? null : periSerno.trim();
	}
	
    /**
     * @return PeriSerno
     */	
	public String getPeriSerno() {
		return this.periSerno;
	}
	
	/**
	 * @param recoverAmt
	 */
	public void setRecoverAmt(java.math.BigDecimal recoverAmt) {
		this.recoverAmt = recoverAmt;
	}
	
    /**
     * @return RecoverAmt
     */	
	public java.math.BigDecimal getRecoverAmt() {
		return this.recoverAmt;
	}
	
	/**
	 * @param recoverDate
	 */
	public void setRecoverDate(String recoverDate) {
		this.recoverDate = recoverDate == null ? null : recoverDate.trim();
	}
	
    /**
     * @return RecoverDate
     */	
	public String getRecoverDate() {
		return this.recoverDate;
	}
	
	/**
	 * @param reclaimAdvanceExpenAmt
	 */
	public void setReclaimAdvanceExpenAmt(java.math.BigDecimal reclaimAdvanceExpenAmt) {
		this.reclaimAdvanceExpenAmt = reclaimAdvanceExpenAmt;
	}
	
    /**
     * @return ReclaimAdvanceExpenAmt
     */	
	public java.math.BigDecimal getReclaimAdvanceExpenAmt() {
		return this.reclaimAdvanceExpenAmt;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}