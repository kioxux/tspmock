/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisNpamConstants;
import cn.com.yusys.yusp.domain.PlaAssetPldLeaseAmtInfo;
import cn.com.yusys.yusp.domain.PlaAssetPldLeaseInfo;
import cn.com.yusys.yusp.repository.mapper.PlaAssetPldLeaseInfoMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldLeaseInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaAssetPldLeaseInfoService {

    @Autowired
    private PlaAssetPldLeaseInfoMapper plaAssetPldLeaseInfoMapper;
    @Autowired
    private PlaAssetPldLeaseAmtInfoService plaAssetPldLeaseAmtInfoService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaAssetPldLeaseInfo selectByPrimaryKey(String papliSerno) {
        return plaAssetPldLeaseInfoMapper.selectByPrimaryKey(papliSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaAssetPldLeaseInfo> selectAll(QueryModel model) {
        List<PlaAssetPldLeaseInfo> records = (List<PlaAssetPldLeaseInfo>) plaAssetPldLeaseInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaAssetPldLeaseInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaAssetPldLeaseInfo> list = plaAssetPldLeaseInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaAssetPldLeaseInfo record) {
        return plaAssetPldLeaseInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaAssetPldLeaseInfo record) {
        return plaAssetPldLeaseInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaAssetPldLeaseInfo record) {
        return plaAssetPldLeaseInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaAssetPldLeaseInfo record) {
        return plaAssetPldLeaseInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String papliSerno) {
        return plaAssetPldLeaseInfoMapper.deleteByPrimaryKey(papliSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaAssetPldLeaseInfoMapper.deleteByIds(ids);
    }

    /**
     * 根据处置申请流水号获取出租信息
     *
     * @author jijian_yx
     * @date 2021/6/10 14:00
     **/
    public PlaAssetPldLeaseInfo selectByPapaiSerno(String papaiSerno) {
        return plaAssetPldLeaseInfoMapper.selectByPapaiSerno(papaiSerno);
    }

    /**
     * 保存/更新出租信息
     *
     * @author jijian_yx
     * @date 2021/6/10 17:49
     **/
    public Map<String, String> insertInfo(PlaAssetPldLeaseInfo plaAssetPldLeaseInfo) {
        Map<String, String> map = new HashMap<>();
        String flag = "";
        String msg = "";
        PlaAssetPldLeaseInfo leaseInfo = selectByPapaiSerno(plaAssetPldLeaseInfo.getPapaiSerno());
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        if (leaseInfo != null) {
            plaAssetPldLeaseInfo.setUpdDate(openDay);
            int result = updateSelective(plaAssetPldLeaseInfo);
            if (result > 0) {
                flag = CmisNpamConstants.FLAG_SUCCESS;
                msg = "更新成功";
            } else {
                flag = CmisNpamConstants.FLAG_FAIL;
                msg = "更新失败";
            }
        } else {
            plaAssetPldLeaseInfo.setUpdDate(openDay);
            int result = insertSelective(plaAssetPldLeaseInfo);
            if (result > 0) {
                flag = CmisNpamConstants.FLAG_SUCCESS;
                msg = "保存成功";
            } else {
                flag = CmisNpamConstants.FLAG_FAIL;
                msg = "保存失败";
            }
        }
        if (CmisNpamConstants.FLAG_SUCCESS.equals(flag)) {
            Date createTime = DateUtils.getCurrTimestamp();
            List<PlaAssetPldLeaseAmtInfo> leaseAmtInfoParamList = plaAssetPldLeaseInfo.getLeaseAmtInfoParam();
            for (PlaAssetPldLeaseAmtInfo leaseAmtInfoParam : leaseAmtInfoParamList) {
                PlaAssetPldLeaseAmtInfo leaseAmtInfo = plaAssetPldLeaseAmtInfoService.selectByPrimaryKey(leaseAmtInfoParam.getPaplaiSerno());
                if (leaseAmtInfo != null) {
                    leaseAmtInfoParam.setUpdDate(openDay);
                    leaseAmtInfoParam.setUpdateTime(createTime);
                    plaAssetPldLeaseAmtInfoService.updateSelective(leaseAmtInfoParam);
                } else {
                    if (StringUtils.isEmpty(leaseAmtInfoParam.getPaplaiSerno())) {
                        // 关联表流水号
                        leaseAmtInfoParam.setPaplaiSerno(StringUtils.getUUID());
                    }
                    leaseAmtInfoParam.setPapliSerno(plaAssetPldLeaseInfo.getPapliSerno());
                    leaseAmtInfoParam.setInputDate(openDay);
                    leaseAmtInfoParam.setCreateTime(createTime);
                    leaseAmtInfoParam.setUpdDate(openDay);
                    leaseAmtInfoParam.setUpdateTime(createTime);
                    plaAssetPldLeaseAmtInfoService.insert(leaseAmtInfoParam);
                }
            }
        }
        map.put("flag", flag);
        map.put("msg", msg);
        return map;
    }

    /**
     * 根据抵债资产处置流水删除出租信息及租金信息
     *
     * @author jijian_yx
     * @date 2021/6/11 10:55
     **/
    public int deleteByPapaiSerno(String papaiSerno) {
        PlaAssetPldLeaseInfo leaseInfo = selectByPapaiSerno(papaiSerno);
        if (leaseInfo != null) {
            String papliSerno = leaseInfo.getPapliSerno();
            /** 根据出租流水号删除租金信息 **/
            plaAssetPldLeaseAmtInfoService.deleteByPapliSerno(papliSerno);
        }
        return plaAssetPldLeaseInfoMapper.deleteByPapaiSerno(papaiSerno);
    }
}
