/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaPescInfo
 * @类描述: pla_pesc_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:52:18
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_pesc_info")
public class PlaPescInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 时效规则编号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PPI_SERNO")
    private String ppiSerno;

    /**
     * 条目名称
     **/
    @Column(name = "ENTRY_NAME", unique = false, nullable = true, length = 5)
    private String entryName;

    /**
     * 时效值
     **/
    @Column(name = "PRESC_VALUE", unique = false, nullable = true, length = 20)
    private String prescValue;

    /**
     * 时效单位
     **/
    @Column(name = "PRESC_UNIT", unique = false, nullable = true, length = 5)
    private String prescUnit;

    /**
     * 是否启用
     **/
    @Column(name = "IS_BEGIN", unique = false, nullable = true, length = 5)
    private String isBegin;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaPescInfo() {
        // default implementation ignored
    }

    /**
     * @param ppiSerno
     */
    public void setPpiSerno(String ppiSerno) {
        this.ppiSerno = ppiSerno;
    }

    /**
     * @return ppiSerno
     */
    public String getPpiSerno() {
        return this.ppiSerno;
    }

    /**
     * @param entryName
     */
    public void setEntryName(String entryName) {
        this.entryName = entryName;
    }

    /**
     * @return entryName
     */
    public String getEntryName() {
        return this.entryName;
    }

    /**
     * @param prescValue
     */
    public void setPrescValue(String prescValue) {
        this.prescValue = prescValue;
    }

    /**
     * @return prescValue
     */
    public String getPrescValue() {
        return this.prescValue;
    }

    /**
     * @param prescUnit
     */
    public void setPrescUnit(String prescUnit) {
        this.prescUnit = prescUnit;
    }

    /**
     * @return prescUnit
     */
    public String getPrescUnit() {
        return this.prescUnit;
    }

    /**
     * @param isBegin
     */
    public void setIsBegin(String isBegin) {
        this.isBegin = isBegin;
    }

    /**
     * @return isBegin
     */
    public String getIsBegin() {
        return this.isBegin;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}