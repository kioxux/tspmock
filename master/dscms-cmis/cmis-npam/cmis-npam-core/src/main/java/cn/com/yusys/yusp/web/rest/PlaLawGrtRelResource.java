/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaLawGrtRel;
import cn.com.yusys.yusp.service.PlaLawGrtRelService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawGrtRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-05-29 15:16:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(value = "诉讼关联担保信息")
@RestController
@RequestMapping("/api/plalawgrtrel")
public class PlaLawGrtRelResource {
    @Autowired
    private PlaLawGrtRelService plaLawGrtRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaLawGrtRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaLawGrtRel> list = plaLawGrtRelService.selectAll(queryModel);
        return new ResultDto<List<PlaLawGrtRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaLawGrtRel>> index(QueryModel queryModel) {
        List<PlaLawGrtRel> list = plaLawGrtRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawGrtRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{plgrSerno}")
    protected ResultDto<PlaLawGrtRel> show(@PathVariable("plgrSerno") String plgrSerno) {
        PlaLawGrtRel plaLawGrtRel = plaLawGrtRelService.selectByPrimaryKey(plgrSerno);
        return new ResultDto<PlaLawGrtRel>(plaLawGrtRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaLawGrtRel> create(@RequestBody PlaLawGrtRel plaLawGrtRel) throws URISyntaxException {
        plaLawGrtRelService.insert(plaLawGrtRel);
        return new ResultDto<PlaLawGrtRel>(plaLawGrtRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaLawGrtRel plaLawGrtRel) throws URISyntaxException {
        int result = plaLawGrtRelService.update(plaLawGrtRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{plgrSerno}")
    protected ResultDto<Integer> delete(@PathVariable("plgrSerno") String plgrSerno) {
        int result = plaLawGrtRelService.deleteByPrimaryKey(plgrSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaLawGrtRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     * @创建人：周茂伟
     */
    @PostMapping("/queryPlaLawGrtRelList")
    protected ResultDto<List<PlaLawGrtRel>> queryPlaLawGrtRelList(@RequestBody QueryModel queryModel) {
        List<PlaLawGrtRel> list = plaLawGrtRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawGrtRel>>(list);
    }
}
