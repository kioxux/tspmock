/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawRecoverInfo
 * @类描述: pla_law_recover_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-07 19:31:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_law_recover_info")
public class PlaLawRecoverInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 回收明细流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PLRI_SERNO")
    private String plriSerno;

    /**
     * 案件流水号
     **/
    @Column(name = "CASE_SERNO", unique = false, nullable = true, length = 40)
    private String caseSerno;

    /**
     * 回收总金额
     **/
    @Column(name = "RECOVER_TOTAL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal recoverTotalAmt;

    /**
     * 回收率
     **/
    @Column(name = "RECOVER_PERCENT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal recoverPercent;

    /**
     * 回收金额
     **/
    @Column(name = "RECOVER_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal recoverAmt;

    /**
     * 币种
     **/
    @Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
    private String curType;

    /**
     * 回收日期
     **/
    @Column(name = "RECOVER_DATE", unique = false, nullable = true, length = 10)
    private String recoverDate;

    /**
     * 回收方式
     **/
    @Column(name = "RECOVER_MODE", unique = false, nullable = true, length = 5)
    private String recoverMode;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaLawRecoverInfo() {
        // default implementation ignored
    }

    /**
     * @param plriSerno
     */
    public void setPlriSerno(String plriSerno) {
        this.plriSerno = plriSerno;
    }

    /**
     * @return plriSerno
     */
    public String getPlriSerno() {
        return this.plriSerno;
    }

    /**
     * @param caseSerno
     */
    public void setCaseSerno(String caseSerno) {
        this.caseSerno = caseSerno;
    }

    /**
     * @return caseSerno
     */
    public String getCaseSerno() {
        return this.caseSerno;
    }

    /**
     * @param recoverTotalAmt
     */
    public void setRecoverTotalAmt(java.math.BigDecimal recoverTotalAmt) {
        this.recoverTotalAmt = recoverTotalAmt;
    }

    /**
     * @return recoverTotalAmt
     */
    public java.math.BigDecimal getRecoverTotalAmt() {
        return this.recoverTotalAmt;
    }

    /**
     * @param recoverPercent
     */
    public void setRecoverPercent(java.math.BigDecimal recoverPercent) {
        this.recoverPercent = recoverPercent;
    }

    /**
     * @return recoverPercent
     */
    public java.math.BigDecimal getRecoverPercent() {
        return this.recoverPercent;
    }

    /**
     * @param recoverAmt
     */
    public void setRecoverAmt(java.math.BigDecimal recoverAmt) {
        this.recoverAmt = recoverAmt;
    }

    /**
     * @return recoverAmt
     */
    public java.math.BigDecimal getRecoverAmt() {
        return this.recoverAmt;
    }

    /**
     * @param curType
     */
    public void setCurType(String curType) {
        this.curType = curType;
    }

    /**
     * @return curType
     */
    public String getCurType() {
        return this.curType;
    }

    /**
     * @param recoverDate
     */
    public void setRecoverDate(String recoverDate) {
        this.recoverDate = recoverDate;
    }

    /**
     * @return recoverDate
     */
    public String getRecoverDate() {
        return this.recoverDate;
    }

    /**
     * @param recoverMode
     */
    public void setRecoverMode(String recoverMode) {
        this.recoverMode = recoverMode;
    }

    /**
     * @return recoverMode
     */
    public String getRecoverMode() {
        return this.recoverMode;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}