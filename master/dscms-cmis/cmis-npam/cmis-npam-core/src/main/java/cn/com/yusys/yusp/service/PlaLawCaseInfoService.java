/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.core.ln3104.Ln3104ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3104.Ln3104RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3104.Lstkhzmx;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.vo.PlaLawCaseInfoVo;
import com.alibaba.excel.util.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.PageHelper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import static cn.com.yusys.yusp.constants.CmisBizConstants.STD_ZB_ASSURE_MEANS.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawCaseInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-03 21:57:56
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaLawCaseInfoService {

    @Autowired
    private PlaLawCaseInfoMapper plaLawCaseInfoMapper;
    @Autowired
    private PlaLawCaseBillRelMapper plaLawCaseBillRelMapper;
    @Autowired
    private PlaLawCaseGrtRelMapper plaLawCaseGrtRelMapper;
    @Autowired
    private CmisBizClientService cmisBizClientService;
    @Autowired
    private ICusClientService icusClientService;
    @Autowired
    private PlaLawAccusedListMapper plaLawAccusedListMapper;
    @Autowired
    private PlaLawPresAssetInfoMapper plaLawPresAssetInfoMapper;
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private PlaLawPerformCaseInfoService plaLawPerformCaseInfoService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaLawCaseInfo selectByPrimaryKey(String caseSerno) {
        return plaLawCaseInfoMapper.selectByPrimaryKey(caseSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaLawCaseInfo> selectAll(QueryModel model) {
        List<PlaLawCaseInfo> records = (List<PlaLawCaseInfo>) plaLawCaseInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaLawCaseInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaLawCaseInfo> list = plaLawCaseInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaLawCaseInfo record) {
        return plaLawCaseInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaLawCaseInfo record) {
        return plaLawCaseInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaLawCaseInfo record) {
        return plaLawCaseInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaLawCaseInfo record) {
        return plaLawCaseInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String caseSerno) {
        return plaLawCaseInfoMapper.deleteByPrimaryKey(caseSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return plaLawCaseInfoMapper.deleteByIds(ids);
    }

    /**
     * 查询诉讼审批通过的借据列表
     *
     * @param queryModel
     * @return
     */
    public List<PlaLawBillRel> selectBillByModel(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PlaLawBillRel> list = plaLawCaseInfoMapper.selectBillByModel(queryModel);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: bathInsert
     * @方法描述: 批量插入涉诉案件借据信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public void bathInsert(String caseSerno, List<PlaLawBillRel> list) {
        List<String> contNos = list.stream().map(PlaLawBillRel::getContNo).distinct().collect(Collectors.toList());
        // 查询担保信息
        List<PlaLawGrtRel> lawGrtRels = plaLawCaseInfoMapper.selectContByContNos(contNos);
        // 查询借据信息
        List<PlaLawBillRel> lawBillRels = plaLawCaseInfoMapper.selectBillByContNos(contNos);
        String openDay =stringRedisTemplate.opsForValue().get("openDay");
        // 申请人
        String inputId = "";
        // 申请机构
        String inputBrId = "";
        // 创建时间
        Date createTime = cn.com.yusys.yusp.commons.util.date.DateUtils.getCurrTimestamp();
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            inputId = userInfo.getLoginCode();
            // 申请机构
            inputBrId = userInfo.getOrg().getCode();
        }
        //1.查询客户号，担保类型，担保合同编号
        ResultDto<List<GrtGuarContClientDto>> GrtGuarContDto = cmisBizClientService.queryGrtGuarContByContNo(contNos);
        if (GrtGuarContDto != null && GrtGuarContDto.getData() != null){
            for (GrtGuarContClientDto grtGuarContClientDto: GrtGuarContDto.getData()) {
                //取出担保类型进行判断
                String guarMode = grtGuarContClientDto.getGuarMode();
                String grtCusId="";
                // 担保合同编号
                String guarContNo=grtGuarContClientDto.getGuarContNo();
                //2.如果担保类型为保证
                if (BAO_ZHENG.equals(guarMode)){

                    // 3.拿到保证人客户号+借款人  放到查询客户的查询结果中
                    // 3.1通过担保合同编号  关联担保合同与人（物）关系表，再关联保证人信息表，查询保证人信息
                    ResultDto<List<GuarGuaranteeDto>> GuarGuaranteeDto = cmisBizClientService.queryGuarGuaranteeByGuarContNo(guarContNo);
                    if (GuarGuaranteeDto != null && GuarGuaranteeDto.getData() != null){
                        for (GuarGuaranteeDto guarGuaranteeDto : GuarGuaranteeDto.getData()){
                            //3.2  取到保证人客户号
                            grtCusId=guarGuaranteeDto.getCusId();
                            // 保存被告人信息
                            savePlaLawAccusedList(grtCusId,caseSerno,STD_ACCUSED_ROLE_02);
                        }
                    }
                }else {
                    //如果担保类型为抵押质押
                    ResultDto<List<GuarBaseInfoClientDto>> GuarBaseInfoDto = cmisBizClientService.selectByGuarBaseInfo(guarContNo);
                    if (GuarBaseInfoDto != null && GuarBaseInfoDto.getData() != null) {
                        for (GuarBaseInfoClientDto guarBaseInfoClientDto : GuarBaseInfoDto.getData()) {
                            PlaLawPresAssetInfo plaLawPresAssetInfo = new PlaLawPresAssetInfo();
                            //把抵质押物信息表中的数据复制到诉讼被告列表中
                            BeanUtils.copyProperties(guarBaseInfoClientDto, plaLawPresAssetInfo);
                            //权属人
                            plaLawPresAssetInfo.setAutho(guarBaseInfoClientDto.getGuarCusName());
                            //保全资产流水号
                            plaLawPresAssetInfo.setPlpaiSerno(UUID.randomUUID().toString());
                            //案件流水号
                            plaLawPresAssetInfo.setCaseSerno(caseSerno);
                            plaLawPresAssetInfo.setCreateTime(createTime);
                            plaLawPresAssetInfo.setInputId(inputId);
                            plaLawPresAssetInfo.setInputBrId(inputBrId);
                            plaLawPresAssetInfo.setInputDate(openDay);
                            plaLawPresAssetInfo.setCreateTime(createTime);
                            plaLawPresAssetInfo.setUpdId(inputId);
                            plaLawPresAssetInfo.setUpdBrId(inputBrId);
                            plaLawPresAssetInfo.setUpdDate(openDay);
                            plaLawPresAssetInfo.setUpdateTime(createTime);
                            plaLawPresAssetInfoMapper.insert(plaLawPresAssetInfo);
                        }
                    }
                }
            }
        }

        List<PlaLawCaseGrtRel> collect1 = lawGrtRels.stream().map(p -> {
            PlaLawCaseGrtRel grtRel = new PlaLawCaseGrtRel();
            BeanUtils.copyProperties(p, grtRel);
            grtRel.setCaseSerno(caseSerno);
            grtRel.setContNo(p.getContNo());
            grtRel.setInputId(SessionUtils.getUserInformation().getLoginCode());
            grtRel.setInputBrId(SessionUtils.getUserInformation().getOrg().getCode());
            grtRel.setInputDate(openDay);
            grtRel.setUpdId(SessionUtils.getUserInformation().getLoginCode());
            grtRel.setUpdBrId(SessionUtils.getUserInformation().getOrg().getCode());
            grtRel.setUpdDate(openDay);
            grtRel.setCreateTime(createTime);
            grtRel.setUpdateTime(createTime);
            return grtRel;
        }).collect(Collectors.toList());

        List<PlaLawCaseBillRel> collect = lawBillRels.stream().map(p -> {
            PlaLawCaseBillRel bill = new PlaLawCaseBillRel();
            BeanUtils.copyProperties(p, bill);
            bill.setCaseSerno(caseSerno);
            bill.setInputId(SessionUtils.getUserInformation().getLoginCode());
            bill.setInputBrId(SessionUtils.getUserInformation().getOrg().getCode());
            bill.setInputDate(openDay);
            bill.setUpdId(SessionUtils.getUserInformation().getLoginCode());
            bill.setUpdBrId(SessionUtils.getUserInformation().getOrg().getCode());
            bill.setUpdDate(openDay);
            bill.setCreateTime(createTime);
            bill.setUpdateTime(createTime);
            return bill;
        }).collect(Collectors.toList());
        //本金总额
        BigDecimal capAmt = new BigDecimal(0);
        //拖欠利息总额
        BigDecimal totalTqlxAmt = new BigDecimal(0);
        for (PlaLawCaseBillRel billRel : collect) {
            //拖欠利息总额
            totalTqlxAmt = totalTqlxAmt.add(billRel.getTotalTqlxAmt() == null ? new BigDecimal(0) : billRel.getTotalTqlxAmt());
            //本金总额：涉诉借据贷款余额之和
            capAmt = capAmt.add(billRel.getLoanBalance() == null ? new BigDecimal(0) : billRel.getLoanBalance());
        }
        PlaLawCaseInfo caseInfo = new PlaLawCaseInfo();
        PlaLawBillRel billRel = list.get(0);
        caseInfo.setCaseSerno(caseSerno);
        caseInfo.setCusId(billRel.getCusId());
        caseInfo.setCusName(billRel.getCusName());
        caseInfo.setCurType(billRel.getCurType());
        caseInfo.setCapAmt(capAmt);
        caseInfo.setTotalAmt(totalTqlxAmt.add(capAmt));
        caseInfo.setTotalTqlxAmt(totalTqlxAmt);
        caseInfo.setOprType("0");
        caseInfo.setInputDate(openDay);
        caseInfo.setUpdDate(openDay);
        caseInfo.setManagerId(billRel.getManagerId());
        caseInfo.setManagerBrId(billRel.getManagerBrId());
        plaLawCaseInfoMapper.insert(caseInfo);
        // 插入借款人
        savePlaLawAccusedList(billRel.getCusId(),caseSerno,STD_ACCUSED_ROLE_01);
        for (PlaLawCaseBillRel caseBillRel : collect) {
            plaLawCaseBillRelMapper.insert(caseBillRel);
        }
        for (PlaLawCaseGrtRel grtRel : collect1) {
            plaLawCaseGrtRelMapper.insert(grtRel);
        }
    }

    /**
     * 案件查询列表
     *
     * @param model
     * @return
     */
    public List<LawCaseInfoDto> queryCaseList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LawCaseInfoDto> map = plaLawCaseInfoMapper.queryCaseList(model);
        PageHelper.clearPage();
        return map;
    }

    /**
     * 查询回收情况
     *
     * @param model
     * @return
     */
    public PlaLawRecoverInfoDto querRecoveryInfo(QueryModel model) {
        PlaLawRecoverInfoDto plaLawRecoverInfoDtoTotal = new PlaLawRecoverInfoDto();
        List<PlaLawRecoverInfoDto> list = new ArrayList<>();
        String caseSerno = (String) model.getCondition().get("caseSerno");
        // 放款金额
        BigDecimal fkjineeeTotal = BigDecimal.ZERO;
        // 回收总金额
        BigDecimal recoverTotalAmtTotal = BigDecimal.ZERO;
        if(StringUtils.nonBlank(caseSerno)) {
            List<PlaLawCaseBillRel> plaLawCaseBillRelList = plaLawCaseBillRelMapper.selectByModel(model);
            if(CollectionUtils.nonEmpty(plaLawCaseBillRelList)) {
                for (PlaLawCaseBillRel plaLawCaseBillRel : plaLawCaseBillRelList) {
                    if(StringUtils.nonBlank(plaLawCaseBillRel.getBillNo())) {
                        Ln3104ReqDto ln3104ReqDto = new Ln3104ReqDto();
                        ln3104ReqDto.setDkjiejuh(plaLawCaseBillRel.getBillNo());
                        ResultDto<Ln3104RespDto> ln3104ResultDto = dscms2CoreLnClientService.ln3104(ln3104ReqDto);
                        if(Objects.nonNull(ln3104ResultDto) && Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3104ResultDto.getCode())
                        && Objects.nonNull(ln3104ResultDto.getData()) && Objects.nonNull(ln3104ResultDto.getData().getZongbish())
                        && ln3104ResultDto.getData().getZongbish() > 0) {
                            List<Lstkhzmx> lstkhzmxList = ln3104ResultDto.getData().getLstkhzmx();
                            if(CollectionUtils.nonEmpty(lstkhzmxList)) {
                                for(Lstkhzmx lstkhzmx : lstkhzmxList) {
                                    PlaLawRecoverInfoDto plaLawRecoverInfoDto = new PlaLawRecoverInfoDto();
                                    // 案件流水号
                                    plaLawRecoverInfoDto.setCaseSerno(caseSerno);
                                    // 借据编号
                                    plaLawRecoverInfoDto.setBillNo(plaLawCaseBillRel.getBillNo());
                                    // 回收金额（归还本金）
                                    plaLawRecoverInfoDto.setRecoverAmt(lstkhzmx.getHkzongee());
                                    recoverTotalAmtTotal = recoverTotalAmtTotal.add(NumberUtils.nullDefaultZero(lstkhzmx.getHkzongee()));
                                    // 回收日期（交易日期）
                                    plaLawRecoverInfoDto.setRecoverDate(lstkhzmx.getJiaoyirq());
                                    // 币种
                                    plaLawRecoverInfoDto.setCurType(lstkhzmx.getHuobdhao());
                                    // 回收方式
                                    plaLawRecoverInfoDto.setRecoverMode("01");
                                    // 放款金额合计
                                   //fkjineeeTotal = fkjineeeTotal.add(NumberUtils.nullDefaultZero(lstkhzmx.getFkjineee()));
                                    list.add(plaLawRecoverInfoDto);
                                }
                            }
                        }
                    }
                }
            }
        }
        // 回收总金额
        plaLawRecoverInfoDtoTotal.setRecoverTotalAmt(recoverTotalAmtTotal);
        // 回收率
        PlaLawPerformCaseInfo plaLawPerformCaseInfo=plaLawPerformCaseInfoService.selectByCaseSerno(caseSerno);
        plaLawRecoverInfoDtoTotal.setRecoverPercent(BigDecimal.ZERO.compareTo(recoverTotalAmtTotal) == 0 ? BigDecimal.ZERO : recoverTotalAmtTotal.multiply
                (NumberUtils.nullDefaultZero("100")).divide(NumberUtils.nullDefaultZero(plaLawPerformCaseInfo.getExeTotalAmt()),2,BigDecimal.ROUND_DOWN));
        plaLawRecoverInfoDtoTotal.setPlaLawRecoverInfoDtoList(list);
        return plaLawRecoverInfoDtoTotal;
    }

    /**
     * 异步导出诉讼申请列表数据
     * @return 导出进度信息
     */
    public ProgressDto asyncExportPlaLawCase(QueryModel model) {
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel)object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            String apiUrl = "/api/plalawcaseinfo/exportPlaLawCase";
            String dataAuth = commonService.setDataAuthority(apiUrl);
            if(StringUtils.nonEmpty(dataAuth)){
                queryModeTemp.setDataAuth(dataAuth);
            }
            return selectCaseList(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(PlaLawCaseInfoVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    public List<PlaLawCaseInfoVo> selectCaseList(QueryModel model) {
        List<PlaLawCaseInfoVo> plaLawCaseInfoVoList = new ArrayList<>() ;
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LawCaseInfoDto> list = plaLawCaseInfoMapper.queryCaseList(model);
        PageHelper.clearPage();

        for (LawCaseInfoDto lawCaseInfoDto:list){
            PlaLawCaseInfoVo plaLawCaseInfoVo = new PlaLawCaseInfoVo() ;
            String inputBrIdName = OcaTranslatorUtils.getOrgName(lawCaseInfoDto.getInputBrId());
            String inputIdName = OcaTranslatorUtils.getUserName(lawCaseInfoDto.getInputId());
            BeanUtils.copyProperties(lawCaseInfoDto, plaLawCaseInfoVo);
            plaLawCaseInfoVo.setInputIdName(inputIdName);
            plaLawCaseInfoVo.setInputBrIdName(inputBrIdName);
            plaLawCaseInfoVoList.add(plaLawCaseInfoVo) ;
        }
        return plaLawCaseInfoVoList;
    }

    /**
     * 保存被告人信息
     * @param cusId
     * @param caseSerno
     * @param accusedRole
     * @return
     */
    public String savePlaLawAccusedList(String cusId ,String caseSerno,String accusedRole){
        //通过客户编号去客户表查询相关数据
        ResultDto<CusBaseDto> cusBaseDtoResultDto = new ResultDto<>();
        if(StringUtils.nonBlank(cusId)){
            if("8".equals(cusId.substring(0,1))){
                cusBaseDtoResultDto=icusClientService.queryCusBaseCropByCusId(cusId);
            }else{
                cusBaseDtoResultDto=icusClientService.queryCusBaseIndivByCusId(cusId);
            }
        }
        if (cusBaseDtoResultDto != null && cusBaseDtoResultDto.getData() != null){
            PlaLawAccusedList plaLawAccusedList = new PlaLawAccusedList();
            //把客户信息表中的数据复制到诉讼被告列表中
            BeanUtils.copyProperties(cusBaseDtoResultDto.getData(), plaLawAccusedList);
            //被告在我行身份
            plaLawAccusedList.setAccusedRole(accusedRole);
            //住所
            plaLawAccusedList.setLivingAddr(cusBaseDtoResultDto.getData().getResiAddr());
            //被告流水号
            plaLawAccusedList.setPlalSerno(UUID.randomUUID().toString());
            //案件流水号
            plaLawAccusedList.setCaseSerno(caseSerno);
            //是否其他被告人  默认为否
            plaLawAccusedList.setIsOtherAccused("0");

            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 创建时间
            Date createTime = cn.com.yusys.yusp.commons.util.date.DateUtils.getCurrTimestamp();
            String openDay =stringRedisTemplate.opsForValue().get("openDay");
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            plaLawAccusedList.setInputId(inputId);
            plaLawAccusedList.setInputBrId(inputBrId);
            plaLawAccusedList.setInputDate(openDay);
            plaLawAccusedList.setCreateTime(createTime);
            plaLawAccusedList.setUpdId(inputId);
            plaLawAccusedList.setUpdBrId(inputBrId);
            plaLawAccusedList.setUpdDate(openDay);
            plaLawAccusedList.setUpdateTime(createTime);
            plaLawAccusedListMapper.insert(plaLawAccusedList);
        }
        return null;
    }


}
