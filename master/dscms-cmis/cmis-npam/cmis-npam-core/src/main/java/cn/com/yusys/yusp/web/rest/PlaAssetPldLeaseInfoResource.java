/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaAssetPldLeaseInfo;
import cn.com.yusys.yusp.service.PlaAssetPldLeaseInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldLeaseInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plaassetpldleaseinfo")
public class PlaAssetPldLeaseInfoResource {
    @Autowired
    private PlaAssetPldLeaseInfoService plaAssetPldLeaseInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaAssetPldLeaseInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaAssetPldLeaseInfo> list = plaAssetPldLeaseInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaAssetPldLeaseInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<PlaAssetPldLeaseInfo>> index(QueryModel queryModel) {
        List<PlaAssetPldLeaseInfo> list = plaAssetPldLeaseInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaAssetPldLeaseInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{papliSerno}")
    protected ResultDto<PlaAssetPldLeaseInfo> show(@PathVariable("papliSerno") String papliSerno) {
        PlaAssetPldLeaseInfo plaAssetPldLeaseInfo = plaAssetPldLeaseInfoService.selectByPrimaryKey(papliSerno);
        return new ResultDto<PlaAssetPldLeaseInfo>(plaAssetPldLeaseInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<PlaAssetPldLeaseInfo> create(@RequestBody PlaAssetPldLeaseInfo plaAssetPldLeaseInfo) throws URISyntaxException {
        plaAssetPldLeaseInfoService.insert(plaAssetPldLeaseInfo);
        return new ResultDto<PlaAssetPldLeaseInfo>(plaAssetPldLeaseInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaAssetPldLeaseInfo plaAssetPldLeaseInfo) throws URISyntaxException {
        int result = plaAssetPldLeaseInfoService.update(plaAssetPldLeaseInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{papliSerno}")
    protected ResultDto<Integer> delete(@PathVariable("papliSerno") String papliSerno) {
        int result = plaAssetPldLeaseInfoService.deleteByPrimaryKey(papliSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaAssetPldLeaseInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据处置申请流水号获取出租信息
     *
     * @author jijian_yx
     * @date 2021/6/10 13:57
     **/
    @ApiOperation("根据处置申请流水号获取出租信息")
    @PostMapping("/querybypapaiserno")
    protected ResultDto<PlaAssetPldLeaseInfo> queryByPapaiSerno(@RequestBody String papaiSerno) {
        PlaAssetPldLeaseInfo plaAssetPldLeaseInfo = plaAssetPldLeaseInfoService.selectByPapaiSerno(papaiSerno);
        return new ResultDto<PlaAssetPldLeaseInfo>(plaAssetPldLeaseInfo);
    }

    /**
     * 保存/更新出租信息
     *
     * @author jijian_yx
     * @date 2021/6/10 18:01
     **/
    @ApiOperation("保存/更新出租信息")
    @PostMapping("/insert")
    protected ResultDto<Map<String,String>> insert(@RequestBody PlaAssetPldLeaseInfo plaAssetPldLeaseInfo) {
        Map<String,String> map = plaAssetPldLeaseInfoService.insertInfo(plaAssetPldLeaseInfo);
        return new ResultDto<Map<String,String>>(map);
    }
}
