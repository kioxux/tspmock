/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.PlaLawAccusedList;
import cn.com.yusys.yusp.domain.PlaLawPersonInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaLawPresAssetInfo;
import cn.com.yusys.yusp.service.PlaLawPresAssetInfoService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawPresAssetInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-06-07 21:29:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plalawpresassetinfo")
public class PlaLawPresAssetInfoResource {
    @Autowired
    private PlaLawPresAssetInfoService plaLawPresAssetInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaLawPresAssetInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaLawPresAssetInfo> list = plaLawPresAssetInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaLawPresAssetInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaLawPresAssetInfo>> index(QueryModel queryModel) {
        List<PlaLawPresAssetInfo> list = plaLawPresAssetInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawPresAssetInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{plpaiSerno}")
    protected ResultDto<PlaLawPresAssetInfo> show(@PathVariable("plpaiSerno") String plpaiSerno) {
        PlaLawPresAssetInfo plaLawPresAssetInfo = plaLawPresAssetInfoService.selectByPrimaryKey(plpaiSerno);
        return new ResultDto<PlaLawPresAssetInfo>(plaLawPresAssetInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaLawPresAssetInfo> create(@RequestBody PlaLawPresAssetInfo plaLawPresAssetInfo) throws URISyntaxException {
        plaLawPresAssetInfoService.insert(plaLawPresAssetInfo);
        return new ResultDto<PlaLawPresAssetInfo>(plaLawPresAssetInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaLawPresAssetInfo plaLawPresAssetInfo) throws URISyntaxException {
        int result = plaLawPresAssetInfoService.update(plaLawPresAssetInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{plpaiSerno}")
    protected ResultDto<Integer> delete(@PathVariable("plpaiSerno") String plpaiSerno) {
        int result = plaLawPresAssetInfoService.deleteByPrimaryKey(plpaiSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaLawPresAssetInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 批量更新新增
     * @param list
     * @return
     */
    @PostMapping("/batchInsert")
    protected ResultDto<Integer> batchInsert(@RequestBody List<PlaLawPresAssetInfo> list) {
        plaLawPresAssetInfoService.batchInsert(list);
        return new ResultDto<>();
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryPlaLawPresAssetInfo")
    protected ResultDto<List<PlaLawPresAssetInfo>> queryPlaLawPresAssetInfo(@RequestBody QueryModel queryModel) {
        List<PlaLawPresAssetInfo> list = plaLawPresAssetInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawPresAssetInfo>>(list);
    }
}
