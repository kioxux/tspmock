/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisNpamConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3061.LstDkzrjj;
import cn.com.yusys.yusp.dto.client.esb.core.ln3063.Lstzrjj;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3248.req.Ln3248ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3248.resp.Ln3248RespDto;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.ln3100.Ln3100Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.PlaTakeoverBillRelMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaTakeoverBillRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 19:54:27
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaTakeoverBillRelService {
    private static final Logger log = LoggerFactory.getLogger(PlaTakeoverBillRelService.class);
    @Autowired
    private PlaTakeoverBillRelMapper plaTakeoverBillRelMapper;
    @Autowired
    private PlaTakeoverAppInfoService plaTakeoverAppInfoService;
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;
    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;
    @Autowired
    private CmisBizClientService cmisBizClientService;
    @Autowired
    private PlaTakeoverBillRelHisService plaTakeoverBillRelHisService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private Ln3100Server ln3100Server;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaTakeoverBillRel selectByPrimaryKey(String ptbrSerno) {
        return plaTakeoverBillRelMapper.selectByPrimaryKey(ptbrSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaTakeoverBillRel> selectAll(QueryModel model) {
        List<PlaTakeoverBillRel> records = (List<PlaTakeoverBillRel>) plaTakeoverBillRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaTakeoverBillRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaTakeoverBillRel> list = plaTakeoverBillRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaTakeoverBillRel record) {
        return plaTakeoverBillRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaTakeoverBillRel record) {
        return plaTakeoverBillRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaTakeoverBillRel record) {
        return plaTakeoverBillRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaTakeoverBillRel record) {
        return plaTakeoverBillRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String ptbrSerno) {
        return plaTakeoverBillRelMapper.deleteByPrimaryKey(ptbrSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaTakeoverBillRelMapper.deleteByIds(ids);
    }

    /**
     * 插入关联借据信息
     * @param lawBillRels
     * @return
     */
    public Integer saveBatchContNo(List<PlaTakeoverBillRel> lawBillRels) {
        int count = 0;
        if (CollectionUtils.nonEmpty(lawBillRels)) {
            String openDays = stringRedisTemplate.opsForValue().get("openDay");
            Map<String, Object> map = new HashMap<>();
            String ptaiSerno = "";
            String billNo = "";
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            for (PlaTakeoverBillRel item : lawBillRels) {
                ptaiSerno = item.getPtaiSerno();
                billNo = item.getBillNo();
                map.put("ptaiSerno", ptaiSerno);
                map.put("billNo", billNo);
                PlaTakeoverBillRel plaTakeoverBillRel = plaTakeoverBillRelMapper.selectByPtaiSerno(map);
                if (plaTakeoverBillRel != null) {
                    throw BizException.error(null, EcnEnum.ECN060003.key, EcnEnum.ECN060003.value + plaTakeoverBillRel.getBillNo());
                } else {
                    // 通过核心接口查询借据信息
                    // 贷款金额
                    BigDecimal loanAmt = BigDecimal.ZERO;
                    // 贷款余额
                    BigDecimal loanBalance = BigDecimal.ZERO;
                    // 拖欠利息总额
                    BigDecimal totalTqlxAmt = BigDecimal.ZERO;
                    ResultDto<Ln3100RespDto> ln3100RespDto =ln3100Server.queryHXBill(billNo);
                    log.info("ln3100接口返回：" + ln3100RespDto);
                    String ln3100Code = Optional.ofNullable(ln3100RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String ln3100Meesage = Optional.ofNullable(ln3100RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    if (Objects.equals(ln3100Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                        if(ln3100RespDto.getData() != null) {
                            loanAmt = ln3100RespDto.getData().getJiejuuje();
                            loanBalance = ln3100RespDto.getData().getYuqibjin().add(ln3100RespDto.getData().getZhchbjin());
                            totalTqlxAmt = ln3100RespDto.getData().getYsyjlixi().add(ln3100RespDto.getData().getCsyjlixi()).add(ln3100RespDto.getData().getYsqianxi()).add(ln3100RespDto.getData().getCsqianxi())
                                    .add(ln3100RespDto.getData().getYsyjfaxi()).add(ln3100RespDto.getData().getCsyjfaxi()).add(ln3100RespDto.getData().getYshofaxi()).add(ln3100RespDto.getData().getCshofaxi())
                                    .add(ln3100RespDto.getData().getYingjifx()).add(ln3100RespDto.getData().getFuxiiiii());
                        }
                    }
                    item.setLoanAmt(loanAmt);
                    item.setLoanBalance(loanBalance);
                    item.setTotalTqlxAmt(totalTqlxAmt);
                    item.setInputId(inputId);
                    item.setInputBrId(inputBrId);
                    item.setInputDate(openDays);
                    item.setCreateTime(createTime);
                    item.setUpdId(inputId);
                    item.setUpdBrId(inputBrId);
                    item.setUpdDate(openDays);
                    item.setUpdateTime(createTime);
                    plaTakeoverBillRelMapper.insertSelective(item);
                }
            }
            map.clear();
            map = plaTakeoverBillRelMapper.selectBySum(ptaiSerno);
            if (CollectionUtils.nonEmpty(map)) {
                // 总户数
                String totalTakeoverCus = map.get("totalTakeoverCus").toString();
                //总笔数
                String takeoverCount = map.get("takeoverCount").toString();
                // 贷款余额
                BigDecimal loanBalance = (BigDecimal) map.get("loanBalance");
                // 拖欠利息总额
                BigDecimal totalTqlxAmt = (BigDecimal) map.get("totalTqlxAmt");
                // 转让总对价
                BigDecimal takeoverTotalPrice = (BigDecimal) map.get("takeoverTotalPrice");
                // 转让总金额
                BigDecimal takeoverTotlAmt = (BigDecimal) map.get("takeoverTotlAmt");
                //获取当前业务流水号下的数据
                PlaTakeoverAppInfo plaTakeoverAppInfo = plaTakeoverAppInfoService.selectByPrimaryKey(ptaiSerno);
                if (plaTakeoverAppInfo != null) {
                    plaTakeoverAppInfo.setLoanBalance(loanBalance);
                   // plaTakeoverAppInfo.setTakeoverCount(takeoverCount);
                    plaTakeoverAppInfo.setTotalTqlxAmt(totalTqlxAmt);
                    plaTakeoverAppInfo.setTotalTakeoverCus(totalTakeoverCus);
                    plaTakeoverAppInfo.setTakeoverTotalPrice(takeoverTotalPrice);
                    plaTakeoverAppInfo.setTakeoverTotlAmt(takeoverTotlAmt);
                    count = plaTakeoverAppInfoService.update(plaTakeoverAppInfo);
                }
            }
        }
        return count;
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int delete(PlaTakeoverBillRel plaTakeoverBillRel) {
        int count = 0;
        count = deleteByPrimaryKey(plaTakeoverBillRel.getPtbrSerno());
        if (count > 0) {
            Map map = plaTakeoverBillRelMapper.selectBySum(plaTakeoverBillRel.getPtaiSerno());
            if (CollectionUtils.nonEmpty(map)) {
                // 总户数
                String totalTakeoverCus = map.get("totalTakeoverCus").toString();
                //总笔数
                String takeoverCount = map.get("takeoverCount").toString();
                // 贷款余额
                BigDecimal loanBalance = (BigDecimal) map.get("loanBalance");
                // 拖欠利息总额
                BigDecimal totalTqlxAmt = (BigDecimal) map.get("totalTqlxAmt");
                // 转让总对价
                BigDecimal takeoverTotalPrice = (BigDecimal) map.get("takeoverTotalPrice");
                // 转让总金额
                BigDecimal takeoverTotlAmt = (BigDecimal) map.get("takeoverTotlAmt");
                //获取当前业务流水号下的数据
                PlaTakeoverAppInfo plaTakeoverAppInfo = plaTakeoverAppInfoService.selectByPrimaryKey(plaTakeoverBillRel.getPtaiSerno());
                if (plaTakeoverAppInfo != null) {
                    plaTakeoverAppInfo.setLoanBalance(loanBalance);
                    //plaTakeoverAppInfo.setTakeoverCount(takeoverCount);
                    plaTakeoverAppInfo.setTotalTqlxAmt(totalTqlxAmt);
                    plaTakeoverAppInfo.setTotalTakeoverCus(totalTakeoverCus);
                    plaTakeoverAppInfo.setTakeoverTotalPrice(takeoverTotalPrice);
                    plaTakeoverAppInfo.setTakeoverTotlAmt(takeoverTotlAmt);
                    count = plaTakeoverAppInfoService.update(plaTakeoverAppInfo);
                }
            }


        }

        return count;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<Lstzrjj> selectByPtaiSernoListjz(String ptaiSerno) {
        List<Lstzrjj> list = plaTakeoverBillRelMapper.selectByPtaiSernoList(ptaiSerno);
        return list;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LstDkzrjj> selectByPtaiSernoListdj(String ptaiSerno) {
        List<LstDkzrjj> list = plaTakeoverBillRelMapper.selectByPtaiSernoListdj(ptaiSerno);
        return list;
    }
    /**
     * 债权转让发送变更代理
     *
     * @author 周茂伟
     **/
    public ResultDto<String> sendToHXBGDL(List<PlaTakeoverBillRel> plaTakeoverBillRelList) {
        ResultDto<String> resultDto = new ResultDto<>();
        String openDays = stringRedisTemplate.opsForValue().get("openDay");
        if(CollectionUtils.isEmpty(plaTakeoverBillRelList)){
            throw BizException.error(null, EcnEnum.ECN060005.key, EcnEnum.ECN060005.value);
        }
        for(PlaTakeoverBillRel plaTakeoverBillRel: plaTakeoverBillRelList){
            if (plaTakeoverBillRel != null) {
                Ln3248ReqDto ln3248ReqDto = new Ln3248ReqDto();
                //贷款借据号
                ln3248ReqDto.setDkjiejuh(plaTakeoverBillRel.getBillNo());
                // 调用核心接口，债权转让发送委托清收变更
                log.info("ln3248请求报文" + ln3248ReqDto);
                ResultDto<Ln3248RespDto> ln3248RespDto = dscms2CoreLnClientService.ln3248(ln3248ReqDto);
                String ln3248Code = Optional.ofNullable(ln3248RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                String ln3248Meesage = Optional.ofNullable(ln3248RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                log.info("ln3248返回报文" + ln3248RespDto);
                if (Objects.equals(ln3248Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                    plaTakeoverBillRel.setIsTransAgcyAsset("0");
                    plaTakeoverBillRel.setModifyDate(openDays);  //变更日期 获取当前时间
                    plaTakeoverBillRel.setRecordDate(openDays);  //记账日期  获取当前时间
                    updateSelective(plaTakeoverBillRel);
                    PlaTakeoverBillRelHis plaTakeoverBillRelHis = new PlaTakeoverBillRelHis();
                    BeanUtils.copyProperties(plaTakeoverBillRel,plaTakeoverBillRelHis);
                    plaTakeoverBillRelHis.setPtbrhSerno(StringUtils.getUUID());
                    plaTakeoverBillRelHis.setChangeStatus("01");//成功
                    plaTakeoverBillRelHisService.insert(plaTakeoverBillRelHis);
                    Map<String, String> map = new HashMap();
                    map.put("billNo", plaTakeoverBillRel.getBillNo());
                    //如果是非委托清收贷款则修改台账状态为关闭并且借据结清(因同一笔申请不可能存在非委托和委托so 直接用主表判断)
                    if("0".equals(plaTakeoverBillRel.getIsTransAgcyAsset())){
                        map.put("accStatus", "0");// 关闭
                        map.put("loanBalance",BigDecimal.ZERO.toPlainString());
                        map.put("debitInt", BigDecimal.ZERO.toPlainString());
                        map.put("penalInt", BigDecimal.ZERO.toPlainString());
                        map.put("compoundInt", BigDecimal.ZERO.toPlainString());
                    }
                    cmisBizClientService.updateAccLoanByBillNo(map);
                } else {
                    resultDto.setCode(ln3248Code);
                    resultDto.setMessage(ln3248Meesage);
                }
            } else {
                throw BizException.error(null, EcnEnum.ECN060005.key, EcnEnum.ECN060005.value);
            }
        }
        return resultDto;
    }

    /**
     * 插入关联借据信息
     * @param lawBillRels
     * @return
     */
    public Integer updateBatchContNo(List<PlaTakeoverBillRel> lawBillRels) {
        int count = 0;
        if (CollectionUtils.nonEmpty(lawBillRels)) {
            Map<String, Object> map = new HashMap<>();
            String ptaiSerno = "";
            String billNo = "";
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDays = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            for (PlaTakeoverBillRel item : lawBillRels) {
                    item.setUpdId(inputId);
                    item.setUpdBrId(inputBrId);
                    item.setUpdDate(openDays);
                    item.setUpdateTime(createTime);
                    plaTakeoverBillRelMapper.updateByPrimaryKey(item);
            }
            map.clear();
            map = plaTakeoverBillRelMapper.selectBySum(ptaiSerno);
            if (CollectionUtils.nonEmpty(map)) {
                // 总户数
                String totalTakeoverCus = map.get("totalTakeoverCus").toString();
                //总笔数
                String takeoverCount = map.get("takeoverCount").toString();
                // 贷款余额
                BigDecimal loanBalance = (BigDecimal) map.get("loanBalance");
                // 拖欠利息总额
                BigDecimal totalTqlxAmt = (BigDecimal) map.get("totalTqlxAmt");
                // 转让总对价
                BigDecimal takeoverTotalPrice = (BigDecimal) map.get("takeoverTotalPrice");
                // 转让总金额
                BigDecimal takeoverTotlAmt = (BigDecimal) map.get("takeoverTotlAmt");
                //获取当前业务流水号下的数据
                PlaTakeoverAppInfo plaTakeoverAppInfo = plaTakeoverAppInfoService.selectByPrimaryKey(ptaiSerno);
                if (plaTakeoverAppInfo != null) {
                    plaTakeoverAppInfo.setLoanBalance(loanBalance);
                    //plaTakeoverAppInfo.setTakeoverCount(takeoverCount);
                    plaTakeoverAppInfo.setTotalTqlxAmt(totalTqlxAmt);
                    plaTakeoverAppInfo.setTotalTakeoverCus(totalTakeoverCus);
                    plaTakeoverAppInfo.setTakeoverTotalPrice(takeoverTotalPrice);
                    plaTakeoverAppInfo.setTakeoverTotlAmt(takeoverTotlAmt);
                    count = plaTakeoverAppInfoService.update(plaTakeoverAppInfo);
                }
            }
        }
        return count;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<PlaTakeoverBillRel> selectByPtaiSernoLists(String ptaiSerno) {
        List<PlaTakeoverBillRel> list = plaTakeoverBillRelMapper.selectByPtaiSernoLists(ptaiSerno);
        return list;
    }
}
