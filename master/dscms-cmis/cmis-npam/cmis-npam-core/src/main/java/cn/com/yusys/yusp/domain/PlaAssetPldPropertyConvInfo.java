/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldPropertyConvInfo
 * @类描述: pla_asset_pld_property_conv_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_asset_pld_property_conv_info")
public class PlaAssetPldPropertyConvInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 转固信息流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PAPPCI_SERNO")
    private String pappciSerno;

    /**
     * 抵债资产处置申请流水号
     **/
    @Column(name = "PAPAI_SERNO", unique = false, nullable = true, length = 40)
    private String papaiSerno;

    /**
     * 抵债资产编号
     **/
    @Column(name = "PLDIMN_NO", unique = false, nullable = true, length = 40)
    private String pldimnNo;

    /**
     * 抵债资产名称
     **/
    @Column(name = "PLDIMN_NAME", unique = false, nullable = true, length = 80)
    private String pldimnName;

    /**
     * 抵债资产类型
     **/
    @Column(name = "PLDIMN_TYPE", unique = false, nullable = true, length = 5)
    private String pldimnType;

    /**
     * 转入价值
     **/
    @Column(name = "TURN_VALUE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal turnValue;

    /**
     * 评估金额
     **/
    @Column(name = "EVAL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal evalAmt;

    /**
     * 入账日期
     **/
    @Column(name = "ASGN_DATE", unique = false, nullable = true, length = 10)
    private String asgnDate;

    /**
     * 处置理由
     **/
    @Column(name = "DISP_RESN", unique = false, nullable = true, length = 2000)
    private String dispResn;

    /**
     * 备注
     **/
    @Column(name = "MEMO", unique = false, nullable = true, length = 2000)
    private String memo;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaAssetPldPropertyConvInfo() {
        // default implementation ignored
    }

    /**
     * @param pappciSerno
     */
    public void setPappciSerno(String pappciSerno) {
        this.pappciSerno = pappciSerno;
    }

    /**
     * @return pappciSerno
     */
    public String getPappciSerno() {
        return this.pappciSerno;
    }

    /**
     * @param papaiSerno
     */
    public void setPapaiSerno(String papaiSerno) {
        this.papaiSerno = papaiSerno;
    }

    /**
     * @return papaiSerno
     */
    public String getPapaiSerno() {
        return this.papaiSerno;
    }

    /**
     * @param pldimnNo
     */
    public void setPldimnNo(String pldimnNo) {
        this.pldimnNo = pldimnNo;
    }

    /**
     * @return pldimnNo
     */
    public String getPldimnNo() {
        return this.pldimnNo;
    }

    /**
     * @param pldimnName
     */
    public void setPldimnName(String pldimnName) {
        this.pldimnName = pldimnName;
    }

    /**
     * @return pldimnName
     */
    public String getPldimnName() {
        return this.pldimnName;
    }

    /**
     * @param pldimnType
     */
    public void setPldimnType(String pldimnType) {
        this.pldimnType = pldimnType;
    }

    /**
     * @return pldimnType
     */
    public String getPldimnType() {
        return this.pldimnType;
    }

    /**
     * @param turnValue
     */
    public void setTurnValue(java.math.BigDecimal turnValue) {
        this.turnValue = turnValue;
    }

    /**
     * @return turnValue
     */
    public java.math.BigDecimal getTurnValue() {
        return this.turnValue;
    }

    /**
     * @param evalAmt
     */
    public void setEvalAmt(java.math.BigDecimal evalAmt) {
        this.evalAmt = evalAmt;
    }

    /**
     * @return evalAmt
     */
    public java.math.BigDecimal getEvalAmt() {
        return this.evalAmt;
    }

    /**
     * @param asgnDate
     */
    public void setAsgnDate(String asgnDate) {
        this.asgnDate = asgnDate;
    }

    /**
     * @return asgnDate
     */
    public String getAsgnDate() {
        return this.asgnDate;
    }

    /**
     * @param dispResn
     */
    public void setDispResn(String dispResn) {
        this.dispResn = dispResn;
    }

    /**
     * @return dispResn
     */
    public String getDispResn() {
        return this.dispResn;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}