/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Supplier;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.file.template.FileSystemTemplate;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisNpamConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.PlaBadDebtWriteoffRecordBillRel;
import cn.com.yusys.yusp.domain.PlaDebtClaimReducRecordAppInfo;
import cn.com.yusys.yusp.dto.CfgSftpDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3045.Ln3045ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3045.Ln3045RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt951.req.Mbt951ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt951.resp.Mbt951RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt952.req.Mbt952ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt952.resp.CplWjplxxOut;
import cn.com.yusys.yusp.dto.client.esb.core.mbt952.resp.Mbt952RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.ln3100.Ln3100Server;
import cn.com.yusys.yusp.util.FtpUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaBadDebtWriteoffRecordApp;
import cn.com.yusys.yusp.repository.mapper.PlaBadDebtWriteoffRecordAppMapper;

import javax.annotation.Resource;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBadDebtWriteoffRecordAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-09 09:34:55
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaBadDebtWriteoffRecordAppService {

    private static final Logger log = LoggerFactory.getLogger(PlaBadDebtWriteoffRecordAppService.class);
    @Autowired
    private PlaBadDebtWriteoffRecordAppMapper plaBadDebtWriteoffRecordAppMapper;

    @Autowired
    private Dscms2CoreMbtClientService dscms2CoreMbtClientService;
    @Autowired
    private PlaBadDebtWriteoffRecordBillRelService plaBadDebtWriteoffRecordBillRelService;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CmisBizClientService cmisBizClientService;
    @Autowired
    private Ln3100Server ln3100Server;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaBadDebtWriteoffRecordApp selectByPrimaryKey(String pbdwraSerno) {
        return plaBadDebtWriteoffRecordAppMapper.selectByPrimaryKey(pbdwraSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaBadDebtWriteoffRecordApp> selectAll(QueryModel model) {
        List<PlaBadDebtWriteoffRecordApp> records = (List<PlaBadDebtWriteoffRecordApp>) plaBadDebtWriteoffRecordAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaBadDebtWriteoffRecordApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaBadDebtWriteoffRecordApp> list = plaBadDebtWriteoffRecordAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaBadDebtWriteoffRecordApp record) {
        return plaBadDebtWriteoffRecordAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaBadDebtWriteoffRecordApp record) {
        return plaBadDebtWriteoffRecordAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaBadDebtWriteoffRecordApp record) {
        int count = 0;
        // 修改
        String updId = "";
        // 修改
        String updBrId = "";
        // 修改日期
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        // 修改时间
        Date updateTime = DateUtils.getCurrTimestamp();
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            updId = userInfo.getLoginCode();
            // 申请机构
            updBrId = userInfo.getOrg().getCode();
        }
        record.setUpdId(updId);
        record.setUpdBrId(updBrId);
        record.setUpdDate(openDay);
        record.setUpdateTime(updateTime);
        record.setInputDate(null);//更新的时候不更新登记日期
        record.setWriteoffStatus(CmisNpamConstants.WRITEOFF_STATUS_01);
        count = plaBadDebtWriteoffRecordAppMapper.updateByPrimaryKeySelective(record);
        return count;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaBadDebtWriteoffRecordApp record) {
        return plaBadDebtWriteoffRecordAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pbdwraSerno) {
        int count =0;
        count =plaBadDebtWriteoffRecordAppMapper.deleteByPrimaryKey(pbdwraSerno);
        // 删除子数据
        if(count > 0){
            List<PlaBadDebtWriteoffRecordBillRel> plaBadDebtWriteoffRecordBillRelList = plaBadDebtWriteoffRecordBillRelService.selectListByPbdwraSerno(pbdwraSerno);
            if(CollectionUtils.nonEmpty(plaBadDebtWriteoffRecordBillRelList)){
                for(PlaBadDebtWriteoffRecordBillRel plaBadDebtWriteoffRecordBillRel:plaBadDebtWriteoffRecordBillRelList){
                    plaBadDebtWriteoffRecordBillRelService.deleteplaBadDebtWriteoffRecordBillRel(plaBadDebtWriteoffRecordBillRel);
                }
            }
        }
        return count;
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaBadDebtWriteoffRecordAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: save
     * @方法描述: 保存
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    public int save(PlaBadDebtWriteoffRecordApp record) {
        int count = 0;
        if (record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            record.setInputId(inputId);
            record.setInputBrId(inputBrId);
            record.setInputDate(openDay);
            record.setCreateTime(createTime);
            record.setUpdId(inputId);
            record.setUpdBrId(inputBrId);
            record.setUpdDate(openDay);
            record.setUpdateTime(createTime);
            record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            // 流水号
            // String serno= StringUtils.getUUID();//sequenceTemplateService.getSequenceTemplate("", new HashMap<>());
            // record.setPbdwasSerno(serno);
            count = plaBadDebtWriteoffRecordAppMapper.insert(record);
        }
        return count;
    }

    /**
     * 呆账核销发送核销记账
     *
     * @author 刘权
     **/
    public ResultDto<String> sendToHXJZ(PlaBadDebtWriteoffRecordApp appInfo) throws Exception {
        ResultDto<String> resultDto = new ResultDto<>();
        if (appInfo != null) {
            Supplier<Mbt951ReqDto> supplier = Mbt951ReqDto::new;
            // 1、先上传文件
            List<PlaBadDebtWriteoffRecordBillRel> list = plaBadDebtWriteoffRecordBillRelService.selectListByPbdwraSerno(appInfo.getPbdwraSerno());
            String fileName = uploadFile(list, appInfo.getPbdwraSerno());
            String openDay = DateUtils.formatDate10To8(stringRedisTemplate.opsForValue().get("openDay"));
            Mbt951ReqDto mbt951ReqDto = supplier.get();
            mbt951ReqDto.setZhhmjcbz("");
            mbt951ReqDto.setBizhongg("01");
            mbt951ReqDto.setKoukzhmc("");
            mbt951ReqDto.setLilvfdsz(BigDecimal.ZERO);
            mbt951ReqDto.setQixiriqi("");
            mbt951ReqDto.setYngxguiy("");
            mbt951ReqDto.setZskhbzhi("");
            mbt951ReqDto.setChaohubz("x");//x 无
            mbt951ReqDto.setShijlilv(BigDecimal.ZERO);
            mbt951ReqDto.setCunqiiii("");
            mbt951ReqDto.setNfshoqfs("");
            mbt951ReqDto.setDaoqxcfs("");
            mbt951ReqDto.setDailzjho("");
            mbt951ReqDto.setLilvfdbl(BigDecimal.ZERO);
            mbt951ReqDto.setZhaiyodm("");
            mbt951ReqDto.setPicriqii(openDay);//DateUtils.getCurrDateStr().replace("-", ""));//
            mbt951ReqDto.setPicileib("QT");
            mbt951ReqDto.setZcuncqii("");
            mbt951ReqDto.setTduifwei("");
            mbt951ReqDto.setChapbhao("");
            mbt951ReqDto.setFilename(fileName);
            mbt951ReqDto.setDfzhangh("");
            mbt951ReqDto.setPiciczlx("A");
            mbt951ReqDto.setBeiyzd02("");
            mbt951ReqDto.setSbzjclfs("0");
            mbt951ReqDto.setBeiyzd01("");
            mbt951ReqDto.setLuruguiy("");
            mbt951ReqDto.setJizhunll(BigDecimal.ZERO);
            mbt951ReqDto.setKoukleix("");
            mbt951ReqDto.setYouhuibz("");
            mbt951ReqDto.setZongjine(BigDecimal.ZERO);
            mbt951ReqDto.setZhufldm2("");
            mbt951ReqDto.setJiefzhje(BigDecimal.ZERO);
            mbt951ReqDto.setZhufldm1("");
            mbt951ReqDto.setLlfdonbz("");
            mbt951ReqDto.setWeixdhao("");
            mbt951ReqDto.setDailxinm("");
            mbt951ReqDto.setQixifans("");
            mbt951ReqDto.setPingzhzl("");
            mbt951ReqDto.setPicihaoo(appInfo.getPbdwraSerno());
            mbt951ReqDto.setKoukzhao("");
            mbt951ReqDto.setFuwudaim("dkplhx");
            mbt951ReqDto.setZhfutojn("");
            mbt951ReqDto.setDlywhaoo("");
            mbt951ReqDto.setBanlwand("");
            mbt951ReqDto.setDaifzhje(BigDecimal.ZERO);
            mbt951ReqDto.setYouhuisz(BigDecimal.ZERO);
            mbt951ReqDto.setLururiqi(openDay);//DateUtils.getCurrDateStr().replace("-", ""));//DateUtils.getCurrDateStr().replace("-", ""));
            mbt951ReqDto.setJysjzifu("");
            mbt951ReqDto.setZongbis(Integer.valueOf(appInfo.getWriteoffCount()));
            log.info("核销记账请求信息报文：" + mbt951ReqDto);
            // 调用核心接口，进行债权减免发送核心记账
            ResultDto<Mbt951RespDto> mbt951RespDto = dscms2CoreMbtClientService.mbt951(mbt951ReqDto);
            String mbt951Code = Optional.ofNullable(mbt951RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String mbt951Meesage = Optional.ofNullable(mbt951RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            log.info("核销记账返回报文：" + mbt951RespDto);
            if (Objects.equals(mbt951Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_02);
                appInfo.setRecordDate(DateUtils.getCurrDateStr());
                appInfo.setWriteoffStatus(CmisNpamConstants.WRITEOFF_STATUS_02);
                updateSelective(appInfo);
                resultDto.setMessage(EcnEnum.ECN060000.value);
            } else {
                appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_04);
                appInfo.setRecordDate(DateUtils.getCurrDateStr());
                appInfo.setWriteoffStatus(CmisNpamConstants.WRITEOFF_STATUS_04);
                updateSelective(appInfo);
                resultDto.setCode(mbt951Code);
                resultDto.setMessage(mbt951Meesage);
            }
        } else {
            throw BizException.error(null, EcnEnum.ECN060005.key, EcnEnum.ECN060005.value);
        }
        return resultDto;
    }


    /**
     * 呆账核销发送同步核销记账
     *
     * @author 刘权
     **/
    public ResultDto<String> sendToTBHXJZ(PlaBadDebtWriteoffRecordApp appInfo) throws Exception {
        ResultDto<String> resultDto = new ResultDto<>();
        InputStreamReader read = null;
        BufferedReader bufferedReader = null;
        FileInputStream fileInputStream = null;
        try {
            if (appInfo != null) {
                Supplier<Mbt952ReqDto> supplier = Mbt952ReqDto::new;
                String openDay = DateUtils.formatDate10To8(stringRedisTemplate.opsForValue().get("openDay"));
                Mbt952ReqDto mbt952ReqDto = supplier.get();
                String pbdwraSerno = appInfo.getPbdwraSerno();
                //批次号
                mbt952ReqDto.setYpicihao(pbdwraSerno);
                //批次日期
                mbt952ReqDto.setPicriqii(openDay);
                // 调用核心接口，进行债权减免发送核心记账
                ResultDto<Mbt952RespDto> mbt952RespDto = dscms2CoreMbtClientService.mbt952(mbt952ReqDto);
                String mbt952Code = Optional.ofNullable(mbt952RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                String mbt952Meesage = Optional.ofNullable(mbt952RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                if (Objects.equals(mbt952Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                    Mbt952RespDto mbt952RespDtoData = mbt952RespDto.getData();
                    List<CplWjplxxOut> cplWjplxxOutList = mbt952RespDtoData.getCplWjplxxOut();
                    // 服务器上文件名称
                    String fileName = "";
                    // 服务器文件路径
                    String remoteFolder = "";
                    // 本地文件路径
                    String loanFolder = "";
                    String path = "/app/ftpuser/ftp/";
                    for (CplWjplxxOut cplWjplxxOut : cplWjplxxOutList) {
                        fileName = cplWjplxxOut.getFilename().substring(19);
                        remoteFolder = path + cplWjplxxOut.getFilename().substring(0, 19);
                        // 下载ftp文件
                        loanFolder = downLoanFile(remoteFolder, fileName);
                        File file = new File(loanFolder + fileName);
                        fileInputStream = new FileInputStream(file);
                        read = new InputStreamReader(fileInputStream, Charset.forName("UTF-8"));
                        bufferedReader = new BufferedReader(read);
                        String lineTxt = null;
                        // 文件日期
                        String fileDate = "";
                        // 借据号
                        String billNo = "";
                        String flag = "";
                        // 记账状态
                        String recordStatus = "";
                        // 核销状态
                        String writeoffStatus = "";
                        // 记账成功
                        boolean successFlag = false;
                        // 记账失败
                        boolean faileFlag = false;
                        Supplier<PlaBadDebtWriteoffRecordBillRel> suppliers = PlaBadDebtWriteoffRecordBillRel::new;
                        PlaBadDebtWriteoffRecordBillRel plaBadDebtWriteoffRecordBillRel = suppliers.get();
                        plaBadDebtWriteoffRecordBillRel.setPbdwraSerno(pbdwraSerno);
                        while ((lineTxt = bufferedReader.readLine()) != null) {
                            log.info("读取文件成功，文件内容为：" + lineTxt);
                            if ((lineTxt != null) && lineTxt.length() > 0) {
                                {
                                    String[] line = lineTxt.split("<f>");
                                    log.info("解析后数据" + line);
                                    if (line[1] != null && line[1].length() > 7) {
                                        fileDate = DateUtils.formatDate8To10(line[1]);
                                    }
                                    billNo = line[2];
                                    flag = line[3];
                                    if ("0".equals(flag)) {
                                        // 记账失败
                                        faileFlag = true;
                                        plaBadDebtWriteoffRecordBillRel.setBillNo(billNo);
                                        plaBadDebtWriteoffRecordBillRel.setPbdwraSerno(pbdwraSerno);
                                        plaBadDebtWriteoffRecordBillRel.setRecordStatus(CmisNpamConstants.HX_STATUS_04);
                                        plaBadDebtWriteoffRecordBillRel.setRecordDate(fileDate);
                                        plaBadDebtWriteoffRecordBillRel.setWriteoffStatus(CmisNpamConstants.WRITEOFF_STATUS_04);
                                        plaBadDebtWriteoffRecordBillRel.setWriteoffDate(fileDate);
                                        plaBadDebtWriteoffRecordBillRelService.updateByPbdwraSerno(plaBadDebtWriteoffRecordBillRel);
                                    } else {
                                        // 记账成功
                                        successFlag = true;
                                        plaBadDebtWriteoffRecordBillRel.setBillNo(billNo);
                                        plaBadDebtWriteoffRecordBillRel.setPbdwraSerno(pbdwraSerno);
                                        plaBadDebtWriteoffRecordBillRel.setRecordStatus(CmisNpamConstants.HX_STATUS_03);
                                        plaBadDebtWriteoffRecordBillRel.setRecordDate(fileDate);
                                        plaBadDebtWriteoffRecordBillRel.setWriteoffStatus(CmisNpamConstants.WRITEOFF_STATUS_03);
                                        plaBadDebtWriteoffRecordBillRel.setWriteoffDate(fileDate);
                                        plaBadDebtWriteoffRecordBillRelService.updateByPbdwraSerno(plaBadDebtWriteoffRecordBillRel);
                                        // 核销成功更新台账状态
                                        Map<String, String> map = new HashMap();
                                        map.put("billNo", billNo);
                                        map.put("accStatus", "5");// 已核销
                                        cmisBizClientService.updateAccLoanByBillNo(map);
                                    }
                                }
                            }
                        }
                        // 记账成功
                        if (successFlag && !faileFlag) {
                            recordStatus = CmisNpamConstants.HX_STATUS_03;
                            writeoffStatus = CmisNpamConstants.WRITEOFF_STATUS_03;
                            // 部分记账成功
                        } else if (successFlag && faileFlag) {
                            recordStatus = CmisNpamConstants.HX_STATUS_05;
                            writeoffStatus = CmisNpamConstants.WRITEOFF_STATUS_04;
                            // 记账失败
                        } else {
                            recordStatus = CmisNpamConstants.HX_STATUS_04;
                            writeoffStatus = CmisNpamConstants.WRITEOFF_STATUS_04;
                        }
                        appInfo.setRecordStatus(recordStatus);
                        appInfo.setWriteoffStatus(writeoffStatus);
                        appInfo.setWriteoffDate(fileDate);
                        appInfo.setRecordDate(fileDate);
                        updateSelective(appInfo);
                        resultDto.setMessage(EcnEnum.ECN060000.value);
                    }
                } else {
                    throw BizException.error(null, EcnEnum.ECN060013.key, EcnEnum.ECN060013.value + mbt952Meesage);
                }

            } else {
                throw BizException.error(null, EcnEnum.ECN060005.key, EcnEnum.ECN060005.value);
            }
        } catch (Exception e) {
            throw BizException.error(null, e.getMessage());
        } finally {
            if (read != null) {
                read.close();
            }
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            if (fileInputStream != null) {
                fileInputStream.close();
            }
        }
        return resultDto;
    }

    public String uploadFile(List<PlaBadDebtWriteoffRecordBillRel> list, String serno) throws Exception {
        BufferedWriter out = null;
        File writename = null;
        // 借据编号
        String billNo = "";
        // 文件体
        String result = "";
        //文件名
        String fileName = "dkplhx" + serno + ".txt";//serno
        log.info("***********查询配置信息开始*START**************");
        ResultDto<CfgSftpDto> resultDto = iCmisCfgClientService.queryCfgSftpByBizScenseType("02");
        String openDay = DateUtils.formatDate10To8(stringRedisTemplate.opsForValue().get("openDay"));
        log.info("***********查询配置信息结束*END*****************");
        //ip
        String ip = resultDto.getData().getHost();
        // 端口
        int port = Integer.valueOf(resultDto.getData().getPort());
        //账号
        String userName = resultDto.getData().getUsername();
        // 密码
        String password = resultDto.getData().getPassword();
        // 本地文件路劲
        String localFilePath = resultDto.getData().getLocalpath() + "/" + fileName;
        String path = resultDto.getData().getServerpath();
        String newPath = "QT";
        log.info("本地存放地址" + localFilePath);
        FtpUtils ftp = null;
        try {
            // 1、获取借据并且拼接
            if (CollectionUtils.nonEmpty(list)) {
                for(int i=0;i<list.size();i++){
                    PlaBadDebtWriteoffRecordBillRel plaBadDebtWriteoffRecordBillRel = list.get(i);
                    billNo = plaBadDebtWriteoffRecordBillRel.getBillNo();
                    if(i!=list.size()-1){
                        result += billNo + "<f>"+ "\n";
                    }else{
                        result += billNo + "<f>";
                    }
                }
//                for (PlaBadDebtWriteoffRecordBillRel plaBadDebtWriteoffRecordBillRel : list) {
//                    billNo = plaBadDebtWriteoffRecordBillRel.getBillNo();
//                    result += billNo + "<f>" + "\n";
//                }
                log.info("拼接借据" + result);
                // 2、写入文件
                log.info("+++++++++++++++++++开始写入文件+++++++++++++++++++++++++++++++");
                writename = new File(localFilePath);
                Boolean b = writename.createNewFile();
                if (!b) {
                    log.info("创建类失败！");
                }
                // 建立写入对象
                out = new BufferedWriter(new FileWriter(writename));
                out.write(result);
                out.flush();
                log.info("+++++++++++++++++++写入文件成功+++++++++++++++++++++++++++++++");
                // 3、上传文件
                log.info("+++++++++++++++++++开始上传文件+++++++++++++++++++++++++++++++");
                ftp = new FtpUtils(ip, port, userName, password);
                ftp.connectServer();
                ftp.makeDirectory(path, openDay, newPath);
                ftp.upload(path + "/" + openDay + "/" + newPath, writename);
            }
        } catch (Exception e) {
            throw new Exception(EcnEnum.ECN069999.value);
        } finally {
            if (out != null) {
                out.close();
            }
            if (ftp != null) {
                ftp.closeConnect();
            }
        }
        return fileName;
    }

    /**
     * 下载文件到本地
     *
     * @param remoteFolder
     * @param fileName
     * @return
     * @throws Exception
     */
    public String downLoanFile(String remoteFolder, String fileName) throws Exception {

        FtpUtils ftp = null;
        log.info("***********查询配置信息开始*START**************");
        ResultDto<CfgSftpDto> resultDto = iCmisCfgClientService.queryCfgSftpByBizScenseType("02");
        log.info("***********查询配置信息结束*END*****************");
        //ip
        String ip = resultDto.getData().getHost();
        // 端口
        int port = Integer.valueOf(resultDto.getData().getPort());
        //账号
        String userName = resultDto.getData().getUsername();
        // 密码
        String password = resultDto.getData().getPassword();
        //本地路劲
        String loanFolder = resultDto.getData().getLocalpath();
        try {
            // 1、连接ftp
            log.info("+++++++++++++++++++连接ftp++++++++++++++++++++++++++++++");
            ftp = new FtpUtils(ip, port, userName, password);
            ftp.connectServer();
            // 2、校验文件是否存在
            boolean isHave = ftp.checkFTPFile(remoteFolder, fileName);
            if (isHave) {
                // 3、获取文件并且解析
                ftp.download(remoteFolder + fileName, loanFolder + fileName);

            } else {
                throw BizException.error(null, EcnEnum.ECN060014.key, EcnEnum.ECN060014.value);
            }
        } catch (Exception e) {
            throw new Exception(EcnEnum.ECN069999.value);
        } finally {
            if (ftp != null) {
                ftp.closeConnect();
            }
        }
        return loanFolder;
    }

    /**
     *  发送核心记账，循环单笔方式
     * @param appInfo
     * @return
     * @throws Exception
     */
    public ResultDto<String> sendToHXRecord(PlaBadDebtWriteoffRecordApp appInfo) throws Exception {
        ResultDto<String> resultDto = new ResultDto<>();
        // 核销总金额
        BigDecimal totalWriteoffAmt = BigDecimal.ZERO;
        // 核销总本金
        BigDecimal totalWriteoffCap = BigDecimal.ZERO;
        // 核销总利息
        BigDecimal totalWriteoffInt = BigDecimal.ZERO;
        // 判断核销状态
//        Boolean successFlag = true;
//        String writeoffStatus = "";
        if (appInfo != null) {
            List<PlaBadDebtWriteoffRecordBillRel> list = plaBadDebtWriteoffRecordBillRelService.selectListByPbdwraSerno(appInfo.getPbdwraSerno());
            for(PlaBadDebtWriteoffRecordBillRel plaBadDebtWriteoffRecordBillRel: list){
                resultDto =plaBadDebtWriteoffRecordBillRelService.sendToDKHXCL(plaBadDebtWriteoffRecordBillRel);
            }
//            // 核销成功 更新主表信息
//            if("0".equals(resultDto.getCode())){
//                for(PlaBadDebtWriteoffRecordBillRel plaBadDebtWriteoffRecordBillRel: list){
//                    totalWriteoffAmt=totalWriteoffAmt.add(plaBadDebtWriteoffRecordBillRel.getLoanBalance().add(plaBadDebtWriteoffRecordBillRel.getTotalTqlxAmt()));
//                    totalWriteoffCap=totalWriteoffCap.add(plaBadDebtWriteoffRecordBillRel.getLoanBalance());
//                    totalWriteoffInt= totalWriteoffInt.add(plaBadDebtWriteoffRecordBillRel.getTotalTqlxAmt());
//                    writeoffStatus=plaBadDebtWriteoffRecordBillRel.getWriteoffStatus();
//                    if(CmisNpamConstants.WRITEOFF_STATUS_04.equals(writeoffStatus)){
//                        successFlag= false;
//                    }
//                }
//                // 如果都是成功
//                if(successFlag){
//                    appInfo.setWriteoffStatus(CmisNpamConstants.WRITEOFF_STATUS_03);
//                    appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_03);
//                    appInfo.setWriteoffDate(DateUtils.getCurrDateStr());
//                    appInfo.setRecordDate(DateUtils.getCurrDateStr());
//                }else{
//                    appInfo.setWriteoffStatus(CmisNpamConstants.WRITEOFF_STATUS_04);
//                    appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_04);
//                    appInfo.setWriteoffDate(DateUtils.getCurrDateStr());
//                    appInfo.setRecordDate(DateUtils.getCurrDateStr());
//                }
//                // 更新主表信息
//                appInfo.setTotalWriteoffAmt(totalWriteoffAmt);
//                appInfo.setTotalWriteoffCap(totalWriteoffCap);
//                appInfo.setTotalWriteoffInt(totalWriteoffInt);
//                updateSelective(appInfo);
//            }
        }else{
            resultDto.setMessage( EcnEnum.ECN060005.value);
        }
       return resultDto;
    }

    /**
     * @方法名称: updateBill
     * @方法描述: 单户呆账核销审批通过后同步核心借据并更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateBill(PlaBadDebtWriteoffRecordApp record) {
        // 1、先查询关联借据信息
        String pbdwraSerno= record.getPbdwraSerno();
        List<PlaBadDebtWriteoffRecordBillRel> plaBadDebtWriteoffRecordBillRelList=plaBadDebtWriteoffRecordBillRelService.selectListByPbdwraSerno(pbdwraSerno);
        // 2、调用和兴3100接口查询借据
        String billNo = "";
        // 核销总金额
        BigDecimal totalWriteoffAmt = BigDecimal.ZERO;
        // 核销总本金
        BigDecimal totalWriteoffCap = BigDecimal.ZERO;
        // 核销总利息
        BigDecimal totalWriteoffInt = BigDecimal.ZERO;
        // 贷款金额
        BigDecimal loanAmt = BigDecimal.ZERO;
        // 贷款余额
        BigDecimal loanBalance = BigDecimal.ZERO;
        // 拖欠利息总额
        BigDecimal totalTqlxAmt = BigDecimal.ZERO;
        for(PlaBadDebtWriteoffRecordBillRel plaBadDebtWriteoffRecordBillRel: plaBadDebtWriteoffRecordBillRelList){
            billNo = plaBadDebtWriteoffRecordBillRel.getBillNo();
            ResultDto<Ln3100RespDto> ln3100RespDto =ln3100Server.queryHXBill(billNo);
            log.info("ln3100接口返回：" + ln3100RespDto);
            String ln3100Code = Optional.ofNullable(ln3100RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String ln3100Meesage = Optional.ofNullable(ln3100RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            if (Objects.equals(ln3100Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                if(ln3100RespDto.getData() != null){
                    loanAmt=ln3100RespDto.getData().getJiejuuje();
                    loanBalance=ln3100RespDto.getData().getYuqibjin().add(ln3100RespDto.getData().getZhchbjin());
                    totalTqlxAmt=ln3100RespDto.getData().getYsyjlixi().add(ln3100RespDto.getData().getCsyjlixi()).add(ln3100RespDto.getData().getYsqianxi())
                            .add(ln3100RespDto.getData().getCsqianxi()).add(ln3100RespDto.getData().getYsyjfaxi()).add(ln3100RespDto.getData().getCsyjfaxi()).add(ln3100RespDto.getData().getYshofaxi())
                            .add(ln3100RespDto.getData().getCshofaxi()).add(ln3100RespDto.getData().getYingjifx()).add(ln3100RespDto.getData().getFuxiiiii());
                    plaBadDebtWriteoffRecordBillRel.setLoanAmt(loanAmt);
                    plaBadDebtWriteoffRecordBillRel.setLoanBalance(loanBalance);
                    plaBadDebtWriteoffRecordBillRel.setTotalTqlxAmt(totalTqlxAmt);
                    // 计算主表金额
                    totalWriteoffCap=totalWriteoffCap.add(loanBalance);
                    totalWriteoffInt= totalWriteoffInt.add(totalTqlxAmt);
                    totalWriteoffAmt=totalWriteoffCap.add(totalWriteoffInt);
                    plaBadDebtWriteoffRecordBillRelService.updateSelective(plaBadDebtWriteoffRecordBillRel);
                }
            }
        }
        // 3、更新主表数据
        record.setTotalWriteoffAmt(totalWriteoffAmt);
        record.setTotalWriteoffCap(totalWriteoffCap);
        record.setTotalWriteoffInt(totalWriteoffInt);
        return updateSelective(record);
    }
}
