/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaAssetPldLeaseAmtInfo;
import cn.com.yusys.yusp.service.PlaAssetPldLeaseAmtInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldLeaseAmtInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-09 15:13:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plaassetpldleaseamtinfo")
public class PlaAssetPldLeaseAmtInfoResource {
    @Autowired
    private PlaAssetPldLeaseAmtInfoService plaAssetPldLeaseAmtInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaAssetPldLeaseAmtInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaAssetPldLeaseAmtInfo> list = plaAssetPldLeaseAmtInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaAssetPldLeaseAmtInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<PlaAssetPldLeaseAmtInfo>> index(QueryModel queryModel) {
        List<PlaAssetPldLeaseAmtInfo> list = plaAssetPldLeaseAmtInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaAssetPldLeaseAmtInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{paplaiSerno}")
    protected ResultDto<PlaAssetPldLeaseAmtInfo> show(@PathVariable("paplaiSerno") String paplaiSerno) {
        PlaAssetPldLeaseAmtInfo plaAssetPldLeaseAmtInfo = plaAssetPldLeaseAmtInfoService.selectByPrimaryKey(paplaiSerno);
        return new ResultDto<PlaAssetPldLeaseAmtInfo>(plaAssetPldLeaseAmtInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<PlaAssetPldLeaseAmtInfo> create(@RequestBody PlaAssetPldLeaseAmtInfo plaAssetPldLeaseAmtInfo) throws URISyntaxException {
        plaAssetPldLeaseAmtInfoService.insert(plaAssetPldLeaseAmtInfo);
        return new ResultDto<PlaAssetPldLeaseAmtInfo>(plaAssetPldLeaseAmtInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaAssetPldLeaseAmtInfo plaAssetPldLeaseAmtInfo) throws URISyntaxException {
        int result = plaAssetPldLeaseAmtInfoService.update(plaAssetPldLeaseAmtInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{paplaiSerno}")
    protected ResultDto<Integer> delete(@PathVariable("paplaiSerno") String paplaiSerno) {
        int result = plaAssetPldLeaseAmtInfoService.deleteByPrimaryKey(paplaiSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaAssetPldLeaseAmtInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据出租流水号获取租金收取情况信息
     *
     * @author jijian_yx
     * @date 2021/6/10 14:24
     **/
    @ApiOperation("根据出租流水号获取租金收取情况信息")
    @PostMapping("/queryamtinfo")
    protected ResultDto<List<PlaAssetPldLeaseAmtInfo>> queryAmtInfo(@RequestBody QueryModel queryModel) {
        List<PlaAssetPldLeaseAmtInfo> list = plaAssetPldLeaseAmtInfoService.queryAmtInfo(queryModel);
        return new ResultDto<List<PlaAssetPldLeaseAmtInfo>>(list);
    }

    /**
     * 保存租金收取信息
     *
     * @author jijian_yx
     * @date 2021/6/11 13:28
     **/
    @ApiOperation("保存租金收取信息")
    @PostMapping("/insert")
    protected ResultDto<Integer> insert(@RequestBody List<PlaAssetPldLeaseAmtInfo> amtInfoList) {
        int result = plaAssetPldLeaseAmtInfoService.insertAmtInfoList(amtInfoList);
        return new ResultDto<Integer>(result);
    }
}
