/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawAccusedList
 * @类描述: pla_law_accused_list数据实体类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-05 17:45:58
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_law_accused_list")
public class PlaLawAccusedList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 被告流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PLAL_SERNO")
    private String plalSerno;

    /**
     * 案件流水号
     **/
    @Column(name = "CASE_SERNO", unique = false, nullable = true, length = 40)
    private String caseSerno;

    /**
     * 客户编号
     **/
    @Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
    private String cusId;

    /**
     * 客户名称
     **/
    @Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
    private String cusName;

    /**
     * 被告身份
     **/
    @Column(name = "ACCUSED_ROLE", unique = false, nullable = true, length = 5)
    private String accusedRole;

    /**
     * 证件类型
     **/
    @Column(name = "CERT_TYPE", unique = false, nullable = true, length = 5)
    private String certType;

    /**
     * 证件号码
     **/
    @Column(name = "CERT_CODE", unique = false, nullable = true, length = 40)
    private String certCode;

    /**
     * 住所
     **/
    @Column(name = "LIVING_ADDR", unique = false, nullable = true, length = 500)
    private String livingAddr;

    /**
     * 是否起诉
     **/
    @Column(name = "IS_LAWSUIT", unique = false, nullable = true, length = 5)
    private String isLawsuit;

    /**
     * 是否撤回
     **/
    @Column(name = "IS_WITHDRAW", unique = false, nullable = true, length = 5)
    private String isWithdraw;

    /**
     * 撤回时间
     **/
    @Column(name = "WITHDRAW_DATE", unique = false, nullable = true, length = 10)
    private String withdrawDate;

    /**
     * 撤回原因
     **/
    @Column(name = "WITHDRAW_RESN", unique = false, nullable = true, length = 5)
    private String withdrawResn;

    /**
     * 是否其他被告人
     **/
    @Column(name = "IS_OTHER_ACCUSED", unique = false, nullable = true, length = 5)
    private String isOtherAccused;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaLawAccusedList() {
        // default implementation ignored
    }

    /**
     * @param plalSerno
     */
    public void setPlalSerno(String plalSerno) {
        this.plalSerno = plalSerno;
    }

    /**
     * @return plalSerno
     */
    public String getPlalSerno() {
        return this.plalSerno;
    }

    /**
     * @param caseSerno
     */
    public void setCaseSerno(String caseSerno) {
        this.caseSerno = caseSerno;
    }

    /**
     * @return caseSerno
     */
    public String getCaseSerno() {
        return this.caseSerno;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param accusedRole
     */
    public void setAccusedRole(String accusedRole) {
        this.accusedRole = accusedRole;
    }

    /**
     * @return accusedRole
     */
    public String getAccusedRole() {
        return this.accusedRole;
    }

    /**
     * @param certType
     */
    public void setCertType(String certType) {
        this.certType = certType;
    }

    /**
     * @return certType
     */
    public String getCertType() {
        return this.certType;
    }

    /**
     * @param certCode
     */
    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    /**
     * @return certCode
     */
    public String getCertCode() {
        return this.certCode;
    }

    /**
     * @param livingAddr
     */
    public void setLivingAddr(String livingAddr) {
        this.livingAddr = livingAddr;
    }

    /**
     * @return livingAddr
     */
    public String getLivingAddr() {
        return this.livingAddr;
    }

    /**
     * @param isLawsuit
     */
    public void setIsLawsuit(String isLawsuit) {
        this.isLawsuit = isLawsuit;
    }

    /**
     * @return isLawsuit
     */
    public String getIsLawsuit() {
        return this.isLawsuit;
    }

    /**
     * @param isWithdraw
     */
    public void setIsWithdraw(String isWithdraw) {
        this.isWithdraw = isWithdraw;
    }

    /**
     * @return isWithdraw
     */
    public String getIsWithdraw() {
        return this.isWithdraw;
    }

    /**
     * @param withdrawDate
     */
    public void setWithdrawDate(String withdrawDate) {
        this.withdrawDate = withdrawDate;
    }

    /**
     * @return withdrawDate
     */
    public String getWithdrawDate() {
        return this.withdrawDate;
    }

    /**
     * @param withdrawResn
     */
    public void setWithdrawResn(String withdrawResn) {
        this.withdrawResn = withdrawResn;
    }

    /**
     * @return withdrawResn
     */
    public String getWithdrawResn() {
        return this.withdrawResn;
    }

    /**
     * @param isOtherAccused
     */
    public void setIsOtherAccused(String isOtherAccused) {
        this.isOtherAccused = isOtherAccused;
    }

    /**
     * @return isOtherAccused
     */
    public String getIsOtherAccused() {
        return this.isOtherAccused;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}