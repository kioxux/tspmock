/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Supplier;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.PlaDebtClaimReducAppInfo;
import cn.com.yusys.yusp.domain.PlaDebtClaimReducRecordAppInfo;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.dto.PlaDebtClaimReducBillRelDto;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaDebtClaimReducBillRel;
import cn.com.yusys.yusp.repository.mapper.PlaDebtClaimReducBillRelMapper;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaDebtClaimReducBillRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-08 19:44:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaDebtClaimReducBillRelService {

    @Autowired
    private PlaDebtClaimReducBillRelMapper plaDebtClaimReducBillRelMapper;

    @Autowired
    private PlaDebtClaimReducAppInfoService plaDebtClaimReducAppInfoService;

    @Autowired
    private PlaDebtClaimReducRecordAppInfoService plaDebtClaimReducRecordAppInfoService;

    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    private static final Logger log = LoggerFactory.getLogger(PlaDebtClaimReducRecordBillRelService.class);

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaDebtClaimReducBillRel selectByPrimaryKey(String pdcrbrSerno) {
        return plaDebtClaimReducBillRelMapper.selectByPrimaryKey(pdcrbrSerno);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaDebtClaimReducBillRel> selectAll(QueryModel model) {
        List<PlaDebtClaimReducBillRel> records = (List<PlaDebtClaimReducBillRel>) plaDebtClaimReducBillRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaDebtClaimReducBillRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaDebtClaimReducBillRel> list = plaDebtClaimReducBillRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaDebtClaimReducBillRel record) {
        return plaDebtClaimReducBillRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaDebtClaimReducBillRel record) {
        return plaDebtClaimReducBillRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaDebtClaimReducBillRel record) {
        return plaDebtClaimReducBillRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaDebtClaimReducBillRel record) {
        return plaDebtClaimReducBillRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pdcrbrSerno) {
        return plaDebtClaimReducBillRelMapper.deleteByPrimaryKey(pdcrbrSerno);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据业务流水号删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPdcraiSerno(String pdcraiSerno) {
        return plaDebtClaimReducBillRelMapper.deleteByPdcraiSerno(pdcraiSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaDebtClaimReducBillRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询单个金额总和
     * @参数与返回说明:
     * @算法描述: 无
     * 刘权
     */

    public Map selectBySum(String pdcraiSerno) {
        Map map = plaDebtClaimReducBillRelMapper.selectBySum(pdcraiSerno);
        return map;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入债权减免申请信息表
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertByBillRel(List<PlaDebtClaimReducBillRel> list) {
        int count = 0;
        if(CollectionUtils.nonEmpty(list)){
            for (PlaDebtClaimReducBillRel record : list) {
                if (record != null) {
                    // 申请人
                    String inputId = "";
                    // 申请机构
                    String inputBrId = "";
                    // 申请时间
                    String openDay = stringRedisTemplate.opsForValue().get("openDay");
                    // 创建时间
                    Date createTime = DateUtils.getCurrTimestamp();
                    // 获取用户信息
                    User userInfo = SessionUtils.getUserInformation();
                    if (userInfo != null) {
                        // 申请人
                        inputId = userInfo.getLoginCode();
                        // 申请机构
                        inputBrId = userInfo.getOrg().getCode();
                    }
                    PlaDebtClaimReducBillRel plaDebtClaimReducBillRel =plaDebtClaimReducBillRelMapper.selectByPdcraiSerno(record.getPdcraiSerno(),record.getBillNo());
                    if(plaDebtClaimReducBillRel !=null){
                        record.setUpdId(inputId);
                        record.setUpdBrId(inputBrId);
                        record.setUpdDate(openDay);
                        record.setUpdateTime(createTime);
                        plaDebtClaimReducBillRelMapper.updateByPrimaryKeySelective(record);
                    }else{
                        record.setPdcrbrSerno(StringUtils.getUUID());
                        record.setInputId(inputId);
                        record.setInputBrId(inputBrId);
                        record.setInputDate(openDay);
                        record.setCreateTime(createTime);
                        record.setUpdId(inputId);
                        record.setUpdBrId(inputBrId);
                        record.setUpdDate(openDay);
                        record.setUpdateTime(createTime);
                        plaDebtClaimReducBillRelMapper.insertSelective(record);
                    }
                }
            }
            Map map = plaDebtClaimReducBillRelMapper.selectBySum(list.get(0).getPdcraiSerno());
            if (CollectionUtils.nonEmpty(map)) {
                // 贷款总金额
                BigDecimal loanAmt=(BigDecimal) map.get("loanAmt");
                // 贷款总余额
                BigDecimal loanBalance=(BigDecimal) map.get("loanBalance");
                // 拖欠利息总额
                BigDecimal totalTqlxAmt=(BigDecimal) map.get("totalTqlxAmt");
                //获取当前业务流水号下的数据
                PlaDebtClaimReducAppInfo plaDebtClaimReducAppInfo = plaDebtClaimReducAppInfoService.selectByPrimaryKey(list.get(0).getPdcraiSerno());
                if (plaDebtClaimReducAppInfo != null) {
                    plaDebtClaimReducAppInfo.setLoanAmt(loanAmt);
                    plaDebtClaimReducAppInfo.setLoanBalance(loanBalance);
                    plaDebtClaimReducAppInfo.setTotalTqlxAmt(totalTqlxAmt);
                    count = plaDebtClaimReducAppInfoService.update(plaDebtClaimReducAppInfo);
                }
            }
        }
        return count;
    }

    /**
     * @方法名称: insert
     * @方法描述:   插入债权减免记账申请信息表
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertByRecordAppInfo(PlaDebtClaimReducBillRel record) {

        int count = 0;
        if (record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();
            }
            record.setCreateTime(createTime);
            record.setUpdId(inputId);
            record.setUpdBrId(inputBrId);
            record.setUpdDate(inputDate);
            record.setUpdateTime(createTime);
            plaDebtClaimReducBillRelMapper.updateByPrimaryKeySelective(record);

            //获取当前债权减免记账申请信息表中的数据
            PlaDebtClaimReducRecordAppInfo plaDebtClaimReducRecordAppInfo = plaDebtClaimReducRecordAppInfoService.selectByPrimaryKey(record.getPdcraiSerno());
            plaDebtClaimReducRecordAppInfo.setCusId(record.getCusId());
            plaDebtClaimReducRecordAppInfo.setCusName(record.getCusName());
        }
        return count;
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPdcrbrSerno(PlaDebtClaimReducBillRel record) {
        int count =0;
        if(record !=null){
            count=plaDebtClaimReducBillRelMapper.deleteByPdcraiSerno(record.getPdcraiSerno());
            if(count> 0){
                Map map = plaDebtClaimReducBillRelMapper.selectBySum(record.getPdcraiSerno());
                if (CollectionUtils.nonEmpty(map)) {
                    // 贷款总金额
                    BigDecimal loanAmt=(BigDecimal) map.get("loanAmt");
                    // 贷款总余额
                    BigDecimal loanBalance=(BigDecimal) map.get("loanBalance");
                    // 拖欠利息总额
                    BigDecimal totalTqlxAmt=(BigDecimal) map.get("totalTqlxAmt");
                    //减免本金合计
                    BigDecimal reducCapAmt = (BigDecimal) map.get("reducCapAmt");
                    //减免欠息合计
                    BigDecimal reducDebitInt = (BigDecimal) map.get("reducDebitInt");
                    //减免罚息合计
                    BigDecimal reducPenalInt = (BigDecimal) map.get("reducPenalInt");
                    //减免复息合计
                    BigDecimal reducCompoundInt = (BigDecimal) map.get("reducCompoundInt");
                    //减免费用合计
                    BigDecimal reducCostAmt = (BigDecimal) map.get("reducCostAmt");
                    //减免费用合计
                    BigDecimal reducTotlAmt = (BigDecimal) map.get("reducTotlAmt");
                    //获取当前业务流水号下的数据
                    PlaDebtClaimReducAppInfo plaDebtClaimReducAppInfo = plaDebtClaimReducAppInfoService.selectByPrimaryKey(record.getPdcraiSerno());
                    if (plaDebtClaimReducAppInfo != null) {
                        plaDebtClaimReducAppInfo.setLoanAmt(loanAmt);
                        plaDebtClaimReducAppInfo.setLoanBalance(loanBalance);
                        plaDebtClaimReducAppInfo.setTotalTqlxAmt(totalTqlxAmt);
                        plaDebtClaimReducAppInfo.setReducCapAmt(reducCapAmt);
                        plaDebtClaimReducAppInfo.setReducDebitInt(reducDebitInt);
                        plaDebtClaimReducAppInfo.setReducPenalInt(reducPenalInt);
                        plaDebtClaimReducAppInfo.setReducCompoundInt(reducCompoundInt);
                        plaDebtClaimReducAppInfo.setReducCostAmt(reducCostAmt);
                        plaDebtClaimReducAppInfo.setReducTotlAmt(reducTotlAmt);
                        count = plaDebtClaimReducAppInfoService.update(plaDebtClaimReducAppInfo);
                    }
                }

            }

        }


        return count;
    }

    /**
     * @方法名称: queryByApproveStatus
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaDebtClaimReducBillRel> queryByApproveStatus(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaDebtClaimReducBillRel> list = plaDebtClaimReducBillRelMapper.queryByApproveStatus(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryByPdcraiSerno
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaDebtClaimReducBillRel> queryByPdcraiSerno(String pdcraiSerno) {
        List<PlaDebtClaimReducBillRel> plaDebtClaimReducBillRels = plaDebtClaimReducBillRelMapper.queryByPdcraiSerno(pdcraiSerno);
        return plaDebtClaimReducBillRels;
    }
    /**
     * 债权减免发送核心记账
     *
     * @author liuquan
     **/
    public ResultDto<PlaDebtClaimReducBillRelDto> queryHXBill(String billNo) {
        PlaDebtClaimReducBillRelDto plaDebtClaimReducBillRelDto = new PlaDebtClaimReducBillRelDto();
        ResultDto<PlaDebtClaimReducBillRelDto> resultDto = new ResultDto<PlaDebtClaimReducBillRelDto>();
        Supplier<ResultDto<Ln3100RespDto>> supplierResp = ResultDto<Ln3100RespDto>::new;
        ResultDto<Ln3100RespDto> ln3100RespDtos = supplierResp.get();
        if (billNo != null) {
            Supplier<Ln3100ReqDto> supplier = Ln3100ReqDto::new;
            Ln3100ReqDto ln3100ReqDto = supplier.get();
            //查询笔数
            ln3100ReqDto.setChxunbis(1);
            //贷款借据号
            ln3100ReqDto.setDkjiejuh(billNo);
            //贷款账号
            ln3100ReqDto.setDkzhangh("");
            //起始笔数
            ln3100ReqDto.setQishibis(0);
            // 调用核心接口，进行债权减免发送核心记账
            ResultDto<Ln3100RespDto> ln3100RespDto = dscms2CoreLnClientService.ln3100(ln3100ReqDto);
            log.info("ln3100接口返回：" + ln3100RespDto);
            String ln3100Code = Optional.ofNullable(ln3100RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String ln3100Meesage = Optional.ofNullable(ln3100RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            if (Objects.equals(ln3100Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                if(ln3100RespDto.getData() != null){
                    plaDebtClaimReducBillRelDto.setRecRemInt(ln3100RespDto.getData().getYsyjlixi());
                    plaDebtClaimReducBillRelDto.setBcmRemInt(ln3100RespDto.getData().getCsyjlixi());
                    plaDebtClaimReducBillRelDto.setRecDebitInt(ln3100RespDto.getData().getYsqianxi());
                    plaDebtClaimReducBillRelDto.setBcmDebitInt(ln3100RespDto.getData().getCsqianxi());
                    plaDebtClaimReducBillRelDto.setRecRemPenalInt(ln3100RespDto.getData().getYsyjfaxi());
                    plaDebtClaimReducBillRelDto.setBcmRemPenalInt(ln3100RespDto.getData().getCsyjfaxi());
                    plaDebtClaimReducBillRelDto.setRecPenalInt(ln3100RespDto.getData().getYshofaxi());
                    plaDebtClaimReducBillRelDto.setBcmPenalInt(ln3100RespDto.getData().getCshofaxi());
                    plaDebtClaimReducBillRelDto.setRecCompoundInt(ln3100RespDto.getData().getYingjifx());
                    plaDebtClaimReducBillRelDto.setCompoundInt(ln3100RespDto.getData().getFuxiiiii());
                    //传入借据号
                    plaDebtClaimReducBillRelDto.setBillNo(billNo);
                    resultDto.setData(plaDebtClaimReducBillRelDto);
                    resultDto.setMessage("查询成功");
                }else {
                    //resultDto.setCode(ln3100Code);
                    resultDto.setMessage(ln3100Meesage);
                }
            } else {
                resultDto.setMessage(ln3100Meesage);
            }
        }
        return resultDto;
    }

    /**
     * @方法名称: insertByPlaDebtClaimReducBillRel
     * @方法描述: 减免借据信息保存
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertByPlaDebtClaimReducBillRel(PlaDebtClaimReducBillRel record) {
        int count = 0;
        if (record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();
            }
            PlaDebtClaimReducBillRel plaDebtClaimReducBillRel = plaDebtClaimReducBillRelMapper.selectByPdcraiSerno(record.getPdcraiSerno(), record.getBillNo());
            if (plaDebtClaimReducBillRel != null) {
                //预计减免欠息
                plaDebtClaimReducBillRel.setReducDebitInt(record.getRecRemInt().add(record.getBcmRemInt().add(record.getRecDebitInt()).add(record.getBcmDebitInt())));
                //预计减免罚息
                plaDebtClaimReducBillRel.setReducPenalInt(record.getRecRemPenalInt().add(record.getBcmRemPenalInt().add(record.getRecPenalInt().add(record.getBcmPenalInt()))));
                //预计减免复息
                plaDebtClaimReducBillRel.setReducCompoundInt(record.getRecCompoundInt().add(record.getCompoundInt()));
                //预计减免费用
                plaDebtClaimReducBillRel.setReducCostAmt(record.getReducCostAmt());
                plaDebtClaimReducBillRel.setTotalTqlxAmt(record.getTotalTqlxAmt());
                plaDebtClaimReducBillRel.setRecRemInt(record.getRecRemInt());
                plaDebtClaimReducBillRel.setBcmRemInt(record.getBcmRemInt());
                plaDebtClaimReducBillRel.setRecDebitInt(record.getRecDebitInt());
                plaDebtClaimReducBillRel.setBcmDebitInt(record.getBcmDebitInt());
                plaDebtClaimReducBillRel.setRecRemPenalInt(record.getRecRemPenalInt());
                plaDebtClaimReducBillRel.setBcmRemPenalInt(record.getBcmRemPenalInt());
                plaDebtClaimReducBillRel.setRecPenalInt(record.getRecPenalInt());
                plaDebtClaimReducBillRel.setBcmPenalInt(record.getBcmPenalInt());
                plaDebtClaimReducBillRel.setRecCompoundInt(record.getRecCompoundInt());
                plaDebtClaimReducBillRel.setCompoundInt(record.getCompoundInt());
                plaDebtClaimReducBillRel.setInputId(inputId);
                plaDebtClaimReducBillRel.setInputBrId(inputBrId);
                plaDebtClaimReducBillRel.setInputDate(inputDate);
                plaDebtClaimReducBillRel.setCreateTime(createTime);
                plaDebtClaimReducBillRel.setUpdId(inputId);
                plaDebtClaimReducBillRel.setUpdBrId(inputBrId);
                plaDebtClaimReducBillRel.setUpdDate(inputDate);
                plaDebtClaimReducBillRel.setUpdateTime(createTime);
                plaDebtClaimReducBillRelMapper.updateByPrimaryKey(plaDebtClaimReducBillRel);
            } else {
                record.setPdcrbrSerno(StringUtils.getUUID());

                record.setInputId(inputId);
                record.setInputBrId(inputBrId);
                record.setInputDate(inputDate);
                record.setCreateTime(createTime);
                record.setUpdId(inputId);
                record.setUpdBrId(inputBrId);
                record.setUpdDate(inputDate);
                record.setUpdateTime(createTime);
                plaDebtClaimReducBillRelMapper.insertSelective(record);
            }
            Map map = plaDebtClaimReducBillRelMapper.selectBySum(record.getPdcraiSerno());
            if (CollectionUtils.nonEmpty(map)) {
                //减免本金合计
                BigDecimal reducCapAmt = (BigDecimal) map.get("reducCapAmt");
                //减免欠息合计
                BigDecimal reducDebitInt = (BigDecimal) map.get("reducDebitInt");
                //减免罚息合计
                BigDecimal reducPenalInt = (BigDecimal) map.get("reducPenalInt");
                //减免复息合计
                BigDecimal reducCompoundInt = (BigDecimal) map.get("reducCompoundInt");
                //减免费用合计
                BigDecimal reducCostAmt = (BigDecimal) map.get("reducCostAmt");
                //减免费用合计
                BigDecimal reducTotlAmt = (BigDecimal) map.get("reducTotlAmt");
                //拖欠利息总额
                BigDecimal totalTqlxAmt = (BigDecimal) map.get("totalTqlxAmt");
                //获取当前业务流水号下的数据
                PlaDebtClaimReducAppInfo plaDebtClaimReducAppInfo = plaDebtClaimReducAppInfoService.selectByPrimaryKey(record.getPdcraiSerno());
                if (plaDebtClaimReducAppInfo != null) {
                    plaDebtClaimReducAppInfo.setReducCapAmt(reducCapAmt);
                    plaDebtClaimReducAppInfo.setReducDebitInt(reducDebitInt);
                    plaDebtClaimReducAppInfo.setReducPenalInt(reducPenalInt);
                    plaDebtClaimReducAppInfo.setReducCompoundInt(reducCompoundInt);
                    plaDebtClaimReducAppInfo.setReducCostAmt(reducCostAmt);
                    plaDebtClaimReducAppInfo.setReducTotlAmt(reducTotlAmt);
                    plaDebtClaimReducAppInfo.setTotalTqlxAmt(totalTqlxAmt);
                    count = plaDebtClaimReducAppInfoService.update(plaDebtClaimReducAppInfo);
                }
            }

        }
        return count;
    }

}
