/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.PlaExpenseRegiInfoDto;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.repository.mapper.PlaExpenseDetailMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.PlaExpenseRegiInfoMapper;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaExpenseRegiInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 15:20:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaExpenseRegiInfoService {

    @Autowired
    private PlaExpenseRegiInfoMapper plaExpenseRegiInfoMapper;

    @Autowired
    private PlaExpenseDetailService plaExpenseDetailService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaExpenseRegiInfo selectByPrimaryKey(String periSerno) {
        return plaExpenseRegiInfoMapper.selectByPrimaryKey(periSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaExpenseRegiInfo> selectAll(QueryModel model) {
        List<PlaExpenseRegiInfo> records = (List<PlaExpenseRegiInfo>) plaExpenseRegiInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaExpenseRegiInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaExpenseRegiInfo> list = plaExpenseRegiInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaExpenseRegiInfo record) {
        return plaExpenseRegiInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaExpenseRegiInfo record) {
        return plaExpenseRegiInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaExpenseRegiInfoDto record) {
        int count = 0;
        if (record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            //费用明细表
            List<PlaExpenseDetail> plaExpenseRegiInfoDtoList = record.getPlaExpenseDetail();
            for (PlaExpenseDetail plaExpenseDetails : plaExpenseRegiInfoDtoList) {
                PlaExpenseDetail  plaExpenseDetail = plaExpenseDetailService.selectByPrimaryKey(plaExpenseDetails.getPedSerno());
                if(plaExpenseDetail != null){
                    plaExpenseDetails.setUpdDate(openDay);
                    plaExpenseDetails.setUpdBrId(inputBrId);
                    plaExpenseDetails.setUpdId(inputId);
                    plaExpenseDetails.setUpdateTime(createTime);
                    count = plaExpenseDetailService.updateSelective(plaExpenseDetails);
                }else{
                    plaExpenseDetails.setInputId(inputId);
                    plaExpenseDetails.setInputBrId(inputBrId);
                    plaExpenseDetails.setInputDate(openDay);
                    plaExpenseDetails.setCreateTime(createTime);
                    plaExpenseDetails.setUpdDate(openDay);
                    plaExpenseDetails.setUpdBrId(inputBrId);
                    plaExpenseDetails.setUpdId(inputId);
                    plaExpenseDetails.setUpdateTime(createTime);
                    plaExpenseDetails.setPedSerno(StringUtils.getUUID());
                    plaExpenseDetails.setPeriSerno(record.getPeriSerno());
                    count = plaExpenseDetailService.insert(plaExpenseDetails);
                }
            }
            List<PlaExpenseDetail> plaExpenseDetailList = plaExpenseDetailService.selectByPeriSerno(record.getPeriSerno());
            // 费用总额
            BigDecimal expenseTotalAmt = new BigDecimal("0.00");
            // 列支总额
            BigDecimal rankAmt = new BigDecimal("0.00");
            // 垫支总额
            BigDecimal advanceExpenAmt = new BigDecimal("0.00");
            //垫支总额
            advanceExpenAmt=plaExpenseDetailList.stream().filter(plaExpenseDetail->{return "01".equals(plaExpenseDetail.getExpenseType());
            }).reduce(BigDecimal.ZERO,(sum,a)-> sum.add(a.getExpenseAmt()),BigDecimal::add);
            //列支总额
            rankAmt=plaExpenseDetailList.stream().filter(plaExpenseDetail->{return "02".equals(plaExpenseDetail.getExpenseType());
            }).reduce(BigDecimal.ZERO,(sum,a)-> sum.add(a.getExpenseAmt()),BigDecimal::add);
            expenseTotalAmt=rankAmt.add(advanceExpenAmt);
            PlaExpenseRegiInfo plaExpenseRegiInfo = new PlaExpenseRegiInfo();
            BeanUtils.copyProperties(record, plaExpenseRegiInfo);
            plaExpenseRegiInfo.setCusId(record.getCusId());
            plaExpenseRegiInfo.setCusName(record.getCusName());
            plaExpenseRegiInfo.setRankAmt(rankAmt);
            plaExpenseRegiInfo.setAdvanceExpenAmt(advanceExpenAmt);
            plaExpenseRegiInfo.setExpenseTotalAmt(expenseTotalAmt);
            plaExpenseRegiInfo.setUpdId(inputId);
            plaExpenseRegiInfo.setUpdBrId(inputBrId);
            plaExpenseRegiInfo.setUpdDate(openDay);
            plaExpenseRegiInfo.setUpdateTime(createTime);
            count = plaExpenseRegiInfoMapper.updateByPrimaryKey(plaExpenseRegiInfo);
        }
        return count;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaExpenseRegiInfo record) {
        return plaExpenseRegiInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String periSerno) {
        return plaExpenseRegiInfoMapper.deleteByPrimaryKey(periSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaExpenseRegiInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: savePlaExpenseRegiInfo
     * @方法描述: 保存
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：蔡宁波
     */

    public int savePlaExpenseRegiInfo(PlaExpenseRegiInfoDto record) {
        String serno = "";
        int count = 0;
        if (record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if(userInfo != null){
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            PlaExpenseRegiInfo plaExpenseRegiInfo = new PlaExpenseRegiInfo();
                //费用登记流水号
                serno =StringUtils.uuid(true);
                // 登记人
                plaExpenseRegiInfo.setInputId(inputId);
                // 登记机构
                plaExpenseRegiInfo.setInputBrId(inputBrId);
                // 登记日期
                plaExpenseRegiInfo.setInputDate(openDay);
                // 登记时间
                plaExpenseRegiInfo.setCreateTime(createTime);
                // 修改人
                plaExpenseRegiInfo.setUpdId(inputId);
                // 修改机构
                plaExpenseRegiInfo.setUpdBrId(inputBrId);
                // 修改日期
                plaExpenseRegiInfo.setUpdDate(openDay);
                // 修改时间
                plaExpenseRegiInfo.setUpdateTime(createTime);
                count = insertSelective(plaExpenseRegiInfo);
            if (count > 0) {
                //费用明细表
                List<PlaExpenseDetail> plaExpenseRegiInfoDtoList = record.getPlaExpenseDetail();
                for (PlaExpenseDetail plaExpenseDetails : plaExpenseRegiInfoDtoList) {
                    PlaExpenseDetail plaExpenseDetail = plaExpenseDetailService.selectByPrimaryKey(plaExpenseDetails.getPedSerno());
                        plaExpenseDetails.setPedSerno(StringUtils.uuid(true));
                        plaExpenseDetails.setPeriSerno(serno);
                        plaExpenseDetails.setInputDate(openDay);
                        plaExpenseDetails.setInputBrId(inputBrId);
                        plaExpenseDetails.setInputId(inputId);
                        plaExpenseDetails.setCreateTime(createTime);
                        plaExpenseDetails.setUpdDate(openDay);
                        plaExpenseDetails.setUpdBrId(inputBrId);
                        plaExpenseDetails.setUpdId(inputId);
                        plaExpenseDetails.setUpdateTime(createTime);
                        count = plaExpenseDetailService.insert(plaExpenseDetails);
                }
            } else {
                throw new YuspException(EcnEnum.ECN069999.key, EcnEnum.ECN060001.value);
            }
        }
            return count;
    }

    /**
     * 根据条件查询
     * @param queryModel
     * @return
     */
    public List<PlaExpenseRegiInfo> selectByCondition(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PlaExpenseRegiInfo> list = plaExpenseRegiInfoMapper.selectByModel(queryModel);
        PageHelper.clearPage();
        return list;
    }
    /**
     * @方法名称: insertReg
     * @方法描述: 保存
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：蔡宁波
     */
    public int insertReg(PlaExpenseRegiInfo plaExpenseRegiInfo) {
        String periSerno = "";
        int count = 0;
        if (plaExpenseRegiInfo != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            //费用登记流水号
            periSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.NPAM_PERI_SERNO, new HashMap<>());
            // 登记人
            plaExpenseRegiInfo.setInputId(inputId);
            // 登记机构
            plaExpenseRegiInfo.setInputBrId(inputBrId);
            // 登记日期
            plaExpenseRegiInfo.setInputDate(openDay);
            // 登记时间
            plaExpenseRegiInfo.setCreateTime(createTime);
            // 修改人
            plaExpenseRegiInfo.setUpdId(inputId);
            // 修改机构
            plaExpenseRegiInfo.setUpdBrId(inputBrId);
            // 修改日期
            plaExpenseRegiInfo.setUpdDate(openDay);
            // 修改时间
            plaExpenseRegiInfo.setUpdateTime(createTime);
            count = insertSelective(plaExpenseRegiInfo);
        } else {
                throw new YuspException(EcnEnum.ECN069999.key, EcnEnum.ECN060001.value);

        }
        return count;
    }
}