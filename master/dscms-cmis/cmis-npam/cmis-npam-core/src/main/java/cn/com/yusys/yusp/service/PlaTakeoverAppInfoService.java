/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisNpamConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.PlaTakeoverAppInfoDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3061.Ln3061ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3061.Ln3061RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3063.Ln3063ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3063.Ln3063RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3063.Lstzrjj;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.PlaAssetPldAppInfoMapper;
import cn.com.yusys.yusp.repository.mapper.PlaBartPldDebtAppInfoMapper;
import cn.com.yusys.yusp.repository.mapper.PlaLawCaseInfoMapper;
import cn.com.yusys.yusp.repository.mapper.PlaTakeoverAppInfoMapper;
import cn.com.yusys.yusp.dto.client.esb.core.ln3061.LstDkzrjj;
import cn.com.yusys.yusp.service.server.ib1241.Ib1241Server;
import cn.com.yusys.yusp.service.server.ln3100.Ln3100Server;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaTakeoverAppInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 19:17:28
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaTakeoverAppInfoService {
    private static final Logger log = LoggerFactory.getLogger(PlaTakeoverAppInfoService.class);
    @Autowired
    private PlaTakeoverAppInfoMapper plaTakeoverAppInfoMapper;

    @Autowired
    private PlaBartPldDebtAppInfoMapper plaBartPldDebtAppInfoMapper;

    @Autowired
    private PlaLawCaseInfoMapper plaLawCaseInfoMapper;

    @Autowired
    private PlaAssetPldAppInfoMapper plaAssetPldAppInfoMapper;

    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    @Autowired
    private PlaTakeoverBillRelService plaTakeoverBillRelService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private Ln3100Server ln3100Server;

    @Autowired
    private Ib1241Server ib1241Server;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaTakeoverAppInfo selectByPrimaryKey(String ptaiSerno) {
        return plaTakeoverAppInfoMapper.selectByPrimaryKey(ptaiSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaTakeoverAppInfo> selectAll(QueryModel model) {
        List<PlaTakeoverAppInfo> records = (List<PlaTakeoverAppInfo>) plaTakeoverAppInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaTakeoverAppInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaTakeoverAppInfo> list = plaTakeoverAppInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaTakeoverAppInfo record) {
        return plaTakeoverAppInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaTakeoverAppInfo record) {
        return plaTakeoverAppInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明
     * @算法描述: 无
     */

    public int update(PlaTakeoverAppInfo record) {
        int count = 0;
        if (record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay =stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            record.setUpdId(inputId);
            record.setUpdBrId(inputBrId);
            record.setUpdDate(openDay);
            record.setUpdateTime(createTime);
            record.setRegiStatus("01");
            count = plaTakeoverAppInfoMapper.updateByPrimaryKeySelective(record);
        }
        return count;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaTakeoverAppInfo record) {
        return plaTakeoverAppInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String ptaiSerno) {
        return plaTakeoverAppInfoMapper.deleteByPrimaryKey(ptaiSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaTakeoverAppInfoMapper.deleteByIds(ids);
    }

    /**
     * 分页查询债权转让申请,案件登记，以物抵债等列表
     *
     * @author cainingbo_yx
     * @date 2021/6/9 20:21
     **/
    public List<PlaTakeoverAppInfoDto> selectTakeoverlist(QueryModel queryModel) {
        List<PlaTakeoverAppInfoDto> plaTakeoverAppInfoList = new ArrayList<PlaTakeoverAppInfoDto>();
        String dispStage = queryModel.getCondition().get("dispStage").toString();
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        if (!"".equals(dispStage) && dispStage != null) {

            if ("01".equals(dispStage)) {//案件诉讼
                plaTakeoverAppInfoList = plaLawCaseInfoMapper.selectByCusId(queryModel);
            } else if ("02".equals(dispStage)) {//以物抵债
                plaTakeoverAppInfoList = plaBartPldDebtAppInfoMapper.selectByCusId(queryModel);
            } else if ("03".equals(dispStage)) {//抵债资产处置
                plaTakeoverAppInfoList = plaAssetPldAppInfoMapper.selectByCusId(queryModel);
            } else if ("04".equals(dispStage) || "05".equals(dispStage)) {
                plaTakeoverAppInfoList = plaTakeoverAppInfoMapper.selectByCusId(queryModel);
            }
        }
        PageHelper.clearPage();
        return plaTakeoverAppInfoList;

    }

    /**
     * @方法名称: transferReg
     * @方法描述: 资产转让协议登记
     * @参数与返回说明: 交易码ln3061
     * @算法描述: 无
     */

    public ResultDto<String> transferReg(PlaTakeoverAppInfo plaTakeoverAppInfo) {
        ResultDto<String> resultDto = new ResultDto<>();
        // 交易对手类型
        String jydsleix = "";
        // 付款周期
        String zrfkzoqi = "3MA15";
        if (plaTakeoverAppInfo != null) {
            // 转让付款周期计算
            String payCycle = plaTakeoverAppInfo.getPayCycle();
            if ("01".equals(payCycle)) {
                // 01 每季度月15号
                zrfkzoqi = "3MA15";
            } else if ("02".equals(payCycle)) {
                //  02 每季度月20
                zrfkzoqi = "3MA20";
            }
            List<LstDkzrjj> lstDkzrjjList =new ArrayList<>();
            lstDkzrjjList =plaTakeoverBillRelService.selectByPtaiSernoListdj(plaTakeoverAppInfo.getPtaiSerno());
            String openDays = stringRedisTemplate.opsForValue().get("openDay");
            String openDay = DateUtils.formatDate10To8(openDays);
            Ln3061ReqDto ln3061ReqDto = new Ln3061ReqDto();
            ln3061ReqDto.setCaozfshi(CmisNpamConstants.HX_CAOZFSHI_A);        //操作方式
            ln3061ReqDto.setXieybhao(plaTakeoverAppInfo.getTakeoverAgrNo());        //协议编号
            ln3061ReqDto.setXieyimch("债权转让");        //协议名称
            ln3061ReqDto.setChanpdma("710002");        //产品代码
            ln3061ReqDto.setChanpmch("资产转让");        //产品名称
            ln3061ReqDto.setHuobdhao("01");        //货币代号
            ln3061ReqDto.setXyzuigxe(plaTakeoverAppInfo.getTakeoverTotlAmt());        //协议最高限额
            ln3061ReqDto.setZcrfshii("01".equals(plaTakeoverAppInfo.getFinancingMode()) ? "2" : "1");        //资产融通方式
            ln3061ReqDto.setZcrtbili(plaTakeoverAppInfo.getFinancingPerc());        //资产融通比例
            ln3061ReqDto.setZjlyzhao(plaTakeoverAppInfo.getTransferAcct());
            ln3061ReqDto.setZjlyzzxh(plaTakeoverAppInfo.getTransferAcctChildNo());        //资金来源账号子序号
            ln3061ReqDto.setZrfukzhh(plaTakeoverAppInfo.getOutsideAcctNo());        //对外付款账号
            ln3061ReqDto.setZrfukzxh(plaTakeoverAppInfo.getOutsideAcctChildNo());        //对外付款账号子序号
            ln3061ReqDto.setQiandriq(openDay);        //签订日期
            ln3061ReqDto.setFengbriq(openDay);        //封包日期
            if (plaTakeoverAppInfo.getToppType() != null) {
                jydsleix = plaTakeoverAppInfo.getToppType().substring(1, 2);
            }
            ln3061ReqDto.setJydsleix(jydsleix);        //交易对手类型
            ln3061ReqDto.setJydszhao(plaTakeoverAppInfo.getToppAcctNo());        //交易对手账号
            ln3061ReqDto.setJydszhzh(plaTakeoverAppInfo.getToppAcctChildNo());        //交易对手账号子序号
            ln3061ReqDto.setJydszhmc(plaTakeoverAppInfo.getToppName());        //交易对手账户名称
            ln3061ReqDto.setZhkaihhh(plaTakeoverAppInfo.getTransferAcctAcctb());        //账户开户行行号
            ln3061ReqDto.setZhkaihhm(plaTakeoverAppInfo.getTransferAcctAcctbName());        //账户开户行行名
            ln3061ReqDto.setZrfkzoqi(zrfkzoqi);//付款周期
            ln3061ReqDto.setLstDkzrjj(lstDkzrjjList);//借据集合
            log.info("转让协议登记请求：" + ln3061ReqDto);
            ResultDto<Ln3061RespDto> ln3061RespDto = dscms2CoreLnClientService.ln3061(ln3061ReqDto);
            log.info("转让协议登记返回：" + ln3061RespDto);
            String ln3061Code = Optional.ofNullable(ln3061RespDto.getCode()).orElse(StringUtils.EMPTY);
            String ln3061Meesage = Optional.ofNullable(ln3061RespDto.getMessage()).orElse(StringUtils.EMPTY);
            log.info("转让协议登记返回信息：" + ln3061Meesage);
            if (Objects.equals(ln3061Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                plaTakeoverAppInfo.setRegiStatus(CmisNpamConstants.HX_REGI_STATUS_02);
                plaTakeoverAppInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_01);
                //登记交易流水号
                plaTakeoverAppInfo.setRegiTranSerno(ln3061RespDto.getData().getJiaoyils());
                //登记交易时间
                plaTakeoverAppInfo.setRegiTranTime(ln3061RespDto.getData().getJiaoyirq());
                updateSelective(plaTakeoverAppInfo);
                resultDto.setMessage(EcnEnum.ECN060000.value);
            } else {
                plaTakeoverAppInfo.setRegiStatus(CmisNpamConstants.HX_REGI_STATUS_04);
                updateSelective(plaTakeoverAppInfo);
                resultDto.setCode(ln3061Code);
                resultDto.setMessage(ln3061Meesage);
            }
        } else {
            throw BizException.error(null, EcnEnum.ECN060005.key, EcnEnum.ECN060005.value);
        }
        return resultDto;
    }


    /**
     * 资产转让记账发生记账核心
     *
     * @author 刘权
     **/
    public ResultDto<String> sendToHXJZ(PlaTakeoverAppInfo appInfo) {
        String accStatus="";
        ResultDto<String> resultDto = new ResultDto<>();
        if (appInfo != null) {
            // 是否委托清收
            String weituoqs = appInfo.getTakeoverCount();
            Ln3063ReqDto ln3063ReqDto = new Ln3063ReqDto();
            String openDays = stringRedisTemplate.opsForValue().get("openDay");
            String openDay = DateUtils.formatDate10To8(openDays);
            List<Lstzrjj> lstzrjjList = new ArrayList<>();
            //TODO 借据
            lstzrjjList = plaTakeoverBillRelService.selectByPtaiSernoListjz(appInfo.getPtaiSerno());
            //开户操作标志
            ln3063ReqDto.setDkkhczbz("4");
            //协议编号
            ln3063ReqDto.setXieybhao(appInfo.getTakeoverAgrNo());
            //协议名称
            ln3063ReqDto.setXieyimch("债权转让");
            //产品代码
            ln3063ReqDto.setChanpdma("710002");
            //产品名称
            ln3063ReqDto.setChanpmch("资产转让");
            //货币代号
            ln3063ReqDto.setHuobdhao("01");
            //协议实际金额
            ln3063ReqDto.setXieyshje(appInfo.getTakeoverTotlAmt());
            //签订日期
            ln3063ReqDto.setQiandriq(openDay);//appInfo.getTranBaseDate()
            //资金来源账号
            ln3063ReqDto.setZjlyzhao("");
            //资金来源账号子序号
            ln3063ReqDto.setZjlyzzxh("");
            //对外付款账号
            ln3063ReqDto.setZrfukzhh("");
            //对外付款账号子序号
            ln3063ReqDto.setZrfukzxh("");
            //转让计价方式信贷 01	按协议包 02	按单笔借据  核心1--按单笔借据 2--按协议包
            ln3063ReqDto.setZrjjfshi(appInfo.getTakeoverPriceMode() == "01" ? "2" : "1");
            //委托清收
            ln3063ReqDto.setWeituoqs(weituoqs);
            //借据列表
            ln3063ReqDto.setLstzrjj(lstzrjjList);
            // 调用核心接口，进行债权减免发送核心记账
            log.info("转让记账请求：" + ln3063ReqDto);
            ResultDto<Ln3063RespDto> ln3063RespDto = dscms2CoreLnClientService.ln3063(ln3063ReqDto);
            log.info("转让记账返回：" + ln3063RespDto);
            String ln3063Code = Optional.ofNullable(ln3063RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String ln3063Meesage = Optional.ofNullable(ln3063RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            log.info("转让记账返回信息：" + ln3063Meesage);
            if (Objects.equals(ln3063Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                if(ln3063RespDto.getData() != null){
                    appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_03);
                    //记账日期
                    appInfo.setRecordDate(openDays);
                    //记账交易流水号
                    appInfo.setRecordTranSerno(ln3063RespDto.getData().getJiaoyils());
                    appInfo.setRecordTranTime(ln3063RespDto.getData().getJiaoyirq());
                    updateSelective(appInfo);
                    // 更新借据子表状态
                    updatePlaBartPldDebtBillRel(appInfo);
                    // 转让成功更新台账状态
                    for (Lstzrjj listjj : lstzrjjList) {
                        Map<String, String> map = new HashMap();
                        map.put("billNo", listjj.getJiejuhao());
                        //如果是非委托清收贷款则修改台账状态为关闭并且借据结清(因同一笔申请不可能存在非委托和委托so 直接用主表判断)
                        if("0".equals(appInfo.getTakeoverCount())){
                            accStatus="0";
                            map.put("accStatus", accStatus);// 关闭
                            map.put("loanBalance",BigDecimal.ZERO.toPlainString());
                            map.put("debitInt", BigDecimal.ZERO.toPlainString());
                            map.put("penalInt", BigDecimal.ZERO.toPlainString());
                            map.put("compoundInt", BigDecimal.ZERO.toPlainString());
                        }
                        cmisBizClientService.updateAccLoanByBillNo(map);
                    }
                    resultDto.setMessage(EcnEnum.ECN060000.value);
                }else{
                    appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_04);
                    appInfo.setRecordDate(openDays);
                    updateSelective(appInfo);
                    resultDto.setCode(ln3063Code);
                    resultDto.setMessage(ln3063Meesage);
                }
            } else {
                appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_04);
                appInfo.setRecordDate(openDays);
                updateSelective(appInfo);
                // 更新借据子表状态
                updatePlaBartPldDebtBillRel(appInfo);
                resultDto.setCode(ln3063Code);
                resultDto.setMessage(ln3063Meesage);
            }
        } else {
            throw BizException.error(null, EcnEnum.ECN060005.key, EcnEnum.ECN060005.value);
        }
        return resultDto;
    }

    /**
     * 调用ib1253查询子序号
     *
     * @author 刘权
     **/
    public ResultDto<Ib1253RespDto> sendToIb1253(Map map) {


        Ib1253ReqDto ib1253ReqDto = new Ib1253ReqDto();
        //客户账号
        ib1253ReqDto.setKehuzhao((String) map.get("toppAcctNo"));
        //子账户序号
        ib1253ReqDto.setZhhaoxuh("");
        //密码校验方式不校验1 0----校验查询密码 2--校验交易密码
        ib1253ReqDto.setYanmbzhi("0");
        //密码
        ib1253ReqDto.setMimammmm("");
        //客户账号2
        ib1253ReqDto.setKehzhao2("");
        //是否标志 1--是 0--否
        ib1253ReqDto.setShifoubz("0");
        //账户分类代码1
        ib1253ReqDto.setZhufldm1("");
        //账户分类代码2
        ib1253ReqDto.setZhufldm2("");

        ResultDto<Ib1253RespDto> resultDto = dscms2CoreIbClientService.ib1253(ib1253ReqDto);
        String ib1253Code = Optional.ofNullable(resultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ib1253Meesage = Optional.ofNullable(resultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        if (Objects.equals(ib1253Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            return resultDto;
        } else {
            resultDto.setMessage(ib1253Meesage);
            return resultDto;
        }
    }


    /**
     * @创建人 liuquan
     * @创建时间
     * @注释 冲正处理
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto ib1241(PlaTakeoverAppInfo appInfo) {
        try {
            // 调用核心冲正交易
            // 冲正交易请求对象
            Ib1241ReqDto ib1241ReqDto = new Ib1241ReqDto();
            // 原交易日期
            ib1241ReqDto.setYjiaoyrq(appInfo.getRecordTranTime());
            // 原柜员流水
            ib1241ReqDto.setYgyliush(appInfo.getRecordTranSerno());
            ResultDto<Ib1241RespDto> ib1241RespDtoResultDto = ib1241Server.sendIb1241(ib1241ReqDto);
            String ib1241Code = Optional.ofNullable(ib1241RespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String ib1241Meesage = Optional.ofNullable(ib1241RespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            if (Objects.equals(ib1241Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                if(ib1241RespDtoResultDto !=null){
                    appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_01);
                    appInfo.setRecordBackSerno(ib1241RespDtoResultDto.getData().getChzhlshu());
                    appInfo.setRecordBackTime(ib1241RespDtoResultDto.getData().getChzhriqi());
                    updateSelective(appInfo);
                    // 同时更新关联借据记账状态
                    QueryModel model= new QueryModel();
                    model.addCondition("ptaiSerno",appInfo.getPtaiSerno());
                    List<PlaTakeoverBillRel> plaTakeoverBillRels= plaTakeoverBillRelService.selectByModel(model);
                    for(PlaTakeoverBillRel plaTakeoverBillRel:plaTakeoverBillRels){
                        plaTakeoverBillRel.setRecordStatus(CmisNpamConstants.HX_STATUS_01);
                        plaTakeoverBillRelService.updateSelective(plaTakeoverBillRel);
                        // 查询台账信息
                        BigDecimal loanBalance = BigDecimal.ZERO;
                        BigDecimal debitInt = BigDecimal.ZERO;
                        BigDecimal penalInt = BigDecimal.ZERO;
                        BigDecimal compoundInt = BigDecimal.ZERO;
                        ResultDto<Ln3100RespDto> ln3100RespDto =ln3100Server.queryHXBill(plaTakeoverBillRel.getBillNo());
                        log.info("ln3100接口返回：" + ln3100RespDto);
                        String ln3100Code = Optional.ofNullable(ln3100RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                        String ln3100Meesage = Optional.ofNullable(ln3100RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                        if (Objects.equals(ln3100Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                            if(ln3100RespDto.getData() != null){
                                loanBalance=ln3100RespDto.getData().getYuqibjin().add(ln3100RespDto.getData().getZhchbjin());
                                debitInt=ln3100RespDto.getData().getYsyjlixi().add(ln3100RespDto.getData().getCsyjlixi()).add(ln3100RespDto.getData().getYsqianxi());
                                penalInt=ln3100RespDto.getData().getCsqianxi().add(ln3100RespDto.getData().getYsyjfaxi()).add(ln3100RespDto.getData().getCsyjfaxi()).add(ln3100RespDto.getData().getYshofaxi().add(ln3100RespDto.getData().getCshofaxi()));
                                compoundInt=ln3100RespDto.getData().getYingjifx().add(ln3100RespDto.getData().getFuxiiiii());
                            }}
                        // 冲正成功更新台账状态
                        Map<String, String> map = new HashMap();
                        map.put("billNo",plaTakeoverBillRel.getBillNo());
                        map.put("accStatus","1");// 冲正成功修改状态
                        map.put("loanBalance",loanBalance.toPlainString());
                        map.put("debitInt", debitInt.toPlainString());
                        map.put("penalInt", penalInt.toPlainString());
                        map.put("compoundInt", compoundInt.toPlainString());
                        cmisBizClientService.updateAccLoanByBillNo(map);
                    }
                }else{
                    return new ResultDto(null).message("冲正处理失败");
                }
            }else{
                return new ResultDto(null).message("冲正处理失败"+ib1241Meesage);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResultDto(null).message(e.getMessage());
        }
        return new ResultDto(null).message("冲正处理成功");
    }

    /**
     * @创建人 liuquan
     * @创建时间
     * @注释 登记冲正
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto DJCZ(PlaTakeoverAppInfo appInfo) {
        try {
            // 调用核心冲正交易
            Ib1241ReqDto ib1241ReqDto = new Ib1241ReqDto();
            // 原交易日期
            ib1241ReqDto.setYjiaoyrq(appInfo.getRegiTranTime());
            // 原柜员流水
            ib1241ReqDto.setYgyliush(appInfo.getRegiTranSerno());
            ResultDto<Ib1241RespDto> ib1241RespDtoResultDto = ib1241Server.sendIb1241(ib1241ReqDto);
            String ib1241Code = Optional.ofNullable(ib1241RespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String ib1241Meesage = Optional.ofNullable(ib1241RespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            if (Objects.equals(ib1241Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                if(ib1241RespDtoResultDto !=null){
                    appInfo.setRegiStatus(CmisNpamConstants.HX_REGI_STATUS_01);
                    appInfo.setRegiBackSerno(ib1241RespDtoResultDto.getData().getChzhlshu());
                    appInfo.setRegiBackTime(ib1241RespDtoResultDto.getData().getChzhriqi());
                    updateSelective(appInfo);
                    // 同时更新关联借据记账状态
                    QueryModel model= new QueryModel();
                    model.addCondition("ptaiSerno",appInfo.getPtaiSerno());
                    List<PlaTakeoverBillRel> plaTakeoverBillRels= plaTakeoverBillRelService.selectByModel(model);
                    for(PlaTakeoverBillRel plaTakeoverBillRel:plaTakeoverBillRels){
                        plaTakeoverBillRel.setRecordStatus(CmisNpamConstants.HX_STATUS_01);
                        //修改登记状态
                        plaTakeoverBillRel.setRegiStatus(CmisNpamConstants.HX_REGI_STATUS_04);
                        plaTakeoverBillRelService.updateSelective(plaTakeoverBillRel);
                        // 冲正成功更新台账状态
                        Map<String, String> map = new HashMap();
                        map.put("billNo",plaTakeoverBillRel.getBillNo());
                        map.put("accStatus","1");// 冲正成功修改状态
                        cmisBizClientService.updateAccLoanByBillNo(map);
                    }
                }else{
                    return new ResultDto(null).message("冲正处理失败");
                }
            }else{
                return new ResultDto(null).message("冲正处理失败"+ib1241Meesage);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResultDto(null).message(e.getMessage());
        }
        return new ResultDto(null).message("冲正处理成功");
    }

    /**
     * 修改借据记账信息
     * @param appInfo
     * @return
     */
    public int updatePlaBartPldDebtBillRel(PlaTakeoverAppInfo appInfo){
        int count =0;
        if(appInfo !=null){
            String ptaiSerno= appInfo.getPtaiSerno();
            List<PlaTakeoverBillRel> list=plaTakeoverBillRelService.selectByPtaiSernoLists(ptaiSerno);
            if(CollectionUtils.nonEmpty(list)){
                for(PlaTakeoverBillRel plaTakeoverBillRel:list){
                    plaTakeoverBillRel.setRecordStatus(appInfo.getRecordStatus());
                    count=plaTakeoverBillRelService.updateSelective(plaTakeoverBillRel);
                }
            }
        }
        return count;
    }
}
