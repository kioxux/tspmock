/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaLawPerformCaseInfo;
import cn.com.yusys.yusp.service.PlaLawPerformCaseInfoService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawPerformCaseInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-07 19:08:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plalawperformcaseinfo")
public class PlaLawPerformCaseInfoResource {
    @Autowired
    private PlaLawPerformCaseInfoService plaLawPerformCaseInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaLawPerformCaseInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaLawPerformCaseInfo> list = plaLawPerformCaseInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaLawPerformCaseInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaLawPerformCaseInfo>> index(QueryModel queryModel) {
        List<PlaLawPerformCaseInfo> list = plaLawPerformCaseInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawPerformCaseInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{plpciSerno}")
    protected ResultDto<PlaLawPerformCaseInfo> show(@PathVariable("plpciSerno") String plpciSerno) {
        PlaLawPerformCaseInfo plaLawPerformCaseInfo = plaLawPerformCaseInfoService.selectByPrimaryKey(plpciSerno);
        return new ResultDto<PlaLawPerformCaseInfo>(plaLawPerformCaseInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaLawPerformCaseInfo> create(@RequestBody PlaLawPerformCaseInfo plaLawPerformCaseInfo) throws URISyntaxException {
        plaLawPerformCaseInfoService.insert(plaLawPerformCaseInfo);
        return new ResultDto<PlaLawPerformCaseInfo>(plaLawPerformCaseInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaLawPerformCaseInfo plaLawPerformCaseInfo) throws URISyntaxException {
        int result = plaLawPerformCaseInfoService.update(plaLawPerformCaseInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{plpciSerno}")
    protected ResultDto<Integer> delete(@PathVariable("plpciSerno") String plpciSerno) {
        int result = plaLawPerformCaseInfoService.deleteByPrimaryKey(plpciSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaLawPerformCaseInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryPlaLawPerformCaseInfo")
    protected ResultDto<List<PlaLawPerformCaseInfo>> queryPlaLawPerformCaseInfo(@RequestBody QueryModel queryModel) {
        List<PlaLawPerformCaseInfo> list = plaLawPerformCaseInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawPerformCaseInfo>>(list);
    }
}
