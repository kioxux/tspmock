/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaAssetPldAppInfo;
import cn.com.yusys.yusp.domain.PlaBartPldDebtAppInfo;
import cn.com.yusys.yusp.domain.PlaTakeoverAppInfo;
import cn.com.yusys.yusp.service.PlaAssetPldAppInfoService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldAppInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plaassetpldappinfo")
public class PlaAssetPldAppInfoResource {
    @Autowired
    private PlaAssetPldAppInfoService plaAssetPldAppInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaAssetPldAppInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaAssetPldAppInfo> list = plaAssetPldAppInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaAssetPldAppInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<PlaAssetPldAppInfo>> index(QueryModel queryModel) {
        List<PlaAssetPldAppInfo> list = plaAssetPldAppInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaAssetPldAppInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{papaiSerno}")
    protected ResultDto<PlaAssetPldAppInfo> show(@PathVariable("papaiSerno") String papaiSerno) {
        PlaAssetPldAppInfo plaAssetPldAppInfo = plaAssetPldAppInfoService.selectByPrimaryKey(papaiSerno);
        return new ResultDto<PlaAssetPldAppInfo>(plaAssetPldAppInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<PlaAssetPldAppInfo> create(@RequestBody PlaAssetPldAppInfo plaAssetPldAppInfo) throws URISyntaxException {
        plaAssetPldAppInfoService.insert(plaAssetPldAppInfo);
        return new ResultDto<PlaAssetPldAppInfo>(plaAssetPldAppInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaAssetPldAppInfo plaAssetPldAppInfo) throws URISyntaxException {
        int result = plaAssetPldAppInfoService.update(plaAssetPldAppInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{papaiSerno}")
    protected ResultDto<Integer> delete(@PathVariable("papaiSerno") String papaiSerno) {
        int result = plaAssetPldAppInfoService.deleteByPrimaryKey(papaiSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaAssetPldAppInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 新增抵债资产处置申请
     *
     * @author jijian_yx
     * @date 2021/6/10 16:37
     **/
    @ApiOperation("新增抵债资产处置申请")
    @PostMapping("/save")
    protected ResultDto<String> save(@RequestBody PlaAssetPldAppInfo appInfo) {
        ResultDto<Integer> result = new ResultDto<>();
        String papaiSerno = plaAssetPldAppInfoService.save(appInfo);
        return new ResultDto<String>(papaiSerno);
    }

    /**
     * 获取抵债资产处置申请列表
     *
     * @author jijian_yx
     * @date 2021/6/9 20:22
     **/
    @ApiOperation("分页查询抵债资产处置申请")
    @PostMapping("/tosignlist")
    protected ResultDto<List<PlaAssetPldAppInfo>> toSignlist(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PlaAssetPldAppInfo> list = plaAssetPldAppInfoService.toSignlist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<PlaAssetPldAppInfo>>(list);
    }

    /**
     * 获取抵债资产处置历史列表
     *
     * @author jijian_yx
     * @date 2021/6/9 20:13
     **/
    @ApiOperation("分页查询抵债资产处置历史")
    @PostMapping("/donesignlist")
    protected ResultDto<List<PlaAssetPldAppInfo>> doneSignlist(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PlaAssetPldAppInfo> list = plaAssetPldAppInfoService.doneSignlist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<PlaAssetPldAppInfo>>(list);
    }

    /**
     * 获取抵债资产处置台账列表
     *
     * @author jijian_yx
     * @date 2021/6/9 20:22
     **/
    @ApiOperation("分页查询抵债资产处置台账")
    @PostMapping("/getassetpldacclist")
    protected ResultDto<List<PlaAssetPldAppInfo>> getAssetPldAccList(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PlaAssetPldAppInfo> list = plaAssetPldAppInfoService.getAssetPldAccList(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<PlaAssetPldAppInfo>>(list);
    }

    /**
     * 导出抵债资产台账
     *
     * @author jijian_yx
     * @date 2021/6/10 9:59
     **/
    @ApiOperation("导出抵债资产台账")
    @PostMapping("/exportPlaAssetPldAppInfo")
    public ResultDto<ProgressDto> exportPlaAssetPldAppInfo(@RequestBody QueryModel queryModel) {
        ProgressDto progressDto = plaAssetPldAppInfoService.exportPlaAssetPldAppInfo(queryModel);
        return ResultDto.success(progressDto);
    }

    /**
     * 以物抵债发送核心记账
     *
     * @author liuquan
     **/
    @ApiOperation("以物抵债发送核心记账")
    @PostMapping("/sendtohxjz")
    protected ResultDto<String> sendToHXJZ(@RequestBody PlaAssetPldAppInfo appInfo) {
        return plaAssetPldAppInfoService.sendToHXJZ(appInfo);
    }
    /**
     * @创建人 zmw
     * @创建时间
     * @注释 冲正处理
     */
    @PostMapping("/czcl")
    protected ResultDto czcl( @RequestBody PlaAssetPldAppInfo appInfo) {
        return  plaAssetPldAppInfoService.ib1241(appInfo);
    }
}
