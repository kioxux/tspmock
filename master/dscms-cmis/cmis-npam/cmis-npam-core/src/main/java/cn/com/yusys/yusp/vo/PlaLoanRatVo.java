/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLoanRat
 * @类描述: pla_loan_rat数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-11 16:11:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@ExcelCsv(namePrefix = "利息试算导出文档", fileType = ExcelCsv.ExportFileType.XLS)
public class PlaLoanRatVo {

	/** 合同编号 **/
	@ExcelField(title = "合同编号", viewLength = 20)
	private String contNo;

	/** 借据编号 **/
	@ExcelField(title = "借据编号", viewLength = 20)
	private String billNo;

	/** 客户编号 **/
	@ExcelField(title = "客户编号", viewLength = 20)
	private String cusId;

	/** 客户名称 **/
	@ExcelField(title = "客户名称", viewLength = 20)
	private String cusName;

	/** 贷款金额 **/
	@ExcelField(title = "贷款金额", viewLength = 20 ,format = "#0.00")
	private java.math.BigDecimal loanAmt;

	/** 贷款余额 **/
	@ExcelField(title = "贷款余额", viewLength = 20 ,format = "#0.00")
	private java.math.BigDecimal loanBalance;

	/** 执行年利率 **/
	@ExcelField(title = "执行年利率", viewLength = 20 ,format = "#0.00")
	private java.math.BigDecimal execRateYear;


	/** 利息 **/
	@ExcelField(title = "利息", viewLength = 20 ,format = "#0.00")
	private java.math.BigDecimal intAmt;

	/** 罚息 **/
	@ExcelField(title = "罚息", viewLength = 20 ,format = "#0.00")
	private java.math.BigDecimal penalInt;

	/** 复利 **/
	@ExcelField(title = "复利", viewLength = 20 ,format = "#0.00")
	private java.math.BigDecimal compoundInt;


	public String getContNo() {
		return contNo;
	}

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public BigDecimal getLoanAmt() {
		return loanAmt;
	}

	public void setLoanAmt(BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}

	public BigDecimal getLoanBalance() {
		return loanBalance;
	}

	public void setLoanBalance(BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}

	public BigDecimal getExecRateYear() {
		return execRateYear;
	}

	public void setExecRateYear(BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}

	public BigDecimal getIntAmt() {
		return intAmt;
	}

	public void setIntAmt(BigDecimal intAmt) {
		this.intAmt = intAmt;
	}

	public BigDecimal getPenalInt() {
		return penalInt;
	}

	public void setPenalInt(BigDecimal penalInt) {
		this.penalInt = penalInt;
	}

	public BigDecimal getCompoundInt() {
		return compoundInt;
	}

	public void setCompoundInt(BigDecimal compoundInt) {
		this.compoundInt = compoundInt;
	}
}