/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.DefaultDataHandle;
import cn.com.yusys.yusp.commons.excelcsv.async.ImportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.vo.PlaPlanDetailVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FileUtils;
import org.openxmlformats.schemas.presentationml.x2006.main.CTCustomerDataList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaPlanDetail;
import cn.com.yusys.yusp.service.PlaPlanDetailService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaPlanDetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-28 14:42:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "清收计划详情")
@RestController
@RequestMapping("/api/plaplandetail")
public class PlaPlanDetailResource {
    private static final Logger log = LoggerFactory.getLogger(PlaPlanDetailResource.class);
    @Autowired
    private PlaPlanDetailService plaPlanDetailService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaPlanDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaPlanDetail> list = plaPlanDetailService.selectAll(queryModel);
        return new ResultDto<List<PlaPlanDetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaPlanDetail>> index(QueryModel queryModel) {
        List<PlaPlanDetail> list = plaPlanDetailService.selectByModel(queryModel);
        return new ResultDto<List<PlaPlanDetail>>(list);
    }


    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("清收计划详情查询")
    @PostMapping("/queryAll")
    protected ResultDto<List<PlaPlanDetail>> queryAll(@RequestBody QueryModel queryModel) {
        List<PlaPlanDetail> list = plaPlanDetailService.selectByModel(queryModel);
        return new ResultDto<List<PlaPlanDetail>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            根据清收计划编号查询 分页查询
     * @算法描述:
     * 刘权
     */
    @ApiOperation("根据清收计划编号查询")
    @PostMapping("/selectByrecoverySerno")
    protected ResultDto<List<PlaPlanDetail>> selectByrecoverySerno(@RequestBody QueryModel queryModel) {
        List<PlaPlanDetail> list = plaPlanDetailService.selectByrecoverySerno(queryModel);
        return new ResultDto<List<PlaPlanDetail>>(list);
    }



    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{ppdSerno}")
    protected ResultDto<PlaPlanDetail> show(@PathVariable("ppdSerno") String ppdSerno) {
        PlaPlanDetail plaPlanDetail = plaPlanDetailService.selectByPrimaryKey(ppdSerno);
        return new ResultDto<PlaPlanDetail>(plaPlanDetail);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaPlanDetail> create(@RequestBody PlaPlanDetail plaPlanDetail) throws URISyntaxException {
        plaPlanDetailService.insert(plaPlanDetail);
        return new ResultDto<PlaPlanDetail>(plaPlanDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaPlanDetail plaPlanDetail) throws URISyntaxException {
        int result = plaPlanDetailService.update(plaPlanDetail);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{ppdSerno}")
    protected ResultDto<Integer> delete(@PathVariable("ppdSerno") String ppdSerno) {
        int result = plaPlanDetailService.deleteByPrimaryKey(ppdSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaPlanDetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 清收计划详情模板
     */
    @PostMapping("/exportplanDetail")
    public ResultDto<ProgressDto> asyncExportPlanDetail() {
        ProgressDto progressDto = plaPlanDetailService.asyncExportPlanDetail();
        return ResultDto.success(progressDto);
    }

    /**
     * Excel数据导入
     * @param fileId Excel文件信息ID
     * @return
     */
    @PostMapping("/importplanDetail")
    public ResultDto<String> asyncImportplanDetail(@RequestParam("fileId") String fileId, @RequestBody Map<String, String> paramsMap) {
        String recoverySerno = "";
        if(paramsMap.containsKey("recoverySerno")){
            recoverySerno = paramsMap.get("recoverySerno");
        }
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = null;
        try {
            tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
            FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
        } catch (IOException e) {
            log.error(EcnEnum.ECN060004.value,e);
            throw BizException.error(null, EcnEnum.ECN060004.key,EcnEnum.ECN060004.value);
        }
        // 将文件内容导入数据库，StudentScore为导入数据的类
        String finalRecoverySerno = recoverySerno;
        ExcelUtils.syncImport(PlaPlanDetailVo.class, tempFile, new DefaultDataHandle(), ExcelUtils.batchConsumer(dataList -> {
            try {
                return plaPlanDetailService.insertPlanDetail(dataList, finalRecoverySerno);
            } catch (Exception e) {
                throw BizException.error(null,EcnEnum.ECN069999.key,e.getMessage());
            }
        }), true);
        return ResultDto.success().message("导入成功！");
    }
}
