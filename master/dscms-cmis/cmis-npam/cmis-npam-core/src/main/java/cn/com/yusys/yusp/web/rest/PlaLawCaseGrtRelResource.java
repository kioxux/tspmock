/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaLawCaseGrtRel;
import cn.com.yusys.yusp.service.PlaLawCaseGrtRelService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawCaseGrtRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-05 17:45:58
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plalawcasegrtrel")
public class PlaLawCaseGrtRelResource {
    @Autowired
    private PlaLawCaseGrtRelService plaLawCaseGrtRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaLawCaseGrtRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaLawCaseGrtRel> list = plaLawCaseGrtRelService.selectAll(queryModel);
        return new ResultDto<List<PlaLawCaseGrtRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaLawCaseGrtRel>> index(QueryModel queryModel) {
        List<PlaLawCaseGrtRel> list = plaLawCaseGrtRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawCaseGrtRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{plcgrSerno}")
    protected ResultDto<PlaLawCaseGrtRel> show(@PathVariable("plcgrSerno") String plcgrSerno) {
        PlaLawCaseGrtRel plaLawCaseGrtRel = plaLawCaseGrtRelService.selectByPrimaryKey(plcgrSerno);
        return new ResultDto<PlaLawCaseGrtRel>(plaLawCaseGrtRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaLawCaseGrtRel> create(@RequestBody PlaLawCaseGrtRel plaLawCaseGrtRel) throws URISyntaxException {
        plaLawCaseGrtRelService.insert(plaLawCaseGrtRel);
        return new ResultDto<PlaLawCaseGrtRel>(plaLawCaseGrtRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaLawCaseGrtRel plaLawCaseGrtRel) throws URISyntaxException {
        int result = plaLawCaseGrtRelService.update(plaLawCaseGrtRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{plcgrSerno}")
    protected ResultDto<Integer> delete(@PathVariable("plcgrSerno") String plcgrSerno) {
        int result = plaLawCaseGrtRelService.deleteByPrimaryKey(plcgrSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaLawCaseGrtRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryByPlaLawCaseGrtRel")
    protected ResultDto<List<PlaLawCaseGrtRel>> queryByPlaLawCaseGrtRel(@RequestBody QueryModel queryModel) {
        List<PlaLawCaseGrtRel> list = plaLawCaseGrtRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawCaseGrtRel>>(list);
    }
}
