/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.List;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaLawCaseSecdBaseInfo;
import cn.com.yusys.yusp.repository.mapper.PlaLawCaseSecdBaseInfoMapper;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawCaseSecdBaseInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-05 17:45:58
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaLawCaseSecdBaseInfoService {

    @Autowired
    private PlaLawCaseSecdBaseInfoMapper plaLawCaseSecdBaseInfoMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PlaLawCaseSecdBaseInfo selectByPrimaryKey(String plcsbiNo) {
        return plaLawCaseSecdBaseInfoMapper.selectByPrimaryKey(plcsbiNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaLawCaseSecdBaseInfo> selectAll(QueryModel model) {
        List<PlaLawCaseSecdBaseInfo> records = (List<PlaLawCaseSecdBaseInfo>) plaLawCaseSecdBaseInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PlaLawCaseSecdBaseInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaLawCaseSecdBaseInfo> list = plaLawCaseSecdBaseInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PlaLawCaseSecdBaseInfo record) {
        return plaLawCaseSecdBaseInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PlaLawCaseSecdBaseInfo record) {
        return plaLawCaseSecdBaseInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PlaLawCaseSecdBaseInfo record) {

        int count=0;
        if(record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            if(plaLawCaseSecdBaseInfoMapper.selectByPrimaryKey(record.getPlcsbiNo()) != null){
                record.setUpdId(inputId);
                record.setUpdBrId(inputBrId);
                record.setUpdDate(openDay);
                record.setUpdateTime(createTime);
                count =  plaLawCaseSecdBaseInfoMapper.updateByPrimaryKeySelective(record);
            } else {
                record.setInputId(inputId);
                record.setInputBrId(inputBrId);
                record.setInputDate(openDay);
                record.setCreateTime(createTime);
                record.setUpdId(inputId);
                record.setUpdBrId(inputBrId);
                record.setUpdDate(openDay);
                record.setUpdateTime(createTime);
                count =  plaLawCaseSecdBaseInfoMapper.insertSelective(record);
            }
        }
            return count;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PlaLawCaseSecdBaseInfo record) {
        return plaLawCaseSecdBaseInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String plcsbiNo) {
        return plaLawCaseSecdBaseInfoMapper.deleteByPrimaryKey(plcsbiNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaLawCaseSecdBaseInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByPlaLawCaseSecdBaseInfo
     * @方法描述: 根据律师姓名查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int selectByPlaLawCaseSecdBaseInfo(String lawyerName) {
        return plaLawCaseSecdBaseInfoMapper.selectByPlaLawCaseSecdBaseInfo(lawyerName);
    }
}
