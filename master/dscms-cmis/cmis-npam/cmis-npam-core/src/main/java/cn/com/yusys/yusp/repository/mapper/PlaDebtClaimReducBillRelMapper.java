/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaDebtClaimReducBillRel;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaDebtClaimReducBillRelMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-08 19:44:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface PlaDebtClaimReducBillRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    PlaDebtClaimReducBillRel selectByPrimaryKey(@Param("pdcrbrSerno") String pdcrbrSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PlaDebtClaimReducBillRel> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insert(PlaDebtClaimReducBillRel record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insertSelective(PlaDebtClaimReducBillRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPrimaryKey(PlaDebtClaimReducBillRel record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPrimaryKeySelective(PlaDebtClaimReducBillRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    int deleteByPrimaryKey(@Param("pdcrbrSerno") String pdcrbrSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询 查询单个金额总和
     * @参数与返回说明:
     * @算法描述: 无
     * 刘权
     */
    Map selectBySum(String pdcraiSerno);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据业务流水号删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    int deleteByPdcraiSerno(@Param("pdcraiSerno") String pdcraiSerno);

    /**
     * @方法名称: selectByPdcraiSerno
     * @方法描述: 根据业务流水号借据编号，查询借据信息
     * @参数与返回说明: 返回借据信息
     * @算法描述: 无
     */
    PlaDebtClaimReducBillRel selectByPdcraiSerno(@Param("pdcraiSerno") String pdcraiSerno, @Param("billNo") String billNo);

    /**
     * @方法名称: queryByApproveStatus
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PlaDebtClaimReducBillRel> queryByApproveStatus(QueryModel model);

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PlaDebtClaimReducBillRel> queryByPdcraiSerno(@Param("pdcraiSerno") String pdcraiSerno);
}