/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaAssetPldRecordRel;
import cn.com.yusys.yusp.repository.mapper.PlaAssetPldRecordRelMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldRecordRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaAssetPldRecordRelService {

    @Autowired
    private PlaAssetPldRecordRelMapper plaAssetPldRecordRelMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaAssetPldRecordRel selectByPrimaryKey(String papreSerno) {
        return plaAssetPldRecordRelMapper.selectByPrimaryKey(papreSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaAssetPldRecordRel> selectAll(QueryModel model) {
        List<PlaAssetPldRecordRel> records = (List<PlaAssetPldRecordRel>) plaAssetPldRecordRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaAssetPldRecordRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaAssetPldRecordRel> list = plaAssetPldRecordRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaAssetPldRecordRel record) {
        return plaAssetPldRecordRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaAssetPldRecordRel record) {
        return plaAssetPldRecordRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaAssetPldRecordRel record) {
        return plaAssetPldRecordRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaAssetPldRecordRel record) {
        return plaAssetPldRecordRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String papreSerno) {
        return plaAssetPldRecordRelMapper.deleteByPrimaryKey(papreSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaAssetPldRecordRelMapper.deleteByIds(ids);
    }

    /**
     * 根据抵债资产处置申请流水号获取关联信息
     *
     * @author jijian_yx
     * @date 2021/6/9 20:25
     **/
    public PlaAssetPldRecordRel getRecordRel(String papaiSerno) {
        return plaAssetPldRecordRelMapper.getRecordRel(papaiSerno);
    }

    /**
     * 保存/更新 抵债资产处置记账关联信息
     *
     * @author jijian_yx
     * @date 2021/6/9 20:25
     **/
    public int save(PlaAssetPldRecordRel recordRel) {
        PlaAssetPldRecordRel plaAssetPldRecordRel = selectByPrimaryKey(recordRel.getPapreSerno());
        int result = 0;
        // 判断是否已经存在
        if (plaAssetPldRecordRel != null) {
            // 更新操作
            result = updateSelective(recordRel);
        } else {
            // 新增操作
            result = insertSelective(recordRel);
        }

        // TODO 发送核心记账逻辑待补充

        return result;
    }
}
