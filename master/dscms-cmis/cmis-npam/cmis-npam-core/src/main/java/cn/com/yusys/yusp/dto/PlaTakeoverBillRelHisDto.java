package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaTakeoverBillRelHis
 * @类描述: pla_takeover_bill_rel_his数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-29 16:14:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PlaTakeoverBillRelHisDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 关联表流水号 **/
	private String ptbrhSerno;
	
	/** 转让业务流水号 **/
	private String ptaiSerno;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 币种 **/
	private String curType;
	
	/** 贷款金额 **/
	private java.math.BigDecimal loanAmt;
	
	/** 贷款余额 **/
	private java.math.BigDecimal loanBalance;
	
	/** 拖欠利息总额 **/
	private java.math.BigDecimal totalTqlxAmt;
	
	/** 贷款起始日 **/
	private String loanStartDate;
	
	/** 贷款到期日 **/
	private String loanEndDate;
	
	/** 执行年利率 **/
	private java.math.BigDecimal execRateYear;
	
	/** 五级分类 **/
	private String fiveClass;
	
	/** 担保方式 **/
	private String guarMode;
	
	/** 封包金额 **/
	private java.math.BigDecimal packageAmt;
	
	/** 转让对价金额 **/
	private java.math.BigDecimal takeoverPrice;
	
	/** 是否转让代理资产 **/
	private String isTransAgcyAsset;
	
	/** 变更状态 **/
	private String changeStatus;
	
	/** 登记状态 **/
	private String regiStatus;
	
	/** 记账状态 **/
	private String recordStatus;
	
	/** 变更日期 **/
	private String modifyDate;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param ptbrhSerno
	 */
	public void setPtbrhSerno(String ptbrhSerno) {
		this.ptbrhSerno = ptbrhSerno == null ? null : ptbrhSerno.trim();
	}
	
    /**
     * @return PtbrhSerno
     */	
	public String getPtbrhSerno() {
		return this.ptbrhSerno;
	}
	
	/**
	 * @param ptaiSerno
	 */
	public void setPtaiSerno(String ptaiSerno) {
		this.ptaiSerno = ptaiSerno == null ? null : ptaiSerno.trim();
	}
	
    /**
     * @return PtaiSerno
     */	
	public String getPtaiSerno() {
		return this.ptaiSerno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return LoanAmt
     */	
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(java.math.BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return LoanBalance
     */	
	public java.math.BigDecimal getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param totalTqlxAmt
	 */
	public void setTotalTqlxAmt(java.math.BigDecimal totalTqlxAmt) {
		this.totalTqlxAmt = totalTqlxAmt;
	}
	
    /**
     * @return TotalTqlxAmt
     */	
	public java.math.BigDecimal getTotalTqlxAmt() {
		return this.totalTqlxAmt;
	}
	
	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate == null ? null : loanStartDate.trim();
	}
	
    /**
     * @return LoanStartDate
     */	
	public String getLoanStartDate() {
		return this.loanStartDate;
	}
	
	/**
	 * @param loanEndDate
	 */
	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate == null ? null : loanEndDate.trim();
	}
	
    /**
     * @return LoanEndDate
     */	
	public String getLoanEndDate() {
		return this.loanEndDate;
	}
	
	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}
	
    /**
     * @return ExecRateYear
     */	
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}
	
	/**
	 * @param fiveClass
	 */
	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass == null ? null : fiveClass.trim();
	}
	
    /**
     * @return FiveClass
     */	
	public String getFiveClass() {
		return this.fiveClass;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode == null ? null : guarMode.trim();
	}
	
    /**
     * @return GuarMode
     */	
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param packageAmt
	 */
	public void setPackageAmt(java.math.BigDecimal packageAmt) {
		this.packageAmt = packageAmt;
	}
	
    /**
     * @return PackageAmt
     */	
	public java.math.BigDecimal getPackageAmt() {
		return this.packageAmt;
	}
	
	/**
	 * @param takeoverPrice
	 */
	public void setTakeoverPrice(java.math.BigDecimal takeoverPrice) {
		this.takeoverPrice = takeoverPrice;
	}
	
    /**
     * @return TakeoverPrice
     */	
	public java.math.BigDecimal getTakeoverPrice() {
		return this.takeoverPrice;
	}
	
	/**
	 * @param isTransAgcyAsset
	 */
	public void setIsTransAgcyAsset(String isTransAgcyAsset) {
		this.isTransAgcyAsset = isTransAgcyAsset == null ? null : isTransAgcyAsset.trim();
	}
	
    /**
     * @return IsTransAgcyAsset
     */	
	public String getIsTransAgcyAsset() {
		return this.isTransAgcyAsset;
	}
	
	/**
	 * @param changeStatus
	 */
	public void setChangeStatus(String changeStatus) {
		this.changeStatus = changeStatus == null ? null : changeStatus.trim();
	}
	
    /**
     * @return ChangeStatus
     */	
	public String getChangeStatus() {
		return this.changeStatus;
	}
	
	/**
	 * @param regiStatus
	 */
	public void setRegiStatus(String regiStatus) {
		this.regiStatus = regiStatus == null ? null : regiStatus.trim();
	}
	
    /**
     * @return RegiStatus
     */	
	public String getRegiStatus() {
		return this.regiStatus;
	}
	
	/**
	 * @param recordStatus
	 */
	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus == null ? null : recordStatus.trim();
	}
	
    /**
     * @return RecordStatus
     */	
	public String getRecordStatus() {
		return this.recordStatus;
	}
	
	/**
	 * @param modifyDate
	 */
	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate == null ? null : modifyDate.trim();
	}
	
    /**
     * @return ModifyDate
     */	
	public String getModifyDate() {
		return this.modifyDate;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}