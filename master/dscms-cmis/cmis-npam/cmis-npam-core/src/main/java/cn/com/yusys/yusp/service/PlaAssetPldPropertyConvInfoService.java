/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisNpamConstants;
import cn.com.yusys.yusp.domain.PlaAssetPldPropertyConvInfo;
import cn.com.yusys.yusp.repository.mapper.PlaAssetPldPropertyConvInfoMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldPropertyConvInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaAssetPldPropertyConvInfoService {

    @Autowired
    private PlaAssetPldPropertyConvInfoMapper plaAssetPldPropertyConvInfoMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaAssetPldPropertyConvInfo selectByPrimaryKey(String pappciSerno) {
        return plaAssetPldPropertyConvInfoMapper.selectByPrimaryKey(pappciSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaAssetPldPropertyConvInfo> selectAll(QueryModel model) {
        List<PlaAssetPldPropertyConvInfo> records = (List<PlaAssetPldPropertyConvInfo>) plaAssetPldPropertyConvInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaAssetPldPropertyConvInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaAssetPldPropertyConvInfo> list = plaAssetPldPropertyConvInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaAssetPldPropertyConvInfo record) {
        return plaAssetPldPropertyConvInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaAssetPldPropertyConvInfo record) {
        return plaAssetPldPropertyConvInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaAssetPldPropertyConvInfo record) {
        return plaAssetPldPropertyConvInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaAssetPldPropertyConvInfo record) {
        return plaAssetPldPropertyConvInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pappciSerno) {
        return plaAssetPldPropertyConvInfoMapper.deleteByPrimaryKey(pappciSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaAssetPldPropertyConvInfoMapper.deleteByIds(ids);
    }

    /**
     * 根据处置申请流水号获取转固信息
     *
     * @author jijian_yx
     * @date 2021/6/10 11:27
     **/
    public PlaAssetPldPropertyConvInfo selectByPapaiSerno(String papaiSerno) {
        return plaAssetPldPropertyConvInfoMapper.selectByPapaiSerno(papaiSerno);
    }

    /**
     * 保存/更新转固信息
     *
     * @author jijian_yx
     * @date 2021/6/11 9:19
     **/
    public Map<String,String> insertInfo(PlaAssetPldPropertyConvInfo plaAssetPldPropertyConvInfo) {
        Map<String,String> map = new HashMap<>();
        String flag = "";
        String msg = "";
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        PlaAssetPldPropertyConvInfo convInfo = selectByPapaiSerno(plaAssetPldPropertyConvInfo.getPapaiSerno());
        if (convInfo != null) {
            plaAssetPldPropertyConvInfo.setUpdDate(openDay);
            updateSelective(plaAssetPldPropertyConvInfo);
            flag = CmisNpamConstants.FLAG_SUCCESS;
            msg = "更新成功";
        } else {
            plaAssetPldPropertyConvInfo.setUpdDate(openDay);
            insertSelective(plaAssetPldPropertyConvInfo);
            flag = CmisNpamConstants.FLAG_SUCCESS;
            msg = "保存成功";
        }
        map.put("flag",flag);
        map.put("msg",msg);
        return map;
    }

    /**
     * 根据抵债资产处置流水删除转固信息
     *
     * @author jijian_yx
     * @date 2021/6/11 10:56
     **/
    public int deleteByPapaiSerno(String papaiSerno) {
        return plaAssetPldPropertyConvInfoMapper.deleteByPapaiSerno(papaiSerno);
    }
}
