/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawBrokeCaseInfo
 * @类描述: pla_law_broke_case_info数据实体类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-03 20:31:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_law_broke_case_info")
public class PlaLawBrokeCaseInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 破产案件编号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "BROKE_CASE_NO")
    private String brokeCaseNo;
    /**
     * 案件编号
     **/
    @Column(name = "CASE_NO", unique = false, nullable = true, length = 40)
    private String caseNo;

    /**
     * 破产公司编号
     **/
    @Column(name = "BROKE_COM_NO", unique = false, nullable = true, length = 80)
    private String brokeComNo;

    /**
     * 破产公司名称
     **/
    @Column(name = "BROKE_COM_NAME", unique = false, nullable = true, length = 80)
    private String brokeComName;

    /**
     * 法院裁定书落款日期
     **/
    @Column(name = "COURT_DCMNTS_INSCRIBE_DATE", unique = false, nullable = true, length = 10)
    private String courtDcmntsInscribeDate;

    /**
     * 破产类型
     **/
    @Column(name = "BROKE_TYPE", unique = false, nullable = true, length = 5)
    private String brokeType;

    /**
     * 破产对象性质
     **/
    @Column(name = "BROKE_OBJ_CHA", unique = false, nullable = true, length = 5)
    private String brokeObjCha;

    /**
     * 受理法院
     **/
    @Column(name = "ACCEPT_COURT", unique = false, nullable = true, length = 80)
    private String acceptCourt;

    /**
     * 破产管理人
     **/
    @Column(name = "BROKE_MANAGER", unique = false, nullable = true, length = 80)
    private String brokeManager;

    /**
     * 破产管理人联系方式
     **/
    @Column(name = "BROKE_MANAGER_TEL_NO", unique = false, nullable = true, length = 20)
    private String brokeManagerTelNo;

    /**
     * 代理方式
     **/
    @Column(name = "AGCY_MODE", unique = false, nullable = true, length = 5)
    private String agcyMode;

    /**
     * 律师姓名
     **/
    @Column(name = "LAWYER_NAME", unique = false, nullable = true, length = 80)
    private String lawyerName;

    /**
     * 律师联系方式
     **/
    @Column(name = "LAWYER_TEL", unique = false, nullable = true, length = 20)
    private String lawyerTel;

    /**
     * 所属律师事务所名称
     **/
    @Column(name = "LAW_OFFICE_NAME", unique = false, nullable = true, length = 80)
    private String lawOfficeName;

    /**
     * 内部代理人姓名
     **/
    @Column(name = "AGCY_NAME", unique = false, nullable = true, length = 80)
    private String agcyName;

    /**
     * 内部代理人联系方式
     **/
    @Column(name = "AGCY_TEL_NO", unique = false, nullable = true, length = 20)
    private String agcyTelNo;

    /**
     * 债权申报截止日期
     **/
    @Column(name = "CLAIMS_DECLARE_END_DATE", unique = false, nullable = true, length = 10)
    private String claimsDeclareEndDate;

    /**
     * 我行申报日期
     **/
    @Column(name = "BANK_DECLARE_DATE", unique = false, nullable = true, length = 10)
    private String bankDeclareDate;

    /**
     * 我行申报债权金额
     **/
    @Column(name = "DECLARE_CLAIMS_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal declareClaimsAmt;

    /**
     * 管理人确认我行债权金额
     **/
    @Column(name = "CFIRM_CLAIMS_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal cfirmClaimsAmt;

    /**
     * 第一次债权人会议时间
     **/
    @Column(name = "FIR_MEET_DATE", unique = false, nullable = true, length = 10)
    private String firMeetDate;

    /**
     * 第二次债权人会议时间
     **/
    @Column(name = "SEC_MEET_DATE", unique = false, nullable = true, length = 10)
    private String secMeetDate;

    /**
     * 案件进程
     **/
    @Column(name = "CASE_PROCESS", unique = false, nullable = true, length = 5)
    private String caseProcess;

    /**
     * 破产终结裁定书落款日期
     **/
    @Column(name = "DCMNTS_INSCRIBE_DATE", unique = false, nullable = true, length = 10)
    private String dcmntsInscribeDate;

    /**
     * 总受偿金额
     **/
    @Column(name = "PAYMENT_TOTAL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal paymentTotalAmt;

    /**
     * 受偿比率
     **/
    @Column(name = "PAYMENT_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal paymentRate;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaLawBrokeCaseInfo() {
        // default implementation ignored
    }

    public String getBrokeComNo() {
        return brokeComNo;
    }

    public void setBrokeComNo(String brokeComNo) {
        this.brokeComNo = brokeComNo;
    }

    /**
     * @param brokeCaseNo
     */
    public void setBrokeCaseNo(String brokeCaseNo) {
        this.brokeCaseNo = brokeCaseNo;
    }

    /**
     * @return brokeCaseNo
     */
    public String getBrokeCaseNo() {
        return this.brokeCaseNo;
    }

    /**
     * @param brokeComName
     */
    public void setBrokeComName(String brokeComName) {
        this.brokeComName = brokeComName;
    }

    /**
     * @return brokeComName
     */
    public String getBrokeComName() {
        return this.brokeComName;
    }

    /**
     * @param courtDcmntsInscribeDate
     */
    public void setCourtDcmntsInscribeDate(String courtDcmntsInscribeDate) {
        this.courtDcmntsInscribeDate = courtDcmntsInscribeDate;
    }

    /**
     * @return courtDcmntsInscribeDate
     */
    public String getCourtDcmntsInscribeDate() {
        return this.courtDcmntsInscribeDate;
    }

    /**
     * @param brokeType
     */
    public void setBrokeType(String brokeType) {
        this.brokeType = brokeType;
    }

    /**
     * @return brokeType
     */
    public String getBrokeType() {
        return this.brokeType;
    }

    /**
     * @param brokeObjCha
     */
    public void setBrokeObjCha(String brokeObjCha) {
        this.brokeObjCha = brokeObjCha;
    }

    /**
     * @return brokeObjCha
     */
    public String getBrokeObjCha() {
        return this.brokeObjCha;
    }

    /**
     * @param acceptCourt
     */
    public void setAcceptCourt(String acceptCourt) {
        this.acceptCourt = acceptCourt;
    }

    /**
     * @return acceptCourt
     */
    public String getAcceptCourt() {
        return this.acceptCourt;
    }

    /**
     * @param brokeManager
     */
    public void setBrokeManager(String brokeManager) {
        this.brokeManager = brokeManager;
    }

    /**
     * @return brokeManager
     */
    public String getBrokeManager() {
        return this.brokeManager;
    }

    /**
     * @param brokeManagerTelNo
     */
    public void setBrokeManagerTelNo(String brokeManagerTelNo) {
        this.brokeManagerTelNo = brokeManagerTelNo;
    }

    /**
     * @return brokeManagerTelNo
     */
    public String getBrokeManagerTelNo() {
        return this.brokeManagerTelNo;
    }

    /**
     * @param agcyMode
     */
    public void setAgcyMode(String agcyMode) {
        this.agcyMode = agcyMode;
    }

    /**
     * @return agcyMode
     */
    public String getAgcyMode() {
        return this.agcyMode;
    }

    /**
     * @param lawyerName
     */
    public void setLawyerName(String lawyerName) {
        this.lawyerName = lawyerName;
    }

    /**
     * @return lawyerName
     */
    public String getLawyerName() {
        return this.lawyerName;
    }

    /**
     * @param lawyerTel
     */
    public void setLawyerTel(String lawyerTel) {
        this.lawyerTel = lawyerTel;
    }

    /**
     * @return lawyerTel
     */
    public String getLawyerTel() {
        return this.lawyerTel;
    }

    /**
     * @param lawOfficeName
     */
    public void setLawOfficeName(String lawOfficeName) {
        this.lawOfficeName = lawOfficeName;
    }

    /**
     * @return lawOfficeName
     */
    public String getLawOfficeName() {
        return this.lawOfficeName;
    }

    /**
     * @param agcyName
     */
    public void setAgcyName(String agcyName) {
        this.agcyName = agcyName;
    }

    /**
     * @return agcyName
     */
    public String getAgcyName() {
        return this.agcyName;
    }

    /**
     * @param agcyTelNo
     */
    public void setAgcyTelNo(String agcyTelNo) {
        this.agcyTelNo = agcyTelNo;
    }

    /**
     * @return agcyTelNo
     */
    public String getAgcyTelNo() {
        return this.agcyTelNo;
    }

    /**
     * @param claimsDeclareEndDate
     */
    public void setClaimsDeclareEndDate(String claimsDeclareEndDate) {
        this.claimsDeclareEndDate = claimsDeclareEndDate;
    }

    /**
     * @return claimsDeclareEndDate
     */
    public String getClaimsDeclareEndDate() {
        return this.claimsDeclareEndDate;
    }

    /**
     * @param bankDeclareDate
     */
    public void setBankDeclareDate(String bankDeclareDate) {
        this.bankDeclareDate = bankDeclareDate;
    }

    /**
     * @return bankDeclareDate
     */
    public String getBankDeclareDate() {
        return this.bankDeclareDate;
    }

    /**
     * @param declareClaimsAmt
     */
    public void setDeclareClaimsAmt(java.math.BigDecimal declareClaimsAmt) {
        this.declareClaimsAmt = declareClaimsAmt;
    }

    /**
     * @return declareClaimsAmt
     */
    public java.math.BigDecimal getDeclareClaimsAmt() {
        return this.declareClaimsAmt;
    }

    /**
     * @param cfirmClaimsAmt
     */
    public void setCfirmClaimsAmt(java.math.BigDecimal cfirmClaimsAmt) {
        this.cfirmClaimsAmt = cfirmClaimsAmt;
    }

    /**
     * @return cfirmClaimsAmt
     */
    public java.math.BigDecimal getCfirmClaimsAmt() {
        return this.cfirmClaimsAmt;
    }

    /**
     * @param firMeetDate
     */
    public void setFirMeetDate(String firMeetDate) {
        this.firMeetDate = firMeetDate;
    }

    /**
     * @return firMeetDate
     */
    public String getFirMeetDate() {
        return this.firMeetDate;
    }

    /**
     * @param secMeetDate
     */
    public void setSecMeetDate(String secMeetDate) {
        this.secMeetDate = secMeetDate;
    }

    /**
     * @return secMeetDate
     */
    public String getSecMeetDate() {
        return this.secMeetDate;
    }

    /**
     * @param caseProcess
     */
    public void setCaseProcess(String caseProcess) {
        this.caseProcess = caseProcess;
    }

    /**
     * @return caseProcess
     */
    public String getCaseProcess() {
        return this.caseProcess;
    }

    /**
     * @param dcmntsInscribeDate
     */
    public void setDcmntsInscribeDate(String dcmntsInscribeDate) {
        this.dcmntsInscribeDate = dcmntsInscribeDate;
    }

    /**
     * @return dcmntsInscribeDate
     */
    public String getDcmntsInscribeDate() {
        return this.dcmntsInscribeDate;
    }

    /**
     * @param paymentTotalAmt
     */
    public void setPaymentTotalAmt(java.math.BigDecimal paymentTotalAmt) {
        this.paymentTotalAmt = paymentTotalAmt;
    }

    /**
     * @return paymentTotalAmt
     */
    public java.math.BigDecimal getPaymentTotalAmt() {
        return this.paymentTotalAmt;
    }

    /**
     * @param paymentRate
     */
    public void setPaymentRate(java.math.BigDecimal paymentRate) {
        this.paymentRate = paymentRate;
    }

    /**
     * @return paymentRate
     */
    public java.math.BigDecimal getPaymentRate() {
        return this.paymentRate;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getCaseNo() {
        return caseNo;
    }
}