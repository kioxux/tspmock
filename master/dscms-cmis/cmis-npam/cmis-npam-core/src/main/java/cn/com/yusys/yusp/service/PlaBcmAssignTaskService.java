/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.CmisNpamConstants;
import cn.com.yusys.yusp.domain.PlaBcmRel;
import cn.com.yusys.yusp.domain.PlaBcmTaskInfo;
import cn.com.yusys.yusp.dto.AccLoanDto;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.repository.mapper.PlaBcmTaskInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaBcmAssignTask;
import cn.com.yusys.yusp.repository.mapper.PlaBcmAssignTaskMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBcmAssignTaskService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-01 10:07:42
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaBcmAssignTaskService {

    @Autowired
    private PlaBcmAssignTaskMapper plaBcmAssignTaskMapper;


    @Autowired
    private CmisBizClientService cmisBizClientService;        //调用贷款台账服务

    @Autowired
    private PlaBcmTaskInfoService plaBcmTaskInfoService;

    @Autowired
    private PlaBcmRelService plaBcmRelService;
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;
    @Autowired
    private PlaBcmTaskInfoMapper plaBcmTaskInfoMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaBcmAssignTask selectByPrimaryKey(String taskNo) {
        return plaBcmAssignTaskMapper.selectByPrimaryKey(taskNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaBcmAssignTask> selectAll(QueryModel model) {
        List<PlaBcmAssignTask> records = (List<PlaBcmAssignTask>) plaBcmAssignTaskMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaBcmAssignTask> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaBcmAssignTask> list = plaBcmAssignTaskMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaBcmAssignTask record) {
        return plaBcmAssignTaskMapper.insert(record);
    }


    /**
     * @方法名称: insertBydoAllocation
     * @方法描述: 催收任务人工指派分配按钮  保存到催收任务信息表
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertBydoAllocation(PlaBcmAssignTask record) {
        int count = 0;
        //取出资产类型字段
        String assetType = record.getAssetType();
        // 申请时间
        String openDay =stringRedisTemplate.opsForValue().get("openDay");
        //如果资产类型为非不良贷款
        // 01 表示非不良贷款
        if ("01".equals(assetType)) {
            //查询出非不良贷款的条件数据
            ResultDto<List<AccLoanDto>> accLoanDto = cmisBizClientService.selectByCusIdOnAssetType(record.getCusId());
            List<AccLoanDto> accLoanDtoList = new ArrayList<>();
            if ("0".equals(accLoanDto.getCode())) {
                accLoanDtoList = accLoanDto.getData();
            } else {
                throw BizException.error("", EcnEnum.ECN069999.key, EcnEnum.ECN069999.value);
            }
            //催收任务信息表
            PlaBcmTaskInfo plaBcmTaskInfo = new PlaBcmTaskInfo();
            //默认赋值相同的字段到催收任务信息表中
            BeanUtils.copyProperties(record, plaBcmTaskInfo);
            //正常本金
            BigDecimal zcbjAmt = new BigDecimal("0.00");
            //逾期本金
            BigDecimal overdueCapAmt = new BigDecimal("0.00");
            //拖欠利息总额
            BigDecimal totalTqlxAmt = new BigDecimal("0.00");
            //欠息
            BigDecimal debitInt = new BigDecimal("0.00");
            //罚息
            BigDecimal penalInt = new BigDecimal("0.00");
            //复息
            BigDecimal compoundInt = new BigDecimal("0.00");

            PlaBcmRel plaBcmRel = new PlaBcmRel();
            //催收关联业务表
            for (Object map : accLoanDtoList) {
                //获取当前贷款台账数据信息
                PlaBcmRel plaBcmRels = ObjectMapperUtils.instance().convertValue(map, PlaBcmRel.class);
                if (plaBcmRels != null) {
                    //催收业务流水号 TODO 流水号标准确定后换流水号不用uuid
                    plaBcmRel.setPbrcSerno(StringUtils.getUUID());
                    //任务编号
                    plaBcmRel.setTaskNo(record.getTaskNo());
                    //借据编号
                    plaBcmRel.setBillNo(plaBcmRels.getBillNo());
                    //合同编号
                    plaBcmRel.setContNo(plaBcmRels.getContNo());
                    //产品名称
                    plaBcmRel.setPrdName(plaBcmRels.getPrdName());
                    //正常本金
                    plaBcmRel.setZcbjAmt(plaBcmRels.getZcbjAmt());
                    //逾期本金
                    plaBcmRel.setOverdueCapAmt(plaBcmRels.getOverdueCapAmt());
                    //欠息
                    plaBcmRel.setDebitInt(plaBcmRels.getDebitInt());
                    //罚息
                    plaBcmRel.setPenalInt(plaBcmRels.getPenalInt());
                    //复息
                    plaBcmRel.setCompoundInt(plaBcmRels.getCompoundInt());
                    //逾期天数
                    plaBcmRel.setOverdueDay(plaBcmRels.getOverdueDay());
                    //五级分类
                    plaBcmRel.setFiveClass(plaBcmRels.getFiveClass());
                    //正常本金总额
                    zcbjAmt = zcbjAmt.add(plaBcmRels.getZcbjAmt() == null ? BigDecimal.ZERO : plaBcmRels.getZcbjAmt());
                    //逾期本金总额
                    overdueCapAmt = overdueCapAmt.add(plaBcmRels.getOverdueCapAmt() == null ? BigDecimal.ZERO : plaBcmRels.getOverdueCapAmt());

                    //罚息加欠息加复息
                    debitInt = debitInt.add(plaBcmRels.getDebitInt() == null ? BigDecimal.ZERO : plaBcmRels.getDebitInt());
                    penalInt = penalInt.add(plaBcmRels.getPenalInt() == null ? BigDecimal.ZERO : plaBcmRels.getPenalInt());
                    compoundInt = compoundInt.add(plaBcmRels.getCompoundInt() == null ? BigDecimal.ZERO : plaBcmRels.getCompoundInt());

                    //拖欠利息总额
                    totalTqlxAmt = totalTqlxAmt.add(debitInt);
                    totalTqlxAmt = totalTqlxAmt.add(penalInt);
                    totalTqlxAmt = totalTqlxAmt.add(compoundInt);

                    //拖欠利息总额
                    plaBcmRel.setTotalTqlxAmt(totalTqlxAmt);
                    // 申请人
                    String inputId = "";
                    // 申请机构
                    String inputBrId = "";
                    // 创建时间
                    Date createTime = DateUtils.getCurrTimestamp();
                    // 获取用户信息
                    User userInfo = SessionUtils.getUserInformation();
                    if (userInfo != null) {
                        // 申请人
                        inputId = userInfo.getLoginCode();
                        // 申请机构
                        inputBrId = userInfo.getOrg().getCode();
                    }
                    //把数据插入到催收关联业务表中
                    plaBcmRelService.selectByPrimaryKey(plaBcmRel.getPbrcSerno());
                    plaBcmRel.setInputId(inputId);
                    plaBcmRel.setInputBrId(inputBrId);
                    plaBcmRel.setInputDate(openDay);
                    plaBcmRel.setCreateTime(createTime);
                    plaBcmRel.setUpdId(inputId);
                    plaBcmRel.setUpdBrId(inputBrId);
                    plaBcmRel.setUpdDate(openDay);
                    plaBcmRel.setUpdateTime(createTime);
                    plaBcmRelService.insert(plaBcmRel);
                }
            }
            plaBcmTaskInfo.setZcbjAmt(zcbjAmt);
            plaBcmTaskInfo.setOverdueCapAmt(overdueCapAmt);
            plaBcmTaskInfo.setTotalTqlxAmt(totalTqlxAmt);
            plaBcmTaskInfo.setTaskFlag("02");   //默认任务标识为人工发起
            plaBcmTaskInfo.setBcmStatus("02");  //默认催收状态为未完成
            //将数据插入到催收任务信息表中
            count = plaBcmTaskInfoService.insert(plaBcmTaskInfo);

            //将任务分配状态修改为已分配
            record.setTaskStatus("01");
            plaBcmAssignTaskMapper.updateByPrimaryKeySelective(record);
        } else {
            //查询出不良贷款的条件数据
            ResultDto<List<AccLoanDto>> accLoanDto = cmisBizClientService.selectByCusIdNotType(record.getCusId());
            List<AccLoanDto> accLoanDtoList = new ArrayList<>();
            if ("0".equals(accLoanDto.getCode())) {
                accLoanDtoList = accLoanDto.getData();
            } else {
                throw BizException.error("", EcnEnum.ECN069999.key, EcnEnum.ECN069999.value);
            }
            //催收任务信息表
            PlaBcmTaskInfo plaBcmTaskInfo = new PlaBcmTaskInfo();
            //默认赋值相同的字段到催收任务信息表中
            BeanUtils.copyProperties(record, plaBcmTaskInfo);

            //正常本金
            BigDecimal zcbjAmt = new BigDecimal("0.00");
            //逾期本金
            BigDecimal overdueCapAmt = new BigDecimal("0.00");
            //拖欠利息总额
            BigDecimal totalTqlxAmt = new BigDecimal("0.00");
            //欠息
            BigDecimal debitInt = new BigDecimal("0.00");
            //罚息
            BigDecimal penalInt = new BigDecimal("0.00");
            //复息
            BigDecimal compoundInt = new BigDecimal("0.00");

            //催收关联业务表
            PlaBcmRel plaBcmRel = new PlaBcmRel();

            for (Object map : accLoanDtoList) {
                //获取当前贷款台账数据信息
                PlaBcmRel plaBcmRels = ObjectMapperUtils.instance().convertValue(map, PlaBcmRel.class);
                if (plaBcmRels != null) {
                    //催收业务流水号 TODO 流水号标准确定后换流水号不用uuid
                    plaBcmRel.setPbrcSerno(StringUtils.getUUID());
                    //任务编号
                    plaBcmRel.setTaskNo(record.getTaskNo());
                    //借据编号
                    plaBcmRel.setBillNo(plaBcmRels.getBillNo());
                    //合同编号
                    plaBcmRel.setContNo(plaBcmRels.getContNo());
                    //产品名称
                    plaBcmRel.setPrdName(plaBcmRels.getPrdName());
                    //正常本金
                    plaBcmRel.setZcbjAmt(plaBcmRels.getZcbjAmt());
                    //逾期本金
                    plaBcmRel.setOverdueCapAmt(plaBcmRels.getOverdueCapAmt());
                    //拖欠利息总额
                    plaBcmRel.setTotalTqlxAmt(plaBcmRels.getTotalTqlxAmt());
                    //欠息
                    plaBcmRel.setDebitInt(plaBcmRels.getDebitInt());
                    //罚息
                    plaBcmRel.setPenalInt(plaBcmRels.getPenalInt());
                    //复息
                    plaBcmRel.setCompoundInt(plaBcmRels.getCompoundInt());
                    //逾期天数
                    plaBcmRel.setOverdueDay(plaBcmRels.getOverdueDay());
                    //五级分类
                    plaBcmRel.setFiveClass(plaBcmRels.getFiveClass());
                    //正常本金总额
                    zcbjAmt = zcbjAmt.add(plaBcmRels.getZcbjAmt() == null ? BigDecimal.ZERO : plaBcmRels.getZcbjAmt());
                    //逾期本金总额
                    overdueCapAmt = overdueCapAmt.add(plaBcmRels.getOverdueCapAmt() == null ? BigDecimal.ZERO : plaBcmRels.getOverdueCapAmt());

                    //罚息加欠息加复息
                    debitInt = debitInt.add(plaBcmRels.getDebitInt() == null ? BigDecimal.ZERO : plaBcmRels.getDebitInt());
                    penalInt = penalInt.add(plaBcmRels.getPenalInt() == null ? BigDecimal.ZERO : plaBcmRels.getPenalInt());
                    compoundInt = compoundInt.add(plaBcmRels.getCompoundInt() == null ? BigDecimal.ZERO : plaBcmRels.getCompoundInt());

                    //拖欠利息总额
                    totalTqlxAmt = totalTqlxAmt.add(debitInt);
                    totalTqlxAmt = totalTqlxAmt.add(penalInt);
                    totalTqlxAmt = totalTqlxAmt.add(compoundInt);

                    //拖欠利息总额
                    plaBcmRel.setTotalTqlxAmt(totalTqlxAmt);
                    // 申请人
                    String inputId = "";
                    // 申请机构
                    String inputBrId = "";
                    // 创建时间
                    Date createTime = DateUtils.getCurrTimestamp();
                    // 获取用户信息
                    User userInfo = SessionUtils.getUserInformation();
                    if (userInfo != null) {
                        // 申请人
                        inputId = userInfo.getLoginCode();
                        // 申请机构
                        inputBrId = userInfo.getOrg().getCode();
                    }
                    //把数据插入到催收关联业务表中
                    plaBcmRelService.selectByPrimaryKey(plaBcmRel.getPbrcSerno());
                    plaBcmRel.setInputId(inputId);
                    plaBcmRel.setInputBrId(inputBrId);
                    plaBcmRel.setInputDate(openDay);
                    plaBcmRel.setCreateTime(createTime);
                    plaBcmRel.setUpdId(inputId);
                    plaBcmRel.setUpdBrId(inputBrId);
                    plaBcmRel.setUpdDate(openDay);
                    plaBcmRel.setUpdateTime(createTime);
                    plaBcmRelService.insert(plaBcmRel);
                }
            }
            plaBcmTaskInfo.setZcbjAmt(zcbjAmt);
            plaBcmTaskInfo.setOverdueCapAmt(overdueCapAmt);
            plaBcmTaskInfo.setTotalTqlxAmt(totalTqlxAmt);
            plaBcmTaskInfo.setTaskFlag("02");   //默认任务标识为人工发起
            plaBcmTaskInfo.setBcmStatus("02");  //默认催收状态为未完成
            //将数据插入到催收任务信息表中
            count = plaBcmTaskInfoService.insert(plaBcmTaskInfo);

            //将任务分配状态修改为已分配
            record.setTaskStatus("01");
            plaBcmAssignTaskMapper.updateByPrimaryKeySelective(record);
        }
        return count;
    }


    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaBcmAssignTask record) {
        return plaBcmAssignTaskMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaBcmAssignTask record) {
        int count = 0;
        if (record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();
            }
            record.setUpdId(inputId);
            record.setUpdBrId(inputBrId);
            record.setUpdDate(inputDate);
            record.setUpdateTime(createTime);
            count = plaBcmAssignTaskMapper.updateByPrimaryKey(record);
        }
        return count;

    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaBcmAssignTask record) {
        return plaBcmAssignTaskMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String taskNo) {
        return plaBcmAssignTaskMapper.deleteByPrimaryKey(taskNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaBcmAssignTaskMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: save
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */

    public PlaBcmAssignTask save(PlaBcmAssignTask record) {
        int count = 0;
        if (record != null) {

            if (record.getTaskNo() == null || record.getTaskNo() == "") {
                String taskNo = sequenceTemplateClient.getSequenceTemplate(CmisNpamConstants.ZCBQ_BCM_SERNO, new HashMap<>());
                record.setTaskNo(taskNo);
            }
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay =stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            PlaBcmAssignTask plaBcmAssignTask = plaBcmAssignTaskMapper.selectByPrimaryKey(record.getTaskNo());
            if (plaBcmAssignTask != null) {
                record.setUpdId(inputId);
                record.setUpdBrId(inputBrId);
                record.setUpdDate(openDay);
                record.setUpdateTime(createTime);
                count = plaBcmAssignTaskMapper.updateByPrimaryKeySelective(record);
            } else {
                // 分配状态  默认不分配
                record.setTaskStatus(CmisNpamConstants.TASK_STATUS_02);
                record.setInputId(inputId);
                record.setInputBrId(inputBrId);
                record.setInputDate(openDay);
                record.setCreateTime(createTime);
                record.setUpdId(inputId);
                record.setUpdBrId(inputBrId);
                record.setUpdDate(openDay);
                record.setUpdateTime(createTime);
                count = plaBcmAssignTaskMapper.insert(record);
            }
        }
        return record;
    }


    /**
     * @方法名称: selectByBcmStatus
     * @方法描述: 根据催收状态查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int selectByBcmStatus(PlaBcmAssignTask record) {
        int count = 0;
        PlaBcmTaskInfo plaBcmTaskInfo = plaBcmTaskInfoMapper.selectByPrimaryKey(record.getTaskNo());
        if ("01".equals(plaBcmTaskInfo.getBcmStatus())){
            throw BizException.error(null, EcnEnum.ECN060025.key,EcnEnum.ECN060025.value);
        }else {
            plaBcmAssignTaskMapper.deleteByPrimaryKey(plaBcmTaskInfo.getTaskNo());
        }
        return count;
    }
}
