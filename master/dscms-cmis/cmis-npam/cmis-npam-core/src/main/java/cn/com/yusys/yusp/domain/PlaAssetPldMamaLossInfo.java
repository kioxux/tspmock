/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldMamaLossInfo
 * @类描述: pla_asset_pld_mama_loss_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_asset_pld_mama_loss_info")
public class PlaAssetPldMamaLossInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 毁损灭失信息流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PAPPMLI_SERNO")
    private String pappmliSerno;

    /**
     * 抵债资产处置申请流水号
     **/
    @Column(name = "PAPAI_SERNO", unique = false, nullable = true, length = 40)
    private String papaiSerno;

    /**
     * 抵债资产编号
     **/
    @Column(name = "PLDIMN_NO", unique = false, nullable = true, length = 40)
    private String pldimnNo;

    /**
     * 抵债资产名称
     **/
    @Column(name = "PLDIMN_NAME", unique = false, nullable = true, length = 80)
    private String pldimnName;

    /**
     * 抵债资产类型
     **/
    @Column(name = "PLDIMN_TYPE", unique = false, nullable = true, length = 5)
    private String pldimnType;

    /**
     * 抵债资产现状
     **/
    @Column(name = "PLDIMN_ASSET_STATUS", unique = false, nullable = true, length = 5)
    private String pldimnAssetStatus;

    /**
     * 情况描述
     **/
    @Column(name = "CASE_DEC", unique = false, nullable = true, length = 2000)
    private String caseDec;

    /**
     * 核销金额
     **/
    @Column(name = "WRITEOFF_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal writeoffAmt;

    /**
     * 核销日期
     **/
    @Column(name = "WRITEOFF_DATE", unique = false, nullable = true, length = 20)
    private String writeoffDate;

    /**
     * 核销理由
     **/
    @Column(name = "WRITEOFF_RESN", unique = false, nullable = true, length = 2000)
    private String writeoffResn;

    /**
     * 备注
     **/
    @Column(name = "MEMO", unique = false, nullable = true, length = 20)
    private String memo;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    /**
     * 补偿金额
     **/
    @Column(name = "COMPENSTATE_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal compenstateAmt;

    public PlaAssetPldMamaLossInfo() {
        // default implementation ignored
    }

    public BigDecimal getCompenstateAmt() {
        return compenstateAmt;
    }

    public void setCompenstateAmt(BigDecimal compenstateAmt) {
        this.compenstateAmt = compenstateAmt;
    }

    /**
     * @param pappmliSerno
     */
    public void setPappmliSerno(String pappmliSerno) {
        this.pappmliSerno = pappmliSerno;
    }

    /**
     * @return pappmliSerno
     */
    public String getPappmliSerno() {
        return this.pappmliSerno;
    }

    /**
     * @param papaiSerno
     */
    public void setPapaiSerno(String papaiSerno) {
        this.papaiSerno = papaiSerno;
    }

    /**
     * @return papaiSerno
     */
    public String getPapaiSerno() {
        return this.papaiSerno;
    }

    /**
     * @param pldimnNo
     */
    public void setPldimnNo(String pldimnNo) {
        this.pldimnNo = pldimnNo;
    }

    /**
     * @return pldimnNo
     */
    public String getPldimnNo() {
        return this.pldimnNo;
    }

    /**
     * @param pldimnName
     */
    public void setPldimnName(String pldimnName) {
        this.pldimnName = pldimnName;
    }

    /**
     * @return pldimnName
     */
    public String getPldimnName() {
        return this.pldimnName;
    }

    /**
     * @param pldimnType
     */
    public void setPldimnType(String pldimnType) {
        this.pldimnType = pldimnType;
    }

    /**
     * @return pldimnType
     */
    public String getPldimnType() {
        return this.pldimnType;
    }

    /**
     * @param pldimnAssetStatus
     */
    public void setPldimnAssetStatus(String pldimnAssetStatus) {
        this.pldimnAssetStatus = pldimnAssetStatus;
    }

    /**
     * @return pldimnAssetStatus
     */
    public String getPldimnAssetStatus() {
        return this.pldimnAssetStatus;
    }

    /**
     * @param caseDec
     */
    public void setCaseDec(String caseDec) {
        this.caseDec = caseDec;
    }

    /**
     * @return caseDec
     */
    public String getCaseDec() {
        return this.caseDec;
    }

    /**
     * @param writeoffAmt
     */
    public void setWriteoffAmt(java.math.BigDecimal writeoffAmt) {
        this.writeoffAmt = writeoffAmt;
    }

    /**
     * @return writeoffAmt
     */
    public java.math.BigDecimal getWriteoffAmt() {
        return this.writeoffAmt;
    }

    /**
     * @param writeoffDate
     */
    public void setWriteoffDate(String writeoffDate) {
        this.writeoffDate = writeoffDate;
    }

    /**
     * @return writeoffDate
     */
    public String getWriteoffDate() {
        return this.writeoffDate;
    }

    /**
     * @param writeoffResn
     */
    public void setWriteoffResn(String writeoffResn) {
        this.writeoffResn = writeoffResn;
    }

    /**
     * @return writeoffResn
     */
    public String getWriteoffResn() {
        return this.writeoffResn;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}