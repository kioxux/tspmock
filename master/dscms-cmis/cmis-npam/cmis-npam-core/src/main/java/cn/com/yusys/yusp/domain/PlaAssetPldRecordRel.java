/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldRecordRel
 * @类描述: pla_asset_pld_record_rel数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_asset_pld_record_rel")
public class PlaAssetPldRecordRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 处置记账流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PAPRE_SERNO")
    private String papreSerno;

    /**
     * 抵债资产处置申请流水号
     **/
    @Column(name = "PAPAI_SERNO", unique = false, nullable = true, length = 40)
    private String papaiSerno;

    /**
     * 抵入金额
     **/
    @Column(name = "DISP_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal dispAmt;

    /**
     * 处置金额
     **/
    @Column(name = "TO_ENTER_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal toEnterAmt;

    /**
     * 处置本金
     **/
    @Column(name = "DISP_EXPENSE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal dispExpense;

    /**
     * 处置费用
     **/
    @Column(name = "DISP_CAP", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal dispCap;

    /**
     * 处置利息
     **/
    @Column(name = "DISP_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal dispInt;

    /**
     * 处置挂账账号
     **/
    @Column(name = "DISP_SUSP_ACCT_NO", unique = false, nullable = true, length = 40)
    private String dispSuspAcctNo;

    /**
     * 挂账编号
     **/
    @Column(name = "SUSP_NO", unique = false, nullable = true, length = 40)
    private String suspNo;

    /**
     * 处置挂账账户名
     **/
    @Column(name = "DISP_SUSP_NAME", unique = false, nullable = true, length = 80)
    private String dispSuspName;

    /**
     * 抵债资产账号
     **/
    @Column(name = "RESIST_ASSET_ACCT_NO", unique = false, nullable = true, length = 40)
    private String resistAssetAcctNo;

    /**
     * 抵债资产户名
     **/
    @Column(name = "RESIST_ASSET_NAME", unique = false, nullable = true, length = 80)
    private String resistAssetName;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaAssetPldRecordRel() {
        // default implementation ignored
    }

    /**
     * @param papreSerno
     */
    public void setPapreSerno(String papreSerno) {
        this.papreSerno = papreSerno;
    }

    /**
     * @return papreSerno
     */
    public String getPapreSerno() {
        return this.papreSerno;
    }

    /**
     * @param papaiSerno
     */
    public void setPapaiSerno(String papaiSerno) {
        this.papaiSerno = papaiSerno;
    }

    /**
     * @return papaiSerno
     */
    public String getPapaiSerno() {
        return this.papaiSerno;
    }

    /**
     * @param dispAmt
     */
    public void setDispAmt(java.math.BigDecimal dispAmt) {
        this.dispAmt = dispAmt;
    }

    /**
     * @return dispAmt
     */
    public java.math.BigDecimal getDispAmt() {
        return this.dispAmt;
    }

    /**
     * @param toEnterAmt
     */
    public void setToEnterAmt(java.math.BigDecimal toEnterAmt) {
        this.toEnterAmt = toEnterAmt;
    }

    /**
     * @return toEnterAmt
     */
    public java.math.BigDecimal getToEnterAmt() {
        return this.toEnterAmt;
    }

    /**
     * @param dispExpense
     */
    public void setDispExpense(java.math.BigDecimal dispExpense) {
        this.dispExpense = dispExpense;
    }

    /**
     * @return dispExpense
     */
    public java.math.BigDecimal getDispExpense() {
        return this.dispExpense;
    }

    /**
     * @param dispCap
     */
    public void setDispCap(java.math.BigDecimal dispCap) {
        this.dispCap = dispCap;
    }

    /**
     * @return dispCap
     */
    public java.math.BigDecimal getDispCap() {
        return this.dispCap;
    }

    /**
     * @param dispInt
     */
    public void setDispInt(java.math.BigDecimal dispInt) {
        this.dispInt = dispInt;
    }

    /**
     * @return dispInt
     */
    public java.math.BigDecimal getDispInt() {
        return this.dispInt;
    }

    /**
     * @param dispSuspAcctNo
     */
    public void setDispSuspAcctNo(String dispSuspAcctNo) {
        this.dispSuspAcctNo = dispSuspAcctNo;
    }

    /**
     * @return dispSuspAcctNo
     */
    public String getDispSuspAcctNo() {
        return this.dispSuspAcctNo;
    }

    /**
     * @param suspNo
     */
    public void setSuspNo(String suspNo) {
        this.suspNo = suspNo;
    }

    /**
     * @return suspNo
     */
    public String getSuspNo() {
        return this.suspNo;
    }

    /**
     * @param dispSuspName
     */
    public void setDispSuspName(String dispSuspName) {
        this.dispSuspName = dispSuspName;
    }

    /**
     * @return dispSuspName
     */
    public String getDispSuspName() {
        return this.dispSuspName;
    }

    /**
     * @param resistAssetAcctNo
     */
    public void setResistAssetAcctNo(String resistAssetAcctNo) {
        this.resistAssetAcctNo = resistAssetAcctNo;
    }

    /**
     * @return resistAssetAcctNo
     */
    public String getResistAssetAcctNo() {
        return this.resistAssetAcctNo;
    }

    /**
     * @param resistAssetName
     */
    public void setResistAssetName(String resistAssetName) {
        this.resistAssetName = resistAssetName;
    }

    /**
     * @return resistAssetName
     */
    public String getResistAssetName() {
        return this.resistAssetName;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}