package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawyerInfo
 * @类描述: pla_lawyer_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 13:49:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PlaLawyerInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 律师编号 **/
	private String lawyerNo;
	
	/** 律师姓名 **/
	private String lawyerName;
	
	/** 律师联系电话 **/
	private String lawyerTelNo;
	
	/** 律师事务所名称 **/
	private String lawOfficeName;
	
	/** 律师状态 **/
	private String lawyerStatus;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param lawyerNo
	 */
	public void setLawyerNo(String lawyerNo) {
		this.lawyerNo = lawyerNo == null ? null : lawyerNo.trim();
	}
	
    /**
     * @return LawyerNo
     */	
	public String getLawyerNo() {
		return this.lawyerNo;
	}
	
	/**
	 * @param lawyerName
	 */
	public void setLawyerName(String lawyerName) {
		this.lawyerName = lawyerName == null ? null : lawyerName.trim();
	}
	
    /**
     * @return LawyerName
     */	
	public String getLawyerName() {
		return this.lawyerName;
	}
	
	/**
	 * @param lawyerTelNo
	 */
	public void setLawyerTelNo(String lawyerTelNo) {
		this.lawyerTelNo = lawyerTelNo == null ? null : lawyerTelNo.trim();
	}
	
    /**
     * @return LawyerTelNo
     */	
	public String getLawyerTelNo() {
		return this.lawyerTelNo;
	}
	
	/**
	 * @param lawOfficeName
	 */
	public void setLawOfficeName(String lawOfficeName) {
		this.lawOfficeName = lawOfficeName == null ? null : lawOfficeName.trim();
	}
	
    /**
     * @return LawOfficeName
     */	
	public String getLawOfficeName() {
		return this.lawOfficeName;
	}
	
	/**
	 * @param lawyerStatus
	 */
	public void setLawyerStatus(String lawyerStatus) {
		this.lawyerStatus = lawyerStatus == null ? null : lawyerStatus.trim();
	}
	
    /**
     * @return LawyerStatus
     */	
	public String getLawyerStatus() {
		return this.lawyerStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}