/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaLawGrtRel;
import cn.com.yusys.yusp.repository.mapper.PlaLawGrtRelMapper;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawGrtRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-05-29 15:16:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaLawGrtRelService {

    @Autowired
    private PlaLawGrtRelMapper plaLawGrtRelMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PlaLawGrtRel selectByPrimaryKey(String plgrSerno) {
        return plaLawGrtRelMapper.selectByPrimaryKey(plgrSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaLawGrtRel> selectAll(QueryModel model) {
        List<PlaLawGrtRel> records = (List<PlaLawGrtRel>) plaLawGrtRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PlaLawGrtRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaLawGrtRel> list = plaLawGrtRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PlaLawGrtRel record) {
        return plaLawGrtRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PlaLawGrtRel record) {
        return plaLawGrtRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PlaLawGrtRel record) {
        return plaLawGrtRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PlaLawGrtRel record) {
        return plaLawGrtRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String plgrSerno) {
        return plaLawGrtRelMapper.deleteByPrimaryKey(plgrSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaLawGrtRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteByAppSerno
     * @方法描述: 根据诉讼主键删除
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */

    public int deleteByAppSerno(String appSerno) {
        return plaLawGrtRelMapper.deleteByAppSerno(appSerno);
    }

    /**
     * @方法名称: deleteByAppSerno
     * @方法描述: 根据诉讼主键和合同编号删除
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */

    public int deleteByCountNo(String appSerno,String contNo) {
        return plaLawGrtRelMapper.deleteByContNo(appSerno,contNo);
    }
}
