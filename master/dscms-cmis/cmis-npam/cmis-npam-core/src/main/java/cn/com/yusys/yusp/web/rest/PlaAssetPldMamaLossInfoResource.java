/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaAssetPldMamaLossInfo;
import cn.com.yusys.yusp.service.PlaAssetPldMamaLossInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldMamaLossInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plaassetpldmamalossinfo")
public class PlaAssetPldMamaLossInfoResource {
    @Autowired
    private PlaAssetPldMamaLossInfoService plaAssetPldMamaLossInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaAssetPldMamaLossInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaAssetPldMamaLossInfo> list = plaAssetPldMamaLossInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaAssetPldMamaLossInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<PlaAssetPldMamaLossInfo>> index(QueryModel queryModel) {
        List<PlaAssetPldMamaLossInfo> list = plaAssetPldMamaLossInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaAssetPldMamaLossInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pappmliSerno}")
    protected ResultDto<PlaAssetPldMamaLossInfo> show(@PathVariable("pappmliSerno") String pappmliSerno) {
        PlaAssetPldMamaLossInfo plaAssetPldMamaLossInfo = plaAssetPldMamaLossInfoService.selectByPrimaryKey(pappmliSerno);
        return new ResultDto<PlaAssetPldMamaLossInfo>(plaAssetPldMamaLossInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<PlaAssetPldMamaLossInfo> create(@RequestBody PlaAssetPldMamaLossInfo plaAssetPldMamaLossInfo) throws URISyntaxException {
        plaAssetPldMamaLossInfoService.insert(plaAssetPldMamaLossInfo);
        return new ResultDto<PlaAssetPldMamaLossInfo>(plaAssetPldMamaLossInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaAssetPldMamaLossInfo plaAssetPldMamaLossInfo) throws URISyntaxException {
        int result = plaAssetPldMamaLossInfoService.update(plaAssetPldMamaLossInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pappmliSerno}")
    protected ResultDto<Integer> delete(@PathVariable("pappmliSerno") String pappmliSerno) {
        int result = plaAssetPldMamaLossInfoService.deleteByPrimaryKey(pappmliSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaAssetPldMamaLossInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据处置申请流水号获取核销信息
     *
     * @author jijian_yx
     * @date 2021/6/10 14:06
     **/
    @ApiOperation("根据处置申请流水号获取核销信息")
    @PostMapping("/querybypapaiserno")
    protected ResultDto<PlaAssetPldMamaLossInfo> queryByPapaiSerno(@RequestBody String papaiSerno) {
        PlaAssetPldMamaLossInfo plaAssetPldMamaLossInfo = plaAssetPldMamaLossInfoService.selectByPapaiSerno(papaiSerno);
        return new ResultDto<PlaAssetPldMamaLossInfo>(plaAssetPldMamaLossInfo);
    }

    /**
     * 保存/更新核销信息
     *
     * @author jijian_yx
     * @date 2021/6/11 9:25
     **/
    @ApiOperation("保存/更新核销信息")
    @PostMapping("/insert")
    protected ResultDto<Map<String,String>> insert(@RequestBody PlaAssetPldMamaLossInfo plaAssetPldMamaLossInfo) {
        Map<String,String> map = plaAssetPldMamaLossInfoService.insertInfo(plaAssetPldMamaLossInfo);
        return new ResultDto<Map<String,String>>(map);
    }
}
