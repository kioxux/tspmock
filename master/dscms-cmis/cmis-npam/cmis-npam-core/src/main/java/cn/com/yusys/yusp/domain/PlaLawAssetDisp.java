/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawAssetDisp
 * @类描述: pla_law_asset_disp数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-07 19:23:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_law_asset_disp")
public class PlaLawAssetDisp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 保全资产处置流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PLAD_SERNO")
    private String pladSerno;

    /**
     * 案件流水号
     **/
    @Column(name = "CASE_SERNO", unique = false, nullable = true, length = 40)
    private String caseSerno;

    /**
     * 资产分类
     **/
    @Column(name = "ASSET_TYPE", unique = false, nullable = true, length = 5)
    private String assetType;

    /**
     * 权属人
     **/
    @Column(name = "AUTHO", unique = false, nullable = true, length = 80)
    private String autho;

    /**
     * 处置财产定价方式
     **/
    @Column(name = "ASSET_PRICE_MODE", unique = false, nullable = true, length = 5)
    private String assetPriceMode;

    /**
     * 评估标的物价
     **/
    @Column(name = "ASS_EVA_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal assEvaAmt;

    /**
     * 处置方式
     **/
    @Column(name = "DISP_TYPE", unique = false, nullable = true, length = 5)
    private String dispType;

    /**
     * 是否成交
     **/
    @Column(name = "IS_DEAL", unique = false, nullable = true, length = 5)
    private String isDeal;

    /**
     * 成交日期
     **/
    @Column(name = "DEAL_DATE", unique = false, nullable = true, length = 10)
    private String dealDate;

    /**
     * 成交价格
     **/
    @Column(name = "DEAL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal dealAmt;

    /**
     * 我行受偿金额
     **/
    @Column(name = "BANK_AOCN", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal bankAocn;

    /**
     * 执行款到我行日期
     **/
    @Column(name = "EXECMN_BACK_DATE", unique = false, nullable = true, length = 10)
    private String execmnBackDate;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaLawAssetDisp() {
        // default implementation ignored
    }

    /**
     * @param pladSerno
     */
    public void setPladSerno(String pladSerno) {
        this.pladSerno = pladSerno;
    }

    /**
     * @return pladSerno
     */
    public String getPladSerno() {
        return this.pladSerno;
    }

    /**
     * @param caseSerno
     */
    public void setCaseSerno(String caseSerno) {
        this.caseSerno = caseSerno;
    }

    /**
     * @return caseSerno
     */
    public String getCaseSerno() {
        return this.caseSerno;
    }

    /**
     * @param assetType
     */
    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    /**
     * @return assetType
     */
    public String getAssetType() {
        return this.assetType;
    }

    /**
     * @param autho
     */
    public void setAutho(String autho) {
        this.autho = autho;
    }

    /**
     * @return autho
     */
    public String getAutho() {
        return this.autho;
    }

    /**
     * @param assetPriceMode
     */
    public void setAssetPriceMode(String assetPriceMode) {
        this.assetPriceMode = assetPriceMode;
    }

    /**
     * @return assetPriceMode
     */
    public String getAssetPriceMode() {
        return this.assetPriceMode;
    }

    /**
     * @param assEvaAmt
     */
    public void setAssEvaAmt(java.math.BigDecimal assEvaAmt) {
        this.assEvaAmt = assEvaAmt;
    }

    /**
     * @return assEvaAmt
     */
    public java.math.BigDecimal getAssEvaAmt() {
        return this.assEvaAmt;
    }

    /**
     * @param dispType
     */
    public void setDispType(String dispType) {
        this.dispType = dispType;
    }

    /**
     * @return dispType
     */
    public String getDispType() {
        return this.dispType;
    }

    /**
     * @param isDeal
     */
    public void setIsDeal(String isDeal) {
        this.isDeal = isDeal;
    }

    /**
     * @return isDeal
     */
    public String getIsDeal() {
        return this.isDeal;
    }

    /**
     * @param dealDate
     */
    public void setDealDate(String dealDate) {
        this.dealDate = dealDate;
    }

    /**
     * @return dealDate
     */
    public String getDealDate() {
        return this.dealDate;
    }

    /**
     * @param dealAmt
     */
    public void setDealAmt(java.math.BigDecimal dealAmt) {
        this.dealAmt = dealAmt;
    }

    /**
     * @return dealAmt
     */
    public java.math.BigDecimal getDealAmt() {
        return this.dealAmt;
    }

    /**
     * @param bankAocn
     */
    public void setBankAocn(java.math.BigDecimal bankAocn) {
        this.bankAocn = bankAocn;
    }

    /**
     * @return bankAocn
     */
    public java.math.BigDecimal getBankAocn() {
        return this.bankAocn;
    }

    /**
     * @param execmnBackDate
     */
    public void setExecmnBackDate(String execmnBackDate) {
        this.execmnBackDate = execmnBackDate;
    }

    /**
     * @return execmnBackDate
     */
    public String getExecmnBackDate() {
        return this.execmnBackDate;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}