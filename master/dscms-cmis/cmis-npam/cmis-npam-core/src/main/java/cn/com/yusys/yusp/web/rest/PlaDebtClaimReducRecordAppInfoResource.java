/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.PlaDebtClaimReducRecordBillRel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaDebtClaimReducRecordAppInfo;
import cn.com.yusys.yusp.service.PlaDebtClaimReducRecordAppInfoService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaDebtClaimReducRecordAppInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-08 19:44:40
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pladebtclaimreducrecordappinfo")
public class PlaDebtClaimReducRecordAppInfoResource {
    @Autowired
    private PlaDebtClaimReducRecordAppInfoService plaDebtClaimReducRecordAppInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaDebtClaimReducRecordAppInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaDebtClaimReducRecordAppInfo> list = plaDebtClaimReducRecordAppInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaDebtClaimReducRecordAppInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaDebtClaimReducRecordAppInfo>> index(QueryModel queryModel) {
        List<PlaDebtClaimReducRecordAppInfo> list = plaDebtClaimReducRecordAppInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaDebtClaimReducRecordAppInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryAll")
    protected ResultDto<List<PlaDebtClaimReducRecordAppInfo>> queryAll(@RequestBody QueryModel queryModel) {
        List<PlaDebtClaimReducRecordAppInfo> list = plaDebtClaimReducRecordAppInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaDebtClaimReducRecordAppInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pdcrraiSerno}")
    protected ResultDto<PlaDebtClaimReducRecordAppInfo> show(@PathVariable("pdcrraiSerno") String pdcrraiSerno) {
        PlaDebtClaimReducRecordAppInfo plaDebtClaimReducRecordAppInfo = plaDebtClaimReducRecordAppInfoService.selectByPrimaryKey(pdcrraiSerno);
        return new ResultDto<PlaDebtClaimReducRecordAppInfo>(plaDebtClaimReducRecordAppInfo);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     *
     */
    @PostMapping("/showByPdcrraiSerno")
    protected ResultDto<PlaDebtClaimReducRecordAppInfo> showByPdcrraiSerno(@RequestBody PlaDebtClaimReducRecordAppInfo plaDebtClaimReducRecordAppInfo) {
        PlaDebtClaimReducRecordAppInfo plaDebtClaimReducRecordAppInfo1 = plaDebtClaimReducRecordAppInfoService.showByPdcrraiSerno(plaDebtClaimReducRecordAppInfo);
        return new ResultDto<PlaDebtClaimReducRecordAppInfo>(plaDebtClaimReducRecordAppInfo1);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaDebtClaimReducRecordAppInfo> create(@RequestBody PlaDebtClaimReducRecordAppInfo plaDebtClaimReducRecordAppInfo) throws URISyntaxException {
        plaDebtClaimReducRecordAppInfoService.insert(plaDebtClaimReducRecordAppInfo);
        return new ResultDto<PlaDebtClaimReducRecordAppInfo>(plaDebtClaimReducRecordAppInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaDebtClaimReducRecordAppInfo plaDebtClaimReducRecordAppInfo) throws URISyntaxException {
        int result = plaDebtClaimReducRecordAppInfoService.update(plaDebtClaimReducRecordAppInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pdcrraiSerno}")
    protected ResultDto<Integer> delete(@PathVariable("pdcrraiSerno") String pdcrraiSerno) {
        int result = plaDebtClaimReducRecordAppInfoService.deleteByPrimaryKey(pdcrraiSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaDebtClaimReducRecordAppInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * 债权减免发送核心记账
     *
     * @author 刘权
     **/
    @ApiOperation("债权减免发送核心记账")
    @PostMapping("/sendtohxjz")
    protected ResultDto<String> sendToHXJZ(@RequestBody PlaDebtClaimReducRecordAppInfo appInfo) {
        return plaDebtClaimReducRecordAppInfoService.sendToHXJZ(appInfo);
    }

    /**
     * 债权减免补发
     *
     * @author 刘权
     **/
    @ApiOperation("债权减免发送贷款本息减免")
    @PostMapping("/sendtojzbf")
    protected ResultDto<String> sendToJZBF(@RequestBody PlaDebtClaimReducRecordBillRel plaDebtClaimReducRecordBillRel) {
        return plaDebtClaimReducRecordAppInfoService.sendToJZBF(plaDebtClaimReducRecordBillRel);
    }
}
