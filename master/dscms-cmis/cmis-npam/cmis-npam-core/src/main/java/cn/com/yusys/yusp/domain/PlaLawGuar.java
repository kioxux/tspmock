/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawGuar
 * @类描述: pla_law_guar数据实体类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-07 22:13:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_law_guar")
public class PlaLawGuar extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PLG_SERNO")
    private String plgSerno;

    /**
     * 案件编号
     **/
    @Column(name = "CASE_NO")
    private String caseNo;

    /**
     * 案件流水号
     **/
    @Column(name = "CASE_SERNO", unique = false, nullable = true, length = 40)
    private String caseSerno;

    /**
     * 受理日期
     **/
    @Column(name = "ACCEPT_DATE", unique = false, nullable = true, length = 10)
    private String acceptDate;

    /**
     * 受理法院
     **/
    @Column(name = "ACCEPT_COURT", unique = false, nullable = true, length = 80)
    private String acceptCourt;

    /**
     * 申请金额
     **/
    @Column(name = "APP_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal appAmt;

    /**
     * 裁定金额
     **/
    @Column(name = "AWARD_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal awardAmt;

    /**
     * 文书生效日期
     **/
    @Column(name = "DCMNTS_INURE_DATE", unique = false, nullable = true, length = 10)
    private String dcmntsInureDate;

    /**
     * 文书落款日期
     **/
    @Column(name = "DCMNTS_INSCRIBE_DATE", unique = false, nullable = true, length = 10)
    private String dcmntsInscribeDate;

    /**
     * 案件进程状态
     **/
    @Column(name = "CASE_PROCESS_STATUS", unique = false, nullable = true, length = 5)
    private String caseProcessStatus;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaLawGuar() {
        // default implementation ignored
    }

    public String getPlgSerno() {
        return plgSerno;
    }

    public void setPlgSerno(String plgSerno) {
        this.plgSerno = plgSerno;
    }

    /**
     * @param caseNo
     */
    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    /**
     * @return caseNo
     */
    public String getCaseNo() {
        return this.caseNo;
    }

    /**
     * @param caseSerno
     */
    public void setCaseSerno(String caseSerno) {
        this.caseSerno = caseSerno;
    }

    /**
     * @return caseSerno
     */
    public String getCaseSerno() {
        return this.caseSerno;
    }

    /**
     * @param acceptDate
     */
    public void setAcceptDate(String acceptDate) {
        this.acceptDate = acceptDate;
    }

    /**
     * @return acceptDate
     */
    public String getAcceptDate() {
        return this.acceptDate;
    }

    /**
     * @param acceptCourt
     */
    public void setAcceptCourt(String acceptCourt) {
        this.acceptCourt = acceptCourt;
    }

    /**
     * @return acceptCourt
     */
    public String getAcceptCourt() {
        return this.acceptCourt;
    }

    /**
     * @param appAmt
     */
    public void setAppAmt(java.math.BigDecimal appAmt) {
        this.appAmt = appAmt;
    }

    /**
     * @return appAmt
     */
    public java.math.BigDecimal getAppAmt() {
        return this.appAmt;
    }

    /**
     * @param awardAmt
     */
    public void setAwardAmt(java.math.BigDecimal awardAmt) {
        this.awardAmt = awardAmt;
    }

    /**
     * @return awardAmt
     */
    public java.math.BigDecimal getAwardAmt() {
        return this.awardAmt;
    }

    /**
     * @param dcmntsInureDate
     */
    public void setDcmntsInureDate(String dcmntsInureDate) {
        this.dcmntsInureDate = dcmntsInureDate;
    }

    /**
     * @return dcmntsInureDate
     */
    public String getDcmntsInureDate() {
        return this.dcmntsInureDate;
    }

    /**
     * @param dcmntsInscribeDate
     */
    public void setDcmntsInscribeDate(String dcmntsInscribeDate) {
        this.dcmntsInscribeDate = dcmntsInscribeDate;
    }

    /**
     * @return dcmntsInscribeDate
     */
    public String getDcmntsInscribeDate() {
        return this.dcmntsInscribeDate;
    }

    /**
     * @param caseProcessStatus
     */
    public void setCaseProcessStatus(String caseProcessStatus) {
        this.caseProcessStatus = caseProcessStatus;
    }

    /**
     * @return caseProcessStatus
     */
    public String getCaseProcessStatus() {
        return this.caseProcessStatus;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}