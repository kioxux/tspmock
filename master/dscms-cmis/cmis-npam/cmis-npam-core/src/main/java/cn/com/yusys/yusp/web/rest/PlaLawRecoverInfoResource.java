/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaLawRecoverInfo;
import cn.com.yusys.yusp.service.PlaLawRecoverInfoService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawRecoverInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-07 19:31:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plalawrecoverinfo")
public class PlaLawRecoverInfoResource {
    @Autowired
    private PlaLawRecoverInfoService plaLawRecoverInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaLawRecoverInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaLawRecoverInfo> list = plaLawRecoverInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaLawRecoverInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaLawRecoverInfo>> index(QueryModel queryModel) {
        List<PlaLawRecoverInfo> list = plaLawRecoverInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawRecoverInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{plriSerno}")
    protected ResultDto<PlaLawRecoverInfo> show(@PathVariable("plriSerno") String plriSerno) {
        PlaLawRecoverInfo plaLawRecoverInfo = plaLawRecoverInfoService.selectByPrimaryKey(plriSerno);
        return new ResultDto<PlaLawRecoverInfo>(plaLawRecoverInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaLawRecoverInfo> create(@RequestBody PlaLawRecoverInfo plaLawRecoverInfo) throws URISyntaxException {
        plaLawRecoverInfoService.insert(plaLawRecoverInfo);
        return new ResultDto<PlaLawRecoverInfo>(plaLawRecoverInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaLawRecoverInfo plaLawRecoverInfo) throws URISyntaxException {
        int result = plaLawRecoverInfoService.update(plaLawRecoverInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{plriSerno}")
    protected ResultDto<Integer> delete(@PathVariable("plriSerno") String plriSerno) {
        int result = plaLawRecoverInfoService.deleteByPrimaryKey(plriSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaLawRecoverInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryPlaLawRecoverInfo")
    protected ResultDto<List<PlaLawRecoverInfo>> queryPlaLawRecoverInfo(@RequestBody QueryModel queryModel) {
        List<PlaLawRecoverInfo> list = plaLawRecoverInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawRecoverInfo>>(list);
    }
}
