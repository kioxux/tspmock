/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldSellInfo
 * @类描述: pla_asset_pld_sell_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_asset_pld_sell_info")
public class PlaAssetPldSellInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 出售信息流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PAPSI_SERNO")
    private String papsiSerno;

    /**
     * 抵债资产处置申请流水号
     **/
    @Column(name = "PAPAI_SERNO", unique = false, nullable = true, length = 40)
    private String papaiSerno;

    /**
     * 抵债资产编号
     **/
    @Column(name = "PLDIMN_NO", unique = false, nullable = true, length = 40)
    private String pldimnNo;

    /**
     * 抵债资产名称
     **/
    @Column(name = "PLDIMN_NAME", unique = false, nullable = true, length = 80)
    private String pldimnName;

    /**
     * 抵债资产类型
     **/
    @Column(name = "PLDIMN_TYPE", unique = false, nullable = true, length = 5)
    private String pldimnType;

    /**
     * 数量
     **/
    @Column(name = "QNT", unique = false, nullable = true, length = 5)
    private String qnt;

    /**
     * 抵入金额(元)
     **/
    @Column(name = "TO_ENTER_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal toEnterAmt;

    /**
     * 评估金额(元)
     **/
    @Column(name = "EVAL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal evalAmt;

    /**
     * 出售价格(元)
     **/
    @Column(name = "SELL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal sellAmt;

    /**
     * 出售日期
     **/
    @Column(name = "SELL_DATE", unique = false, nullable = true, length = 10)
    private String sellDate;

    /**
     * 买受人名称
     **/
    @Column(name = "BUYER_NAME", unique = false, nullable = true, length = 80)
    private String buyerName;

    /**
     * 备注
     **/
    @Column(name = "MEMO", unique = false, nullable = true, length = 2000)
    private String memo;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaAssetPldSellInfo() {
        // default implementation ignored
    }

    /**
     * @param papsiSerno
     */
    public void setPapsiSerno(String papsiSerno) {
        this.papsiSerno = papsiSerno;
    }

    /**
     * @return papsiSerno
     */
    public String getPapsiSerno() {
        return this.papsiSerno;
    }

    /**
     * @param papaiSerno
     */
    public void setPapaiSerno(String papaiSerno) {
        this.papaiSerno = papaiSerno;
    }

    /**
     * @return papaiSerno
     */
    public String getPapaiSerno() {
        return this.papaiSerno;
    }

    /**
     * @param pldimnNo
     */
    public void setPldimnNo(String pldimnNo) {
        this.pldimnNo = pldimnNo;
    }

    /**
     * @return pldimnNo
     */
    public String getPldimnNo() {
        return this.pldimnNo;
    }

    /**
     * @param pldimnName
     */
    public void setPldimnName(String pldimnName) {
        this.pldimnName = pldimnName;
    }

    /**
     * @return pldimnName
     */
    public String getPldimnName() {
        return this.pldimnName;
    }

    /**
     * @param pldimnType
     */
    public void setPldimnType(String pldimnType) {
        this.pldimnType = pldimnType;
    }

    /**
     * @return pldimnType
     */
    public String getPldimnType() {
        return this.pldimnType;
    }

    /**
     * @param toEnterAmt
     */
    public void setToEnterAmt(java.math.BigDecimal toEnterAmt) {
        this.toEnterAmt = toEnterAmt;
    }

    /**
     * @return toEnterAmt
     */
    public java.math.BigDecimal getToEnterAmt() {
        return this.toEnterAmt;
    }

    /**
     * @param evalAmt
     */
    public void setEvalAmt(java.math.BigDecimal evalAmt) {
        this.evalAmt = evalAmt;
    }

    /**
     * @return evalAmt
     */
    public java.math.BigDecimal getEvalAmt() {
        return this.evalAmt;
    }

    /**
     * @param sellAmt
     */
    public void setSellAmt(java.math.BigDecimal sellAmt) {
        this.sellAmt = sellAmt;
    }

    /**
     * @return sellAmt
     */
    public java.math.BigDecimal getSellAmt() {
        return this.sellAmt;
    }

    /**
     * @param sellDate
     */
    public void setSellDate(String sellDate) {
        this.sellDate = sellDate;
    }

    /**
     * @return sellDate
     */
    public String getSellDate() {
        return this.sellDate;
    }

    /**
     * @param buyerName
     */
    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    /**
     * @return buyerName
     */
    public String getBuyerName() {
        return this.buyerName;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    public String getQnt() {
        return qnt;
    }

    public void setQnt(String qnt) {
        this.qnt = qnt;
    }
}