/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaLawPerformCaseInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawPerformCaseInfoMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-07 19:08:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface PlaLawPerformCaseInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    PlaLawPerformCaseInfo selectByPrimaryKey(@Param("plpciSerno") String plpciSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PlaLawPerformCaseInfo> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insert(PlaLawPerformCaseInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insertSelective(PlaLawPerformCaseInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPrimaryKey(PlaLawPerformCaseInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPrimaryKeySelective(PlaLawPerformCaseInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    int deleteByPrimaryKey(@Param("plpciSerno") String plpciSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectByPlaLawPerformCaseInfo
     * @方法描述: 根据律师姓名查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    int selectByPlaLawPerformCaseInfo(@Param("lawyerName") String lawyerName);
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据案件编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    PlaLawPerformCaseInfo selectByCaseSerno(@Param("caseSerno") String caseSerno);
}