/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaCardWriteoffApp
 * @类描述: pla_card_writeoff_app数据实体类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-11 13:51:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_card_writeoff_app")
public class PlaCardWriteoffApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 业务流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PCWA_SERNO")
    private String pcwaSerno;

    /**
     * 核销总户数
     **/
    @Column(name = "TOTAL_WRITEOFF_CUS", unique = false, nullable = true, length = 20)
    private String totalWriteoffCus;

    /**
     * 透支余额
     **/
    @Column(name = "OVERDRAFT_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal overdraftAmt;

    /**
     * 利息合计
     **/
    @Column(name = "TOTAL_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalInt;

    /**
     * 费用合计
     **/
    @Column(name = "TOTAL_COST_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalCostAmt;

    /**
     * 核销本金
     **/
    @Column(name = "WRITEOFF_CAP", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal writeoffCap;

    /**
     * 核销利息
     **/
    @Column(name = "WRITEOFF_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal writeoffInt;

    /**
     * 核销费用
     **/
    @Column(name = "WRITEOFF_COST", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal writeoffCost;

    /**
     * 核销总金额
     **/
    @Column(name = "TOTAL_WRITEOFF_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalWriteoffAmt;

    /**
     * 追索方式
     **/
    @Column(name = "RECOURSE_MODE", unique = false, nullable = true, length = 5)
    private String recourseMode;

    /**
     * 清单类型
     **/
    @Column(name = "DATABOOK_TYPE", unique = false, nullable = true, length = 5)
    private String databookType;

    /**
     * 核销案存资产类型
     **/
    @Column(name = "WRITEOFF_ASSET_TYPE", unique = false, nullable = true, length = 5)
    private String writeoffAssetType;

    /**
     * 担保方式
     **/
    @Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
    private String guarMode;

    /**
     * 是否保留追诉权
     **/
    @Column(name = "IS_RESERVA_PROSECUTE", unique = false, nullable = true, length = 5)
    private String isReservaProsecute;

    /**
     * 持卡人和担保人基本情况
     **/
    @Column(name = "CARDHOLDER_BASE_CASE", unique = false, nullable = true, length = 2000)
    private String cardholderBaseCase;

    /**
     * 持卡人和担保人债权追偿情况
     **/
    @Column(name = "CARDHOLDER_DOUBTFUL_CASE", unique = false, nullable = true, length = 2000)
    private String cardholderDoubtfulCase;

    /**
     * 内部证件清单
     **/
    @Column(name = "CERT_INVENTORY", unique = false, nullable = true, length = 2000)
    private String certInventory;

    /**
     * 呆账形成原因
     **/
    @Column(name = "BECOME_BAD_RESN", unique = false, nullable = true, length = 2000)
    private String becomeBadResn;

    /**
     * 责任认定情况
     **/
    @Column(name = "DUTY_RESN", unique = false, nullable = true, length = 2000)
    private String dutyResn;

    /**
     * 申请核销理由
     **/
    @Column(name = "WRITEOFF_REASON", unique = false, nullable = true, length = 2000)
    private String writeoffReason;

    /**
     * 审批状态
     **/
    @Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
    private String approveStatus;

    /**
     * 备注
     **/
    @Column(name = "MEMO", unique = false, nullable = true, length = 2000)
    private String memo;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaCardWriteoffApp() {
        // default implementation ignored
    }

    /**
     * @param pcwaSerno
     */
    public void setPcwaSerno(String pcwaSerno) {
        this.pcwaSerno = pcwaSerno;
    }

    /**
     * @return pcwaSerno
     */
    public String getPcwaSerno() {
        return this.pcwaSerno;
    }

    /**
     * @param totalWriteoffCus
     */
    public void setTotalWriteoffCus(String totalWriteoffCus) {
        this.totalWriteoffCus = totalWriteoffCus;
    }

    /**
     * @return totalWriteoffCus
     */
    public String getTotalWriteoffCus() {
        return this.totalWriteoffCus;
    }

    /**
     * @param overdraftAmt
     */
    public void setOverdraftAmt(java.math.BigDecimal overdraftAmt) {
        this.overdraftAmt = overdraftAmt;
    }

    /**
     * @return overdraftAmt
     */
    public java.math.BigDecimal getOverdraftAmt() {
        return this.overdraftAmt;
    }

    /**
     * @param totalInt
     */
    public void setTotalInt(java.math.BigDecimal totalInt) {
        this.totalInt = totalInt;
    }

    /**
     * @return totalInt
     */
    public java.math.BigDecimal getTotalInt() {
        return this.totalInt;
    }

    /**
     * @param totalCostAmt
     */
    public void setTotalCostAmt(java.math.BigDecimal totalCostAmt) {
        this.totalCostAmt = totalCostAmt;
    }

    /**
     * @return totalCostAmt
     */
    public java.math.BigDecimal getTotalCostAmt() {
        return this.totalCostAmt;
    }

    /**
     * @param writeoffCap
     */
    public void setWriteoffCap(java.math.BigDecimal writeoffCap) {
        this.writeoffCap = writeoffCap;
    }

    /**
     * @return writeoffCap
     */
    public java.math.BigDecimal getWriteoffCap() {
        return this.writeoffCap;
    }

    /**
     * @param writeoffInt
     */
    public void setWriteoffInt(java.math.BigDecimal writeoffInt) {
        this.writeoffInt = writeoffInt;
    }

    /**
     * @return writeoffInt
     */
    public java.math.BigDecimal getWriteoffInt() {
        return this.writeoffInt;
    }

    /**
     * @param writeoffCost
     */
    public void setWriteoffCost(java.math.BigDecimal writeoffCost) {
        this.writeoffCost = writeoffCost;
    }

    /**
     * @return writeoffCost
     */
    public java.math.BigDecimal getWriteoffCost() {
        return this.writeoffCost;
    }

    /**
     * @param totalWriteoffAmt
     */
    public void setTotalWriteoffAmt(java.math.BigDecimal totalWriteoffAmt) {
        this.totalWriteoffAmt = totalWriteoffAmt;
    }

    /**
     * @return totalWriteoffAmt
     */
    public java.math.BigDecimal getTotalWriteoffAmt() {
        return this.totalWriteoffAmt;
    }

    /**
     * @param recourseMode
     */
    public void setRecourseMode(String recourseMode) {
        this.recourseMode = recourseMode;
    }

    /**
     * @return recourseMode
     */
    public String getRecourseMode() {
        return this.recourseMode;
    }

    /**
     * @param databookType
     */
    public void setDatabookType(String databookType) {
        this.databookType = databookType;
    }

    /**
     * @return databookType
     */
    public String getDatabookType() {
        return this.databookType;
    }

    /**
     * @param writeoffAssetType
     */
    public void setWriteoffAssetType(String writeoffAssetType) {
        this.writeoffAssetType = writeoffAssetType;
    }

    /**
     * @return writeoffAssetType
     */
    public String getWriteoffAssetType() {
        return this.writeoffAssetType;
    }

    /**
     * @param guarMode
     */
    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    /**
     * @return guarMode
     */
    public String getGuarMode() {
        return this.guarMode;
    }

    /**
     * @param isReservaProsecute
     */
    public void setIsReservaProsecute(String isReservaProsecute) {
        this.isReservaProsecute = isReservaProsecute;
    }

    /**
     * @return isReservaProsecute
     */
    public String getIsReservaProsecute() {
        return this.isReservaProsecute;
    }

    /**
     * @param cardholderBaseCase
     */
    public void setCardholderBaseCase(String cardholderBaseCase) {
        this.cardholderBaseCase = cardholderBaseCase;
    }

    /**
     * @return cardholderBaseCase
     */
    public String getCardholderBaseCase() {
        return this.cardholderBaseCase;
    }

    /**
     * @param cardholderDoubtfulCase
     */
    public void setCardholderDoubtfulCase(String cardholderDoubtfulCase) {
        this.cardholderDoubtfulCase = cardholderDoubtfulCase;
    }

    /**
     * @return cardholderDoubtfulCase
     */
    public String getCardholderDoubtfulCase() {
        return this.cardholderDoubtfulCase;
    }

    /**
     * @param certInventory
     */
    public void setCertInventory(String certInventory) {
        this.certInventory = certInventory;
    }

    /**
     * @return certInventory
     */
    public String getCertInventory() {
        return this.certInventory;
    }

    /**
     * @param becomeBadResn
     */
    public void setBecomeBadResn(String becomeBadResn) {
        this.becomeBadResn = becomeBadResn;
    }

    /**
     * @return becomeBadResn
     */
    public String getBecomeBadResn() {
        return this.becomeBadResn;
    }

    /**
     * @param dutyResn
     */
    public void setDutyResn(String dutyResn) {
        this.dutyResn = dutyResn;
    }

    /**
     * @return dutyResn
     */
    public String getDutyResn() {
        return this.dutyResn;
    }

    /**
     * @param writeoffReason
     */
    public void setWriteoffReason(String writeoffReason) {
        this.writeoffReason = writeoffReason;
    }

    /**
     * @return writeoffReason
     */
    public String getWriteoffReason() {
        return this.writeoffReason;
    }

    /**
     * @param approveStatus
     */
    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    /**
     * @return approveStatus
     */
    public String getApproveStatus() {
        return this.approveStatus;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}