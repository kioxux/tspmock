/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.DefaultDataHandle;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.user.UserIdentity;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.PlaBadDebtWriteoffBillRel;
import cn.com.yusys.yusp.domain.PlaCardWriteoffApp;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.vo.PlaBadDebtWriteoffBillVo;
import cn.com.yusys.yusp.vo.PlaCardInfoRelVo;
import cn.com.yusys.yusp.web.rest.PlaPlanDetailResource;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaCardInfoRel;
import cn.com.yusys.yusp.repository.mapper.PlaCardInfoRelMapper;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaCardInfoRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-06-11 13:51:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaCardInfoRelService {
    private static final Logger log = LoggerFactory.getLogger(PlaPlanDetailResource.class);
    @Autowired
    private PlaCardInfoRelMapper plaCardInfoRelMapper;
    @Autowired
    private PlaCardWriteoffAppService plaCardWriteoffAppService;
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;
    @Autowired
    private CommonService commonService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PlaCardInfoRel selectByPrimaryKey(String pbdwabSerno) {
        return plaCardInfoRelMapper.selectByPrimaryKey(pbdwabSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaCardInfoRel> selectAll(QueryModel model) {
        List<PlaCardInfoRel> records = (List<PlaCardInfoRel>) plaCardInfoRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PlaCardInfoRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaCardInfoRel> list = plaCardInfoRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PlaCardInfoRel record) {
        return plaCardInfoRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PlaCardInfoRel record) {
        return plaCardInfoRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PlaCardInfoRel record) {
        return plaCardInfoRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PlaCardInfoRel record) {
        return plaCardInfoRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pbdwabSerno) {
        return plaCardInfoRelMapper.deleteByPrimaryKey(pbdwabSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaCardInfoRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: asyncImportplanDetail
     * @方法描述: 导入
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    public String asyncImportplanDetail(String fileId, Map<String, String> paramsMap) {
        String serno = "";
        if (paramsMap.containsKey("pcwaSerno")) {
            serno = paramsMap.get("pcwaSerno");
        }
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = null;
        try {
            tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
            FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
        } catch (IOException e) {
            log.error(EcnEnum.ECN060004.value, e);
            throw BizException.error(null, EcnEnum.ECN060004.key, EcnEnum.ECN060004.value);
        }
        // 将文件内容导入数据库，StudentScore为导入数据的类
        String finalSerno = serno;
        ExcelUtils.syncImport(PlaCardInfoRelVo.class, tempFile, new DefaultDataHandle(), ExcelUtils.batchConsumer(dataList -> {
            try {
                return insertPlaCardInfoRel(dataList, finalSerno);
            } catch (Exception e) {
                throw BizException.error(null, EcnEnum.ECN069999.key, e.getMessage());
            }
        }), true);
        return EcnEnum.ECN060000.value;
    }
    /**
     * @方法名称: insertPlanDetail
     * @方法描述: 导入
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertPlaCardInfoRel(List insertPlaCardInfoRelList, String serno) throws Exception {
        int count = 0;
        // 证件号码
        String certCode ="";
        List<PlaCardInfoRel> plaCardInfoRelList = (List<PlaCardInfoRel>) BeanUtils.beansCopy(insertPlaCardInfoRelList, PlaCardInfoRel.class);
        for (PlaCardInfoRel plaCardInfoRel : plaCardInfoRelList) {
            // 校验身份证号码是否合法
            certCode = plaCardInfoRel.getCertCode();
            if(certCode ==null || certCode.length()<18){
                throw BizException.error(null, EcnEnum.ECN060018.key,EcnEnum.ECN060018.value+certCode);
            }
            //判断核销总金额必须小于等于5万
            //取到核销总金额
            BigDecimal totalWriteoffAmt = plaCardInfoRel.getTotalWriteoffAmt();
            if (totalWriteoffAmt.longValue() > 50000){
                throw BizException.error(null, EcnEnum.ECN060021.key,EcnEnum.ECN060021.value+plaCardInfoRel.getCardNo());
            }
            //取到逾期本金总金额
            BigDecimal writeoffCap = plaCardInfoRel.getWriteoffCap();
            //取到核销利息
            BigDecimal writeoffInt = plaCardInfoRel.getWriteoffInt();
            //取到核销费用
            BigDecimal writeoffCost = plaCardInfoRel.getWriteoffCost();
            if (writeoffCap.add(writeoffInt).add(writeoffCost).compareTo(totalWriteoffAmt) != 0){
                throw BizException.error(null, EcnEnum.ECN060022.key,EcnEnum.ECN060022.value+plaCardInfoRel.getCardNo());
            }

            QueryModel query = new QueryModel() ;
            query.addCondition("accno", plaCardInfoRel.getAccno());
            query.addCondition("cusName", plaCardInfoRel.getCusName());
            List<PlaCardInfoRel> plaCardInfoRels = plaCardInfoRelMapper.selectByModel(query);
            //数据库中已存在，更新信息
            if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(plaCardInfoRels)){
                PlaCardInfoRel plaCardInfoRelsInfo = plaCardInfoRels.get(0);
                throw BizException.error(null, EcnEnum.ECN060016.key,EcnEnum.ECN060016.value+plaCardInfoRelsInfo.getCardNo());
            }
            //数据库中不存在，新增信息
            else{
                //生成主键
                Map paramMap= new HashMap<>() ;
                String pbdwabSerno = sequenceTemplateClient.getSequenceTemplate("TRADE_ID_SEQ", paramMap);
                plaCardInfoRel.setPbdwabSerno(pbdwabSerno);
                // 申请人
                String inputId = "";
                // 申请机构
                String inputBrId = "";
                // 申请时间
                String openDay = stringRedisTemplate.opsForValue().get("openDay");
                // 创建时间
                Date createTime = DateUtils.getCurrTimestamp();
                // 获取用户信息
                User userInfo = SessionUtils.getUserInformation();
                if(userInfo != null){
                    // 申请人
                    inputId = userInfo.getLoginCode();
                    // 申请机构
                    inputBrId = userInfo.getOrg().getCode();
                }
                plaCardInfoRel.setPcwaSerno(serno);
                plaCardInfoRel.setInputId(inputId);
                plaCardInfoRel.setInputBrId(inputBrId);
                plaCardInfoRel.setInputDate(openDay);
                plaCardInfoRel.setCreateTime(createTime);
                plaCardInfoRel.setUpdId(inputId);
                plaCardInfoRel.setUpdBrId(inputBrId);
                plaCardInfoRel.setUpdDate(openDay);
                plaCardInfoRel.setUpdateTime(createTime);
                count = insertSelective(plaCardInfoRel);
            }
        }
        if(count>0){
            String totalWriteoffCus = "0";
            // 透支余额
            BigDecimal overdraftAmt = new BigDecimal("0.00");
            // 利息合计
            BigDecimal totalInt = new BigDecimal("0.00");
            // 费用合计
            BigDecimal totalCostAmt = new BigDecimal("0.00");
            // 核销总金额
            BigDecimal totalWriteoffAmt = new BigDecimal("0.00");
            // 核销本金
            BigDecimal writeoffCap = new BigDecimal("0.00");
            // 核销利息
            BigDecimal writeoffInt = new BigDecimal("0.00");
            // 核销费用
            BigDecimal writeoffCost = new BigDecimal("0.00");
            PlaCardWriteoffApp plaCardWriteoffApp =new PlaCardWriteoffApp();
            HashMap<String,Object> countInfo = plaCardInfoRelMapper.countRelInfo(serno);
            if(countInfo.containsKey("totalWriteoffCus")){
                totalWriteoffCus= countInfo.get("totalWriteoffCus").toString();
            }
            if(countInfo.containsKey("overdraftAmt")){
                overdraftAmt= new BigDecimal(countInfo.get("overdraftAmt").toString());
            }
            if(countInfo.containsKey("totalCostAmt")){
                totalCostAmt= new BigDecimal(countInfo.get("totalCostAmt").toString());
            }
            if(countInfo.containsKey("totalInt")){
                totalInt= new BigDecimal(countInfo.get("totalInt").toString());
            }
            if(countInfo.containsKey("totalWriteoffAmt")){
                totalWriteoffAmt=new BigDecimal(countInfo.get("totalWriteoffAmt").toString());
            }
            if(countInfo.containsKey("writeoffCap")){
                writeoffCap=new BigDecimal(countInfo.get("writeoffCap").toString());
            }
            if(countInfo.containsKey("writeoffInt")){
                writeoffInt=new BigDecimal(countInfo.get("writeoffInt").toString());
            }
            if(countInfo.containsKey("writeoffCost")){
                writeoffCost=new BigDecimal(countInfo.get("writeoffCost").toString());
            }
            plaCardWriteoffApp.setOverdraftAmt(overdraftAmt);
            plaCardWriteoffApp.setTotalWriteoffCus(totalWriteoffCus);
            plaCardWriteoffApp.setTotalInt(totalInt);
            plaCardWriteoffApp.setTotalWriteoffAmt(totalWriteoffAmt);
            plaCardWriteoffApp.setTotalCostAmt(totalCostAmt);
            plaCardWriteoffApp.setWriteoffCap(writeoffCap);
            plaCardWriteoffApp.setWriteoffInt(writeoffInt);
            plaCardWriteoffApp.setWriteoffCost(writeoffCost);
            plaCardWriteoffApp.setPcwaSerno(serno);
            if(null !=plaCardWriteoffAppService.selectByPrimaryKey(serno)){
                plaCardWriteoffAppService.updateSelective(plaCardWriteoffApp);
            }else{
                plaCardWriteoffAppService.save(plaCardWriteoffApp);
            }
        }
        return count;
    }
    /**
     * 模板
     * @return 导出进度信息
     */
    public ProgressDto asyncExportPlanDetail(QueryModel model) {
        boolean flag = false;


        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {
                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(PlaCardInfoRelVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 异步下载信用卡核销台账列表
     * @return 导出进度信息
     */
    public ProgressDto asyncExportPlaCardInfoRel(QueryModel model) {
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel)object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            model.addCondition("approveStatus", "997");
            String apiUrl = "/api/placardinforel/exportPlaCardInfoRel";
            String dataAuth = commonService.setDataAuthority(apiUrl);
            if(StringUtils.nonEmpty(dataAuth)){
                queryModeTemp.setDataAuth(dataAuth);
            }
            return selectByPlaCardInfoRel(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(PlaCardInfoRelVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    public List<PlaCardInfoRelVo> selectByPlaCardInfoRel(QueryModel model) {
        List<PlaCardInfoRelVo> plaCardInfoRelVoList = new ArrayList<>();
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaCardInfoRel> list = plaCardInfoRelMapper.selectByModel(model);
        PageHelper.clearPage();

        for (PlaCardInfoRel plaCardInfoRel:list){
            PlaCardInfoRelVo plaCardInfoRelVo = new PlaCardInfoRelVo();
            String inputIdName = OcaTranslatorUtils.getUserName(plaCardInfoRel.getInputId());
            org.springframework.beans.BeanUtils.copyProperties(plaCardInfoRel, plaCardInfoRelVo);
            plaCardInfoRelVo.setInputIdName(inputIdName);
            plaCardInfoRelVoList.add(plaCardInfoRelVo);
        }
        return plaCardInfoRelVoList;
    }
}
