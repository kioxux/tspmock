package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaUncretBadAssetAccList
 * @类描述: pla_uncret_bad_asset_acc_list数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 15:56:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PlaUncretBadAssetAccListDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 关联表流水号 **/
	private String pubaalSerno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 交易编号 **/
	private String tranId;
	
	/** 交易金额 **/
	private java.math.BigDecimal tranAmt;
	
	/** 币种 **/
	private String curType;
	
	/** 投资资产名称 **/
	private String investAssetName;
	
	/** 交易品种名称 **/
	private String tranBizName;
	
	/** 交易成交日期 **/
	private String tranDealDate;
	
	/** 交易到期日期 **/
	private String tranEndDate;
	
	/** 处置方式 **/
	private String dispMode;
	
	/** 进展描述 **/
	private String prgrDec;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pubaalSerno
	 */
	public void setPubaalSerno(String pubaalSerno) {
		this.pubaalSerno = pubaalSerno == null ? null : pubaalSerno.trim();
	}
	
    /**
     * @return PubaalSerno
     */	
	public String getPubaalSerno() {
		return this.pubaalSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param tranId
	 */
	public void setTranId(String tranId) {
		this.tranId = tranId == null ? null : tranId.trim();
	}
	
    /**
     * @return TranId
     */	
	public String getTranId() {
		return this.tranId;
	}
	
	/**
	 * @param tranAmt
	 */
	public void setTranAmt(java.math.BigDecimal tranAmt) {
		this.tranAmt = tranAmt;
	}
	
    /**
     * @return TranAmt
     */	
	public java.math.BigDecimal getTranAmt() {
		return this.tranAmt;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param investAssetName
	 */
	public void setInvestAssetName(String investAssetName) {
		this.investAssetName = investAssetName == null ? null : investAssetName.trim();
	}
	
    /**
     * @return InvestAssetName
     */	
	public String getInvestAssetName() {
		return this.investAssetName;
	}
	
	/**
	 * @param tranBizName
	 */
	public void setTranBizName(String tranBizName) {
		this.tranBizName = tranBizName == null ? null : tranBizName.trim();
	}
	
    /**
     * @return TranBizName
     */	
	public String getTranBizName() {
		return this.tranBizName;
	}
	
	/**
	 * @param tranDealDate
	 */
	public void setTranDealDate(String tranDealDate) {
		this.tranDealDate = tranDealDate == null ? null : tranDealDate.trim();
	}
	
    /**
     * @return TranDealDate
     */	
	public String getTranDealDate() {
		return this.tranDealDate;
	}
	
	/**
	 * @param tranEndDate
	 */
	public void setTranEndDate(String tranEndDate) {
		this.tranEndDate = tranEndDate == null ? null : tranEndDate.trim();
	}
	
    /**
     * @return TranEndDate
     */	
	public String getTranEndDate() {
		return this.tranEndDate;
	}
	
	/**
	 * @param dispMode
	 */
	public void setDispMode(String dispMode) {
		this.dispMode = dispMode == null ? null : dispMode.trim();
	}
	
    /**
     * @return DispMode
     */	
	public String getDispMode() {
		return this.dispMode;
	}
	
	/**
	 * @param prgrDec
	 */
	public void setPrgrDec(String prgrDec) {
		this.prgrDec = prgrDec == null ? null : prgrDec.trim();
	}
	
    /**
     * @return PrgrDec
     */	
	public String getPrgrDec() {
		return this.prgrDec;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}