/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBadDebtWriteoffClassInfo
 * @类描述: pla_bad_debt_writeoff_class_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-10-19 23:07:10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_bad_debt_writeoff_class_info")
public class PlaBadDebtWriteoffClassInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 借据编号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "BILL_NO")
    private String billNo;

    /**
     * 五级分类
     **/
    @Column(name = "FIVE_CLASS", unique = false, nullable = false, length = 5)
    private String fiveClass;

    /**
     * 十级分类
     **/
    @Column(name = "TEN_CLASS", unique = false, nullable = true, length = 5)
    private String tenClass;

    /**
     * 核销日期
     **/
    @Column(name = "WRITEOFF_DATE", unique = false, nullable = false, length = 10)
    private String writeoffDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaBadDebtWriteoffClassInfo() {
        // default implementation ignored
    }


    /**
     * @param billNo
     */
    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    /**
     * @return billNo
     */
    public String getBillNo() {
        return this.billNo;
    }

    /**
     * @param fiveClass
     */
    public void setFiveClass(String fiveClass) {
        this.fiveClass = fiveClass;
    }

    /**
     * @return fiveClass
     */
    public String getFiveClass() {
        return this.fiveClass;
    }

    /**
     * @param tenClass
     */
    public void setTenClass(String tenClass) {
        this.tenClass = tenClass;
    }

    /**
     * @return tenClass
     */
    public String getTenClass() {
        return this.tenClass;
    }

    /**
     * @param writeoffDate
     */
    public void setWriteoffDate(String writeoffDate) {
        this.writeoffDate = writeoffDate;
    }

    /**
     * @return writeoffDate
     */
    public String getWriteoffDate() {
        return this.writeoffDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}