/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.PlaLawPaymentInfo;
import cn.com.yusys.yusp.dto.PlaLawPaymentInfoDto;
import cn.com.yusys.yusp.repository.mapper.PlaLawPaymentInfoMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawPaymentInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-05 16:11:03
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaLawPaymentInfoService {

    @Autowired
    private PlaLawPaymentInfoMapper plaLawPaymentInfoMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaLawPaymentInfo selectByPrimaryKey(String plpiSerno) {
        return plaLawPaymentInfoMapper.selectByPrimaryKey(plpiSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaLawPaymentInfo> selectAll(QueryModel model) {
        return plaLawPaymentInfoMapper.selectByModel(model);
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaLawPaymentInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaLawPaymentInfo> list = plaLawPaymentInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int insert(PlaLawPaymentInfo record) {
        return plaLawPaymentInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaLawPaymentInfo record) {
        return plaLawPaymentInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaLawPaymentInfo record) {
        return plaLawPaymentInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaLawPaymentInfo record) {
        return plaLawPaymentInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String plpiSerno) {
        return plaLawPaymentInfoMapper.deleteByPrimaryKey(plpiSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaLawPaymentInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteByBrokeCaseNo
     * @方法描述: 根据破产案件编号删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByBrokeCaseNo(String brokeCaseNo) {
        return plaLawPaymentInfoMapper.deleteByBrokeCaseNo(brokeCaseNo);
    }


    /**
     * @方法名称: batchInsert
     * @方法描述: 保存受偿清单数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int batchInsert(PlaLawPaymentInfoDto plaLawPaymentInfoDto) {
        int count = 0;
        if (plaLawPaymentInfoDto != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            List<PlaLawPaymentInfo> paymentInfoList = plaLawPaymentInfoDto.getPlaLawPaymentInfo();
            for (PlaLawPaymentInfo plaLawPaymentInfo : paymentInfoList) {
                PlaLawPaymentInfo plaLawPaymentInfo1 = plaLawPaymentInfoMapper.selectByPrimaryKey(plaLawPaymentInfo.getPlpiSerno());
                if (plaLawPaymentInfo1 != null) {
                    plaLawPaymentInfo.setUpdId(inputId);
                    plaLawPaymentInfo.setUpdBrId(inputBrId);
                    plaLawPaymentInfo.setUpdDate(openDay);
                    plaLawPaymentInfo.setUpdateTime(createTime);
                    plaLawPaymentInfoMapper.updateByPrimaryKeySelective(plaLawPaymentInfo);
                } else {
                    plaLawPaymentInfo.setInputId(inputId);
                    plaLawPaymentInfo.setInputBrId(inputBrId);
                    plaLawPaymentInfo.setInputDate(openDay);
                    plaLawPaymentInfo.setCreateTime(createTime);
                    plaLawPaymentInfo.setUpdId(inputId);
                    plaLawPaymentInfo.setUpdBrId(inputBrId);
                    plaLawPaymentInfo.setUpdDate(openDay);
                    plaLawPaymentInfo.setUpdateTime(createTime);
                    insert(plaLawPaymentInfo);
                }
            }

        } else {
            throw BizException.error(null, "99999", "数据不存在");
        }
        return count;
    }
}
