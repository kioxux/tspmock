/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldLeaseAmtInfo
 * @类描述: pla_asset_pld_lease_amt_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-09 15:13:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_asset_pld_lease_amt_info")
public class PlaAssetPldLeaseAmtInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 出租租金信息流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PAPLAI_SERNO")
    private String paplaiSerno;

    /**
     * 出租流水号
     **/
    @Column(name = "PAPLI_SERNO", unique = false, nullable = true, length = 40)
    private String papliSerno;

    /**
     * 租金收取时间
     **/
    @Column(name = "LEASE_COLLEC_DATE", unique = false, nullable = true, length = 20)
    private String leaseCollecDate;

    /**
     * 租金收取金额
     **/
    @Column(name = "LEASE_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal leaseAmt;

    /**
     * 收取方式
     **/
    @Column(name = "LEASE_COLLEC_MODE", unique = false, nullable = true, length = 5)
    private String leaseCollecMode;

    /**
     * 租金缴纳人名称
     **/
    @Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
    private String cusName;

    /**
     * 收款账号
     **/
    @Column(name = "PYEE_ACCNO", unique = false, nullable = true, length = 40)
    private java.math.BigDecimal pyeeAccno;

    /**
     * 备注
     **/
    @Column(name = "MEMO", unique = false, nullable = true, length = 2000)
    private String memo;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaAssetPldLeaseAmtInfo() {
        // default implementation ignored
    }

    public BigDecimal getLeaseAmt() {
        return leaseAmt;
    }

    public void setLeaseAmt(BigDecimal leaseAmt) {
        this.leaseAmt = leaseAmt;
    }

    /**
     * @param paplaiSerno
     */
    public void setPaplaiSerno(String paplaiSerno) {
        this.paplaiSerno = paplaiSerno;
    }

    /**
     * @return paplaiSerno
     */
    public String getPaplaiSerno() {
        return this.paplaiSerno;
    }

    /**
     * @param papliSerno
     */
    public void setPapliSerno(String papliSerno) {
        this.papliSerno = papliSerno;
    }

    /**
     * @return papliSerno
     */
    public String getPapliSerno() {
        return this.papliSerno;
    }

    /**
     * @param leaseCollecDate
     */
    public void setLeaseCollecDate(String leaseCollecDate) {
        this.leaseCollecDate = leaseCollecDate;
    }

    /**
     * @return leaseCollecDate
     */
    public String getLeaseCollecDate() {
        return this.leaseCollecDate;
    }

    /**
     * @param leaseCollecMode
     */
    public void setLeaseCollecMode(String leaseCollecMode) {
        this.leaseCollecMode = leaseCollecMode;
    }

    /**
     * @return leaseCollecMode
     */
    public String getLeaseCollecMode() {
        return this.leaseCollecMode;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param pyeeAccno
     */
    public void setPyeeAccno(java.math.BigDecimal pyeeAccno) {
        this.pyeeAccno = pyeeAccno;
    }

    /**
     * @return pyeeAccno
     */
    public java.math.BigDecimal getPyeeAccno() {
        return this.pyeeAccno;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }
}