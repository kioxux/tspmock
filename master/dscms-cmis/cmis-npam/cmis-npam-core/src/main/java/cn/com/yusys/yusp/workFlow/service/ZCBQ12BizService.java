package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisNpamConstants;
import cn.com.yusys.yusp.domain.PlaBartPldDebtAppInfo;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.PlaBartPldDebtAppInfoService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @className PlaLawArbitrateAppBiz
 * @author hujun
 * @Description 以物抵债登记审批流程
 * @Date 2021/06/12
 */
@Service
public class ZCBQ12BizService implements ClientBizInterface {
	private final Logger log = LoggerFactory.getLogger(ZCBQ12BizService.class);
	@Autowired
	private AmqpTemplate amqpTemplate;
	@Autowired
	private PlaBartPldDebtAppInfoService plaBartPldDebtAppInfoService;

	@Override
	public void bizOp(ResultInstanceDto resultInstanceDto) {
		String currentOpType = resultInstanceDto.getCurrentOpType();
		String serno = resultInstanceDto.getBizId();
		log.info("以物抵债登记审批后业务处理类型" + currentOpType);
		try {
			if (OpType.STRAT.equals(currentOpType)) {
				log.info("以物抵债登记审批" + serno + "流程发起操作，流程参数" + resultInstanceDto);
			} else if (OpType.RUN.equals(currentOpType)) {
				log.info("以物抵债登记审批" + serno + "流程提交操作，流程参数" + resultInstanceDto);
				PlaBartPldDebtAppInfo info=plaBartPldDebtAppInfoService.selectByPrimaryKey(serno);
				info.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
				plaBartPldDebtAppInfoService.updateSelective(info);
			} else if (OpType.JUMP.equals(currentOpType)) {
				log.info("以物抵债登记审批" + serno + "流程跳转操作，流程参数" + resultInstanceDto);
			} else if (OpType.END.equals(currentOpType)) {
				log.info("以物抵债登记审批" + serno + "流程结束操作，流程参数" + resultInstanceDto);
				//针对流程到办结节点，进行以下处理-修改业务数据状态为通过
				PlaBartPldDebtAppInfo info=plaBartPldDebtAppInfoService.selectByPrimaryKey(serno);
				info.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
				//修改登记状态
				info.setRegiStatus(CmisNpamConstants.HX_REGI_STATUS_01);
				//修改记账状态
				info.setRecordStatus(CmisNpamConstants.HX_STATUS_01);
				plaBartPldDebtAppInfoService.updateSelective(info);
			} else if (OpType.RETURN_BACK.equals(currentOpType)) {
				//判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
				if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)){
					PlaBartPldDebtAppInfo info=plaBartPldDebtAppInfoService.selectByPrimaryKey(serno);
					info.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
					plaBartPldDebtAppInfoService.updateSelective(info);
				}
			} else if (OpType.CALL_BACK.equals(currentOpType)) {
				log.info("以物抵债登记审批"+serno+"打回操作，流程参数："+ resultInstanceDto.toString());
				//判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
				if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
					PlaBartPldDebtAppInfo info=plaBartPldDebtAppInfoService.selectByPrimaryKey(serno);
					info.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
					plaBartPldDebtAppInfoService.updateSelective(info);
				}
			} else if (OpType.TACK_BACK.equals(currentOpType)) {
				log.info("以物抵债登记审批"+serno+"拿回操作，流程参数："+ resultInstanceDto.toString());
				//判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
				if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
					PlaBartPldDebtAppInfo info=plaBartPldDebtAppInfoService.selectByPrimaryKey(serno);
					info.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
					plaBartPldDebtAppInfoService.updateSelective(info);
				}
			} else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
				log.info("以物抵债登记审批" + serno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
				//流程拿回到第一个节点，申请主表的业务
				PlaBartPldDebtAppInfo info=plaBartPldDebtAppInfoService.selectByPrimaryKey(serno);
				info.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
				plaBartPldDebtAppInfoService.updateSelective(info);
			} else if (OpType.REFUSE.equals(currentOpType)) {
				log.info("以物抵债登记审批" + serno + "否决操作，流程参数：" + resultInstanceDto);
				// 否决改变标志 审批中 111-> 审批不通过 998
				PlaBartPldDebtAppInfo info=plaBartPldDebtAppInfoService.selectByPrimaryKey(serno);
				info.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
				plaBartPldDebtAppInfoService.updateSelective(info);
			} else {
				log.warn("以物抵债登记审批" + serno + "未知操作:" + resultInstanceDto);
			}
		} catch (Exception e) {
			log.error("后业务处理失败", e);
			try {
				BizCommonUtils bizCommonUtils = new BizCommonUtils();
				bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
			} catch (Exception e1) {
				log.error("发送异常消息失败", e1);
			}
		}
	}
	// 判定流程能否进行业务处理
	@Override
	public boolean should(ResultInstanceDto resultInstanceDto) {
		String flowCode = resultInstanceDto.getFlowCode();
		return (CmisFlowConstants.ZCBQ12.equals(flowCode) || CmisFlowConstants.ZCBQ13.equals(flowCode));
	}
}
