/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.repository.mapper.PlaPlanDetailMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaPlan;
import cn.com.yusys.yusp.repository.mapper.PlaPlanMapper;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaPlanService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-28 14:41:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaPlanService {

    @Autowired
    private PlaPlanMapper plaPlanMapper;

    @Autowired
    private PlaPlanDetailMapper plaPlanDetailMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PlaPlan selectByPrimaryKey(String recoverySerno) {
        return plaPlanMapper.selectByPrimaryKey(recoverySerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaPlan> selectAll(QueryModel model) {
        List<PlaPlan> records = (List<PlaPlan>) plaPlanMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PlaPlan> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaPlan> list = plaPlanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PlaPlan record) {
        return plaPlanMapper.insert(record);
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertByserno(PlaPlan record) {
        PlaPlan plaPlan2 = plaPlanMapper.selectByPrimaryKey(record.getRecoverySerno());
        int result = 0;
        if (plaPlan2 != null){
            result = plaPlanMapper.updateByPrimaryKeySelective(record);
        }else {
            result = plaPlanMapper.insert(record);
        }
        return  result;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PlaPlan record) {
        return plaPlanMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PlaPlan record) {
        return plaPlanMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PlaPlan record) {
        User userInfo = SessionUtils.getUserInformation();
//        if(userInfo == null){
//            return new ResultDto<>(null).message("未查询到用户信息");
//        }
        record.setUpdId(userInfo.getLoginCode());  //最近修改人
        record.setUpdBrId(userInfo.getOrg().getCode());  //最近修改机构
        record.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));  //最近修改日期
        record.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT), DateFormatEnum.DEFAULT.getValue()));  //修改时间
        return plaPlanMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String recoverySerno) {
        int count = plaPlanMapper.deleteByPrimaryKey(recoverySerno);
         if(count >0){
             plaPlanDetailMapper.deleteBySerno(recoverySerno);
         }
        return count;
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaPlanMapper.deleteByIds(ids);
    }
}
