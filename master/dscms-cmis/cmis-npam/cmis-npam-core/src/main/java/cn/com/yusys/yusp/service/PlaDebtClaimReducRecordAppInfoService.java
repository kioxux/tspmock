/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Supplier;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisNpamConstants;
import cn.com.yusys.yusp.domain.PlaBartPldDebtAppInfo;
import cn.com.yusys.yusp.domain.PlaBartPldDebtBillRel;
import cn.com.yusys.yusp.domain.PlaDebtClaimReducRecordBillRel;
import cn.com.yusys.yusp.dto.AccLoanDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3300.Da3300RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3085.req.Ln3085ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3085.resp.Ln3085RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3128.resp.Ln3128RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3249.resp.Ln3249RespDto;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.ln3100.Ln3100Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaDebtClaimReducRecordAppInfo;
import cn.com.yusys.yusp.repository.mapper.PlaDebtClaimReducRecordAppInfoMapper;

import javax.persistence.Column;

import static cn.com.yusys.yusp.constants.CmisBizConstants.APPLY_STATE_TODO;
import static cn.com.yusys.yusp.constants.CmisNpamConstants.HX_STATUS_01;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaDebtClaimReducRecordAppInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-08 19:44:40
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaDebtClaimReducRecordAppInfoService {

    @Autowired
    private PlaDebtClaimReducRecordAppInfoMapper plaDebtClaimReducRecordAppInfoMapper;

    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    @Autowired
    private PlaDebtClaimReducRecordBillRelService plaDebtClaimReducRecordBillRelService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private Ln3100Server ln3100Server;

    private static final Logger log = LoggerFactory.getLogger(PlaDebtClaimReducRecordBillRelService.class);

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaDebtClaimReducRecordAppInfo selectByPrimaryKey(String pdcrraiSerno) {
        return plaDebtClaimReducRecordAppInfoMapper.selectByPrimaryKey(pdcrraiSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaDebtClaimReducRecordAppInfo> selectAll(QueryModel model) {
        List<PlaDebtClaimReducRecordAppInfo> records = (List<PlaDebtClaimReducRecordAppInfo>) plaDebtClaimReducRecordAppInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<PlaDebtClaimReducRecordAppInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaDebtClaimReducRecordAppInfo> list = plaDebtClaimReducRecordAppInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaDebtClaimReducRecordAppInfo record) {
        int count = 0;
        if (record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }

            // 修改人
            record.setUpdId(inputId);
            // 修改机构
            record.setUpdBrId(inputBrId);
            // 修改日期
            record.setUpdDate(openDay);
            //默认新增赋值状态为待发起
            record.setApproveStatus(APPLY_STATE_TODO);
            // 修改日期
            record.setUpdateTime(createTime);
            //默认新增记账状态为待记账
            record.setRecordStatus(HX_STATUS_01);
            PlaDebtClaimReducRecordAppInfo plaDebtClaimReducRecordAppInfo = plaDebtClaimReducRecordAppInfoMapper.selectByPrimaryKey(record.getPdcrraiSerno());
            if (plaDebtClaimReducRecordAppInfo != null) {
                count = plaDebtClaimReducRecordAppInfoMapper.updateByPrimaryKeySelective(record);
            } else {
                record.setInputId(inputId);
                record.setInputBrId(inputBrId);
                record.setInputDate(openDay);
                record.setCreateTime(createTime);
                count = plaDebtClaimReducRecordAppInfoMapper.insert(record);
            }

        }
        return count;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaDebtClaimReducRecordAppInfo record) {
        return plaDebtClaimReducRecordAppInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaDebtClaimReducRecordAppInfo record) {
        return plaDebtClaimReducRecordAppInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaDebtClaimReducRecordAppInfo record) {
        return plaDebtClaimReducRecordAppInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pdcrraiSerno) {
        return plaDebtClaimReducRecordAppInfoMapper.deleteByPrimaryKey(pdcrraiSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaDebtClaimReducRecordAppInfoMapper.deleteByIds(ids);
    }


    /**
     * 债权减免发送核心记账
     *
     * @author 刘权
     **/
    public ResultDto<String> sendToHXJZ(PlaDebtClaimReducRecordAppInfo appInfo) {
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        ResultDto<String> resultDto = new ResultDto<>();
        // 记账成功
        int recordeSuccessCount = 0;
        //记账状态
        String recordStatus = "";
        if (appInfo != null) {
            List<PlaDebtClaimReducRecordBillRel> list = plaDebtClaimReducRecordBillRelService.selectByPdcraiSerno(appInfo.getPdcrraiSerno());
            for (PlaDebtClaimReducRecordBillRel plaDebtClaimReducRecordBillRel : list) {
                boolean flag = sendToHXBXJM(plaDebtClaimReducRecordBillRel);
                if (flag) {
                    recordeSuccessCount++;
                }
            }
            if (recordeSuccessCount == list.size()) {
                recordStatus = CmisNpamConstants.HX_STATUS_03;
            } else {
                recordStatus = CmisNpamConstants.HX_STATUS_04;
            }
            appInfo.setRecordStatus(recordStatus);
            appInfo.setRecordDate(openDay);
            updateSelective(appInfo);
        } else {
            throw BizException.error(null, EcnEnum.ECN060005.key, EcnEnum.ECN060005.value);
        }
        return resultDto;
    }

    /**
     * 核心记账补发
     *
     * @param plaDebtClaimReducRecordBillRel
     * @return
     */
    public ResultDto<String> sendToJZBF(PlaDebtClaimReducRecordBillRel plaDebtClaimReducRecordBillRel) {
        Ln3100ReqDto ln3100ReqDto = new Ln3100ReqDto();
        ResultDto<String> resultDto = new ResultDto<>();
        // 申请流水号
        String pdcrraiSerno = plaDebtClaimReducRecordBillRel.getPdcrraiSerno();
        boolean flag = sendToHXBXJM(plaDebtClaimReducRecordBillRel);
        QueryModel model = new QueryModel();
        model.addCondition("pdcraiSerno", pdcrraiSerno);
        model.addCondition("recordStatus", CmisNpamConstants.HX_STATUS_04);
        // 如果借据都记账成功则修改记账状态
        List<PlaDebtClaimReducRecordBillRel> plaDebtClaimReducRecordBillRelList = plaDebtClaimReducRecordBillRelService.selectByModel(model);
        // 如果不存在记账失败情况更新记账状态
        if (plaDebtClaimReducRecordBillRelList.size() == 0) {
            PlaDebtClaimReducRecordAppInfo appInfo = selectByPrimaryKey(pdcrraiSerno);
            appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_03);
            update(appInfo);
        }
        return resultDto;
    }

    /**
     * 债权减免发送贷款本息减免
     *
     * @author 刘权
     **/
    public boolean sendToHXBXJM(PlaDebtClaimReducRecordBillRel plaDebtClaimReducRecordBillRel) {

        ResultDto<String> resultDto = new ResultDto<>();
        Ln3085ReqDto ln3085ReqDto = new Ln3085ReqDto();
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        boolean flag = false;
        if (plaDebtClaimReducRecordBillRel != null) {
            List<AccLoanDto> accLoanDtoList = new ArrayList<>();
            AccLoanDto accLoanDto = new AccLoanDto();
            // 插入借据
            accLoanDto.setBillNo(plaDebtClaimReducRecordBillRel.getBillNo());
            //操作标志
            ln3085ReqDto.setDaikczbz(CmisNpamConstants.HX_OPT_STATUS_3);
            //贷款借据号
            ln3085ReqDto.setDkjiejuh(plaDebtClaimReducRecordBillRel.getBillNo());//
            //货币代号 01 人民币待转换 todo
            ln3085ReqDto.setHuobdhao("01");
            //贷款归还方式
            //ln3085ReqDto.setDaikghfs(CmisNpamConstants.HX_DAIKGHFS_16);
            //核销本金
            ln3085ReqDto.setHexiaobj(BigDecimal.ZERO);
            //核销利息
            ln3085ReqDto.setHexiaolx(BigDecimal.ZERO);
            //已核销本金利息
            ln3085ReqDto.setYihxbjlx(BigDecimal.ZERO);
            //正常本金 只能减免利息so本金为空
            ln3085ReqDto.setZhchbjin(BigDecimal.ZERO);
            //逾期本金
            ln3085ReqDto.setYuqibjin(BigDecimal.ZERO);
            //呆滞本金
            ln3085ReqDto.setDzhibjin(BigDecimal.ZERO);
            //呆账本金
            ln3085ReqDto.setDaizbjin(BigDecimal.ZERO);
            //应收应计利息
            ln3085ReqDto.setYsyjlixi(plaDebtClaimReducRecordBillRel.getRecRemInt());
            //催收应计利息
            ln3085ReqDto.setCsyjfaxi(plaDebtClaimReducRecordBillRel.getBcmRemInt());
            //应收欠息
            ln3085ReqDto.setYsqianxi(plaDebtClaimReducRecordBillRel.getRecDebitInt());
            //催收欠息
            ln3085ReqDto.setCsqianxi(plaDebtClaimReducRecordBillRel.getBcmDebitInt());
            //应收应计罚息
            ln3085ReqDto.setYsyjfaxi(plaDebtClaimReducRecordBillRel.getRecRemPenalInt());
            //催收应计罚息
            ln3085ReqDto.setCsyjfaxi(plaDebtClaimReducRecordBillRel.getBcmRemPenalInt());
            //应收罚息
            ln3085ReqDto.setYshofaxi(plaDebtClaimReducRecordBillRel.getRecPenalInt());
            //催收罚息
            ln3085ReqDto.setCshofaxi(plaDebtClaimReducRecordBillRel.getBcmPenalInt());
            //应计复息
            ln3085ReqDto.setYingjifx(plaDebtClaimReducRecordBillRel.getRecCompoundInt());
            //复息
            ln3085ReqDto.setFuxiiiii(plaDebtClaimReducRecordBillRel.getCompoundInt());
            log.info("ln3085接口请求：" + ln3085ReqDto);
            // 调用核心接口，债权减免发送贷款本息减免  登记成功更改记账状态
            ResultDto<Ln3085RespDto> ln3085RespDto = dscms2CoreLnClientService.ln3085(ln3085ReqDto);
            log.info("ln3085返回请求：" + ln3085RespDto);
            String ln3085Code = Optional.ofNullable(ln3085RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String ln3085Meesage = Optional.ofNullable(ln3085RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            // 记账成功
            if (Objects.equals(ln3085Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                if (ln3085RespDto.getData() != null) {
                    plaDebtClaimReducRecordBillRel.setRecordStatus(CmisNpamConstants.HX_STATUS_03);
                    plaDebtClaimReducRecordBillRel.setRecordDate(openDay);
                    plaDebtClaimReducRecordBillRel.setRecordTranSerno(ln3085RespDto.getData().getJiaoyils());
                    plaDebtClaimReducRecordBillRel.setRecordTranTime(ln3085RespDto.getData().getJiaoyirq());
                    plaDebtClaimReducRecordBillRelService.updateSelective(plaDebtClaimReducRecordBillRel);
                    //记账成功更新台账信息
                    // 借据编号
                    String billNo = plaDebtClaimReducRecordBillRel.getBillNo();
                    ResultDto<Ln3100RespDto> ln3100RespDto = ln3100Server.queryHXBill(billNo);
                    // 借据金额
                    BigDecimal loanAmt = BigDecimal.ZERO;
                    // 贷款余额
                    BigDecimal loanBalance = BigDecimal.ZERO;
                    // 欠息
                    BigDecimal debitInt = BigDecimal.ZERO;
                    // 罚息
                    BigDecimal penalInt = BigDecimal.ZERO;
                    // 复息
                    BigDecimal compoundInt = BigDecimal.ZERO;
                    log.info("ln3100接口返回：" + ln3100RespDto);
                    String ln3100Code = Optional.ofNullable(ln3100RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String ln3100Meesage = Optional.ofNullable(ln3100RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    if (Objects.equals(ln3100Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                        if (ln3100RespDto.getData() != null) {
                            loanAmt = ln3100RespDto.getData().getJiejuuje();
                            loanBalance = ln3100RespDto.getData().getZhchbjin().add(ln3100RespDto.getData().getYuqibjin());
                            debitInt = ln3100RespDto.getData().getYsyjlixi().add(ln3100RespDto.getData().getCsyjlixi()).add(ln3100RespDto.getData().getYsqianxi()).add(ln3100RespDto.getData().getCsqianxi());
                            penalInt = ln3100RespDto.getData().getYsyjfaxi().add(ln3100RespDto.getData().getCsyjfaxi()).add(ln3100RespDto.getData().getYshofaxi()).add(ln3100RespDto.getData().getCshofaxi());
                            compoundInt = ln3100RespDto.getData().getYingjifx().add(ln3100RespDto.getData().getFuxiiiii());
                            accLoanDto.setLoanBalance(loanBalance);
                            accLoanDto.setDebitInt(debitInt);
                            accLoanDto.setPenalInt(penalInt);
                            accLoanDto.setCompoundInt(compoundInt);
                            accLoanDtoList.add(accLoanDto);
                        }
                    }
                    // 调用biz接口更新金额
                    cmisBizClientService.updateAccLoan(accLoanDtoList);
                    flag = true;
                } else {
                    // 记账失败
                    plaDebtClaimReducRecordBillRel.setRecordStatus(CmisNpamConstants.HX_STATUS_04);
                    plaDebtClaimReducRecordBillRel.setRecordDate(openDay);
                    plaDebtClaimReducRecordBillRelService.updateSelective(plaDebtClaimReducRecordBillRel);
                    flag = false;
                }
            } else {
                throw BizException.error(null, ln3085Code, ln3085Meesage);
            }
        } else {
            throw BizException.error(null, EcnEnum.ECN060005.key, EcnEnum.ECN060005.value);
        }
        return flag;
    }

    /**
     * @方法名称: showByPdcrraiSerno
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     * liuquan
     */

    public PlaDebtClaimReducRecordAppInfo showByPdcrraiSerno(PlaDebtClaimReducRecordAppInfo plaDebtClaimReducRecordAppInfo) {
        return plaDebtClaimReducRecordAppInfoMapper.showByPdcrraiSerno(plaDebtClaimReducRecordAppInfo.getPdcrraiSerno());
    }


    /**
     * 债权减免发送核心记账
     *
     * @author 茂茂
     **/
    public ResultDto<Ln3100RespDto> queryHXBill(String billNo) {
        Supplier<ResultDto<Ln3100RespDto>> supplierResp = ResultDto<Ln3100RespDto>::new;
        ResultDto<Ln3100RespDto> ln3100RespDtos = supplierResp.get();
        if (billNo != null) {
            Supplier<Ln3100ReqDto> supplier = Ln3100ReqDto::new;
            Ln3100ReqDto ln3100ReqDto = supplier.get();
            //查询笔数
            ln3100ReqDto.setChxunbis(1);
            //贷款借据号
            ln3100ReqDto.setDkjiejuh(billNo);
            //贷款账号
            ln3100ReqDto.setDkzhangh("");
            //起始笔数
            ln3100ReqDto.setQishibis(0);
            // 调用核心接口，进行债权减免发送核心记账
            ln3100RespDtos = dscms2CoreLnClientService.ln3100(ln3100ReqDto);
        } else {
            throw BizException.error(null, EcnEnum.ECN060005.key, EcnEnum.ECN060005.value);
        }
        return ln3100RespDtos;
    }
}
