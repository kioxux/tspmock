/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.List;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaLawAssetDisp;
import cn.com.yusys.yusp.repository.mapper.PlaLawAssetDispMapper;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawAssetDispService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-07 19:23:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaLawAssetDispService {

    @Autowired
    private PlaLawAssetDispMapper plaLawAssetDispMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PlaLawAssetDisp selectByPrimaryKey(String pladSerno) {
        return plaLawAssetDispMapper.selectByPrimaryKey(pladSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaLawAssetDisp> selectAll(QueryModel model) {
        List<PlaLawAssetDisp> records = (List<PlaLawAssetDisp>) plaLawAssetDispMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PlaLawAssetDisp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaLawAssetDisp> list = plaLawAssetDispMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PlaLawAssetDisp record) {
        return plaLawAssetDispMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PlaLawAssetDisp record) {
        return plaLawAssetDispMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PlaLawAssetDisp record) {
        return plaLawAssetDispMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PlaLawAssetDisp record) {
        return plaLawAssetDispMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pladSerno) {
        return plaLawAssetDispMapper.deleteByPrimaryKey(pladSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaLawAssetDispMapper.deleteByIds(ids);
    }

    public int batchUpdate(List<PlaLawAssetDisp> plaLawAssetDisp) {
        // 申请人
        String inputId = "";
        // 申请机构
        String inputBrId = "";
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        // 创建时间
        Date createTime = DateUtils.getCurrTimestamp();
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            inputId = userInfo.getLoginCode();
            // 申请机构
            inputBrId = userInfo.getOrg().getCode();
        }
        for (PlaLawAssetDisp lawAssetDisp : plaLawAssetDisp) {
            lawAssetDisp.setUpdId(inputId);
            lawAssetDisp.setUpdBrId(inputBrId);
            lawAssetDisp.setUpdDate(openDay);
            lawAssetDisp.setUpdateTime(createTime);
            plaLawAssetDispMapper.updateByPrimaryKeySelective(lawAssetDisp);
        }
        return plaLawAssetDisp.size();
    }


    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ResultDto<Integer> insertByPlaLawAssetDispService(PlaLawAssetDisp record) {
        ResultDto<Integer> resultDto = new ResultDto<Integer>();
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("autho", record.getAutho());
        queryModel.addCondition("caseSerno", record.getCaseSerno());
        PlaLawAssetDisp plaLawAssetDisp = plaLawAssetDispMapper.selectByCaseSerno(queryModel);
        int result = 0;
        if (plaLawAssetDisp != null){
            resultDto.setCode("1");
            resultDto.setMessage("不能引入重复的数据");
        }else {
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            record.setInputDate(openDay);
            record.setCreateTime(createTime);
            record.setUpdDate(openDay);
            record.setUpdateTime(createTime);
            result = plaLawAssetDispMapper.insert(record);
            resultDto.setMessage("引入成功");
        }
        return resultDto;
    }
}
