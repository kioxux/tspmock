/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.PlaLawBrokeCaseInfo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaLawArbitrateCaseInfo;
import cn.com.yusys.yusp.service.PlaLawArbitrateCaseInfoService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawArbitrateCaseInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-05 15:48:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plalawarbitratecaseinfo")
public class PlaLawArbitrateCaseInfoResource {
    @Autowired
    private PlaLawArbitrateCaseInfoService plaLawArbitrateCaseInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaLawArbitrateCaseInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaLawArbitrateCaseInfo> list = plaLawArbitrateCaseInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaLawArbitrateCaseInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaLawArbitrateCaseInfo>> index(QueryModel queryModel) {
        List<PlaLawArbitrateCaseInfo> list = plaLawArbitrateCaseInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawArbitrateCaseInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{arbitrateCaseNo}")
    protected ResultDto<PlaLawArbitrateCaseInfo> show(@PathVariable("arbitrateCaseNo") String arbitrateCaseNo) {
        PlaLawArbitrateCaseInfo plaLawArbitrateCaseInfo = plaLawArbitrateCaseInfoService.selectByPrimaryKey(arbitrateCaseNo);
        return new ResultDto<PlaLawArbitrateCaseInfo>(plaLawArbitrateCaseInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaLawArbitrateCaseInfo> create(@RequestBody PlaLawArbitrateCaseInfo plaLawArbitrateCaseInfo) throws URISyntaxException {
        plaLawArbitrateCaseInfoService.insert(plaLawArbitrateCaseInfo);
        return new ResultDto<PlaLawArbitrateCaseInfo>(plaLawArbitrateCaseInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaLawArbitrateCaseInfo plaLawArbitrateCaseInfo) throws URISyntaxException {
        int result = plaLawArbitrateCaseInfoService.updateSelective(plaLawArbitrateCaseInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{arbitrateCaseNo}")
    protected ResultDto<Integer> delete(@PathVariable("arbitrateCaseNo") String arbitrateCaseNo) {
        int result = plaLawArbitrateCaseInfoService.deleteByPrimaryKey(arbitrateCaseNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaLawArbitrateCaseInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @ApiOperation("仲裁案件新增")
    @PostMapping("/insert")
    protected ResultDto<PlaLawArbitrateCaseInfo> insert(@RequestBody PlaLawArbitrateCaseInfo plaLawArbitrateCaseInfo) throws URISyntaxException {
        plaLawArbitrateCaseInfoService.insertSelective(plaLawArbitrateCaseInfo);
        return new ResultDto<PlaLawArbitrateCaseInfo>(plaLawArbitrateCaseInfo);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     * 创建人：周茂伟
     */
    @ApiOperation("仲裁案件列表查询")
    @PostMapping("/queryPlaLawArbitrateCaseList")
    protected ResultDto<List<PlaLawArbitrateCaseInfo>> queryPlaLawArbitrateCaseList(@RequestBody QueryModel queryModel) {
        List<PlaLawArbitrateCaseInfo> list = plaLawArbitrateCaseInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawArbitrateCaseInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("仲裁案件信息查询")
    @PostMapping("/queryPlaLawArbitrateCaseInfo")
    protected ResultDto<PlaLawArbitrateCaseInfo> queryPlaLawArbitrateCaseInfo(@RequestBody PlaLawArbitrateCaseInfo plaLawArbitrateCaseInfo) {
        PlaLawArbitrateCaseInfo plaLawArbitrateCaseInfos = plaLawArbitrateCaseInfoService.selectByPrimaryKey(plaLawArbitrateCaseInfo.getArbitrateCaseNo());
        return new ResultDto<PlaLawArbitrateCaseInfo>(plaLawArbitrateCaseInfos);
    }
}
