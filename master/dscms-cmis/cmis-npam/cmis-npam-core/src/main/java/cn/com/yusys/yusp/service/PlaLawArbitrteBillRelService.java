/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.PlaLawArbitrteBillRelMapper;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawArbitrteBillRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-05 16:54:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaLawArbitrteBillRelService {

    @Autowired
    private PlaLawArbitrteBillRelMapper plaLawArbitrteBillRelMapper;
    @Autowired
    private PlaLawArbitrateCaseInfoService plaLawArbitrateCaseInfoService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PlaLawArbitrteBillRel selectByPrimaryKey(String serno) {
        return plaLawArbitrteBillRelMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaLawArbitrteBillRel> selectAll(QueryModel model) {
        List<PlaLawArbitrteBillRel> records = (List<PlaLawArbitrteBillRel>) plaLawArbitrteBillRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PlaLawArbitrteBillRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaLawArbitrteBillRel> list = plaLawArbitrteBillRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PlaLawArbitrteBillRel record) {
        return plaLawArbitrteBillRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PlaLawArbitrteBillRel record) {
        return plaLawArbitrteBillRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PlaLawArbitrteBillRel record) {
        return plaLawArbitrteBillRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PlaLawArbitrteBillRel record) {
        return plaLawArbitrteBillRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return plaLawArbitrteBillRelMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaLawArbitrteBillRelMapper.deleteByIds(ids);
    }

    /**
     * 批量引入合同借据信息
     * @param lawBillRels
     * @return
     */
    public Integer saveBatchContNo(List<PlaLawArbitrteBillRel> lawBillRels) {
        int count = 0;
        if(CollectionUtils.nonEmpty(lawBillRels)){
            Map<String, Object> map = new HashMap<>();
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            String arbitrateCaseNo = "";
            String billNo="";
            if(userInfo != null){
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            for (PlaLawArbitrteBillRel item: lawBillRels) {
                arbitrateCaseNo= item.getArbitrateCaseNo();
                billNo = item.getBillNo();
                map.put("arbitrateCaseNo",arbitrateCaseNo);
                map.put("billNo",billNo);
                PlaLawArbitrteBillRel plaLawArbitrteBillRel = plaLawArbitrteBillRelMapper.selectByArbitrateCaseNo(map);
                if(plaLawArbitrteBillRel != null){
                    throw BizException.error(null, "99999", "不能引入相同借据");
                }else{
                    item.setInputId(inputId);
                    item.setInputBrId(inputBrId);
                    item.setInputDate(openDay);
                    item.setCreateTime(createTime);
                    item.setUpdId(inputId);
                    item.setUpdBrId(inputBrId);
                    item.setUpdDate(openDay);
                    item.setUpdateTime(createTime);
                    plaLawArbitrteBillRelMapper.insertSelective(item);
                }
            }
            map.clear();
            map = plaLawArbitrteBillRelMapper.selectBySum(arbitrateCaseNo);
            if (CollectionUtils.nonEmpty(map)) {
                // 仲裁总标的
                BigDecimal arbitrateSubjectAmt=(BigDecimal) map.get("arbitrateSubjectAmt");
                // 贷款余额
                BigDecimal arbitrateCapAmt=(BigDecimal) map.get("arbitrateCapAmt");
                // 拖欠利息总额
                BigDecimal totalTqlxAmt=(BigDecimal) map.get("totalTqlxAmt");
                //获取当前业务流水号下的数据
                PlaLawArbitrateCaseInfo plaLawArbitrateCaseInfo = plaLawArbitrateCaseInfoService.selectByPrimaryKey(arbitrateCaseNo);
                if (plaLawArbitrateCaseInfo != null) {
                    plaLawArbitrateCaseInfo.setArbitrateCapAmt(arbitrateCapAmt);
                    plaLawArbitrateCaseInfo.setArbitrateSubjectAmt(arbitrateSubjectAmt);
                    plaLawArbitrateCaseInfo.setTotalTqlxAmt(totalTqlxAmt);
                    count = plaLawArbitrateCaseInfoService.update(plaLawArbitrateCaseInfo);
                }
            }
        }
        return  count;
    }
    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int delete(PlaLawArbitrteBillRel plaLawArbitrteBillRel) {
        int count= 0;
        if(plaLawArbitrteBillRel != null){
            count =plaLawArbitrteBillRelMapper.deleteByPrimaryKey(plaLawArbitrteBillRel.getSerno());
            if(count > 0){
               Map map = plaLawArbitrteBillRelMapper.selectBySum(plaLawArbitrteBillRel.getArbitrateCaseNo());
                if (CollectionUtils.nonEmpty(map)) {
                    // 仲裁总标的
                    BigDecimal arbitrateSubjectAmt=(BigDecimal) map.get("arbitrateSubjectAmt");
                    // 贷款余额
                    BigDecimal arbitrateCapAmt=(BigDecimal) map.get("arbitrateCapAmt");
                    // 拖欠利息总额
                    BigDecimal totalTqlxAmt=(BigDecimal) map.get("totalTqlxAmt");
                    //获取当前业务流水号下的数据
                    PlaLawArbitrateCaseInfo plaLawArbitrateCaseInfo = plaLawArbitrateCaseInfoService.selectByPrimaryKey(plaLawArbitrteBillRel.getArbitrateCaseNo());
                    if (plaLawArbitrateCaseInfo != null) {
                        plaLawArbitrateCaseInfo.setArbitrateCapAmt(arbitrateCapAmt);
                        plaLawArbitrateCaseInfo.setArbitrateSubjectAmt(arbitrateSubjectAmt);
                        plaLawArbitrateCaseInfo.setTotalTqlxAmt(totalTqlxAmt);
                        count = plaLawArbitrateCaseInfoService.update(plaLawArbitrateCaseInfo);
                    }
                }
            }
        }
        return count;
    }
}
