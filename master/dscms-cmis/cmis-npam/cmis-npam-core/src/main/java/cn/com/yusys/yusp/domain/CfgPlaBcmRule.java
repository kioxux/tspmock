/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: CfgPlaBcmRule
 * @类描述: cfg_pla_bcm_rule数据实体类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-05-31 15:18:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_pla_bcm_rule")
public class CfgPlaBcmRule extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 规则编号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "RULE_ID")
    private String ruleId;

    /**
     * 规则名称
     **/
    @Column(name = "RULE_NAME", unique = false, nullable = true, length = 40)
    private String ruleName;

    /**
     * 资产类型
     **/
    @Column(name = "ASSET_TYPE", unique = false, nullable = true, length = 5)
    private String assetType;

    /**
     * 任务生成频率
     **/
    @Column(name = "TASK_FREQ", unique = false, nullable = true, length = 5)
    private String taskFreq;

    /**
     * 催收类型
     **/
    @Column(name = "BCM_TYPE", unique = false, nullable = true, length = 5)
    private String bcmType;

    /**
     * 任务要求完成时间（天)
     **/
    @Column(name = "FINISH_DAY", unique = false, nullable = true, length = 5)
    private String finishDay;

    /**
     * 催收提醒模板
     **/
    @Column(name = "BCM_TEMPLATE", unique = false, nullable = true, length = 500)
    private String bcmTemplate;

    /**
     * 规则状态
     **/
    @Column(name = "RULE_STATUS", unique = false, nullable = true, length = 5)
    private String ruleStatus;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public CfgPlaBcmRule() {
        // default implementation ignored
    }

    /**
     * @param ruleId
     */
    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    /**
     * @return ruleId
     */
    public String getRuleId() {
        return this.ruleId;
    }

    /**
     * @param ruleName
     */
    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    /**
     * @return ruleName
     */
    public String getRuleName() {
        return this.ruleName;
    }

    /**
     * @param assetType
     */
    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    /**
     * @return assetType
     */
    public String getAssetType() {
        return this.assetType;
    }

    /**
     * @param taskFreq
     */
    public void setTaskFreq(String taskFreq) {
        this.taskFreq = taskFreq;
    }

    /**
     * @return taskFreq
     */
    public String getTaskFreq() {
        return this.taskFreq;
    }

    /**
     * @param bcmType
     */
    public void setBcmType(String bcmType) {
        this.bcmType = bcmType;
    }

    /**
     * @return bcmType
     */
    public String getBcmType() {
        return this.bcmType;
    }

    /**
     * @param finishDay
     */
    public void setFinishDay(String finishDay) {
        this.finishDay = finishDay;
    }

    /**
     * @return finishDay
     */
    public String getFinishDay() {
        return this.finishDay;
    }

    /**
     * @param bcmTemplate
     */
    public void setBcmTemplate(String bcmTemplate) {
        this.bcmTemplate = bcmTemplate;
    }

    /**
     * @return bcmTemplate
     */
    public String getBcmTemplate() {
        return this.bcmTemplate;
    }

    /**
     * @param ruleStatus
     */
    public void setRuleStatus(String ruleStatus) {
        this.ruleStatus = ruleStatus;
    }

    /**
     * @return ruleStatus
     */
    public String getRuleStatus() {
        return this.ruleStatus;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }
}