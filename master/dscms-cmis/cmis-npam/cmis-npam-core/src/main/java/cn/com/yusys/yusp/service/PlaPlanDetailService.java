/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.PlaPlanDetail;
import cn.com.yusys.yusp.dto.AccLoanDto;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.repository.mapper.PlaPlanDetailMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.vo.PlaPlanDetailVo;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaPlanDetailService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-28 14:42:52
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaPlanDetailService {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(PlaDebtClaimReducRecordBillRelService.class);
    @Autowired
    private PlaPlanDetailMapper plaPlanDetailMapper;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private CmisBizClientService cmisBizClientService;        //调用贷款台账服务

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaPlanDetail selectByPrimaryKey(String ppdSerno) {
        return plaPlanDetailMapper.selectByPrimaryKey(ppdSerno);
    }


    /**
     * @方法名称: selectAll
     * @方法描述: 根据清收计划编号查询
     * @参数与返回说明:
     * @算法描述: 无
     * 刘权
     */

    @Transactional(readOnly = true)
    public List<PlaPlanDetail> selectByrecoverySerno(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaPlanDetail> records = (List<PlaPlanDetail>) plaPlanDetailMapper.selectByrecoverySerno(model);
        PageHelper.clearPage();
        return records;
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaPlanDetail> selectAll(QueryModel model) {
        List<PlaPlanDetail> records = (List<PlaPlanDetail>) plaPlanDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaPlanDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaPlanDetail> list = plaPlanDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaPlanDetail record) {
        return plaPlanDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaPlanDetail record) {
        return plaPlanDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaPlanDetail record) {
        return plaPlanDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaPlanDetail record) {
        return plaPlanDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String ppdSerno) {
        return plaPlanDetailMapper.deleteByPrimaryKey(ppdSerno);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据清收计划流水号删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteBySerno(String recoverySerno) {
        return plaPlanDetailMapper.deleteByPrimaryKey(recoverySerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaPlanDetailMapper.deleteByIds(ids);
    }

    /**
     * 清收计划详情模板
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportPlanDetail() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {

                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(PlaPlanDetailVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 批量插入清收计划明细表
     *
     * @param perPlanDetailList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertPlanDetail(List perPlanDetailList, String recoverySerno) throws Exception {
        List<PlaPlanDetail> planDetailList = (List<PlaPlanDetail>) BeanUtils.beansCopy(perPlanDetailList, PlaPlanDetail.class);
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        User userInfo = SessionUtils.getUserInformation();
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            PlaPlanDetailMapper plaPlanDetailMapper = sqlSession.getMapper(PlaPlanDetailMapper.class);
            for (PlaPlanDetail planDetail : planDetailList) {

                QueryModel query = new QueryModel();
                query.addCondition("billNo", planDetail.getBillNo());
                query.addCondition("recoverySerno", recoverySerno);
                List<PlaPlanDetail> plaPlanDetails = plaPlanDetailMapper.selectByModel(query);
                //数据库中已存在，更新信息
                if (CollectionUtils.isNotEmpty(plaPlanDetails)) {
                    PlaPlanDetail plaPlanDetail = plaPlanDetails.get(0);
                    throw BizException.error(null, EcnEnum.ECN060003.key, EcnEnum.ECN060003.value + planDetail.getBillNo());
                }//数据库中不存在，新增信息
                else {
                    //生成主键
                    Map paramMap = new HashMap<>();
                    String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, paramMap);

                    AccLoanDto accLoanDto = new AccLoanDto();
                    //插入Excel导入数据中的客户编号
                    accLoanDto.setCusId(planDetail.getCusId());
                    //插入Excel导入数据中的客户名称
                    accLoanDto.setCusName(planDetail.getCusName());
                    //插入Excel导入数据中的借据编号
                    accLoanDto.setBillNo(planDetail.getBillNo());
                    //插入Excel导入数据中的责任人
                    accLoanDto.setManagerId(planDetail.getManagerId());

                    ResultDto<List<AccLoanDto>> listResultDto = cmisBizClientService.selectByCusId(accLoanDto);
                    if (listResultDto != null){
                        List<AccLoanDto> accLoanDtoList = new ArrayList<>();
                        if (listResultDto.getData() != null) {
                            planDetail.setPpdSerno(pkValue);
                            //清收计划流水号
                            planDetail.setRecoverySerno(recoverySerno);
                            //登记人
                            planDetail.setInputId(userInfo.getLoginCode());
                            //登记机构
                            planDetail.setInputBrId(userInfo.getOrg().getCode());
                            //登记日期
                            planDetail.setInputDate(openDay);
                            //最近修改人
                            planDetail.setUpdId(userInfo.getLoginCode());
                            //最近修改机构
                            planDetail.setUpdBrId(userInfo.getOrg().getCode());
                            //最近修改日期
                            planDetail.setUpdDate(openDay);
                            //创建时间
                            planDetail.setCreateTime(DateUtils.getCurrTimestamp());
                            //修改时间
                            planDetail.setUpdateTime(DateUtils.getCurrTimestamp());
                            //判断贷款金额是否为空
                            if ("".equals(planDetail.getLoanAmt()) || planDetail.getLoanAmt() == null ){
                                throw BizException.error(null, EcnEnum.ECN060001.key, EcnEnum.ECN060001.value + planDetail.getBillNo());
                            }
                            //判断贷款余额是否为空
                            if ("".equals(planDetail.getLoanBalance()) || planDetail.getLoanBalance() == null){
                                throw BizException.error(null, EcnEnum.ECN060001.key, EcnEnum.ECN060001.value + planDetail.getBillNo());
                            }
                            //判断资产类型是否为空
                            if ("".equals(planDetail.getAssetType()) || planDetail.getAssetType() == null){
                                throw BizException.error(null, EcnEnum.ECN060001.key, EcnEnum.ECN060001.value + planDetail.getBillNo());
                            }
                            //判断处置方式是否为空
                            if ("".equals(planDetail.getDispMode()) || planDetail.getDispMode() == null){
                                throw BizException.error(null, EcnEnum.ECN060001.key, EcnEnum.ECN060001.value + planDetail.getBillNo());
                            }
                            //判断预计清收金额是否为空
                            if ("".equals(planDetail.getRecoveryAmt()) || planDetail.getRecoveryAmt() == null){
                                throw BizException.error(null, EcnEnum.ECN060001.key, EcnEnum.ECN060001.value + planDetail.getBillNo());
                            }
                            plaPlanDetailMapper.insertSelective(planDetail);
                        }else{
                            throw BizException.error(null, EcnEnum.ECN060023.key, EcnEnum.ECN060023.value + planDetail.getBillNo());
                        }
                    }
                }
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        return perPlanDetailList.size();
    }


}
