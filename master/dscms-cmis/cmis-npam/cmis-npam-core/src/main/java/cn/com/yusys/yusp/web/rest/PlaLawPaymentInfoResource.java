/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.PlaLawPaymentInfoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaLawPaymentInfo;
import cn.com.yusys.yusp.service.PlaLawPaymentInfoService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawPaymentInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-05 16:11:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plalawpaymentinfo")
public class PlaLawPaymentInfoResource {
    @Autowired
    private PlaLawPaymentInfoService plaLawPaymentInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaLawPaymentInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaLawPaymentInfo> list = plaLawPaymentInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaLawPaymentInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaLawPaymentInfo>> index(QueryModel queryModel) {
        List<PlaLawPaymentInfo> list = plaLawPaymentInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawPaymentInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{plpiSerno}")
    protected ResultDto<PlaLawPaymentInfo> show(@PathVariable("plpiSerno") String plpiSerno) {
        PlaLawPaymentInfo plaLawPaymentInfo = plaLawPaymentInfoService.selectByPrimaryKey(plpiSerno);
        return new ResultDto<PlaLawPaymentInfo>(plaLawPaymentInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaLawPaymentInfo> create(@RequestBody PlaLawPaymentInfo plaLawPaymentInfo) throws URISyntaxException {
        plaLawPaymentInfoService.insert(plaLawPaymentInfo);
        return new ResultDto<PlaLawPaymentInfo>(plaLawPaymentInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaLawPaymentInfo plaLawPaymentInfo) throws URISyntaxException {
        int result = plaLawPaymentInfoService.update(plaLawPaymentInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{plpiSerno}")
    protected ResultDto<Integer> delete(@PathVariable("plpiSerno") String plpiSerno) {
        int result = plaLawPaymentInfoService.deleteByPrimaryKey(plpiSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaLawPaymentInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:deleteByBrokeCaseNo
     * @函数描述:根据破产案件编号删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteByBrokeCaseNo/{brokeCaseNo}")
    protected ResultDto<Integer> deleteByBrokeCaseNo(@PathVariable("brokeCaseNo") String brokeCaseNo) {
        int result = plaLawPaymentInfoService.deleteByBrokeCaseNo(brokeCaseNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * 批量插入
     * @param plaLawPaymentInfoDto
     * @return
     * @throws URISyntaxException
     */
    @PostMapping("/batchInsert")
    protected ResultDto<Integer> batchInsert(@RequestBody PlaLawPaymentInfoDto plaLawPaymentInfoDto) throws URISyntaxException {
        int res = plaLawPaymentInfoService.batchInsert(plaLawPaymentInfoDto);
        return new ResultDto<Integer>(res);
    }
}
