/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.vo;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;
import java.math.BigDecimal;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBadDebtWriteoffBillRel
 * @类描述: pla_bad_debt_writeoff_bill_rel数据实体类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-06-09 09:34:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@ExcelCsv(namePrefix = "批量核销信息导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class PlaBadDebtWriteoffBillVo{


	/** 合同编号 **/
	@ExcelField(title = "合同编号", viewLength = 20)
	private String contNo;
	
	/** 借据编号 **/
	@ExcelField(title = "借据编号", viewLength = 20)
	private String billNo;
	
	/** 客户编号 **/
	@ExcelField(title = "客户编号", viewLength = 20)
	private String cusId;
	
	/** 客户名称 **/
	@ExcelField(title = "客户名称", viewLength = 20)
	private String cusName;
	
	/** 产品名称 **/
	@ExcelField(title = "产品名称", viewLength = 20)
	private String prdName;
	
	/** 币种 **/
	@ExcelField(title = "币种", viewLength = 20 ,dictCode = "STD_ZB_CUR_TYP")
	private String curType;
	
	/** 贷款金额 **/
	@ExcelField(title = "贷款金额", viewLength = 20 ,format = "#0.00")
	private java.math.BigDecimal loanAmt;
	
	/** 贷款余额 **/
	@ExcelField(title = "贷款余额", viewLength = 20 ,format = "#0.00")
	private java.math.BigDecimal loanBalance;
	
	/** 拖欠利息总额 **/
	@ExcelField(title = "拖欠利息总额", viewLength = 20 ,format = "#0.00")
	private java.math.BigDecimal totalTqlxAmt;
	
	/** 贷款起始日 **/
	@ExcelField(title = "贷款起始日", viewLength = 20)
	private String loanStartDate;
	
	/** 贷款到期日 **/
	@ExcelField(title = "贷款到期日", viewLength = 20)
	private String loanEndDate;
	
	/** 执行年利率 **/
	@ExcelField(title = "执行年利率", viewLength = 20)
	private java.math.BigDecimal execRateYear;
	
	/** 五级分类 **/
	@ExcelField(title = "五级分类", viewLength = 20 ,dictCode = "STD_FIVE_CLASS")
	private String fiveClass;

	/** 责任人 **/
	@ExcelField(title = "责任人", viewLength = 20)
	private String managerIdName;
	
	/** 责任机构 **/
	@ExcelField(title = "责任机构", viewLength = 30)
	private String managerBrIdName;

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public void setCurType(String curType) {
		this.curType = curType;
	}

	public void setLoanAmt(BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}

	public void setLoanBalance(BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}

	public void setTotalTqlxAmt(BigDecimal totalTqlxAmt) {
		this.totalTqlxAmt = totalTqlxAmt;
	}

	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate;
	}

	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate;
	}

	public void setExecRateYear(BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}

	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}

	public String getContNo() {
		return contNo;
	}

	public String getBillNo() {
		return billNo;
	}

	public String getCusId() {
		return cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public String getPrdName() {
		return prdName;
	}

	public String getCurType() {
		return curType;
	}

	public BigDecimal getLoanAmt() {
		return loanAmt;
	}

	public BigDecimal getLoanBalance() {
		return loanBalance;
	}

	public BigDecimal getTotalTqlxAmt() {
		return totalTqlxAmt;
	}

	public String getLoanStartDate() {
		return loanStartDate;
	}

	public String getLoanEndDate() {
		return loanEndDate;
	}

	public BigDecimal getExecRateYear() {
		return execRateYear;
	}

	public String getFiveClass() {
		return fiveClass;
	}

	public String getManagerIdName() {
		return managerIdName;
	}

	public void setManagerIdName(String managerIdName) {
		this.managerIdName = managerIdName;
	}

	public String getManagerBrIdName() {
		return managerBrIdName;
	}

	public void setManagerBrIdName(String managerBrIdName) {
		this.managerBrIdName = managerBrIdName;
	}
}