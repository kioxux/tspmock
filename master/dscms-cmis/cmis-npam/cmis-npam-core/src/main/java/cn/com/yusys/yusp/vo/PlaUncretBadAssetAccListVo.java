package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

@ExcelCsv(namePrefix = "非信贷不良资产台账列表导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class PlaUncretBadAssetAccListVo {

    /*
        客户编号
         */
    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /*
    客户名称
     */
    @ExcelField(title = "客户名称", viewLength = 20)
    private String cusName;

    /*
    交易编号
     */
    @ExcelField(title = "交易编号", viewLength = 20)
    private String tranId;

    /*
    交易金额
     */
    @ExcelField(title = "交易金额", viewLength = 20 ,format = "#0.00")
    private java.math.BigDecimal tranAmt;

    /*
    币种
     */
    @ExcelField(title = "币种", dictCode = "STD_ZB_CUR_TYP", viewLength = 20)
    private String curType;

    /*
   投资资产名称
    */
    @ExcelField(title = "投资资产名称", viewLength = 20)
    private String investAssetName;

    /*
  交易品种名称
   */
    @ExcelField(title = "交易品种名称", viewLength = 20)
    private String tranBizName;

    /*
  交易成交日期
   */
    @ExcelField(title = "交易成交日期", viewLength = 20)
    private String tranDealDate;

    /*
  交易到期日期
   */
    @ExcelField(title = "交易到期日期", viewLength = 20)
    private String tranEndDate;

    /*
  处置方式
   */
    @ExcelField(title = "处置方式", dictCode = "STD_DISP_TYPE" , viewLength = 20)
    private String dispMode;

    /*
  登记人
   */
    @ExcelField(title = "登记人", viewLength = 20)
    private String inputId;

    /*
  登记机构
   */
    @ExcelField(title = "登记机构", viewLength = 20)
    private String inputBrId;

    /*
  登记日期
   */
    @ExcelField(title = "登记日期", viewLength = 20)
    private String inputDate;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    public BigDecimal getTranAmt() {
        return tranAmt;
    }

    public void setTranAmt(BigDecimal tranAmt) {
        this.tranAmt = tranAmt;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getInvestAssetName() {
        return investAssetName;
    }

    public void setInvestAssetName(String investAssetName) {
        this.investAssetName = investAssetName;
    }

    public String getTranBizName() {
        return tranBizName;
    }

    public void setTranBizName(String tranBizName) {
        this.tranBizName = tranBizName;
    }

    public String getTranDealDate() {
        return tranDealDate;
    }

    public void setTranDealDate(String tranDealDate) {
        this.tranDealDate = tranDealDate;
    }

    public String getTranEndDate() {
        return tranEndDate;
    }

    public void setTranEndDate(String tranEndDate) {
        this.tranEndDate = tranEndDate;
    }

    public String getDispMode() {
        return dispMode;
    }

    public void setDispMode(String dispMode) {
        this.dispMode = dispMode;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }
}
