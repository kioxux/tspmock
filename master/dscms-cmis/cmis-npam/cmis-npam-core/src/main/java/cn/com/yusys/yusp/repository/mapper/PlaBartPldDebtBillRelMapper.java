/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaBartPldDebtBillRel;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBartPldDebtBillRelMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface PlaBartPldDebtBillRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    PlaBartPldDebtBillRel selectByPrimaryKey(@Param("ptbrSerno") String ptbrSerno);

    /**
     * @方法名称: selectByBill
     * @方法描述: 根据借据号查询借据是否已存在
     * @参数与返回说明:
     * @算法描述: 无
     */
    PlaBartPldDebtBillRel selectByBill(@Param("billNo") String billNo,@Param("pdraiSerno") String pdraiSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PlaBartPldDebtBillRel> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insert(PlaBartPldDebtBillRel record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insertSelective(PlaBartPldDebtBillRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPrimaryKey(PlaBartPldDebtBillRel record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPrimaryKeySelective(PlaBartPldDebtBillRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    int deleteByPrimaryKey(@Param("ptbrSerno") String ptbrSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int deleteByIds(@Param("ids") String ids);

    /**
     * 根据以物抵债申请流水获取关联借据信息
     *
     * @author jijian_yx
     * @date 2021/6/10 15:51
     **/
    List<PlaBartPldDebtBillRel> queryByPtaiSerno(@Param("pdraiSerno") String pdraiSerno);


    /**
     * 根据以物抵债流水号删除关联借据信息
     *
     * @author jijian_yx
     * @date 2021/6/11 9:32
     **/
    int deleteBillRelByPrimaryKey(@Param("pdraiSerno") String pdraiSerno);

    /**
     * 根据以物抵债流水号统计关联借据的金额信息
     *
     * @author jijian_yx
     * @date 2021/6/11 9:32
     **/
    HashMap<String, Object> countAmtById(@Param("pdraiSerno") String pdraiSerno);

    /**
     * 根据以物抵债流水号删除多笔关联借据信息
     *
     * @author jijian_yx
     * @date 2021/6/11 9:33
     **/
    int deleteBillRelByIds(@Param("ids") String ids);


    /**
     * 根据业务流水号获取关联借据信息
     *
     * @author liuquan
     **/
    List<PlaBartPldDebtBillRel> queryByPdraiSerno(QueryModel model);

    /**
     * 根据以物抵债流水号查询借据
     *
     * @param pdraiSerno
     * @return
     */
    List<PlaBartPldDebtBillRel> queryBillByPdraiSerno(@Param("pdraiSerno") String pdraiSerno);
}