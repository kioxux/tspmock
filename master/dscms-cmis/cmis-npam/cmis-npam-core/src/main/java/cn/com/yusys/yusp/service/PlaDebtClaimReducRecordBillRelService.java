/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisNpamConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AccLoanDto;
import cn.com.yusys.yusp.dto.PlaDebtClaimReducRecordBillRelDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.ib1241.Ib1241Server;
import cn.com.yusys.yusp.service.server.ln3100.Ln3100Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.PlaDebtClaimReducRecordBillRelMapper;

import static cn.com.yusys.yusp.constants.CmisNpamConstants.HX_STATUS_01;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaDebtClaimReducRecordBillRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-08 19:44:40
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaDebtClaimReducRecordBillRelService {

    private static final Logger log = LoggerFactory.getLogger(PlaDebtClaimReducRecordBillRelService.class);

    @Autowired
    private PlaDebtClaimReducRecordBillRelMapper plaDebtClaimReducRecordBillRelMapper;
    @Autowired
    private PlaDebtClaimReducRecordAppInfoService plaDebtClaimReducRecordAppInfoService;
    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;
    @Autowired
    private PlaDebtClaimReducRecordBillRelService plaDebtClaimReducRecordBillRelService;

    @Autowired
    private CmisBizClientService cmisBizClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private Ln3100Server ln3100Server;

    @Autowired
    private Ib1241Server ib1241Server;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaDebtClaimReducRecordBillRel selectByPrimaryKey(String pdcrrbrSerno) {
        return plaDebtClaimReducRecordBillRelMapper.selectByPrimaryKey(pdcrrbrSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaDebtClaimReducRecordBillRel> selectAll(QueryModel model) {
        List<PlaDebtClaimReducRecordBillRel> records = (List<PlaDebtClaimReducRecordBillRel>) plaDebtClaimReducRecordBillRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaDebtClaimReducRecordBillRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaDebtClaimReducRecordBillRel> list = plaDebtClaimReducRecordBillRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaDebtClaimReducRecordBillRelDto record) {
        return plaDebtClaimReducRecordBillRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaDebtClaimReducRecordBillRel record) {
        return plaDebtClaimReducRecordBillRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaDebtClaimReducRecordBillRel record) {
        return plaDebtClaimReducRecordBillRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaDebtClaimReducRecordBillRel record) {
        return plaDebtClaimReducRecordBillRelMapper.updateByPrimaryKeySelective(record);
    }


    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaDebtClaimReducRecordBillRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPdcrrbrSernolete(PlaDebtClaimReducRecordBillRel plaDebtClaimReducRecordBillRel) {
        int count = 0;
        count = plaDebtClaimReducRecordBillRelMapper.deleteByPrimaryKey(plaDebtClaimReducRecordBillRel);
        if (count > 0) {
            Map map = plaDebtClaimReducRecordBillRelMapper.selectBySum(plaDebtClaimReducRecordBillRel.getPdcrraiSerno());
            if (CollectionUtils.nonEmpty(map)) {
                // 贷款总金额
                BigDecimal loanAmt = (BigDecimal) map.get("loanAmt");
                // 贷款总余额
                BigDecimal loanBalance = (BigDecimal) map.get("loanBalance");
                // 拖欠利息总额
                BigDecimal totalTqlxAmt = (BigDecimal) map.get("totalTqlxAmt");
                //减免本金合计
                BigDecimal reducCapAmt = (BigDecimal) map.get("reducCapAmt");
                //减免欠息合计
                BigDecimal reducDebitInt = (BigDecimal) map.get("reducDebitInt");
                //减免罚息合计
                BigDecimal reducPenalInt = (BigDecimal) map.get("reducPenalInt");
                //减免复息合计
                BigDecimal reducCompoundInt = (BigDecimal) map.get("reducCompoundInt");
                //减免费用合计
                BigDecimal reducCostAmt = (BigDecimal) map.get("reducCostAmt");
                //减免费用合计
                BigDecimal reducTotlAmt = (BigDecimal) map.get("reducTotlAmt");
                //获取当前业务流水号下的数据
                PlaDebtClaimReducRecordAppInfo plaDebtClaimReducRecordAppInfo = plaDebtClaimReducRecordAppInfoService.selectByPrimaryKey(plaDebtClaimReducRecordBillRel.getPdcrraiSerno());
                if (plaDebtClaimReducRecordAppInfo != null) {
                    plaDebtClaimReducRecordAppInfo.setLoanAmt(loanAmt);
                    plaDebtClaimReducRecordAppInfo.setLoanBalance(loanBalance);
                    plaDebtClaimReducRecordAppInfo.setTotalTqlxAmt(totalTqlxAmt);
                    plaDebtClaimReducRecordAppInfo.setReducCapAmt(reducCapAmt);
                    plaDebtClaimReducRecordAppInfo.setReducDebitInt(reducDebitInt);
                    plaDebtClaimReducRecordAppInfo.setReducPenalInt(reducPenalInt);
                    plaDebtClaimReducRecordAppInfo.setReducCompoundInt(reducCompoundInt);
                    plaDebtClaimReducRecordAppInfo.setReducCostAmt(reducCostAmt);
                    plaDebtClaimReducRecordAppInfo.setReducTotlAmt(reducTotlAmt);
                    List<PlaDebtClaimReducRecordBillRel> list= plaDebtClaimReducRecordBillRelMapper.selectByPdcrraiSerno(plaDebtClaimReducRecordBillRel.getPdcrraiSerno());
                    if(CollectionUtils.isEmpty(list)){
                        plaDebtClaimReducRecordAppInfo.setPdcraiSerno("");
                        plaDebtClaimReducRecordAppInfo.setCusId("");
                        plaDebtClaimReducRecordAppInfo.setCusName("");
                    }
                    count = plaDebtClaimReducRecordAppInfoService.update(plaDebtClaimReducRecordAppInfo);
                }
            }
        }
        return count;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int savePlaDebtClaimReducRecordBillRel(List<PlaDebtClaimReducRecordBillRelDto> plaDebtClaimReducRecordBillRelList) {
        int count = 0;
        // 记账申请流水号
        String pdcrraiSerno = "";
        String cusId = "";
        String cusName = "";
        // 减免申请流水号
        String pdcraiSerno = "";
        if (CollectionUtils.nonEmpty(plaDebtClaimReducRecordBillRelList)) {
            Map<String, Object> map = new HashMap<>();
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            for (PlaDebtClaimReducRecordBillRelDto record : plaDebtClaimReducRecordBillRelList) {
                if (record != null) {
                    pdcrraiSerno = record.getPdcrraiSerno();
                    pdcraiSerno = record.getPdcraiSerno();
                    cusId = record.getCusId();
                    cusName = record.getCusName();
                    map.put("pdcrraiSerno", pdcrraiSerno);
                    PlaDebtClaimReducRecordBillRel plaDebtClaimReducRecordBillRel = plaDebtClaimReducRecordBillRelMapper.selectByPdcrrbrSerno(map);
                    map.put("billNo", record.getBillNo());
                    PlaDebtClaimReducRecordBillRel RecordBillRel = plaDebtClaimReducRecordBillRelMapper.selectByPdcrrbrSerno(map);
                    map.clear();
                    if (plaDebtClaimReducRecordBillRel != null && !record.getCusId().equals(plaDebtClaimReducRecordBillRel.getCusId())) {
                        throw BizException.error(null, "99999", "只能引入相同客户数据");
                    } else if (RecordBillRel != null) {
                        throw BizException.error(null, "99999", "不能引入相同借据");
                    } else {
                        record.setPdcrrbrSerno(StringUtils.getUUID());
                        record.setInputId(inputId);
                        record.setInputBrId(inputBrId);
                        record.setInputDate(openDay);
                        record.setCreateTime(createTime);
                        record.setUpdId(inputId);
                        record.setUpdBrId(inputBrId);
                        record.setUpdDate(openDay);
                        record.setUpdateTime(createTime);
                        //默认新增记账状态为待记账
                        record.setRecordStatus(HX_STATUS_01);
                        plaDebtClaimReducRecordBillRelMapper.insert(record);
                    }
                }
            }
            map.clear();
            map = plaDebtClaimReducRecordBillRelMapper.selectBySum(pdcrraiSerno);
            if (CollectionUtils.nonEmpty(map)) {
                // 贷款总金额
                BigDecimal loanAmt = (BigDecimal) map.get("loanAmt");
                // 贷款总余额
                BigDecimal loanBalance = (BigDecimal) map.get("loanBalance");
                // 拖欠利息总额
                BigDecimal totalTqlxAmt = (BigDecimal) map.get("totalTqlxAmt");
                //减免本金合计
                BigDecimal reducCapAmt = (BigDecimal) map.get("reducCapAmt");
                //减免欠息合计
                BigDecimal reducDebitInt = (BigDecimal) map.get("reducDebitInt");
                //减免罚息合计
                BigDecimal reducPenalInt = (BigDecimal) map.get("reducPenalInt");
                //减免复息合计
                BigDecimal reducCompoundInt = (BigDecimal) map.get("reducCompoundInt");
                //减免费用合计
                BigDecimal reducCostAmt = (BigDecimal) map.get("reducCostAmt");
                //减免费用合计
                BigDecimal reducTotlAmt = (BigDecimal) map.get("reducTotlAmt");
                //获取当前业务流水号下的数据
                PlaDebtClaimReducRecordAppInfo plaDebtClaimReducRecordAppInfo = plaDebtClaimReducRecordAppInfoService.selectByPrimaryKey(pdcrraiSerno);
                if (plaDebtClaimReducRecordAppInfo != null) {
                    plaDebtClaimReducRecordAppInfo.setCusId(cusId);
                    plaDebtClaimReducRecordAppInfo.setCusName(cusName);
                    plaDebtClaimReducRecordAppInfo.setLoanAmt(loanAmt);
                    plaDebtClaimReducRecordAppInfo.setLoanBalance(loanBalance);
                    plaDebtClaimReducRecordAppInfo.setTotalTqlxAmt(totalTqlxAmt);
                    plaDebtClaimReducRecordAppInfo.setReducCapAmt(reducCapAmt);
                    plaDebtClaimReducRecordAppInfo.setReducDebitInt(reducDebitInt);
                    plaDebtClaimReducRecordAppInfo.setReducPenalInt(reducPenalInt);
                    plaDebtClaimReducRecordAppInfo.setReducCompoundInt(reducCompoundInt);
                    plaDebtClaimReducRecordAppInfo.setReducCostAmt(reducCostAmt);
                    plaDebtClaimReducRecordAppInfo.setReducTotlAmt(reducTotlAmt);
                    plaDebtClaimReducRecordAppInfo.setPdcraiSerno(pdcraiSerno);
                    plaDebtClaimReducRecordAppInfo.setUpdId(inputId);
                    plaDebtClaimReducRecordAppInfo.setUpdBrId(inputBrId);
                    plaDebtClaimReducRecordAppInfo.setUpdDate(openDay);
                    plaDebtClaimReducRecordAppInfo.setUpdateTime(createTime);
                    count = plaDebtClaimReducRecordAppInfoService.updateSelective(plaDebtClaimReducRecordAppInfo);
                } else {
                    PlaDebtClaimReducRecordAppInfo plaDebtClaimReducRecordAppInfo1 = new PlaDebtClaimReducRecordAppInfo();
                    plaDebtClaimReducRecordAppInfo1.setPdcrraiSerno(pdcrraiSerno);
                    plaDebtClaimReducRecordAppInfo1.setCusId(cusId);
                    plaDebtClaimReducRecordAppInfo1.setCusName(cusName);
                    plaDebtClaimReducRecordAppInfo1.setLoanAmt(loanAmt);
                    plaDebtClaimReducRecordAppInfo1.setLoanBalance(loanBalance);
                    plaDebtClaimReducRecordAppInfo1.setTotalTqlxAmt(totalTqlxAmt);
                    plaDebtClaimReducRecordAppInfo1.setReducCapAmt(reducCapAmt);
                    plaDebtClaimReducRecordAppInfo1.setReducDebitInt(reducDebitInt);
                    plaDebtClaimReducRecordAppInfo1.setReducPenalInt(reducPenalInt);
                    plaDebtClaimReducRecordAppInfo1.setReducCompoundInt(reducCompoundInt);
                    plaDebtClaimReducRecordAppInfo1.setReducCostAmt(reducCostAmt);
                    plaDebtClaimReducRecordAppInfo1.setReducTotlAmt(reducTotlAmt);
                    plaDebtClaimReducRecordAppInfo1.setInputId(inputId);
                    plaDebtClaimReducRecordAppInfo1.setInputBrId(inputBrId);
                    plaDebtClaimReducRecordAppInfo1.setInputDate(openDay);
                    plaDebtClaimReducRecordAppInfo1.setCreateTime(createTime);
                    plaDebtClaimReducRecordAppInfo1.setPdcraiSerno(pdcraiSerno);
                    plaDebtClaimReducRecordAppInfo1.setUpdId(inputId);
                    plaDebtClaimReducRecordAppInfo1.setUpdBrId(inputBrId);
                    plaDebtClaimReducRecordAppInfo1.setUpdDate(openDay);
                    plaDebtClaimReducRecordAppInfo1.setUpdateTime(createTime);
                    count = plaDebtClaimReducRecordAppInfoService.insertSelective(plaDebtClaimReducRecordAppInfo1);
                }
            }
        }
        return count;
    }

    /**
     * @方法名称: selectByPdcraiSerno
     * @方法描述: 根据减免流水查询借据
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaDebtClaimReducRecordBillRel> selectByPdcraiSerno(String pdcraiSerno) {
        return plaDebtClaimReducRecordBillRelMapper.selectByPdcrraiSerno(pdcraiSerno);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/30 20:36
     * @注释 冲正处理
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto ib1241(PlaDebtClaimReducRecordBillRel plaDebtClaimReducRecordBillRel) {
        try {
            // 调用核心冲正交易
            Ib1241ReqDto ib1241ReqDto = new Ib1241ReqDto();
            // 原交易日期
            ib1241ReqDto.setYjiaoyrq(plaDebtClaimReducRecordBillRel.getRecordTranTime());
            // 原柜员流水
            ib1241ReqDto.setYgyliush(plaDebtClaimReducRecordBillRel.getRecordTranSerno());
            ResultDto<Ib1241RespDto> ib1241RespDtoResultDto = ib1241Server.sendIb1241(ib1241ReqDto);
            String ib1241Code = Optional.ofNullable(ib1241RespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String ib1241Meesage = Optional.ofNullable(ib1241RespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            List<AccLoanDto> accLoanDtoList = new ArrayList<>();
            AccLoanDto accLoanDto = new AccLoanDto();
            if (Objects.equals(ib1241Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                if (ib1241RespDtoResultDto != null) {
                    // 已发送核心冲正成功
                    plaDebtClaimReducRecordBillRel.setRecordStatus(CmisNpamConstants.HX_STATUS_01);
                    plaDebtClaimReducRecordBillRel.setRecordBackSerno(ib1241RespDtoResultDto.getData().getChzhlshu());
                    plaDebtClaimReducRecordBillRel.setRecordBackTime(ib1241RespDtoResultDto.getData().getChzhriqi());
                    int i = plaDebtClaimReducRecordBillRelMapper.updateByPrimaryKeySelective(plaDebtClaimReducRecordBillRel);
                    //记账成功更新台账信息
                    // 借据编号
                    String billNo = plaDebtClaimReducRecordBillRel.getBillNo();
                    ResultDto<Ln3100RespDto> ln3100RespDto = ln3100Server.queryHXBill(billNo);
                    // 借据金额
                    BigDecimal loanAmt = BigDecimal.ZERO;
                    // 贷款余额
                    BigDecimal loanBalance = BigDecimal.ZERO;
                    // 欠息
                    BigDecimal debitInt = BigDecimal.ZERO;
                    // 罚息
                    BigDecimal penalInt = BigDecimal.ZERO;
                    // 复息
                    BigDecimal compoundInt = BigDecimal.ZERO;
                    log.info("ln3100接口返回：" + ln3100RespDto);
                    String ln3100Code = Optional.ofNullable(ln3100RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String ln3100Meesage = Optional.ofNullable(ln3100RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    if (Objects.equals(ln3100Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                        if (ln3100RespDto.getData() != null) {
                            loanAmt = ln3100RespDto.getData().getJiejuuje();
                            loanBalance = ln3100RespDto.getData().getZhchbjin().add(ln3100RespDto.getData().getYuqibjin());
                            debitInt = ln3100RespDto.getData().getYsyjlixi().add(ln3100RespDto.getData().getCsyjlixi()).add(ln3100RespDto.getData().getYsqianxi()).add(ln3100RespDto.getData().getCsqianxi());
                            penalInt = ln3100RespDto.getData().getYsyjfaxi().add(ln3100RespDto.getData().getCsyjfaxi()).add(ln3100RespDto.getData().getYshofaxi()).add(ln3100RespDto.getData().getCshofaxi());
                            compoundInt = ln3100RespDto.getData().getYingjifx().add(ln3100RespDto.getData().getFuxiiiii());
                            accLoanDto.setBillNo(billNo);
                            accLoanDto.setLoanBalance(loanBalance);
                            accLoanDto.setDebitInt(debitInt);
                            accLoanDto.setPenalInt(penalInt);
                            accLoanDto.setCompoundInt(compoundInt);
                            accLoanDtoList.add(accLoanDto);
                        }
                    }
                    // 调用biz接口更新金额
                    cmisBizClientService.updateAccLoan(accLoanDtoList);
                    if (i != 1) {
                        return new ResultDto(null).message("冲正处理失败");
                    }
                }
            }else{
                return new ResultDto(null).message("冲正处理失败");
            }
            if(StringUtils.nonBlank(plaDebtClaimReducRecordBillRel.getPdcrraiSerno())) {
                QueryModel model = new QueryModel();
                model.addCondition("pdcraiSerno", plaDebtClaimReducRecordBillRel.getPdcrraiSerno());
                model.addCondition("recordStatus", CmisNpamConstants.HX_STATUS_03);
                // 如果借据都记账成功则修改记账状态
                List<PlaDebtClaimReducRecordBillRel> plaDebtClaimReducRecordBillRelList = plaDebtClaimReducRecordBillRelService.selectByModel(model);
                // 如果不存在记账失败情况更新记账状态
                if (CollectionUtils.isEmpty(plaDebtClaimReducRecordBillRelList)) {
                    PlaDebtClaimReducRecordAppInfo appInfo = plaDebtClaimReducRecordAppInfoService.selectByPrimaryKey(plaDebtClaimReducRecordBillRel.getPdcrraiSerno());
                    appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_01);
                    plaDebtClaimReducRecordAppInfoService.update(appInfo);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResultDto(null).message(e.getMessage());
        }
        return new ResultDto(null).message("冲正处理成功");
    }
}
