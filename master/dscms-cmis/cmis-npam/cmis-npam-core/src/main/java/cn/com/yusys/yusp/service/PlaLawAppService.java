/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import org.codehaus.commons.compiler.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaLawApp;
import cn.com.yusys.yusp.repository.mapper.PlaLawAppMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-28 20:00:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaLawAppService {

    @Autowired
    private PlaLawAppMapper plaLawAppMapper;
    @Autowired
    private PlaLawBillRelService plaLawBillRelService;
    @Autowired
    private PlaLawGrtRelService plaLawGrtRelService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaLawApp selectByPrimaryKey(String appSerno) {
        return plaLawAppMapper.selectByPrimaryKey(appSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaLawApp> selectAll(QueryModel model) {
        List<PlaLawApp> records = (List<PlaLawApp>) plaLawAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaLawApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaLawApp> list = plaLawAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaLawApp record) {
        record.setInputBrId(SessionUtils.getUserInformation().getOrg().getCode());
        record.setInputId(SessionUtils.getUserInformation().getLoginCode());
        record.setManagerBrId(SessionUtils.getUserInformation().getLoginCode());
        record.setManagerId(SessionUtils.getUserInformation().getOrg().getCode());
        record.setInputDate(DateUtils.getCurrDateStr());
        return plaLawAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaLawApp record) {
        return plaLawAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaLawApp record) {
        return plaLawAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaLawApp record) {
        // 申请时间
        String openDay =stringRedisTemplate.opsForValue().get("openDay");
        // 申请人
        String updId = "";
        // 申请机构
        String updBrId = "";
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            updId = userInfo.getLoginCode();
            // 申请机构
            updBrId = userInfo.getOrg().getCode();
        }
        // 创建时间
        Date updateTime = DateUtils.getCurrTimestamp();
        record.setUpdId(updId);
        record.setUpdBrId(updBrId);
        record.setUpdDate(openDay);
        record.setUpdateTime(updateTime);
        return plaLawAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String appSerno) {
        return plaLawAppMapper.deleteByPrimaryKey(appSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaLawAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: savePlaLawApp
     * @方法描述: 保存
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */

    public PlaLawApp savePlaLawApp(PlaLawApp record) {
        // 流水号
        String serno = "";
        int count = 0;
        if (record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                openDay = DateUtils.getCurrDateStr();
            }
            serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.NPAM_APP_SERNO , new HashMap<>());
            record.setAppSerno(serno);
            // 登记人
            record.setInputId(inputId);
            // 登记机构
            record.setInputBrId(inputBrId);
            // 登记日期
            record.setInputDate(openDay);
            // 登记时间
            record.setCreateTime(createTime);
            // 修改人
            record.setUpdId(inputId);
            // 修改机构
            record.setUpdBrId(inputBrId);
            // 修改日期
            record.setUpdDate(openDay);
            // 修改时间
            record.setUpdateTime(createTime);
            record.setApproveStatus("000");
            count = insertSelective(record);
            if (count < 1) {
                throw new YuspException(EcnEnum.ECN069999.key, EcnEnum.ECN060001.value);
            }
        } else {
            throw new YuspException(EcnEnum.ECN069999.key, EcnEnum.ECN060001.value);
        }
        return record;
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除诉讼主表，同时删除关联表数据
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    @Transactional
    public int delete(String appSerno) {
        int count = 0;
        if (StringUtils.nonBlank(appSerno)) {
            count = deleteByPrimaryKey(appSerno);
            if (count > 0) {
                plaLawBillRelService.deleteByAppSerno(appSerno);
                plaLawGrtRelService.deleteByAppSerno(appSerno);
            } else {
                throw new YuspException(EcnEnum.ECN069999.key, EcnEnum.ECN060002.value);
            }
        }
        return count;
    }
}
