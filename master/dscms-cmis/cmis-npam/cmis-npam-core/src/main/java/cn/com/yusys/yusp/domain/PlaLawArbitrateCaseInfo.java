/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawArbitrateCaseInfo
 * @类描述: pla_law_arbitrate_case_info数据实体类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-05 15:48:24
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_law_arbitrate_case_info")
public class PlaLawArbitrateCaseInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 仲裁案件流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "ARBITRATE_CASE_NO")
    private String arbitrateCaseNo;

    /**
     * 案件编号
     **/
    @Column(name = "CASE_NO", unique = false, nullable = true, length = 40)
    private String caseNo;

    /**
     * 客户编号
     **/
    @Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
    private String cusId;

    /**
     * 客户名称
     **/
    @Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
    private String cusName;

    /**
     * 仲裁标的
     **/
    @Column(name = "ARBITRATE_SUBJECT_AMT", unique = false, nullable = true, length = 16)
    private BigDecimal arbitrateSubjectAmt;

    /**
     * 本金
     **/
    @Column(name = "ARBITRATE_CAP_AMT", unique = false, nullable = true, length = 16)
    private BigDecimal arbitrateCapAmt;

    /**
     * 拖欠利息总额
     **/
    @Column(name = "TOTAL_TQLX_AMT", unique = false, nullable = true, length = 16)
    private BigDecimal totalTqlxAmt;

    /**
     * 仲裁机构
     **/
    @Column(name = "ARBITRATE_ORG", unique = false, nullable = true, length = 80)
    private String arbitrateOrg;

    /**
     * 是否有仲裁协议
     **/
    @Column(name = "IS_HAVE_ARBITRATE_AGR", unique = false, nullable = true, length = 5)
    private String isHaveArbitrateAgr;

    /**
     * 仲裁协议是否有效
     **/
    @Column(name = "IS_ARBITRATE_AGR_VLD", unique = false, nullable = true, length = 5)
    private String isArbitrateAgrVld;

    /**
     * 受理时间
     **/
    @Column(name = "ACCEPT_DATE", unique = false, nullable = true, length = 10)
    private String acceptDate;

    /**
     * 案件进程
     **/
    @Column(name = "CASE_PROCESS", unique = false, nullable = true, length = 5)
    private String caseProcess;

    /**
     * 仲裁事由
     **/
    @Column(name = "ARBITRATE_RESN", unique = false, nullable = true, length = 2000)
    private String arbitrateResn;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaLawArbitrateCaseInfo() {
        // default implementation ignored
    }

    /**
     * @param arbitrateCaseNo
     */
    public void setArbitrateCaseNo(String arbitrateCaseNo) {
        this.arbitrateCaseNo = arbitrateCaseNo;
    }

    /**
     * @return arbitrateCaseNo
     */
    public String getArbitrateCaseNo() {
        return this.arbitrateCaseNo;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param arbitrateSubjectAmt
     */
    public void setArbitrateSubjectAmt(BigDecimal arbitrateSubjectAmt) {
        this.arbitrateSubjectAmt = arbitrateSubjectAmt;
    }

    /**
     * @return arbitrateSubjectAmt
     */
    public BigDecimal getArbitrateSubjectAmt() {
        return this.arbitrateSubjectAmt;
    }

    /**
     * @param arbitrateCapAmt
     */
    public void setArbitrateCapAmt(BigDecimal arbitrateCapAmt) {
        this.arbitrateCapAmt = arbitrateCapAmt;
    }

    /**
     * @return arbitrateCapAmt
     */
    public BigDecimal getArbitrateCapAmt() {
        return this.arbitrateCapAmt;
    }

    public BigDecimal getTotalTqlxAmt() {
        return totalTqlxAmt;
    }

    public void setTotalTqlxAmt(BigDecimal totalTqlxAmt) {
        this.totalTqlxAmt = totalTqlxAmt;
    }

    /**
     * @param arbitrateOrg
     */
    public void setArbitrateOrg(String arbitrateOrg) {
        this.arbitrateOrg = arbitrateOrg;
    }

    /**
     * @return arbitrateOrg
     */
    public String getArbitrateOrg() {
        return this.arbitrateOrg;
    }

    /**
     * @param isHaveArbitrateAgr
     */
    public void setIsHaveArbitrateAgr(String isHaveArbitrateAgr) {
        this.isHaveArbitrateAgr = isHaveArbitrateAgr;
    }

    /**
     * @return isHaveArbitrateAgr
     */
    public String getIsHaveArbitrateAgr() {
        return this.isHaveArbitrateAgr;
    }

    /**
     * @param isArbitrateAgrVld
     */
    public void setIsArbitrateAgrVld(String isArbitrateAgrVld) {
        this.isArbitrateAgrVld = isArbitrateAgrVld;
    }

    /**
     * @return isArbitrateAgrVld
     */
    public String getIsArbitrateAgrVld() {
        return this.isArbitrateAgrVld;
    }

    /**
     * @param acceptDate
     */
    public void setAcceptDate(String acceptDate) {
        this.acceptDate = acceptDate;
    }

    /**
     * @return acceptDate
     */
    public String getAcceptDate() {
        return this.acceptDate;
    }

    /**
     * @param caseProcess
     */
    public void setCaseProcess(String caseProcess) {
        this.caseProcess = caseProcess;
    }

    /**
     * @return caseProcess
     */
    public String getCaseProcess() {
        return this.caseProcess;
    }

    /**
     * @param arbitrateResn
     */
    public void setArbitrateResn(String arbitrateResn) {
        this.arbitrateResn = arbitrateResn;
    }

    /**
     * @return arbitrateResn
     */
    public String getArbitrateResn() {
        return this.arbitrateResn;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getCaseNo() {
        return caseNo;
    }
}