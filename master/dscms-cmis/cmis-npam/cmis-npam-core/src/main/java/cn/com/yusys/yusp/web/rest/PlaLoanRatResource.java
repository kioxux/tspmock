/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaLoanRat;
import cn.com.yusys.yusp.service.PlaLoanRatService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLoanRatResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 16:11:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plaloanrat")
public class PlaLoanRatResource {
    @Autowired
    private PlaLoanRatService plaLoanRatService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaLoanRat>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaLoanRat> list = plaLoanRatService.selectAll(queryModel);
        return new ResultDto<List<PlaLoanRat>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<PlaLoanRat>> index(@RequestBody QueryModel queryModel) {
        List<PlaLoanRat> list = plaLoanRatService.selectByModel(queryModel);
        return new ResultDto<List<PlaLoanRat>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByBillNoList")
    protected ResultDto<List<PlaLoanRat>> selectByBillNoList(@RequestBody QueryModel queryModel) {
        List<PlaLoanRat> list = plaLoanRatService.selectByBillNoList(queryModel);
        return new ResultDto<List<PlaLoanRat>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{plrSerno}")
    protected ResultDto<PlaLoanRat> show(@PathVariable("plrSerno") String plrSerno) {
        PlaLoanRat plaLoanRat = plaLoanRatService.selectByPrimaryKey(plrSerno);
        return new ResultDto<PlaLoanRat>(plaLoanRat);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<PlaLoanRat> create(@RequestBody PlaLoanRat plaLoanRat) throws URISyntaxException {
        plaLoanRatService.insert(plaLoanRat);
        return new ResultDto<PlaLoanRat>(plaLoanRat);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaLoanRat plaLoanRat) throws URISyntaxException {
        int result = plaLoanRatService.update(plaLoanRat);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{plrSerno}")
    protected ResultDto<Integer> delete(@PathVariable("plrSerno") String plrSerno) {
        int result = plaLoanRatService.deleteByPrimaryKey(plrSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaLoanRatService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:export
     * @函数描述:导出
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("导出利息试算结果")
    @PostMapping("/export")
    protected ResultDto<ProgressDto> export(@RequestBody QueryModel queryModel) {
        return ResultDto.success(plaLoanRatService.export(queryModel));
    }

    /**
     * @函数名称:index
     * @函数描述:调用核心利息试算
     * @参数与返回说明:
     * @param
     *
     * @算法描述:
     */
    @PostMapping("/queryhxloanrat")
    protected ResultDto<String> queryHXLoanrat(@RequestBody List<PlaLoanRat> billNoList) throws Exception {
        return plaLoanRatService.queryHXLoanrat(billNoList);
    }
}
