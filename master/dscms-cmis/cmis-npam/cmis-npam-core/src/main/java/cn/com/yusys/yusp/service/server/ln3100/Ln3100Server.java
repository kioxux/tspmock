package cn.com.yusys.yusp.service.server.ln3100;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.service.Dscms2CoreLnClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.function.Supplier;

/**
 * @version 1.0.0
 * @项目名称: cmis-psp模块
 * @类名称: Xddh0017Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: wrw
 * @创建时间: 2021-08-30 09:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Ln3100Server {
    private static final Logger logger = LoggerFactory.getLogger(Ln3100Server.class);
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;
    /**
     * 查询核心借据
     * @param billNo
     * @return
     */
    public ResultDto<Ln3100RespDto> queryHXBill(String billNo) {
        Supplier<ResultDto<Ln3100RespDto>> supplierResp = ResultDto<Ln3100RespDto>::new;
        ResultDto<Ln3100RespDto> ln3100RespDtos = supplierResp.get();
        if (billNo != null) {
            Supplier<Ln3100ReqDto> supplier = Ln3100ReqDto::new;
            Ln3100ReqDto ln3100ReqDto = supplier.get();
            //查询笔数
            ln3100ReqDto.setChxunbis(1);
            //贷款借据号
            ln3100ReqDto.setDkjiejuh(billNo);
            //贷款账号
            ln3100ReqDto.setDkzhangh("");
            //起始笔数
            ln3100ReqDto.setQishibis(0);
            // 调用核心接口，进行债权减免发送核心记账
            ln3100RespDtos = dscms2CoreLnClientService.ln3100(ln3100ReqDto);
        }
        return ln3100RespDtos;
    }
}

