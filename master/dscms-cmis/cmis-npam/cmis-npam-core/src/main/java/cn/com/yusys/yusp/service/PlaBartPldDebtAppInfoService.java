/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisNpamConstants;
import cn.com.yusys.yusp.domain.PlaBartPldDebtAppInfo;
import cn.com.yusys.yusp.domain.PlaBartPldDebtBillRel;
import cn.com.yusys.yusp.dto.AccLoanDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3300.Da3300ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3300.Da3300RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3301.req.Da3301ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3301.req.Listnm0;
import cn.com.yusys.yusp.dto.client.esb.core.da3301.req.Listnm1;
import cn.com.yusys.yusp.dto.client.esb.core.da3301.resp.Da3301RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.PlaBartPldDebtAppInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.server.ib1241.Ib1241Server;
import cn.com.yusys.yusp.service.server.ln3100.Ln3100Server;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.math.BigDecimal;
import java.util.*;
import java.util.function.Supplier;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBartPldDebtAppInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaBartPldDebtAppInfoService {
    @Autowired
    private PlaBartPldDebtAppInfoMapper plaBartPldDebtAppInfoMapper;
    @Autowired
    private PlaBartPldDebtBillRelService plaBartPldDebtBillRelService;
    @Autowired
    private Dscms2CoreDaClientService dscms2CoreDaClientService;
    @Autowired
    private PlaDebtClaimReducRecordAppInfoService plaDebtClaimReducRecordAppInfoService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;
    @Autowired
    private CmisBizClientService cmisBizClientService;
    @Autowired
    private Ln3100Server ln3100Server;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private Ib1241Server ib1241Server;
    // 日志
    private static final Logger log = LoggerFactory.getLogger(PlaBartPldDebtAppInfoService.class);

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaBartPldDebtAppInfo selectByPrimaryKey(String pdraiSerno) {
        PlaBartPldDebtAppInfo plaBartPldDebtAppInfo = plaBartPldDebtAppInfoMapper.selectByPrimaryKey(pdraiSerno);
        return plaBartPldDebtAppInfo;
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaBartPldDebtAppInfo> selectAll(QueryModel model) {
        List<PlaBartPldDebtAppInfo> records = (List<PlaBartPldDebtAppInfo>) plaBartPldDebtAppInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaBartPldDebtAppInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaBartPldDebtAppInfo> list = plaBartPldDebtAppInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaBartPldDebtAppInfo record) {
        if (record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            record.setInputId(inputId);
            record.setInputBrId(inputBrId);
            record.setInputDate(openDay);
            record.setApproveStatus("000");
            record.setCreateTime(createTime);
        }
        return plaBartPldDebtAppInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaBartPldDebtAppInfo record) {
        return plaBartPldDebtAppInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaBartPldDebtAppInfo record) {
        // 修改
        String updId = "";
        // 修改
        String updBrId = "";
        // 修改时间
        Date updateTime = DateUtils.getCurrTimestamp();
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            updId = userInfo.getLoginCode();
            // 申请机构
            updBrId = userInfo.getOrg().getCode();
        }
        record.setUpdId(updId);
        record.setUpdBrId(updBrId);
        record.setUpdDate(openDay);
        record.setUpdateTime(updateTime);
        return plaBartPldDebtAppInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaBartPldDebtAppInfo record) {
        return plaBartPldDebtAppInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pdraiSerno) {
        /** 删除关联借据信息 **/
        plaBartPldDebtBillRelService.deleteBillRelByPrimaryKey(pdraiSerno);
        /** 删除申请信息 **/
        return plaBartPldDebtAppInfoMapper.deleteByPrimaryKey(pdraiSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        /** 删除多笔关联借据信息 **/
        plaBartPldDebtBillRelService.deleteBillRelByIds(ids);
        /** 删除多笔申请信息 **/
        return plaBartPldDebtAppInfoMapper.deleteByIds(ids);
    }

    /**
     * 新增客户以物抵债申请
     *
     * @author jijian_yx
     * @date 2021/6/9 20:04
     **/
    public String save(PlaBartPldDebtAppInfo appInfo) {
        // TODO 申请信息待对接补全
        String pdraiSerno = "";
        if (null == appInfo.getPdraiSerno()) {
            pdraiSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.NPAM_PDRAI_SERNO, new HashMap<>());
            appInfo.setPdraiSerno(pdraiSerno);
        }
        // 创建时间
        Date createTime = DateUtils.getCurrTimestamp();
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        appInfo.setInputDate(openDay);
        appInfo.setCreateTime(createTime);
        appInfo.setUpdDate(openDay);
        appInfo.setUpdateTime(createTime);
        appInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
        appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_01);
        appInfo.setRegiStatus(CmisNpamConstants.HX_REGI_STATUS_01);
        int result = plaBartPldDebtAppInfoMapper.insertSelective(appInfo);
        if (result > 0) {
            return pdraiSerno;
        } else {
            return "";
        }
    }

    /**
     * 分页查询以物抵债登记申请
     *
     * @author jijian_yx
     * @date 2021/6/9 20:06
     **/
    public List<PlaBartPldDebtAppInfo> toSignlist(QueryModel model) {
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_000992111);
        return plaBartPldDebtAppInfoMapper.selectByModel(model);
    }

    /**
     * 分页查询以物抵债历史
     *
     * @author jijian_yx
     * @date 2021/6/9 20:06
     **/
    public List<PlaBartPldDebtAppInfo> doneSignlist(QueryModel model) {
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_996997998);
        return plaBartPldDebtAppInfoMapper.selectByModel(model);
    }

    /**
     * 分页查询以物抵债台账信息
     *
     * @author jijian_yx
     * @date 2021/6/9 20:06
     **/
    public List<PlaBartPldDebtAppInfo> getBartPldDebtAccList(QueryModel model) {
        model.getCondition().put("approveStatus", CmisCommonConstants.WF_STATUS_997);
        return plaBartPldDebtAppInfoMapper.selectByModel(model);
    }

    /**
     * 以物抵债发送核心登记
     *
     * @author jijian_yx
     * @date 2021/6/9 20:55
     **/
    public ResultDto<String> sendToHXDJ(PlaBartPldDebtAppInfo appInfo) {
        ResultDto<String> resultDto = new ResultDto<>();
        if (appInfo != null) {
            // 费用计算
            // 费用
            BigDecimal expense = BigDecimal.ZERO;
            List<PlaBartPldDebtBillRel> plaBartPldDebtBillRelList = plaBartPldDebtBillRelService.queryBillByPdraiSerno(appInfo.getPdraiSerno());
            for (PlaBartPldDebtBillRel plaBartPldDebtBillRel : plaBartPldDebtBillRelList) {
                if(plaBartPldDebtBillRel.getResistExpense()!=null){
                    expense = expense.add(plaBartPldDebtBillRel.getResistExpense());
                }
            }
            Da3300ReqDto da3300ReqDto = new Da3300ReqDto();
            // 开户操作标识 1、录入 2、修改 3、复核 4、直通
            da3300ReqDto.setDkkhczbz("4");
            //操作方式 A 增加
            da3300ReqDto.setCaozfshi("A");
            // 抵债资产编号
            da3300ReqDto.setDzzcbhao(appInfo.getPldimnNo());
            // 抵债资产名称
            da3300ReqDto.setDzzcminc(appInfo.getPldimnMemo());
            // 入账机构 todo
            da3300ReqDto.setRuzjigou(EsbEnum.BRCHNO_CORE.key);
            // 客户号
            da3300ReqDto.setKehuhaoo(appInfo.getCusId());
            // 客户名称
            da3300ReqDto.setKehmingc(appInfo.getCusName());
            // 抵债物资类别
            da3300ReqDto.setDzwzleib(appInfo.getResistDebtType());
            // 产权证明种类todo
            da3300ReqDto.setCqzmzlei("1");
            // 币种
            da3300ReqDto.setHuobdhao("01");
            // 代保管抵债资产金额
            da3300ReqDto.setDbgdzcje(appInfo.getEvalValue());
            // 抵债资产单位
            da3300ReqDto.setDzzcdanw("");
            // 抵债资产数量
            da3300ReqDto.setDzzcshul(Long.valueOf(appInfo.getQnt()));
            // 评估价格
            da3300ReqDto.setPingjiaz(appInfo.getRcvValue().subtract(expense));//appInfo.getEvalValue());
            // 抵债资产入账价值
            da3300ReqDto.setDzzcrzjz(BigDecimal.ZERO);
            da3300ReqDto.setZhaiyoms("");
            da3300ReqDto.setBeizhuuu("");
            log.info("以物抵债登记接口da3300请求参数" + da3300ReqDto);
            // 调用核心接口，进行抵债资产登记  登记成功更改记账状态
            ResultDto<Da3300RespDto> da3300RespDto = dscms2CoreDaClientService.da3300(da3300ReqDto);
            log.info("以物抵债登记接口da3300返回结果" + da3300RespDto);
            String da3300Code = Optional.ofNullable(da3300RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String da3300Meesage = Optional.ofNullable(da3300RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            log.info("以物抵债登记接口da3300返回message:" + da3300Meesage);
            if (Objects.equals(da3300Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                if(da3300RespDto.getData() !=null){
                    appInfo.setRegiStatus(CmisNpamConstants.HX_REGI_STATUS_02);//登记状态已登记
                    appInfo.setRegiTranSerno(da3300RespDto.getData().getJiaoyils());
                    appInfo.setRegiTranTime(da3300RespDto.getData().getJiaoyirq());
                    updateSelective(appInfo);
                    resultDto.setMessage("登记成功");
                }else{
                    appInfo.setRegiStatus(CmisNpamConstants.HX_REGI_STATUS_04);// 登记状态为登记失败
                    updateSelective(appInfo);
                    resultDto.setCode(da3300Code);
                    resultDto.setMessage(da3300Meesage);
                }
            } else {
                appInfo.setRegiStatus(CmisNpamConstants.HX_REGI_STATUS_04);// 登记状态为登记失败
                updateSelective(appInfo);
                resultDto.setCode(da3300Code);
                resultDto.setMessage(da3300Meesage);
            }
        } else {
            throw BizException.error(null, EcnEnum.ECN060005.key, EcnEnum.ECN060005.value);
        }
        return resultDto;
    }

    /**
     * 以物抵债发送核心记账
     *
     * @author liuquan
     **/
    public ResultDto<String> sendToHXJZ(PlaBartPldDebtAppInfo appInfo) {
        ResultDto<String> resultDto = new ResultDto<>();
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        if (appInfo != null) {
            List<Listnm1> listnm1List = new ArrayList<>();
            Supplier<Ln3100RespDto> suppliers = Ln3100RespDto::new;
            Da3301ReqDto da3301ReqDto = new Da3301ReqDto();
            List<Listnm0> listnm0List = new ArrayList<Listnm0>();
            // 费用
            BigDecimal expense = BigDecimal.ZERO;
            //抵债总本金
            BigDecimal resistCap = BigDecimal.ZERO;
            List<AccLoanDto> accLoanDtoList =new ArrayList<>();
            AccLoanDto accLoanDto = new AccLoanDto();
            List<PlaBartPldDebtBillRel> plaBartPldDebtBillRelList = plaBartPldDebtBillRelService.queryBillByPdraiSerno(appInfo.getPdraiSerno());
            for (PlaBartPldDebtBillRel plaBartPldDebtBillRel : plaBartPldDebtBillRelList) {
                if(plaBartPldDebtBillRel.getResistExpense()!=null){
                    expense = expense.add(plaBartPldDebtBillRel.getResistExpense());
                }
                if(plaBartPldDebtBillRel.getResistCap()!=null){
                    resistCap = resistCap.add(plaBartPldDebtBillRel.getResistCap()).add(plaBartPldDebtBillRel.getResistInt()!=null?plaBartPldDebtBillRel.getResistInt():BigDecimal.ZERO);
                }
                Listnm1 listnm1 = new Listnm1();
                listnm1.setDkjiejuh(plaBartPldDebtBillRel.getBillNo());
                listnm1.setZhchbjin(plaBartPldDebtBillRel.getResistCap());
                listnm1.setYsqianxi(plaBartPldDebtBillRel.getResistInt());
                listnm1List.add(listnm1);
            }
            //业务操作标志
            da3301ReqDto.setDaikczbz("3");
            // 抵债资产入账方式
            da3301ReqDto.setDizruzfs("0");
            //抵债资产编号
            da3301ReqDto.setDzzcbhao(appInfo.getPldimnNo());
            //抵债资产名称
            da3301ReqDto.setDzzcminc(appInfo.getPldimnMemo());
            //营业机构
            da3301ReqDto.setYngyjigo(EsbEnum.BRCHNO_CORE.key);
            //客户号
            da3301ReqDto.setKehuhaoo(appInfo.getCusId());
            //客户名称
            da3301ReqDto.setKehmingc(appInfo.getCusName());
            //待处理抵债资产金额
            da3301ReqDto.setDcldzcje(appInfo.getRcvValue().subtract(expense));
            //还费金额
            da3301ReqDto.setHfeijine(BigDecimal.ZERO);
            //资金去向
            da3301ReqDto.setZijinqux("1");
            //抵债资产取得方式 字典项不一样，01-2协商，02-1裁定
            da3301ReqDto.setDzzcqdfs("01".equals(appInfo.getResistDebtGetMode()) ? "2" : "1");
            //还款金额
            da3301ReqDto.setHuankjee(resistCap);
            // 抵债暂收账号
            da3301ReqDto.setDzzszhao("");
            //抵债暂收账号子序号
            da3301ReqDto.setDzzszzxh("");
            // 费用信息
            da3301ReqDto.setListnm0(listnm0List);
            //抵债资产与贷款关联表
            da3301ReqDto.setListnm1(listnm1List);
            log.info("以物抵债记账接口da3301请求参数" + da3301ReqDto);
            // 调用核心接口，进行抵债资产登记  登记成功更改记账状态
            ResultDto<Da3301RespDto> da3301RespDto = dscms2CoreDaClientService.da3301(da3301ReqDto);
            log.info("以物抵债记账接口da3301返回参数" + da3301RespDto);
            String da3301Code = Optional.ofNullable(da3301RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String da3301Meesage = Optional.ofNullable(da3301RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            log.info("以物抵债记账接口da3301messgge" + da3301Meesage);
            if (Objects.equals(da3301Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                if(da3301RespDto.getData()!= null){
                    appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_03);//记账成功
                    appInfo.setRecordDate(openDay);//记账时间
                    appInfo.setRecordTranSerno(da3301RespDto.getData().getJiaoyils());
                    appInfo.setRecordTranTime(da3301RespDto.getData().getJiaoyirq());
                    updateSelective(appInfo);
                    resultDto.setMessage("记账成功");
                    //记账成功更新台账信息
                    for(PlaBartPldDebtBillRel plaBartPldDebtBillRels : plaBartPldDebtBillRelList){
                        // 借据编号
                        String billNo= plaBartPldDebtBillRels.getBillNo();
                        ResultDto<Ln3100RespDto> ln3100RespDto =ln3100Server.queryHXBill(billNo);
                        // 借据金额
                        BigDecimal loanAmt = BigDecimal.ZERO;
                        // 贷款余额
                        BigDecimal loanBalance = BigDecimal.ZERO;
                        // 欠息
                        BigDecimal debitInt = BigDecimal.ZERO;
                        // 罚息
                        BigDecimal penalInt = BigDecimal.ZERO;
                        // 复息
                        BigDecimal compoundInt = BigDecimal.ZERO;
                        log.info("ln3100接口返回：" + ln3100RespDto);
                        String ln3100Code = Optional.ofNullable(ln3100RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                        String ln3100Meesage = Optional.ofNullable(ln3100RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                        if (Objects.equals(ln3100Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                            if(ln3100RespDto.getData() != null){
                                loanAmt = ln3100RespDto.getData().getJiejuuje();
                                loanBalance = ln3100RespDto.getData().getZhchbjin().add(ln3100RespDto.getData().getYuqibjin());
                                debitInt = ln3100RespDto.getData().getYsyjlixi().add(ln3100RespDto.getData().getCsyjlixi()).add(ln3100RespDto.getData().getYsqianxi()).add(ln3100RespDto.getData().getCsqianxi());
                                penalInt = ln3100RespDto.getData().getYsyjfaxi().add(ln3100RespDto.getData().getCsyjfaxi()).add(ln3100RespDto.getData().getYshofaxi()).add(ln3100RespDto.getData().getCshofaxi());
                                compoundInt = ln3100RespDto.getData().getYingjifx().add(ln3100RespDto.getData().getFuxiiiii());
                                accLoanDto.setBillNo(billNo);
                                accLoanDto.setLoanBalance(loanBalance);
                                accLoanDto.setDebitInt(debitInt);
                                accLoanDto.setPenalInt(penalInt);
                                accLoanDto.setCompoundInt(compoundInt);
                                accLoanDtoList.add(accLoanDto);
                            }
                        }
                    }
                    // 调用biz接口更新金额
                    cmisBizClientService.updateAccLoan(accLoanDtoList);
                }else{
                    appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_04);// 记账失败
                    appInfo.setRecordDate(openDay);//记账时间
                    updateSelective(appInfo);
                    resultDto.setCode(da3301Code);
                    resultDto.setMessage(da3301Meesage);
                }
            } else {
                appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_04);// 记账失败
                appInfo.setRecordDate(openDay);//记账时间
                updateSelective(appInfo);
                resultDto.setMessage("记账失败");
                resultDto.setCode(da3301Code);
                resultDto.setMessage(da3301Meesage);
            }
        } else {
            throw BizException.error(null, EcnEnum.ECN060005.key, EcnEnum.ECN060005.value);
        }
        return resultDto;
    }


    /**
     * @创建人
     * @创建时间
     * @注释 冲正处理
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto ib1241(PlaBartPldDebtAppInfo plaBartPldDebtAppInfo) {
        try {
            // 调用核心冲正交易
            // 交易流水号
            String ygyliush ="";
            // 交易日期
            String yjiaoyrq ="";
            // 判断的登记冲正还是记账冲正
            if((CmisNpamConstants.HX_STATUS_01.equals(plaBartPldDebtAppInfo.getRecordStatus())&&CmisNpamConstants.HX_REGI_STATUS_02.equals(plaBartPldDebtAppInfo.getRegiStatus()))||(CmisNpamConstants.HX_STATUS_04.equals(plaBartPldDebtAppInfo.getRecordStatus())&&CmisNpamConstants.HX_REGI_STATUS_02.equals(plaBartPldDebtAppInfo.getRegiStatus()))){
                ygyliush = plaBartPldDebtAppInfo.getRegiTranSerno();
                yjiaoyrq =  plaBartPldDebtAppInfo.getRegiTranTime();

            }else if(CmisNpamConstants.HX_STATUS_03.equals(plaBartPldDebtAppInfo.getRecordStatus())){
                ygyliush = plaBartPldDebtAppInfo.getRecordTranSerno();
                yjiaoyrq =  plaBartPldDebtAppInfo.getRecordTranTime();
            }
            // 调用核心冲正交易
            Ib1241ReqDto ib1241ReqDto = new Ib1241ReqDto();
            // 原交易日期
            ib1241ReqDto.setYjiaoyrq(yjiaoyrq);
            // 原柜员流水
            ib1241ReqDto.setYgyliush(ygyliush);
            ResultDto<Ib1241RespDto> ib1241RespDtoResultDto = ib1241Server.sendIb1241(ib1241ReqDto);
            String ib1241Code = Optional.ofNullable(ib1241RespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String ib1241Meesage = Optional.ofNullable(ib1241RespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            if (Objects.equals(ib1241Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                if(ib1241RespDtoResultDto.getData() !=null){
                    if((CmisNpamConstants.HX_STATUS_01.equals(plaBartPldDebtAppInfo.getRecordStatus()) && CmisNpamConstants.HX_REGI_STATUS_02.equals(plaBartPldDebtAppInfo.getRegiStatus()))||CmisNpamConstants.HX_STATUS_04.equals(plaBartPldDebtAppInfo.getRecordStatus()) ){
                        // 已发送核心冲正成功
                        plaBartPldDebtAppInfo.setRegiStatus(CmisNpamConstants.HX_REGI_STATUS_01);
                        // 冲正流水号
                        plaBartPldDebtAppInfo.setRecordBackSerno(ib1241RespDtoResultDto.getData().getChzhlshu());
                        // 冲正日期
                        plaBartPldDebtAppInfo.setRecordBackTime(ib1241RespDtoResultDto.getData().getChzhriqi());
                        //冲正成功更新台账信息
                        List<AccLoanDto> accLoanDtoList =new ArrayList<>();
                        AccLoanDto accLoanDto = new AccLoanDto();
                        List<PlaBartPldDebtBillRel> plaBartPldDebtBillRelList = plaBartPldDebtBillRelService.queryBillByPdraiSerno(plaBartPldDebtAppInfo.getPdraiSerno());
                        for(PlaBartPldDebtBillRel plaBartPldDebtBillRels : plaBartPldDebtBillRelList){
                            // 借据编号
                            String billNo= plaBartPldDebtBillRels.getBillNo();
                            ResultDto<Ln3100RespDto> ln3100RespDto =ln3100Server.queryHXBill(billNo);
                            // 借据金额
                            BigDecimal loanAmt = BigDecimal.ZERO;
                            // 贷款余额
                            BigDecimal loanBalance = BigDecimal.ZERO;
                            // 欠息
                            BigDecimal debitInt = BigDecimal.ZERO;
                            // 罚息
                            BigDecimal penalInt = BigDecimal.ZERO;
                            // 复息
                            BigDecimal compoundInt = BigDecimal.ZERO;
                            log.info("ln3100接口返回：" + ln3100RespDto);
                            String ln3100Code = Optional.ofNullable(ln3100RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                            String ln3100Meesage = Optional.ofNullable(ln3100RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                            if (Objects.equals(ln3100Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                                if(ln3100RespDto.getData() != null){
                                    loanAmt = ln3100RespDto.getData().getJiejuuje();
                                    loanBalance = ln3100RespDto.getData().getZhchbjin().add(ln3100RespDto.getData().getYuqibjin());
                                    debitInt = ln3100RespDto.getData().getYsyjlixi().add(ln3100RespDto.getData().getCsyjlixi()).add(ln3100RespDto.getData().getYsqianxi()).add(ln3100RespDto.getData().getCsqianxi());
                                    penalInt = ln3100RespDto.getData().getYsyjfaxi().add(ln3100RespDto.getData().getCsyjfaxi()).add(ln3100RespDto.getData().getYshofaxi()).add(ln3100RespDto.getData().getCshofaxi());
                                    compoundInt = ln3100RespDto.getData().getYingjifx().add(ln3100RespDto.getData().getFuxiiiii());
                                    accLoanDto.setBillNo(billNo);
                                    accLoanDto.setLoanBalance(loanBalance);
                                    accLoanDto.setDebitInt(debitInt);
                                    accLoanDto.setPenalInt(penalInt);
                                    accLoanDto.setCompoundInt(compoundInt);
                                    accLoanDtoList.add(accLoanDto);
                                }
                            }
                        }
                        // 调用biz接口更新金额
                        cmisBizClientService.updateAccLoan(accLoanDtoList);
                    }else if(CmisNpamConstants.HX_STATUS_03.equals(plaBartPldDebtAppInfo.getRecordStatus())){
                        // 已发送核心冲正成功
                        plaBartPldDebtAppInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_01);
                        // 冲正流水号
                        plaBartPldDebtAppInfo.setRecordBackSerno(ib1241RespDtoResultDto.getData().getChzhlshu());
                        // 冲正日期
                        plaBartPldDebtAppInfo.setRecordBackTime(ib1241RespDtoResultDto.getData().getChzhriqi());
                    }
                }else{
                    return new ResultDto(null).message("冲正处理失败");
                }
                int i = plaBartPldDebtAppInfoMapper.updateByPrimaryKeySelective(plaBartPldDebtAppInfo);
                if (i != 1) {
                    return new ResultDto(null).message("冲正处理失败");
                }
            }else{
                return new ResultDto(null).message("冲正处理失败"+ib1241Meesage);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResultDto(null).message(e.getMessage());
        }
        return new ResultDto(null).message("冲正处理成功");
    }
}
