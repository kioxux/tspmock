/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.PlaLawBillRel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaLawArbitrteBillRel;
import cn.com.yusys.yusp.service.PlaLawArbitrteBillRelService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawArbitrteBillRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-05 16:54:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plalawarbitrtebillrel")
public class PlaLawArbitrteBillRelResource {
    @Autowired
    private PlaLawArbitrteBillRelService plaLawArbitrteBillRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaLawArbitrteBillRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaLawArbitrteBillRel> list = plaLawArbitrteBillRelService.selectAll(queryModel);
        return new ResultDto<List<PlaLawArbitrteBillRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaLawArbitrteBillRel>> index(QueryModel queryModel) {
        List<PlaLawArbitrteBillRel> list = plaLawArbitrteBillRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawArbitrteBillRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<PlaLawArbitrteBillRel> show(@PathVariable("serno") String serno) {
        PlaLawArbitrteBillRel plaLawArbitrteBillRel = plaLawArbitrteBillRelService.selectByPrimaryKey(serno);
        return new ResultDto<PlaLawArbitrteBillRel>(plaLawArbitrteBillRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaLawArbitrteBillRel> create(@RequestBody PlaLawArbitrteBillRel plaLawArbitrteBillRel) throws URISyntaxException {
        plaLawArbitrteBillRelService.insert(plaLawArbitrteBillRel);
        return new ResultDto<PlaLawArbitrteBillRel>(plaLawArbitrteBillRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaLawArbitrteBillRel plaLawArbitrteBillRel) throws URISyntaxException {
        int result = plaLawArbitrteBillRelService.update(plaLawArbitrteBillRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody PlaLawArbitrteBillRel plaLawArbitrteBillRel) {
        int result = plaLawArbitrteBillRelService.delete(plaLawArbitrteBillRel);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaLawArbitrteBillRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 批量引入合同借据信息
     * @param lawBillRels
     * @return
     */
    @PostMapping("/saveContNo")
    protected ResultDto<Integer> saveBatchContNo(@RequestBody List<PlaLawArbitrteBillRel> lawBillRels) {
        Integer list = plaLawArbitrteBillRelService.saveBatchContNo(lawBillRels);
        return new ResultDto< Integer>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryplaLawArbitrteBillRelList")
    protected ResultDto<List<PlaLawArbitrteBillRel>> queryplaLawArbitrteBillRelList(@RequestBody QueryModel queryModel) {
        List<PlaLawArbitrteBillRel> list = plaLawArbitrteBillRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawArbitrteBillRel>>(list);
    }
}
