/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaBcmRel;
import cn.com.yusys.yusp.service.PlaBcmRelService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBcmRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-04 11:12:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "催收关联业务表")
@RestController
@RequestMapping("/api/plabcmrel")
public class PlaBcmRelResource {
    @Autowired
    private PlaBcmRelService plaBcmRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaBcmRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaBcmRel> list = plaBcmRelService.selectAll(queryModel);
        return new ResultDto<List<PlaBcmRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaBcmRel>> index(QueryModel queryModel) {
        List<PlaBcmRel> list = plaBcmRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaBcmRel>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     * 刘权
     */
    @ApiOperation("催收关联业务分页查询")
    @PostMapping("/queryAll")
    protected ResultDto<List<PlaBcmRel>> queryAll(@RequestBody QueryModel queryModel) {
        List<PlaBcmRel> list = plaBcmRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaBcmRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pbrcSerno}")
    protected ResultDto<PlaBcmRel> show(@PathVariable("pbrcSerno") String pbrcSerno) {
        PlaBcmRel plaBcmRel = plaBcmRelService.selectByPrimaryKey(pbrcSerno);
        return new ResultDto<PlaBcmRel>(plaBcmRel);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaBcmRel> create(@RequestBody PlaBcmRel plaBcmRel) throws URISyntaxException {
        plaBcmRelService.insert(plaBcmRel);
        return new ResultDto<PlaBcmRel>(plaBcmRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaBcmRel plaBcmRel) throws URISyntaxException {
        int result = plaBcmRelService.update(plaBcmRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pbrcSerno}")
    protected ResultDto<Integer> delete(@PathVariable("pbrcSerno") String pbrcSerno) {
        int result = plaBcmRelService.deleteByPrimaryKey(pbrcSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaBcmRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
