/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaLawyerInfo;
import cn.com.yusys.yusp.service.PlaLawyerInfoService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawyerInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 刘权
 * @创建时间: 2021-06-08 13:49:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(value = "律师信息表")
@RestController
@RequestMapping("/api/plalawyerinfo")
public class PlaLawyerInfoResource {
    @Autowired
    private PlaLawyerInfoService plaLawyerInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaLawyerInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaLawyerInfo> list = plaLawyerInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaLawyerInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaLawyerInfo>> index(QueryModel queryModel) {
        List<PlaLawyerInfo> list = plaLawyerInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawyerInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     * 刘权
     */
    @ApiOperation("律师信息列表查询")
    @PostMapping("/queryAll")
    protected ResultDto<List<PlaLawyerInfo>> queryAll(@RequestBody QueryModel queryModel) {
        List<PlaLawyerInfo> list = plaLawyerInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawyerInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{lawyerNo}")
    protected ResultDto<PlaLawyerInfo> show(@PathVariable("lawyerNo") String lawyerNo) {
        PlaLawyerInfo plaLawyerInfo = plaLawyerInfoService.selectByPrimaryKey(lawyerNo);
        return new ResultDto<PlaLawyerInfo>(plaLawyerInfo);
    }

    /**
     * @函数名称:show
     * @函数描述:  根据主键删除 更新律师状态
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("律师信息列表删除更新状态")
    @PostMapping("/updateByLawyerStatus")
    protected ResultDto<PlaLawyerInfo> updateByLawyerStatus(@RequestBody String lawyerNo) {
        PlaLawyerInfo plaLawyerInfo = plaLawyerInfoService.updateByLawyerStatus(lawyerNo);
        return new ResultDto<PlaLawyerInfo>(plaLawyerInfo);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaLawyerInfo> create(@RequestBody PlaLawyerInfo plaLawyerInfo) throws URISyntaxException {
        plaLawyerInfoService.insert(plaLawyerInfo);
        return new ResultDto<PlaLawyerInfo>(plaLawyerInfo);
    }

    /**
     * @函数名称:insert
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:  律师信息列表新增
     * @算法描述:
     * 刘权
     */
    @ApiOperation("律师信息列表新增")
    @PostMapping("/insert")
    protected ResultDto<PlaLawyerInfo> insert(@RequestBody PlaLawyerInfo plaLawyerInfo) throws URISyntaxException {
        plaLawyerInfoService.save(plaLawyerInfo);
        return new ResultDto<PlaLawyerInfo>(plaLawyerInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaLawyerInfo plaLawyerInfo) throws URISyntaxException {
        int result = plaLawyerInfoService.update(plaLawyerInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:updateByLawyerNo
     * @函数描述: 对象修改
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("律师信息列表修改")
    @PostMapping("/updateByLawyerNo")
    protected ResultDto<Integer> updateByLawyerNo(@RequestBody PlaLawyerInfo plaLawyerInfo) throws URISyntaxException {
        int result = plaLawyerInfoService.updateSelective(plaLawyerInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody PlaLawyerInfo plaLawyerInfo) {
        return new ResultDto<Integer>(plaLawyerInfoService.deleteByPrimaryKey(plaLawyerInfo.getLawyerNo()));
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaLawyerInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: showByLawyerNo
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("律师信息列表根据主键回显数据")
    @PostMapping("/showByLawyerNo")
    protected ResultDto<PlaLawyerInfo> showByLawyerNo(@RequestBody String lawyerNo) {
        PlaLawyerInfo plaLawyerInfo = plaLawyerInfoService.selectByPrimaryKey(lawyerNo);
        return new ResultDto<PlaLawyerInfo>(plaLawyerInfo);
    }

    /**
     * @函数名称: selectByLawyerName
     * @函数描述: 根据律师姓名查询
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByLawyerName")
    protected ResultDto<Integer> selectByLawyerName(@RequestBody PlaLawyerInfo plaLawyerInfo) {
        ResultDto<Integer> integerResultDto = plaLawyerInfoService.selectByLawyerName(plaLawyerInfo);
        return integerResultDto;

    }
}
