/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.PlaDebtClaimReducAppInfoDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaDebtClaimReducAppInfo;
import cn.com.yusys.yusp.service.PlaDebtClaimReducAppInfoService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaDebtClaimReducAppInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-06-08 19:44:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pladebtclaimreducappinfo")
public class PlaDebtClaimReducAppInfoResource {
    @Autowired
    private PlaDebtClaimReducAppInfoService plaDebtClaimReducAppInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaDebtClaimReducAppInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaDebtClaimReducAppInfo> list = plaDebtClaimReducAppInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaDebtClaimReducAppInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaDebtClaimReducAppInfo>> index(QueryModel queryModel) {
        List<PlaDebtClaimReducAppInfo> list = plaDebtClaimReducAppInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaDebtClaimReducAppInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryAll")
    protected ResultDto<List<PlaDebtClaimReducAppInfo>> queryAll(@RequestBody QueryModel queryModel) {
        List<PlaDebtClaimReducAppInfo> list = plaDebtClaimReducAppInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaDebtClaimReducAppInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pdcraiSerno}")
    protected ResultDto<PlaDebtClaimReducAppInfo> show(@PathVariable("pdcraiSerno") String pdcraiSerno) {
        PlaDebtClaimReducAppInfo plaDebtClaimReducAppInfo = plaDebtClaimReducAppInfoService.selectByPrimaryKey(pdcraiSerno);
        return new ResultDto<PlaDebtClaimReducAppInfo>(plaDebtClaimReducAppInfo);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/showByPdcraiSerno")
    protected ResultDto<PlaDebtClaimReducAppInfo> showByPdcraiSerno(@RequestBody String pdcraiSerno) {
        PlaDebtClaimReducAppInfo plaDebtClaimReducAppInfo = plaDebtClaimReducAppInfoService.selectByPrimaryKey(pdcraiSerno);
        return new ResultDto<PlaDebtClaimReducAppInfo>(plaDebtClaimReducAppInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaDebtClaimReducAppInfo> create(@RequestBody PlaDebtClaimReducAppInfo plaDebtClaimReducAppInfo) throws URISyntaxException {
        plaDebtClaimReducAppInfoService.insert(plaDebtClaimReducAppInfo);
        return new ResultDto<PlaDebtClaimReducAppInfo>(plaDebtClaimReducAppInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> updatePlaDebtClaimReducAppInfo(@RequestBody PlaDebtClaimReducAppInfoDto plaDebtClaimReducAppInfoDto) throws URISyntaxException {
        int result = plaDebtClaimReducAppInfoService.updatePlaDebtClaimReducAppInfo(plaDebtClaimReducAppInfoDto);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pdcraiSerno}")
    protected ResultDto<Integer> delete(@PathVariable("pdcraiSerno") String pdcraiSerno) {
        int result = plaDebtClaimReducAppInfoService.deleteByPrimaryKey(pdcraiSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteByApproveStatus")
    protected ResultDto<Integer> deleteByApproveStatus(@RequestBody PlaDebtClaimReducAppInfo plaDebtClaimReducAppInfo) {
        int result = plaDebtClaimReducAppInfoService.deleteByApproveStatus(plaDebtClaimReducAppInfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaDebtClaimReducAppInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "债权减免列表查询")
    @PostMapping("/queryPlaDebtClaimReducAppList")
    protected ResultDto<List<PlaDebtClaimReducAppInfo>> queryPlaDebtClaimReducAppList(@RequestBody QueryModel queryModel) {
        List<PlaDebtClaimReducAppInfo> list = plaDebtClaimReducAppInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaDebtClaimReducAppInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryPlaDebtClaimReducAppInfo")
    protected ResultDto<PlaDebtClaimReducAppInfo> queryPlaDebtClaimReducAppInfo(@RequestBody PlaDebtClaimReducAppInfo plaDebtClaimReducAppInfo) {
        PlaDebtClaimReducAppInfo plaDebtClaimReducAppInfos = plaDebtClaimReducAppInfoService.selectByPrimaryKey(plaDebtClaimReducAppInfo.getPdcraiSerno());
        return new ResultDto<PlaDebtClaimReducAppInfo>(plaDebtClaimReducAppInfos);
    }



}
