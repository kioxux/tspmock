/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisNpamConstants;
import cn.com.yusys.yusp.domain.PlaBadDebtWriteoffAppBatch;
import cn.com.yusys.yusp.domain.PlaBartPldDebtAppInfo;
import cn.com.yusys.yusp.domain.PlaBartPldDebtBillRel;
import cn.com.yusys.yusp.dto.client.esb.core.da3301.req.Listnm1;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.repository.mapper.PlaBartPldDebtBillRelMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBartPldDebtBillRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaBartPldDebtBillRelService {

    @Autowired
    private PlaBartPldDebtBillRelMapper plaBartPldDebtBillRelMapper;
    @Autowired
    private PlaBartPldDebtAppInfoService plaBartPldDebtAppInfoService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaBartPldDebtBillRel selectByPrimaryKey(String ptbrSerno) {
        return plaBartPldDebtBillRelMapper.selectByPrimaryKey(ptbrSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaBartPldDebtBillRel> selectAll(QueryModel model) {
        List<PlaBartPldDebtBillRel> records = (List<PlaBartPldDebtBillRel>) plaBartPldDebtBillRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaBartPldDebtBillRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaBartPldDebtBillRel> list = plaBartPldDebtBillRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaBartPldDebtBillRel record) {
        return plaBartPldDebtBillRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaBartPldDebtBillRel record) {
        return plaBartPldDebtBillRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaBartPldDebtBillRel record) {
        return plaBartPldDebtBillRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaBartPldDebtBillRel record) {
        return plaBartPldDebtBillRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 删除关联借据后需要更新主表的金额信息
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(PlaBartPldDebtBillRel info) {
        int count =plaBartPldDebtBillRelMapper.deleteByPrimaryKey(info.getPtbrSerno());
        if(count>0){
            PlaBartPldDebtAppInfo plaBartPldDebtAppInfo = plaBartPldDebtAppInfoService.selectByPrimaryKey(info.getPdraiSerno());
            if(plaBartPldDebtAppInfo !=null){
                // 核销总金额
                BigDecimal loanBalance = new BigDecimal("0.00");
                // 核销总本金
                BigDecimal loanAmt = new BigDecimal("0.00");
                // 核销总利息
                BigDecimal totalTqlxAmt = new BigDecimal("0.00");
                Map amtMap= plaBartPldDebtBillRelMapper.countAmtById(info.getPdraiSerno());

                if(amtMap.containsKey("loanBalance")){
                    loanBalance= new BigDecimal(amtMap.get("loanBalance").toString());
                }
                if(amtMap.containsKey("loanAmt")){
                    loanAmt= new BigDecimal(amtMap.get("loanAmt").toString());
                }
                if(amtMap.containsKey("totalTqlxAmt")){
                    totalTqlxAmt=new BigDecimal(amtMap.get("totalTqlxAmt").toString());
                }
                plaBartPldDebtAppInfo.setLoanAmt(loanAmt);
                plaBartPldDebtAppInfo.setLoanBalance(loanBalance);
                plaBartPldDebtAppInfo.setTotalTqlxAmt(totalTqlxAmt);
                plaBartPldDebtAppInfoService.updateSelective(plaBartPldDebtAppInfo);
            }
        }
        return count;
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaBartPldDebtBillRelMapper.deleteByIds(ids);
    }

    /**
     * 根据以物抵债流水号删除关联借据信息
     *
     * @author jijian_yx
     * @date 2021/6/11 10:37
     **/
    public int deleteBillRelByPrimaryKey(String ptbrSerno) {
        return plaBartPldDebtBillRelMapper.deleteBillRelByPrimaryKey(ptbrSerno);
    }

    /**
     * 根据以物抵债流水号删除多笔关联借据信息
     *
     * @author jijian_yx
     * @date 2021/6/11 10:38
     **/
    public int deleteBillRelByIds(String ids) {
        return plaBartPldDebtBillRelMapper.deleteBillRelByIds(ids);
    }


    /**
     * 根据以物抵债申请流水获取关联借据信息
     *
     * @author jijian_yx
     * @date 2021/6/10 15:50
     **/
    public List<PlaBartPldDebtBillRel> queryByPtaiSerno(String pdraiSerno) {
        return plaBartPldDebtBillRelMapper.queryByPtaiSerno(pdraiSerno);
    }
    /**
     * 以物抵债申请 引入借据保存
     *
     * @author jijian_yx
     * @date 2021/6/9 20:07
     **/
    public ResultDto<String> saveData(List<PlaBartPldDebtBillRel> billRelList) throws ParseException {
        ResultDto<String> resultDto = new ResultDto<>();
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        for (PlaBartPldDebtBillRel billRel : billRelList) {
            // 判断当前行是否已存在
            PlaBartPldDebtBillRel bartPldDebtBillRel = plaBartPldDebtBillRelMapper.selectByBill(billRel.getBillNo(),billRel.getPdraiSerno());
            if (bartPldDebtBillRel != null) {
                throw BizException.error(null, EcnEnum.ECN060003.key,EcnEnum.ECN060003.value+billRel.getBillNo());
            } else {
                // 新增操作
                if (StringUtils.isEmpty(billRel.getPtbrSerno())) {
                    // 关联表流水号
                    billRel.setPtbrSerno(StringUtils.getUUID());
                }
                billRel.setInputDate(openDay);
                billRel.setUpdDate(openDay);
                insertSelective(billRel);
            }
        }
        /*PlaBartPldDebtAppInfo plaBartPldDebtAppInfo = plaBartPldDebtAppInfoService.selectByPrimaryKey(billRelList.get(0).getPtaiSerno());
        if(plaBartPldDebtAppInfo !=null){
            // 核销总金额
            BigDecimal loanBalance = new BigDecimal("0.00");
            // 核销总本金
            BigDecimal loanAmt = new BigDecimal("0.00");
            // 核销总利息
            BigDecimal totalTqlxAmt = new BigDecimal("0.00");
            Map amtMap= plaBartPldDebtBillRelMapper.countAmtById(billRelList.get(0).getPtaiSerno());

            if(amtMap.containsKey("loanBalance")){
                loanBalance= new BigDecimal(amtMap.get("loanBalance").toString());
            }
            if(amtMap.containsKey("loanAmt")){
                loanAmt= new BigDecimal(amtMap.get("loanAmt").toString());
            }
            if(amtMap.containsKey("totalTqlxAmt")){
                totalTqlxAmt=new BigDecimal(amtMap.get("totalTqlxAmt").toString());
            }
            plaBartPldDebtAppInfo.setLoanAmt(loanAmt);
            plaBartPldDebtAppInfo.setLoanBalance(loanBalance);
            plaBartPldDebtAppInfo.setTotalTqlxAmt(totalTqlxAmt);
            plaBartPldDebtAppInfoService.updateSelective(plaBartPldDebtAppInfo);
        }
//        resultDto.setCode(CmisNpamConstants.FLAG_SUCCESS);*/
        resultDto.setMessage("操作成功");
        return resultDto;
    }
    /**
     * 以物抵债申请 保存/更新关联借据信息
     *
     * @author jijian_yx
     * @date 2021/6/9 20:07
     **/
    public ResultDto<String> save(List<PlaBartPldDebtBillRel> billRelList) {
        ResultDto<String> resultDto = new ResultDto<>();
        for (PlaBartPldDebtBillRel billRel : billRelList) {
            // 判断当前行是否已存在
            PlaBartPldDebtBillRel bartPldDebtBillRel = selectByPrimaryKey(billRel.getPtbrSerno());
            if (bartPldDebtBillRel != null) {
                // 更新操作
                updateSelective(billRel);
            } else {
                // 新增操作
                if (StringUtils.isEmpty(billRel.getPtbrSerno())) {
                    // 关联表流水号
                    billRel.setPtbrSerno(StringUtils.getUUID());
                }
                insertSelective(billRel);
            }
        }
//        resultDto.setCode(CmisNpamConstants.FLAG_SUCCESS);
        resultDto.setMessage("操作成功");
        return resultDto;
    }


    /**
     * 根据业务流水号获取关联借据信息
     *
     * @author 刘权
     **/
    public List<PlaBartPldDebtBillRel> queryByPdraiSerno(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaBartPldDebtBillRel> plaBartPldDebtBillRels = plaBartPldDebtBillRelMapper.queryByPdraiSerno(model);
        PageHelper.clearPage();
        return plaBartPldDebtBillRels;
    }
    /**
     * 根据以物抵债流水号查询借据
     * @author 茂茂
     */
    public List<PlaBartPldDebtBillRel> queryBillByPdraiSerno(String PdraiSerno) {
        List<PlaBartPldDebtBillRel> billList= plaBartPldDebtBillRelMapper.queryBillByPdraiSerno(PdraiSerno);
        return billList;
    }
}
