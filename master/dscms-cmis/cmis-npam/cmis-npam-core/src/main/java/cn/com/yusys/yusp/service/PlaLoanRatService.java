/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.PlaLoanRat;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3249.req.Ln3249ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3249.resp.Ln3249RespDto;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.PlaLoanRatMapper;
import cn.com.yusys.yusp.vo.PlaLoanRatVo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Supplier;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLoanRatService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 16:11:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaLoanRatService {
    private static final Logger log = LoggerFactory.getLogger(PlaLoanRatService.class);

    @Autowired
    private PlaLoanRatMapper plaLoanRatMapper;
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private PlaDebtClaimReducRecordAppInfoService plaDebtClaimReducRecordAppInfoService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PlaLoanRat selectByPrimaryKey(String plrSerno) {
        return plaLoanRatMapper.selectByPrimaryKey(plrSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaLoanRat> selectAll(QueryModel model) {
        List<PlaLoanRat> records = (List<PlaLoanRat>) plaLoanRatMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PlaLoanRat> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaLoanRat> list = plaLoanRatMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByBillNoList
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaLoanRat> selectByBillNoList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaLoanRat>list = plaLoanRatMapper.selectByBillNoList(((List<String>)model.getCondition().get("billNo")));
        PageHelper.clearPage();
        return list;
    }
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PlaLoanRat record) {
        return plaLoanRatMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PlaLoanRat record) {
        return plaLoanRatMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PlaLoanRat record) {
        return plaLoanRatMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PlaLoanRat record) {
        return plaLoanRatMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String plrSerno) {
        return plaLoanRatMapper.deleteByPrimaryKey(plrSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaLoanRatMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: export
     * @方法描述: 导出
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ProgressDto export(QueryModel model) {
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel) object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            return selectByBillNoList(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(PlaLoanRatVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 调用核心利息试算
     * @param billNoList
     * @return
     */
    public ResultDto<String> queryHXLoanrat( List<PlaLoanRat> billNoList) throws Exception {
        ResultDto<String> resultDto = new ResultDto<>();
        Supplier <Ln3249RespDto> supplierResp = Ln3249RespDto::new;
        Ln3249RespDto ln3249RespDto =supplierResp.get();

        // 申请人
        String inputId = "";
        // 申请机构
        String inputBrId = "";
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        // 创建时间
        Date createTime = DateUtils.getCurrTimestamp();
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if(userInfo != null){

            // 申请人
            inputId = userInfo.getLoginCode();
            // 申请机构
            inputBrId = userInfo.getOrg().getCode();
        }
        //计息天数
        String dayNum = "";
        if(CollectionUtils.nonEmpty(billNoList)){
            if(CollectionUtils.nonEmpty(billNoList)){
                for(PlaLoanRat plaLoanRat:billNoList){
                    String billNo = plaLoanRat.getBillNo();
                    String loanRatDare= DateUtils.formatDate10To8(plaLoanRat.getLoanRatDare());
                    String loanRatStartDare=  DateUtils.formatDate10To8(plaLoanRat.getLoanRatStartDare());
                    ln3249RespDto = sendHXln3249(billNo,loanRatStartDare,loanRatDare);
                    if(ln3249RespDto !=null){
                        // 贷款余额
                        //plaLoanRat.setLoanBalance(ln3249RespDto.getDaikuyue());
                        // 起息时间
                        loanRatDare=DateUtils.formatDate8To10(loanRatDare);
                        plaLoanRat.setLoanRatStartDare(loanRatDare);
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        // 利息
                        plaLoanRat.setIntAmt(ln3249RespDto.getQianxiii());
                        // 罚息
                        plaLoanRat.setPenalInt(ln3249RespDto.getFaxiiiii());
                        // 复利
                        plaLoanRat.setCompoundInt(ln3249RespDto.getFuxiiiii());

                        PlaLoanRat plaLoanRats =plaLoanRatMapper.selectByBillNo(billNo);
                        plaLoanRat.setUpdId(inputId);
                        plaLoanRat.setUpdBrId(inputBrId);
                        plaLoanRat.setUpdDate(openDay);
                        plaLoanRat.setUpdateTime(createTime);
                        if(plaLoanRats != null){
                            plaLoanRat.setPlrSerno(plaLoanRats.getPlrSerno());
                            updateSelective(plaLoanRat);
                        }else{
                            //试算流水号
                            plaLoanRat.setPlrSerno(StringUtils.getUUID());
                            plaLoanRat.setInputId(inputId);
                            plaLoanRat.setInputBrId(inputBrId);
                            plaLoanRat.setInputDate(openDay);
                            plaLoanRat.setCreateTime(createTime);
                            insert(plaLoanRat);
                        }
                    }else{
                        resultDto.setCode(EcnEnum.ECN060000.key);
                        resultDto.setMessage(EcnEnum.ECN060000.value);
                    }
                }
            }
        }

        return resultDto;
    }

    /**
     * 调用
     * @param billNo
     * @return
     */
    public Ln3249RespDto sendHXln3249(String billNo,String qishiriq,String zhzhriqi) throws Exception{
      log.info("开始利息试算，本笔借据为："+billNo);
      boolean flag = false;
        Supplier <Ln3249ReqDto> supplierReq = Ln3249ReqDto::new;
        Supplier <Ln3249RespDto> supplierResp = Ln3249RespDto::new;
        Ln3249RespDto Ln3249Resp=supplierResp.get();
        Ln3249ReqDto ln3249ReqDto= supplierReq.get();
        ln3249ReqDto.setDkjiejuh(billNo);
        ln3249ReqDto.setQishiriq(qishiriq);
        ln3249ReqDto.setZhzhriqi(zhzhriqi);
        log.info("利息试算请求："+ln3249ReqDto);
        ResultDto<Ln3249RespDto> ln3249RespDto = dscms2CoreLnClientService.ln3249(ln3249ReqDto);
        log.info("利息试算返回："+ln3249RespDto);
        String ln3249Code = Optional.ofNullable(ln3249RespDto.getCode()).orElse(StringUtils.EMPTY);
        String ln3249Meesage = Optional.ofNullable(ln3249RespDto.getMessage()).orElse(StringUtils.EMPTY);
        log.info("利息试算返回信息："+ln3249Meesage);
        if(Objects.equals(ln3249Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            Ln3249Resp=  ln3249RespDto.getData();
        }else {
            throw BizException.error(null,ln3249Code, ln3249Meesage );
        }
      return Ln3249Resp;
    }

}
