/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.domain.PlaLawGuar;
import cn.com.yusys.yusp.repository.mapper.PlaLawGuarMapper;
import com.alibaba.excel.util.DateUtils;
import com.alibaba.excel.util.StringUtils;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawGuarService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-07 22:13:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaLawGuarService {

    @Autowired
    private PlaLawGuarMapper plaLawGuarMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaLawGuar selectByPrimaryKey(String caseNo) {
        return plaLawGuarMapper.selectByPrimaryKey(caseNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaLawGuar> selectAll(QueryModel model) {
        return plaLawGuarMapper.selectByModel(model);
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaLawGuar> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaLawGuar> list = plaLawGuarMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaLawGuar record) {
        return plaLawGuarMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaLawGuar record) {
        return plaLawGuarMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaLawGuar record) {
        if (StringUtils.isEmpty(record.getPlgSerno())) {
            return plaLawGuarMapper.insertSelective(record);
        } else {
            return plaLawGuarMapper.updateByPrimaryKey(record);
        }
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaLawGuar record) {
        return plaLawGuarMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String caseNo) {
        return plaLawGuarMapper.deleteByPrimaryKey(caseNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaLawGuarMapper.deleteByIds(ids);
    }


    @Transactional(rollbackFor = Throwable.class)
    public void batchInsert(PlaLawGuar plaLawGuar) {
        plaLawGuarMapper.deleteByPrimaryKey(plaLawGuar.getCaseNo());
        Date inputDate = new Date();
        plaLawGuar.setUpdId(SessionUtils.getUserInformation().getLoginCode());
        plaLawGuar.setUpdBrId(SessionUtils.getUserInformation().getOrg().getCode());
        plaLawGuar.setUpdDate(DateUtils.format(inputDate, "yyyyMMdd"));
        plaLawGuar.setCreateTime(inputDate);
        plaLawGuar.setUpdateTime(inputDate);
        plaLawGuar.setInputId(SessionUtils.getUserInformation().getLoginCode());
        plaLawGuar.setInputBrId(SessionUtils.getUserInformation().getOrg().getCode());
        plaLawGuar.setInputDate(DateUtils.format(inputDate, "yyyyMMdd"));
        plaLawGuarMapper.insert(plaLawGuar);
    }
}
