/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.List;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaLawPerformCaseInfo;
import cn.com.yusys.yusp.repository.mapper.PlaLawPerformCaseInfoMapper;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawPerformCaseInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-07 19:08:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaLawPerformCaseInfoService {

    @Autowired
    private PlaLawPerformCaseInfoMapper plaLawPerformCaseInfoMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PlaLawPerformCaseInfo selectByPrimaryKey(String plpciSerno) {
        return plaLawPerformCaseInfoMapper.selectByPrimaryKey(plpciSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaLawPerformCaseInfo> selectAll(QueryModel model) {
        List<PlaLawPerformCaseInfo> records = (List<PlaLawPerformCaseInfo>) plaLawPerformCaseInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PlaLawPerformCaseInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaLawPerformCaseInfo> list = plaLawPerformCaseInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PlaLawPerformCaseInfo record) {
        return plaLawPerformCaseInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PlaLawPerformCaseInfo record) {
        return plaLawPerformCaseInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PlaLawPerformCaseInfo record) {
        int count=0;
        if(record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            if(plaLawPerformCaseInfoMapper.selectByPrimaryKey(record.getPlpciSerno()) != null){
                record.setUpdId(inputId);
                record.setUpdBrId(inputBrId);
                record.setUpdDate(openDay);
                record.setUpdateTime(createTime);
                count =  plaLawPerformCaseInfoMapper.updateByPrimaryKeySelective(record);
            } else {
                record.setInputId(inputId);
                record.setInputBrId(inputBrId);
                record.setInputDate(openDay);
                record.setCreateTime(createTime);
                record.setUpdId(inputId);
                record.setUpdBrId(inputBrId);
                record.setUpdDate(openDay);
                record.setUpdateTime(createTime);
                count =  plaLawPerformCaseInfoMapper.insertSelective(record);
            }
        }
        return count;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PlaLawPerformCaseInfo record) {
        return plaLawPerformCaseInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String plpciSerno) {
        return plaLawPerformCaseInfoMapper.deleteByPrimaryKey(plpciSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaLawPerformCaseInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByPlaLawPerformCaseInfo
     * @方法描述: 根据律师姓名查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int selectByPlaLawPerformCaseInfo(String lawyerName) {
        return plaLawPerformCaseInfoMapper.selectByPlaLawPerformCaseInfo(lawyerName);
    }

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据案件编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaLawPerformCaseInfo selectByCaseSerno(String caseSerno) {
        return plaLawPerformCaseInfoMapper.selectByCaseSerno(caseSerno);
    }
}
