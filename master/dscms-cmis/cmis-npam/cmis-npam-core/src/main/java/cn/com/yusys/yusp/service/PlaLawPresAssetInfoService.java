/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.List;

import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.domain.PlaLawAccusedList;
import cn.com.yusys.yusp.domain.PlaLawPersonInfo;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaLawPresAssetInfo;
import cn.com.yusys.yusp.repository.mapper.PlaLawPresAssetInfoMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawPresAssetInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-07 21:29:31
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaLawPresAssetInfoService {

    @Autowired
    private PlaLawPresAssetInfoMapper plaLawPresAssetInfoMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaLawPresAssetInfo selectByPrimaryKey(String plpaiSerno) {
        return plaLawPresAssetInfoMapper.selectByPrimaryKey(plpaiSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaLawPresAssetInfo> selectAll(QueryModel model) {
        List<PlaLawPresAssetInfo> records = (List<PlaLawPresAssetInfo>) plaLawPresAssetInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaLawPresAssetInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaLawPresAssetInfo> list = plaLawPresAssetInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaLawPresAssetInfo record) {
        return plaLawPresAssetInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaLawPresAssetInfo record) {
        return plaLawPresAssetInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaLawPresAssetInfo record) {
        return plaLawPresAssetInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaLawPresAssetInfo record) {
        return plaLawPresAssetInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String plpaiSerno) {
        return plaLawPresAssetInfoMapper.deleteByPrimaryKey(plpaiSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaLawPresAssetInfoMapper.deleteByIds(ids);
    }

    /**
     * 批量保存
     * @param list
     */
    public void batchInsert(List<PlaLawPresAssetInfo> list) {
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        for (PlaLawPresAssetInfo assetInfo : list) {
            assetInfo.setUpdId(SessionUtils.getUserInformation().getLoginCode());
            assetInfo.setUpdBrId(SessionUtils.getUserInformation().getOrg().getCode());
            assetInfo.setUpdDate(openDay);
            assetInfo.setUpdateTime(DateUtils.getCurrTimestamp());
            if (plaLawPresAssetInfoMapper.selectByPrimaryKey(assetInfo.getPlpaiSerno()) != null) {
                plaLawPresAssetInfoMapper.updateByPrimaryKey(assetInfo);
            } else {
                assetInfo.setInputId(SessionUtils.getUserInformation().getLoginCode());
                assetInfo.setInputBrId(SessionUtils.getUserInformation().getOrg().getCode());
                assetInfo.setInputDate(openDay);
                assetInfo.setCreateTime(DateUtils.getCurrTimestamp());
                plaLawPresAssetInfoMapper.insert(assetInfo);
            }
        }
    }
}
