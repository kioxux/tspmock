/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.user.UserIdentity;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisNpamConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.client.esb.core.da3302.req.Da3302ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3302.resp.Da3302RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3307.req.Da3307ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3307.resp.Da3307RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3320.Da3320ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3320.Da3320RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.dealsy.DealsyReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.dealsy.DealsyRespDto;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.PlaAssetPldAppInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.server.ib1241.Ib1241Server;
import cn.com.yusys.yusp.vo.PlaAssetPldAppInfoVo;
import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldAppInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaAssetPldAppInfoService {
    private static final Logger log = LoggerFactory.getLogger(PlaAssetPldAppInfoService.class);
    @Autowired
    private PlaAssetPldAppInfoMapper plaAssetPldAppInfoMapper;
    @Autowired
    private PlaAssetPldLeaseInfoService plaAssetPldLeaseInfoService;
    @Autowired
    private PlaAssetPldPropertyConvInfoService plaAssetPldPropertyConvInfoService;
    @Autowired
    private PlaAssetPldSellInfoService plaAssetPldSellInfoService;
    @Autowired
    private PlaAssetPldMamaLossInfoService plaAssetPldMamaLossInfoService;
    @Autowired
    private Dscms2CoreDaClientService dscms2CoreDaClientService;
    @Autowired
    private Dscms2YpxtClientService dscms2YpxtClientService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private Ib1241Server ib1241Server;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaAssetPldAppInfo selectByPrimaryKey(String papaiSerno) {
        return plaAssetPldAppInfoMapper.selectByPrimaryKey(papaiSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaAssetPldAppInfo> selectAll(QueryModel model) {
        List<PlaAssetPldAppInfo> records = (List<PlaAssetPldAppInfo>) plaAssetPldAppInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaAssetPldAppInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaAssetPldAppInfo> list = plaAssetPldAppInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaAssetPldAppInfo record) {
        return plaAssetPldAppInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaAssetPldAppInfo record) {
        return plaAssetPldAppInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaAssetPldAppInfo record) {
        if(record != null){
            // 资产处置方式
            String dispMode= record.getDispMode();
            // 处置流水号
            String papaiSerno = record.getPapaiSerno();
            // 如果是出租 则判断他之前是否保存了其他类型数据，有则删除
            if(CmisNpamConstants.DISP_MODE_01.equals(dispMode)){
                /** 删除转固信息 **/
                plaAssetPldPropertyConvInfoService.deleteByPapaiSerno(papaiSerno);
                /** 删除出售信息 **/
                plaAssetPldSellInfoService.deleteByPapaiSerno(papaiSerno);
                /** 删除核销信息 **/
                plaAssetPldMamaLossInfoService.deleteByPapaiSerno(papaiSerno);
            }else if(CmisNpamConstants.DISP_MODE_02.equals(dispMode)){
                /** 删除出租信息及租金信息 **/
                plaAssetPldLeaseInfoService.deleteByPapaiSerno(papaiSerno);
                /** 删除转固信息 **/
                plaAssetPldPropertyConvInfoService.deleteByPapaiSerno(papaiSerno);
                /** 删除核销信息 **/
                plaAssetPldMamaLossInfoService.deleteByPapaiSerno(papaiSerno);
            }else if(CmisNpamConstants.DISP_MODE_03.equals(dispMode)){
                /** 删除出租信息及租金信息 **/
                plaAssetPldLeaseInfoService.deleteByPapaiSerno(papaiSerno);
                /** 删除出售信息 **/
                plaAssetPldSellInfoService.deleteByPapaiSerno(papaiSerno);
                /** 删除核销信息 **/
                plaAssetPldMamaLossInfoService.deleteByPapaiSerno(papaiSerno);
            }else{
                /** 删除出租信息及租金信息 **/
                plaAssetPldLeaseInfoService.deleteByPapaiSerno(papaiSerno);
                /** 删除转固信息 **/
                plaAssetPldPropertyConvInfoService.deleteByPapaiSerno(papaiSerno);
                /** 删除出售信息 **/
                plaAssetPldSellInfoService.deleteByPapaiSerno(papaiSerno);
            }
        }
        return plaAssetPldAppInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaAssetPldAppInfo record) {
        return plaAssetPldAppInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String papaiSerno) {
        /** 删除出租信息及租金信息 **/
        plaAssetPldLeaseInfoService.deleteByPapaiSerno(papaiSerno);
        /** 删除转固信息 **/
        plaAssetPldPropertyConvInfoService.deleteByPapaiSerno(papaiSerno);
        /** 删除出售信息 **/
        plaAssetPldSellInfoService.deleteByPapaiSerno(papaiSerno);
        /** 删除核销信息 **/
        plaAssetPldMamaLossInfoService.deleteByPapaiSerno(papaiSerno);
        /** 删除抵债资产处置申请 **/
        return plaAssetPldAppInfoMapper.deleteByPrimaryKey(papaiSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaAssetPldAppInfoMapper.deleteByIds(ids);
    }

    /**
     * 新增抵债资产处置申请
     *
     * @author jijian_yx
     * @date 2021/6/10 16:37
     **/
    public String save(PlaAssetPldAppInfo appInfo) {
        // TODO 申请信息待对接补全
        String papaiSerno = "";
        if (null == appInfo.getPapaiSerno()) {
            papaiSerno =  sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.NPAM_PAPAI_SERNO, new HashMap<>());
            appInfo.setPapaiSerno(papaiSerno);
        }
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        Date createTime = DateUtils.getCurrTimestamp();// 创建时间
        appInfo.setInputDate(openDay);
        appInfo.setCreateTime(createTime);
        appInfo.setUpdDate(openDay);
        appInfo.setUpdateTime(createTime);
        appInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
        int result = plaAssetPldAppInfoMapper.insertSelective(appInfo);
        if (result > 0) {
            return papaiSerno;
        } else {
            return "";
        }
    }

    /**
     * 分页查询抵债资产处置申请
     *
     * @author jijian_yx
     * @date 2021/6/9 20:21
     **/
    public List<PlaAssetPldAppInfo> toSignlist(QueryModel model) {
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_000992111);
        return plaAssetPldAppInfoMapper.selectByModel(model);
    }

    /**
     * 分页查询抵债资产处置历史
     *
     * @author jijian_yx
     * @date 2021/6/9 20:21
     **/
    public List<PlaAssetPldAppInfo> doneSignlist(QueryModel model) {
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_996997998);
        return plaAssetPldAppInfoMapper.selectByModel(model);
    }

    /**
     * 分页查询抵债资产处置台账信息
     *
     * @author jijian_yx
     * @date 2021/6/9 20:21
     **/
    public List<PlaAssetPldAppInfo> getAssetPldAccList(QueryModel model) {
        model.getCondition().put("approveStatus", CmisCommonConstants.WF_STATUS_997);
        return plaAssetPldAppInfoMapper.selectByModel(model);
    }

    /**
     * 导出抵债资产台账
     *
     * @author jijian_yx
     * @date 2021/6/10 9:59
     **/
    public ProgressDto exportPlaAssetPldAppInfo(QueryModel model) {

        boolean flag = false;

        // 角色控制数据权限
        User loginUser = SessionUtils.getUserInformation();
        List<? extends UserIdentity> roles = loginUser.getRoles();
        if (null != roles && roles.size() > 0) {
            for (int i = 0; i < roles.size(); i++) {
                // 小微客户经理 R0010; 综合客户经理 R0020;
                //零售客户经理 R0030   小企业客户经理 R0050    虚拟客户经理  R0200
                if ("R0010".equals(roles.get(i).getCode()) || "R0020".equals(roles.get(i).getCode()) || "R0030".equals(roles.get(i).getCode()) ||
                        "R0050".equals(roles.get(i).getCode()) || "R0200".equals(roles.get(i).getCode())) {
                    flag = true;
                    break;
                }
            }
        }

        if (flag) {
            model.addCondition("managerId", loginUser.getLoginCode());
        } else {
            User userinfo = SessionUtils.getUserInformation();
            String orgCode = userinfo.getOrg().getCode();
            // 权限范围登陆机构及其下级机构
            List<String> orgCodes = commonService.getLowerOrgId(orgCode);
            String orgCodeList = "";
            if (null != orgCodes && orgCodes.size() > 0) {
                orgCodeList = org.apache.commons.lang.StringUtils.join(orgCodes, ",");
            }
            model.addCondition("orgCodeList", orgCodeList);
        }


        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel) object;
            queryModeTemp.setSize(size);
            queryModeTemp.setPage(page);
            return selectByplaAssetPldAppInfo(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(PlaAssetPldAppInfoVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    public List<PlaAssetPldAppInfoVo> selectByplaAssetPldAppInfo(QueryModel model) {
        List<PlaAssetPldAppInfoVo> plaAssetPldAppInfoVoList = new ArrayList<>();
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaAssetPldAppInfo> list = plaAssetPldAppInfoMapper.selectByModel(model);
        PageHelper.clearPage();

        for (PlaAssetPldAppInfo plaAssetPldAppInfo:list){
            PlaAssetPldAppInfoVo plaAssetPldAppInfoVo = new PlaAssetPldAppInfoVo();
            String inputIdName = OcaTranslatorUtils.getUserName(plaAssetPldAppInfo.getInputId());
            String inputBrIdName = OcaTranslatorUtils.getOrgName(plaAssetPldAppInfo.getInputBrId());
            BeanUtils.copyProperties(plaAssetPldAppInfo, plaAssetPldAppInfoVo);
            plaAssetPldAppInfoVo.setInputIdName(inputIdName);
            plaAssetPldAppInfoVo.setInputBrIdName(inputBrIdName);
            plaAssetPldAppInfoVoList.add(plaAssetPldAppInfoVo);
        }
        return plaAssetPldAppInfoVoList;
    }

    /**
     * 以物抵债发送核心记账
     *
     * @author liuquan
     **/
    public ResultDto<String> sendToHXJZ(PlaAssetPldAppInfo appInfo) {
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        ResultDto<String> resultDto = new ResultDto<>();
        if (appInfo != null) {
            // 1、判断处置方式 01出租，02出售，03转固，04毁灭
            String dispMode = appInfo.getDispMode();
            // 业务流水
            String papaiSerno = appInfo.getPapaiSerno();
            // 变现费用
            BigDecimal sellAmt=BigDecimal.ZERO;
            // 补偿金额
            BigDecimal bcompenstateAmt=BigDecimal.ZERO;
            //待处理抵债资产
            BigDecimal dcldzzic= da3320Service(appInfo.getPldimnNo());
            //补偿账号
            String buczhhao= "";
            String buczhzxh= "";
            // 3-损毁  2- 自用 1-变卖
            if("01".equals(dispMode)){
                resultDto =da3307Service(appInfo);
                return resultDto;
            }else if("02".equals(dispMode)){
                dispMode = "1";
                PlaAssetPldSellInfo plaAssetPldSellInfo= plaAssetPldSellInfoService.selectByPapaiSerno(papaiSerno);
                sellAmt = plaAssetPldSellInfo.getSellAmt();
            }else if("03".equals(dispMode)){
                dispMode = "2";
                PlaAssetPldPropertyConvInfo plaAssetPldPropertyConvInfo =plaAssetPldPropertyConvInfoService.selectByPapaiSerno(papaiSerno);
                sellAmt = plaAssetPldPropertyConvInfo.getTurnValue();
            }else{
                dispMode = "3";
                buczhhao="803000028823888";
                buczhzxh= " 00001";
                PlaAssetPldMamaLossInfo plaAssetPldMamaLossInfo =plaAssetPldMamaLossInfoService.selectByPapaiSerno(papaiSerno);
                bcompenstateAmt= plaAssetPldMamaLossInfo.getCompenstateAmt();
            }
            Da3302ReqDto da3302ReqDto = new Da3302ReqDto();
            // 业务操作标志 3 直通
            da3302ReqDto.setDaikczbz("3");
            // 抵债资产编号
            da3302ReqDto.setDzzcbhao(appInfo.getPldimnNo());
            // 抵债资产名称
            da3302ReqDto.setDzzcminc(appInfo.getPldimnName());
            // 营业机构
            da3302ReqDto.setYngyjigo("017001");
            // 客户号
            da3302ReqDto.setKehuhaoo(appInfo.getCusId());
            // 客户名称
            da3302ReqDto.setKehuzwmc(appInfo.getCusName());
            // 待变现抵债资产
            da3302ReqDto.setDbxdzzic(sellAmt);
            // 费用金额
            da3302ReqDto.setFeiyjine(BigDecimal.ZERO);
            // 待处理抵债资产
            da3302ReqDto.setDcldzzic(dcldzzic);
            // 抵债资产变现款项账号
            da3302ReqDto.setDzzcbxkx("62312390009145176");
            // 抵债资产变现账号子序号
            da3302ReqDto.setDzzcbzxh("00001");
            // 费用挂账账户
            da3302ReqDto.setFygzzhao("");
            // 费用挂账账号子序号
            da3302ReqDto.setFygzzzxh("");
            // 补偿金额  损毁的时候用的
            da3302ReqDto.setBuchjine(bcompenstateAmt);
            // 补偿账号
            da3302ReqDto.setBuczhhao(buczhhao);
            // 补偿账号子序号
            da3302ReqDto.setBuczhzxh(buczhzxh);

            // 抵债资产处置种类  3-损毁  2- 自用 1-变卖
            da3302ReqDto.setDzzcczzl(dispMode);
            // 抵债资产是否处置完毕标志
            da3302ReqDto.setDzzcczwb("");
            //抵债资产状态 2入账
            da3302ReqDto.setDzzcztai("2");
            //入账操作方式 付待保管
            da3302ReqDto.setRzczfshi("1");

            log.info("抵债资产处置记账接口da3302请求参数"+da3302ReqDto);
            // 调用核心接口，进行抵债资产登记  登记成功更改记账状态
            ResultDto<Da3302RespDto> da3302RespDto=dscms2CoreDaClientService.da3302(da3302ReqDto);
            log.info("抵债资产处置记账接口da3302返回参数"+da3302RespDto);
            String da3302Code = Optional.ofNullable(da3302RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String  da3302Meesage = Optional.ofNullable(da3302RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            log.info("抵债资产处置记账接口da3302返回message"+da3302Meesage);
            if (Objects.equals(da3302Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                // 押品处置信息同步
                Boolean flag = dealsyCall(appInfo);
                if(flag) {
                    appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_03);
                    appInfo.setRecordDate(openDay);
                    appInfo.setRecordTranSerno(da3302RespDto.getData().getJiaoyils());
                    appInfo.setRecordTranTime(da3302RespDto.getData().getJiaoyirq());
                    updateSelective(appInfo);
                    resultDto.setMessage(EcnEnum.ECN060000.value);
                } else {
                    appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_04);
                    appInfo.setRecordDate(openDay);
                    updateSelective(appInfo);
                    resultDto.code(da3302Code);
                    resultDto.setMessage(da3302Meesage);
                }
            }else {
                appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_04);
                appInfo.setRecordDate(openDay);
                updateSelective(appInfo);
                resultDto.code(da3302Code);
                resultDto.setMessage(da3302Meesage);
            }
        } else {
            throw BizException.error(null, EcnEnum.ECN060005.key, EcnEnum.ECN060005.value);
        }
        return resultDto;
    }

    /**
     * 押品处置信息同步
     * @param appInfo
     */
    public Boolean dealsyCall(PlaAssetPldAppInfo appInfo) {
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        log.info("押品处置信息同步开始（调用押品处置信息同步接口），抵债资产编号：{}",appInfo.getPldimnNo());
        Boolean flag = false;
        DealsyReqDto dealsyReqDto = new DealsyReqDto();
        // 押品统一编号
        dealsyReqDto.setYptybh(appInfo.getPldimnNo());
        // 押品处置方式
        dealsyReqDto.setYpczfs(appInfo.getDispMode());
        // 现金回收来源编号
        dealsyReqDto.setXjhsbh("");
        // 抵债资产编号
        dealsyReqDto.setDzzcbh("");
        // 抵债资产类型 TODO
        dealsyReqDto.setDzzclx("");
        // 以资抵债金额 TODO
        dealsyReqDto.setYzdzje(appInfo.getPldimnAmt());
        // 处置回收金额总额
        dealsyReqDto.setCzhsze(BigDecimal.ZERO);
        // 处置回收金额类型
        dealsyReqDto.setCzhslx("");
        // 处置回收金额
        dealsyReqDto.setCzhsje(BigDecimal.ZERO);
        // 是否处置完毕
        dealsyReqDto.setIsflag("");
        // 处置直接费用总额
        dealsyReqDto.setZjfyze(BigDecimal.ZERO);
        // 处置直接费用类型
        dealsyReqDto.setZjfylx("");
        // 处置直接费用金额
        dealsyReqDto.setZjfyje(BigDecimal.ZERO);
        // 处置日期
        dealsyReqDto.setCzrqyp(DateUtils.formatDate10To8(openDay));
        // 备注
        dealsyReqDto.setRemark(appInfo.getMemo());
        // 经办人
        dealsyReqDto.setJbrbhy(appInfo.getInputId());
        // 经办机构
        dealsyReqDto.setJbjgyp(appInfo.getManagerBrId());
        // 经办日期
        dealsyReqDto.setJbrqyp(DateUtils.formatDate10To8(openDay));
        // 操作
        dealsyReqDto.setOperat("01");
        // 流水号
        dealsyReqDto.setSernum(appInfo.getPapaiSerno());
        log.info("押品处置信息同步执行中（调用押品处置信息同步接口），抵债资产编号：{}，请求报文：{}",appInfo.getPldimnNo(), JSON.toJSONString(dealsyReqDto));
        ResultDto<DealsyRespDto> resultDto = dscms2YpxtClientService.dealsy(dealsyReqDto);
        log.info("押品处置信息同步执行中（调用押品处置信息同步接口），抵债资产编号：{}，响应报文：{}",appInfo.getPldimnNo(), JSON.toJSONString(resultDto));
        String dealsyCode = Optional.ofNullable(resultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String  dealsyMeesage = Optional.ofNullable(resultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        log.info("抵债资产处置记账接口dealsy返回message"+dealsyMeesage);
        if (Objects.equals(dealsyCode, SuccessEnum.CMIS_SUCCSESS.key)) {
            flag  = true;
        }
        log.info("押品处置信息同步结束（调用押品处置信息同步接口），抵债资产编号：{}",appInfo.getPldimnNo());
        return flag;
    }
    /**
     * @创建人 liuquan
     * @创建时间
     * @注释 冲正处理
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto ib1241(PlaAssetPldAppInfo appInfo) {
        try {
            // 调用核心冲正交易
            Ib1241ReqDto ib1241ReqDto = new Ib1241ReqDto();

            // 1、拼装请求报文
            // 原交易日期
            ib1241ReqDto.setYjiaoyrq(appInfo.getRecordTranTime());
            // 原柜员流水
            ib1241ReqDto.setYgyliush(appInfo.getRecordTranSerno());
            ResultDto<Ib1241RespDto> ib1241RespDtoResultDto = ib1241Server.sendIb1241(ib1241ReqDto);
            String ib1241Code = Optional.ofNullable(ib1241RespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String ib1241Meesage = Optional.ofNullable(ib1241RespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            if (Objects.equals(ib1241Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                if(ib1241RespDtoResultDto !=null){
                    appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_01);
                    updateSelective(appInfo);
                }else{
                    return new ResultDto(null).message("冲正处理失败");
                }
            }else{
                return new ResultDto(null).message("冲正处理失败"+ib1241Meesage);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResultDto(null).message(e.getMessage());
        }
        return new ResultDto(null).message("冲正处理成功");
    }
    /**
     * 以物抵债发送核心记账
     *
     * @author liuquan
     **/
    public ResultDto<String> da3307Service(PlaAssetPldAppInfo appInfo) {
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        ResultDto<String> resultDto = new ResultDto<>();
        if(appInfo != null){
            PlaAssetPldLeaseInfo plaAssetPldLeaseInfo = plaAssetPldLeaseInfoService.selectByPapaiSerno(appInfo.getPapaiSerno());
            Da3307ReqDto da3307ReqDto = new Da3307ReqDto();
            da3307ReqDto.setDzzcbhao(plaAssetPldLeaseInfo.getPldimnNo());//抵债资产编号
            da3307ReqDto.setDzzcminc(plaAssetPldLeaseInfo.getPldimnName());//抵债资产名称
            da3307ReqDto.setDcldzzic(appInfo.getPldimnAmt());//待处理抵债资产
            da3307ReqDto.setJieszhao("805000000529288");//结算账号
            da3307ReqDto.setJeszhzxh("00001");//结算账号子序号
            da3307ReqDto.setShiftanx("0");//是否摊销
            da3307ReqDto.setShiftanx("");//摊销入账周期
            da3307ReqDto.setQishriqi(DateUtils.formatDate10To8(plaAssetPldLeaseInfo.getStartDate()));//起始日期
            da3307ReqDto.setDaoqriqi(DateUtils.formatDate10To8(plaAssetPldLeaseInfo.getEndDate()));//到期日期
            da3307ReqDto.setJiaoyije(plaAssetPldLeaseInfo.getRent());//交易金额
            // 调用核心接口，进行抵债资产出租记账
            ResultDto<Da3307RespDto> da3307RespDto=dscms2CoreDaClientService.da3307(da3307ReqDto);
            log.info("抵债资产处置记账接口da3307返回参数"+da3307RespDto);
            String da3307Code = Optional.ofNullable(da3307RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String  da3307Meesage = Optional.ofNullable(da3307RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            log.info("抵债资产处置记账接口da3307返回message"+da3307Meesage);
            if (Objects.equals(da3307Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                // 押品处置信息同步
                Boolean flag = dealsyCall(appInfo);
                if(flag) {
                    appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_03);
                    appInfo.setRecordDate(openDay);
                    appInfo.setRecordTranSerno(da3307RespDto.getData().getJiaoyils());
                    appInfo.setRecordTranTime(da3307RespDto.getData().getJiaoyirq());
                    updateSelective(appInfo);
                    resultDto.setMessage(EcnEnum.ECN060000.value);
                } else {
                    appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_04);
                    appInfo.setRecordDate(openDay);
                    updateSelective(appInfo);
                    resultDto.code(da3307Code);
                    resultDto.setMessage(da3307Meesage);
                }
            }else {
                appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_04);
                appInfo.setRecordDate(openDay);
                updateSelective(appInfo);
                resultDto.code(da3307Code);
                resultDto.setMessage(da3307Meesage);
            }
        }else {
            throw BizException.error(null, EcnEnum.ECN060005.key, EcnEnum.ECN060005.value);
        }
        return resultDto;
    }

    /**
     * 待处理资产查询
     *
     * @author zhou
     **/
    public BigDecimal da3320Service(String pldimnNo) {
        // 待处理资产
        BigDecimal  dcldzzic= BigDecimal.ZERO;
        if(pldimnNo != null){
            Da3320ReqDto da3320ReqDto = new Da3320ReqDto();
            da3320ReqDto.setDzzcbhao(pldimnNo);//抵债资产编号
            // 调用核心接口，进行抵债资产出租记账
            ResultDto<Da3320RespDto> da3320RespDto=dscms2CoreDaClientService.da3320(da3320ReqDto);
            log.info("抵债资产处置记账接口da3320返回参数"+da3320RespDto);
            String da3320Code = Optional.ofNullable(da3320RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String  da3320Meesage = Optional.ofNullable(da3320RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            log.info("抵债资产处置记账接口da3320返回message"+da3320Meesage);
            if (Objects.equals(da3320Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                dcldzzic = da3320RespDto.getData().getDcldzzic(); // todo 先用评估金额
            }else {
                throw BizException.error(null, da3320Code, da3320Meesage);
            }
        }else {
            throw BizException.error(null, EcnEnum.ECN060005.key, EcnEnum.ECN060005.value);
        }
        return dcldzzic;
    }
}
