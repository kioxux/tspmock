package cn.com.yusys.yusp.dto;


import java.io.Serializable;
import java.math.BigDecimal;


public class LawCaseInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /*
   案件编号
    */
    private String caseNo;

    /*
   案件编号
    */
    private String caseSerno;

    /*
    客户编号
     */
    private String cusId;

    /*
    客户名称
     */
    private String cusName;

    /*
    案件类型
     */
    private String caseType;

    /*
    案件进程
     */
    private String caseProcess;

    /*
    标的金额（元）
     */
    private BigDecimal totalAmt;

    /*
    本金金额（元）
     */
    private BigDecimal capAmt;

    /*
    拖欠利息总额（元）
     */
    private BigDecimal totalTqlxAmt;

    /*
   登记人
    */
    private String inputId;

    /*
    登记机构
     */
    private String inputBrId;

    public String getCaseSerno() {
        return caseSerno;
    }

    public void setCaseSerno(String caseSerno) {
        this.caseSerno = caseSerno;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getCusId() {
        return cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public String getCaseType() {
        return caseType;
    }

    public String getCaseProcess() {
        return caseProcess;
    }

    public BigDecimal getTotalAmt() {
        return totalAmt;
    }

    public BigDecimal getCapAmt() {
        return capAmt;
    }

    public BigDecimal getTotalTqlxAmt() {
        return totalTqlxAmt;
    }



    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public void setCaseProcess(String caseProcess) {
        this.caseProcess = caseProcess;
    }

    public void setTotalAmt(BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }

    public void setCapAmt(BigDecimal capAmt) {
        this.capAmt = capAmt;
    }

    public void setTotalTqlxAmt(BigDecimal totalTqlxAmt) {
        this.totalTqlxAmt = totalTqlxAmt;
    }

    public String getInputId() {
        return inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }
}
