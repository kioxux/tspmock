/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaTakeoverAppInfo;
import cn.com.yusys.yusp.domain.PlaTakeoverBillRel;
import cn.com.yusys.yusp.dto.PlaTakeoverAppInfoDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.service.PlaTakeoverAppInfoService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaTakeoverAppInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 19:17:28
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/platakeoverappinfo")
public class PlaTakeoverAppInfoResource {
    @Autowired
    private PlaTakeoverAppInfoService plaTakeoverAppInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaTakeoverAppInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaTakeoverAppInfo> list = plaTakeoverAppInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaTakeoverAppInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaTakeoverAppInfo>> index(QueryModel queryModel) {
        List<PlaTakeoverAppInfo> list = plaTakeoverAppInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaTakeoverAppInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{ptaiSerno}")
    protected ResultDto<PlaTakeoverAppInfo> show(@PathVariable("ptaiSerno") String ptaiSerno) {
        PlaTakeoverAppInfo plaTakeoverAppInfo = plaTakeoverAppInfoService.selectByPrimaryKey(ptaiSerno);
        return new ResultDto<PlaTakeoverAppInfo>(plaTakeoverAppInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaTakeoverAppInfo> create(@RequestBody PlaTakeoverAppInfo plaTakeoverAppInfo) throws URISyntaxException {
        plaTakeoverAppInfoService.insert(plaTakeoverAppInfo);
        return new ResultDto<PlaTakeoverAppInfo>(plaTakeoverAppInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaTakeoverAppInfo plaTakeoverAppInfo) throws URISyntaxException {
        int result = plaTakeoverAppInfoService.update(plaTakeoverAppInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{ptaiSerno}")
    protected ResultDto<Integer> delete(@PathVariable("ptaiSerno") String ptaiSerno) {
        int result = plaTakeoverAppInfoService.deleteByPrimaryKey(ptaiSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaTakeoverAppInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/insert")
    protected ResultDto<PlaTakeoverAppInfo> insert(@RequestBody PlaTakeoverAppInfo plaTakeoverAppInfo) throws URISyntaxException {
        plaTakeoverAppInfoService.insert(plaTakeoverAppInfo);
        return new ResultDto<PlaTakeoverAppInfo>(plaTakeoverAppInfo);
    }
    /**
     * 获取债权转让申请,案件登记，以物抵债等列表
     * @函数名称：selectTakeoverlist
     * @author cainingbo_yx
     * @date 2021/6/10 20:22
     **/
    @ApiOperation("分页查询申请列表")
    @PostMapping("/selectTakeoverlist")
    protected ResultDto<List<PlaTakeoverAppInfoDto>> selectTakeoverlist(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PlaTakeoverAppInfoDto> list = plaTakeoverAppInfoService.selectTakeoverlist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<PlaTakeoverAppInfoDto>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryAll")
    protected ResultDto<List<PlaTakeoverAppInfo>> queryAll(@RequestBody QueryModel queryModel) {
        List<PlaTakeoverAppInfo> list = plaTakeoverAppInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaTakeoverAppInfo>>(list);
    }



    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/showByPtaiSerno")
    protected ResultDto<PlaTakeoverAppInfo> showByPtaiSerno(@RequestBody String ptaiSerno) {
        PlaTakeoverAppInfo plaTakeoverAppInfo = plaTakeoverAppInfoService.selectByPrimaryKey(ptaiSerno);
        return new ResultDto<PlaTakeoverAppInfo>(plaTakeoverAppInfo);
    }

    /**
     * @函数名称:transferReg
     * @函数描述:资产转让协议登记
     * @参数与返回说明: 交易码ln3061
     * @算法描述:
     */
    @PostMapping("/transferReg")
    protected ResultDto<String> transferReg(@RequestBody PlaTakeoverAppInfo plaTakeoverAppInfo) {
          return  plaTakeoverAppInfoService.transferReg(plaTakeoverAppInfo);
    }

    /**
     * @函数名称:transferReg
     * @函数描述: 资产转让记账发生记账核心
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/sendToHXJZ")
    protected ResultDto<String> sendToHXJZ(@RequestBody PlaTakeoverAppInfo appInfo) {
        return  plaTakeoverAppInfoService.sendToHXJZ(appInfo);
    }



    /**
     * @函数名称: sendToIb1253
     * @函数描述: 调用ib1253查询子序号
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据账号自动回显名称")
    @PostMapping("/sendToIb1253")
    protected ResultDto<Ib1253RespDto> sendToIb1253(@RequestBody Map map) {
        ResultDto<Ib1253RespDto> resultDto = plaTakeoverAppInfoService.sendToIb1253(map);
        return resultDto;
    }

    /**
     * @创建人 liuquan
     * @创建时间
     * @注释 冲正处理
     */
    @PostMapping("/czcl")
    protected ResultDto czcl( @RequestBody PlaTakeoverAppInfo appInfo) {
        return  plaTakeoverAppInfoService.ib1241(appInfo);
    }

    /**
     * @创建人 liuquan
     * @创建时间
     * @注释 登记冲正
     */
    @PostMapping("/DJCZ")
    protected ResultDto DJCZ( @RequestBody PlaTakeoverAppInfo appInfo) {
        return  plaTakeoverAppInfoService.DJCZ(appInfo);
    }
}
