/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaCardWriteoffApp;
import cn.com.yusys.yusp.service.PlaCardWriteoffAppService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaCardWriteoffAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-06-11 13:51:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/placardwriteoffapp")
public class PlaCardWriteoffAppResource {
    @Autowired
    private PlaCardWriteoffAppService plaCardWriteoffAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaCardWriteoffApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaCardWriteoffApp> list = plaCardWriteoffAppService.selectAll(queryModel);
        return new ResultDto<List<PlaCardWriteoffApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaCardWriteoffApp>> index(QueryModel queryModel) {
        List<PlaCardWriteoffApp> list = plaCardWriteoffAppService.selectByModel(queryModel);
        return new ResultDto<List<PlaCardWriteoffApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pcwaSerno}")
    protected ResultDto<PlaCardWriteoffApp> show(@PathVariable("pcwaSerno") String pcwaSerno) {
        PlaCardWriteoffApp plaCardWriteoffApp = plaCardWriteoffAppService.selectByPrimaryKey(pcwaSerno);
        return new ResultDto<PlaCardWriteoffApp>(plaCardWriteoffApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaCardWriteoffApp> create(@RequestBody PlaCardWriteoffApp plaCardWriteoffApp) throws URISyntaxException {
        plaCardWriteoffAppService.insert(plaCardWriteoffApp);
        return new ResultDto<PlaCardWriteoffApp>(plaCardWriteoffApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "信用卡核销申请修改")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaCardWriteoffApp plaCardWriteoffApp) throws URISyntaxException {
        int result = plaCardWriteoffAppService.update(plaCardWriteoffApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "信用卡核销申请删除")
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody PlaCardWriteoffApp plaCardWriteoffApp) {
        int result = plaCardWriteoffAppService.deleteByPrimaryKey(plaCardWriteoffApp.getPcwaSerno());
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaCardWriteoffAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     * 创建人：周茂伟
     */
    @ApiOperation(value = "信用卡核销列表查询")
    @PostMapping("/queryPlaCardWriteoffAppList")
    protected ResultDto<List<PlaCardWriteoffApp>> queryPlaCardWriteoffAppList(@RequestBody QueryModel queryModel) {
        List<PlaCardWriteoffApp> list = plaCardWriteoffAppService.selectByModel(queryModel);
        return new ResultDto<List<PlaCardWriteoffApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:详细信息查询
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "信用卡核销详细信息查询")
    @PostMapping("/queryPlaCardWriteoffAppInfo")
    protected ResultDto<PlaCardWriteoffApp> queryPlaCardWriteoffAppInfo(@RequestBody PlaCardWriteoffApp plaCardWriteoffApp) {
        PlaCardWriteoffApp plaCardWriteoffApps = plaCardWriteoffAppService.selectByPrimaryKey(plaCardWriteoffApp.getPcwaSerno());
        return new ResultDto<PlaCardWriteoffApp>(plaCardWriteoffApps);
    }

    /**
     * @函数名称:save
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "信用卡核销申请保存")
    @PostMapping("/save")
    protected ResultDto<PlaCardWriteoffApp> save(@RequestBody PlaCardWriteoffApp plaCardWriteoffApp) throws URISyntaxException {
        plaCardWriteoffAppService.save(plaCardWriteoffApp);
        return new ResultDto<PlaCardWriteoffApp>(plaCardWriteoffApp);
    }
}
