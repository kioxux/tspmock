package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.PlaExpenseDetail;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaExpenseRegiInfo
 * @类描述: pla_expense_regi_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-10 10:53:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PlaExpenseRegiInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 费用登记流水号 **/
	private String periSerno;
	
	/** 处置阶段 **/
	private String dispStage;

	/** 客户编号 **/
	private String cusId;

	/** 客户名称 **/
	private String cusName;
	
	/** 关联业务流水号 **/
	private String serno;
	
	/** 费用总额 **/
	private java.math.BigDecimal expenseTotalAmt;
	
	/** 垫支总额 **/
	private java.math.BigDecimal advanceExpenAmt;
	
	/** 列支总额 **/
	private java.math.BigDecimal rankAmt;
	
	/** 备注 **/
	private String memo;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;

	/** 费用明细登记表 **/
	private List<PlaExpenseDetail> plaExpenseDetail;


	public List<PlaExpenseDetail> getPlaExpenseDetail() {
		return plaExpenseDetail;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public void setPlaExpenseDetail(List<PlaExpenseDetail> plaExpenseDetail) {
		this.plaExpenseDetail = plaExpenseDetail;
	}
	/**
	 * @param periSerno
	 */
	public void setPeriSerno(String periSerno) {
		this.periSerno = periSerno == null ? null : periSerno.trim();
	}
	
    /**
     * @return PeriSerno
     */	
	public String getPeriSerno() {
		return this.periSerno;
	}
	
	/**
	 * @param dispStage
	 */
	public void setDispStage(String dispStage) {
		this.dispStage = dispStage == null ? null : dispStage.trim();
	}
	
    /**
     * @return DispStage
     */	
	public String getDispStage() {
		return this.dispStage;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param expenseTotalAmt
	 */
	public void setExpenseTotalAmt(java.math.BigDecimal expenseTotalAmt) {
		this.expenseTotalAmt = expenseTotalAmt;
	}
	
    /**
     * @return ExpenseTotalAmt
     */	
	public java.math.BigDecimal getExpenseTotalAmt() {
		return this.expenseTotalAmt;
	}
	
	/**
	 * @param advanceExpenAmt
	 */
	public void setAdvanceExpenAmt(java.math.BigDecimal advanceExpenAmt) {
		this.advanceExpenAmt = advanceExpenAmt;
	}
	
    /**
     * @return AdvanceExpenAmt
     */	
	public java.math.BigDecimal getAdvanceExpenAmt() {
		return this.advanceExpenAmt;
	}
	
	/**
	 * @param rankAmt
	 */
	public void setRankAmt(java.math.BigDecimal rankAmt) {
		this.rankAmt = rankAmt;
	}
	
    /**
     * @return RankAmt
     */	
	public java.math.BigDecimal getRankAmt() {
		return this.rankAmt;
	}
	
	/**
	 * @param memo
	 */
	public void setMemo(String memo) {
		this.memo = memo == null ? null : memo.trim();
	}
	
    /**
     * @return Memo
     */	
	public String getMemo() {
		return this.memo;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}