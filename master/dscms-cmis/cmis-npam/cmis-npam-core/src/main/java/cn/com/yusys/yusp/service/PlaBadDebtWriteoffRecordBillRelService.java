/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Supplier;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisNpamConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AccLoanDto;
import cn.com.yusys.yusp.dto.CusBaseDto;
import cn.com.yusys.yusp.dto.PlaBadDebtWriteoffRecordBillRelDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3045.Ln3045ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3045.Ln3045RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.ib1241.Ib1241Server;
import cn.com.yusys.yusp.service.server.ln3100.Ln3100Server;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.PlaBadDebtWriteoffRecordBillRelMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBadDebtWriteoffRecordBillRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-09 09:34:55
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaBadDebtWriteoffRecordBillRelService {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(PlaBadDebtWriteoffRecordBillRelService.class);

    @Autowired
    private PlaBadDebtWriteoffRecordBillRelMapper plaBadDebtWriteoffRecordBillRelMapper;
    @Autowired
    private PlaBadDebtWriteoffRecordAppService plaBadDebtWriteoffRecordAppService;
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;
    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;
    @Autowired
    private CmisBizClientService cmisBizClientService;
    @Autowired
    private CmisCusClientService cmisCusClientService;
    @Autowired
    private PlaBadDebtWriteoffAppSigService plaBadDebtWriteoffAppSigService;
    @Autowired
    private Ln3100Server ln3100Server;
    @Autowired
    private PlaBadDebtWriteoffClassInfoService plaBadDebtWriteoffClassInfoService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private Ib1241Server ib1241Server;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaBadDebtWriteoffRecordBillRel selectByPrimaryKey(String pbdwrbrSerno) {
        return plaBadDebtWriteoffRecordBillRelMapper.selectByPrimaryKey(pbdwrbrSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaBadDebtWriteoffRecordBillRel> selectAll(QueryModel model) {
        List<PlaBadDebtWriteoffRecordBillRel> records = (List<PlaBadDebtWriteoffRecordBillRel>) plaBadDebtWriteoffRecordBillRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaBadDebtWriteoffRecordBillRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaBadDebtWriteoffRecordBillRel> list = plaBadDebtWriteoffRecordBillRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaBadDebtWriteoffRecordBillRelDto record) {
        return plaBadDebtWriteoffRecordBillRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaBadDebtWriteoffRecordBillRel record) {
        return plaBadDebtWriteoffRecordBillRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaBadDebtWriteoffRecordBillRel record) {
        return plaBadDebtWriteoffRecordBillRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly=true)
    public int updateSelective(PlaBadDebtWriteoffRecordBillRel record) {
        return plaBadDebtWriteoffRecordBillRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 删除引用的时候需要更新主业务表中统计的金额信息
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(PlaBadDebtWriteoffRecordBillRel plaBadDebtWriteoffRecordBillRel) {
        // 关联表流水号
        String pbdwrbrSerno = plaBadDebtWriteoffRecordBillRel.getPbdwrbrSerno();
        // 主表流水号
        String pbdwraSerno = plaBadDebtWriteoffRecordBillRel.getPbdwraSerno();
        int count = plaBadDebtWriteoffRecordBillRelMapper.deleteByPrimaryKey(pbdwrbrSerno);
        if (count > 0) {
            // 核销总金额
            String totalWriteoffAmt = "0";
            // 核销总本金
            String totalWriteoffCap = "0";
            // 核销总利息
            String totalWriteoffInt = "0";

            // 核销总笔数
            String totalWriteoffNum = "0";
            // 核销总户数
            String totalWriteoffCus = "0";
            PlaBadDebtWriteoffRecordApp plaBadDebtWriteoffRecordApp = new PlaBadDebtWriteoffRecordApp();
            Map amtMap = plaBadDebtWriteoffRecordBillRelMapper.selectByPbdwraSerno(pbdwraSerno);
            if (amtMap.containsKey("writeoffcount")) {
                totalWriteoffNum = amtMap.get("writeoffcount").toString();
            }
            if (amtMap.containsKey("totalwriteoffamt")) {
                totalWriteoffAmt = amtMap.get("totalwriteoffamt").toString();
            }
            if (amtMap.containsKey("totalwriteoffcap")) {
                totalWriteoffCap = amtMap.get("totalwriteoffcap").toString();
            }
            if (amtMap.containsKey("totalwriteoffint")) {
                totalWriteoffInt = amtMap.get("totalwriteoffint").toString();
            }
            if (amtMap.containsKey("totalwriteoffcus")) {
                totalWriteoffCus = amtMap.get("totalwriteoffcus").toString();
            }
            // 修改主表金额
            plaBadDebtWriteoffRecordApp.setPbdwraSerno(pbdwraSerno);
            PlaBadDebtWriteoffRecordApp plaBadDebtWriteoffRecordAppInfo = plaBadDebtWriteoffRecordAppService.selectByPrimaryKey(pbdwraSerno);
            if (plaBadDebtWriteoffRecordAppInfo != null) {
                plaBadDebtWriteoffRecordAppInfo.setTotalWriteoffAmt(NumberUtils.toBigDecimal(totalWriteoffAmt));
                plaBadDebtWriteoffRecordAppInfo.setTotalWriteoffCap(NumberUtils.toBigDecimal(totalWriteoffCap));
                plaBadDebtWriteoffRecordAppInfo.setTotalWriteoffInt(NumberUtils.toBigDecimal(totalWriteoffInt));
                plaBadDebtWriteoffRecordAppInfo.setWriteoffCount(totalWriteoffNum);
                plaBadDebtWriteoffRecordAppInfo.setTotalWriteoffCus(totalWriteoffCus);
                // 如果总笔数为0 则修改核心关联流水号
                if ("0".equals(totalWriteoffNum)) {
                    plaBadDebtWriteoffRecordAppInfo.setPbdwasSerno("");
                }
                plaBadDebtWriteoffRecordAppService.update(plaBadDebtWriteoffRecordAppInfo);
            }
        }
        return count;
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaBadDebtWriteoffRecordBillRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: save
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */

    public int save(PlaBadDebtWriteoffRecordBillRelDto record) {
        int count = 0;
        if (record != null) {
            //流水号
            String serno = record.getPbdwraSerno();
            // 借据编号
            String billNo = record.getBillNo();
            // 合同编号
            String contNo = record.getContNo();
            PlaBadDebtWriteoffRecordBillRel plaBadDebtWriteoffRecordBillRel = plaBadDebtWriteoffRecordBillRelMapper.selectplaBadDebtWriteoffRecordBillRel(record);
            if (plaBadDebtWriteoffRecordBillRel != null) {
                throw BizException.error("null", EcnEnum.ECN060003.key, EcnEnum.ECN060003.value + ":" + billNo);
            }
            // 核销总金额
            String totalWriteoffAmt = "0";
            // 核销总本金
            String totalWriteoffCap = "0";
            // 核销总利息
            String totalWriteoffInt = "0";

            // 核销总笔数
            String totalWriteoffNum = "0";
            // 核销总户数
            String totalWriteoffCus = "0";
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            record.setPbdwrbrSerno(StringUtils.getUUID());
            record.setInputId(inputId);
            record.setInputBrId(inputBrId);
            record.setInputDate(openDay);
            record.setCreateTime(createTime);
            record.setUpdId(inputId);
            record.setUpdBrId(inputBrId);
            record.setUpdDate(openDay);
            record.setUpdateTime(createTime);
            // 流水号
            // String serno= StringUtils.getUUID();//sequenceTemplateService.getSequenceTemplate("", new HashMap<>());
            count = plaBadDebtWriteoffRecordBillRelMapper.insert(record);
            if (count > 0) {
                PlaBadDebtWriteoffRecordApp plaBadDebtWriteoffRecordApp = new PlaBadDebtWriteoffRecordApp();
                Map amtMap = plaBadDebtWriteoffRecordBillRelMapper.selectByPbdwraSerno(serno);
                if (amtMap.containsKey("writeoffcount") && amtMap.get("writeoffcount").toString() != null) {
                    totalWriteoffNum = amtMap.get("writeoffcount").toString();
                }
                if (amtMap.containsKey("totalwriteoffamt") && amtMap.get("totalwriteoffamt").toString() != null) {
                    totalWriteoffAmt = amtMap.get("totalwriteoffamt").toString();
                }
                if (amtMap.containsKey("totalwriteoffcap")&& amtMap.get("totalwriteoffcap").toString() !=null) {
                    totalWriteoffCap = amtMap.get("totalwriteoffcap").toString();
                }
                if (amtMap.containsKey("totalwriteoffint") && amtMap.get("totalwriteoffint").toString() != null) {
                    totalWriteoffInt = amtMap.get("totalwriteoffint").toString();
                }
                if (amtMap.containsKey("totalwriteoffcus") && amtMap.get("totalwriteoffcus").toString() != null) {
                    totalWriteoffCus = amtMap.get("totalwriteoffcus").toString();
                }
                // 修改主表金额
                plaBadDebtWriteoffRecordApp.setPbdwraSerno(serno);
                PlaBadDebtWriteoffRecordApp plaBadDebtWriteoffRecordAppInfo = plaBadDebtWriteoffRecordAppService.selectByPrimaryKey(serno);
                if (plaBadDebtWriteoffRecordAppInfo != null) {
                    plaBadDebtWriteoffRecordAppInfo.setTotalWriteoffAmt(NumberUtils.toBigDecimal(totalWriteoffAmt));
                    plaBadDebtWriteoffRecordAppInfo.setTotalWriteoffCap(NumberUtils.toBigDecimal(totalWriteoffCap));
                    plaBadDebtWriteoffRecordAppInfo.setTotalWriteoffInt(NumberUtils.toBigDecimal(totalWriteoffInt));
                    plaBadDebtWriteoffRecordAppInfo.setWriteoffCount(totalWriteoffNum);
                    plaBadDebtWriteoffRecordAppInfo.setTotalWriteoffCus(totalWriteoffCus);
                    // 如果总笔数为1 则修改核心关联流水号
                    if ("1".equals(totalWriteoffNum)) {
                        plaBadDebtWriteoffRecordAppInfo.setPbdwasSerno(record.getSerno());
                    }
                    plaBadDebtWriteoffRecordAppService.update(plaBadDebtWriteoffRecordAppInfo);
                } else {
                    plaBadDebtWriteoffRecordApp.setTotalWriteoffAmt(NumberUtils.toBigDecimal(totalWriteoffAmt));
                    plaBadDebtWriteoffRecordApp.setTotalWriteoffCap(NumberUtils.toBigDecimal(totalWriteoffCap));
                    plaBadDebtWriteoffRecordApp.setTotalWriteoffInt(NumberUtils.toBigDecimal(totalWriteoffInt));
                    plaBadDebtWriteoffRecordApp.setWriteoffCount(totalWriteoffNum);
                    plaBadDebtWriteoffRecordApp.setTotalWriteoffCus(totalWriteoffCus);
                    plaBadDebtWriteoffRecordApp.setPbdwasSerno(record.getSerno());
                    plaBadDebtWriteoffRecordAppService.save(plaBadDebtWriteoffRecordApp);
                }
            }
        }
        return count;
    }

    /**
     * @方法名称: saveList
     * @方法描述: 批量保存
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    public int saveList(List<PlaBadDebtWriteoffRecordBillRelDto> plaBadDebtWriteoffRecordBillRelList) {
        int count = 0;
        if (CollectionUtils.isNotEmpty(plaBadDebtWriteoffRecordBillRelList)) {
            for (PlaBadDebtWriteoffRecordBillRelDto plaBadDebtWriteoffRecordBillRelDto : plaBadDebtWriteoffRecordBillRelList) {
                count = save(plaBadDebtWriteoffRecordBillRelDto);
            }
        }
        return count;
    }

    /**
     * 呆账核销发送贷款核销处理
     *
     * @author 刘权
     **/
    public ResultDto<String> sendToDKHXCL(PlaBadDebtWriteoffRecordBillRel appInfo) {
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        ResultDto<String> resultDto = new ResultDto<>();
        // 核销总金额
        BigDecimal totalWriteoffAmt = BigDecimal.ZERO;
        // 核销总本金
        BigDecimal totalWriteoffCap = BigDecimal.ZERO;
        // 核销总利息
        BigDecimal totalWriteoffInt = BigDecimal.ZERO;

        Supplier<AccLoanDto> supplier =AccLoanDto::new;

        Supplier<PlaBadDebtWriteoffClassInfo> suppliers =PlaBadDebtWriteoffClassInfo::new;
        // 创建时间
        Date createTime = DateUtils.getCurrTimestamp();
        // 判断核销状态
        Boolean successFlag = true;
        String writeoffStatus = "";
        if (appInfo != null) {
            PlaBadDebtWriteoffRecordBillRel plaBadDebtWriteoffRecordBillRels=updateRecordBill(appInfo);
            Ln3045ReqDto ln3045ReqDto = new Ln3045ReqDto();
            //业务操作标志 3-直通
            ln3045ReqDto.setDaikczbz("3");
            //贷款借据号
            ln3045ReqDto.setDkjiejuh(plaBadDebtWriteoffRecordBillRels.getBillNo());
            //贷款账号
            ln3045ReqDto.setDkzhangh("");
            //货币代号 01-人民币
            ln3045ReqDto.setHuobdhao("01");
            //核销本金
            ln3045ReqDto.setHexiaobj(plaBadDebtWriteoffRecordBillRels.getLoanBalance());
            //核销利息
            ln3045ReqDto.setHexiaolx(plaBadDebtWriteoffRecordBillRels.getTotalTqlxAmt());
            //核销来源账号
            ln3045ReqDto.setHexiaozh("");
            //核销来源账号子序号
            ln3045ReqDto.setHexiaozx("");
            //贷款归还方式 14-核销
            ln3045ReqDto.setDaikghfs("14");
            // 调用核心接口，进行发送核心记账
            log.info("请求参数："+ln3045ReqDto);
            ResultDto<Ln3045RespDto> ln3045RespDto = dscms2CoreLnClientService.ln3045(ln3045ReqDto);
            log.info("返回参数："+ln3045RespDto);
            String ln3045Code = Optional.ofNullable(ln3045RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String ln3045Meesage = Optional.ofNullable(ln3045RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            if (Objects.equals(ln3045Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_03);
                appInfo.setRecordDate(DateUtils.formatDate8To10(ln3045RespDto.getData().getJiaoyirq()));
                appInfo.setWriteoffStatus(CmisNpamConstants.WRITEOFF_STATUS_03);
                appInfo.setWriteoffDate(DateUtils.formatDate8To10(ln3045RespDto.getData().getJiaoyirq()));
                appInfo.setRecordTranSerno(ln3045RespDto.getData().getJiaoyils());
                appInfo.setRecordTranTime(ln3045RespDto.getData().getJiaoyirq());
                updateSelective(appInfo);
                // 核销成功更新台账前保存台账五，十级分类于表中
                // 1、先查询员台账信息
                AccLoanDto accLoanDto=supplier.get();
                accLoanDto.setBillNo(plaBadDebtWriteoffRecordBillRels.getBillNo());
                ResultDto<AccLoanDto> resultDtoAcc=cmisBizClientService.selectClassByBillNo(accLoanDto);
                if(resultDtoAcc !=null){
                    PlaBadDebtWriteoffClassInfo plaBadDebtWriteoffClassInfo= suppliers.get();
                    plaBadDebtWriteoffClassInfo.setBillNo(resultDtoAcc.getData().getBillNo());
                    plaBadDebtWriteoffClassInfo.setFiveClass(resultDtoAcc.getData().getFiveClass());
                    plaBadDebtWriteoffClassInfo.setTenClass(resultDtoAcc.getData().getTenClass());
                    plaBadDebtWriteoffClassInfo.setWriteoffDate(DateUtils.formatDate8To10(ln3045RespDto.getData().getJiaoyirq()));
                    plaBadDebtWriteoffClassInfo.setCreateTime(createTime);
                    plaBadDebtWriteoffClassInfo.setUpdateTime(createTime);
                    plaBadDebtWriteoffClassInfoService.insert(plaBadDebtWriteoffClassInfo);
                }
                // 核销成功更新台账状态
                Map<String, String> map = new HashMap();
                map.put("billNo", appInfo.getBillNo());
                map.put("accStatus", "5");// 已核销
                map.put("fiveClass","50");// 损失
                // 判断是个人客户还是公司客户
                ResultDto<CusBaseDto> cusBaseDto =cmisCusClientService.cusBaseInfo(appInfo.getCusId());
                if(cusBaseDto != null && cusBaseDto.getData() !=null){
                    String cusCatalog = cusBaseDto.getData().getCusCatalog();
                    if("2".equals(cusCatalog)){
                        map.put("tenClass","50");// 损失
                    }
                }
                cmisBizClientService.updateAccLoanByBillNo(map);
                // 查询关联借据信息
                List<PlaBadDebtWriteoffRecordBillRel> lists = selectListByPbdwraSerno(appInfo.getPbdwraSerno());
                // 查询主表信息
                PlaBadDebtWriteoffRecordApp plaBadDebtWriteoffRecordApp = plaBadDebtWriteoffRecordAppService.selectByPrimaryKey(appInfo.getPbdwraSerno());
                for(PlaBadDebtWriteoffRecordBillRel plaBadDebtWriteoffRecordBillRel: lists){
                    totalWriteoffAmt=totalWriteoffAmt.add(plaBadDebtWriteoffRecordBillRel.getLoanBalance().add(plaBadDebtWriteoffRecordBillRel.getTotalTqlxAmt()));
                    totalWriteoffCap=totalWriteoffCap.add(plaBadDebtWriteoffRecordBillRel.getLoanBalance());
                    totalWriteoffInt= totalWriteoffInt.add(plaBadDebtWriteoffRecordBillRel.getTotalTqlxAmt());
                    writeoffStatus=plaBadDebtWriteoffRecordBillRel.getWriteoffStatus();
                    if(CmisNpamConstants.WRITEOFF_STATUS_04.equals(writeoffStatus)){
                        successFlag= false;
                    }
                }
                // 如果都是成功
                if(successFlag){
                    plaBadDebtWriteoffRecordApp.setWriteoffStatus(CmisNpamConstants.WRITEOFF_STATUS_03);
                    plaBadDebtWriteoffRecordApp.setRecordStatus(CmisNpamConstants.HX_STATUS_03);
                    plaBadDebtWriteoffRecordApp.setWriteoffDate(openDay);
                    plaBadDebtWriteoffRecordApp.setRecordDate(openDay);
                }else{
                    plaBadDebtWriteoffRecordApp.setWriteoffStatus(CmisNpamConstants.WRITEOFF_STATUS_04);
                    plaBadDebtWriteoffRecordApp.setRecordStatus(CmisNpamConstants.HX_STATUS_04);
                    plaBadDebtWriteoffRecordApp.setWriteoffDate(openDay);
                    plaBadDebtWriteoffRecordApp.setRecordDate(openDay);
                }
                // 更新主表信息
                plaBadDebtWriteoffRecordApp.setTotalWriteoffAmt(totalWriteoffAmt);
                plaBadDebtWriteoffRecordApp.setTotalWriteoffCap(totalWriteoffCap);
                plaBadDebtWriteoffRecordApp.setTotalWriteoffInt(totalWriteoffInt);
                plaBadDebtWriteoffRecordAppService.updateSelective(plaBadDebtWriteoffRecordApp);
                resultDto.setMessage("记账成功");
            } else {
                appInfo.setRecordStatus(CmisNpamConstants.HX_STATUS_04);
                appInfo.setWriteoffStatus(CmisNpamConstants.WRITEOFF_STATUS_04);
                appInfo.setWriteoffDate(openDay);
                appInfo.setRecordDate(openDay);
                updateSelective(appInfo);
                resultDto.setCode(ln3045Code);
                resultDto.setMessage(ln3045Meesage);
            }
        } else {
            throw BizException.error(null, EcnEnum.ECN060005.key, EcnEnum.ECN060005.value);
        }
        return resultDto;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaBadDebtWriteoffRecordBillRel> selectListByPbdwraSerno(String pbdwraSerno) {
        List<PlaBadDebtWriteoffRecordBillRel> list = plaBadDebtWriteoffRecordBillRelMapper.selectListByPbdwraSerno(pbdwraSerno);
        return list;
    }

    /**
     * @创建人
     * @创建时间
     * @注释 冲正处理
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto ib1241(PlaBadDebtWriteoffRecordBillRel plaBadDebtWriteoffRecordBillRel) {
        try {
            // 调用核心冲正交易
            Ib1241ReqDto ib1241ReqDto = new Ib1241ReqDto();
            // 原交易日期
            ib1241ReqDto.setYjiaoyrq(plaBadDebtWriteoffRecordBillRel.getRecordTranTime());
            // 原柜员流水
            ib1241ReqDto.setYgyliush(plaBadDebtWriteoffRecordBillRel.getRecordTranSerno());
            ResultDto<Ib1241RespDto> ib1241RespDtoResultDto = ib1241Server.sendIb1241(ib1241ReqDto);
            String ib1241Code = Optional.ofNullable(ib1241RespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String ib1241Meesage = Optional.ofNullable(ib1241RespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            if (Objects.equals(ib1241Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                // 已发送核心冲正成功
                plaBadDebtWriteoffRecordBillRel.setRecordStatus(CmisNpamConstants.HX_STATUS_01);
                plaBadDebtWriteoffRecordBillRel.setRecordBackSerno(ib1241RespDtoResultDto.getData().getChzhlshu());
                plaBadDebtWriteoffRecordBillRel.setRecordBackTime(ib1241RespDtoResultDto.getData().getChzhriqi());
                int i = plaBadDebtWriteoffRecordBillRelMapper.updateByPrimaryKeySelective(plaBadDebtWriteoffRecordBillRel);
                if (i != 1) {
                    return new ResultDto(null).message("冲正处理失败");
                } else {
                    // 冲正成功更新台账状态
                    Map<String, String> map = new HashMap();
                    map.put("billNo", plaBadDebtWriteoffRecordBillRel.getBillNo());
                    map.put("accStatus", "1");// 冲正成功修改状态
                    cmisBizClientService.updateAccLoanByBillNo(map);
                    //冲正成功删除五十级分类
                    plaBadDebtWriteoffClassInfoService.deleteByPrimaryKey(plaBadDebtWriteoffRecordBillRel.getBillNo());
                }
                QueryModel model = new QueryModel();
                model.addCondition("pbdwraSerno",plaBadDebtWriteoffRecordBillRel.getPbdwraSerno());
                model.addCondition("recordStatus",CmisNpamConstants.HX_STATUS_03);
                // 判断如果全部借据都冲正 则修改主表记账状态，核销状态
                List<PlaBadDebtWriteoffRecordBillRel> list = plaBadDebtWriteoffRecordBillRelMapper.selectByModel(model);
                if(CollectionUtils.isEmpty(list)){
                    PlaBadDebtWriteoffRecordApp plaBadDebtWriteoffRecordApp= new PlaBadDebtWriteoffRecordApp();
                    plaBadDebtWriteoffRecordApp.setRecordStatus(CmisNpamConstants.HX_STATUS_01);
                    plaBadDebtWriteoffRecordApp.setWriteoffStatus(CmisNpamConstants.WRITEOFF_STATUS_01);
                    plaBadDebtWriteoffRecordApp.setPbdwraSerno(plaBadDebtWriteoffRecordBillRel.getPbdwraSerno());
                    plaBadDebtWriteoffRecordAppService.updateSelective(plaBadDebtWriteoffRecordApp);
                }
            } else {
                return new ResultDto(null).message("冲正处理失败" + ib1241Meesage);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResultDto(null).message(e.getMessage());
        }
        return new ResultDto(null).message("冲正处理成功");
    }

    /**
     * @方法名称: updateByPbdwraSerno
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateByPbdwraSerno(PlaBadDebtWriteoffRecordBillRel record) {
        return plaBadDebtWriteoffRecordBillRelMapper.updateByPbdwraSerno(record);
    }

    /**
     * 记账之前更新借据台账
     * @param appInfo
     * @return
     */
    public PlaBadDebtWriteoffRecordBillRel updateRecordBill(PlaBadDebtWriteoffRecordBillRel appInfo){
        //借据编号
        String billNo = appInfo.getBillNo();
        // 贷款金额
        BigDecimal loanAmt = BigDecimal.ZERO;
        // 贷款余额
        BigDecimal loanBalance = BigDecimal.ZERO;
        // 拖欠利息总额
        BigDecimal totalTqlxAmt = BigDecimal.ZERO;

        ResultDto<Ln3100RespDto> ln3100RespDto=ln3100Server.queryHXBill(billNo);
        log.info("ln3100接口返回：" + ln3100RespDto);
        String ln3100Code = Optional.ofNullable(ln3100RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ln3100Meesage = Optional.ofNullable(ln3100RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        if (Objects.equals(ln3100Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            if(ln3100RespDto.getData() != null){
                loanAmt=ln3100RespDto.getData().getJiejuuje();
                loanBalance=ln3100RespDto.getData().getYuqibjin().add(ln3100RespDto.getData().getZhchbjin());
                totalTqlxAmt=ln3100RespDto.getData().getYsyjlixi().add(ln3100RespDto.getData().getCsyjlixi()).add(ln3100RespDto.getData().getYsqianxi())
                        .add(ln3100RespDto.getData().getCsqianxi()).add(ln3100RespDto.getData().getYsyjfaxi()).add(ln3100RespDto.getData().getCsyjfaxi()).add(ln3100RespDto.getData().getYshofaxi())
                        .add(ln3100RespDto.getData().getCshofaxi()).add(ln3100RespDto.getData().getYingjifx()).add(ln3100RespDto.getData().getFuxiiiii());
                appInfo.setLoanAmt(loanAmt);
                appInfo.setLoanBalance(loanBalance);
                appInfo.setTotalTqlxAmt(totalTqlxAmt);
                updateSelective(appInfo);
            }
        }
        return appInfo;
    }
    /**
     * @方法名称: deleteplaBadDebtWriteoffRecordBillRel
     * @方法描述: 根据主键删除
     * @参数与返回说明: 删除引用的时候需要更新主业务表中统计的金额信息
     * @算法描述: 无
     */
    public int deleteplaBadDebtWriteoffRecordBillRel(PlaBadDebtWriteoffRecordBillRel plaBadDebtWriteoffRecordBillRel) {
        int count = plaBadDebtWriteoffRecordBillRelMapper.deleteByPrimaryKey(plaBadDebtWriteoffRecordBillRel.getPbdwrbrSerno());
        return count;
    }
}
