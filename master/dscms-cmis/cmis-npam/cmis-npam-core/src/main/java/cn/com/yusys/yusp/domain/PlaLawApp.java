/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawApp
 * @类描述: pla_law_app数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-28 20:00:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_law_app")
public class PlaLawApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 诉讼申请流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "APP_SERNO")
    @NotBlank(message = "流水号为空")
    private String appSerno;

    /**
     * 诉讼方案名称
     **/
    @Column(name = "PLAN_NAME", unique = false, nullable = true, length = 50)
    private String planName;

    /**
     * 申请事项
     **/
    @Column(name = "APP_EVEN", unique = false, nullable = true, length = 5)
    private String appEven;

    /**
     * 当事人客户编号
     **/
    @Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
    private String cusId;

    /**
     * 当事人客户名称
     **/
    @Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
    private String cusName;

    /**
     * 当事人身份
     **/
    @Column(name = "PARTY_ROLE", unique = false, nullable = true, length = 5)
    private String partyRole;

    /**
     * 客户联系电话
     **/
    @Column(name = "LINK_PHONE", unique = false, nullable = true, length = 20)
    private String linkPhone;

    /**
     * 币种
     **/
    @Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
    private String curType;

    /**
     * 标的金额
     **/
    @Column(name = "TOTAL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalAmt;

    /**
     * 本金总额（元)
     **/
    @Column(name = "CAP_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal capAmt;

    /**
     * 拖欠利息总额
     **/
    @Column(name = "TOTAL_TQLX_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalTqlxAmt;

    /**
     * 五级分类
     **/
    @Column(name = "FIVE_CLASS", unique = false, nullable = true, length = 5)
    private String fiveClass;

    /**
     * 代理方式
     **/
    @Column(name = "AGT_MODE", unique = false, nullable = true, length = 5)
    private String agtMode;

    /**
     * 代理类型
     **/
    @Column(name = "AGT_TYPE", unique = false, nullable = true, length = 5)
    private String agtType;

    /**
     * 案由
     **/
    @Column(name = "CASE_REASON", unique = false, nullable = true, length = 5)
    private String caseReason;

    /**
     * 债务人基本情况
     **/
    @Column(name = "DEBTOR_BAS_CASE", unique = false, nullable = true, length = 2000)
    private String debtorBasCase;

    /**
     * 担保人基本情况
     **/
    @Column(name = "GUAR_BAS_CASE", unique = false, nullable = true, length = 2000)
    private String guarBasCase;

    /**
     * 支行处置情况
     **/
    @Column(name = "DISP_CASE", unique = false, nullable = true, length = 2000)
    private String dispCase;

    /**
     * 起诉理由和意见
     **/
    @Column(name = "RESN_ADVICE", unique = false, nullable = true, length = 2000)
    private String resnAdvice;

    /**
     * 备注
     **/
    @Column(name = "MEMO", unique = false, nullable = true, length = 2000)
    private String memo;

    /**
     * 审批状态
     **/
    @Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
    private String approveStatus;

    /**
     * 责任人
     **/
    @Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
    private String managerId;

    /**
     * 责任机构
     **/
    @Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
    private String managerBrId;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaLawApp() {
        // default implementation ignored
    }

    /**
     * @param appSerno
     */
    public void setAppSerno(String appSerno) {
        this.appSerno = appSerno;
    }

    /**
     * @return appSerno
     */
    public String getAppSerno() {
        return this.appSerno;
    }

    /**
     * @param planName
     */
    public void setPlanName(String planName) {
        this.planName = planName;
    }

    /**
     * @return planName
     */
    public String getPlanName() {
        return this.planName;
    }

    /**
     * @param appEven
     */
    public void setAppEven(String appEven) {
        this.appEven = appEven;
    }

    /**
     * @return appEven
     */
    public String getAppEven() {
        return this.appEven;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param partyRole
     */
    public void setPartyRole(String partyRole) {
        this.partyRole = partyRole;
    }

    /**
     * @return partyRole
     */
    public String getPartyRole() {
        return this.partyRole;
    }

    /**
     * @param linkPhone
     */
    public void setLinkPhone(String linkPhone) {
        this.linkPhone = linkPhone;
    }

    /**
     * @return linkPhone
     */
    public String getLinkPhone() {
        return this.linkPhone;
    }

    /**
     * @param curType
     */
    public void setCurType(String curType) {
        this.curType = curType;
    }

    /**
     * @return curType
     */
    public String getCurType() {
        return this.curType;
    }

    /**
     * @param totalAmt
     */
    public void setTotalAmt(java.math.BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }

    /**
     * @return totalAmt
     */
    public java.math.BigDecimal getTotalAmt() {
        return this.totalAmt;
    }

    /**
     * @param capAmt
     */
    public void setCapAmt(java.math.BigDecimal capAmt) {
        this.capAmt = capAmt;
    }

    /**
     * @return capAmt
     */
    public java.math.BigDecimal getCapAmt() {
        return this.capAmt;
    }

    /**
     * @param totalTqlxAmt
     */
    public void setTotalTqlxAmt(java.math.BigDecimal totalTqlxAmt) {
        this.totalTqlxAmt = totalTqlxAmt;
    }

    /**
     * @return totalTqlxAmt
     */
    public java.math.BigDecimal getTotalTqlxAmt() {
        return this.totalTqlxAmt;
    }

    /**
     * @param fiveClass
     */
    public void setFiveClass(String fiveClass) {
        this.fiveClass = fiveClass;
    }

    /**
     * @return fiveClass
     */
    public String getFiveClass() {
        return this.fiveClass;
    }

    /**
     * @param agtMode
     */
    public void setAgtMode(String agtMode) {
        this.agtMode = agtMode;
    }

    /**
     * @return agtMode
     */
    public String getAgtMode() {
        return this.agtMode;
    }

    /**
     * @param agtType
     */
    public void setAgtType(String agtType) {
        this.agtType = agtType;
    }

    /**
     * @return agtType
     */
    public String getAgtType() {
        return this.agtType;
    }

    /**
     * @param caseReason
     */
    public void setCaseReason(String caseReason) {
        this.caseReason = caseReason;
    }

    /**
     * @return caseReason
     */
    public String getCaseReason() {
        return this.caseReason;
    }

    /**
     * @param debtorBasCase
     */
    public void setDebtorBasCase(String debtorBasCase) {
        this.debtorBasCase = debtorBasCase;
    }

    /**
     * @return debtorBasCase
     */
    public String getDebtorBasCase() {
        return this.debtorBasCase;
    }

    /**
     * @param guarBasCase
     */
    public void setGuarBasCase(String guarBasCase) {
        this.guarBasCase = guarBasCase;
    }

    /**
     * @return guarBasCase
     */
    public String getGuarBasCase() {
        return this.guarBasCase;
    }

    /**
     * @param dispCase
     */
    public void setDispCase(String dispCase) {
        this.dispCase = dispCase;
    }

    /**
     * @return dispCase
     */
    public String getDispCase() {
        return this.dispCase;
    }

    /**
     * @param resnAdvice
     */
    public void setResnAdvice(String resnAdvice) {
        this.resnAdvice = resnAdvice;
    }

    /**
     * @return resnAdvice
     */
    public String getResnAdvice() {
        return this.resnAdvice;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param approveStatus
     */
    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    /**
     * @return approveStatus
     */
    public String getApproveStatus() {
        return this.approveStatus;
    }

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    /**
     * @return managerId
     */
    public String getManagerId() {
        return this.managerId;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    /**
     * @return managerBrId
     */
    public String getManagerBrId() {
        return this.managerBrId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}