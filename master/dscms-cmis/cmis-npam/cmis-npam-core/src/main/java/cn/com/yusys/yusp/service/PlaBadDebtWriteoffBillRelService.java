/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.DefaultDataHandle;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.user.UserIdentity;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.PlaBadDebtWriteoffAppBatch;
import cn.com.yusys.yusp.domain.PlaBadDebtWriteoffAppSig;
import cn.com.yusys.yusp.dto.LawCaseInfoDto;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.vo.PlaBadDebtWriteoffBillVo;
import cn.com.yusys.yusp.vo.PlaLawCaseInfoVo;
import cn.com.yusys.yusp.vo.PlaPlanDetailVo;
import cn.com.yusys.yusp.web.rest.PlaPlanDetailResource;
import com.alibaba.fastjson.JSON;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaBadDebtWriteoffBillRel;
import cn.com.yusys.yusp.repository.mapper.PlaBadDebtWriteoffBillRelMapper;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBadDebtWriteoffBillRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-06-09 09:34:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaBadDebtWriteoffBillRelService {
    private static final Logger log = LoggerFactory.getLogger(PlaPlanDetailResource.class);
    @Autowired
    private PlaBadDebtWriteoffBillRelMapper plaBadDebtWriteoffBillRelMapper;
    @Autowired
    private PlaBadDebtWriteoffAppSigService plaBadDebtWriteoffAppSigService;
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;
    @Autowired
    private PlaBadDebtWriteoffAppBatchService plaBadDebtWriteoffAppBatchService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PlaBadDebtWriteoffBillRel selectByPrimaryKey(String pwbrSerno) {
        return plaBadDebtWriteoffBillRelMapper.selectByPrimaryKey(pwbrSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaBadDebtWriteoffBillRel> selectAll(QueryModel model) {
        List<PlaBadDebtWriteoffBillRel> records = (List<PlaBadDebtWriteoffBillRel>) plaBadDebtWriteoffBillRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PlaBadDebtWriteoffBillRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaBadDebtWriteoffBillRel> list = plaBadDebtWriteoffBillRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PlaBadDebtWriteoffBillRel record) {
        return plaBadDebtWriteoffBillRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PlaBadDebtWriteoffBillRel record) {
        return plaBadDebtWriteoffBillRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PlaBadDebtWriteoffBillRel record) {
        return plaBadDebtWriteoffBillRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PlaBadDebtWriteoffBillRel record) {
        return plaBadDebtWriteoffBillRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pwbrSerno) {
        return plaBadDebtWriteoffBillRelMapper.deleteByPrimaryKey(pwbrSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaBadDebtWriteoffBillRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: save
     * @方法描述:
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    public int save(PlaBadDebtWriteoffBillRel record) {
        int count=0;
        if(record !=null){
            //流水号
            String serno = record.getSerno();
            // 借据编号
            String billNo = record.getBillNo();
             // 判断该笔借据是否已经引用
            PlaBadDebtWriteoffBillRel PlaBadDebtWriteoffBillRel=plaBadDebtWriteoffBillRelMapper.selectPlaBadDebtWriteoffBillRel(record);
            if(PlaBadDebtWriteoffBillRel !=null){
                throw BizException.error("null", EcnEnum.ECN060003.key,EcnEnum.ECN060003.value+":"+billNo);
            }
            int  counts=plaBadDebtWriteoffBillRelMapper.selectCount(record);
            if(counts >0 ){
                throw BizException.error("null", EcnEnum.ECN060003.key,EcnEnum.ECN060003.value+":"+billNo+EcnEnum.ECN060024.value);
            }
            // 核销总金额
            String totalWriteoffAmt = "0";
            // 核销总本金
            String totalWriteoffCap = "0";
            // 核销总利息
            String totalWriteoffInt = "0";

            // 核销总笔数
            String totalWriteoffNum ="0";
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if(userInfo != null){
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            record.setInputId(inputId);
            record.setWriteoffFlag("01");
            record.setInputBrId(inputBrId);
            record.setInputDate(openDay);
            record.setCreateTime(createTime);
            record.setUpdId(inputId);
            record.setUpdBrId(inputBrId);
            record.setUpdDate(openDay);
            record.setUpdateTime(createTime);
            // 流水号
            // String serno= StringUtils.getUUID();//sequenceTemplateService.getSequenceTemplate("", new HashMap<>());
            count=plaBadDebtWriteoffBillRelMapper.insert(record);
            if(count >0){
                PlaBadDebtWriteoffAppSig plaBadDebtWriteoffAppSig =new PlaBadDebtWriteoffAppSig();
                Map<String,Object> amtMap= plaBadDebtWriteoffBillRelMapper.selectBySerno(serno,"01");
                if(amtMap.containsKey("totalwriteoffnum")){
                    totalWriteoffNum= amtMap.get("totalwriteoffnum").toString();
                }
                if(amtMap.containsKey("totalwriteoffamt")){
                    totalWriteoffAmt= amtMap.get("totalwriteoffamt").toString();
                }
                if(amtMap.containsKey("totalwriteoffcap")){
                    totalWriteoffCap= amtMap.get("totalwriteoffcap").toString();
                }
                if(amtMap.containsKey("totalwriteoffint")){
                    totalWriteoffInt=amtMap.get("totalwriteoffint").toString();
                }
                // 修改主表金额
                plaBadDebtWriteoffAppSig.setPbdwasSerno(serno);
                plaBadDebtWriteoffAppSig.setTotalWriteoffAmt(NumberUtils.toBigDecimal(totalWriteoffAmt));
                plaBadDebtWriteoffAppSig.setTotalWriteoffCap(NumberUtils.toBigDecimal(totalWriteoffCap));
                plaBadDebtWriteoffAppSig.setTotalWriteoffInt(NumberUtils.toBigDecimal(totalWriteoffInt));
                plaBadDebtWriteoffAppSig.setTotalWriteoffNum(totalWriteoffNum);
                plaBadDebtWriteoffAppSigService.updateSelective(plaBadDebtWriteoffAppSig);
            }
        }
        return count;
    }
    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主表流水号删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteBySerno(String serno) {
        return plaBadDebtWriteoffBillRelMapper.deleteBySerno(serno);
    }
    /**
     * @方法名称: deleteBill
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteBill(PlaBadDebtWriteoffBillRel plaBadDebtWriteoffBillRel) {
        int count =0;
        if(plaBadDebtWriteoffBillRel !=null){
            String pwbrSerno= plaBadDebtWriteoffBillRel.getPwbrSerno();
            String serno= plaBadDebtWriteoffBillRel.getSerno();
            count = plaBadDebtWriteoffBillRelMapper.deleteByPrimaryKey(pwbrSerno);
            if(count >0){
                // 核销总金额
                BigDecimal totalWriteoffAmt = new BigDecimal("0.00");
                // 核销总本金
                BigDecimal totalWriteoffCap = new BigDecimal("0.00");
                // 核销总利息
                BigDecimal totalWriteoffInt = new BigDecimal("0.00");
                // 核销总笔数
                String totalWriteoffNum ="0";
                PlaBadDebtWriteoffAppSig plaBadDebtWriteoffAppSig =new PlaBadDebtWriteoffAppSig();
                Map amtMap= plaBadDebtWriteoffBillRelMapper.selectBySerno(serno,"01");
                if(amtMap.containsKey("totalwriteoffnum")){
                    totalWriteoffNum= amtMap.get("totalwriteoffnum").toString();
                }
                if(amtMap.containsKey("totalwriteoffamt")){
                    totalWriteoffAmt= new BigDecimal(amtMap.get("totalwriteoffamt").toString());
                }
                if(amtMap.containsKey("totalWriteoffCap")){
                    totalWriteoffCap= new BigDecimal(amtMap.get("totalwriteoffcap").toString());
                }
                if(amtMap.containsKey("totalwriteoffint")){
                    totalWriteoffInt=new BigDecimal(amtMap.get("totalwriteoffint").toString());
                }
                // 修改主表金额
                plaBadDebtWriteoffAppSig.setPbdwasSerno(serno);
                plaBadDebtWriteoffAppSig.setTotalWriteoffAmt(totalWriteoffAmt);
                plaBadDebtWriteoffAppSig.setTotalWriteoffCap(totalWriteoffCap);
                plaBadDebtWriteoffAppSig.setTotalWriteoffInt(totalWriteoffInt);
                plaBadDebtWriteoffAppSig.setTotalWriteoffNum(totalWriteoffNum);
                plaBadDebtWriteoffAppSigService.updateSelective(plaBadDebtWriteoffAppSig);
            }
        }
        return count;
    }
    /**
     * @方法名称: saveList
     * @方法描述: 批量保存
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    public int saveList(List<PlaBadDebtWriteoffBillRel> plaBadDebtWriteoffBillRelList) {
        int count =0;
        if(CollectionUtils.isNotEmpty(plaBadDebtWriteoffBillRelList)){
            for(PlaBadDebtWriteoffBillRel plaBadDebtWriteoffBillRel:plaBadDebtWriteoffBillRelList){
                count =save(plaBadDebtWriteoffBillRel);
            }
        }
        return count;
    }
    /**
     * @方法名称: asyncImportplanDetail
     * @方法描述: 导入
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    public String asyncImportplanDetail(String fileId,Map<String, String> paramsMap) {
        String serno = "";
        if (paramsMap.containsKey("pwbrSerno")) {
            serno = paramsMap.get("pwbrSerno");
        }
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = null;
        try {
            tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
            FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
        } catch (IOException e) {
            log.error(EcnEnum.ECN060004.value, e);
            throw BizException.error(null, EcnEnum.ECN060004.key, EcnEnum.ECN060004.value);
        }
        // 将文件内容导入数据库，StudentScore为导入数据的类
        String finalSerno = serno;
        ExcelUtils.syncImport(PlaBadDebtWriteoffBillVo.class, tempFile, new DefaultDataHandle(), ExcelUtils.batchConsumer(dataList -> {
            try {
                return insertPlaBadDebtWriteoffBill(dataList, finalSerno);
            } catch (Exception e) {
                throw BizException.error(null, EcnEnum.ECN069999.key, e.getMessage());
            }
        }), true);
        return EcnEnum.ECN060000.value;
    }
    /**
     * @方法名称: insertPlanDetail
     * @方法描述: 导入
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertPlaBadDebtWriteoffBill(List insertPlaBadDebtWriteoffBillList, String serno) throws Exception {
        int count = 0;
        List<PlaBadDebtWriteoffBillRel> plaBadDebtWriteoffBillList = (List<PlaBadDebtWriteoffBillRel>) BeanUtils.beansCopy(insertPlaBadDebtWriteoffBillList, PlaBadDebtWriteoffBillRel.class);

        Map<String, List<PlaBadDebtWriteoffBillRel>> groupMap = plaBadDebtWriteoffBillList.stream().collect(Collectors.groupingBy(PlaBadDebtWriteoffBillRel::getCusId)) ;
        if(groupMap.size()>1){
            for (String cusId : groupMap.keySet()) {
                List<PlaBadDebtWriteoffBillRel> plaBadDebtWriteoffBillRelList = groupMap.get(cusId) ;
                BigDecimal loanTotal = BigDecimal.ZERO ;
                for (PlaBadDebtWriteoffBillRel plaRel : plaBadDebtWriteoffBillRelList) {
                    BigDecimal loanBalance = NumberUtils.nullDefaultZero(plaRel.getLoanBalance()) ;
                    loanTotal = loanTotal.add(loanBalance) ;
                }
                //判断贷款余额必须小于等于5万
                if (loanTotal.longValue() > 50000){
                    throw BizException.error(null, EcnEnum.ECN060021.key,EcnEnum.ECN060021.value+"客户信息："+cusId);
                }
            }
        }


            for (PlaBadDebtWriteoffBillRel plaBadDebtWriteoffBill : plaBadDebtWriteoffBillList) {
                //判断时间
                String loanStartDate =plaBadDebtWriteoffBill.getLoanStartDate();
                if(loanStartDate.length()<10 && loanStartDate.indexOf("-")==-1){
                    throw BizException.error(null, EcnEnum.ECN060017.key,EcnEnum.ECN060017.value+plaBadDebtWriteoffBill.getBillNo());
                }

                QueryModel query = new QueryModel() ;
                query.addCondition("billNo", plaBadDebtWriteoffBill.getBillNo());
                query.addCondition("serno", serno);
                List<PlaBadDebtWriteoffBillRel> plaBadDebtWriteoffBills = plaBadDebtWriteoffBillRelMapper.selectByModel(query) ;
                //数据库中已存在，更新信息
                if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(plaBadDebtWriteoffBills)){
                    PlaBadDebtWriteoffBillRel PlaBadDebtWriteoffBillDetail = plaBadDebtWriteoffBills.get(0);
                    throw BizException.error(null, EcnEnum.ECN060003.key,EcnEnum.ECN060003.value+PlaBadDebtWriteoffBillDetail.getBillNo());
                }
                //数据库中不存在，新增信息
                else{
                    //生成主键
                    Map paramMap= new HashMap<>() ;
                    String pwbrSerno = sequenceTemplateClient.getSequenceTemplate("TRADE_ID_SEQ", paramMap);
                    plaBadDebtWriteoffBill.setPwbrSerno(pwbrSerno);
                    // 申请人
                    String inputId = "";
                    // 申请机构
                    String inputBrId = "";
                    // 申请时间
                    String openDay = stringRedisTemplate.opsForValue().get("openDay");
                    // 创建时间
                    Date createTime = DateUtils.getCurrTimestamp();
                    // 获取用户信息
                    User userInfo = SessionUtils.getUserInformation();
                    if(userInfo != null){
                        // 申请人
                        inputId = userInfo.getLoginCode();
                        // 申请机构
                        inputBrId = userInfo.getOrg().getCode();
                    }
                    plaBadDebtWriteoffBill.setSerno(serno);
                    plaBadDebtWriteoffBill.setWriteoffFlag("02");
                    plaBadDebtWriteoffBill.setInputId(inputId);
                    plaBadDebtWriteoffBill.setInputBrId(inputBrId);
                    plaBadDebtWriteoffBill.setInputDate(openDay);
                    plaBadDebtWriteoffBill.setCreateTime(createTime);
                    plaBadDebtWriteoffBill.setUpdId(inputId);
                    plaBadDebtWriteoffBill.setUpdBrId(inputBrId);
                    plaBadDebtWriteoffBill.setUpdDate(openDay);
                    plaBadDebtWriteoffBill.setUpdateTime(createTime);
                    count = insertSelective(plaBadDebtWriteoffBill);
                }

                    // 核销总金额
                    BigDecimal totalWriteoffAmt = new BigDecimal("0.00");
                    // 核销总本金
                    BigDecimal totalWriteoffCap = new BigDecimal("0.00");
                    // 核销总利息
                    BigDecimal totalWriteoffInt = new BigDecimal("0.00");
                    // 核销总笔数
                    String totalWriteoffNum ="0";
                    //核销总户数
                    String totalWriteoffCus ="0";
                    PlaBadDebtWriteoffAppBatch plaBadDebtWriteoffAppBatch =new PlaBadDebtWriteoffAppBatch();
                    Map amtMap= plaBadDebtWriteoffBillRelMapper.selectBySerno(serno,"02");
                    if(amtMap.containsKey("totalwriteoffnum")){
                        totalWriteoffNum= amtMap.get("totalwriteoffnum").toString();
                    }
                    if(amtMap.containsKey("totalwriteoffamt")){
                        totalWriteoffAmt= new BigDecimal(amtMap.get("totalwriteoffamt").toString());
                    }
                    if(amtMap.containsKey("totalwriteoffcap")){
                        totalWriteoffCap= new BigDecimal(amtMap.get("totalwriteoffcap").toString());
                    }
                    if(amtMap.containsKey("totalwriteoffint")){
                        totalWriteoffInt=new BigDecimal(amtMap.get("totalwriteoffint").toString());
                    }
                    if(amtMap.containsKey("totalwriteoffcus")){
                        totalWriteoffCus=amtMap.get("totalwriteoffcus").toString();
                    }
                    // 修改主表金额
                if(null == plaBadDebtWriteoffAppBatchService.selectByPrimaryKey(serno)){
                    plaBadDebtWriteoffAppBatch.setPbdwabSerno(serno);
                    plaBadDebtWriteoffAppBatch.setTotalWriteoffAmt(totalWriteoffAmt);
                    plaBadDebtWriteoffAppBatch.setTotalWriteoffCus(totalWriteoffCus);
                    plaBadDebtWriteoffAppBatch.setTotalWriteoffCap(totalWriteoffCap);
                    plaBadDebtWriteoffAppBatch.setTotalWriteoffInt(totalWriteoffInt);
                    plaBadDebtWriteoffAppBatch.setTotalWriteoffNum(totalWriteoffNum);
                    plaBadDebtWriteoffAppBatchService.save(plaBadDebtWriteoffAppBatch);
                }else{
                    plaBadDebtWriteoffAppBatch.setPbdwabSerno(serno);
                    plaBadDebtWriteoffAppBatch.setTotalWriteoffAmt(totalWriteoffAmt);
                    plaBadDebtWriteoffAppBatch.setTotalWriteoffCus(totalWriteoffCus);
                    plaBadDebtWriteoffAppBatch.setTotalWriteoffCap(totalWriteoffCap);
                    plaBadDebtWriteoffAppBatch.setTotalWriteoffInt(totalWriteoffInt);
                    plaBadDebtWriteoffAppBatch.setTotalWriteoffNum(totalWriteoffNum);
                    plaBadDebtWriteoffAppBatchService.updateSelective(plaBadDebtWriteoffAppBatch);
                }


            }
        return count;
    }
    /**
     * 模板
     * @return 导出进度信息
     */
    public ProgressDto asyncExportPlaBadDebtWriteoffBillRel(QueryModel model) {

        boolean flag = false;

        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel) object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            return selectByPlaBadDebtWriteoffBillRel(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(PlaBadDebtWriteoffBillVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    public List<PlaBadDebtWriteoffBillVo> selectByPlaBadDebtWriteoffBillRel(QueryModel model) {
        List<PlaBadDebtWriteoffBillVo> plaBadDebtWriteoffBillVoList = new ArrayList<>() ;
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaBadDebtWriteoffBillRel> list = plaBadDebtWriteoffBillRelMapper.selectByModel1(model);
        PageHelper.clearPage();

        for (PlaBadDebtWriteoffBillRel plaBadDebtWriteoffBillRel:list){
            PlaBadDebtWriteoffBillVo plaBadDebtWriteoffBillVo = new PlaBadDebtWriteoffBillVo();
            String orgName = OcaTranslatorUtils.getOrgName(plaBadDebtWriteoffBillRel.getManagerBrId());//责任机构
            String userName = OcaTranslatorUtils.getUserName(plaBadDebtWriteoffBillRel.getManagerId());   //责任人
            org.springframework.beans.BeanUtils.copyProperties(plaBadDebtWriteoffBillRel, plaBadDebtWriteoffBillVo);
            plaBadDebtWriteoffBillVo.setManagerBrIdName(orgName);
            plaBadDebtWriteoffBillVo.setManagerIdName(userName);
            plaBadDebtWriteoffBillVoList.add(plaBadDebtWriteoffBillVo);
        }
        return plaBadDebtWriteoffBillVoList;
    }

    /**
     * 批量呆账核销业务列表详情模板
     * @return 导出进度信息
     */
    public ProgressDto asyncExportPlaBad() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {

                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(PlaBadDebtWriteoffBillVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaBadDebtWriteoffBillRel> selectByserno(String serno) {
        return plaBadDebtWriteoffBillRelMapper.selectByserno(serno);
    }

}
