/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawPaymentInfo
 * @类描述: pla_law_payment_info数据实体类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-05 16:11:03
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_law_payment_info")
public class PlaLawPaymentInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 受偿流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PLPI_SERNO")
    private String plpiSerno;

    /**
     * 破产案件编号
     **/
    @Column(name = "BROKE_CASE_NO", unique = false, nullable = true, length = 40)
    private String brokeCaseNo;

    /**
     * 受偿金额
     **/
    @Column(name = "PAYMENT_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal paymentAmt;

    /**
     * 受偿日期
     **/
    @Column(name = "PAYMENT_DATE", unique = false, nullable = true, length = 10)
    private String paymentDate;

    /**
     * 债权类型
     **/
    @Column(name = "CLAIM_TYPE", unique = false, nullable = true, length = 5)
    private String claimType;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaLawPaymentInfo() {
        // default implementation ignored
    }

    /**
     * @param plpiSerno
     */
    public void setPlpiSerno(String plpiSerno) {
        this.plpiSerno = plpiSerno;
    }

    /**
     * @return plpiSerno
     */
    public String getPlpiSerno() {
        return this.plpiSerno;
    }

    /**
     * @param brokeCaseNo
     */
    public void setBrokeCaseNo(String brokeCaseNo) {
        this.brokeCaseNo = brokeCaseNo;
    }

    /**
     * @return brokeCaseNo
     */
    public String getBrokeCaseNo() {
        return this.brokeCaseNo;
    }

    /**
     * @param paymentAmt
     */
    public void setPaymentAmt(java.math.BigDecimal paymentAmt) {
        this.paymentAmt = paymentAmt;
    }

    /**
     * @return paymentAmt
     */
    public java.math.BigDecimal getPaymentAmt() {
        return this.paymentAmt;
    }

    /**
     * @param paymentDate
     */
    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    /**
     * @return paymentDate
     */
    public String getPaymentDate() {
        return this.paymentDate;
    }

    /**
     * @param claimType
     */
    public void setClaimType(String claimType) {
        this.claimType = claimType;
    }

    /**
     * @return claimType
     */
    public String getClaimType() {
        return this.claimType;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}