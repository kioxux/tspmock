/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.List;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.PageHelper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaLawPersonInfo;
import cn.com.yusys.yusp.repository.mapper.PlaLawPersonInfoMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawPersonInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-07 19:18:54
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaLawPersonInfoService {

    @Autowired
    private PlaLawPersonInfoMapper plaLawPersonInfoMapper;

    @Autowired
    private PlaLawAccusedListService plaLawAccusedListService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaLawPersonInfo selectByPrimaryKey(String plpiSerno) {
        return plaLawPersonInfoMapper.selectByPrimaryKey(plpiSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaLawPersonInfo> selectAll(QueryModel model) {
        List<PlaLawPersonInfo> records = (List<PlaLawPersonInfo>) plaLawPersonInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaLawPersonInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaLawPersonInfo> list = plaLawPersonInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaLawPersonInfo record) {
        return plaLawPersonInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaLawPersonInfo record) {
        return plaLawPersonInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaLawPersonInfo record) {
        return plaLawPersonInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaLawPersonInfo record) {
        return plaLawPersonInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String plpiSerno) {
        return plaLawPersonInfoMapper.deleteByPrimaryKey(plpiSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaLawPersonInfoMapper.deleteByIds(ids);
    }

    /**
     * 批量更新
     *
     * @param plaLawPersonInfo
     * @return
     */
    public int batchUpdate(List<PlaLawPersonInfo> plaLawPersonInfo) {
        // 申请人
        String inputId = "";
        // 申请机构
        String inputBrId = "";
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        // 创建时间
        Date createTime = DateUtils.getCurrTimestamp();
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            inputId = userInfo.getLoginCode();
            // 申请机构
            inputBrId = userInfo.getOrg().getCode();
        }
        for (PlaLawPersonInfo lawPersonInfo : plaLawPersonInfo) {
            if (plaLawPersonInfoMapper.selectByPrimaryKey(lawPersonInfo.getPlpiSerno()) != null) {
                lawPersonInfo.setInputId(inputId);
                lawPersonInfo.setInputBrId(inputBrId);
                lawPersonInfo.setInputDate(openDay);
                lawPersonInfo.setCreateTime(createTime);
                lawPersonInfo.setUpdId(inputId);
                lawPersonInfo.setUpdBrId(inputBrId);
                lawPersonInfo.setUpdDate(openDay);
                lawPersonInfo.setUpdateTime(createTime);
                plaLawPersonInfoMapper.insertSelective(lawPersonInfo);
            } else {
                lawPersonInfo.setUpdId(inputId);
                lawPersonInfo.setUpdBrId(inputBrId);
                lawPersonInfo.setUpdDate(openDay);
                lawPersonInfo.setUpdateTime(createTime);
                plaLawPersonInfoMapper.updateByPrimaryKeySelective(lawPersonInfo);
            }
        }
        return plaLawPersonInfo.size();
    }

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

   /* public PlaLawPersonInfo selectByCaseSerno(String caseSerno) {
        return plaLawPersonInfoMapper.selectByCaseSerno(caseSerno);
    }*/

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ResultDto<Integer> insertByAccusedList(PlaLawPersonInfo record) {
        ResultDto<Integer> resultDto = new ResultDto<Integer>();
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("cusId", record.getCusId());
        queryModel.addCondition("caseSerno", record.getCaseSerno());
        PlaLawPersonInfo plaLawPersonInfo = plaLawPersonInfoMapper.selectByCaseSerno(queryModel);
        int result = 0;
        if (plaLawPersonInfo != null){
            resultDto.setCode("1");
            resultDto.setMessage("不能引入重复的被执行人");
        }else {
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            record.setInputDate(openDay);
            record.setCreateTime(createTime);
            record.setUpdDate(openDay);
            record.setUpdateTime(createTime);
            result =  plaLawPersonInfoMapper.insert(record);
            resultDto.setMessage("引入成功");
        }
        return resultDto;
    }
}
