/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaUncretBadAssetAccList
 * @类描述: pla_uncret_bad_asset_acc_list数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 15:56:25
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_uncret_bad_asset_acc_list")
public class PlaUncretBadAssetAccList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 关联表流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PUBAAL_SERNO")
    private String pubaalSerno;

    /**
     * 客户编号
     **/
    @Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
    private String cusId;

    /**
     * 客户名称
     **/
    @Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
    private String cusName;

    /**
     * 交易编号
     **/
    @Column(name = "TRAN_ID", unique = false, nullable = true, length = 20)
    private String tranId;

    /**
     * 交易金额
     **/
    @Column(name = "TRAN_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal tranAmt;

    /**
     * 币种
     **/
    @Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
    private String curType;

    /**
     * 投资资产名称
     **/
    @Column(name = "INVEST_ASSET_NAME", unique = false, nullable = true, length = 200)
    private String investAssetName;

    /**
     * 交易品种名称
     **/
    @Column(name = "TRAN_BIZ_NAME", unique = false, nullable = true, length = 80)
    private String tranBizName;

    /**
     * 交易成交日期
     **/
    @Column(name = "TRAN_DEAL_DATE", unique = false, nullable = true, length = 20)
    private String tranDealDate;

    /**
     * 交易到期日期
     **/
    @Column(name = "TRAN_END_DATE", unique = false, nullable = true, length = 20)
    private String tranEndDate;

    /**
     * 处置方式
     **/
    @Column(name = "DISP_MODE", unique = false, nullable = true, length = 5)
    private String dispMode;

    /**
     * 进展描述
     **/
    @Column(name = "PRGR_DEC", unique = false, nullable = true, length = 2000)
    private String prgrDec;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaUncretBadAssetAccList() {
        // default implementation ignored
    }

    /**
     * @param pubaalSerno
     */
    public void setPubaalSerno(String pubaalSerno) {
        this.pubaalSerno = pubaalSerno;
    }

    /**
     * @return pubaalSerno
     */
    public String getPubaalSerno() {
        return this.pubaalSerno;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param tranId
     */
    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    /**
     * @return tranId
     */
    public String getTranId() {
        return this.tranId;
    }

    /**
     * @param tranAmt
     */
    public void setTranAmt(java.math.BigDecimal tranAmt) {
        this.tranAmt = tranAmt;
    }

    /**
     * @return tranAmt
     */
    public java.math.BigDecimal getTranAmt() {
        return this.tranAmt;
    }

    /**
     * @param curType
     */
    public void setCurType(String curType) {
        this.curType = curType;
    }

    /**
     * @return curType
     */
    public String getCurType() {
        return this.curType;
    }

    /**
     * @param investAssetName
     */
    public void setInvestAssetName(String investAssetName) {
        this.investAssetName = investAssetName;
    }

    /**
     * @return investAssetName
     */
    public String getInvestAssetName() {
        return this.investAssetName;
    }

    /**
     * @param tranBizName
     */
    public void setTranBizName(String tranBizName) {
        this.tranBizName = tranBizName;
    }

    /**
     * @return tranBizName
     */
    public String getTranBizName() {
        return this.tranBizName;
    }

    /**
     * @param tranDealDate
     */
    public void setTranDealDate(String tranDealDate) {
        this.tranDealDate = tranDealDate;
    }

    /**
     * @return tranDealDate
     */
    public String getTranDealDate() {
        return this.tranDealDate;
    }

    /**
     * @param tranEndDate
     */
    public void setTranEndDate(String tranEndDate) {
        this.tranEndDate = tranEndDate;
    }

    /**
     * @return tranEndDate
     */
    public String getTranEndDate() {
        return this.tranEndDate;
    }

    /**
     * @param dispMode
     */
    public void setDispMode(String dispMode) {
        this.dispMode = dispMode;
    }

    /**
     * @return dispMode
     */
    public String getDispMode() {
        return this.dispMode;
    }

    /**
     * @param prgrDec
     */
    public void setPrgrDec(String prgrDec) {
        this.prgrDec = prgrDec;
    }

    /**
     * @return prgrDec
     */
    public String getPrgrDec() {
        return this.prgrDec;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}