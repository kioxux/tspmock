/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.PlaBadDebtWriteoffRecordApp;
import cn.com.yusys.yusp.dto.PlaBadDebtWriteoffRecordBillRelDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaBadDebtWriteoffRecordBillRel;
import cn.com.yusys.yusp.service.PlaBadDebtWriteoffRecordBillRelService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBadDebtWriteoffRecordBillRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-06-09 09:34:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plabaddebtwriteoffrecordbillrel")
public class PlaBadDebtWriteoffRecordBillRelResource {
    @Autowired
    private PlaBadDebtWriteoffRecordBillRelService plaBadDebtWriteoffRecordBillRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaBadDebtWriteoffRecordBillRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaBadDebtWriteoffRecordBillRel> list = plaBadDebtWriteoffRecordBillRelService.selectAll(queryModel);
        return new ResultDto<List<PlaBadDebtWriteoffRecordBillRel>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/query")
    protected ResultDto<List<PlaBadDebtWriteoffRecordBillRel>> index(QueryModel queryModel) {
        List<PlaBadDebtWriteoffRecordBillRel> list = plaBadDebtWriteoffRecordBillRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaBadDebtWriteoffRecordBillRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{pbdwrbrSerno}")
    protected ResultDto<PlaBadDebtWriteoffRecordBillRel> show(@PathVariable("pbdwrbrSerno") String pbdwrbrSerno) {
        PlaBadDebtWriteoffRecordBillRel plaBadDebtWriteoffRecordBillRel = plaBadDebtWriteoffRecordBillRelService.selectByPrimaryKey(pbdwrbrSerno);
        return new ResultDto<PlaBadDebtWriteoffRecordBillRel>(plaBadDebtWriteoffRecordBillRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaBadDebtWriteoffRecordBillRelDto> create(@RequestBody PlaBadDebtWriteoffRecordBillRelDto plaBadDebtWriteoffRecordBillRel) throws URISyntaxException {
        plaBadDebtWriteoffRecordBillRelService.insert(plaBadDebtWriteoffRecordBillRel);
        return new ResultDto<PlaBadDebtWriteoffRecordBillRelDto>(plaBadDebtWriteoffRecordBillRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaBadDebtWriteoffRecordBillRel plaBadDebtWriteoffRecordBillRel) throws URISyntaxException {
        int result = plaBadDebtWriteoffRecordBillRelService.update(plaBadDebtWriteoffRecordBillRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody PlaBadDebtWriteoffRecordBillRel plaBadDebtWriteoffRecordBillRel) {
        int result = plaBadDebtWriteoffRecordBillRelService.deleteByPrimaryKey(plaBadDebtWriteoffRecordBillRel);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaBadDebtWriteoffRecordBillRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *分页查询类
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "核销记账借据列表查询")
    @PostMapping("/queryPlaBadDebtWriteoffRecordBillRel")
    protected ResultDto<List<PlaBadDebtWriteoffRecordBillRel>> queryPlaBadDebtWriteoffRecordBillRel(@RequestBody QueryModel queryModel) {
        List<PlaBadDebtWriteoffRecordBillRel> list = plaBadDebtWriteoffRecordBillRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaBadDebtWriteoffRecordBillRel>>(list);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "借据引入")
    @PostMapping("/save")
    protected ResultDto<PlaBadDebtWriteoffRecordBillRelDto> save(@RequestBody PlaBadDebtWriteoffRecordBillRelDto plaBadDebtWriteoffRecordBillRelDto) throws URISyntaxException {
        plaBadDebtWriteoffRecordBillRelService.save(plaBadDebtWriteoffRecordBillRelDto);
        return new ResultDto<PlaBadDebtWriteoffRecordBillRelDto>(plaBadDebtWriteoffRecordBillRelDto);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "借据引入多选")
    @PostMapping("/saveList")
    protected ResultDto<Integer> saveList(@RequestBody List<PlaBadDebtWriteoffRecordBillRelDto> plaBadDebtWriteoffRecordBillRelList) throws URISyntaxException {
        int count =plaBadDebtWriteoffRecordBillRelService.saveList(plaBadDebtWriteoffRecordBillRelList);
        return new ResultDto<Integer>(count);
    }

    /**
     * 呆账核销发送贷款核销处理
     * 记账补发
     * @author 刘权
     **/
    @ApiOperation("呆账核销发送贷款核销处理")
    @PostMapping("/sendtodkhxcl")
    protected ResultDto<String> sendToDKHXCL(@RequestBody PlaBadDebtWriteoffRecordBillRel appInfo) {
        ResultDto<String> result=plaBadDebtWriteoffRecordBillRelService.sendToDKHXCL(appInfo);
        return result;
    }

    /**
     * @创建人  刘权
     * @创建时间
     * @注释 记账冲正
     */
    @PostMapping("/czcl")
    protected ResultDto czcl( @RequestBody PlaBadDebtWriteoffRecordBillRel plaBadDebtWriteoffRecordBillRel) {
        return  plaBadDebtWriteoffRecordBillRelService.ib1241(plaBadDebtWriteoffRecordBillRel);

    }
}
