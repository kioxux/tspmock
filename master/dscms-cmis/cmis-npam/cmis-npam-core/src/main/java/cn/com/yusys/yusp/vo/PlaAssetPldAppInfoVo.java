/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.util.Date;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldAppInfo
 * @类描述: pla_asset_pld_app_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@ExcelCsv(namePrefix = "抵债资产台账导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class PlaAssetPldAppInfoVo {

    /**
     * 业务流水号
     **/
    @ExcelField(title = "业务流水号", viewLength = 20)
    private String papaiSerno;

    /**
     * 抵债资产编号
     **/
    @ExcelField(title = "抵债资产编号", viewLength = 20)
    private String pldimnNo;

    /**
     * 抵债资产名称
     **/
    @ExcelField(title = "抵债资产名称", viewLength = 20)
    private String pldimnName;

    /**
     * 客户编号
     **/
    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /**
     * 客户名称
     **/
    @ExcelField(title = "客户名称", viewLength = 20)
    private String cusName;


    /**
     * 处置方式
     **/
    @ExcelField(title = "处置方式", dictCode = "STD_DISP_MODE" , viewLength = 20)
    private String dispMode;


    /**
     * 登记人
     **/
    @ExcelField(title = "登记人", viewLength = 20)
    private String inputIdName;

    /**
     * 登记机构
     **/
    @ExcelField(title = "登记机构", viewLength = 30)
    private String inputBrIdName;

    /**
     * 记账日期
     **/
    @ExcelField(title = "记账日期", viewLength = 10)
    private String recordDate;

    /**
     * 记账状态
     **/
    @ExcelField(title = "记账状态", dictCode = "STD_RECORD_STATUS" , viewLength = 10)
    private String recordStatus;


    public String getPapaiSerno() {
        return papaiSerno;
    }

    public void setPapaiSerno(String papaiSerno) {
        this.papaiSerno = papaiSerno;
    }

    public String getPldimnNo() {
        return pldimnNo;
    }

    public void setPldimnNo(String pldimnNo) {
        this.pldimnNo = pldimnNo;
    }

    public String getPldimnName() {
        return pldimnName;
    }

    public void setPldimnName(String pldimnName) {
        this.pldimnName = pldimnName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getDispMode() {
        return dispMode;
    }

    public void setDispMode(String dispMode) {
        this.dispMode = dispMode;
    }

    public String getInputIdName() {
        return inputIdName;
    }

    public void setInputIdName(String inputIdName) {
        this.inputIdName = inputIdName;
    }

    public String getInputBrIdName() {
        return inputBrIdName;
    }

    public void setInputBrIdName(String inputBrIdName) {
        this.inputBrIdName = inputBrIdName;
    }

    public String getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(String recordDate) {
        this.recordDate = recordDate;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }
}