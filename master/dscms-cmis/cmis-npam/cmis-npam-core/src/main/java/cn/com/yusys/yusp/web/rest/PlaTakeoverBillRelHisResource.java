/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.PlaTakeoverBillRel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaTakeoverBillRelHis;
import cn.com.yusys.yusp.service.PlaTakeoverBillRelHisService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaTakeoverBillRelHisResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-29 16:14:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/platakeoverbillrelhis")
public class PlaTakeoverBillRelHisResource {
    @Autowired
    private PlaTakeoverBillRelHisService plaTakeoverBillRelHisService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaTakeoverBillRelHis>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaTakeoverBillRelHis> list = plaTakeoverBillRelHisService.selectAll(queryModel);
        return new ResultDto<List<PlaTakeoverBillRelHis>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaTakeoverBillRelHis>> index(QueryModel queryModel) {
        List<PlaTakeoverBillRelHis> list = plaTakeoverBillRelHisService.selectByModel(queryModel);
        return new ResultDto<List<PlaTakeoverBillRelHis>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{ptbrhSerno}")
    protected ResultDto<PlaTakeoverBillRelHis> show(@PathVariable("ptbrhSerno") String ptbrhSerno) {
        PlaTakeoverBillRelHis plaTakeoverBillRelHis = plaTakeoverBillRelHisService.selectByPrimaryKey(ptbrhSerno);
        return new ResultDto<PlaTakeoverBillRelHis>(plaTakeoverBillRelHis);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaTakeoverBillRelHis> create(@RequestBody PlaTakeoverBillRelHis plaTakeoverBillRelHis) throws URISyntaxException {
        plaTakeoverBillRelHisService.insert(plaTakeoverBillRelHis);
        return new ResultDto<PlaTakeoverBillRelHis>(plaTakeoverBillRelHis);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaTakeoverBillRelHis plaTakeoverBillRelHis) throws URISyntaxException {
        int result = plaTakeoverBillRelHisService.update(plaTakeoverBillRelHis);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{ptbrhSerno}")
    protected ResultDto<Integer> delete(@PathVariable("ptbrhSerno") String ptbrhSerno) {
        int result = plaTakeoverBillRelHisService.deleteByPrimaryKey(ptbrhSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaTakeoverBillRelHisService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryPlaTakeoverBillelHis")
    protected ResultDto<List<PlaTakeoverBillRelHis>> queryPlaTakeoverBillelHis(@RequestBody QueryModel queryModel) {
        List<PlaTakeoverBillRelHis> list = plaTakeoverBillRelHisService.selectByModel(queryModel);
        return new ResultDto<List<PlaTakeoverBillRelHis>>(list);
    }


}
