/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaAssetPldPropertyConvInfo;
import cn.com.yusys.yusp.service.PlaAssetPldPropertyConvInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldPropertyConvInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plaassetpldpropertyconvinfo")
public class PlaAssetPldPropertyConvInfoResource {
    @Autowired
    private PlaAssetPldPropertyConvInfoService plaAssetPldPropertyConvInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaAssetPldPropertyConvInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaAssetPldPropertyConvInfo> list = plaAssetPldPropertyConvInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaAssetPldPropertyConvInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<PlaAssetPldPropertyConvInfo>> index(QueryModel queryModel) {
        List<PlaAssetPldPropertyConvInfo> list = plaAssetPldPropertyConvInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaAssetPldPropertyConvInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pappciSerno}")
    protected ResultDto<PlaAssetPldPropertyConvInfo> show(@PathVariable("pappciSerno") String pappciSerno) {
        PlaAssetPldPropertyConvInfo plaAssetPldPropertyConvInfo = plaAssetPldPropertyConvInfoService.selectByPrimaryKey(pappciSerno);
        return new ResultDto<PlaAssetPldPropertyConvInfo>(plaAssetPldPropertyConvInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<PlaAssetPldPropertyConvInfo> create(@RequestBody PlaAssetPldPropertyConvInfo plaAssetPldPropertyConvInfo) throws URISyntaxException {
        plaAssetPldPropertyConvInfoService.insert(plaAssetPldPropertyConvInfo);
        return new ResultDto<PlaAssetPldPropertyConvInfo>(plaAssetPldPropertyConvInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaAssetPldPropertyConvInfo plaAssetPldPropertyConvInfo) throws URISyntaxException {
        int result = plaAssetPldPropertyConvInfoService.update(plaAssetPldPropertyConvInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pappciSerno}")
    protected ResultDto<Integer> delete(@PathVariable("pappciSerno") String pappciSerno) {
        int result = plaAssetPldPropertyConvInfoService.deleteByPrimaryKey(pappciSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaAssetPldPropertyConvInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据处置申请流水号获取转固信息
     *
     * @author jijian_yx
     * @date 2021/6/10 11:27
     **/
    @ApiOperation("根据处置申请流水号获取转固信息")
    @PostMapping("/querybypapaiserno")
    protected ResultDto<PlaAssetPldPropertyConvInfo> queryByPapaiSerno(@RequestBody String papaiSerno) {
        PlaAssetPldPropertyConvInfo plaAssetPldPropertyConvInfo = plaAssetPldPropertyConvInfoService.selectByPapaiSerno(papaiSerno);
        return new ResultDto<PlaAssetPldPropertyConvInfo>(plaAssetPldPropertyConvInfo);
    }

    /**
     * 保存/更新转固信息
     *
     * @author jijian_yx
     * @date 2021/6/11 9:19
     **/
    @ApiOperation("保存/更新转固信息")
    @PostMapping("/insert")
    protected ResultDto<Map<String,String>> insert(@RequestBody PlaAssetPldPropertyConvInfo plaAssetPldPropertyConvInfo) {
        Map<String,String> map = plaAssetPldPropertyConvInfoService.insertInfo(plaAssetPldPropertyConvInfo);
        return new ResultDto<Map<String,String>>(map);
    }
}
