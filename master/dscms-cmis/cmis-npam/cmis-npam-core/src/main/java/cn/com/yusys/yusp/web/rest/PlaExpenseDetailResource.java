/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.PlaExpenseDetail;
import cn.com.yusys.yusp.service.PlaExpenseDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaExpenseDetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 15:20:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plaexpensedetail")
public class PlaExpenseDetailResource {
    @Autowired
    private PlaExpenseDetailService plaExpenseDetailService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaExpenseDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaExpenseDetail> list = plaExpenseDetailService.selectAll(queryModel);
        return new ResultDto<List<PlaExpenseDetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaExpenseDetail>> index(QueryModel queryModel) {
        List<PlaExpenseDetail> list = plaExpenseDetailService.selectByModel(queryModel);
        return new ResultDto<List<PlaExpenseDetail>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pedSerno}")
    protected ResultDto<PlaExpenseDetail> show(@PathVariable("pedSerno") String pedSerno) {
        PlaExpenseDetail plaExpenseDetail = plaExpenseDetailService.selectByPrimaryKey(pedSerno);
        return new ResultDto<PlaExpenseDetail>(plaExpenseDetail);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaExpenseDetail> create(@RequestBody PlaExpenseDetail plaExpenseDetail) throws URISyntaxException {
        plaExpenseDetailService.insert(plaExpenseDetail);
        return new ResultDto<PlaExpenseDetail>(plaExpenseDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaExpenseDetail plaExpenseDetail) throws URISyntaxException {
        int result = plaExpenseDetailService.update(plaExpenseDetail);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pedSerno}")
    protected ResultDto<Integer> delete(@PathVariable("pedSerno") String pedSerno) {
        int result = plaExpenseDetailService.deleteByPrimaryKey(pedSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaExpenseDetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleteExpenseDetail
     * @函数描述:单个对象删除
     * @参数与返回说明:
     * @算法描述:
     * @ author liuquan
     */
    @PostMapping("/deleteExpenseDetail")
    protected ResultDto<Integer> deleteExpenseDetail(@RequestBody PlaExpenseDetail plaExpenseDetail) {
        int result = plaExpenseDetailService.deleteByExpenseDetail(plaExpenseDetail);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据条件查询.
     *
     * @return
     */
    @PostMapping("/selectByCondition")
    protected ResultDto<List<PlaExpenseDetail>> selectByCondition(@RequestBody  QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())){
            queryModel.setSort(" ped_serno desc");
        }
        List<PlaExpenseDetail> list = plaExpenseDetailService.selectByModel(queryModel);
        return new ResultDto<List<PlaExpenseDetail>>(list);
    }


}
