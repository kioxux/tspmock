/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaTakeoverBillRel;
import cn.com.yusys.yusp.dto.client.esb.core.ln3061.LstDkzrjj;
import cn.com.yusys.yusp.dto.client.esb.core.ln3063.Lstzrjj;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaTakeoverBillRelMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 19:54:27
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface PlaTakeoverBillRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    PlaTakeoverBillRel selectByPrimaryKey(@Param("ptbrSerno") String ptbrSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PlaTakeoverBillRel> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insert(PlaTakeoverBillRel record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insertSelective(PlaTakeoverBillRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPrimaryKey(PlaTakeoverBillRel record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPrimaryKeySelective(PlaTakeoverBillRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    int deleteByPrimaryKey(@Param("ptbrSerno") String ptbrSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectByPtaiSerno
     * @方法描述: 查询借据是否已经引入
     * @参数与返回说明:
     * @算法描述: 无
     */
    PlaTakeoverBillRel selectByPtaiSerno(Map map);

    /**
     * @方法名称: selectByPtaiSerno
     * @方法描述: 计算金额
     * @参数与返回说明:
     * @算法描述: 无
     */
    Map selectBySum(String ptaiSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<Lstzrjj> selectByPtaiSernoList(@Param("ptaiSerno") String ptaiSerno);
    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LstDkzrjj> selectByPtaiSernoListdj(@Param("ptaiSerno") String ptaiSerno);

    /**
     * @方法名称: selectByPtaiSerno
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PlaTakeoverBillRel> selectByPtaiSernoLists(@Param("ptaiSerno") String ptaiSerno);
}