/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.PlaTakeoverBillRelHis;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaTakeoverBillRel;
import cn.com.yusys.yusp.service.PlaTakeoverBillRelService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaTakeoverBillRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 19:54:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/platakeoverbillrel")
public class PlaTakeoverBillRelResource {
    @Autowired
    private PlaTakeoverBillRelService plaTakeoverBillRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaTakeoverBillRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaTakeoverBillRel> list = plaTakeoverBillRelService.selectAll(queryModel);
        return new ResultDto<List<PlaTakeoverBillRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaTakeoverBillRel>> index(QueryModel queryModel) {
        List<PlaTakeoverBillRel> list = plaTakeoverBillRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaTakeoverBillRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{ptbrSerno}")
    protected ResultDto<PlaTakeoverBillRel> show(@PathVariable("ptbrSerno") String ptbrSerno) {
        PlaTakeoverBillRel plaTakeoverBillRel = plaTakeoverBillRelService.selectByPrimaryKey(ptbrSerno);
        return new ResultDto<PlaTakeoverBillRel>(plaTakeoverBillRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaTakeoverBillRel> create(@RequestBody PlaTakeoverBillRel plaTakeoverBillRel) throws URISyntaxException {
        plaTakeoverBillRelService.insert(plaTakeoverBillRel);
        return new ResultDto<PlaTakeoverBillRel>(plaTakeoverBillRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaTakeoverBillRel plaTakeoverBillRel) throws URISyntaxException {
        int result = plaTakeoverBillRelService.update(plaTakeoverBillRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody PlaTakeoverBillRel plaTakeoverBillRel) {
        int result = plaTakeoverBillRelService.delete(plaTakeoverBillRel);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaTakeoverBillRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/saveContNo")
    protected ResultDto<Integer> saveBatchContNo(@RequestBody List<PlaTakeoverBillRel> lawBillRels) {
        Integer list = plaTakeoverBillRelService.saveBatchContNo(lawBillRels);
        return new ResultDto< Integer>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryAll")
    protected ResultDto<List<PlaTakeoverBillRel>> queryAll(@RequestBody QueryModel queryModel) {
        List<PlaTakeoverBillRel> list = plaTakeoverBillRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaTakeoverBillRel>>(list);
    }


    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/showByPtbrSerno")
    protected ResultDto<PlaTakeoverBillRel> showByPtbrSerno(@RequestBody String ptbrSerno) {
        PlaTakeoverBillRel plaTakeoverBillRel = plaTakeoverBillRelService.selectByPrimaryKey(ptbrSerno);
        return new ResultDto<PlaTakeoverBillRel>(plaTakeoverBillRel);
    }
    /**
     * 债权转让发送变更代理
     *
     * @author 周茂伟
     **/
    @ApiOperation("债权转让发送变更代理")
    @PostMapping("/sendtohxbgdl")
    protected ResultDto<String> sendToHXBGDL(@RequestBody List<PlaTakeoverBillRel> plaTakeoverBillRelList) {
        return plaTakeoverBillRelService.sendToHXBGDL(plaTakeoverBillRelList);
    }

    /**
     * 批量修改借据信息
     * @param lawBillRels
     * @return
     */
    @PostMapping("/updateBatchContNo")
    protected ResultDto<Integer> updateBatchContNo(@RequestBody List<PlaTakeoverBillRel> lawBillRels) {
        Integer list = plaTakeoverBillRelService.updateBatchContNo(lawBillRels);
        return new ResultDto< Integer>(list);
    }
}
