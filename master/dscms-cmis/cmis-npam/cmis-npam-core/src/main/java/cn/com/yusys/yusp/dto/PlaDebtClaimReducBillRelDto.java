package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class PlaDebtClaimReducBillRelDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 借据编号 **/
    private String billNo;

    /** 应收应记利息 **/
    private java.math.BigDecimal recRemInt;

    /** 催收应计利息 **/
    private java.math.BigDecimal bcmRemInt;

    /** 应收欠息 **/
    private java.math.BigDecimal recDebitInt;

    /** 催收欠息 **/
    private java.math.BigDecimal bcmDebitInt;

    /** 应收应计罚息 **/
    private java.math.BigDecimal recRemPenalInt;

    /** 催收应计罚息 **/
    private java.math.BigDecimal bcmRemPenalInt;

    /** 应收罚息 **/
    private java.math.BigDecimal recPenalInt;

    /** 催收罚息 **/
    private java.math.BigDecimal bcmPenalInt;

    /** 应计复息 **/
    private java.math.BigDecimal recCompoundInt;

    /** 复息 **/
    private java.math.BigDecimal compoundInt;

    public String getBillNo(String dkjiejuh) {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public BigDecimal getRecRemInt() {
        return recRemInt;
    }

    public void setRecRemInt(BigDecimal recRemInt) {
        this.recRemInt = recRemInt;
    }

    public BigDecimal getBcmRemInt() {
        return bcmRemInt;
    }

    public void setBcmRemInt(BigDecimal bcmRemInt) {
        this.bcmRemInt = bcmRemInt;
    }

    public BigDecimal getRecDebitInt() {
        return recDebitInt;
    }

    public void setRecDebitInt(BigDecimal recDebitInt) {
        this.recDebitInt = recDebitInt;
    }

    public BigDecimal getBcmDebitInt() {
        return bcmDebitInt;
    }

    public void setBcmDebitInt(BigDecimal bcmDebitInt) {
        this.bcmDebitInt = bcmDebitInt;
    }

    public BigDecimal getRecRemPenalInt() {
        return recRemPenalInt;
    }

    public void setRecRemPenalInt(BigDecimal recRemPenalInt) {
        this.recRemPenalInt = recRemPenalInt;
    }

    public BigDecimal getBcmRemPenalInt() {
        return bcmRemPenalInt;
    }

    public void setBcmRemPenalInt(BigDecimal bcmRemPenalInt) {
        this.bcmRemPenalInt = bcmRemPenalInt;
    }

    public BigDecimal getRecPenalInt() {
        return recPenalInt;
    }

    public void setRecPenalInt(BigDecimal recPenalInt) {
        this.recPenalInt = recPenalInt;
    }

    public BigDecimal getBcmPenalInt() {
        return bcmPenalInt;
    }

    public void setBcmPenalInt(BigDecimal bcmPenalInt) {
        this.bcmPenalInt = bcmPenalInt;
    }

    public BigDecimal getRecCompoundInt() {
        return recCompoundInt;
    }

    public void setRecCompoundInt(BigDecimal recCompoundInt) {
        this.recCompoundInt = recCompoundInt;
    }

    public BigDecimal getCompoundInt() {
        return compoundInt;
    }

    public void setCompoundInt(BigDecimal compoundInt) {
        this.compoundInt = compoundInt;
    }
}
