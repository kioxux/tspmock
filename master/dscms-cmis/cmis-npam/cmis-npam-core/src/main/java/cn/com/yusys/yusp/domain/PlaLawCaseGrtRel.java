/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawCaseGrtRel
 * @类描述: pla_law_case_grt_rel数据实体类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-05 17:45:58
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_law_case_grt_rel")
public class PlaLawCaseGrtRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 诉讼关联担保流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PLCGR_SERNO")
    private String plcgrSerno;

    /**
     * 诉讼申请流水号
     **/
    @Column(name = "CASE_SERNO", unique = false, nullable = true, length = 40)
    private String caseSerno;

    /**
     * 担保合同编号
     **/
    @Column(name = "GUAR_CONT_NO", unique = false, nullable = true, length = 40)
    private String guarContNo;

    /**
     * 担保合同类型
     **/
    @Column(name = "GUAR_CONT_TYPE", unique = false, nullable = true, length = 5)
    private String guarContType;

    /**
     * 担保方式
     **/
    @Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
    private String guarMode;

    /**
     * 担保合同金额
     **/
    @Column(name = "GUAR_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal guarAmt;

    /**
     * 担保起始日
     **/
    @Column(name = "GUAR_START_DATE", unique = false, nullable = true, length = 20)
    private String guarStartDate;

    /**
     * 担保到期日
     **/
    @Column(name = "GUAR_END_DATE", unique = false, nullable = true, length = 20)
    private String guarEndDate;

    /**
     * 担保状态
     **/
    @Column(name = "GUAR_CONT_STATE", unique = false, nullable = true, length = 5)
    private String guarContState;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    /**
     * 合同编号
     **/
    @Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
    private String contNo;

    public PlaLawCaseGrtRel() {
        // default implementation ignored
    }

    /**
     * @param plcgrSerno
     */
    public void setPlcgrSerno(String plcgrSerno) {
        this.plcgrSerno = plcgrSerno;
    }

    /**
     * @return plcgrSerno
     */
    public String getPlcgrSerno() {
        return this.plcgrSerno;
    }

    /**
     * @param caseSerno
     */
    public void setCaseSerno(String caseSerno) {
        this.caseSerno = caseSerno;
    }

    /**
     * @return caseSerno
     */
    public String getCaseSerno() {
        return this.caseSerno;
    }

    /**
     * @param guarContNo
     */
    public void setGuarContNo(String guarContNo) {
        this.guarContNo = guarContNo;
    }

    /**
     * @return guarContNo
     */
    public String getGuarContNo() {
        return this.guarContNo;
    }

    /**
     * @param guarContType
     */
    public void setGuarContType(String guarContType) {
        this.guarContType = guarContType;
    }

    /**
     * @return guarContType
     */
    public String getGuarContType() {
        return this.guarContType;
    }

    /**
     * @param guarMode
     */
    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    /**
     * @return guarMode
     */
    public String getGuarMode() {
        return this.guarMode;
    }

    /**
     * @param guarAmt
     */
    public void setGuarAmt(java.math.BigDecimal guarAmt) {
        this.guarAmt = guarAmt;
    }

    /**
     * @return guarAmt
     */
    public java.math.BigDecimal getGuarAmt() {
        return this.guarAmt;
    }

    /**
     * @param guarStartDate
     */
    public void setGuarStartDate(String guarStartDate) {
        this.guarStartDate = guarStartDate;
    }

    /**
     * @return guarStartDate
     */
    public String getGuarStartDate() {
        return this.guarStartDate;
    }

    /**
     * @param guarEndDate
     */
    public void setGuarEndDate(String guarEndDate) {
        this.guarEndDate = guarEndDate;
    }

    /**
     * @return guarEndDate
     */
    public String getGuarEndDate() {
        return this.guarEndDate;
    }

    /**
     * @param guarContState
     */
    public void setGuarContState(String guarContState) {
        this.guarContState = guarContState;
    }

    /**
     * @return guarContState
     */
    public String getGuarContState() {
        return this.guarContState;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }
}