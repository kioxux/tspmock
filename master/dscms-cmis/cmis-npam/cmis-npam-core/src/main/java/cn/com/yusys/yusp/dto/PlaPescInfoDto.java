package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaPescInfo
 * @类描述: pla_pesc_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:52:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PlaPescInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 时效规则编号 **/
	private String ppiSerno;
	
	/** 条目名称 **/
	private String entryName;
	
	/** 时效值 **/
	private String prescValue;
	
	/** 时效单位 **/
	private String prescUnit;
	
	/** 是否启用 **/
	private String isBegin;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param ppiSerno
	 */
	public void setPpiSerno(String ppiSerno) {
		this.ppiSerno = ppiSerno == null ? null : ppiSerno.trim();
	}
	
    /**
     * @return PpiSerno
     */	
	public String getPpiSerno() {
		return this.ppiSerno;
	}
	
	/**
	 * @param entryName
	 */
	public void setEntryName(String entryName) {
		this.entryName = entryName == null ? null : entryName.trim();
	}
	
    /**
     * @return EntryName
     */	
	public String getEntryName() {
		return this.entryName;
	}
	
	/**
	 * @param prescValue
	 */
	public void setPrescValue(String prescValue) {
		this.prescValue = prescValue == null ? null : prescValue.trim();
	}
	
    /**
     * @return PrescValue
     */	
	public String getPrescValue() {
		return this.prescValue;
	}
	
	/**
	 * @param prescUnit
	 */
	public void setPrescUnit(String prescUnit) {
		this.prescUnit = prescUnit == null ? null : prescUnit.trim();
	}
	
    /**
     * @return PrescUnit
     */	
	public String getPrescUnit() {
		return this.prescUnit;
	}
	
	/**
	 * @param isBegin
	 */
	public void setIsBegin(String isBegin) {
		this.isBegin = isBegin == null ? null : isBegin.trim();
	}
	
    /**
     * @return IsBegin
     */	
	public String getIsBegin() {
		return this.isBegin;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}