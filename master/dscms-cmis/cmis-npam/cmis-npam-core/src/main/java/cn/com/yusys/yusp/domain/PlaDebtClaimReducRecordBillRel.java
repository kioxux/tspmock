/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaDebtClaimReducRecordBillRel
 * @类描述: pla_debt_claim_reduc_record_bill_rel数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-24 10:08:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_debt_claim_reduc_record_bill_rel")
public class PlaDebtClaimReducRecordBillRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 关联表流水号
     **/
    @Id
    @Column(name = "PDCRRBR_SERNO")
    private String pdcrrbrSerno;

    /**
     * 减免关联流水号
     **/
    @Id
    @Column(name = "PDCRRAI_SERNO")
    private String pdcrraiSerno;

    /**
     * 合同编号
     **/
    @Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
    private String contNo;

    /**
     * 借据编号
     **/
    @Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
    private String billNo;

    /**
     * 客户编号
     **/
    @Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
    private String cusId;

    /**
     * 客户名称
     **/
    @Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
    private String cusName;

    /**
     * 产品名称
     **/
    @Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
    private String prdName;

    /**
     * 币种
     **/
    @Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
    private String curType;

    /**
     * 贷款金额
     **/
    @Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal loanAmt;

    /**
     * 贷款余额
     **/
    @Column(name = "LOAN_BALANCE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal loanBalance;

    /**
     * 拖欠利息总额
     **/
    @Column(name = "TOTAL_TQLX_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalTqlxAmt;

    /**
     * 贷款起始日
     **/
    @Column(name = "LOAN_START_DATE", unique = false, nullable = true, length = 20)
    private String loanStartDate;

    /**
     * 贷款到期日
     **/
    @Column(name = "LOAN_END_DATE", unique = false, nullable = true, length = 20)
    private String loanEndDate;

    /**
     * 执行年利率
     **/
    @Column(name = "EXEC_RATE_YEAR", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal execRateYear;

    /**
     * 五级分类
     **/
    @Column(name = "FIVE_CLASS", unique = false, nullable = true, length = 5)
    private String fiveClass;

    /**
     * 担保方式
     **/
    @Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
    private String guarMode;

    /**
     * 减免本金
     **/
    @Column(name = "REDUC_CAP_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal reducCapAmt;

    /**
     * 减免欠息
     **/
    @Column(name = "REDUC_DEBIT_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal reducDebitInt;

    /**
     * 减免罚息
     **/
    @Column(name = "REDUC_PENAL_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal reducPenalInt;

    /**
     * 减免复息
     **/
    @Column(name = "REDUC_COMPOUND_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal reducCompoundInt;

    /**
     * 减免费用
     **/
    @Column(name = "REDUC_COST_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal reducCostAmt;

    /**
     * 应收应记利息
     **/
    @Column(name = "REC_REM_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal recRemInt;

    /**
     * 催收应计利息
     **/
    @Column(name = "BCM_REM_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal bcmRemInt;

    /**
     * 应收欠息
     **/
    @Column(name = "REC_DEBIT_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal recDebitInt;

    /**
     * 催收欠息
     **/
    @Column(name = "BCM_DEBIT_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal bcmDebitInt;

    /**
     * 应收应计罚息
     **/
    @Column(name = "REC_REM_PENAL_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal recRemPenalInt;

    /**
     * 催收应计罚息
     **/
    @Column(name = "BCM_REM_PENAL_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal bcmRemPenalInt;

    /**
     * 应收罚息
     **/
    @Column(name = "REC_PENAL_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal recPenalInt;

    /**
     * 催收罚息
     **/
    @Column(name = "BCM_PENAL_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal bcmPenalInt;

    /**
     * 应计复息
     **/
    @Column(name = "REC_COMPOUND_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal recCompoundInt;

    /**
     * 复息
     **/
    @Column(name = "COMPOUND_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal compoundInt;

    /**
     * 记账状态
     **/
    @Column(name = "RECORD_STATUS", unique = false, nullable = true, length = 5)
    private String recordStatus;

    /**
     * 记账日期
     **/
    @Column(name = "RECORD_DATE", unique = false, nullable = true, length = 10)
    private String recordDate;

    /**
     * 记账交易流水号
     **/
    @Column(name = "RECORD_TRAN_SERNO", unique = false, nullable = true, length = 40)
    private String recordTranSerno;

    /**
     * 记账交易时间
     **/
    @Column(name = "RECORD_TRAN_TIME", unique = false, nullable = true, length = 19)
    private String recordTranTime;

    /**
     * 记账冲正交易流水号
     **/
    @Column(name = "RECORD_BACK_SERNO", unique = false, nullable = true, length = 40)
    private String recordBackSerno;

    /**
     * 记账冲正交易时间
     **/
    @Column(name = "RECORD_BACK_TIME", unique = false, nullable = true, length = 19)
    private String recordBackTime;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaDebtClaimReducRecordBillRel() {
        // default implementation ignored
    }

    public BigDecimal getRecRemInt() {
        return recRemInt;
    }

    public void setRecRemInt(BigDecimal recRemInt) {
        this.recRemInt = recRemInt;
    }

    public BigDecimal getBcmRemInt() {
        return bcmRemInt;
    }

    public void setBcmRemInt(BigDecimal bcmRemInt) {
        this.bcmRemInt = bcmRemInt;
    }

    public BigDecimal getRecDebitInt() {
        return recDebitInt;
    }

    public void setRecDebitInt(BigDecimal recDebitInt) {
        this.recDebitInt = recDebitInt;
    }

    public BigDecimal getBcmDebitInt() {
        return bcmDebitInt;
    }

    public void setBcmDebitInt(BigDecimal bcmDebitInt) {
        this.bcmDebitInt = bcmDebitInt;
    }

    public BigDecimal getRecRemPenalInt() {
        return recRemPenalInt;
    }

    public void setRecRemPenalInt(BigDecimal recRemPenalInt) {
        this.recRemPenalInt = recRemPenalInt;
    }

    public BigDecimal getBcmRemPenalInt() {
        return bcmRemPenalInt;
    }

    public void setBcmRemPenalInt(BigDecimal bcmRemPenalInt) {
        this.bcmRemPenalInt = bcmRemPenalInt;
    }

    public BigDecimal getRecPenalInt() {
        return recPenalInt;
    }

    public void setRecPenalInt(BigDecimal recPenalInt) {
        this.recPenalInt = recPenalInt;
    }

    public BigDecimal getBcmPenalInt() {
        return bcmPenalInt;
    }

    public void setBcmPenalInt(BigDecimal bcmPenalInt) {
        this.bcmPenalInt = bcmPenalInt;
    }

    public BigDecimal getRecCompoundInt() {
        return recCompoundInt;
    }

    public void setRecCompoundInt(BigDecimal recCompoundInt) {
        this.recCompoundInt = recCompoundInt;
    }

    public BigDecimal getCompoundInt() {
        return compoundInt;
    }

    public void setCompoundInt(BigDecimal compoundInt) {
        this.compoundInt = compoundInt;
    }

    /**
     * @param pdcrrbrSerno
     */
    public void setPdcrrbrSerno(String pdcrrbrSerno) {
        this.pdcrrbrSerno = pdcrrbrSerno;
    }

    /**
     * @return pdcrrbrSerno
     */
    public String getPdcrrbrSerno() {
        return this.pdcrrbrSerno;
    }

    /**
     * @param pdcraiSerno
     */
    public void setPdcrraiSerno(String pdcraiSerno) {
        this.pdcrraiSerno = pdcraiSerno;
    }

    /**
     * @return pdcraiSerno
     */
    public String getPdcrraiSerno() {
        return this.pdcrraiSerno;
    }

    /**
     * @param contNo
     */
    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    /**
     * @return contNo
     */
    public String getContNo() {
        return this.contNo;
    }

    /**
     * @param billNo
     */
    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    /**
     * @return billNo
     */
    public String getBillNo() {
        return this.billNo;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param prdName
     */
    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    /**
     * @return prdName
     */
    public String getPrdName() {
        return this.prdName;
    }

    /**
     * @param curType
     */
    public void setCurType(String curType) {
        this.curType = curType;
    }

    /**
     * @return curType
     */
    public String getCurType() {
        return this.curType;
    }

    /**
     * @param loanAmt
     */
    public void setLoanAmt(java.math.BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    /**
     * @return loanAmt
     */
    public java.math.BigDecimal getLoanAmt() {
        return this.loanAmt;
    }

    /**
     * @param loanBalance
     */
    public void setLoanBalance(java.math.BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    /**
     * @return loanBalance
     */
    public java.math.BigDecimal getLoanBalance() {
        return this.loanBalance;
    }

    /**
     * @param totalTqlxAmt
     */
    public void setTotalTqlxAmt(java.math.BigDecimal totalTqlxAmt) {
        this.totalTqlxAmt = totalTqlxAmt;
    }

    /**
     * @return totalTqlxAmt
     */
    public java.math.BigDecimal getTotalTqlxAmt() {
        return this.totalTqlxAmt;
    }

    /**
     * @param loanStartDate
     */
    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    /**
     * @return loanStartDate
     */
    public String getLoanStartDate() {
        return this.loanStartDate;
    }

    /**
     * @param loanEndDate
     */
    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    /**
     * @return loanEndDate
     */
    public String getLoanEndDate() {
        return this.loanEndDate;
    }

    /**
     * @param execRateYear
     */
    public void setExecRateYear(java.math.BigDecimal execRateYear) {
        this.execRateYear = execRateYear;
    }

    /**
     * @return execRateYear
     */
    public java.math.BigDecimal getExecRateYear() {
        return this.execRateYear;
    }

    /**
     * @param fiveClass
     */
    public void setFiveClass(String fiveClass) {
        this.fiveClass = fiveClass;
    }

    /**
     * @return fiveClass
     */
    public String getFiveClass() {
        return this.fiveClass;
    }

    /**
     * @param guarMode
     */
    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    /**
     * @return guarMode
     */
    public String getGuarMode() {
        return this.guarMode;
    }

    /**
     * @param reducCapAmt
     */
    public void setReducCapAmt(java.math.BigDecimal reducCapAmt) {
        this.reducCapAmt = reducCapAmt;
    }

    /**
     * @return reducCapAmt
     */
    public java.math.BigDecimal getReducCapAmt() {
        return this.reducCapAmt;
    }

    /**
     * @param reducDebitInt
     */
    public void setReducDebitInt(java.math.BigDecimal reducDebitInt) {
        this.reducDebitInt = reducDebitInt;
    }

    /**
     * @return reducDebitInt
     */
    public java.math.BigDecimal getReducDebitInt() {
        return this.reducDebitInt;
    }

    /**
     * @param reducPenalInt
     */
    public void setReducPenalInt(java.math.BigDecimal reducPenalInt) {
        this.reducPenalInt = reducPenalInt;
    }

    /**
     * @return reducPenalInt
     */
    public java.math.BigDecimal getReducPenalInt() {
        return this.reducPenalInt;
    }

    /**
     * @param reducCompoundInt
     */
    public void setReducCompoundInt(java.math.BigDecimal reducCompoundInt) {
        this.reducCompoundInt = reducCompoundInt;
    }

    /**
     * @return reducCompoundInt
     */
    public java.math.BigDecimal getReducCompoundInt() {
        return this.reducCompoundInt;
    }

    /**
     * @param reducCostAmt
     */
    public void setReducCostAmt(java.math.BigDecimal reducCostAmt) {
        this.reducCostAmt = reducCostAmt;
    }

    /**
     * @return reducCostAmt
     */
    public java.math.BigDecimal getReducCostAmt() {
        return this.reducCostAmt;
    }

    /**
     * @param recordStatus
     */
    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    /**
     * @return recordStatus
     */
    public String getRecordStatus() {
        return this.recordStatus;
    }

    /**
     * @param recordDate
     */
    public void setRecordDate(String recordDate) {
        this.recordDate = recordDate;
    }

    /**
     * @return recordDate
     */
    public String getRecordDate() {
        return this.recordDate;
    }

    /**
     * @param recordTranSerno
     */
    public void setRecordTranSerno(String recordTranSerno) {
        this.recordTranSerno = recordTranSerno;
    }

    /**
     * @return recordTranSerno
     */
    public String getRecordTranSerno() {
        return this.recordTranSerno;
    }



    /**
     * @param recordBackSerno
     */
    public void setRecordBackSerno(String recordBackSerno) {
        this.recordBackSerno = recordBackSerno;
    }

    /**
     * @return recordBackSerno
     */
    public String getRecordBackSerno() {
        return this.recordBackSerno;
    }

    public String getRecordTranTime() {
        return recordTranTime;
    }

    public void setRecordTranTime(String recordTranTime) {
        this.recordTranTime = recordTranTime;
    }

    public String getRecordBackTime() {
        return recordBackTime;
    }

    public void setRecordBackTime(String recordBackTime) {
        this.recordBackTime = recordBackTime;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}