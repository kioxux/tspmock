/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBadDebtWriteoffRecordBillRel
 * @类描述: pla_bad_debt_writeoff_record_bill_rel数据实体类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-09 09:34:55
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_bad_debt_writeoff_record_bill_rel")
public class PlaBadDebtWriteoffRecordBillRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 借据关联流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PBDWRBR_SERNO")
    private String pbdwrbrSerno;

    /**
     * 记账申请流水号
     **/
    @Column(name = "PBDWRA_SERNO", unique = false, nullable = true, length = 40)
    private String pbdwraSerno;

    /**
     * 合同编号
     **/
    @Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
    private String contNo;

    /**
     * 借据编号
     **/
    @Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
    private String billNo;

    /**
     * 客户编号
     **/
    @Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
    private String cusId;

    /**
     * 客户名称
     **/
    @Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
    private String cusName;

    /**
     * 产品名称
     **/
    @Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
    private String prdName;

    /**
     * 币种
     **/
    @Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
    private String curType;

    /**
     * 贷款金额
     **/
    @Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal loanAmt;

    /**
     * 贷款余额
     **/
    @Column(name = "LOAN_BALANCE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal loanBalance;

    /**
     * 拖欠利息总额
     **/
    @Column(name = "TOTAL_TQLX_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalTqlxAmt;

    /**
     * 贷款起始日
     **/
    @Column(name = "LOAN_START_DATE", unique = false, nullable = true, length = 20)
    private String loanStartDate;

    /**
     * 贷款到期日
     **/
    @Column(name = "LOAN_END_DATE", unique = false, nullable = true, length = 20)
    private String loanEndDate;

    /**
     * 执行年利率
     **/
    @Column(name = "EXEC_RATE_YEAR", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal execRateYear;

    /**
     * 五级分类
     **/
    @Column(name = "FIVE_CLASS", unique = false, nullable = true, length = 5)
    private String fiveClass;

    /**
     * 记账状态
     **/
    @Column(name = "RECORD_STATUS", unique = false, nullable = true, length = 5)
    private String recordStatus;

    /**
     * 核销状态
     **/
    @Column(name = "WRITEOFF_STATUS", unique = false, nullable = true, length = 5)
    private String writeoffStatus;

    /**
     * 记账日期
     **/
    @Column(name = "RECORD_DATE", unique = false, nullable = true, length = 10)
    private String recordDate;

    /**
     * 核销日期
     **/
    @Column(name = "WRITEOFF_DATE", unique = false, nullable = true, length = 10)
    private String writeoffDate;

    /**
     * 核销标识
     **/
    @Column(name = "WRITEOFF_FLAG", unique = false, nullable = true, length = 5)
    private String writeoffFlag;

    /**
     * 责任人
     **/
    @Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
    private String managerId;

    /**
     * 责任机构
     **/
    @Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
    private String managerBrId;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    /**
     * 记账交易流水
     **/
    @Column(name = "RECORD_TRAN_SERNO", unique = false, nullable = true, length = 40)
    private String recordTranSerno;

    /**
     * 记账交易时间
     **/
    @Column(name = "RECORD_TRAN_TIME", unique = false, nullable = true, length = 20)
    private String recordTranTime;

    /**
     * 冲正交易流水
     **/
    @Column(name = "RECORD_BACK_SERNO", unique = false, nullable = true, length = 40)
    private String recordBackSerno;

    /**
     * 冲正交易时间
     **/
    @Column(name = "RECORD_BACK_TIME", unique = false, nullable = true, length = 20)
    private String recordBackTime;

    public PlaBadDebtWriteoffRecordBillRel() {
        // default implementation ignored
    }

    public String getRecordTranSerno() {
        return recordTranSerno;
    }

    public void setRecordTranSerno(String recordTranSerno) {
        this.recordTranSerno = recordTranSerno;
    }

    public String getRecordTranTime() {
        return recordTranTime;
    }

    public void setRecordTranTime(String recordTranTime) {
        this.recordTranTime = recordTranTime;
    }

    public String getRecordBackSerno() {
        return recordBackSerno;
    }

    public void setRecordBackSerno(String recordBackSerno) {
        this.recordBackSerno = recordBackSerno;
    }

    public String getRecordBackTime() {
        return recordBackTime;
    }

    public void setRecordBackTime(String recordBackTime) {
        this.recordBackTime = recordBackTime;
    }

    /**
     * @param pbdwrbrSerno
     */
    public void setPbdwrbrSerno(String pbdwrbrSerno) {
        this.pbdwrbrSerno = pbdwrbrSerno;
    }

    /**
     * @return pbdwrbrSerno
     */
    public String getPbdwrbrSerno() {
        return this.pbdwrbrSerno;
    }

    /**
     * @param pbdwraSerno
     */
    public void setPbdwraSerno(String pbdwraSerno) {
        this.pbdwraSerno = pbdwraSerno;
    }

    /**
     * @return pbdwraSerno
     */
    public String getPbdwraSerno() {
        return this.pbdwraSerno;
    }

    /**
     * @param contNo
     */
    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    /**
     * @return contNo
     */
    public String getContNo() {
        return this.contNo;
    }

    /**
     * @param billNo
     */
    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    /**
     * @return billNo
     */
    public String getBillNo() {
        return this.billNo;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param prdName
     */
    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    /**
     * @return prdName
     */
    public String getPrdName() {
        return this.prdName;
    }

    /**
     * @param curType
     */
    public void setCurType(String curType) {
        this.curType = curType;
    }

    /**
     * @return curType
     */
    public String getCurType() {
        return this.curType;
    }

    /**
     * @param loanAmt
     */
    public void setLoanAmt(java.math.BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    /**
     * @return loanAmt
     */
    public java.math.BigDecimal getLoanAmt() {
        return this.loanAmt;
    }

    /**
     * @param loanBalance
     */
    public void setLoanBalance(java.math.BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    /**
     * @return loanBalance
     */
    public java.math.BigDecimal getLoanBalance() {
        return this.loanBalance;
    }

    /**
     * @param totalTqlxAmt
     */
    public void setTotalTqlxAmt(java.math.BigDecimal totalTqlxAmt) {
        this.totalTqlxAmt = totalTqlxAmt;
    }

    /**
     * @return totalTqlxAmt
     */
    public java.math.BigDecimal getTotalTqlxAmt() {
        return this.totalTqlxAmt;
    }

    /**
     * @param loanStartDate
     */
    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    /**
     * @return loanStartDate
     */
    public String getLoanStartDate() {
        return this.loanStartDate;
    }

    /**
     * @param loanEndDate
     */
    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    /**
     * @return loanEndDate
     */
    public String getLoanEndDate() {
        return this.loanEndDate;
    }

    /**
     * @param execRateYear
     */
    public void setExecRateYear(java.math.BigDecimal execRateYear) {
        this.execRateYear = execRateYear;
    }

    /**
     * @return execRateYear
     */
    public java.math.BigDecimal getExecRateYear() {
        return this.execRateYear;
    }

    /**
     * @param fiveClass
     */
    public void setFiveClass(String fiveClass) {
        this.fiveClass = fiveClass;
    }

    /**
     * @return fiveClass
     */
    public String getFiveClass() {
        return this.fiveClass;
    }

    /**
     * @param recordStatus
     */
    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    /**
     * @return recordStatus
     */
    public String getRecordStatus() {
        return this.recordStatus;
    }

    /**
     * @param writeoffStatus
     */
    public void setWriteoffStatus(String writeoffStatus) {
        this.writeoffStatus = writeoffStatus;
    }

    /**
     * @return writeoffStatus
     */
    public String getWriteoffStatus() {
        return this.writeoffStatus;
    }

    /**
     * @param recordDate
     */
    public void setRecordDate(String recordDate) {
        this.recordDate = recordDate;
    }

    /**
     * @return recordDate
     */
    public String getRecordDate() {
        return this.recordDate;
    }

    /**
     * @param writeoffDate
     */
    public void setWriteoffDate(String writeoffDate) {
        this.writeoffDate = writeoffDate;
    }

    /**
     * @return writeoffDate
     */
    public String getWriteoffDate() {
        return this.writeoffDate;
    }

    /**
     * @param writeoffFlag
     */
    public void setWriteoffFlag(String writeoffFlag) {
        this.writeoffFlag = writeoffFlag;
    }

    /**
     * @return writeoffFlag
     */
    public String getWriteoffFlag() {
        return this.writeoffFlag;
    }

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    /**
     * @return managerId
     */
    public String getManagerId() {
        return this.managerId;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    /**
     * @return managerBrId
     */
    public String getManagerBrId() {
        return this.managerBrId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}