package cn.com.yusys.yusp.dto;

import javax.persistence.Column;

public class PlaTakeoverAppInfoDto {
    /**
     * 借款人客户编号
     **/
    private String cusId;

    /**
     * 借款人客户名称
     **/
    private String cusName;


    private String serno;


    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }


    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }
}
