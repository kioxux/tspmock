/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.PlaExpenseRegiInfo;
import cn.com.yusys.yusp.dto.PlaExpenseRegiInfoDto;
import cn.com.yusys.yusp.service.PlaExpenseRegiInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaExpenseRegiInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 15:20:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plaexpenseregiinfo")
public class PlaExpenseRegiInfoResource {
    @Autowired
    private PlaExpenseRegiInfoService plaExpenseRegiInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaExpenseRegiInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaExpenseRegiInfo> list = plaExpenseRegiInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaExpenseRegiInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaExpenseRegiInfo>> index(QueryModel queryModel) {
        List<PlaExpenseRegiInfo> list = plaExpenseRegiInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaExpenseRegiInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{periSerno}")
    protected ResultDto<PlaExpenseRegiInfo> show(@PathVariable("periSerno") String periSerno) {
        PlaExpenseRegiInfo plaExpenseRegiInfo = plaExpenseRegiInfoService.selectByPrimaryKey(periSerno);
        return new ResultDto<PlaExpenseRegiInfo>(plaExpenseRegiInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaExpenseRegiInfo> create(@RequestBody PlaExpenseRegiInfo plaExpenseRegiInfo) throws URISyntaxException {
        plaExpenseRegiInfoService.insert(plaExpenseRegiInfo);
        return new ResultDto<PlaExpenseRegiInfo>(plaExpenseRegiInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaExpenseRegiInfoDto plaExpenseRegiInfoDto) throws URISyntaxException {
        int result = plaExpenseRegiInfoService.update(plaExpenseRegiInfoDto);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{periSerno}")
    protected ResultDto<Integer> delete(@PathVariable("periSerno") String periSerno) {
        int result = plaExpenseRegiInfoService.deleteByPrimaryKey(periSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaExpenseRegiInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:insertReaplyReg
     * @函数描述:费用登记菜单保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertReaplyReg")
    protected ResultDto<PlaExpenseRegiInfoDto> insertReaplyReg(@RequestBody PlaExpenseRegiInfoDto plaExpenseRegiInfoDto) throws URISyntaxException {
        plaExpenseRegiInfoService.savePlaExpenseRegiInfo(plaExpenseRegiInfoDto);
        return new ResultDto<PlaExpenseRegiInfoDto>(plaExpenseRegiInfoDto);
    }

    /**
     * 按条件查询
     *
     * @return
     */
    @PostMapping("/selectByCondition")
    protected ResultDto<List<PlaExpenseRegiInfo>> selectByCondition(@RequestBody  QueryModel queryModel) {
//        if (StringUtils.isBlank(queryModel.getSort())){
//            queryModel.setSort(" serno desc");
//        }
        return new ResultDto<List<PlaExpenseRegiInfo>>(plaExpenseRegiInfoService.selectByCondition(queryModel));
    }



    /**
     * @函数名称:updateReplyReg
     * @函数描述:对象修改
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateReplyReg")
    protected ResultDto<Integer> updateReplyReg(@RequestBody PlaExpenseRegiInfoDto plaExpenseRegiInfoDto) throws URISyntaxException {
        int result = plaExpenseRegiInfoService.update(plaExpenseRegiInfoDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleteReplyReq
     * @函数描述:单个对象删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteReplyReq")
    protected ResultDto<Integer> deleteReplyReq(@PathVariable("periSerno") String periSerno) {
        int result = plaExpenseRegiInfoService.deleteByPrimaryKey(periSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:insertReg
     * @函数描述:费用管理新增向导点击下一步数据插入到费用登记表中
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertReg")
    protected ResultDto<PlaExpenseRegiInfo> insertReg(@RequestBody PlaExpenseRegiInfo plaExpenseRegiInfo) throws URISyntaxException {
        plaExpenseRegiInfoService.insertReg(plaExpenseRegiInfo);
        return new ResultDto<PlaExpenseRegiInfo>(plaExpenseRegiInfo);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "明细查询")
    @PostMapping("/queryPlaExpenseRegiInfo")
    protected ResultDto<PlaExpenseRegiInfo> queryPlaExpenseRegiInfo(@RequestBody String periSerno) {
        PlaExpenseRegiInfo plaExpenseRegiInfo = plaExpenseRegiInfoService.selectByPrimaryKey(periSerno);
        return new ResultDto<PlaExpenseRegiInfo>(plaExpenseRegiInfo);
    }
}
