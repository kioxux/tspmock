/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawRecoverInfo
 * @类描述: pla_law_recover_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-07 19:31:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class PlaLawRecoverInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 案件流水号
     **/
    private String caseSerno;

    /**
     * 借据编号
     **/
    private String billNo;


    /**
     * 回收总金额
     **/
    private java.math.BigDecimal recoverTotalAmt;

    /**
     * 回收率
     **/
    private java.math.BigDecimal recoverPercent;

    /**
     * 回收金额
     **/
    private java.math.BigDecimal recoverAmt;

    /**
     * 币种
     **/
    private String curType;

    /**
     * 回收日期
     **/
    private String recoverDate;

    /**
     * 回收方式
     **/
    private String recoverMode;

    // 还款记录集合
    List<PlaLawRecoverInfoDto> plaLawRecoverInfoDtoList;
    /**
     * @param caseSerno
     */
    public void setCaseSerno(String caseSerno) {
        this.caseSerno = caseSerno;
    }

    /**
     * @return caseSerno
     */
    public String getCaseSerno() {
        return this.caseSerno;
    }

    /**
     * @param recoverTotalAmt
     */
    public void setRecoverTotalAmt(java.math.BigDecimal recoverTotalAmt) {
        this.recoverTotalAmt = recoverTotalAmt;
    }

    /**
     * @return recoverTotalAmt
     */
    public java.math.BigDecimal getRecoverTotalAmt() {
        return this.recoverTotalAmt;
    }

    /**
     * @param recoverPercent
     */
    public void setRecoverPercent(java.math.BigDecimal recoverPercent) {
        this.recoverPercent = recoverPercent;
    }

    /**
     * @return recoverPercent
     */
    public java.math.BigDecimal getRecoverPercent() {
        return this.recoverPercent;
    }

    /**
     * @param recoverAmt
     */
    public void setRecoverAmt(java.math.BigDecimal recoverAmt) {
        this.recoverAmt = recoverAmt;
    }

    /**
     * @return recoverAmt
     */
    public java.math.BigDecimal getRecoverAmt() {
        return this.recoverAmt;
    }

    /**
     * @param curType
     */
    public void setCurType(String curType) {
        this.curType = curType;
    }

    /**
     * @return curType
     */
    public String getCurType() {
        return this.curType;
    }

    /**
     * @param recoverDate
     */
    public void setRecoverDate(String recoverDate) {
        this.recoverDate = recoverDate;
    }

    /**
     * @return recoverDate
     */
    public String getRecoverDate() {
        return this.recoverDate;
    }

    /**
     * @param recoverMode
     */
    public void setRecoverMode(String recoverMode) {
        this.recoverMode = recoverMode;
    }

    /**
     * @return recoverMode
     */
    public String getRecoverMode() {
        return this.recoverMode;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public List<PlaLawRecoverInfoDto> getPlaLawRecoverInfoDtoList() {
        return plaLawRecoverInfoDtoList;
    }

    public void setPlaLawRecoverInfoDtoList(List<PlaLawRecoverInfoDto> plaLawRecoverInfoDtoList) {
        this.plaLawRecoverInfoDtoList = plaLawRecoverInfoDtoList;
    }
}