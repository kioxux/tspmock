/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.dto.client.http.image.imageDataSize.ImageDataReqDto;
import cn.com.yusys.yusp.service.Dscms2ImageClientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaBcmReceiptInfo;
import cn.com.yusys.yusp.service.PlaBcmReceiptInfoService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBcmReceiptInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-04 11:28:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "催收回执信息表")
@RestController
@RequestMapping("/api/plabcmreceiptinfo")
public class PlaBcmReceiptInfoResource {
    @Autowired
    private PlaBcmReceiptInfoService plaBcmReceiptInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaBcmReceiptInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaBcmReceiptInfo> list = plaBcmReceiptInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaBcmReceiptInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaBcmReceiptInfo>> index(QueryModel queryModel) {
        List<PlaBcmReceiptInfo> list = plaBcmReceiptInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaBcmReceiptInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("催收回执信息分页查询")
    @PostMapping("/queryAll")
    protected ResultDto<List<PlaBcmReceiptInfo>> queryAll(@RequestBody QueryModel queryModel) {
        List<PlaBcmReceiptInfo> list = plaBcmReceiptInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaBcmReceiptInfo>>(list);
    }


    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pbreiSerno}")
    protected ResultDto<PlaBcmReceiptInfo> show(@PathVariable("pbreiSerno") String pbreiSerno) {
        PlaBcmReceiptInfo plaBcmReceiptInfo = plaBcmReceiptInfoService.selectByPrimaryKey(pbreiSerno);
        return new ResultDto<PlaBcmReceiptInfo>(plaBcmReceiptInfo);
    }

    /**
     * @函数名称:show
     * @函数描述:  根据客户编号查询单个对象
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据客户编号查询单个对象")
    @PostMapping("/queryByTaskNo/{pbreiSerno}")
    protected ResultDto<PlaBcmReceiptInfo> queryByTaskNo(@PathVariable("taskNo") String taskNo) {
        PlaBcmReceiptInfo plaBcmReceiptInfo = plaBcmReceiptInfoService.selectByTaskNo(taskNo);
        return new ResultDto<PlaBcmReceiptInfo>(plaBcmReceiptInfo);
    }

    /**
     * @函数名称:show
     * @函数描述:   根据催收回执流水号查询单个对象，回显数据
     * @参数与返回说明:
     * @算法描述:
     * 刘权
     */
    @ApiOperation("根据催收回执流水号查询催收回执信息")
    @PostMapping("/showByPbreiSerno")
    protected ResultDto<PlaBcmReceiptInfo> showByPbreiSerno(@RequestBody PlaBcmReceiptInfo plaBcmReceiptInfo) {
        return new ResultDto<PlaBcmReceiptInfo>(plaBcmReceiptInfoService.selectByPrimaryKey(plaBcmReceiptInfo.getPbreiSerno()));
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaBcmReceiptInfo> create(@RequestBody PlaBcmReceiptInfo plaBcmReceiptInfo) throws URISyntaxException {
        plaBcmReceiptInfoService.insert(plaBcmReceiptInfo);
        return new ResultDto<PlaBcmReceiptInfo>(plaBcmReceiptInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaBcmReceiptInfo plaBcmReceiptInfo) throws URISyntaxException {
        int result = plaBcmReceiptInfoService.update(plaBcmReceiptInfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:  催收回执信息修改
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("催收回执信息修改")
    @PostMapping("/updateByPbreiSerno")
    protected ResultDto<Integer> updateByPbreiSerno(@RequestBody PlaBcmReceiptInfo plaBcmReceiptInfo) throws URISyntaxException {
        int result = plaBcmReceiptInfoService.updateByPbreiSerno(plaBcmReceiptInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("催收回执信息删除")
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody PlaBcmReceiptInfo plaBcmReceiptInfo) {
        return new ResultDto<Integer>(plaBcmReceiptInfoService.deleteByPrimaryKey(plaBcmReceiptInfo.getPbreiSerno()));
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaBcmReceiptInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:create
     * @函数描述: 催收结果登记新增保存
     * @参数与返回说明:
     * @算法描述:
     * 刘权
     */
    @ApiOperation("催收回执信息新增")
    @PostMapping("/save")
    protected ResultDto<PlaBcmReceiptInfo> save(@RequestBody PlaBcmReceiptInfo plaBcmReceiptInfo) throws URISyntaxException {
        plaBcmReceiptInfoService.save(plaBcmReceiptInfo);
        return new ResultDto<PlaBcmReceiptInfo>(plaBcmReceiptInfo);
    }

    @ApiOperation("催收回执信息影像拦截")
    @PostMapping("/saveByImageImageDataSize")
    protected ResultDto<Map<String, Integer>> saveByImageImageDataSize(@RequestBody ImageDataReqDto imageDataReqDto) throws URISyntaxException {
        ResultDto<Map<String, Integer>> mapResultDto = plaBcmReceiptInfoService.saveByImageImageDataSize(imageDataReqDto);
        return mapResultDto;
    }
}
