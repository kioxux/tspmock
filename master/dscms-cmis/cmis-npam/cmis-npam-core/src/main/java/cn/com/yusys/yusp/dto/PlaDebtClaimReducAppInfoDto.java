package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.PlaDebtClaimReducBillRel;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaDebtClaimReducAppInfo
 * @类描述: pla_debt_claim_reduc_app_info数据实体类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-06-18 11:29:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PlaDebtClaimReducAppInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 债权减免申请流水号 **/
	private String pdcraiSerno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 贷款总金额 **/
	private java.math.BigDecimal loanAmt;
	
	/** 贷款总余额 **/
	private java.math.BigDecimal loanBalance;
	
	/** 拖欠总利息 **/
	private java.math.BigDecimal totalTqlxAmt;
	
	/** 减免总额 **/
	private java.math.BigDecimal reducTotlAmt;
	
	/** 减免本金合计 **/
	private java.math.BigDecimal reducCapAmt;
	
	/** 减免欠息合计 **/
	private java.math.BigDecimal reducDebitInt;
	
	/** 减免罚息合计 **/
	private java.math.BigDecimal reducPenalInt;
	
	/** 减免复息合计 **/
	private java.math.BigDecimal reducCompoundInt;
	
	/** 减免费用合计 **/
	private java.math.BigDecimal reducCostAmt;
	
	/** 费用减免详情 **/
	private String reducCostDetail;
	
	/** 减免原因 **/
	private String reducResn;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 备注 **/
	private String memo;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	/** 费用明细登记表 **/
	private List<PlaDebtClaimReducBillRel> plaDebtClaimReducBillRel;
	
	/**
	 * @param pdcraiSerno
	 */
	public void setPdcraiSerno(String pdcraiSerno) {
		this.pdcraiSerno = pdcraiSerno == null ? null : pdcraiSerno.trim();
	}
	
    /**
     * @return PdcraiSerno
     */	
	public String getPdcraiSerno() {
		return this.pdcraiSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return LoanAmt
     */	
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(java.math.BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return LoanBalance
     */	
	public java.math.BigDecimal getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param totalTqlxAmt
	 */
	public void setTotalTqlxAmt(java.math.BigDecimal totalTqlxAmt) {
		this.totalTqlxAmt = totalTqlxAmt;
	}
	
    /**
     * @return TotalTqlxAmt
     */	
	public java.math.BigDecimal getTotalTqlxAmt() {
		return this.totalTqlxAmt;
	}
	
	/**
	 * @param reducTotlAmt
	 */
	public void setReducTotlAmt(java.math.BigDecimal reducTotlAmt) {
		this.reducTotlAmt = reducTotlAmt;
	}
	
    /**
     * @return ReducTotlAmt
     */	
	public java.math.BigDecimal getReducTotlAmt() {
		return this.reducTotlAmt;
	}
	
	/**
	 * @param reducCapAmt
	 */
	public void setReducCapAmt(java.math.BigDecimal reducCapAmt) {
		this.reducCapAmt = reducCapAmt;
	}
	
    /**
     * @return ReducCapAmt
     */	
	public java.math.BigDecimal getReducCapAmt() {
		return this.reducCapAmt;
	}
	
	/**
	 * @param reducDebitInt
	 */
	public void setReducDebitInt(java.math.BigDecimal reducDebitInt) {
		this.reducDebitInt = reducDebitInt;
	}
	
    /**
     * @return ReducDebitInt
     */	
	public java.math.BigDecimal getReducDebitInt() {
		return this.reducDebitInt;
	}
	
	/**
	 * @param reducPenalInt
	 */
	public void setReducPenalInt(java.math.BigDecimal reducPenalInt) {
		this.reducPenalInt = reducPenalInt;
	}
	
    /**
     * @return ReducPenalInt
     */	
	public java.math.BigDecimal getReducPenalInt() {
		return this.reducPenalInt;
	}
	
	/**
	 * @param reducCompoundInt
	 */
	public void setReducCompoundInt(java.math.BigDecimal reducCompoundInt) {
		this.reducCompoundInt = reducCompoundInt;
	}
	
    /**
     * @return ReducCompoundInt
     */	
	public java.math.BigDecimal getReducCompoundInt() {
		return this.reducCompoundInt;
	}
	
	/**
	 * @param reducCostAmt
	 */
	public void setReducCostAmt(java.math.BigDecimal reducCostAmt) {
		this.reducCostAmt = reducCostAmt;
	}
	
    /**
     * @return ReducCostAmt
     */	
	public java.math.BigDecimal getReducCostAmt() {
		return this.reducCostAmt;
	}
	
	/**
	 * @param reducCostDetail
	 */
	public void setReducCostDetail(String reducCostDetail) {
		this.reducCostDetail = reducCostDetail == null ? null : reducCostDetail.trim();
	}
	
    /**
     * @return ReducCostDetail
     */	
	public String getReducCostDetail() {
		return this.reducCostDetail;
	}
	
	/**
	 * @param reducResn
	 */
	public void setReducResn(String reducResn) {
		this.reducResn = reducResn == null ? null : reducResn.trim();
	}
	
    /**
     * @return ReducResn
     */	
	public String getReducResn() {
		return this.reducResn;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param memo
	 */
	public void setMemo(String memo) {
		this.memo = memo == null ? null : memo.trim();
	}
	
    /**
     * @return Memo
     */	
	public String getMemo() {
		return this.memo;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public void setPlaDebtClaimReducBillRel(List<PlaDebtClaimReducBillRel> plaDebtClaimReducBillRel) {
		this.plaDebtClaimReducBillRel = plaDebtClaimReducBillRel;
	}

	public List<PlaDebtClaimReducBillRel> getPlaDebtClaimReducBillRel() {
		return plaDebtClaimReducBillRel;
	}
}