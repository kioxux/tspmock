/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawPresAssetInfo
 * @类描述: pla_law_pres_asset_info数据实体类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-07 21:29:30
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_law_pres_asset_info")
public class PlaLawPresAssetInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 保全资产流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PLPAI_SERNO")
    private String plpaiSerno;

    /**
     * 案件流水号
     **/
    @Column(name = "CASE_SERNO", unique = false, nullable = true, length = 40)
    private String caseSerno;

    /**
     * 押品编号
     **/
    @Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
    private String guarNo;

    /**
     * 押品类型
     **/
    @Column(name = "GUAR_TYPE", unique = false, nullable = true, length = 5)
    private String guarType;

    /**
     * 权属人
     **/
    @Column(name = "AUTHO", unique = false, nullable = true, length = 80)
    private String autho;

    /**
     * 评估价值
     **/
    @Column(name = "EVAL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal evalAmt;

    /**
     * 保全起始日期
     **/
    @Column(name = "PRES_START_DATE", unique = false, nullable = true, length = 10)
    private String presStartDate;

    /**
     * 保全到期日期
     **/
    @Column(name = "PRES_END_DATE", unique = false, nullable = true, length = 10)
    private String presEndDate;

    /**
     * 我行保全情况
     **/
    @Column(name = "BANK_PRES_CASE", unique = false, nullable = true, length = 5)
    private String bankPresCase;

    /**
     * 其他保全情况
     **/
    @Column(name = "OTHER_PRES_CASE", unique = false, nullable = true, length = 2000)
    private String otherPresCase;

    /**
     * 资产分类
     **/
    @Column(name = "ASSET_TYPE", unique = false, nullable = true, length = 5)
    private String assetType;

    /**
     * 剩余价值
     **/
    @Column(name = "SURPLUS_VALUE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal surplusValue;

    /**
     * 是否抵（质)押等优先权
     **/
    @Column(name = "IS_PLD_PRI_WRR", unique = false, nullable = true, length = 5)
    private String isPldPriWrr;

    /**
     * 保全顺位
     **/
    @Column(name = "PRES_ORDER", unique = false, nullable = true, length = 20)
    private String presOrder;

    /**
     * 首保全法院/机关
     **/
    @Column(name = "FIRST_INSU_COURT", unique = false, nullable = true, length = 20)
    private String firstInsuCourt;

    /**
     * 保全标的（元)
     **/
    @Column(name = "PRES_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal presAmt;

    /**
     * 备注
     **/
    @Column(name = "MEMO", unique = false, nullable = true, length = 100)
    private String memo;

    /**
     * 是否抵质押以外保全资产
     **/
    @Column(name = "IS_OTHER_PRES_ASSET", unique = false, nullable = true, length = 5)
    private String isOtherPresAsset;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaLawPresAssetInfo() {
        // default implementation ignored
    }

    /**
     * @param plpaiSerno
     */
    public void setPlpaiSerno(String plpaiSerno) {
        this.plpaiSerno = plpaiSerno;
    }

    /**
     * @return plpaiSerno
     */
    public String getPlpaiSerno() {
        return this.plpaiSerno;
    }

    /**
     * @param caseSerno
     */
    public void setCaseSerno(String caseSerno) {
        this.caseSerno = caseSerno;
    }

    /**
     * @return caseSerno
     */
    public String getCaseSerno() {
        return this.caseSerno;
    }

    /**
     * @param guarNo
     */
    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    /**
     * @return guarNo
     */
    public String getGuarNo() {
        return this.guarNo;
    }

    /**
     * @param guarType
     */
    public void setGuarType(String guarType) {
        this.guarType = guarType;
    }

    /**
     * @return guarType
     */
    public String getGuarType() {
        return this.guarType;
    }

    /**
     * @param autho
     */
    public void setAutho(String autho) {
        this.autho = autho;
    }

    /**
     * @return autho
     */
    public String getAutho() {
        return this.autho;
    }

    /**
     * @param evalAmt
     */
    public void setEvalAmt(java.math.BigDecimal evalAmt) {
        this.evalAmt = evalAmt;
    }

    /**
     * @return evalAmt
     */
    public java.math.BigDecimal getEvalAmt() {
        return this.evalAmt;
    }

    /**
     * @param presStartDate
     */
    public void setPresStartDate(String presStartDate) {
        this.presStartDate = presStartDate;
    }

    /**
     * @return presStartDate
     */
    public String getPresStartDate() {
        return this.presStartDate;
    }

    /**
     * @param presEndDate
     */
    public void setPresEndDate(String presEndDate) {
        this.presEndDate = presEndDate;
    }

    /**
     * @return presEndDate
     */
    public String getPresEndDate() {
        return this.presEndDate;
    }

    /**
     * @param bankPresCase
     */
    public void setBankPresCase(String bankPresCase) {
        this.bankPresCase = bankPresCase;
    }

    /**
     * @return bankPresCase
     */
    public String getBankPresCase() {
        return this.bankPresCase;
    }

    /**
     * @param otherPresCase
     */
    public void setOtherPresCase(String otherPresCase) {
        this.otherPresCase = otherPresCase;
    }

    /**
     * @return otherPresCase
     */
    public String getOtherPresCase() {
        return this.otherPresCase;
    }

    /**
     * @param assetType
     */
    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    /**
     * @return assetType
     */
    public String getAssetType() {
        return this.assetType;
    }

    /**
     * @param surplusValue
     */
    public void setSurplusValue(java.math.BigDecimal surplusValue) {
        this.surplusValue = surplusValue;
    }

    /**
     * @return surplusValue
     */
    public java.math.BigDecimal getSurplusValue() {
        return this.surplusValue;
    }

    /**
     * @param isPldPriWrr
     */
    public void setIsPldPriWrr(String isPldPriWrr) {
        this.isPldPriWrr = isPldPriWrr;
    }

    /**
     * @return isPldPriWrr
     */
    public String getIsPldPriWrr() {
        return this.isPldPriWrr;
    }

    /**
     * @param presOrder
     */
    public void setPresOrder(String presOrder) {
        this.presOrder = presOrder;
    }

    /**
     * @return presOrder
     */
    public String getPresOrder() {
        return this.presOrder;
    }

    /**
     * @param firstInsuCourt
     */
    public void setFirstInsuCourt(String firstInsuCourt) {
        this.firstInsuCourt = firstInsuCourt;
    }

    /**
     * @return firstInsuCourt
     */
    public String getFirstInsuCourt() {
        return this.firstInsuCourt;
    }

    /**
     * @param presAmt
     */
    public void setPresAmt(java.math.BigDecimal presAmt) {
        this.presAmt = presAmt;
    }

    /**
     * @return presAmt
     */
    public java.math.BigDecimal getPresAmt() {
        return this.presAmt;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param isOtherPresAsset
     */
    public void setIsOtherPresAsset(String isOtherPresAsset) {
        this.isOtherPresAsset = isOtherPresAsset;
    }

    /**
     * @return isOtherPresAsset
     */
    public String getIsOtherPresAsset() {
        return this.isOtherPresAsset;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}