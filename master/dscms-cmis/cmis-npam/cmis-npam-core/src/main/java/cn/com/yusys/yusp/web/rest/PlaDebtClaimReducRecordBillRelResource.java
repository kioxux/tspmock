/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.PlaDebtClaimReducRecordBillRelDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaDebtClaimReducRecordBillRel;
import cn.com.yusys.yusp.service.PlaDebtClaimReducRecordBillRelService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaDebtClaimReducRecordBillRelResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-08 19:44:40
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pladebtclaimreducrecordbillrel")
public class PlaDebtClaimReducRecordBillRelResource {
    @Autowired
    private PlaDebtClaimReducRecordBillRelService plaDebtClaimReducRecordBillRelService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaDebtClaimReducRecordBillRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaDebtClaimReducRecordBillRel> list = plaDebtClaimReducRecordBillRelService.selectAll(queryModel);
        return new ResultDto<List<PlaDebtClaimReducRecordBillRel>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaDebtClaimReducRecordBillRel>> index(QueryModel queryModel) {
        List<PlaDebtClaimReducRecordBillRel> list = plaDebtClaimReducRecordBillRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaDebtClaimReducRecordBillRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pdcrrbrSerno}")
    protected ResultDto<PlaDebtClaimReducRecordBillRel> show(@PathVariable("pdcrrbrSerno") String pdcrrbrSerno) {
        PlaDebtClaimReducRecordBillRel plaDebtClaimReducRecordBillRel = plaDebtClaimReducRecordBillRelService.selectByPrimaryKey(pdcrrbrSerno);
        return new ResultDto<PlaDebtClaimReducRecordBillRel>(plaDebtClaimReducRecordBillRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaDebtClaimReducRecordBillRelDto> create(@RequestBody PlaDebtClaimReducRecordBillRelDto plaDebtClaimReducRecordBillRel) throws URISyntaxException {
        plaDebtClaimReducRecordBillRelService.insert(plaDebtClaimReducRecordBillRel);
        return new ResultDto<PlaDebtClaimReducRecordBillRelDto>(plaDebtClaimReducRecordBillRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaDebtClaimReducRecordBillRel plaDebtClaimReducRecordBillRel) throws URISyntaxException {
        int result = plaDebtClaimReducRecordBillRelService.update(plaDebtClaimReducRecordBillRel);
        return new ResultDto<Integer>(result);
    }



    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaDebtClaimReducRecordBillRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryAll")
    protected ResultDto<List<PlaDebtClaimReducRecordBillRel>> queryAll(@RequestBody QueryModel queryModel) {
        List<PlaDebtClaimReducRecordBillRel> list = plaDebtClaimReducRecordBillRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaDebtClaimReducRecordBillRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/showByPdcrrbrSerno")
    protected ResultDto<PlaDebtClaimReducRecordBillRel> showByPdcrrbrSerno(@RequestBody String pdcrrbrSerno) {
        PlaDebtClaimReducRecordBillRel plaDebtClaimReducRecordBillRel = plaDebtClaimReducRecordBillRelService.selectByPrimaryKey(pdcrrbrSerno);
        return new ResultDto<PlaDebtClaimReducRecordBillRel>(plaDebtClaimReducRecordBillRel);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteByPdcrrbrSerno")
    protected ResultDto<Integer> deleteByPdcrrbrSernolete(@RequestBody PlaDebtClaimReducRecordBillRel plaDebtClaimReducRecordBillRel) {
        int result = plaDebtClaimReducRecordBillRelService.deleteByPdcrrbrSernolete(plaDebtClaimReducRecordBillRel);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/savePlaDebtClaimReducRecordBillRel")
    protected ResultDto<Integer> savePlaDebtClaimReducRecordBillRel(@RequestBody List<PlaDebtClaimReducRecordBillRelDto> plaDebtClaimReducRecordBillRelList) throws URISyntaxException {
        int result=plaDebtClaimReducRecordBillRelService.savePlaDebtClaimReducRecordBillRel(plaDebtClaimReducRecordBillRelList);
        return new ResultDto<Integer>(result);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/30 20:27
     * @注释 冲正处理
     */
    @PostMapping("/czcl")
    protected ResultDto czcl( @RequestBody PlaDebtClaimReducRecordBillRel plaDebtClaimReducRecordBillRel) {
        return  plaDebtClaimReducRecordBillRelService.ib1241(plaDebtClaimReducRecordBillRel);

    }
}
