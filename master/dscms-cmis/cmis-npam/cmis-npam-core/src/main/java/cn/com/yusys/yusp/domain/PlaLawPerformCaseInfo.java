/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawPerformCaseInfo
 * @类描述: pla_law_perform_case_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-07 19:08:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_law_perform_case_info")
public class PlaLawPerformCaseInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 执行情况流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PLPCI_SERNO")
    private String plpciSerno;

    /**
     * 执行案号
     **/
    @Column(name = "EXE_CASE_NO", unique = false, nullable = true, length = 40)
    private String exeCaseNo;

    /**
     * 案件流水号
     **/
    @Column(name = "CASE_SERNO", unique = false, nullable = true, length = 40)
    private String caseSerno;

    /**
     * 执行立案日期
     **/
    @Column(name = "EXE_FILING_DATE", unique = false, nullable = true, length = 10)
    private String exeFilingDate;

    /**
     * 执行现状
     **/
    @Column(name = "EXE_STATUS", unique = false, nullable = true, length = 5)
    private String exeStatus;

    /**
     * 是否第三人执行异议
     **/
    @Column(name = "IS_TDP_EXEC_DISSENT", unique = false, nullable = true, length = 5)
    private String isTdpExecDissent;

    /**
     * 执行总标的
     **/
    @Column(name = "EXE_TOTAL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal exeTotalAmt;

    /**
     * 执行法院
     **/
    @Column(name = "ACCEPT_COURT", unique = false, nullable = true, length = 80)
    private String acceptCourt;

    /**
     * 执行法官
     **/
    @Column(name = "JUDGE", unique = false, nullable = true, length = 80)
    private String judge;

    /**
     * 代理方式
     **/
    @Column(name = "AGCY_MODE", unique = false, nullable = true, length = 5)
    private String agcyMode;

    /**
     * 代理律师
     **/
    @Column(name = "LAWYER_NAME", unique = false, nullable = true, length = 80)
    private String lawyerName;

    /**
     * 律师联系方式
     **/
    @Column(name = "LAWYER_TEL_NO", unique = false, nullable = true, length = 20)
    private String lawyerTelNo;

    /**
     * 所属律师事务所名称
     **/
    @Column(name = "LAW_OFFICE_NAME", unique = false, nullable = true, length = 80)
    private String lawOfficeName;

    /**
     * 内部代理人姓名
     **/
    @Column(name = "AGCY_NAME", unique = false, nullable = true, length = 80)
    private String agcyName;

    /**
     * 内部代理人联系方式
     **/
    @Column(name = "AGCY_TEL_NO", unique = false, nullable = true, length = 20)
    private String agcyTelNo;

    /**
     * 是否和解
     **/
    @Column(name = "IS_RECNC", unique = false, nullable = true, length = 5)
    private String isRecnc;

    /**
     * 和解日期
     **/
    @Column(name = "RECNC_DATE", unique = false, nullable = true, length = 10)
    private String recncDate;

    /**
     * 和解内容
     **/
    @Column(name = "RECNC_CONT", unique = false, nullable = true, length = 2000)
    private String recncCont;

    /**
     * 案件文书落款日期
     **/
    @Column(name = "DCMNTS_INSCRIBE_DATE", unique = false, nullable = true, length = 10)
    private String dcmntsInscribeDate;

    /**
     * 原因
     **/
    @Column(name = "RESN", unique = false, nullable = true, length = 5)
    private String resn;

    /**
     * 恢复执行案号
     **/
    @Column(name = "UNSTP_EXE_CASE_NO", unique = false, nullable = true, length = 40)
    private String unstpExeCaseNo;

    /**
     * 恢复执行日期
     **/
    @Column(name = "UNSTP_EXE_DATE", unique = false, nullable = true, length = 10)
    private String unstpExeDate;

    /**
     * 备注
     **/
    @Column(name = "MEMO", unique = false, nullable = true, length = 2000)
    private String memo;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaLawPerformCaseInfo() {
        // default implementation ignored
    }

    /**
     * @param plpciSerno
     */
    public void setPlpciSerno(String plpciSerno) {
        this.plpciSerno = plpciSerno;
    }

    /**
     * @return plpciSerno
     */
    public String getPlpciSerno() {
        return this.plpciSerno;
    }

    /**
     * @param exeCaseNo
     */
    public void setExeCaseNo(String exeCaseNo) {
        this.exeCaseNo = exeCaseNo;
    }

    /**
     * @return exeCaseNo
     */
    public String getExeCaseNo() {
        return this.exeCaseNo;
    }

    /**
     * @param caseSerno
     */
    public void setCaseSerno(String caseSerno) {
        this.caseSerno = caseSerno;
    }

    /**
     * @return caseSerno
     */
    public String getCaseSerno() {
        return this.caseSerno;
    }

    /**
     * @param exeFilingDate
     */
    public void setExeFilingDate(String exeFilingDate) {
        this.exeFilingDate = exeFilingDate;
    }

    /**
     * @return exeFilingDate
     */
    public String getExeFilingDate() {
        return this.exeFilingDate;
    }

    /**
     * @param exeStatus
     */
    public void setExeStatus(String exeStatus) {
        this.exeStatus = exeStatus;
    }

    /**
     * @return exeStatus
     */
    public String getExeStatus() {
        return this.exeStatus;
    }

    /**
     * @param isTdpExecDissent
     */
    public void setIsTdpExecDissent(String isTdpExecDissent) {
        this.isTdpExecDissent = isTdpExecDissent;
    }

    /**
     * @return isTdpExecDissent
     */
    public String getIsTdpExecDissent() {
        return this.isTdpExecDissent;
    }

    /**
     * @param exeTotalAmt
     */
    public void setExeTotalAmt(java.math.BigDecimal exeTotalAmt) {
        this.exeTotalAmt = exeTotalAmt;
    }

    /**
     * @return exeTotalAmt
     */
    public java.math.BigDecimal getExeTotalAmt() {
        return this.exeTotalAmt;
    }

    /**
     * @param acceptCourt
     */
    public void setAcceptCourt(String acceptCourt) {
        this.acceptCourt = acceptCourt;
    }

    /**
     * @return acceptCourt
     */
    public String getAcceptCourt() {
        return this.acceptCourt;
    }

    /**
     * @param judge
     */
    public void setJudge(String judge) {
        this.judge = judge;
    }

    /**
     * @return judge
     */
    public String getJudge() {
        return this.judge;
    }

    /**
     * @param agcyMode
     */
    public void setAgcyMode(String agcyMode) {
        this.agcyMode = agcyMode;
    }

    /**
     * @return agcyMode
     */
    public String getAgcyMode() {
        return this.agcyMode;
    }

    /**
     * @param lawyerName
     */
    public void setLawyerName(String lawyerName) {
        this.lawyerName = lawyerName;
    }

    /**
     * @return lawyerName
     */
    public String getLawyerName() {
        return this.lawyerName;
    }

    /**
     * @param lawyerTelNo
     */
    public void setLawyerTelNo(String lawyerTelNo) {
        this.lawyerTelNo = lawyerTelNo;
    }

    /**
     * @return lawyerTelNo
     */
    public String getLawyerTelNo() {
        return this.lawyerTelNo;
    }

    /**
     * @param lawOfficeName
     */
    public void setLawOfficeName(String lawOfficeName) {
        this.lawOfficeName = lawOfficeName;
    }

    /**
     * @return lawOfficeName
     */
    public String getLawOfficeName() {
        return this.lawOfficeName;
    }

    /**
     * @param agcyName
     */
    public void setAgcyName(String agcyName) {
        this.agcyName = agcyName;
    }

    /**
     * @return agcyName
     */
    public String getAgcyName() {
        return this.agcyName;
    }

    /**
     * @param agcyTelNo
     */
    public void setAgcyTelNo(String agcyTelNo) {
        this.agcyTelNo = agcyTelNo;
    }

    /**
     * @return agcyTelNo
     */
    public String getAgcyTelNo() {
        return this.agcyTelNo;
    }

    /**
     * @param isRecnc
     */
    public void setIsRecnc(String isRecnc) {
        this.isRecnc = isRecnc;
    }

    /**
     * @return isRecnc
     */
    public String getIsRecnc() {
        return this.isRecnc;
    }

    /**
     * @param recncDate
     */
    public void setRecncDate(String recncDate) {
        this.recncDate = recncDate;
    }

    /**
     * @return recncDate
     */
    public String getRecncDate() {
        return this.recncDate;
    }

    /**
     * @param recncCont
     */
    public void setRecncCont(String recncCont) {
        this.recncCont = recncCont;
    }

    /**
     * @return recncCont
     */
    public String getRecncCont() {
        return this.recncCont;
    }

    /**
     * @param dcmntsInscribeDate
     */
    public void setDcmntsInscribeDate(String dcmntsInscribeDate) {
        this.dcmntsInscribeDate = dcmntsInscribeDate;
    }

    /**
     * @return dcmntsInscribeDate
     */
    public String getDcmntsInscribeDate() {
        return this.dcmntsInscribeDate;
    }

    /**
     * @param resn
     */
    public void setResn(String resn) {
        this.resn = resn;
    }

    /**
     * @return resn
     */
    public String getResn() {
        return this.resn;
    }

    /**
     * @param unstpExeCaseNo
     */
    public void setUnstpExeCaseNo(String unstpExeCaseNo) {
        this.unstpExeCaseNo = unstpExeCaseNo;
    }

    /**
     * @return unstpExeCaseNo
     */
    public String getUnstpExeCaseNo() {
        return this.unstpExeCaseNo;
    }

    /**
     * @param unstpExeDate
     */
    public void setUnstpExeDate(String unstpExeDate) {
        this.unstpExeDate = unstpExeDate;
    }

    /**
     * @return unstpExeDate
     */
    public String getUnstpExeDate() {
        return this.unstpExeDate;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}