/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaAssetPldSellInfo;
import cn.com.yusys.yusp.service.PlaAssetPldSellInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldSellInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plaassetpldsellinfo")
public class PlaAssetPldSellInfoResource {
    @Autowired
    private PlaAssetPldSellInfoService plaAssetPldSellInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaAssetPldSellInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaAssetPldSellInfo> list = plaAssetPldSellInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaAssetPldSellInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<PlaAssetPldSellInfo>> index(QueryModel queryModel) {
        List<PlaAssetPldSellInfo> list = plaAssetPldSellInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaAssetPldSellInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{papsiSerno}")
    protected ResultDto<PlaAssetPldSellInfo> show(@PathVariable("papsiSerno") String papsiSerno) {
        PlaAssetPldSellInfo plaAssetPldSellInfo = plaAssetPldSellInfoService.selectByPrimaryKey(papsiSerno);
        return new ResultDto<PlaAssetPldSellInfo>(plaAssetPldSellInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<PlaAssetPldSellInfo> create(@RequestBody PlaAssetPldSellInfo plaAssetPldSellInfo) throws URISyntaxException {
        plaAssetPldSellInfoService.insert(plaAssetPldSellInfo);
        return new ResultDto<PlaAssetPldSellInfo>(plaAssetPldSellInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaAssetPldSellInfo plaAssetPldSellInfo) throws URISyntaxException {
        int result = plaAssetPldSellInfoService.update(plaAssetPldSellInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{papsiSerno}")
    protected ResultDto<Integer> delete(@PathVariable("papsiSerno") String papsiSerno) {
        int result = plaAssetPldSellInfoService.deleteByPrimaryKey(papsiSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaAssetPldSellInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据处置申请流水号获取出售信息
     *
     * @author jijian_yx
     * @date 2021/6/10 10:13
     **/
    @ApiOperation("根据处置申请流水号获取出售信息")
    @PostMapping("/querybypapaiserno")
    protected ResultDto<PlaAssetPldSellInfo> queryByPapaiSerno(@RequestBody String papaiSerno) {
        PlaAssetPldSellInfo plaAssetPldSellInfo = plaAssetPldSellInfoService.selectByPapaiSerno(papaiSerno);
        return new ResultDto<PlaAssetPldSellInfo>(plaAssetPldSellInfo);
    }

    /**
     * 保存/更新出售信息
     *
     * @author jijian_yx
     * @date 2021/6/10 18:01
     **/
    @ApiOperation("保存/更新出售信息")
    @PostMapping("/insert")
    protected ResultDto<Map<String,String>> insert(@RequestBody PlaAssetPldSellInfo plaAssetPldSellInfo) {
        Map<String,String> map = plaAssetPldSellInfoService.insertInfo(plaAssetPldSellInfo);
        return new ResultDto<Map<String,String>>(map);
    }
}
