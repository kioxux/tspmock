package cn.com.yusys.yusp.service.client.common;

import cn.com.yusys.yusp.commons.data.authority.DataAuthorizationInfo;
import cn.com.yusys.yusp.commons.data.authority.DataAuthorizationService;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.redis.template.YuspRedisTemplate;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 通用外围接口调用类
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class CommonService {

    private static final Logger logger = LoggerFactory.getLogger(CommonService.class);
    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private DataAuthorizationService dataAuthorizationService;

    @Autowired
    private YuspRedisTemplate yuspRedisTemplate;

    /**
     * 根据机构编号获取下级机构号
     *
     * @author jijian_yx
     * @date 2021/8/24 17:28
     **/
    public List<String> getLowerOrgId(String orgCode) {
        logger.info("根据机构编号获取下级机构号开始，请求参数:" + orgCode);
        List<String> orgCodes = new ArrayList<>();
        ResultDto<List<String>> resultDto = null;
        try {
            resultDto = adminSmOrgService.getLowerOrgId(orgCode);
            if (ResultDto.success().getCode().equals(resultDto.getCode())) {
                orgCodes = resultDto.getData();
            }
        } catch (Exception e) {
            logger.info("根据机构编号获取下级机构号异常，异常信息为:" + e.getMessage());
        }
        logger.info("根据机构编号获取下级机构号结束，响应参数:" + JSON.toJSONString(resultDto));
        return orgCodes;
    }
    /**
     * 适用于导出查询sql中设置配置的角色相关数据权限
     *
     * @author jijian_yx
     * @date 2021/10/11 22:10
     **/
    public String setDataAuthority(String apiUrl) {
        logger.info("导出查询sql设置角色数据权限开始");
        String dataAuth = "";
        try {
            User user = SessionUtils.getUserInformation();
            User loginUser = SessionUtils.getUserInformation();
            List<DataAuthorizationInfo> dataAuthorizationInfoList =
                    dataAuthorizationService.matching(apiUrl, "POST", loginUser.getUserId());
            if (null != dataAuthorizationInfoList && dataAuthorizationInfoList.size() > 0) {
                for (DataAuthorizationInfo dataAuthorizationInfo : dataAuthorizationInfoList) {
                    String sqlTemplate = dataAuthorizationInfo.getSqlTemplate();
                    dataAuth += " AND " + sqlTemplate;
                }
                dataAuth = dataAuth.replaceAll("\\$", "");
                dataAuth = dataAuth.replaceAll("\\{", "");
                dataAuth = dataAuth.replaceAll("}", "");
            }
            String currentOrgCode = user.getOrg().getCode();// 当前用户所属的机构码
            if (!cn.com.yusys.yusp.commons.util.StringUtils.isBlank(currentOrgCode)) {
                if (dataAuth.contains("_orgCode")) {
                    dataAuth = dataAuth.replaceAll("_orgCode", "'" + currentOrgCode + "'");
                }
                if (dataAuth.contains("_orgTree")) {
                    Object hget = yuspRedisTemplate.hget("orgTree", currentOrgCode);
                    if (Objects.nonNull(hget)) {
                        List<String> orgTreeList = (List<String>) ObjectMapperUtils.instance().convertValue(hget, List.class);
                        StringBuilder orgTree = new StringBuilder();
                        if (null != orgTreeList && orgTreeList.size() > 0) {
                            for (String orgCode : orgTreeList) {
                                if (cn.com.yusys.yusp.commons.util.StringUtils.isBlank(orgTree.toString())) {
                                    orgTree.append("'").append(orgCode).append("'");
                                } else {
                                    orgTree.append(",'").append(orgCode).append("'");
                                }
                            }
                        }
                        dataAuth = dataAuth.replaceAll("_orgTree", orgTree.toString());
                    }
                }
            }
            String currentUserCode = user.getUserId();
            if (cn.com.yusys.yusp.commons.util.StringUtils.nonBlank(currentUserCode)) {
                if (dataAuth.contains("_userCode")) {
                    dataAuth = dataAuth.replaceAll("_userCode", "'" + currentUserCode + "'");
                }
                if (dataAuth.contains("_areaXwUser")) {
                    Object areaXwUserHget = yuspRedisTemplate.hget("areaXwUser", currentUserCode);
                    if (Objects.nonNull(areaXwUserHget)) {
                        List<String> areaXwUserList = (List<String>) ObjectMapperUtils.instance().convertValue(areaXwUserHget, List.class);
                        StringBuilder areaXwUser = new StringBuilder();
                        if (null != areaXwUserList && areaXwUserList.size() > 0) {
                            for (String wxCode : areaXwUserList) {
                                if (cn.com.yusys.yusp.commons.util.StringUtils.isBlank(areaXwUser.toString())) {
                                    areaXwUser.append("'").append(wxCode).append("'");
                                } else {
                                    areaXwUser.append(",'").append(wxCode).append("'");
                                }
                            }
                        }
                        dataAuth = dataAuth.replaceAll("_areaXwUser", areaXwUser.toString());
                    }
                }
                if (dataAuth.contains("_areaXwOrg")) {
                    Object areaXwOrgHget = yuspRedisTemplate.hget("areaXwOrg", currentUserCode);
                    if (Objects.nonNull(areaXwOrgHget)) {
                        List<String> areaXwOrgList = (List<String>) ObjectMapperUtils.instance().convertValue(areaXwOrgHget, List.class);
                        StringBuilder areaXwOrg = new StringBuilder();
                        if (null != areaXwOrgList && areaXwOrgList.size() > 0) {
                            for (String wxOrgCode : areaXwOrgList) {
                                if (cn.com.yusys.yusp.commons.util.StringUtils.isBlank(areaXwOrg.toString())) {
                                    areaXwOrg.append("'").append(wxOrgCode).append("'");
                                } else {
                                    areaXwOrg.append(",'").append(wxOrgCode).append("'");
                                }
                            }
                        }
                        dataAuth = dataAuth.replaceAll("_areaXwOrg", areaXwOrg.toString());
                    }
                }
                if (dataAuth.contains("_allXwUser")) {
                    Object allXwHget = yuspRedisTemplate.hget("allXwUser", currentUserCode);
                    if (Objects.nonNull(allXwHget)) {
                        List<String> allXwUserList = (List<String>) ObjectMapperUtils.instance().convertValue(allXwHget, List.class);
                        StringBuilder allXwUser = new StringBuilder();
                        if (null != allXwUserList && allXwUserList.size() > 0) {
                            for (String xwCode : allXwUserList) {
                                if (cn.com.yusys.yusp.commons.util.StringUtils.isBlank(allXwUser.toString())) {
                                    allXwUser.append("'").append(xwCode).append("'");
                                } else {
                                    allXwUser.append(",'").append(xwCode).append("'");
                                }
                            }
                        }
                        dataAuth = dataAuth.replaceAll("_allXwUser", allXwUser.toString());
                    }
                }
                if (dataAuth.contains("_jzzyUser")) {
                    Object jzzyHget = yuspRedisTemplate.hget("jzzyUser", currentUserCode);
                    if (Objects.nonNull(jzzyHget)) {
                        List<String> jzzyUserList = (List<String>) ObjectMapperUtils.instance().convertValue(jzzyHget, List.class);
                        StringBuilder jzzyUser = new StringBuilder();
                        if (null != jzzyUserList && jzzyUserList.size() > 0) {
                            for (String jzzyCode : jzzyUserList) {
                                if (cn.com.yusys.yusp.commons.util.StringUtils.isBlank(jzzyUser.toString())) {
                                    jzzyUser.append("'").append(jzzyCode).append("'");
                                } else {
                                    jzzyUser.append(",'").append(jzzyCode).append("'");
                                }
                            }
                        }
                        dataAuth = dataAuth.replaceAll("_jzzyUser", jzzyUser.toString());
                    }
                }
            }
            logger.info("导出查询sql设置角色数据权限,API[{}],数据权限[{}]", apiUrl, dataAuth);
        } catch (Exception e) {
            logger.info("导出查询sql设置角色数据权限异常[{}]", e.getMessage());
            e.printStackTrace();
        }
        logger.info("导出查询sql设置角色数据权限结束");
        return dataAuth;
    }
}
