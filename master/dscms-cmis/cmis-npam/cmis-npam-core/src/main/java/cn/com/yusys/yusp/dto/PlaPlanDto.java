package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaPlan
 * @类描述: pla_plan数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-28 14:41:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PlaPlanDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 清收计划编号 **/
	private String recoverySerno;
	
	/** 清收年份 **/
	private String recoveryYear;
	
	/** 预计清收总额 **/
	private java.math.BigDecimal recoveryTotalAmt;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param recoverySerno
	 */
	public void setRecoverySerno(String recoverySerno) {
		this.recoverySerno = recoverySerno == null ? null : recoverySerno.trim();
	}
	
    /**
     * @return RecoverySerno
     */	
	public String getRecoverySerno() {
		return this.recoverySerno;
	}
	
	/**
	 * @param recoveryYear
	 */
	public void setRecoveryYear(String recoveryYear) {
		this.recoveryYear = recoveryYear == null ? null : recoveryYear.trim();
	}
	
    /**
     * @return RecoveryYear
     */	
	public String getRecoveryYear() {
		return this.recoveryYear;
	}
	
	/**
	 * @param recoveryTotalAmt
	 */
	public void setRecoveryTotalAmt(java.math.BigDecimal recoveryTotalAmt) {
		this.recoveryTotalAmt = recoveryTotalAmt;
	}
	
    /**
     * @return RecoveryTotalAmt
     */	
	public java.math.BigDecimal getRecoveryTotalAmt() {
		return this.recoveryTotalAmt;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}