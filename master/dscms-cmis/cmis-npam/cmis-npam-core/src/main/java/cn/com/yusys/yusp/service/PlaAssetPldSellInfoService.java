/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisNpamConstants;
import cn.com.yusys.yusp.domain.PlaAssetPldSellInfo;
import cn.com.yusys.yusp.repository.mapper.PlaAssetPldSellInfoMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldSellInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaAssetPldSellInfoService {

    @Autowired
    private PlaAssetPldSellInfoMapper plaAssetPldSellInfoMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaAssetPldSellInfo selectByPrimaryKey(String papsiSerno) {
        return plaAssetPldSellInfoMapper.selectByPrimaryKey(papsiSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaAssetPldSellInfo> selectAll(QueryModel model) {
        List<PlaAssetPldSellInfo> records = (List<PlaAssetPldSellInfo>) plaAssetPldSellInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaAssetPldSellInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaAssetPldSellInfo> list = plaAssetPldSellInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaAssetPldSellInfo record) {
        return plaAssetPldSellInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaAssetPldSellInfo record) {
        return plaAssetPldSellInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaAssetPldSellInfo record) {
        return plaAssetPldSellInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaAssetPldSellInfo record) {
        return plaAssetPldSellInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String papsiSerno) {
        return plaAssetPldSellInfoMapper.deleteByPrimaryKey(papsiSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaAssetPldSellInfoMapper.deleteByIds(ids);
    }

    /**
     * 根据处置申请流水号获取出售信息
     *
     * @author jijian_yx
     * @date 2021/6/10 10:34
     **/
    public PlaAssetPldSellInfo selectByPapaiSerno(String papaiSerno) {
        return plaAssetPldSellInfoMapper.selectByPapaiSerno(papaiSerno);
    }

    /**
     * 保存/更新出售信息
     *
     * @author jijian_yx
     * @date 2021/6/11 9:22
     **/
    public Map<String,String> insertInfo(PlaAssetPldSellInfo plaAssetPldSellInfo) {
        Map<String,String> map = new HashMap<>();
        String flag = "";
        String msg = "";
        PlaAssetPldSellInfo sellInfo = selectByPapaiSerno(plaAssetPldSellInfo.getPapaiSerno());
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        if (sellInfo != null) {
            plaAssetPldSellInfo.setUpdDate(openDay);
            updateSelective(plaAssetPldSellInfo);
            flag = CmisNpamConstants.FLAG_SUCCESS;
            msg = "更新成功";
        } else {
            plaAssetPldSellInfo.setUpdDate(openDay);
            insertSelective(plaAssetPldSellInfo);
            flag = CmisNpamConstants.FLAG_SUCCESS;
            msg = "保存成功";
        }
        map.put("flag",flag);
        map.put("msg",msg);
        return map;
    }

    /**
     * 根据抵债资产处置流水删除出售信息
     *
     * @author jijian_yx
     * @date 2021/6/11 10:56
     **/
    public int deleteByPapaiSerno(String papaiSerno) {
        return plaAssetPldSellInfoMapper.deleteByPapaiSerno(papaiSerno);
    }
}
