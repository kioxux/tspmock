/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisNpamConstants;
import cn.com.yusys.yusp.dto.client.http.image.imageDataSize.ImageDataReqDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaBcmReceiptInfo;
import cn.com.yusys.yusp.repository.mapper.PlaBcmReceiptInfoMapper;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBcmReceiptInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-04 11:28:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaBcmReceiptInfoService {

    @Autowired
    private PlaBcmReceiptInfoMapper plaBcmReceiptInfoMapper;
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;
    @Autowired
    private Dscms2ImageClientService dscms2ImageClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaBcmReceiptInfo selectByPrimaryKey(String pbreiSerno) {
        return plaBcmReceiptInfoMapper.selectByPrimaryKey(pbreiSerno);
    }

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据任务编号查询
     * @参数与返回说明:
     * @算法描述: 无
     * 刘权
     */

    public PlaBcmReceiptInfo selectByTaskNo(String taskNo) {
        return plaBcmReceiptInfoMapper.selectByTaskNo(taskNo);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaBcmReceiptInfo> selectAll(QueryModel model) {
        List<PlaBcmReceiptInfo> records = (List<PlaBcmReceiptInfo>) plaBcmReceiptInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaBcmReceiptInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaBcmReceiptInfo> list = plaBcmReceiptInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaBcmReceiptInfo record) {
        return plaBcmReceiptInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaBcmReceiptInfo record) {
        return plaBcmReceiptInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaBcmReceiptInfo record) {
        return plaBcmReceiptInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateByPbreiSerno
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     * 刘权
     */

    public int updateByPbreiSerno(PlaBcmReceiptInfo record) {

        int count = 0 ;
        if(record !=null){
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if(userInfo != null){
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();
            }
            record.setCreateTime(createTime);
            record.setUpdId(inputId);
            record.setUpdBrId(inputBrId);
            record.setUpdDate(inputDate);
            record.setUpdateTime(createTime);
            count=plaBcmReceiptInfoMapper.updateByPrimaryKey(record);
        }
        return count;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaBcmReceiptInfo record) {
        return plaBcmReceiptInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pbreiSerno) {
        return plaBcmReceiptInfoMapper.deleteByPrimaryKey(pbreiSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaBcmReceiptInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: save
     * @方法描述: 保存
     * @参数与返回说明:
     * @算法描述: 无
     * 刘权
     */

    public int  save(PlaBcmReceiptInfo record) {
        int count = 0;
        if(record != null) {

            if (record.getPbreiSerno() == null || record.getPbreiSerno() == ""){
                String pbreiSerno = sequenceTemplateClient.getSequenceTemplate(CmisNpamConstants.ZCBQ_REG_SERNO, new HashMap<>());
                record.setPbreiSerno(pbreiSerno);
            }
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if(userInfo != null){
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();
            }
            PlaBcmReceiptInfo plaBcmReceiptInfo = plaBcmReceiptInfoMapper.selectByPrimaryKey(record.getPbreiSerno());
            if(plaBcmReceiptInfo != null){
                record.setUpdId(inputId);
                record.setUpdBrId(inputBrId);
                record.setUpdDate(inputDate);
                record.setUpdateTime(createTime);
                count=plaBcmReceiptInfoMapper.updateByPrimaryKey(record);
            }else{
                record.setCreateTime(createTime);
                record.setUpdId(inputId);
                record.setUpdBrId(inputBrId);
                record.setUpdDate(inputDate);
                record.setUpdateTime(createTime);
                count=plaBcmReceiptInfoMapper.insert(record);
            }
        }
        return count;
    }

    public ResultDto<Map<String, Integer>> saveByImageImageDataSize(ImageDataReqDto imageDataReqDto) {
        ResultDto<Map<String, Integer>> map = dscms2ImageClientService.imageImageDataSize(imageDataReqDto);
        return map;
    }
}
