/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaBadDebtWriteoffBillRel;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBadDebtWriteoffBillRelMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-09 09:34:55
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface PlaBadDebtWriteoffBillRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    PlaBadDebtWriteoffBillRel selectByPrimaryKey(@Param("pwbrSerno") String pwbrSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PlaBadDebtWriteoffBillRel> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insert(PlaBadDebtWriteoffBillRel record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insertSelective(PlaBadDebtWriteoffBillRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPrimaryKey(PlaBadDebtWriteoffBillRel record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPrimaryKeySelective(PlaBadDebtWriteoffBillRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    int deleteByPrimaryKey(@Param("pwbrSerno") String pwbrSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多主表流水号删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int deleteBySerno(@Param("serno") String serno);

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据流水号查询金额信息
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    Map selectBySerno(@Param("serno") String serno, @Param("writeoffFlag") String writeoffFlag);

    /**
     * @方法名称: selectPlaBadDebtWriteoffBillRel
     * @方法描述: 根据流水号借据编号查询借据是否已经引入
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    PlaBadDebtWriteoffBillRel selectPlaBadDebtWriteoffBillRel(PlaBadDebtWriteoffBillRel record);
    /**
     * @方法名称: selectCount
     * @方法描述: 根据借据编号查询借据存在在途借据
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    int selectCount(PlaBadDebtWriteoffBillRel record);

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PlaBadDebtWriteoffBillRel> selectByserno(@Param("serno") String serno);

    /**
     * @方法名称: selectByCusId
     * @方法描述: 根据客户编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PlaBadDebtWriteoffBillRel> selectByCusId(@Param("cusId") String cusId , @Param("serno") String serno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PlaBadDebtWriteoffBillRel> selectByModel1(QueryModel model);
}