/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.PlaPescInfo;
import cn.com.yusys.yusp.repository.mapper.PlaPescInfoMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaPescInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:52:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaPescInfoService {

    @Autowired
    private PlaPescInfoMapper plaPescInfoMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PlaPescInfo selectByPrimaryKey(String ppiSerno) {
        return plaPescInfoMapper.selectByPrimaryKey(ppiSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaPescInfo> selectAll(QueryModel model) {
        List<PlaPescInfo> records = (List<PlaPescInfo>) plaPescInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PlaPescInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaPescInfo> list = plaPescInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PlaPescInfo record) {
        return plaPescInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PlaPescInfo record) {
        return plaPescInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PlaPescInfo record) {
        int count = 0;
        if (record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();
            }
            record.setUpdId(inputId);
            record.setUpdBrId(inputBrId);
            record.setUpdDate(inputDate);
            record.setUpdateTime(createTime);
            count = plaPescInfoMapper.updateByPrimaryKey(record);
        }
        return count;

    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PlaPescInfo record) {
        return plaPescInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String ppiSerno) {
        return plaPescInfoMapper.deleteByPrimaryKey(ppiSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaPescInfoMapper.deleteByIds(ids);
    }
    /**
     * @方法名称: savePlaPescInfo
     * @方法描述: 保存
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：蔡宁波
     */
    public int save(PlaPescInfo plaPescInfo) {
        int count = 0;
        if(plaPescInfo != null){
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if(userInfo != null){
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();
            }
                plaPescInfo.setPpiSerno(StringUtils.uuid(true));
                plaPescInfo.setInputId(inputId);
                plaPescInfo.setInputBrId(inputBrId);
                plaPescInfo.setInputDate(inputDate);
                plaPescInfo.setCreateTime(createTime);
                plaPescInfo.setUpdId(inputId);
                plaPescInfo.setUpdBrId(inputBrId);
                plaPescInfo.setUpdDate(inputDate);
                plaPescInfo.setUpdateTime(createTime);
                count=plaPescInfoMapper.insert(plaPescInfo);

        }
        return count;
    }
    /**
     * 根据条件查询
     * @param queryModel
     * @return
     */
    public List<PlaPescInfo> selectByCondition(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PlaPescInfo> list = plaPescInfoMapper.selectByModel(queryModel);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insertReplyPescs
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：蔡宁波
     */
    public int insertReplyPescs(List<PlaPescInfo> plaPescInfoList) {
        // 规则编号
        String ruleId="";
        int count=0;
        if(plaPescInfoList != null){
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if(userInfo != null){
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
                //催收规则参数配表

                for (PlaPescInfo plaPescInfos:plaPescInfoList) {
                    PlaPescInfo plaPescInfo = plaPescInfoMapper.selectByPrimaryKey(plaPescInfos.getPpiSerno());
                    if(plaPescInfo !=null){
                        plaPescInfos.setUpdDate(openDay);
                        plaPescInfos.setUpdBrId(inputBrId);
                        plaPescInfos.setUpdId(inputId);
                        plaPescInfos.setUpdateTime(createTime);
                        count=plaPescInfoMapper.updateByPrimaryKeySelective(plaPescInfos);
                    }else{
                        plaPescInfos.setInputDate(openDay);
                        plaPescInfos.setInputBrId(inputBrId);
                        plaPescInfos.setInputId(inputId);
                        plaPescInfos.setCreateTime(createTime);
                        plaPescInfos.setUpdDate(openDay);
                        plaPescInfos.setUpdBrId(inputBrId);
                        plaPescInfos.setUpdId(inputId);
                        plaPescInfos.setUpdateTime(createTime);
                        count=plaPescInfoMapper.insert(plaPescInfos);
                    }
                }
            }

        return count;
    }
}
