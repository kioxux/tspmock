/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.PlaPlan;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaLawBrokeCaseInfo;
import cn.com.yusys.yusp.service.PlaLawBrokeCaseInfoService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawBrokeCaseInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-03 20:31:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plalawbrokecaseinfo")
public class PlaLawBrokeCaseInfoResource {
    @Autowired
    private PlaLawBrokeCaseInfoService plaLawBrokeCaseInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/queryAll")
    protected ResultDto<List<PlaLawBrokeCaseInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaLawBrokeCaseInfo> list = plaLawBrokeCaseInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaLawBrokeCaseInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaLawBrokeCaseInfo>> index(QueryModel queryModel) {
        List<PlaLawBrokeCaseInfo> list = plaLawBrokeCaseInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawBrokeCaseInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{brokeCaseNo}")
    protected ResultDto<PlaLawBrokeCaseInfo> show(@PathVariable("brokeCaseNo") String brokeCaseNo) {
        PlaLawBrokeCaseInfo plaLawBrokeCaseInfo = plaLawBrokeCaseInfoService.selectByPrimaryKey(brokeCaseNo);
        return new ResultDto<PlaLawBrokeCaseInfo>(plaLawBrokeCaseInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaLawBrokeCaseInfo> create(@RequestBody PlaLawBrokeCaseInfo plaLawBrokeCaseInfo) throws URISyntaxException {
        plaLawBrokeCaseInfoService.insert(plaLawBrokeCaseInfo);
        return new ResultDto<PlaLawBrokeCaseInfo>(plaLawBrokeCaseInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaLawBrokeCaseInfo plaLawBrokeCaseInfo) throws URISyntaxException {
        int result = plaLawBrokeCaseInfoService.updateSelective(plaLawBrokeCaseInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{brokeCaseNo}")
    protected ResultDto<Integer> delete(@PathVariable("brokeCaseNo") String brokeCaseNo) {
        int result = plaLawBrokeCaseInfoService.deleteByPrimaryKey(brokeCaseNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaLawBrokeCaseInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @ApiOperation("破产案件新增")
    @PostMapping("/insert")
    protected ResultDto<PlaLawBrokeCaseInfo> insert(@RequestBody PlaLawBrokeCaseInfo plaLawBrokeCaseInfo) throws URISyntaxException {
        plaLawBrokeCaseInfoService.insertSelective(plaLawBrokeCaseInfo);
        return new ResultDto<PlaLawBrokeCaseInfo>(plaLawBrokeCaseInfo);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("破产案件列表查询")
    @PostMapping("/queryPlaLawBrokeCaseList")
    protected ResultDto<List<PlaLawBrokeCaseInfo>> queryPlaLawBrokeCaseList(@RequestBody QueryModel queryModel) {
        List<PlaLawBrokeCaseInfo> list = plaLawBrokeCaseInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawBrokeCaseInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation("破产案件详细信息查询")
    @PostMapping("/queryPlaLawBrokeCaseInfo")
    protected ResultDto<PlaLawBrokeCaseInfo> queryPlaLawBrokeCaseInfo(@RequestBody PlaLawBrokeCaseInfo plaLawBrokeCaseInfo)  {
        PlaLawBrokeCaseInfo plaLawBrokeCaseInfos = plaLawBrokeCaseInfoService.selectByPrimaryKey(plaLawBrokeCaseInfo.getBrokeCaseNo());
        return new ResultDto<PlaLawBrokeCaseInfo>(plaLawBrokeCaseInfos);
    }
}
