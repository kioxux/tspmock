/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaCardInfoRel
 * @类描述: pla_card_info_rel数据实体类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-06-11 13:51:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@ExcelCsv(namePrefix = "信用卡核销卡信息导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class PlaCardInfoRelVo {

	/** 账户编号 **/
	@ExcelField(title = "账户编号", viewLength = 20)
	private String accno;
	
	/** 客户名称 **/
	@ExcelField(title = "客户名称", viewLength = 20)
	private String cusName;
	
	/** 身份证号码 **/
	@ExcelField(title = "身份证号码", viewLength = 20)
	private String certCode;
	
	/** 最新介质信用卡卡号 **/
	@ExcelField(title = "信用卡卡号", viewLength = 20)
	private String cardNo;
	
	/** 授信额度 **/
	@ExcelField(title = "授信额度", viewLength = 20 ,format = "#0.00")
	private java.math.BigDecimal lmtAmt;
	
	/** 逾期天数 **/
	@ExcelField(title = "账龄", viewLength = 20)
	private String overdueDay;

	/** 核销本金 **/
	@ExcelField(title = "逾期本金金额", viewLength = 20 ,format = "#0.00")
	private java.math.BigDecimal writeoffCap;

	/** 核销利息 **/
	@ExcelField(title = "核销利息", viewLength = 20 ,format = "#0.00")
	private java.math.BigDecimal writeoffInt;

	/** 核销费用 **/
	@ExcelField(title = "核销费用", viewLength = 20 ,format = "#0.00")
	private java.math.BigDecimal writeoffCost;

	/** 核销总金额 **/
	@ExcelField(title = "核销总金额", viewLength = 20 ,format = "#0.00")
	private java.math.BigDecimal totalWriteoffAmt;

	/*
    登记日期
     */
	@ExcelField(title = "登记日期", viewLength = 20)
	private String inputDate;

	/*
  登记人
   */
	@ExcelField(title = "登记人", viewLength = 20)
	private String inputIdName;


	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getInputIdName() {
		return inputIdName;
	}

	public void setInputIdName(String inputIdName) {
		this.inputIdName = inputIdName;
	}

	public String getAccno() {
		return accno;
	}

	public void setAccno(String accno) {
		this.accno = accno;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getCertCode() {
		return certCode;
	}

	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public BigDecimal getLmtAmt() {
		return lmtAmt;
	}

	public void setLmtAmt(BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}

	public String getOverdueDay() {
		return overdueDay;
	}

	public void setOverdueDay(String overdueDay) {
		this.overdueDay = overdueDay;
	}

	public BigDecimal getWriteoffCap() {
		return writeoffCap;
	}

	public void setWriteoffCap(BigDecimal writeoffCap) {
		this.writeoffCap = writeoffCap;
	}

	public BigDecimal getWriteoffInt() {
		return writeoffInt;
	}

	public void setWriteoffInt(BigDecimal writeoffInt) {
		this.writeoffInt = writeoffInt;
	}

	public BigDecimal getWriteoffCost() {
		return writeoffCost;
	}

	public void setWriteoffCost(BigDecimal writeoffCost) {
		this.writeoffCost = writeoffCost;
	}

	public BigDecimal getTotalWriteoffAmt() {
		return totalWriteoffAmt;
	}

	public void setTotalWriteoffAmt(BigDecimal totalWriteoffAmt) {
		this.totalWriteoffAmt = totalWriteoffAmt;
	}

}