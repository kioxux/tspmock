/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaBartPldDebtAppInfo;
import cn.com.yusys.yusp.domain.PlaBartPldDebtBillRel;
import cn.com.yusys.yusp.service.PlaBartPldDebtAppInfoService;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBartPldDebtAppInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plabartplddebtappinfo")
public class PlaBartPldDebtAppInfoResource {
    @Autowired
    private PlaBartPldDebtAppInfoService plaBartPldDebtAppInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaBartPldDebtAppInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaBartPldDebtAppInfo> list = plaBartPldDebtAppInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaBartPldDebtAppInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<PlaBartPldDebtAppInfo>> index(QueryModel queryModel) {
        System.out.println(JSONObject.toJSON(queryModel));
        List<PlaBartPldDebtAppInfo> list = plaBartPldDebtAppInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaBartPldDebtAppInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pdraiSerno}")
    protected ResultDto<PlaBartPldDebtAppInfo> show(@PathVariable("pdraiSerno") String pdraiSerno) {
        PlaBartPldDebtAppInfo plaBartPldDebtAppInfo = plaBartPldDebtAppInfoService.selectByPrimaryKey(pdraiSerno);
        return new ResultDto<PlaBartPldDebtAppInfo>(plaBartPldDebtAppInfo);
    }
    /**
     * @函数名称:show
     * @函数描述:根据主键ID查询详情信息
     * @参数与返回说明: 主键ID
     * @算法描述:
     */
    @PostMapping("/getdetail")
    protected ResultDto<PlaBartPldDebtAppInfo> getDetail(@RequestBody String pdraiSerno) {
        PlaBartPldDebtAppInfo plaBartPldDebtAppInfo = plaBartPldDebtAppInfoService.selectByPrimaryKey(pdraiSerno);
        return new ResultDto<PlaBartPldDebtAppInfo>(plaBartPldDebtAppInfo);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<PlaBartPldDebtAppInfo> create(@RequestBody PlaBartPldDebtAppInfo plaBartPldDebtAppInfo) throws URISyntaxException {
        plaBartPldDebtAppInfoService.insert(plaBartPldDebtAppInfo);
        return new ResultDto<PlaBartPldDebtAppInfo>(plaBartPldDebtAppInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaBartPldDebtAppInfo plaBartPldDebtAppInfo) throws URISyntaxException {
        int result = plaBartPldDebtAppInfoService.update(plaBartPldDebtAppInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pdraiSerno}")
    protected ResultDto<Integer> delete(@PathVariable("pdraiSerno") String pdraiSerno) {
        int result = plaBartPldDebtAppInfoService.deleteByPrimaryKey(pdraiSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaBartPldDebtAppInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 新增以物抵债申请
     *
     * @author jijian_yx
     * @date 2021/6/9 20:23
     **/
    @ApiOperation("新增以物抵债申请")
    @PostMapping("/save")
    protected ResultDto<String> save(@RequestBody PlaBartPldDebtAppInfo appInfo) {
        String pdraiSerno = plaBartPldDebtAppInfoService.save(appInfo);
        return new ResultDto<String>(pdraiSerno);
    }

    /**
     * 获取以物抵债申请列表
     *
     * @author jijian_yx
     * @date 2021/6/9 19:46
     **/
    @ApiOperation("分页查询以物抵债登记申请")
    @PostMapping("/tosignlist")
    protected ResultDto<List<PlaBartPldDebtAppInfo>> toSignlist(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PlaBartPldDebtAppInfo> list = plaBartPldDebtAppInfoService.toSignlist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<PlaBartPldDebtAppInfo>>(list);
    }

    /**
     * 获取以物抵债历史列表
     *
     * @author jijian_yx
     * @date 2021/6/9 19:45
     **/
    @ApiOperation("分页查询以物抵债登记历史")
    @PostMapping("/donesignlist")
    protected ResultDto<List<PlaBartPldDebtAppInfo>> doneSignlist(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PlaBartPldDebtAppInfo> list = plaBartPldDebtAppInfoService.doneSignlist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<PlaBartPldDebtAppInfo>>(list);
    }

    /**
     * 获取以物抵债台账列表
     *
     * @author jijian_yx
     * @date 2021/6/9 19:45
     **/
    @ApiOperation("分页查询以物抵债台账")
    @PostMapping("/getbartplddebtacclist")
    protected ResultDto<List<PlaBartPldDebtAppInfo>> getBartPldDebtAccList(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PlaBartPldDebtAppInfo> list = plaBartPldDebtAppInfoService.getBartPldDebtAccList(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<PlaBartPldDebtAppInfo>>(list);
    }

    /**
     * 以物抵债发送核心登记
     *
     * @author jijian_yx
     * @date 2021/6/9 20:53
     **/
    @ApiOperation("以物抵债发送核心登记")
    @PostMapping("/sendtohxdj")
    protected ResultDto<String> sendToHXDJ(@RequestBody PlaBartPldDebtAppInfo appInfo) {
        return plaBartPldDebtAppInfoService.sendToHXDJ(appInfo);
    }

    /**
     * 以物抵债发送核心记账
     *
     * @author liuquan
     **/
    @ApiOperation("以物抵债发送核心记账")
    @PostMapping("/sendtohxjz")
    protected ResultDto<String> sendToHXJZ(@RequestBody PlaBartPldDebtAppInfo appInfo) {
        return plaBartPldDebtAppInfoService.sendToHXJZ(appInfo);
    }

    /**
     * @创建人
     * @创建时间
     * @注释 冲正处理
     */
    @PostMapping("/czcl")
    protected ResultDto czcl( @RequestBody PlaBartPldDebtAppInfo plaBartPldDebtAppInfo) {
        return  plaBartPldDebtAppInfoService.ib1241(plaBartPldDebtAppInfo);

    }
}
