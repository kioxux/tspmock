/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaCardInfoRel;
import cn.com.yusys.yusp.service.PlaCardInfoRelService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaCardInfoRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-06-11 13:51:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/placardinforel")
public class PlaCardInfoRelResource {
    @Autowired
    private PlaCardInfoRelService plaCardInfoRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaCardInfoRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaCardInfoRel> list = plaCardInfoRelService.selectAll(queryModel);
        return new ResultDto<List<PlaCardInfoRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaCardInfoRel>> index(QueryModel queryModel) {
        List<PlaCardInfoRel> list = plaCardInfoRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaCardInfoRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pbdwabSerno}")
    protected ResultDto<PlaCardInfoRel> show(@PathVariable("pbdwabSerno") String pbdwabSerno) {
        PlaCardInfoRel plaCardInfoRel = plaCardInfoRelService.selectByPrimaryKey(pbdwabSerno);
        return new ResultDto<PlaCardInfoRel>(plaCardInfoRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaCardInfoRel> create(@RequestBody PlaCardInfoRel plaCardInfoRel) throws URISyntaxException {
        plaCardInfoRelService.insert(plaCardInfoRel);
        return new ResultDto<PlaCardInfoRel>(plaCardInfoRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaCardInfoRel plaCardInfoRel) throws URISyntaxException {
        int result = plaCardInfoRelService.update(plaCardInfoRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pbdwabSerno}")
    protected ResultDto<Integer> delete(@PathVariable("pbdwabSerno") String pbdwabSerno) {
        int result = plaCardInfoRelService.deleteByPrimaryKey(pbdwabSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaCardInfoRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     * @创建人：周茂伟
     */
    @PostMapping("/queryPlaCardInfoRelList")
    protected ResultDto<List<PlaCardInfoRel>> queryPlaCardInfoRelList(@RequestBody QueryModel queryModel) {
        List<PlaCardInfoRel> list = plaCardInfoRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaCardInfoRel>>(list);
    }

    /**
     * Excel数据导入
     * @param fileId Excel文件信息ID
     * @return
     */
    @PostMapping("/importplanDetail")
    public ResultDto<String> asyncImportplanDetail(@RequestParam("fileId") String fileId, @RequestBody Map<String, String> paramsMap) {
        String message=plaCardInfoRelService.asyncImportplanDetail(fileId,paramsMap);
        return ResultDto.success().message(message);
    }

    /**
     * Excel模板导出
     */
    @PostMapping("/exportplanDetail")
    public ResultDto<ProgressDto> asyncExportPlanDetail(QueryModel model) {
        ProgressDto progressDto = plaCardInfoRelService.asyncExportPlanDetail(model);
        return ResultDto.success(progressDto);
    }

    /**
     * 异步下载信用卡核销台账列表
     */
    @PostMapping("/exportPlaCardInfoRel")
    public ResultDto<ProgressDto> asyncExportPlaCardInfoRel(@RequestBody QueryModel model) {
        ProgressDto progressDto = plaCardInfoRelService.asyncExportPlaCardInfoRel(model);
        return ResultDto.success(progressDto);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteByPbdwabSerno")
    protected ResultDto<Integer> deleteByPbdwabSerno(@RequestBody String pbdwabSerno) {
        int result = plaCardInfoRelService.deleteByPrimaryKey(pbdwabSerno);
        return new ResultDto<Integer>(result);
    }
}
