package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-npam-core模块
 * @类名称: CfgPlaBcmParam
 * @类描述: cfg_pla_bcm_param数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 20:09:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgPlaBcmParamDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 规则参数流水号 **/
	private String cpbpSerno;
	
	/** 规则编号 **/
	private String ruleId;
	
	/** 规则名称 **/
	private String ruleName;
	
	/** 业务条线 **/
	private String bizLine;
	
	/** 参数代码 **/
	private String paramsCode;
	
	/** 操作符号 **/
	private String oprSymbol;
	
	/** 参数值 **/
	private String paramValue;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param cpbpSerno
	 */
	public void setCpbpSerno(String cpbpSerno) {
		this.cpbpSerno = cpbpSerno == null ? null : cpbpSerno.trim();
	}
	
    /**
     * @return CpbpSerno
     */	
	public String getCpbpSerno() {
		return this.cpbpSerno;
	}
	
	/**
	 * @param ruleId
	 */
	public void setRuleId(String ruleId) {
		this.ruleId = ruleId == null ? null : ruleId.trim();
	}
	
    /**
     * @return RuleId
     */	
	public String getRuleId() {
		return this.ruleId;
	}
	
	/**
	 * @param ruleName
	 */
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName == null ? null : ruleName.trim();
	}
	
    /**
     * @return RuleName
     */	
	public String getRuleName() {
		return this.ruleName;
	}
	
	/**
	 * @param bizLine
	 */
	public void setBizLine(String bizLine) {
		this.bizLine = bizLine == null ? null : bizLine.trim();
	}
	
    /**
     * @return BizLine
     */	
	public String getBizLine() {
		return this.bizLine;
	}
	
	/**
	 * @param paramsCode
	 */
	public void setParamsCode(String paramsCode) {
		this.paramsCode = paramsCode == null ? null : paramsCode.trim();
	}
	
    /**
     * @return ParamsCode
     */	
	public String getParamsCode() {
		return this.paramsCode;
	}
	
	/**
	 * @param oprSymbol
	 */
	public void setOprSymbol(String oprSymbol) {
		this.oprSymbol = oprSymbol == null ? null : oprSymbol.trim();
	}
	
    /**
     * @return OprSymbol
     */	
	public String getOprSymbol() {
		return this.oprSymbol;
	}
	
	/**
	 * @param paramValue
	 */
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue == null ? null : paramValue.trim();
	}
	
    /**
     * @return ParamValue
     */	
	public String getParamValue() {
		return this.paramValue;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}