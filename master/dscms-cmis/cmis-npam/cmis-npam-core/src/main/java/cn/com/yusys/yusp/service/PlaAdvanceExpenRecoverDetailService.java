/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.PlaAdvanceExpenRecoverDetail;
import cn.com.yusys.yusp.repository.mapper.PlaAdvanceExpenRecoverDetailMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAdvanceExpenRecoverDetailService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:52:18
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaAdvanceExpenRecoverDetailService {

    @Autowired
    private PlaAdvanceExpenRecoverDetailMapper plaAdvanceExpenRecoverDetailMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public PlaAdvanceExpenRecoverDetail selectByPrimaryKey(String paerdSerno) {
        return plaAdvanceExpenRecoverDetailMapper.selectByPrimaryKey(paerdSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<PlaAdvanceExpenRecoverDetail> selectAll(QueryModel model) {
        return plaAdvanceExpenRecoverDetailMapper.selectByModel(model);
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<PlaAdvanceExpenRecoverDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaAdvanceExpenRecoverDetail> list = plaAdvanceExpenRecoverDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(PlaAdvanceExpenRecoverDetail record) {
        return plaAdvanceExpenRecoverDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(PlaAdvanceExpenRecoverDetail record) {
        return plaAdvanceExpenRecoverDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(PlaAdvanceExpenRecoverDetail record) {
        int count = 0;
        if (record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();
            }
            record.setUpdId(inputId);
            record.setUpdBrId(inputBrId);
            record.setUpdDate(inputDate);
            record.setUpdateTime(createTime);
            count = plaAdvanceExpenRecoverDetailMapper.updateByPrimaryKey(record);
        }
        return count;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(PlaAdvanceExpenRecoverDetail record) {
        return plaAdvanceExpenRecoverDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String paerdSerno) {
        return plaAdvanceExpenRecoverDetailMapper.deleteByPrimaryKey(paerdSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return plaAdvanceExpenRecoverDetailMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: save
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：蔡宁波
     */
    public int insertReplyAdv(List<PlaAdvanceExpenRecoverDetail> plaAdvanceExpenRecoverDetailList) {
        int count = 0;
        if (null != plaAdvanceExpenRecoverDetailList) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (null != userInfo) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            for (PlaAdvanceExpenRecoverDetail plaAdvanceExpenRecoverDetails : plaAdvanceExpenRecoverDetailList) {
                PlaAdvanceExpenRecoverDetail recoverDetail = plaAdvanceExpenRecoverDetailMapper.selectByPrimaryKey(plaAdvanceExpenRecoverDetails.getPaerdSerno());
                if (null != recoverDetail) {
                    plaAdvanceExpenRecoverDetails.setUpdDate(openDay);
                    plaAdvanceExpenRecoverDetails.setUpdBrId(inputBrId);
                    plaAdvanceExpenRecoverDetails.setUpdId(inputId);
                    plaAdvanceExpenRecoverDetails.setUpdateTime(createTime);
                    count = plaAdvanceExpenRecoverDetailMapper.updateByPrimaryKey(plaAdvanceExpenRecoverDetails);
                } else {
                    plaAdvanceExpenRecoverDetails.setInputDate(openDay);
                    plaAdvanceExpenRecoverDetails.setInputBrId(inputBrId);
                    plaAdvanceExpenRecoverDetails.setInputId(inputId);
                    plaAdvanceExpenRecoverDetails.setCreateTime(createTime);
                    plaAdvanceExpenRecoverDetails.setUpdDate(openDay);
                    plaAdvanceExpenRecoverDetails.setUpdBrId(inputBrId);
                    plaAdvanceExpenRecoverDetails.setUpdId(inputId);
                    plaAdvanceExpenRecoverDetails.setUpdateTime(createTime);
                    count = plaAdvanceExpenRecoverDetailMapper.insert(plaAdvanceExpenRecoverDetails);
                }
            }
        }
        return count;
    }

    /**
     * 根据条件查询
     *
     * @param queryModel
     * @return
     */
    public List<PlaAdvanceExpenRecoverDetail> selectByCondition(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PlaAdvanceExpenRecoverDetail> list = plaAdvanceExpenRecoverDetailMapper.selectByModel(queryModel);
        PageHelper.clearPage();
        return list;
    }
}