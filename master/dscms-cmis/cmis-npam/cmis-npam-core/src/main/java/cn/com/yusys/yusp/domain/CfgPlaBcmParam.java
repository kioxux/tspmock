/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/*
 * @项目名称: cmis-npam-core模块
 * @类名称: CfgPlaBcmParam
 * @类描述: cfg_pla_bcm_param数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-01 20:09:23
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_pla_bcm_param")
public class CfgPlaBcmParam extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /* 规则参数流水号 */
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "CPBP_SERNO")
    private String cpbpSerno;

    /* 规则编号 */
    @Column(name = "RULE_ID", unique = false, nullable = true, length = 40)
    private String ruleId;

    /* 规则名称 */
    @Column(name = "RULE_NAME", unique = false, nullable = true, length = 40)
    private String ruleName;

    /* 业务条线 */
    @Column(name = "BIZ_LINE", unique = false, nullable = true, length = 5)
    private String bizLine;

    /* 参数代码 */
    @Column(name = "PARAMS_CODE", unique = false, nullable = true, length = 5)
    private String paramsCode;

    /* 操作符号 */
    @Column(name = "OPR_SYMBOL", unique = false, nullable = true, length = 5)
    private String oprSymbol;

    /* 参数值 */
    @Column(name = "PARAM_VALUE", unique = false, nullable = true, length = 10)
    private String paramValue;

    /* 登记人 */
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /* 登记机构 */
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /* 登记日期 */
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /* 最近修改人 */
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /* 最近修改机构 */
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /* 最近修改日期 */
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /* 创建时间 */
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /* 修改时间 */
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    /*
     * 默认构造函数
     */
    public CfgPlaBcmParam() {
        // default implementation ignored
    }

    /*
     * @param cpbpSerno
     */
    public void setCpbpSerno(String cpbpSerno) {
        this.cpbpSerno = cpbpSerno;
    }

    /*
     * @return cpbpSerno
     */
    public String getCpbpSerno() {
        return this.cpbpSerno;
    }

    /*
     * @param ruleId
     */
    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    /*
     * @return ruleId
     */
    public String getRuleId() {
        return this.ruleId;
    }

    /*
     * @param ruleName
     */
    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    /*
     * @return ruleName
     */
    public String getRuleName() {
        return this.ruleName;
    }

    /*
     * @param bizLine
     */
    public void setBizLine(String bizLine) {
        this.bizLine = bizLine;
    }

    /*
     * @return bizLine
     */
    public String getBizLine() {
        return this.bizLine;
    }

    /*
     * @param paramsCode
     */
    public void setParamsCode(String paramsCode) {
        this.paramsCode = paramsCode;
    }

    /*
     * @return paramsCode
     */
    public String getParamsCode() {
        return this.paramsCode;
    }

    /*
     * @param oprSymbol
     */
    public void setOprSymbol(String oprSymbol) {
        this.oprSymbol = oprSymbol;
    }

    /*
     * @return oprSymbol
     */
    public String getOprSymbol() {
        return this.oprSymbol;
    }

    /*
     * @param paramValue
     */
    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    /*
     * @return paramValue
     */
    public String getParamValue() {
        return this.paramValue;
    }

    /*
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /*
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /*
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /*
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /*
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /*
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /*
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /*
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /*
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /*
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /*
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /*
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /*
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /*
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /*
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /*
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }
}