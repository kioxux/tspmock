/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.dto.PlaDebtClaimReducBillRelDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaDebtClaimReducBillRel;
import cn.com.yusys.yusp.service.PlaDebtClaimReducBillRelService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaDebtClaimReducBillRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-06-08 19:44:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pladebtclaimreducbillrel")
public class PlaDebtClaimReducBillRelResource {
    @Autowired
    private PlaDebtClaimReducBillRelService plaDebtClaimReducBillRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaDebtClaimReducBillRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaDebtClaimReducBillRel> list = plaDebtClaimReducBillRelService.selectAll(queryModel);
        return new ResultDto<List<PlaDebtClaimReducBillRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaDebtClaimReducBillRel>> index(QueryModel queryModel) {
        List<PlaDebtClaimReducBillRel> list = plaDebtClaimReducBillRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaDebtClaimReducBillRel>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryAll")
    protected ResultDto<List<PlaDebtClaimReducBillRel>> queryByApproveStatus(@RequestBody QueryModel queryModel) {
        List<PlaDebtClaimReducBillRel> list = plaDebtClaimReducBillRelService.queryByApproveStatus(queryModel);
        return new ResultDto<List<PlaDebtClaimReducBillRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pdcrbrSerno}")
    protected ResultDto<PlaDebtClaimReducBillRel> show(@PathVariable("pdcrbrSerno") String pdcrbrSerno) {
        PlaDebtClaimReducBillRel plaDebtClaimReducBillRel = plaDebtClaimReducBillRelService.selectByPrimaryKey(pdcrbrSerno);
        return new ResultDto<PlaDebtClaimReducBillRel>(plaDebtClaimReducBillRel);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/showByPdcrbrSerno")
    protected ResultDto<PlaDebtClaimReducBillRel> showByPdcrbrSerno(@RequestBody String pdcrbrSerno) {
        PlaDebtClaimReducBillRel plaDebtClaimReducBillRel = plaDebtClaimReducBillRelService.selectByPrimaryKey(pdcrbrSerno);
        return new ResultDto<PlaDebtClaimReducBillRel>(plaDebtClaimReducBillRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaDebtClaimReducBillRel> create(@RequestBody PlaDebtClaimReducBillRel plaDebtClaimReducBillRel) throws URISyntaxException {
        plaDebtClaimReducBillRelService.insert(plaDebtClaimReducBillRel);
        return new ResultDto<PlaDebtClaimReducBillRel>(plaDebtClaimReducBillRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaDebtClaimReducBillRel plaDebtClaimReducBillRel) throws URISyntaxException {
        int result = plaDebtClaimReducBillRelService.update(plaDebtClaimReducBillRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pdcrbrSerno}")
    protected ResultDto<Integer> delete(@PathVariable("pdcrbrSerno") String pdcrbrSerno) {
        int result = plaDebtClaimReducBillRelService.deleteByPrimaryKey(pdcrbrSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaDebtClaimReducBillRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:insert
     * @函数描述: 保存债权减免申请信息表
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insert")
    protected ResultDto<PlaDebtClaimReducBillRel> insert(@RequestBody List<PlaDebtClaimReducBillRel> list) throws URISyntaxException {
        plaDebtClaimReducBillRelService.insertByBillRel(list);
        return new ResultDto<PlaDebtClaimReducBillRel>();
    }

    /**
     * @函数名称:insert
     * @函数描述:  保存到债权减免记账申请信息表
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertByRecordAppInfo")
    protected ResultDto<PlaDebtClaimReducBillRel> insertByRecordAppInfo(@RequestBody PlaDebtClaimReducBillRel plaDebtClaimReducBillRel) throws URISyntaxException {
        plaDebtClaimReducBillRelService.insertByRecordAppInfo(plaDebtClaimReducBillRel);
        return new ResultDto<PlaDebtClaimReducBillRel>(plaDebtClaimReducBillRel);
    }
    /**
     * @函数名称:delete
     * @函数描述: 根据业务流水号删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteByPdcraiSerno/{pdcraiSerno}")
    protected ResultDto<Integer> deleteByPdcraiSerno(@PathVariable("pdcraiSerno") String pdcraiSerno) {
        int result = plaDebtClaimReducBillRelService.deleteByPdcraiSerno(pdcraiSerno);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:delete
     * @函数描述: 根据业务流水号删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteByPdcrbrSerno")
    protected ResultDto<Integer> deleteByPdcrbrSerno(@RequestBody PlaDebtClaimReducBillRel plaDebtClaimReducBillRel) {
        int result = plaDebtClaimReducBillRelService.deleteByPdcrbrSerno(plaDebtClaimReducBillRel);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:show
     * @函数描述:  根据根据业务流水号查询
     * @参数与返回说明:
     * @算法描述:
     * @return
     */
    @PostMapping("/queryByPdcraiSerno")
    protected ResultDto<List<PlaDebtClaimReducBillRel>> queryByPdcraiSerno(@RequestBody QueryModel queryModel) {
        String pdcraiSerno = queryModel.getCondition().get("pdcraiSerno").toString() ;
        List<PlaDebtClaimReducBillRel> list = plaDebtClaimReducBillRelService.queryByPdcraiSerno(pdcraiSerno);
        return new ResultDto<List<PlaDebtClaimReducBillRel>>(list);
    }

    /**
     * 债权减免发送核心记账
     *
     * @author 刘权
     **/
    @ApiOperation("债权减免发送核心记账")
    @PostMapping("/queryHXBill")
    protected ResultDto<PlaDebtClaimReducBillRelDto> queryHXBill(@RequestBody String billNo) {
        ResultDto<PlaDebtClaimReducBillRelDto> plaDebtClaimReducBillRelDto = plaDebtClaimReducBillRelService.queryHXBill(billNo);
        return plaDebtClaimReducBillRelDto;
    }


    /**
     * @函数名称:insertByPlaDebtClaimReducBillRel
     * @函数描述: 减免借据信息保存
     * @参数与返回说明:
     * @算法描述:
     * liuquan
     */
    @PostMapping("/insertByPlaDebtClaimReducBillRel")
    protected ResultDto<PlaDebtClaimReducBillRel> insertByPlaDebtClaimReducBillRel(@RequestBody PlaDebtClaimReducBillRel plaDebtClaimReducBillRel) throws URISyntaxException {
        plaDebtClaimReducBillRelService.insertByPlaDebtClaimReducBillRel(plaDebtClaimReducBillRel);
        return new ResultDto<PlaDebtClaimReducBillRel>(plaDebtClaimReducBillRel);
    }

}
