package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.CfgPlaBcmParam;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @项目名称: cmis-npam-core模块
 * @类名称: CfgPlaBcmRule
 * @类描述: cfg_pla_bcm_rule数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-03 19:52:47
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgPlaBcmRuleDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 催收规则参数配表 **/
	private List<CfgPlaBcmParam> cfgPlaBcmParam;


	public List<CfgPlaBcmParam> getCfgPlaBcmParam() {
		return cfgPlaBcmParam;
	}

	public void setCfgPlaBcmParam(List<CfgPlaBcmParam> cfgPlaBcmParam) {
		this.cfgPlaBcmParam = cfgPlaBcmParam;
	}

	/** 规则编号 **/
	private String ruleId;
	
	/** 规则名称 **/
	private String ruleName;
	
	/** 资产类型 **/
	private String assetType;
	
	/** 任务生成频率 **/
	private String taskFreq;
	
	/** 催收类型 **/
	private String bcmType;
	
	/** 任务要求完成时间（天) **/
	private String finishDay;
	
	/** 催收提醒模板 **/
	private String bcmTemplate;
	
	/** 规则状态 **/
	private String ruleStatus;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private Date createTime;
	
	/** 修改时间 **/
	private Date updateTime;
	
	
	/**
	 * @param ruleId
	 */
	public void setRuleId(String ruleId) {
		this.ruleId = ruleId == null ? null : ruleId.trim();
	}
	
    /**
     * @return RuleId
     */	
	public String getRuleId() {
		return this.ruleId;
	}
	
	/**
	 * @param ruleName
	 */
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName == null ? null : ruleName.trim();
	}
	
    /**
     * @return RuleName
     */	
	public String getRuleName() {
		return this.ruleName;
	}
	
	/**
	 * @param assetType
	 */
	public void setAssetType(String assetType) {
		this.assetType = assetType == null ? null : assetType.trim();
	}
	
    /**
     * @return AssetType
     */	
	public String getAssetType() {
		return this.assetType;
	}
	
	/**
	 * @param taskFreq
	 */
	public void setTaskFreq(String taskFreq) {
		this.taskFreq = taskFreq == null ? null : taskFreq.trim();
	}
	
    /**
     * @return TaskFreq
     */	
	public String getTaskFreq() {
		return this.taskFreq;
	}
	
	/**
	 * @param bcmType
	 */
	public void setBcmType(String bcmType) {
		this.bcmType = bcmType == null ? null : bcmType.trim();
	}
	
    /**
     * @return BcmType
     */	
	public String getBcmType() {
		return this.bcmType;
	}
	
	/**
	 * @param finishDay
	 */
	public void setFinishDay(String finishDay) {
		this.finishDay = finishDay == null ? null : finishDay.trim();
	}
	
    /**
     * @return FinishDay
     */	
	public String getFinishDay() {
		return this.finishDay;
	}
	
	/**
	 * @param bcmTemplate
	 */
	public void setBcmTemplate(String bcmTemplate) {
		this.bcmTemplate = bcmTemplate == null ? null : bcmTemplate.trim();
	}
	
    /**
     * @return BcmTemplate
     */	
	public String getBcmTemplate() {
		return this.bcmTemplate;
	}
	
	/**
	 * @param ruleStatus
	 */
	public void setRuleStatus(String ruleStatus) {
		this.ruleStatus = ruleStatus == null ? null : ruleStatus.trim();
	}
	
    /**
     * @return RuleStatus
     */	
	public String getRuleStatus() {
		return this.ruleStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public Date getUpdateTime() {
		return this.updateTime;
	}


}