/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.text.SimpleDateFormat;
import java.util.*;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.PlaPlanDetail;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.repository.mapper.PlaPlanDetailMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.vo.PlaPlanDetailVo;
import cn.com.yusys.yusp.vo.PlaUncretBadAssetAccListVo;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaUncretBadAssetAccList;
import cn.com.yusys.yusp.repository.mapper.PlaUncretBadAssetAccListMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaUncretBadAssetAccListService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 15:56:25
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaUncretBadAssetAccListService {

    //日志
    private static final Logger log = LoggerFactory.getLogger(PlaTakeoverBillRelService.class);

    @Autowired
    private PlaUncretBadAssetAccListMapper plaUncretBadAssetAccListMapper;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    private SequenceTemplateService sequenceTemplateClient;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PlaUncretBadAssetAccList selectByPrimaryKey(String pubaalSerno) {
        return plaUncretBadAssetAccListMapper.selectByPrimaryKey(pubaalSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PlaUncretBadAssetAccList> selectAll(QueryModel model) {
        List<PlaUncretBadAssetAccList> records = (List<PlaUncretBadAssetAccList>) plaUncretBadAssetAccListMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PlaUncretBadAssetAccList> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaUncretBadAssetAccList> list = plaUncretBadAssetAccListMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PlaUncretBadAssetAccList record) {
        return plaUncretBadAssetAccListMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PlaUncretBadAssetAccList record) {
        return plaUncretBadAssetAccListMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaUncretBadAssetAccList record) {
        return plaUncretBadAssetAccListMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PlaUncretBadAssetAccList record) {
        // 申请人
        String inputId = "";
        // 申请机构
        String inputBrId = "";
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        // 创建时间
        Date createTime = DateUtils.getCurrTimestamp();
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            inputId = userInfo.getLoginCode();
            // 申请机构
            inputBrId = userInfo.getOrg().getCode();
            record.setUpdId(inputId);
            record.setUpdBrId(inputBrId);
            record.setUpdDate(openDay);
            record.setUpdateTime(createTime);
        }
        return plaUncretBadAssetAccListMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pubaalSerno) {
        return plaUncretBadAssetAccListMapper.deleteByPrimaryKey(pubaalSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaUncretBadAssetAccListMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: save
     * @方法描述: 保存
     * @参数与返回说明:
     * @算法描述: 无
     * 刘权
     */

    public int save(PlaUncretBadAssetAccList record) {

        int count = 0;
        if (record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                record.setCreateTime(createTime);
                record.setInputId(inputId);
                record.setInputBrId(inputBrId);
                record.setInputDate(openDay);
                record.setUpdId(inputId);
                record.setUpdBrId(inputBrId);
                record.setUpdDate(openDay);
                record.setUpdateTime(createTime);
                return plaUncretBadAssetAccListMapper.insert(record);
            }
        }
        return count;
    }

    /**
     * 信贷不良资产台账列表Excel导入模板
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportPlaUncretBad() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {

                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(PlaUncretBadAssetAccListVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 批量插入信贷不良资产台账列表
     *
     * @param perPlaUncretBadAssetAccList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertPlaUncretBadAssetAccList(List perPlaUncretBadAssetAccList, String pubaalSerno) throws Exception {
        List<PlaUncretBadAssetAccList> plaUncretBadAssetAccList = (List<PlaUncretBadAssetAccList>) BeanUtils.beansCopy(perPlaUncretBadAssetAccList, PlaUncretBadAssetAccList.class);
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        User userInfo = SessionUtils.getUserInformation();
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            PlaUncretBadAssetAccListMapper plaUncretBadAssetAccListMapper = sqlSession.getMapper(PlaUncretBadAssetAccListMapper.class);
            for (PlaUncretBadAssetAccList plaUncretBadAssetAcc : plaUncretBadAssetAccList) {

                QueryModel query = new QueryModel();
                query.addCondition("cusId", plaUncretBadAssetAcc.getCusId());
                query.addCondition("pubaalSerno", pubaalSerno);
                List<PlaUncretBadAssetAccList> plaUncretBadAssetAccLists = plaUncretBadAssetAccListMapper.selectByModel(query);
                //数据库中已存在，更新信息
                if (CollectionUtils.isNotEmpty(plaUncretBadAssetAccLists)) {
                    PlaUncretBadAssetAccList planUncretBadAssetAccList = plaUncretBadAssetAccLists.get(0);
                    throw BizException.error(null, EcnEnum.ECN060003.key, EcnEnum.ECN060003.value + plaUncretBadAssetAcc.getCusId());
                }//数据库中不存在，新增信息
                else {
                    //生成主键
                    Map paramMap = new HashMap<>();
                    String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, paramMap);
                    plaUncretBadAssetAcc.setPubaalSerno(pkValue);
                    //TODO：STD_INTBANK_TYPE客户类型字典项转换方法待定
                    plaUncretBadAssetAcc.setInputId(userInfo.getLoginCode());
                    plaUncretBadAssetAcc.setInputBrId(userInfo.getOrg().getCode());
                    plaUncretBadAssetAcc.setInputDate(openDay);
                    plaUncretBadAssetAcc.setCreateTime(DateUtils.getCurrTimestamp());
                    //最近修改人
                    plaUncretBadAssetAcc.setUpdId(userInfo.getLoginCode());
                    //最近修改机构
                    plaUncretBadAssetAcc.setUpdBrId(userInfo.getOrg().getCode());
                    //最近修改日期
                    plaUncretBadAssetAcc.setUpdDate(openDay);
                    //创建时间
                    plaUncretBadAssetAcc.setCreateTime(DateUtils.getCurrTimestamp());
                    //修改时间
                    plaUncretBadAssetAcc.setUpdateTime(DateUtils.getCurrTimestamp());

                    plaUncretBadAssetAccListMapper.insertSelective(plaUncretBadAssetAcc);
                }
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }

        return perPlaUncretBadAssetAccList.size();
    }
}
