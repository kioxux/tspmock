/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBadDebtWriteoffAppBatch
 * @类描述: pla_bad_debt_writeoff_app_batch数据实体类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-09 09:34:55
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_bad_debt_writeoff_app_batch")
public class PlaBadDebtWriteoffAppBatch extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 业务流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PBDWAB_SERNO")
    private String pbdwabSerno;

    /**
     * 核销总户数
     **/
    @Column(name = "TOTAL_WRITEOFF_CUS", unique = false, nullable = true, length = 20)
    private String totalWriteoffCus;

    /**
     * 核销总笔数
     **/
    @Column(name = "TOTAL_WRITEOFF_NUM", unique = false, nullable = true, length = 20)
    private String totalWriteoffNum;

    /**
     * 核销总金额
     **/
    @Column(name = "TOTAL_WRITEOFF_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalWriteoffAmt;

    /**
     * 核销总本金
     **/
    @Column(name = "TOTAL_WRITEOFF_CAP", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalWriteoffCap;

    /**
     * 核销总利息
     **/
    @Column(name = "TOTAL_WRITEOFF_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalWriteoffInt;

    /**
     * 核销类型
     **/
    @Column(name = "WRITEOFF_TYPE", unique = false, nullable = true, length = 5)
    private String writeoffType;

    /**
     * 是否保留追诉权
     **/
    @Column(name = "IS_RESERVA_PROSECUTE", unique = false, nullable = true, length = 5)
    private String isReservaProsecute;

    /**
     * 诉讼情况
     **/
    @Column(name = "LAW_CASE", unique = false, nullable = true, length = 5)
    private String lawCase;

    /**
     * 我行贷款（或债权、股权、投资、拆出资金等)发放（生)情况
     **/
    @Column(name = "BANK_LOAN_PAYOUT_CASE", unique = false, nullable = true, length = 2000)
    private String bankLoanPayoutCase;

    /**
     * 形成不良原因
     **/
    @Column(name = "BECOME_BAD_RESN", unique = false, nullable = true, length = 2000)
    private String becomeBadResn;

    /**
     * 贷款（或债权、股权、投资、拆出资金等)追索及企业财产清理情况
     **/
    @Column(name = "LOAN_RGT_ASSET_CASE", unique = false, nullable = true, length = 2000)
    private String loanRgtAssetCase;

    /**
     * 呆账责任认定和追究情况
     **/
    @Column(name = "DOUBTFUL_CASE", unique = false, nullable = true, length = 2000)
    private String doubtfulCase;

    /**
     * 核销理由和结论意见
     **/
    @Column(name = "WRITEOFF_RESN", unique = false, nullable = true, length = 2000)
    private String writeoffResn;

    /**
     * 审批状态
     **/
    @Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
    private String approveStatus;

    /**
     * 备注
     **/
    @Column(name = "MEMO", unique = false, nullable = true, length = 2000)
    private String memo;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaBadDebtWriteoffAppBatch() {
        // default implementation ignored
    }

    /**
     * @param pbdwabSerno
     */
    public void setPbdwabSerno(String pbdwabSerno) {
        this.pbdwabSerno = pbdwabSerno;
    }

    /**
     * @return pbdwabSerno
     */
    public String getPbdwabSerno() {
        return this.pbdwabSerno;
    }

    /**
     * @param totalWriteoffCus
     */
    public void setTotalWriteoffCus(String totalWriteoffCus) {
        this.totalWriteoffCus = totalWriteoffCus;
    }

    /**
     * @return totalWriteoffCus
     */
    public String getTotalWriteoffCus() {
        return this.totalWriteoffCus;
    }

    /**
     * @param totalWriteoffNum
     */
    public void setTotalWriteoffNum(String totalWriteoffNum) {
        this.totalWriteoffNum = totalWriteoffNum;
    }

    /**
     * @return totalWriteoffNum
     */
    public String getTotalWriteoffNum() {
        return this.totalWriteoffNum;
    }

    /**
     * @param totalWriteoffAmt
     */
    public void setTotalWriteoffAmt(java.math.BigDecimal totalWriteoffAmt) {
        this.totalWriteoffAmt = totalWriteoffAmt;
    }

    /**
     * @return totalWriteoffAmt
     */
    public java.math.BigDecimal getTotalWriteoffAmt() {
        return this.totalWriteoffAmt;
    }

    /**
     * @param totalWriteoffCap
     */
    public void setTotalWriteoffCap(java.math.BigDecimal totalWriteoffCap) {
        this.totalWriteoffCap = totalWriteoffCap;
    }

    /**
     * @return totalWriteoffCap
     */
    public java.math.BigDecimal getTotalWriteoffCap() {
        return this.totalWriteoffCap;
    }

    /**
     * @param totalWriteoffInt
     */
    public void setTotalWriteoffInt(java.math.BigDecimal totalWriteoffInt) {
        this.totalWriteoffInt = totalWriteoffInt;
    }

    /**
     * @return totalWriteoffInt
     */
    public java.math.BigDecimal getTotalWriteoffInt() {
        return this.totalWriteoffInt;
    }

    /**
     * @param writeoffType
     */
    public void setWriteoffType(String writeoffType) {
        this.writeoffType = writeoffType;
    }

    /**
     * @return writeoffType
     */
    public String getWriteoffType() {
        return this.writeoffType;
    }

    /**
     * @param isReservaProsecute
     */
    public void setIsReservaProsecute(String isReservaProsecute) {
        this.isReservaProsecute = isReservaProsecute;
    }

    /**
     * @return isReservaProsecute
     */
    public String getIsReservaProsecute() {
        return this.isReservaProsecute;
    }

    /**
     * @param lawCase
     */
    public void setLawCase(String lawCase) {
        this.lawCase = lawCase;
    }

    /**
     * @return lawCase
     */
    public String getLawCase() {
        return this.lawCase;
    }

    /**
     * @param bankLoanPayoutCase
     */
    public void setBankLoanPayoutCase(String bankLoanPayoutCase) {
        this.bankLoanPayoutCase = bankLoanPayoutCase;
    }

    /**
     * @return bankLoanPayoutCase
     */
    public String getBankLoanPayoutCase() {
        return this.bankLoanPayoutCase;
    }

    /**
     * @param becomeBadResn
     */
    public void setBecomeBadResn(String becomeBadResn) {
        this.becomeBadResn = becomeBadResn;
    }

    /**
     * @return becomeBadResn
     */
    public String getBecomeBadResn() {
        return this.becomeBadResn;
    }

    /**
     * @param loanRgtAssetCase
     */
    public void setLoanRgtAssetCase(String loanRgtAssetCase) {
        this.loanRgtAssetCase = loanRgtAssetCase;
    }

    /**
     * @return loanRgtAssetCase
     */
    public String getLoanRgtAssetCase() {
        return this.loanRgtAssetCase;
    }

    /**
     * @param doubtfulCase
     */
    public void setDoubtfulCase(String doubtfulCase) {
        this.doubtfulCase = doubtfulCase;
    }

    /**
     * @return doubtfulCase
     */
    public String getDoubtfulCase() {
        return this.doubtfulCase;
    }

    /**
     * @param writeoffResn
     */
    public void setWriteoffResn(String writeoffResn) {
        this.writeoffResn = writeoffResn;
    }

    /**
     * @return writeoffResn
     */
    public String getWriteoffResn() {
        return this.writeoffResn;
    }

    /**
     * @param approveStatus
     */
    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    /**
     * @return approveStatus
     */
    public String getApproveStatus() {
        return this.approveStatus;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}