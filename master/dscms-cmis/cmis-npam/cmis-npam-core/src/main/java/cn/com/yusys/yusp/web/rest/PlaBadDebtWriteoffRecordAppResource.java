/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.PlaDebtClaimReducRecordAppInfo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaBadDebtWriteoffRecordApp;
import cn.com.yusys.yusp.service.PlaBadDebtWriteoffRecordAppService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBadDebtWriteoffRecordAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-06-09 09:34:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plabaddebtwriteoffrecordapp")
public class PlaBadDebtWriteoffRecordAppResource {
    @Autowired
    private PlaBadDebtWriteoffRecordAppService plaBadDebtWriteoffRecordAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaBadDebtWriteoffRecordApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaBadDebtWriteoffRecordApp> list = plaBadDebtWriteoffRecordAppService.selectAll(queryModel);
        return new ResultDto<List<PlaBadDebtWriteoffRecordApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaBadDebtWriteoffRecordApp>> index(QueryModel queryModel) {
        List<PlaBadDebtWriteoffRecordApp> list = plaBadDebtWriteoffRecordAppService.selectByModel(queryModel);
        return new ResultDto<List<PlaBadDebtWriteoffRecordApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pbdwraSerno}")
    protected ResultDto<PlaBadDebtWriteoffRecordApp> show(@PathVariable("pbdwraSerno") String pbdwraSerno) {
        PlaBadDebtWriteoffRecordApp plaBadDebtWriteoffRecordApp = plaBadDebtWriteoffRecordAppService.selectByPrimaryKey(pbdwraSerno);
        return new ResultDto<PlaBadDebtWriteoffRecordApp>(plaBadDebtWriteoffRecordApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaBadDebtWriteoffRecordApp> create(@RequestBody PlaBadDebtWriteoffRecordApp plaBadDebtWriteoffRecordApp) throws URISyntaxException {
        plaBadDebtWriteoffRecordAppService.insert(plaBadDebtWriteoffRecordApp);
        return new ResultDto<PlaBadDebtWriteoffRecordApp>(plaBadDebtWriteoffRecordApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人周茂伟
     */
    @ApiOperation(value = "修改")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaBadDebtWriteoffRecordApp plaBadDebtWriteoffRecordApp) throws URISyntaxException {
        int result = plaBadDebtWriteoffRecordAppService.update(plaBadDebtWriteoffRecordApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "删除")
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody PlaBadDebtWriteoffRecordApp plaBadDebtWriteoffRecordApp) {
        int result = plaBadDebtWriteoffRecordAppService.deleteByPrimaryKey(plaBadDebtWriteoffRecordApp.getPbdwraSerno());
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaBadDebtWriteoffRecordAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "核销记账申请列表查询")
    @PostMapping("/PlaBadDebtWriteoffRecordApp")
    protected ResultDto<List<PlaBadDebtWriteoffRecordApp>> queryPlaBadDebtWriteoffRecordApp(@RequestBody QueryModel queryModel) {
        List<PlaBadDebtWriteoffRecordApp> list = plaBadDebtWriteoffRecordAppService.selectByModel(queryModel);
        return new ResultDto<List<PlaBadDebtWriteoffRecordApp>>(list);
    }

    /**
     * @函数名称:save
     * @函数描述:保存
     * @参数与返回说明:
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "保存")
    @PostMapping("/save")
    protected ResultDto<PlaBadDebtWriteoffRecordApp> save(@RequestBody PlaBadDebtWriteoffRecordApp plaBadDebtWriteoffRecordApp) throws URISyntaxException {
        plaBadDebtWriteoffRecordAppService.save(plaBadDebtWriteoffRecordApp);
        return new ResultDto<PlaBadDebtWriteoffRecordApp>(plaBadDebtWriteoffRecordApp);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "详细信息查询")
    @PostMapping("/queryPlaBadDebtWriteoffRecordAppInfo")
    protected ResultDto<PlaBadDebtWriteoffRecordApp> queryPlaBadDebtWriteoffRecordAppInfo(@RequestBody PlaBadDebtWriteoffRecordApp plaBadDebtWriteoffRecordApp) {
        PlaBadDebtWriteoffRecordApp plaBadDebtWriteoffRecordApps = plaBadDebtWriteoffRecordAppService.selectByPrimaryKey(plaBadDebtWriteoffRecordApp.getPbdwraSerno());
        return new ResultDto<PlaBadDebtWriteoffRecordApp>(plaBadDebtWriteoffRecordApps);
    }

    /**
     * 呆账核销发送核销记账
     *
     * @author 刘权
     **/
    @ApiOperation("呆账核销发送核销记账")
    @PostMapping("/sendtohxjz")
    protected ResultDto<String> sendToHXJZ(@RequestBody PlaBadDebtWriteoffRecordApp appInfo) throws Exception {
        return plaBadDebtWriteoffRecordAppService.sendToHXJZ(appInfo);
    }

    /**
     * 呆账核销发送同步核销记账
     *
     * @author 刘权
     **/
    @ApiOperation("呆账核销发送同步核销记账")
    @PostMapping("/sendtotbxhjz")
    protected ResultDto<String> sendToTBHXJZ(@RequestBody PlaBadDebtWriteoffRecordApp appInfo) throws Exception {
        return plaBadDebtWriteoffRecordAppService.sendToTBHXJZ(appInfo);
    }

    /**
     * 呆账核销发送核销记账
     *
     * @author 周茂伟
     **/
    @ApiOperation("呆账核销发送核销记账-单笔")
    @PostMapping("/sendToHXRecord")
    protected ResultDto<String> sendToHXRecord(@RequestBody PlaBadDebtWriteoffRecordApp appInfo) throws Exception {
        return plaBadDebtWriteoffRecordAppService.sendToHXRecord(appInfo);
    }
}
