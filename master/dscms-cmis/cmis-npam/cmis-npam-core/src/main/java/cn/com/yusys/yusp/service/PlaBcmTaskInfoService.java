/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisNpamConstants;
import cn.com.yusys.yusp.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.PlaBcmTaskInfoMapper;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBcmTaskInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-03 16:26:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaBcmTaskInfoService {

    @Autowired
    private PlaBcmTaskInfoMapper plaBcmTaskInfoMapper;

    @Autowired
    private PlaBcmReceiptInfoService plaBcmReceiptInfoService;

    @Autowired
    private PlaBcmAssignTaskService plaBcmAssignTaskService;

	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PlaBcmTaskInfo selectByPrimaryKey(String taskNo) {
        return plaBcmTaskInfoMapper.selectByPrimaryKey(taskNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaBcmTaskInfo> selectAll(QueryModel model) {
        List<PlaBcmTaskInfo> records = (List<PlaBcmTaskInfo>) plaBcmTaskInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PlaBcmTaskInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaBcmTaskInfo> list = plaBcmTaskInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PlaBcmTaskInfo record) {
        int count = 0 ;
        if (record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();

                record.setInputId(inputId);
                record.setInputBrId(inputBrId);
                record.setInputDate(inputDate);
                record.setCreateTime(createTime);
                record.setUpdId(inputId);
                record.setUpdBrId(inputBrId);
                record.setUpdDate(inputDate);
                record.setUpdateTime(createTime);
                return plaBcmTaskInfoMapper.insert(record);
            }
        }
        return count;
    }


    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PlaBcmTaskInfo record) {
        return plaBcmTaskInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PlaBcmTaskInfo record) {
        int count = 0 ;
        if (record != null){
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if(userInfo != null){
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();
            }
            //获取当前催收回执信息表里任务编号相同的数据
            PlaBcmReceiptInfo plaBcmReceiptInfo = plaBcmReceiptInfoService.selectByTaskNo(record.getTaskNo());
            if (plaBcmReceiptInfo == null){
                throw BizException.error(null,"9999", "催收结果登记表不能为空");

            }else {
                //更改催收状态为已完成
                record.setBcmStatus(CmisNpamConstants.BCM_STATUS_01);
                record.setInputId(inputId);
                record.setInputBrId(inputBrId);
                record.setInputDate(inputDate);
                record.setCreateTime(createTime);
                record.setUpdId(inputId);
                record.setUpdBrId(inputBrId);
                record.setUpdDate(inputDate);
                record.setUpdateTime(createTime);
                count=plaBcmTaskInfoMapper.updateByPrimaryKey(record);
            }
        }
        return count;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PlaBcmTaskInfo record) {
        return plaBcmTaskInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String taskNo) {
        return plaBcmTaskInfoMapper.deleteByPrimaryKey(taskNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaBcmTaskInfoMapper.deleteByIds(ids);
    }

    /**
     * @函数名称:selectNumByInputId
     * @函数描述:根据客户经理工号查询待处理数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    public List<Map<String, Object>> selectNumByInputId(QueryModel queryModel) {
        return plaBcmTaskInfoMapper.selectNumByInputId(queryModel);
    }
}
