/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLoanRat
 * @类描述: pla_loan_rat数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-10 11:13:24
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_loan_rat")
public class PlaLoanRat extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 试算流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PLR_SERNO")
    private String plrSerno;

    /**
     * 合同编号
     **/
    @Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
    private String contNo;

    /**
     * 借据编号
     **/
    @Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
    private String billNo;

    /**
     * 客户编号
     **/
    @Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
    private String cusId;

    /**
     * 客户名称
     **/
    @Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
    private String cusName;

    /**
     * 产品名称
     **/
    @Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
    private String prdName;

    /**
     * 贷款金额
     **/
    @Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal loanAmt;

    /**
     * 贷款余额
     **/
    @Column(name = "LOAN_BALANCE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal loanBalance;

    /**
     * 执行年利率
     **/
    @Column(name = "EXEC_RATE_YEAR", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal execRateYear;

    /**
     * 起息时间
     **/
    @Column(name = "LOAN_RAT_START_DARE", unique = false, nullable = true, length = 20)
    private String loanRatStartDare;

    /**
     * 计息时间
     **/
    @Column(name = "LOAN_RAT_DARE", unique = false, nullable = true, length = 20)
    private String loanRatDare;

    /**
     * 计息天数
     **/
    @Column(name = "LOAN_RAT_DAY", unique = false, nullable = true, length = 20)
    private String loanRatDay;

    /**
     * 利息
     **/
    @Column(name = "INT_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal intAmt;

    /**
     * 罚息
     **/
    @Column(name = "PENAL_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal penalInt;

    /**
     * 复利
     **/
    @Column(name = "COMPOUND_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal compoundInt;

    /**
     * 应还利息
     **/
    @Column(name = "DUETO_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal duetoInt;

    /**
     * 已还利息
     **/
    @Column(name = "PAID_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal paidInt;

    /**
     * 欠息金额
     **/
    @Column(name = "DEBIT_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal debitInt;

    /**
     * 正常利率
     **/
    @Column(name = "NORMAL_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal normalRate;

    /**
     * 罚息利率
     **/
    @Column(name = "PENAL_INT_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal penalIntRate;

    /**
     * 复利利率
     **/
    @Column(name = "COMPOUND_INT_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal compoundIntRate;

    /**
     * 逾期时间
     **/
    @Column(name = "OVERDUE_DAY", unique = false, nullable = true, length = 20)
    private String overdueDay;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaLoanRat() {
        // default implementation ignored
    }

    /**
     * @param plrSerno
     */
    public void setPlrSerno(String plrSerno) {
        this.plrSerno = plrSerno;
    }

    /**
     * @return plrSerno
     */
    public String getPlrSerno() {
        return this.plrSerno;
    }

    /**
     * @param contNo
     */
    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    /**
     * @return contNo
     */
    public String getContNo() {
        return this.contNo;
    }

    /**
     * @param billNo
     */
    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    /**
     * @return billNo
     */
    public String getBillNo() {
        return this.billNo;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param prdName
     */
    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    /**
     * @return prdName
     */
    public String getPrdName() {
        return this.prdName;
    }

    /**
     * @param loanAmt
     */
    public void setLoanAmt(java.math.BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    /**
     * @return loanAmt
     */
    public java.math.BigDecimal getLoanAmt() {
        return this.loanAmt;
    }

    /**
     * @param loanBalance
     */
    public void setLoanBalance(java.math.BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    /**
     * @return loanBalance
     */
    public java.math.BigDecimal getLoanBalance() {
        return this.loanBalance;
    }

    /**
     * @param execRateYear
     */
    public void setExecRateYear(java.math.BigDecimal execRateYear) {
        this.execRateYear = execRateYear;
    }

    /**
     * @return execRateYear
     */
    public java.math.BigDecimal getExecRateYear() {
        return this.execRateYear;
    }

    /**
     * @param loanRatStartDare
     */
    public void setLoanRatStartDare(String loanRatStartDare) {
        this.loanRatStartDare = loanRatStartDare;
    }

    /**
     * @return loanRatStartDare
     */
    public String getLoanRatStartDare() {
        return this.loanRatStartDare;
    }

    /**
     * @param loanRatDare
     */
    public void setLoanRatDare(String loanRatDare) {
        this.loanRatDare = loanRatDare;
    }

    /**
     * @return loanRatDare
     */
    public String getLoanRatDare() {
        return this.loanRatDare;
    }

    /**
     * @param loanRatDay
     */
    public void setLoanRatDay(String loanRatDay) {
        this.loanRatDay = loanRatDay;
    }

    /**
     * @return loanRatDay
     */
    public String getLoanRatDay() {
        return this.loanRatDay;
    }

    /**
     * @param intAmt
     */
    public void setIntAmt(java.math.BigDecimal intAmt) {
        this.intAmt = intAmt;
    }

    /**
     * @return intAmt
     */
    public java.math.BigDecimal getIntAmt() {
        return this.intAmt;
    }

    /**
     * @param penalInt
     */
    public void setPenalInt(java.math.BigDecimal penalInt) {
        this.penalInt = penalInt;
    }

    /**
     * @return penalInt
     */
    public java.math.BigDecimal getPenalInt() {
        return this.penalInt;
    }

    /**
     * @param compoundInt
     */
    public void setCompoundInt(java.math.BigDecimal compoundInt) {
        this.compoundInt = compoundInt;
    }

    /**
     * @return compoundInt
     */
    public java.math.BigDecimal getCompoundInt() {
        return this.compoundInt;
    }

    /**
     * @param duetoInt
     */
    public void setDuetoInt(java.math.BigDecimal duetoInt) {
        this.duetoInt = duetoInt;
    }

    /**
     * @return duetoInt
     */
    public java.math.BigDecimal getDuetoInt() {
        return this.duetoInt;
    }

    /**
     * @param paidInt
     */
    public void setPaidInt(java.math.BigDecimal paidInt) {
        this.paidInt = paidInt;
    }

    /**
     * @return paidInt
     */
    public java.math.BigDecimal getPaidInt() {
        return this.paidInt;
    }

    /**
     * @param debitInt
     */
    public void setDebitInt(java.math.BigDecimal debitInt) {
        this.debitInt = debitInt;
    }

    /**
     * @return debitInt
     */
    public java.math.BigDecimal getDebitInt() {
        return this.debitInt;
    }

    /**
     * @param normalRate
     */
    public void setNormalRate(java.math.BigDecimal normalRate) {
        this.normalRate = normalRate;
    }

    /**
     * @return normalRate
     */
    public java.math.BigDecimal getNormalRate() {
        return this.normalRate;
    }

    /**
     * @param penalIntRate
     */
    public void setPenalIntRate(java.math.BigDecimal penalIntRate) {
        this.penalIntRate = penalIntRate;
    }

    /**
     * @return penalIntRate
     */
    public java.math.BigDecimal getPenalIntRate() {
        return this.penalIntRate;
    }

    /**
     * @param compoundIntRate
     */
    public void setCompoundIntRate(java.math.BigDecimal compoundIntRate) {
        this.compoundIntRate = compoundIntRate;
    }

    /**
     * @return compoundIntRate
     */
    public java.math.BigDecimal getCompoundIntRate() {
        return this.compoundIntRate;
    }

    /**
     * @param overdueDay
     */
    public void setOverdueDay(String overdueDay) {
        this.overdueDay = overdueDay;
    }

    /**
     * @return overdueDay
     */
    public String getOverdueDay() {
        return this.overdueDay;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}