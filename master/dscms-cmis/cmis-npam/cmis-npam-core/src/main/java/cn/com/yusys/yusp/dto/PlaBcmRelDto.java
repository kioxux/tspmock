package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBcmRel
 * @类描述: pla_bcm_rel数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-04 11:12:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PlaBcmRelDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 催收业务流水号 **/
	private String pbrcSerno;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 正常本金 **/
	private java.math.BigDecimal zcbjAmt;
	
	/** 逾期本金 **/
	private java.math.BigDecimal overdueCapAmt;
	
	/** 拖欠利息总额 **/
	private java.math.BigDecimal totalTqlxAmt;
	
	/** 欠息 **/
	private java.math.BigDecimal debitInt;
	
	/** 罚息 **/
	private java.math.BigDecimal penalInt;
	
	/** 复息 **/
	private java.math.BigDecimal compoundInt;
	
	/** 逾期天数 **/
	private String overdueDay;
	
	/** 五级分类 **/
	private String fiveClass;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pbrcSerno
	 */
	public void setPbrcSerno(String pbrcSerno) {
		this.pbrcSerno = pbrcSerno == null ? null : pbrcSerno.trim();
	}
	
    /**
     * @return PbrcSerno
     */	
	public String getPbrcSerno() {
		return this.pbrcSerno;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param zcbjAmt
	 */
	public void setZcbjAmt(java.math.BigDecimal zcbjAmt) {
		this.zcbjAmt = zcbjAmt;
	}
	
    /**
     * @return ZcbjAmt
     */	
	public java.math.BigDecimal getZcbjAmt() {
		return this.zcbjAmt;
	}
	
	/**
	 * @param overdueCapAmt
	 */
	public void setOverdueCapAmt(java.math.BigDecimal overdueCapAmt) {
		this.overdueCapAmt = overdueCapAmt;
	}
	
    /**
     * @return OverdueCapAmt
     */	
	public java.math.BigDecimal getOverdueCapAmt() {
		return this.overdueCapAmt;
	}
	
	/**
	 * @param totalTqlxAmt
	 */
	public void setTotalTqlxAmt(java.math.BigDecimal totalTqlxAmt) {
		this.totalTqlxAmt = totalTqlxAmt;
	}
	
    /**
     * @return TotalTqlxAmt
     */	
	public java.math.BigDecimal getTotalTqlxAmt() {
		return this.totalTqlxAmt;
	}
	
	/**
	 * @param debitInt
	 */
	public void setDebitInt(java.math.BigDecimal debitInt) {
		this.debitInt = debitInt;
	}
	
    /**
     * @return DebitInt
     */	
	public java.math.BigDecimal getDebitInt() {
		return this.debitInt;
	}
	
	/**
	 * @param penalInt
	 */
	public void setPenalInt(java.math.BigDecimal penalInt) {
		this.penalInt = penalInt;
	}
	
    /**
     * @return PenalInt
     */	
	public java.math.BigDecimal getPenalInt() {
		return this.penalInt;
	}
	
	/**
	 * @param compoundInt
	 */
	public void setCompoundInt(java.math.BigDecimal compoundInt) {
		this.compoundInt = compoundInt;
	}
	
    /**
     * @return CompoundInt
     */	
	public java.math.BigDecimal getCompoundInt() {
		return this.compoundInt;
	}
	
	/**
	 * @param overdueDay
	 */
	public void setOverdueDay(String overdueDay) {
		this.overdueDay = overdueDay == null ? null : overdueDay.trim();
	}
	
    /**
     * @return OverdueDay
     */	
	public String getOverdueDay() {
		return this.overdueDay;
	}
	
	/**
	 * @param fiveClass
	 */
	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass == null ? null : fiveClass.trim();
	}
	
    /**
     * @return FiveClass
     */	
	public String getFiveClass() {
		return this.fiveClass;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}