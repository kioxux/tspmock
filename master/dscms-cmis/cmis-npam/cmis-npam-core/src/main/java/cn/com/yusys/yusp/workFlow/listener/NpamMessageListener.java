package cn.com.yusys.yusp.workFlow.listener;


import cn.com.yusys.yusp.flow.client.ClientBizFactory;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @className NpamMessageListener
 * @Description 流程mq监听处理
 * @Date 2021/6/12  14:28:43
 */
@Service
public class NpamMessageListener {
    private final Logger log = LoggerFactory.getLogger(NpamMessageListener.class);

    @Autowired
    private ClientBizFactory clientBizFactory;

    @RabbitListener(queuesToDeclare = {
            @Queue("yusp-flow.ZCBQ01"),// 司法诉讼申请审批流程（非小微）
            @Queue("yusp-flow.ZCBQ02"),// 司法诉讼申请审批流程（小微）
            @Queue("yusp-flow.ZCBQ03"),// 核销申请审批流程（对公及个人经营性贷款）
            @Queue("yusp-flow.ZCBQ04"),// 核销申请审批流程（小微）
            @Queue("yusp-flow.ZCBQ05"),// 核销申请审批流程（消费类贷款）
            @Queue("yusp-flow.ZCBQ06"),// 核销申请审批流程（网金）
            @Queue("yusp-flow.ZCBQ07"),// 核销申请审批流程（信用卡透支贷款）
            @Queue("yusp-flow.ZCBQ08"),// 核销记账审批流程
            @Queue("yusp-flow.ZCBQ09"),// 债权减免申请（非小微）
            @Queue("yusp-flow.ZCBQ10"),// 债权减免申请（小微）
            @Queue("yusp-flow.ZCBQ11"),// 债权减免记账
            @Queue("yusp-flow.ZCBQ12"),// 以物抵债登记审批流程（非小微）
            @Queue("yusp-flow.ZCBQ13"),// 以物抵债登记审批流程（小微）
            @Queue("yusp-flow.ZCBQ14"),// 抵债资产处置审批流程（非小微）
            @Queue("yusp-flow.ZCBQ15"),// 抵债资产处置审批流程（小微）
            @Queue("yusp-flow.SGCZ26"),// 司法诉讼申请审批流程（寿光）
            @Queue("yusp-flow.SGCZ27"),// 核销申请审批流程（寿光）
            @Queue("yusp-flow.SGCZ28"),// 核销记账审批流程（寿光）
            @Queue("yusp-flow.DHCZ26"),// 司法诉讼申请审批流程（东海）
            @Queue("yusp-flow.DHCZ27"),// 核销申请审批流程（东海）
            @Queue("yusp-flow.DHCZ28"),// 核销记账审批流程（东海）
    })
    @RabbitHandler
    public void receiveQueue(ResultInstanceDto message) {
        log.info("资产保全业务监听:"+message);
        clientBizFactory.processBiz(message);

    }
}
