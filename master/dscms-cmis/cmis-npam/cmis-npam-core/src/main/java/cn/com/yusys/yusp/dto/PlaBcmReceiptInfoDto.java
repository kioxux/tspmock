package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBcmReceiptInfo
 * @类描述: pla_bcm_receipt_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-04 11:28:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PlaBcmReceiptInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 催收回执流水号 **/
	private String pbreiSerno;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 催收方式 **/
	private String bcmMode;
	
	/** 催收日期 **/
	private String bcmDate;
	
	/** 催收对象类型 **/
	private String bcmObjType;
	
	/** 催收对象名称 **/
	private String cusName;
	
	/** 联系方式 **/
	private String tel;
	
	/** 催收结果 **/
	private String bcmResult;
	
	/** 催收情况说明 **/
	private String bcmCaseDesc;
	
	/** 是否回执 **/
	private String isRcp;
	
	/** 回执日期 **/
	private String rcpDate;
	
	/** 回执内容 **/
	private String rcpContent;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pbreiSerno
	 */
	public void setPbreiSerno(String pbreiSerno) {
		this.pbreiSerno = pbreiSerno == null ? null : pbreiSerno.trim();
	}
	
    /**
     * @return PbreiSerno
     */	
	public String getPbreiSerno() {
		return this.pbreiSerno;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param bcmMode
	 */
	public void setBcmMode(String bcmMode) {
		this.bcmMode = bcmMode == null ? null : bcmMode.trim();
	}
	
    /**
     * @return BcmMode
     */	
	public String getBcmMode() {
		return this.bcmMode;
	}
	
	/**
	 * @param bcmDate
	 */
	public void setBcmDate(String bcmDate) {
		this.bcmDate = bcmDate == null ? null : bcmDate.trim();
	}
	
    /**
     * @return BcmDate
     */	
	public String getBcmDate() {
		return this.bcmDate;
	}
	
	/**
	 * @param bcmObjType
	 */
	public void setBcmObjType(String bcmObjType) {
		this.bcmObjType = bcmObjType == null ? null : bcmObjType.trim();
	}
	
    /**
     * @return BcmObjType
     */	
	public String getBcmObjType() {
		return this.bcmObjType;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param tel
	 */
	public void setTel(String tel) {
		this.tel = tel == null ? null : tel.trim();
	}
	
    /**
     * @return Tel
     */	
	public String getTel() {
		return this.tel;
	}
	
	/**
	 * @param bcmResult
	 */
	public void setBcmResult(String bcmResult) {
		this.bcmResult = bcmResult == null ? null : bcmResult.trim();
	}
	
    /**
     * @return BcmResult
     */	
	public String getBcmResult() {
		return this.bcmResult;
	}
	
	/**
	 * @param bcmCaseDesc
	 */
	public void setBcmCaseDesc(String bcmCaseDesc) {
		this.bcmCaseDesc = bcmCaseDesc == null ? null : bcmCaseDesc.trim();
	}
	
    /**
     * @return BcmCaseDesc
     */	
	public String getBcmCaseDesc() {
		return this.bcmCaseDesc;
	}
	
	/**
	 * @param isRcp
	 */
	public void setIsRcp(String isRcp) {
		this.isRcp = isRcp == null ? null : isRcp.trim();
	}
	
    /**
     * @return IsRcp
     */	
	public String getIsRcp() {
		return this.isRcp;
	}
	
	/**
	 * @param rcpDate
	 */
	public void setRcpDate(String rcpDate) {
		this.rcpDate = rcpDate == null ? null : rcpDate.trim();
	}
	
    /**
     * @return RcpDate
     */	
	public String getRcpDate() {
		return this.rcpDate;
	}
	
	/**
	 * @param rcpContent
	 */
	public void setRcpContent(String rcpContent) {
		this.rcpContent = rcpContent == null ? null : rcpContent.trim();
	}
	
    /**
     * @return RcpContent
     */	
	public String getRcpContent() {
		return this.rcpContent;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}