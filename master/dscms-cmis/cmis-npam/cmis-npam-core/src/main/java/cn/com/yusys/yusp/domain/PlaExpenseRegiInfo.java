/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaExpenseRegiInfo
 * @类描述: pla_expense_regi_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-10 10:41:24
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_expense_regi_info")
public class PlaExpenseRegiInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 费用登记流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PERI_SERNO")
    private String periSerno;

    /**
     * 处置阶段
     **/
    @Column(name = "DISP_STAGE", unique = false, nullable = true, length = 5)
    private String dispStage;

    /**
     * 客户编号
     **/
    @Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
    private String cusId;

    /**
     * 客户名称
     **/
    @Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
    private String cusName;

    /**
     * 关联业务流水号
     **/
    @Column(name = "SERNO", unique = false, nullable = true, length = 40)
    private String serno;

    /**
     * 费用总额
     **/
    @Column(name = "EXPENSE_TOTAL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal expenseTotalAmt;

    /**
     * 垫支总额
     **/
    @Column(name = "ADVANCE_EXPEN_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal advanceExpenAmt;

    /**
     * 列支总额
     **/
    @Column(name = "RANK_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal rankAmt;

    /**
     * 备注
     **/
    @Column(name = "MEMO", unique = false, nullable = true, length = 2000)
    private String memo;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaExpenseRegiInfo() {
        // default implementation ignored
    }

    /**
     * @param periSerno
     */
    public void setPeriSerno(String periSerno) {
        this.periSerno = periSerno;
    }

    /**
     * @return periSerno
     */
    public String getPeriSerno() {
        return this.periSerno;
    }

    /**
     * @param dispStage
     */
    public void setDispStage(String dispStage) {
        this.dispStage = dispStage;
    }

    /**
     * @return dispStage
     */
    public String getDispStage() {
        return this.dispStage;
    }

    /**
     * @param serno
     */
    public void setSerno(String serno) {
        this.serno = serno;
    }

    /**
     * @return serno
     */
    public String getSerno() {
        return this.serno;
    }

    /**
     * @param expenseTotalAmt
     */
    public void setExpenseTotalAmt(java.math.BigDecimal expenseTotalAmt) {
        this.expenseTotalAmt = expenseTotalAmt;
    }

    /**
     * @return expenseTotalAmt
     */
    public java.math.BigDecimal getExpenseTotalAmt() {
        return this.expenseTotalAmt;
    }

    /**
     * @param advanceExpenAmt
     */
    public void setAdvanceExpenAmt(java.math.BigDecimal advanceExpenAmt) {
        this.advanceExpenAmt = advanceExpenAmt;
    }

    /**
     * @return advanceExpenAmt
     */
    public java.math.BigDecimal getAdvanceExpenAmt() {
        return this.advanceExpenAmt;
    }

    /**
     * @param rankAmt
     */
    public void setRankAmt(java.math.BigDecimal rankAmt) {
        this.rankAmt = rankAmt;
    }

    /**
     * @return rankAmt
     */
    public java.math.BigDecimal getRankAmt() {
        return this.rankAmt;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }
}