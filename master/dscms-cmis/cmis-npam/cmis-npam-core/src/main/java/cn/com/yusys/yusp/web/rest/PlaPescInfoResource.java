/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.PlaPescInfo;
import cn.com.yusys.yusp.service.PlaPescInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaPescInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:52:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plapescinfo")
public class PlaPescInfoResource {
    @Autowired
    private PlaPescInfoService plaPescInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaPescInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaPescInfo> list = plaPescInfoService.selectAll(queryModel);
        return new ResultDto<List<PlaPescInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaPescInfo>> index(QueryModel queryModel) {
        List<PlaPescInfo> list = plaPescInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaPescInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{ppiSerno}")
    protected ResultDto<PlaPescInfo> show(@PathVariable("ppiSerno") String ppiSerno) {
        PlaPescInfo plaPescInfo = plaPescInfoService.selectByPrimaryKey(ppiSerno);
        return new ResultDto<PlaPescInfo>(plaPescInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaPescInfo> create(@RequestBody PlaPescInfo plaPescInfo) throws URISyntaxException {
        plaPescInfoService.insert(plaPescInfo);
        return new ResultDto<PlaPescInfo>(plaPescInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaPescInfo plaPescInfo) throws URISyntaxException {
        int result = plaPescInfoService.update(plaPescInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{ppiSerno}")
    protected ResultDto<Integer> delete(@PathVariable("ppiSerno") String ppiSerno) {
        int result = plaPescInfoService.deleteByPrimaryKey(ppiSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaPescInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据条件查询.
     *
     * @return
     */
    @PostMapping("/selectByCondition")
    protected ResultDto<List<PlaPescInfo>> selectByCondition(@RequestBody  QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())){
            queryModel.setSort(" ppi_serno desc");
        }
        List<PlaPescInfo> list = plaPescInfoService.selectByModel(queryModel);
        return new ResultDto<List<PlaPescInfo>>(list);
    }

    /**
     * @函数名称: insertReplyPescs
     * @函数描述:批量对象新增
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertReplyPescs")
    protected ResultDto<Integer> insertReplyPesc(@RequestBody List<PlaPescInfo> plaPescInfoList) throws URISyntaxException {
        int result =plaPescInfoService.insertReplyPescs(plaPescInfoList);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateReplyPesc
     * @函数描述:对象修改
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateReplyPesc")
    protected ResultDto<Integer> updateReplyPesc(@RequestBody PlaPescInfo plaPescInfo) throws URISyntaxException {
        int result = plaPescInfoService.update(plaPescInfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deletePescInfo
     * @函数描述:单个对象删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deletePescInfo/{ppiSerno}")
    protected ResultDto<Integer> deletePescInfo(@PathVariable("ppiSerno") String ppiSerno) {
        int result = plaPescInfoService.deleteByPrimaryKey(ppiSerno);
        return new ResultDto<Integer>(result);
    }



}
