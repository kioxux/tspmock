/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaBartPldDebtBillRel;
import cn.com.yusys.yusp.service.PlaBartPldDebtBillRelService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBartPldDebtBillRelResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plabartplddebtbillrel")
public class PlaBartPldDebtBillRelResource {
    @Autowired
    private PlaBartPldDebtBillRelService plaBartPldDebtBillRelService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaBartPldDebtBillRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaBartPldDebtBillRel> list = plaBartPldDebtBillRelService.selectAll(queryModel);
        return new ResultDto<List<PlaBartPldDebtBillRel>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryinfo")
    protected ResultDto<List<PlaBartPldDebtBillRel>> queryInfo(QueryModel queryModel) {
        List<PlaBartPldDebtBillRel> list = plaBartPldDebtBillRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaBartPldDebtBillRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{ptbrSerno}")
    protected ResultDto<PlaBartPldDebtBillRel> show(@PathVariable("ptbrSerno") String ptbrSerno) {
        PlaBartPldDebtBillRel plaBartPldDebtBillRel = plaBartPldDebtBillRelService.selectByPrimaryKey(ptbrSerno);
        return new ResultDto<PlaBartPldDebtBillRel>(plaBartPldDebtBillRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<PlaBartPldDebtBillRel> create(@RequestBody PlaBartPldDebtBillRel plaBartPldDebtBillRel) throws URISyntaxException {
        plaBartPldDebtBillRelService.insert(plaBartPldDebtBillRel);
        return new ResultDto<PlaBartPldDebtBillRel>(plaBartPldDebtBillRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaBartPldDebtBillRel plaBartPldDebtBillRel) throws URISyntaxException {
        int result = plaBartPldDebtBillRelService.update(plaBartPldDebtBillRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody PlaBartPldDebtBillRel plaBartPldDebtBillRel) {
        int result = plaBartPldDebtBillRelService.deleteByPrimaryKey(plaBartPldDebtBillRel);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaBartPldDebtBillRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据以物抵债申请流水获取关联借据信息
     *
     * @author jijian_yx
     * @date 2021/6/10 15:35
     **/
    @ApiOperation("根据以物抵债申请流水获取关联借据信息")
    @PostMapping("/querybyptaiserno")
    protected ResultDto<List<PlaBartPldDebtBillRel>> queryByPtaiSerno(@RequestParam("pdraiSerno") String pdraiSerno) {
        List<PlaBartPldDebtBillRel> list = plaBartPldDebtBillRelService.queryByPtaiSerno(pdraiSerno);
        return new ResultDto<List<PlaBartPldDebtBillRel>>(list);
    }

    /**
     * 以物抵债申请 保存/更新关联借据信息
     *
     * @author jijian_yx
     * @date 2021/6/9 20:06
     **/
    @ApiOperation("保存/更新关联借据信息")
    @PostMapping("/save")
    protected ResultDto<String> save(@RequestBody List<PlaBartPldDebtBillRel> billRelList) {
        return plaBartPldDebtBillRelService.save(billRelList);
    }
    /**
     * 以物抵债申请 借据引入保存
     *
     * @author jijian_yx
     * @date 2021/6/9 20:06
     **/
    @ApiOperation("借据引入保存")
    @PostMapping("/savedebt")
    protected ResultDto<String> saveDebt(@RequestBody List<PlaBartPldDebtBillRel> billRelList) throws ParseException {
        return plaBartPldDebtBillRelService.saveData(billRelList);
    }



    /**
     * 根据业务流水号获取关联借据信息
     *
     * @author 刘权
     **/
    @ApiOperation("根据业务流水号获取关联借据信息")
    @PostMapping("/queryByPdraiSerno")
    protected ResultDto<List<PlaBartPldDebtBillRel>> queryByPdraiSerno(@RequestBody QueryModel model) {
        List<PlaBartPldDebtBillRel> list = plaBartPldDebtBillRelService.queryByPdraiSerno(model);
        return new ResultDto<List<PlaBartPldDebtBillRel>>(list);
    }

}
