/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaDebtClaimReducRecordBillRel
 * @类描述: pla_debt_claim_reduc_record_bill_rel数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-24 10:08:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class PlaDebtClaimReducRecordBillRelDto extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 关联表流水号
     **/
    private String pdcrrbrSerno;

    /**
     * 记账申请流水号
     */
    private String pdcrraiSerno;

    /**
     * 减免关联流水号
     **/

    private String pdcraiSerno;

    /**
     * 合同编号
     **/
    private String contNo;

    /**
     * 借据编号
     **/
    private String billNo;

    /**
     * 客户编号
     **/
    private String cusId;

    /**
     * 客户名称
     **/
    private String cusName;

    /**
     * 产品名称
     **/
    private String prdName;

    /**
     * 币种
     **/
    private String curType;

    /**
     * 贷款金额
     **/
    private BigDecimal loanAmt;

    /**
     * 贷款余额
     **/
    private BigDecimal loanBalance;

    /**
     * 拖欠利息总额
     **/
    private BigDecimal totalTqlxAmt;

    /**
     * 贷款起始日
     **/
    private String loanStartDate;

    /**
     * 贷款到期日
     **/
    private String loanEndDate;

    /**
     * 执行年利率
     **/
    private BigDecimal execRateYear;

    /**
     * 五级分类
     **/
    private String fiveClass;

    /**
     * 担保方式
     **/
    private String guarMode;

    /**
     * 减免本金
     **/
    private BigDecimal reducCapAmt;

    /**
     * 减免欠息
     **/
    private BigDecimal reducDebitInt;

    /**
     * 减免罚息
     **/
    private BigDecimal reducPenalInt;

    /**
     * 减免复息
     **/
    private BigDecimal reducCompoundInt;

    /**
     * 减免费用
     **/
    private BigDecimal reducCostAmt;

    /**
     * 应收应记利息
     **/
    private BigDecimal recRemInt;

    /**
     * 催收应计利息
     **/
    private BigDecimal bcmRemInt;

    /**
     * 应收欠息
     **/
    private BigDecimal recDebitInt;

    /**
     * 催收欠息
     **/
    private BigDecimal bcmDebitInt;

    /**
     * 应收应计罚息
     **/
    private BigDecimal recRemPenalInt;

    /**
     * 催收应计罚息
     **/
    private BigDecimal bcmRemPenalInt;

    /**
     * 应收罚息
     **/
    private BigDecimal recPenalInt;

    /**
     * 催收罚息
     **/
    private BigDecimal bcmPenalInt;

    /**
     * 应计复息
     **/
    private BigDecimal recCompoundInt;

    /**
     * 复息
     **/
    private BigDecimal compoundInt;

    /**
     * 记账状态
     **/
    private String recordStatus;

    /**
     * 记账日期
     **/
    private String recordDate;

    /**
     * 记账交易流水号
     **/
    private String recordTranSerno;

    /**
     * 记账交易时间
     **/
    private String recordTranTime;

    /**
     * 记账冲正交易流水号
     **/
    private String recordBackSerno;

    /**
     * 记账冲正交易时间
     **/
    private String recordBackTime;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 登记日期
     **/
    private String inputDate;

    /**
     * 最近修改人
     **/
    private String updId;

    /**
     * 最近修改机构
     **/
    private String updBrId;

    /**
     * 最近修改日期
     **/
    private String updDate;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;

    public PlaDebtClaimReducRecordBillRelDto() {
        // default implementation ignored
    }

    public String getPdcrrbrSerno() {
        return pdcrrbrSerno;
    }

    public void setPdcrrbrSerno(String pdcrrbrSerno) {
        this.pdcrrbrSerno = pdcrrbrSerno;
    }

    public String getPdcrraiSerno() {
        return pdcrraiSerno;
    }

    public void setPdcrraiSerno(String pdcrraiSerno) {
        this.pdcrraiSerno = pdcrraiSerno;
    }

    public String getPdcraiSerno() {
        return pdcraiSerno;
    }

    public void setPdcraiSerno(String pdcraiSerno) {
        this.pdcraiSerno = pdcraiSerno;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public BigDecimal getTotalTqlxAmt() {
        return totalTqlxAmt;
    }

    public void setTotalTqlxAmt(BigDecimal totalTqlxAmt) {
        this.totalTqlxAmt = totalTqlxAmt;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public BigDecimal getExecRateYear() {
        return execRateYear;
    }

    public void setExecRateYear(BigDecimal execRateYear) {
        this.execRateYear = execRateYear;
    }

    public String getFiveClass() {
        return fiveClass;
    }

    public void setFiveClass(String fiveClass) {
        this.fiveClass = fiveClass;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public BigDecimal getReducCapAmt() {
        return reducCapAmt;
    }

    public void setReducCapAmt(BigDecimal reducCapAmt) {
        this.reducCapAmt = reducCapAmt;
    }

    public BigDecimal getReducDebitInt() {
        return reducDebitInt;
    }

    public void setReducDebitInt(BigDecimal reducDebitInt) {
        this.reducDebitInt = reducDebitInt;
    }

    public BigDecimal getReducPenalInt() {
        return reducPenalInt;
    }

    public void setReducPenalInt(BigDecimal reducPenalInt) {
        this.reducPenalInt = reducPenalInt;
    }

    public BigDecimal getReducCompoundInt() {
        return reducCompoundInt;
    }

    public void setReducCompoundInt(BigDecimal reducCompoundInt) {
        this.reducCompoundInt = reducCompoundInt;
    }

    public BigDecimal getReducCostAmt() {
        return reducCostAmt;
    }

    public void setReducCostAmt(BigDecimal reducCostAmt) {
        this.reducCostAmt = reducCostAmt;
    }

    public BigDecimal getRecRemInt() {
        return recRemInt;
    }

    public void setRecRemInt(BigDecimal recRemInt) {
        this.recRemInt = recRemInt;
    }

    public BigDecimal getBcmRemInt() {
        return bcmRemInt;
    }

    public void setBcmRemInt(BigDecimal bcmRemInt) {
        this.bcmRemInt = bcmRemInt;
    }

    public BigDecimal getRecDebitInt() {
        return recDebitInt;
    }

    public void setRecDebitInt(BigDecimal recDebitInt) {
        this.recDebitInt = recDebitInt;
    }

    public BigDecimal getBcmDebitInt() {
        return bcmDebitInt;
    }

    public void setBcmDebitInt(BigDecimal bcmDebitInt) {
        this.bcmDebitInt = bcmDebitInt;
    }

    public BigDecimal getRecRemPenalInt() {
        return recRemPenalInt;
    }

    public void setRecRemPenalInt(BigDecimal recRemPenalInt) {
        this.recRemPenalInt = recRemPenalInt;
    }

    public BigDecimal getBcmRemPenalInt() {
        return bcmRemPenalInt;
    }

    public void setBcmRemPenalInt(BigDecimal bcmRemPenalInt) {
        this.bcmRemPenalInt = bcmRemPenalInt;
    }

    public BigDecimal getRecPenalInt() {
        return recPenalInt;
    }

    public void setRecPenalInt(BigDecimal recPenalInt) {
        this.recPenalInt = recPenalInt;
    }

    public BigDecimal getBcmPenalInt() {
        return bcmPenalInt;
    }

    public void setBcmPenalInt(BigDecimal bcmPenalInt) {
        this.bcmPenalInt = bcmPenalInt;
    }

    public BigDecimal getRecCompoundInt() {
        return recCompoundInt;
    }

    public void setRecCompoundInt(BigDecimal recCompoundInt) {
        this.recCompoundInt = recCompoundInt;
    }

    public BigDecimal getCompoundInt() {
        return compoundInt;
    }

    public void setCompoundInt(BigDecimal compoundInt) {
        this.compoundInt = compoundInt;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(String recordDate) {
        this.recordDate = recordDate;
    }

    public String getRecordTranSerno() {
        return recordTranSerno;
    }

    public void setRecordTranSerno(String recordTranSerno) {
        this.recordTranSerno = recordTranSerno;
    }

    public String getRecordTranTime() {
        return recordTranTime;
    }

    public void setRecordTranTime(String recordTranTime) {
        this.recordTranTime = recordTranTime;
    }

    public String getRecordBackSerno() {
        return recordBackSerno;
    }

    public void setRecordBackSerno(String recordBackSerno) {
        this.recordBackSerno = recordBackSerno;
    }

    public String getRecordBackTime() {
        return recordBackTime;
    }

    public void setRecordBackTime(String recordBackTime) {
        this.recordBackTime = recordBackTime;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}