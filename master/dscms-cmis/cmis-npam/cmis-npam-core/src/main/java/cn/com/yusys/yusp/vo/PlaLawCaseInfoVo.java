package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

@ExcelCsv(namePrefix = "案件查询列表导出", fileType = ExcelCsv.ExportFileType.XLS)
public class PlaLawCaseInfoVo {

    /*
   案件编号
    */
    @ExcelField(title = "案件编号", viewLength = 20)
    private String caseNo;

    /*
    客户编号
     */
    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /*
    客户名称
     */
    @ExcelField(title = "客户名称", viewLength = 20)
    private String cusName;

    /*
    案件类型
     */
    @ExcelField(title = "案件类型", dictCode = "STD_CASE_TYPE" ,viewLength = 20)
    private String caseType;

    /*
    案件进程
     */
    @ExcelField(title = "案件进程", dictCode = "STD_CASE_PROCESS_STATUS" ,viewLength = 20)
    private String caseProcess;

    /*
    标的金额（元）
     */
    @ExcelField(title = "标的金额（元）", viewLength = 20 ,format = "#0.00")
    private java.math.BigDecimal totalAmt;

    /*
    本金金额（元）
     */
    @ExcelField(title = "本金金额（元）", viewLength = 20 ,format = "#0.00")
    private java.math.BigDecimal capAmt;

    /*
    拖欠利息总额（元）
     */
    @ExcelField(title = "拖欠利息总额（元）", viewLength = 20 ,format = "#0.00")
    private java.math.BigDecimal totalTqlxAmt;

    /*
   登记人
    */
    @ExcelField(title = "登记人", viewLength = 20)
    private String inputIdName;

    /*
    登记机构
     */
    @ExcelField(title = "登记机构", viewLength = 30)
    private String inputBrIdName;

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public String getCaseProcess() {
        return caseProcess;
    }

    public void setCaseProcess(String caseProcess) {
        this.caseProcess = caseProcess;
    }

    public BigDecimal getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }

    public BigDecimal getCapAmt() {
        return capAmt;
    }

    public void setCapAmt(BigDecimal capAmt) {
        this.capAmt = capAmt;
    }

    public BigDecimal getTotalTqlxAmt() {
        return totalTqlxAmt;
    }

    public void setTotalTqlxAmt(BigDecimal totalTqlxAmt) {
        this.totalTqlxAmt = totalTqlxAmt;
    }

    public String getInputIdName() {
        return inputIdName;
    }

    public void setInputIdName(String inputIdName) {
        this.inputIdName = inputIdName;
    }

    public String getInputBrIdName() {
        return inputBrIdName;
    }

    public void setInputBrIdName(String inputBrIdName) {
        this.inputBrIdName = inputBrIdName;
    }
}
