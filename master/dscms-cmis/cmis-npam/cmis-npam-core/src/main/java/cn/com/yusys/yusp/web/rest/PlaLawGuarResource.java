/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaLawGuar;
import cn.com.yusys.yusp.service.PlaLawGuarService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawGuarResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-06-07 22:13:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plalawguar")
public class PlaLawGuarResource {
    @Autowired
    private PlaLawGuarService plaLawGuarService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaLawGuar>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaLawGuar> list = plaLawGuarService.selectAll(queryModel);
        return new ResultDto<>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaLawGuar>> index(QueryModel queryModel) {
        List<PlaLawGuar> list = plaLawGuarService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{caseNo}")
    protected ResultDto<PlaLawGuar> show(@PathVariable("caseNo") String caseNo) {
        PlaLawGuar plaLawGuar = plaLawGuarService.selectByPrimaryKey(caseNo);
        return new ResultDto<>(plaLawGuar);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchInsert")
    protected ResultDto<PlaLawGuar> batchInsert(@RequestBody PlaLawGuar plaLawGuar) {
        plaLawGuarService.batchInsert(plaLawGuar);
        return new ResultDto<>(plaLawGuar);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaLawGuar plaLawGuar) throws URISyntaxException {
        int result = plaLawGuarService.update(plaLawGuar);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{caseNo}")
    protected ResultDto<Integer> delete(@PathVariable("caseNo") String caseNo) {
        int result = plaLawGuarService.deleteByPrimaryKey(caseNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaLawGuarService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<PlaLawGuar>> queryPlaLawGuar(@RequestBody QueryModel queryModel) {
        List<PlaLawGuar> list = plaLawGuarService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }
}
