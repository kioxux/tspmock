/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.CfgPlaBcmParam;
import cn.com.yusys.yusp.domain.CfgPlaBcmRule;
import cn.com.yusys.yusp.dto.CfgPlaBcmRuleDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.CfgPlaBcmParamMapper;
import cn.com.yusys.yusp.repository.mapper.CfgPlaBcmRuleMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: CfgPlaBcmRuleService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-05-31 15:18:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgPlaBcmRuleService {

    @Autowired
    private CfgPlaBcmRuleMapper cfgPlaBcmRuleMapper;

    @Autowired
    private CfgPlaBcmParamMapper cfgPlaBcmParamMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CfgPlaBcmRule selectByPrimaryKey(String ruleId) {
        return cfgPlaBcmRuleMapper.selectByPrimaryKey(ruleId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<CfgPlaBcmRule> selectAll(QueryModel model) {
        return cfgPlaBcmRuleMapper.selectByModel(model);
    }

    /**
     * /**
     *
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CfgPlaBcmRule> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgPlaBcmRule> list = cfgPlaBcmRuleMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(CfgPlaBcmRule record) {
        return cfgPlaBcmRuleMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(CfgPlaBcmRule record) {
        return cfgPlaBcmRuleMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(CfgPlaBcmRule record) {
        return cfgPlaBcmRuleMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(CfgPlaBcmRule record) {
        return cfgPlaBcmRuleMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键修改规则状态
     * @参数与返回说明:
     * @算法描述: 无
     * 刘权
     */
    public int updateByRuleStatus(CfgPlaBcmRule record) {
        String ruleStatus = record.getRuleStatus();
        if ("".equals(ruleStatus)) {
            throw new YuspException(EcbEnum.COMMON_EXCEPTION_DEF.key, EcbEnum.COMMON_EXCEPTION_DEF.value + ",字段为空！");
        }
        return cfgPlaBcmRuleMapper.updateByRuleStatus(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String ruleId) {
        int count = cfgPlaBcmRuleMapper.deleteByPrimaryKey(ruleId);
        if (count > 0) {
            cfgPlaBcmParamMapper.deleteByRuleId(ruleId);
        }
        return count;
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return cfgPlaBcmRuleMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: save
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    public int save(CfgPlaBcmRuleDto cfgPlaBcmRuleDto) {
        // 规则编号
        String ruleId = "";
        int count = 0;
        if (cfgPlaBcmRuleDto != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            ruleId = cfgPlaBcmRuleDto.getRuleId();
            CfgPlaBcmRule cfgPlaBcmRule = selectByPrimaryKey(ruleId);
            CfgPlaBcmRule cfgPlaBcmRules = new CfgPlaBcmRule();
            BeanUtils.copyProperties(cfgPlaBcmRuleDto, cfgPlaBcmRules);
            if (cfgPlaBcmRule != null) {
                // 修改人
                cfgPlaBcmRules.setUpdId(inputId);
                // 修改机构
                cfgPlaBcmRules.setUpdBrId(inputBrId);
                // 修改日期
                cfgPlaBcmRules.setUpdDate(openDay);
                // 修改时间
                cfgPlaBcmRules.setUpdateTime(createTime);
                count = updateSelective(cfgPlaBcmRules);
            } else {
                // 登记时间
                cfgPlaBcmRules.setCreateTime(createTime);
                // 修改人
                cfgPlaBcmRules.setUpdId(inputId);
                // 修改机构
                cfgPlaBcmRules.setUpdBrId(inputBrId);
                // 修改日期
                cfgPlaBcmRules.setUpdDate(openDay);
                // 修改时间
                cfgPlaBcmRules.setUpdateTime(createTime);
                count = insert(cfgPlaBcmRules);
            }
            if (count > 0) {
                //催收规则参数配表
                List<CfgPlaBcmParam> cfgPlaBcmParamList = cfgPlaBcmRuleDto.getCfgPlaBcmParam();
                for (CfgPlaBcmParam cfgPlaBcmParams : cfgPlaBcmParamList) {
                    CfgPlaBcmParam cfgPlaBcmParam = cfgPlaBcmParamMapper.selectByPrimaryKey(cfgPlaBcmParams.getCpbpSerno());
                    if (cfgPlaBcmParam != null) {
                        cfgPlaBcmParams.setUpdDate(openDay);
                        cfgPlaBcmParams.setUpdBrId(inputBrId);
                        cfgPlaBcmParams.setUpdId(inputId);
                        cfgPlaBcmParams.setUpdateTime(createTime);
                        count = cfgPlaBcmParamMapper.updateByPrimaryKey(cfgPlaBcmParams);
                    } else {
                        cfgPlaBcmParams.setRuleId(ruleId);
                        cfgPlaBcmParams.setInputDate(openDay);
                        cfgPlaBcmParams.setInputBrId(inputBrId);
                        cfgPlaBcmParams.setInputId(inputId);
                        cfgPlaBcmParams.setCreateTime(createTime);
                        cfgPlaBcmParams.setUpdDate(openDay);
                        cfgPlaBcmParams.setUpdBrId(inputBrId);
                        cfgPlaBcmParams.setUpdId(inputId);
                        cfgPlaBcmParams.setUpdateTime(createTime);
                        count = cfgPlaBcmParamMapper.insert(cfgPlaBcmParams);
                    }
                }
            }
        }
        return count;
    }
}