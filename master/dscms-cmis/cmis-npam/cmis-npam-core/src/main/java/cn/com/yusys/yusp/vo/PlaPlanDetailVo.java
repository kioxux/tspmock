package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

@ExcelCsv(namePrefix = "清收计划明细表导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class PlaPlanDetailVo {

    /*
    客户编号
     */
    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /*
    客户名称
     */
    @ExcelField(title = "客户名称", viewLength = 20)
    private String cusName;

    /*
    借据编号
     */
    @ExcelField(title = "借据编号", viewLength = 20)
    private String billNo;

    /*
    贷款金额
     */
    @ExcelField(title = "贷款金额", viewLength = 20)
    private java.math.BigDecimal loanAmt;

    /*
    贷款余额
     */
    @ExcelField(title = "贷款余额", viewLength = 20)
    private java.math.BigDecimal loanBalance;

    /*
    责任机构
     */
    @ExcelField(title = "责任机构", viewLength = 20)
    private String managerBrId;

    /*
   责任人
    */
    @ExcelField(title = "责任人", viewLength = 20)
    private String managerId;

    /*
   资产类型
    */
    @ExcelField(title = "资产类型", dictCode = "STD_ASSET_TYPE",viewLength = 20)
    private String assetType;

    /*
   处置方式
    */
    @ExcelField(title = "处置方式", dictCode = "STD_DISP_TYPE", viewLength = 20)
    private String dispMode;

    /*
   预计清收金额
    */
    @ExcelField(title = "预计清收金额", viewLength = 20)
    private java.math.BigDecimal recoveryAmt;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public String getDispMode() {
        return dispMode;
    }

    public void setDispMode(String dispMode) {
        this.dispMode = dispMode;
    }

    public BigDecimal getRecoveryAmt() {
        return recoveryAmt;
    }

    public void setRecoveryAmt(BigDecimal recoveryAmt) {
        this.recoveryAmt = recoveryAmt;
    }
}
