/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaExpenseDetail
 * @类描述: pla_expense_detail数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 15:20:16
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_expense_detail")
public class PlaExpenseDetail extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 关联表流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PED_SERNO")
    private String pedSerno;

    /**
     * 费用登记流水号
     **/
    @Column(name = "PERI_SERNO", unique = false, nullable = true, length = 40)
    private String periSerno;

    /**
     * 费用发生类型
     **/
    @Column(name = "EXPENSE_HPP_TYPE", unique = false, nullable = true, length = 5)
    private String expenseHppType;

    /**
     * 费用发生阶段
     **/
    @Column(name = "EXPENSE_HPP_PHASE", unique = false, nullable = true, length = 5)
    private String expenseHppPhase;

    /**
     * 费用金额
     **/
    @Column(name = "EXPENSE_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal expenseAmt;

    /**
     * 费用发生日期
     **/
    @Column(name = "EXPENSE_HPP_DATE", unique = false, nullable = true, length = 20)
    private String expenseHppDate;

    /**
     * 费用类型
     **/
    @Column(name = "EXPENSE_TYPE", unique = false, nullable = true, length = 5)
    private String expenseType;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaExpenseDetail() {
        // default implementation ignored
    }

    /**
     * @param pedSerno
     */
    public void setPedSerno(String pedSerno) {
        this.pedSerno = pedSerno;
    }

    /**
     * @return pedSerno
     */
    public String getPedSerno() {
        return this.pedSerno;
    }

    /**
     * @param periSerno
     */
    public void setPeriSerno(String periSerno) {
        this.periSerno = periSerno;
    }

    /**
     * @return periSerno
     */
    public String getPeriSerno() {
        return this.periSerno;
    }

    /**
     * @param expenseHppType
     */
    public void setExpenseHppType(String expenseHppType) {
        this.expenseHppType = expenseHppType;
    }

    /**
     * @return expenseHppType
     */
    public String getExpenseHppType() {
        return this.expenseHppType;
    }

    /**
     * @param expenseHppPhase
     */
    public void setExpenseHppPhase(String expenseHppPhase) {
        this.expenseHppPhase = expenseHppPhase;
    }

    /**
     * @return expenseHppPhase
     */
    public String getExpenseHppPhase() {
        return this.expenseHppPhase;
    }

    /**
     * @param expenseAmt
     */
    public void setExpenseAmt(java.math.BigDecimal expenseAmt) {
        this.expenseAmt = expenseAmt;
    }

    /**
     * @return expenseAmt
     */
    public java.math.BigDecimal getExpenseAmt() {
        return this.expenseAmt;
    }

    /**
     * @param expenseHppDate
     */
    public void setExpenseHppDate(String expenseHppDate) {
        this.expenseHppDate = expenseHppDate;
    }

    /**
     * @return expenseHppDate
     */
    public String getExpenseHppDate() {
        return this.expenseHppDate;
    }

    /**
     * @param expenseType
     */
    public void setExpenseType(String expenseType) {
        this.expenseType = expenseType;
    }

    /**
     * @return expenseType
     */
    public String getExpenseType() {
        return this.expenseType;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}