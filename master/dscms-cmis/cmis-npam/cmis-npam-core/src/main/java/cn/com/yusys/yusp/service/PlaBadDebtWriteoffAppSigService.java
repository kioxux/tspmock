/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.PlaBadDebtWriteoffBillRel;
import cn.com.yusys.yusp.dto.PlaDebtClaimReducBillRelDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.ln3100.Ln3100Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaBadDebtWriteoffAppSig;
import cn.com.yusys.yusp.repository.mapper.PlaBadDebtWriteoffAppSigMapper;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBadDebtWriteoffAppSigService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-06-09 09:34:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaBadDebtWriteoffAppSigService {

    @Autowired
    private PlaBadDebtWriteoffAppSigMapper plaBadDebtWriteoffAppSigMapper;
    @Autowired
    private PlaBadDebtWriteoffBillRelService plaBadDebtWriteoffBillRelService;
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;
    @Autowired
    private Ln3100Server ln3100Server;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    private static final Logger log = LoggerFactory.getLogger(PlaBadDebtWriteoffAppSigService.class);
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PlaBadDebtWriteoffAppSig selectByPrimaryKey(String pbdwasSerno) {
        return plaBadDebtWriteoffAppSigMapper.selectByPrimaryKey(pbdwasSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaBadDebtWriteoffAppSig> selectAll(QueryModel model) {
        List<PlaBadDebtWriteoffAppSig> records = (List<PlaBadDebtWriteoffAppSig>) plaBadDebtWriteoffAppSigMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PlaBadDebtWriteoffAppSig> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaBadDebtWriteoffAppSig> list = plaBadDebtWriteoffAppSigMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PlaBadDebtWriteoffAppSig record) {
        return plaBadDebtWriteoffAppSigMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PlaBadDebtWriteoffAppSig record) {
        return plaBadDebtWriteoffAppSigMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PlaBadDebtWriteoffAppSig record) {
        int count=0;
        // 申请人
        String inputId = "";
        // 申请机构
        String inputBrId = "";
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        // 创建时间
        Date createTime = DateUtils.getCurrTimestamp();
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if(userInfo != null){

            // 申请人
            inputId = userInfo.getLoginCode();
            // 申请机构
            inputBrId = userInfo.getOrg().getCode();
        }
        record.setUpdId(inputId);
        record.setUpdBrId(inputBrId);
        record.setUpdDate(openDay);
        record.setUpdateTime(createTime);
        //默认赋值审批状态为 000
        record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
        count=plaBadDebtWriteoffAppSigMapper.updateByPrimaryKeySelective(record);
        return count ;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PlaBadDebtWriteoffAppSig record) {
        return plaBadDebtWriteoffAppSigMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pbdwasSerno) {
        return plaBadDebtWriteoffAppSigMapper.deleteByPrimaryKey(pbdwasSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaBadDebtWriteoffAppSigMapper.deleteByIds(ids);
    }
    /**
     * @方法名称: save
     * @方法描述: 保存
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    public int save(PlaBadDebtWriteoffAppSig record) {
        int count=0;
        if(record !=null){
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if(userInfo != null){
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            record.setInputId(inputId);
            record.setInputBrId(inputBrId);
            record.setInputDate(openDay);
            record.setUpdateTime(createTime);
            record.setApproveStatus("000");
            // 流水号
            // String serno= StringUtils.getUUID();//sequenceTemplateService.getSequenceTemplate("", new HashMap<>());
            // record.setPbdwasSerno(serno);
            count=plaBadDebtWriteoffAppSigMapper.insert(record);
        }
        return count;
    }

    /**
     * @方法名称: updateBill
     * @方法描述: 单户呆账核销审批通过后同步核心借据并更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateBill(PlaBadDebtWriteoffAppSig record) {
        // 1、先查询关联借据信息
        String pbdwasSerno= record.getPbdwasSerno();
        List<PlaBadDebtWriteoffBillRel> plaBadDebtWriteoffBillRelList=plaBadDebtWriteoffBillRelService.selectByserno(pbdwasSerno);
        // 2、调用和兴3100接口查询借据
        String billNo = "";
        // 核销总金额
        BigDecimal totalWriteoffAmt = BigDecimal.ZERO;
        // 核销总本金
        BigDecimal totalWriteoffCap = BigDecimal.ZERO;
        // 核销总利息
        BigDecimal totalWriteoffInt = BigDecimal.ZERO;
        // 贷款金额
        BigDecimal loanAmt = BigDecimal.ZERO;
        // 贷款余额
        BigDecimal loanBalance = BigDecimal.ZERO;
        // 拖欠利息总额
        BigDecimal totalTqlxAmt = BigDecimal.ZERO;
        for(PlaBadDebtWriteoffBillRel plaBadDebtWriteoffBillRel: plaBadDebtWriteoffBillRelList){
            billNo = plaBadDebtWriteoffBillRel.getBillNo();
            ResultDto<Ln3100RespDto> ln3100RespDto =ln3100Server.queryHXBill(billNo);
            log.info("ln3100接口返回：" + ln3100RespDto);
            String ln3100Code = Optional.ofNullable(ln3100RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String ln3100Meesage = Optional.ofNullable(ln3100RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            if (Objects.equals(ln3100Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                if(ln3100RespDto.getData() != null){
                    loanAmt=ln3100RespDto.getData().getJiejuuje();
                    loanBalance=ln3100RespDto.getData().getYuqibjin().add(ln3100RespDto.getData().getZhchbjin());
                    totalTqlxAmt=ln3100RespDto.getData().getYsyjlixi().add(ln3100RespDto.getData().getCsyjlixi()).add(ln3100RespDto.getData().getYsqianxi())
                            .add(ln3100RespDto.getData().getCsqianxi()).add(ln3100RespDto.getData().getYsyjfaxi()).add(ln3100RespDto.getData().getCsyjfaxi()).add(ln3100RespDto.getData().getYshofaxi())
                            .add(ln3100RespDto.getData().getCshofaxi()).add(ln3100RespDto.getData().getYingjifx()).add(ln3100RespDto.getData().getFuxiiiii());
                    plaBadDebtWriteoffBillRel.setLoanAmt(loanAmt);
                    plaBadDebtWriteoffBillRel.setLoanBalance(loanBalance);
                    plaBadDebtWriteoffBillRel.setTotalTqlxAmt(totalTqlxAmt);
                    // 计算主表金额
                    totalWriteoffCap=totalWriteoffCap.add(loanBalance);
                    totalWriteoffInt= totalWriteoffInt.add(totalTqlxAmt);
                    totalWriteoffAmt=totalWriteoffCap.add(totalWriteoffInt);
                    plaBadDebtWriteoffBillRelService.updateSelective(plaBadDebtWriteoffBillRel);
                }
            }
        }
        // 3、更新主表数据
        record.setTotalWriteoffAmt(totalWriteoffAmt);
        record.setTotalWriteoffCap(totalWriteoffCap);
        record.setTotalWriteoffInt(totalWriteoffInt);
        return plaBadDebtWriteoffAppSigMapper.updateByPrimaryKeySelective(record);
    }
}
