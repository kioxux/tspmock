/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaAssetPldRecordRel;
import cn.com.yusys.yusp.service.PlaAssetPldRecordRelService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAssetPldRecordRelResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:03:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plaassetpldrecordrel")
public class PlaAssetPldRecordRelResource {
    @Autowired
    private PlaAssetPldRecordRelService plaAssetPldRecordRelService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaAssetPldRecordRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaAssetPldRecordRel> list = plaAssetPldRecordRelService.selectAll(queryModel);
        return new ResultDto<List<PlaAssetPldRecordRel>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<PlaAssetPldRecordRel>> index(QueryModel queryModel) {
        List<PlaAssetPldRecordRel> list = plaAssetPldRecordRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaAssetPldRecordRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{papreSerno}")
    protected ResultDto<PlaAssetPldRecordRel> show(@PathVariable("papreSerno") String papreSerno) {
        PlaAssetPldRecordRel plaAssetPldRecordRel = plaAssetPldRecordRelService.selectByPrimaryKey(papreSerno);
        return new ResultDto<PlaAssetPldRecordRel>(plaAssetPldRecordRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<PlaAssetPldRecordRel> create(@RequestBody PlaAssetPldRecordRel plaAssetPldRecordRel) throws URISyntaxException {
        plaAssetPldRecordRelService.insert(plaAssetPldRecordRel);
        return new ResultDto<PlaAssetPldRecordRel>(plaAssetPldRecordRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaAssetPldRecordRel plaAssetPldRecordRel) throws URISyntaxException {
        int result = plaAssetPldRecordRelService.update(plaAssetPldRecordRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{papreSerno}")
    protected ResultDto<Integer> delete(@PathVariable("papreSerno") String papreSerno) {
        int result = plaAssetPldRecordRelService.deleteByPrimaryKey(papreSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaAssetPldRecordRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据抵债资产处置申请流水号获取关联信息
     *
     * @author jijian_yx
     * @date 2021/6/9 20:24
     **/
    @ApiOperation("根据抵债资产处置申请流水号获取关联信息")
    @PostMapping("/getrecordrel")
    protected ResultDto<PlaAssetPldRecordRel> getRecordRel(@RequestBody String papaiSerno) {
        PlaAssetPldRecordRel recordRel = plaAssetPldRecordRelService.getRecordRel(papaiSerno);
        return new ResultDto<PlaAssetPldRecordRel>(recordRel);
    }

    /**
     * 保存/更新 抵债资产处置记账关联信息
     *
     * @author jijian_yx
     * @date 2021/6/9 20:24
     **/
    @ApiOperation("保存/更新抵债资产处置记账关联信息")
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody PlaAssetPldRecordRel recordRel) {
        int result = plaAssetPldRecordRelService.save(recordRel);
        return new ResultDto<Integer>(result);
    }
}
