/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.PlaDebtClaimReducBillRel;
import cn.com.yusys.yusp.dto.PlaDebtClaimReducAppInfoDto;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaDebtClaimReducAppInfo;
import cn.com.yusys.yusp.repository.mapper.PlaDebtClaimReducAppInfoMapper;

import static cn.com.yusys.yusp.constants.CmisBizConstants.APPLY_STATE_CALL_BACK;
import static cn.com.yusys.yusp.constants.CmisBizConstants.APPLY_STATE_QUIT;
import static jdk.nashorn.tools.Shell.SUCCESS;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaDebtClaimReducAppInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-06-08 19:44:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaDebtClaimReducAppInfoService {

    @Autowired
    private PlaDebtClaimReducAppInfoMapper plaDebtClaimReducAppInfoMapper;

    @Autowired
    private PlaDebtClaimReducBillRelService plaDebtClaimReducBillRelService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PlaDebtClaimReducAppInfo selectByPrimaryKey(String pdcraiSerno) {
        return plaDebtClaimReducAppInfoMapper.selectByPrimaryKey(pdcraiSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaDebtClaimReducAppInfo> selectAll(QueryModel model) {
        List<PlaDebtClaimReducAppInfo> records = (List<PlaDebtClaimReducAppInfo>) plaDebtClaimReducAppInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PlaDebtClaimReducAppInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaDebtClaimReducAppInfo> list = plaDebtClaimReducAppInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PlaDebtClaimReducAppInfo record) {
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        record.setApproveStatus("000");
        record.setInputId(SessionUtils.getUserInformation().getLoginCode());
        record.setInputBrId(SessionUtils.getUserInformation().getOrg().getCode());
        record.setInputDate(openDay);
        record.setUpdId(SessionUtils.getUserInformation().getLoginCode());
        record.setUpdBrId(SessionUtils.getUserInformation().getOrg().getCode());
        record.setUpdDate(openDay);
        record.setCreateTime(DateUtils.getCurrTimestamp());
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        return plaDebtClaimReducAppInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PlaDebtClaimReducAppInfo record) {
        return plaDebtClaimReducAppInfoMapper.insertSelective(record);
    }


    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PlaDebtClaimReducAppInfo record) {
        return plaDebtClaimReducAppInfoMapper.updateByPrimaryKeySelective(record);
    }
    /**
     * @方法名称: updatePlaDebtClaimReducAppInfo
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updatePlaDebtClaimReducAppInfo(PlaDebtClaimReducAppInfoDto plaDebtClaimReducAppInfoDto) {
        int count =0;
        if(plaDebtClaimReducAppInfoDto !=null){
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
            }
            PlaDebtClaimReducAppInfo plaDebtClaimReducAppInfo = new PlaDebtClaimReducAppInfo();
            BeanUtils.copyProperties(plaDebtClaimReducAppInfoDto,plaDebtClaimReducAppInfo);
            plaDebtClaimReducAppInfo.setUpdId(inputId);
            plaDebtClaimReducAppInfo.setUpdBrId(inputBrId);
            plaDebtClaimReducAppInfo.setUpdDate(openDay);
            plaDebtClaimReducAppInfo.setUpdateTime(createTime);
            count =plaDebtClaimReducAppInfoMapper.updateByPrimaryKeySelective(plaDebtClaimReducAppInfo);
            List<PlaDebtClaimReducBillRel> plaDebtClaimReducBillRelList = plaDebtClaimReducAppInfoDto.getPlaDebtClaimReducBillRel();
                plaDebtClaimReducBillRelService.insertByBillRel(plaDebtClaimReducBillRelList);
        }
        return count;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PlaDebtClaimReducAppInfo record) {
        return plaDebtClaimReducAppInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pdcraiSerno) {
        return plaDebtClaimReducAppInfoMapper.deleteByPrimaryKey(pdcraiSerno);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByApproveStatus(PlaDebtClaimReducAppInfo plaDebtClaimReducAppInfo) {
        if (APPLY_STATE_CALL_BACK.equals(plaDebtClaimReducAppInfo.getApproveStatus())){
            plaDebtClaimReducAppInfo.setApproveStatus(APPLY_STATE_QUIT);
            return  plaDebtClaimReducAppInfoMapper.updateByPrimaryKey(plaDebtClaimReducAppInfo);
        } else if (APPLY_STATE_QUIT.equals(plaDebtClaimReducAppInfo.getApproveStatus())){
            return SUCCESS;
        }
        return plaDebtClaimReducAppInfoMapper.deleteByPrimaryKey(plaDebtClaimReducAppInfo);

    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaDebtClaimReducAppInfoMapper.deleteByIds(ids);
    }
}
