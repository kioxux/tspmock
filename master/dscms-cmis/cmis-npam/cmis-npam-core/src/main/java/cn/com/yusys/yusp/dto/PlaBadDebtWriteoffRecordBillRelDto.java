/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBadDebtWriteoffRecordBillRel
 * @类描述: pla_bad_debt_writeoff_record_bill_rel数据实体类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-09 09:34:55
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class PlaBadDebtWriteoffRecordBillRelDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 借据关联流水号
     **/
    private String pbdwrbrSerno;

    /**
     * 记账申请流水号
     **/
    private String pbdwraSerno;

    /**
     * 合同编号
     **/
    private String contNo;

    /**
     * 借据编号
     **/
    private String billNo;

    /**
     * 客户编号
     **/
    private String cusId;

    /**
     * 客户名称
     **/
    private String cusName;

    /**
     * 产品名称
     **/
    private String prdName;

    /**
     * 币种
     **/
    private String curType;

    /**
     * 贷款金额
     **/
    private java.math.BigDecimal loanAmt;

    /**
     * 贷款余额
     **/
    private java.math.BigDecimal loanBalance;

    /**
     * 拖欠利息总额
     **/
    private java.math.BigDecimal totalTqlxAmt;

    /**
     * 贷款起始日
     **/
    private String loanStartDate;

    /**
     * 贷款到期日
     **/
    private String loanEndDate;

    /**
     * 执行年利率
     **/
    private java.math.BigDecimal execRateYear;

    /**
     * 五级分类
     **/
    private String fiveClass;

    /**
     * 记账状态
     **/
    private String recordStatus;

    /**
     * 核销状态
     **/
    private String writeoffStatus;

    /**
     * 记账日期
     **/
    private String recordDate;

    /**
     * 核销日期
     **/
    private String writeoffDate;

    /**
     * 核销标识
     **/
    private String writeoffFlag;

    /**
     * 责任人
     **/
    private String managerId;

    /**
     * 责任机构
     **/
    private String managerBrId;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 登记日期
     **/
    private String inputDate;

    /**
     * 最近修改人
     **/
    private String updId;

    /**
     * 最近修改机构
     **/
    private String updBrId;

    /**
     * 最近修改日期
     **/
    private String updDate;

    /**
     * 核销申请流水号
     */
    private String serno;
    /**
     * 创建时间
     **/
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    private java.util.Date updateTime;

    /**
     * 记账交易流水
     **/
    private String recordTranSerno;

    /**
     * 记账交易时间
     **/
    private String recordTranTime;

    /**
     * 冲正交易流水
     **/
    private String recordBackSerno;

    /**
     * 冲正交易时间
     **/
    private String recordBackTime;


    public PlaBadDebtWriteoffRecordBillRelDto() {
        // default implementation ignored
    }

    public String getPbdwrbrSerno() {
        return pbdwrbrSerno;
    }

    public void setPbdwrbrSerno(String pbdwrbrSerno) {
        this.pbdwrbrSerno = pbdwrbrSerno;
    }

    public String getPbdwraSerno() {
        return pbdwraSerno;
    }

    public void setPbdwraSerno(String pbdwraSerno) {
        this.pbdwraSerno = pbdwraSerno;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public BigDecimal getTotalTqlxAmt() {
        return totalTqlxAmt;
    }

    public void setTotalTqlxAmt(BigDecimal totalTqlxAmt) {
        this.totalTqlxAmt = totalTqlxAmt;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public BigDecimal getExecRateYear() {
        return execRateYear;
    }

    public void setExecRateYear(BigDecimal execRateYear) {
        this.execRateYear = execRateYear;
    }

    public String getFiveClass() {
        return fiveClass;
    }

    public void setFiveClass(String fiveClass) {
        this.fiveClass = fiveClass;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getWriteoffStatus() {
        return writeoffStatus;
    }

    public void setWriteoffStatus(String writeoffStatus) {
        this.writeoffStatus = writeoffStatus;
    }

    public String getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(String recordDate) {
        this.recordDate = recordDate;
    }

    public String getWriteoffDate() {
        return writeoffDate;
    }

    public void setWriteoffDate(String writeoffDate) {
        this.writeoffDate = writeoffDate;
    }

    public String getWriteoffFlag() {
        return writeoffFlag;
    }

    public void setWriteoffFlag(String writeoffFlag) {
        this.writeoffFlag = writeoffFlag;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getRecordTranSerno() {
        return recordTranSerno;
    }

    public void setRecordTranSerno(String recordTranSerno) {
        this.recordTranSerno = recordTranSerno;
    }

    public String getRecordTranTime() {
        return recordTranTime;
    }

    public void setRecordTranTime(String recordTranTime) {
        this.recordTranTime = recordTranTime;
    }

    public String getRecordBackSerno() {
        return recordBackSerno;
    }

    public void setRecordBackSerno(String recordBackSerno) {
        this.recordBackSerno = recordBackSerno;
    }

    public String getRecordBackTime() {
        return recordBackTime;
    }

    public void setRecordBackTime(String recordBackTime) {
        this.recordBackTime = recordBackTime;
    }
}