/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.DefaultDataHandle;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.vo.PlaPlanDetailVo;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaBadDebtWriteoffBillRel;
import cn.com.yusys.yusp.service.PlaBadDebtWriteoffBillRelService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBadDebtWriteoffBillRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-06-09 09:34:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plabaddebtwriteoffbillrel")
public class PlaBadDebtWriteoffBillRelResource {
    @Autowired
    private PlaBadDebtWriteoffBillRelService plaBadDebtWriteoffBillRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaBadDebtWriteoffBillRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaBadDebtWriteoffBillRel> list = plaBadDebtWriteoffBillRelService.selectAll(queryModel);
        return new ResultDto<List<PlaBadDebtWriteoffBillRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/query")
    protected ResultDto<List<PlaBadDebtWriteoffBillRel>> index(QueryModel queryModel) {
        List<PlaBadDebtWriteoffBillRel> list = plaBadDebtWriteoffBillRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaBadDebtWriteoffBillRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pwbrSerno}")
    protected ResultDto<PlaBadDebtWriteoffBillRel> show(@PathVariable("pwbrSerno") String pwbrSerno) {
        PlaBadDebtWriteoffBillRel plaBadDebtWriteoffBillRel = plaBadDebtWriteoffBillRelService.selectByPrimaryKey(pwbrSerno);
        return new ResultDto<PlaBadDebtWriteoffBillRel>(plaBadDebtWriteoffBillRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveinfo")
    protected ResultDto<PlaBadDebtWriteoffBillRel> create(@RequestBody PlaBadDebtWriteoffBillRel plaBadDebtWriteoffBillRel) throws URISyntaxException {
        plaBadDebtWriteoffBillRelService.insert(plaBadDebtWriteoffBillRel);
        return new ResultDto<PlaBadDebtWriteoffBillRel>(plaBadDebtWriteoffBillRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaBadDebtWriteoffBillRel plaBadDebtWriteoffBillRel) throws URISyntaxException {
        int result = plaBadDebtWriteoffBillRelService.update(plaBadDebtWriteoffBillRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "借据列表删除")
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody PlaBadDebtWriteoffBillRel plaBadDebtWriteoffBillRel) {
        int result = plaBadDebtWriteoffBillRelService.deleteBill(plaBadDebtWriteoffBillRel);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaBadDebtWriteoffBillRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "核销借据列表查询")
    @PostMapping("/queryplabaddebtwriteoffbill")
    protected ResultDto<List<PlaBadDebtWriteoffBillRel>> queryPlaBadDebtWriteoffBill(@RequestBody QueryModel queryModel) {
        List<PlaBadDebtWriteoffBillRel> list = plaBadDebtWriteoffBillRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaBadDebtWriteoffBillRel>>(list);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "借据引入")
    @PostMapping("/save")
    protected ResultDto<PlaBadDebtWriteoffBillRel> save(@RequestBody PlaBadDebtWriteoffBillRel plaBadDebtWriteoffBillRel) throws URISyntaxException {
        plaBadDebtWriteoffBillRelService.save(plaBadDebtWriteoffBillRel);
        return new ResultDto<PlaBadDebtWriteoffBillRel>(plaBadDebtWriteoffBillRel);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "借据引入多选")
    @PostMapping("/saveList")
    protected ResultDto<Integer> saveList(@RequestBody List<PlaBadDebtWriteoffBillRel> plaBadDebtWriteoffBillRelList) throws URISyntaxException {
        int count =plaBadDebtWriteoffBillRelService.saveList(plaBadDebtWriteoffBillRelList);
        return new ResultDto<Integer>(count);
    }


    /**
     * Excel数据导入
     * @param fileId Excel文件信息ID
     * @return
     */
    @PostMapping("/importplanDetail")
    public ResultDto<String> asyncImportplanDetail(@RequestParam("fileId") String fileId, @RequestBody Map<String, String> paramsMap) {
        String message=plaBadDebtWriteoffBillRelService.asyncImportplanDetail(fileId,paramsMap);
        return ResultDto.success().message(message);
    }

    /**
     * Excel模板导出
     */
    @PostMapping("/exportPlaBadDebtWriteoffBillRel")
    public ResultDto<ProgressDto> asyncExportPlanDetail(@RequestBody QueryModel model) {
        ProgressDto progressDto = plaBadDebtWriteoffBillRelService.asyncExportPlaBadDebtWriteoffBillRel(model);
        return ResultDto.success(progressDto);
    }

    /**
     * 批量呆账核销业务列表详情模板
     */
    @PostMapping("/exportPlaBad")
    public ResultDto<ProgressDto> asyncExportPlaBad() {
        ProgressDto progressDto = plaBadDebtWriteoffBillRelService.asyncExportPlaBad();
        return ResultDto.success(progressDto);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "核销业务清单查询")
    @PostMapping("/queryPlabaddebtwriteOffBillByContNo")
    protected ResultDto<List<PlaBadDebtWriteoffBillRel>> queryPlabaddebtwriteOffBillByContNo(@RequestBody QueryModel queryModel) {
        List<PlaBadDebtWriteoffBillRel> list = plaBadDebtWriteoffBillRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaBadDebtWriteoffBillRel>>(list);
    }
}
