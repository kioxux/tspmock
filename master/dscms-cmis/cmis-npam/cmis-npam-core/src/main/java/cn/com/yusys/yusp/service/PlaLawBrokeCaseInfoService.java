/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaLawBrokeCaseInfo;
import cn.com.yusys.yusp.repository.mapper.PlaLawBrokeCaseInfoMapper;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawBrokeCaseInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-03 20:31:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PlaLawBrokeCaseInfoService {

    @Autowired
    private PlaLawBrokeCaseInfoMapper plaLawBrokeCaseInfoMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PlaLawBrokeCaseInfo selectByPrimaryKey(String brokeCaseNo) {
        return plaLawBrokeCaseInfoMapper.selectByPrimaryKey(brokeCaseNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PlaLawBrokeCaseInfo> selectAll(QueryModel model) {
        List<PlaLawBrokeCaseInfo> records = (List<PlaLawBrokeCaseInfo>) plaLawBrokeCaseInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PlaLawBrokeCaseInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PlaLawBrokeCaseInfo> list = plaLawBrokeCaseInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PlaLawBrokeCaseInfo record) {
        return plaLawBrokeCaseInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PlaLawBrokeCaseInfo record) {
        String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.NPAM_BROKE_CASE_NO, new HashMap<>());
        record.setBrokeCaseNo(serno);
        return plaLawBrokeCaseInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PlaLawBrokeCaseInfo record) {
        return plaLawBrokeCaseInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PlaLawBrokeCaseInfo record) {
        // 申请时间
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        record.setUpdDate(openDay);
        return plaLawBrokeCaseInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String brokeCaseNo) {
        return plaLawBrokeCaseInfoMapper.deleteByPrimaryKey(brokeCaseNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return plaLawBrokeCaseInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByPlaLawBrokeCaseInfo
     * @方法描述: 根据律师姓名查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int selectByPlaLawBrokeCaseInfo(String lawyerName) {
        return plaLawBrokeCaseInfoMapper.selectByPlaLawBrokeCaseInfo(lawyerName);
    }
}
