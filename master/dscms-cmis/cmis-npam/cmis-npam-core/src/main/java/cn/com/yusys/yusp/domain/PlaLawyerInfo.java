/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawyerInfo
 * @类描述: pla_lawyer_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 13:49:06
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_lawyer_info")
public class PlaLawyerInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 律师编号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "LAWYER_NO")
    private String lawyerNo;

    /**
     * 律师姓名
     **/
    @Column(name = "LAWYER_NAME", unique = false, nullable = true, length = 80)
    private String lawyerName;

    /**
     * 律师联系电话
     **/
    @Column(name = "LAWYER_TEL_NO", unique = false, nullable = true, length = 20)
    private String lawyerTelNo;

    /**
     * 律师事务所名称
     **/
    @Column(name = "LAW_OFFICE_NAME", unique = false, nullable = true, length = 80)
    private String lawOfficeName;

    /**
     * 律师状态
     **/
    @Column(name = "LAWYER_STATUS", unique = false, nullable = true, length = 5)
    private String lawyerStatus;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaLawyerInfo() {
        // default implementation ignored
    }

    /**
     * @param lawyerNo
     */
    public void setLawyerNo(String lawyerNo) {
        this.lawyerNo = lawyerNo;
    }

    /**
     * @return lawyerNo
     */
    public String getLawyerNo() {
        return this.lawyerNo;
    }

    /**
     * @param lawyerName
     */
    public void setLawyerName(String lawyerName) {
        this.lawyerName = lawyerName;
    }

    /**
     * @return lawyerName
     */
    public String getLawyerName() {
        return this.lawyerName;
    }

    /**
     * @param lawyerTelNo
     */
    public void setLawyerTelNo(String lawyerTelNo) {
        this.lawyerTelNo = lawyerTelNo;
    }

    /**
     * @return lawyerTelNo
     */
    public String getLawyerTelNo() {
        return this.lawyerTelNo;
    }

    /**
     * @param lawOfficeName
     */
    public void setLawOfficeName(String lawOfficeName) {
        this.lawOfficeName = lawOfficeName;
    }

    /**
     * @return lawOfficeName
     */
    public String getLawOfficeName() {
        return this.lawOfficeName;
    }

    /**
     * @param lawyerStatus
     */
    public void setLawyerStatus(String lawyerStatus) {
        this.lawyerStatus = lawyerStatus;
    }

    /**
     * @return lawyerStatus
     */
    public String getLawyerStatus() {
        return this.lawyerStatus;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}