/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaBadDebtWriteoffAppBatch;
import cn.com.yusys.yusp.service.PlaBadDebtWriteoffAppBatchService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBadDebtWriteoffAppBatchResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-06-09 09:34:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plabaddebtwriteoffappbatch")
public class PlaBadDebtWriteoffAppBatchResource {
    @Autowired
    private PlaBadDebtWriteoffAppBatchService plaBadDebtWriteoffAppBatchService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaBadDebtWriteoffAppBatch>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaBadDebtWriteoffAppBatch> list = plaBadDebtWriteoffAppBatchService.selectAll(queryModel);
        return new ResultDto<List<PlaBadDebtWriteoffAppBatch>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaBadDebtWriteoffAppBatch>> index(QueryModel queryModel) {
        List<PlaBadDebtWriteoffAppBatch> list = plaBadDebtWriteoffAppBatchService.selectByModel(queryModel);
        return new ResultDto<List<PlaBadDebtWriteoffAppBatch>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pbdwabSerno}")
    protected ResultDto<PlaBadDebtWriteoffAppBatch> show(@PathVariable("pbdwabSerno") String pbdwabSerno) {
        PlaBadDebtWriteoffAppBatch plaBadDebtWriteoffAppBatch = plaBadDebtWriteoffAppBatchService.selectByPrimaryKey(pbdwabSerno);
        return new ResultDto<PlaBadDebtWriteoffAppBatch>(plaBadDebtWriteoffAppBatch);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaBadDebtWriteoffAppBatch> create(@RequestBody PlaBadDebtWriteoffAppBatch plaBadDebtWriteoffAppBatch) throws URISyntaxException {
        plaBadDebtWriteoffAppBatchService.insert(plaBadDebtWriteoffAppBatch);
        return new ResultDto<PlaBadDebtWriteoffAppBatch>(plaBadDebtWriteoffAppBatch);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "批量核销列表修改")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaBadDebtWriteoffAppBatch plaBadDebtWriteoffAppBatch) throws URISyntaxException {
        int result = plaBadDebtWriteoffAppBatchService.update(plaBadDebtWriteoffAppBatch);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "批量核销列表删除")
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody PlaBadDebtWriteoffAppBatch plaBadDebtWriteoffAppBatch) {
        int result = plaBadDebtWriteoffAppBatchService.deleteByPrimaryKey(plaBadDebtWriteoffAppBatch.getPbdwabSerno());
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaBadDebtWriteoffAppBatchService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     */
    @ApiOperation(value = "批量核销列表查询")
    @PostMapping("/queryPlaBadDebtWriteoffAppBatchList")
    protected ResultDto<List<PlaBadDebtWriteoffAppBatch>> queryPlaBadDebtWriteoffAppBatchList(@RequestBody QueryModel queryModel) {
        return new ResultDto<List<PlaBadDebtWriteoffAppBatch>>(plaBadDebtWriteoffAppBatchService.selectByModel(queryModel));
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "批量核销详细信息查询")
    @PostMapping("/queryPlaBadDebtWriteoffAppBatchInfo")
    protected ResultDto<PlaBadDebtWriteoffAppBatch> queryPlaBadDebtWriteoffAppBatchInfo(@RequestBody PlaBadDebtWriteoffAppBatch plaBadDebtWriteoffAppBatch) {
        PlaBadDebtWriteoffAppBatch plaBadDebtWriteoffAppBatchs = plaBadDebtWriteoffAppBatchService.selectByPrimaryKey(plaBadDebtWriteoffAppBatch.getPbdwabSerno());
        return new ResultDto<PlaBadDebtWriteoffAppBatch>(plaBadDebtWriteoffAppBatchs);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "批量核销保存")
    @PostMapping("/save")
    protected ResultDto<PlaBadDebtWriteoffAppBatch> save(@RequestBody PlaBadDebtWriteoffAppBatch plaBadDebtWriteoffAppBatch) throws URISyntaxException {
        plaBadDebtWriteoffAppBatchService.save(plaBadDebtWriteoffAppBatch);
        return new ResultDto<PlaBadDebtWriteoffAppBatch>(plaBadDebtWriteoffAppBatch);
    }

}
