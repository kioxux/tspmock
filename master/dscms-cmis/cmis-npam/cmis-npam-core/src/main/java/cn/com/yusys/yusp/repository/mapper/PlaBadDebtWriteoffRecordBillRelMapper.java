/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaBadDebtWriteoffRecordBillRel;
import cn.com.yusys.yusp.dto.PlaBadDebtWriteoffRecordBillRelDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBadDebtWriteoffRecordBillRelMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-09 09:34:55
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface PlaBadDebtWriteoffRecordBillRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    PlaBadDebtWriteoffRecordBillRel selectByPrimaryKey(@Param("pbdwrbrSerno") String pbdwrbrSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PlaBadDebtWriteoffRecordBillRel> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insert(PlaBadDebtWriteoffRecordBillRelDto record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insertSelective(PlaBadDebtWriteoffRecordBillRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPrimaryKey(PlaBadDebtWriteoffRecordBillRel record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPrimaryKeySelective(PlaBadDebtWriteoffRecordBillRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    int deleteByPrimaryKey(@Param("pbdwrbrSerno") String pbdwrbrSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectCount
     * @方法描述: 查询是否引入
     * @参数与返回说明:
     * @算法描述: 无
     */
    PlaBadDebtWriteoffRecordBillRel selectplaBadDebtWriteoffRecordBillRel(PlaBadDebtWriteoffRecordBillRelDto record);

    /**
     * 查询户数，金额
     *
     * @param serno
     * @return
     */
    Map selectByPbdwraSerno(@Param("serno") String serno);

    /**
     * @方法名称: selectListByPbdwraSerno
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PlaBadDebtWriteoffRecordBillRel> selectListByPbdwraSerno(String pbdwraSerno);

    /**
     * @方法名称: updateByPbdwraSerno
     * @方法描述: 根据主表流水号，借据号更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPbdwraSerno(PlaBadDebtWriteoffRecordBillRel record);
}