/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaAdvanceExpenRecoverDetail
 * @类描述: pla_advance_expen_recover_detail数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 16:52:18
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_advance_expen_recover_detail")
public class PlaAdvanceExpenRecoverDetail extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 关联表流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PAERD_SERNO")
    private String paerdSerno;

    /**
     * 费用明细流水号
     **/
    @Column(name = "PERI_SERNO", unique = false, nullable = true, length = 40)
    private String periSerno;

    /**
     * 回收金额
     **/
    @Column(name = "RECOVER_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal recoverAmt;

    /**
     * 回收日期
     **/
    @Column(name = "RECOVER_DATE", unique = false, nullable = true, length = 20)
    private String recoverDate;

    /**
     * 回收后剩余垫支金额
     **/
    @Column(name = "RECLAIM_ADVANCE_EXPEN_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal reclaimAdvanceExpenAmt;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaAdvanceExpenRecoverDetail() {
        // default implementation ignored
    }

    /**
     * @param paerdSerno
     */
    public void setPaerdSerno(String paerdSerno) {
        this.paerdSerno = paerdSerno;
    }

    /**
     * @return paerdSerno
     */
    public String getPaerdSerno() {
        return this.paerdSerno;
    }

    /**
     * @param periSerno
     */
    public void setPeriSerno(String periSerno) {
        this.periSerno = periSerno;
    }

    /**
     * @return periSerno
     */
    public String getPeriSerno() {
        return this.periSerno;
    }

    /**
     * @param recoverAmt
     */
    public void setRecoverAmt(java.math.BigDecimal recoverAmt) {
        this.recoverAmt = recoverAmt;
    }

    /**
     * @return recoverAmt
     */
    public java.math.BigDecimal getRecoverAmt() {
        return this.recoverAmt;
    }

    /**
     * @param recoverDate
     */
    public void setRecoverDate(String recoverDate) {
        this.recoverDate = recoverDate;
    }

    /**
     * @return recoverDate
     */
    public String getRecoverDate() {
        return this.recoverDate;
    }

    /**
     * @param reclaimAdvanceExpenAmt
     */
    public void setReclaimAdvanceExpenAmt(java.math.BigDecimal reclaimAdvanceExpenAmt) {
        this.reclaimAdvanceExpenAmt = reclaimAdvanceExpenAmt;
    }

    /**
     * @return reclaimAdvanceExpenAmt
     */
    public java.math.BigDecimal getReclaimAdvanceExpenAmt() {
        return this.reclaimAdvanceExpenAmt;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}