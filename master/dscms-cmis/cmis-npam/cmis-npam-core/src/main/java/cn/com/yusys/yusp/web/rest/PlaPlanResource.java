/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaPlan;
import cn.com.yusys.yusp.service.PlaPlanService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaPlanResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-28 14:41:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "清收计划管理")
@RestController
@RequestMapping("/api/plaplan")
public class PlaPlanResource {
    @Autowired
    private PlaPlanService plaPlanService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaPlan>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaPlan> list = plaPlanService.selectAll(queryModel);
        return new ResultDto<List<PlaPlan>>(list);
    }

	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaPlan>> index(QueryModel queryModel) {
        List<PlaPlan> list = plaPlanService.selectByModel(queryModel);
        return new ResultDto<List<PlaPlan>>(list);
    }


    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     * 创建人：刘权
     */
    @ApiOperation("清收计划管理列表查询")
    @PostMapping("/queryAll")
    protected ResultDto<List<PlaPlan>> queryAll(@RequestBody QueryModel queryModel) {
        List<PlaPlan> list = plaPlanService.selectByModel(queryModel);
        return new ResultDto<List<PlaPlan>>(list);
    }

    /**
     * @函数名称:showost
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{recoverySerno}")
    protected ResultDto<PlaPlan> show(@PathVariable("recoverySerno") String recoverySerno) {
        PlaPlan plaPlan = plaPlanService.selectByPrimaryKey(recoverySerno);
        return new ResultDto<PlaPlan>(plaPlan);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaPlan> create(@RequestBody PlaPlan plaPlan) throws URISyntaxException {
        plaPlanService.insert(plaPlan);
        return new ResultDto<PlaPlan>(plaPlan);
    }


    /**
     * @函数名称:insert
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("清收计划管理列表新增")
    @PostMapping("/insert")
    protected ResultDto<PlaPlan> insert(@RequestBody PlaPlan plaPlan) throws URISyntaxException {
        plaPlanService.insertByserno(plaPlan);
        return new ResultDto<PlaPlan>(plaPlan);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("清收计划管理列表修改")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaPlan plaPlan) throws URISyntaxException {
        int result = plaPlanService.updateSelective(plaPlan);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("清收计划管理列表删除")
    @PostMapping("/delete/{recoverySerno}")
    protected ResultDto<Integer> delete(@PathVariable("recoverySerno") String recoverySerno) {
        int result = plaPlanService.deleteByPrimaryKey(recoverySerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaPlanService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
