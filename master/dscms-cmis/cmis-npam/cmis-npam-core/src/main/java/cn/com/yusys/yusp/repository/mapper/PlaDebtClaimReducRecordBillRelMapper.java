/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PlaDebtClaimReducRecordBillRel;
import cn.com.yusys.yusp.dto.PlaDebtClaimReducRecordBillRelDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaDebtClaimReducRecordBillRelMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-08 19:44:40
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface PlaDebtClaimReducRecordBillRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    PlaDebtClaimReducRecordBillRel selectByPrimaryKey(@Param("pdcrrbrSerno") String pdcrrbrSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PlaDebtClaimReducRecordBillRel> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insert(PlaDebtClaimReducRecordBillRelDto record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insertSelective(PlaDebtClaimReducRecordBillRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPrimaryKey(PlaDebtClaimReducRecordBillRel record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPrimaryKeySelective(PlaDebtClaimReducRecordBillRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    int deleteByPrimaryKey(PlaDebtClaimReducRecordBillRel record);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectByPdcrrbrSerno
     * @方法描述: 通过业务流水号或者借据编号客户号查询记账借据信息
     * @参数与返回说明:记账借据基本信息
     * @算法描述: 无
     */
    PlaDebtClaimReducRecordBillRel selectByPdcrrbrSerno(Map map);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PlaDebtClaimReducRecordBillRel> selectByPdcrraiSerno(@Param("pdcrraiSerno") String pdcrraiSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询 查询单个金额总和
     * @参数与返回说明:
     * @算法描述: 无
     * 刘权
     */
    Map selectBySum(String pdcrraiSerno);
}