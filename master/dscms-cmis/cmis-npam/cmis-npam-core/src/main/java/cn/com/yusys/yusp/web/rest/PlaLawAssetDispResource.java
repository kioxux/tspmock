/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaLawAssetDisp;
import cn.com.yusys.yusp.service.PlaLawAssetDispService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawAssetDispResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-07 19:23:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plalawassetdisp")
public class PlaLawAssetDispResource {
    @Autowired
    private PlaLawAssetDispService plaLawAssetDispService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaLawAssetDisp>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaLawAssetDisp> list = plaLawAssetDispService.selectAll(queryModel);
        return new ResultDto<List<PlaLawAssetDisp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaLawAssetDisp>> index(QueryModel queryModel) {
        List<PlaLawAssetDisp> list = plaLawAssetDispService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawAssetDisp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pladSerno}")
    protected ResultDto<PlaLawAssetDisp> show(@PathVariable("pladSerno") String pladSerno) {
        PlaLawAssetDisp plaLawAssetDisp = plaLawAssetDispService.selectByPrimaryKey(pladSerno);
        return new ResultDto<PlaLawAssetDisp>(plaLawAssetDisp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaLawAssetDisp> create(@RequestBody PlaLawAssetDisp plaLawAssetDisp) throws URISyntaxException {
        plaLawAssetDispService.insert(plaLawAssetDisp);
        return new ResultDto<PlaLawAssetDisp>(plaLawAssetDisp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaLawAssetDisp plaLawAssetDisp) throws URISyntaxException {
        int result = plaLawAssetDispService.update(plaLawAssetDisp);
        return new ResultDto<Integer>(result);
    }

    /**
     * 批量更新
     * @param plaLawAssetDisp
     * @return
     * @throws URISyntaxException
     */
    @PostMapping("/batchUpdate")
    protected ResultDto<Integer> batchUpdate(@RequestBody List<PlaLawAssetDisp> plaLawAssetDisp) throws URISyntaxException {
        int result = plaLawAssetDispService.batchUpdate(plaLawAssetDisp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pladSerno}")
    protected ResultDto<Integer> delete(@PathVariable("pladSerno") String pladSerno) {
        int result = plaLawAssetDispService.deleteByPrimaryKey(pladSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaLawAssetDispService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertByPlaLawAssetDispService")
    protected ResultDto<Integer> insert(@RequestBody PlaLawAssetDisp plaLawAssetDisp) throws URISyntaxException {
        return plaLawAssetDispService.insertByPlaLawAssetDispService(plaLawAssetDisp);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryPlaLawAssetDisp")
    protected ResultDto<List<PlaLawAssetDisp>> queryPlaLawAssetDisp(@RequestBody QueryModel queryModel) {
        List<PlaLawAssetDisp> list = plaLawAssetDispService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawAssetDisp>>(list);
    }
}
