/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.DefaultDataHandle;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.vo.PlaPlanDetailVo;
import cn.com.yusys.yusp.vo.PlaUncretBadAssetAccListVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaUncretBadAssetAccList;
import cn.com.yusys.yusp.service.PlaUncretBadAssetAccListService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaUncretBadAssetAccListResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 刘权
 * @创建时间: 2021-06-08 15:56:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(value = "非信贷不良资产台账表")
@RestController
@RequestMapping("/api/plauncretbadassetacclist")
public class PlaUncretBadAssetAccListResource {
    private static final Logger log = LoggerFactory.getLogger(PlaUncretBadAssetAccListResource.class);
    @Autowired
    private PlaUncretBadAssetAccListService plaUncretBadAssetAccListService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaUncretBadAssetAccList>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaUncretBadAssetAccList> list = plaUncretBadAssetAccListService.selectAll(queryModel);
        return new ResultDto<List<PlaUncretBadAssetAccList>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaUncretBadAssetAccList>> index(QueryModel queryModel) {
        List<PlaUncretBadAssetAccList> list = plaUncretBadAssetAccListService.selectByModel(queryModel);
        return new ResultDto<List<PlaUncretBadAssetAccList>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     * 刘权
     */
    @ApiOperation("非信贷不良资产台账表全表分页查询")
    @PostMapping("/queryAll")
    protected ResultDto<List<PlaUncretBadAssetAccList>> queryAll(@RequestBody QueryModel queryModel) {
        List<PlaUncretBadAssetAccList> list = plaUncretBadAssetAccListService.selectByModel(queryModel);
        return new ResultDto<List<PlaUncretBadAssetAccList>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pubaalSerno}")
    protected ResultDto<PlaUncretBadAssetAccList> show(@PathVariable("pubaalSerno") String pubaalSerno) {
        PlaUncretBadAssetAccList plaUncretBadAssetAccList = plaUncretBadAssetAccListService.selectByPrimaryKey(pubaalSerno);
        return new ResultDto<PlaUncretBadAssetAccList>(plaUncretBadAssetAccList);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaUncretBadAssetAccList> create(@RequestBody PlaUncretBadAssetAccList plaUncretBadAssetAccList) throws URISyntaxException {
        plaUncretBadAssetAccListService.insert(plaUncretBadAssetAccList);
        return new ResultDto<PlaUncretBadAssetAccList>(plaUncretBadAssetAccList);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaUncretBadAssetAccList plaUncretBadAssetAccList) throws URISyntaxException {
        int result = plaUncretBadAssetAccListService.update(plaUncretBadAssetAccList);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("非信贷不良资产台账表删除")
    @PostMapping("/delete/{pubaalSerno}")
    protected ResultDto<Integer> delete(@PathVariable("pubaalSerno") String pubaalSerno) {
        int result = plaUncretBadAssetAccListService.deleteByPrimaryKey(pubaalSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaUncretBadAssetAccListService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:insert
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:  非信贷不良资产台账表新增
     * @算法描述:
     * 刘权
     */
    @ApiOperation("非信贷不良资产台账表新增")
    @PostMapping("/insert")
    protected ResultDto<PlaUncretBadAssetAccList> insert(@RequestBody PlaUncretBadAssetAccList plaUncretBadAssetAccList) throws URISyntaxException {
        plaUncretBadAssetAccListService.save(plaUncretBadAssetAccList);
        return new ResultDto<PlaUncretBadAssetAccList>(plaUncretBadAssetAccList);
    }

    /**
     * @函数名称: updateByPubaalSerno
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * 刘权
     */
    @ApiOperation("非信贷不良资产台账表修改")
    @PostMapping("/updateByPubaalSerno")
    protected ResultDto<Integer> updateByPubaalSerno(@RequestBody PlaUncretBadAssetAccList plaUncretBadAssetAccList) throws URISyntaxException {
        int result = plaUncretBadAssetAccListService.updateSelective(plaUncretBadAssetAccList);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("非信贷不良资产台账表根据主键回显数据")
    @PostMapping("/showByPubaalSerno")
    protected ResultDto<PlaUncretBadAssetAccList> showByPubaalSerno(@RequestBody String pubaalSerno) {
        PlaUncretBadAssetAccList plaUncretBadAssetAccList = plaUncretBadAssetAccListService.selectByPrimaryKey(pubaalSerno);
        return new ResultDto<PlaUncretBadAssetAccList>(plaUncretBadAssetAccList);
    }


    /**
     * 非信贷不良资产台账列表Excel导入模板
     */
    @PostMapping("/exportPlaUncretBad")
    public ResultDto<ProgressDto> asyncExportPlaUncretBad() {
        ProgressDto progressDto = plaUncretBadAssetAccListService.asyncExportPlaUncretBad();
        return ResultDto.success(progressDto);
    }

    /**
     * Excel数据导入
     * @param fileId Excel文件信息ID
     * @return
     */
    @PostMapping("/importPlaUncretBad")
    public ResultDto<String> asyncImportPlaUncretBad(@RequestParam("fileId") String fileId, @RequestBody Map<String, String> paramsMap) {
        //关联表流水号
        String pubaalSerno = "";
        if(paramsMap.containsKey("pubaalSerno")){
            pubaalSerno = paramsMap.get("pubaalSerno");
        }
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = null;
        try {
            tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
            FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
        } catch (IOException e) {
            log.error(EcnEnum.ECN060004.value,e);
            throw BizException.error(null, EcnEnum.ECN060004.key,EcnEnum.ECN060004.value);
        }
        // 将文件内容导入数据库，StudentScore为导入数据的类
        String finalPubaalSerno = pubaalSerno;
        ExcelUtils.syncImport(PlaUncretBadAssetAccListVo.class, tempFile, new DefaultDataHandle(), ExcelUtils.batchConsumer(dataList -> {
            try {
                return plaUncretBadAssetAccListService.insertPlaUncretBadAssetAccList(dataList, finalPubaalSerno);
            } catch (Exception e) {
                throw BizException.error(null,EcnEnum.ECN069999.key,e.getMessage());
            }
        }), true);
        return ResultDto.success().message("导入成功！");
    }

}
