/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.PlaLawPersonInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaLawAccusedList;
import cn.com.yusys.yusp.service.PlaLawAccusedListService;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawAccusedListResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-05 17:45:58
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/plalawaccusedlist")
public class PlaLawAccusedListResource {
    @Autowired
    private PlaLawAccusedListService plaLawAccusedListService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaLawAccusedList>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaLawAccusedList> list = plaLawAccusedListService.selectAll(queryModel);
        return new ResultDto<List<PlaLawAccusedList>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaLawAccusedList>> index(QueryModel queryModel) {
        List<PlaLawAccusedList> list = plaLawAccusedListService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawAccusedList>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{plalSerno}")
    protected ResultDto<PlaLawAccusedList> show(@PathVariable("plalSerno") String plalSerno) {
        PlaLawAccusedList plaLawAccusedList = plaLawAccusedListService.selectByPrimaryKey(plalSerno);
        return new ResultDto<PlaLawAccusedList>(plaLawAccusedList);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PlaLawAccusedList> create(@RequestBody PlaLawAccusedList plaLawAccusedList) throws URISyntaxException {
        plaLawAccusedListService.insert(plaLawAccusedList);
        return new ResultDto<PlaLawAccusedList>(plaLawAccusedList);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaLawAccusedList plaLawAccusedList) throws URISyntaxException {
        int result = plaLawAccusedListService.update(plaLawAccusedList);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/batchUpdate")
    protected ResultDto<Integer> batchUpdate(@RequestBody List<PlaLawAccusedList> plaLawAccusedList) throws URISyntaxException {
        int result = plaLawAccusedListService.batchUpdate(plaLawAccusedList);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{plalSerno}")
    protected ResultDto<Integer> delete(@PathVariable("plalSerno") String plalSerno) {
        int result = plaLawAccusedListService.deleteByPrimaryKey(plalSerno);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaLawAccusedListService.deleteByIds(ids);
        return new ResultDto<>(result);
    }


    @PostMapping("/batchInsert")
    protected ResultDto<Integer> batchInsert(@RequestBody List<PlaLawAccusedList> list) {
        plaLawAccusedListService.batchInsert(list);
        return new ResultDto<>();
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryPlaLawAccusedList")
    protected ResultDto<List<PlaLawAccusedList>> queryPlaLawAccusedList(@RequestBody QueryModel queryModel) {
        List<PlaLawAccusedList> list = plaLawAccusedListService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawAccusedList>>(list);
    }
}
