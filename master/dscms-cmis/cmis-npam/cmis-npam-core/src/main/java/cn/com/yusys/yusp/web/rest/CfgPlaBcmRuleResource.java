/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.CfgPlaBcmRuleDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgPlaBcmRule;
import cn.com.yusys.yusp.service.CfgPlaBcmRuleService;

/**
 * @项目名称: cmis-npam-core模块
 * @类名称: CfgPlaBcmRuleResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-05-31 15:18:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "催收规则配置表")
@RestController
@RequestMapping("/api/cfgplabcmrule")
public class CfgPlaBcmRuleResource {
    @Autowired
    private CfgPlaBcmRuleService cfgPlaBcmRuleService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgPlaBcmRule>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgPlaBcmRule> list = cfgPlaBcmRuleService.selectAll(queryModel);
        return new ResultDto<List<CfgPlaBcmRule>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgPlaBcmRule>> index(QueryModel queryModel) {
        List<CfgPlaBcmRule> list = cfgPlaBcmRuleService.selectByModel(queryModel);
        return new ResultDto<List<CfgPlaBcmRule>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{ruleId}")
    protected ResultDto<CfgPlaBcmRule> show(@PathVariable("ruleId") String ruleId) {
        CfgPlaBcmRule cfgPlaBcmRule = cfgPlaBcmRuleService.selectByPrimaryKey(ruleId);
        return new ResultDto<CfgPlaBcmRule>(cfgPlaBcmRule);
    }
    /**
     * @函数名称:show
     * @函数描述: 点击修改,查看时，查询单个对象回显数据
     * @参数与返回说明:
     * @算法描述:
     * 刘权
     */
    @PostMapping("/showByRuleId/{ruleId}")
    protected ResultDto<CfgPlaBcmRule> showByRuleId(@PathVariable("ruleId") String ruleId) {
        CfgPlaBcmRule cfgPlaBcmRule = cfgPlaBcmRuleService.selectByPrimaryKey(ruleId);
        return new ResultDto<CfgPlaBcmRule>(cfgPlaBcmRule);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgPlaBcmRule> create(@RequestBody CfgPlaBcmRule cfgPlaBcmRule) throws URISyntaxException {
        cfgPlaBcmRuleService.insert(cfgPlaBcmRule);
        return new ResultDto<CfgPlaBcmRule>(cfgPlaBcmRule);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgPlaBcmRule cfgPlaBcmRule) throws URISyntaxException {
        int result = cfgPlaBcmRuleService.update(cfgPlaBcmRule);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:  根据主键更新非空字段
     * @参数与返回说明:
     * @算法描述:
     * 刘权
     */
    @ApiOperation(value = "催收任务配置列表修改")
    @PostMapping("/updateByRuleId")
    protected ResultDto<Integer> updateByRuleId(@RequestBody CfgPlaBcmRule cfgPlaBcmRule) throws URISyntaxException {
        int result = cfgPlaBcmRuleService.updateSelective(cfgPlaBcmRule);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:update
     * @函数描述: 根据主键修改规则状态
     * @参数与返回说明:
     * @算法描述:
     * 刘权
     */
    @ApiOperation(value = "催收任务配置列表规则状态停用，启用")
    @PostMapping("/updateByRuleStatus")
    protected ResultDto<Integer> updateByRuleStatus(@RequestBody CfgPlaBcmRule cfgPlaBcmRule) throws URISyntaxException {
        int result = cfgPlaBcmRuleService.updateByRuleStatus(cfgPlaBcmRule);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{ruleId}")
    protected ResultDto<Integer> delete(@PathVariable("ruleId") String ruleId) {
        int result = cfgPlaBcmRuleService.deleteByPrimaryKey(ruleId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgPlaBcmRuleService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryCfgPlaBcmRuleList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     * 创建人：周茂伟
     */
    @ApiOperation(value = "催收任务配置列表查询")
    @PostMapping("/queryCfgPlaBcmRuleList")
    protected ResultDto<List<CfgPlaBcmRule>> queryCfgPlaBcmRuleList(@RequestBody QueryModel queryModel) {
        List<CfgPlaBcmRule> list = cfgPlaBcmRuleService.selectByModel(queryModel);
        return new ResultDto<List<CfgPlaBcmRule>>(list);
    }

    /**
     * @函数名称:save
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "催收任务配置保存")
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody CfgPlaBcmRuleDto cfgPlaBcmRuleDto) throws URISyntaxException {
        int count = cfgPlaBcmRuleService.save(cfgPlaBcmRuleDto);
        return new ResultDto<>(count);
    }
}
