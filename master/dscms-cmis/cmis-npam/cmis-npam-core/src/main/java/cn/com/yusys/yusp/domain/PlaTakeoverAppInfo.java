/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaTakeoverAppInfo
 * @类描述: pla_takeover_app_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-23 17:06:06
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_takeover_app_info")
public class PlaTakeoverAppInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 转让业务流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PTAI_SERNO")
    private String ptaiSerno;

    /**
     * 客户编号
     **/
    @Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
    private String cusId;

    /**
     * 客户名称
     **/
    @Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
    private String cusName;

    /**
     * 总笔数
     **/
    @Column(name = "TAKEOVER_COUNT", unique = false, nullable = true, length = 20)
    private String takeoverCount;

    /**
     * 总户数
     **/
    @Column(name = "TOTAL_TAKEOVER_CUS", unique = false, nullable = true, length = 20)
    private String totalTakeoverCus;

    /**
     * 贷款余额
     **/
    @Column(name = "LOAN_BALANCE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal loanBalance;

    /**
     * 拖欠利息总额
     **/
    @Column(name = "TOTAL_TQLX_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalTqlxAmt;

    /**
     * 转让方式
     **/
    @Column(name = "TAKEOVER_MODE", unique = false, nullable = true, length = 5)
    private String takeoverMode;

    /**
     * 转让协议编号
     **/
    @Column(name = "TAKEOVER_AGR_NO", unique = false, nullable = true, length = 40)
    private String takeoverAgrNo;

    /**
     * 转让协议签订日
     **/
    @Column(name = "TAKEOVER_AGR_SIGN_DATE", unique = false, nullable = true, length = 10)
    private String takeoverAgrSignDate;

    /**
     * 转让计价方式
     **/
    @Column(name = "TAKEOVER_PRICE_MODE", unique = false, nullable = true, length = 5)
    private String takeoverPriceMode;

    /**
     * 交易基准日
     **/
    @Column(name = "TRAN_BASE_DATE", unique = false, nullable = true, length = 10)
    private String tranBaseDate;

    /**
     * 币种
     **/
    @Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
    private String curType;

    /**
     * 资产转让金额
     **/
    @Column(name = "TAKEOVER_TOTL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal takeoverTotlAmt;

    /**
     * 评估公司
     **/
    @Column(name = "EVAL_CPRT", unique = false, nullable = true, length = 80)
    private String evalCprt;

    /**
     * 评估价格
     **/
    @Column(name = "EVAL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal evalAmt;

    /**
     * 转让总对价
     **/
    @Column(name = "TAKEOVER_TOTAL_PRICE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal takeoverTotalPrice;

    /**
     * 资产交割日
     **/
    @Column(name = "ASSET_PAY_DATE", unique = false, nullable = true, length = 10)
    private String assetPayDate;

    /**
     * 资金融通方式
     **/
    @Column(name = "FINANCING_MODE", unique = false, nullable = true, length = 5)
    private String financingMode;

    /**
     * 资产转让款支付方式
     **/
    @Column(name = "TAKEOVER_DEFRAY_MODE", unique = false, nullable = true, length = 5)
    private String takeoverDefrayMode;

    /**
     * 资产转让款入账日期
     **/
    @Column(name = "TAKEOVER_PYEE_DATE", unique = false, nullable = true, length = 10)
    private String takeoverPyeeDate;

    /**
     * 资金融通比例
     **/
    @Column(name = "FINANCING_PERC", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal financingPerc;

    /**
     * 资金来源账号
     **/
    @Column(name = "TRANSFER_ACCT", unique = false, nullable = true, length = 40)
    private String transferAcct;

    /**
     * 资金来源账户名称
     **/
    @Column(name = "TRANSFER_ACCT_NAME", unique = false, nullable = true, length = 80)
    private String transferAcctName;

    /**
     * 资金来源子账号账号
     **/
    @Column(name = "TRANSFER_ACCT_CHILD_NO", unique = false, nullable = true, length = 40)
    private String transferAcctChildNo;

    /**
     * 对外付款账号
     **/
    @Column(name = "OUTSIDE_ACCT_NO", unique = false, nullable = true, length = 40)
    private String outsideAcctNo;

    /**
     * 对外付款账号子序号
     **/
    @Column(name = "OUTSIDE_ACCT_CHILD_NO", unique = false, nullable = true, length = 40)
    private String outsideAcctChildNo;

    /**
     * 对外付款账户名称
     **/
    @Column(name = "OUTSIDE_ACCT_NAME", unique = false, nullable = true, length = 80)
    private String outsideAcctName;

    /**
     * 交易对手类型
     **/
    @Column(name = "TOPP_TYPE", unique = false, nullable = true, length = 5)
    private String toppType;

    /**
     * 交易对手名称
     **/
    @Column(name = "TOPP_NAME", unique = false, nullable = true, length = 80)
    private String toppName;

    /**
     * 交易对手账户名称
     **/
    @Column(name = "TOPP_ACCT_NAME", unique = false, nullable = true, length = 80)
    private String toppAcctName;

    /**
     * 交易对手账号
     **/
    @Column(name = "TOPP_ACCT_NO", unique = false, nullable = true, length = 40)
    private String toppAcctNo;

    /**
     * 交易对手账号子序号
     **/
    @Column(name = "TOPP_ACCT_CHILD_NO", unique = false, nullable = true, length = 40)
    private String toppAcctChildNo;

    /**
     * 账号开户行行号
     **/
    @Column(name = "TRANSFER_ACCT_ACCTB", unique = false, nullable = true, length = 40)
    private String transferAcctAcctb;

    /**
     * 账号客户行行名
     **/
    @Column(name = "TRANSFER_ACCT_ACCTB_NAME", unique = false, nullable = true, length = 80)
    private String transferAcctAcctbName;

    /**
     * 封包日期
     **/
    @Column(name = "PACKAGE_DATE", unique = false, nullable = true, length = 10)
    private String packageDate;

    /**
     * 付款周期
     **/
    @Column(name = "PAY_CYCLE", unique = false, nullable = true, length = 20)
    private String payCycle;

    /**
     * 转让类型
     **/
    @Column(name = "TRANSFER_TYPE", unique = false, nullable = true, length = 5)
    private String transferType;

    /**
     * 登记状态
     **/
    @Column(name = "REGI_STATUS", unique = false, nullable = true, length = 5)
    private String regiStatus;

    /**
     * 记账状态
     **/
    @Column(name = "RECORD_STATUS", unique = false, nullable = true, length = 5)
    private String recordStatus;

    /**
     * 记账日期
     **/
    @Column(name = "RECORD_DATE", unique = false, nullable = true, length = 10)
    private String recordDate;

    /**
     * 登记交易流水号
     **/
    @Column(name = "REGI_TRAN_SERNO", unique = false, nullable = true, length = 40)
    private String regiTranSerno;

    /**
     * 登记交易时间
     **/
    @Column(name = "REGI_TRAN_TIME", unique = false, nullable = true, length = 19)
    private String regiTranTime;

    /**
     * 记账交易流水号
     **/
    @Column(name = "RECORD_TRAN_SERNO", unique = false, nullable = true, length = 40)
    private String recordTranSerno;

    /**
     * 记账交易时间
     **/
    @Column(name = "RECORD_TRAN_TIME", unique = false, nullable = true, length = 19)
    private String recordTranTime;

    /**
     * 登记冲正交易流水号
     **/
    @Column(name = "REGI_BACK_SERNO", unique = false, nullable = true, length = 40)
    private String regiBackSerno;

    /**
     * 登记冲正交易时间
     **/
    @Column(name = "REGI_BACK_TIME", unique = false, nullable = true, length = 19)
    private String regiBackTime;

    /**
     * 记账冲正交易流水号
     **/
    @Column(name = "RECORD_BACK_SERNO", unique = false, nullable = true, length = 40)
    private String recordBackSerno;

    /**
     * 记账冲正交易时间
     **/
    @Column(name = "RECORD_BACK_TIME", unique = false, nullable = true, length = 19)
    private String recordBackTime;

    /**
     * 备注
     **/
    @Column(name = "MEMO", unique = false, nullable = true, length = 2000)
    private String memo;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

	public PlaTakeoverAppInfo() {
		// default implementation ignored
	}


    /**
     * @param ptaiSerno
     */
    public void setPtaiSerno(String ptaiSerno) {
        this.ptaiSerno = ptaiSerno;
    }

    /**
     * @return ptaiSerno
     */
    public String getPtaiSerno() {
        return this.ptaiSerno;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param takeoverCount
     */
    public void setTakeoverCount(String takeoverCount) {
        this.takeoverCount = takeoverCount;
    }

    /**
     * @return takeoverCount
     */
    public String getTakeoverCount() {
        return this.takeoverCount;
    }

    /**
     * @param totalTakeoverCus
     */
    public void setTotalTakeoverCus(String totalTakeoverCus) {
        this.totalTakeoverCus = totalTakeoverCus;
    }

    /**
     * @return totalTakeoverCus
     */
    public String getTotalTakeoverCus() {
        return this.totalTakeoverCus;
    }

    /**
     * @param loanBalance
     */
    public void setLoanBalance(java.math.BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    /**
     * @return loanBalance
     */
    public java.math.BigDecimal getLoanBalance() {
        return this.loanBalance;
    }

    /**
     * @param totalTqlxAmt
     */
    public void setTotalTqlxAmt(java.math.BigDecimal totalTqlxAmt) {
        this.totalTqlxAmt = totalTqlxAmt;
    }

    /**
     * @return totalTqlxAmt
     */
    public java.math.BigDecimal getTotalTqlxAmt() {
        return this.totalTqlxAmt;
    }

    /**
     * @param takeoverMode
     */
    public void setTakeoverMode(String takeoverMode) {
        this.takeoverMode = takeoverMode;
    }

    /**
     * @return takeoverMode
     */
    public String getTakeoverMode() {
        return this.takeoverMode;
    }

    /**
     * @param takeoverAgrNo
     */
    public void setTakeoverAgrNo(String takeoverAgrNo) {
        this.takeoverAgrNo = takeoverAgrNo;
    }

    /**
     * @return takeoverAgrNo
     */
    public String getTakeoverAgrNo() {
        return this.takeoverAgrNo;
    }

    /**
     * @param takeoverAgrSignDate
     */
    public void setTakeoverAgrSignDate(String takeoverAgrSignDate) {
        this.takeoverAgrSignDate = takeoverAgrSignDate;
    }

    /**
     * @return takeoverAgrSignDate
     */
    public String getTakeoverAgrSignDate() {
        return this.takeoverAgrSignDate;
    }

    /**
     * @param takeoverPriceMode
     */
    public void setTakeoverPriceMode(String takeoverPriceMode) {
        this.takeoverPriceMode = takeoverPriceMode;
    }

    /**
     * @return takeoverPriceMode
     */
    public String getTakeoverPriceMode() {
        return this.takeoverPriceMode;
    }

    /**
     * @param tranBaseDate
     */
    public void setTranBaseDate(String tranBaseDate) {
        this.tranBaseDate = tranBaseDate;
    }

    /**
     * @return tranBaseDate
     */
    public String getTranBaseDate() {
        return this.tranBaseDate;
    }

    /**
     * @param curType
     */
    public void setCurType(String curType) {
        this.curType = curType;
    }

    /**
     * @return curType
     */
    public String getCurType() {
        return this.curType;
    }

    /**
     * @param takeoverTotlAmt
     */
    public void setTakeoverTotlAmt(java.math.BigDecimal takeoverTotlAmt) {
        this.takeoverTotlAmt = takeoverTotlAmt;
    }

    /**
     * @return takeoverTotlAmt
     */
    public java.math.BigDecimal getTakeoverTotlAmt() {
        return this.takeoverTotlAmt;
    }

    /**
     * @param evalCprt
     */
    public void setEvalCprt(String evalCprt) {
        this.evalCprt = evalCprt;
    }

    /**
     * @return evalCprt
     */
    public String getEvalCprt() {
        return this.evalCprt;
    }

    /**
     * @param evalAmt
     */
    public void setEvalAmt(java.math.BigDecimal evalAmt) {
        this.evalAmt = evalAmt;
    }

    /**
     * @return evalAmt
     */
    public java.math.BigDecimal getEvalAmt() {
        return this.evalAmt;
    }

    /**
     * @param takeoverTotalPrice
     */
    public void setTakeoverTotalPrice(java.math.BigDecimal takeoverTotalPrice) {
        this.takeoverTotalPrice = takeoverTotalPrice;
    }

    /**
     * @return takeoverTotalPrice
     */
    public java.math.BigDecimal getTakeoverTotalPrice() {
        return this.takeoverTotalPrice;
    }

    /**
     * @param assetPayDate
     */
    public void setAssetPayDate(String assetPayDate) {
        this.assetPayDate = assetPayDate;
    }

    /**
     * @return assetPayDate
     */
    public String getAssetPayDate() {
        return this.assetPayDate;
    }

    /**
     * @param financingMode
     */
    public void setFinancingMode(String financingMode) {
        this.financingMode = financingMode;
    }

    /**
     * @return financingMode
     */
    public String getFinancingMode() {
        return this.financingMode;
    }

    /**
     * @param takeoverDefrayMode
     */
    public void setTakeoverDefrayMode(String takeoverDefrayMode) {
        this.takeoverDefrayMode = takeoverDefrayMode;
    }

    /**
     * @return takeoverDefrayMode
     */
    public String getTakeoverDefrayMode() {
        return this.takeoverDefrayMode;
    }

    /**
     * @param takeoverPyeeDate
     */
    public void setTakeoverPyeeDate(String takeoverPyeeDate) {
        this.takeoverPyeeDate = takeoverPyeeDate;
    }

    /**
     * @return takeoverPyeeDate
     */
    public String getTakeoverPyeeDate() {
        return this.takeoverPyeeDate;
    }

    /**
     * @param financingPerc
     */
    public void setFinancingPerc(java.math.BigDecimal financingPerc) {
        this.financingPerc = financingPerc;
    }

    /**
     * @return financingPerc
     */
    public java.math.BigDecimal getFinancingPerc() {
        return this.financingPerc;
    }

    /**
     * @param transferAcct
     */
    public void setTransferAcct(String transferAcct) {
        this.transferAcct = transferAcct;
    }

    /**
     * @return transferAcct
     */
    public String getTransferAcct() {
        return this.transferAcct;
    }

    /**
     * @param transferAcctName
     */
    public void setTransferAcctName(String transferAcctName) {
        this.transferAcctName = transferAcctName;
    }

    /**
     * @return transferAcctName
     */
    public String getTransferAcctName() {
        return this.transferAcctName;
    }

    /**
     * @param transferAcctChildNo
     */
    public void setTransferAcctChildNo(String transferAcctChildNo) {
        this.transferAcctChildNo = transferAcctChildNo;
    }

    /**
     * @return transferAcctChildNo
     */
    public String getTransferAcctChildNo() {
        return this.transferAcctChildNo;
    }

    /**
     * @param outsideAcctNo
     */
    public void setOutsideAcctNo(String outsideAcctNo) {
        this.outsideAcctNo = outsideAcctNo;
    }

    /**
     * @return outsideAcctNo
     */
    public String getOutsideAcctNo() {
        return this.outsideAcctNo;
    }

    /**
     * @param outsideAcctChildNo
     */
    public void setOutsideAcctChildNo(String outsideAcctChildNo) {
        this.outsideAcctChildNo = outsideAcctChildNo;
    }

    /**
     * @return outsideAcctChildNo
     */
    public String getOutsideAcctChildNo() {
        return this.outsideAcctChildNo;
    }

    /**
     * @param outsideAcctName
     */
    public void setOutsideAcctName(String outsideAcctName) {
        this.outsideAcctName = outsideAcctName;
    }

    /**
     * @return outsideAcctName
     */
    public String getOutsideAcctName() {
        return this.outsideAcctName;
    }

    /**
     * @param toppType
     */
    public void setToppType(String toppType) {
        this.toppType = toppType;
    }

    /**
     * @return toppType
     */
    public String getToppType() {
        return this.toppType;
    }

    /**
     * @param toppName
     */
    public void setToppName(String toppName) {
        this.toppName = toppName;
    }

    /**
     * @return toppName
     */
    public String getToppName() {
        return this.toppName;
    }

    /**
     * @param toppAcctName
     */
    public void setToppAcctName(String toppAcctName) {
        this.toppAcctName = toppAcctName;
    }

    /**
     * @return toppAcctName
     */
    public String getToppAcctName() {
        return this.toppAcctName;
    }

    /**
     * @param toppAcctNo
     */
    public void setToppAcctNo(String toppAcctNo) {
        this.toppAcctNo = toppAcctNo;
    }

    /**
     * @return toppAcctNo
     */
    public String getToppAcctNo() {
        return this.toppAcctNo;
    }

    /**
     * @param toppAcctChildNo
     */
    public void setToppAcctChildNo(String toppAcctChildNo) {
        this.toppAcctChildNo = toppAcctChildNo;
    }

    /**
     * @return toppAcctChildNo
     */
    public String getToppAcctChildNo() {
        return this.toppAcctChildNo;
    }

    /**
     * @param transferAcctAcctb
     */
    public void setTransferAcctAcctb(String transferAcctAcctb) {
        this.transferAcctAcctb = transferAcctAcctb;
    }

    /**
     * @return transferAcctAcctb
     */
    public String getTransferAcctAcctb() {
        return this.transferAcctAcctb;
    }

    /**
     * @param transferAcctAcctbName
     */
    public void setTransferAcctAcctbName(String transferAcctAcctbName) {
        this.transferAcctAcctbName = transferAcctAcctbName;
    }

    /**
     * @return transferAcctAcctbName
     */
    public String getTransferAcctAcctbName() {
        return this.transferAcctAcctbName;
    }

    /**
     * @param packageDate
     */
    public void setPackageDate(String packageDate) {
        this.packageDate = packageDate;
    }

    /**
     * @return packageDate
     */
    public String getPackageDate() {
        return this.packageDate;
    }

    /**
     * @param payCycle
     */
    public void setPayCycle(String payCycle) {
        this.payCycle = payCycle;
    }

    /**
     * @return payCycle
     */
    public String getPayCycle() {
        return this.payCycle;
    }

    /**
     * @param transferType
     */
    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    /**
     * @return transferType
     */
    public String getTransferType() {
        return this.transferType;
    }

    /**
     * @param regiStatus
     */
    public void setRegiStatus(String regiStatus) {
        this.regiStatus = regiStatus;
    }

    /**
     * @return regiStatus
     */
    public String getRegiStatus() {
        return this.regiStatus;
    }

    /**
     * @param recordStatus
     */
    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    /**
     * @return recordStatus
     */
    public String getRecordStatus() {
        return this.recordStatus;
    }

    /**
     * @param recordDate
     */
    public void setRecordDate(String recordDate) {
        this.recordDate = recordDate;
    }

    /**
     * @return recordDate
     */
    public String getRecordDate() {
        return this.recordDate;
    }

    /**
     * @param regiTranSerno
     */
    public void setRegiTranSerno(String regiTranSerno) {
        this.regiTranSerno = regiTranSerno;
    }

    /**
     * @return regiTranSerno
     */
    public String getRegiTranSerno() {
        return this.regiTranSerno;
    }

    public String getRegiTranTime() {
        return regiTranTime;
    }

    public void setRegiTranTime(String regiTranTime) {
        this.regiTranTime = regiTranTime;
    }

    public String getRecordTranTime() {
        return recordTranTime;
    }

    public void setRecordTranTime(String recordTranTime) {
        this.recordTranTime = recordTranTime;
    }

    public String getRegiBackTime() {
        return regiBackTime;
    }

    public void setRegiBackTime(String regiBackTime) {
        this.regiBackTime = regiBackTime;
    }

    public String getRecordBackTime() {
        return recordBackTime;
    }

    public void setRecordBackTime(String recordBackTime) {
        this.recordBackTime = recordBackTime;
    }

    /**
     * @param recordTranSerno
     */
    public void setRecordTranSerno(String recordTranSerno) {
        this.recordTranSerno = recordTranSerno;
    }

    /**
     * @return recordTranSerno
     */
    public String getRecordTranSerno() {
        return this.recordTranSerno;
    }



    /**
     * @param regiBackSerno
     */
    public void setRegiBackSerno(String regiBackSerno) {
        this.regiBackSerno = regiBackSerno;
    }

    /**
     * @return regiBackSerno
     */
    public String getRegiBackSerno() {
        return this.regiBackSerno;
    }



    /**
     * @param recordBackSerno
     */
    public void setRecordBackSerno(String recordBackSerno) {
        this.recordBackSerno = recordBackSerno;
    }

    /**
     * @return recordBackSerno
     */
    public String getRecordBackSerno() {
        return this.recordBackSerno;
    }



    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}