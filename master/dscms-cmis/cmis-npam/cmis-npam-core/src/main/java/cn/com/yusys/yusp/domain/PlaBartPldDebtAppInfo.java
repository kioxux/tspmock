/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBartPldDebtAppInfo
 * @类描述: pla_bart_pld_debt_app_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-23 16:51:56
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_bart_pld_debt_app_info")
public class PlaBartPldDebtAppInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 抵债业务流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PDRAI_SERNO")
    private String pdraiSerno;

    /**
     * 客户编号
     **/
    @Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
    private String cusId;

    /**
     * 客户名称
     **/
    @Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
    private String cusName;

    /**
     * 贷款金额
     **/
    @Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal loanAmt;

    /**
     * 贷款余额
     **/
    @Column(name = "LOAN_BALANCE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal loanBalance;

    /**
     * 拖欠利息总额
     **/
    @Column(name = "TOTAL_TQLX_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalTqlxAmt;

    /**
     * 是否我行抵押物
     **/
    @Column(name = "IS_BANK_PLDIMN", unique = false, nullable = true, length = 5)
    private String isBankPldimn;

    /**
     * 抵质押物编号
     **/
    @Column(name = "PLDIMN_NO", unique = false, nullable = true, length = 40)
    private String pldimnNo;

    /**
     * 抵债物类型
     **/
    @Column(name = "RESIST_DEBT_TYPE", unique = false, nullable = true, length = 10)
    private String resistDebtType;

    /**
     * 最高可抵押金额
     **/
    @Column(name = "HIGH_PLD_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal highPldAmt;

    /**
     * 抵质押物名称
     **/
    @Column(name = "PLDIMN_MEMO", unique = false, nullable = true, length = 80)
    private String pldimnMemo;

    /**
     * 抵质押人名称
     **/
    @Column(name = "PLDIMN_PERSON", unique = false, nullable = true, length = 80)
    private String pldimnPerson;

    /**
     * 权属证件号码
     **/
    @Column(name = "AUTHO_CERT_CODE", unique = false, nullable = true, length = 20)
    private String authoCertCode;

    /**
     * 抵押物状态
     **/
    @Column(name = "PLDIMN_STATUS", unique = false, nullable = true, length = 5)
    private String pldimnStatus;

    /**
     * 抵债物取得方式
     **/
    @Column(name = "RESIST_DEBT_GET_MODE", unique = false, nullable = true, length = 5)
    private String resistDebtGetMode;

    /**
     * 债务人名称
     **/
    @Column(name = "DEBTOR_NAME", unique = false, nullable = true, length = 80)
    private String debtorName;

    /**
     * 债务金额
     **/
    @Column(name = "DEBT_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal debtAmt;

    /**
     * 数量
     **/
    @Column(name = "QNT", unique = false, nullable = true, length = 10)
    private String qnt;

    /**
     * 存放地址
     **/
    @Column(name = "DEPO_ADDR", unique = false, nullable = true, length = 80)
    private String depoAddr;

    /**
     * 评估价值
     **/
    @Column(name = "EVAL_VALUE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal evalValue;

    /**
     * 接收价值（法院裁定价/协议价值)
     **/
    @Column(name = "RCV_VALUE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal rcvValue;

    /**
     * 法律文书编号
     **/
    @Column(name = "LEGAL_INSTRU_NO", unique = false, nullable = true, length = 40)
    private String legalInstruNo;

    /**
     * 法律文书名称
     **/
    @Column(name = "LEGAL_INSTRU_NAME", unique = false, nullable = true, length = 80)
    private String legalInstruName;

    /**
     * 法院/仲裁机构名称
     **/
    @Column(name = "COURT", unique = false, nullable = true, length = 80)
    private String court;

    /**
     * 是否出租
     **/
    @Column(name = "IS_RENT", unique = false, nullable = true, length = 5)
    private String isRent;

    /**
     * 租赁起始日
     **/
    @Column(name = "RENT_START_DATE", unique = false, nullable = true, length = 20)
    private String rentStartDate;

    /**
     * 租赁到期日
     **/
    @Column(name = "RENT_END_DATE", unique = false, nullable = true, length = 20)
    private String rentEndDate;

    /**
     * 法律文书生效日期
     **/
    @Column(name = "LEGAL_INSTRU_INURE_DATE", unique = false, nullable = true, length = 20)
    private String legalInstruInureDate;

    /**
     * 法律文书落款日期
     **/
    @Column(name = "LEGAL_INSTRU_END_DATE", unique = false, nullable = true, length = 20)
    private String legalInstruEndDate;

    /**
     * 账务机构编号
     **/
    @Column(name = "FINA_BR_ID", unique = false, nullable = true, length = 40)
    private String finaBrId;

    /**
     * 账务机构名称
     **/
    @Column(name = "FINA_BR_NAME", unique = false, nullable = true, length = 80)
    private String finaBrName;

    /**
     * 审批状态
     **/
    @Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
    private String approveStatus;

    /**
     * 登记状态
     **/
    @Column(name = "REGI_STATUS", unique = false, nullable = true, length = 5)
    private String regiStatus;

    /**
     * 记账状态
     **/
    @Column(name = "RECORD_STATUS", unique = false, nullable = true, length = 5)
    private String recordStatus;

    /**
     * 记账日期
     **/
    @Column(name = "RECORD_DATE", unique = false, nullable = true, length = 10)
    private String recordDate;

    /**
     * 登记交易流水号
     **/
    @Column(name = "REGI_TRAN_SERNO", unique = false, nullable = true, length = 40)
    private String regiTranSerno;

    /**
     * 登记交易时间
     **/
    @Column(name = "REGI_TRAN_TIME", unique = false, nullable = true, length = 19)
    private String regiTranTime;

    /**
     * 记账交易流水号
     **/
    @Column(name = "RECORD_TRAN_SERNO", unique = false, nullable = true, length = 40)
    private String recordTranSerno;

    /**
     * 记账交易时间
     **/
    @Column(name = "RECORD_TRAN_TIME", unique = false, nullable = true, length = 19)
    private String recordTranTime;

    /**
     * 登记冲正交易流水号
     **/
    @Column(name = "REGI_BACK_SERNO", unique = false, nullable = true, length = 40)
    private String regiBackSerno;

    /**
     * 登记冲正交易时间
     **/
    @Column(name = "REGI_BACK_TIME", unique = false, nullable = true, length = 19)
    private String regiBackTime;

    /**
     * 记账冲正交易流水号
     **/
    @Column(name = "RECORD_BACK_SERNO", unique = false, nullable = true, length = 40)
    private String recordBackSerno;

    /**
     * 记账冲正交易时间
     **/
    @Column(name = "RECORD_BACK_TIME", unique = false, nullable = true, length = 19)
    private String recordBackTime;

    /**
     * 责任人
     **/
    @Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
    private String managerId;

    /**
     * 责任机构
     **/
    @Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
    private String managerBrId;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaBartPldDebtAppInfo() {
        // default implementation ignored
    }


    /**
     * @param pdraiSerno
     */
    public void setPdraiSerno(String pdraiSerno) {
        this.pdraiSerno = pdraiSerno;
    }

    /**
     * @return pdraiSerno
     */
    public String getPdraiSerno() {
        return this.pdraiSerno;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param loanAmt
     */
    public void setLoanAmt(java.math.BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    /**
     * @return loanAmt
     */
    public java.math.BigDecimal getLoanAmt() {
        return this.loanAmt;
    }

    /**
     * @param loanBalance
     */
    public void setLoanBalance(java.math.BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    /**
     * @return loanBalance
     */
    public java.math.BigDecimal getLoanBalance() {
        return this.loanBalance;
    }

    /**
     * @param totalTqlxAmt
     */
    public void setTotalTqlxAmt(java.math.BigDecimal totalTqlxAmt) {
        this.totalTqlxAmt = totalTqlxAmt;
    }

    /**
     * @return totalTqlxAmt
     */
    public java.math.BigDecimal getTotalTqlxAmt() {
        return this.totalTqlxAmt;
    }

    /**
     * @param isBankPldimn
     */
    public void setIsBankPldimn(String isBankPldimn) {
        this.isBankPldimn = isBankPldimn;
    }

    /**
     * @return isBankPldimn
     */
    public String getIsBankPldimn() {
        return this.isBankPldimn;
    }

    /**
     * @param pldimnNo
     */
    public void setPldimnNo(String pldimnNo) {
        this.pldimnNo = pldimnNo;
    }

    /**
     * @return pldimnNo
     */
    public String getPldimnNo() {
        return this.pldimnNo;
    }

    /**
     * @param resistDebtType
     */
    public void setResistDebtType(String resistDebtType) {
        this.resistDebtType = resistDebtType;
    }

    /**
     * @return resistDebtType
     */
    public String getResistDebtType() {
        return this.resistDebtType;
    }

    /**
     * @param highPldAmt
     */
    public void setHighPldAmt(java.math.BigDecimal highPldAmt) {
        this.highPldAmt = highPldAmt;
    }

    /**
     * @return highPldAmt
     */
    public java.math.BigDecimal getHighPldAmt() {
        return this.highPldAmt;
    }

    /**
     * @param pldimnMemo
     */
    public void setPldimnMemo(String pldimnMemo) {
        this.pldimnMemo = pldimnMemo;
    }

    /**
     * @return pldimnMemo
     */
    public String getPldimnMemo() {
        return this.pldimnMemo;
    }

    /**
     * @param pldimnPerson
     */
    public void setPldimnPerson(String pldimnPerson) {
        this.pldimnPerson = pldimnPerson;
    }

    /**
     * @return pldimnPerson
     */
    public String getPldimnPerson() {
        return this.pldimnPerson;
    }

    /**
     * @param authoCertCode
     */
    public void setAuthoCertCode(String authoCertCode) {
        this.authoCertCode = authoCertCode;
    }

    /**
     * @return authoCertCode
     */
    public String getAuthoCertCode() {
        return this.authoCertCode;
    }

    /**
     * @param pldimnStatus
     */
    public void setPldimnStatus(String pldimnStatus) {
        this.pldimnStatus = pldimnStatus;
    }

    /**
     * @return pldimnStatus
     */
    public String getPldimnStatus() {
        return this.pldimnStatus;
    }

    /**
     * @param resistDebtGetMode
     */
    public void setResistDebtGetMode(String resistDebtGetMode) {
        this.resistDebtGetMode = resistDebtGetMode;
    }

    /**
     * @return resistDebtGetMode
     */
    public String getResistDebtGetMode() {
        return this.resistDebtGetMode;
    }

    /**
     * @param debtorName
     */
    public void setDebtorName(String debtorName) {
        this.debtorName = debtorName;
    }

    /**
     * @return debtorName
     */
    public String getDebtorName() {
        return this.debtorName;
    }

    /**
     * @param debtAmt
     */
    public void setDebtAmt(java.math.BigDecimal debtAmt) {
        this.debtAmt = debtAmt;
    }

    /**
     * @return debtAmt
     */
    public java.math.BigDecimal getDebtAmt() {
        return this.debtAmt;
    }

    /**
     * @param qnt
     */
    public void setQnt(String qnt) {
        this.qnt = qnt;
    }

    /**
     * @return qnt
     */
    public String getQnt() {
        return this.qnt;
    }

    /**
     * @param depoAddr
     */
    public void setDepoAddr(String depoAddr) {
        this.depoAddr = depoAddr;
    }

    /**
     * @return depoAddr
     */
    public String getDepoAddr() {
        return this.depoAddr;
    }

    /**
     * @param evalValue
     */
    public void setEvalValue(java.math.BigDecimal evalValue) {
        this.evalValue = evalValue;
    }

    /**
     * @return evalValue
     */
    public java.math.BigDecimal getEvalValue() {
        return this.evalValue;
    }

    /**
     * @param rcvValue
     */
    public void setRcvValue(java.math.BigDecimal rcvValue) {
        this.rcvValue = rcvValue;
    }

    /**
     * @return rcvValue
     */
    public java.math.BigDecimal getRcvValue() {
        return this.rcvValue;
    }

    /**
     * @param legalInstruNo
     */
    public void setLegalInstruNo(String legalInstruNo) {
        this.legalInstruNo = legalInstruNo;
    }

    /**
     * @return legalInstruNo
     */
    public String getLegalInstruNo() {
        return this.legalInstruNo;
    }

    /**
     * @param legalInstruName
     */
    public void setLegalInstruName(String legalInstruName) {
        this.legalInstruName = legalInstruName;
    }

    /**
     * @return legalInstruName
     */
    public String getLegalInstruName() {
        return this.legalInstruName;
    }

    /**
     * @param court
     */
    public void setCourt(String court) {
        this.court = court;
    }

    /**
     * @return court
     */
    public String getCourt() {
        return this.court;
    }

    /**
     * @param isRent
     */
    public void setIsRent(String isRent) {
        this.isRent = isRent;
    }

    /**
     * @return isRent
     */
    public String getIsRent() {
        return this.isRent;
    }

    /**
     * @param rentStartDate
     */
    public void setRentStartDate(String rentStartDate) {
        this.rentStartDate = rentStartDate;
    }

    /**
     * @return rentStartDate
     */
    public String getRentStartDate() {
        return this.rentStartDate;
    }

    /**
     * @param rentEndDate
     */
    public void setRentEndDate(String rentEndDate) {
        this.rentEndDate = rentEndDate;
    }

    /**
     * @return rentEndDate
     */
    public String getRentEndDate() {
        return this.rentEndDate;
    }

    /**
     * @param legalInstruInureDate
     */
    public void setLegalInstruInureDate(String legalInstruInureDate) {
        this.legalInstruInureDate = legalInstruInureDate;
    }

    /**
     * @return legalInstruInureDate
     */
    public String getLegalInstruInureDate() {
        return this.legalInstruInureDate;
    }

    /**
     * @param legalInstruEndDate
     */
    public void setLegalInstruEndDate(String legalInstruEndDate) {
        this.legalInstruEndDate = legalInstruEndDate;
    }

    /**
     * @return legalInstruEndDate
     */
    public String getLegalInstruEndDate() {
        return this.legalInstruEndDate;
    }

    /**
     * @param finaBrId
     */
    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId;
    }

    /**
     * @return finaBrId
     */
    public String getFinaBrId() {
        return this.finaBrId;
    }

    /**
     * @param finaBrName
     */
    public void setFinaBrName(String finaBrName) {
        this.finaBrName = finaBrName;
    }

    /**
     * @return finaBrName
     */
    public String getFinaBrName() {
        return this.finaBrName;
    }

    /**
     * @param approveStatus
     */
    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    /**
     * @return approveStatus
     */
    public String getApproveStatus() {
        return this.approveStatus;
    }

    /**
     * @param regiStatus
     */
    public void setRegiStatus(String regiStatus) {
        this.regiStatus = regiStatus;
    }

    /**
     * @return regiStatus
     */
    public String getRegiStatus() {
        return this.regiStatus;
    }

    /**
     * @param recordStatus
     */
    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    /**
     * @return recordStatus
     */
    public String getRecordStatus() {
        return this.recordStatus;
    }

    /**
     * @param recordDate
     */
    public void setRecordDate(String recordDate) {
        this.recordDate = recordDate;
    }

    /**
     * @return recordDate
     */
    public String getRecordDate() {
        return this.recordDate;
    }

    /**
     * @param regiTranSerno
     */
    public void setRegiTranSerno(String regiTranSerno) {
        this.regiTranSerno = regiTranSerno;
    }

    /**
     * @return regiTranSerno
     */
    public String getRegiTranSerno() {
        return this.regiTranSerno;
    }



    /**
     * @param recordTranSerno
     */
    public void setRecordTranSerno(String recordTranSerno) {
        this.recordTranSerno = recordTranSerno;
    }

    /**
     * @return recordTranSerno
     */
    public String getRecordTranSerno() {
        return this.recordTranSerno;
    }



    /**
     * @param regiBackSerno
     */
    public void setRegiBackSerno(String regiBackSerno) {
        this.regiBackSerno = regiBackSerno;
    }

    /**
     * @return regiBackSerno
     */
    public String getRegiBackSerno() {
        return this.regiBackSerno;
    }


    /**
     * @param recordBackSerno
     */
    public void setRecordBackSerno(String recordBackSerno) {
        this.recordBackSerno = recordBackSerno;
    }

    /**
     * @return recordBackSerno
     */
    public String getRecordBackSerno() {
        return this.recordBackSerno;
    }

    public String getRegiTranTime() {
        return regiTranTime;
    }

    public void setRegiTranTime(String regiTranTime) {
        this.regiTranTime = regiTranTime;
    }

    public String getRecordTranTime() {
        return recordTranTime;
    }

    public void setRecordTranTime(String recordTranTime) {
        this.recordTranTime = recordTranTime;
    }

    public String getRegiBackTime() {
        return regiBackTime;
    }

    public void setRegiBackTime(String regiBackTime) {
        this.regiBackTime = regiBackTime;
    }

    public String getRecordBackTime() {
        return recordBackTime;
    }

    public void setRecordBackTime(String recordBackTime) {
        this.recordBackTime = recordBackTime;
    }

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    /**
     * @return managerId
     */
    public String getManagerId() {
        return this.managerId;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    /**
     * @return managerBrId
     */
    public String getManagerBrId() {
        return this.managerBrId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}