package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.CfgPlaBcmParam;
import cn.com.yusys.yusp.domain.PlaLawPaymentInfo;

import java.io.Serializable;
import java.util.List;

public class PlaLawPaymentInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;

    //受偿清单信息表
    private List<PlaLawPaymentInfo> plaLawPaymentInfo;

    public List<PlaLawPaymentInfo> getPlaLawPaymentInfo() {
        return plaLawPaymentInfo;
    }

    public void setPlaLawPaymentInfo(List<PlaLawPaymentInfo> plaLawPaymentInfo) {
        this.plaLawPaymentInfo = plaLawPaymentInfo;
    }

}
