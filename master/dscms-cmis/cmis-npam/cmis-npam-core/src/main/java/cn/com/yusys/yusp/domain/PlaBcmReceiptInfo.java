/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaBcmReceiptInfo
 * @类描述: pla_bcm_receipt_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-04 11:28:18
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_bcm_receipt_info")
public class PlaBcmReceiptInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 催收回执流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PBREI_SERNO")
    private String pbreiSerno;

    /**
     * 任务编号
     **/
    @Column(name = "TASK_NO", unique = false, nullable = true, length = 40)
    private String taskNo;

    /**
     * 催收方式
     **/
    @Column(name = "BCM_MODE", unique = false, nullable = true, length = 5)
    private String bcmMode;

    /**
     * 催收日期
     **/
    @Column(name = "BCM_DATE", unique = false, nullable = true, length = 10)
    private String bcmDate;

    /**
     * 催收对象类型
     **/
    @Column(name = "BCM_OBJ_TYPE", unique = false, nullable = true, length = 5)
    private String bcmObjType;

    /**
     * 催收对象名称
     **/
    @Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
    private String cusName;

    /**
     * 联系方式
     **/
    @Column(name = "TEL", unique = false, nullable = true, length = 20)
    private String tel;

    /**
     * 催收结果
     **/
    @Column(name = "BCM_RESULT", unique = false, nullable = true, length = 5)
    private String bcmResult;

    /**
     * 催收情况说明
     **/
    @Column(name = "BCM_CASE_DESC", unique = false, nullable = true, length = 2000)
    private String bcmCaseDesc;

    /**
     * 是否回执
     **/
    @Column(name = "IS_RCP", unique = false, nullable = true, length = 5)
    private String isRcp;

    /**
     * 回执日期
     **/
    @Column(name = "RCP_DATE", unique = false, nullable = true, length = 10)
    private String rcpDate;

    /**
     * 回执内容
     **/
    @Column(name = "RCP_CONTENT", unique = false, nullable = true, length = 2000)
    private String rcpContent;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaBcmReceiptInfo() {
        // default implementation ignored
    }

    /**
     * @param pbreiSerno
     */
    public void setPbreiSerno(String pbreiSerno) {
        this.pbreiSerno = pbreiSerno;
    }

    /**
     * @return pbreiSerno
     */
    public String getPbreiSerno() {
        return this.pbreiSerno;
    }

    /**
     * @param taskNo
     */
    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    /**
     * @return taskNo
     */
    public String getTaskNo() {
        return this.taskNo;
    }

    /**
     * @param bcmMode
     */
    public void setBcmMode(String bcmMode) {
        this.bcmMode = bcmMode;
    }

    /**
     * @return bcmMode
     */
    public String getBcmMode() {
        return this.bcmMode;
    }

    /**
     * @param bcmDate
     */
    public void setBcmDate(String bcmDate) {
        this.bcmDate = bcmDate;
    }

    /**
     * @return bcmDate
     */
    public String getBcmDate() {
        return this.bcmDate;
    }

    /**
     * @param bcmObjType
     */
    public void setBcmObjType(String bcmObjType) {
        this.bcmObjType = bcmObjType;
    }

    /**
     * @return bcmObjType
     */
    public String getBcmObjType() {
        return this.bcmObjType;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param tel
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * @return tel
     */
    public String getTel() {
        return this.tel;
    }

    /**
     * @param bcmResult
     */
    public void setBcmResult(String bcmResult) {
        this.bcmResult = bcmResult;
    }

    /**
     * @return bcmResult
     */
    public String getBcmResult() {
        return this.bcmResult;
    }

    /**
     * @param bcmCaseDesc
     */
    public void setBcmCaseDesc(String bcmCaseDesc) {
        this.bcmCaseDesc = bcmCaseDesc;
    }

    /**
     * @return bcmCaseDesc
     */
    public String getBcmCaseDesc() {
        return this.bcmCaseDesc;
    }

    /**
     * @param isRcp
     */
    public void setIsRcp(String isRcp) {
        this.isRcp = isRcp;
    }

    /**
     * @return isRcp
     */
    public String getIsRcp() {
        return this.isRcp;
    }

    /**
     * @param rcpDate
     */
    public void setRcpDate(String rcpDate) {
        this.rcpDate = rcpDate;
    }

    /**
     * @return rcpDate
     */
    public String getRcpDate() {
        return this.rcpDate;
    }

    /**
     * @param rcpContent
     */
    public void setRcpContent(String rcpContent) {
        this.rcpContent = rcpContent;
    }

    /**
     * @return rcpContent
     */
    public String getRcpContent() {
        return this.rcpContent;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}