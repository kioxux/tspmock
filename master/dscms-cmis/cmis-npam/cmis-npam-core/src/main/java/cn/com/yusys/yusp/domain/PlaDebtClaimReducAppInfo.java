/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaDebtClaimReducAppInfo
 * @类描述: pla_debt_claim_reduc_app_info数据实体类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-06-08 19:44:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pla_debt_claim_reduc_app_info")
public class PlaDebtClaimReducAppInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 债权减免申请流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PDCRAI_SERNO")
    private String pdcraiSerno;

    /**
     * 客户编号
     **/
    @Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
    private String cusId;

    /**
     * 客户名称
     **/
    @Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
    private String cusName;

    /**
     * 贷款总金额
     **/
    @Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal loanAmt;

    /**
     * 贷款总余额
     **/
    @Column(name = "LOAN_BALANCE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal loanBalance;

    /**
     * 拖欠总利息
     **/
    @Column(name = "TOTAL_TQLX_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal totalTqlxAmt;

    /**
     * 减免总额
     **/
    @Column(name = "REDUC_TOTL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal reducTotlAmt;

    /**
     * 减免本金合计
     **/
    @Column(name = "REDUC_CAP_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal reducCapAmt;

    /**
     * 减免欠息合计
     **/
    @Column(name = "REDUC_DEBIT_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal reducDebitInt;

    /**
     * 减免罚息合计
     **/
    @Column(name = "REDUC_PENAL_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal reducPenalInt;

    /**
     * 减免复息合计
     **/
    @Column(name = "REDUC_COMPOUND_INT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal reducCompoundInt;

    /**
     * 减免费用合计
     **/
    @Column(name = "REDUC_COST_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal reducCostAmt;

    /**
     * 费用减免详情
     **/
    @Column(name = "REDUC_COST_DETAIL", unique = false, nullable = true, length = 2000)
    private String reducCostDetail;

    /**
     * 减免原因
     **/
    @Column(name = "REDUC_RESN", unique = false, nullable = true, length = 2000)
    private String reducResn;

    /**
     * 审批状态
     **/
    @Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
    private String approveStatus;

    /**
     * 备注
     **/
    @Column(name = "MEMO", unique = false, nullable = true, length = 2000)
    private String memo;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public PlaDebtClaimReducAppInfo() {
        // default implementation ignored
    }

    /**
     * @param pdcraiSerno
     */
    public void setPdcraiSerno(String pdcraiSerno) {
        this.pdcraiSerno = pdcraiSerno;
    }

    /**
     * @return pdcraiSerno
     */
    public String getPdcraiSerno() {
        return this.pdcraiSerno;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param loanAmt
     */
    public void setLoanAmt(java.math.BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    /**
     * @return loanAmt
     */
    public java.math.BigDecimal getLoanAmt() {
        return this.loanAmt;
    }

    /**
     * @param loanBalance
     */
    public void setLoanBalance(java.math.BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    /**
     * @return loanBalance
     */
    public java.math.BigDecimal getLoanBalance() {
        return this.loanBalance;
    }

    /**
     * @param totalTqlxAmt
     */
    public void setTotalTqlxAmt(java.math.BigDecimal totalTqlxAmt) {
        this.totalTqlxAmt = totalTqlxAmt;
    }

    /**
     * @return totalTqlxAmt
     */
    public java.math.BigDecimal getTotalTqlxAmt() {
        return this.totalTqlxAmt;
    }

    /**
     * @param reducTotlAmt
     */
    public void setReducTotlAmt(java.math.BigDecimal reducTotlAmt) {
        this.reducTotlAmt = reducTotlAmt;
    }

    /**
     * @return reducTotlAmt
     */
    public java.math.BigDecimal getReducTotlAmt() {
        return this.reducTotlAmt;
    }

    /**
     * @param reducCapAmt
     */
    public void setReducCapAmt(java.math.BigDecimal reducCapAmt) {
        this.reducCapAmt = reducCapAmt;
    }

    /**
     * @return reducCapAmt
     */
    public java.math.BigDecimal getReducCapAmt() {
        return this.reducCapAmt;
    }

    /**
     * @param reducDebitInt
     */
    public void setReducDebitInt(java.math.BigDecimal reducDebitInt) {
        this.reducDebitInt = reducDebitInt;
    }

    /**
     * @return reducDebitInt
     */
    public java.math.BigDecimal getReducDebitInt() {
        return this.reducDebitInt;
    }

    /**
     * @param reducPenalInt
     */
    public void setReducPenalInt(java.math.BigDecimal reducPenalInt) {
        this.reducPenalInt = reducPenalInt;
    }

    /**
     * @return reducPenalInt
     */
    public java.math.BigDecimal getReducPenalInt() {
        return this.reducPenalInt;
    }

    /**
     * @param reducCompoundInt
     */
    public void setReducCompoundInt(java.math.BigDecimal reducCompoundInt) {
        this.reducCompoundInt = reducCompoundInt;
    }

    /**
     * @return reducCompoundInt
     */
    public java.math.BigDecimal getReducCompoundInt() {
        return this.reducCompoundInt;
    }

    /**
     * @param reducCostAmt
     */
    public void setReducCostAmt(java.math.BigDecimal reducCostAmt) {
        this.reducCostAmt = reducCostAmt;
    }

    /**
     * @return reducCostAmt
     */
    public java.math.BigDecimal getReducCostAmt() {
        return this.reducCostAmt;
    }

    /**
     * @param reducCostDetail
     */
    public void setReducCostDetail(String reducCostDetail) {
        this.reducCostDetail = reducCostDetail;
    }

    /**
     * @return reducCostDetail
     */
    public String getReducCostDetail() {
        return this.reducCostDetail;
    }

    /**
     * @param reducResn
     */
    public void setReducResn(String reducResn) {
        this.reducResn = reducResn;
    }

    /**
     * @return reducResn
     */
    public String getReducResn() {
        return this.reducResn;
    }

    /**
     * @param approveStatus
     */
    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    /**
     * @return approveStatus
     */
    public String getApproveStatus() {
        return this.approveStatus;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}