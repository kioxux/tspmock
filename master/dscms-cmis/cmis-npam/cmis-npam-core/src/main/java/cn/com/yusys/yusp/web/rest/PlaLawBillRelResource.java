/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PlaLawBillRel;
import cn.com.yusys.yusp.service.PlaLawBillRelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;


/**
 * @项目名称: cmis-npam-core模块
 * @类名称: PlaLawBillRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 周茂伟
 * @创建时间: 2021-05-29 10:54:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "诉讼关联借据信息")
@RestController
@RequestMapping("/api/plalawbillrel")
public class PlaLawBillRelResource {
    @Autowired
    private PlaLawBillRelService plaLawBillRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PlaLawBillRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<PlaLawBillRel> list = plaLawBillRelService.selectAll(queryModel);
        return new ResultDto<List<PlaLawBillRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PlaLawBillRel>> index(QueryModel queryModel) {
        List<PlaLawBillRel> list = plaLawBillRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawBillRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{plbrSerno}")
    protected ResultDto<PlaLawBillRel> show(@PathVariable("plbrSerno") String plbrSerno) {
        PlaLawBillRel plaLawBillRel = plaLawBillRelService.selectByPrimaryKey(plbrSerno);
        return new ResultDto<PlaLawBillRel>(plaLawBillRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "诉讼借据保存")
    @PostMapping("/save")
    protected ResultDto<PlaLawBillRel> create(@RequestBody PlaLawBillRel plaLawBillRel) throws URISyntaxException {
        plaLawBillRelService.insert(plaLawBillRel);
        return new ResultDto<PlaLawBillRel>(plaLawBillRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PlaLawBillRel plaLawBillRel) throws URISyntaxException {
        int result = plaLawBillRelService.update(plaLawBillRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "诉讼借据删除")
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@Validated @RequestBody PlaLawBillRel plaLawBillRel) {
        int result = plaLawBillRelService.deleteByPrimaryKey(plaLawBillRel);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = plaLawBillRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:queryPlaLawBillRelByAppSerno
     * @函数描述:通过诉讼编号查询借据列表
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "诉讼借据查询")
    @PostMapping("/queryPlaLawBillRelByAppSerno")
    protected ResultDto<List<PlaLawBillRel>> queryPlaLawBillRelByAppSerno(@RequestBody QueryModel queryModel) {
        List<PlaLawBillRel> list = plaLawBillRelService.selectByModel(queryModel);
        return new ResultDto<List<PlaLawBillRel>>(list);
    }

    /**
     *  批量引入合同借据信息
     * @param ctrLoanConts,appserno
     * @return
     */
    @PostMapping("/saveContNo")
    protected ResultDto<Integer> saveBatchContNo(@RequestBody Map ctrLoanConts) {
        Integer count = plaLawBillRelService.saveBatchContNo(ctrLoanConts);
        return new ResultDto< Integer>(count);
    }


}
