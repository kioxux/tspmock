# 张家港农商行新信贷开发规范

>本文档适用于张家港农商行新信贷项目的开发人员。本文档旨在为开发人员在项目开发期间对项目结构的组织、命名、代码编写、代码提交等作出指导性规范。


## 微服务划分

| 服务英文名      | 服务中文名   | 端口 | 服务说明                                                     | 备注 |
| --------------- | ------------ | ---- | ------------------------------------------------------------ | ---- |
| cmis-biz        | 流程作业服务 | 6001 | 包含以下功能：<br/>1）对公、小微和零售的贷前和贷中功能；<br/>2）档案管理功能；<br/>3）征信管理功能；<br/>4）押品管理功能； |      |
| cmis-cfg        | 参数管理服务 | 6002 | 包含产品配置和参数配置功能；                                 |      |
| cmis-psp        | 贷后管理服务 | 6003 | 包含贷后功能；                                               |      |
| cmis-cus        | 客户管理服务 | 6004 | 包含客户管理功能；                                           |      |
| cmis-bsp        | 通讯转换服务 | 6005 | 包含新信贷的客户端和服务端接口；                             |      |
| cmis-npam       | 资产保全服务 | 6006 | 包含资产保全功能；                                           |      |
| cmis-lmt        | 额度管理服务 | 6007 | 包含额度管理功能；                                           |      |
| cmis-batch      | 批量管理服务 | 6008 | 包含日终批量管理功能；                                       |      |
| cmis-common-app | 公共服务     |      | 包含公共日志、翻译、常量、枚举类和注解等功能；               |      |




## 项目结构

项目按照Maven的Java目录规范，微服务以Maven模块的形式存在， 微服务内再以子模块的形式划分微服务功能。

###  微服务模块结构

新的业务微服务模块组织结构示例如下(xxx代表业务模块名称，后文的xxx均与此相同)

├─cmis-xxx(业务微服务)
│  └─cmis-xxx-client(业务微服务提供的Feign接口模块)
│  │  └─pom.xml(业务client模块POM文件)
│  └─cmis-xxx-core(业务微服务核心业务模块)
│  │  └─pom.xml(业务核心模块POM文件)
│  └─cmis-xxx-dto(业务微服务数据传输类模块)
│  │  └─pom.xml(业务微服务数据传输模块POM文件)
│  └─cmis-xxx-starter(业务微服务启动模块)
│  │  └─pom.xml(业务启动模块POM文件)
│  └─pom.xml(业务微服务POM文件)

├─cmis-common-app(公共微服务)
│  └─cmis-common(XXX模块)
│  │  └─pom.xml(XXX模块POM文件)
│  └─cmis-common-context(XXX模块)
│  │  └─pom.xml(XXX模块POM文件)
│  └─cmis-common-log(公共微服务日志模块)
│  │  └─pom.xml(公共微服务日志模块POM文件)
│  └─cmis-common-translator(公共微服务翻译模块)
│  │  └─pom.xml(公共微服务翻译模块POM文件)
│  └─pom.xml(公共微服务POM文件)

├─config(业务微服务对应nacos配置文件)

├─doc（业务微服务相关说明文档）


#### 微服务命名规则

### 各模块内包的命名规则

各业务模块的根包均需要以`cn.com.yusys.yusp`开始。

* cmis-xxx-starter
    * 启动类路径：`cn.com.yusys.yusp`
    * 配置类包路径：`cn.com.yusys.yusp.config`
    * 资源配置路径：`resources/config`
* cmis-xxx-dto
    * dto类路径：`cn.com.yusys.yusp.dto`
      * 服务端dto类路径：`cn.com.yusys.yusp.dto.server.tradecode`
        * 请求dto类路径：`cn.com.yusys.yusp.dto.server.tradecode.req`
        * 响应dto类路径：`cn.com.yusys.yusp.dto.server.tradecode.resp`
      * 客户端dto类路径：`cn.com.yusys.yusp.dto.client.trademethod.tradesystem.tradecode`
        * 请求dto类路径：`cn.com.yusys.yusp.dto.client.trademethod.tradesystem.tradecode.req`
        * 响应dto类路径：`cn.com.yusys.yusp.dto.client.trademethod.tradesystem.tradecode.resp`
      * 说明：
        * 目前只有`cmis-bsp-dto`中存在客户端路径；
        * `trademethod`是指交易方式，包含`esb`和`http`；
        * `tradesystem`是指交易系统，包含但不局限于：`core`,`ecif`,`lsnp`等；
        * 请求dto类路径和响应dto类路径可根据接口的复杂程度决定是否需要新增`req`和`resp`；
* cmis-xxx-core
    * domain类路径：`cn.com.yusys.yusp.domain`
    * mapper类路径：`cn.com.yusys.yusp.repository.mapper`
    * service类路径：`cn.com.yusys.yusp.service`
      * 服务端service类路径：`cn.com.yusys.yusp.service.server.tradecode`
      * 客户端service类路径：`cn.com.yusys.yusp.service.client.microservice.tradecode`
        * 说明：`microservice`指各微服务模块简称，包含`biz`,`bsp`,`cfg`,`cus`,`lmt`,`npam`,`psp`等；
    * web接口类路径：`cn.com.yusys.yusp.web`
      * 前后端交互web接口类路径：`cn.com.yusys.yusp.web.rest`
      * 服务端web接口类路径：`cn.com.yusys.yusp.web.server.tradecode`
      * 客户端web接口类路径：`cn.com.yusys.yusp.web.client.trademethod`
        * 说明：
          - 目前只有`cmis-bsp-client`中存在客户端路径；
          - `trademethod`是指交易方式，包含`esb`和`http`；
    * 实时客户端交易类路径：`cn.com.yusys.yusp.online.client.trademethod.tradesystem.tradecode`
      * 请求交易类路径：`cn.com.yusys.yusp.online.client.trademethod.tradesystem.tradecode.req`
      * 响应交易类路径：`cn.com.yusys.yusp.online.client.trademethod.tradesystem.tradecode.resp`
      * 说明：
        - 目前只有`cmis-bsp-core`中存在实时客户端交易路径；
        - `trademethod`是指交易方式，包含`esb`和`http`；
        - `tradesystem`是指交易系统，包含但不局限于：`core`,`ecif`,`lsnp`等；
    * mapper文件路径：`resources/mapper`
    * 客户端示例代码路径：`resources/template/client/trademethod/tradesystem/tradecode`
      * 说明：
        - 目前只有`cmis-bsp-core`中存在客户端示例代码路径；
        - `trademethod`是指交易方式，包含`esb`和`http`；
        - `tradesystem`是指交易系统，包含但不局限于：`core`,`ecif`,`lsnp`等；
    * 服务端示例报文路径：`resources/template/client/microservice/tradecode`
      * 说明：`microservice`指各微服务模块简称，包含`biz`,`bsp`,`cfg`,`cus`,`lmt`,`npam`,`psp`等；
    * 此模块可根据实际情况配置其他的类路径，如：`cn.com.yusys.yusp.config`、`cn.com.yusys.yusp.constants`、`cn.com.yusys.yusp.utils`、`cn.com.yusys.yusp.mq`等
 * cmis-xxx-client
    * Feign类路径：`cn.com.yusys.yusp.service`
* cmis-common-app
    * cmis-common:公共模块
        * 常量类路径：`cn.com.yusys.yusp.constants`
        * 返回码枚举类路径：`cn.com.yusys.yusp.enums.returncode`
        * 服务端和客户端枚举类路径：`cn.com.yusys.yusp.enums.online`，客户端接口中枚举值是以各系统要求为准，服务端中存量接口的字典项和现有的不一致写在此目录；

### 各模块内功能类命名规则

* cmis-xxx-starter
    * 启动类：`CmisXxxStarterMicroserviceApp.java`
* cmis-xxx-dto
    * DTO：`XxxDto.java`
* cmis-xxx-core
    * Domain：`Xxx.java`
    * Mapper：`XxxMapper.java`
    * Mapper XML文件：`XxxMapper.xml`
    * Service接口：`XxxService.java`
    * Resource类：`XxxResource.java`
* cmis-xxx-client
    * Feign接口类：`XxxClientService.java`


## 代码规范

### 后端代码规范

#### 常用命名规范

* Java文件名按照大驼峰规则命名， 如：CustInfo.java
* Mapper文件按照大驼峰规则命名， 如：CustInfoMapper.xml
* 包名均采用小写英文字母组成，如：cn.com.yusys.yusp
* 成员变量、局部变量、成员方法按照小驼峰规则命名
* 静态变量名字母全部大写，多个单词以下划线连接
* 对于资源对象或者流对象， 必须在finally中关闭，或者使用try-with-resources语法。禁止在finally中return
* 对于会出现空指针异常的对象，应该添加代码判断
* 只允许使用BigDecimal类型表示金额

**示例：**

```java
public class CustInfoService{

    //静态变量
    public static final String MARK_DOT = ".";
    //成员变量
    @Autowired
    private CustInfoMapper custInfoMapper；
    
    …………
    
    // 成员方法
    public CustInfo getById(String id) {
        //局部变量
        CustInfo custInfo = this.custInfoMapper.findById(id)；
        return cust Info；
    }
}
```

### 代码注释规范

#### 代码注释要尽量以简洁、清晰为目标。

* 类(包含枚举、接口)注释规范，类的头部应描述清楚类的功能， 类的初始作者和修订作者
* 公开方法注释规范，公开方法应描述清楚方法的功能
* Pojo类注释规范， Pojo类在字段上应描述清楚字段的含义
* 公开方法注释规范，公开方法应描述清楚方法的功能
* Pojo类注释规范， Pojo类在字段上应描述清楚字段的含义
* 复杂业务逻辑应尽量补全业务逻辑的流程走向，业务含义等注释信息
* 常用类中的字段必须描述请求该字段的含义和作用
* 除方法内的注释外，其余注释应使用文档注释
* 方法参数要使用`@param`注解进行说明
* 方法返回值要使用`@return`注解进行说明

**示例：**

```java
/**
 *客户信息
 *@author xxx
 */
 public class CustInfo{
 ……
    /**
     *客户名称
     */
    private String name；
    
    /**
     *根据客户id获取客户名称
     *@param id 客户的ID
     *@return 客户的名称
     */
    public String getById(String id) {
        ……
    }
 }
    
```

### 代码风格规范

* 类或方法的左花括号应位于行尾
* 类或方法的右花括号应独立使用一行
* if语句中单行语句块禁止省略花括号
* 赋值号(=) 、参数之间、for语句表达式均应以空格分隔
* 方法之间要保留一个空行
* 过长的一行代码应分割为多行显示，从第二行起要与第一行保留一个缩进单位
* 代码中未使用的import应去掉
* 注意服务间的依赖关系，不要出现相互依赖
* 未完整实现的代码需要添加todo注释， 如：

```java
// TODO(“待xxxApi接口提供后完") ；
```

## 接口规范

### URL规范

各个微服务的接口（URL）必须以/api开头。接口设计在RESTful设计规范基础上使用如下格式：

| HTTP动词 |URL  | 调用方法 |
| --- | --- | --- |
| GET | /api/xxx | index |
| GET | /api/xxx/{id} | show |
| POST | /api/xxx | create |
| POST | /api/xxx/update | update |
| POST | /api/xxx/delete/{id} | delete |
| POST | /api/xxx/batchdelete/{ids} | batchDelete |

注意：为兼容HTTP 1.0规范，HTTP动词只允许使用GET和POST。

### 接口报文规范  TODO 待和顾老板沟通

* 查询接口和数据变化的接口使用POST请求

分页查询参数通过查询字符串传输：
```json
condition={"guarNo":""}&page=1&size=10&sort=pkId&order=desc
```

查询条件封装在condition字段中，分页排序参数为：page、size、sort、order
接口响应数据格式：
```json
{"code":0, "total":2,"message":null,"data":{}}
```
code：业务响应状态码，0表示成功，非0表示失败
message：业务响应的消息
total：业务数据总条数
data：业务数据

POST接口应以良好的JSON格式向后端接口传输。

### Swagger规范

为方便前端开发人员核对接口和对接口信息可汇总，后端开发人员需要必要的数据传输类(DTO、BO、VO、POJO等) 、 接口层(Resource ) 使用Swagger注解。

数据传输类使用的Swagger注解：

* @ApiModel， 标注在数据传输类上，用于描述该类的作用
* @ApiModelProperty，标注在数据传输类的字段或属性(getter方法) 上，用于描述该字段或属性的含义

**示例：**

```java
/**
 * 客户信息数据传输类
 * @author xxx
 */
 @ApiModel("客户信息")
 public class CustInfoDto implements Serializable{
      private static final long serialVersionUID =1L；
      /** 客户编号 **/
      @ApiModelProperty("客户编号")
      private String custId；
      
      /** 证件类型 **/
      @ApiModelProperty("证件类型")
      private String idType；
      ……
 }
```

接口层使用的Swagger注解

* @Api， 标注在 Resource类上， 用于描述该类的作用
* @ApiOperation，标注在接口方法上，用于描述该方法的作用
* @ApilmplicitParam， 标注接口接受参数的含义
* @ApilmplicitParams， 标注接口接受多个参数
* @ApiResponse，标注接口响应内容
* @ApiResponses，标注接口响应多种情况的内容


**示例：**
```java
/**
 * 客户管理接口
 * @author xxx
 */
 @Api(value =“客户管理"， tags =“客户管理")
 @RestController
 @RequestMapping("/api/custinfo")
 public class CustInfoResource {
        /**
         * 根据客户编号查询客户信息
         * @param custId客户ID
         * @return ResultDto<CustBo>
         */
         @ApiOperation("根据客户编号查询客户信息")
         @PostMapping("/getcustInfoByCustId")
         @ApiImplicitParam(paramType="query"， name=“custId"， value=“客户ID"， required=true， dataType=“String")
         @ApiResponses({@ApiResponse(code=400， message= “无效的客户ID“)})
         public ResultDto<CustBo> getCustInfoByCustId(@RequestParam("custId") String custId) {
            ……
         }
 }
 
```

**数据传输对象校验规范**

数据传输对象(包括数据库存储对象domain)应使用`javax.validation.constraints`包下和`org.hibernate.validator.constraints`包下的注解进行合法性校验。

`javax.validation.constraints` 包下常用的校验注解：

* @Max最大值校验
* @Min最小值校验
* @NotBlank非空白
* @NotEmpty非空
* @NotNull非null
* @Size大小校验
* @Pattern正则表达式校验

`org.hibernate.validator.constraints`包下常用的校验注解：

* @Length长度校验
* @Range范围校验
* @URL URL校验

数据字典校验注解：@InDataCode

在使用时配合@valid进行校验。注意：当上述的校验注解无法满足时， 需要手动编写代码实现。


### FeignClient编写规范 

* 每个FeignClient都要配置对应的熔断类

重要： Feign接口中使用到的 `@RequestParam` 和 @`PathVariable` 都需要指定 `value`

**示例：**

```java
/**
 * 封装的接口类:客户Feign接口
 * @author xxx
 */
 @FeignClient(name="cmis-cus" path = "/api/custinfo"}
 public interface CustInfoClientService {
    ……
    
 }
```

## 字典项设计规范

* 字典项的值设计要与数标文件一致
* 字典类别英文别名应全部采用大写字母，单词间使用下划线( _ )连接，不能使用特殊符号，注意前后不能有空格
* 字典类别名称应使用简短的中文进行描述
* 字典代码不能使用特殊符号，注意前后不能有空格
* 对于严格有顺序的列表项要使用字典排序进行排序，排序只能使用自然数，数值不宜过大

## 数据库设计规范

* 数据库和表的字符集统一使用utf8，排序规则为utf8_bin
* 数据库表设计遵守3NF范式
* 数据库表名以小写英文字母组成，多个英文单词使用下划线( _ )连接
* 数据库表字段以大写英文字母组成，多个英文单词使用下划线( _ )连接
* 每张表必须有主键，主键索引以PK_开始
* 建表时需要对表和字段增加注释， 以便于维护
* 与财务相关的金额类数据必须使用`NUMBER`类型
* 对于经常出现在`where`中的字段， 建议对其建立索引
* 频繁更新的字段不适宜建索引
* 限制每张表上的索引数量，建议单表表索引不超过5个
* INSERT语句后要显示列举需要插入的字段名称
* 表连接的数量不宜过多，不能超过6个
* 使用`SELECT`语句时，查询的字段必须要显示列出，禁止使用`SELECT * `操作
* 大规模的数据写入，应采用分批写入的方式
* 慎用子查询， 可改为`JOIN`操作
* 需注意避免长事务的产生，一个事务中包含太多操作可能发生长事务。因此，每次提交的事务不能超过10万条记录
* 对于很大的表进行全表删除时， 不要使用全表delete，应该用 truncate或者`drop`重建
* 如果是删除部分数据，则要分批提交以降低事物长度，比如每删除10万行就提交， 以免造成数据库undo资源不足而回滚
* 如果对很大的表进行update， 并且 update记录很多，也应该分批提交
* 在删除历史数据的同时，如果同一个表还会有大量插入，则必须尽量缩小操作单元、分批提交、加快提交频率，否则插入会遇到索引分裂等待的严重堵塞
* 代码中编写SQL语句时，参数必须使用占位符，禁止使用字符串连接，避免出现SQL注入的可能性

> TODO 待顾老板补充TDSQL中的规范

## 日志规范

* 日志中不能出现敏感信息如手机号，证件号，姓名，银行卡号。TODO 和金增沟通过；
* 只打印必要的信息，明细数据尽量不要打印
* 开发测试过程中为调试代码而打印的日志不能带上生产环境
* 避免重复打印日志，浪费磁盘空间
* 日志打印时， 禁止将对象使用工具转换成JSON字符串转， 避免转换过程中因为抛出异常而影像业务正常流程，该情况下应调用对象的toString方法打印日志
* `debug/trace/info`级别的日志输出，应使用条件和占位符的方式

**示例：**

```java
    logger.info("xxx number is {}“， num) ；
```


## 异常处理规范

异常处理指导

* 对于会出现空指针异常的对象，应该先做判空判断

* 禁止在catch语句块中对异常不做任何处理

* 禁止在catch语句块中调用`e.printStackTrace() `打印异常堆栈， 应使用 log.error记录异常堆栈

* catch语句块中的异常需要以error级别的日志输出异常堆栈， 不要用info级别输出或不输出异常， 这对线上查问题非常重要

* 不要使用System.out.println输出调试信息

* 所有异常在resource层处理完，可以统一抛出相应异常由统一异常切面处理，也可以捕获所有异常返回

* `@Transactional`不要加在Service类上，应该针对方法设置，查询方法加只读事务

* 嵌套事务时需要注意，内层事务异常抛出异常后，外层事务也要抛出异常

* 示例代码如下：

  ```java
  @Transactional(rollbackFor = {BizException.class, Exception.class}) 
  public Xdkh0008DataRespDto getXdht0008(String cusId) throws BizException, Exception {
          logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value);
          Xdkh0008DataRespDto xdkh0008DataRespDto = new Xdkh0008DataRespDto();
          try {
              //根据客户号查询集团编号和集团名称
              CusGrpMemberRel cusGrpMemberRel = cusGrpMemberRelMapper.getGruopInfoByCusId(cusId);
              if (cusGrpMemberRel != null) {
                  
              } else {
                  throw BizException.error(null, EcsEnum.ECS040006.key, EcsEnum.ECS040006.value);
              }
          } catch (BizException e) {
              logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, e.getMessage());
              throw BizException.error(null, e.getErrorCode(), e.getMessage());
          } catch (Exception e) {
              logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, e.getMessage());
              throw new Exception(EpbEnum.EPB099999.value);
          }
          logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, xdkh0008DataRespDto);
          return xdkh0008DataRespDto;
      }
  ```

  


异常码规范

1. 业务接口成功的异常码值为0000；
2. 系统运行时异常、框架异常等非业务功能性一场统称为系统异常；
3. 错误码共9位，其中前3位是英文字母，第4-5位和端口匹配，第6-9位是服务编码，取值范围位0001-9999；
4. EPB09：代表公共的错误异常；

各微服务异常码规范命名：

| 服务英文名      | 服务中文名   | 端口 | 异常码 | 枚举类  | 异常码示例                                     |
| --------------- | ------------ | ---- | ------ | ------- | ---------------------------------------------- |
| cmis-biz        | 流程作业服务 | 6001 | ECB01  | EcbEnum | ECB010001("10001", "传入参数异常!")            |
| cmis-cfg        | 参数管理服务 | 6002 | ECF02  | EcfEnum | ECF020001("20001", "参数信息获取失败"),        |
| cmis-psp        | 贷后管理服务 | 6003 | EPS03  | EpsEnum | EPS030001("30001", "贷后管理参数获取失败"),    |
| cmis-cus        | 客户管理服务 | 6004 | ECS04  | EcsEnum | ECS040001("40001", "查询类型queryType为空！"), |
| cmis-bsp        | 通讯转换服务 | 6005 | ESP05  | EspEnum | ESP050001("50001", "通讯转换服务异常"),        |
| cmis-npam       | 资产保全服务 | 6006 | ECN06  | EcnEnum | ESP060001("60001", "资产保全服务异常")         |
| cmis-lmt        | 额度管理服务 | 6007 | ECL07  | EclEnum | ECL070001("70001", "资产保全服务异常")         |
| cmis-batch      | 批量管理服务 | 6008 | ECH08  | EchEnum | ECH080001("80001", "批量管理服务异常")         |
| cmis-common-app | 公共服务     |      |        |         |                                                |

服务调用方需要对`Feign`接口进行`try`操作，如果发生失败，必须在`catch`语句块或`finally`语句中做处理，回滚业务或补偿（服务不可用的情况）；
由于所有`Feign` 接口都有相应熔断实现，实现中都是返回null，因此需要对`Feign`接口返回做相应的判空处理，应对超时的情况发生。



## 开发工具配置规范

> 见SVN路径：8_培训资料/8.1_新人指南/IntelliJ IDEA参考手册.doc


## 代码提交规范

* 提交代码前要先pull远程仓库最新的代码到本地
* 提交代码前要保证本地代码是能编译通过的
* 提交代码时遇到代码冲突时要谨慎操作，最好联系到相关开发人员一起解决冲突，避免覆盖掉他人的代码


## 提交代码注释规范


开发人员提交代码到Gitlab仓库时， 要按照以下格式填写提交消息。

* feat： 新特性描述
* fix：修复问题描述
* refactor：重构代码描述
* style：代码格式修改描述
* test：测试代码描述
* docs： 文档变化描述
* del：删除文件

注意：需要带上需求编号。