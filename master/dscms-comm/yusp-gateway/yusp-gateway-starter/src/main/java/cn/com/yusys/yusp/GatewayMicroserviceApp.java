package cn.com.yusys.yusp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 网关启动类
 *
 * @author yangzai
 * @since 3.1.1-SNAPSHOT
 **/
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = { "cn.com.yusys.yusp"})
public class GatewayMicroserviceApp {

    public static void main(String[] args) {
        SpringApplication.run(GatewayMicroserviceApp.class, args);
    }

}
