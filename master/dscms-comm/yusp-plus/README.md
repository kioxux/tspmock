
<p align="center" >
   <H1>yusp-plus-uaa</H1>
</p>

###同步记录
同步plus开源项目组代码截止日期为：20210622 
## Introduction

yusp-plus-uaa是一个认证中心，其核心设计目标是颁发token，解决登录问题。


## Features
- 1、简单：不需要任何额外的配置便可启动，满足基础的认证功能。
- 2、标准：所有的认证操作均满足oauth2标准。
- 3、可定制：支持新的授权模型，可以根据需求进行定制化开发。

## Development
- 1、支持基础的认证服务 2020.11.11

## instructions
### mysql instructions

- 1、初始化数据库脚本 
***
![](yusp-plus-uaa/doc/pic/table_w.jpg)
- 2、更改数据库连接信息 
***
![](yusp-plus-uaa/doc/pic/databseandredis.jpg)
- 3、更改nacos配置
***
![](yusp-plus-uaa/doc/pic/nocos.jpg)
- 4、启动oca并保证oca与uaa在注册中心的同一个命名空间下
***
- 5、使用postman发送获取图片验证码接口
***
![](yusp-plus-uaa/doc/pic/image.jpg)
- 6、使用postman发送获取token接口
***
![](yusp-plus-uaa/doc/pic/oauth1.jpg)   密码为123456 ![](yusp-plus-uaa/doc/pic/oauth2.jpg)
密码为：WQvVvpBOzyeuxpqrTY5A9ZZnO0VBY6xgS8ogQ32hGuGoxrybj+rEAl+y/ay+S+nbW81tF67BpcslF2HacdjTwf4r6KzucJrDgLb1+suQIOsrFe5iLTcP0S6LPmeHQEMpBmDrknbRf+S6TKtowQ28dNwbUI6KdKNU09SXbTQ07WQ=
     