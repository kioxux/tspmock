package cn.com.yusys.yusp.common.query;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;


@Data
@Slf4j
@NoArgsConstructor
public class PageQuery<T>{
    private Integer page=1;
    private Integer size=10;
    private String sort;

    public Page<T> getIPage() {
        //分页参数
        int curPage = Optional.ofNullable(this.getPage()).orElse(1);
        int limit = Optional.ofNullable(this.getSize()).orElse(10);;
        //分页对象
        return new Page<>(curPage, limit);
    }
}
