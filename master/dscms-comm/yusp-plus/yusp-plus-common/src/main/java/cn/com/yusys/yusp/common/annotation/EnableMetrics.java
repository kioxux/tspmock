package cn.com.yusys.yusp.common.annotation;

import cn.com.yusys.yusp.common.aspect.MetricsAspect;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 开启日志切面，此注解添加到Spring配置类上
 * @author danyubin
 * @since 2.3.3
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Configuration
@Import({MetricsAspect.class})
public @interface EnableMetrics {
}
