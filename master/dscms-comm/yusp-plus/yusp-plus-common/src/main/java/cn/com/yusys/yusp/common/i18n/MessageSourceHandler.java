package cn.com.yusys.yusp.common.i18n;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * @program: yusp-plus
 * @description: 国际化信息源处理类
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2020-12-24 16:29
 */
//@Component
public class MessageSourceHandler {
    @Autowired
    private MessageSource messageSource;

    public String getMessage(String messageKey) {
        String message = messageSource.getMessage(messageKey, null, LocaleContextHolder.getLocale());
        return message;
    }

    public String getMessage(String messageKey, Object[] objects) {
        String message = messageSource.getMessage(messageKey, objects, LocaleContextHolder.getLocale());
        return message;
    }

}
