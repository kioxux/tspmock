package cn.com.yusys.yusp.flow.util;

import cn.com.yusys.license.LicenseManager;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.*;
import java.lang.invoke.MethodHandles;
import java.net.URISyntaxException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

@Service
public class LicenseCheck {
    private final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());
    private static AtomicInteger cur = new AtomicInteger(0);
    private final String bootstrap = "org.springframework.cloud.bootstrap.BootstrapApplicationListener";
    private static final String PRODUCT_CODE = "0000001802";
    private static final String EXPEND_INFO = "eChain";
    private Environment env;
    private String licenseModel = "PRO";
    private String licenseFile;
    private String licenseContext;
    private LicenseManager lManager = LicenseManager.getInstance();
    @Autowired
    private Environment environment;

    public LicenseCheck() {
    }

    public void check() {
        if (!this.validateLicense(this.environment)) {
            System.exit(0);
        }

    }

    private boolean validateLicense(Environment env) {
        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setFormatter(new Formatter() {
            public String format(LogRecord record) {
                return record.getMessage();
            }
        });
        this.logger.setUseParentHandlers(false);
        this.logger.addHandler(consoleHandler);
        this.env = env;
        boolean check = true;
        return check;
    }

    private void setLicenseConfig() {
        String licenseMacFile = "";
        this.licenseModel = this.env.getProperty("echain.license.licenseModel") == null ? this.licenseModel : this.env.getProperty("echain.license.licenseModel");
        this.licenseContext = this.env.getProperty("echain.license.licenseContext");
        this.licenseFile = this.env.getProperty("echain.license.licenseFile");
        if (StringUtils.isEmpty(this.licenseContext) && StringUtils.isEmpty(this.licenseFile)) {
            try {
                Files.find(Paths.get(Thread.currentThread().getContextClassLoader().getResource("").toURI()), 1, (path, basicFileAttributes) -> {
                    return path.getFileName().toString().matches("^YTEC-\\d{10}-\\d-\\d{10}.lic$");
                }, new FileVisitOption[0]).findFirst().ifPresent((path) -> {
                    this.licenseFile = path.toString();
                    this.lManager.setLicenseFile(this.licenseFile);
                });
            } catch (URISyntaxException | IOException var3) {
            }
        }

        licenseMacFile = this.env.getProperty("license.licenseMacFile");
        this.lManager.setLicenseModel(this.licenseModel);
        InputStream inputStream = null;
        if (this.licenseContext != null && !"".equals(this.licenseContext)) {
            inputStream = this.getLicContextInputStream(this.licenseContext);
        } else {
            if (!StringUtils.isNotEmpty(this.licenseFile)) {
                throw new RuntimeException("License文件:" + this.licenseFile + "读取异常!!!");
            }

            inputStream = this.getLicFileInputStream(this.licenseFile);
        }

        this.lManager.setMacFile(licenseMacFile);
        this.lManager.setInputStream(inputStream);
    }

    private InputStream getLicContextInputStream(String licenseContext) {
        return new ByteArrayInputStream(licenseContext.getBytes());
    }

    private InputStream getLicFileInputStream(String licenseFile) {
        File tf = new File(licenseFile);
        Object inputStream;
        if (tf.exists()) {
            try {
                inputStream = new FileInputStream(tf);
            } catch (FileNotFoundException var6) {
                throw new RuntimeException("License文件:" + licenseFile + "读取异常!!!");
            }
        } else {
            try {
                Resource resource = new ClassPathResource(licenseFile);
                inputStream = resource.getInputStream();
            } catch (Exception var5) {
                throw new RuntimeException("License文件:" + licenseFile + "不存在!!!");
            }
        }

        return (InputStream)inputStream;
    }
}
