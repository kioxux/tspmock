package cn.com.yusys.yusp.common.exception;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

/**
 * @program: yusp-plus
 * @description: 全局异常处理
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2020-11-19 15:30
 */
@Slf4j
@RestControllerAdvice
@Order(value = Integer.MIN_VALUE+1)
public class PlusExceptionHandler {

    /**
     * 处理全局异常
     */
    @ExceptionHandler(Exception.class)
    public ResultDto handleException(Exception e) {

        log.error("统一异常处理：Exception, 异常原因：", e);
        return ResultDto.error("500", "系统业务繁忙，请稍后再试");
    }

    /**
     * 处理业务异常
     */
    @ExceptionHandler(BizException.class)
    public ResultDto<Object> handleBizException(BizException e) {
        log.error("统一异常处理：BizException, 异常原因：", e);
        return StringUtils.isEmpty(e.getErrorCode()) ? ResultDto.error(e.getMessage()) : ResultDto.error(e.getErrorCode(), e.getMessage());
    }


    /**
     * 前端表单对象校验异常处理
     * MethodArgumentNotValidException适用于@Valid注解抛出的异常
     *
     * <pre>@Valid</pre>注解用于校验@RequestBody
     *
     * @param ex 异常
     * @return R 返回对象
     */
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResultDto handleValidationExceptions(MethodArgumentNotValidException ex) {
        log.error("统一异常处理：MethodArgumentNotValidException, 异常原因：", ex);
//        return ResultDto.error("400", ex.getBindingResult().getAllErrors().stream().map((error) -> {
//            String fieldName = ((FieldError) error).getField();
//            String errorMessage = error.getDefaultMessage();
//            String code = error.getCode();
//            return fieldName + ": " + errorMessage + ":" + code;
////            return fieldName + ": " + errorMessage;
//        }).collect(Collectors.joining()));

//        String collect = ex.getBindingResult().getAllErrors().stream().map((error) -> {
//            String errorMessage = error.getDefaultMessage();
//            return fieldName + ": " + errorMessage + ":" + code;
////            return fieldName + ": " + errorMessage;
//        }).collect(Collectors.joining());
        String defaultMessage = ex.getBindingResult().getAllErrors().get(0).getDefaultMessage();
        String[] split = defaultMessage.split("&");
        if (split.length > 1){

            return ResultDto.error(split[0],split[1]);
        }else{
            return ResultDto.error("400", ex.getBindingResult().getAllErrors().stream().map((error) -> {
                String fieldName = ((FieldError) error).getField();
                String errorMessage = error.getDefaultMessage();
                String code = error.getCode();
                return fieldName + ": " + errorMessage + ":" + code;
//            return fieldName + ": " + errorMessage;
            }).collect(Collectors.joining()));
        }
    }

    /**
     * URL变量，参数校验异常处理
     * ValidationException适用于@Validated注解抛出的异常
     *
     * <pre>@Validated</pre>注解用于校验@Pathvariable和@RequestParam注解, 该注解需要添加到Controller类上
     *
     * @param ex 异常
     * @return ResultDto 返回对象
     */
    @ExceptionHandler({ConstraintViolationException.class})
    public ResultDto handleConstraintViolationExceptions(ConstraintViolationException ex) {

        log.error("统一异常处理：ConstraintViolationException, 异常原因：", ex);
        return ResultDto.error("400", ex.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(",")));
    }


    /**
     * 处理YuspException异常
     */
    @ExceptionHandler(YuspException.class)
    public ResultDto handleException(YuspException e) {

        log.error("统一异常处理：YuspExcepiton,异常原因：", e);
        return ResultDto.error(e.getCode(), e.getMessage());
    }

}
