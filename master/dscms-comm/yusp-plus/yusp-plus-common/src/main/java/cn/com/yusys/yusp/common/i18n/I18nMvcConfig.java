package cn.com.yusys.yusp.common.i18n;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @program: yusp-plus
 * @description: 国际化配置类
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2020-12-24 16:26
 */
//@Configuration
public class I18nMvcConfig implements WebMvcConfigurer {
    /**
     * 吧解析器加入到容器中
     *
     * @return
     */
    @Bean
    public LocaleResolver localeResolver() {
        return new I18nLocaleResolver();
    }
}
