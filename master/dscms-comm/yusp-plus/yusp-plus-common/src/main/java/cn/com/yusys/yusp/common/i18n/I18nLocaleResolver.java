package cn.com.yusys.yusp.common.i18n;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * @program: yusp-plus
 * @description: 国际化语言
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2020-12-24 16:23
 */
public class I18nLocaleResolver implements LocaleResolver {


    @Override
    public Locale resolveLocale(HttpServletRequest httpServletRequest) {

        //获取请求中的语言参数
        Locale locale = null;
        final String language = httpServletRequest.getHeader("Accept-Language"); //例如 zh_CN  en_US
        if (!StringUtils.isEmpty(language)) { // return target == null || target.length() == 0;
            final String[] s = language.split("_");
            // s[0]:国家 s[1]:地区
            locale = new Locale(s[0], s[1]);
            //将locale放入LocaleContextHolder容器中
            LocaleContextHolder.setLocale(locale);
            return locale;
        }
        locale = Locale.getDefault(); // 如果没有请求参数就是默认的
        return locale;
    }

    @Override
    public void setLocale(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Locale locale) {
    }
}
