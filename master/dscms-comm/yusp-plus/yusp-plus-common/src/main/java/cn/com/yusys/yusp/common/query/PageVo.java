package cn.com.yusys.yusp.common.query;

import lombok.Data;

import java.util.List;
@Data
public class PageVo<T> {

    private Integer total;
    private List<T> data;

}
