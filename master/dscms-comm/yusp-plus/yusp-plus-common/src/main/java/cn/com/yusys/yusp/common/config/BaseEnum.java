package cn.com.yusys.yusp.common.config;

public interface BaseEnum {
    /**
     * 获取枚举编码
     *
     * @return 编码
     */
    String getCode();
}
