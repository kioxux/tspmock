package cn.com.yusys.yusp.common.config;

import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;

import java.util.HashMap;
import java.util.Map;


public class CodeToEnumConverterFactory implements ConverterFactory<String, BaseEnum> {
    private static final Map<Class, Converter> CONVERTERS = new HashMap<>();

    /**
     * 获取一个从 code 转化为 T 的转换器，T 是一个泛型，有多个实现
     *
     * @param targetEnumType 转换后的类型
     * @return 返回一个转化器
     */
    @Override
    public <T extends BaseEnum> Converter<String, T> getConverter(Class<T> targetEnumType) {
        Converter<String, T> converter = CONVERTERS.get(targetEnumType);
        if (converter == null) {
            converter = new CodeToEnumConverter<>(targetEnumType);
            CONVERTERS.put(targetEnumType, converter);
        }
        return converter;
    }
}