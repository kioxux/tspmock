
<p align="center" >
   <H1>yusp-plus-uaa</H1>
</p>

## Introduction
yusp-plus-uaa是一个认证中心，其核心设计目标是颁发token，解决登录问题。


## Features
- 1、简单：不需要任何额外的配置便可启动，满足基础的认证功能。
- 2、标准：所有的认证操作均满足oauth2标准。
- 3、可定制：支持新的授权模型，可以根据需求进行定制化开发。

## Development
- 1、支持基础的认证服务 2020.11.11
- 2、支持多种授权类型:password、oca、third_party
## instructions
### uaa功能介绍
- 1、与yusp-plus-gateway，yusp-plus-oca绑定在一起，实现统一开发平台的基础功能，其中uaa作为认证服务中心，uaa的oca授权模式实现改功能。
- 2、作为token的颁发中心，由外部系统进行认证，根据认证结果颁发token，uaa的third_party授权模式实现该功能。
### 文件位置
- 1、初始化数据库脚本： 初始化脚本存放于yusp-plus/yusp-plus-uaa/doc/db/outh2_uaa.sql
- 2、基础配置文件： 基础配置文件存放于yusp-plus/yusp-plus-uaa/yusp-plus-uaa-starter/src/main/resources/config/application.yml.back , bootstrap.yml.back
- 3、证书容器存放位置： 证书容器存放于yusp-plus/yusp-plus-uaa/yusp-plus-uaa-starter/src/main/resources/keystore.jks
## Uaa场景
### 如何修改token过期时间以及配置图片验证码功能
配置图片验证码与token时间,打开表oauth_client_details，其中access_token_validity字段配置token的过期时间（单位s），additional_information配置图片验证码是否生效，若要图片验证码生效，将additional_information字段设置为"image_status": "true"。
***
![](doc/picture/databaseInfo.png) 

###如何配置自定义的证书容器
- 2、 配置证书容器，将生成的证书容器yusp-plus/yusp-plus-uaa/yusp-plus-uaa-starter/src/main/resources目录下，并在配置文件中完善以下配置：
```yaml
#uaa配置
uaa:
  keyStore:
    name:   #证书容器名
    password:  #证书容器密码
    keyPair:   #密钥对名称
``` 
### 一个uaa如何对接多个oca
可以根据不同的clientId来区分不同的Oca
![](doc/picture/clientId.png) 
 uaa对接不同的oca,完善以下示例代码便可（只是示例）。
```yaml
@Service
public class CustomOcaLoginUserInfoImpl implements OcaLoginUserInfo {
//使用feign接口调用
    @Autowired
    MultiOcaFeignService multiOcaFeignService;

    @Autowired
    OcaFeignService ocaFeignService;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public TokenParamDto getLoginUserInfo(String username, String password) {
        //1)获取client_id
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User clientDetails = (User) authentication.getPrincipal();
        String clientId = clientDetails.getUsername();

        //2)封装请求参数
        LoginOcaRequestDto loginOcaRequestDto = new LoginOcaRequestDto();
        loginOcaRequestDto.setLoginCode(username);
        loginOcaRequestDto.setPassword(password);

        //3)根据clientId判断走哪个oca进行验证
        LoginOcaResultDto<UserInfoForOcaDto> loginOcaResultDto;
        if (clientId.equals("test")) {
            loginOcaResultDto = ocaFeignService.queryUserAndCheckSecret(loginOcaRequestDto);
        } else if (clientId.equals("admin")) {
            loginOcaResultDto = multiOcaFeignService.queryUserAndCheckSecret(loginOcaRequestDto);
        } else {
            loginOcaResultDto = new LoginOcaResultDto<>();
        }

        //4)判断认证是否成功
        if (loginOcaResultDto == null) {
            log.error(UaaErrorCodeEnum.SYSTEM_ERROR.getMessage()+",The correct information is not returned from Oca");
            throw new UaaErrorException(UaaErrorCodeEnum.SYSTEM_ERROR.getCode(), UaaErrorCodeEnum.SYSTEM_ERROR.getMessage() + ",The correct information is not returned from Oca ");
        }
        if (!loginOcaResultDto.getCode().equals(SuccessfulAuthenticationCodeEnum.SUCCESSFULLY_CHECKED.getCode())
                && !loginOcaResultDto.getCode().equals(SuccessfulAuthenticationCodeEnum.FIRSTLOGIN.getCode())) {
            throw new UaaOauthException(loginOcaResultDto.getCode(), loginOcaResultDto.getMessage());
        }

        //5)封装TokenParamDto
        TokenParamDto tokenParamDto = new TokenParamDto();
        tokenParamDto.setBusinessCode(loginOcaResultDto.getCode());
        tokenParamDto.setLoginCode(loginOcaResultDto.getData().getLoginCode());
        tokenParamDto.setOrgId(loginOcaResultDto.getData().getOrgId());
        tokenParamDto.setUserId(loginOcaResultDto.getData().getUserId());

        return tokenParamDto;
    }
}

``` 
### 如何对接第三方认证中心进行认证

- 1、根据实际情况完善以下代码
```yaml
@Service
public class CustomThirdLoginUserInfoImpl implements ThirdLoginUserInfo {
    @Override
    public TokenParamDto getLoginUserInfo() {

        //1) 封装restTemplate的请求参数

        //1.1)从请求中获取数据
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        //1.2)假设请求数据字段为 ticket,并可以通过该字段从第三方获取认证结果
        String ticket = httpServletRequest.getParameter("ticket");

        //2)构造RestTemplate请求
        RestTemplate restTemplate = new RestTemplate();

        //2.1)设置请求路径
        String url="https://xxx.xxx.xxx";

        //2.2) 发送请求并获取值，返回值类型不为TokenParamDto，则需要根据实际的返回对象给TokenParamDto的各个字段赋值
        ResponseEntity<TokenParamDto> restTemplateForEntity = restTemplate.getForEntity(url, TokenParamDto.class,ticket);

        //2.3) 判断远程调用是否成功，如果不成功，则抛出UaaErrorException；
        if(!restTemplateForEntity.getStatusCode().is2xxSuccessful()){
            throw new UaaErrorException("错误码","错误信息");
        }

        //2.3) 判断认证是否成功，如TokenParamDto中的businessCode字段不为"00000000",则判断为错误。根据实际情况自行判断
        if(!restTemplateForEntity.getBody().getBusinessCode().equals("00000000")){

            //2.4) Uaa认证异常统一抛UaaOauthException()
            throw new UaaOauthException("错误码","错误消息");
        }

        return restTemplateForEntity.getBody();
    }

}

``` 
- 2 前端变更请求参数
***
![](doc/picture/qianduanlogin.jpg) 
