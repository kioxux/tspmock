package cn.com.yusys.yusp.commons.session.user.impl;

import cn.com.yusys.yusp.commons.session.user.Menu;

import java.io.Serializable;
import java.util.List;

/**
 * 缺省菜单DTO
 *
 * @author dusong
 * @since 2.1.1
 */
public class MenuImpl implements Menu, Serializable {

    private static final long serialVersionUID = 1779666267546345663L;
    /**
     * 菜单记录编号
     */
    private String menuId;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 上级菜单编号
     */
    private String upMenuId;

    /**
     * 顺序
     */
    private int menuOrder;

    /**
     * 图标
     */
    private String menuIcon;

    /**
     * 系统业务功能URL链接
     */
    private String funcUrl;

    /**
     * 系统业务功能JS链接
     */
    private String funcUrlJs;

    /**
     * 系统业务功能CSS链接
     */
    private String funcUrlCss;

    /**
     * 所属业务功能编号
     */
    private String funcId;
    /**
     * 菜单国际化key
     */
    private String i18nKey;

    private String menuType;

    /**
     * 数据授权 菜单树menuId改为id 传递给前端
     */
    private String id;

    private String funcName;

    private String upMenuName;

    private List<MenuImpl> childrenList;

    public String getFuncId() {
        return funcId;
    }

    public void setFuncId(String funcId) {
        this.funcId = funcId;
    }

    @Override
    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    @Override
    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    @Override
    public String getUpMenuId() {
        return upMenuId;
    }

    public void setUpMenuId(String upMenuId) {
        this.upMenuId = upMenuId;
    }

    public int getMenuOrder() {
        return menuOrder;
    }

    public void setMenuOrder(int menuOrder) {
        this.menuOrder = menuOrder;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    public String getFuncUrl() {
        return funcUrl;
    }

    public void setFuncUrl(String funcUrl) {
        this.funcUrl = funcUrl;
    }

    public String getFuncUrlJs() {
        return funcUrlJs;
    }

    public void setFuncUrlJs(String funcUrlJs) {
        this.funcUrlJs = funcUrlJs;
    }

    public String getFuncUrlCss() {
        return funcUrlCss;
    }

    public void setFuncUrlCss(String funcUrlCss) {
        this.funcUrlCss = funcUrlCss;
    }

    public String getI18nKey() {
        return i18nKey;
    }

    public void setI18nKey(String i18nKey) {
        this.i18nKey = i18nKey;
    }

    public String getMenuType() {
        return menuType;
    }

    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFuncName() {
        return funcName;
    }

    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }

    public String getUpMenuName() {
        return upMenuName;
    }

    public void setUpMenuName(String upMenuName) {
        this.upMenuName = upMenuName;
    }

    public List<MenuImpl> getChildrenList() {
        return childrenList;
    }

    public void setChildrenList(List<MenuImpl> childrenList) {
        this.childrenList = childrenList;
    }
}
