package cn.com.yusys.yusp.uaa.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description:
 * @author lty
 * @date 2021/3/29　　
 */
@Data
@NoArgsConstructor
public class MobileInfo extends TokenParamDto{
    private String mobile_code;
}
