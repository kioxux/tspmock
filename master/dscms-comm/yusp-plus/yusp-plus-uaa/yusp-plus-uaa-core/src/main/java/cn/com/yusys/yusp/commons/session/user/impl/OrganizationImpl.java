package cn.com.yusys.yusp.commons.session.user.impl;

import cn.com.yusys.yusp.commons.session.user.Organization;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OrganizationImpl implements Organization {

    /**
     * 记录编号
     */
    @JsonProperty(value = "id")
    private String orgId;
    /**
     * 角色代码
     */
    @JsonProperty(value = "code")
    private String orgCode;
    /**
     * 角色名称
     */
    @JsonProperty(value = "name")
    private String orgName;

    /**
     * 机构层级
     */
    private String orgLevel;

    @Override
    public String getName() {
        return this.orgName;
    }

    @Override
    public String getCode() {
        return this.orgCode;
    }

    @Override
    public String getId() {
        return this.orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public void setOrgLevel(String orgLevel) {
        this.orgLevel = orgLevel;
    }
}
