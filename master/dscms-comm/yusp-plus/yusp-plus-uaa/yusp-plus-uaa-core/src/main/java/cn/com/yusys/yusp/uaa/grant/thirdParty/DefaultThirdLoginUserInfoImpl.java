package cn.com.yusys.yusp.uaa.grant.thirdParty;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.uaa.exception.UaaOauthException;
import cn.com.yusys.yusp.uaa.pojo.TokenParamDto;

/**
 * @author lty
 * @version 3.1.1-SNAPSHOT
 * @description ThirdLoginUserInfo的默认实现
 * @date 2020/12/29
 */
public class DefaultThirdLoginUserInfoImpl implements ThirdLoginUserInfo {

    @Override
    public TokenParamDto getLoginUserInfo() {

        TokenParamDto tokenParamDto = new TokenParamDto();
        tokenParamDto.setUserId("test data");
        tokenParamDto.setLoginCode("test data");
        tokenParamDto.setOrgId("test data");
        tokenParamDto.setBusinessCode("00000000");
        if (!StringUtils.isEmpty(tokenParamDto.getBusinessCode()) && !"00000000".equals(tokenParamDto.getBusinessCode())) {
            throw new UaaOauthException("错误码", "错误消息");
        }
        return tokenParamDto;
    }

}
