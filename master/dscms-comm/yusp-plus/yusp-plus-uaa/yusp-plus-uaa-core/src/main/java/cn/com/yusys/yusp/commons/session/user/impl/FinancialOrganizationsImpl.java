package cn.com.yusys.yusp.commons.session.user.impl;

import cn.com.yusys.yusp.commons.session.user.FinancialOrganizations;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancialOrganizationsImpl implements FinancialOrganizations {

    /**
     * 记录编号
     */
    @JsonProperty(value = "id")
    private String instuId;
    /**
     * 部门代码
     */
    @JsonProperty(value = "code")
    private String instuCde;
    /**
     * 部门名称
     */
    @JsonProperty(value = "name")
    private String instuName;


    @Override
    public String getName() {
        return this.instuName;
    }

    @Override
    public String getCode() {
        return this.instuCde;
    }

    @Override
    public String getId() {
        return this.instuId;
    }

    public void setInstuId(String instuId) {
        this.instuId = instuId;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public void setInstuName(String instuName) {
        this.instuName = instuName;
    }
}
