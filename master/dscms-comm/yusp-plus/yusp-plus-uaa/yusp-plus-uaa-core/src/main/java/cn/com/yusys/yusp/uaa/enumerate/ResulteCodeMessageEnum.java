package cn.com.yusys.yusp.uaa.enumerate;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description: 返回结果信息
 * @author lty
 * @date 2021/3/29　　
 */
public enum  ResulteCodeMessageEnum {

    SMS_SENT("88000001","验证码已发送，请勿重复点击"),
    FIRSTLOGIN("10000002","首次登录请修改密码");

    private String code;
    private String message;

    ResulteCodeMessageEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
