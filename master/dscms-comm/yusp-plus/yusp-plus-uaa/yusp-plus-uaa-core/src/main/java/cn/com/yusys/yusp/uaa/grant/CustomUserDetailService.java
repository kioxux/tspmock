package cn.com.yusys.yusp.uaa.grant;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author lty
 * @version: 3.1.1-SNAPSHOT
 * @description: 自定义认证服务
 * @date 2020/12/29
 */
public interface CustomUserDetailService extends UserDetailsService {

    UserDetails loadUserByOca(String username);

    UserDetails loadUserByThirdParty(String username);

    UserDetails loadUserByMobile(String username);

    /**
     * @param servtp    渠道码
     * @param loginCode 登录码
     * @param orgCode   机构码
     * @return 用户信息
     */
    UserDetails loadUserByAppClient(String servtp, String loginCode, String orgCode);

}