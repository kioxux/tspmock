package cn.com.yusys.yusp.commons.session.user.impl;

import cn.com.yusys.yusp.commons.session.user.Role;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RoleImpl implements Role {

    /**
     * 记录编号
     */
    @JsonProperty(value = "id")
    private String roleId;
    /**
     * 角色代码
     */
    @JsonProperty(value = "code")
    private String roleCode;
    /**
     * 角色名称
     */
    @JsonProperty(value = "name")
    private String roleName;

    @Override
    public String getName() {
        return this.roleName;
    }

    @Override
    public String getCode() {
        return this.roleCode;
    }

    @Override
    public String getId() {
        return this.roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

}
