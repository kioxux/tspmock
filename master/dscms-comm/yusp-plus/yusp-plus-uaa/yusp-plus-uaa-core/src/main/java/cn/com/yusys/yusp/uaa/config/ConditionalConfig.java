package cn.com.yusys.yusp.uaa.config;

import cn.com.yusys.yusp.uaa.grant.app.AppClientLoginUserInfo;
import cn.com.yusys.yusp.uaa.grant.app.DefaultAppClientLoginUserInfoImpl;
import cn.com.yusys.yusp.uaa.grant.mobile.DefaultMobileLoginUserInfoImpl;
import cn.com.yusys.yusp.uaa.grant.mobile.MobileLoginUserInfo;
import cn.com.yusys.yusp.uaa.grant.oca.DefaultOcaLoginUserInfoImpl;
import cn.com.yusys.yusp.uaa.grant.oca.OcaLoginUserInfo;
import cn.com.yusys.yusp.uaa.grant.password.DefaultPasswordLoginUserInfoImpl;
import cn.com.yusys.yusp.uaa.grant.password.PasswordLoginUserInfo;
import cn.com.yusys.yusp.uaa.grant.thirdParty.DefaultThirdLoginUserInfoImpl;
import cn.com.yusys.yusp.uaa.grant.thirdParty.ThirdLoginUserInfo;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConditionalConfig {

    @Bean
    @ConditionalOnMissingBean(OcaLoginUserInfo.class)
    OcaLoginUserInfo ocaLoginUserInfo(){
        return new DefaultOcaLoginUserInfoImpl();
    }

    @Bean
    @ConditionalOnMissingBean(PasswordLoginUserInfo.class)
    PasswordLoginUserInfo passwordLoginUserInfo(){
        return new DefaultPasswordLoginUserInfoImpl();
    }

    @ConditionalOnMissingBean(ThirdLoginUserInfo.class)
    @Bean
    ThirdLoginUserInfo thirdLoginUserInfo(){
        return new DefaultThirdLoginUserInfoImpl();
    }

    @Bean
    @ConditionalOnMissingBean(MobileLoginUserInfo.class)
    MobileLoginUserInfo mobileLoginUserInfo(){
        return new DefaultMobileLoginUserInfoImpl();
    }

    @Bean
    @ConditionalOnMissingBean(AppClientLoginUserInfo.class)
    AppClientLoginUserInfo appClientLoginUserInfo(){
        return new DefaultAppClientLoginUserInfoImpl();
    }

}
