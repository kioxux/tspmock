package cn.com.yusys.yusp.uaa.pojo;

/**
 * @version 3.1.1-SNAPSHOT
 * @description 与oca交互所需请求参数信息
 * @author lty
 * @date 2020/12/29　　
 */
public class LoginOcaRequestDto {

    private String loginCode;

    private String password;

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
