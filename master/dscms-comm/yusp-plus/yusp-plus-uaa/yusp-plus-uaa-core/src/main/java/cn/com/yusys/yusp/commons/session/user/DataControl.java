package cn.com.yusys.yusp.commons.session.user;

/**
 * 数据权限资源详情.
 * <p>
 * 微服务提供查询用户控制数据权限接口使用的数据
 *
 * @author dusong
 * @since 2.1.1
 */
public interface DataControl {

    /**
     * 控制点记录编号(为*时表示默认过滤器).
     *
     * @return
     */
    String getContrId();

    /**
     * 控制点URL信息.
     *
     * @return
     */
    String getContrUrl();

    /**
     * 权限模板编号.
     *
     * @return
     */
    String getAuthTmplId();

    /**
     * 数据权限SQL条件.
     *
     * @return
     */
    String getSqlString();

    /**
     * SQL占位符名称.
     *
     * @return
     */
    String getSqlName();

    /**
     * 可用的控制点记录编号(*表示都可用).
     *
     * @return
     */
    String getContrInclude();

    /**
     * 数据模板优先级, 值越小优先级越高.
     *
     * @return
     */
    String getPriority();

    /**
     * 控制点的httpMethod
     * @return
     */
    String getHttpMethod();

}
