package cn.com.yusys.yusp.commons.session.user.impl;

import cn.com.yusys.yusp.commons.session.user.User;

import java.io.Serializable;
import java.util.List;

/**
 * 缺省用户信息
 *
 * @author dusong
 * @since 2.1.1
 */
public class UserInformation implements User, Serializable {
    private static final long serialVersionUID = -8366929034564774130L;

    /**
     * 用户Id
     */
    private String userId;
    /**
     * 登录代码
     */
    private String loginCode;
    /**
     * 姓名
     */
    private String userName;
    /**
     * 用户编号
     */
    private String userCode;
    /**
     * 用户头像
     */
    private String userAvatar;
    /**
     * 用户邮箱
     */
    private String userEmail;
    /**
     * 移动电话
     */
    private String userMobilephone;

    /**
     * 登录时间
     */
    private String lastLoginTime;

    /**
     * 用户所属机构机构层级
     */
    private String orgLevel;

    private DepartmentImpl dpt;

    private DepartmentImpl upDpt;

    private List<RoleImpl> roles;

    private OrganizationImpl org;

    private OrganizationImpl upOrg;

    private FinancialOrganizationsImpl instuOrg;

    private ClientImpl logicSys;

    @Override
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    @Override
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserMobilephone() {
        return userMobilephone;
    }

    public void setUserMobilephone(String userMobilephone) {
        this.userMobilephone = userMobilephone;
    }

    public String getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getOrgLevel() {
        return orgLevel;
    }

    public void setOrgLevel(String orgLevel) {
        this.orgLevel = orgLevel;
    }

    @Override
    public DepartmentImpl getDpt() {
        return dpt;
    }

    public void setDpt(DepartmentImpl dpt) {
        this.dpt = dpt;
    }

    @Override
    public DepartmentImpl getUpDpt() {
        return upDpt;
    }

    public void setUpDpt(DepartmentImpl upDpt) {
        this.upDpt = upDpt;
    }

    @Override
    public List<RoleImpl> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleImpl> roles) {
        this.roles = roles;
    }

    @Override
    public OrganizationImpl getOrg() {
        return org;
    }

    public void setOrg(OrganizationImpl org) {
        this.org = org;
    }

    @Override
    public OrganizationImpl getUpOrg() {
        return upOrg;
    }

    public void setUpOrg(OrganizationImpl upOrg) {
        this.upOrg = upOrg;
    }

    @Override
    public FinancialOrganizationsImpl getInstuOrg() {
        return instuOrg;
    }

    public void setInstuOrg(FinancialOrganizationsImpl instuOrg) {
        this.instuOrg = instuOrg;
    }

    @Override
    public ClientImpl getLogicSys() {
        return logicSys;
    }

    public void setLogicSys(ClientImpl logicSys) {
        this.logicSys = logicSys;
    }

}
