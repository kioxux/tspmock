package cn.com.yusys.yusp.uaa.config.token;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
/**
 * @version: 3.1.2-SNAPSHOT
 * @description: 互斥策略 默认关闭
 * @author lty
 * @date 2021/4/1　　
 */
@Data
@ConfigurationProperties(prefix = "yusp.uaa.mutex")
public class CustomUaaStrategyProperties {

    //开关
    private boolean enable =false;

}
