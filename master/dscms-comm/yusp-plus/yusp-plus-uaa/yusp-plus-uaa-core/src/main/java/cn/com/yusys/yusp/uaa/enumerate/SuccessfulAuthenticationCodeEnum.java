package cn.com.yusys.yusp.uaa.enumerate;


public enum SuccessfulAuthenticationCodeEnum {

    SUCCESSFULLY_CHECKED("00000000","重置密码成功"),
    FIRSTLOGIN("10000002","首次登录请修改密码");

    private String code;
    private String message;

    SuccessfulAuthenticationCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
