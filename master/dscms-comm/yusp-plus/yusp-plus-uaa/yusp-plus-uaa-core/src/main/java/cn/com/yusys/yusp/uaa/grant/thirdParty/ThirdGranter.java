package cn.com.yusys.yusp.uaa.grant.thirdParty;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.uaa.enumerate.UaaErrorCodeEnum;
import cn.com.yusys.yusp.uaa.exception.UaaErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description: 授权入口
 * @author lty
 * @date 2020/12/29　　
 */
public class ThirdGranter extends AbstractTokenGranter {
    //授权类型为oca
    private static final String GRANT_TYPE = "third_party";

    private final AuthenticationManager authenticationManager;

    public ThirdGranter(AuthenticationManager authenticationManager, AuthorizationServerTokenServices tokenServices
            , ClientDetailsService clientDetailsService, OAuth2RequestFactory requestFactory) {
        super(tokenServices, clientDetailsService, requestFactory, GRANT_TYPE);
        this.authenticationManager = authenticationManager;
    }

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    /**
     * @description： 获取前端传来的数据
     * @author： lty
     * @date： 2020/12/29 　　
     */
    @Override
    protected OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest) {
        Map<String, String> parameters = new LinkedHashMap<>(tokenRequest.getRequestParameters());
        String username = parameters.get("username");
        if(StringUtils.isEmpty(username)){
            log.error(UaaErrorCodeEnum.SYSTEM_ERROR.getMessage()+"Request param username missing");
            throw new UaaErrorException(UaaErrorCodeEnum.SYSTEM_ERROR.getCode(),UaaErrorCodeEnum.SYSTEM_ERROR.getMessage());
        }
        String password = parameters.get("password");
        // Protect from downstream leaks of password
        parameters.remove("password");

        Authentication userAuth = new ThirdAuthenticationToken(username, password);
        ((AbstractAuthenticationToken) userAuth).setDetails(parameters);
        userAuth = authenticationManager.authenticate(userAuth);
        if (userAuth == null || !userAuth.isAuthenticated()) {
            throw new InvalidGrantException("Could not authenticate oca: " + username);
        }

        OAuth2Request storedOAuth2Request = getRequestFactory().createOAuth2Request(client, tokenRequest);
        return new OAuth2Authentication(storedOAuth2Request, userAuth);
    }
}