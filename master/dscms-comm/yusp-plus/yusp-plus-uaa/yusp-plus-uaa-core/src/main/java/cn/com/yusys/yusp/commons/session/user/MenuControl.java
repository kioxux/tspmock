package cn.com.yusys.yusp.commons.session.user;

import java.util.List;
import java.util.Map;

/**
 * 用户被授权的菜单和控制点资源详情
 *
 * @author dusong
 * @since 2.1.1
 */
public interface MenuControl {

    /**
     * 用户被授权的菜单资源详情
     */
    List<? extends Menu> getMenu();

    /**
     * 用户被授权的控制点资源详情
     */
    List<? extends Control> getContr();

    /**
     * 需要扩展的信息
     */
    Map<String, Object> getDetails();

}
