package cn.com.yusys.yusp.commons.session.user;

/**
 * 菜单资源详情.
 * <p>
 * 存入缓存中的用户被授权的菜单信息结构
 *
 * @author dusong
 * @since 2.1.1
 */
public interface Menu {

    /**
     * 菜单记录编号
     */
    String getMenuId();

    /**
     * 菜单名称
     */
    String getMenuName();

    /**
     * 获得上级菜单
     *
     * @return
     */
    String getUpMenuId();

}
