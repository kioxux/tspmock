package cn.com.yusys.yusp.uaa.grant.oca;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.uaa.exception.UaaOauthException;
import cn.com.yusys.yusp.uaa.pojo.TokenParamDto;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description:  OcaLoginUserInfo的默认实现
 * @author lty
 * @date 2020/12/29　　
 */
public class DefaultOcaLoginUserInfoImpl implements OcaLoginUserInfo {

    @Override
    public TokenParamDto getLoginUserInfo(String username ,String password) {
//        if(!Constants.DEFAULT_SWAGGER_USER.equals(username)||!Constants.DEFAULT_SWAGGER_CERTIFICATE.equals(password)) {
//            throw new UaaOauthException(UaaErrorCodeEnum.AUTHHENTICATION_FAILURE.getCode(),UaaErrorCodeEnum.AUTHHENTICATION_FAILURE.getMessage());
//        }
        TokenParamDto tokenParamDto = new TokenParamDto();
        tokenParamDto.setUserId("test data");
        tokenParamDto.setLoginCode("test data");
        tokenParamDto.setOrgId("test data");
        tokenParamDto.setBusinessCode("00000000");
        if(StringUtils.isEmpty(tokenParamDto.getBusinessCode())||!"00000000".equals(tokenParamDto.getBusinessCode())){
            throw new UaaOauthException("错误码","错误消息");
        }
        return tokenParamDto;
    }
}
