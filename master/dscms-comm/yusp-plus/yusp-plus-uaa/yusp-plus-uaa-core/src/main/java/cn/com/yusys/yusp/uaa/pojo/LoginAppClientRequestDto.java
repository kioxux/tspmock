package cn.com.yusys.yusp.uaa.pojo;

/**
 * 移动端验证
 *
 * @author yangzai
 * @since 2021/5/31
 **/
public class LoginAppClientRequestDto {

    /**
     * 渠道码
     */
    private String servtp;

    /**
     * 登录码
     */
    private String loginCode;

    /**
     * 机构码
     */
    private String orgCode;

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }
}