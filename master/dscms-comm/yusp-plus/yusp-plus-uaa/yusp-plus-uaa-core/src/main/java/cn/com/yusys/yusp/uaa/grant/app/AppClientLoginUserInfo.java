package cn.com.yusys.yusp.uaa.grant.app;

import cn.com.yusys.yusp.uaa.pojo.TokenParamDto;

/**
 * @author lty
 * @version 3.1.1-SNAPSHOT
 * @description 获取Oca的UserInfo信息
 * @date 2020/12/29
 */
public interface AppClientLoginUserInfo {

    TokenParamDto getLoginUserInfo(String servtp, String loginCode, String orgCode);

}