package cn.com.yusys.yusp.uaa.grant.app;

import cn.com.yusys.yusp.uaa.grant.oca.OcaAuthenticationToken;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author lty
 * @version 3.1.1-SNAPSHOT
 * @description 授权入口
 * @date 2020/12/29
 */
public class AppClientGranter extends AbstractTokenGranter {

    // 授权类型为appclient
    private static final String GRANT_TYPE = "appclient";

    private final AuthenticationManager authenticationManager;

    public AppClientGranter(AuthenticationManager authenticationManager, AuthorizationServerTokenServices tokenServices
            , ClientDetailsService clientDetailsService, OAuth2RequestFactory requestFactory) {
        super(tokenServices, clientDetailsService, requestFactory, GRANT_TYPE);
        this.authenticationManager = authenticationManager;
    }

    /**
     * @description 获取前端传来的数据
     * @author lty
     * @date 2020/12/29
     */
    @Override
    protected OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest) {
        Map<String, String> parameters = new LinkedHashMap<>(tokenRequest.getRequestParameters());
        String loginCode = parameters.get("loginCode");
        String orgCode = parameters.get("orgCode");

        Authentication userAuth = new AppClientAuthenticationToken(loginCode, orgCode);
        ((AbstractAuthenticationToken) userAuth).setDetails(parameters);
        userAuth = authenticationManager.authenticate(userAuth);
        if (userAuth == null || !userAuth.isAuthenticated()) {
            throw new InvalidGrantException("Could not authenticate appclient: " + loginCode);
        }

        OAuth2Request storedOAuth2Request = getRequestFactory().createOAuth2Request(client, tokenRequest);
        return new OAuth2Authentication(storedOAuth2Request, userAuth);
    }

}