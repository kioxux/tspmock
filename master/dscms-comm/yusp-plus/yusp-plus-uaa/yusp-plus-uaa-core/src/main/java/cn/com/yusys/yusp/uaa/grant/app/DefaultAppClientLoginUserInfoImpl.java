package cn.com.yusys.yusp.uaa.grant.app;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.uaa.exception.UaaOauthException;
import cn.com.yusys.yusp.uaa.pojo.TokenParamDto;

/**
 * @author lty
 * @version 3.1.1-SNAPSHOT
 * @description OcaLoginUserInfo的默认实现
 * @date 2020/12/29
 */
public class DefaultAppClientLoginUserInfoImpl implements AppClientLoginUserInfo {

    @Override
    public TokenParamDto getLoginUserInfo(String servtp, String loginCode, String orgCode) {
        TokenParamDto tokenParamDto = new TokenParamDto();
        tokenParamDto.setUserId(loginCode);
        tokenParamDto.setLoginCode(loginCode);
        tokenParamDto.setOrgId(orgCode);
        tokenParamDto.setBusinessCode("00000000");
        if (StringUtils.isEmpty(tokenParamDto.getBusinessCode()) || !"00000000".equals(tokenParamDto.getBusinessCode())) {
            throw new UaaOauthException("错误码", "错误消息");
        }
        return tokenParamDto;
    }

}