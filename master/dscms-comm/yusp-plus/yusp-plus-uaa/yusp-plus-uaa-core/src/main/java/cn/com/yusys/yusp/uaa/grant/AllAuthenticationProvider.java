package cn.com.yusys.yusp.uaa.grant;

import cn.com.yusys.yusp.uaa.grant.app.AppClientAuthenticationToken;
import cn.com.yusys.yusp.uaa.grant.mobile.MobileAuthenticationToken;
import cn.com.yusys.yusp.uaa.grant.oca.OcaAuthenticationToken;
import cn.com.yusys.yusp.uaa.grant.thirdParty.ThirdAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Objects;

/**
 * @author lty
 * @version 3.1.1-SNAPSHOT
 * @description 所有的AuthenticationProvider逻辑在这里验证
 * @date 2020/12/29
 */
@Component
public class AllAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    CustomUserDetailService customUserDetailService;

    /**
     * @description 配置认证方式
     * @author lty
     * @date 2020/12/29
     */
    @Override
    public Authentication authenticate(Authentication authentication) {
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        String grantType = httpServletRequest.getParameter("grant_type");
        if ("password".equals(grantType)) {
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = (UsernamePasswordAuthenticationToken) authentication;
            return usernamePasswordAuthenticationinfo(usernamePasswordAuthenticationToken);
        } else if ("oca".equals(grantType)) {
            OcaAuthenticationToken ocaAuthenticationToken = (OcaAuthenticationToken) authentication;
            return ocaAuthenticationTokenInfo(ocaAuthenticationToken);
        } else if ("third_party".equals(grantType)) {
            ThirdAuthenticationToken thirdAuthenticationToken = (ThirdAuthenticationToken) authentication;
            return thirdAuthenticationTokenInfo(thirdAuthenticationToken);
        } else if ("mobile".equals(grantType)) {
            MobileAuthenticationToken mobileAuthenticationToken = (MobileAuthenticationToken) authentication;
            return mobileAuthenticationTokenInfo(mobileAuthenticationToken);
        } else if ("appclient".equals(grantType)) {
            AppClientAuthenticationToken appClientAuthenticationToken = (AppClientAuthenticationToken) authentication;
            return appClientAuthenticationTokenInfo(appClientAuthenticationToken);
        }
        return null;
    }


    @Override
    public boolean supports(Class<?> authentication) {
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        String grantType = httpServletRequest.getParameter("grant_type");
        if ("password".equals(grantType)) {
            return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
        } else if ("oca".equals(grantType)) {
            return OcaAuthenticationToken.class.isAssignableFrom(authentication);
        } else if ("third_party".equals(grantType)) {
            return ThirdAuthenticationToken.class.isAssignableFrom(authentication);
        } else if ("mobile".equals(grantType)) {
            return MobileAuthenticationToken.class.isAssignableFrom(authentication);
        } else if ("appclient".equals(grantType)) {
            return AppClientAuthenticationToken.class.isAssignableFrom(authentication);
        }
        return false;
    }

    /**
     * @description third_party模式的认证
     * @author lty
     * @date 2020/12/30
     */
    private Authentication thirdAuthenticationTokenInfo(ThirdAuthenticationToken thirdAuthenticationToken) {
        String username = (String) thirdAuthenticationToken.getPrincipal();
        String password = (String) thirdAuthenticationToken.getCredentials();
        UserDetails user = customUserDetailService.loadUserByThirdParty(username);
        ThirdAuthenticationToken authenticationResult = new ThirdAuthenticationToken(user, password, user.getAuthorities());
        authenticationResult.setDetails(thirdAuthenticationToken.getDetails());
        return authenticationResult;
    }

    /**
     * @description oca模式的认证
     * @author lty
     * @date 2020/12/29
     */
    private OcaAuthenticationToken ocaAuthenticationTokenInfo(OcaAuthenticationToken ocaAuthenticationToken) {
        String username = (String) ocaAuthenticationToken.getPrincipal();
        String password = (String) ocaAuthenticationToken.getCredentials();
        UserDetails user = customUserDetailService.loadUserByOca(username);
        OcaAuthenticationToken authenticationResult = new OcaAuthenticationToken(user, password, user.getAuthorities());
        authenticationResult.setDetails(ocaAuthenticationToken.getDetails());
        return authenticationResult;
    }

    /**
     * @description 密码模式的认证
     * @author lty
     * @date 2020/12/29
     */
    private UsernamePasswordAuthenticationToken usernamePasswordAuthenticationinfo(UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) {
        String username = (String) usernamePasswordAuthenticationToken.getPrincipal();
        String password = (String) usernamePasswordAuthenticationToken.getCredentials();
        UserDetails user = customUserDetailService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken authenticationResult = new UsernamePasswordAuthenticationToken(user, password, user.getAuthorities());
        authenticationResult.setDetails(usernamePasswordAuthenticationToken.getDetails());
        return authenticationResult;
    }

    /**
     * @description 手机模式的认证
     * @author lty
     * @date 2021/3/29
     */
    private Authentication mobileAuthenticationTokenInfo(MobileAuthenticationToken mobileAuthenticationToken) {
        String mobile = (String) mobileAuthenticationToken.getPrincipal();
        String code = (String) mobileAuthenticationToken.getCredentials();
        UserDetails user = customUserDetailService.loadUserByMobile(mobile);
        MobileAuthenticationToken authenticationResult = new MobileAuthenticationToken(user, code, user.getAuthorities());
        authenticationResult.setDetails(mobileAuthenticationToken.getDetails());
        return authenticationResult;
    }

    /**
     * @description 移动端认证
     * @author lty
     * @date 2020/12/29
     */
    private AppClientAuthenticationToken appClientAuthenticationTokenInfo(AppClientAuthenticationToken appClientAuthenticationToken) {
        Map<String, String> detailsMap = (Map<String, String>) appClientAuthenticationToken.getDetails();
        String servtp = detailsMap.get("servtp");
        String loginCode = detailsMap.get("loginCode");
        String orgCode = detailsMap.get("orgCode");
        UserDetails user = customUserDetailService.loadUserByAppClient(servtp, loginCode, orgCode);
        AppClientAuthenticationToken authenticationResult = new AppClientAuthenticationToken(user, loginCode, user.getAuthorities());
        authenticationResult.setDetails(appClientAuthenticationToken.getDetails());
        return authenticationResult;
    }

}
