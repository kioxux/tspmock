package cn.com.yusys.yusp.uaa.grant.oca;

import cn.com.yusys.yusp.uaa.pojo.TokenParamDto;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description: 获取Oca的UserInfo信息
 * @author lty
 * @date 2020/12/29　　
 */
public interface OcaLoginUserInfo {
    TokenParamDto getLoginUserInfo(String username, String password);
}
