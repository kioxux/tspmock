package cn.com.yusys.yusp.commons.session.user.impl;

import cn.com.yusys.yusp.commons.session.user.MenuControl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 缺省菜单控制点DTO.
 *
 * @author dusong
 * @since 2.1.1
 */
public class MenuControlImpl implements MenuControl, Serializable {
    private static final long serialVersionUID = -5464765872441970689L;
    /**
     * 用户被授权的菜单资源详情
     */
    private List<MenuImpl> menu;

    /**
     * 用户被授权的控制点资源详情
     */
    private List<ControlImpl> contr;

    /**
     * 需要扩展的信息
     */
    private Map<String, Object> details;

    public MenuControlImpl() {
    }

    public MenuControlImpl(List<MenuImpl> menu, List<ControlImpl> contr) {
        this.menu = menu;
        this.contr = contr;
    }

    @Override
    public List<MenuImpl> getMenu() {
        return menu;
    }

    public void setMenu(List<MenuImpl> menu) {
        this.menu = menu;
    }

    @Override
    public List<ControlImpl> getContr() {
        return contr;
    }

    public void setContr(List<ControlImpl> contr) {
        this.contr = contr;
    }

    @Override
    public Map<String, Object> getDetails() {
        return this.details;
    }

    public void setDetails(Map<String, Object> details) {
        this.details = details;
    }
}
