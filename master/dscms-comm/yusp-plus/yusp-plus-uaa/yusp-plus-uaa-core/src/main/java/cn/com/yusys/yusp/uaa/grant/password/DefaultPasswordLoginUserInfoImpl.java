package cn.com.yusys.yusp.uaa.grant.password;

import cn.com.yusys.yusp.commons.module.constant.Constants;
import cn.com.yusys.yusp.commons.session.user.impl.OrganizationImpl;
import cn.com.yusys.yusp.commons.session.user.impl.UserInformation;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.uaa.enumerate.UaaErrorCodeEnum;
import cn.com.yusys.yusp.uaa.exception.UaaOauthException;
import cn.com.yusys.yusp.uaa.pojo.TokenParamDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author lty
 * @version: 3.1.1-SNAPSHOT
 * @description: password模式的默认实现
 * @date 2020/12/30
 */
public class DefaultPasswordLoginUserInfoImpl implements PasswordLoginUserInfo {

    private static final Logger logger = LoggerFactory.getLogger(DefaultPasswordLoginUserInfoImpl.class);

    @Autowired
    SwaggerCacheService swaggerCacheService;

    @Override
    public TokenParamDto getLoginUserInfo(String username, String password) {
        if (!Constants.DEFAULT_SWAGGER_USER.equals(username) || !Constants.DEFAULT_SWAGGER_CERTIFICATE.equals(password)) {
            throw new UaaOauthException(UaaErrorCodeEnum.AUTHHENTICATION_FAILURE.getCode(), UaaErrorCodeEnum.AUTHHENTICATION_FAILURE.getMessage());
        }
        String clientId = "test";
        String userId = "Swagger";
        String loginCode = "Swagger";
        String orgId = "swaggerOrgId";
        String orgCode = "SwaggerOrgCode";
        TokenParamDto tokenParamDto = new TokenParamDto();
        tokenParamDto.setUserId(userId);
        tokenParamDto.setLoginCode(loginCode);
        tokenParamDto.setOrgId(orgId);
        tokenParamDto.setBusinessCode("00000000");
        if (StringUtils.isEmpty(tokenParamDto.getBusinessCode()) || !"00000000".equals(tokenParamDto.getBusinessCode())) {
            throw new UaaOauthException("错误码", "错误消息");
        }
        UserInformation userInformation = new UserInformation();
        userInformation.setLoginCode(loginCode);
        userInformation.setUserId(userId);
        OrganizationImpl org = new OrganizationImpl();
        org.setOrgId(orgId);
        org.setOrgCode(orgCode);
        userInformation.setOrg(org);
        // 模拟swagger用户登录后往Redis中存信息，方便SessionUtils获取用户信息
        swaggerCacheService.getUserInfo(clientId, userId, userInformation);
        return tokenParamDto;
    }

}
