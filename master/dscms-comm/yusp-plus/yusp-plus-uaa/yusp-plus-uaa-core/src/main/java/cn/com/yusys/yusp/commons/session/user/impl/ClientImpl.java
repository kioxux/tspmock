package cn.com.yusys.yusp.commons.session.user.impl;

import cn.com.yusys.yusp.commons.session.user.Client;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientImpl implements Client {

    /**
     * 记录编号
     */
    @JsonProperty(value = "id")
    private String sysId;
    /**
     * 部门代码
     */
    @JsonProperty(value = "code")
    private String sysCode;
    /**
     * 部门名称
     */
    @JsonProperty(value = "name")
    private String sysName;

    @Override
    public String getName() {
        return this.sysName;
    }

    @Override
    public String getCode() {
        return this.sysCode;
    }

    @Override
    public String getId() {
        return this.sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public void setSysCode(String sysCode) {
        this.sysCode = sysCode;
    }

    public void setSysName(String sysName) {
        this.sysName = sysName;
    }
}
