package cn.com.yusys.yusp.uaa.pojo;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @类名 LoginMobileRequestDto
 * @描述 oca交互所需
 * @作者 黄勃
 * @时间 2021/9/23 10:52
 **/
@Data
public class LoginMobileRequestDto {
    private String LoginCode;

    private String mobile;
}
