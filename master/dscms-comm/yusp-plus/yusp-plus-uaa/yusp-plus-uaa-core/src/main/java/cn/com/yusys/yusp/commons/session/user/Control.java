package cn.com.yusys.yusp.commons.session.user;

/**
 * 控制点资源详情.
 * 存入缓存中的用户被授权的控制点信息结构;
 *
 * @author dusong
 * @since 2.1.1
 */
public interface Control {

    /**
     * 控制操作URL(用于后台校验时使用)
     */
    String getContrUrl();

    /**
     * 请求类型
     */
    String getMethodType();

}