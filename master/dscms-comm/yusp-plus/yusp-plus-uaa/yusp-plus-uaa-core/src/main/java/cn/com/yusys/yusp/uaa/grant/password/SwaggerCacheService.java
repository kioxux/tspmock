package cn.com.yusys.yusp.uaa.grant.password;

import cn.com.yusys.yusp.commons.session.user.impl.UserInformation;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @author yangzai
 * @since 2021/5/18
 **/
@Service
public class SwaggerCacheService {

    @Cacheable(value = {"Session"}, key = "#p0+'-'+#p1")
    public UserInformation getUserInfo(String clientId, String userId, UserInformation userInformation) {
        return userInformation;
    }

}