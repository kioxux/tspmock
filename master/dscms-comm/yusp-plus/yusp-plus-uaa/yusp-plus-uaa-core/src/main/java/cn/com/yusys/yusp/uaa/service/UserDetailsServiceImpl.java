package cn.com.yusys.yusp.uaa.service;

import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.uaa.grant.app.AppClientLoginUserInfo;
import cn.com.yusys.yusp.uaa.pojo.LoginUserInfo;
import cn.com.yusys.yusp.uaa.pojo.TokenParamDto;
import cn.com.yusys.yusp.uaa.enumerate.UaaErrorCodeEnum;
import cn.com.yusys.yusp.uaa.exception.UaaErrorException;
import cn.com.yusys.yusp.uaa.exception.UaaOauthException;
import cn.com.yusys.yusp.uaa.grant.CustomUserDetailService;
import cn.com.yusys.yusp.uaa.grant.mobile.MobileLoginUserInfo;
import cn.com.yusys.yusp.uaa.grant.thirdParty.ThirdLoginUserInfo;
import cn.com.yusys.yusp.uaa.grant.oca.OcaLoginUserInfo;
import cn.com.yusys.yusp.uaa.grant.password.PasswordLoginUserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class UserDetailsServiceImpl implements CustomUserDetailService {

    //设置redis的缓存key值
    @Value("${uaa.image.code:IMAGE_CODE_REDIS_KEY}")
    public String IMAGE_CODE_REDIS_KEY;

    @Autowired
    private PasswordLoginUserInfo passwordLoginUserInfo;

    @Autowired
    private ThirdLoginUserInfo thirdLoginUserInfo;

    @Autowired
    private OcaLoginUserInfo ocaLoginUserInfo;

    @Autowired
    private MobileLoginUserInfo mobileLoginUserInfo;

    @Autowired
    private AppClientLoginUserInfo appClientLoginUserInfo;

    @Autowired
    private ClientDetailsService clientDetailsService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public UserDetailsServiceImpl() {
    }

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * @description： password模式下的认证，基于用户名密码，测试使用
     * @author： lty
     * @date： 2020/12/29
     * @param： username
     * @return： LoginUserInfo
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //1)验证图片验证码是否正确
        verifyCodeImage();

        //2)获取LoginUserInfo信息
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        String password = httpServletRequest.getParameter("password");
        List<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<>();
        LoginUserInfo loginUserInfo = new LoginUserInfo(username, password, simpleGrantedAuthorities);
        TokenParamDto tokenParamDto = passwordLoginUserInfo.getLoginUserInfo(username, password);
        if (tokenParamDto == null) {
            throw new UaaOauthException(UaaErrorCodeEnum.AUTHHENTICATION_FAILURE.getCode(), UaaErrorCodeEnum.AUTHHENTICATION_FAILURE.getMessage());
        }
        BeanUtils.beanCopy(tokenParamDto, loginUserInfo);

        return loginUserInfo;
    }

    /**
     * @description： oca模式下的认证
     * @author： lty
     * @date： 2020/12/29
     * @param： username
     * @return： LoginUserInfo
     * @throws： UaaException
     */
    @Override
    public UserDetails loadUserByOca(String username) {

        //1)验证图片验证码是否正确
        verifyCodeImage();

        //2)获取LoginUserInfo信息
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        String password = httpServletRequest.getParameter("password");
        List<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<>();
        LoginUserInfo loginUserInfo = new LoginUserInfo(username, password, simpleGrantedAuthorities);
        TokenParamDto tokenParamDto = ocaLoginUserInfo.getLoginUserInfo(username, password);
        if (tokenParamDto == null) {
            throw new UaaOauthException(UaaErrorCodeEnum.AUTHHENTICATION_FAILURE.getCode(), UaaErrorCodeEnum.AUTHHENTICATION_FAILURE.getMessage());
        }
        BeanUtils.beanCopy(tokenParamDto, loginUserInfo);

        return loginUserInfo;
    }

    /**
     * @description： 第三方模式下的
     * @author： lty
     * @date： 2020/12/29
     */
    @Override
    public UserDetails loadUserByThirdParty(String username) {
        //1)验证图片验证码是否正确
        verifyCodeImage();

        //2)获取LoginUserInfo信息
        List<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<>();
        LoginUserInfo loginUserInfo = new LoginUserInfo(username, "", simpleGrantedAuthorities);
        TokenParamDto tokenParamDto = thirdLoginUserInfo.getLoginUserInfo();
        if (tokenParamDto == null) {
            throw new UaaOauthException(UaaErrorCodeEnum.AUTHHENTICATION_FAILURE.getCode(), UaaErrorCodeEnum.AUTHHENTICATION_FAILURE.getMessage());
        }
        BeanUtils.beanCopy(tokenParamDto, loginUserInfo);

        return loginUserInfo;
    }

    @Override
    public UserDetails loadUserByMobile(String loginCode) {
        verifyCode();

        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        String mobile = httpServletRequest.getParameter("mobile");
        List<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<>();
        LoginUserInfo loginUserInfo = new LoginUserInfo(loginCode, mobile, simpleGrantedAuthorities);
        TokenParamDto tokenParamDto = mobileLoginUserInfo.getLoginUserInfo(mobile);
        if (tokenParamDto == null) {
            throw new UaaOauthException(UaaErrorCodeEnum.AUTHHENTICATION_FAILURE.getCode(), UaaErrorCodeEnum.AUTHHENTICATION_FAILURE.getMessage());
        }
        BeanUtils.beanCopy(tokenParamDto, loginUserInfo);

        return loginUserInfo;
    }

    @Override
    public UserDetails loadUserByAppClient(String servtp, String loginCode, String orgCode) {
        // 获取LoginUserInfo信息
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        List<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<>();
        LoginUserInfo loginUserInfo = new LoginUserInfo(loginCode, loginCode, simpleGrantedAuthorities);
        TokenParamDto tokenParamDto = appClientLoginUserInfo.getLoginUserInfo(servtp, loginCode, orgCode);
        if (tokenParamDto == null) {
            throw new UaaOauthException(UaaErrorCodeEnum.AUTHHENTICATION_FAILURE.getCode(), UaaErrorCodeEnum.AUTHHENTICATION_FAILURE.getMessage());
        }
        BeanUtils.beanCopy(tokenParamDto, loginUserInfo);
        return loginUserInfo;
    }


    /**
     * @description： 验证验证码
     * @author： lty
     * @date： 2020/12/29
     * @throws： UaaException
     */
    private void verifyCodeImage() {

        //获取client_id
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User clientDetails = (User) authentication.getPrincipal();

        //如果没有验证码功能，则数据库中该client配置需要增加额外的信息，key：image_status，value: false
        if (clientDetailsService.loadClientByClientId(clientDetails.getUsername()).getAdditionalInformation().containsKey("image_status")
                && clientDetailsService.loadClientByClientId(clientDetails.getUsername()).getAdditionalInformation().get("image_status").equals("false")) {
            return;
        }
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();

        String uuId = httpServletRequest.getParameter("imageUUID");
        if (StringUtils.isEmpty(uuId)) {
            log.error(UaaErrorCodeEnum.SYSTEM_ERROR.getMessage() + " imageUUID parameter is missing");
            throw new UaaErrorException(UaaErrorCodeEnum.SYSTEM_ERROR.getCode(), UaaErrorCodeEnum.SYSTEM_ERROR.getMessage() + " imageUUID parameter is missing");
        }

        String imageCode = httpServletRequest.getParameter("imageCode");
        if (StringUtils.isEmpty(imageCode)) {
            log.error(UaaErrorCodeEnum.IAMGE_CODE_ERROR.getMessage() + " imageCode parameter is missing");
            throw new UaaOauthException(UaaErrorCodeEnum.IAMGE_CODE_ERROR.getCode(), UaaErrorCodeEnum.IAMGE_CODE_ERROR.getMessage());
        }

        String redisImageCode = stringRedisTemplate.opsForValue().get("IMAGE_CODE_REDIS_KEY" + "-" + uuId);
        if (redisImageCode == null) {
            throw new UaaOauthException(UaaErrorCodeEnum.IAMGE_CODE_ERROR.getCode(), UaaErrorCodeEnum.IAMGE_CODE_ERROR.getMessage());
        }
        if (!redisImageCode.equals(imageCode)) {
            throw new UaaOauthException(UaaErrorCodeEnum.IAMGE_CODE_ERROR.getCode(), UaaErrorCodeEnum.IAMGE_CODE_ERROR.getMessage());
        }
    }

    private void verifyCode() {
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();

        String mobile = httpServletRequest.getParameter("mobile");
        if (StringUtils.isEmpty(mobile)) {
            log.error(UaaErrorCodeEnum.SYSTEM_ERROR.getMessage() + " mobile parameter is missing");
            throw new UaaOauthException(UaaErrorCodeEnum.SYSTEM_ERROR.getCode(), UaaErrorCodeEnum.SYSTEM_ERROR.getMessage());
        }

        String verificationCode = httpServletRequest.getParameter("verificationCode");
        if (StringUtils.isEmpty(verificationCode)) {
            log.error(UaaErrorCodeEnum.IAMGE_CODE_ERROR.getMessage() + " verificationCode parameter is missing");
            throw new UaaOauthException(UaaErrorCodeEnum.IAMGE_CODE_ERROR.getCode(), UaaErrorCodeEnum.IAMGE_CODE_ERROR.getMessage());
        }

        String redisVerificationCode = stringRedisTemplate.opsForValue().get("PASSWORD_RESET_MOBILE_CODE" + "-" + mobile);
        if (redisVerificationCode == null) {
            throw new UaaOauthException(UaaErrorCodeEnum.IAMGE_CODE_ERROR.getCode(), UaaErrorCodeEnum.IAMGE_CODE_ERROR.getMessage());
        }
        if (!redisVerificationCode.equals(verificationCode)) {
            throw new UaaOauthException(UaaErrorCodeEnum.IAMGE_CODE_ERROR.getCode(), UaaErrorCodeEnum.IAMGE_CODE_ERROR.getMessage());
        }
    }
}
