package cn.com.yusys.yusp.uaa.config;


import cn.com.yusys.yusp.uaa.grant.AllAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description: 安全配置，配置白名单等
 * @author lty
 * @date 2020/12/28　　
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    AllAuthenticationProvider allAuthenticationProvider;
    /**
     * @description: 配置AuthenticationManager
     * @author： lty
     * @date： 2020/12/28 　　
     */
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * @description： 配置静态资源白名单
     * @author： lty
     * @date： 2020/12/28 　　
     */
    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/api/mobile/**","/api/codeimage/**","/assets/**", "/css/**", "/images/**");
    }

    /**
     * @description： 配置认证链路白名单
     * @author： lty
     * @date： 2020/12/28 　　
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authenticationProvider(allAuthenticationProvider)
                .authorizeRequests()
                .antMatchers("/api/logout")
                .permitAll()
                .anyRequest().authenticated()
                .and()
                .httpBasic().and()
                .csrf().disable()
                .cors().disable()
                .headers().frameOptions().disable();
    }
}


