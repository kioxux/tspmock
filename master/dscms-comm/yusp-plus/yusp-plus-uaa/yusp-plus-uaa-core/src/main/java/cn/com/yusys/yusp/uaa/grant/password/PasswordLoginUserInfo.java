package cn.com.yusys.yusp.uaa.grant.password;

import cn.com.yusys.yusp.uaa.pojo.TokenParamDto;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description: 单体模式下获取用户信息
 * @author lty
 * @date 2020/12/29　　
 */
public interface PasswordLoginUserInfo {
    TokenParamDto getLoginUserInfo(String username,String password);
}
