package cn.com.yusys.yusp.uaa.exception;

import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description: uaa交互参数缺失异常
 * @author lty
 * @date 2020/12/29　　
 */
public class UaaErrorException extends OAuth2Exception{
    private String code;

    public UaaErrorException(String code, String msg) {
        super(msg);
        this.code = code;
    }

    public UaaErrorException(String msg, Throwable t) {
        super(msg, t);
    }

    public UaaErrorException(String msg) {
        super(msg);
    }

    @Override
    public int getHttpErrorCode() {
        return 500;
    }

    @Override
    public String getOAuth2ErrorCode() {
        return this.code;
    }
}
