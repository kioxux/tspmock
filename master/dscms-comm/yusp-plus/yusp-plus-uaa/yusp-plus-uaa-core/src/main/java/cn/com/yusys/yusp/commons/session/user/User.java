package cn.com.yusys.yusp.commons.session.user;

import java.util.List;

/**
 * 用户信息DTO.
 *
 * @author dusong
 * @since 2.1.1
 */
public interface User {

    /**
     * 登录代码.
     *
     * @return
     */
    String getLoginCode();

    /**
     * 用户id
     *
     * @return
     */
    String getUserId();

    /**
     * 姓名.
     *
     * @return
     */
    String getUserName();

    /**
     * 所属机构编号.
     *
     * @return
     */
    UserIdentity getOrg();

    /**
     * 金融机构
     *
     * @return
     */
    UserIdentity getInstuOrg();

    /**
     * 角色
     *
     * @return
     */
    List<? extends UserIdentity> getRoles();

    /**
     * 部门信息
     *
     * @return
     */
    UserIdentity getDpt();

    /**
     * 上级部门
     *
     * @return
     */
    UserIdentity getUpDpt();

    /**
     * 上级机构
     *
     * @return
     */
    UserIdentity getUpOrg();

    /**
     * 客户端信息
     *
     * @return
     */
    Client getLogicSys();
}
