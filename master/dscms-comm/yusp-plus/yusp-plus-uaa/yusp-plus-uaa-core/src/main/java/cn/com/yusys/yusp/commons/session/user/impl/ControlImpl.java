package cn.com.yusys.yusp.commons.session.user.impl;

import cn.com.yusys.yusp.commons.session.user.Control;

import java.io.Serializable;

/**
 * 缺省控制点DTO.
 *
 * @author dusong
 * @since 2.1.1
 */
public class ControlImpl implements Control, Serializable {

    private static final long serialVersionUID = 968250810551107614L;
    /**
     * 控制操作代码
     */
    private String contrCode;

    /**
     * 控制操作名称
     */
    private String contrName;

    /**
     * 控制操作URL(用于后台校验时使用)
     */
    private String contrUrl;

    /**
     * 逻辑系统记录编号
     */
    private String sysId;

    /**
     * 所属业务功能编号
     */
    private String funcId;

    /**
     * 请求类型
     */
    private String methodType;

    /**
     * 需要扩展的信息
     */
    private String details;

    public String getContrCode() {
        return contrCode;
    }

    public void setContrCode(String contrCode) {
        this.contrCode = contrCode;
    }

    public String getContrName() {
        return contrName;
    }

    public void setContrName(String contrName) {
        this.contrName = contrName;
    }

    @Override
    public String getContrUrl() {
        return contrUrl;
    }

    public void setContrUrl(String contrUrl) {
        this.contrUrl = contrUrl;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getFuncId() {
        return funcId;
    }

    public void setFuncId(String funcId) {
        this.funcId = funcId;
    }

    @Override
    public String getMethodType() {
        return methodType;
    }

    public void setMethodType(String methodType) {
        this.methodType = methodType;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
