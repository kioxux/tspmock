package cn.com.yusys.yusp.commons.session.user;

/**
 * 普通对象.
 *
 * @author dusong
 * @since 2.1.1
 */

public interface UserIdentity {
    /**
     * 名称.
     *
     * @return
     */
    String getName();

    /**
     * 代码.
     *
     * @return
     */
    String getCode();

    /**
     * ID.
     *
     * @return
     */

    String getId();

}