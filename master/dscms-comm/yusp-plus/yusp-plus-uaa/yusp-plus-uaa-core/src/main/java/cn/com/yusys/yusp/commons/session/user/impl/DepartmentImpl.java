package cn.com.yusys.yusp.commons.session.user.impl;

import cn.com.yusys.yusp.commons.session.user.Department;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DepartmentImpl implements Department {

    /**
     * 记录编号
     */
    @JsonProperty(value = "id")
    private String dptId;
    /**
     * 部门代码
     */
    @JsonProperty(value = "code")
    private String dptCde;
    /**
     * 部门名称
     */
    @JsonProperty(value = "name")
    private String dptName;


    @Override
    public String getName() {
        return this.dptName;
    }

    @Override
    public String getCode() {
        return this.dptCde;
    }

    @Override
    public String getId() {
        return this.dptId;
    }

    public void setDptId(String dptId) {
        this.dptId = dptId;
    }

    public void setDptCde(String dptCde) {
        this.dptCde = dptCde;
    }

    public void setDptName(String dptName) {
        this.dptName = dptName;
    }
}
