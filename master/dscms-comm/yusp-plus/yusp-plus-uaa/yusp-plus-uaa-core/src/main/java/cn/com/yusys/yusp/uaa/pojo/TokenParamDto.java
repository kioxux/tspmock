package cn.com.yusys.yusp.uaa.pojo;

/**
 * @author lty
 * @version 3.1.1-SNAPSHOT
 * @description 用于接收userId，orgId，LoginCode，businessCode
 * @date 2020/12/29
 */
public class TokenParamDto {

    //用户id
    private String userId;

    //机构id
    private String orgId;

    //登陆码
    private String loginCode;

    //业务状态代码
    private String businessCode;

    private Boolean loginSingleAgent;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

    public Boolean getLoginSingleAgent() {
        return loginSingleAgent;
    }

    public void setLoginSingleAgent(Boolean loginSingleAgent) {
        this.loginSingleAgent = loginSingleAgent;
    }

}
