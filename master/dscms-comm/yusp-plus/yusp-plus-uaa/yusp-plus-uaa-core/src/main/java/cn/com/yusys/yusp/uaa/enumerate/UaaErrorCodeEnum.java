package cn.com.yusys.yusp.uaa.enumerate;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description: uaa抛出的错误码
 * @author lty
 * @date 2020/12/29　　
 */
public enum UaaErrorCodeEnum {

    SYSTEM_ERROR("system error","系统错误，请联系管理员！"),
    IAMGE_CODE_ERROR("10000004","请输入正确的验证码！"),

    AUTHHENTICATION_FAILURE("10000001","请输入正确的用户名或密码！");

    private String code;
    private String message;

    UaaErrorCodeEnum(String code ,String message) {
        this.code=code;
        this.message=message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
