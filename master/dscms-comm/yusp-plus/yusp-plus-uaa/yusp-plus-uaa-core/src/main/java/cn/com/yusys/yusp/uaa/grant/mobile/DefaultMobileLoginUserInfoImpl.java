package cn.com.yusys.yusp.uaa.grant.mobile;

import cn.com.yusys.yusp.uaa.pojo.MobileInfo;
import cn.com.yusys.yusp.uaa.pojo.TokenParamDto;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description:  ThirdLoginUserInfo的默认实现
 * @author lty
 * @date 2020/12/29　　
 */

public class DefaultMobileLoginUserInfoImpl implements MobileLoginUserInfo {

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    ObjectMapper objectMapper;

    @Override
    public TokenParamDto getLoginUserInfo(String mobile) {

        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        String code = httpServletRequest.getParameter("verificationCode");

        String redisStorage
        =redisTemplate.opsForValue().get("PASSWORD_RESET_MOBILE_CODE" + "-" + mobile);
        //redisStorage = redisStorage.split("_")[0];
        MobileInfo mobileInfo = new MobileInfo();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("mobile_code",redisStorage);
        try {
            mobileInfo = objectMapper.readValue(jsonObject.toJSONString(), MobileInfo.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        if(!code.equals(mobileInfo.getMobile_code())){
            return null;
        }
        return (TokenParamDto)mobileInfo;
    }
}
