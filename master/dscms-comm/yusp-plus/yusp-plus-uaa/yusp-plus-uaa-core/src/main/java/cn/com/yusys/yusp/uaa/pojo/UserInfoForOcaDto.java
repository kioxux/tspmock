package cn.com.yusys.yusp.uaa.pojo;

import java.io.Serializable;

/**
 * yusp-app-framework
 *
 * @author wujiangpeng
 * @description 用户返回封装
 * @email wujp4@yusys.com.cn
 * @since 2020-10-28 17:40
 */
public class UserInfoForOcaDto implements Serializable {

    private String userId;

    private String loginCode;

    private String orgId;

    /**
     * 渠道互斥标识
     */
    private Boolean loginSingleAgent;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Boolean getLoginSingleAgent() {
        return loginSingleAgent;
    }

    public void setLoginSingleAgent(Boolean loginSingleAgent) {
        this.loginSingleAgent = loginSingleAgent;
    }

}
