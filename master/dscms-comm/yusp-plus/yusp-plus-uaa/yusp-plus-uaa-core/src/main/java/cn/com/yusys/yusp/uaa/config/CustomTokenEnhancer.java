package cn.com.yusys.yusp.uaa.config;


import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.uaa.pojo.LoginUserInfo;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author lty
 * @version: 3.1.1-SNAPSHOT
 * @description:  增强token信息
 * @date 2020/12/28
 */
public class CustomTokenEnhancer implements TokenEnhancer {
    /**
     * @description： 设置token包含code，user_id，user_id，login_code的值。
     * @author： lty
     * @date： 2020/12/28
     */
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {

        final Map<String, Object> additionalInfo = new HashMap<>();

        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        String grantType = httpServletRequest.getParameter("grant_type");
        if("client_credentials".equals(grantType)){
            ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
            return accessToken;
        }

        LoginUserInfo loginResultDtoLoginUserInfo = (LoginUserInfo) authentication.getPrincipal();

        if (StringUtils.nonEmpty(loginResultDtoLoginUserInfo.getBusinessCode())) {
            additionalInfo.put("code", loginResultDtoLoginUserInfo.getBusinessCode());
        }

        if (StringUtils.nonEmpty(loginResultDtoLoginUserInfo.getUserId())) {
            additionalInfo.put("user_id", loginResultDtoLoginUserInfo.getUserId());
        }

        if (StringUtils.nonEmpty(loginResultDtoLoginUserInfo.getOrgId())) {
            additionalInfo.put("org_id", loginResultDtoLoginUserInfo.getOrgId());
        }

        if (StringUtils.nonEmpty(loginResultDtoLoginUserInfo.getOrgId())) {
            additionalInfo.put("login_code", loginResultDtoLoginUserInfo.getLoginCode());
        }

        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        return accessToken;
    }
}
