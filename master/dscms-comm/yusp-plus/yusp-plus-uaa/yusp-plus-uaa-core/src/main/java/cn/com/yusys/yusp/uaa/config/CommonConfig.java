package cn.com.yusys.yusp.uaa.config;

import cn.com.yusys.yusp.uaa.config.token.CustomRedisTokenStore;
import cn.com.yusys.yusp.uaa.config.token.CustomUaaStrategyProperties;
import cn.com.yusys.yusp.uaa.exception.CustomWebResponseExceptionTranslator;
import cn.com.yusys.yusp.uaa.grant.app.AppClientGranter;
import cn.com.yusys.yusp.uaa.grant.mobile.MobileGranter;
import cn.com.yusys.yusp.uaa.grant.oca.OcaGranter;
import cn.com.yusys.yusp.uaa.grant.thirdParty.ThirdGranter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenGranter;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeTokenGranter;
import org.springframework.security.oauth2.provider.code.InMemoryAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.implicit.ImplicitTokenGranter;
import org.springframework.security.oauth2.provider.password.ResourceOwnerPasswordTokenGranter;
import org.springframework.security.oauth2.provider.refresh.RefreshTokenGranter;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;

import javax.sql.DataSource;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author lty
 * @version 3.1.1-SNAPSHOT
 * @description uaa所需bean的配置
 * @date 2020/12/28
 */

@Configuration
public class CommonConfig {

    @Value("${uaa.keyStore.name:keystore.jks}")
    private String keyStoreName;

    @Value("${uaa.keyStore.password:password}")
    private String keyStorePassword;

    @Value("${uaa.keyStore.keyPair:selfsigned}")
    private String keyPair;
    //使用数据库管理客户端时注入
    private final DataSource dataSource;

    //redis连接工厂
    private final RedisConnectionFactory redisConnectionFactory;


    //配置授权管理
    private final AuthenticationManager authenticationManager;

    private ObjectMapper objectMapper;

    /**
     * @param dataSource
     * @param redisConnectionFactory
     * @param authenticationManager
     */
    @Autowired
    public CommonConfig(DataSource dataSource, RedisConnectionFactory redisConnectionFactory, AuthenticationManager authenticationManager, ObjectMapper objectMapper) {
        this.dataSource = dataSource;
        this.redisConnectionFactory = redisConnectionFactory;
        this.authenticationManager = authenticationManager;
        this.objectMapper = objectMapper;
    }

    /**
     * @description： 客户端信息获取方式（数据库）
     * @author： lty
     * @date： 2020/12/28
     */
    @Bean
    public ClientDetailsService clientDetails() {
        return new JdbcClientDetailsService(dataSource);
    }

    /**
     * @description： uaa异常转换配置
     * @author： lty
     * @date： 2020/12/28
     */
    @Bean
    public WebResponseExceptionTranslator webResponseExceptionTranslator() {
        return new CustomWebResponseExceptionTranslator();
    }

    /**
     * @description： token生成服务
     * @author： lty
     * @date： 2020/12/28
     */
    @Bean
    public DefaultTokenServices defaultTokenServices() {
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(customTokenEnhancer(), jwtAccessTokenConverter()));
        DefaultTokenServices tokenServices = new DefaultTokenServices();
        tokenServices.setTokenStore(redisTokenStore());
        tokenServices.setSupportRefreshToken(true);
        tokenServices.setTokenEnhancer(tokenEnhancerChain);
        // 不允许重复使用刷新token，每次刷新Token后会返回新的访问Token和刷新Token，必须使用该新的刷新Token才能进行token刷新
        tokenServices.setReuseRefreshToken(false);
        tokenServices.setClientDetailsService(clientDetails());
        return tokenServices;
    }

    /**
     * @description： 读取uaa的配置
     * @author： lty
     * @date： 2021/3/15 　　：
     */
    @Bean
    public CustomUaaStrategyProperties customUaaStrategyProperties() {
        return new CustomUaaStrategyProperties();
    }

    /**
     * @description： 根据互斥策略是否开启选择对应的token存储方式
     * @author： lty
     * @date： 2020/12/28
     */

    @Bean
    public TokenStore redisTokenStore() {
        return new CustomRedisTokenStore(redisConnectionFactory);
    }

    /**
     * @description： 自定义tokenEnhancer配置
     * @author： lty
     * @date： 2020/12/28
     */
    @Bean
    public CustomTokenEnhancer customTokenEnhancer() {
        return new CustomTokenEnhancer();
    }

    /**
     * @description： 配置jwt的Converter
     * @author： lty
     * @date： 2020/12/28
     */
    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setKeyPair(keyPair());
        return converter;
    }

    /**
     * @description： 配置密钥对
     * @author： lty
     * @date： 2020/12/28
     */
    @Bean
    public KeyPair keyPair() {
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(new ClassPathResource(keyStoreName), keyStorePassword.toCharArray());
        return keyStoreKeyFactory.getKeyPair(keyPair);
    }

    /**
     * @description： 配置加密方式
     * @author： lty
     * @date： 2020/12/28
     */
    @ConditionalOnMissingBean(PasswordEncoder.class)
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new StandardPasswordEncoder();
    }

    /**
     * @description： 读取授权类型
     * @author： lty
     * @date： 2020/12/28
     */
    @Bean
    public TokenGranter tokenGranter() {
        return (grantType, tokenRequest) -> {
            CompositeTokenGranter delegate = new CompositeTokenGranter(getAllTokenGranters());
            return delegate.grant(grantType, tokenRequest);
        };
    }

    /**
     * @description： 获取所有的授权类型
     * @author： lty
     * @date： 2020/12/28
     */
    private List<TokenGranter> getAllTokenGranters() {
        AuthorizationServerTokenServices tokenServices = defaultTokenServices();
        AuthorizationCodeServices authorizationCodeServices = authorizationCodeServices();
        OAuth2RequestFactory requestFactory = requestFactory();
        //获取默认的授权模式
        List<TokenGranter> tokenGranters = getDefaultTokenGranters(tokenServices, authorizationCodeServices, requestFactory);
        if (authenticationManager != null) {
            //添加oca的授权模式
            tokenGranters.add(new OcaGranter(authenticationManager, tokenServices, clientDetails(), requestFactory));
            //添加third_party模式
            tokenGranters.add(new ThirdGranter(authenticationManager, tokenServices, clientDetails(), requestFactory));
            //添加手机登录模式
            tokenGranters.add(new MobileGranter(authenticationManager, tokenServices, clientDetails(), requestFactory));
            //添加移动端登录模式
            tokenGranters.add(new AppClientGranter(authenticationManager, tokenServices, clientDetails(), requestFactory));
        }
        return tokenGranters;
    }

    /**
     * @description： 默认的授权类型
     * @author： lty
     * @date： 2020/12/28
     */
    private List<TokenGranter> getDefaultTokenGranters(AuthorizationServerTokenServices tokenServices
            , AuthorizationCodeServices authorizationCodeServices, OAuth2RequestFactory requestFactory) {
        List<TokenGranter> tokenGranters = new ArrayList<>();
        // 添加授权码模式
        tokenGranters.add(new AuthorizationCodeTokenGranter(tokenServices, authorizationCodeServices, clientDetails(), requestFactory));
        // 添加刷新令牌的模式
        tokenGranters.add(new RefreshTokenGranter(tokenServices, clientDetails(), requestFactory));
        // 添加隐士授权模式
        tokenGranters.add(new ImplicitTokenGranter(tokenServices, clientDetails(), requestFactory));
        // 添加客户端模式
        tokenGranters.add(new ClientCredentialsTokenGranter(tokenServices, clientDetails(), requestFactory));
        if (authenticationManager != null) {
            // 添加密码模式
            tokenGranters.add(new ResourceOwnerPasswordTokenGranter(authenticationManager, tokenServices, clientDetails(), requestFactory));
        }
        return tokenGranters;
    }

    /**
     * @description： 授权码模式+在内存进行
     * @author： lty
     * @date： 2020/12/28
     */
    private AuthorizationCodeServices authorizationCodeServices() {

        return new InMemoryAuthorizationCodeServices();
    }

    /**
     * @description： 授权类型请求配置
     * @author： lty
     * @date： 2020/12/28
     */
    private OAuth2RequestFactory requestFactory() {
        return new DefaultOAuth2RequestFactory(clientDetails());
    }
}
