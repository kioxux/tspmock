package cn.com.yusys.yusp.uaa.feign;

import cn.com.yusys.yusp.uaa.pojo.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 类简介：
 * 调用oca的feign包接口
 *
 * @author lty on 2020.10.26
 */
@FeignClient(value = "${yusp.oca.service-name:yusp-app-oca}")
public interface OcaFeignService {

    @PostMapping(value = "/api/login/queryuserandchecksecret")
    LoginOcaResultDto<UserInfoForOcaDto> queryUserAndCheckSecret(@RequestBody LoginOcaRequestDto loginRequestDto);

    /**
     * 移动端登录校验
     *
     * @param loginAppClientRequestDto 移动端登录校验参数
     * @return 移动端用户信息
     */
    @PostMapping(value = "/api/login/checkappclientuserexist")
    LoginOcaResultDto<UserInfoForOcaDto> checkAppClientUserExist(@RequestBody LoginAppClientRequestDto loginAppClientRequestDto);

    @PostMapping(value = "/api/login/checkUserMobile")
    LoginOcaResultDto<UserInfoForOcaDto> checkUserMobile(@RequestBody LoginMobileRequestDto loginMobileRequestDto);

}
