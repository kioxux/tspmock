package cn.com.yusys.yusp.uaa.grant.mobile;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.uaa.enumerate.UaaErrorCodeEnum;
import cn.com.yusys.yusp.uaa.exception.UaaErrorException;
import cn.com.yusys.yusp.uaa.feign.OcaFeignService;
import cn.com.yusys.yusp.uaa.pojo.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @类名 MobileLoginUserInfoImpl
 * @描述 mobile方式用户登录信息实现
 * @作者 黄勃
 * @时间 2021/9/23 10:02
 **/
@Service
@Slf4j
public class MobileLoginUserInfoImpl implements MobileLoginUserInfo{
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private OcaFeignService ocaFeignService;

    @Override
    public TokenParamDto getLoginUserInfo(String mobile) {
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) Objects
                .requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        String loginCode = httpServletRequest.getParameter("loginCode");

        // 调用oca的服务校验用户手机号
        LoginMobileRequestDto requestDto = new LoginMobileRequestDto();
        requestDto.setLoginCode(loginCode);
        requestDto.setMobile(mobile);
        LoginOcaResultDto<UserInfoForOcaDto> loginOcaResultDto = ocaFeignService.checkUserMobile(requestDto);
        if(loginOcaResultDto==null){
            log.error(UaaErrorCodeEnum.SYSTEM_ERROR.getMessage()+ "The correct information is not returned from Oca");
            throw new UaaErrorException(UaaErrorCodeEnum.SYSTEM_ERROR.getCode(),
                    UaaErrorCodeEnum.SYSTEM_ERROR.getMessage()+",The correct information is not returned from Oca ");
        }

        // 校验短信验证码
        String code = httpServletRequest.getParameter("verificationCode");
        String redisStorage
                =stringRedisTemplate.opsForValue().get("PASSWORD_RESET_MOBILE_CODE" + "-" + mobile);
        if(!StringUtils.equals(code, redisStorage)) {
            log.error(UaaErrorCodeEnum.IAMGE_CODE_ERROR.getMessage()+ ",手机短信验证码校验不通过！");
            throw new UaaErrorException(UaaErrorCodeEnum.IAMGE_CODE_ERROR.getCode(),
                    UaaErrorCodeEnum.IAMGE_CODE_ERROR.getMessage()+",手机短信验证码校验不通过！");
        }

        TokenParamDto tokenParamDto = new TokenParamDto();
        tokenParamDto.setBusinessCode("00000000");
        tokenParamDto.setLoginCode(loginCode);
        tokenParamDto.setOrgId(loginOcaResultDto.getData().getOrgId());
        tokenParamDto.setUserId(loginOcaResultDto.getData().getUserId());
//        MobileInfo mobileInfo = new MobileInfo();
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("mobile_code",redisStorage);
//        try {
//            mobileInfo = objectMapper.readValue(jsonObject.toJSONString(), MobileInfo.class);
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
        return tokenParamDto;
    }
}
