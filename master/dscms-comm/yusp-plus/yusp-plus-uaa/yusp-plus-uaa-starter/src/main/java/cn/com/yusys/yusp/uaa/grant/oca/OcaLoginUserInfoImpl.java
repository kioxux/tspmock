package cn.com.yusys.yusp.uaa.grant.oca;

import cn.com.yusys.yusp.uaa.enumerate.SuccessfulAuthenticationCodeEnum;
import cn.com.yusys.yusp.uaa.enumerate.UaaErrorCodeEnum;
import cn.com.yusys.yusp.uaa.exception.UaaErrorException;
import cn.com.yusys.yusp.uaa.exception.UaaOauthException;
import cn.com.yusys.yusp.uaa.feign.OcaFeignService;
import cn.com.yusys.yusp.uaa.pojo.LoginOcaRequestDto;
import cn.com.yusys.yusp.uaa.pojo.LoginOcaResultDto;
import cn.com.yusys.yusp.uaa.pojo.TokenParamDto;
import cn.com.yusys.yusp.uaa.pojo.UserInfoForOcaDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description:  OcaLoginUserInfo的默认实现
 * @author lty
 * @date 2020/12/29　　
 */
@Service
public class OcaLoginUserInfoImpl implements OcaLoginUserInfo {

    @Autowired
    OcaFeignService ocaFeignService;

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    /**
     * @description： 远程调用oca接口，并填充TokenParamDto
     * @author： lty
     * @date： 2020/12/29
     */
    @Override
    public TokenParamDto getLoginUserInfo(String username, String password) {

        LoginOcaRequestDto loginOcaRequestDto = new LoginOcaRequestDto();
        loginOcaRequestDto.setLoginCode(username);
        loginOcaRequestDto.setPassword(password);

        LoginOcaResultDto<UserInfoForOcaDto> loginOcaResultDto = ocaFeignService.queryUserAndCheckSecret(loginOcaRequestDto);

        if(loginOcaResultDto==null){
            log.error(UaaErrorCodeEnum.SYSTEM_ERROR.getMessage()+ "The correct information is not returned from Oca");
            throw new UaaErrorException(UaaErrorCodeEnum.SYSTEM_ERROR.getCode(),UaaErrorCodeEnum.SYSTEM_ERROR.getMessage()+",The correct information is not returned from Oca ");
        }
        if(!SuccessfulAuthenticationCodeEnum.SUCCESSFULLY_CHECKED.getCode().equals(loginOcaResultDto.getCode())
                &&!SuccessfulAuthenticationCodeEnum.FIRSTLOGIN.getCode().equals(loginOcaResultDto.getCode())){
            log.error(loginOcaResultDto.getCode(),loginOcaResultDto.getMessage());
            throw new UaaOauthException(loginOcaResultDto.getCode(),loginOcaResultDto.getMessage());
        }

        TokenParamDto tokenParamDto = new TokenParamDto();
        tokenParamDto.setBusinessCode(loginOcaResultDto.getCode());
        tokenParamDto.setLoginCode(loginOcaResultDto.getData().getLoginCode());
        tokenParamDto.setOrgId(loginOcaResultDto.getData().getOrgId());
        tokenParamDto.setUserId(loginOcaResultDto.getData().getUserId());
        tokenParamDto.setLoginSingleAgent(loginOcaResultDto.getData().getLoginSingleAgent());

        return tokenParamDto;
    }

    public OcaFeignService getOcaFeignService() {
        return ocaFeignService;
    }

    public void setOcaFeignService(OcaFeignService ocaFeignService) {
        this.ocaFeignService = ocaFeignService;
    }

    public OcaLoginUserInfoImpl() {
    }
}
