package cn.com.yusys.yusp.uaa.grant.app;

import cn.com.yusys.yusp.uaa.enumerate.UaaErrorCodeEnum;
import cn.com.yusys.yusp.uaa.exception.UaaErrorException;
import cn.com.yusys.yusp.uaa.exception.UaaOauthException;
import cn.com.yusys.yusp.uaa.feign.OcaFeignService;
import cn.com.yusys.yusp.uaa.pojo.LoginAppClientRequestDto;
import cn.com.yusys.yusp.uaa.pojo.LoginOcaResultDto;
import cn.com.yusys.yusp.uaa.pojo.TokenParamDto;
import cn.com.yusys.yusp.uaa.pojo.UserInfoForOcaDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 远程调用oca接口进行移动端登录校验
 *
 * @author yangzai
 * @since 2021/5/31
 **/
@Service
public class AppClientLoginUserInfoImpl implements AppClientLoginUserInfo {

    private static final Logger log = LoggerFactory.getLogger(AppClientLoginUserInfoImpl.class);

    @Autowired
    private OcaFeignService ocaFeignService;

    private AppClientLoginUserInfoImpl(){

    }

    /**
     * @description 远程调用oca接口，并填充TokenParamDto
     * @author lty
     * @date 2020/12/29
     */
    @Override
    public TokenParamDto getLoginUserInfo(String servtp, String loginCode, String orgCode) {
        LoginAppClientRequestDto loginAppClientRequestDto = new LoginAppClientRequestDto();
        loginAppClientRequestDto.setServtp(servtp);
        loginAppClientRequestDto.setLoginCode(loginCode);
        loginAppClientRequestDto.setOrgCode(orgCode);

        LoginOcaResultDto<UserInfoForOcaDto> loginOcaResultDto = ocaFeignService.checkAppClientUserExist(loginAppClientRequestDto);

        if (loginOcaResultDto == null) {
            log.error(UaaErrorCodeEnum.SYSTEM_ERROR.getMessage() + " The correct information is not returned from appClient!");
            throw new UaaErrorException(UaaErrorCodeEnum.SYSTEM_ERROR.getCode(), UaaErrorCodeEnum.SYSTEM_ERROR.getMessage() + ",The correct information is not returned from appClient!");
        }
        if (!"0".equals(loginOcaResultDto.getCode())) {
            log.error(loginOcaResultDto.getCode(), loginOcaResultDto.getMessage());
            throw new UaaOauthException(loginOcaResultDto.getCode(), loginOcaResultDto.getMessage());
        }
        TokenParamDto tokenParamDto = new TokenParamDto();
        tokenParamDto.setBusinessCode(loginOcaResultDto.getCode());
        tokenParamDto.setLoginCode(loginOcaResultDto.getData().getLoginCode());
        tokenParamDto.setOrgId(loginOcaResultDto.getData().getOrgId());
        tokenParamDto.setUserId(loginOcaResultDto.getData().getUserId());
        tokenParamDto.setLoginSingleAgent(loginOcaResultDto.getData().getLoginSingleAgent());
        return tokenParamDto;
    }

}