package cn.com.yusys.yusp.uaa.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.uaa.enumerate.ResulteCodeMessageEnum;
import cn.com.yusys.yusp.uaa.pojo.MobileInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author lty
 * @version: 3.1.1-SNAPSHOT
 * @description: 用于短信验证码登录
 * @date 2021/3/29
 */
@Slf4j
@RestController
@RequestMapping(value = "/api")
public class MobileLoginController {

    //redis 的key前缀
    private final String MOBILE_INFO = "MOBILE_NUMBER_";

    //过期时间
    private final long EXPIRATION_TIME = 60000;
    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    ObjectMapper objectMapper;

    @PostMapping("/mobile/code")
    public ResultDto getMobileInfo( String mobile) {
        //实现逻辑]
        //设计redis的key为：MOBILE_NUMBER_具体的手机号。
        //1、获取到手机号，则到redis中查看该手机号是否有对应的信息，有则直接返回。
        String redisCode = stringRedisTemplate.opsForValue().get(MOBILE_INFO + mobile);
        if (!StringUtils.isEmpty(redisCode)) {
            long currentTime = Long.parseLong(redisCode.split("_")[1]);
            //60s内不能再发
            if (System.currentTimeMillis() - currentTime < EXPIRATION_TIME) {

                //1.1、如果存在则返回消息说明认证短信已发送，请不要重复操作   验证码为：88000001
                return new ResultDto().error(ResulteCodeMessageEnum.SMS_SENT.getCode(), ResulteCodeMessageEnum.SMS_SENT.getMessage());
            }

        }

        //2、生成验证码
        int code = (int) ((Math.random() * 9 + 1) * 100000);
        String codeNum = String.valueOf(code);

        //3、获取用户信息
        MobileInfo mobileInfo = new MobileInfo();
        mobileInfo.setMobile_code(codeNum);
        mobileInfo.setUserId("40");
        mobileInfo.setOrgId("40");
        mobileInfo.setLoginCode("40");
        String redisStorage = null;
        try {
            redisStorage = objectMapper.writeValueAsString(mobileInfo)+"_"+ System.currentTimeMillis();
        } catch (JsonProcessingException e) {
            log.error("json 序列化错误");
        }

        //存入redis，防止同一个手机号在60秒内再次发送验证码
        stringRedisTemplate.opsForValue().set(MOBILE_INFO + mobile,
                redisStorage, 10, TimeUnit.MINUTES);

        //调用短信平台
        log.info("调用短信平台");
        return ResultDto.success();
    }
}
