package cn.com.yusys.yusp.single.exception;

import lombok.Data;

@Data
public class AuthException extends Exception{

    private String code;

    public AuthException(String code, String msg) {
        super(msg);
        this.code = code;
    }

    public AuthException(String msg, Throwable t) {
        super(msg, t);
    }

    public AuthException(String msg) {
        super(msg);
    }

}
