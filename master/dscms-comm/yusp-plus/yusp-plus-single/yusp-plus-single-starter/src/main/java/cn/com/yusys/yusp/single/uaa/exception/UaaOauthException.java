package cn.com.yusys.yusp.single.uaa.exception;

import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
/**
 * @version: 3.1.1-SNAPSHOT
 * @description: 定制的uaa异常，支持业务码与业务消息的抛出
 * @author lty
 * @date 2020/12/29　　
 */
public class UaaOauthException extends OAuth2Exception {

    private String code;

    public UaaOauthException(String code, String msg) {
        super(msg);
        this.code = code;
    }

    public UaaOauthException(String msg, Throwable t) {
        super(msg, t);
    }

    public UaaOauthException(String msg) {
        super(msg);
    }

    @Override
    public int getHttpErrorCode() {
        return 401;
    }

    @Override
    public String getOAuth2ErrorCode() {
        return this.code;
    }
}
