package cn.com.yusys.yusp.single.uaa.config;

import cn.com.yusys.yusp.single.uaa.provider.AllAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description: 安全配置，配置白名单等
 * @author lty
 * @date 2020/12/28　　
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    AllAuthenticationProvider authenticationProvider;
    /**
     * @description: 配置AuthenticationManager
     * @author： lty
     * @date： 2020/12/28 　　
     */
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * @description： 配置静态资源白名单
     * @author： lty
     * @date： 2020/12/28 　　
     */
    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers(HttpMethod.OPTIONS, "/**")
                .antMatchers("/assets/**", "/css/**", "/images/**")

                .antMatchers("/swagger-resources/**")
                .antMatchers("/swagger-resources")
                .antMatchers("/favicon.ico")
                .antMatchers("/content/**")
                // 由于在管控平台中查看每个服务的SwaggerAPI接口文档时，会先通过网关到UAA获取一个OAuth Token，然后使用这个Token再访问各个服务的/v2/api-docs接口,现在UAA只管Toke生成，不做Token校验，因此，访问UAA的/v2/api-docs接口携带的OAuth Token将无法校验。
                .antMatchers("/v2/**")
                .antMatchers("/doc.html")
                .antMatchers("/h2-console/**")
                .antMatchers("/webjars/**");
    }

    /**
     * @description： 配置认证链路白名单
     * @author： lty
     * @date： 2020/12/28 　　
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authenticationProvider(authenticationProvider)
                .authorizeRequests()
                .antMatchers("/api/**")
                .permitAll()
                .anyRequest().authenticated()
                .and()
                .httpBasic().and()
                .csrf().disable()
                .cors()
                .and()
                .headers().frameOptions().disable();
    }
}


