package cn.com.yusys.yusp.single.uaa.service;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.oca.domain.constants.ResponseAndMessageEnum;
import cn.com.yusys.yusp.oca.domain.vo.UserEntityVo;
import cn.com.yusys.yusp.oca.service.LoginCheckSecretService;
import cn.com.yusys.yusp.single.uaa.dto.LoginUserInfo;
import cn.com.yusys.yusp.single.uaa.enumerate.UaaErrorCodeEnum;
import cn.com.yusys.yusp.single.uaa.exception.UaaErrorException;
import cn.com.yusys.yusp.single.uaa.exception.UaaOauthException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

    //设置redis的缓存key值
    @Value("${uaa.image.code:IMAGE_CODE_REDIS_KEY}")
    public String IMAGE_CODE_REDIS_KEY;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ClientDetailsService clientDetailsService;

    @Autowired
    LoginCheckSecretService loginCheckSecretService;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //1)验证图片验证码是否正确
        verifyCodeImage();

        //2)获取LoginUserInfo信息
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        String password = httpServletRequest.getParameter("password");
        List<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<>();
        ResultDto resultDto = loginCheckSecretService.queryUserAndCheckSecret(username,password);

        //3)判断请求是否有效
        if(!ResponseAndMessageEnum.SUCCESS.getCode().equals(resultDto.getCode())&&
        !ResponseAndMessageEnum.FIRSTLOGIN.getCode().equals(resultDto.getCode())){
            throw new UaaOauthException(resultDto.getCode(),resultDto.getMessage());
        }

        //4)封装token参数
        UserEntityVo userEntityVo =(UserEntityVo)(resultDto.getData());
        LoginUserInfo loginUserInfo = new LoginUserInfo(username,password,simpleGrantedAuthorities);
        loginUserInfo.setUserId(userEntityVo.getUserId());
        loginUserInfo.setLoginCode(userEntityVo.getLoginCode());
        loginUserInfo.setOrgId(userEntityVo.getOrgId());
        loginUserInfo.setBusinessCode(resultDto.getCode());
        return loginUserInfo;
    }


    /**
     * @description： 验证验证码
     * @author： lty
     * @date： 2020/12/29
     * @throws： UaaException
     */
    private void verifyCodeImage() {

        //获取client_id
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User clientDetails = (User) authentication.getPrincipal();

        //如果没有验证码功能，则数据库中该client配置需要增加额外的信息，key：image_status，value: false
        if (clientDetailsService.loadClientByClientId(clientDetails.getUsername()).getAdditionalInformation().containsKey("image_status")
                && clientDetailsService.loadClientByClientId(clientDetails.getUsername()).getAdditionalInformation().get("image_status").equals("false")) {
            return;
        }
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();

        String uuId = httpServletRequest.getParameter("imageUUID");
        if (StringUtils.isEmpty(uuId)) {
            log.error(UaaErrorCodeEnum.SYSTEM_ERROR.getMessage() + " imageUUID parameter is missing");
            throw new UaaErrorException(UaaErrorCodeEnum.SYSTEM_ERROR.getCode(), UaaErrorCodeEnum.SYSTEM_ERROR.getMessage() + " imageUUID parameter is missing");
        }

        String imageCode = httpServletRequest.getParameter("imageCode");
        if (StringUtils.isEmpty(imageCode)) {
            log.error(UaaErrorCodeEnum.SYSTEM_ERROR.getMessage() + " imageCode parameter is missing");
            throw new UaaOauthException(UaaErrorCodeEnum.IAMGE_CODE_ERROR.getCode(), UaaErrorCodeEnum.IAMGE_CODE_ERROR.getMessage());
        }

        String redisImageCode = stringRedisTemplate.opsForValue().get("IMAGE_CODE_REDIS_KEY" + "-" + uuId);
        if (redisImageCode == null) {
            throw new UaaOauthException(UaaErrorCodeEnum.IAMGE_CODE_ERROR.getCode(), UaaErrorCodeEnum.IAMGE_CODE_ERROR.getMessage());
        }
        if (!redisImageCode.equals(imageCode)) {
            throw new UaaOauthException(UaaErrorCodeEnum.IAMGE_CODE_ERROR.getCode(), UaaErrorCodeEnum.IAMGE_CODE_ERROR.getMessage());
        }
    }
}
