package cn.com.yusys.yusp.single.filter;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.context.UserContext;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.single.exception.AuthException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * @author lty
 * @version: 3.1.1-SNAPSHOT
 * @description:
 * @date 2021/1/20
 */
@Slf4j
@WebFilter(urlPatterns = "/api/*", filterName = "apiFilter")
public class SessionFilter implements Filter {

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    TokenStore tokenStore;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String method = request.getMethod();
        if (HttpMethod.OPTIONS.toString().equals(method)) {
            response.setStatus(HttpStatus.NO_CONTENT.value());
        } else {
            try {
                this.checkAuth(request);
            } catch (AuthException e) {
                ServletOutputStream out = response.getOutputStream();
                ResultDto resultDto = ResultDto.error(e.getCode(),e.getMessage());

                out.write(objectMapper.writeValueAsBytes(resultDto));
                ((HttpServletResponse) servletResponse).setStatus(401);
                out.flush();
                out.close();
                return;
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }

    private void checkAuth(HttpServletRequest httpServletRequest) throws AuthException {

        if (httpServletRequest.getRequestURI().contains("/api/codeimage") ||
                httpServletRequest.getRequestURI().contains("/api/logout")) {
            return;
        }

        String accessToken = httpServletRequest.getHeader("Authorization");
        if (StringUtils.nonEmpty(accessToken) && accessToken.length() > 8) {
            accessToken = accessToken.substring(7);
        }
        OAuth2AccessToken auth2AccessToken = tokenStore.readAccessToken(accessToken);
        if(auth2AccessToken!=null){
            setLoginInfoToUserContext(auth2AccessToken);
        }else {
            log.warn("校验token失败");
           throw new AuthException("90000001","token认证未通过");
        }

    }

    private static void setLoginInfoToUserContext(OAuth2AccessToken accessToken) {
        UserContext userContext = parseByJwt(accessToken.getValue());
        if (userContext != null) {
            UserContext.addUserContext(userContext);
        }

    }

    private static UserContext parseByJwt(String jwtAccessToken) {
        if (StringUtils.nonEmpty(jwtAccessToken)) {
            Jwt jwt = JwtHelper.decode(jwtAccessToken);
            Map<String, String> claims = (Map) ObjectMapperUtils.toObject(jwt.getClaims(), HashMap.class);
            return UserContext.of((String) claims.get("client_id"), (String) claims.get("user_id"), (String) claims.get("login_code"), (String) claims.get("org_id"));
        } else {
            return null;
        }
    }
}
