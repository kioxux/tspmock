package cn.com.yusys.yusp.single.uaa.config;

import cn.com.yusys.yusp.single.uaa.exception.CustomWebResponseExceptionTranslator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;

import javax.sql.DataSource;
import java.security.KeyPair;
import java.util.Arrays;

/**
 * @author lty
 * @version: 3.1.1-SNAPSHOT
 * @description: uaa所需bean的配置
 * @date 2020/12/28
 */
@Configuration
public class CommonConfig {

    @Value("${uaa.keyStore.name:keystore.jks}")
    private String keyStoreName;

    @Value("${uaa.keyStore.password:password}")
    private String keyStorePassword;

    @Value("${uaa.keyStore.keyPair:selfsigned}")
    private String keyPair;
    //使用数据库管理客户端时注入
    private final DataSource dataSource;

    //redis连接工厂
    private final RedisConnectionFactory redisConnectionFactory;

    //配置授权管理
    private final AuthenticationManager authenticationManager;
    @Autowired
    public CommonConfig(DataSource dataSource, RedisConnectionFactory redisConnectionFactory, AuthenticationManager authenticationManager) {
        this.dataSource = dataSource;
        this.redisConnectionFactory = redisConnectionFactory;
        this.authenticationManager = authenticationManager;
    }

    /**
     * @description： 客户端信息获取方式（数据库）
     * @author： lty
     * @date： 2020/12/28
     */
    @Bean
    public ClientDetailsService clientDetails() {
        return new JdbcClientDetailsService(dataSource);
    }

    /**
     * @description： uaa异常转换配置
     * @author： lty
     * @date： 2020/12/28
     */
    @Bean
    public WebResponseExceptionTranslator webResponseExceptionTranslator() {
        return new CustomWebResponseExceptionTranslator();
    }

    /**
     * @description： token生成服务
     * @author： lty
     * @date： 2020/12/28
     */
    @Bean
    public DefaultTokenServices defaultTokenServices() {
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(customTokenEnhancer(), jwtAccessTokenConverter()));
        DefaultTokenServices tokenServices = new DefaultTokenServices();
        tokenServices.setTokenStore(redisTokenStore());
        tokenServices.setSupportRefreshToken(true);
        tokenServices.setTokenEnhancer(tokenEnhancerChain);
        // 不允许重复使用刷新token，每次刷新Token后会返回新的访问Token和刷新Token，必须使用该新的刷新Token才能进行token刷新
        tokenServices.setReuseRefreshToken(false);
        tokenServices.setClientDetailsService(clientDetails());
        return tokenServices;
    }

    /**
     * @description： 使用redis作为token缓存
     * @author： lty
     * @date： 2020/12/28
     */
    @Bean
    public TokenStore redisTokenStore() {
        return new RedisTokenStore(redisConnectionFactory);
    }

    /**
     * @description： 自定义tokenEnhancer配置
     * @author： lty
     * @date： 2020/12/28
     */
    @Bean
    public CustomTokenEnhancer customTokenEnhancer() {
        return new CustomTokenEnhancer();
    }

    /**
     * @description： 配置jwt的Converter
     * @author： lty
     * @date： 2020/12/28
     */
    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setKeyPair(keyPair());
        return converter;
    }

    /**
     * @description： 配置密钥对
     * @author： lty
     * @date： 2020/12/28
     */
    @Bean
    public KeyPair keyPair() {
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(new ClassPathResource(keyStoreName), keyStorePassword.toCharArray());
        return keyStoreKeyFactory.getKeyPair(keyPair);
    }

    /**
     * @description： 配置加密方式
     * @author： lty
     * @date： 2020/12/28
     */
    @ConditionalOnMissingBean(PasswordEncoder.class)
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new StandardPasswordEncoder();
    }

//    public static void main(String[] args){
//        System.out.println(new BCryptPasswordEncoder().encode("123456"));
//    }
}
