package cn.com.yusys.yusp.single.uaa.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

/**
 * @author lty
 * @version: 3.1.1-SNAPSHOT
 * @description: 授权配置类
 * @date 2020/12/28
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    //配置UserDetailsService的类
    private final UserDetailsService userDetailService;

    //授权管理配置
    private final AuthenticationManager authenticationManager;

    //客户端配置
    private final ClientDetailsService clientDetails;

    //uaa异常转换配置
    private final WebResponseExceptionTranslator webResponseExceptionTranslator;

    //默认的token服务配置
    private final DefaultTokenServices defaultTokenServices;

    //token缓存配置
    private final TokenStore redisTokenStore;

    //配置jwt
    private final JwtAccessTokenConverter jwtAccessTokenConverter;


    /**
     * @param userDetailService
     * @param authenticationManager
     * @param redisConnectionFactory
     * @param clientDetails
     * @param webResponseExceptionTranslator
     * @param defaultTokenServices
     * @param redisTokenStore
     * @param jwtAccessTokenConverter
     */
    @Autowired
    public AuthorizationServerConfig(UserDetailsService userDetailService, AuthenticationManager authenticationManager, RedisConnectionFactory redisConnectionFactory, ClientDetailsService clientDetails, WebResponseExceptionTranslator webResponseExceptionTranslator, DefaultTokenServices defaultTokenServices, TokenStore redisTokenStore, JwtAccessTokenConverter jwtAccessTokenConverter) {
        this.userDetailService = userDetailService;
        this.authenticationManager = authenticationManager;
        //使用redis作为token的保存时注入
        this.clientDetails = clientDetails;
        this.webResponseExceptionTranslator = webResponseExceptionTranslator;
        this.defaultTokenServices = defaultTokenServices;
        this.redisTokenStore = redisTokenStore;
        this.jwtAccessTokenConverter = jwtAccessTokenConverter;
    }

    /**
     * @description：客户端配置
     * @author： lty
     * @date：2020/12/28
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clientDetailsServiceConfigurer) throws Exception {
        clientDetailsServiceConfigurer.withClientDetails(clientDetails);
    }

    /**
     * @description：端点配置
     * @author： lty
     * @date：2020/12/28
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints.tokenStore(redisTokenStore)
                .accessTokenConverter(jwtAccessTokenConverter)
                .userDetailsService(userDetailService)
                .authenticationManager(authenticationManager)
                .tokenServices(defaultTokenServices)
                .exceptionTranslator(webResponseExceptionTranslator)

        ;
    }

    /**
     * @description：授权配置
     * @author： lty
     * @date：2020/12/28
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) {
        security.checkTokenAccess("isAuthenticated()")
                .tokenKeyAccess("isAuthenticated()");
        security.allowFormAuthenticationForClients();
    }
}
