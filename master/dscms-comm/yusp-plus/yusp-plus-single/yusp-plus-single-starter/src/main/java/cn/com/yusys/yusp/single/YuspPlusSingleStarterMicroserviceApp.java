package cn.com.yusys.yusp.single;

import cn.com.yusys.yusp.common.annotation.EnableMetrics;
import cn.com.yusys.yusp.commons.autoconfigure.swagger.ServletSwaggerAutoConfiguration;
import cn.com.yusys.yusp.commons.autoconfigure.swagger.YuSwaggerAutoConfiguration;
import cn.com.yusys.yusp.commons.starter.config.WebFilterAutoConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * @author Administrator
 */
@EnableMetrics
@SpringBootApplication(exclude = {RabbitAutoConfiguration.class, WebFilterAutoConfiguration.class,
        ServletSwaggerAutoConfiguration.class,
        YuSwaggerAutoConfiguration.class}
        , scanBasePackages = {"cn.com.yusys.yusp.single", "cn.com.yusys.yusp.oca", "cn.com.yusys.yusp.notice", "cn.com.yusys.yusp.common.exception", "cn.com.yusys.yusp.common.config", "cn.com.yusys.yusp.common.i18n", "cn.com.yusys.yusp.flow"})
@MapperScan(basePackages = {"cn.com.yusys.yusp.oca.quartz.job.dao", "cn.com.yusys.yusp.oca.dao", "cn.com.yusys.yusp.notice.dao", "cn.com.yusys.yusp.flow.repository.mapper"})
@EnableAsync(proxyTargetClass = true)
@EnableSwagger2
@ServletComponentScan(basePackages = "cn.com.yusys.yusp.single.filter")
public class YuspPlusSingleStarterMicroserviceApp {
    public static void main(String[] args) {
        SpringApplication.run(YuspPlusSingleStarterMicroserviceApp.class, args);
    }
}
