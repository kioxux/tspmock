package cn.com.yusys.yusp.single.uaa.controller;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.concurrent.TimeUnit;

/**
 * @author lty
 * @version: 3.1.1-SNAPSHOT
 * @description: 验证码生成接口
 * @date 2020/12/29
 */
@RestController
@RequestMapping("/api")
public class CodeGenerateController {

    private final Logger log = LoggerFactory.getLogger(CodeGenerateController.class);

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    DefaultKaptcha defaultKaptcha;

    @GetMapping(value = "/codeimage/{uuId}")
    public void genCodeImage(@PathVariable("uuId") String uuId,
                             HttpServletResponse response) throws Exception {
        byte[] captcha;
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            String createText = defaultKaptcha.createText();
            //设置验证码超时时间
            int time = 60;
            //设置redis的缓存key值
            String key = "IMAGE_CODE_REDIS_KEY";
            stringRedisTemplate.opsForValue().set(key + "-" + uuId,
                    createText, time, TimeUnit.SECONDS);
            BufferedImage bi = defaultKaptcha.createImage(createText);
            ImageIO.write(bi, "png", out);
        } catch (Exception e) {
            log.error("image code generate failed",e);
            return;
        }
        captcha = out.toByteArray();
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setContentType("image/png");
        ServletOutputStream sout = response.getOutputStream();
        sout.write(captcha);
        sout.flush();
        sout.close();
    }

}
