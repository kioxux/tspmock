package cn.com.yusys.yusp.single.uaa.dto;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description: 认证未通过时返回错误码以及错误信息
 * @author lty
 * @date 2020/12/29　　
 */
public class UaaResultDto {
    //业务错误码 -认证通过不传或为空
    private String code;

    //认证失败时的提示信息
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
