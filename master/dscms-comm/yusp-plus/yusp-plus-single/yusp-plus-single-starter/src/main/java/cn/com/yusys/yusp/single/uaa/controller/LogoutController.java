package cn.com.yusys.yusp.single.uaa.controller;

import cn.com.yusys.yusp.commons.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

/**
 * @author lty
 * @version: 3.1.1-SNAPSHOT
 * @description: 登出接口
 * @date 2020/12/29
 */
@RestController
@RequestMapping(value = "/api")
public class LogoutController {

    @Autowired
    @Qualifier("redisTokenStore")
    TokenStore tokenStore;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * @description： 登出接口，清除token对应的redis
     * @author： lty
     * @date： 2020/12/29
     */
    @PostMapping(value = "/logout")
    public ResponseEntity postLogoutPage(HttpServletResponse response) {

        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        String accessToken = httpServletRequest.getHeader("Authorization").substring(7);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        OAuth2AccessToken oAuth2AccessToken = tokenStore.readAccessToken(accessToken);

        //清除redis
        if (auth != null && StringUtils.nonEmpty(accessToken)) {
            OAuth2RefreshToken oAuth2RefreshToken = oAuth2AccessToken.getRefreshToken();

            tokenStore.removeAccessToken(oAuth2AccessToken);
            if (oAuth2RefreshToken != null) {
                tokenStore.removeRefreshToken(oAuth2RefreshToken);
                tokenStore.removeAccessTokenUsingRefreshToken(oAuth2RefreshToken);
            }

            //清除上下文
            new SecurityContextLogoutHandler().logout(httpServletRequest, response, auth);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }
}
