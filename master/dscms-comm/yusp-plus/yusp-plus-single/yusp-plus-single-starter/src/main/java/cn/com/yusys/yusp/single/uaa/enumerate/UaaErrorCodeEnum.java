package cn.com.yusys.yusp.single.uaa.enumerate;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description: uaa抛出的错误码
 * @author lty
 * @date 2020/12/29　　
 */
public enum UaaErrorCodeEnum {

    SYSTEM_ERROR("system error","System error"),
    IAMGE_CODE_ERROR("10000004","Image captcha verification failed"),
    AUTHHENTICATION_FAILURE("10000001","Bad credentials");

    private String code;
    private String message;

    UaaErrorCodeEnum(String code ,String message) {
        this.code=code;
        this.message=message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
