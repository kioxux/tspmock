
<p align="center" >
   <H1>yusp-plus-single</H1>
</p>

## Introduction

yusp-plus-single是一款集成了认证授权、权限管理的单体springboot系统，使用方式同yusp-plus系统一致。

### swagger使用流程

1、swagger的登录地址：http://ip:port/doc.html; 例：http://localhost:30001/doc.html
***
2、swagger的登录页面；
***
![](doc/picture/swagger.png)
3、添加token请求头
***
![](doc/picture/header.png)
4、token的获取可以参考前端发起请求的请求头，将Authorization字段的值进行复制。