/*1,使用system用户(密码为安装oracle设置的密码)连接oracle*/

/*2,创建表空间及空间大小
    yusptablespace根据自己需要而创建的表空间名称
    datefile及为表空间的完整路径，其中E:\oracle\oradata\orcl为安装oracle时，oracle安装目录内文件，
    50M为空间大小
    20480M为空间最大扩展大小*/
    create tablespace yusptablespace
            datafile 'E:\oracle\oradata\orcl\yusptablespace.dbf'
            size 50m
            autoextend on
            next 50m maxsize 20480m
            extent management local;

/*3,创建用户并绑定表空间
    yusp:自行创建的用户名
    123456：自行创建的密码
    yusptablespace:步骤二中创建的表空间名称*/
    create user yusp identified by 123456 default tablespace yusptablespace;

/*4,用户授权
    dba:用户授权类型（授权全部权限给用户）
    yusp:步骤三中创建的用户名*/
    grant dba to yusp;