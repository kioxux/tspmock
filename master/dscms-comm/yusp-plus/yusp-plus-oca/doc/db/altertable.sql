-- 更改机构表结构
ALTER table admin_sm_org MODIFY `ORG_LEVEL` TINYINT(10) not null COMMENT '机构层级';
ALTER table admin_sm_org MODIFY `ORG_STS` char(1) not null COMMENT '状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效';
ALTER table admin_sm_org MODIFY `LAST_CHG_DT` datetime DEFAULT null COMMENT '最新变更时间';

-- 更改用户表结构
ALTER table admin_sm_user MODIFY `USER_STS` char(1) NOT NULL COMMENT '状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效';
ALTER table admin_sm_user MODIFY `LAST_LOGIN_TIME` datetime DEFAULT NULL COMMENT '最近登录时间';
ALTER table admin_sm_user MODIFY `LAST_EDIT_PASS_TIME` datetime DEFAULT NULL COMMENT '最近一次修改密码时间';
ALTER table admin_sm_user MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';

-- 更改部门表结构
ALTER table `admin_sm_dpt` change `DPT_CDE` `DPT_CODE` varchar(100) NOT NULL COMMENT '部门代码';
ALTER table `admin_sm_dpt` change `BELONG_ORG_ID` `ORG_ID` varchar(32) NOT NULL COMMENT '所属机构编号';
ALTER table admin_sm_dpt MODIFY `DPT_STS` char(1) NOT NULL COMMENT '状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效';
ALTER table admin_sm_dpt MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';

-- 更改岗位表结构
ALTER table `admin_sm_duty` change `DUTY_CDE` `DUTY_CODE` varchar(100) NOT NULL COMMENT '岗位代码';
ALTER table `admin_sm_duty` change `BELONG_ORG_ID` `ORG_ID` varchar(32) NOT NULL COMMENT '所属机构编号';
ALTER table admin_sm_duty MODIFY `DUTY_STS` char(1) NOT NULL COMMENT '状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效';
ALTER table admin_sm_duty MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';

-- 更改角色表结构
ALTER table admin_sm_role MODIFY `ROLE_STS` char(1) NOT NULL COMMENT '状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效';
ALTER table admin_sm_role MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';

-- 更改数据权限模板表结构（更新于：2021-1-6）
ALTER table admin_sm_data_auth_tmpl MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';

-- 更改数据权限与控制点交互表结构（更新于：2021-1-6）
ALTER table admin_sm_data_auth MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';

-- 更改控制点表结构（更新于：2021-1-6）
ALTER table admin_sm_res_contr MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';

-- 更改授权表结构（更新于：2021-1-6）
ALTER table admin_sm_auth_reco MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';

-- 更改系统业务功能表
ALTER TABLE `admin_sm_busi_func` MODIFY COLUMN `FUNC_ICON` varchar(200) DEFAULT NULL COMMENT '图标',
MODIFY COLUMN `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';

-- 更改系统功能模块表
ALTER TABLE `admin_sm_func_mod` MODIFY COLUMN `IS_OUTER` varchar(10) DEFAULT NULL COMMENT '是否外部系统',
 MODIFY COLUMN `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';

-- 更改系统菜单表
ALTER TABLE `admin_sm_menu`
ADD COLUMN `DELETED` INTEGER NOT NULL DEFAULT '0' COMMENT '逻辑删除，1：删除 0：未删除' AFTER `I18N_KEY`,
ADD COLUMN `MENU_CLASSIFY` char(1) NOT NULL DEFAULT '0' COMMENT '菜单分类，0 菜单， 1是菜单目录' AFTER `DELETED`,
MODIFY COLUMN `MENU_ORDER` INT (5) NOT NULL COMMENT '顺序',
MODIFY COLUMN `LAST_CHG_DT` datetime DEFAULT NULL COMMENT '最新变更时间' AFTER `LAST_CHG_USR`;

-- 更改系统参数表结构(2021-01-13)
ALTER table admin_sm_prop MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';

-- 更改用户表结构(2021-01-13)
ALTER table admin_sm_user MODIFY `DEADLINE` datetime DEFAULT NULL COMMENT '有效期到';
ALTER table admin_sm_user MODIFY `USER_BIRTHDAY` datetime DEFAULT NULL COMMENT '生日';
ALTER table admin_sm_user MODIFY `ENTRANTS_DATE` datetime DEFAULT NULL COMMENT '入职日期';
ALTER table admin_sm_user MODIFY `POSITION_TIME` datetime DEFAULT NULL COMMENT '任职时间';
ALTER table admin_sm_user MODIFY `FINANCIAL_JOB_TIME` datetime DEFAULT NULL COMMENT '从业时间';

-- 更改提示信息管理表结构(2021-01-13)
ALTER table admin_sm_message MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最后修改时间';

-- 更改资源对象授权记录表结构(2021-01-14)
ALTER table admin_sm_auth_reco MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';
ALTER table admin_sm_busi_func MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';
ALTER table admin_sm_crel_stra MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';
ALTER table admin_sm_data_auth MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';
ALTER table admin_sm_data_auth_tmpl MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';
ALTER table admin_sm_dpt MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';
ALTER table admin_sm_duty MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';
ALTER table admin_sm_func_mod MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';
ALTER table admin_sm_instu MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';
ALTER table admin_sm_lookup MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';
ALTER table admin_sm_lookup_item MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';
ALTER table admin_sm_lookup_type MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';
ALTER table admin_sm_menu MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';
ALTER table admin_sm_org MODIFY `LAST_CHG_DT` datetime DEFAULT NULL COMMENT '最新变更时间';
ALTER table admin_sm_password_log MODIFY `PWD_UP_TIME` datetime NOT NULL COMMENT '最新变更时间';
ALTER table admin_sm_password_log MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';
ALTER table admin_sm_res_contr MODIFY `LAST_CHG_DT` datetime DEFAULT NULL COMMENT '最新变更时间';
ALTER table admin_sm_role MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';
ALTER table admin_sm_user_duty_rel MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';
ALTER table admin_sm_user_mgr_org MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';
ALTER table admin_sm_user_role_rel MODIFY `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间';

-- 数据字典表（新增一张表）
CREATE TABLE `admin_sm_lookup_dict` (
  `LOOKUP_ITEM_ID` varchar(32) NOT NULL COMMENT '字典项编号，默认uuid',
  `LOOKUP_CODE` varchar(100) NOT NULL COMMENT '字典类别code码',
  `LOOKUP_NAME` varchar(200) NOT NULL COMMENT '字典类别名称',
  `LOOKUP_TYPE_ID` varchar(32) NOT NULL COMMENT '字典类别分类标识id',
  `LOOKUP_TYPE_NAME` varchar(100) DEFAULT NULL COMMENT '字典类别分类标识名称',
  `UP_LOOKUP_ITEM_ID` varchar(32) DEFAULT NULL COMMENT '上级字典内容编号',
  `LOOKUP_ITEM_CODE` varchar(100) NOT NULL COMMENT '字典代码',
  `LOOKUP_ITEM_NAME` varchar(100) NOT NULL COMMENT '字典名称',
  `LOOKUP_ITEM_COMMENT` varchar(150) DEFAULT NULL COMMENT '字典备注说明',
  `LOOKUP_ITEM_ORDER` int(5) DEFAULT NULL COMMENT '字典项排序',
  `INSTU_ID` varchar(32) DEFAULT NULL COMMENT '金融机构编号',
  `LAST_CHG_USR` varchar(32) NOT NULL COMMENT '最新变更用户',
  `LAST_CHG_DT` datetime NOT NULL COMMENT '最新变更时间',
  PRIMARY KEY (`LOOKUP_ITEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据字典内容表';

-- 数据字典表 初始化数据
INSERT INTO admin_sm_lookup_dict VALUES ('87br87dcb83f4ab4b22d5fee13iec056', 'COMMON_PARAM', '公共参数', '-1', '-1', 'COMMON_INIT_PARAM', 'COMMON_PARAM', '公共参数', NULL, '1', NULL, '40', '2021-01-15 17:20:03');
INSERT INTO admin_sm_lookup_dict VALUES ('COMMON_INIT_PARAM', 'COMMON_INIT_PARAM', '初始化字典参分类', '-1', '-1', '-1', 'COMMON_INIT_PARAM', '初始化字典参分类', '', '0', '', '40', '2021-01-15 17:20:03');
INSERT INTO admin_sm_lookup_dict VALUES ('87br87dcb83f4ab4b23d5fee14iec057', 'SYSTEM_MANAGE', '系统管理', '-1', '-1', 'COMMON_INIT_PARAM', 'SYSTEM_MANAGE', '系统管理', NULL, '2', NULL, '40', '2021-01-15 17:20:03');


-- 表结构改动
ALTER TABLE admin_sm_user_duty_rel drop COLUMN `REL_STS`;
ALTER TABLE admin_sm_user_mgr_org drop COLUMN `REL_STS`;
ALTER TABLE admin_sm_user_role_rel drop COLUMN `REL_STS`;