1、删除已删除用户的权限信息
DELETE 
FROM
	admin_sm_auth_reco 
WHERE
	auth_reco_id IN (
SELECT
	* 
FROM
	(
SELECT
	reco.auth_reco_id 
FROM
	admin_sm_auth_reco reco 
WHERE
	reco.authobj_type = 'U' 
	AND reco.authobj_id NOT IN ( SELECT us.user_id FROM admin_sm_user us ) 
	) temp 
)


2、删除已删除角色的权限信息
DELETE 
FROM
	admin_sm_auth_reco 
WHERE
	auth_reco_id IN (
SELECT
	* 
FROM
	(
SELECT
	reco.auth_reco_id 
FROM
	`admin_sm_auth_reco` reco 
WHERE
	reco.authobj_type = 'R' 
	AND reco.authobj_id NOT IN ( SELECT role.role_id FROM admin_sm_role role ) 
	) temp 
	)


3、删除已删除用户的用户角色关联数据
DELETE 
FROM
	admin_sm_user_role_rel 
WHERE
	user_role_rel_id IN (
SELECT
	* 
FROM
	( SELECT rel.user_role_rel_id FROM admin_sm_user_role_rel rel WHERE rel.user_id NOT IN ( SELECT us.user_id FROM admin_sm_user us ) ) temp 
	)


4、删除已删除用户的用户机构关联数据
DELETE 
FROM
	admin_sm_user_mgr_org 
WHERE
	user_mgr_org_id IN (
SELECT
	* 
FROM
	( SELECT rel.user_mgr_org_id FROM admin_sm_user_mgr_org rel WHERE rel.user_id NOT IN ( SELECT us.user_id FROM admin_sm_user us ) ) temp 
	)


5、删除已删除用户的用户部门关联数据
DELETE 
FROM
	admin_sm_user_duty_rel 
WHERE
	user_duty_rel_id IN (
SELECT
	* 
FROM
	( SELECT rel.user_duty_rel_id FROM admin_sm_user_duty_rel rel WHERE rel.user_id NOT IN ( SELECT us.user_id FROM admin_sm_user us ) ) temp 
	)


6、删除已删除的用户的密码修改记录表
DELETE 
FROM
	admin_sm_password_log
WHERE
	user_id IN (
SELECT
	* 
FROM
	( SELECT rel.user_id FROM admin_sm_password_log rel WHERE rel.user_id NOT IN ( SELECT us.user_id FROM admin_sm_user us ) ) temp 
	)

7、删除已经被删除的菜单
delete  FROM `admin_sm_menu` where deleted = '1'

8、删除上级菜单不存在的菜单
DELETE  FROM admin_sm_menu
WHERE
	up_menu_id IN (
	select ddd.up_menu_id from
		(SELECT DISTINCT ( t.up_menu_id )
			FROM admin_sm_menu t
		WHERE
			t.up_menu_id != '-1'
			AND t.up_menu_id NOT IN (
		SELECT temp.*
			FROM ( SELECT m.menu_id FROM admin_sm_menu m WHERE m.menu_id IN ( SELECT DISTINCT ( up_menu_id ) FROM admin_sm_menu WHERE up_menu_id != '-1' ) ) temp
		)) ddd
)
9、删除已删除菜单的权限信息
DELETE 
FROM
	admin_sm_auth_reco 
WHERE
	auth_reco_id IN (
SELECT
	* 
FROM
	( SELECT reco.auth_reco_id FROM admin_sm_auth_reco reco WHERE reco.menu_id NOT IN ( SELECT menu.menu_id FROM admin_sm_menu menu ) ) temp 
	)

10、查询admin_sm_auth_reco表中authobj_id和authres_id值相同重复的数据（需保证此脚本查不出数据，否则数据授权页会报错；如果有重复的，手动删除下）
select authobj_id, authres_id,MENU_ID
from admin_sm_auth_reco
-- where AUTHOBJ_ID ='R0020'
group by authobj_id, authres_id,menu_id
having count(1) > 1
