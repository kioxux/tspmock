##替换菜单名称，执行查询后，将查询结果导出为Insert语句


#1、查询新增菜单

SELECT
	* 
FROM
	admin_sm_menu 
WHERE
	menu_name in ( '个人客户管理');

#2、查询下级菜单

SELECT
	* 
FROM
	admin_sm_menu 
WHERE
	UP_MENU_ID in ( SELECT menu.menu_id FROM admin_sm_menu menu WHERE menu.menu_name in ( '个人客户管理') );


#3、查询菜单对应的业务功能

SELECT
	func.* 
FROM
	admin_sm_busi_func func 
WHERE
	func.func_id IN ( SELECT menu.FUNC_ID FROM admin_sm_menu menu WHERE menu.menu_name in ( '个人客户管理') );

#4、查询下级菜单的业务功能

SELECT
	func.* 
FROM
	admin_sm_busi_func func 
WHERE
	func.func_id IN ( SELECT func_id FROM admin_sm_menu WHERE UP_MENU_ID in ( SELECT menu.menu_id FROM admin_sm_menu menu WHERE menu.menu_name in ( '个人客户管理') ) );
	
#5、查询菜单权限

SELECT
	* 
FROM
	admin_sm_auth_reco
WHERE
	authres_id IN ( SELECT menu_id FROM admin_sm_menu WHERE menu_name in ( '个人客户管理') );
	
#6、查询当前菜单的下级菜单权限

SELECT
	* 
FROM
	admin_sm_auth_reco 
WHERE
	authres_id IN ( SELECT menu_id FROM admin_sm_menu WHERE UP_MENU_ID in ( SELECT menu.menu_id FROM admin_sm_menu menu WHERE menu.menu_name in ( '个人客户管理') ) );
	
#7、查询菜单下的控制点

SELECT
	* 
FROM
	admin_sm_res_contr 
WHERE
	func_id IN ( SELECT menu.FUNC_ID FROM admin_sm_menu menu WHERE menu.menu_name in ( '个人客户管理') );
	
#8、查询菜单下的下级菜单的控制点

SELECT
	* 
FROM
	admin_sm_res_contr 
WHERE
	func_id IN ( SELECT func_id FROM admin_sm_menu WHERE UP_MENU_ID in ( SELECT menu.menu_id FROM admin_sm_menu menu WHERE menu.menu_name in ( '个人客户管理') ) );

	
#9、查询菜单的控制点的权限

SELECT
	* 
FROM
	admin_sm_auth_reco 
WHERE
	authres_id IN ( SELECT contr_id FROM admin_sm_res_contr WHERE func_id IN ( SELECT menu.FUNC_ID FROM admin_sm_menu menu WHERE menu.menu_name in ( '个人客户管理') ) );


#10、查询菜单下级菜单的控制点权限

SELECT
	* 
FROM
	admin_sm_auth_reco 
WHERE
	authres_id IN (
SELECT
	contr_id 
FROM
	admin_sm_res_contr 
WHERE
	func_id IN ( SELECT func_id FROM admin_sm_menu WHERE UP_MENU_ID in ( SELECT menu.menu_id FROM admin_sm_menu menu WHERE menu.menu_name in ( '个人客户管理') ) )
	);

#11、查询菜单的控制点的数据权限模板
SELECT
	da.*
FROM
	admin_sm_data_auth da
WHERE
	da.contr_id IN ( SELECT contr_id FROM admin_sm_res_contr WHERE func_id IN ( SELECT menu.FUNC_ID FROM admin_sm_menu menu WHERE menu.menu_name IN ( '个人客户管理' ) ) );

#12、查询菜单的下级菜单的控制点的数据权限模板
SELECT
	da.*
FROM
	admin_sm_data_auth da
WHERE
	da.contr_id IN (
SELECT
	contr_id
FROM
	admin_sm_res_contr
WHERE
	func_id IN ( SELECT func_id FROM admin_sm_menu WHERE UP_MENU_ID IN ( SELECT menu.menu_id FROM admin_sm_menu menu WHERE menu.menu_name IN ( '个人客户管理' ) ) )
	);

#13、查询菜单控制点的数据模板的权限信息（数据权限时，menu_id记录的是控制点id）
SELECT
	*
FROM
	admin_sm_auth_reco
WHERE
	menu_id IN ( SELECT contr_id FROM admin_sm_res_contr WHERE func_id IN ( SELECT menu.FUNC_ID FROM admin_sm_menu menu WHERE menu.menu_name IN ( '个人客户管理' ) ) );

#14、查询菜单下级菜单控制点的数据模板的权限信息（数据权限时，menu_id记录的是控制点id）
SELECT
	*
FROM
	admin_sm_auth_reco
WHERE
	menu_id IN (
SELECT
	contr_id
FROM
	admin_sm_res_contr
WHERE
	func_id IN ( SELECT func_id FROM admin_sm_menu WHERE UP_MENU_ID IN ( SELECT menu.menu_id FROM admin_sm_menu menu WHERE menu.menu_name IN ( '个人客户管理' ) ) )
);