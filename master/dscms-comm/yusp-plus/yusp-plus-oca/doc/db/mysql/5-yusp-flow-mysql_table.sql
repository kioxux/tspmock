/*
Navicat MySQL Data Transfer

Source Server         : 192.168.251.166
Source Server Version : 50637
Source Host           : 192.168.251.166:3306
Source Database       : samplemicroservice

Target Server Type    : MYSQL
Target Server Version : 50637
File Encoding         : 65001

Date: 2020-03-11 13:52:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for n_wf_biz
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_biz`;
CREATE TABLE `n_wf_biz` (
  `BIZ_TYPE` varchar(32) NOT NULL,
  `FLOW_ID` int(11) NOT NULL,
  `FLOW_NAME` varchar(32) DEFAULT NULL,
  `PAGE_URL` varchar(60) DEFAULT NULL,
  `EXT` varchar(60) DEFAULT NULL,
  `ORG_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`BIZ_TYPE`,`FLOW_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for n_wf_biz_node
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_biz_node`;
CREATE TABLE `n_wf_biz_node` (
  `BIZ_TYPE` varchar(32) NOT NULL,
  `FLOW_ID` int(11) NOT NULL,
  `NODE_ID` varchar(32) NOT NULL,
  `NODE_NAME` varchar(32) DEFAULT NULL,
  `PAGE_URL` varchar(60) DEFAULT NULL,
  `EXT` varchar(60) DEFAULT NULL,
  `ORG_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`BIZ_TYPE`,`FLOW_ID`,`NODE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for n_wf_comment
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_comment`;
CREATE TABLE `n_wf_comment` (
  `COMMENT_ID` varchar(32) NOT NULL,
  `INSTANCE_ID` varchar(32) DEFAULT NULL,
  `NODE_ID` varchar(10) DEFAULT NULL,
  `USER_ID` varchar(32) DEFAULT NULL,
  `COMMENT_SIGN` varchar(5) DEFAULT NULL,
  `START_TIME` varchar(32) DEFAULT NULL,
  `NODE_LEVEL` int(11) DEFAULT NULL,
  `USER_COMMENT` longtext,
  `USER_NAME` varchar(32) DEFAULT NULL,
  `NODE_NAME` varchar(32) DEFAULT NULL,
  `MAIN_INSTANCE_ID` varchar(32) DEFAULT NULL,
  `EXT` longtext,
  PRIMARY KEY (`COMMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for n_wf_comment_his
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_comment_his`;
CREATE TABLE `n_wf_comment_his` (
  `COMMENT_ID` varchar(32) NOT NULL,
  `INSTANCE_ID` varchar(32) DEFAULT NULL,
  `NODE_ID` varchar(10) DEFAULT NULL,
  `USER_ID` varchar(32) DEFAULT NULL,
  `COMMENT_SIGN` varchar(5) DEFAULT NULL,
  `START_TIME` varchar(32) DEFAULT NULL,
  `NODE_LEVEL` int(11) DEFAULT NULL,
  `USER_COMMENT` longtext,
  `USER_NAME` varchar(32) DEFAULT NULL,
  `NODE_NAME` varchar(32) DEFAULT NULL,
  `MAIN_INSTANCE_ID` varchar(32) DEFAULT NULL,
  `EXT` longtext,
  PRIMARY KEY (`COMMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for n_wf_copy_user
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_copy_user`;
CREATE TABLE `n_wf_copy_user` (
  `INSTANCE_ID` varchar(32) DEFAULT NULL,
  `NODE_ID` varchar(5) DEFAULT NULL,
  `USER_ID` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for n_wf_entrust
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_entrust`;
CREATE TABLE `n_wf_entrust` (
  `BIZ_TYPE` varchar(32) NOT NULL,
  `USER_ID` varchar(32) NOT NULL,
  `USER_NAME` varchar(32) DEFAULT NULL,
  `OTHER_USER_ID` varchar(32) DEFAULT NULL,
  `OTHER_USER_NAME` varchar(32) DEFAULT NULL,
  `START_TIME` varchar(32) DEFAULT NULL,
  `END_TIME` varchar(32) DEFAULT NULL,
  `ORG_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`BIZ_TYPE`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for n_wf_entrust_user
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_entrust_user`;
CREATE TABLE `n_wf_entrust_user` (
  `INSTANCE_ID` varchar(32) NOT NULL,
  `NODE_ID` varchar(32) NOT NULL,
  `USER_ID` varchar(32) NOT NULL,
  `OTHER_USER_ID` varchar(32) DEFAULT NULL,
  `USER_NAME` varchar(32) DEFAULT NULL,
  `OTHER_USER_NAME` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`INSTANCE_ID`,`NODE_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for n_wf_exception
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_exception`;
CREATE TABLE `n_wf_exception` (
  `PK_ID` varchar(32) NOT NULL,
  `INSTANCE_ID` varchar(32) DEFAULT NULL,
  `FLOW_ID` int(11) DEFAULT NULL,
  `NODE_ID` varchar(32) DEFAULT NULL,
  `NODE_NAME` varchar(32) DEFAULT NULL,
  `FLOW_NAME` varchar(50) DEFAULT NULL,
  `BIZ_PARAM` longtext,
  `EXCEPTION_INFO` longtext,
  `EXCEPTION_TIME` varchar(32) DEFAULT NULL,
  `USER_ID` varchar(32) DEFAULT NULL,
  `USER_NAME` varchar(50) DEFAULT NULL,
  `BIZ_ID` varchar(32) DEFAULT NULL,
  `BIZ_TYPE` varchar(32) DEFAULT NULL,
  `OP_TYPE` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`PK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for n_wf_flow
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_flow`;
CREATE TABLE `n_wf_flow` (
  `FLOW_ID` int(11) NOT NULL,
  `SYSTEM_ID` varchar(32) DEFAULT NULL,
  `FLOW_STATE` varchar(5) DEFAULT NULL,
  `FLOW_CONTENT` longtext,
  `START_TIME` varchar(32) DEFAULT NULL,
  `FLOW_SIGN` int(11) NOT NULL,
  `FLOW_NAME` varchar(100) DEFAULT NULL,
  `FLOW_ADMIN` varchar(32) DEFAULT NULL,
  `ORG_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`FLOW_ID`,`FLOW_SIGN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for n_wf_instance
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_instance`;
CREATE TABLE `n_wf_instance` (
  `INSTANCE_ID` varchar(32) NOT NULL,
  `FLOW_NAME` varchar(32) DEFAULT NULL,
  `FLOW_ID` varchar(32) DEFAULT NULL,
  `FLOW_ADMIN` varchar(32) DEFAULT NULL,
  `FLOW_STARTER` varchar(32) DEFAULT NULL,
  `START_TIME` varchar(32) DEFAULT NULL,
  `SYSTEM_ID` varchar(32) DEFAULT NULL,
  `ORG_ID` varchar(32) DEFAULT NULL,
  `FLOW_STATE` varchar(5) DEFAULT NULL,
  `BIZ_ID` varchar(32) DEFAULT NULL,
  `BIZ_USER_NAME` varchar(32) DEFAULT NULL,
  `BIZ_USER_ID` varchar(32) DEFAULT NULL,
  `FLOW_PARAM` text,
  `BIZ_TYPE` varchar(32) DEFAULT NULL,
  `FLOW_STARTER_NAME` varchar(32) DEFAULT NULL,
  `MAIN_INSTANCE_ID` varchar(32) DEFAULT NULL,
  `BIZ_PARAM1` varchar(32) DEFAULT NULL,
  `BIZ_PARAM2` varchar(32) DEFAULT NULL,
  `BIZ_PARAM3` varchar(32) DEFAULT NULL,
  `BIZ_PARAM4` varchar(32) DEFAULT NULL,
  `BIZ_PARAM5` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`INSTANCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for n_wf_instance_his
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_instance_his`;
CREATE TABLE `n_wf_instance_his` (
  `INSTANCE_ID` varchar(32) NOT NULL,
  `FLOW_NAME` varchar(32) DEFAULT NULL,
  `FLOW_ID` varchar(32) DEFAULT NULL,
  `FLOW_ADMIN` varchar(32) DEFAULT NULL,
  `FLOW_STARTER` varchar(32) DEFAULT NULL,
  `START_TIME` varchar(32) DEFAULT NULL,
  `SYSTEM_ID` varchar(32) DEFAULT NULL,
  `ORG_ID` varchar(32) DEFAULT NULL,
  `FLOW_STATE` varchar(5) DEFAULT NULL,
  `BIZ_ID` varchar(32) DEFAULT NULL,
  `BIZ_USER_NAME` varchar(32) DEFAULT NULL,
  `BIZ_USER_ID` varchar(32) DEFAULT NULL,
  `FLOW_PARAM` text,
  `BIZ_TYPE` varchar(32) DEFAULT NULL,
  `END_TIME` varchar(32) DEFAULT NULL,
  `FLOW_STARTER_NAME` varchar(32) DEFAULT NULL,
  `MAIN_INSTANCE_ID` varchar(32) DEFAULT NULL,
  `BIZ_PARAM1` varchar(32) DEFAULT NULL,
  `BIZ_PARAM2` varchar(32) DEFAULT NULL,
  `BIZ_PARAM3` varchar(32) DEFAULT NULL,
  `BIZ_PARAM4` varchar(32) DEFAULT NULL,
  `BIZ_PARAM5` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`INSTANCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for n_wf_instance_sub
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_instance_sub`;
CREATE TABLE `n_wf_instance_sub` (
  `INSTANCE_ID` varchar(32) DEFAULT NULL,
  `BIZ_TYPE` varchar(32) NOT NULL,
  `MAIN_INSTANCE_ID` varchar(32) NOT NULL,
  `START_TIME` varchar(32) DEFAULT NULL,
  `SUB_FLOW_STARTER` varchar(32) DEFAULT NULL,
  `START_NODE_ID` varchar(32) NOT NULL,
  PRIMARY KEY (`BIZ_TYPE`,`MAIN_INSTANCE_ID`,`START_NODE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for n_wf_metting
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_metting`;
CREATE TABLE `n_wf_metting` (
  `FLOW_NAME` varchar(100) DEFAULT NULL,
  `INSTANCE_ID` varchar(32) NOT NULL,
  `USER_ID` varchar(32) DEFAULT NULL,
  `BIZ_ID` varchar(32) DEFAULT NULL,
  `METTING_STS` varchar(2) DEFAULT NULL,
  `START_TIME` varchar(32) DEFAULT NULL,
  `BIZ_PAGE` varchar(50) DEFAULT NULL,
  `NODE_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`INSTANCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for n_wf_metting_his
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_metting_his`;
CREATE TABLE `n_wf_metting_his` (
  `INSTANCE_ID` varchar(32) NOT NULL,
  `BIZ_ID` varchar(32) DEFAULT NULL,
  `START_TIME` varchar(32) DEFAULT NULL,
  `END_TIME` varchar(32) DEFAULT NULL,
  `METTING_SUB` varchar(50) DEFAULT NULL,
  `METTING_YEAR` varchar(32) DEFAULT NULL,
  `METTING_NO` varchar(4) DEFAULT NULL,
  `METTING_COMMENT` varchar(200) DEFAULT NULL,
  `METTING_STS` varchar(4) DEFAULT NULL,
  `USER_ID` varchar(32) NOT NULL,
  `USER_NAME` varchar(32) DEFAULT NULL,
  `BIZ_PAGE` varchar(50) DEFAULT NULL,
  `NODE_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`INSTANCE_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for n_wf_node
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_node`;
CREATE TABLE `n_wf_node` (
  `INSTANCE_ID` varchar(32) NOT NULL COMMENT '实例id',
  `NODE_ID` varchar(10) NOT NULL COMMENT '节点id',
  `NODE_SIGN` varchar(32) DEFAULT NULL COMMENT '节点标识',
  `NODE_NAME` varchar(32) DEFAULT NULL COMMENT '节点名称',
  `NODE_STATE` varchar(5) DEFAULT NULL COMMENT '节点状态',
  `START_TIME` varchar(32) DEFAULT NULL COMMENT '节点开始时间',
  `ORG_ID` varchar(32) DEFAULT NULL COMMENT '提交人机构id',
  `LAST_NODE_ID` varchar(10) DEFAULT NULL COMMENT '上一节点id',
  `LAST_NODE_NAME` varchar(32) DEFAULT NULL COMMENT '上一节点名称',
  `NODE_TYPE` varchar(5) DEFAULT NULL COMMENT '节点类型',
  `NEXT_NODE_ID` varchar(10) DEFAULT NULL COMMENT '下一节点id',
  `NEXT_USER_ID` varchar(32) DEFAULT NULL COMMENT '下一节点处理人',
  PRIMARY KEY (`INSTANCE_ID`,`NODE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='节点流转表';

-- ----------------------------
-- Table structure for n_wf_node_done
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_node_done`;
CREATE TABLE `n_wf_node_done` (
  `INSTANCE_ID` varchar(32) NOT NULL COMMENT '实例id',
  `NODE_ID` varchar(10) NOT NULL COMMENT '节点id',
  `NODE_SIGN` varchar(32) DEFAULT NULL COMMENT '节点标识',
  `NODE_NAME` varchar(32) DEFAULT NULL COMMENT '节点名称',
  `NODE_STATE` varchar(5) DEFAULT NULL COMMENT '节点状态',
  `START_TIME` varchar(32) DEFAULT NULL COMMENT '节点开始时间',
  `END_TIME` varchar(32) DEFAULT NULL COMMENT '节点审批结束时间',
  `ORG_ID` varchar(32) DEFAULT NULL COMMENT '提交人机构id',
  `LAST_NODE_ID` varchar(10) DEFAULT NULL COMMENT '上一节点id',
  `LAST_NODE_NAME` varchar(32) DEFAULT NULL COMMENT '上一节点名称',
  `NODE_TYPE` varchar(5) DEFAULT NULL COMMENT '节点类型',
  `NEXT_NODE_ID` varchar(10) DEFAULT NULL COMMENT '下一节点id',
  `NEXT_USER_ID` varchar(32) DEFAULT NULL COMMENT '下一节点处理人',
  PRIMARY KEY (`INSTANCE_ID`,`NODE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='节点已办表';

-- ----------------------------
-- Table structure for n_wf_node_his
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_node_his`;
CREATE TABLE `n_wf_node_his` (
  `INSTANCE_ID` varchar(32) NOT NULL,
  `NODE_ID` varchar(10) NOT NULL,
  `NODE_SIGN` varchar(32) DEFAULT NULL,
  `NODE_NAME` varchar(32) DEFAULT NULL,
  `NODE_STATE` varchar(5) DEFAULT NULL,
  `START_TIME` varchar(32) DEFAULT NULL,
  `END_TIME` varchar(32) DEFAULT NULL,
  `ORG_ID` varchar(32) DEFAULT NULL,
  `LAST_NODE_ID` varchar(10) DEFAULT NULL,
  `LAST_NODE_NAME` varchar(32) DEFAULT NULL,
  `NODE_TYPE` varchar(5) DEFAULT NULL,
  `NEXT_NODE_ID` varchar(10) DEFAULT NULL,
  `NEXT_USER_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`INSTANCE_ID`,`NODE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for n_wf_taskpool
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_taskpool`;
CREATE TABLE `n_wf_taskpool` (
  `POOL_ID` varchar(32) NOT NULL,
  `POOL_NAME` varchar(32) DEFAULT NULL,
  `POOL_DESC` varchar(32) DEFAULT NULL,
  `ORG_ID` varchar(32) DEFAULT NULL,
  `SYSTEM_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`POOL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for n_wf_taskpool_config
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_taskpool_config`;
CREATE TABLE `n_wf_taskpool_config` (
  `POOL_ID` varchar(32) NOT NULL,
  `POOL_TYPE` varchar(32) NOT NULL,
  `CODE` varchar(32) NOT NULL,
  PRIMARY KEY (`POOL_ID`,`POOL_TYPE`,`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for n_wf_user_done
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_user_done`;
CREATE TABLE `n_wf_user_done` (
  `INSTANCE_ID` varchar(32) NOT NULL,
  `NODE_ID` varchar(10) NOT NULL,
  `USER_ID` varchar(32) NOT NULL,
  `START_TIME` varchar(32) DEFAULT NULL,
  `END_TIME` varchar(32) DEFAULT NULL,
  `USER_NAME` varchar(32) DEFAULT NULL,
  `LAST_USER_ID` varchar(32) DEFAULT NULL,
  `LAST_USER_NAME` varchar(32) DEFAULT NULL,
  `SIGN_IN` varchar(2) DEFAULT NULL,
  `USER_LEVEL` int(11) DEFAULT NULL,
  PRIMARY KEY (`INSTANCE_ID`,`NODE_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for n_wf_user_his
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_user_his`;
CREATE TABLE `n_wf_user_his` (
  `INSTANCE_ID` varchar(32) NOT NULL COMMENT '实例号',
  `NODE_ID` varchar(10) NOT NULL COMMENT '节点ID',
  `USER_ID` varchar(32) NOT NULL COMMENT '用户ID',
  `START_TIME` varchar(32) DEFAULT NULL COMMENT '开始时间',
  `END_TIME` varchar(32) DEFAULT NULL COMMENT '结束时间',
  `USER_NAME` varchar(32) DEFAULT NULL COMMENT '用户姓名',
  `LAST_USER_ID` varchar(32) DEFAULT NULL COMMENT '上一节点处理人',
  `LAST_USER_NAME` varchar(32) DEFAULT NULL COMMENT '上一节点处理人姓名',
  `SIGN_IN` varchar(2) DEFAULT NULL COMMENT '是否签收 1-已签收，0-未签收',
  `USER_LEVEL` int(11) DEFAULT NULL COMMENT '用户顺序，越小越靠前',
  PRIMARY KEY (`INSTANCE_ID`,`NODE_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户办结历史表';

-- ----------------------------
-- Table structure for n_wf_user_todo
-- ----------------------------
DROP TABLE IF EXISTS `n_wf_user_todo`;
CREATE TABLE `n_wf_user_todo` (
  `INSTANCE_ID` varchar(32) NOT NULL,
  `NODE_ID` varchar(10) NOT NULL,
  `USER_ID` varchar(32) NOT NULL,
  `START_TIME` varchar(32) DEFAULT NULL,
  `USER_NAME` varchar(32) DEFAULT NULL,
  `LAST_USER_ID` varchar(32) DEFAULT NULL,
  `LAST_USER_NAME` varchar(32) DEFAULT NULL,
  `SIGN_IN` varchar(2) DEFAULT NULL,
  `USER_LEVEL` int(11) DEFAULT NULL,
  PRIMARY KEY (`INSTANCE_ID`,`NODE_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户待办表';
