
-- 业务功能模块

insert into admin_sm_func_mod (MOD_ID, MOD_NAME, MOD_DESC, IS_OUTER, IS_APP, USER_NAME, PASSWORD, USER_KEY, PWD_KEY, LAST_CHG_USR, LAST_CHG_DT)
values ('0d9311da5f944af89484636194d81c0f', '流程管理', null, 'N', 'N', null, null, null, null, '40', '2020-04-26 09:46:05');

insert into admin_sm_func_mod (MOD_ID, MOD_NAME, MOD_DESC, IS_OUTER, IS_APP, USER_NAME, PASSWORD, USER_KEY, PWD_KEY, LAST_CHG_USR, LAST_CHG_DT)
values ('4734c5bcf0ce474cac250d3b780425dc', '我的工作台', null, 'N', 'N', null, null, null, null, '40', '2020-04-26 09:46:22');

-- 业务功能

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('60e20517f4134223a1eacbfddc4b898d', '0d9311da5f944af89484636194d81c0f', '流程定义', null, 'pages/workflow/studio/nwflist/nwflist', null, null, 1, 'el-icon-yx-menu-1', '40', '2020-04-26 09:40:25');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('caa38a80191d4235b8bb9824c98ef69a', '0d9311da5f944af89484636194d81c0f', '流程仿真', null, 'pages/workflow/studio/wfsimulation/wfsimulation', null, null, 1, 'el-icon-yx-folder-download', '40', '2020-04-26 09:40:49');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('fd7acade58684346a18df612724d31ed', '0d9311da5f944af89484636194d81c0f', '业务配置', null, 'pages/workflow/studio/config/nwfbiz', null, null, 2, 'el-icon-yx-coin-euro', '40', '2020-04-26 09:41:37');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('c1fa76f984bf438fb5d15408cae8d233', '0d9311da5f944af89484636194d81c0f', '委托配置', null, 'pages/workflow/studio/config/nwfagentset', null, null, 1, 'el-icon-yx-compass', '40', '2020-04-26 09:41:58');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('bb1c1d59d9954897b3874476812a6804', '0d9311da5f944af89484636194d81c0f', '项目池配置', null, 'pages/workflow/studio/config/nwftaskpoolset', null, null, 2, 'el-icon-yx-folder-minus', '40', '2020-04-26 09:42:19');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('d2d7f69ef09447719f54eaa0725d6bb3', '0d9311da5f944af89484636194d81c0f', '全局监控', null, 'pages/workflow/studio/wfmonitor/wfmonitor', null, null, 1, 'el-icon-yx-blog', '40', '2020-04-26 09:42:39');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('5707b30b30ad44709081c4ebad66425b', '0d9311da5f944af89484636194d81c0f', '办结实例监控', null, 'pages/workflow/studio/wfmonitor/wfendinstance', null, null, 2, 'el-icon-yx-price-tag', '40', '2020-04-26 09:43:15');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('c6f7d84e825341ee8286f3582c683901', '0d9311da5f944af89484636194d81c0f', '异常队列', null, 'pages/workflow/studio/exception/nwfexception', null, null, 2, 'el-icon-yx-compass', '40', '2020-04-26 09:43:36');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('90d24357049a44d1a1e30cbdbdaba0e6', '0d9311da5f944af89484636194d81c0f', '人员待办监控', null, 'pages/workflow/studio/wfmonitor/usertodo', null, null, 2, 'el-icon-yx-folder-upload', '40', '2020-04-26 09:43:54');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('b52cab9273cb472b9cf6d88e5aca497c', '4734c5bcf0ce474cac250d3b780425dc', '发起示例', null, 'pages/workflow/demo/demo', null, null, 1, 'el-icon-yx-location2', '40', '2020-04-26 09:46:48');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('f797fc7fc21941739201eb88d620030d', '4734c5bcf0ce474cac250d3b780425dc', '我的待办', null, 'pages/workflow/bench/todo/todo', null, null, 1, 'el-icon-yx-folder-download', '40', '2020-04-26 09:47:08');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('3118801e74de4622ac824deed960c53a', '4734c5bcf0ce474cac250d3b780425dc', '我的已办', null, 'pages/workflow/bench/done/done', null, null, 1, 'el-icon-yx-folder-upload', '40', '2020-04-26 09:47:26');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('a6c672765a334167b58613b50f57f0ea', '4734c5bcf0ce474cac250d3b780425dc', '我的办结', null, 'pages/workflow/bench/his/his', null, null, 1, 'el-icon-yx-box-add', '40', '2020-04-26 09:47:43');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('a837f42ea7054718a6ed199d2f16a5d0', '4734c5bcf0ce474cac250d3b780425dc', '办理中', null, 'pages/workflow/bench/start/todo', null, null, 1, 'el-icon-yx-compass', '40', '2020-04-26 09:48:00');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('8099a5b031f64056b0ffab79019f5a9b', '4734c5bcf0ce474cac250d3b780425dc', '已办结', null, 'pages/workflow/bench/start/his', null, null, 1, 'el-icon-yx-compass', '40', '2020-04-26 09:48:19');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('3ae7fa39b05546d8afcbe2c2d77722dc', '4734c5bcf0ce474cac250d3b780425dc', '我的抄送', null, 'pages/workflow/bench/copy/nwfcopyuser', null, null, 1, 'el-icon-yx-location', '40', '2020-04-26 09:48:36');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('a473b91049d44943907b667c3b110823', '4734c5bcf0ce474cac250d3b780425dc', '我的委托', null, 'pages/workflow/bench/entrust/nwfagent', null, null, 1, 'el-icon-yx-location2', '40', '2020-04-26 09:48:52');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('c76c994182ea4e4fa8349179099abce6', '4734c5bcf0ce474cac250d3b780425dc', '我的项目池', null, 'pages/workflow/bench/taskpool/taskpool', null, null, 1, 'el-icon-yx-location2', '40', '2020-04-26 09:49:25');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('cb1b5ce207fa44ceb97cf45036b50268', '4734c5bcf0ce474cac250d3b780425dc', '秘书会办', null, 'pages/workflow/bench/metting/nwfmetting', null, null, 2, 'el-icon-yx-location', '40', '2020-04-26 09:49:42');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('3716a15cf1ff4e00bcb0af391b92a89d', '4734c5bcf0ce474cac250d3b780425dc', '会办投票', null, 'pages/workflow/bench/metting/nwfmettinghis', null, null, 2, 'el-icon-yx-drawer', '40', '2020-04-26 09:50:01');

insert into admin_sm_busi_func (FUNC_ID, MOD_ID, FUNC_NAME, FUNC_DESC, FUNC_URL, FUNC_URL_JS, FUNC_URL_CSS, FUNC_ORDER, FUNC_ICON, LAST_CHG_USR, LAST_CHG_DT)
values ('d52302d8bd124c88a9afb9b34d678c2d', '0d9311da5f944af89484636194d81c0f', '运行实例监控', null, 'pages/workflow/studio/wfmonitor/wfruninstance', null, null, 2, 'el-icon-yx-user-minus', '40', '2020-04-26 09:42:57');

-- 菜单

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('aeb5196abaa64f25b7a2c47c6e4a9e69', 'test', null, '39c0cf27c05a4eff8aba3be93ccb4bf6', '流程管理', 4, null, null, '40', '2020-04-26 09:50:34');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('97b6f77e94c7453fa422ec8bc0827590', 'test', '60e20517f4134223a1eacbfddc4b898d', 'aeb5196abaa64f25b7a2c47c6e4a9e69', '流程定义', 1, 'el-icon-yx-menu-1', null, '40', '2020-04-26 09:51:17');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('1939da3cbbb64daeb008ae1b54c5117b', 'test', 'caa38a80191d4235b8bb9824c98ef69a', 'aeb5196abaa64f25b7a2c47c6e4a9e69', '流程仿真', 2, 'el-icon-yx-newspaper', null, '40', '2020-04-26 09:51:45');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('68efd6082a984e878a20bb97fc92b41b', 'test', 'fd7acade58684346a18df612724d31ed', 'aeb5196abaa64f25b7a2c47c6e4a9e69', '业务配置', 3, 'el-icon-yx-home3', null, '40', '2020-04-26 09:52:11');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('916357f8af694b1ebb6b93c2b25dadb3', 'test', null, 'aeb5196abaa64f25b7a2c47c6e4a9e69', '高级配置', 4, 'el-icon-yx-price-tag', null, '40', '2020-04-26 09:52:48');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('768fe25057394bf5a56be040ee420424', 'test', null, 'aeb5196abaa64f25b7a2c47c6e4a9e69', '流程监控', 5, 'el-icon-yx-folder-minus', null, '40', '2020-04-26 09:53:39');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('c228f7c0c45a4cf9a712b04eb93df5f6', 'test', 'c1fa76f984bf438fb5d15408cae8d233', '916357f8af694b1ebb6b93c2b25dadb3', '委托配置', 1, 'el-icon-yx-barcode', null, '40', '2020-04-26 09:54:23');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('d9f19e59fcc14084b182e6ed2b9ec0a6', 'test', 'bb1c1d59d9954897b3874476812a6804', '916357f8af694b1ebb6b93c2b25dadb3', '项目池配置', 2, 'el-icon-yx-download', null, '40', '2020-04-26 09:54:50');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('41aa202dd22d4ad4af569541e2737391', 'test', 'd2d7f69ef09447719f54eaa0725d6bb3', '768fe25057394bf5a56be040ee420424', '全局监控', 1, 'el-icon-yx-compass2', null, '40', '2020-04-26 09:55:24');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('3a3e7e1249934fc899bd90c81187bddf', 'test', 'd52302d8bd124c88a9afb9b34d678c2d', '768fe25057394bf5a56be040ee420424', '运行实例监控', 2, 'el-icon-yx-price-tag', null, '40', '2020-04-26 09:55:52');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('777d2d0b29bf47499982085e07c4d7c6', 'test', '5707b30b30ad44709081c4ebad66425b', '768fe25057394bf5a56be040ee420424', '办结实例监控', 3, 'el-icon-yx-lifebuoy', null, '40', '2020-04-26 09:56:20');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('009a8c075bd745f2a4860699425b6f14', 'test', 'c6f7d84e825341ee8286f3582c683901', '768fe25057394bf5a56be040ee420424', '异常队列', 4, 'el-icon-yx-sphere', null, '40', '2020-04-26 09:56:46');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('772053935ede469ba7ec036e516cc6e2', 'test', '90d24357049a44d1a1e30cbdbdaba0e6', '768fe25057394bf5a56be040ee420424', '人员待办监控', 5, null, null, '40', '2020-04-26 09:57:13');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('e94ddc333f1248799ca5e31ec51d552a', 'test', null, '39c0cf27c05a4eff8aba3be93ccb4bf6', '我的工作台', 2, 'el-icon-yx-user', null, '40', '2020-04-26 09:58:36');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('47556a4c54da42d5bc9ad8d87f36c7d5', 'test', 'b52cab9273cb472b9cf6d88e5aca497c', 'e94ddc333f1248799ca5e31ec51d552a', '发起示例', 1, 'el-icon-yx-copy', null, '40', '2020-04-26 09:59:00');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('a70c9cce715a4c73b6827a7f7ba337cc', 'test', 'f797fc7fc21941739201eb88d620030d', 'e94ddc333f1248799ca5e31ec51d552a', '我的待办', 2, 'el-icon-yx-users', null, '40', '2020-04-26 09:59:34');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('129ec68727224868a9e439cb9a24b63a', 'test', '3118801e74de4622ac824deed960c53a', 'e94ddc333f1248799ca5e31ec51d552a', '我的已办', 3, 'el-icon-yx-user-minus', null, '40', '2020-04-26 10:00:03');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('2c5b70fe79124261bd9edf85c7010568', 'test', 'a6c672765a334167b58613b50f57f0ea', 'e94ddc333f1248799ca5e31ec51d552a', '我的办结', 4, 'el-icon-yx-user-check', null, '40', '2020-04-26 10:00:37');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('edb70e2ba72c45358364c46c27f59432', 'test', null, 'e94ddc333f1248799ca5e31ec51d552a', '我的发起', 5, 'el-icon-yx-spinner', null, '40', '2020-04-26 10:01:01');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('05e53e39cfc04d50a5a4371ff6294773', 'test', 'a473b91049d44943907b667c3b110823', 'e94ddc333f1248799ca5e31ec51d552a', '我的委托', 7, 'el-icon-yx-cogs', null, '40', '2020-04-26 10:02:39');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('574908bc8df946e598a33c97fb4c313d', 'test', 'c76c994182ea4e4fa8349179099abce6', 'e94ddc333f1248799ca5e31ec51d552a', '我的项目池', 8, 'el-icon-yx-binoculars', null, '40', '2020-04-26 10:03:06');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('11cbe28116f5495095750c18979405ae', 'test', null, 'e94ddc333f1248799ca5e31ec51d552a', '我的会议', 9, 'el-icon-yx-bubble', null, '40', '2020-04-26 10:03:32');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('eba8aeca0959461092f5e3b1d32ffc6c', 'test', 'cb1b5ce207fa44ceb97cf45036b50268', '11cbe28116f5495095750c18979405ae', '秘书会办', 1, 'el-icon-yx-bubbles', null, '40', '2020-04-26 10:03:55');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('e4131ef72be3444bbcd0d3f881679dbf', 'test', '3716a15cf1ff4e00bcb0af391b92a89d', '11cbe28116f5495095750c18979405ae', '会办投票', 2, 'el-icon-yx-user-tie', null, '40', '2020-04-26 10:04:22');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('6a55fa64482d430da4cdff4aad973e47', 'test', '3ae7fa39b05546d8afcbe2c2d77722dc', 'e94ddc333f1248799ca5e31ec51d552a', '我的抄送', 6, 'el-icon-yx-map2', null, '40', '2020-04-26 10:02:25');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('149e56b12604410db8f6a34928a1ba4f', 'test', 'a837f42ea7054718a6ed199d2f16a5d0', 'edb70e2ba72c45358364c46c27f59432', '办理中', 1, 'el-icon-yx-location', null, '40', '2020-04-26 11:02:18');

insert into admin_sm_menu (MENU_ID, SYS_ID, FUNC_ID, UP_MENU_ID, MENU_NAME, MENU_ORDER, MENU_ICON, MENU_TIP, LAST_CHG_USR, LAST_CHG_DT)
values ('1b3d5b4f121740f3bcdf7b66acd63add', 'test', '8099a5b031f64056b0ffab79019f5a9b', 'edb70e2ba72c45358364c46c27f59432', '已办结', 2, 'el-icon-yx-gift', null, '40', '2020-04-26 11:04:27');

-- 菜单、控制点权限

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('ffe464e107e24dca9bdc29683ba2cd63', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '009a8c075bd745f2a4860699425b6f14', '40', '2020-04-26', '009a8c075bd745f2a4860699425b6f14');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('020fdb03965b4a548419fb1386ad0c7b', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '0bebfbf96424497c8c99bb4693185c84', '40', '2020-04-26', '0bebfbf96424497c8c99bb4693185c84');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('764076fab82d43aa8629421b39b706b0', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', 'dafdca98d69041a5aa190a6e489be358', '40', '2020-04-26', 'dafdca98d69041a5aa190a6e489be358');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('e04034fa60204f60916ab829d199a09f', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '2c5b70fe79124261bd9edf85c7010568', '40', '2020-04-26', '2c5b70fe79124261bd9edf85c7010568');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('5b4bfd5e15754ed78f4c5f90483d5d9f', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '3936f63cdc994a038a4935e9bcbcdc3d', '40', '2020-04-26', '3936f63cdc994a038a4935e9bcbcdc3d');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('db72fd5261d54ba49ba6e2651796a756', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '4173d1d5031842c4b844d1728105f1c8', '40', '2020-04-26', '4173d1d5031842c4b844d1728105f1c8');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('b162669de8f141dfb078176ca3914821', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', 'ed03c7a1318f40fa9dadcba37209d286', '40', '2020-04-26', 'ed03c7a1318f40fa9dadcba37209d286');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('fd8bde9260554794b0f499d83b8d99da', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '1211', '40', '2020-04-26', '1211');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('52b0c9b617a04b45a439991e4d5057e6', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '1939da3cbbb64daeb008ae1b54c5117b', '40', '2020-04-26', '1939da3cbbb64daeb008ae1b54c5117b');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('278d690baf88450584b12c73d1bdf0c4', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '3fdfb39fccac4d1da47174f5b5fb1402', '40', '2020-04-26', '3fdfb39fccac4d1da47174f5b5fb1402');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('d57814ec31904f3fb004f239a1cc64a5', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '4be52cb3923f4b60aaf41add36736d87', '40', '2020-04-26', '4be52cb3923f4b60aaf41add36736d87');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('4ef634f876c64ebe8d0072dd82f1f962', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '916357f8af694b1ebb6b93c2b25dadb3', '40', '2020-04-26', '916357f8af694b1ebb6b93c2b25dadb3');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('30e92b307a72400d8295fdaf23c96e53', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', 'c228f7c0c45a4cf9a712b04eb93df5f6', '40', '2020-04-26', 'c228f7c0c45a4cf9a712b04eb93df5f6');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('01699975f6314816b5e88c1fdc6af69a', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', 'e22e72e7cadd49a0bb42c6544f5dd12f', '40', '2020-04-26', 'e22e72e7cadd49a0bb42c6544f5dd12f');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('b3bcb9a0ae294e14889b272b2537c970', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '149e56b12604410db8f6a34928a1ba4f', '40', '2020-04-26', '149e56b12604410db8f6a34928a1ba4f');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('3976250153a2496a9ff27ed3ef82b375', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', 'edb70e2ba72c45358364c46c27f59432', '40', '2020-04-26', 'edb70e2ba72c45358364c46c27f59432');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('f22eb7cd640c4462932e891d31d50d9f', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '3a3e7e1249934fc899bd90c81187bddf', '40', '2020-04-26', '3a3e7e1249934fc899bd90c81187bddf');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('92f80d0257484f48b794c333e46dece0', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '68efd6082a984e878a20bb97fc92b41b', '40', '2020-04-26', '68efd6082a984e878a20bb97fc92b41b');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('cd9c2c23df1d4d2cb5ce64ee23caf863', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '8878aee19d8a4943ab8fcdd06028144b', '40', '2020-04-26', '8878aee19d8a4943ab8fcdd06028144b');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('b62113db32d947eea7e30859f1e068bf', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '440523ac1e9740feb4b782b6d8e00a91', '40', '2020-04-26', '440523ac1e9740feb4b782b6d8e00a91');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('c397e74998e548a68732ed329637c501', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '74d47cbb74d64263b5b2da4f18bfdd2d', '40', '2020-04-26', '74d47cbb74d64263b5b2da4f18bfdd2d');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('efadb6e2f50f4e958ec8059fd77a0f12', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '8367fb9ae69c4e9aaf6c31316c01a38f', '40', '2020-04-26', '8367fb9ae69c4e9aaf6c31316c01a38f');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('9d72b4d5d2af4834aa09e847f334eb83', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '907c8c7ad35646b285d37bac9f74fb0b', '40', '2020-04-26', '907c8c7ad35646b285d37bac9f74fb0b');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('39169a90f9014af6b55b5ebb91f23afe', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', 'a854b72e7bec46d1ab27d182b2f88b5d', '40', '2020-04-26', 'a854b72e7bec46d1ab27d182b2f88b5d');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('df282ca8fede472c87a56789ee0f385a', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', 'eba8aeca0959461092f5e3b1d32ffc6c', '40', '2020-04-26', 'eba8aeca0959461092f5e3b1d32ffc6c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('099b10f759294beeb3d0f1afa43c43ec', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '768fe25057394bf5a56be040ee420424', '40', '2020-04-26', '768fe25057394bf5a56be040ee420424');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('b8c5e32a77d34b77993fc3c95668f616', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '288a43e9d2c6432986b15e9faf274960', '40', '2020-04-26', '288a43e9d2c6432986b15e9faf274960');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('f4ae08b676bc4825ba360f02e1817db9', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '4eccf88dbdce4151a2a31ea0f236021c', '40', '2020-04-26', '4eccf88dbdce4151a2a31ea0f236021c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('64113420260a42c29ff283f5b691e445', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '5ed68cd744914c83ba300d85eda1b3c8', '40', '2020-04-26', '5ed68cd744914c83ba300d85eda1b3c8');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('f062bde11a4448889d6b63eb4cbd0040', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '6a55fa64482d430da4cdff4aad973e47', '40', '2020-04-26', '6a55fa64482d430da4cdff4aad973e47');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('3cb2a25ade0d4ae4b4809206d67d5df6', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '8b0c26d45792416fb31d314586f6ebf7', '40', '2020-04-26', '8b0c26d45792416fb31d314586f6ebf7');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('7ed5f63fb12249bc933d05155c457dd4', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '97b6f77e94c7453fa422ec8bc0827590', '40', '2020-04-26', '97b6f77e94c7453fa422ec8bc0827590');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('86cd9717a2d246b7885eb8e13009d8ab', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', 'a9dd3aa991cb423e94212dd18a42200c', '40', '2020-04-26', 'a9dd3aa991cb423e94212dd18a42200c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('7c639c3689f14ce0b1f4767c24ada78c', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', 'aeb5196abaa64f25b7a2c47c6e4a9e69', '40', '2020-04-26', 'aeb5196abaa64f25b7a2c47c6e4a9e69');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('9a63d8257ed242a592bf52c6bc25b0fc', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '39c0cf27c05a4eff8aba3be93ccb4bf6', '40', '2020-04-26', '39c0cf27c05a4eff8aba3be93ccb4bf6');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('af9f154c9c564e0488df67a7b2c7c989', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '129ec68727224868a9e439cb9a24b63a', '40', '2020-04-26', '129ec68727224868a9e439cb9a24b63a');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('748ad45fa5c745bea1868a3c9a995327', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '5cb209f943974c038e5d93292a271e11', '40', '2020-04-26', '5cb209f943974c038e5d93292a271e11');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('383f5638f74845a6a5e3d9c89de81f84', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '05e53e39cfc04d50a5a4371ff6294773', '40', '2020-04-26', '05e53e39cfc04d50a5a4371ff6294773');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('66900af8c066405fb16b88ac05817955', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', 'gm-10000', '40', '2020-04-26', 'gm-10000');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('8d6d8890f05f48849aae669bb9b0fcbe', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', 'd9d4e444f1c4448dbed674d11e138a5a', '40', '2020-04-26', 'd9d4e444f1c4448dbed674d11e138a5a');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('72fcb39b982c4c16b76312ea1f660174', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', 'e94ddc333f1248799ca5e31ec51d552a', '40', '2020-04-26', 'e94ddc333f1248799ca5e31ec51d552a');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('cd9c00e4203f47d4a663a5c225fdcf44', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '11cbe28116f5495095750c18979405ae', '40', '2020-04-26', '11cbe28116f5495095750c18979405ae');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('7787e8754fa3465cb497b305fd7856e3', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '1b3d5b4f121740f3bcdf7b66acd63add', '40', '2020-04-26', '1b3d5b4f121740f3bcdf7b66acd63add');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('2ae57f9e5e824447a353d2b1ca226a7c', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '733c7872049a429c8381b10c5969fca0', '40', '2020-04-26', '733c7872049a429c8381b10c5969fca0');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('290a603acebb434191761641ac325e83', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '41aa202dd22d4ad4af569541e2737391', '40', '2020-04-26', '41aa202dd22d4ad4af569541e2737391');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('32eefcb79bb3424cb807f7c68b7456b3', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '47556a4c54da42d5bc9ad8d87f36c7d5', '40', '2020-04-26', '47556a4c54da42d5bc9ad8d87f36c7d5');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('dadbab1421344de88dc11d0fc203d8b6', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '574908bc8df946e598a33c97fb4c313d', '40', '2020-04-26', '574908bc8df946e598a33c97fb4c313d');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('82d52ea79e7c4f18939ea8bb5b5f5588', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '63c2e6d0c5dc496490f4ea44dd3abf6e', '40', '2020-04-26', '63c2e6d0c5dc496490f4ea44dd3abf6e');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('7b3ec2812a844860aeabc89d06da6c14', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '772053935ede469ba7ec036e516cc6e2', '40', '2020-04-26', '772053935ede469ba7ec036e516cc6e2');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('041874ebb6e54985ab4536dab29d718a', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', '777d2d0b29bf47499982085e07c4d7c6', '40', '2020-04-26', '777d2d0b29bf47499982085e07c4d7c6');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('0cb000294a7c43b4aef583bdae2abc1a', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', 'a70c9cce715a4c73b6827a7f7ba337cc', '40', '2020-04-26', 'a70c9cce715a4c73b6827a7f7ba337cc');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('cfaebef5be5645d0b596d9ac28ffd31f', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', 'd9f19e59fcc14084b182e6ed2b9ec0a6', '40', '2020-04-26', 'd9f19e59fcc14084b182e6ed2b9ec0a6');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('649a0a4b77ac4d859eb40fead5b038b8', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'M', 'e4131ef72be3444bbcd0d3f881679dbf', '40', '2020-04-26', 'e4131ef72be3444bbcd0d3f881679dbf');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('1d4889a654744230a96e17d7e46f7002', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '6dc6031acaec4ee7ae7a5f1158d5d5b9', '40', '2020-04-26', '4173d1d5031842c4b844d1728105f1c8');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('2e7a4a875aba43b1853e2513f42ff5ff', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '1795f499879c41b6897fac13e466c184', '40', '2020-04-26', '4173d1d5031842c4b844d1728105f1c8');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('7debea8f21c24675966b48c5c13c06c3', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '63f3ec7af420454caadded14d3ff6100', '40', '2020-04-26', '4173d1d5031842c4b844d1728105f1c8');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('43e0c65ec3364555bcaf32127821366d', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'c2246098622649678444b1b122ecf499', '40', '2020-04-26', '4173d1d5031842c4b844d1728105f1c8');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('3a52d29dbc4241b89d3db5fda0c68123', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '387f91f7ad684ee195dcdadef6a06002', '40', '2020-04-26', '63c2e6d0c5dc496490f4ea44dd3abf6e');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('f5bf7157f4d344b98106573687614425', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'ab7700d89a8b4edd98c315212186740d', '40', '2020-04-26', '63c2e6d0c5dc496490f4ea44dd3abf6e');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('02eb479a44e14543a500960dfc255a59', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '465c979a9ecf4a3fa7e9071a727b0a25', '40', '2020-04-26', '63c2e6d0c5dc496490f4ea44dd3abf6e');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('492b6e99c89143f3b79348ce2e9abb4d', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '7fd355081b564cfd9a472bdcf3ee0dda', '40', '2020-04-26', '63c2e6d0c5dc496490f4ea44dd3abf6e');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('05ae04707e614489b4ae28e3c05da903', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'ccca8093dc064f5abcc89ec6c020759c', '40', '2020-04-26', '63c2e6d0c5dc496490f4ea44dd3abf6e');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('30268970d43d407bb71be24faf6ba15e', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '98fe97c56f954f9e9166296c682674cf', '40', '2020-04-26', '63c2e6d0c5dc496490f4ea44dd3abf6e');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('d54e16ad320c47859da4a7e46db196c8', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '49969970a3ef4ff58bf544e1b6a07b86', '40', '2020-04-26', '8878aee19d8a4943ab8fcdd06028144b');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('853d2dd732e746d483e58d7a3dfb9ea0', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '7baa6759f828434bb28c5e5dae73d145', '40', '2020-04-26', '8878aee19d8a4943ab8fcdd06028144b');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('eb9ce10ec20f4f24ae181d58e8975653', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'b2e4e75b25e845e6a73361662bd1dcb6', '40', '2020-04-26', '8878aee19d8a4943ab8fcdd06028144b');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('44eda8d00791448a81c58b9b7a5ff560', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '97028e1e74bc411f927d28c1c102e1b4', '40', '2020-04-26', '8878aee19d8a4943ab8fcdd06028144b');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('761adcb2c7e4492ea31586b2515b4462', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '0f0b2e4bbb6c4264bc5a4faf72c12cfa', '40', '2020-04-26', '8878aee19d8a4943ab8fcdd06028144b');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('7a2aef741a434550a6272e6cb45d3254', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'b7a6b4072fb34f21a98552a0ba73b7b0', '40', '2020-04-26', '8878aee19d8a4943ab8fcdd06028144b');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('0f3348f3bcdc4f3e8e613f54d7c36641', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '53c5a7c1e12b43e88cff989292852bbc', '40', '2020-04-26', '0bebfbf96424497c8c99bb4693185c84');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('487a04e4618d4ebc9d0fb89aaefc07b4', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'aa23821c3f6e42f1ac77a7603a612204', '40', '2020-04-26', '0bebfbf96424497c8c99bb4693185c84');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('a7a0e0a7a24b4bd9b7b4e1fab97b8f75', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '3571e82ec7364658ac5b1b2bfd74b322', '40', '2020-04-26', '0bebfbf96424497c8c99bb4693185c84');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('585fe208131a4e8198924b7a464719b2', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '938a66e0e3fd440e8713b8b35409af0d', '40', '2020-04-26', '0bebfbf96424497c8c99bb4693185c84');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('3822171e10ce4013adc9e9b0580f3d89', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'cacdac94d56143208e887f7f5e2ad3f9', '40', '2020-04-26', '0bebfbf96424497c8c99bb4693185c84');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('9f045c77a4d04a1e97c93afbea906f55', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '87b5da3f2b044e9c85a83203b57eb96c', '40', '2020-04-26', '0bebfbf96424497c8c99bb4693185c84');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('c38de36eef2a4e65ab8413c2e9c3c526', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'da9267cd702346298972c666ed98ca7d', '40', '2020-04-26', '0bebfbf96424497c8c99bb4693185c84');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('53afdb270bc54e78afbe5e6897cf38fc', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'e3931683d6dc4e1f81e94cd921655ee9', '40', '2020-04-26', '0bebfbf96424497c8c99bb4693185c84');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('ae79529099594bbba70224023525360f', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '28bfd469c0834dd6875720ab77b8402b', '40', '2020-04-26', '907c8c7ad35646b285d37bac9f74fb0b');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('4db9b2594f5d487fadac807ea6647647', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '7e8c5f294a964a12a06134d7cc7bb88a', '40', '2020-04-26', '907c8c7ad35646b285d37bac9f74fb0b');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('58e0d01691e743b6a75bf0158f106e42', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'c3c0902727ad40c5a023ad664c453e0d', '40', '2020-04-26', '907c8c7ad35646b285d37bac9f74fb0b');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('019b5cb45ec24e829f1c0c824257bb0f', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '51458009ba1f44c2a21ebe1a8276bbea', '40', '2020-04-26', '907c8c7ad35646b285d37bac9f74fb0b');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('a63b39f42ddc429cbe2f0e5f98b450f2', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '6b3ddd4d8e1a4ca2841a9bfb9222ef7c', '40', '2020-04-26', '907c8c7ad35646b285d37bac9f74fb0b');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('bafc638cf62d4056a6f1283ab8948185', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'bb165140736c45a7a58b1d34ad10b7d7', '40', '2020-04-26', '907c8c7ad35646b285d37bac9f74fb0b');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('6a77caccb6ad418096329022b7a5780b', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'f18527cbb8944ea0a28bb4175364abb2', '40', '2020-04-26', '8367fb9ae69c4e9aaf6c31316c01a38f');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('0e787b6a9b114f8d8fbc0f2f5902778f', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '164acad3a573437b8d72d4ff7e834147', '40', '2020-04-26', '8367fb9ae69c4e9aaf6c31316c01a38f');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('8f9dcc5469a64e5b943f88ca3c586d9f', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '8f60418721974203a4b386f97c122355', '40', '2020-04-26', '8367fb9ae69c4e9aaf6c31316c01a38f');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('c8fb1d5021524b83b3ed714d715c33a9', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'ce46bf878e224463afdac7c43928575c', '40', '2020-04-26', '8367fb9ae69c4e9aaf6c31316c01a38f');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('f3e4e98904b346939cc9a3e23d968195', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '27187a925ab84709b6b359f3ab7984e1', '40', '2020-04-26', '8367fb9ae69c4e9aaf6c31316c01a38f');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('45257f7ec3ec41f88a9d580b659a0f03', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'c1050950748140639587b0c682a8b59c', '40', '2020-04-26', '8367fb9ae69c4e9aaf6c31316c01a38f');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('acdaf5dec88249a9ad6cd27066bad06f', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '319b2a5dbab143399009ecafd24d9d94', '40', '2020-04-26', '1211');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('98914ac834784c9d980c4e85e0a15841', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '5a7d28a5c7fb42a683901326d1d29360', '40', '2020-04-26', '1211');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('d61fa477fc214eaf857a48954c3683e9', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '1abb327863bc4f8b9f0a7a33204aa6a6', '40', '2020-04-26', '1211');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('4b83afc27b574eb29ba379241558483b', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '7235ea87a9144bd7bce318bd38e66f0d', '40', '2020-04-26', '1211');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('252858439036464eb70053cc4ec2d461', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '4c77830aac4248c99809396196b98aeb', '40', '2020-04-26', 'e22e72e7cadd49a0bb42c6544f5dd12f');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('b232c26bee824a25ac01863c499e31a4', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '2d4cb0a177b4450e9a45433c963b696a', '40', '2020-04-26', 'e22e72e7cadd49a0bb42c6544f5dd12f');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('542a81e277a04fd4b18d08f125e2f083', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'aa691267e001463bb2c8699edbdad759', '40', '2020-04-26', 'e22e72e7cadd49a0bb42c6544f5dd12f');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('53b2232abbed4c18ba2ac2ab854ece1c', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '41645bf02d3745618a43f126ac0b20c1', '40', '2020-04-26', 'e22e72e7cadd49a0bb42c6544f5dd12f');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('ddadd80ad1e74e8f9c7a8fa5d0b3da31', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'c5b56f246204421c8f638fb71fe8cc05', '40', '2020-04-26', 'e22e72e7cadd49a0bb42c6544f5dd12f');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('db2e3569957341f1acac3f74caf4acc1', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '9c407aaacc354e7a800959bd233fb44a', '40', '2020-04-26', 'a9dd3aa991cb423e94212dd18a42200c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('529faf7e53a04c15acfc3efcc550c5f4', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'cf45cfdde79d4bc8a1769ac0dec4e5fb', '40', '2020-04-26', 'a9dd3aa991cb423e94212dd18a42200c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('b2f8a1645723440896a91e30408c571d', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'eb9ccc6364fb4038b909d0d67390890c', '40', '2020-04-26', 'a9dd3aa991cb423e94212dd18a42200c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('f5732f530a564a6a97398fcf25bc4a10', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'f04702152c524f5180c403ec8bd23120', '40', '2020-04-26', 'a9dd3aa991cb423e94212dd18a42200c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('9fccf69e73634629a97a984785a90575', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'be7e41c20eb44ba19b9387825c5b5752', '40', '2020-04-26', 'a9dd3aa991cb423e94212dd18a42200c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('00848d74a030403ebf98eccc411ddbf1', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '29ae768414f240e9b51e9039019460f2', '40', '2020-04-26', 'a9dd3aa991cb423e94212dd18a42200c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('57aec1d80dc540fab1d0bee144252137', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '3868b7d52fe9487fad953916810dc97e', '40', '2020-04-26', 'a9dd3aa991cb423e94212dd18a42200c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('ac39b8e7f18e4703b1ec19c89ae2f191', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '317a3f9500a0405583ce752a00f2a495', '40', '2020-04-26', 'a9dd3aa991cb423e94212dd18a42200c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('8bdcd345f49a48c48ef411eef991dce9', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '39edaea7b2c24824981d57e479451afe', '40', '2020-04-26', 'a9dd3aa991cb423e94212dd18a42200c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('43be4bd0f2484b838b7260e742cf3fa2', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '78b1a9198d6b4107bb59d15750d3d94e', '40', '2020-04-26', 'a9dd3aa991cb423e94212dd18a42200c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('5ec29ebf97a54088b16bd8f45aaa1b47', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '49feee5d2b734e2ab8b4704d4a220d6c', '40', '2020-04-26', '4eccf88dbdce4151a2a31ea0f236021c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('9ef014524ed44adb9fab44816e8c64be', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'e5699c8722a44f808623aa762895b83a', '40', '2020-04-26', '4eccf88dbdce4151a2a31ea0f236021c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('5fa94dcbd5d447d99ab1afb44b7c0095', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '9d47c375398c41a3a4e3cef576b45a88', '40', '2020-04-26', '4eccf88dbdce4151a2a31ea0f236021c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('ba6d127a4bf64b78b26781c723e85b0c', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'd4175b661b364333af85803c91429cdb', '40', '2020-04-26', '4eccf88dbdce4151a2a31ea0f236021c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('f4ca14647e2141a5bdc9c68c00d2cd7e', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'b0f56c4cf36e4adc9bb3ccfb71dfd80b', '40', '2020-04-26', '4eccf88dbdce4151a2a31ea0f236021c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('6d9224f47d07405890dd088ad9777e3d', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '78db9388ea00473eb8efba468213941d', '40', '2020-04-26', '4eccf88dbdce4151a2a31ea0f236021c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('0d4577f7e0bb46948965ce4f5cf4b170', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '6922013364a94cd1a6f9b1ede231815f', '40', '2020-04-26', '4eccf88dbdce4151a2a31ea0f236021c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('3872c16d49424d4c914a770eb7acfc3a', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'cc885d433edc40098b2e49be2360e571', '40', '2020-04-26', '4eccf88dbdce4151a2a31ea0f236021c');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('3de3245be5874e0481f1d806cbd6b9f6', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'da1b6574e28646909fa61820b59b7f8c', '40', '2020-04-26', '5ed68cd744914c83ba300d85eda1b3c8');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('0f99b89e60ce45ae963dbd67e0d731f6', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '9889ad530b47428fb596e52724fc8ac0', '40', '2020-04-26', '5ed68cd744914c83ba300d85eda1b3c8');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('21e1a1a34fae4f6589e8fbbbff0480d6', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'c9ed77239d964cb2a8fa9c72e808808e', '40', '2020-04-26', '5ed68cd744914c83ba300d85eda1b3c8');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('30f772e104094384a7e89ed3a5aa5b57', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'a445e4fba7e3416f8d37c3d052cbe1bf', '40', '2020-04-26', '5ed68cd744914c83ba300d85eda1b3c8');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('bf824b61aa7c47eaadceb84a81564c88', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'f0a57450a4e54da4b97085be4f5a617d', '40', '2020-04-26', 'ed03c7a1318f40fa9dadcba37209d286');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('b0fa463ff9814fa7bc499e0504cca698', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'c46569693f4e4afd8c66214c87a602f7', '40', '2020-04-26', 'ed03c7a1318f40fa9dadcba37209d286');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('0c072bd3598143c69828f7f884bbfc1f', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'fbd2cd29cea9403a8b49b242a9ac6e60', '40', '2020-04-26', 'ed03c7a1318f40fa9dadcba37209d286');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('698c1db10cb04993b62b5b01556936b7', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '3fa4ad892f104ff6bbee12481fffff59', '40', '2020-04-26', 'ed03c7a1318f40fa9dadcba37209d286');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('5fdaa77828ce48dea727cdff8624d29f', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '7ff44f04753e46cfac874c32ce6dfc26', '40', '2020-04-26', 'd9d4e444f1c4448dbed674d11e138a5a');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('02e6ac8512a845b5a9c0a54ed8354e6a', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', 'f665e30adf50441496c64ccd995e563b', '40', '2020-04-26', 'd9d4e444f1c4448dbed674d11e138a5a');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('9a7c6ecca26448c1aae39702d4636e05', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '7eae393b1279441aa06af36bb69de3ee', '40', '2020-04-26', 'd9d4e444f1c4448dbed674d11e138a5a');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('f29ecebaf8a841c58cd0ea9004157204', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '0e075f900717499cb021977132f97a79', '40', '2020-04-26', '3936f63cdc994a038a4935e9bcbcdc3d');

insert into admin_sm_auth_reco (AUTH_RECO_ID, SYS_ID, AUTHOBJ_TYPE, AUTHOBJ_ID, AUTHRES_TYPE, AUTHRES_ID, LAST_CHG_USR, LAST_CHG_DT, MENU_ID)
values ('0fc6a405ac05471ea09bde41629aec73', 'test', 'R', 'cc81a8d86f274c81bc680a0bbd27e358', 'C', '9bfd1f87a0fc4dfaa3d2f77b3917d763', '40', '2020-04-26', '3936f63cdc994a038a4935e9bcbcdc3d');

