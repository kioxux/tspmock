## 消息

### 启用消息

在启动引导类上添加注解`@EnableMessageService`开启消息服务。

### 消息发送模式

目前支持`memory`和`rabbit`两种模式. 通过`spring.stream.default-binder`配置。

可通过配置文件中`spring.cloud.stream.default-binder`j进行切换. 当模式为`rabbit`是需要配置`RabbitMQ`的配置, 参考示例:

```yaml

spring:
  application:
    stream:
      default-binder: rabbit
  rabbitmq:
    password: admin
    username: admin
    addresses: 139.155.247.143
    port: 5672
```

*注意*:      
如果使用`memory`模式时, 配置文件中使用下面的方式排除`RabbitMQ`的自动加载类, 以免程序启动时去连接`RabbitMQ`服务器。

```yaml
spring:
  autoconfigure:
    exclude:
      - org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration
```     

也可以通过在启动引导类上通过`@SpringBootApplication`的`exclude`进行排除。

### 发送邮件消息的邮件配置参考

邮件发送配置参考:

```yaml
spring:
  mail:
    enabled: true
    host: smtp.163.com
    port: 25
    username: xxx@163.com
    password: yyyy
    properties:
      mail:
        smtp:
          auth: true
          socketFactoryClass: javax.net.ssl.SSLSocketFactory
          starttls:
            enable: true
            required: true
```

*注意*：  
上面的邮件配置中`spring.mail.enabled`设为`false`或者不配置整个`spring.mail`均不会启用邮件发送渠道。

### 如何发送消息

注入`MessageSendService`, 调用`send`方法即可

### 消息模板参数的语法

消息模板参数是`K-V`键值对的Map对象, `Key`存储的模板名称, `Value`存储的是模板名称对应的值.

界面上操作消息模板要注入模板变量, 使用`${Key}`的形式填写, 发送消息时`MessageUtils#parseParam`方法会对模板变量进行解析.

在模板变量中注入, MessageConstant.MESSAGE_PARAM_START_TIME和MessageConstant.MESSAGE_PARAM_END_TIME可指定消息发送的时间范围

### 扩展新的消息发送渠道

1. `MessageChannelEnum`类添加新的发送渠道
2. 添加新的渠道发送实现类,需要实现`MessageChannelPublisher`接口

### 应用实例启动后加载消息队列

应用实例启动后会从数据库加载消息队列进行发送, 为避免未发送的消息被启动的实例加载重复发送, 每个实例应单独配置自己的`pkHash`范围.详见`cn.com.yusys.yusp.message.config.PkHash`

### 可以定义的配置

以下配置需要修改就添加到配置文件中,无需修改则不用添加, 程序会使用默认值

```yaml
yusp:
  email-send-pool:
    core-pool-size: 8 # 发送Email线程池的核心线程数,默认为8个线程
    max-pool-size: 20 # 发送Email线程池的最大线程数,默认为20个线程
    queue-capacity: 1000 # 发送Email线程池的最大队列容量,默认为1000
  do-not-disturb-start-time-hour: 8 # 免打扰开始时间的小时配置,默认为8点
  do-not-disturb-start-time-minute: 0 # 免打扰开始时间的分钟配置,默认为第0分
  do-not-disturb-start-time-second: 0 # 免打扰开始时间的秒数配置,默认为第0秒
  do-not-disturb-end-time-hour: 21 # 免打扰结束时间的小时配置, 默认为21点
  do-not-disturb-end-time-minute: 30 # 免打扰结束时间的分钟配置, 默认为第30分钟
  do-not-disturb-end-time-second: 0 # 免打扰结束时间的秒数配置,默认为第0秒
  default-message-title: No Title # 当消息转系统公告的时候没有标题时的默认标题
  pk-hash-min: 0 # pkHash随机数区间最小值， 默认为0
  pk-hash-max: 50 # pkHash随机数区间最大值

```