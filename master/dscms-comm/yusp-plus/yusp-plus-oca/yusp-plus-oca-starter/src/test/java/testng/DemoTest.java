/**
 * Copyright (C), 2014-2021
 * FileName: demoTest
 * Author: Administrator
 * Date: 2021/3/8 10:44
 * Description:
 * History:
 * <author> <time> <version> <desc>
 * Administrator 10:44 1.0.0 新建类
 */

package testng;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.*;

/**
 * 该测试用例用来展示testNG基本注解的执行情况，通过执行该测试类，根据控制台输出日志情况，查看各注解什么情况下会执行
 *
 * 〈TestNG 基本注解〉
 * BeforeSuite注解 在该套件的所有测试都运行在注释的方法之前，仅运行一次。
 * AfterSuite注解 在该套件的所有测试都运行在注释方法之后，仅运行一次。
 * BeforeClass注解 在调用当前类的第一个测试方法之前运行，注释方法仅运行一次。
 * AfterClass注解 在调用当前类的第一个测试方法之后运行，注释方法仅运行一次。
 * BeforeTest注解 注释的方法将在属于<test>标签内的类的所有测试方法运行之前运行。
 * AfterTest注解 注释的方法将在属于<test>标签内的类的所有测试方法运行之后运行。
 * BeforeGroups注解 配置方法将在之前运行组列表。此方法保证在调用属于这些组中的任何一个的第一个测试方法之前不久运行。
 * AfterGroups注解 此配置方法将在之后运行组列表。该方法保证在调用属于任何这些组的最后一个测试方法之后不久运行。
 * BeforeMethod注解 注释方法将在每个测试方法之前运行。@AfterMethod注释方法将在每个测试方法之后运行。
 *
 * @author Administrator
 * @create 2021/3/8
 * @since 1.0.0
 */

@Test(groups = {"demo"})
public class DemoTest {
    private static final Logger log = LoggerFactory.getLogger(DemoTest.class);
    public static String CONSTANT;

    @Test(groups = "oca")
    public void test1(){
        String email = "123456@qq.com";
        Assert.assertNotNull(email);
        Assert.assertEquals(email, "123456@qq.com");
        log.info("DemoTest Run test1");
        //throw new RuntimeException("error");
    }

    @Test(groups = "uaa") // add group
    public void test2(){
        log.info("DemoTest Run test2");
    }

    @Test(groups = {"oca","uaa"}) // add group
    public void test3(){
        log.info("DemoTest Run test3");
    }


    @BeforeSuite
    public void test4(){
        CONSTANT= "DemoTest static constant";
        log.info("DemoTest Run BeforeSuite");
    }

    @AfterSuite
    public void test5(){
        log.info("DemoTest Run AfterSuite");
    }

    @BeforeTest
    public void test6(){
        log.info("DemoTest Run BeforeTest");
    }

    @AfterTest
    public void test7(){
        log.info("DemoTest Run AfterTest");
    }

    @AfterClass
    public void test8(){
        log.info("DemoTest Run AfterClass");
    }

    @BeforeClass
    public void test9(){
        log.info("DemoTest Run BeforeClass");
    }

    /*@BeforeGroups("oca")
    public void test10(){
        log.info("DemoTest Run BeforeGroups oca");
    }


    @AfterGroups("oca")
    public void test11(){
        log.info("DemoTest Run AfterGroupS oca");
    }*/


    @BeforeMethod(groups = "uaa")
    public void test12(){
        log.info("DemoTest Run BeforeMethod");
    }

    @AfterMethod
    public void test13(){
        log.info("DemoTest Run AfterMethod");
    }

    @BeforeGroups("uaa")
    public void test14(){
        log.info("DemoTest Run BeforeGroups uaa");
    }

    @AfterGroups("uaa")
    public void test15(){
        log.info("DemoTest Run AfterGroupS uaa");
    }

    @Test(dependsOnGroups = {"oca","uaa"})
    public void test16(){
        log.info("DemoTest Run dependsOnGroups");
    }
}

