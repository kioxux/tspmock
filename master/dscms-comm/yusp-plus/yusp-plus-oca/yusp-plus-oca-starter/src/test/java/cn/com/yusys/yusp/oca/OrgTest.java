package cn.com.yusys.yusp.oca;

import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.dao.AdminSmOrgDao;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmOrgEntity;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
public class OrgTest {

    @Autowired
    private AdminSmOrgDao adminSmOrgDao;

    private static int MAX_LEVEL=6;//机构层级
    private static int MAX_SUB_COUNT=50;//单节点子机构数量上限
    private static int MAX_SUM=6000; //总量上限
    private int sum=0;


    @Test
    public void createOrgDara(){
        for(int j=1;j<20;j++){
            AdminSmOrgEntity root=new AdminSmOrgEntity();
            root.setOrgCode("ORG00"+j);
            root.setOrgName("机构"+root.getOrgCode());
            System.out.println("----------------------------------------------------------------"+root.getOrgName());
            root.setUpOrgId("500");
            root.setOrgLevel(2);
            root.setInstuId("ecd20f1d26b2449fb763b73afa219d96");
            root.setLastChgDt(new Date());
            root.setLastChgUsr("40");
            root.setOrgSts(AvailableStateEnum.ENABLED);
            adminSmOrgDao.insert(root);
            LambdaQueryWrapper<AdminSmOrgEntity> wrapper=new QueryWrapper<AdminSmOrgEntity>().lambda();
            wrapper.eq(AdminSmOrgEntity::getOrgCode,root.getOrgCode());
            root=adminSmOrgDao.selectList(wrapper).get(0);
            creatTreeFromAroot(root);
        }
        System.out.println("----------------------------------------------------------------end");
    }

    private void creatTreeFromAroot(AdminSmOrgEntity parent){
        if(parent.getOrgLevel()>MAX_LEVEL){
            return;
        }
        if(sum>MAX_SUM){
            return;
        }

        sum++;
        int orgSum= new Double(Math.random()*MAX_SUB_COUNT).intValue();
        for(int i=1;i<orgSum;i++){
            AdminSmOrgEntity child=new AdminSmOrgEntity();
            child.setOrgCode(parent.getOrgCode()+i);
            child.setOrgName("机构"+child.getOrgCode());
            System.out.println("----------------------------------------------------------------"+child.getOrgName());
            child.setUpOrgId(parent.getOrgId());
            child.setOrgLevel(parent.getOrgLevel()+1);
            child.setInstuId("ecd20f1d26b2449fb763b73afa219d96");
            child.setLastChgDt(new Date());
            child.setLastChgUsr("40");
            child.setOrgSts(Math.random()*10>2?AvailableStateEnum.ENABLED :AvailableStateEnum.DISABLED);
            adminSmOrgDao.insert(child);
            sum++;
            LambdaQueryWrapper<AdminSmOrgEntity> wrapper=new QueryWrapper<AdminSmOrgEntity>().lambda();
            wrapper.eq(AdminSmOrgEntity::getOrgCode,child.getOrgCode());
            child=adminSmOrgDao.selectList(wrapper).get(0);
            creatTreeFromAroot(child);
        }
    }
}
