/**
 * Copyright (C), 2014-2021
 * FileName: AdminSmOrgControllerTest
 * Author: zhui
 * Date: 2021/3/4 15:19
 * Description:
 * History:
 * <author> <time> <version> <desc>
 * Administrator 15:19 1.0.0 新建类
 */

package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/*import cn.com.yusys.yusp.flow.service.OcaServiceProvider;
import cn.com.yusys.yusp.flow.service.impl.WfOrgImpl;
import cn.com.yusys.yusp.flow.domain.NWfFlow;
import cn.com.yusys.yusp.flow.service.NWfFlowService;*/
import cn.com.yusys.yusp.oca.domain.form.AdminSmMenuConditionForm;
import cn.com.yusys.yusp.oca.domain.query.AdminSmOrgExtQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmMenuVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmOrgVo;
import cn.com.yusys.yusp.oca.service.AdminSmMenuService;
import cn.com.yusys.yusp.oca.service.AdminSmOrgService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

//@RunWith(SpringRunner.class) junit4提供，spring-boot-starter-test
//@ContextConfiguration 加载spring配置文件 （springboot项目不需要，会自动加载）
//@ExtendWith(SpringExtension.class) junit5使用该注解替代@RunWith
//@WebMvcTest 只启动mvc层，不涉及依赖注入时可用该注解替代SpringBootTest注解
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT) 指定虚拟端口或者真实端口
@Import({ FeignAutoConfiguration.class, HttpMessageConvertersAutoConfiguration.class })
@EnableFeignClients(clients = AdminSmOrgControllerTest.OcaServiceFeignClient.class)
@SpringBootTest
public class AdminSmOrgControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private AdminSmOrgService adminSmOrgService;
   /* @Autowired
    private WfOrgImpl wfOrgImpl;*/
    @Autowired
    private AdminSmMenuService adminSmMenuService;
    /*@Autowired
    private NWfFlowService nWfFlowService;*/

    @FeignClient(name = "yusp-app-oca-test", url = "http://127.0.0.1:30006",qualifier="OcaServiceFeignProvider",path = "/api/wf")
    public interface OcaServiceFeignClient /*extends OcaServiceProvider */{
    }


    AdminSmOrgExtQuery query=null;

    @BeforeClass
    public void init(){
        query = new AdminSmOrgExtQuery();
    }

    @Test
    public void testQueryPage() {
        query.setKeyWord("11");
        query.setOrgCode("22");
        Page<AdminSmOrgVo> adminSmOrgVoPage = adminSmOrgService.queryPage(query);
        Assert.assertNotNull(adminSmOrgVoPage);
        //Assert.assertTrue(adminSmOrgVoPage.getTotal()>0);
    }

    @Test
    public void testwfuser(){
        QueryModel queryModel = new QueryModel();
        queryModel.setCondition("{\"userId\":\"admin\",\"userName\":\"admin\"}");
        //wfOrgImpl.getUsers(queryModel);
        //wfOrgImpl.getLowerOrgId("100");
    }

    @Test
    public void testpage(){
        AdminSmMenuConditionForm adminSmMenuConditionForm = new AdminSmMenuConditionForm();
        adminSmMenuConditionForm.setPage(1);
        adminSmMenuConditionForm.setSize(5);
        adminSmMenuConditionForm.setKeyWord("系统");
        Page<AdminSmMenuVo> menuListForContr = adminSmMenuService.getMenuListForContr(adminSmMenuConditionForm);
        System.out.println("success");
    }

    @Test
    public void testTkmapperPage(){
        QueryModel queryModel = new QueryModel();
        queryModel.setPage(1);
        queryModel.setSize(2);
        //List<NWfFlow> nWfFlows = nWfFlowService.selectByModel(queryModel);
        System.out.println("Success");
    }
}
