/**
 * Copyright (C), 2014-2021
 * FileName: DemoTest2
 * Author: Administrator
 * Date: 2021/3/8 14:41
 * Description:
 * History:
 * <author> <time> <version> <desc>
 * Administrator 14:41 1.0.0 新建类
 */

package testng;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * 〈TestNG 特殊用法〉
 * @author Administrator
 * @create 2021/3/8
 * @since 1.0.0
 */

@Test(groups = {"demo2"})
public class DemoTest2 {
    private static final Logger log = LoggerFactory.getLogger(DemoTest2.class);


    /**
     * 该测试类用来演示@Test的特殊用法，支持哪些参数配置，包括超时测试、依赖测试、异常测试、执行优先级测试、执行次数和线程控制 以及动态传参
     *
     * 单独执行该测试类时可先将test3方法中的dependsOnGroups = {"demo"}注释掉，套件测试时可放开查看效果
     *
     * 〈test注解基本特性参数说明：〉
     * timeOut 超时时间 单位毫秒
     * invocationCount 运行次数
     * threadPoolSize 线程数
     * dataProvider 指定参数提供者 对应@dataProvider的name名
     * priority 优先级 越小优先级越大
     * expectedExceptions 预期的异常响应
     * enabled 是否启用该测试案例
     * groups 测试组
     * dependsOnMethods 指定依赖的测试方法
     * dependsOnGroups 指定依赖的测试组
     *
    */

    // 超时测试
    @Test(timeOut = 5000,invocationCount = 2,threadPoolSize = 2,dataProvider = "providerDemo",priority = 1) //毫秒
    public void test1(String key,int time) throws InterruptedException {
        log.info("DemoTest2 run test1 begin"+time);
        log.info(DemoTest.CONSTANT);
        //Thread.sleep(5010);
        Thread.sleep(time);
        log.info("DemoTest2 run test1 success");
    }

    //异常测试
    //@Test(expectedExceptions = NullPointerException.class, enabled = false)
    @Test(expectedExceptions = NullPointerException.class,dataProvider = "providerDemo",priority = 0)
    public void test2(Map<String,Integer> map){
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            int length = entry.getKey().length();
            int i = 1/entry.getValue();
            log.info("DemoTest2 sout exception");
        }
    }

    //@Test(expectedExceptions = NullPointerException.class)
    @Test(expectedExceptions = NullPointerException.class,dependsOnMethods = {"test1"},dependsOnGroups = {"demo"},priority = 2)
    public void test3(){
        log.info("DemoTest2 run test3");
        String a=null;
        log.info("DemoTest2 sout exception"+a.indexOf(3));
        //throw new NullPointerException();
    }

    @DataProvider(name = "providerDemo")
    public Object[][] providerMap(Method method, ITestContext context) {
        Object[][] obj = null;
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        //map.put(null, 1);
        map.put("key0", 0);
        map.put("key1", 1);

        //不同测试方法，返回不同参数值
        if(method.getName().equals("test2")) {
            obj = new Object[][]{{map}};
        }else if (method.getName().equals("test1")){
            obj = new Object[][]{{"time1",4000},{"time2",6000}};
        }

        //可以通过上下文获取测试属性
        for (String includedGroup : context.getIncludedGroups()) {
            log.info("includedGroup"+includedGroup);
        }
        return obj;
    }

}
