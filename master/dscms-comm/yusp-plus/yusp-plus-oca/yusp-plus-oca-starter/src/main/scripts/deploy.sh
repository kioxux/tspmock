#!/bin/bash
SERVICE_NAME=yusp-plus-oca-starter
SERVICE_VERSION=2.4.3-SNAPSHOT
unzip -o $HOME/app/SERVICE_NAME-SERVICE_VERSION.zip -d $HOME/app/$SERVICE_NAME
cd $HOME/app/$SERVICE_NAME/scripts
./shutdown.sh
./startup.sh

