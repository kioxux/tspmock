package cn.com.yusys.yusp.oca;

import cn.com.yusys.yusp.common.annotation.EnableMetrics;
import cn.com.yusys.yusp.message.annotation.EnableMessageService;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * OCA启动类
 *
 * @author Administrator
 */
@EnableMetrics
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"cn.com.yusys.yusp.oca", "cn.com.yusys.yusp.notice", "cn.com.yusys.yusp.sequence",
        "cn.com.yusys.yusp.common.exception", "cn.com.yusys.yusp.common.config", "cn.com.yusys.yusp.common.i18n",
        "cn.com.yusys.yusp.flow", "cn.com.yusys.yusp.utrace","cn.com.yusys.yusp.workFlow",
        "cn.com.yusys.yusp.web.server", "cn.com.yusys.yusp.service.server", "cn.com.yusys.yusp.dscms", "cn.com.yusys.yusp.message", "cn.com.yusys.yusp.service"})
@MapperScan(basePackages = {"cn.com.yusys.yusp.oca.dao", "cn.com.yusys.yusp.notice.dao", "cn.com.yusys.yusp.sequence.dao","cn.com.yusys.yusp.dscms.repository.mapper", "cn.com.yusys.yusp.flow.repository.mapper", "cn.com.yusys.yusp.utrace.dao","cn.com.yusys.yusp.message.dao"})
@EnableAsync(proxyTargetClass = true)
@EnableSwagger2
@EnableFeignClients("cn.com.yusys.yusp")
@EnableMessageService
public class YuspPlusOcaStarterMicroserviceApp {

    public static void main(String[] args) {
        SpringApplication.run(YuspPlusOcaStarterMicroserviceApp.class, args);
    }

}
