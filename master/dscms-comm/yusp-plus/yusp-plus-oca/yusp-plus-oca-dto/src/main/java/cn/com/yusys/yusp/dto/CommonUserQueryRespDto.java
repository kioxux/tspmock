package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * 通用客户经理查询
 *
 * @author:xuchao
 */
public class CommonUserQueryRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    //客户经理工号
    private String loginCode;
    //客户经理名称
    private String userName;
    //客户经理联系方式
    private String phone;
    //客户经理所在机构号
    private String orgId;
    //客户经理所在机构名称
    private String orgName;
    //客户经理角色码
    private String roleCode;
    //客户经理角色名称
    private String roleName;
    //客户经理状态
    private String userSts;

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getUserSts() {
        return userSts;
    }

    public void setUserSts(String userSts) {
        this.userSts = userSts;
    }
}
