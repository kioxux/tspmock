package cn.com.yusys.yusp.dto;

/**
 * 系统参数查询对象
 *
 * @author yangzai
 * @since 2021-06-21
 */
public class AdminSmPropDto {

    /**
     * 属性名
     */
    private String propName;
    /**
     * 属性描述
     */
    private String propDesc;
    /**
     * 属性值
     */
    private String propValue;
    /**
     * 备注
     */
    private String propRemark;

    public String getPropName() {
        return propName;
    }

    public void setPropName(String propName) {
        this.propName = propName;
    }

    public String getPropDesc() {
        return propDesc;
    }

    public void setPropDesc(String propDesc) {
        this.propDesc = propDesc;
    }

    public String getPropValue() {
        return propValue;
    }

    public void setPropValue(String propValue) {
        this.propValue = propValue;
    }

    public String getPropRemark() {
        return propRemark;
    }

    public void setPropRemark(String propRemark) {
        this.propRemark = propRemark;
    }
}