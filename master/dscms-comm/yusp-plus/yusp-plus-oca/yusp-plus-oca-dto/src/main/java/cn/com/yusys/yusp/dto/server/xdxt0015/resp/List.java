package cn.com.yusys.yusp.dto.server.xdxt0015.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/24 19:39:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/24 19:39
 * @since 2021/5/24 19:39
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "actorNo")
    private String actorNo;//客户经理号
    @JsonProperty(value = "actorName")
    private String actorName;//客户经理名
    @JsonProperty(value = "orgId")
    private String orgId;//机构号
    @JsonProperty(value = "xfOrgId")
    private String xfOrgId;//小贷机构号
    @JsonProperty(value = "telNum")
    private String telNum;//手机号
    @JsonProperty(value = "roleNo")
    private String roleNo;//角色编号
    @JsonProperty(value = "roleName")
    private String roleName;//角色名
    @JsonProperty(value = "organShortForm")
    private String organShortForm;//小贷机构名

    public String getActorNo() {
        return actorNo;
    }

    public void setActorNo(String actorNo) {
        this.actorNo = actorNo;
    }

    public String getActorName() {
        return actorName;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getXfOrgId() {
        return xfOrgId;
    }

    public void setXfOrgId(String xfOrgId) {
        this.xfOrgId = xfOrgId;
    }

    public String getTelNum() {
        return telNum;
    }

    public void setTelNum(String telNum) {
        this.telNum = telNum;
    }

    public String getRoleNo() {
        return roleNo;
    }

    public void setRoleNo(String roleNo) {
        this.roleNo = roleNo;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getOrganShortForm() {
        return organShortForm;
    }

    public void setOrganShortForm(String organShortForm) {
        this.organShortForm = organShortForm;
    }

    @Override
    public String toString() {
        return "List{" +
                "actorNo='" + actorNo + '\'' +
                ", actorName='" + actorName + '\'' +
                ", orgId='" + orgId + '\'' +
                ", xfOrgId='" + xfOrgId + '\'' +
                ", telNum='" + telNum + '\'' +
                ", roleNo='" + roleNo + '\'' +
                ", roleName='" + roleName + '\'' +
                ", organShortForm='" + organShortForm + '\'' +
                '}';
    }
}
