package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * 用角色去查用户列表
 */
public class GetUserInfoByDutyCodeDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private String dutyCode;

    private int pageNum;

    private int pageSize;

    public void setDutyCode(String dutyCode) {
        this.dutyCode = dutyCode;
    }

    public String getDutyCode() {
        return dutyCode;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}