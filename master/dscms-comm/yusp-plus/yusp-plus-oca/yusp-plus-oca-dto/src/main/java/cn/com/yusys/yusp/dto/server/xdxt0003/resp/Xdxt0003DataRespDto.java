package cn.com.yusys.yusp.dto.server.xdxt0003.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：分页查询小微客户经理
 *
 * @author chenyong
 * * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxt0003DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "num")
    private String num;//总数
    @JsonProperty(value = "managerList")
    private java.util.List<List> list;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Xdxt0003DataRespDto{" +
                "num='" + num + '\'' +
                ", list=" + list +
                '}';
    }
}
