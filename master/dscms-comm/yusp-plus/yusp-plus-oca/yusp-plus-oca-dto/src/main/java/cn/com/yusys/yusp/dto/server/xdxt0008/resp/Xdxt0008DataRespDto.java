package cn.com.yusys.yusp.dto.server.xdxt0008.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据客户经理号查询账务机构号
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxt0008DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "orgNo")
    private String orgNo;//账户机构号

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    @Override
    public String toString() {
        return "Xdxt0008DataRespDto{" +
                "orgNo='" + orgNo + '\'' +
                '}';
    }
}
