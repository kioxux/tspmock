package cn.com.yusys.yusp.dto;

import java.io.Serializable;

public class UserAndOrgInfoRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    // 机构号
    private String orgNo;
    // 机构名称
    private String orgName;
    // 客户经理号
    private String managerId;
    // 客户经理名称
    private String managerName;

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }
}