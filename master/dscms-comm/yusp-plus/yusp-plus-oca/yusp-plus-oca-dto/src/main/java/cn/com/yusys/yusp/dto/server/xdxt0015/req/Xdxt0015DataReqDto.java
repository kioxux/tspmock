package cn.com.yusys.yusp.dto.server.xdxt0015.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：用户机构角色信息列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxt0015DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "actorNo")
    private String actorNo;//客户经理号
    @JsonProperty(value = "actorName")
    private String actorName;//客户经理名
    @JsonProperty(value = "orgId")
    private String orgId;//机构号

    public String getActorNo() {
        return actorNo;
    }

    public void setActorNo(String actorNo) {
        this.actorNo = actorNo;
    }

    public String getActorName() {
        return actorName;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    @Override
    public String toString() {
        return "Xdxt0015DataReqDto{" +
                "actorNo='" + actorNo + '\'' +
                ", actorName='" + actorName + '\'' +
                ", orgId='" + orgId + '\'' +
                '}';
    }
}
