package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * 用角色去查用户列表
 */
public class GetUserInfoByRoleCodeDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private String roleCode;

    private int pageNum;

    private int pageSize;

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
