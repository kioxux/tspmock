package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.List;

public class UserAndOrgInfoReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    // 机构号列表
    private List<String> orgIds;
    // 页码
    private Integer pageNum;
    // 条数
    private Integer pageSize;

    public List<String> getOrgIds() {
        return orgIds;
    }

    public void setOrgIds(List<String> orgIds) {
        this.orgIds = orgIds;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
