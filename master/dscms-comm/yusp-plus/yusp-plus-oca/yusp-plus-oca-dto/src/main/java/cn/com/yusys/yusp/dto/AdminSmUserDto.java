package cn.com.yusys.yusp.dto;

import java.util.Date;


/**
 * @version 1.0.0
 * @项目名称: yusp-plus-oca模块
 * @类名称: AdminSmUser
 * @类描述: AdminSmUser用户表数据实体类
 * @功能描述:
 * @创建人: ZSM
 * @创建时间: 2021-05-13 14:55:40
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class AdminSmUserDto {

    /**
     * 账号
     */
    private String loginCode;
    /**
     * 姓名
     */
    private String userName;
    /**
     * 员工号
     */
    private String userCode;
    /**
     * 证件类型
     */
    private String certType;
    /**
     * 证件号码
     */
    private String certNo;

    /**
     * 有效期到
     */
    private Date deadline;

    /**
     * 所属机构编号
     */
    private String orgId;

    /**
     * 所属部门编号
     */
    private String dptId;

    /**
     * 性别
     */
    private String userSex;

    /**
     * 生日
     */
    private Date userBirthday;

    /**
     * 邮箱
     */
    private String userEmail;

    /**
     * 移动电话
     */
    private String userMobilephone;

    /**
     * 办公电话
     */
    private String userOfficetel;

    /**
     * 学历
     */
    private String userEducation;
    /**
     * 资格证书
     */
    private String userCertificate;

    /**
     * 入职日期
     */
    private Date entrantsDate;

    /**
     * 任职时间
     */
    private Date positionTime;

    /**
     * 从业时间
     */
    private Date financialJobTime;

    /**
     * 职级
     */
    private String positionDegree;

    /**
     * 用户头像
     */
    private String userAvatar;

    /**
     * 常用IP，逗号分隔
     */
    private String offenIp;

    /**
     * 状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效 F：冻结
     */
    private String userSts;

    /**
     * 是否信贷人员标识 Y:是 N:否
     *
     * @return
     */
    private String isCredit;

    public String getIsCredit() {
        return isCredit;
    }

    public void setIsCredit(String isCredit) {
        this.isCredit = isCredit;
    }

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getDptId() {
        return dptId;
    }

    public void setDptId(String dptId) {
        this.dptId = dptId;
    }

    public String getUserSex() {
        return userSex;
    }

    public void setUserSex(String userSex) {
        this.userSex = userSex;
    }

    public Date getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(Date userBirthday) {
        this.userBirthday = userBirthday;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserMobilephone() {
        return userMobilephone;
    }

    public void setUserMobilephone(String userMobilephone) {
        this.userMobilephone = userMobilephone;
    }

    public String getUserOfficetel() {
        return userOfficetel;
    }

    public void setUserOfficetel(String userOfficetel) {
        this.userOfficetel = userOfficetel;
    }

    public String getUserEducation() {
        return userEducation;
    }

    public void setUserEducation(String userEducation) {
        this.userEducation = userEducation;
    }

    public String getUserCertificate() {
        return userCertificate;
    }

    public void setUserCertificate(String userCertificate) {
        this.userCertificate = userCertificate;
    }

    public Date getEntrantsDate() {
        return entrantsDate;
    }

    public void setEntrantsDate(Date entrantsDate) {
        this.entrantsDate = entrantsDate;
    }

    public Date getPositionTime() {
        return positionTime;
    }

    public void setPositionTime(Date positionTime) {
        this.positionTime = positionTime;
    }

    public Date getFinancialJobTime() {
        return financialJobTime;
    }

    public void setFinancialJobTime(Date financialJobTime) {
        this.financialJobTime = financialJobTime;
    }

    public String getPositionDegree() {
        return positionDegree;
    }

    public void setPositionDegree(String positionDegree) {
        this.positionDegree = positionDegree;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getOffenIp() {
        return offenIp;
    }

    public void setOffenIp(String offenIp) {
        this.offenIp = offenIp;
    }

    public String getUserSts() {
        return userSts;
    }

    public void setUserSts(String userSts) {
        this.userSts = userSts;
    }
}