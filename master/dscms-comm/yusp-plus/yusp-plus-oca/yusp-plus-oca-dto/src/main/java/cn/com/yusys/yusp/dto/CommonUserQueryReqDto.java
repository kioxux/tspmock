package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * 通用客户经理查询
 *
 * @author:xuchao
 */
public class CommonUserQueryReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    // 客户经理编号
    private String loginCode;
    // 客户经理名称
    private String userName;
    // 客户经理证件号
    private String certNo;

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }
}
