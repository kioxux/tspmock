package cn.com.yusys.yusp.dto;

import java.util.Date;


/**
 * @version 1.0.0
 * @项目名称: yusp-plus-oca模块
 * @类名称: AdminSmOrg
 * @类描述: AdminSmOrg机构表数据实体类
 * @功能描述:
 * @创建人: ZSM
 * @创建时间: 2021-05-13 14:55:40
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class AdminSmOrgDto {

    /**
     * 记录编号
     */
    private String orgId;

    /**
     * 金融机构编号
     */
    private String instuId;

    /**
     * 机构代码
     */
    private String orgCode;

    /**
     * 机构名称
     */
    private String orgName;

    /**
     * 上级机构记录编号
     */
    private String upOrgId;

    /**
     * 机构层级
     */
    private Integer orgLevel;

    /**
     * 机构类别
     */
    private String orgType;

    /**
     * 机构层级索引
     */
    private String orgSeq;

    /**
     * 地址
     */
    private String orgAddr;

    /**
     * 邮编
     */
    private String zipCde;

    /**
     * 联系电话
     */
    private String contTel;

    /**
     * 联系人
     */
    private String contUsr;

    /**
     * 状态：对应字典项=NORM_STS A：启用 I：停用 W：待启用
     */
    private String orgSts;

    /**
     * 最新变更用户
     */
    private String lastChgUsr;

    /**
     * 最新变更时间
     */
    private Date lastChgDt;

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getInstuId() {
        return instuId;
    }

    public void setInstuId(String instuId) {
        this.instuId = instuId;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getUpOrgId() {
        return upOrgId;
    }

    public void setUpOrgId(String upOrgId) {
        this.upOrgId = upOrgId;
    }

    public Integer getOrgLevel() {
        return orgLevel;
    }

    public void setOrgLevel(Integer orgLevel) {
        this.orgLevel = orgLevel;
    }

    public String getOrgAddr() {
        return orgAddr;
    }

    public void setOrgAddr(String orgAddr) {
        this.orgAddr = orgAddr;
    }

    public String getZipCde() {
        return zipCde;
    }

    public void setZipCde(String zipCde) {
        this.zipCde = zipCde;
    }

    public String getContTel() {
        return contTel;
    }

    public void setContTel(String contTel) {
        this.contTel = contTel;
    }

    public String getContUsr() {
        return contUsr;
    }

    public void setContUsr(String contUsr) {
        this.contUsr = contUsr;
    }

    public String getOrgSts() {
        return orgSts;
    }

    public void setOrgSts(String orgSts) {
        this.orgSts = orgSts;
    }

    public String getLastChgUsr() {
        return lastChgUsr;
    }

    public void setLastChgUsr(String lastChgUsr) {
        this.lastChgUsr = lastChgUsr;
    }

    public Date getLastChgDt() {
        return lastChgDt;
    }

    public void setLastChgDt(Date lastChgDt) {
        this.lastChgDt = lastChgDt;
    }

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    public String getOrgSeq() {
        return orgSeq;
    }

    public void setOrgSeq(String orgSeq) {
        this.orgSeq = orgSeq;
    }
}