package cn.com.yusys.yusp.dto;

/**
 * @version 1.0.0
 * @项目名称: yusp-plus-oca模块
 * @类名称: GetIsXwUserDto
 * @类描述: 是否小微客户经理
 * @功能描述:
 * @创建人: ZSM
 * @创建时间: 2021-05-13 14:55:40
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class GetIsXwUserDto {
    /**
     * 是否小微客户经理 Y：是，N:否
     */
    private String isXWUser;

    public String getIsXWUser() {
        return isXWUser;
    }

    public void setIsXWUser(String isXWUser) {
        this.isXWUser = isXWUser;
    }
}
