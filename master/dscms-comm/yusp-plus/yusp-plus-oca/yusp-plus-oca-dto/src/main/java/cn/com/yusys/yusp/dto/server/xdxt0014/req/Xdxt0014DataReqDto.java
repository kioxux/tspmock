package cn.com.yusys.yusp.dto.server.xdxt0014.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：字典项对象通用列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxt0014DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "opttype")
    private String opttype;//字典类型
    @JsonProperty(value = "enname")
    private String enname;//字典项名称

    public String getOpttype() {
        return opttype;
    }

    public void setOpttype(String opttype) {
        this.opttype = opttype;
    }

    public String getEnname() {
        return enname;
    }

    public void setEnname(String enname) {
        this.enname = enname;
    }

    @Override
    public String toString() {
        return "Xdxt0014DataReqDto{" +
                "opttype='" + opttype + '\'' +
                ", enname='" + enname + '\'' +
                '}';
    }
}
