package cn.com.yusys.yusp.dto;

import javax.validation.constraints.NotEmpty;

/**
 * 系统参数查询对象
 *
 * @author yangzai
 * @since 2021-06-21
 */
public class AdminSmPropQueryDto {

    @NotEmpty
    private String propName;

    public String getPropName() {
        return propName;
    }

    public void setPropName(String propName) {
        this.propName = propName;
    }

}