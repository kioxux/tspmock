package cn.com.yusys.yusp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 查询用户和岗位响应
 *
 * @author :xuchao
 */
@JsonPropertyOrder(alphabetic = true)
public class UserAndDutyRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    // 客户经理姓名
    @JsonProperty(value = "actorName")
    private String actorName;
    // 岗位编号
    @JsonProperty(value = "dutyNo")
    private String dutyNo;
    // 客户经理工号
    @JsonProperty(value = "actorNo")
    private String actorNo;
    // 岗位名称
    @JsonProperty(value = "dutyName")
    private String dutyName;
    // 手机号
    @JsonProperty(value = "phone")
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getActorName() {
        return actorName;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    public String getDutyNo() {
        return dutyNo;
    }

    public void setDutyNo(String dutyNo) {
        this.dutyNo = dutyNo;
    }

    public String getActorNo() {
        return actorNo;
    }

    public void setActorNo(String actorNo) {
        this.actorNo = actorNo;
    }

    public String getDutyName() {
        return dutyName;
    }

    public void setDutyName(String dutyName) {
        this.dutyName = dutyName;
    }

    @Override
    public String toString() {
        return "UserAndDutyRespDto{" +
                "actorName='" + actorName + '\'' +
                ", dutyNo='" + dutyNo + '\'' +
                ", actorNo='" + actorNo + '\'' +
                ", dutyName='" + dutyName + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}