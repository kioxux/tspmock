package cn.com.yusys.yusp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 查询用户和岗位请求Dto
 *
 * @author :xuchao
 */
@JsonPropertyOrder(alphabetic = true)
public class UserAndDutyReqDto implements Serializable {

    private static final long serialVersionUID = 1L;
    // 客户经理工号
    @JsonProperty(value = "managerId")
    private String managerId;

    // 岗位编号
    @JsonProperty(value = "dutyNo")
    private String dutyNo;

    // 岗位名称
    @JsonProperty(value = "dutyName")
    private String dutyName;

    //机构
    @JsonProperty(value = "ORG_ID")
    private String orgId;

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getDutyNo() {
        return dutyNo;
    }

    public void setDutyNo(String dutyNo) {
        this.dutyNo = dutyNo;
    }

    public String getDutyName() {
        return dutyName;
    }

    public void setDutyName(String dutyName) {
        this.dutyName = dutyName;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }


    @Override
    public String toString() {
        return "UserAndDutyReqDto{" +
                "managerId='" + managerId + '\'' +
                ", dutyNo='" + dutyNo + '\'' +
                ", dutyName='" + dutyName + '\'' +
                '}';
    }
}