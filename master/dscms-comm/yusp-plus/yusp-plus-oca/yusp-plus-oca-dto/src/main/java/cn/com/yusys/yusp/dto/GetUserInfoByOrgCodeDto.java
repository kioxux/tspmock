package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * 用机构去查用户列表
 */
public class GetUserInfoByOrgCodeDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private String orgCode;

    private int pageNum;

    private int pageSize;
    
    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}