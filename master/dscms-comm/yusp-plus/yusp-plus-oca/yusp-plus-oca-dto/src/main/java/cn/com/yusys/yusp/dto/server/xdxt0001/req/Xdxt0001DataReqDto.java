package cn.com.yusys.yusp.dto.server.xdxt0001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：信贷同步人力资源
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxt0001DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "syncSysCode")
    private String syncSysCode;//同步系统编码
    @JsonProperty(value = "workNo")
    private String workNo;//工号
    @JsonProperty(value = "name")
    private String name;//姓名
    @JsonProperty(value = "personDeptNo")
    private String personDeptNo;//人力编制部门编号
    @JsonProperty(value = "personDeptName")
    private String personDeptName;//人力编制部门名称
    @JsonProperty(value = "personWorkDeptNo")
    private String personWorkDeptNo;//人力工作部门编号
    @JsonProperty(value = "personWorkDeptName")
    private String personWorkDeptName;//人力工作部门名称
    @JsonProperty(value = "coreDeptNo")
    private String coreDeptNo;//核心编制部门编号
    @JsonProperty(value = "coreDeptName")
    private String coreDeptName;//核心编制部门名称
    @JsonProperty(value = "coreWorkDeptNo")
    private String coreWorkDeptNo;//核心工作部门编号
    @JsonProperty(value = "coreWorkDeptName")
    private String coreWorkDeptName;//核心工作部门名称
    @JsonProperty(value = "dutyNo")
    private String dutyNo;//岗位编号
    @JsonProperty(value = "dutyName")
    private String dutyName;//岗位名称
    @JsonProperty(value = "memDutyNo")
    private String memDutyNo;//职务编号
    @JsonProperty(value = "memDutyName")
    private String memDutyName;//职务名称
    @JsonProperty(value = "employeeStatusNo")
    private String employeeStatusNo;//员工状态编号
    @JsonProperty(value = "workStatusNo")
    private String workStatusNo;//工作状态编号
    @JsonProperty(value = "employeeChaNo")
    private String employeeChaNo;//员工性质编号
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certNo")
    private String certNo;//营业执照号码
    @JsonProperty(value = "mobile")
    private String mobile;//手机号
    @JsonProperty(value = "wechat")
    private String wechat;//微信号
    @JsonProperty(value = "email")
    private String email;//邮箱
    @JsonProperty(value = "sexNo")
    private String sexNo;//性别编号
    @JsonProperty(value = "birday")
    private String birday;//出生年月
    @JsonProperty(value = "age")
    private String age;//年龄
    @JsonProperty(value = "politicalStatusNo")
    private String politicalStatusNo;//政治面貌编号
    @JsonProperty(value = "eduNo")
    private String eduNo;//学历编号
    @JsonProperty(value = "titleNo")
    private String titleNo;//职称编号
    @JsonProperty(value = "cprtStartDate")
    private String cprtStartDate;//参加工作时间
    @JsonProperty(value = "indivWorkJobY")
    private String indivWorkJobY;//人行时间
    @JsonProperty(value = "cprtAge")
    private String cprtAge;//工龄
    @JsonProperty(value = "bankCprtAge")
    private String bankCprtAge;//行龄
    @JsonProperty(value = "privNo")
    private String privNo;//上柜权限编号
    @JsonProperty(value = "saleLiceEndDate")
    private String saleLiceEndDate;//营销证书到期日
    @JsonProperty(value = "opposeFakeLiceEndDate")
    private String opposeFakeLiceEndDate;//反假证书到期日
    @JsonProperty(value = "isCretPerson")
    private String isCretPerson;//是否信贷人员标识字段

    public String getSyncSysCode() {
        return syncSysCode;
    }

    public void setSyncSysCode(String syncSysCode) {
        this.syncSysCode = syncSysCode;
    }

    public String getWorkNo() {
        return workNo;
    }

    public void setWorkNo(String workNo) {
        this.workNo = workNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPersonDeptNo() {
        return personDeptNo;
    }

    public void setPersonDeptNo(String personDeptNo) {
        this.personDeptNo = personDeptNo;
    }

    public String getPersonDeptName() {
        return personDeptName;
    }

    public void setPersonDeptName(String personDeptName) {
        this.personDeptName = personDeptName;
    }

    public String getPersonWorkDeptNo() {
        return personWorkDeptNo;
    }

    public void setPersonWorkDeptNo(String personWorkDeptNo) {
        this.personWorkDeptNo = personWorkDeptNo;
    }

    public String getPersonWorkDeptName() {
        return personWorkDeptName;
    }

    public void setPersonWorkDeptName(String personWorkDeptName) {
        this.personWorkDeptName = personWorkDeptName;
    }

    public String getCoreDeptNo() {
        return coreDeptNo;
    }

    public void setCoreDeptNo(String coreDeptNo) {
        this.coreDeptNo = coreDeptNo;
    }

    public String getCoreDeptName() {
        return coreDeptName;
    }

    public void setCoreDeptName(String coreDeptName) {
        this.coreDeptName = coreDeptName;
    }

    public String getCoreWorkDeptNo() {
        return coreWorkDeptNo;
    }

    public void setCoreWorkDeptNo(String coreWorkDeptNo) {
        this.coreWorkDeptNo = coreWorkDeptNo;
    }

    public String getCoreWorkDeptName() {
        return coreWorkDeptName;
    }

    public void setCoreWorkDeptName(String coreWorkDeptName) {
        this.coreWorkDeptName = coreWorkDeptName;
    }

    public String getDutyNo() {
        return dutyNo;
    }

    public void setDutyNo(String dutyNo) {
        this.dutyNo = dutyNo;
    }

    public String getDutyName() {
        return dutyName;
    }

    public void setDutyName(String dutyName) {
        this.dutyName = dutyName;
    }

    public String getMemDutyNo() {
        return memDutyNo;
    }

    public void setMemDutyNo(String memDutyNo) {
        this.memDutyNo = memDutyNo;
    }

    public String getMemDutyName() {
        return memDutyName;
    }

    public void setMemDutyName(String memDutyName) {
        this.memDutyName = memDutyName;
    }

    public String getEmployeeStatusNo() {
        return employeeStatusNo;
    }

    public void setEmployeeStatusNo(String employeeStatusNo) {
        this.employeeStatusNo = employeeStatusNo;
    }

    public String getWorkStatusNo() {
        return workStatusNo;
    }

    public void setWorkStatusNo(String workStatusNo) {
        this.workStatusNo = workStatusNo;
    }

    public String getEmployeeChaNo() {
        return employeeChaNo;
    }

    public void setEmployeeChaNo(String employeeChaNo) {
        this.employeeChaNo = employeeChaNo;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSexNo() {
        return sexNo;
    }

    public void setSexNo(String sexNo) {
        this.sexNo = sexNo;
    }

    public String getBirday() {
        return birday;
    }

    public void setBirday(String birday) {
        this.birday = birday;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPoliticalStatusNo() {
        return politicalStatusNo;
    }

    public void setPoliticalStatusNo(String politicalStatusNo) {
        this.politicalStatusNo = politicalStatusNo;
    }

    public String getEduNo() {
        return eduNo;
    }

    public void setEduNo(String eduNo) {
        this.eduNo = eduNo;
    }

    public String getTitleNo() {
        return titleNo;
    }

    public void setTitleNo(String titleNo) {
        this.titleNo = titleNo;
    }

    public String getCprtStartDate() {
        return cprtStartDate;
    }

    public void setCprtStartDate(String cprtStartDate) {
        this.cprtStartDate = cprtStartDate;
    }

    public String getIndivWorkJobY() {
        return indivWorkJobY;
    }

    public void setIndivWorkJobY(String indivWorkJobY) {
        this.indivWorkJobY = indivWorkJobY;
    }

    public String getCprtAge() {
        return cprtAge;
    }

    public void setCprtAge(String cprtAge) {
        this.cprtAge = cprtAge;
    }

    public String getBankCprtAge() {
        return bankCprtAge;
    }

    public void setBankCprtAge(String bankCprtAge) {
        this.bankCprtAge = bankCprtAge;
    }

    public String getPrivNo() {
        return privNo;
    }

    public void setPrivNo(String privNo) {
        this.privNo = privNo;
    }

    public String getSaleLiceEndDate() {
        return saleLiceEndDate;
    }

    public void setSaleLiceEndDate(String saleLiceEndDate) {
        this.saleLiceEndDate = saleLiceEndDate;
    }

    public String getOpposeFakeLiceEndDate() {
        return opposeFakeLiceEndDate;
    }

    public void setOpposeFakeLiceEndDate(String opposeFakeLiceEndDate) {
        this.opposeFakeLiceEndDate = opposeFakeLiceEndDate;
    }

    public String getIsCretPerson() {
        return isCretPerson;
    }

    public void setIsCretPerson(String isCretPerson) {
        this.isCretPerson = isCretPerson;
    }

    @Override
    public String toString() {
        return "Xdxt0001DataReqDto{" +
                "syncSysCode='" + syncSysCode + '\'' +
                ", workNo='" + workNo + '\'' +
                ", name='" + name + '\'' +
                ", personDeptNo='" + personDeptNo + '\'' +
                ", personDeptName='" + personDeptName + '\'' +
                ", personWorkDeptNo='" + personWorkDeptNo + '\'' +
                ", personWorkDeptName='" + personWorkDeptName + '\'' +
                ", coreDeptNo='" + coreDeptNo + '\'' +
                ", coreDeptName='" + coreDeptName + '\'' +
                ", coreWorkDeptNo='" + coreWorkDeptNo + '\'' +
                ", coreWorkDeptName='" + coreWorkDeptName + '\'' +
                ", dutyNo='" + dutyNo + '\'' +
                ", dutyName='" + dutyName + '\'' +
                ", memDutyNo='" + memDutyNo + '\'' +
                ", memDutyName='" + memDutyName + '\'' +
                ", employeeStatusNo='" + employeeStatusNo + '\'' +
                ", workStatusNo='" + workStatusNo + '\'' +
                ", employeeChaNo='" + employeeChaNo + '\'' +
                ", certType='" + certType + '\'' +
                ", certNo='" + certNo + '\'' +
                ", mobile='" + mobile + '\'' +
                ", wechat='" + wechat + '\'' +
                ", email='" + email + '\'' +
                ", sexNo='" + sexNo + '\'' +
                ", birday='" + birday + '\'' +
                ", age='" + age + '\'' +
                ", politicalStatusNo='" + politicalStatusNo + '\'' +
                ", eduNo='" + eduNo + '\'' +
                ", titleNo='" + titleNo + '\'' +
                ", cprtStartDate='" + cprtStartDate + '\'' +
                ", indivWorkJobY='" + indivWorkJobY + '\'' +
                ", cprtAge='" + cprtAge + '\'' +
                ", bankCprtAge='" + bankCprtAge + '\'' +
                ", privNo='" + privNo + '\'' +
                ", saleLiceEndDate='" + saleLiceEndDate + '\'' +
                ", opposeFakeLiceEndDate='" + opposeFakeLiceEndDate + '\'' +
                ", isCretPerson='" + isCretPerson + '\'' +
                '}';
    }
}
