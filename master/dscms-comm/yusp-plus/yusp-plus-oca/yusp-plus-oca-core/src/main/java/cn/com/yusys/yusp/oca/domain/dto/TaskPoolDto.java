package cn.com.yusys.yusp.oca.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *@program: yusp-plus
 *@description: 产品附加信息传参
 *@author: wujiangpeng
 *@email: wujp4@yusys.com.cn
 *@create: 2020-12-02 16:36
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskPoolDto {
    private String productId;
    @JsonProperty("taskPoolId")
    private String addValue;
    @JsonProperty("taskPoolName")
    private String addName;
}
