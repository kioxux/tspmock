package cn.com.yusys.yusp.oca.domain.vo;

//import com.sun.istack.internal.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *@program: yusp-plus
 *@description: 保存信息vo
 *@author: wujiangpeng
 *@email: wujp4@yusys.com.cn
 *@create: 2020-11-17 19:05
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminSmMessageReq {
    /**
     * 信息码
     */
//    @NotNull
    private String code;
    /**
     * 信息级别:success成功 info信息 warning警告 error错误
     */
//    @NotNull
    private String messageLevel;
    /**
     * 提示内容
     */
//    @NotNull
    private String message;
    /**
     * 消息类别：COMINFO系统级通用提示 DBERR数据库错误提示 MODULEINFO模块提示
     */
//    @NotNull
    private String messageType;
    /**
     * 所属模块名称
     */
    private String funcName;
}
