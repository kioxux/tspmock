package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.common.utils.RAMPager;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.ObjectUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.oca.dao.AdminSmDptDao;
import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmDptEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmDptQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmUserQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmDptVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmOrgVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmUserVo;
import cn.com.yusys.yusp.oca.service.AdminSmDptService;
import cn.com.yusys.yusp.oca.service.AdminSmOrgService;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/*
 * @author terry
 * @email tanrui1@yusys.com.cn
 */
@Service("adminSmDptService")
@Slf4j
public class AdminSmDptServiceImpl extends ServiceImpl<AdminSmDptDao, AdminSmDptEntity> implements AdminSmDptService {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    AdminSmOrgService adminSmOrgService;

    @Autowired
    AdminSmUserService adminSmUserService;

    /**
     * 分页查询
     *
     * @param query
     * @return
     */
    @Override
    public Page<AdminSmDptVo> queryPage(AdminSmDptQuery query) {
        Page<AdminSmDptVo> page = query.getIPage();
        QueryWrapper<AdminSmDptVo> queryWrapper = new QueryWrapper<AdminSmDptVo>()
                .like(!StringUtils.isEmpty(query.getDptCode()), "d.DPT_CODE", query.getDptCode())//部门编码模糊匹配
                .like(!StringUtils.isEmpty(query.getDptName()), "d.DPT_NAME", query.getDptName())//部门名称模糊匹配
                .eq(ObjectUtils.nonNull(query.getDptSts()), "d.DPT_STS", query.getDptSts());//部门状态精确匹配
        if (StringUtils.nonEmpty(query.getKeyWord())) {
            queryWrapper.and(wrapper -> wrapper
                    .like("d.DPT_CODE", query.getKeyWord())//关键字模糊匹配部门编码
                    .or()
                    .like("d.DPT_NAME", query.getKeyWord())//关键字模糊匹配部门名称
            );
        }
        queryWrapper.orderByDesc("d.LAST_CHG_DT");
        // 3. 查询所有的部门
        final List<AdminSmDptVo> dptVoList = this.baseMapper.selectAllDpt(queryWrapper);

        List<String> orgIds;
        if (StringUtils.isEmpty(query.getOrgId())) {
            orgIds = adminSmOrgService.getAllAccessibleOrgIds();//未指定orgId就查询当前用户有权访问的机构下的所有部门
        } else {
            orgIds = adminSmOrgService.getAllProgeny(query.getOrgId()).stream().map(AdminSmOrgVo::getOrgId).collect(Collectors.toList());//指定了orgId就查指定机构及其下级机构下的部门
        }
        // 3.2 遍历机构，从符合条件的所有部门里筛选出orgId在orgIds中的部门列表
        List<AdminSmDptVo> dptResultList = dptVoList.stream().filter(item -> orgIds.contains(item.getOrgId())).collect(Collectors.toList());
        List<AdminSmDptVo> sortedResultList = dptResultList.stream().sorted((node1, node2) -> DateUtils.compare(node2.getLastChgDt(), node1.getLastChgDt())).collect(Collectors.toList());//按日期降序
        //代码分页
        RAMPager<AdminSmDptVo> pager = new RAMPager<>(sortedResultList, query.getSize());
        page.setRecords(pager.page(query.getPage()));
        page.setTotal(sortedResultList.size());
        return page;
    }


    /**
     * 批量启用
     *
     * @param ids
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchEnable(String[] ids) {
        if (CollectionUtils.nonEmpty(ids)) {
            List<String> idList = Arrays.asList(ids);
            String userId = SessionUtils.getUserId();
            idList.forEach((id) -> {
                AdminSmDptEntity dptEntity = this.baseMapper.selectById(id);
                dptEntity.setLastChgUsr(userId);
                dptEntity.setDptSts(AvailableStateEnum.ENABLED);
                this.baseMapper.updateById(dptEntity);
            });
        }
    }

    /**
     * 批量停用
     *
     * @param ids
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchDisable(String[] ids) {
        if (CollectionUtils.nonEmpty(ids)) {
            List<String> idList = Arrays.asList(ids);
            String userId = SessionUtils.getUserId();
            idList.forEach((id) -> {
                if (checkBlocked(id)) {
                    throw BizException.error(null, "50100001", "该部门绑定有用户或存在生效的下级部门，请删除关联信息后操作");
                } else {
                    AdminSmDptEntity entity = this.baseMapper.selectById(id);
                    entity.setLastChgUsr(userId);
                    entity.setDptSts(AvailableStateEnum.DISABLED);
                    this.baseMapper.updateById(entity);
                }
            });
        }
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchDelete(String[] ids) {
        if (CollectionUtils.nonEmpty(ids)) {
            List<String> idList = Arrays.asList(ids);
            idList.forEach((id) -> {
                if (checkBlocked(id)) {
                    throw BizException.error(null, "50100001", "该部门绑定有用户或存在生效的下级部门，请删除关联信息后操作");
                } else {
                    this.baseMapper.deleteById(id);
                }
            });
        }
    }

    /**
     * 检查部门是否已关联其他信息
     *
     * @param dptId
     * @return
     */
    private boolean checkBlocked(String dptId) {
        LambdaQueryWrapper<AdminSmDptEntity> wrapper = new QueryWrapper<AdminSmDptEntity>().lambda();
        wrapper.eq(AdminSmDptEntity::getUpDptId, dptId).eq(AdminSmDptEntity::getDptSts, AvailableStateEnum.ENABLED);
        Integer countSon = this.baseMapper.selectCount(wrapper);//是否有生效的下级部门
        LambdaQueryWrapper<AdminSmUserEntity> userWrapper = new QueryWrapper<AdminSmUserEntity>().lambda();
        userWrapper.eq(AdminSmUserEntity::getDptId, dptId).eq(AdminSmUserEntity::getUserSts, AvailableStateEnum.ENABLED.getCode());
        int countUser = this.adminSmUserService.count(userWrapper);//是否有关联用户
        return countSon > 0 || countUser > 0;
    }

    @Override
    public boolean save(AdminSmDptEntity entity) {
        LambdaQueryWrapper<AdminSmDptEntity> codeWrapper = new QueryWrapper<AdminSmDptEntity>().lambda();
        codeWrapper.eq(AdminSmDptEntity::getDptCode, entity.getDptCode());
        AdminSmDptEntity check = this.baseMapper.selectOne(codeWrapper);
        if (Objects.nonNull(check)) {
            throw BizException.error(null, "50100002", entity.getDptCode());
        }
        AdminSmDptEntity parent = getById(entity.getUpDptId());
        if (ObjectUtils.nonNull(parent)) {
            entity.setOrgId(parent.getOrgId());
        }
        String userId = SessionUtils.getUserId();
        entity.setLastChgUsr(userId);
        entity.setDptSts(Optional.ofNullable(entity.getDptSts()).orElse(AvailableStateEnum.UNENABLED));//新增的数据默认是待启用的
        log.info("New department data: [new department: {}] ", entity.getDptName());
        return this.baseMapper.insert(entity) > 0;
    }

    @Override
    public Page<AdminSmUserVo> memberPage(AdminSmUserQuery query) {
        if (!StringUtils.isBlank(query.getDptId())) {
            return adminSmUserService.queryPage(query);
        }
        return null;
    }

    @Override
    public boolean updateById(AdminSmDptEntity entity) {
        if (AvailableStateEnum.DISABLED.equals(entity.getDptSts()) && checkBlocked(entity.getDptId())) {//改为停用时要判断是否关联其他信息
            throw BizException.error(null, "50100001", "该部门绑定有用户或存在生效的下级部门，请删除关联信息后操作");
        }
        String userId = SessionUtils.getUserId();
        entity.setLastChgUsr(userId);
        entity.setDptSts(Optional.ofNullable(entity.getDptSts()).orElse(AvailableStateEnum.UNENABLED));
        return super.updateById(entity);
    }


    @Override
    public PageUtils getDptByParam(String orgCode) {
        List<AdminSmDptEntity> dptList = this.list(new QueryWrapper<AdminSmDptEntity>().eq(!StringUtils.isEmpty(orgCode), "ORG_ID", orgCode));
        // 特别扯淡，用机构树的做法渲染部门树...无语
        List<AdminSmOrgVo> collect = dptList.stream().map(adminSmDptEntity -> {
            AdminSmOrgVo orgExtVo = new AdminSmOrgVo();
            orgExtVo.setOrgCode(adminSmDptEntity.getDptCode());
            orgExtVo.setOrgName(adminSmDptEntity.getDptName());
            return orgExtVo;
        }).collect(Collectors.toList());

        // 不分页
        return new PageUtils(collect, collect.size(), 0, 0);
    }

    @Override
    public List<AdminSmDptEntity> dptTree(AdminSmDptQuery query) {
        /**
         * 这里可以根据需要传入条件，查询部分的部门
         * 这里只用了orgId和dptSts两个参数
         */
        query.setOrgId(StringUtils.isEmpty(query.getOrgId()) ? SessionUtils.getUserOrganizationId() : query.getOrgId());
        LambdaQueryWrapper<AdminSmDptEntity> queryWrapper = new QueryWrapper<AdminSmDptEntity>().lambda();
        queryWrapper.eq(AdminSmDptEntity::getOrgId, query.getOrgId());
        queryWrapper.eq(ObjectUtils.nonNull(query.getDptSts()), AdminSmDptEntity::getDptSts, query.getDptSts());

        // 1. 查出所有部门
        List<AdminSmDptEntity> dptEntityList = baseMapper.selectList(queryWrapper);

        // 2 组装成父子的树形结构
        // 2.1 找到所有的一级部门 目前一级部门的部门id写死为字符串0
        List<AdminSmDptEntity> level1Dpt = dptEntityList.stream().filter((dptEntity -> AdminSmDptEntity.DEFAULT_UP_DPT_ID.equals(dptEntity.getUpDptId())))
                .peek((dpt) -> {
                    // 2.2 递归下探当前部门的子部门树组装
                    dpt.setChildren(getChildren(dpt, dptEntityList));
                }).collect(Collectors.toList());
        return level1Dpt;
    }

    @Override
    public Page<AdminSmDptVo> getDeptsForWf(AdminSmDptQuery query) {
        Page<AdminSmDptVo> page = query.getIPage();
        QueryWrapper<AdminSmDptVo> wrapper = new QueryWrapper<>();
        wrapper.eq("T1.DPT_STS", "A");
        creatWrapper(wrapper, "T1.ORG_ID", query.getOrgId());
        creatWrapper(wrapper, "T1.DPT_CODE", query.getDptCode());
        creatWrapper(wrapper, "T1.DPT_NAME", query.getDptName());
        return this.baseMapper.getDeptsForWf(page, wrapper);
    }

    public void creatWrapper(QueryWrapper<AdminSmDptVo> wrapper, String column, String value) {
        if (StringUtils.nonEmpty(value)) {
            boolean condition = value.startsWith("%") || value.endsWith("%");
            wrapper.like(condition, column, value);
            wrapper.eq(!condition, column, value);
        }
    }

    /**
     * 递归查找传入部门的子部门，拼装成子部门树结构
     *
     * @param root 传入的部门实体
     * @param all  全量部门列表
     * @return 子部门树
     */
    private List<AdminSmDptEntity> getChildren(AdminSmDptEntity root, List<AdminSmDptEntity> all) {
        return all.stream()
                .filter(dptEntity -> dptEntity.getUpDptId().equals(root.getDptId()))
                .peek(dptEntity -> {
                    // 1. 递归查找子部门
                    dptEntity.setChildren(getChildren(dptEntity, all));
                }).collect(Collectors.toList());
    }

}