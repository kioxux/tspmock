package cn.com.yusys.yusp.oca.domain.vo;

import lombok.Data;

@Data
public class AdminSmDataTmplListVo {

    private String authTmplId;

    private String authTmplName;

    private String sqlString;

    private String sqlName;
    /**
     * 用于标记是否关联，0是未关联，1是已关联
     */
    private int mark;
    /**
     * 资源控制点ID
     */
    private String authRecoId;
    /**
     * 控制点id
     */
    private String contrId;
}
