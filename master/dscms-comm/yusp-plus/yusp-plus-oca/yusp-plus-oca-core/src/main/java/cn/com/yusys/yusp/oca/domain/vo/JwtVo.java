package cn.com.yusys.yusp.oca.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: yusp-plus
 * @description: jwtvo对象
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2020-11-18 16:28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JwtVo {

    private String user_id;

    private String client_id;
}
