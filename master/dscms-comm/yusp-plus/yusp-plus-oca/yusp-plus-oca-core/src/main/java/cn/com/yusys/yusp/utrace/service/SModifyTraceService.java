package cn.com.yusys.yusp.utrace.service;

import cn.com.yusys.yusp.utrace.domain.entity.SModifyTraceEntity;
import cn.com.yusys.yusp.utrace.domain.vo.UTraceQueryVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 小U留痕记录表
 *
 * @author
 * @email
 * @date 2021-05-17 15:13:18
 */
public interface SModifyTraceService extends IService<SModifyTraceEntity> {

    /**
     * 列表查询，分页
     *
     * @param params
     * @return
     */
    Page<SModifyTraceEntity> queryPage(UTraceQueryVo params);

    /**
     * 查询所有更改记录
     *
     * @param params
     * @return
     */
    List<SModifyTraceEntity> queryUtraceByPk(UTraceQueryVo params);

    /**
     * 保存更改记录
     *
     * @param sModifyTraceEntityList
     */
    void addUtrace(List<SModifyTraceEntity> sModifyTraceEntityList);
}

