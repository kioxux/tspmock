package cn.com.yusys.yusp.oca.command;

import cn.com.yusys.yusp.commons.session.SessionService;
import cn.com.yusys.yusp.oca.service.impl.RedisCacheLoadServicImpl;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 启动时加载数据到缓存
 *
 * @author yangzai
 * @since 2021/4/23
 **/
public class RedisCacheStartLoader implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(RedisCacheStartLoader.class);

    @Autowired
    private RedisCacheLoadServicImpl redisCacheLoadService;

    @Autowired
    private SessionService ocaSessionService;

    ThreadFactory guavaThreadFactory = new ThreadFactoryBuilder().setNameFormat("RedisCacheLoader-pool-").build();

    ExecutorService executorService = new ThreadPoolExecutor(1, 1,
            0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>(10), guavaThreadFactory);

    @Override
    public void run(String... args) {
        // 启动时加载，为不影响启动，单独开启一个线程用户执行数据查询及缓存初始化操作
        executorService.execute(() -> {
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载用户信息到Redis>>>>>>>>>>>>>>>>>>>");
            redisCacheLoadService.loadUserInfoData();
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载用户信息到Redis结束>>>>>>>>>>>>>>>>>>>");

            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载机构信息到Redis>>>>>>>>>>>>>>>>>>>");
            redisCacheLoadService.loadOrgInfoData();
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载机构信息到Redis结束>>>>>>>>>>>>>>>>>>>");

            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载机构树信息到Redis>>>>>>>>>>>>>>>>>>>");
            redisCacheLoadService.loadOrgTreeData();
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载机构树信息到Redis结束>>>>>>>>>>>>>>>>>>>");

            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载区域中心负责人下小微客户经理信息到Redis>>>>>>>>>>>>>>>>>>>");
            redisCacheLoadService.loadAreaXwUserData();
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载区域中心负责人下小微客户经理信息到Redis结束>>>>>>>>>>>>>>>>>>>");

            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载集中作业总行本地机构（除小微）下的所有用户信息到Redis>>>>>>>>>>>>>>>>>>>");
            redisCacheLoadService.loadJzzyUserData();
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载集中作业总行本地机构（除小微）下的所有用户信息到Redis结束>>>>>>>>>>>>>>>>>>>");

            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载所有小微客户经理信息到Redis>>>>>>>>>>>>>>>>>>>");
            redisCacheLoadService.loadAllXwUserData();
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载所有小微客户经理到Redis结束>>>>>>>>>>>>>>>>>>>");

            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载部门信息到Redis>>>>>>>>>>>>>>>>>>>");
            redisCacheLoadService.loadDeptInfoData();
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载部门信息到Redis结束>>>>>>>>>>>>>>>>>>>");

            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载系统提示信息到Redis>>>>>>>>>>>>>>>>>>>");
            redisCacheLoadService.loadSmMessageInfoData();
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载系统提示信息到Redis结束>>>>>>>>>>>>>>>>>>>");

            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载字典项到Redis>>>>>>>>>>>>>>>>>>>");
            redisCacheLoadService.loadSmLookupDictInfoData();
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载字典项到Redis结束>>>>>>>>>>>>>>>>>>>");

            //初始化控制点信息
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载控制点信息到Redis>>>>>>>>>>>>>>>>>>>");
            ocaSessionService.getAllControls();
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载控制点信息到Redis结束>>>>>>>>>>>>>>>>>>>");

            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载营业日期到Redis>>>>>>>>>>>>>>>>>>>");
            redisCacheLoadService.loadOpenDayData();
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载营业日期到Redis结束>>>>>>>>>>>>>>>>>>>");

            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载机构下小微客户经理信息到Redis>>>>>>>>>>>>>>>>>>>>>>>>>>");
            redisCacheLoadService.loadXwUserForOrg();
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>结束加载机构下小微客户经理信息到Redis>>>>>>>>>>>>>>>>>>>>>>>>>>");

            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载机构下非小微用户信息到Redis>>>>>>>>>>>>>>>>>>>>>>>>>>");
            redisCacheLoadService.loadNotXwUserForOrg();
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>结束加载机构下非小微用户信息到Redis>>>>>>>>>>>>>>>>>>>>>>>>>>");
        });
    }

}