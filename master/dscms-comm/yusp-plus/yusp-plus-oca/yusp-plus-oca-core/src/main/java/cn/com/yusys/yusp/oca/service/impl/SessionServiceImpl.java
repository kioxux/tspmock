package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.session.SessionService;
import cn.com.yusys.yusp.commons.session.user.Control;
import cn.com.yusys.yusp.commons.session.user.DataControl;
import cn.com.yusys.yusp.commons.session.user.MenuControl;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.user.impl.*;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.domain.entity.*;
import cn.com.yusys.yusp.oca.domain.vo.AdminDataTmplControlVo;
import cn.com.yusys.yusp.oca.service.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service("ocaSessionService")
public class SessionServiceImpl implements SessionService {
    private static final Logger log = LoggerFactory.getLogger(SessionServiceImpl.class);

    @Autowired
    AdminSmMenuServiceImpl adminSmMenuService;

    @Autowired
    AdminSmResContrServiceImpl adminSmResContrService;

    @Autowired
    AdminSmDptService adminSmDptService;

    @Autowired
    AdminSmUserRoleRelService adminSmUserRoleRelService;

    @Autowired
    AdminSmRoleService adminSmRoleService;

    @Autowired
    AdminSmOrgService adminSmOrgService;

    @Autowired
    AdminSmLogicSysService adminSmLogicSysService;

    @Autowired
    AdminSmInstuService adminSmInstuService;

    @Autowired
    AdminSmUserService adminSmUserService;

    @Autowired
    AdminSmAuthRecoService adminSmAuthRecoService;

    @Override
    public List<? extends Control> getAllControls() {
        return adminSmResContrService.selectControlImplList();
    }

    @Override
    public User getUserInfo(String clinetId, String userId) {

        UserInformation userInformation = new UserInformation();

        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(clinetId)) {
            // throw new YuspException("500", "用户id或客户端id为空");
            // 外系统通过bsp调用oca接口时，不带token,不走网关；clinetId与userId为空导致数据权限过滤器获取用户缓存信息抛异常；
            // 由于bsp调用的接口不确定且无规律，逐个添加白名单太麻烦；故不再抛出异常，仅打印日志并返回null;
            log.error("用户id或客户端id为空");
            return null;
        }

        //根据jwttoken信息解密出的sysId查询用户逻辑系统信息
        AdminSmLogicSysEntity logicSysEntity = adminSmLogicSysService.getOne(new QueryWrapper<AdminSmLogicSysEntity>().eq("SYS_ID", clinetId));
        if (Objects.nonNull(logicSysEntity)) {
            ClientImpl client = new ClientImpl();
            BeanUtils.copyProperties(logicSysEntity, client);
            userInformation.setLogicSys(client);
        }

        //根据jwttoken信息解密出的loginCode查询用户详细信息
        AdminSmUserEntity userEntity = adminSmUserService.getOne(new QueryWrapper<AdminSmUserEntity>().eq("USER_ID", userId).eq("USER_STS", AvailableStateEnum.ENABLED));

        if (Objects.nonNull(userEntity)) {
            // TODO 数据字段需要一致 lastLoginTime
            BeanUtils.copyProperties(userEntity, userInformation);
            userInformation.setLastLoginTime(DateUtils.formatDateTimeByDef(userEntity.getLastLoginTime()));
            //根据用户部门id查询部门详细信息
            AdminSmDptEntity dptEntity = adminSmDptService.getOne(new QueryWrapper<AdminSmDptEntity>().eq("DPT_ID", userEntity.getDptId()).eq("DPT_STS", AvailableStateEnum.ENABLED));

            if (Objects.nonNull(dptEntity)) {
                DepartmentImpl department = new DepartmentImpl();
                BeanUtils.copyProperties(dptEntity, department);
                userInformation.setDpt(department);

                //根据用户部门id查询上级部门部门详细信息
                if (StringUtils.nonEmpty(dptEntity.getUpDptId())) {
                    AdminSmDptEntity upDptEntity = adminSmDptService.getOne(new QueryWrapper<AdminSmDptEntity>().eq("DPT_ID", dptEntity.getUpDptId()).eq("DPT_STS", AvailableStateEnum.ENABLED));

                    if (Objects.nonNull(upDptEntity)) {
                        DepartmentImpl department1 = new DepartmentImpl();
                        BeanUtils.copyProperties(upDptEntity, department1);
                        userInformation.setUpDpt(department1);
                    }
                }
            }

            //根据用户id查询用户角色
            List<AdminSmUserRoleRelEntity> userRoleList = adminSmUserRoleRelService.list(new QueryWrapper<AdminSmUserRoleRelEntity>().eq("USER_ID", userEntity.getUserId()));

            if (CollectionUtils.nonEmpty(userRoleList)) {
                ArrayList<RoleImpl> roles = new ArrayList<>();
                for (AdminSmUserRoleRelEntity userRole : userRoleList) {
                    AdminSmRoleEntity roleEntity = adminSmRoleService.getOne(new QueryWrapper<AdminSmRoleEntity>().eq("ROLE_ID", userRole.getRoleId()).eq("ROLE_STS", AvailableStateEnum.ENABLED));
                    if (Objects.nonNull(roleEntity)) {
                        RoleImpl role = new RoleImpl();
                        BeanUtils.copyProperties(roleEntity, role);
                        roles.add(role);
                    }
                }
                userInformation.setRoles(roles);
            }

            //根据用户机构Id查询机构详细信息
            AdminSmOrgEntity orgEntity = adminSmOrgService.getOne(new QueryWrapper<AdminSmOrgEntity>().eq("ORG_ID", userEntity.getOrgId()).eq("ORG_STS", AvailableStateEnum.ENABLED));

            if (Objects.nonNull(orgEntity)) {
                OrganizationImpl organization = new OrganizationImpl();
                BeanUtils.copyProperties(orgEntity, organization);
                userInformation.setOrg(organization);

                if (StringUtils.nonEmpty(orgEntity.getUpOrgId())) {
                    //根据机构id查询上级机构详细信息
                    AdminSmOrgEntity upOrgEntity = adminSmOrgService.getOne(new QueryWrapper<AdminSmOrgEntity>().eq("ORG_ID", orgEntity.getUpOrgId()).eq("ORG_STS", AvailableStateEnum.ENABLED));

                    if (Objects.nonNull(upOrgEntity)) {
                        OrganizationImpl organization1 = new OrganizationImpl();
                        BeanUtils.copyProperties(upOrgEntity, organization1);
                        userInformation.setUpOrg(organization1);
                    }
                }

                if (Objects.nonNull(orgEntity.getOrgLevel())) {
                    userInformation.setOrgLevel(orgEntity.getOrgLevel().toString());
                }

                //根据金融机构ID查询金融机构信息
                if (StringUtils.nonEmpty(orgEntity.getInstuId())) {
                    AdminSmInstuEntity instuEntity = adminSmInstuService.getOne(new QueryWrapper<AdminSmInstuEntity>().eq("INSTU_ID", orgEntity.getInstuId()).eq("INSTU_STS", AvailableStateEnum.ENABLED));

                    if (Objects.nonNull(instuEntity)) {
                        FinancialOrganizationsImpl financialOrganizations = new FinancialOrganizationsImpl();
                        BeanUtils.copyProperties(instuEntity, financialOrganizations);
                        userInformation.setInstuOrg(financialOrganizations);
                    }
                }
            }

        }
        return userInformation;
    }

    @Override
    public MenuControl getMenuControl(String clientId, String userId) {
        List<MenuImpl> menuVoList = adminSmMenuService.getAdminSmMenu(userId);
        List<ControlImpl> contrVoList = adminSmResContrService.getAdminSmContr(userId);
        MenuControlImpl menuControlBean = new MenuControlImpl();
        menuControlBean.setContr(contrVoList);
        menuControlBean.setMenu(menuVoList);
        return menuControlBean;
    }

    @Override
    public List<? extends DataControl> getDataControl(String clientId, String userId) {
        List<AdminDataTmplControlVo> tmplControlVoList = adminSmAuthRecoService.getDataTmplControl(userId);
        List<DataControlImpl> list = new ArrayList<>();
        for (AdminDataTmplControlVo vo : tmplControlVoList) {
            DataControlImpl controlVo = new DataControlImpl();
            BeanUtils.copyProperties(vo, controlVo);
            list.add(controlVo);
        }
        return list;
    }
}
