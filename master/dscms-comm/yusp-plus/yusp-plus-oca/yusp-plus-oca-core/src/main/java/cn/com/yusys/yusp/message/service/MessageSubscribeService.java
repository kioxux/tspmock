package cn.com.yusys.yusp.message.service;

import cn.com.yusys.yusp.message.entity.MessageSubscribeEntity;
import cn.com.yusys.yusp.message.form.MessageSubscribeForm;
import cn.com.yusys.yusp.message.query.MessageSubscribeQuery;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Set;

/**
 * 用户订阅
 *
 * @author xiaodg@yusys.com.cn
 */
public interface MessageSubscribeService extends IService<MessageSubscribeEntity> {

    /**
     * 保存订阅
     *
     * @param messageSubscribeForm 消息订阅表单 {@link MessageSubscribeForm}
     * @return {@code Boolean}
     */
    Boolean saveMessageSubscribe(MessageSubscribeForm messageSubscribeForm);

    /**
     * 根据消息订阅查询对象查询订阅值
     *
     * @param messageSubscribeQuery 消息订阅查询对象 {@link MessageSubscribeQuery}
     * @return {@code String}
     */
    String findSubscribeValBy(MessageSubscribeQuery messageSubscribeQuery);

    /**
     * 根据消息类型和渠道类型查询出所有的订阅人
     *
     * @param messageType 消息类型 {@code String}
     * @param channelType 渠道类型 {@code String}
     * @param sendUserId  发送消息的用户ID {@code String}
     * @return Set<String> 用户ID集合 {@link Set}
     */
    Set<String> findUserIdsBy(String messageType, String channelType, String sendUserId);
}

