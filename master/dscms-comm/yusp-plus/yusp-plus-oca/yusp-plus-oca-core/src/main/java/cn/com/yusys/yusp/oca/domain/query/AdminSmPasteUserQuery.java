package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.common.query.PageQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmUserVo;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class AdminSmPasteUserQuery extends PageQuery<AdminSmUserVo> {

    private String keyWord;

    @NotEmpty(message = "Which user do you want to copy from?")
    private String expectedUserId;
}
