package cn.com.yusys.yusp.message.service.impl;

import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.message.config.SignConstants;
import cn.com.yusys.yusp.message.dao.MessageTypeDao;
import cn.com.yusys.yusp.message.entity.MessageTempEntity;
import cn.com.yusys.yusp.message.entity.MessageTypeEntity;
import cn.com.yusys.yusp.message.query.MessageTypePage;
import cn.com.yusys.yusp.message.service.MessageTempService;
import cn.com.yusys.yusp.message.service.MessageTypeService;
import cn.com.yusys.yusp.message.vo.MessageTypeVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

/**
 * 消息类型服务类
 *
 * @author xiaodg@yusys.com.cn
 */
@Service
@Slf4j
public class MessageTypeServiceImpl extends ServiceImpl<MessageTypeDao, MessageTypeEntity> implements MessageTypeService {

    @Autowired
    private MessageTempService messageTempService;

    private MessageTypeVo convert(MessageTypeEntity messageTypeEntity) {
        return BeanUtils.beanCopy(messageTypeEntity, MessageTypeVo.class);
    }

    @Override
    public IPage<MessageTypeVo> queryPage(MessageTypePage messageTypeQuery) {
        return this.lambdaQuery()
                .eq(StringUtils.isNotBlank(messageTypeQuery.getTemplateType()), MessageTypeEntity::getTemplateType, messageTypeQuery.getTemplateType())
                .like(StringUtils.isNotBlank(messageTypeQuery.getMessageDesc()), MessageTypeEntity::getMessageDesc, messageTypeQuery.getMessageDesc())
                .page(messageTypeQuery)
                .convert(this::convert);
    }


    @Override
    public IPage<MessageTypeVo> queryPageWithChannelTypes(MessageTypePage messageTypeQuery) {
        IPage<MessageTypeVo> page = this.queryPage(messageTypeQuery);
        page.getRecords().forEach(typeVo -> {
            // 加载每个消息类型配置过的模板类型
            typeVo.setChannelType(messageTempService.lambdaQuery().eq(MessageTempEntity::getMessageType, typeVo.getMessageType()).list().stream().map(MessageTempEntity::getChannelType).collect(Collectors.joining(SignConstants.MARK_COMMA)));

        });
        return page;
    }

}