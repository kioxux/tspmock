package cn.com.yusys.yusp.message.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 消息类型
 *
 * @author xiaodg@yusys.com.cn
 */
@Data
@TableName("message_type")
public class MessageTypeEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 消息类型
     */
    @TableId
    private String messageType;
    /**
     * 描述
     */
    private String messageDesc;
    /**
     * 消息等级[小先发]
     */
    private String messageLevel;
    /**
     * 模板类型[实时模板、订阅模板]
     */
    private String templateType;

}
