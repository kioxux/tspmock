package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.common.utils.Query;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExporterImporter;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.async.ImportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.IterableModel;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.oca.dao.AdminSmLogDao;
import cn.com.yusys.yusp.oca.domain.dto.AdminSmLogDto;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmLogEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.domain.form.AdminSmLogForm;
import cn.com.yusys.yusp.oca.domain.query.AdminSmLogQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmLogPojo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmLogVo;
import cn.com.yusys.yusp.oca.service.AdminSmLogService;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author danyu
 */
@Service("adminSmLogService")
public class AdminSmLogServiceImpl extends ServiceImpl<AdminSmLogDao, AdminSmLogEntity> implements AdminSmLogService {

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private AdminSmUserService adminSmUserService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AdminSmLogEntity> page = this.page(
                new Query<AdminSmLogEntity>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

    @Override
    public IPage<AdminSmLogVo> pageLogByCondition(AdminSmLogForm adminSmLogForm) {
        int page = adminSmLogForm.getPage();
        int size = adminSmLogForm.getSize();
        String logTypeId = adminSmLogForm.getLogTypeId();
        String operObjId = adminSmLogForm.getOperObjId();
        String beginTime = adminSmLogForm.getBeginTime();
        String endTime = adminSmLogForm.getEndTime();
        String userName = adminSmLogForm.getUserName();

        // 组装查询条件
        QueryWrapper<AdminSmLogVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(!StringUtils.isEmpty(logTypeId), "log.LOG_TYPE_ID", logTypeId);
        queryWrapper.eq(!StringUtils.isEmpty(operObjId), "log.OPER_OBJ_ID", operObjId);
        // 顾银华添加页面中缺失的查询条件：操作用户
        queryWrapper.eq(!StringUtils.isEmpty(userName), "u.USER_NAME", userName);
        queryWrapper.between((!StringUtils.isEmpty(beginTime) && !StringUtils.isEmpty(endTime)), "log.OPER_TIME", beginTime, endTime);
        // 组装page分页对象
        Page<AdminSmLogVo> pageList = new Page<>(page, size);
        pageList.addOrder(OrderItem.desc("log.OPER_TIME"));

        IPage<AdminSmLogVo> pageResult = this.baseMapper.pageLogByCondition(pageList, queryWrapper);

        return pageList;
    }

    @Override
    public ResultDto addLog(AdminSmLogDto logDto) {

        AdminSmLogEntity logEntity = new AdminSmLogEntity();
        BeanUtils.beanCopy(logDto, logEntity);
        logEntity.setLogId(IdWorker.get32UUID());
        logEntity.setOperTime(DateUtils.formatDateTimeByDef());
        this.save(logEntity);

        return ResultDto.success();
    }

    @Override
    public File export(Map<String, String> params) {

        IterableModel iterableModel = new IterableModel((pageSize, pageNum) -> {
            //TODO 组装查询条件
            QueryWrapper<AdminSmLogEntity> queryWrapper = new QueryWrapper<>();

            // 组装page分页对象
            Page<AdminSmLogEntity> page = new Page<>(Long.valueOf(pageNum), Long.valueOf(pageSize));
            page.addOrder(OrderItem.desc("OPER_TIME"));
            IPage<AdminSmLogEntity> pageResult = this.page(page, queryWrapper);
            return pageResult.getRecords();
        });

        // 使用 excel 导出工具类导出
        return new ExporterImporter().export(AdminSmLogPojo.class, iterableModel);
    }

    /**
     * excel 导出
     *
     * @param params
     * @return
     */
    @Override
    public File asyncExportFile(Map<String, String> params) {
        Integer pageSize = 0;
        Integer pageNum = 0;

        if (params.containsKey("pageNum") && params.containsKey("pageSize")) {
            pageSize = Integer.valueOf(params.get("pageSize"));
            pageNum = Integer.valueOf(params.get("pageNum"));
        }

        //查询记录数
        IterableModel iterableModel = null;

        /**
         * 分页查询日志数据，并将日志数据组装成 excel
         */
        if (pageSize == 0 || pageNum == 0) {
            Integer finalPageNum = pageNum;
            Integer finalPageSize = pageSize;

            iterableModel = new IterableModel((one, two) -> {
                QueryWrapper<AdminSmLogEntity> queryWrapper = new QueryWrapper<>();
                // 组装page分页对象
                Page<AdminSmLogEntity> page = new Page<>(Long.valueOf(one), Long.valueOf(two));
                page.addOrder(OrderItem.desc("OPER_TIME"));
                IPage<AdminSmLogEntity> pageResult = this.page(page, queryWrapper);
                return pageResult.getRecords();
            });
        } else {
            Integer finalPageNum1 = pageNum;
            Integer finalPageSize1 = pageSize;
            iterableModel = new IterableModel((one, two) -> {
                QueryWrapper<AdminSmLogEntity> queryWrapper = new QueryWrapper<>();
                // 组装page分页对象
                Page<AdminSmLogEntity> page = new Page<>(Long.valueOf(finalPageNum1), Long.valueOf(finalPageSize1));
                // 设置Mybatis每次查询最大查询数量
                page.setMaxLimit(10000L);
                page.addOrder(OrderItem.desc("OPER_TIME"));
                IPage<AdminSmLogEntity> pageResult = this.page(page, queryWrapper);
                return pageResult.getRecords();
            }, pageNum, pageSize);
        }

        // 使用excel 导出工具类导出
        return new ExporterImporter().export(AdminSmLogPojo.class, iterableModel);
    }

    private <T> T getQueryCondition(Map<String, String> params, T t) {
        String json = params.get("condition");

        try {
            if (json != null) {
                return (T) objectMapper.readValue(json, t.getClass());
            }
        } catch (Exception e) {
            throw new YuspException("500", "解析查询参数异常");
        }

        return t;
    }

    /**
     * @description： 批量插入
     * @author： lty
     * @date： 2021/1/13
     */
    @Override
    public int insertBatch(List<AdminSmLogEntity> logPojoList) {
        saveBatch(logPojoList);
        return logPojoList.size();
    }

    /**
     * 导出匹配查询接口
     *
     * @param logQuery
     */
    @Override
    public List<AdminSmLogEntity> selectByForm(AdminSmLogQuery logQuery, IPage<AdminSmLogEntity> page) {

        QueryWrapper<AdminSmLogEntity> wrapper = new QueryWrapper<>();
        /**
         * 前端传递的是用户名，需要先查询用户 id；
         * 是模糊查询所以是List
         */
        if (!StringUtils.isEmpty(logQuery.getUserName())) {
            List<AdminSmUserEntity> userList = adminSmUserService.selectIdLikeName(logQuery.getUserName());
            List<String> userIdList = userList.stream().map(AdminSmUserEntity::getUserId).collect(Collectors.toList());
            wrapper.in(userIdList.size() > 0, "USER_ID", userIdList);
        }
        wrapper.like(!StringUtils.isEmpty(logQuery.getLogTypeId()), "LOG_TYPE_ID", logQuery.getLogTypeId());
        wrapper.like(!StringUtils.isEmpty(logQuery.getOperObjId()), "OPER_OBJ_ID", logQuery.getOperObjId());
        wrapper.between(
                (!StringUtils.isEmpty(logQuery.getBeginTime()) && !StringUtils.isEmpty(logQuery.getEndTime())),
                "OPER_TIME", logQuery.getBeginTime(), logQuery.getEndTime());

        IPage<AdminSmLogEntity> entityPage = this.baseMapper.selectPage(page, wrapper);
        return entityPage.getRecords();
    }

    /**
     * 异步导入，批量保存的方法
     *
     * @param entityList
     * @return
     */
    @Override
    public Integer saveBatchByEntity(List<AdminSmLogPojo> entityList) {
        Collection<AdminSmLogEntity> adminSmLogEntities = BeanUtils.beansCopy(entityList, AdminSmLogEntity.class);
        this.saveBatch(adminSmLogEntities);
        return null;
    }

    /**
     * 异步导出文件
     *
     * @param logQuery
     * @return
     */
    @Override
    public ProgressDto translateFile(AdminSmLogQuery logQuery) {
        ExportContext context = ExportContext.of(AdminSmLogPojo.class);
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object o) {
                AdminSmLogQuery adminSmLogQuery = (AdminSmLogQuery) o;
                IPage<AdminSmLogEntity> page = new Page<>(i, i1);
                List<AdminSmLogEntity> list = selectByForm(adminSmLogQuery, page);
                Collection<AdminSmLogPojo> adminSmLogPojos = BeanUtils.beansCopy(list, AdminSmLogPojo.class);
                return adminSmLogPojos;
            }
        };
        return ExcelUtils.asyncExport(context.data(dataAcquisition, logQuery));
    }

    /**
     * 文件上传，异步导入
     *
     * @param uploadFile
     * @return
     */
    @Override
    public ProgressDto asyncImport(MultipartFile uploadFile) {
        ProgressDto progressDto = null;
        String fileName = uploadFile.getOriginalFilename();
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        try {
            /**
             * 将 MultipartFile 文件转成
             */
            File temporaryFile = ExcelUtils.getTemporaryFile(suffixName);
            uploadFile.transferTo(temporaryFile);
            ImportContext importContext = new ImportContext();
            importContext.setHeadClass(AdminSmLogPojo.class);
            importContext.setFile(temporaryFile.getAbsolutePath());
            /**
             * true 为批量执行导入 Object o 为 List 集合
             * false 为逐条执行导入 Object o 为 AdminSmLogPojo 对象
             */
            importContext.batch(true);

            progressDto = ExcelUtils.asyncImport(ImportContext.of(AdminSmLogPojo.class)
                    // 批量操作需要将batch设置为true
                    .batch(true)
                    // 传入 excel 临时文件
                    .file(temporaryFile)
                    // 使用batchInsert对数据进行批量操作
                    .dataStorage(ExcelUtils.batchConsumer(this::saveBatchByEntity)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return progressDto;
    }

    /**
     * 导出空模板
     *
     * @return
     */
    @Override
    public File downLoadTemplate() {
        File file = ExcelUtils.syncExport(AdminSmLogPojo.class, new ArrayList<AdminSmLogPojo>());
        return file;
    }
}