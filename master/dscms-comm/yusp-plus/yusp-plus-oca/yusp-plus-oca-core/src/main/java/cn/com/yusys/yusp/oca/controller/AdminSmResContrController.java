package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmResContrEntity;
import cn.com.yusys.yusp.oca.domain.form.*;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmContrTreeVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmMenuVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmResContrVo;
import cn.com.yusys.yusp.oca.service.AdminSmDataAuthTmplService;
import cn.com.yusys.yusp.oca.service.AdminSmResContrService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 系统功能控制点表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-27 14:57:08
 */
@RestController
@RequestMapping("/api/adminsmrescontr")
public class AdminSmResContrController {

    @Autowired
    private AdminSmResContrService adminSmResContrService;
    @Autowired
    private AdminSmDataAuthTmplService adminSmDataAuthTmplService;


    /**
     * 控制点页面新增、修改时所需的菜单列表
     * @param adminSmMenuConditionForm
     * @return
     */
    @PostMapping("/getmenulist")
    public ResultDto getMenuList(@RequestBody AdminSmMenuConditionForm adminSmMenuConditionForm) {
        Page<AdminSmMenuVo> page = adminSmResContrService.getMenuList(adminSmMenuConditionForm);
        return ResultDto.success(page.getRecords()).total(page.getTotal());
    }

    /**
     * 查询控制点列表
     * @param adminSmResContrForm
     * @return
     */
    @GetMapping("/getcontrinfo")
    public ResultDto getContrInfo(AdminSmResContrForm adminSmResContrForm) {
        Page<AdminSmResContrVo> page = adminSmResContrService.queryPageWithCondition(adminSmResContrForm);
        return ResultDto.success(page);
    }

    /**
     * 信息
     * 不知道干嘛的
     */
    @GetMapping("/info/{contrId}")
    public ResultDto info(@PathVariable("contrId") String contrId) {
        AdminSmResContrEntity adminSmResContr = adminSmResContrService.getById(contrId);
        return ResultDto.success(adminSmResContr);
    }

    /**
     * 验证contrCode是否已经存在
     *
     * 如果返回大于0：表示已经存在
     * 如果返回0：表示可以使用该contrCode
     * 如果返回-1：表示contrCode为空，需要填写该值
     *
     * @param adminSmResContrForm
     * @return
     */
    @PostMapping("/checkcode")
    public ResultDto checkCode(@RequestBody AdminSmResContrForm adminSmResContrForm) {
        int count = adminSmResContrService.checkCode(adminSmResContrForm);
        return ResultDto.success(count);
    }

    /**
     * 新增控制点
     * @param adminSmResContrSaveForm
     * @return
     */
    @PostMapping("/createcontr")
    public ResultDto createContr(@RequestBody AdminSmResContrSaveForm adminSmResContrSaveForm) {
        adminSmResContrService.createContr(adminSmResContrSaveForm);
        return ResultDto.success();
    }

    /**
     * 修改控制点
     *
     * 修改的时候还要验证 contrCode，需要调用一次checkCode接口
     *
     * @param adminSmResContrSaveForm
     * @return
     */
    @PostMapping("/editcontr")
    public ResultDto updateContr(@RequestBody AdminSmResContrSaveForm adminSmResContrSaveForm) {
        adminSmResContrService.updateContr(adminSmResContrSaveForm);
        return ResultDto.success();
    }

    /**
     * 删除控制点
     * @param ids
     * @return
     */
    @PostMapping("/deletecontr")
    public ResultDto deleteContr(@RequestBody String[] ids) {
        adminSmResContrService.deleteContr(ids);
        return ResultDto.success();
    }

    /**
     * 获取业务功能树
     * @return
     */
    @GetMapping("/treequery")
    public ResultDto getFuncTree() {
        List<AdminSmContrTreeVo> treeList = adminSmResContrService.getFuncTree();
        return ResultDto.success(treeList);
    }

    /**
     * 查询控制点可授权的数据权限模板列表
     * @param form
     * @return
     */
    @PostMapping("/datatmpllist")
    public ResultDto getDataTmplList(@RequestBody AdminSmDataTmplConditionForm form) {
        IPage pageForContr = adminSmDataAuthTmplService.getListForContr(form);
        return ResultDto.success().data(pageForContr.getRecords()).total(pageForContr.getTotal());
    }

    /**
     * 控制点关联数据权限模板操作
     * @param adminSmResContrSaveForm
     * @return
     */
    @PostMapping("/relationtmpl")
    public ResultDto relationTmpl(@RequestBody AdminSmResContrSaveForm adminSmResContrSaveForm) {
        String result = adminSmResContrService.relationTmpl(adminSmResContrSaveForm, true);
        if ("修改关联成功！".equals(result)) {
            return ResultDto.success().message(result);
        } else {
            return ResultDto.error(result);
        }
    }

    /**
     * 查询控制点已经关联的模板
     * @param adminSmDataAuthForm
     * @return
     */
    @PostMapping("/associatedtmpl")
    public ResultDto associatedTmpl(@RequestBody AdminSmDataAuthForm adminSmDataAuthForm) {
        return ResultDto.success(adminSmDataAuthTmplService.associatedTmpl(adminSmDataAuthForm.getContrId()));
    }
}
