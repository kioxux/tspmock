package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.session.SessionService;
import cn.com.yusys.yusp.commons.session.user.MenuControl;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.oca.service.AdminSmAuthRecoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;

@Slf4j
@RestController
@RequestMapping("/api/account")
public class AdminMenuAndContrController {

    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Autowired
    SessionService ocaSessionService;
    @Autowired
    AdminSmAuthRecoService adminSmAuthRecoService;

    @GetMapping("/menuandcontr")
    public MenuControl getMenuAndContr() {
        String userId = SessionUtils.getUserId();
        String clientId = SessionUtils.getClientId();
        String menuControlKey = "MenuControl:" + clientId + "-" + userId;
        String dataTmplControlKey = "DataControl:" + clientId + "-" + userId;
        String allControlKey = "Control:" + clientId + "-" + userId;
        try {
            stringRedisTemplate.delete(Arrays.asList(menuControlKey, dataTmplControlKey, allControlKey));
        } catch (Exception e) {
            log.warn("redis connect warning", e);
        }
        /**
         * 异步加载数据权限,所有控制点
         */
        CompletableFuture.runAsync(() -> {
            ocaSessionService.getDataControl(clientId, userId);
        });

        return ocaSessionService.getMenuControl(clientId, userId);
    }
}
