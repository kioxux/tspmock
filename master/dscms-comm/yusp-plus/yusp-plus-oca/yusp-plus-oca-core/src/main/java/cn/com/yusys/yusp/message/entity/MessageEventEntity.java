package cn.com.yusys.yusp.message.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 消息事件
 *
 * @author xiaodg@yusys.com.cn
 */
@Data
@TableName("message_event")
@AllArgsConstructor
@NoArgsConstructor
public class MessageEventEntity implements Serializable {

    private static final long serialVersionUID = 9112619056513606606L;
    /**
     * 事件唯一编号
     */
    @TableId
    private String eventNo;
    /**
     * 参数
     */
    private String templateParam;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 消息类型
     */
    private String messageType;

}
