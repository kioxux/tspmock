package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.oca.dao.AdminSmLoginLogDao;
import cn.com.yusys.yusp.oca.domain.constants.Constants;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmLoginLogEntity;
import cn.com.yusys.yusp.oca.service.AdminSmLoginLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @description: 类说明
 * @author: zhangsong
 * @date: 2021/4/1
 */
@Service("adminSmLoginLogService")
public class AdminSmLoginLogServiceImpl extends ServiceImpl<AdminSmLoginLogDao, AdminSmLoginLogEntity> implements AdminSmLoginLogService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public void saveLog(AdminSmLoginLogEntity adminSmLoginLogEntity) {
        this.baseMapper.insert(adminSmLoginLogEntity);
    }

    @Override
    public AdminSmLoginLogEntity getLastSuccessLogin(String loginCode) {
        return this.baseMapper.getLastLoginLog(loginCode, Constants.LoginLogConstance.SUCCESS);
    }

    @Override
    public void getOpenDay() {
        String openDay = this.baseMapper.getOpenDay();
        if(StringUtils.isBlank(openDay)){
            openDay = DateUtils.getCurrDateStr();
        }
        stringRedisTemplate.opsForValue().set(Constants.SystemUserConstance.REDIS_KEY_OPENDAY, openDay);
    }
}
