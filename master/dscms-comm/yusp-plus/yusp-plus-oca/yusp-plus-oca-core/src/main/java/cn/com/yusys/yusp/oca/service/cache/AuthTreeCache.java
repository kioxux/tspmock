package cn.com.yusys.yusp.oca.service.cache;

import cn.com.yusys.yusp.oca.domain.vo.AdminSmAuthTreeVo;
import cn.com.yusys.yusp.oca.service.AdminSmDataAuthTmplService;
import cn.com.yusys.yusp.oca.service.AdminSmMenuService;
import cn.com.yusys.yusp.oca.service.AdminSmResContrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AuthTreeCache {

    @Autowired
    private AdminSmMenuService adminSmMenuService;
    @Autowired
    private AdminSmResContrService adminSmResContrService;
    @Autowired
    private AdminSmDataAuthTmplService adminSmDataAuthTmplService;

    @Cacheable(value = "AuthTreeCache@5M", key = "#root.methodName", condition = "#b")
    public List<AdminSmAuthTreeVo> getUserAuthMenuData(String id, boolean b) {
        List<AdminSmAuthTreeVo> treeVoList = adminSmMenuService.selectAuthTree(id);
        return treeVoList;
    }

    @Cacheable(value = "AuthTreeCache@5M", key = "#root.methodName", condition = "#b")
    public List<AdminSmAuthTreeVo> getUserAuthDataTmplData(String id, boolean b) {
        List<AdminSmAuthTreeVo> treeVoList = adminSmDataAuthTmplService.selectAuthTree(id);
        return treeVoList;
    }

    @Cacheable(value = "AuthTreeCache@5M", key = "#root.methodName", condition = "#b")
    public List<AdminSmAuthTreeVo> getUserAuthContrData(String id, boolean b) {
        List<AdminSmAuthTreeVo> treeVoList = adminSmResContrService.selectAuthTree(id);
        return treeVoList;
    }
}