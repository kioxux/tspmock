package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.dto.AdminSmCrelStraDto;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmCrelStraEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * 认证策略参数表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2021-03-30 11:27:32
 */
public interface AdminSmCrelStraService extends IService<AdminSmCrelStraEntity> {

    PageUtils queryPage(Map<String, Object> params);

    ResultDto<Object> checkBeforeLogin(String loginCode);

    ResultDto<Object> passwordErrorLimit(String loginCode, String ip);

    ResultDto<Object> checkSuccessLogin(String userId, String ip);

    boolean getLoignSingleAgentEnabled();

    /**
     * 批量修改认证策略
     *
     * @param adminSmCrelStraDto
     */
    void updateById(AdminSmCrelStraDto adminSmCrelStraDto);
}

