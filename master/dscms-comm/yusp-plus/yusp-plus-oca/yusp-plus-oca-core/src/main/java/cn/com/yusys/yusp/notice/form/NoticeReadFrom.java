package cn.com.yusys.yusp.notice.form;

import lombok.Data;

@Data
public class NoticeReadFrom {
    private String creatorId;
    private String creatorName;
    private String creatorTime;
    private String isTop;
    private String noticeId;
    private String noticeLevel;
    private String noticeTitle;
    private String pubSts;
    private String readSts;
    private String readUserId;
}
