package cn.com.yusys.yusp.notice.constant;

public class NoticeConstant {

    public static final String RICHEDIT_REL_MOD = "NOTICE";

    // 表示查看公告的指定用户权限
    public static final String RECIVE_TYPE_USER = "USER";

    // 表示查看公告的角色权限
    public static final String RECIVE_TYPE_ROLE = "ROLE";
    // 表示所有角色权限或所有部门权限都可以查看该公告
    public static final String RECIVE_OGJ_ID_NA = "NA";
    // 表示查看公告的部门权限
    public static final String RECIVE_TYPE_ORG = "ORG";
    // 权限表新增判断
    public static final String RECIVE_CREATE = "CREATE";
    // 权限表修改判断
    public static final String RECIVE_UPDATE = "UPDATE";

}

