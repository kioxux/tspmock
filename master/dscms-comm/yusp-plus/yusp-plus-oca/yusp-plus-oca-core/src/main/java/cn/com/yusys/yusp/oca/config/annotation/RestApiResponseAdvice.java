package cn.com.yusys.yusp.oca.config.annotation;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.oca.domain.constants.Constants;
import cn.com.yusys.yusp.oca.domain.constants.ResponseAndMessageEnum;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.service.AdminSmCrelStraService;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import cn.com.yusys.yusp.oca.utils.IpUtil;
import cn.com.yusys.yusp.oca.utils.JsonUtil;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

/**
 * @description: 登录动作完成后校验
 * @author: zhangsong
 * @date: 2021/3/31
 */
@ControllerAdvice
public class RestApiResponseAdvice implements ResponseBodyAdvice<ResultDto> {
    @Autowired
    private AdminSmCrelStraService adminSmCrelStraService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        LoginSuccessStrategy loginSuccessStrategy = methodParameter.getMethod().getAnnotation(LoginSuccessStrategy.class);
        return loginSuccessStrategy != null;
    }

    @Override
    public ResultDto beforeBodyWrite(ResultDto resultDto, MethodParameter methodParameter, MediaType mediaType,
                                     Class<? extends HttpMessageConverter<?>> aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        ResultDto checkResult = null;
        String ip = IpUtil.getIpAddr(serverHttpRequest);
        if (!resultDto.getCode().equals(ResponseAndMessageEnum.SUCCESS.getCode())) {
            //密码错误次数校验
            if (resultDto.getCode().equals(ResponseAndMessageEnum.BAD_CREDENTIALS.getCode())) {
                String loginCode = resultDto.getExtData() == null ? null : resultDto.getExtData().get("loginCode") == null ? null : resultDto.getExtData().get("loginCode").toString();
                checkResult = adminSmCrelStraService.passwordErrorLimit(loginCode, ip);

                return checkResult;
            }
            return resultDto;
        }

        HashMap resultMap = JsonUtil.parseJson(JSON.toJSONString(resultDto.getData()), HashMap.class);
        String userId = resultMap.get("userId") == null ? "" : resultMap.get("userId").toString();

        if (StringUtils.isBlank(userId)) {
            return resultDto;
        }

        checkResult = adminSmCrelStraService.checkSuccessLogin(userId, ip);
        if (!checkResult.getCode().equals(ResponseAndMessageEnum.SUCCESS.getCode())) {
            //警告信息不影响用户返回
            if (checkResult.getExtData() != null && checkResult.getExtData().get("actionType") != null
                    && checkResult.getExtData().get("actionType").toString().equals(Constants.SystemUserConstance.USER_STATE_WARNING)) {
                checkResult.setExtData(null);
                resultDto.setExtData(JsonUtil.parseJson(JSON.toJSONString(checkResult), HashMap.class));

                return resultDto;
            }

            checkResult.setData(resultDto.getData());

            return checkResult;
        }

        //更新用户最近登录时间信息
        if (resultDto.getCode().equals(ResponseAndMessageEnum.SUCCESS.getCode())) {
            adminSmUserService.updateLoginTime(userId, new Date());
        }

        //查询用户loginCode
        AdminSmUserEntity byId = adminSmUserService.getById(userId);
        if (Objects.nonNull(byId)) {
            //删除缓存中因为密码输入错误次数过多，而禁止登录的数据
            String loginCodeCountCacheKey = String.format("loginErrorCount:loginCode_%s", byId.getLoginCode());
            //判断是否存在key,有--删除
            Boolean hasKey = stringRedisTemplate.hasKey(loginCodeCountCacheKey);
            if (hasKey) {
                stringRedisTemplate.delete(loginCodeCountCacheKey);
            }
        }
        return resultDto;
    }
}
