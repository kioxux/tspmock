package cn.com.yusys.yusp.utrace.dao;

import cn.com.yusys.yusp.utrace.domain.entity.SModifyTraceEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 小U留痕记录表
 *
 * @author
 * @email
 * @date 2021-05-17 15:13:18
 */
@Mapper
public interface SModifyTraceDao extends BaseMapper<SModifyTraceEntity> {
}
