package cn.com.yusys.yusp.message.util;

import cn.com.yusys.yusp.commons.exception.PlatformException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.message.config.MessageConstants;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * 消息模块工具类
 *
 * @author xiaodg@yusys.com.cn
 **/
@Slf4j
public final class MessageUtils {

    private MessageUtils() {

    }

    /**
     * 解析消息模板
     *
     * @param template 模板字符串 {@code String}
     * @param params   模板参数 {@link Map}
     * @return 解析后的模板内容 {@code String}
     */
    public static String parseParam(String template, Map<String, String> params) {
        for (String key : params.keySet()) {
            String keyTemplate = "${" + key + "}";
            if (template.contains(keyTemplate)) {
                template = template.replace(keyTemplate, params.get(key));
            }
        }
        return template;
    }

    /**
     * 校验字符串不能为null和空, 抛出异常
     *
     * @param str     需要判断的字符串 {@code String}
     * @param message 当str为null或empty时抛出的异常
     * @return {@code String} 若str不为null且不为empty, 返回str本身
     */
    public static String requireNonNullAndEmptyString(String str, String message) {
        if (StringUtils.isEmpty(str)) {
            throw new PlatformException(Objects.requireNonNull(message));
        }
        return str;
    }

    /**
     * 校验字符串不能为null和空, 如果为null或empty返回用户提供的字符串
     *
     * @param str      需要判断的字符串 {@code String}
     * @param supplier 当str为null或empty时用户提供的字符串(不可为null)
     * @return {@code String}  若str不为null且不为empty, 返回str本身, 否则返回用户提供的字符串
     */
    public static String requireNonNullAndEmptyString(String str, Supplier<String> supplier) {
        if (StringUtils.isEmpty(str)) {
            return Objects.requireNonNull(supplier.get());
        }
        return str;
    }

    /**
     * 解析消息等级为消息发送优先级
     *
     * @param messageLevel 消息等级
     * @return {@link Integer}
     */
    public static Integer parseMessageLevel(String messageLevel) {
        try {
            return Integer.parseInt(messageLevel);
        } catch (Exception e) {
            log.error("解析消息等级为消息优先级失败: {}, err: {}", messageLevel, e);
            return Integer.valueOf(MessageConstants.MESSAGE_LEVEL_LOW);
        }
    }
}
