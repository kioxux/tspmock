package cn.com.yusys.yusp.notice.controller;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.notice.entity.AdminSmNoticeReadEntity;
import cn.com.yusys.yusp.notice.service.AdminSmNoticeReadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 系统公告用户查阅历史表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-15 13:05:27
 */
@Slf4j
@RestController
@RequestMapping("/api/notice/adminsmnoticeread")
public class AdminSmNoticeReadController {

    @Autowired
    private AdminSmNoticeReadService adminSmNoticeReadService;

    /**
     * 保存查看公告记录
     * @param noticeIds
     * @return
     */
    @GetMapping("/save")
    public ResultDto recordRead(@RequestParam List<String> noticeIds){
        log.info("已读参数-------------------------：" + noticeIds);
		adminSmNoticeReadService.recordRead(noticeIds);
        return ResultDto.success();
    }
}
