package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 产品附加信息
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-12-02 18:03:52
 */
@Data
@TableName("cms_product_add_info")
public class ProductAddInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 产品附加信息Id
	 */
	@TableId
	private String productAddId;
	/**
	 * 产品id
	 */
	private String productId;
	/**
	 * 附加类型0-任务池，1-审批规则，2-资料信息，3-会签规则
	 */
	private String addType;
	/**
	 * 附加值
	 */
	private String addValue;
	/**
	 * 附加名称
	 */
	private String addName;
	/**
	 * 描述
	 */
	private String addDesc;
	/**
	 * 排序
	 */
	private Integer addOrder;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 产品显示状态(1-逻辑有效，0-逻辑删除)
	 */
	private String logicStatus;

}
