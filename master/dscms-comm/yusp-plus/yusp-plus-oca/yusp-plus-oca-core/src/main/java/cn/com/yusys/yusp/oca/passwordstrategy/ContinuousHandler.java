package cn.com.yusys.yusp.oca.passwordstrategy;

import cn.com.yusys.yusp.oca.domain.constants.Constants;
import org.passay.EnglishSequenceData;
import org.passay.IllegalSequenceRule;
import org.passay.Rule;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: yusp-plus
 * @description: 口令复杂-数字
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2021-03-30 16:40
 */
@Component
public class ContinuousHandler implements Handler {
    @Override
    public List<Rule> getPasswordStrategy(String name, String detail) {
        ArrayList<Rule> passwordStratety = new ArrayList<>();
        passwordStratety.add(new IllegalSequenceRule(EnglishSequenceData.Alphabetical, Integer.parseInt(detail), false));
        return passwordStratety;
    }

    @Override
    public void afterPropertiesSet() throws Exception {

        PasswordFactory.register(Constants.SystemUserConstance.PASSWD_SEQUNNUMBER_RULE, this);
        PasswordFactory.registerErrorCode("ILLEGAL_ALPHABETICAL_SEQUENCE",PasswordStrategyConstant.PASSWD_CONTINUOUS_ERROR);

    }
}
