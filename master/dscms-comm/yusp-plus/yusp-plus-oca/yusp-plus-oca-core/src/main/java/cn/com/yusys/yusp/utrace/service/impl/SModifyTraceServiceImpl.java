package cn.com.yusys.yusp.utrace.service.impl;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.utrace.dao.SModifyTraceDao;
import cn.com.yusys.yusp.utrace.domain.entity.SModifyTraceEntity;
import cn.com.yusys.yusp.utrace.domain.vo.UTraceQueryVo;
import cn.com.yusys.yusp.utrace.service.SModifyTraceService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@Service("sModifyTraceService")
public class SModifyTraceServiceImpl extends ServiceImpl<SModifyTraceDao, SModifyTraceEntity> implements SModifyTraceService {

    private static final DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Override
    public Page<SModifyTraceEntity> queryPage(UTraceQueryVo params) {
        Page<SModifyTraceEntity> iPage = params.getIPage();
        QueryWrapper<SModifyTraceEntity> wrapper = new QueryWrapper<SModifyTraceEntity>().eq(StringUtils.nonEmpty(params.getMFieldId()), "m_field_id", params.getMFieldId())
                .in(StringUtils.nonEmpty(params.getMPkV()), "m_pk_v", Arrays.asList(params.getMPkV().split(",")))
                .likeRight(StringUtils.nonEmpty(params.getMonth()), "m_datetime", params.getMonth())
                .orderByDesc("m_datetime");
        return this.baseMapper.selectPage(iPage, wrapper);
    }

    @Override
    public List<SModifyTraceEntity> queryUtraceByPk(UTraceQueryVo params) {
        QueryWrapper<SModifyTraceEntity> queryWrapper = new QueryWrapper<SModifyTraceEntity>().eq(StringUtils.nonEmpty(params.getMFieldId()), "m_field_id", params.getMFieldId())
                .in(StringUtils.nonEmpty(params.getMPkV()), "m_pk_v", Arrays.asList(params.getMPkV().split(",")))
                .orderByDesc("m_datetime");
        // 查出当前修改项最后一次修改记录
        return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addUtrace(List<SModifyTraceEntity> sModifyTraceEntityList) {
        if (!CollectionUtils.isEmpty(sModifyTraceEntityList)) {
            List<SModifyTraceEntity> collect = sModifyTraceEntityList.stream().filter(sModifyTraceEntity -> !sModifyTraceEntity.getMOldV().equals(sModifyTraceEntity.getMNewV())).collect(Collectors.toList());
            collect.stream().forEach(sModifyTraceEntity -> {
                        LocalDateTime localDateTime = LocalDateTime.now();
                        sModifyTraceEntity.setMDatetime(localDateTime.format(df));
                    }
            );
            this.saveBatch(collect);
        }
    }
}