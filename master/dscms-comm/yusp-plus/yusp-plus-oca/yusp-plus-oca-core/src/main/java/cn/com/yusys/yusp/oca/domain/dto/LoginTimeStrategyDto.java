package cn.com.yusys.yusp.oca.domain.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @description: 类说明
 * @author: zhangsong
 * @date: 2021/3/31
 */
@Data
public class LoginTimeStrategyDto implements Serializable {
    private String weekdays;
    private String startTime;
    private String endTime;
}
