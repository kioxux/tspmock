package cn.com.yusys.yusp.oca.service.impl;


import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.common.utils.Query;
import cn.com.yusys.yusp.commons.session.user.Control;
import cn.com.yusys.yusp.commons.session.user.impl.ControlImpl;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.oca.dao.AdminSmResContrDao;
import cn.com.yusys.yusp.oca.domain.entity.*;
import cn.com.yusys.yusp.oca.domain.form.AdminSmMenuConditionForm;
import cn.com.yusys.yusp.oca.domain.form.AdminSmResContrForm;
import cn.com.yusys.yusp.oca.domain.form.AdminSmResContrSaveForm;
import cn.com.yusys.yusp.oca.domain.vo.*;
import cn.com.yusys.yusp.oca.service.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


/**
 * @author xufy1
 */
@Slf4j
@Service("adminSmResContrService")
public class AdminSmResContrServiceImpl extends ServiceImpl<AdminSmResContrDao, AdminSmResContrEntity> implements AdminSmResContrService {

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    AdminSmFuncModService adminSmFuncModService;
    @Autowired
    AdminSmBusiFuncService adminSmBusiFuncService;
    @Autowired
    AdminSmDataAuthService adminSmDataAuthService;
    @Autowired
    AdminSmAuthRecoService adminSmAuthRecoService;
    @Autowired
    AdminSmResContrService adminSmResContrService;
    @Autowired
    AdminSmDataAuthTmplService adminSmDataAuthTmplService;
    @Autowired
    AdminSmMenuService adminSmMenuService;

    @Override
    public List<ControlImpl> getAdminSmContr(String userId) {
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StringUtils.nonEmpty(userId), "t_user.USER_ID", userId);
        List<ControlImpl> contrVoList = this.baseMapper.getAdminSmContrList(queryWrapper);
        return contrVoList;
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AdminSmResContrEntity> page = this.page(
                new Query<AdminSmResContrEntity>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

    /**
     * 查询控制点列表
     *
     * @param form
     * @return
     */
    @Override
    public Page<AdminSmResContrVo> queryPageWithCondition(AdminSmResContrForm form) {
        Page<AdminSmResContrVo> pageVo = new Page<>(form.getPage(), form.getSize());
        if (!StringUtils.isEmpty(form.getMenuId()) && StringUtils.isEmpty(form.getFuncId())) {
            return pageVo;
        }
        QueryWrapper<AdminSmResContrVo> wrapper = new QueryWrapper<>();
        String keyWord = form.getKeyWord();
        String funcId = form.getFuncId();
        if (StringUtils.isEmpty(keyWord)) {
            String contrName = form.getContrName();
            String contrCode = form.getContrCode();
            String contrUrl = form.getContrUrl();
            wrapper.eq(!StringUtils.isEmpty(funcId), "c.func_id", funcId);
            wrapper.like(!StringUtils.isEmpty(contrName), "c.CONTR_NAME", contrName);
            wrapper.like(!StringUtils.isEmpty(contrCode), "c.CONTR_CODE", contrCode);
            wrapper.like(!StringUtils.isEmpty(contrUrl), "c.CONTR_URL", contrUrl);
        } else {
            wrapper.eq(!StringUtils.isEmpty(funcId), "c.func_id", funcId);
            wrapper.like("c.CONTR_NAME", keyWord).or();
            wrapper.like("c.CONTR_CODE", keyWord).or();
            wrapper.like("c.CONTR_URL", keyWord).or();
            wrapper.like("m.menu_name", keyWord);
        }
        this.baseMapper.queryPageWithCondition(pageVo, wrapper);
        List<AdminSmResContrVo> records = pageVo.getRecords();
        return pageVo;
    }

    /**
     * 获取业务功能树
     *
     * @return list
     */
    @Override
    public List<AdminSmContrTreeVo> getFuncTree() {

        List<AdminSmBusiFuncEntity> funcEntityList = adminSmBusiFuncService.list();
        List<AdminSmFuncModEntity> modEntityList = adminSmFuncModService.list();
        List<AdminSmContrTreeVo> treeList = new ArrayList<>();

        funcEntityList.forEach(func -> {
            func.setNodeId(func.getFuncId());
            func.setNodeName(func.getFuncName());
            func.setUpTreeId(func.getModId());
        });
        Map<String, List<AdminSmContrTreeVo>> funcGroupByMod = funcEntityList.stream().collect(Collectors.groupingBy(func -> func.getModId(), Collectors.toList()));

        modEntityList.forEach(mod -> {
            mod.setNodeId(mod.getModId());
            mod.setNodeName(mod.getModName());
            mod.setUpTreeId("0");
            mod.setChildren(funcGroupByMod.get(mod.getModId()));
            treeList.add(mod);
        });

        return treeList;
    }

    /**
     * 批量删除 控制点
     * 删除控制点时，需要同时删除与数据权限相关联的admin_sm_data_auth表记录
     * 但是，权限模板是不删除的
     *
     * @param ids ids 逗号，分隔
     * @return int
     */
    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void deleteContr(String[] ids) {
        if (ids != null && ids.length != 0) {
            /**
             * 获取将要删除控制点下已经关联的数据权限模板 IdList
             */
            List<AdminSmDataAuthEntity> dataAuthEntityList = adminSmDataAuthService.getListByContrIds(Arrays.asList(ids));
            List<String> dataAuthIdList = dataAuthEntityList.stream().map(AdminSmDataAuthEntity::getAuthTmplId).collect(Collectors.toList());
            List<String> contrIdList = Arrays.asList(ids);
            /**
             * 删除admin_sm_data_auth关联表记录
             */
            adminSmDataAuthService.deleteByContrIds(ids);
            /**
             * 删除控制点
             */
            this.baseMapper.deleteBatchIds(contrIdList);
            /**
             * 删除控制点授权，同时删除控制点关联的数据权限模板授权
             */
            adminSmAuthRecoService.deleteByResContrList(contrIdList, dataAuthIdList);
        }
    }

    /**
     * 验证contrCode是否重复
     *
     * @param adminSmResContrForm
     * @return
     */
    @Override
    public int checkCode(AdminSmResContrForm adminSmResContrForm) {
        String contrId = adminSmResContrForm.getContrId();
        String contrCode = adminSmResContrForm.getContrCode();
        String funcId = adminSmResContrForm.getFuncId();
        if (!StringUtils.isEmpty(contrId)) {
            AdminSmResContrEntity entity = this.getById(contrId);
            if (entity != null) {
                return 0;
            }
        }
        if (StringUtils.isEmpty(funcId) || StringUtils.isEmpty(contrCode)) {
            return -1;
        }
        QueryWrapper<AdminSmResContrEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("CONTR_CODE", contrCode);
        wrapper.eq("FUNC_ID", funcId);
        List<AdminSmResContrEntity> list = this.baseMapper.selectList(wrapper);
        return list.size();
    }

    /**
     * 新增控制点
     *
     * @param adminSmResContrSaveForm
     */
    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void createContr(AdminSmResContrSaveForm adminSmResContrSaveForm) {
        if (StringUtils.isEmpty(adminSmResContrSaveForm.getContrId())) {
            adminSmResContrSaveForm.setContrId(StringUtils.getUUID());
        }
        AdminSmResContrEntity adminSmResContrEntity = BeanUtils.beanCopy(adminSmResContrSaveForm, AdminSmResContrEntity.class);
        /**
         * 控制点关联数据模板
         */
        relationTmpl(adminSmResContrSaveForm, false);
        /**
         * 所有新增控制点，自动给id为 40 的系统管理员分配；
         */
        this.save(adminSmResContrEntity);
        AdminSmAuthRecoEntity adminSmAuthRecoEntity = getAuthEntityWithContr(adminSmResContrSaveForm);
        adminSmAuthRecoService.save(adminSmAuthRecoEntity);
    }

    private AdminSmAuthRecoEntity getAuthEntityWithContr(AdminSmResContrSaveForm form) {
        AdminSmAuthRecoEntity entity = new AdminSmAuthRecoEntity();
        String sysId = StringUtils.isEmpty(SessionUtils.getClientId()) ? "test" : SessionUtils.getClientId();
        entity.setAuthRecoId(StringUtils.getUUID());
        entity.setSysId(sysId);
        entity.setLastChgUsr(SessionUtils.getUserId());
        entity.setAuthresId(form.getContrId());
        entity.setAuthresType("C");
        entity.setAuthobjType("R");
        entity.setAuthobjId("cc81a8d86f274c81bc680a0bbd27e358");
        entity.setMenuId(form.getMenuId());
        return entity;
    }

    private AdminSmResContrEntity getAdminSmResContrEntity(AdminSmResContrSaveForm form) {
        AdminSmResContrEntity entity = new AdminSmResContrEntity();
        if (StringUtils.isEmpty(form.getContrId())) {
            form.setContrId(StringUtils.getUUID());
        }
        entity.setContrId(form.getContrId());
        entity.setLastChgUsr(SessionUtils.getUserId());
        entity.setContrCode(form.getContrCode());
        entity.setContrName(form.getContrName());
        entity.setContrRemark(form.getContrRemark());
        entity.setContrUrl(form.getContrUrl());
        entity.setFuncId(form.getFuncId());
        entity.setMethodType(form.getMethodType());
        return entity;
    }

    /**
     * 修改控制点
     *
     * @param adminSmResContrSaveForm
     */
    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateContr(AdminSmResContrSaveForm adminSmResContrSaveForm) {
        AdminSmResContrEntity adminSmResContrEntity = getAdminSmResContrEntity(adminSmResContrSaveForm);
        this.updateById(adminSmResContrEntity);
        /**
         * 关联控制点和数据模板
         * 删除原有授权记录
         * 给系统管理员 40 授权新数据模板
         */
        relationTmpl(adminSmResContrSaveForm, true);
    }

    /**
     * 联合admin_sm_auth_reco表，查询权限树
     *
     * @param id
     * @return
     */
    @Override
    public List<AdminSmAuthTreeVo> selectAuthTree(String id) {
        List<AdminSmAuthTreeVo> treeVoList = this.baseMapper.selectAuthTree(id);
        return treeVoList;
    }

    /**
     * 分页查询权限表中的控制点数据
     *
     * @param id
     * @param page
     * @param size
     * @return
     */
    @Override
    public IPage<AdminSmAuthTreeVo> selectAuthTreePage(String id, String keyWord, int page, int size) {
        IPage<AdminSmAuthTreeVo> result = new Page<>(Long.valueOf(page), Long.valueOf(size));
        QueryWrapper<AdminSmAuthTreeVo> wrapper = new QueryWrapper<>();
        wrapper.eq("r.authobj_id", id);
        wrapper.like(!StringUtils.isEmpty(keyWord), "c.contr_name", keyWord);
        result = baseMapper.selectAuthTreePage(result, wrapper);
        return result;
    }

    /**
     * 控制点关联数据权限模板操作
     *
     * @param adminSmResContrSaveForm
     */
    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public String relationTmpl(AdminSmResContrSaveForm adminSmResContrSaveForm, boolean createOrUpdate) {
        List<String> authDataTmplIdList = adminSmResContrSaveForm.getAuthDataTmplIdList();
        /**
         * 0、判断修改的控制点模板是否能修改删除
         */
        List<AdminRecoWithTmplVo> recoTmplVoList = adminSmAuthRecoService.getByMenuIdWithTmpl(adminSmResContrSaveForm.getContrId());
        StringBuffer stringBuffer = new StringBuffer();
        if (createOrUpdate && recoTmplVoList != null && recoTmplVoList.size() > 0) {
            // flag：是否有不能修改数据模板
            boolean flag = false;
            stringBuffer.append("这些数据模板已授权，不能取消关联！\n");
            if (authDataTmplIdList == null || authDataTmplIdList.size() == 0) {
                flag = true;
                for (AdminRecoWithTmplVo vo : recoTmplVoList) {
                    stringBuffer.append("已被授权的数据模板名称：" + vo.getAuthTmplName() + "\n");
                }

            } else {
                for (AdminRecoWithTmplVo vo : recoTmplVoList) {
                    if (!authDataTmplIdList.contains(vo.getAuthTmplId())) {
                        flag = true;
                        stringBuffer.append("已被授权的数据模板名称：" + vo.getAuthTmplName());
                    }
                }
            }
            if (flag) {
                stringBuffer.append("如需修改关联的数据模板，请先删除相关的授权记录！");
                return stringBuffer.toString();
            }
        }
        /**
         * 1、如果是修改已关联的模板，首先修改之前关联模板数据的status状态码
         */
        List<AdminSmDataAuthTmplEntity> tmplEntityList = new ArrayList<>();
        if (createOrUpdate) {
            tmplEntityList = adminSmDataAuthTmplService.getByContrId(adminSmResContrSaveForm.getContrId());
            if (tmplEntityList != null && tmplEntityList.size() > 0) {
                tmplEntityList.stream().forEach(entitiy -> {
                    entitiy.setStatus(0);
                });
            }
        }
        /**
         * 2、删除之前关联模板的数据
         */
        String[] ids = {adminSmResContrSaveForm.getContrId()};
        adminSmDataAuthService.deleteByContrIds(ids);
        /**
         * 3、添加新的关联模板数据
         */
        if (authDataTmplIdList != null && authDataTmplIdList.size() > 0) {
            Set<AdminSmDataAuthEntity> authEntityList = new HashSet<>();
            for (String tmplId : authDataTmplIdList) {
                AdminSmDataAuthEntity entity = new AdminSmDataAuthEntity();
                entity.setAuthId(StringUtils.getUUID());
                entity.setAuthTmplId(tmplId);
                entity.setContrId(adminSmResContrSaveForm.getContrId());
                entity.setLastChgUsr(SessionUtils.getUserId());
                authEntityList.add(entity);
                AdminSmDataAuthTmplEntity tmplEntity = new AdminSmDataAuthTmplEntity();
                tmplEntity.setAuthTmplId(tmplId);
                tmplEntity.setStatus(1);
                tmplEntityList.add(tmplEntity);
            }
            adminSmDataAuthService.saveBatch(authEntityList);
        }
        /**
         * 4、修改新关联数据模板的关联状态
         */
        if (tmplEntityList != null && tmplEntityList.size() > 0) {
            adminSmDataAuthTmplService.updateBatchById(tmplEntityList);
        }
        return "修改关联成功！";
    }

    /**
     * 查询控制点关联菜单列表
     *
     * @param adminSmMenuConditionForm
     * @return
     */
    @Override
    public Page<AdminSmMenuVo> getMenuList(AdminSmMenuConditionForm adminSmMenuConditionForm) {
        return adminSmMenuService.getMenuListForContr(adminSmMenuConditionForm);
    }

    /**
     * 缓存中添加 userId 现有的控制点
     *
     * @return
     */
    @Override
    public List<? extends Control> selectControlImplList() {
        return this.list().stream().map(c -> {
            ControlImpl control = new ControlImpl();
            BeanUtils.beanCopy(c, control);
            return control;
        }).collect(Collectors.toList());
    }


}