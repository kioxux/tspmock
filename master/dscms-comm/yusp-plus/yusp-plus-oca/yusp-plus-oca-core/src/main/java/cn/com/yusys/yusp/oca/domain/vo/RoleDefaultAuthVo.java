package cn.com.yusys.yusp.oca.domain.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author danyu
 */
@Data
public class RoleDefaultAuthVo {

    /**
     * 数据权限记录id
     */
    private String authId;

    /**
     * 权限模板编号
     */
    private String authTmplId;

    /**
     * 最后修改日志
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

    /**
     * 操作员姓名
     */
    private String userName;

    /**
     * 数据权限模板名
     */
    private String authTmplName;

    /**
     * 数据权限SQL条件
     */
    private String sqlString;
}
