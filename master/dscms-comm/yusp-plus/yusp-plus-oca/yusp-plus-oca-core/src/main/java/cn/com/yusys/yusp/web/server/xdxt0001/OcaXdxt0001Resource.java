package cn.com.yusys.yusp.web.server.xdxt0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.OcaTradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxt0001.req.Xdxt0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0001.resp.Xdxt0001DataRespDto;
import cn.com.yusys.yusp.enums.OcaTradeEnum;
import cn.com.yusys.yusp.service.server.xdxt0001.Xdxt0001Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:信贷同步人力资源
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0001:信贷同步人力资源")
@RestController
@RequestMapping("/api/ocaxt4bsp")
public class OcaXdxt0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(OcaXdxt0001Resource.class);

    @Autowired
    private Xdxt0001Service xdxt0001Service;
    /**
     * 交易码：xdxt0001
     * 交易描述：信贷同步人力资源
     *
     * @param xdxt0001DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("信贷同步人力资源")
    @PostMapping("/xdxt0001")
    protected @ResponseBody
    ResultDto<Xdxt0001DataRespDto> xdxt0001(@Validated @RequestBody Xdxt0001DataReqDto xdxt0001DataReqDto) throws Exception {
        logger.info(OcaTradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0001.key, OcaTradeEnum.TRADE_CODE_XDXT0001.value, JSON.toJSONString(xdxt0001DataReqDto));
        Xdxt0001DataRespDto xdxt0001DataRespDto = new Xdxt0001DataRespDto();// 响应Dto:信贷同步人力资源
        ResultDto<Xdxt0001DataRespDto> xdxt0001DataResultDto = new ResultDto<>();
        try {
            // 从xdxt0001DataReqDto获取业务值进行业务逻辑处理
            logger.info(OcaTradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0001.key, OcaTradeEnum.TRADE_CODE_XDXT0001.value, JSON.toJSONString(xdxt0001DataReqDto));
            xdxt0001DataRespDto = xdxt0001Service.getXdxt0001(xdxt0001DataReqDto);
            logger.info(OcaTradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0001.key, OcaTradeEnum.TRADE_CODE_XDXT0001.value, JSON.toJSONString(xdxt0001DataRespDto));
            // 封装xdxt0001DataResultDto中正确的返回码和返回信息
            xdxt0001DataResultDto.setCode(OcaTradeEnum.CMIS_SUCCSESS.key);
            xdxt0001DataResultDto.setMessage(OcaTradeEnum.CMIS_SUCCSESS.value);
        }catch (BizException e) {
            logger.info(OcaTradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0001.key, OcaTradeEnum.TRADE_CODE_XDXT0001.value, e.getMessage());
            // 封装xdkh0001DataResultDto中异常返回码和返回信息
            xdxt0001DataResultDto.setCode(e.getErrorCode());
            xdxt0001DataResultDto.setMessage(e.getMessage());
        }  catch (Exception e) {
            logger.info(OcaTradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0001.key, OcaTradeEnum.TRADE_CODE_XDXT0001.value, e.getMessage());
            // 封装xdxt0001DataResultDto中异常返回码和返回信息
            xdxt0001DataResultDto.setCode(OcaTradeEnum.EPB099999.key);
            xdxt0001DataResultDto.setMessage(OcaTradeEnum.EPB099999.value);
        }
        // 封装xdxt0001DataRespDto到xdxt0001DataResultDto中
        xdxt0001DataResultDto.setData(xdxt0001DataRespDto);
        logger.info(OcaTradeLogConstants.RESOURCE_END_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0001.key, OcaTradeEnum.TRADE_CODE_XDXT0001.value, JSON.toJSONString(xdxt0001DataResultDto));
        return xdxt0001DataResultDto;
    }
}
