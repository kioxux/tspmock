package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.oca.config.validation.Insert;
import cn.com.yusys.yusp.oca.config.validation.Update;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserDutyRelEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserMgrOrgEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserRoleRelEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmPasteUserQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmUserDutyRelQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmUserQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmUserRoleRelQuery;
import cn.com.yusys.yusp.oca.domain.vo.*;
import cn.com.yusys.yusp.oca.service.AdminSmOrgService;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 系统用户表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-11-16 15:31:54
 */
@RestController
@RequestMapping("/api/adminsmuser")
public class AdminSmUserController {

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    /**
     * 分页查询orgId下所有用户列表,按最后修改时间降序
     */
    @GetMapping("/page")
    public ResultDto<AdminSmUserVo> page(AdminSmUserQuery query) {
        return ResultDto.success(adminSmUserService.queryPage(query));
    }

    /**
     * 分页查询orgId下所有用户列表,按最后修改时间降序
     */
    @GetMapping("/pageforxw")
    public ResultDto<AdminSmUserVo> pageforxw(AdminSmUserQuery query) {
        return ResultDto.success(adminSmUserService.queryPageForXw(query));
    }

    /**
     * 分页查询orgId下所有用户列表,按最后修改时间降序 (贷后专用-适配角色数据授权)
     */
    @GetMapping("/pageForDh")
    public ResultDto<AdminSmUserVo> pageForDh(AdminSmUserQuery query) {
        return ResultDto.success(adminSmUserService.queryPageForDh(query));
    }

    /**
     * 分页查询orgId下所有用户列表,按最后修改时间降序
     */
    @GetMapping("/pageAll")
    public ResultDto<AdminSmUserVo> pageAll(AdminSmUserQuery query) {
        return ResultDto.success(adminSmUserService.queryPageAll(query));
    }
    /**
     * 详情
     */
    @GetMapping("/info/{userId}")
    public ResultDto<AdminSmUserDetailVo> info(@PathVariable("userId") String userId) {
        return ResultDto.success(adminSmUserService.getDetailById(userId));
    }

    /**
     * 新增用户
     *
     * @return
     */
    @PostMapping("/add")
    public ResultDto<Objects> add(@RequestBody @Validated({Insert.class}) AdminSmUserEntity entity) {
        adminSmUserService.save(entity);
        return ResultDto.successMessage("新增成功");
    }

    /**
     * 修改用户
     *
     * @return
     */
    @PostMapping("/update")
    public ResultDto<Object> update(@RequestBody @Validated({Update.class}) AdminSmUserEntity entity) {
        adminSmUserService.updateById(entity);
        return ResultDto.successMessage("修改成功");
    }

    /**
     * 批量启用
     */
    @PostMapping("/batchenable")
    public ResultDto<Object> batchEnable(@RequestBody @NotNull String[] ids) {
        adminSmUserService.batchEnable(ids);
        return ResultDto.successMessage("启用成功");
    }

    /**
     * 批量停用
     */
    @PostMapping("/batchdisable")
    public ResultDto<Object> batchDisable(@RequestBody @NotNull String[] ids) {
        adminSmUserService.batchDisable(ids);
        return ResultDto.successMessage("停用成功");
    }

    /**
     * 批量删除
     */
    @PostMapping("/batchdelete")
    public ResultDto<Object> batchDelete(@RequestBody @NotNull String[] ids) {
        adminSmUserService.batchDelete(ids);
        return ResultDto.successMessage("删除成功");
    }

    /**
     * 查询用户所在机构及其所有上级机构下可选的所有角色列表
     *
     * @param query
     * @return 附带该用户与这些角色的关联状态
     */
    @GetMapping("/queryuserrole")
    public ResultDto<UserRoleRelVo> getUserRoleList(AdminSmUserRoleRelQuery query) {
        return ResultDto.success(adminSmUserService.getUserRoleList(query));
    }

    /**
     * 批量保存用户角色关联数据
     *
     * @param userId 被关联的用户id
     * @param ids    将要关联的角色Ids
     * @return
     */
    @PostMapping("/saveuserrole/{userId}")
    public ResultDto<Object> saveUserRoleRelList(@PathVariable() String userId, @RequestBody(required = false) String[] ids) {
        List<String> roleIds = new ArrayList<>();
        if (CollectionUtils.nonEmpty(ids)) {
            roleIds = Arrays.asList(ids);
        }
        List<AdminSmUserRoleRelEntity> list = roleIds.stream().map(roleId -> new AdminSmUserRoleRelEntity(userId, roleId)).collect(Collectors.toList());
        adminSmUserService.saveUserRoleList(userId, list);
        return ResultDto.successMessage("保存成功");
    }

    /**
     * 查询用户所在机构及其所有上级机构下可选的所有岗位列表
     *
     * @param query
     * @return 附带该用户与这些岗位的关联状态
     */
    @GetMapping("/queryuserduty")
    public ResultDto<UserDutyRelVo> getUserDutyList(AdminSmUserDutyRelQuery query) {
        return ResultDto.success(adminSmUserService.getUserDutyList(query));
    }

    /**
     * 批量保存用户岗位关联数据
     *
     * @param userId 被关联的用户id
     * @param ids    将要关联的岗位Ids
     * @return
     */
    @PostMapping("/saveuserduty/{userId}")
    public ResultDto<Object> saveUserDutyRelList(@PathVariable() String userId, @RequestBody(required = false) String[] ids) {
        List<String> dutyIds = new ArrayList<>();
        if (CollectionUtils.nonEmpty(ids)) {
            dutyIds = Arrays.asList(ids);
        }
        List<AdminSmUserDutyRelEntity> list = dutyIds.stream().map(dutyId -> new AdminSmUserDutyRelEntity(userId, dutyId)).collect(Collectors.toList());
        adminSmUserService.saveUserDutyList(userId, list);
        return ResultDto.successMessage("保存成功");
    }

    /**
     * 查询用户可授权机构树
     *
     * @param userId
     * @return 附带该用户与这些机构的授权状态
     */
    @GetMapping("/queryuserorg")
    public ResultDto<UserMgrOrgVo> queryUserMgrOrgList(@RequestParam String userId) {
        List<UserMgrOrgVo> list = adminSmUserService.getUserMgrOrgList(userId);
        return ResultDto.success(list);
    }

    /**
     * 批量保存用户授权机构数据
     *
     * @param userId 被授权的用户id
     * @param ids    将要授权的机构Ids
     * @return
     */
    @PostMapping("/saveusermgrorg/{userId}")
    public ResultDto<Object> saveUserMgrOrg(@PathVariable() String userId, @RequestBody(required = false) String[] ids) {
        List<String> orgIds = new ArrayList<>();
        if (CollectionUtils.nonEmpty(ids)) {
            orgIds = Arrays.asList(ids);
        }
        List<AdminSmUserMgrOrgEntity> list = orgIds.stream().map(orgId -> new AdminSmUserMgrOrgEntity(userId, orgId)).collect(Collectors.toList());
        adminSmUserService.saveUserMgrOrg(userId, list);
        return ResultDto.successMessage("保存成功");
    }

    /**
     * 功能授权 粘贴用
     * 分页查询当前用户有权访问的所有用户列表,排除指定userId，按最后修改时间降序
     */
    @GetMapping("/waitpasteuserpage")
    public ResultDto<AdminSmUserVo> page(@Validated AdminSmPasteUserQuery query) {
        return ResultDto.success(adminSmUserService.queryPageExcept(query));
    }

    /**
     * 通过登录账号查询用户
     *
     * @param loginCode 账号
     * @return
     */
    @PostMapping("/getbylogincode")
    public ResultDto<AdminSmUserDto> getByLoginCode(@RequestBody String loginCode) {
        return ResultDto.success(adminSmUserService.getByLoginCode(loginCode));
    }

    /**
     * 通过多条登录账号查询多个用户
     *
     * @param loginCodes 账号列表
     * @return
     */
    @PostMapping("/getbylogincodelist")
    public ResultDto<List<AdminSmUserDto>> getByLoginCodeList(@RequestBody List<String> loginCodes) {
        List<AdminSmUserDto> adminSmUserDtoList = adminSmUserService.getByLoginCodeList(loginCodes);
        return ResultDto.success(adminSmUserDtoList);
    }

    /**
     * 通过姓名模糊查询多个用户
     *
     * @param name 姓名
     * @return
     */
    @PostMapping("/getbyname")
    public ResultDto<List<AdminSmUserDto>> getByName(@RequestBody String name) {
        List<AdminSmUserDto> adminSmUserDtoList = adminSmUserService.getByName(name);
        return ResultDto.success(adminSmUserDtoList);
    }

    /**
     * 通过是否小微客户经理
     *
     * @param loginCode 用户账号
     * @return
     */
    @PostMapping("/getisxwuserbylogincode")
    public ResultDto<GetIsXwUserDto> getIsXWUserByLoginCode(@RequestBody String loginCode) {
        GetIsXwUserDto getIsXwUserDto = adminSmUserService.getIsXWUserByLoginCode(loginCode);
        return ResultDto.success(getIsXwUserDto);
    }

    /**
     * 通过机构号查询用户信息
     *
     * @param orgId 机构号
     * @return
     */
    @PostMapping("/getbyorgid")
    public ResultDto<AdminSmUserDto> getByOrgid(@RequestBody String orgId) {
        List<AdminSmUserDto> adminSmUserDtoList = adminSmUserService.getByOrgid(orgId);
        return ResultDto.success(adminSmUserDtoList);
    }

    /**
     * 通过机构号查询查询用户和岗位信息
     *
     * @param userAndDutyReqDto
     * @return
     */
    @PostMapping("/getuserandduty")
    public ResultDto<List<UserAndDutyRespDto>> getUserAndDuty(@RequestBody UserAndDutyReqDto userAndDutyReqDto) {
        List<UserAndDutyRespDto> userAndDutyRespDtos = adminSmUserService.getUserAndDuty(userAndDutyReqDto);
        return ResultDto.success(userAndDutyRespDtos);
    }

    /**
     * 通用客户经理查询
     *
     * @param commonUserQueryReqDto
     * @return
     */
    @PostMapping("/getcommonuserinfo")
    public ResultDto<List<CommonUserQueryRespDto>> getCommonUserInfo(@RequestBody CommonUserQueryReqDto commonUserQueryReqDto) {
        List<CommonUserQueryRespDto> commonUserQueryRespDtos = adminSmUserService.getCommonUserInfo(commonUserQueryReqDto);
        return ResultDto.success(commonUserQueryRespDtos);
    }


    /**
     * 通过流程实例号获取当前处理用户的详细信息
     *
     * @param instanceId 流程实例
     * @return
     */
    @PostMapping("/instance/todouserinfo")
    public ResultDto<AdminSmUserDto> getTodoUserInfo(@RequestBody String instanceId) {
        return ResultDto.success(adminSmUserService.getTodoUserInfo(instanceId));
    }

    /**
     * @函数名称:queryCusFlowCount
     * @函数描述:查询在途任务数量，公共API接口
     * @参数与返回说明:cusId
     * @算法描述:
     */
    @PostMapping("/queryCusFlowCount")
    protected ResultDto<Integer> queryCusFlowCount(@RequestBody String cusId) {
        int result = adminSmUserService.queryCusFlowCount(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 查询角色下的用户-提供给后端
     */
    @PostMapping("/getuserlist")
    public ResultDto<List<AdminSmUserDto>> getUserList(@RequestBody GetUserInfoByOrgCodeDto getUserInfoByOrgCodeDto) {
        List<AdminSmUserDto> adminSmUserDtos = adminSmUserService.getUserList(getUserInfoByOrgCodeDto);
        return ResultDto.success(adminSmUserDtos);
    }
}