package cn.com.yusys.yusp.oca.domain.entity;

import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.config.validation.Insert;
import cn.com.yusys.yusp.oca.config.validation.Update;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * 系统角色表
 * 
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-18 18:06:35
 */
@Data
@TableName("admin_sm_role")
@NoArgsConstructor
public class AdminSmRoleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 记录编号
	 */
	@TableId(type = IdType.UUID)
	@NotBlank(groups = Update.class,message="roleId can not be empty!")
	private String roleId;
	/**
	 * 角色代码
	 */
	@NotBlank(groups = Insert.class,message="roleCode can not be empty!")
	private String roleCode;
	/**
	 * 角色名称
	 */
	@NotBlank(groups = Insert.class,message="roleName can not be empty!")
	private String roleName;
	/**
	 * 所属机构编号
	 */
	@NotBlank(groups = Insert.class,message="orgId can not be empty!")
	private String orgId;
	/**
	 * 角色层级
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	private Integer roleLevel;
	/**
	 * 状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效
	 */
	private AvailableStateEnum roleSts;
	/**
	 * 最新变更用户
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
	/**
	 * 最新变更时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

}
