package cn.com.yusys.yusp.notice.service;


import cn.com.yusys.yusp.notice.entity.AdminSmRicheditFileInfoEntity;
import cn.com.yusys.yusp.notice.form.AdminSmRicheditFileInfoForm;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2021-03-31 11:00:50
 */
public interface AdminSmRicheditFileInfoService extends IService<AdminSmRicheditFileInfoEntity> {

    /**
     * 新增文件数据
     * @param fileInfoFormList
     */
    void addFileInfo(List<AdminSmRicheditFileInfoForm> fileInfoFormList);

    /**
     * 根据业务编号获取文件信息
     * @param noticeId
     * @return
     */
    List<AdminSmRicheditFileInfoEntity> getFileByBusNo(String noticeId);

    /**
     * 删除文件信息
     * @param noticeId
     */
    void deleteByBusNo(String ... noticeId);

    /**
     * 批量删除富文本文件数据及oss中的文件
     * @param fileInfoFormList
     */
    void deleteFileInfo(List<AdminSmRicheditFileInfoForm> fileInfoFormList);
}

