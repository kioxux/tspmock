package cn.com.yusys.yusp.web.server.xdxt0014;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.OcaTradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxt0014.req.Xdxt0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0014.resp.Xdxt0014DataRespDto;
import cn.com.yusys.yusp.enums.OcaTradeEnum;
import cn.com.yusys.yusp.service.server.xdxt0014.Xdxt0014Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 接口处理类:字典项对象通用列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0014:字典项对象通用列表查询")
@RestController
@RequestMapping("/api/ocaxt4bsp")
public class OcaXdxt0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(OcaXdxt0014Resource.class);

    @Autowired
    private Xdxt0014Service xdxt0014Service;
    /**
     * 交易码：xdxt0014
     * 交易描述：字典项对象通用列表查询
     *
     * @param xdxt0014DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("字典项对象通用列表查询")
    @PostMapping("/xdxt0014")
    protected @ResponseBody
    ResultDto<Xdxt0014DataRespDto> xdxt0014(@Validated @RequestBody Xdxt0014DataReqDto xdxt0014DataReqDto) throws Exception {
        logger.info(OcaTradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0014.key, OcaTradeEnum.TRADE_CODE_XDXT0014.value, JSON.toJSONString(xdxt0014DataReqDto));
        Xdxt0014DataRespDto xdxt0014DataRespDto = new Xdxt0014DataRespDto();// 响应Dto:字典项对象通用列表查询
        ResultDto<Xdxt0014DataRespDto> xdxt0014DataResultDto = new ResultDto<>();
        try {
            // 从xdxt0014DataReqDto获取业务值进行业务逻辑处理
            logger.info(OcaTradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0014.key, OcaTradeEnum.TRADE_CODE_XDXT0014.value, JSON.toJSONString(xdxt0014DataReqDto));
            xdxt0014DataRespDto = xdxt0014Service.getXdxt0014(xdxt0014DataReqDto);
            logger.info(OcaTradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0014.key, OcaTradeEnum.TRADE_CODE_XDXT0014.value, JSON.toJSONString(xdxt0014DataRespDto));

            // 封装xdxt0014DataResultDto中正确的返回码和返回信息
            xdxt0014DataResultDto.setCode(OcaTradeEnum.CMIS_SUCCSESS.key);
            xdxt0014DataResultDto.setMessage(OcaTradeEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(OcaTradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0014.key, OcaTradeEnum.TRADE_CODE_XDXT0014.value, e.getMessage());
            // 封装xdxt0014DataResultDto中异常返回码和返回信息
            xdxt0014DataResultDto.setCode(OcaTradeEnum.EPB099999.key);
            xdxt0014DataResultDto.setMessage(OcaTradeEnum.EPB099999.value);
        }
        // 封装xdxt0014DataRespDto到xdxt0014DataResultDto中
        xdxt0014DataResultDto.setData(xdxt0014DataRespDto);
        logger.info(OcaTradeLogConstants.RESOURCE_END_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0014.key, OcaTradeEnum.TRADE_CODE_XDXT0014.value, JSON.toJSONString(xdxt0014DataResultDto));
        return xdxt0014DataResultDto;
    }
}
