package cn.com.yusys.yusp.message.dao;

import cn.com.yusys.yusp.message.entity.MessageContentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 消息发送具体内容
 *
 * @author xiaodg@yusys.com.cn
 */

public interface MessageContentDao extends BaseMapper<MessageContentEntity> {

}
