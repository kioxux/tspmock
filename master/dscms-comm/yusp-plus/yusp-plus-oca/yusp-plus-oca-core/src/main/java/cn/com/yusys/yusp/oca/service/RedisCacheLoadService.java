package cn.com.yusys.yusp.oca.service;

/**
 * 缓存初始化加载
 *
 * @author yangzai
 * @since 2021/4/23
 **/
public interface RedisCacheLoadService {

    /**
     * 初始化用户信息
     */
    void loadUserInfoData();

    /**
     * 初始化机构信息
     */
    void loadOrgInfoData();

    /**
     * 加载机构的下级机构树
     */
    void loadOrgTreeData();

    /**
     * 初始化部门信息
     */
    void loadDeptInfoData();

    /**
     * 初始化系统提示信息
     */
    void loadSmMessageInfoData();

    /**
     * 初始化字典项信息
     */
    void loadSmLookupDictInfoData();


    /**
     * 加载区域中心负责人下的小微客户经理
     */
    @Deprecated
    void loadAreaXwUserData();

    /**
     * 加载集中作业总行本地机构（除小微）下的客户经理
     */
    void loadJzzyUserData();

    /**
     * 加载小额贷款管理部下的所有小微客户经理
     */
    void loadAllXwUserData();

    /**
     * 加载刷新全系统的redis缓存数据
     */
    void loadAllSystemCacheData();

    /**
     * 加载营业日期
     **/
    void loadOpenDayData();

    /**
     * 加下机构下小微客户经理
     **/
    void loadXwUserForOrg();

    /**
     * 加下机构下非小微用户
     **/
    void loadNotXwUserForOrg();
}