package cn.com.yusys.yusp.service.server.xdxt0001;

import java.util.EnumSet;

public enum SeniorManagerEnum {
    /**
     * 00010001
     */
    SENIOR_MANAGER_00010001("00010001", "季颖"),
    /**
     * 00020006
     */
    SENIOR_MANAGER_00020006("00020006", "吴开"),
    /**
     * 66010932
     */
    SENIOR_MANAGER_66010932("66010932", "顾晓菲"),
    /**
     * 00151252
     */
    SENIOR_MANAGER_00151252("00151252", "杨诗林"),
    /**
     * 00140099
     */
    SENIOR_MANAGER_00140099("00140099", "陆亚明"),
    /**
     * 00064052
     */
    SENIOR_MANAGER_00064052("00064052", "郭卫东"),
    /**
     * 00110085
     */
    SENIOR_MANAGER_00110085("00110085", "孙瑜"),
    /**
     * 20010271
     */
    SENIOR_MANAGER_20010271("20010271", "陈金龙"),
    /**
     * 17010062
     */
    SENIOR_MANAGER_17010062("17010062", "陶怡"),
    /**
     * 75010310
     */
    SENIOR_MANAGER_75010310("75010310", "王辉");

    public String userCode;
    public String userName;

    /**
     * 判断移动OA推送的客户信息是否在枚举列表中
     *
     * @param userCode
     * @return
     * @author 顾银华
     */
    public static boolean isSeniorManager(final String userCode) {
        // 默认当前用户不在当前枚举列表中
        boolean existsFlag = Boolean.FALSE;
        for (SeniorManagerEnum enums : EnumSet.allOf(SeniorManagerEnum.class)) {
            if (enums.userCode.equalsIgnoreCase(userCode)) {
                // 如果匹配，直接跳出
                existsFlag = true;
                break;
            }
        }
        return existsFlag;
    }

    /**
     * 定义带键值对的构造函数
     */
    private SeniorManagerEnum(final String userCode, final String userName) {
        this.userCode = userCode;
        this.userName = userName;
    }
}
