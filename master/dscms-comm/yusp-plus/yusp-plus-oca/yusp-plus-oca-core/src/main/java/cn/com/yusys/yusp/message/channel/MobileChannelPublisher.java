package cn.com.yusys.yusp.message.channel;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxRespDto;
import cn.com.yusys.yusp.message.constant.ReceivedUserTypeEnum;
import cn.com.yusys.yusp.message.entity.MessageContentEntity;
import cn.com.yusys.yusp.message.entity.MessagePoolEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.service.Dscms2DxptClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

/**
 * 发布手机短信消息
 * <p>
 * 需要使用者结合短信平台实现
 *
 * @author xiaodg@yusys.com.cn
 **/
@Component
@Slf4j
public class MobileChannelPublisher implements MessageChannelPublisher {
    private static Logger logger = LoggerFactory.getLogger(MobileChannelPublisher.class);
    @Autowired
    Dscms2DxptClientService dscms2DxptClientService;

    /**
     * 发布消息
     * <p>
     * 发送失败, 抛出异常, 供Spring Retry框架进行重试
     *
     * @param userEntity           用户信息 {@link AdminSmUserEntity}
     * @param messagePoolEntity    消息队列 {@link MessagePoolEntity}
     * @param messageContentEntity 消息内容 {@link MessageContentEntity}
     * @return CompletableFuture<Boolean> 发送是否成功 {@code CompletableFuture<Boolean>}
     */
    @Override
    @Async
    public CompletableFuture<Boolean> publish(MessagePoolEntity messagePoolEntity, MessageContentEntity messageContentEntity, AdminSmUserEntity userEntity) {
        logger.info("发布手机短信消息,消息队列为:[{}],消息内容为:[{}],用户信息为:[{}]开始", JSON.toJSONString(messagePoolEntity, SerializerFeature.WriteMapNullValue),
                JSON.toJSONString(messageContentEntity, SerializerFeature.WriteMapNullValue), JSON.toJSONString(userEntity, SerializerFeature.WriteMapNullValue));
        SenddxReqDto senddxReqDto = new SenddxReqDto();
        try {
            logger.info("发布手机短信消息前组装请求对象前为:[{}]", JSON.toJSONString(senddxReqDto, SerializerFeature.WriteMapNullValue));
            senddxReqDto.setInfopt("dx");
            SenddxReqList senddxReqList = new SenddxReqList();
            if (ReceivedUserTypeEnum.JKR.getCode().equals(messagePoolEntity.getUserType())) {
                senddxReqList.setMobile(messagePoolEntity.getUserAddr());
            } else {
                senddxReqList.setMobile(userEntity.getUserMobilephone());
            }
            senddxReqList.setSmstxt(messageContentEntity.getContent());
            ArrayList<SenddxReqList> list = new ArrayList<>();
            list.add(senddxReqList);
            senddxReqDto.setSenddxReqList(list);
            logger.info("发布手机短信消息前组装请求对象后为:[{}]", JSON.toJSONString(senddxReqDto, SerializerFeature.WriteMapNullValue));
        } catch (Exception e) {
            log.error("发布手机短信消息前组装请求对象失败,异常信息为:[{}] ", e.getMessage());
            return CompletableFuture.completedFuture(false);
        }

        // 调用BSP服务接口，发送消息至短信平台
        try {
            logger.info("发布手机短信消息,短信/微信发送批量接口请求对象为:[{}]", JSON.toJSONString(senddxReqDto, SerializerFeature.WriteMapNullValue));
            ResultDto<SenddxRespDto> result = dscms2DxptClientService.senddx(senddxReqDto);
            logger.info("发布手机短信消息,短信/微信发送批量接口响应对象为:[{}]", JSON.toJSONString(result, SerializerFeature.WriteMapNullValue));

            if ("0".equals(result.getCode())) {
                log.info("发送短信消息: 短信消息发送成功, 接受用户ID: {}, 消息内容: {}", userEntity.getUserId(), messageContentEntity.getContent());
                return CompletableFuture.completedFuture(true);
            } else {
                log.error("发送短信消息: 短信消息发送失败, {}", result.getMessage());
                return CompletableFuture.completedFuture(false);
            }
        } catch (Exception e) {
            log.error("发送短信消息: 短信消息发送失败, ", e);
            return CompletableFuture.completedFuture(false);
        }
    }

}
