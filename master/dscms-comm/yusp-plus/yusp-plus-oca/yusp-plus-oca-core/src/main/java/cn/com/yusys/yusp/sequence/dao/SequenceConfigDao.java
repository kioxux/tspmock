package cn.com.yusys.yusp.sequence.dao;

import cn.com.yusys.yusp.sequence.domain.entity.SequenceConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 序列号模版配置
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2021-04-22 14:43:47
 */
@Mapper
public interface SequenceConfigDao extends BaseMapper<SequenceConfig> {

}
