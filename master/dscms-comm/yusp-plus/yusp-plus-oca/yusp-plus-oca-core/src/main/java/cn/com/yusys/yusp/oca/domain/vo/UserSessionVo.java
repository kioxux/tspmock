package cn.com.yusys.yusp.oca.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 *@program: yusp-plus
 *@description: 根据用户session返回用户vo
 *@author: wujiangpeng
 *@email: wujp4@yusys.com.cn
 *@create: 2020-11-18 15:29
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserSessionVo {

    /**
     * 记录编号
     */
    private String userId;
    /**
     * 账号
     */
    private String loginCode;
    /**
     * 姓名
     */
    private String userName;
    /**
     * 员工号
     */
    private String userCode;
    /**
     * 邮箱
     */
    private String userEmail;
    /**
     * 移动电话
     */
    private String userMobilephone;
    /**
     * 用户头像
     */
    private String userAvatar;
    /**
     * 最近登录时间
     */
    private Date lastLoginTime;
    /**
     * 机构层级
     */
    private String orgLevel;


    private DptSessionVo dpt;

    private DptSessionVo upDpt;

    private List<RoleSessionVo> roles;

    private OrgSessionVo org;

    private OrgSessionVo upOrg;

    private InstuSessionVo instuOrg;

    private LogicSysSessionVo logicSys;

}
