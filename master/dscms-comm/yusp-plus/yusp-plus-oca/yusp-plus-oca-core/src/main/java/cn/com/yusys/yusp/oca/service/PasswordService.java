package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.bo.*;

/**
 * @author JP
 * @Classname PasswordService
 * @Description
 * @Date 2020/12/14 11:34
 * @Created by wujp4@yusys.com.cn
 */
public interface PasswordService {
    /**
     * 校验用户密码
     */
    ResultDto checkPasswrod(PasswordCheckBo passwordCheckBo);

    /**
     * 密码修改
     */
    ResultDto passwordModification(PasswordModifyBo passwordModifyBo);

    /**
     * 密码重置
     */
    ResultDto resetPassword(PasswordResetBo passwordResetBo);

    /**
     * 忘记密码参数校验并发送手机验证码
     */
    ResultDto sendVerificationCode(PasswordForgetAndResetCheckBo passwordForgetAndResetCheckBo);

    /**
     * 忘记密码校验手机验证码
     */
    ResultDto checkVerificationCode(PasswordForgetAndResetCheckBo passwordForgetAndResetCheckBo);

    /**
     * 忘记密码重置密码
     */
    ResultDto forgetAndResetPassword(PasswordForgetAndResetBo passwordForgetAndResetBo);

    /**
     * 密码验证绑定手机号
     */
    ResultDto mobileBinding(MobileBindingBo mobileBindingBo);
}
