package cn.com.yusys.yusp.oca.domain.session;

import cn.com.yusys.yusp.commons.session.user.impl.OrganizationImpl;

/**
 * 机构数据对象
 * @author yangzai
 * @since 2021-06-17
 */
public class CmisOrganizationImpl extends OrganizationImpl {

    /**
     * 机构类别
     */
    private String orgType;

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

}