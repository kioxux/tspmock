package cn.com.yusys.yusp.notice.service;

import java.util.List;

public interface ThirdPartOssService {

    void delete(List<String> filePathList);
}
