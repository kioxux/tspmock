package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * 系统菜单表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-19 16:32:07
 */
@Data
@TableName("admin_sm_menu")
public class AdminSmMenuEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 菜单编号
     */
    @TableId
    private String menuId;
    /**
     * 逻辑系统记录编号
     */
    private String sysId;
    /**
     * 业务功能编号
     */
    @TableField(value = "func_id", updateStrategy = FieldStrategy.IGNORED)
    private String funcId;
    /**
     * 上级菜单编号
     */
    private String upMenuId;
    /**
     * 菜单名称
     */
    @NotBlank(message = "menuName can not be empty!")
    private String menuName;
    /**
     * 顺序
     */
    private Integer menuOrder;
    /**
     * 图标
     */
    private String menuIcon;
    /**
     * 说明(菜单描述)
     */
    private String menuTip;
    /**
     * 国际化key值
     */
    private String i18nKey;
    /**
     * 最新变更用户
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
    /**
     * 最新变更时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;
    /**
     * 菜单分类
     */
    @NotBlank(message = "menuClassify can not be empty!")
    private String menuClassify;
    /**
     * 物理删除 modify by wangyang at 2021-07-12
     */
    // @TableLogic
    private Integer deleted;

}
