package cn.com.yusys.yusp.oca.domain.vo;

import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 系统机构表拓展实体
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-11-27 18:06:35
 */
@Data
public class AdminSmOrgVo {
    private String orgId;
    private String belgMainOrgId;
    private String orgCode;
    private String belgMainOrgName;
    private String orgName;
    private Integer orgLevel;
    private String orgType;
    private AvailableStateEnum orgSts;
    private String instuId;
    private String instuName;
    private AvailableStateEnum instuSts;
    private String upOrgId;
    private String upOrgName;
    private String upOrgCode;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastChgDt;
    private String lastChgUsr;
    private String lastChgName;

}
