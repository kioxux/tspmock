package cn.com.yusys.yusp.oca.domain.form;

import lombok.Data;

import java.util.List;

@Data
public class AdminSmDataTmplConditionForm {
    /**
     * 查询的控制点 id
     */
    private String contrId;
    /**
     * 用于前端复选框反选
     */
    private List<String> lastTmplIds;
    /**
     * 关键字
     */
    private String keyWord;
    /**
     * 1：复选框选择
     * 0：复选框未选
     */
    private int check;

    private int page;

    private int size;
}
