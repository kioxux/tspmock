package cn.com.yusys.yusp.oca.job;

import cn.com.yusys.yusp.oca.service.impl.RedisCacheLoadServicImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @program: yusp-plus
 * @description: 用户信息初始化进redis
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2021-03-02 14:24
 */
@Component
public class OrgLoadTask {

    private Logger logger = LoggerFactory.getLogger(OrgLoadTask.class);

    @Autowired
    private RedisCacheLoadServicImpl redisCacheLoadService;

    public void run(String params) {
        logger.info("orgLoadTask定时任务正在执行，参数为：{}", params);
        redisCacheLoadService.loadOrgInfoData();
    }

}