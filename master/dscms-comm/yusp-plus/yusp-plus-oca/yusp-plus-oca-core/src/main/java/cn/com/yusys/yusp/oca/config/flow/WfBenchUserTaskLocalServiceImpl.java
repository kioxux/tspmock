/**
 * Copyright (C), 2014-2021
 * FileName: WfBenchUserTaskServiceImpl
 * Author: Administrator
 * Date: 2021/3/21 11:12
 * Description:
 * History:
 * <author> <time> <version> <desc>
 * Administrator 11:12 1.0.0 新建类
 */

package cn.com.yusys.yusp.oca.config.flow;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.dto.result.ResultTaskpoolDto;
import cn.com.yusys.yusp.flow.other.Cons;
import cn.com.yusys.yusp.flow.other.enums.UserType;
import cn.com.yusys.yusp.flow.other.exception.WorkflowException;
import cn.com.yusys.yusp.flow.repository.mapper.NWfOcaTaskpoolMapper;
import cn.com.yusys.yusp.flow.repository.mapper.WorkflowMonitorMapper;
import cn.com.yusys.yusp.flow.service.WfBenchUserTaskService;
import cn.com.yusys.yusp.flow.util.WorkFlowUtil;
import cn.com.yusys.yusp.oca.service.AdminSmDutyService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 〈〉
 * @author zhui
 * @create 2021/3/21
 * @since 1.0.0
 */
public class WfBenchUserTaskLocalServiceImpl implements WfBenchUserTaskService {
    @Autowired
    private NWfOcaTaskpoolMapper nWfOcaTaskpoolMapper;
    @Autowired
    private WorkflowMonitorMapper workflowMonitorMapper;
    @Autowired
    private AdminSmDutyService dutyService;

    @Override
    public List<ResultTaskpoolDto> getUserTaskPool(String userId) {
        List<String> dutyCodes = dutyService.getDutysByUserIdForWf(userId);
        if(WorkFlowUtil.isNullOrEmpty(userId)){
            throw new WorkflowException(Cons.ERROR_MSG1);
        }
        List<ResultTaskpoolDto> list = nWfOcaTaskpoolMapper.selectUserTaskPool(dutyCodes);
        return getResultTaskpoolDtos(list);
    }

    @Override
    public List<ResultTaskpoolDto> getUserTaskPoolByModel(String userId,QueryModel model) {
        List<String> dutyCodes = dutyService.getDutysByUserIdForWf(userId);
        if(WorkFlowUtil.isNullOrEmpty(userId)){
            throw new WorkflowException(Cons.ERROR_MSG1);
        }
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ResultTaskpoolDto> list = nWfOcaTaskpoolMapper.selectUserTaskPoolByModel(model.getCondition(),dutyCodes);
        PageHelper.clearPage();
        return getResultTaskpoolDtos(list);
    }

    private List<ResultTaskpoolDto> getResultTaskpoolDtos(List<ResultTaskpoolDto> list) {
        for(ResultTaskpoolDto poolT:list){
            String poolId = poolT.getPoolId();
            if(!poolId.startsWith(UserType.POOL)){
                poolId = UserType.POOL + poolId;
            }
            Integer num = workflowMonitorMapper.getUserTodoNum(poolId);
            if(null == num){
                poolT.setNum(0);
            }else{
                poolT.setNum(num);
            }
        }
        return list;
    }
}
