package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.oca.domain.entity.AdminSmInstuEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmInstuQuery;
import cn.com.yusys.yusp.oca.domain.vo.InstuExtVo;
import cn.com.yusys.yusp.oca.domain.vo.UserSessionVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 金融机构表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-11-19 14:30:22
 */
public interface AdminSmInstuService extends IService<AdminSmInstuEntity> {

    Page<InstuExtVo> queryPage(AdminSmInstuQuery params);

    boolean save(AdminSmInstuEntity entity, UserSessionVo userSessionVo);

    boolean batchEnable(String[] ids);

    boolean batchDisable(String[] ids);

    boolean batchDelete(String[] ids);

    boolean updateById(AdminSmInstuEntity entity);
}

