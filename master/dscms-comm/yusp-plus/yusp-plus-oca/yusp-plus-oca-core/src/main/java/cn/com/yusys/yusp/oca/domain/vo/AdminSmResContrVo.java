package cn.com.yusys.yusp.oca.domain.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class AdminSmResContrVo implements Serializable {
    /**
     * 记录编号
     */
    private String contrId;
    /**
     * 所属业务功能编号
     */
    private String funcId;
    /**
     * 控制操作代码
     */
    private String contrCode;
    /**
     * 控制操作名称
     */
    private String contrName;
    /**
     * 控制操作URL(用于后台校验时使用)
     */
    private String contrUrl;
    /**
     * 请求类型
     */
    private String methodType;
    /**
     * 菜单id
     */
    private String menuId;
    /**
     * 菜单名称
     */
    private String menuName;
    /**
     * 最新变更用户
     */
    private String lastChgUsr;
    /**
     * 备注
     */
    private String contrRemark;

//    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//    @TableField(fill = FieldFill.INSERT_UPDATE)
//    private Date lastChgDt;
    private String lastChgDt;

    private String lastChgName;


//    /**
//     * 记录编号
//     */
//    private String contrId;
//    /**
//     * 所属业务功能编号
//     */
//    private String funcId;
//    /**
//     * 控制操作代码
//     */
//    private String contrCode;
//    /**
//     * 控制操作名称
//     */
//    private String contrName;
//    /**
//     * 控制操作URL(用于后台校验时使用)
//     */
//    private String contrUrl;

//    /**
//     * 最新变更用户
//     */
//    private String lastChgUsr;
//    /**
//     * 最新变更时间
//     */
//    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//    @TableField(fill = FieldFill.INSERT_UPDATE)
//    private Date lastChgDt;
//    /**
//     * 请求类型
//     */
//    private String methodType;
//    /**
//     * 业务功能名称
//     */
//    private String funcName;
//    /**
//     * 模块名称
//     */
//    private String modName;
//    /**
//     * 修改人名称
//     */
//    private String lastChgName;

}
