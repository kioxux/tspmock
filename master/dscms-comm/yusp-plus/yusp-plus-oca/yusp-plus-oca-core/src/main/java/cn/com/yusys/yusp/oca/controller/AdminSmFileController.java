package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.excelcsv.handle.IProgress;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.config.VerifyFileType;
import cn.com.yusys.yusp.oca.domain.form.AdminSmLogForm;
import cn.com.yusys.yusp.oca.domain.query.AdminSmLogQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmLogPojo;
import cn.com.yusys.yusp.oca.service.AdminSmFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/api/test/file")
public class AdminSmFileController {

    @Autowired
    private AdminSmFileService adminSmFileService;
    @Autowired
    private IProgress iProgress;

    /**
     * 测试接口
     * @param uploadFile
     * @return
     */
    @PostMapping("/type")
    public ResultDto verifyFileType(@RequestBody MultipartFile uploadFile) {

        String fileType = null;
        try {
            fileType = VerifyFileType.getFileType(uploadFile);
            log.info("verifyFileType方法查看文件类型 - {}", fileType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResultDto.success().data(fileType);
    }

    /**
     * 文件下载，异步导出
     * @param params
     * @return
     */
    @GetMapping("/loadfile")
    public ResultDto export(@RequestParam Map<String, String> params) {
        /**
         * 生成文件唯一 id
         */
        String fileCode = adminSmFileService.fileCode();
        /**
         * 异步发送文件到文件系统
         */
        adminSmFileService.asyncSendFile(params, fileCode);
        return ResultDto.success().extParam("data", fileCode);
    }

    /**
     * 文件上传，异步导入
     * @param uploadFile
     * @return
     */
    @PostMapping("/uploadfile")
    public ResultDto asyncImport(@RequestBody MultipartFile uploadFile) {

        if (null == uploadFile) {
            return ResultDto.error("上传文件为空！");
        }
        ProgressDto progressDto = adminSmFileService.asyncImport(uploadFile);
        return ResultDto.success(progressDto);
    }

    /**
     * 按照条件查询数据，并生成 excel 文件
     * @param logPojo
     * @return
     */
    @PostMapping("/translatefile")
    public ResultDto asyncExport(@RequestBody AdminSmLogPojo logPojo) {
        ProgressDto progressDto = adminSmFileService.translateFile(logPojo);
        return ResultDto.success(progressDto);
    }

    /**
     * 使用 taskId 下载 excel 临时文件
     * @param taskId
     * @return
     */
    @GetMapping("/downloadfile/{taskId}")
    public void asyncExport(@PathVariable String taskId, HttpServletResponse response) {
        ProgressDto progress = iProgress.progress(taskId);
        String filePath = progress.getFileId();
        String fileName = filePath.substring(filePath.lastIndexOf("\\") + 1);
        File file = null;
        try {
            // path是指欲下载的文件的路径。
            file = new File(filePath);
            // 以流的形式下载文件。
            InputStream fis = new BufferedInputStream(new FileInputStream(filePath));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            // 清空response
            response.reset();
            // 设置response的Header
            response.addHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes()));
            response.addHeader("Content-Length", "" + file.length());
            response.setContentType("application/octet-stream; charset=utf-8");
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            toClient.write(buffer);
            toClient.flush();
            toClient.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (file.exists()) {
                file.delete();
            }
        }
    }

    /**
     * 下载 excel 文件模板
     * @return
     */
    @GetMapping("/template")
    public void downLoadTemplate(HttpServletResponse response) {
        File file = adminSmFileService.downLoadTemplate();
        String fileName = file.getName();
        try {
            // 以流的形式下载文件。
            InputStream fis = new BufferedInputStream(new FileInputStream(file));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            // 清空response
            response.reset();
            // 设置response的Header
            response.addHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes()));
            response.addHeader("Content-Length", "" + file.length());
            response.setContentType("application/octet-stream");
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            toClient.write(buffer);
            toClient.flush();
            toClient.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (file.exists()) {
                file.delete();
            }
        }
    }
}
