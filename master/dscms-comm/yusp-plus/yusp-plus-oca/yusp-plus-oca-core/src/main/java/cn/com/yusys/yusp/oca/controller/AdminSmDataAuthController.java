package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmDataAuthEntity;
import cn.com.yusys.yusp.oca.service.AdminSmDataAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;



/**
 * 数据权限表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-12-01 09:52:42
 */
@RestController
@RequestMapping("/api/adminsmdataauth")
public class AdminSmDataAuthController {
    @Autowired
    private AdminSmDataAuthService adminSmDataAuthService;

    /**
     * 删除数据权限信息,同时删除其授权信息
     * @param ids
     * @return
     */
    @PostMapping("/deletedataauth")
    public ResultDto deleteDataAuth(@RequestBody String[] ids) {
        adminSmDataAuthService.deleteDataAuth(ids);
        return ResultDto.success("删除权限信息成功");
    }

    /**
     * 修改数据权限信息
     * @param dataAuthEntity
     * @return
     */
    @PostMapping("/edtidataauth")
    public ResultDto editDatuAuth(@RequestBody AdminSmDataAuthEntity dataAuthEntity) {
        adminSmDataAuthService.updateById(dataAuthEntity);
        return ResultDto.success("修改数据权限信息成功");
    }

    /**
     * 根据控制点id分页查询非当前持有的数据权限模板
     * @param params
     * @return
     */
    @GetMapping("/authtmplquery")
    public ResultDto pageAuthTmplByContrId(@RequestParam Map<String, String> params) {
        PageUtils page = adminSmDataAuthService.pageAuthTmplByContrId(params);
        return ResultDto.success().extParam("data", page.getList()).extParam("total", page.getTotalCount());
    }

    /**
     * 分页查询控制点下的数据权限模板列表
     * @param params
     * @return
     */
    @GetMapping("/getdataauth")
    public ResultDto pageResControlDataAuthTmpl(@RequestParam Map<String, String> params) {
        PageUtils page = adminSmDataAuthService.pageResControlDataAuthTmpl(params);
        return ResultDto.success().extParam("data", page.getList()).extParam("total", page.getTotalCount());
    }

    /**
     * 添加控制点与数据库之间的交互
     * @param adminSmDataAuth
     * @return
     */
    @PostMapping("/createdataauth")
    public ResultDto createDataAuth(@RequestBody AdminSmDataAuthEntity adminSmDataAuth) {
        adminSmDataAuthService.createDataAuth(adminSmDataAuth);
        return ResultDto.success();
    }

    /**
     * 获取数据权限管理树，由1级：模块列表 2级：业务功能列表 3级：控制点列表 组成
     * @param nodeId 写死0？
     * @return
     */
    @GetMapping("/treequery")
    public ResultDto getDataAuthTree(@RequestParam(required = false) String nodeId) {
        PageUtils page = adminSmDataAuthService.getDataAuthTree(nodeId);
        return ResultDto.success().extParam("data", page.getList()).extParam("total", page.getTotalCount());
    }

    /**
     * 信息
     */
    @GetMapping("/info/{authId}")
    public ResultDto info(@PathVariable("authId") String authId){
		AdminSmDataAuthEntity adminSmDataAuth = adminSmDataAuthService.getById(authId);
        return ResultDto.success().extParam("adminSmDataAuth", adminSmDataAuth);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public ResultDto save(@RequestBody AdminSmDataAuthEntity adminSmDataAuth){
		adminSmDataAuthService.save(adminSmDataAuth);
        return ResultDto.success();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public ResultDto update(@RequestBody AdminSmDataAuthEntity adminSmDataAuth){
		adminSmDataAuthService.updateById(adminSmDataAuth);
        return ResultDto.success();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public ResultDto delete(@RequestBody String[] authIds){
		adminSmDataAuthService.removeByIds(Arrays.asList(authIds));
        return ResultDto.success();
    }
}
