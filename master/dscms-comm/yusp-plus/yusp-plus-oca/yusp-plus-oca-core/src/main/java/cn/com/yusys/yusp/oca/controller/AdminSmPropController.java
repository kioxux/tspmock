package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.AdminSmPropDto;
import cn.com.yusys.yusp.dto.AdminSmPropQueryDto;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmPropEditBo;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmPropSaveBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmPropEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmPropQuery;
import cn.com.yusys.yusp.oca.service.AdminSmPropService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 系统参数表
 */
@RestController
@RequestMapping("/api/adminsmprop")
public class AdminSmPropController {

    @Autowired
    private AdminSmPropService adminSmPropService;

    /**
     * 列表
     */
    @GetMapping("/page")
    public ResultDto list(AdminSmPropQuery adminSmPropQuery) {
        Page<AdminSmPropEntity> page = adminSmPropService.queryPropPage(adminSmPropQuery);
        return ResultDto.success(page);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public ResultDto save(@Valid @RequestBody AdminSmPropSaveBo adminSmProp) {
        adminSmPropService.saveProp(adminSmProp);
        return ResultDto.success();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public ResultDto update(@Valid @RequestBody AdminSmPropEditBo adminSmProp) {
        adminSmPropService.updatePropById(adminSmProp);
        return ResultDto.success();
    }

    /**
     * 批量删除系统参数
     */
    @PostMapping("/delete")
    public ResultDto delete(@RequestBody @NotNull String[] propNames) {
        List<String> propNameList = Arrays.asList(propNames);
        propNameList.forEach(propName -> adminSmPropService.deleteProp(propName));
        return ResultDto.success();
    }

    /**
     * 查询系统参数对象信息
     *
     * @param queryDto 查询条件
     * @return 返回对象
     */
    @PostMapping("/getpropvalue")
    public ResultDto<AdminSmPropDto> getPropValue(@RequestBody @Validated AdminSmPropQueryDto queryDto) {
        AdminSmPropDto adminSmPropDto = adminSmPropService.queryProp(queryDto);
        return ResultDto.success().data(adminSmPropDto);
    }

}