package cn.com.yusys.yusp.oca.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.List;

@Data
public class AdminSmContrTreeVo {

    @TableField(exist = false)
    private String upTreeId;

    @TableField(exist = false)
    private String nodeId;

    @TableField(exist = false)
    private String nodeName;

    @TableField(exist = false)
    private List<AdminSmContrTreeVo> children;
}
