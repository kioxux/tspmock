package cn.com.yusys.yusp.oca.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @program: yusp-plus
 * @description: 系统策略更新类
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2021-04-02 11:09
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminSmCrelStraDto {
    /**
     * 记录编号
     */
    @NotBlank(message = "记录编号不能为空！")
    private String crelId;
    /**
     * 是否启用 1:是 2:否
     */
    @NotBlank(message = "启用标识不能为空！")
    private String enableFlag;
    /**
     * 策略明细
     */
    private String crelDetail;
}
