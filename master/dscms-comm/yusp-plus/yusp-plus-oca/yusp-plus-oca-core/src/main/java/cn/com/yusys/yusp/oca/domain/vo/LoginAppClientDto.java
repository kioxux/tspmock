package cn.com.yusys.yusp.oca.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * 移动端登录验证信息
 *
 * @author yangzai
 * @since 2021/5/31
 **/
public class LoginAppClientDto {

    /**
     * 渠道码
     */
    @NotEmpty(message = "渠道码不允许为空！")
    private String servtp;

    /**
     * 登录码
     */
    @NotEmpty(message = "登录码不允许为空！")
    private String loginCode;

    /**
     * 机构码
     */
    @NotEmpty(message = "机构码不允许为空！")
    private String orgCode;

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }
}
