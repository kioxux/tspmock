/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dscms.web.rest;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.dscms.domain.CfgScheduleInfo;
import cn.com.yusys.yusp.dscms.domain.EmpScheduleInfo;
import cn.com.yusys.yusp.dscms.service.CfgScheduleInfoService;
import cn.com.yusys.yusp.dscms.service.EmpScheduleInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgScheduleInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-23 10:05:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgscheduleinfo")
public class CfgScheduleInfoResource {
    @Autowired
    private CfgScheduleInfoService cfgScheduleInfoService;
    @Autowired
    private EmpScheduleInfoService empScheduleInfoService;
	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgScheduleInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgScheduleInfo> list = cfgScheduleInfoService.selectAll(queryModel);
        return new ResultDto<List<CfgScheduleInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgScheduleInfo>> index(QueryModel queryModel) {
        List<CfgScheduleInfo> list = cfgScheduleInfoService.selectByModel(queryModel);
        return new ResultDto<List<CfgScheduleInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CfgScheduleInfo> show(@PathVariable("serno") String serno) {
        CfgScheduleInfo cfgScheduleInfo = cfgScheduleInfoService.selectByPrimaryKey(serno);
        return new ResultDto<CfgScheduleInfo>(cfgScheduleInfo);
    }
    /**
     * @函数名称:checkusertime
     * @函数描述:查询是否提前签退
     * @参数与返回说明:
     * @算法描述:
     */

    @GetMapping("/checkusertime/{userCode}")
    protected ResultDto<CfgScheduleInfo> checkusertime(@PathVariable("userCode") String userCode){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat time = new SimpleDateFormat("HH:mm");
        String curentDate = format.format(new Date());
        String curentTime = time .format(new Date());
        EmpScheduleInfo emp = new EmpScheduleInfo();
        CfgScheduleInfo cfg = new CfgScheduleInfo();
        QueryModel qm = new QueryModel();

        List<EmpScheduleInfo> list = empScheduleInfoService.queryByUserCode(userCode);
        for (EmpScheduleInfo em : list) {
            if(curentDate.equals(em.getDutyDate())){
                emp = em;
                break;
            }else{
                cfg.setStatus("1");

                //throw BizException.error(null, "999999","\"该员工今日无排班\"");
            }
        }
        if(emp.getScheduleTimeA()!= null && !"".equals(emp.getScheduleTimeA())){
            CfgScheduleInfo timeA = cfgScheduleInfoService.queryByScheduleTimeYype(emp.getScheduleTimeA());
            if(curentTime.compareTo(timeA.getStartTime())>0 && curentTime.compareTo(timeA.getEndTime())<=0){
                cfg.setStatus("2");
            }
        }
        if(emp.getScheduleTimeB()!= null && !"".equals(emp.getScheduleTimeB())){
            CfgScheduleInfo timeB = cfgScheduleInfoService.queryByScheduleTimeYype(emp.getScheduleTimeB());
            if(curentTime.compareTo(timeB.getStartTime())>0 && curentTime.compareTo(timeB.getEndTime())<=0){
                cfg.setStatus("2");
            }
        }
        if(emp.getScheduleTimeC()!= null && !"".equals(emp.getScheduleTimeC())){
            CfgScheduleInfo timeC = cfgScheduleInfoService.queryByScheduleTimeYype(emp.getScheduleTimeC());
            if(curentTime.compareTo(timeC.getStartTime())>0 && curentTime.compareTo(timeC.getEndTime())<=0){
                cfg.setStatus("2");
            }
        }
        if(emp.getScheduleTimeD()!= null && !"".equals(emp.getScheduleTimeD())){
            CfgScheduleInfo timeD = cfgScheduleInfoService.queryByScheduleTimeYype(emp.getScheduleTimeD());
            if(curentTime.compareTo(timeD.getStartTime())>0 && curentTime.compareTo(timeD.getEndTime())<=0){
                cfg.setStatus("2");
            }
        }
        if(cfg.getStatus()==null){
            cfg.setStatus("0");
        }
        return new ResultDto<CfgScheduleInfo>(cfg);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgScheduleInfo> create(@RequestBody CfgScheduleInfo cfgScheduleInfo) throws URISyntaxException {
        cfgScheduleInfoService.insert(cfgScheduleInfo);
        return new ResultDto<CfgScheduleInfo>(cfgScheduleInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgScheduleInfo cfgScheduleInfo) throws URISyntaxException {
        int result = cfgScheduleInfoService.update(cfgScheduleInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cfgScheduleInfoService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgScheduleInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchUpdate
     * @函数描述:批量修改
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchUpdate")
    protected ResultDto<Integer> batchUpdate(@RequestBody List<CfgScheduleInfo> cfgScheduleInfolist) throws URISyntaxException {
        int result = cfgScheduleInfoService.batchUpdate(cfgScheduleInfolist);
        return new ResultDto<Integer>(result);
    }


}
