package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 认证信息表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-11 14:11:01
 */
@Data
@TableName("admin_sm_auth_info")
public class AdminSmAuthInfoEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 记录编号
     */
    @TableId
    private String authId;
    /**
     * 认证类型名称
     */
    private String authName;
    /**
     * 实现类名称
     */
    private String beanName;
    /**
     * 备注
     */
    private String authRemark;

}
