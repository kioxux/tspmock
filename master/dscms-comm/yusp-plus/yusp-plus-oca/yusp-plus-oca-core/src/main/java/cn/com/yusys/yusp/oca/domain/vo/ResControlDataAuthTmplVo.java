package cn.com.yusys.yusp.oca.domain.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;


/**
 * @author danyu
 */
@Data
public class ResControlDataAuthTmplVo {

	/**
	 * 数据授权记录id
	 */
	private String authId;
	/**
	 * 控制点id
	 */
	private String contrId;
	/**
	 * 数据授权模板id
	 */
	private String authTmplId;
	/**
	 * 数据权限模板名
	 */
	private String authTmplName;
	/**
	 * 数据权限SQL条件
	 */
	private String sqlString;
	/**
	 * 最新变更用户
	 */
	private String lastChgName;
	/**
	 * 最新变更时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

}
