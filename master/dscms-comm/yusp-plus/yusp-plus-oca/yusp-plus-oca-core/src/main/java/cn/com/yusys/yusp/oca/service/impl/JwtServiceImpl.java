package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.oca.domain.vo.JwtVo;
import cn.com.yusys.yusp.oca.service.JwtService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.stereotype.Service;

/**
 * @program: yusp-plus
 * @description: Jwt实现类
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2020-11-20 13:37
 */
@Slf4j
@Service
public class JwtServiceImpl implements JwtService {

    @Autowired
    ObjectMapper objectMapper;


    /**
     * 根据authorization或去用户Id和客户端Id
     *
     * @param authorization
     * @return
     */
    @Override
    public JwtVo getUserIdAndClientIdByAuthorization(String authorization) {
        //判断authorization是否为空
        if (StringUtils.isEmpty(authorization)) {
            log.error("authorization 为空！");
            throw new YuspException("5000", "authorization 为空！");
        }
        //获取jwttoken信息
        String jwtToken = authorization.split(" ")[1];
        //jwttoken信息解密
        JwtVo jwtVo;
        try {
            jwtVo = objectMapper.readValue(JwtHelper.decode(jwtToken).getClaims(), JwtVo.class);
            return jwtVo;
        } catch (Exception e) {
            log.error("解析jwt异常：{},token为：{}", e, jwtToken);
            throw new YuspException("5001", "解析jwt异常！");
        }
    }
}
