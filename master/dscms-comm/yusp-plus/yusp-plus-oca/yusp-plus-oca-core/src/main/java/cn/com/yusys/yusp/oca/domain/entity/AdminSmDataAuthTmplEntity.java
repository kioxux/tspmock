package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 数据权限模板表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-08 18:53:56
 */
@Data
@TableName("admin_sm_data_auth_tmpl")
public class AdminSmDataAuthTmplEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 记录编号
	 */
	@TableId
	private String authTmplId;
	/**
	 * 数据权限模板名
	 */
	private String authTmplName;
	/**
	 * 数据权限SQL条件
	 */
	private String sqlString;
	/**
	 * SQL占位符名称
	 */
	private String sqlName;
	/**
	 * 用于表示该数据模板有没有被控制点关联，0未关联，1关联
	 */
	private int status;
	/**
	 * 最新变更用户
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
	/**
	 * 最新变更时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;
	/**
	 * 优先级,值越小优先级越高
	 */
	private String priority;

}
