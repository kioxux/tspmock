package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.common.query.PageQuery;
import cn.com.yusys.yusp.oca.domain.vo.UserRelationshipVo;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 用角色去查用户列表
 */
@Data
public class AdminSmRoleUserRelQuery extends PageQuery<UserRelationshipVo> {

    @NotEmpty
    private String roleId;

    private String userCode;

    private String loginCode;

    private String userName;

    private String orgId;

     private Boolean checked;


}
