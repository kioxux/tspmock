package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户授权管理机构表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-02 16:29:50
 */
@Data
@TableName("admin_sm_user_mgr_org")
@NoArgsConstructor
public class AdminSmUserMgrOrgEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 记录编号
	 */
	@TableId(type = IdType.UUID)
	private String userMgrOrgId;
	/**
	 * 用户编号
	 */
	private String userId;
	/**
	 * 被授权管理机构编号
	 */
	private String orgId;

	/**
	 * 最新变更用户
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
	/**
	 * 最新变更时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

	public AdminSmUserMgrOrgEntity(String userId, String orgId) {
		this.userId = userId;
		this.orgId = orgId;
	}
}
