package cn.com.yusys.yusp.oca.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAndRoleVo {

    /**
     * 记录编号
     */
    private String UserId;

    /**
     * 所属机构编号
     */
    private String orgId;
    /**
     * 所属部门编号
     */
    private String dptId;
    /**
     * 角色编号
     */
    private String roleId;
}
