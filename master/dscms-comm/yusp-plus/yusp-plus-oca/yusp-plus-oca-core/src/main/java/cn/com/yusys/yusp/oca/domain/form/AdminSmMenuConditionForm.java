package cn.com.yusys.yusp.oca.domain.form;

import lombok.Data;

@Data
public class AdminSmMenuConditionForm {
    /**
     * 如果是修改，上次选择菜单的 id
     */
    private String lastMenuId;
    /**
     * 关键字查询
     */
    private String keyWord;
    /**
     * 1：复选框选择
     * 0：复选框未选
     */
    private int check;

    private int page;

    private int size;
}
