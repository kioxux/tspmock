package cn.com.yusys.yusp.oca.domain.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author danyu
 */
@Data
public class AdminSmLookupVo {

	/**
	 * 记录编号
	 */
	private String lookupId;
	/**
	 * 金融机构编号
	 */
	private String instuId;
	/**
	 * 所属目录编号
	 */
	private String lookupTypeId;
	/**
	 * 字典类别英文别名
	 */
	private String lookupCode;
	/**
	 * 字典类别名称
	 */
	private String lookupName;
	/**
	 * 最新变更用户
	 */
	private String lastChgUsr;
	/**
	 * 最新变更时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

}
