package cn.com.yusys.yusp.oca.domain.session;

import cn.com.yusys.yusp.commons.session.user.UserIdentity;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 系统岗位信息
 *
 * @author yangzai
 * @since 2021-06-17
 */
public class DutyImpl implements UserIdentity {

    @JsonProperty("id")
    private String dutyId;

    @JsonProperty("code")
    private String dutyCode;

    @JsonProperty("name")
    private String dutyName;

    public void setDutyId(String dutyId) {
        this.dutyId = dutyId;
    }

    public void setDutyCode(String dutyCode) {
        this.dutyCode = dutyCode;
    }

    public void setDutyName(String dutyName) {
        this.dutyName = dutyName;
    }

    @Override
    public String getName() {
        return this.dutyName;
    }

    @Override
    public String getCode() {
        return this.dutyCode;
    }

    @Override
    public String getId() {
        return this.dutyId;
    }

}