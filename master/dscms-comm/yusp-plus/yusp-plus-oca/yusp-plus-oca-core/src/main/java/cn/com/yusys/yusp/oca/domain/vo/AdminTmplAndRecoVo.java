package cn.com.yusys.yusp.oca.domain.vo;

import lombok.Data;

@Data
public class AdminTmplAndRecoVo {
    /**
     * 关联的控制点 Id
     */
    private String contrId;
    /**
     * 授权记录Id
     */
    private String authRecoId;
    /**
     * 记录编号
     */
    private String authTmplId;
    /**
     * 数据权限模板名
     */
    private String authTmplName;
    /**
     * 数据权限SQL条件
     */
    private String sqlString;
    /**
     * SQL占位符名称
     */
    private String sqlName;
    /**
     * 用于表示该数据模板有没有被控制点关联，0未关联，1关联
     */
    private int status;
    /**
     * 优先级,值越小优先级越高
     */
    private String priority;

}
