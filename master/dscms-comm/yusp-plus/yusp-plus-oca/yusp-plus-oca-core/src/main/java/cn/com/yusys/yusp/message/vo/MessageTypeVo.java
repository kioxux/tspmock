package cn.com.yusys.yusp.message.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * 消息类型
 *
 * @author xiaodg@yusys.com.cn
 */
@Data
public class MessageTypeVo implements Serializable {

    private static final long serialVersionUID = 4182431166074860187L;
    /**
     * 消息类型
     */
    @TableId
    private String messageType;
    /**
     * 描述
     */
    private String messageDesc;
    /**
     * 消息等级[小先发]
     */
    private String messageLevel;
    /**
     * 模板类型[实时模板、订阅模板]
     */
    private String templateType;

    /**
     * 消息渠道
     */
    private String channelType;

}
