package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 申请信息影像表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-12-02 18:03:52
 */
@Data
@TableName("cms_apply_image_info")
public class ApplyImageInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 影像Id
	 */
	@TableId
	private String imageId;
	/**
	 * 申请流水号
	 */
	private String applyId;
	/**
	 * 影像类型(01-客户基础档案,02-调查报告档案,03-担保品档案,04-信贷决策档案,05-贷后管理档案,06-资产保全档案,99-其他档案)
	 */
	private String imageType;
	/**
	 * 影像路径
	 */
	private String imagePath;
	/**
	 * 影像创建时间
	 */
	private Date createTime;
	/**
	 * 影像更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑显示状态(1:逻辑显示,2:逻辑删除)
	 */
	private String logicStatus;

}
