package cn.com.yusys.yusp.oca.domain.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MenuVo {

    private String id;

    private String menuId;

    private String sysId;

    private String menuName;

    private String upMenuId;

    private Integer menuOrder;

    private String menuIcon;

    private String funcId;

    private String i18nKey;

    private String menuType;

    private String menuTip;

    private String menuClassify;

    private String upMenuName;

    private String funcUrl;

    private List<MenuVo> childrenList;

}
