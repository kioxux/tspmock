package cn.com.yusys.yusp.oca.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * @类名 MobileBindingBo
 * @描述
 * @作者 黄勃
 * @时间 2021/9/17 10:39
 **/
@ApiModel(value = "MobileBindingBo", description = "密码验证绑定手机号")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MobileBindingBo {
    @ApiModelProperty(value = "原密码", name = "oldPassword", required = true)
    @NotEmpty(message = "原密码不能为空")
    private String oldPassword;

    @ApiModelProperty(value = "移动电话", name = "userMobilePhone", required = true)
    @NotEmpty(message = "手机号不能为空")
    private String userMobilePhone;

    @ApiModelProperty(value = "账号", name = "loginCode", required = true)
    @NotEmpty(message = "账号不能为空")
    private String loginCode;
}
