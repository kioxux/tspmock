package cn.com.yusys.yusp.oca.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *@program: yusp-plus
 *@description: 产品保存Vo
 *@author: wujiangpeng
 *@email: wujp4@yusys.com.cn
 *@create: 2020-12-03 10:34
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductSaveVo {
    /**
     * 产品id
    */
    private String productId;
}
