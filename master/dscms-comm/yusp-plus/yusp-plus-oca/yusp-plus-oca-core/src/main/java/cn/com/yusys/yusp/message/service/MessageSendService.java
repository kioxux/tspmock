package cn.com.yusys.yusp.message.service;

import cn.com.yusys.yusp.dto.ReceivedUserDto;
import cn.com.yusys.yusp.message.config.MessageConstants;
import cn.com.yusys.yusp.message.entity.MessagePoolEntity;
import cn.com.yusys.yusp.message.entity.MessageTempEntity;
import cn.com.yusys.yusp.message.enumeration.MessageChannelEnum;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 发送消息服务类
 *
 * @author xiaodg@yusys.com.cn
 **/
public interface MessageSendService {

    /**
     * 发送消息
     * 注意：该方法仅会发送<pre>messageType</pre>特定的<pre>MessageTempEntity</pre>渠道消息
     *
     * @param messageType       消息类型 {@code String}
     * @param messageTempEntity 消息模板 {@link MessageTempEntity}
     * @param receiveUserIds    接收消息的用户的ID集合, 如果消息类型是订阅类型，程序会自动处理加上订阅关系的用户ID集合  {@code Set<String>}
     * @param sendUserId        发送者ID {@code String}
     * @param params            模板参数, 在模板变量中注入, {@link MessageConstants#MESSAGE_PARAM_START_TIME}和{@link MessageConstants#MESSAGE_PARAM_END_TIME}可指定消息发送的时间范围, 时间格式为: "yyyy-MM-dd HH:mm:ss" {@link Map}
     */
    void sendMessage(String messageType, MessageTempEntity messageTempEntity, Set<String> receiveUserIds, String sendUserId, Map<String, String> params);

    /**
     * 发送消息
     * 注意：该方法会发送<pre>messageType</pre>所有的渠道消息
     *
     * @param messageType    消息类型 {@code String}
     * @param receiveUserIds 接收消息的用户的ID集合, 如果消息类型是订阅类型，程序会自动处理加上订阅关系的用户ID集合  {@code Set<String>}
     * @param sendUserId     发送者ID {@code String}
     * @param params         模板参数, 在模板变量中注入, {@link MessageConstants#MESSAGE_PARAM_START_TIME}和{@link MessageConstants#MESSAGE_PARAM_END_TIME}可指定消息发送的时间范围, 时间格式为: "yyyy-MM-dd HH:mm:ss" {@link Map}
     */
    void sendMessage(String messageType, Set<String> receiveUserIds, String sendUserId, Map<String, String> params);

    /**
     * 发送消息
     * 注意：该方法仅会发送<pre>messageType</pre>特定的<pre>MessageChannelEnum</pre>渠道消息
     *
     * @param messageType        消息类型 {@code String}
     * @param messageChannelEnum 消息模板 {@link MessageChannelEnum}
     * @param receiveUserIds     接收消息的用户的ID集合, 如果消息类型是订阅类型，程序会自动处理加上订阅关系的用户ID集合  {@code Set<String>}
     * @param sendUserId         发送者ID {@code String}
     * @param params             模板参数, 在模板变量中注入, {@link MessageConstants#MESSAGE_PARAM_START_TIME}和{@link MessageConstants#MESSAGE_PARAM_END_TIME}可指定消息发送的时间范围, 时间格式为: "yyyy-MM-dd HH:mm:ss"{@link Map}
     */
    void sendMessage(String messageType, MessageChannelEnum messageChannelEnum, Set<String> receiveUserIds, String sendUserId, Map<String, String> params);


    /**
     * 发送消息队列到Bind
     *
     * @param messagePoolEntity 消息队列 {@link MessagePoolEntity}
     * @param isTime            是否免打扰 {@link MessageConstants#DO_NOT_DISTURB_MODE}, {@link MessageConstants#NON_DO_NOT_DISTURB_MODE}
     */
    void sendMessagePoolEntityToBind(MessagePoolEntity messagePoolEntity, String isTime);

    /**
     * 根据消息队列历史得pkNo重发消息
     * <p>
     * 当消息队列历史的发送状态不成功时可用
     *
     * @param pkNo 消息队列主键
     */
    void resendMessage(String pkNo);

    /**
     * 发送消息
     * 张家港农商改造，将区分接收人类型，借款人类型会直接传入手机号
     * @param messageType
     * @param receivedUserList
     * @param sendUserId
     * @param params
     */
    void sendMessage(String messageType, List<ReceivedUserDto> receivedUserList, String sendUserId, Map<String, String> params);

    void sendMessage(String messageType, MessageTempEntity messageTempEntity, List<ReceivedUserDto> receivedUserList, String sendUserId, Map<String, String> params);
}
