package cn.com.yusys.yusp.notice.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.notice.form.AdminSmRicheditFileInfoForm;
import cn.com.yusys.yusp.notice.service.AdminSmRicheditFileInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;




/**
 * 
 *
 * @author danyb1
 * @date 2021-03-31 11:00:50
 */
@Slf4j
@Api("接口")
@RestController
@RequestMapping("/api/adminsmricheditfileinfo")
public class AdminSmRicheditFileInfoController {

    @Autowired
    private AdminSmRicheditFileInfoService adminSmRicheditFileInfoService;

    /**
     * 新增文件数据
     * @param fileInfoFormList
     * @return
     */
    @ApiOperation("保存")
    @RequestMapping("/add")
    public ResultDto addFileInfo(@RequestBody List<AdminSmRicheditFileInfoForm> fileInfoFormList){
        log.info("保存新增文件数据： " + fileInfoFormList);
		adminSmRicheditFileInfoService.addFileInfo(fileInfoFormList);
        return ResultDto.success();
    }

    /**
     * 删除数据库数据及oss中的文件
     */
    @ApiOperation("删除")
    @RequestMapping("/delete")
    public ResultDto delete(@RequestBody List<AdminSmRicheditFileInfoForm> fileInfoFormList){
        // TODO 文件信息删除
        adminSmRicheditFileInfoService.deleteFileInfo(fileInfoFormList);
        return ResultDto.success();
    }
}
