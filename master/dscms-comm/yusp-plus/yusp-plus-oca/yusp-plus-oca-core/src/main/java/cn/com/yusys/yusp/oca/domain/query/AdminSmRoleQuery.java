package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.common.query.PageQuery;
import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmRoleVo;
import lombok.Data;

@Data
public class AdminSmRoleQuery extends PageQuery<AdminSmRoleVo> {
    private String orgId;
    private String keyWord;
    private String roleCode;
    private String roleName;
    private AvailableStateEnum roleSts;
}
