/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dscms.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: EmpAttendInfo
 * @类描述: emp_attend_info数据实体类
 * @功能描述: 
 * @创建人: kioxu
 * @创建时间: 2021-05-10 15:30:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "emp_attend_info")
public class EmpAttendInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 业务类型 **/
	@Column(name = "ATTEND_OPT_TYPE", unique = false, nullable = true, length = 5)
	private String attendOptType;
	
	/** 员工号 **/
	@Column(name = "USER_CODE", unique = false, nullable = true, length = 20)
	private String userCode;
	
	/** 员工姓名 **/
	@Column(name = "USER_NAME", unique = false, nullable = true, length = 20)
	private String userName;
	
	/** 起始日期 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 20)
	private String startDate;
	
	/** 到期日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 20)
	private String endDate;
	
	/** 请假原因 **/
	@Column(name = "VACATION_RESN", unique = false, nullable = true, length = 1000)
	private String vacationResn;
	
	/** 申请时间 **/
	@Column(name = "APP_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date appTime;
	
	/** 是否销假 **/
	@Column(name = "IS_CANCEL_VACATION", unique = false, nullable = true, length = 5)
	private String isCancelVacation;
	
	/** 销假时间 **/
	@Column(name = "CANCEL_VACATION_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date cancelVacationTime;
	
	/** 审批状态 **/
	@Column(name = "APPR_STATUS", unique = false, nullable = true, length = 5)
	private String apprStatus;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param attendOptType
	 */
	public void setAttendOptType(String attendOptType) {
		this.attendOptType = attendOptType;
	}
	
    /**
     * @return attendOptType
     */
	public String getAttendOptType() {
		return this.attendOptType;
	}
	
	/**
	 * @param userCode
	 */
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	
    /**
     * @return userCode
     */
	public String getUserCode() {
		return this.userCode;
	}
	
	/**
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
    /**
     * @return userName
     */
	public String getUserName() {
		return this.userName;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param vacationResn
	 */
	public void setVacationResn(String vacationResn) {
		this.vacationResn = vacationResn;
	}
	
    /**
     * @return vacationResn
     */
	public String getVacationResn() {
		return this.vacationResn;
	}
	
	/**
	 * @param appTime
	 */
	public void setAppTime(java.util.Date appTime) {
		this.appTime = appTime;
	}
	
    /**
     * @return appTime
     */
	public java.util.Date getAppTime() {
		return this.appTime;
	}
	
	/**
	 * @param isCancelVacation
	 */
	public void setIsCancelVacation(String isCancelVacation) {
		this.isCancelVacation = isCancelVacation;
	}
	
    /**
     * @return isCancelVacation
     */
	public String getIsCancelVacation() {
		return this.isCancelVacation;
	}
	
	/**
	 * @param cancelVacationTime
	 */
	public void setCancelVacationTime(java.util.Date cancelVacationTime) {
		this.cancelVacationTime = cancelVacationTime;
	}
	
    /**
     * @return cancelVacationTime
     */
	public java.util.Date getCancelVacationTime() {
		return this.cancelVacationTime;
	}
	
	/**
	 * @param apprStatus
	 */
	public void setApprStatus(String apprStatus) {
		this.apprStatus = apprStatus;
	}
	
    /**
     * @return apprStatus
     */
	public String getApprStatus() {
		return this.apprStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}