package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.oca.domain.bo.AdminSmOrgTreeNodeBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserRoleRelEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmRoleUserRelQuery;
import cn.com.yusys.yusp.oca.domain.vo.UserRelationshipVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Set;

/**
 * 用户角色关联表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-11-18 18:06:35
 */
public interface AdminSmUserRoleRelService extends IService<AdminSmUserRoleRelEntity> {

    List<AdminSmOrgTreeNodeBo> getOrgTree(String roleId);

    Page<UserRelationshipVo> memberPage(AdminSmRoleUserRelQuery query);

    boolean save(List<AdminSmUserRoleRelEntity> entityList);

    boolean remove(List<AdminSmUserRoleRelEntity> entityList);

    Set<String> findUserIdsByRoleId(String roleId);

    List<AdminSmUserRoleRelEntity> findUserRoleRelsByUser(AdminSmUserEntity user);
}