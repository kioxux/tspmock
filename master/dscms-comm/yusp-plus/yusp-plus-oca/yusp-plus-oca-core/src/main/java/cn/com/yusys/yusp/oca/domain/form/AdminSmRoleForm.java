package cn.com.yusys.yusp.oca.domain.form;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * @author danyu
 */
@Data
public class AdminSmRoleForm {

	/**
	 * {"roleCode":"R112","roleName":"测试角色2","orgId":"100","roleSts":"A","lastChgUsr":"40"}
	 *
	 */
	/**
	 * 角色代码
	 */
	private String roleCode;
	/**
	 * 角色名称
	 */
	private String roleName;
	/**
	 * 所属机构编号
	 */
	private String orgId;
	/**
	 * 状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效
	 */
	private String roleSts;
	/**
	 * 最新变更用户
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;

	private String roleId;
	private String roleLevel;

}
