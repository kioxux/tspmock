package cn.com.yusys.yusp.message.validator;


import cn.com.yusys.yusp.message.enumeration.MessageTempSendTypeEnum;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 消息模板类型校验器
 *
 * @author xiaodg
 */
public class MessageTempTypeValidator implements ConstraintValidator<MessageTempType, String> {
    /**
     * 消息模板类型集合
     */
    public static final Set<String> TYPES = Stream.of(MessageTempSendTypeEnum.values()).map(MessageTempSendTypeEnum::getType).collect(Collectors.toSet());

    /**
     * 输入参数是否是合法的消息模板类型
     *
     * @param value   消息模板类型字符串 {@code String}
     * @param context 校验器上下文 {@link ConstraintValidatorContext}
     * @return {@code boolean}
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return TYPES.contains(value);
    }
}
