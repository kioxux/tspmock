package cn.com.yusys.yusp.sequence.util;

import cn.com.yusys.yusp.commons.sequence.enumeration.CycleType;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.sequence.domain.entity.SequenceConfig;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 序列号模版配置通用工具类
 *
 * @author wangyang
 * @author Cytus_
 */
public class SequenceUtils {

    private static final Pattern SEQUENCE_ID_PATTERN = Pattern.compile("[A-Z]{1}[A-Z0-9_]+");

    private SequenceUtils() {
        super();
    }

    /**
     * 将模板内容中{}包围的输入要素字符串用map中的值替换了
     */
    @Deprecated
    public static String getMsgContent(String templet, Map<String, String> paramsMap) {
        String newTemplet = templet;
        if (newTemplet == null || newTemplet == "") {
            return "";
        }

        // 遍历map中的键值对，替换模板中被'{}'包围的字符串
        Iterator<Map.Entry<String, String>> iterator = paramsMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            String key = entry.getKey();
            if (newTemplet.contains("{" + key + "}")) {
                newTemplet = newTemplet.replaceAll("\\{" + key + "\\}", entry.getValue());
            }
        }
        return newTemplet;
    }

    /**
     * @方法名称:numFormatToSeq
     * @方法描述:在纯数字前补0,转换成长度为5位的字符串（如果数字大于100000，则取数字尾数）。 如
     * 123-->00123，9999-->09999，100123123-->23123
     * @参数与返回说明: len 需要的长度
     * value 要操作的值
     * @算法描述:
     */
    @Deprecated
    public static String numFormatToSeq(int len, int value) {
        // 如果要改动返回字符串长度，改动maxValue(返回几位数字，则尾数改为几个0)
        if (len <= 0) {
            return "";
        }
        // 补足指定长度的0
        StringBuilder seq = new StringBuilder();
        for (int i = 0; i < len; i++) {
            seq.append('0');
        }
        seq.append(value);
        return seq.substring(seq.length() - len);
    }

    /**
     * 字符串必须为大写字母、数字、下划线组成且首字母只能是下划线
     *
     * @param str
     * @return
     */
    public static boolean reg(String str) {
        Matcher mt = SEQUENCE_ID_PATTERN.matcher(str);
        return mt.matches();
    }

    public static boolean dropSequence(String seqId) {
        return cn.com.yusys.yusp.commons.sequence.util.SequenceUtils.getMainSequence().dropSequence(seqId, false);
    }

    public static long getCurrentValue(String seqId) {
        return cn.com.yusys.yusp.commons.sequence.util.SequenceUtils.getMainSequence().getCurrentSequenceNo(seqId);
    }

    public static boolean createSequence(String seqId) {
        return cn.com.yusys.yusp.commons.sequence.util.SequenceUtils.getMainSequence().createSequence(seqId);
    }

    public static boolean addSequenceConfig(SequenceConfig sequenceConfig) {
        return cn.com.yusys.yusp.commons.sequence.util.SequenceUtils.getMainSequence().addSequenceConfig(config(sequenceConfig));
    }

    public static long getNextSequenceNo(String seqId) {
        return cn.com.yusys.yusp.commons.sequence.util.SequenceUtils.getSequenceNo(seqId);
    }

    public static String getSequence(String seqId, Map<String, String> params) {
        return cn.com.yusys.yusp.commons.sequence.util.SequenceUtils.getSequence(seqId, params);
    }

    public static cn.com.yusys.yusp.commons.sequence.SequenceConfig config(SequenceConfig config) {
        cn.com.yusys.yusp.commons.sequence.SequenceConfig sequenceConfig = new cn.com.yusys.yusp.commons.sequence.SequenceConfig();
        sequenceConfig.setSeqId(config.getSeqId());
        sequenceConfig.setSeqName(config.getSeqName());
        sequenceConfig.setStartValue(config.getStartvalue().longValue());
        sequenceConfig.setMaxValue(config.getMaximumvalue().longValue());
        sequenceConfig.setIncrementValue(config.getIncrementvalue().longValue());
        CycleType cycleType = getCycleType(config.getIsCycle());
        sequenceConfig.setCycleType(cycleType);
        sequenceConfig.setCacheValue(config.getCachevalue().longValue());
        sequenceConfig.setSequenceTemplate(config.getSeqTemplet());
        sequenceConfig.setSeqPlace(config.getSeqPlace().intValue());
        sequenceConfig.setFillString("Y".equals(config.getZeroFill()) ? "0" : "");
        return sequenceConfig;
    }

    public static SequenceConfig domain(cn.com.yusys.yusp.commons.sequence.SequenceConfig config) {
        SequenceConfig sequenceConfig = new SequenceConfig();
        sequenceConfig.setSeqId(config.getSeqId());
        sequenceConfig.setSeqName(config.getSeqName());
        sequenceConfig.setStartvalue(new Long(config.getStartValue()).intValue());
        sequenceConfig.setMaximumvalue(new Long(config.getMaxValue()).intValue());
        sequenceConfig.setIncrementvalue(new Long(config.getIncrementValue()).intValue());
        sequenceConfig.setIsCycle(getCycle(config.getCycleType()));
        sequenceConfig.setCachevalue(new Long(config.getCacheValue()).intValue());
        sequenceConfig.setSeqTemplet(config.getSequenceTemplate());
        sequenceConfig.setSeqPlace(new Long(config.getSeqPlace()).intValue());
        sequenceConfig.setZeroFill("Y".equals(config.getFillString()) ? "Y" : "N");
        return sequenceConfig;
    }

    private static String getCycle(CycleType cycleType) {
        if (Objects.nonNull(cycleType)) {
            String result;
            switch (cycleType) {
                case DAY:
                    result = "D";
                    break;
                case MONTH:
                    result = "M";
                    break;
                case YEAR:
                    result = "Y";
                    break;
                case SEASON:
                    result = "S";
                    break;
                case HALF_Y:
                    result = "H";
                    break;
                case WEEK:
                    result = "W";
                    break;
                case MAX:
                    result = "X";
                    break;
                default:
                    result = "N";
                    break;
            }
            return result;
        } else {
            return "N";
        }
    }

    private static CycleType getCycleType(String cycle) {
        if (StringUtils.nonBlank(cycle)) {
            CycleType type;
            switch (cycle) {
                case "D":
                    type = CycleType.DAY;
                    break;
                case "M":
                    type = CycleType.MONTH;
                    break;
                case "Y":
                    type = CycleType.YEAR;
                    break;
                case "S":
                    type = CycleType.SEASON;
                    break;
                case "H":
                    type = CycleType.HALF_Y;
                    break;
                case "W":
                    type = CycleType.WEEK;
                    break;
                case "X":
                    type = CycleType.MAX;
                    break;
                default:
                    type = CycleType.NONE;
                    break;
            }
            return type;
        }
        return CycleType.NONE;
    }

}