package cn.com.yusys.yusp.message.service.impl;


import cn.com.yusys.yusp.message.async.MessagePoolPublishAsyncTaskExecutor;
import cn.com.yusys.yusp.message.async.MessageSendBindAsyncTaskExecutor;
import cn.com.yusys.yusp.message.channel.YuMessageChannel;
import cn.com.yusys.yusp.message.entity.MessagePoolEntity;
import cn.com.yusys.yusp.message.service.MessageBindService;
import cn.com.yusys.yusp.message.util.MessageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Service;

/**
 * 消息消息到消息中间件
 * <p>
 * 从消息中间件收到消息
 *
 * @author xiaodg@yusys.com.cn
 **/
@Service
@Slf4j
public class MessageBindServiceImpl implements MessageBindService {

    @Autowired
    private MessageSendBindAsyncTaskExecutor messageSendAsyncTaskExecutor;

    @Autowired
    private MessagePoolPublishAsyncTaskExecutor messagePoolPublishAsyncTaskExecutor;

    @Autowired
    private YuMessageChannel yuMessageChannel;

    /**
     * 消息发送到指定的路由目的地
     */
    @Override
    @SendTo(YuMessageChannel.MESSAGE_CHANNEL_OUTPUT)
    public void send(MessagePoolEntity messagePoolEntity) {
        messageSendAsyncTaskExecutor.doSend(() -> {
            log.info("发送消息到Bind通道: {}", messagePoolEntity);
            yuMessageChannel.output().send(MessageBuilder.withPayload(messagePoolEntity).setPriority(MessageUtils.parseMessageLevel(messagePoolEntity.getMessageLevel())).build());
        });
    }

    /**
     * 消息发送到指定的路由目的地
     */
    @Override
    @SendTo(YuMessageChannel.MESSAGE_CHANNEL_OUTPUT)
    public void send(MessagePoolEntity messagePoolEntity, Long delaySeconds) {
        messageSendAsyncTaskExecutor.doSend(() -> {
            log.info("发送消息到Bind通道: {}", messagePoolEntity);
            yuMessageChannel.input().send(MessageBuilder.withPayload(messagePoolEntity).setPriority(MessageUtils.parseMessageLevel(messagePoolEntity.getMessageLevel())).build());
        }, delaySeconds);
    }

    /**
     * 监听消息中间件发送过来的消息
     *
     * @param messagePoolEntity 消息队列实体 {@link MessagePoolEntity}
     */
    @Override
    @StreamListener(YuMessageChannel.MESSAGE_CHANNEL_INPUT)
    public void receive(@Payload MessagePoolEntity messagePoolEntity) {
        log.info("从Bind通道收到消息: {}", messagePoolEntity);
        messagePoolPublishAsyncTaskExecutor.publishMessagePool(messagePoolEntity);
    }
}
