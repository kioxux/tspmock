package cn.com.yusys.yusp.oca.domain.vo;

import lombok.Data;

@Data
public class AdminDataTmplControlVo {

    private String authId;

    private String sysId;

    private String contrId;

    private String contrUrl;

    private String authTmplId;

    private String sqlString;

    private String sqlName;

    private String priority;
}
