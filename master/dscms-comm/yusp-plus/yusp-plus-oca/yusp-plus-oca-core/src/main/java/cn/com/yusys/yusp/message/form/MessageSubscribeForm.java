package cn.com.yusys.yusp.message.form;

import cn.com.yusys.yusp.message.validator.MessageChannelType;
import cn.com.yusys.yusp.message.validator.MessageSubscribeType;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 消息订阅表单
 *
 * @author xiaodg@yusys.com.cn
 **/
@Data
public class MessageSubscribeForm implements Serializable {
    private static final long serialVersionUID = -5479335920096949018L;

    /**
     * 渠道类型
     */
    @MessageChannelType
    private String channelType;

    /**
     * 消息类型
     */
    @NotBlank(message = "消息类型不能为空")
    @Size(max = 32, message = "错误的消息类型")
    private String messageType;

    /**
     * 订阅类型
     */
    @MessageSubscribeType
    private String subscribeType;

    /**
     * 订阅值，以英文逗号分隔
     */
    @NotBlank(message = "订阅值不能为空")
    private String subscribeValue;
}
