package cn.com.yusys.yusp.message.dao;

import cn.com.yusys.yusp.message.entity.MessageSubscribeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户订阅
 *
 * @author xiaodg@yusys.com.cn
 */

public interface MessageSubscribeDao extends BaseMapper<MessageSubscribeEntity> {

}
