package cn.com.yusys.yusp.oca.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author xufy1@yusys.com.cn
 * @desc 控制点左侧列表树
 * @date 2020-12-09 14:20
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminSmResContrTreeVo {

    String nodeName;

    String upTreeId;

    String nodeType;

    String nodeId;

}
