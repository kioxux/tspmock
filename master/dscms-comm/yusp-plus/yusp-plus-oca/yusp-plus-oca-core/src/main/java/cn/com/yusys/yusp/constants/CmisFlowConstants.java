package cn.com.yusys.yusp.constants;

public class CmisFlowConstants {
    /**
     * 请假及提前签退审批流程
     */
    public static final String XTGL01 = "XTGL01";

    /**
     * 流程状态标识位 000-待发起
     **/
    public final static String WF_STATUS_000 = "000";
    /**
     * 流程状态标识位 111-审批中
     **/
    public final static String WF_STATUS_111 = "111";
    /**
     * 流程状态标识位 990-取消
     **/
    public final static String WF_STATUS_990 = "990";
    /**
     * 流程状态标识位 991-拿回
     **/
    public final static String WF_STATUS_991 = "991";
    /**
     * 流程状态标识位 992-打回
     **/
    public final static String WF_STATUS_992 = "992";
    /**
     * 流程状态标识位 993-再议
     **/
    public final static String WF_STATUS_993 = "993";
    /**
     * 流程状态标识位 996-自行退出
     **/
    public final static String WF_STATUS_996 = "996";
    /**
     * 流程状态标识位 997-通过
     **/
    public final static String WF_STATUS_997 = "997";
    /**
     * 流程状态标识位 998-拒绝
     **/
    public final static String WF_STATUS_998 = "998";
    /**
     * 签到签退标识 01-签到
     */
    public final static String CHECK_OPT_TYPE_01 = "01";
    /**
     * 签到签退标识 02-签退
     */
    public final static String CHECK_OPT_TYPE_02 = "02";

    /**
     * 操作标识 01-新增
     **/
    public final static String OPR_TYPE_ADD = "01";
    /**
     * 操作标识 02-删除
     **/
    public final static String OPR_TYPE_DELETE = "02";

    /**
     * 操作类型 新增
     **/
    public static final String OPR_TYPE_01 = "01";
    /**
     * 操作类型 删除
     **/
    public static final String OPR_TYPE_02 = "02";
}
