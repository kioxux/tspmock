package cn.com.yusys.yusp.notice.form;

import lombok.Data;

@Data
public class NoticeConditionForm<T> {

    private String reciveOgjId;
    private String creatorId;
    private String userId;
    private String roleId;
    private T roles;
    /**
     * 下面三个为查询所需字段
     */
    private String noticeTitle;
    private String beginTime;
    private String endTime;
}
