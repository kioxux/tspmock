package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmAuthInfoEntity;
import cn.com.yusys.yusp.oca.service.AdminSmAuthInfoService;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmAuthInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 认证信息表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-11 14:11:01
 */
//@RestController
//@RequestMapping("/api/adminsmauthinfo")
public class AdminSmAuthInfoController {
//    @Autowired
//    private AdminSmAuthInfoService adminSmAuthInfoService;
//
//    /**
//     * 列表
//     */
//    @RequestMapping("/list")
//    public ResultDto list(@RequestParam Map<String, Object> params) {
//        PageUtils page = adminSmAuthInfoService.queryPage(params);
//        return ResultDto.success().extParam("page", page);
//    }
//
//    /**
//     * 信息
//     */
//    @RequestMapping("/info/{authId}")
//    public ResultDto info(@PathVariable("authId") String authId) {
//        AdminSmAuthInfoEntity adminSmAuthInfo = adminSmAuthInfoService.getById(authId);
//        return ResultDto.success().extParam("adminSmAuthInfo", adminSmAuthInfo);
//    }
//
//    /**
//     * 保存
//     */
//    @RequestMapping("/save")
//    public ResultDto save(@RequestBody AdminSmAuthInfoEntity adminSmAuthInfo) {
//        adminSmAuthInfoService.save(adminSmAuthInfo);
//        return ResultDto.success();
//    }
//
//    /**
//     * 修改
//     */
//    @RequestMapping("/update")
//    public ResultDto update(@RequestBody AdminSmAuthInfoEntity adminSmAuthInfo) {
//        adminSmAuthInfoService.updateById(adminSmAuthInfo);
//        return ResultDto.success();
//    }
//
//    /**
//     * 删除
//     */
//    @RequestMapping("/delete")
//    public ResultDto delete(@RequestBody String[] authIds) {
//        adminSmAuthInfoService.removeByIds(Arrays.asList(authIds));
//        return ResultDto.success();
//    }
//
//    /**
//     * getAuthKeyValue
//     * 认证信息KV
//     */
//    @GetMapping("/authkv")
//    protected ResultDto getAuthKeyValue() {
//        List<AdminSmAuthInfoVo> list = this.adminSmAuthInfoService.getAuthKeyValue();
//        return ResultDto.success().extParam("data", list);
//    }
}
