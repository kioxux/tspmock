/**
 * Copyright (C), 2014-2021
 * FileName: WfOrgImpl
 * Author: Administrator
 * Date: 2021/3/17 15:14
 * Description:
 * History:
 * <author> <time> <version> <desc>
 * Administrator 15:14 1.0.0 新建类
 */

package cn.com.yusys.yusp.oca.config.flow;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.dto.WFUserDto;
import cn.com.yusys.yusp.flow.dto.WFUserExtDto;
import cn.com.yusys.yusp.flow.other.org.*;
import cn.com.yusys.yusp.flow.repository.mapper.NWfOcaTaskpoolMapper;
import cn.com.yusys.yusp.oca.domain.query.*;
import cn.com.yusys.yusp.oca.domain.vo.*;
import cn.com.yusys.yusp.oca.service.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;


/**
 * 〈〉
 * @author zhui
 * @create 2021/3/17
 * @since 1.0.0
 */
public class WfLocalOrgImpl implements OrgInterface {
    @Autowired
    private AdminSmOrgService orgService;
    @Autowired
    private AdminSmUserService userService;
    @Autowired
    private AdminSmDptService dptService;
    @Autowired
    private AdminSmDutyService dutyService;
    @Autowired
    private AdminSmRoleService roleService;
    @Autowired
    private NWfOcaTaskpoolMapper nWfOcaTaskpoolMapper;

    @Override
    public List<WFUser> getUsers(QueryModel param) {
        AdminSmUserQuery query = new AdminSmUserQuery();
        query.setUserName((String) param.getCondition().get("userName"));
        query.setLoginCode((String) param.getCondition().get("userId"));
        query.setPage(param.getPage());
        query.setSize(param.getSize());
        query.setSort(param.getSort());
        Page<AdminSmUserVo> usersForWf = userService.getUsersForWf(query);
        List<AdminSmUserVo> users = usersForWf.getRecords();
        com.github.pagehelper.Page<WFUser> wfUsers = users.stream().map(vo -> {
            WFUser user = new WFUser();
            user.setUserId(vo.getUserId());
            user.setUserName(vo.getUserName());
            user.setUserMobile(vo.getUserMobilephone());
            user.setUserEmail(vo.getUserEmail());
            return user;
        }).collect(Collectors.toCollection(com.github.pagehelper.Page<WFUser>::new));
        wfUsers.setTotal(usersForWf.getTotal());
        return wfUsers;
    }

    @Override
    public List<WFDept> getDepts(QueryModel param) {
        AdminSmDptQuery query = new AdminSmDptQuery();
        query.setOrgId((String) param.getCondition().get("orgId"));
        query.setDptCode((String) param.getCondition().get("deptId"));
        query.setDptName((String) param.getCondition().get("deptName"));
        query.setPage(param.getPage());
        query.setSize(param.getSize());
        query.setSort(param.getSort());
        Page<AdminSmDptVo> deptsForWf = dptService.getDeptsForWf(query);
        List<AdminSmDptVo> depts = deptsForWf.getRecords();

        com.github.pagehelper.Page<WFDept> wfDepts = depts.stream().map(vo -> {
            WFDept dept = new WFDept();
            dept.setDeptId(vo.getDptId());
            dept.setDeptName(vo.getDptName());
            return dept;
        }).collect(Collectors.toCollection(com.github.pagehelper.Page<WFDept>::new));
        wfDepts.setTotal(deptsForWf.getTotal());
        return wfDepts;
    }

    @Override
    public List<WFDuty> getDutys(QueryModel param) {
        AdminSmDutyQuery query = new AdminSmDutyQuery();
        query.setDutyCode((String) param.getCondition().get("dutyId"));
        query.setDutyName((String) param.getCondition().get("dutyName"));
        query.setPage(param.getPage());
        query.setSize(param.getSize());
        query.setSort(param.getSort());
        Page<AdminSmDutyVo> dutysForWf = dutyService.getDutysForWf(query);
        List<AdminSmDutyVo> dutys = dutysForWf.getRecords();

        com.github.pagehelper.Page<WFDuty> wfDuties = dutys.stream().map(vo -> {
            WFDuty duty = new WFDuty();
            duty.setDutyId(vo.getDutyId());
            duty.setDutyName(vo.getDutyName());
            return duty;
        }).collect(Collectors.toCollection(com.github.pagehelper.Page<WFDuty>::new));
        wfDuties.setTotal(dutysForWf.getTotal());
        return wfDuties;
    }

    @Override
    public List<WFOrg> getOrgs(QueryModel param) {
        AdminSmOrgExtQuery query = new AdminSmOrgExtQuery();
        query.setOrgName((String) param.getCondition().get("orgName"));
        query.setOrgCode((String) param.getCondition().get("orgCode"));
        query.setUpOrgId((String) param.getCondition().get("orgId"));
        query.setPage(param.getPage());
        query.setSize(param.getSize());
        query.setSort(param.getSort());
        Page<AdminSmOrgVo> orgsForWf = orgService.getOrgsForWf(query);
        List<AdminSmOrgVo> orgs = orgsForWf.getRecords();

        com.github.pagehelper.Page<WFOrg> wfOrgs = orgs.stream().map(vo -> {
            WFOrg org = new WFOrg();
            org.setOrgId(vo.getOrgId());
            org.setOrgName(vo.getOrgName());
            org.setOrgLevel(vo.getOrgLevel().toString());
            return org;
        }).collect(Collectors.toCollection(com.github.pagehelper.Page<WFOrg>::new));
        wfOrgs.setTotal(orgsForWf.getTotal());
        return wfOrgs;
    }

    @Override
    public List<WFRole> getRoles(QueryModel param) {
        AdminSmRoleQuery query = new AdminSmRoleQuery();
        query.setRoleCode((String) param.getCondition().get("roleId"));
        query.setRoleName((String) param.getCondition().get("roleName"));
        query.setPage(param.getPage());
        query.setSize(param.getSize());
        query.setSort(param.getSort());
        Page<AdminSmRoleVo> rolesForWf = roleService.getRolesForWf(query);
        PageHelper.startPage(param.getPage(), param.getSize());
        List<AdminSmRoleVo> roles = rolesForWf.getRecords();

        com.github.pagehelper.Page<WFRole> wfRoles = roles.stream().map(vo -> {
            WFRole role = new WFRole();
            role.setRoleId(vo.getRoleId());
            role.setRoleName(vo.getRoleName());
            return role;
        }).collect(Collectors.toCollection(com.github.pagehelper.Page<WFRole>::new));
        wfRoles.setTotal(rolesForWf.getTotal());
        return wfRoles;
    }

    @Override
    public List<WFUser> getUsersByOrg(String systemId, String orgId) {
        List<AdminSmUserVo> users = userService.getUsersByOrgForWf(orgId);
        return getWfUserList(users);
    }

    @Override
    public List<WFUser> getUsersByDept(String systemId, String deptId) {
        List<AdminSmUserVo> users = userService.getUsersByDeptForWf(deptId);
        return getWfUserList(users);
    }

    @Override
    public List<WFUser> getUsersByDuty(String systemId, String dutyId) {
        List<AdminSmUserVo> users = userService.getUsersByDutyForWf(dutyId);
        return getWfUserList(users);
    }

    @Override
    public List<WFUser> getUsersByRole(String systemId, String roleId) {
        List<AdminSmUserVo> users = userService.getUsersByRoleForWf(roleId);
        return getWfUserList(users);
    }

    @Override
    public WFUser getUserInfo(String systemId, String userId) {
        AdminSmUserVo userInfoForWf = userService.getUserInfoForWf(userId);
        WFUser user = new WFUser();
        user.setUserId(userInfoForWf.getUserId());
        user.setUserName(userInfoForWf.getUserName());
        user.setUserMobile(userInfoForWf.getUserMobilephone());
        user.setUserEmail(userInfoForWf.getUserEmail());
        return user;
    }

    @Override
    public List<String> getLowerOrgId(String orgCode) {
        return orgService.getLowerOrgId(orgCode);
    }

    @Override
    public List<WFUserExtDto> getUserDetailBatch(List<WFUserDto> list) {
        return nWfOcaTaskpoolMapper.getUserDetailBatch(list);
    }

    public List<WFUser> getWfUserList(List<AdminSmUserVo> users){
        return users.stream().map(vo -> {
            WFUser user = new WFUser();
            user.setUserId(vo.getUserId());
            user.setUserName(vo.getUserName());
            user.setUserMobile(vo.getUserMobilephone());
            user.setUserEmail(vo.getUserEmail());
            return user;
        }).collect(Collectors.toList());
    }
}
