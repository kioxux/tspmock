package cn.com.yusys.yusp.notice.dao;


import cn.com.yusys.yusp.notice.entity.AdminSmRicheditFileInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2021-03-31 11:00:50
 */
@Mapper
public interface AdminSmRicheditFileInfoDao extends BaseMapper<AdminSmRicheditFileInfoEntity> {

}
