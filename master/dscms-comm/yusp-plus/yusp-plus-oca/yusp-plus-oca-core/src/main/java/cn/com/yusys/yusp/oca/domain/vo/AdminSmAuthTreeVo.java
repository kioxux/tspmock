package cn.com.yusys.yusp.oca.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.List;
import java.util.Objects;

@Data
public class AdminSmAuthTreeVo {

    private String authRecoId;

    private String authresId;

    private String upTreeId;

    private String nodeName;

    private String authresType;

    private int orderColumn;

    // 1、表示该对象已有该权限，0 表示该对象没有分配该权限
    private int state;

    @TableField(exist = false)
    private List<AdminSmAuthTreeVo> children;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AdminSmAuthTreeVo that = (AdminSmAuthTreeVo) o;
        return Objects.equals(authresId, that.authresId);
    }

}
