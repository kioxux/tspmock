package cn.com.yusys.yusp.notice.form;

import lombok.Data;

import java.util.HashMap;
import java.util.List;

@Data
public class ResultForm extends HashMap<String, Object> {

    public ResultForm() {
        put("code", 0);
        put("total", 0);
        put("message", "");
        put("level", "");
        put("i18nData", "");
    }

    public static ResultForm ok() {
        return new ResultForm();
    }

    public static ResultForm ok(Object o) {
        ResultForm r = new ResultForm();
        r.put("data", o);
        return r;
    }

    @Override
    public ResultForm put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}
