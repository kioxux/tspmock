package cn.com.yusys.yusp.oca.domain.form;

import lombok.Data;

@Data
public class AdminTmplAuthForm {
    /**
     * 之前的数据模板授权的数据 id
     */
    private String lastAuthresId;
    /**
     * 授权对象 id
     */
    private String authobjId;
    /**
     * 授权对象类型
     */
    private String authobjType;
    /**
     * 新授权的数据模板 id
     */
    private String authresId;
    /**
     * 相关联的控制点 id
     */
    private String contrId;
}
