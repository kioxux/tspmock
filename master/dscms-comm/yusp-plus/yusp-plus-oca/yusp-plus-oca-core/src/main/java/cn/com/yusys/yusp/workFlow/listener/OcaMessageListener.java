package cn.com.yusys.yusp.workFlow.listener;


import cn.com.yusys.yusp.flow.client.ClientBizFactory;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @className OcaMessageListener
 * @Description 流程mq监听处理
 * @Date 2021年6月21日14:22:56
 */
@Service
public class OcaMessageListener {
    private final Logger log = LoggerFactory.getLogger(OcaMessageListener.class);


    private ClientBizFactory clientBizFactory;

    @Autowired
    public OcaMessageListener(ClientBizFactory clientBizFactory) {
        this.clientBizFactory = clientBizFactory;
    }

    @RabbitListener(queuesToDeclare = {
            @Queue("yusp-flow.XTGL01") //请假及提前签退审批流程

    })// 队列名称为【yusp-flow.流程申请类型】,可以添加多个
    @RabbitHandler
    public void receiveQueue(ResultInstanceDto message) {
        log.info("Oca服务客户端业务监听:" + message);
        clientBizFactory.processBiz(message);

    }
}



