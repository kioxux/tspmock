package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.common.utils.Query;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.oca.dao.AdminSmFuncModDao;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmFuncModBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmBusiFuncEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmFuncModEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmFuncModQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmFuncModVo;
import cn.com.yusys.yusp.oca.service.AdminSmBusiFuncService;
import cn.com.yusys.yusp.oca.service.AdminSmFuncModService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


@Service("adminSmFuncModService")
public class AdminSmFuncModServiceImpl extends ServiceImpl<AdminSmFuncModDao, AdminSmFuncModEntity> implements AdminSmFuncModService {

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    AdminSmBusiFuncService adminSmBusiFuncService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AdminSmFuncModEntity> page = this.page(
                new Query<AdminSmFuncModEntity>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

    @Override
    public Page<AdminSmFuncModVo> queryPageWithCondition(AdminSmFuncModQuery adminSmFuncModQuery) {

        Page<AdminSmFuncModVo> iPage = adminSmFuncModQuery.getIPage();

        QueryWrapper<AdminSmFuncModVo> queryWrapper = new QueryWrapper();
        queryWrapper.like(StringUtils.nonEmpty(adminSmFuncModQuery.getModelName()), "m.mod_name", adminSmFuncModQuery.getModelName());
        queryWrapper.orderByDesc("m.last_chg_dt");

        return this.baseMapper.queryPageWithCondition(iPage, queryWrapper);
    }

    /**
     * 删除模块信息，如模块已关联业务功能，不允许删除
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public int removeByModId(String[] modIdStr) {
        // 查询是否有关联业务功能
        List<AdminSmBusiFuncEntity> busiFuncList = adminSmBusiFuncService.list(new QueryWrapper<AdminSmBusiFuncEntity>()
                .in("mod_id", modIdStr));
        if (busiFuncList.size() > 0) {
            return -1;
        }
        return this.baseMapper.deleteBatchIds(Arrays.asList(modIdStr));
    }

    /**
     * 保存数据前查询模块名称是否已经存在
     * 移植原oca
     */
    @Override
    public List<String> checkName(String modName, String modId) {
        List<String> funcModIdList = (List<String>) (List) this.baseMapper.selectObjs(new QueryWrapper<AdminSmFuncModEntity>()
                .select("mod_id").eq(!StringUtils.isEmpty(modName), "mod_name", modName)
                .ne(!StringUtils.isEmpty(modId), "mod_id", modId)
        );
        return funcModIdList;
    }

    /**
     * 模块 保存
     *
     * @param adminSmFuncBo
     * @return
     */
    @Override
    public int saveFuncMod(AdminSmFuncModBo adminSmFuncBo) {

        //校验 模块名是否已存在
        List<String> strings = this.checkName(adminSmFuncBo.getModName(), adminSmFuncBo.getModId());
        if (strings.size() > 0) {
            return -1;
        }
        AdminSmFuncModEntity adminSmFuncModEntity =
                BeanUtils.beanCopy(adminSmFuncBo, AdminSmFuncModEntity.class);
        adminSmFuncModEntity.setLastChgUsr(SessionUtils.getUserId());
        return this.baseMapper.insert(adminSmFuncModEntity);
    }

    /**
     * 模块 修改
     *
     * @param adminSmFuncBo
     * @return
     */
    @Override
    public int updateFuncMod(AdminSmFuncModBo adminSmFuncBo) {

        //校验 模块名是否已存在
        List<String> strings = this.checkName(adminSmFuncBo.getModName(), adminSmFuncBo.getModId());
        if (strings.size() > 0) {
            return -1;
        }
        AdminSmFuncModEntity adminSmFuncModEntity =
                BeanUtils.beanCopy(adminSmFuncBo, AdminSmFuncModEntity.class);
        adminSmFuncModEntity.setLastChgUsr(SessionUtils.getUserId());
        return this.baseMapper.updateById(adminSmFuncModEntity);
    }
}