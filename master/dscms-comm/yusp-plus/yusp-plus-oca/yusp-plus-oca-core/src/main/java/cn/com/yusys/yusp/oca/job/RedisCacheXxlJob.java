package cn.com.yusys.yusp.oca.job;

import cn.com.yusys.yusp.oca.service.impl.RedisCacheLoadServicImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RedisCacheXxlJob {

    @Autowired
    private RedisCacheLoadServicImpl redisCacheLoadServic;

    public void insertUserToRedis(String param) throws Exception {
        redisCacheLoadServic.loadUserInfoData();
    }

    /**
     * 将机构信息初始化到redis
     */
    public void insertOrgToRedis(String param) throws Exception {
        redisCacheLoadServic.loadOrgInfoData();
    }

    /**
     * 将系统提示消息初始化到redis
     */
    public void insertDptToRedis(String param) throws Exception {
        redisCacheLoadServic.loadDeptInfoData();
    }

    /**
     * 将系统提示消息初始化到redis
     */
    public void insertMessageToRedis(String param) throws Exception {
        redisCacheLoadServic.loadSmMessageInfoData();
    }

}