package cn.com.yusys.yusp.oca.dao;

import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserDutyRelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;

/**
 * 用户岗位关联表
 *
 * @author 顾银华
 * @date 2021-08-13 21:55:19
 */

public interface AdminSmUserDutyRelDao extends BaseMapper<AdminSmUserDutyRelEntity> {
    /**
     * 根据岗位编号查询用户信息
     *
     * @param iPage
     * @param dutyCode
     * @return
     */
    IPage<AdminSmUserDto> getUserInfoByDutyCode(IPage<AdminSmUserDto> iPage, @Param("dutyCode") String dutyCode);
}
