package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.commons.redis.template.YuspRedisTemplate;
import cn.com.yusys.yusp.commons.session.SessionService;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.oca.domain.constants.Constants;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmDptEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmMessageEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmOrgEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Redis缓存加载数据
 *
 * @author yangzai
 * @since 2021/4/23
 **/
@Service
public class RedisCacheLoadServicImpl implements RedisCacheLoadService {

    private final Logger logger = LoggerFactory.getLogger(RedisCacheLoadServicImpl.class);

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private AdminSmRoleService adminSmRoleService;

    @Autowired
    private AdminSmDptService adminSmDptService;

    @Autowired
    private AdminSmPropService adminSmPropService;

    @Autowired
    private AdminSmMessageService adminSmMessageService;

    @Autowired
    private YuspRedisTemplate yuspRedisTemplate;

    @Autowired
    private AdminSmLookupDictService adminSmLookupDictService;

    @Autowired
    private SessionService ocaSessionService;

    @Autowired
    private AdminSmLoginLogService adminSmLoginLogService;

    @Override
    public void loadUserInfoData() {
        // TODO 查询待优化
        List<AdminSmUserEntity> list = adminSmUserService.list();
        // 使用hash类型存储字典 例如: redisKey -> userName  HashKey -> admin HashValue -> 系统管理员
        Map<String, Object> userMap = list.stream().collect(Collectors.toMap(AdminSmUserEntity::getLoginCode, AdminSmUserEntity::getUserName));
        yuspRedisTemplate.hmset(Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_USER_NAME, userMap);
    }

    @Override
    public void loadOrgInfoData() {
        // TODO 查询待优化
        List<AdminSmOrgEntity> list = adminSmOrgService.list();
        // 使用hash类型存储字典 例如: redisKey -> orgName  HashKey -> 100201 HashValue -> 银行
        Map<String, Object> orgMap = list.stream().collect(Collectors.toMap(AdminSmOrgEntity::getOrgCode, AdminSmOrgEntity::getOrgName));
        yuspRedisTemplate.hmset(Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_ORG_NAME, orgMap);
    }

    @Override
    public void loadOrgTreeData() {
        List<AdminSmOrgEntity> list = adminSmOrgService.list();
        for (AdminSmOrgEntity entity : list) {
            String orgSeq = entity.getOrgSeq();
            String orgCode = entity.getOrgCode();
            List<String> orgCodes = new ArrayList<>();
            for (AdminSmOrgEntity cEntity : list) {
                if (cEntity.getOrgSeq().contains(orgSeq)) {
                    orgCodes.add(cEntity.getOrgCode());
                }
            }
            yuspRedisTemplate.hset(Constants.SystemUserConstance.REDIS_KEY_ORG_TREE, orgCode, orgCodes);
        }
    }

    @Override
    public void loadDeptInfoData() {
        //初始化部门信息
        // TODO 查询待优化
        List<AdminSmDptEntity> dptList = adminSmDptService.list();
        Map<String, Object> dptMap = dptList.stream().collect(Collectors.toMap(AdminSmDptEntity::getDptId, AdminSmDptEntity::getDptName));
        yuspRedisTemplate.hmset(Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_DPT_NAME, dptMap);
    }

    @Override
    public void loadSmMessageInfoData() {
        // 初始化系统提示信息
        // TODO 查询待优化
        List<AdminSmMessageEntity> messageList = adminSmMessageService.list();
        // 使用hash类型存储字典 例如: redisKey -> message  HashKey -> 40 HashValue -> 您好，该时间段不允许登录系统，请您谅解！
        Map<String, Object> messageMap = messageList.stream().collect(Collectors.toMap(AdminSmMessageEntity::getCode, AdminSmMessageEntity::getMessage));
        yuspRedisTemplate.hmset(Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_SYSTEM_TIP_MESSAGE, messageMap);
    }

    @Override
    public void loadSmLookupDictInfoData() {
        // 初始化字典项信息
        this.adminSmLookupDictService.refreshLookupDict();
    }

    @Override
    @Deprecated
    public void loadAreaXwUserData() {
        // 改为在cmis-biz服务中加载此缓存数据
        // 获取所有的小微分中心负责人
//        List<AdminSmUserDto> xwAreaManagerList = adminSmRoleService.getUserInfoByRoleCode(Constants.SystemUserConstance.ROLE_CODE_XW_AREA_MANAGER);
//        if(CollectionUtils.nonEmpty(xwAreaManagerList)){
//            for (AdminSmUserDto adminSmUserDto: xwAreaManagerList) {
//                // TODO 调用cmis-biz服务，获取指定小微分中心负责人的管辖的所有小微客户经理
//                String userCode = adminSmUserDto.getUserCode();
//
//                List<String> xwUserCodes = new ArrayList<>();
//                yuspRedisTemplate.hset(Constants.SystemUserConstance.REDIS_KEY_AREA_XW_USER, userCode, xwUserCodes);
//            }
//        }
    }

    @Override
    public void loadJzzyUserData() {
        yuspRedisTemplate.del(Constants.SystemUserConstance.ROLE_CODE_JZZY_DA);
        // 1、查询所有 【集中作业档案人员 R1034】 人员
//        List<AdminSmUserVo> jzzyDaList = adminSmUserService.getUsersByRoleForWf(Constants.SystemUserConstance.ROLE_CODE_JZZY_DA);
        // 1、更正 所有用户
        List<AdminSmUserEntity> allSysUserList = adminSmUserService.list();
        // 2、集中作业总行本地机构（除小微）下的所有用户
        List<AdminSmUserEntity> userList = adminSmUserService.getJzzyUserList();
        // 3、为所有【集中作业档案人员 R1034】人员添加【集中作业总行本地机构（除小微）下的所有用户】信息缓存
        if (CollectionUtils.nonEmpty(allSysUserList) && CollectionUtils.nonEmpty(userList)) {
            for (AdminSmUserEntity adminSmUserVo : allSysUserList) {
                yuspRedisTemplate.hset(Constants.SystemUserConstance.REDIS_KEY_JZZY_USER, adminSmUserVo.getUserId(), userList.stream().map(AdminSmUserEntity::getUserId).collect(Collectors.toList()));
            }
        }
    }

    @Override
    public void loadAllXwUserData() {
        yuspRedisTemplate.del(Constants.SystemUserConstance.ROLE_CODE_XW_FKSJ_MANAGER);
        // 1、查询所有 【小微风控审计部负责人 R1026】 人员
//        List<AdminSmUserVo> xwFksjList = adminSmUserService.getUsersByRoleForWf(Constants.SystemUserConstance.ROLE_CODE_XW_FKSJ_MANAGER);
        // 1、更正 所有用户
        List<AdminSmUserEntity> allSysUserList = adminSmUserService.list();
        // 2、查询所有小微客户经理
        List<AdminSmUserEntity> userList = adminSmUserService.getAllXwUserList();
        // 3、为所有【小微风控审计部负责人 R1026】人员添加【所有小微客户经理】信息缓存
        if (CollectionUtils.nonEmpty(allSysUserList) && CollectionUtils.nonEmpty(userList)) {
            for (AdminSmUserEntity adminSmUserVo : allSysUserList) {
                yuspRedisTemplate.hset(Constants.SystemUserConstance.REDIS_KEY_ALL_XW_USER, adminSmUserVo.getUserId(), userList.stream().map(AdminSmUserEntity::getUserId).collect(Collectors.toList()));
            }
        }
    }

    @Override
    public void loadXwUserForOrg() {
//        yuspRedisTemplate.del(Constants.SystemUserConstance.REDIS_KEY_ORG_XW_USER);
        // 1、所有用户
        List<AdminSmUserEntity> allSysUserList = adminSmUserService.list();
        if (CollectionUtils.nonEmpty(allSysUserList)) {
            for (AdminSmUserEntity adminSmUserVo : allSysUserList) {
                // 2、查询机构下小微客户经理
                List<String> orgCodes = adminSmOrgService.getLowerOrgId(adminSmUserVo.getOrgId());
                List<AdminSmUserEntity> userList = adminSmUserService.getXwUserForOrg(orgCodes);
                if (CollectionUtils.nonEmpty(userList)) {
                    yuspRedisTemplate.hset(Constants.SystemUserConstance.REDIS_KEY_ORG_XW_USER, adminSmUserVo.getUserId(), userList.stream().map(AdminSmUserEntity::getUserId).collect(Collectors.toList()));
                }
            }
        }
    }

    @Override
    public void loadNotXwUserForOrg() {
        // yuspRedisTemplate.del(Constants.SystemUserConstance.REDIS_KEY_ORG_NOT_XW_USER);
        // 1、所有用户
        List<AdminSmUserEntity> allSysUserList = adminSmUserService.list();
        if (CollectionUtils.nonEmpty(allSysUserList)) {
            for (AdminSmUserEntity adminSmUserVo : allSysUserList) {
                // 2、查询机构下非小微用户
                List<String> orgCodes = adminSmOrgService.getLowerOrgId(adminSmUserVo.getOrgId());
                List<AdminSmUserEntity> userList = adminSmUserService.getNotXwUserForOrg(orgCodes);
                if (CollectionUtils.nonEmpty(userList)) {
                    yuspRedisTemplate.hset(Constants.SystemUserConstance.REDIS_KEY_ORG_NOT_XW_USER, adminSmUserVo.getUserId(), userList.stream().map(AdminSmUserEntity::getUserId).collect(Collectors.toList()));
                }
            }
        }
    }

    @Override
    public void loadOpenDayData() {
        adminSmLoginLogService.getOpenDay();
    }

    @Override
    @Async
    public void loadAllSystemCacheData() {
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载用户信息到Redis>>>>>>>>>>>>>>>>>>>");
        loadUserInfoData();
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载用户信息到Redis结束>>>>>>>>>>>>>>>>>>>");

        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载机构信息到Redis>>>>>>>>>>>>>>>>>>>");
        loadOrgInfoData();
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载机构信息到Redis结束>>>>>>>>>>>>>>>>>>>");

        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载机构树信息到Redis>>>>>>>>>>>>>>>>>>>");
        loadOrgTreeData();
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载机构树信息到Redis结束>>>>>>>>>>>>>>>>>>>");

        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载区域中心负责人下小微客户经理信息到Redis>>>>>>>>>>>>>>>>>>>");
        loadAreaXwUserData();
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载区域中心负责人下小微客户经理信息到Redis结束>>>>>>>>>>>>>>>>>>>");

        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载集中作业总行本地机构（除小微）下的所有用户信息到Redis>>>>>>>>>>>>>>>>>>>");
        loadJzzyUserData();
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载集中作业总行本地机构（除小微）下的所有用户信息到Redis结束>>>>>>>>>>>>>>>>>>>");

        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载所有小微客户经理信息到Redis>>>>>>>>>>>>>>>>>>>");
        loadAllXwUserData();
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载所有小微客户经理到Redis结束>>>>>>>>>>>>>>>>>>>");

        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载部门信息到Redis>>>>>>>>>>>>>>>>>>>");
        loadDeptInfoData();
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载部门信息到Redis结束>>>>>>>>>>>>>>>>>>>");

        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载系统提示信息到Redis>>>>>>>>>>>>>>>>>>>");
        loadSmMessageInfoData();
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载系统提示信息到Redis结束>>>>>>>>>>>>>>>>>>>");

        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载字典项到Redis>>>>>>>>>>>>>>>>>>>");
        loadSmLookupDictInfoData();
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载字典项到Redis结束>>>>>>>>>>>>>>>>>>>");

        //初始化控制点信息
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载控制点信息到Redis>>>>>>>>>>>>>>>>>>>");
        ocaSessionService.getAllControls();
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载控制点信息到Redis结束>>>>>>>>>>>>>>>>>>>");

        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载营业日期到Redis>>>>>>>>>>>>>>>>>>>");
        loadOpenDayData();
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载营业日期到Redis结束>>>>>>>>>>>>>>>>>>>");

        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载机构下小微客户经理信息到Redis>>>>>>>>>>>>>>>>>>>>>>>>>>");
        loadXwUserForOrg();
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>结束加载机构下小微客户经理信息到Redis>>>>>>>>>>>>>>>>>>>>>>>>>>");

        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载机构下非小微用户信息到Redis>>>>>>>>>>>>>>>>>>>>>>>>>>");
        loadNotXwUserForOrg();
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>结束加载机构下非小微用户信息到Redis>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }

}