package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户角色关联表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-01 21:55:19
 */
@Data
@NoArgsConstructor
@TableName("admin_sm_user_duty_rel")
public class AdminSmUserDutyRelEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 记录编号
	 */
	@TableId(type = IdType.UUID)
	private String userDutyRelId;
	/**
	 * 用户编号
	 */
	private String userId;
	/**
	 * 岗位编号
	 */
	private String dutyId;

	/**
	 * 最新变更用户
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
	/**
	 * 最新变更时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

	public AdminSmUserDutyRelEntity(String userId, String dutyId) {
		this.userId = userId;
		this.dutyId = dutyId;
	}
}
