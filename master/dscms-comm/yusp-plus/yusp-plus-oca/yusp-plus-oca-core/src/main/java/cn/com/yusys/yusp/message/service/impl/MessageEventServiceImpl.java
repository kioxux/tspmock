package cn.com.yusys.yusp.message.service.impl;


import cn.com.yusys.yusp.message.dao.MessageEventDao;
import cn.com.yusys.yusp.message.entity.MessageEventEntity;
import cn.com.yusys.yusp.message.service.MessageEventService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 消息事件服务类
 *
 * @author xiaodg@yusys.com.cn
 */
@Service
@Slf4j
public class MessageEventServiceImpl extends ServiceImpl<MessageEventDao, MessageEventEntity> implements MessageEventService {

}