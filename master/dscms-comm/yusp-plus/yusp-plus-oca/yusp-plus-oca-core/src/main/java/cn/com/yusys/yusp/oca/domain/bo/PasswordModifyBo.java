package cn.com.yusys.yusp.oca.domain.bo;

import cn.com.yusys.yusp.oca.config.annotation.PasswordStrategy;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * @program: yusp-plus
 * @description: 密码修改接收参数
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2020-12-14 15:07
 */
@ApiModel(description = "用户密码修改接收类", value = "用户密码修改接收类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PasswordModifyBo {

    @ApiModelProperty(name = "sysId", value = "客户端Id", required = false, example = "test")
    private String sysId;

    @ApiModelProperty(name = "rawPassword", value = "原密码", required = true, example = "test")
//    @NotEmpty(message = "{password}")
    @NotEmpty
    private String rawPassword;

    @ApiModelProperty(name = "password", value = "修改后密码", required = true, example = "test")
    @NotEmpty(message = "密码不能为空")
    @PasswordStrategy
    private String password;

    @ApiModelProperty(name = "loginCode", value = "用户名", required = false, example = "test")
    private String loginCode;

    @ApiModelProperty(name = "lastChgUsr", value = "最后修改者", required = false, example = "test")
    @TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
}
