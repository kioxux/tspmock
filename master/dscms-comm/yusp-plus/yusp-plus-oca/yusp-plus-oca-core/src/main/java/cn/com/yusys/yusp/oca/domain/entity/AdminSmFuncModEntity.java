package cn.com.yusys.yusp.oca.domain.entity;

import cn.com.yusys.yusp.oca.domain.vo.AdminSmContrTreeVo;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统功能模块表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-26 10:50:57
 */
@Data
@TableName("admin_sm_func_mod")
public class AdminSmFuncModEntity extends AdminSmContrTreeVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 记录编号
     */
    @TableId
    private String modId;
    /**
     * 模块名称
     */
    private String modName;
    /**
     * 模块描述
     */
    private String modDesc;
    /**
     * 是否外部系统
     */
    private String isOuter;
    /**
     * 是否APP功能
     */
    private String isApp;
    /**
     * 外部系统登录名
     */
    private String userName;
    /**
     * 外部系统登录密码
     */
    private String password;
    /**
     * 外部系统用户变量名称
     */
    private String userKey;
    /**
     * 外部系统密码变量名称
     */
    private String pwdKey;
    /**
     * 最新变更用户
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
    /**
     * 最新变更时间
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

}
