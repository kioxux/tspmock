package cn.com.yusys.yusp.notice.vo;

import lombok.Data;

@Data
public class AdminSmReciveVo {

    private String roleId;

    private String orgId;
}
