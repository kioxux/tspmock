package cn.com.yusys.yusp.oca.domain.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

/**
 * 数据字典内容表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2021-01-15 16:44:33
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdminSmLookupDictVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 字典项编号，默认uuid
     */
    private String lookupItemId;
    /**
     * 字典类别code码
     */
    private String lookupCode;
    /**
     * 字典类别名称
     */
    private String lookupName;
    /**
     * 字典类别分类标识id
     */
    private String lookupTypeId;
    /**
     * 字典类别分类标识名称
     */
    private String lookupTypeName;
    /**
     * 上级字典内容编号
     */
    private String upLookupItemId;
    /**
     * 字典代码
     */
    private String lookupItemCode;
    /**
     * 字典名称
     */
    private String lookupItemName;
    /**
     * 字典备注说明
     */
    private String lookupItemComment;
    /**
     * 字典项排序
     */
    private Integer lookupItemOrder;
    /**
     * 金融机构编号
     */
    private String instuId;

    /**
     * 详情的标识字段
     */
    private String lookupItemsString;

}
