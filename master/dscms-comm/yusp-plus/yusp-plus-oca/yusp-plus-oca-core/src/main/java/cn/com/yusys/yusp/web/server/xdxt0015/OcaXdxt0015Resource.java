package cn.com.yusys.yusp.web.server.xdxt0015;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.OcaTradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxt0015.req.Xdxt0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0015.resp.Xdxt0015DataRespDto;
import cn.com.yusys.yusp.enums.OcaTradeEnum;
import cn.com.yusys.yusp.service.server.xdxt0015.Xdxt0015Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:用户机构角色信息列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0015:用户机构角色信息列表查询")
@RestController
@RequestMapping("/api/ocaxt4bsp")
public class OcaXdxt0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(OcaXdxt0015Resource.class);

    @Autowired
    private Xdxt0015Service xdxt0015Service;

    /**
     * 交易码：xdxt0015
     * 交易描述：用户机构角色信息列表查询
     *
     * @param xdxt0015DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("用户机构角色信息列表查询")
    @PostMapping("/xdxt0015")
    protected @ResponseBody
    ResultDto<Xdxt0015DataRespDto> xdxt0015(@Validated @RequestBody Xdxt0015DataReqDto xdxt0015DataReqDto) throws Exception {
        logger.info(OcaTradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0015.key, OcaTradeEnum.TRADE_CODE_XDXT0015.value, JSON.toJSONString(xdxt0015DataReqDto));
        Xdxt0015DataRespDto xdxt0015DataRespDto = new Xdxt0015DataRespDto();// 响应Dto:用户机构角色信息列表查询
        ResultDto<Xdxt0015DataRespDto> xdxt0015DataResultDto = new ResultDto<>();
        try {
            logger.info(OcaTradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0014.key, OcaTradeEnum.TRADE_CODE_XDXT0014.value, JSON.toJSONString(xdxt0015DataReqDto));
            xdxt0015DataRespDto = xdxt0015Service.getXdxt0015(xdxt0015DataReqDto);
            logger.info(OcaTradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0014.key, OcaTradeEnum.TRADE_CODE_XDXT0014.value, JSON.toJSONString(xdxt0015DataRespDto));

            // 封装xdxt0015DataResultDto中正确的返回码和返回信息
            xdxt0015DataResultDto.setCode(OcaTradeEnum.CMIS_SUCCSESS.key);
            xdxt0015DataResultDto.setMessage(OcaTradeEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(OcaTradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0015.key, OcaTradeEnum.TRADE_CODE_XDXT0015.value, e.getMessage());
            // 封装xdtz0001DataResultDto中异常返回码和返回信息
            xdxt0015DataResultDto.setCode(e.getErrorCode());
            xdxt0015DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(OcaTradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0015.key, OcaTradeEnum.TRADE_CODE_XDXT0015.value, e.getMessage());
            // 封装xdxt0015DataResultDto中异常返回码和返回信息
            xdxt0015DataResultDto.setCode(OcaTradeEnum.EPB099999.key);
            xdxt0015DataResultDto.setMessage(OcaTradeEnum.EPB099999.value);
        }
        // 封装xdxt0015DataRespDto到xdxt0015DataResultDto中
        xdxt0015DataResultDto.setData(xdxt0015DataRespDto);
        logger.info(OcaTradeLogConstants.RESOURCE_END_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0015.key, OcaTradeEnum.TRADE_CODE_XDXT0015.value, JSON.toJSONString(xdxt0015DataResultDto));
        return xdxt0015DataResultDto;
    }
}
