package cn.com.yusys.yusp.oca.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class UserMgrOrgVo {
    private String orgId;
    private String orgName;
    private String upOrgId;
    private Boolean checked=false;
    private List<UserMgrOrgVo> children;
}
