package cn.com.yusys.yusp.oca.config;

import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * mp 自动填充，主要用于oca 日期存储
 *
 * @author Administrator
 */
@Component
public class MetaObjectHandlerConfig implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        //保存时间
        this.setFieldValByName("lastChgDt", DateUtils.getCurrDate(), metaObject);
        if (this.getFieldValByName("lastChgUsr", metaObject) == null) {
            this.setFieldValByName("lastChgUsr", Optional.ofNullable(SessionUtils.getLoginCode()).orElse("sys"), metaObject);
        }

    }

    @Override
    public void updateFill(MetaObject metaObject) {
        //修改时间
        this.setFieldValByName("lastChgDt", DateUtils.getCurrDate(), metaObject);
        if (this.getFieldValByName("lastChgUsr", metaObject) == null) {
            this.setFieldValByName("lastChgUsr", Optional.ofNullable(SessionUtils.getLoginCode()).orElse("sys"), metaObject);
        }
    }

}