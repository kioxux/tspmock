package cn.com.yusys.yusp.notice.form;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AdminSmRicheditFileInfoForm {
    /**
     * 文件 id
     */
    private String fileId;
    /**
     * 文件大小
     */
    private BigDecimal fileSize;
    /**
     * oss 保存完整路径
     */
    private String filePath;
    /**
     * 文件后缀
     */
    private String extName;
    /**
     * 文件名称
     */
    private String fileName;
    /**
     * 公告 id
     */
    private String busNo;
    /**
     * 文件虚拟路径
     */
    private String parentFolder;
}
