package cn.com.yusys.yusp.web.server.xdxt0003;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.OcaTradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxt0003.req.Xdxt0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0003.resp.Xdxt0003DataRespDto;
import cn.com.yusys.yusp.enums.OcaTradeEnum;
import cn.com.yusys.yusp.service.server.xdxt0003.Xdxt0003Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:分页查询小微客户经理
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0003:分页查询小微客户经理")
@RestController
@RequestMapping("/api/ocaxt4bsp")
public class OcaXdxt0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(OcaXdxt0003Resource.class);

    @Autowired
    private Xdxt0003Service xdxt0003Service;

    /**
     * 交易码：xdxt0003
     * 交易描述：分页查询小微客户经理
     *
     * @param xdxt0003DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("分页查询小微客户经理")
    @PostMapping("/xdxt0003")
    protected @ResponseBody
    ResultDto<Xdxt0003DataRespDto> xdxt0003(@Validated @RequestBody Xdxt0003DataReqDto xdxt0003DataReqDto) throws Exception {
        logger.info(OcaTradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0003.key, OcaTradeEnum.TRADE_CODE_XDXT0003.value, JSON.toJSONString(xdxt0003DataReqDto));
        Xdxt0003DataRespDto xdxt0003DataRespDto = new Xdxt0003DataRespDto();// 响应Dto:分页查询小微客户经理
        ResultDto<Xdxt0003DataRespDto> xdxt0003DataResultDto = new ResultDto<>();
        try {
            // 从xdxt0003DataReqDto获取业务值进行业务逻辑处理
            logger.info(OcaTradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0003.key, OcaTradeEnum.TRADE_CODE_XDXT0003.value, JSON.toJSONString(xdxt0003DataReqDto));
            xdxt0003DataRespDto = xdxt0003Service.getXdxt0003(xdxt0003DataReqDto);
            logger.info(OcaTradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0003.key, OcaTradeEnum.TRADE_CODE_XDXT0003.value, JSON.toJSONString(xdxt0003DataReqDto));
            // 封装xdxt0003DataResultDto中正确的返回码和返回信息
            xdxt0003DataResultDto.setCode(OcaTradeEnum.CMIS_SUCCSESS.key);
            xdxt0003DataResultDto.setMessage(OcaTradeEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(OcaTradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0003.key, OcaTradeEnum.TRADE_CODE_XDXT0003.value, e.getMessage());
            // 封装xdxt0003DataResultDto中异常返回码和返回信息
            xdxt0003DataResultDto.setCode(OcaTradeEnum.EPB099999.key);
            xdxt0003DataResultDto.setMessage(OcaTradeEnum.EPB099999.value);
        }
        // 封装xdxt0003DataRespDto到xdxt0003DataResultDto中
        xdxt0003DataResultDto.setData(xdxt0003DataRespDto);
        logger.info(OcaTradeLogConstants.RESOURCE_END_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0003.key, OcaTradeEnum.TRADE_CODE_XDXT0003.value, JSON.toJSONString(xdxt0003DataResultDto));
        return xdxt0003DataResultDto;
    }
}
