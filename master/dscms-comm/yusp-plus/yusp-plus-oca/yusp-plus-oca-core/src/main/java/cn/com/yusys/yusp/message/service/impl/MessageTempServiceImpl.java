package cn.com.yusys.yusp.message.service.impl;

import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.message.dao.MessageTempDao;
import cn.com.yusys.yusp.message.entity.MessageTempEntity;
import cn.com.yusys.yusp.message.query.MessageTempQuery;
import cn.com.yusys.yusp.message.service.MessageTempService;
import cn.com.yusys.yusp.message.util.MessageUtils;
import cn.com.yusys.yusp.message.vo.MessageTempVo;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 消息模板服务类
 *
 * @author xiaodg@yusys.com.cn
 */
@Service("messageTempService")
@Slf4j
public class MessageTempServiceImpl extends ServiceImpl<MessageTempDao, MessageTempEntity> implements MessageTempService {

    private MessageTempVo convert(MessageTempEntity messageTempEntity) {
        return BeanUtils.beanCopy(messageTempEntity, MessageTempVo.class);
    }

    private void validMessageTypeAndMessageChannelType(String messageType, String channelType) {
        MessageUtils.requireNonNullAndEmptyString(messageType, "messageType不能为空");
        MessageUtils.requireNonNullAndEmptyString(channelType, "channelType不能为空");
    }

    private List<MessageTempEntity> queryByMessageTypeAndChannelType(String messageType, String channelType) {
        validMessageTypeAndMessageChannelType(messageType, channelType);
        return this.lambdaQuery()
                .eq(MessageTempEntity::getMessageType, messageType)
                .eq(MessageTempEntity::getChannelType, channelType)
                .list();
    }

    @Override
    public List<MessageTempVo> queryBy(MessageTempQuery messageTempQuery) {
        return this.queryByMessageTypeAndChannelType(messageTempQuery.getMessageType(), messageTempQuery.getChannelType())
                .stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    @Override
    public Boolean saveOrUpdateMessageTemp(MessageTempEntity messageTempEntity) {
        final String messageType = messageTempEntity.getMessageType();
        final String channelType = messageTempEntity.getChannelType();
        validMessageTypeAndMessageChannelType(messageType, channelType);
        final List<MessageTempEntity> temps = this.queryByMessageTypeAndChannelType(messageType, channelType);
        try {
            if (Objects.nonNull(temps) && temps.size() > 0) {
                update(messageTempEntity, this.lambdaUpdate().eq(MessageTempEntity::getMessageType, messageType).eq(MessageTempEntity::getChannelType, channelType).getWrapper());
            } else {
                save(messageTempEntity);
            }
        } catch (Exception e) {
            log.error("保存或更新消息模板[{}]失败, 异常: {} ", messageType, e);
            return false;
        }
        return true;
    }

}