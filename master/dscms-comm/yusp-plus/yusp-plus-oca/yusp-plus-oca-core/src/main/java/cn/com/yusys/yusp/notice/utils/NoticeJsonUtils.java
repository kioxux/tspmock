package cn.com.yusys.yusp.notice.utils;

import cn.com.yusys.yusp.notice.form.NoticeConditionForm;
import cn.com.yusys.yusp.notice.form.NoticeRoleForm;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

/**
 * 该类用于转换json格式的数据为实体类
 * @param
 */
public class NoticeJsonUtils {

    public static Object getCondition(String jsonString) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JavaType javaType = mapper.getTypeFactory()
                .constructParametricType(List.class, NoticeRoleForm.class);
        JavaType javaType1 = mapper.getTypeFactory()
                .constructParametricType(NoticeConditionForm.class, javaType);
        Object o = mapper.readValue(jsonString, javaType1);
        return o;
    }
}
