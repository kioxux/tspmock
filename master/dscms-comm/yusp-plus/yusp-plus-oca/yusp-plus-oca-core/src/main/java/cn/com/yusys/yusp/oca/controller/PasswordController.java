package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.oca.domain.bo.*;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import cn.com.yusys.yusp.oca.service.PasswordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.xml.transform.Result;

/**
 * @program: yusp-plus
 * @description: 密码相关操作
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2020-12-14 11:28
 */
@Api("密码相关请求")
@Slf4j
@RestController
@RequestMapping("/api")
public class PasswordController {
    private final Logger log = LoggerFactory.getLogger(PasswordController.class);

    @Autowired
    PasswordService passwordService;
    @Autowired
    AdminSmUserService adminSmUserService;

    /**
     * 用户密码校验
     */
    @ApiOperation(value = "密码验密", notes = "密码验密")
    @PostMapping("/passwordcheck/checkpwd")
    public ResultDto checkPasswrod(@Valid @RequestBody PasswordCheckBo passwordCheckBo) {
        ResultDto passwordCheckResult = passwordService.checkPasswrod(passwordCheckBo);
        return passwordCheckResult;
    }

    /**
     * 密码修改
     *
     * @param passwordModifyBo
     * @return
     * @description: 原密码匹配，新密码校验
     */
    @ApiOperation(value = "密码修改", notes = "密码修改")
    @PostMapping("/password/passwordmodification")
    public ResultDto passwordModification(@Valid @RequestBody PasswordModifyBo passwordModifyBo) {
        ResultDto result = passwordService.passwordModification(passwordModifyBo);
        return result;
    }

    /**
     * 密码重置
     *
     * @param
     * @return
     * @description: 密码重置为123456
     */
    @ApiOperation(value = "密码重置", notes = "密码重置")
    @PostMapping("/password/resetpassword")
    public ResultDto resetPassword(@Valid @RequestBody PasswordResetBo passwordResetBo) {
        ResultDto result = passwordService.resetPassword(passwordResetBo);
        return result;
    }

    @ApiOperation(value = "忘记密码重置时校验账号手机号并发送验证码")
    @PostMapping("/password/sendVerificationCode")
    public ResultDto sendVerificationCode(@Valid @RequestBody PasswordForgetAndResetCheckBo passwordForgetAndResetCheckBo) {
        return passwordService.sendVerificationCode(passwordForgetAndResetCheckBo);
    }
    @ApiOperation(value = "登录时校验账号手机号并发送验证码")
    @PostMapping("/password/sendLoginVerificationCode")
    public ResultDto sendLoginVerificationCode(@Valid @RequestBody PasswordForgetAndResetCheckBo passwordForgetAndResetCheckBo) {
        AdminSmUserEntity adminSmUserEntity = adminSmUserService.getAllByLoginCode(passwordForgetAndResetCheckBo.getLoginCode());
        log.info(adminSmUserEntity.getUserMobilephone());
        passwordForgetAndResetCheckBo.setMobile(adminSmUserEntity.getUserMobilephone());
        return passwordService.sendVerificationCode(passwordForgetAndResetCheckBo);
    }

    @ApiOperation(value = "忘记密码校验手机验证码")
    @PostMapping("/password/checkVerificationCode")
    public ResultDto checkVerificationCode(@Valid @RequestBody PasswordForgetAndResetCheckBo passwordForgetAndResetCheckBo) {
        AdminSmUserEntity adminSmUserEntity = adminSmUserService.getAllByLoginCode(passwordForgetAndResetCheckBo.getLoginCode());
        passwordForgetAndResetCheckBo.setMobile(adminSmUserEntity.getUserMobilephone());
        return passwordService.checkVerificationCode(passwordForgetAndResetCheckBo);
    }


    @ApiOperation(value = "忘记密码重置密码")
    @PostMapping("/password/forgetAndResetPassword")
    public ResultDto forgetAndResetPassword(@Valid @RequestBody PasswordForgetAndResetBo passwordForgetAndResetBo) {
        return passwordService.forgetAndResetPassword(passwordForgetAndResetBo);
    }

    @ApiOperation(value = "密码验证绑定手机号")
    @PostMapping("/password/mobileBinding")
    public ResultDto mobileBinding(@Valid @RequestBody MobileBindingBo mobileBindingBo) {
        return passwordService.mobileBinding(mobileBindingBo);
    }
}
