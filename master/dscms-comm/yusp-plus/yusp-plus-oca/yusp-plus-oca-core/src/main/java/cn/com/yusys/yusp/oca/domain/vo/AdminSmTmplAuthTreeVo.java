package cn.com.yusys.yusp.oca.domain.vo;

import lombok.Data;


@Data
public class AdminSmTmplAuthTreeVo extends AdminSmAuthTreeVo {

    private String sqlName;

    private String sqlString;
}
