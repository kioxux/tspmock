package cn.com.yusys.yusp.service.server.xdxt0003;

import cn.com.yusys.yusp.common.utils.RAMPager;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.OcaTradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxt0003.req.Xdxt0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0003.resp.Xdxt0003DataRespDto;
import cn.com.yusys.yusp.enums.OcaTradeEnum;
import cn.com.yusys.yusp.oca.dao.AdminSmUserDao;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmUserVo;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: yusp-oca模块
 * @类名称: Xdxt0003Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-25 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxt0003Service extends ServiceImpl<AdminSmUserDao, AdminSmUserEntity> {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0003Service.class);

    @Autowired
    private AdminSmUserService adminSmUserService;

    /**
     * 分页查询小微客户经理
     *
     * @return
     */
    @Transactional
    public Xdxt0003DataRespDto getXdxt0003(Xdxt0003DataReqDto xdxt0003DataReqDto) {
        logger.info(OcaTradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0003.key, OcaTradeEnum.TRADE_CODE_XDXT0003.value, JSON.toJSONString(xdxt0003DataReqDto));
        Xdxt0003DataRespDto xdxt0003DataRespDto = new Xdxt0003DataRespDto();
        try {
            String loginCode = "";
            String userName = "";
            if (StringUtils.nonEmpty(xdxt0003DataReqDto.getManagerId())) {
                loginCode = xdxt0003DataReqDto.getManagerId();
            }
            if (StringUtils.nonEmpty(xdxt0003DataReqDto.getManagerName())) {
                userName = xdxt0003DataReqDto.getManagerName();
            }
            List<AdminSmUserVo> adminSmUserVos = this.baseMapper.getByLoginCodeAndUsernamePage(loginCode, userName);
            //代码分页
            RAMPager<AdminSmUserVo> pager = new RAMPager<>(adminSmUserVos, Integer.valueOf(xdxt0003DataReqDto.getPageSize()));
            adminSmUserVos = pager.page(Integer.valueOf(xdxt0003DataReqDto.getPageNum()));
            List<cn.com.yusys.yusp.dto.server.xdxt0003.resp.List> list = adminSmUserVos.stream().map(e -> {
                cn.com.yusys.yusp.dto.server.xdxt0003.resp.List xdxt0003List = new cn.com.yusys.yusp.dto.server.xdxt0003.resp.List();
                xdxt0003List.setManagerId(e.getLoginCode());
                xdxt0003List.setManagerName(e.getUserName());
                return xdxt0003List;
            }).collect(Collectors.toList());
            xdxt0003DataRespDto.setList(list);
            xdxt0003DataRespDto.setNum(String.valueOf(adminSmUserVos.size()));
        } catch (Exception e) {
            logger.info(OcaTradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0003.key, OcaTradeEnum.TRADE_CODE_XDXT0003.value, e.getMessage());
        }
        logger.info(OcaTradeLogConstants.SERVICE_END_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0003.key, OcaTradeEnum.TRADE_CODE_XDXT0003.value, JSON.toJSONString(xdxt0003DataRespDto));
        return xdxt0003DataRespDto;
    }
}
