package cn.com.yusys.yusp.oca.domain.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * 系统用户 excel解析实体
 *
 */
@Data
public class AdminSmUserExcelVo  {


	private String userId;

	@ExcelProperty("账号")
	private String loginCode;

	@ExcelProperty("用户名")
	private String userName;

	@ExcelProperty("工号")
	private String userCode;

	@ExcelProperty("机构代码")
	private String orgId;

	@ExcelProperty("性别")
	private String userSex;

	@ExcelProperty("角色代码")
	private String roleId;


}
