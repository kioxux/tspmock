package cn.com.yusys.yusp.oca.config;


import cn.com.yusys.yusp.oca.domain.entity.AdminSmCrelStraEntity;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @description: 类说明
 * @author: zhangsong
 * @date: 2021/3/30
 */

@Data
@Component
//@ConfigurationProperties(prefix = "crel")
public class CrelStraConfig {
    private List<AdminSmCrelStraEntity> strategy;
}
