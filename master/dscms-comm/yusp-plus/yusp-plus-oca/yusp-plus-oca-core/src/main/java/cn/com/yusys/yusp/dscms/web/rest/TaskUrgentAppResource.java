/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dscms.web.rest;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.dscms.domain.CfgTaskUrgentInfo;
import cn.com.yusys.yusp.dscms.domain.TaskUrgentApp;
import cn.com.yusys.yusp.dscms.domain.TaskUrgentDetailInfo;
import cn.com.yusys.yusp.dscms.repository.mapper.CfgTaskUrgentInfoMapper;
import cn.com.yusys.yusp.dscms.service.CfgTaskUrgentInfoService;
import cn.com.yusys.yusp.dscms.service.TaskUrgentAppService;
import cn.com.yusys.yusp.dscms.service.TaskUrgentDetailInfoService;
import cn.com.yusys.yusp.dto.TaskUrgentAppDto;
import com.fasterxml.jackson.datatype.jsr310.DecimalUtils;
import org.bouncycastle.pqc.math.linearalgebra.IntUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TaskUrgentAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 刘权
 * @创建时间: 2021-04-22 14:52:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/taskurgentapp")
public class TaskUrgentAppResource {
    @Autowired
    private TaskUrgentAppService taskUrgentAppService;

    @Autowired
    private TaskUrgentDetailInfoService taskUrgentDetailInfoService;
    @Autowired
    private CfgTaskUrgentInfoService cfgTaskUrgentInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<TaskUrgentApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<TaskUrgentApp> list = taskUrgentAppService.selectAll(queryModel);
        return new ResultDto<List<TaskUrgentApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<TaskUrgentApp>> index(@RequestBody QueryModel queryModel) {
        List<TaskUrgentApp> list = taskUrgentAppService.selectByModel(queryModel);
        return new ResultDto<List<TaskUrgentApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<TaskUrgentApp> show(@PathVariable("pkId") String pkId) {
        TaskUrgentApp taskUrgentApp = taskUrgentAppService.selectByPrimaryKey(pkId);
        return new ResultDto<TaskUrgentApp>(taskUrgentApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    @Transactional(rollbackFor = Exception.class)
    protected ResultDto<Integer> createTaskUrgentApp(@RequestBody TaskUrgentAppDto taskUrgentAppDto) throws URISyntaxException {
        TaskUrgentApp taskUrgentApp = new TaskUrgentApp();
        BeanUtils.copyProperties(taskUrgentAppDto, taskUrgentApp);
        java.util.Random random = new  java.util.Random();
        int a = random.nextInt(9000)+1000;
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddhhmmssSSS");
        String dateString = df.format(new Date());
        taskUrgentApp.setPkId("PK"+ dateString + a);
        int res = 0;
        // 校验 业务是否重复加急
        QueryModel infoModel = new QueryModel();
        infoModel.addCondition("serno",taskUrgentApp.getSerno());
        infoModel.addCondition("bizType", taskUrgentApp.getBizType());
        List<TaskUrgentApp> infoList = taskUrgentAppService.selectByModel(infoModel);
        if(infoList.size() > 0){
            return new ResultDto<>(res);
        }
        if(StringUtils.nonEmpty(taskUrgentApp.getUrgentType())){
            String urgentType = taskUrgentApp.getUrgentType();
            taskUrgentApp.setInputId(taskUrgentAppDto.getManagerId());
            taskUrgentApp.setInputBrId(taskUrgentAppDto.getManagerBrId());
            taskUrgentApp.setManagerId(taskUrgentAppDto.getManagerId());
            taskUrgentApp.setManagerBrId(taskUrgentAppDto.getManagerBrId());
            taskUrgentApp.setInputDate(DateUtils.getCurrDateStr());
            if("1".equals(urgentType)){ // 管理岗加急
                taskUrgentApp.setInputId(taskUrgentAppDto.getInputId());
                taskUrgentApp.setInputBrId(taskUrgentAppDto.getInputBrId());
                res = taskUrgentAppService.insert(taskUrgentApp);
            }else if("3".equals(urgentType)){ //系统加急
                res = taskUrgentAppService.insert(taskUrgentApp);
            }else if("2".equals(urgentType)){ // 客户经理加急
                //客户经理加急增加控制，检查是否在配置表中1：管理岗加急   2：客户经理加急 3：系统加急 9：不加急
                    CfgTaskUrgentInfo cfgTaskUrgentInfo = cfgTaskUrgentInfoService.selectBizType();
                    int result = cfgTaskUrgentInfo.getBizType().indexOf(taskUrgentApp.getBizType());
                    if(result == -1){
                        throw BizException.error(null, "999999","\"发起加急的业务类型，不属于当前加急笔数配置表中的类型\"保存失败！");
                    }
                QueryModel detailModel = new QueryModel();
                detailModel.addCondition("userCode", SessionUtils.getLoginCode());
                detailModel.addCondition("urgentStatus", "1");
                List<TaskUrgentDetailInfo> detailList = taskUrgentDetailInfoService.selectByModel(detailModel);
                if(CollectionUtils.nonEmpty(detailList)){
                    TaskUrgentDetailInfo taskUrgentDetailInfo = detailList.get(0);
                    taskUrgentApp.setPwbrSerno(taskUrgentAppDto.getPwbrSerno());
                    res = taskUrgentAppService.insert(taskUrgentApp);
                    if (res == 1) {
                        //增加数据，需要修改配置数据
                        /******************当前剩余笔数 -1******************/
                        BigDecimal curtSurplusQnt = new BigDecimal(0);
                        if((taskUrgentDetailInfo.getCurtSurplusQnt()!=null)){
                            curtSurplusQnt =taskUrgentDetailInfo.getCurtSurplusQnt();
                        }
                        curtSurplusQnt =curtSurplusQnt.subtract(new BigDecimal(1));
                        taskUrgentDetailInfo.setCurtSurplusQnt(curtSurplusQnt);
                        /******************当前剩余笔数 -1******************/

                        /******************本年使用次数+1******************/
                        BigDecimal curtYeayUtilTimes = new BigDecimal(0);
                        if((taskUrgentDetailInfo.getCurtYeayUtilTimes()!=null)){
                            curtYeayUtilTimes =taskUrgentDetailInfo.getCurtYeayUtilTimes();
                        }
                        curtYeayUtilTimes =curtYeayUtilTimes.add(new BigDecimal(1));
                        taskUrgentDetailInfo.setCurtYeayUtilTimes(curtYeayUtilTimes);
                        /******************本年使用次数+1******************/

                        /******************本期使用次数+1******************/
                        BigDecimal curtPeriodUtilTimes = new BigDecimal(0);
                        if((taskUrgentDetailInfo.getCurtPeriodUtilTimes()!=null)){
                            curtPeriodUtilTimes =taskUrgentDetailInfo.getCurtPeriodUtilTimes();
                        }
                        curtPeriodUtilTimes =curtPeriodUtilTimes.add(new BigDecimal(1));
                        taskUrgentDetailInfo.setCurtPeriodUtilTimes(curtPeriodUtilTimes);
                        /******************本期使用次数+1******************/


                        taskUrgentDetailInfoService.update(taskUrgentDetailInfo);
                    }
                }else{
                    throw BizException.error(null, "999999", "未查询到客户经理" + SessionUtils.getLoginCode() + "的加急笔数配置，加急失败！");
                }
            }
        }else{
            throw BizException.error(null, "999999", "加急类型不能为空！请检查参数");
        }
        return new ResultDto<>(res);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody TaskUrgentApp taskUrgentApp) throws URISyntaxException {
        int result = taskUrgentAppService.update(taskUrgentApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = taskUrgentAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = taskUrgentAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:getOrgIdName
     * @函数描述:根据机构号获取机构名
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/getOrgIdName/{orgId}")
    protected ResultDto<String> getOrgIdName(@PathVariable String orgId) {
        String result = taskUrgentAppService.getOrgIdName(orgId);
        return new ResultDto<String>(result);
    }

}
