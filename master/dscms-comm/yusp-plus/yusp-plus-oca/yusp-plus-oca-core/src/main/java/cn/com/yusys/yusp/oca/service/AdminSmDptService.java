package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmDptEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmDptQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmUserQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmDptVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmUserVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 系统部门表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-11-12 10:47:26
 */
public interface AdminSmDptService extends IService<AdminSmDptEntity> {

    Page<AdminSmDptVo> queryPage(AdminSmDptQuery query);

    void batchDisable(String[] ids);

    void batchEnable(String[] ids);

    void batchDelete(String[] ids);

    Page<AdminSmUserVo> memberPage(AdminSmUserQuery query);

    PageUtils getDptByParam(String orgCode);

    List<AdminSmDptEntity> dptTree(AdminSmDptQuery query);

    /**
     * 工作流查询部门信息
     *
     * @param query
     * @return
     */
    Page<AdminSmDptVo> getDeptsForWf(AdminSmDptQuery query);

}

