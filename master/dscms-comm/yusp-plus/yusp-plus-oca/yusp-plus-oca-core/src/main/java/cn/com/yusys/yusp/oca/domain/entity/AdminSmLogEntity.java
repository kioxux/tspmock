package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;

/**
 * 系统操作日志表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-02 22:18:19
 */
@Data
@TableName("admin_sm_log")
public class AdminSmLogEntity implements Serializable {

	/**
	 * 记录编号
	 */
	private String logId;
	/**
	 * 用户ID
	 */
	private String userId;
	/**
	 * 操作时间
	 */
	private String operTime;
	/**
	 * 操作对象ID
	 */
	private String operObjId;
	/**
	 * 操作前值
	 */
	private String beforeValue;
	/**
	 * 操作后值
	 */
	private String afterValue;
	/**
	 * 操作标志
	 */
	private String operFlag;
	/**
	 * 日志类型
	 */
	private String logTypeId;
	/**
	 * 日志内容
	 */
	private String content;
	/**
	 * 操作者机构
	 */
	private String orgId;
	/**
	 * 登录IP
	 */
	private String loginIp;

}
