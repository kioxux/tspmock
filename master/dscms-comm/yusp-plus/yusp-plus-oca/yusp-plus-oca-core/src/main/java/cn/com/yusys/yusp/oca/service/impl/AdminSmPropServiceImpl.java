package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.commons.exception.PlatformException;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.dto.AdminSmPropDto;
import cn.com.yusys.yusp.dto.AdminSmPropQueryDto;
import cn.com.yusys.yusp.oca.dao.AdminSmPropDao;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmPropEditBo;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmPropSaveBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmPropEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmPropQuery;
import cn.com.yusys.yusp.oca.service.AdminSmPropService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service("adminSmPropService")
public class AdminSmPropServiceImpl extends ServiceImpl<AdminSmPropDao, AdminSmPropEntity> implements AdminSmPropService {

    @Override
    public Page<AdminSmPropEntity> queryPropPage(AdminSmPropQuery adminSmPropQuery) {
        //获取分页参数
        Page<AdminSmPropEntity> page = adminSmPropQuery.getIPage();
        //创建查询wrapper
        LambdaQueryWrapper<AdminSmPropEntity> wrapper = Wrappers.lambdaQuery();
        //添加查询条件
        Optional.ofNullable(adminSmPropQuery.getKeyWord()).ifPresent((param) -> wrapper.like(AdminSmPropEntity::getPropName, param).or().like(AdminSmPropEntity::getPropDesc, param));
        Optional.ofNullable(adminSmPropQuery.getPropName()).ifPresent((param) -> wrapper.like(AdminSmPropEntity::getPropName, param));
        Optional.ofNullable(adminSmPropQuery.getPropDesc()).ifPresent((param) -> wrapper.like(AdminSmPropEntity::getPropDesc, param));
        wrapper.orderByDesc(AdminSmPropEntity::getLastChgDt);
        //分页查询
        return this.baseMapper.selectPage(page, wrapper);
    }

    @Override
    public void saveProp(AdminSmPropSaveBo adminSmProp) {
        //校验新增系统参数名是否已经存在
        List<AdminSmPropEntity> adminSmPropEntities = this.list(new QueryWrapper<AdminSmPropEntity>().eq("PROP_NAME", adminSmProp.getPropName()));
        if (CollectionUtils.nonEmpty(adminSmPropEntities)) {
            throw new PlatformException("系统参数名已存在");
        }
        //创建新增对象
        AdminSmPropEntity adminSmPropEntity = new AdminSmPropEntity();
        //新增对象添加数据
        BeanUtils.beanCopy(adminSmProp, adminSmPropEntity);
        adminSmPropEntity.setInstuId(SessionUtils.getUserInformation().getInstuOrg().getId());
        adminSmPropEntity.setLastChgUsr(SessionUtils.getUserId());
        adminSmPropEntity.setLastChgDt(new Date());
        //新增
        this.save(adminSmPropEntity);
    }

    @Override
    @CacheEvict(value = "SystemProp", key = "#adminSmProp.propName")
    public void updatePropById(AdminSmPropEditBo adminSmProp) {
        //校验新增系统参数名是否已经存在
        List<AdminSmPropEntity> adminSmPropEntities = this.list(new QueryWrapper<AdminSmPropEntity>().eq("PROP_NAME", adminSmProp.getPropName()).ne("PROP_ID", adminSmProp.getPropId()));
        if (CollectionUtils.nonEmpty(adminSmPropEntities)) {
            throw new PlatformException("系统参数名已存在");
        }
        //创建修改对象
        AdminSmPropEntity adminSmPropEntity = new AdminSmPropEntity();
        //修改对象添加数据
        BeanUtils.beanCopy(adminSmProp, adminSmPropEntity);
        adminSmPropEntity.setInstuId(SessionUtils.getUserInformation().getInstuOrg().getId());
        adminSmPropEntity.setLastChgUsr(SessionUtils.getUserId());
        adminSmPropEntity.setLastChgDt(new Date());
        //修改对象
        this.updateById(adminSmPropEntity);
    }

    @Override
    @CacheEvict(value = "SystemProp", key = "#propName")
    public void deleteProp(String propName) {
        LambdaQueryWrapper<AdminSmPropEntity> adminSmPropEntityQueryWrapper = new LambdaQueryWrapper<>();
        adminSmPropEntityQueryWrapper.eq(AdminSmPropEntity::getPropName, propName);
        this.remove(adminSmPropEntityQueryWrapper);
    }

    @Override
    @Cacheable(value = "SystemProp", key = "#adminSmPropQueryDto.propName", condition = "#result == null")
    public AdminSmPropDto queryProp(AdminSmPropQueryDto adminSmPropQueryDto) {
        LambdaQueryWrapper<AdminSmPropEntity> adminSmPropEntityQueryWrapper = new LambdaQueryWrapper<>();
        adminSmPropEntityQueryWrapper.eq(AdminSmPropEntity::getPropName, adminSmPropQueryDto.getPropName());
        AdminSmPropEntity adminSmPropEntity = this.getOne(adminSmPropEntityQueryWrapper);
        if (adminSmPropEntity != null) {
            return BeanUtils.beanCopy(adminSmPropEntity, AdminSmPropDto.class);
        } else {
            throw new PlatformException("系统参数名[" + adminSmPropQueryDto.getPropName() + "]不已存在！");
        }
    }

}