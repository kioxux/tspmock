package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.common.query.PageQuery;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmAuthRecoEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AdminSmAuthRecoQuery extends PageQuery<AdminSmAuthRecoEntity> {

    /**
     * 逻辑系统记录编号
     */
    private String sysId;
    /**
     * 授权对象类型（R-角色，U-用户，D-部门，G-机构，OU-对象组）
     */
    private String objectType;
    /**
     * 授权对象记录编号
     */
    private String objectId;
    /**
     * 授权资源类型（M-菜单，C-控制点，D-数据权限）
     */
    private String[] resourceType;

}

