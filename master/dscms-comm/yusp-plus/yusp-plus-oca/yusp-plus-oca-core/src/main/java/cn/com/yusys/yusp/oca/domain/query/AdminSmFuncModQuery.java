package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.common.query.PageQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmFuncModVo;
import lombok.Data;

/**
 * @author xufy1@yusys.com.cn
 * @desc
 * @date 2021-01-05 20:24
 */
@Data
public class AdminSmFuncModQuery extends PageQuery<AdminSmFuncModVo> {

    private String modelName;
}
