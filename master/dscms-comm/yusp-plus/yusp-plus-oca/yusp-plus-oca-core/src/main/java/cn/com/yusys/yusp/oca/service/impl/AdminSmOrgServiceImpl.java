package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.common.utils.RAMPager;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.redis.template.YuspRedisTemplate;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.Asserts;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.UserAndOrgInfoReqDto;
import cn.com.yusys.yusp.dto.UserAndOrgInfoRespDto;
import cn.com.yusys.yusp.oca.dao.AdminSmOrgDao;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmOrgTreeNodeBo;
import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.domain.constants.Constants;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmOrgEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserMgrOrgEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmOrgExtQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmOrgTreeQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmOrgDetailVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmOrgVo;
import cn.com.yusys.yusp.oca.service.AdminSmOrgService;
import cn.com.yusys.yusp.oca.service.AdminSmUserMgrOrgService;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import cn.com.yusys.yusp.oca.service.cache.AllOrgEntitiesCache;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 系统机构表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-11-27 18:06:35
 */
@Service("adminSmOrgService")
@Slf4j
public class AdminSmOrgServiceImpl extends ServiceImpl<AdminSmOrgDao, AdminSmOrgEntity> implements AdminSmOrgService {

    public static final String C_COMMA = ",";
    @Autowired
    private AdminSmUserMgrOrgService adminSmUserMgrOrgService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private AllOrgEntitiesCache allOrgEntitiesCache;
    @Autowired
    private YuspRedisTemplate yuspRedisTemplate;
    @Autowired
    private RedisCacheLoadServicImpl redisCacheLoadService;

    /**
     * 分页查询
     *
     * @param query
     * @return
     */
    @Override
    public Page<AdminSmOrgVo> queryPage(AdminSmOrgExtQuery query) {
        Page<AdminSmOrgVo> page = query.getIPage();
        // 获取根节点本身及所有后裔列表
        List<AdminSmOrgVo> allProgeny = this.getAllProgeny(query);
        // 根据查询条件过滤allProgeny
        List<AdminSmOrgVo> filteredList = allProgeny.stream().filter((item) ->
                item.getOrgCode().contains(Optional.ofNullable(query.getOrgCode()).orElse(""))// 机构码模糊匹配
                        && item.getOrgName().contains(Optional.ofNullable(query.getOrgName()).orElse(""))// 机构名模糊匹配
                        && item.getOrgSts().equals(Optional.ofNullable(query.getOrgSts()).orElse(item.getOrgSts())) //机构状态精确匹配
                        && (
                        item.getOrgCode().contains(Optional.ofNullable(query.getKeyWord()).orElse(""))//机构名或机构码模糊匹配
                                || item.getOrgName().contains(Optional.ofNullable(query.getKeyWord()).orElse(""))
                )
        ).collect(Collectors.toList());
        //代码分页
        RAMPager<AdminSmOrgVo> pager = new RAMPager<>(filteredList, query.getSize());
        page.setRecords(pager.page(query.getPage()));
        page.setTotal(filteredList.size());
        return page;
    }

    @Override
    public AdminSmOrgDetailVo getDetailById(String orgId) {
        Asserts.notEmpty(orgId, "orgId cannot be null or empty!!");
        return this.baseMapper.getDetailById(orgId);
    }

    /**
     * 获取指定根节点本身及所有后裔列表
     *
     * @param orgId
     * @return
     */
    @Override
    public List<AdminSmOrgVo> getAllProgeny(String orgId) {
        AdminSmOrgExtQuery query = new AdminSmOrgExtQuery();
        query.setUpOrgId(orgId);
        return this.getAllProgeny(query);
    }

    /**
     * 获取指定根机构的所有子孙节点列表
     *
     * @param query - orgCode：默认值是当前用户所在机构
     *              - orgSts：为null或空字串时查询所有状态的机构，有值时只返回对应状态的机构树
     *              - sort：默认值是last_chg_dt desc
     * @return 包含根节点本身
     */
    @Override
    public List<AdminSmOrgVo> getAllProgeny(AdminSmOrgExtQuery query) {
        //参数准备
        if (StringUtils.isEmpty(query.getUpOrgId())) {
            query.setUpOrgId(SessionUtils.getUserOrganizationId());
        }
        // Asserts.notEmpty(query.getUpOrgId(),"org id can not be empty!");//TODO 暂不校验，下方取默认值
        // query.setSort(Optional.ofNullable(query.getSort()).orElse("last_chg_dt desc"));
        // STEP 1 全表查询所有机构
        List<AdminSmOrgVo> allExtNodes = this.baseMapper.selectAllOrgExtVo(query.getSort());// 前端传的sort字段无效
        // STEP 2 依据rootId获取第一代子节点列表
        if (StringUtils.isEmpty(query.getUpOrgId())) {//字段可能为空字符串
            query.setUpOrgId("1");// 机构id为空时取虚拟根机构节点id
        }
        final String rootId = query.getUpOrgId();
        List<AdminSmOrgVo> firstGeneration = allExtNodes.stream().filter((item) -> item.getUpOrgId().equals(rootId)).collect(Collectors.toList());
        // STEP 3 递归将所有子孙节点加入到allProgeny中(包含firstGeneration)
        final List<AdminSmOrgVo> allProgeny = new ArrayList<>();
        firstGeneration.forEach((son) -> addToResult(son, allProgeny, allExtNodes));
        List<AdminSmOrgVo> sortedProgeny = allProgeny.stream().sorted((node1, node2) -> DateUtils.compare(node2.getLastChgDt(), node1.getLastChgDt())).collect(Collectors.toList());//按日期降序
        //STEP 4 把自己也加进结果集(避免参数错误，排除机构号为1的虚拟根节点)
        if (!"1".equals(rootId)) {
            AdminSmOrgVo self = allExtNodes.stream().filter((item) -> rootId.equals(item.getOrgId())).collect(Collectors.toList()).get(0);
            Optional.ofNullable(self).ifPresent((item) -> sortedProgeny.add(0, item));
        }
        return sortedProgeny;
    }

    /**
     * @param parent      当前父节点
     * @param result      待操作结果集合
     * @param allExtNodes 全集（数据源）
     */
    private void addToResult(AdminSmOrgVo parent, List<AdminSmOrgVo> result, List<AdminSmOrgVo> allExtNodes) {
        Optional.ofNullable(parent).ifPresent(result::add);
        List<AdminSmOrgVo> children = allExtNodes.stream().filter((item) -> item.getUpOrgId().equals(parent.getOrgId())).collect(Collectors.toList());
        children.forEach((child) -> addToResult(child, result, allExtNodes));
    }

    /**
     * 获取指定用户有权访问的机构id集合，包含用户所在机构、其所有子孙机构以及用户授权机构
     *
     * @param userId
     * @return
     */
    @Override
    public List<String> getAllAccessibleOrgIds(String userId) {
        AdminSmUserEntity user = adminSmUserService.getById(userId);
        List<AdminSmOrgVo> allProgeny = this.getAllProgeny(user.getOrgId());//用户所在机构以及所有子孙机构
        AdminSmUserEntity userQuery = new AdminSmUserEntity();
        userQuery.setUserId(userId);
        List<AdminSmUserMgrOrgEntity> allUserMgrOrgs = this.adminSmUserMgrOrgService.findOrgRelsByUser(userQuery);//用户所有授权机构列表
        List<String> orgIds = allProgeny.stream().map(AdminSmOrgVo::getOrgId).collect(Collectors.toList());
        orgIds.addAll(allUserMgrOrgs.stream().map(AdminSmUserMgrOrgEntity::getOrgId).collect(Collectors.toList()));//合并两个集合的orgId
        return orgIds;
    }

    @Override
    public List<String> getAllAccessibleOrgIds() {
        return this.getAllAccessibleOrgIds(SessionUtils.getUserId());
    }

    @Override
    public Set<AdminSmOrgTreeNodeBo> getAllAncestryOrgs(String orgId) {
        if (StringUtils.isEmpty(orgId)) {
            orgId = SessionUtils.getUserOrganizationId();
        }
        Asserts.notEmpty(orgId, "orgId can not be empty!");
        AdminSmOrgTreeNodeBo leaf = this.baseMapper.getAllAncestryOrgs(orgId);//这里的children属性其实是当前节点的父级节点列表
        Set<AdminSmOrgTreeNodeBo> res = new HashSet<>();
        addToResult(leaf, res);
        res.forEach((item) -> item.setChildren(null));
        return res;
    }

    private void addToResult(AdminSmOrgTreeNodeBo currentNode, Set<AdminSmOrgTreeNodeBo> res) {
        res.add(currentNode);
        List<AdminSmOrgTreeNodeBo> list = currentNode.getChildren();
        if (Objects.nonNull(list) && list.size() > 0) {
            addToResult(list.get(0), res);
        }
    }

    @Override
    public AdminSmOrgEntity getParentOrg(String orgId) {
        if (StringUtils.isEmpty(orgId)) {
            orgId = SessionUtils.getUserOrganizationId();
        }
        AdminSmOrgEntity self = getById(orgId);
        return getById(self.getUpOrgId());
    }

    @Override
    public Set<AdminSmOrgEntity> getSiblingOrgs(String orgId) {
        if (StringUtils.isEmpty(orgId)) {
            orgId = SessionUtils.getUserOrganizationId();
        }
        AdminSmOrgEntity self = getById(orgId);
        LambdaQueryWrapper<AdminSmOrgEntity> wrapper = new QueryWrapper<AdminSmOrgEntity>().lambda();
        wrapper.eq(AdminSmOrgEntity::getUpOrgId, self.getUpOrgId());
        List<AdminSmOrgEntity> siblingOrgs = this.baseMapper.selectList(wrapper);
        Asserts.nonEmpty(siblingOrgs, "orgId:" + self.getUpOrgId() + " does not exist!");
        return new HashSet<>(siblingOrgs);
    }

    @Override
    public Page<AdminSmOrgVo> getOrgsForWf(AdminSmOrgExtQuery query) {
        Page<AdminSmOrgVo> page = query.getIPage();
        QueryWrapper<AdminSmOrgVo> wrapper = new QueryWrapper<>();
        wrapper.eq("T1.ORG_STS", "A");
        creatWrapper(wrapper, "T1.UP_ORG_ID", query.getUpOrgId());
        creatWrapper(wrapper, "T1.ORG_CODE", query.getOrgCode());
        creatWrapper(wrapper, "T1.ORG_NAME", query.getOrgName());
        return this.baseMapper.getOrgsForWf(page, wrapper);
    }

    public void creatWrapper(QueryWrapper<AdminSmOrgVo> wrapper, String column, String value) {
        if (StringUtils.nonEmpty(value)) {
            boolean condition = value.startsWith("%") || value.endsWith("%");
            wrapper.like(condition, column, value);
            wrapper.eq(!condition, column, value);
        }
    }

    public List<String> getLowerOrgId(String orgCode) {
        QueryWrapper<AdminSmOrgVo> wrapper = new QueryWrapper<>();
        List<AdminSmOrgVo> orgList = this.baseMapper.getChildOrgCode(wrapper);
        List<String> firstChilds = Arrays.asList(orgCode);
        List<String> allChilds = new ArrayList<>();
        Map<String, String> familyNameList = orgList.stream().sorted(Comparator.comparing(AdminSmOrgVo::getOrgLevel)).collect(Collectors.toMap(AdminSmOrgVo::getOrgId, AdminSmOrgVo::getUpOrgId, (existing, replacement) -> existing));
        if (familyNameList.get(orgCode) == null) {
            return allChilds;
        } else {
            getChild(familyNameList, firstChilds, allChilds);
        }
        return allChilds;
    }

    public void getChild(Map<String, String> familyNameList, List<String> checkPeoples, List<String> allChilds) {
        allChilds.addAll(checkPeoples);
        List<String> nextCheckPeoples = new ArrayList<>();
        Map<String, String> familyNameListCopy = new HashMap();
        //map不让动态修改。。。那就copy一个吧, 嗯 好办法
        familyNameListCopy.putAll(familyNameList);
        familyNameListCopy.forEach((people, parent) -> {
            if (checkPeoples.contains(parent)) {
                nextCheckPeoples.add(people);
                //要保证一个org只能有一个upOrg，有多个要注掉remove这句
                familyNameList.remove(people);
            }
        });
        if (nextCheckPeoples.size() > 0) {
            getChild(familyNameList, nextCheckPeoples, allChilds);
        }
    }

    /**
     * 迭代取出当前机构的下级机构
     * add by 顾银华 at 2021-09-21
     *
     * @param orgCode
     * @param allOrgList
     * @param resultList
     */
    public void getChildList(String orgCode, List<AdminSmOrgDto> allOrgList, List<AdminSmOrgDto> resultList) {
        allOrgList.stream().forEach(adminSmOrgDto -> {
            if (adminSmOrgDto.getUpOrgId().equalsIgnoreCase(orgCode)) {
                resultList.add(adminSmOrgDto);
                getChildList(adminSmOrgDto.getOrgCode(), allOrgList, resultList);
            }
        });
    }

    public AdminSmOrgEntity getById(String orgId) {
        Asserts.notEmpty(orgId, "orgId can not be empty!");
        AdminSmOrgEntity res = super.getById(orgId);
        Asserts.nonNull(res, "orgId:" + orgId + " does not exist!");
        return res;
    }


    /**
     * 不是严格的机构树，是一个多根树，其中一个根是用户所在机构，其他根是胡无交集的用户授权机构构成的若干子树，这些子树的逻辑层级不严格对应在树中的层级
     *
     * @param query
     * @return orgId 为空时查当前登录用户所在机构，orgSts为空时查所有状态
     */
    @Override
    public List<AdminSmOrgTreeNodeBo> getOrgTree(AdminSmOrgTreeQuery query) {
        //参数准备
        if (StringUtils.isEmpty(query.getOrgId())) {
            query.setOrgId(SessionUtils.getUserOrganizationId());
        }
        //STEP 1 用户所在机构组成的机构树
        List<AdminSmOrgTreeNodeBo> rootList = getOrgTreeByOrgId(query.getOrgId(), query.getOrgSts());
        List<AdminSmOrgTreeNodeBo> resList = new ArrayList<>(rootList);
        //STEP 2  用户授权机构构成的机构树
        if (StringUtils.nonEmpty(SessionUtils.getUserId())) {
            AdminSmUserEntity userQuery = new AdminSmUserEntity();
            userQuery.setUserId(SessionUtils.getUserId());
            List<AdminSmUserMgrOrgEntity> userMgrOrgEntityList = adminSmUserMgrOrgService.findOrgRelsByUser(userQuery);//用户授权机构rel列表

            if (userMgrOrgEntityList.size() > 0) {
                List<String> filteredOrgIds = getAllOrgEntities(query.getOrgSts()).stream().map(AdminSmOrgEntity::getOrgId).collect(Collectors.toList());//按状态过滤后的所有机构id
                List<String> userMgrOrgIds = userMgrOrgEntityList.stream().map(AdminSmUserMgrOrgEntity::getOrgId).filter(filteredOrgIds::contains).collect(Collectors.toList());//提取用户授权机构id列表
                userMgrOrgIds.remove(SessionUtils.getUserOrganizationId());//从用户授权机构列表中排除当前登录用户所在机构（理论上没有这样的数据，万一呢？）
                List<AdminSmOrgTreeNodeBo> orgNodeList = this.listToTree(userMgrOrgIds);
                resList.addAll(0, orgNodeList);
            }
        }
        return resList;
    }

    /**
     * 以指定机构ID为根节点获取机构树
     *
     * @param orgId 必输
     * @return
     */
    public List<AdminSmOrgTreeNodeBo> getOrgTreeByOrgId(String orgId, AvailableStateEnum state) {
        Asserts.notEmpty(orgId, "orgId can not be empty!");
        // STEP 1 全表查询所有机构
        List<AdminSmOrgEntity> originList = getAllOrgEntities(state);
        List<AdminSmOrgTreeNodeBo> allNodes = (List<AdminSmOrgTreeNodeBo>) BeanUtils.beansCopy(originList, AdminSmOrgTreeNodeBo.class);// entity-》treeNode
        // STEP 2 以 orgId 指代的机构作为根节点
        List<AdminSmOrgTreeNodeBo> rootList = allNodes.stream().filter((node) -> node.getOrgId().equals(orgId)).collect(Collectors.toList());
        Asserts.nonEmpty(rootList, "orgId:" + orgId + " is not exist!!");
        this.appendChildren(rootList.get(0), allNodes, null);
        return rootList;
    }

    public List<AdminSmOrgTreeNodeBo> getOrgTreeByOrgId(String orgId) {
        return getOrgTreeByOrgId(orgId, null);
    }

    private List<AdminSmOrgEntity> getAllOrgEntities(AvailableStateEnum state) {
        List<AdminSmOrgEntity> allOrgEntities = this.allOrgEntitiesCache.getAllOrgEntities();
        return Objects.nonNull(state) ? allOrgEntities.stream().filter(org -> org.getOrgSts().equals(state)).collect(Collectors.toList()) : allOrgEntities;
    }

    private List<AdminSmOrgTreeNodeBo> listToTree(List<String> orgIds) {
        List<AdminSmOrgTreeNodeBo> res = new ArrayList<>();
        List<AdminSmOrgEntity> sortedUserMgrOrgEntitys = this.lambdaQuery()
                .select()
                .in(AdminSmOrgEntity::getOrgId, orgIds)
                .orderByAsc(AdminSmOrgEntity::getOrgLevel)
                .list();// 按级别排序
        do {
            AdminSmOrgTreeNodeBo left = BeanUtils.beanCopy(sortedUserMgrOrgEntitys.get(0), AdminSmOrgTreeNodeBo.class);
            sortedUserMgrOrgEntitys = initChildren(left, sortedUserMgrOrgEntitys);
            res.add(left);
        } while (sortedUserMgrOrgEntitys.size() != 0);
        return res;
    }

    /**
     * 从origin中筛选出left的children，并从origin中删除这些child
     *
     * @param left
     * @param origin
     */
    private List<AdminSmOrgEntity> initChildren(AdminSmOrgTreeNodeBo left, List<AdminSmOrgEntity> origin) {
        List<AdminSmOrgEntity> entityChildren = origin.stream()
                .filter(entity -> entity.getUpOrgId().equals(left.getOrgId()))
                .collect(Collectors.toList());
        List<AdminSmOrgTreeNodeBo> nodeChildren = entityChildren.stream().map(entity -> BeanUtils.beanCopy(entity, AdminSmOrgTreeNodeBo.class)).collect(Collectors.toList());
        left.setChildren(nodeChildren);
        origin.removeIf(entity -> entity.getOrgId().equals(left.getOrgId()));//删除自身
        origin.removeAll(entityChildren);//删除子节点
        nodeChildren.forEach(child -> initChildren(child, origin));
        return origin;
    }

    /**
     * 批量查询机构树
     *
     * @param orgIds       要查的多根机构树的根节点，要求必须是同级机构
     * @param orgSts       需要的机构状态（其中某个上级机构不符合会级联排除其所有子节点）
     * @param exceptOrgIds 返回多根树中需要排除的节点（会级联）
     * @return
     */
    @Override
    public List<AdminSmOrgTreeNodeBo> getOrgTrees(List<String> orgIds, AvailableStateEnum orgSts, String... exceptOrgIds) {
        if (orgIds.size() > 0) {
            List<AdminSmOrgTreeNodeBo> res = new ArrayList<>();
            // STEP 1 全表查询所有机构
            LambdaQueryWrapper<AdminSmOrgEntity> wrapper = Wrappers.lambdaQuery();
            Optional.ofNullable(orgSts).ifPresent((sts) -> wrapper.eq(AdminSmOrgEntity::getOrgSts, sts));
            wrapper.orderByDesc(AdminSmOrgEntity::getLastChgDt);//todo 机构树直接按更新时间排序,有需求再加
            List<AdminSmOrgEntity> originList = this.baseMapper.selectList(wrapper);
            List<AdminSmOrgTreeNodeBo> allNodes = (List<AdminSmOrgTreeNodeBo>) BeanUtils.beansCopy(originList, AdminSmOrgTreeNodeBo.class);// entity-》treeNode
            // STEP 2 遍历orgId
            orgIds.forEach(orgId -> {
                List<AdminSmOrgTreeNodeBo> rootList = allNodes.stream().filter((node) -> node.getOrgId().equals(orgId)).collect(Collectors.toList());
                if (rootList.size() > 0) {
                    //Asserts.nonEmpty(rootList,"orgId "+orgId+" is not exist!!");
                    this.appendChildren(rootList.get(0), allNodes, exceptOrgIds);
                    res.add(rootList.get(0));
                }
            });
            return res;
        }
        return new ArrayList<>();
    }

    private void appendChildren(AdminSmOrgTreeNodeBo parent, List<AdminSmOrgTreeNodeBo> all, String[] exceptOrgIds) {
        List<AdminSmOrgTreeNodeBo> children = all.stream().filter((node) -> node.getUpOrgId().equals(parent.getOrgId())).collect(Collectors.toList());
        if (exceptOrgIds != null && exceptOrgIds.length > 0) {
            children.removeIf((node) -> Arrays.asList(exceptOrgIds).contains(node.getOrgId()));//排除指定节点，不加入结果集
        }
        parent.setChildren(children);
        children.forEach((child) -> appendChildren(child, all, exceptOrgIds));
    }


    @Override
    public boolean save(AdminSmOrgEntity entity) {
        LambdaQueryWrapper<AdminSmOrgEntity> codeWrapper = new QueryWrapper<AdminSmOrgEntity>().lambda();
        codeWrapper.eq(AdminSmOrgEntity::getOrgCode, entity.getOrgCode());
        AdminSmOrgEntity check = this.baseMapper.selectOne(codeWrapper);
        if (Objects.nonNull(check)) {
            throw BizException.error("exist", "50700001", entity.getOrgCode());
        }
        AdminSmOrgEntity parent = getById(entity.getUpOrgId());
        entity.setOrgId(entity.getOrgCode());// 机构表主键强制等于机构码
        entity.setInstuId(parent.getInstuId());
        entity.setOrgLevel(parent.getOrgLevel() + 1);
        entity.setOrgSts(Optional.ofNullable(entity.getOrgSts()).orElse(AvailableStateEnum.UNENABLED));//新增的数据默认是待启用的
        entity.setOrgSeq(parent.getOrgSeq().concat(entity.getOrgCode()).concat(C_COMMA));// 机构层级索引信息
        entity.setLastChgUsr(SessionUtils.getUserId());
        boolean result = this.baseMapper.insert(entity) > 0;
        if (result) {
            entity = this.lambdaQuery().eq(AdminSmOrgEntity::getOrgCode, entity.getOrgCode()).one();
            this.allOrgEntitiesCache.addOrUpdateCache(entity);//更新单条
            log.info("New organization data: [new organization: {}] ", entity.getOrgName());
            // 新增用户缓存信息
            yuspRedisTemplate.hset(Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_ORG_NAME, parent.getOrgCode(), parent.getOrgName());
        }
        // 更新用户机构树信息
        redisCacheLoadService.loadOrgTreeData();
        return result;
    }

    /**
     * 批量启用
     *
     * @param ids
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchEnable(String[] ids) {
        if (CollectionUtils.nonEmpty(ids)) {
            List<String> idList = Arrays.asList(ids);
            String userId = SessionUtils.getUserId();
            idList.forEach((id) -> {
                AdminSmOrgEntity entity = new AdminSmOrgEntity();
                entity.setOrgId(id);
                entity.setOrgSts(AvailableStateEnum.ENABLED);
                entity.setLastChgUsr(userId);
                this.allOrgEntitiesCache.addOrUpdateCache(entity);//更新单条
                this.updateById(entity);
            });
        }
    }

    /**
     * 批量停用
     *
     * @param ids
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchDisable(String[] ids) {
        if (CollectionUtils.nonEmpty(ids)) {
            List<String> idList = Arrays.asList(ids);
            String userId = SessionUtils.getUserId();
            idList.forEach((id) -> {
                if (checkBlocked(id)) {
                    throw BizException.error(null, "50700002", "该机构有生效状态的子机构或绑定有其他信息，请删除关联信息后操作");
                } else {
                    AdminSmOrgEntity entity = new AdminSmOrgEntity();
                    entity.setOrgId(id);
                    entity.setOrgSts(AvailableStateEnum.DISABLED);
                    entity.setLastChgUsr(userId);
                    this.allOrgEntitiesCache.addOrUpdateCache(entity);//更新单条
                    this.baseMapper.updateById(entity);
                }
            });
        }
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchDelete(String[] ids) {
        if (CollectionUtils.nonEmpty(ids)) {
            List<String> idList = Arrays.asList(ids);
            this.allOrgEntitiesCache.clearCache(idList.toArray());//清除单条记录
            idList.forEach((id) -> {
                if (checkBlocked(id)) {
                    throw BizException.error(null, "50700002", "该机构有生效状态的子机构或绑定有其他信息，请删除关联信息后操作");
                } else {
                    AdminSmOrgDetailVo adminSmOrgDetail = this.baseMapper.getDetailById(id);
                    this.baseMapper.deleteById(id);
                    // 新删除用户缓存信息
                    yuspRedisTemplate.hset(Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_ORG_NAME, adminSmOrgDetail.getOrgCode(), adminSmOrgDetail.getOrgName());
                }
            });
            // 更新用户机构树信息
            redisCacheLoadService.loadOrgTreeData();
        }
    }

    /**
     * 检查机构是否已关联其他信息
     *
     * @param orgId
     * @return
     */
    private boolean checkBlocked(String orgId) {
        QueryWrapper<AdminSmOrgEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("UP_ORG_ID", orgId);
        Integer countSon = this.baseMapper.selectCount(wrapper);//是否有下级机构
        Integer countRel = this.baseMapper.queryRelByOrgId(orgId);//是否有关联用户、角色、岗位、部门
        return countSon > 0 || countRel > 0;
    }

    @Override
    @Transactional
    public boolean updateBy(AdminSmOrgEntity entity) {
        if (AvailableStateEnum.DISABLED.equals(entity.getOrgSts()) && checkBlocked(entity.getOrgId())) {//改为停用时要判断是否关联其他信息
            throw BizException.error(null, "50700002", "该机构有生效状态的子机构或绑定有其他信息，请删除关联信息后操作");
        }
        entity.setOrgSts(Optional.ofNullable(entity.getOrgSts()).orElse(AvailableStateEnum.UNENABLED));
        this.allOrgEntitiesCache.addOrUpdateCache(entity);//更新单条
        // 新增用户缓存信息
        yuspRedisTemplate.hset(Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_ORG_NAME, entity.getOrgCode(), entity.getOrgName());
        AdminSmOrgEntity oldOrgEntity = super.getById(entity.getOrgId());
        // 处理当前机构及其下属所有机构的机构索引信息
        String oldOrgSeq = oldOrgEntity.getOrgSeq();// 原机构索引信息
        // 更新机构信息
        entity.setLastChgUsr(SessionUtils.getUserId());
        super.updateById(entity);
        AdminSmOrgEntity parent = getById(entity.getUpOrgId());
        String newOrgSeq = parent.getOrgSeq().concat(entity.getOrgCode()).concat(C_COMMA);
        // 根据原机构的机构索引信息，查询所有下级子机构，然后replace替换
        LambdaQueryWrapper<AdminSmOrgEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.likeRight(AdminSmOrgEntity::getOrgSeq, oldOrgSeq);
        List<AdminSmOrgEntity> childList = super.list(queryWrapper);
        childList.forEach(adminSmOrgEntity -> {
            adminSmOrgEntity.setOrgSeq(adminSmOrgEntity.getOrgSeq().replace(oldOrgSeq, newOrgSeq));
            super.updateById(adminSmOrgEntity);
        });
        // 更新用户机构树信息
        redisCacheLoadService.loadOrgTreeData();
        return true;
    }

    /**
     * 获取机构从属路径，方便其他service按老版oca有机构视图的前提下组织逻辑
     *
     * @param orgId
     * @return
     */
    @Override
    public String getOrgSeq(String orgId) {
        StringBuffer res = new StringBuffer();
        reduceOrgSeq(res, orgId);
        res.insert(0, C_COMMA);
        return res.toString();
    }

    private void reduceOrgSeq(StringBuffer res, String currentOrgId) {
        if (StringUtils.isEmpty(currentOrgId)) {
            return;
        }
        if (currentOrgId.equals("1")) {
            return;
        }
        res.insert(0, currentOrgId + C_COMMA);
        AdminSmOrgEntity org = this.baseMapper.selectById(currentOrgId);
        reduceOrgSeq(res, org.getUpOrgId());
    }

    /**
     * 通过机构代码获取机构详情
     *
     * @param orgCode 机构代码
     * @return
     */
    @Override
    public AdminSmOrgDto getByOrgCode(String orgCode) {
        //根据机构号查询机构信息
        AdminSmOrgEntity adminSmOrgEntity = this.getOne(new QueryWrapper<AdminSmOrgEntity>().eq("ORG_CODE", orgCode));
        AdminSmOrgDto adminSmOrgDto = new AdminSmOrgDto();
        //封装返回数据
        if (Objects.nonNull(adminSmOrgEntity)) {
            adminSmOrgDto = BeanUtils.beanCopy(adminSmOrgEntity, AdminSmOrgDto.class);
        } else {
            log.error("机构代码不存在！");
        }
        return adminSmOrgDto;
    }

    /**
     * 通过多个机构代码获取多个机构
     *
     * @param orgCodes 机构代码列表
     * @return
     */
    @Override
    public List<AdminSmOrgDto> getByOrgCodeList(List<String> orgCodes) {
        //根据手机号查询用户信息
        List<AdminSmOrgEntity> adminSmOrgEntities = this.list(new QueryWrapper<AdminSmOrgEntity>().in("ORG_CODE", orgCodes));
        List<AdminSmOrgDto> adminSmOrgDtoList = new ArrayList<>();
        //封装返回数据
        if (CollectionUtils.nonEmpty(adminSmOrgEntities)) {
            adminSmOrgDtoList = (List<AdminSmOrgDto>) BeanUtils.beansCopy(adminSmOrgEntities, AdminSmOrgDto.class);
        } else {
            log.error("机构代码不存在！");
        }
        return adminSmOrgDtoList;
    }

    @Override
    public List<String> getOrgCodesByOrgSeq(String orgSeq) {
        Asserts.nonBlank(orgSeq, "orgSeq cannot be null");
        LambdaQueryWrapper<AdminSmOrgEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.likeRight(AdminSmOrgEntity::getOrgSeq, orgSeq);
        List<AdminSmOrgEntity> childList = super.list(queryWrapper);
        if (CollectionUtils.nonEmpty(childList)) {
            return childList.stream().map(AdminSmOrgEntity::getOrgCode).distinct().collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    /*
     * 根据机构编号查询当前机构及下级机构信息
     * add by 顾银华 at 2021-09-21 17:07:45
     */
    @Override
    public List<AdminSmOrgDto> getChildrenOrgList(String orgCode) {
        Asserts.nonBlank(orgCode, "orgCode[机构编号] cannot be null");
        QueryWrapper<AdminSmOrgDto> wrapper = new QueryWrapper<>();
        List<AdminSmOrgDto> orgList = this.baseMapper.getChildListByOrgCode(wrapper);
        List<AdminSmOrgDto> allChilds = new ArrayList<>();

        if (orgList.isEmpty()) {
            return allChilds;
        }
        orgList.stream().forEach(adminSmOrg -> {
            if (orgCode.equalsIgnoreCase(adminSmOrg.getOrgCode())) {
                allChilds.add(adminSmOrg);
                // 2.生成二级及二级SQL语句
                getChildList(adminSmOrg.getOrgCode(), orgList, allChilds);
            }
        });

        return allChilds;
    }

    @Override
    public List<String> getRemoteOrgList(String queryType) {
        Asserts.nonBlank(queryType, "QUERY_TYPE[查询类型] cannot be null");
        QueryWrapper<AdminSmOrgEntity> wrapper = new QueryWrapper<>();
        List<String> allRemoteOrgList = new ArrayList<>();
        if ("0".equalsIgnoreCase(queryType)) {
            wrapper.in(queryType != null, "ORG_TYPE", "1", "2", "3");
        } else if ("1".equalsIgnoreCase(queryType)) {
            wrapper.notIn(queryType != null, "ORG_TYPE", "1", "2", "3", "A");
        } else {
            log.error("传入的查询类型错误！");
            return allRemoteOrgList;
        }
        List<AdminSmOrgEntity> orgList = super.list(wrapper);
        orgList.stream().forEach(adminSmOrg -> {
            allRemoteOrgList.add(adminSmOrg.getOrgCode());
        });
        return allRemoteOrgList;
    }

    /**
     * 根据机构编号列表分页查询用户和机构信息
     *
     * @param userAndOrgInfoReqDto
     * @return
     */
    @Override
    public List<UserAndOrgInfoRespDto> getUserAndOrgInfo(UserAndOrgInfoReqDto userAndOrgInfoReqDto) {
        //机构编号列表
        List<String> orgIds = userAndOrgInfoReqDto.getOrgIds();
        IPage<UserAndOrgInfoRespDto> iPage = new Page<>(userAndOrgInfoReqDto.getPageNum(), userAndOrgInfoReqDto.getPageSize());
        iPage = this.baseMapper.getUserAndOrgInfo(iPage, orgIds);
        List<UserAndOrgInfoRespDto> list = iPage.getRecords();
        return list;
    }

    /**
     * 根据机构编号及下级机构号分页查询用户和机构信息
     *
     * @return
     */
    @Override
    public List<UserAndOrgInfoRespDto> getUserByLowerOrgInfo(QueryModel model) {
        //机构编号列表
        PageHelper.startPage(model.getPage(), model.getSize());
        List<UserAndOrgInfoRespDto> list = this.baseMapper.getUserByLowerOrgInfo(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/11 16:23
     * @注释 条件查询机构 筛选已经选取的
     */
    @Override
    public ResultDto<AdminSmOrgVo> querypagebyall(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List list = this.baseMapper.querypagebyall(model);
        PageHelper.clearPage();
        return new ResultDto(list);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/11 16:23
     * @注释 条件查询机构 筛选已经选取的 (贷后专用-适配角色数据授权)
     */
    @Override
    public ResultDto<AdminSmOrgVo> querypagebyallForDh(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List list = this.baseMapper.querypagebyall(model);
        PageHelper.clearPage();
        return new ResultDto(list);
    }
}