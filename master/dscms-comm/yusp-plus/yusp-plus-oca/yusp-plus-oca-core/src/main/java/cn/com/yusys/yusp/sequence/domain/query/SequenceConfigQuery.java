package cn.com.yusys.yusp.sequence.domain.query;

import cn.com.yusys.yusp.common.query.PageQuery;
import cn.com.yusys.yusp.sequence.domain.entity.SequenceConfig;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SequenceConfigQuery extends PageQuery<SequenceConfig> {
    /**
     * 序列名称
     */
    private String seqName;
    /**
     * 序列ID
     */
    private String seqId;
}
