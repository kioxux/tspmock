package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.common.query.PageQuery;
import cn.com.yusys.yusp.oca.domain.vo.UserRoleRelVo;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 用用户去查角色列表
 */
@Data
public class AdminSmUserRoleRelQuery extends PageQuery<UserRoleRelVo> {

    @NotEmpty
    private String userId;

    private String keyWord;

    private Boolean checked;


}
