package cn.com.yusys.yusp.constants;

/**
 * OCA交易日志常量类</br>
 * 为了避免引入cmis-common or yusp-plus-common，同时避免和同名类冲突，在TradeLogConstants前增加Oca标识。
 *
 * @author leehuang
 * @date 2021/04/26 09:50
 */
public class OcaTradeLogConstants {

    public static final String RESOURCE_BEGIN_PREFIX_LOGGER = "处理[{}|{}]的Resource逻辑开始,请求参数为:[{}]";
    public static final String RESOURCE_EXCEPTION_PREFIX_LOGGER = "处理[{}|{}]的Resource逻辑结束,异常信息为:[{}]";
    public static final String RESOURCE_END_PREFIX_LOGGER = "处理[{}|{}]的Resource逻辑结束,响应参数为:[{}]";

    public static final String SERVICE_BEGIN_PREFIX_LOGGER = "处理[{}|{}]的Service逻辑开始";
    public static final String SERVICE_EXCEPTION_PREFIX_LOGGER = "处理[{}|{}]的Service逻辑结束,异常信息为:[{}]";
    public static final String SERVICE_END_PREFIX_LOGGER = "处理[{}|{}]的Service逻辑结束";

    public static final String CALL_SERVICE_BEGIN_PREFIX_LOGGER = "调用[{}|{}]的Service逻辑开始,请求参数为:[{}]";
    public static final String CALL_SERVICE_END_PREFIX_LOGGER = "调用[{}|{}]的Service逻辑结束,响应参数为:[{}]";

}
