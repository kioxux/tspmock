package cn.com.yusys.yusp.oca.service;


import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.commons.session.user.impl.MenuImpl;
import cn.com.yusys.yusp.oca.domain.bo.DragMenuBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmMenuEntity;
import cn.com.yusys.yusp.oca.domain.form.AdminSmMenuConditionForm;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmAuthTreeVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmMenuVo;
import cn.com.yusys.yusp.oca.domain.vo.MenuVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 系统菜单表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-19 16:32:07
 */
public interface AdminSmMenuService extends IService<AdminSmMenuEntity> {


    PageUtils queryPage(Map<String, Object> params);

    List<MenuImpl> getAdminSmMenu(String userId);

    List<MenuVo> getMenuTree(String sysId, boolean lazy, String menuId);

    MenuVo saveAdminSmMunu(AdminSmMenuEntity adminSmMenu);

    void updateMenuById(AdminSmMenuEntity adminSmMenu);

    MenuVo getMenuInfo(String menuId);

    /**
     * 递归查询 菜单树
     *
     * @param sysId    系统id, 默认test
     * @param pid      菜单父id字段
     * @param treeDeep 树递归深度
     * @return
     */
    List<MenuVo> queryTreeMenuList(String sysId, String pid, String treeDeep);

    /**
     * 搜索树
     *
     * @param sysId
     * @param menuName
     * @return
     */
    List<MenuVo> querySearchTree(String sysId, String menuName);

    /**
     * 修改菜单树,菜单树可拖动
     *
     * @param menuVoList
     */
    int updateAllMenuTree(List<MenuVo> menuVoList);

    /**
     * 查询权限管理树
     *
     * @param id
     * @return
     */
    List<AdminSmAuthTreeVo> selectAuthTree(String id);

    /**
     * 修改菜单树，执行位置的菜单id
     *
     * @param dragMenuBo
     * @return
     */
    int updateMenuTreeByDragMenu(DragMenuBo dragMenuBo);

    /**
     * 查询控制点关联菜单列表
     *
     * @param adminSmMenuConditionForm
     * @return
     */
    Page<AdminSmMenuVo> getMenuListForContr(AdminSmMenuConditionForm adminSmMenuConditionForm);

    /**
     * 批量删除菜单
     *
     * @param ids
     */
    int removeMenuByIds(String[] ids);
}

