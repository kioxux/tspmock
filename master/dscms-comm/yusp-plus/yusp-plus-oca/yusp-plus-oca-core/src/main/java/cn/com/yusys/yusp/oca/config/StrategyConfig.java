package cn.com.yusys.yusp.oca.config;

import cn.com.yusys.yusp.oca.interceptor.LoginInterceptor;
import cn.com.yusys.yusp.oca.interceptor.RequestFilter;
import cn.com.yusys.yusp.oca.service.AdminSmCrelStraService;
import cn.com.yusys.yusp.oca.utils.I18nMessageByCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @program: yusp-plus
 * @description: 策略配置类
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2021-04-15 11:07
 */
@Configuration
public class StrategyConfig implements WebMvcConfigurer {
    @Autowired
    private AdminSmCrelStraService adminSmCrelStraService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private I18nMessageByCode i18nMessageByCode;

    @Override
    public void addInterceptors(InterceptorRegistry interceptorRegistry) {
        interceptorRegistry.addInterceptor(new LoginInterceptor(adminSmCrelStraService, stringRedisTemplate, i18nMessageByCode)).addPathPatterns("/api/login/**");
    }

    @Bean
    LoginInterceptor loginInterceptor() {
        return new LoginInterceptor(adminSmCrelStraService, stringRedisTemplate, i18nMessageByCode);
    }

    @Bean
    public FilterRegistrationBean httpServletRequestReplacedRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new RequestFilter());
        registration.addUrlPatterns("/api/login/*");
        registration.setOrder(1);
        return registration;
    }
}
