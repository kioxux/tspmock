package cn.com.yusys.yusp.oca.domain.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.util.Date;

@Data

public class ApplySaveInfoBo {
    /**
     * 客户姓名
     */
    @NotEmpty(message = "客户名不能为空")
    private String cusName;
    /**
     * 客户英文名
     */
    private String cusEname;
    /**
     * 生日
     */
    private Date birthday;
    /**
     * 婚姻状况 1：已婚 2：未婚 3：离异 4：丧偶 5：再婚 9：未知)
     */
    @NotEmpty(message = "婚姻状态不能为空")
    private String maritalStatus;
    /**
     * 证件类型(10110:居民身份证,10119:组织机构代码,20150:护照,99099:其他证件)
     */
    @NotEmpty(message = "证件类型不能为空")
    private String certType;
    /**
     * 证件号码
     */
    @NotEmpty(message = "证件号码不能为空")
    private String certCode;
    /**
     * 证件有效起始日期
     */
    @NotEmpty(message = "证件起始日期不能为空")

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date certStartDate;
    /**
     * 证件有效起始日期
     */
    @NotEmpty(message = "证件起始日期不能为空")

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date certEndDate;
    /**
     * 性别(1:男,2:女,5,其他,9,未知)
     */
    @NotEmpty(message = "性别不能为空")
    private String sex;
    /**
     * 年龄
     */
    @NotEmpty(message = "年龄不能为空")
    private Integer age;
    /**
     * 家庭地址
     */
    @NotEmpty(message = "家庭住址不能为空")
    private String address;
    /**
     * 家庭月收入
     */
    @NotEmpty(message = "家庭月收入不能为空")
    private BigDecimal monthlyIncome;
    /**
     * 电话号码
     */
    @NotEmpty(message = "电话号码不能为空")
    private String phone;
    /**
     * 邮箱
     */
    @NotEmpty(message = "电子邮箱不能为空")
    private String email;
    /**
     * 现入住时间
     */
    private Date occupancyDate;
    /**
     * 居住状态(1:自置,2:按揭,3:亲属楼宇,4:集体宿舍,5:租房,6:共有住宅,8:其他,9:未知)
     */
    @NotEmpty(message = "居住状况不能为空")
    private String liveStatus;
    /**
     * 最高学位(1:博士,2:硕士,3:学士,9:其他)
     */
    private String degreeLevel;
    /**
     * 学历状态 1：文盲 2：初中或初中以下 3：高中 4：大专或本科 5：硕士研究生 6：博士或博士后 9：未知)
     */
    private String eduLevel;
    /**
     * 产品名称
     */
    @NotEmpty(message = "产品名不能为空")
    private String productName;
    /**
     * 申请金额
     */
    @NotEmpty(message = "申请金额不能为空")
    private BigDecimal applyAmt;
    /**
     * 申请期限
     */
    @NotEmpty(message = "申请期限不能为空")
    private Integer applyTerm;
    /**
     * 居住地邮编
     */
    private String livePostcode;

    /**
     * 工作状态(1:全职,2:兼职,3:自雇,9:其他)
     */
    private String workStatus;
    /**
     * 工作年限
     */
    @NotEmpty(message = "工作年限不能为空")
    private BigDecimal workYears;
    /**
     * 公司名称
     */
    @NotEmpty(message = "公司名称不能为空")
    private String companyName;

    /**
     * 公司地址
     */
    @NotEmpty(message = "公司地址不能为空")
    private String companyAddr;
    /**
     * 入职日期
     */
    @NotEmpty(message = "入职日期不能为空")
    private Date inCompanyDate;
    /**
     * 公司类型(01:政府机关,02:社会团体,03:事业单位,04:国有企业,05:国有控股企业,11:外资企业,12:合资企业,13:私营企业,99:其他)
     */
    @NotEmpty(message = "公司类型不能为空")
    private String companyType;
    /**
     * 部门
     */
    private String dutyName;
    /**
     * 职位(01:国家机关,02:党群组织,03:企事业单位负责人,11:专业技术人员,12:办事人员和有关人员,21:商业,22:服务业人员,31:农林牧渔水利业生产人员,41:生产/运输设备操作人员及有关人员,51:军人,99:其他)
     */
    @NotEmpty(message = "职位不能为空")
    private String postName;
    /**
     * 公司电话
     */
    @NotEmpty(message = "公司电话不能为空")
    private String companyPhone;
    /**
     * 户籍地址
     */
    @NotEmpty(message = "户籍地址不能为空")
    private String censusAddr;
    /**
     * 户籍地址邮编
     */
    @NotEmpty(message = "户籍地邮编不能为空")
    private String censusPostcode;
    /**
     * 通讯地址
     */
    @NotEmpty(message = "通讯地址不能为空")
    private String communicationAddr;
    /**
     * 通讯邮编
     */
    @NotEmpty(message = "通讯地邮编不能为空")
    private String communicationPostcode;
    /**
     * 居住地址
     */
    @NotEmpty(message = "居住地址不能为空")
    private String liveAddress;

}
