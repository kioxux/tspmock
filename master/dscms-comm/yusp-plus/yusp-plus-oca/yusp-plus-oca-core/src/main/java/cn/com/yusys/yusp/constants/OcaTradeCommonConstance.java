package cn.com.yusys.yusp.constants;

/**
 * 服务端交易通用常量类
 */
public class OcaTradeCommonConstance {
    /**
     * 通用是否Y:是 N:否
     */
    public static final String COMMON_YES_NO_Y = "Y";
    public static final String COMMON_YES_NO_N = "N";

}
