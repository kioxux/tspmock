package cn.com.yusys.yusp.notice.service.impl;

import cn.com.yusys.yusp.notice.service.ThirdPartOssService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service("thirdPartOssService")
public class ThirdPartOssServiceImpl implements ThirdPartOssService {

    @Override
    public void delete(List<String> filePathList) {
    }

}
