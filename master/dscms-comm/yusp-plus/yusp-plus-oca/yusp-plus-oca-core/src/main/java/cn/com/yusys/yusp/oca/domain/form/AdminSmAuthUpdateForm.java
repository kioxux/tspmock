package cn.com.yusys.yusp.oca.domain.form;

import cn.com.yusys.yusp.oca.domain.vo.AdminSmAuthTreeVo;
import lombok.Data;

import java.util.List;

@Data
public class AdminSmAuthUpdateForm {
    /**
     * 修改对象Id
     */
    private String authObjId;
    /**
     * 修改对象类型
     */
    private String authObjType;
    /**
     * 复制对象Id
     */
    private String copyForAuthObjIds;
    /**
     * 修改过的权限列表
     */
    private List<AdminSmAuthTreeVo> authFormList;
}
