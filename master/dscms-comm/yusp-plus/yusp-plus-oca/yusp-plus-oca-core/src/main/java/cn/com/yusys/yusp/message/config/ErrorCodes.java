package cn.com.yusys.yusp.message.config;

/**
 * 错误代码
 *
 * @author xiaodg@yusys.com.cn
 **/
public interface ErrorCodes {
    /**
     * 请求参数错误
     */
    String BAD_REQUEST_PARAM = "400";
}
