package cn.com.yusys.yusp.message.async;

import cn.com.yusys.yusp.message.channel.MessageChannelPublishHolder;
import cn.com.yusys.yusp.message.channel.MessageChannelPublisher;
import cn.com.yusys.yusp.message.config.MessageConstants;
import cn.com.yusys.yusp.message.constant.ReceivedUserTypeEnum;
import cn.com.yusys.yusp.message.entity.MessageContentEntity;
import cn.com.yusys.yusp.message.entity.MessagePoolEntity;
import cn.com.yusys.yusp.message.service.MessageContentService;
import cn.com.yusys.yusp.message.service.MessagePoolHisService;
import cn.com.yusys.yusp.message.service.MessagePoolService;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * 消息发布任务处理类
 *
 * @author xiaodg@yusys.com.cn
 **/
@Component
@Slf4j
public class MessagePoolPublishAsyncTaskExecutor {

    @Autowired
    private MessagePoolService messagePoolService;

    @Autowired
    private MessageContentService messageContentService;

    @Autowired
    private MessagePoolHisService messagePoolHisService;

    @Autowired
    private MessageChannelPublishHolder messageChannelPublishHolder;

    @Autowired
    private AdminSmUserService adminSmUserService;

    /**
     * 发布消息
     *
     * @param messagePoolEntity 消息队列 {@link MessagePoolEntity}
     */
    @Async
    public void publishMessagePool(@NotNull MessagePoolEntity messagePoolEntity) {
        // 使用主键从数据库查询消息队列
        final String pkNo = messagePoolEntity.getPkNo();
        if (!StringUtils.isEmpty(pkNo)) {
            final MessagePoolEntity messagePoolEntityDb = messagePoolService.lambdaQuery().eq(MessagePoolEntity::getPkNo, pkNo).one();
            if (Objects.isNull(messagePoolEntityDb)) {
                log.error("发布消息: 无法从数据库查询到消息队列: {} ", pkNo);
                return;
            }
            final String channelType = messagePoolEntity.getChannelType();
            final List<MessageContentEntity> messageContentList = messageContentService.getByEventNoAndMessageChannelType(messagePoolEntity.getEventNo(), channelType);
            log.info("根据事件唯一编号:[{}]查询消息发送具体内容,返回条数是:[{}]", messagePoolEntity.getEventNo(), messageContentList.size());
            if (messageContentList.size() <= 0) {
                log.error("发布消息: 无法从数据库查询到消息队列的消息内容, pkNo: {}", pkNo);
                return;
            }
            AdminSmUserEntity userEntity = null;
            // 若为借款人
            if (ReceivedUserTypeEnum.JKR.getCode().equals(messagePoolEntity.getUserType())) {
                userEntity = new AdminSmUserEntity();
            } else { // 若为非借款人，即admin_sm_user中的本行人员
                // 用户消息接收用户信息
                userEntity = adminSmUserService.getById(Objects.requireNonNull(messagePoolEntityDb.getUserNo()));
                if (Objects.isNull(userEntity)) {
                    log.error("发布消息: 发送消息时没有找到接收的用户, messagePoolEntity pkNo: {}", pkNo);
                    return;
                }
            }

            // 获取对应的消息渠道发送实现类
            final MessageChannelPublisher messageChannelPublisher = messageChannelPublishHolder.of(channelType);
            if (Objects.isNull(messageChannelPublisher)) {
                log.error("发布消息: 不支持的消息渠道发送类型[{}]没有对应的消息发送渠道实现类)", channelType);
                return;
            }
            final AdminSmUserEntity finalUserEntity = userEntity;
            messageContentList.forEach(messageContent -> {
                log.info("根据事件唯一编号:[{}]查询消息发送具体内容为:[{}]发送开始", messagePoolEntity.getEventNo(), JSON.toJSONString(messageContent, SerializerFeature.WriteMapNullValue));
                RetryTemplate retryTemplate = RetryTemplate.builder()
                        .maxAttempts(Optional.ofNullable(messageContent.getSendNum()).orElse(MessageConstants.MESSAGE_SEND_DEFAULT_RETRY_TIMES))
                        // 重试间隔时间, 单位毫秒
                        .fixedBackoff(MessageConstants.MESSAGE_SEND_DEFAULT_FIXED_BACKOFF_MILS)
                        .retryOn(Exception.class)
                        .build();

                try {
                    retryTemplate.execute(context -> {
                        if (!messageChannelPublisher.publish(messagePoolEntityDb, messageContent, finalUserEntity).get()) {
                            String errMsg = "第[" + (context.getRetryCount() + 1) + "]次发送消息[pkNo: " + messagePoolEntityDb.getPkNo() + "]失败";
                            log.error(errMsg);
                            // 抛出异常触发Spring retry重试机制
                            throw new Exception(errMsg);
                        } else {
                            // 记录消息发送历史
                            log.error("根据事件唯一编号:[{}]查询消息发送具体内容为:[{}]发布消息正常,记录消息发送历史", messagePoolEntity.getEventNo(), JSON.toJSONString(messageContent, SerializerFeature.WriteMapNullValue));
                            messagePoolHisService.recordMessagePoolHis(MessageConstants.SEND_END, messagePoolEntity);
                        }
                        return messagePoolEntityDb;
                    }, context -> {
                        // 重试完成失败进入这里, 记录消息发送历史
                        log.error("根据事件唯一编号:[{}]查询消息发送具体内容为:[{}]发布消息异常,重试完成失败进入这里, 记录消息发送历史", messagePoolEntity.getEventNo(), JSON.toJSONString(messageContent, SerializerFeature.WriteMapNullValue));
                        messagePoolHisService.recordMessagePoolHis(MessageConstants.SEND_EXCEPTION, messagePoolEntity);
                        return messagePoolEntityDb;
                    });

                } catch (Exception e) {
                    log.error("根据事件唯一编号:[{}]查询消息发送具体内容为:[{}]发布消息异常,异常信息为:[{}]", messagePoolEntity.getEventNo(), JSON.toJSONString(messageContent, SerializerFeature.WriteMapNullValue), e);
                }
                log.info("根据事件唯一编号:[{}]查询消息发送具体内容为:[{}]发送结束", messagePoolEntity.getEventNo(), JSON.toJSONString(messageContent, SerializerFeature.WriteMapNullValue));
            });
        } else {
            log.error("收到不存在的消息队列: {}", messagePoolEntity);
        }
    }
}
