package cn.com.yusys.yusp.sequence.controller;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.sequence.domain.entity.SequenceConfig;
import cn.com.yusys.yusp.sequence.domain.query.SequenceConfigQuery;
import cn.com.yusys.yusp.sequence.domain.vo.SequenceConfigAddVo;
import cn.com.yusys.yusp.sequence.domain.vo.SequenceConfigUpdateVo;
import cn.com.yusys.yusp.sequence.service.SequenceConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;


/**
 * 序列号模版配置
 *
 * @author wujp4
 * @date 2021-04-22 14:43:47
 */
@Api("序列号模版配置接口")
@RestController
@RequestMapping("/api/sequenceconfig")
public class SequenceConfigController {
    @Autowired
    private SequenceConfigService sequenceConfigService;

    /**
     * 分页查询序列化模板
     */
    @ApiOperation("分页查询序列化模板")
    @GetMapping("/page")
    public ResultDto<SequenceConfig> list(SequenceConfigQuery sequenceConfigQuery) {
        PageUtils page = sequenceConfigService.queryPage(sequenceConfigQuery);

        return ResultDto.success(page);
    }

    /**
     * 新增序列化模板
     */
    @ApiOperation("新增序列化模板")
    @PostMapping("/add")
    public ResultDto add(@Valid @RequestBody SequenceConfigAddVo sequenceConfigAddVo) {
        sequenceConfigService.save(sequenceConfigAddVo);

        return ResultDto.success();
    }


    /**
     * 获得序列化模板详情
     */
    @ApiOperation("获得序列化模板详情")
    @GetMapping("/getDetail/{id}")
    public ResultDto info(@PathVariable("id") String id) {
        SequenceConfig sequenceConfig = sequenceConfigService.getById(id);

        return ResultDto.success(sequenceConfig);
    }

    /**
     * 更新序列化模板详情
     */
    @ApiOperation("更新序列化模板详情")
    @PostMapping("/update")
    public ResultDto update(@Valid @RequestBody SequenceConfigUpdateVo sequenceConfigUpdateVo) {
        sequenceConfigService.updateById(sequenceConfigUpdateVo);

        return ResultDto.success();
    }

    /**
     * 删除序列化模板
     */
    @ApiOperation("删除序列化模板")
    @PostMapping("/delete/{id}")
    public ResultDto delete(@PathVariable("id") String id) {
        sequenceConfigService.removeByIds(id);

        return ResultDto.success();
    }

    /**
     * 创建序列化模板
     */
    @ApiOperation("创建序列化模板")
    @PostMapping("/createSequence/{seqId}")
    public ResultDto createSequence(@PathVariable("seqId") String seqId) {
        sequenceConfigService.createSequence(seqId);

        return ResultDto.success();
    }

    /**
     * 重置序列化模板
     */
    @ApiOperation("重置序列化模板")
    @PostMapping("/reCreateSequence/{seqId}")
    public ResultDto reCreateSequence(@PathVariable(value = "seqId", required = true) String seqId) {
        sequenceConfigService.reCreateSequence(seqId);

        return ResultDto.success();
    }

    /**
     * 获得当前序列值序列
     */
    @ApiOperation("获得当前序列值序列")
    @PostMapping("/getCurrentSeq/{seqId}")
    public ResultDto getCurrentSeq(@PathVariable(value = "seqId", required = true) String seqId) {
        String currentValue = sequenceConfigService.getCurrentSeq(seqId);

        return ResultDto.success().data(currentValue);
    }


    /**
     * 获得下一序列值
     */
    @ApiOperation("获得下一序列值")
    @PostMapping("/getNextSeq/{seqId}")
    public ResultDto getNextSeq(@PathVariable(value = "seqId", required = true) String seqId) {
        String nextValue = sequenceConfigService.getNextSeq(seqId);

        return ResultDto.success().data(nextValue);
    }


    /**
     * 获得配置的模版序列
     */
    @ApiOperation("获得配置的模版序列")
    @PostMapping("/getSequenceWithTemplate/{seqId}")
    public ResultDto getSequenceWithTemplate(@PathVariable(value = "seqId", required = true) String seqId, Map<String, String> paramsMap) {
        String template = sequenceConfigService.getSequenceWithTemplate(seqId, paramsMap);

        return ResultDto.success().data(template);
    }

    /**
     * 获得下一批序列值
     */
    @ApiOperation("获得下一批序列值")
    @PostMapping("/getNextSequenceWithCount/{seqId}/{count}")
    public ResultDto getNextSequenceWithCount(@PathVariable(value = "seqId") String seqId, @PathVariable("count") Integer count) {
        List<String> nextValues = sequenceConfigService.getNextSequenceWithCount(seqId, count);

        return ResultDto.success().data(nextValues);
    }

    /**
     * 获得配置的模版序列
     */
    @ApiOperation("获得配置的模版序列")
    @PostMapping("/getSequenceWithTemplateWithCount/{seqId}/{count}")
    public ResultDto getSequenceWithTemplateWithCount(@PathVariable(value = "seqId") String seqId, @PathVariable("count") Integer count, @RequestBody(required = false) Map<String, String> paramsMap) {
        List<String> nextValues = sequenceConfigService.getSequenceWithTemplateWithCount(seqId, count, paramsMap);

        return ResultDto.success().data(nextValues);
    }
}
