package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.notice.vo.AdminSmReciveVo;
import cn.com.yusys.yusp.oca.domain.bo.PasswordForgetAndResetCheckBo;
import cn.com.yusys.yusp.oca.domain.bo.PasswordResetBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserDutyRelEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserMgrOrgEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserRoleRelEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmPasteUserQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmUserDutyRelQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmUserQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmUserRoleRelQuery;
import cn.com.yusys.yusp.oca.domain.vo.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * 系统用户表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-11-16 15:31:54
 */
public interface AdminSmUserService extends IService<AdminSmUserEntity> {

    Page<AdminSmUserVo> queryPage(AdminSmUserQuery query);

    Page<AdminSmUserVo> queryPageForXw(AdminSmUserQuery query);

    Page<AdminSmUserVo> queryPageForDh(AdminSmUserQuery query);

    Page<AdminSmUserVo> queryPageAll(AdminSmUserQuery query);

    AdminSmUserDetailVo getDetailById(String userId);

    void batchDisable(String[] ids);

    void batchDelete(String[] ids);

    void batchEnable(String[] ids);

    List<UserRoleRelVo> getUserRoleList(AdminSmUserRoleRelQuery query);

    void saveUserRoleList(String userId, List<AdminSmUserRoleRelEntity> list);

    List<UserDutyRelVo> getUserDutyList(AdminSmUserDutyRelQuery query);

    void saveUserDutyList(String userId, List<AdminSmUserDutyRelEntity> list);

    List<UserMgrOrgVo> getUserMgrOrgList(String userId);

    void saveUserMgrOrg(String userId, List<AdminSmUserMgrOrgEntity> list);

    UserSessionVo getUserByAuthorization(String userId, String clientId);

    ResultDto modifyPassword(String password, String loginCode);

    ResultDto resetPassword(PasswordResetBo passwordResetBo);

    ResultDto forgetPasswordCheck(PasswordForgetAndResetCheckBo passwordForgetAndResetCheckBo);

    ResultDto modifyUserMobilePhone(String userMobilePhone, String loginCode);

    List<UserSubscribeVo> selectUserSubscribeVoList();

    Set<String> findUserIdsByOrgId(String orgId);

    void updateLoginTime(String userId, Date lastLoginTime);

    Page<AdminSmUserVo> queryPageExcept(AdminSmPasteUserQuery query);

    void freezingUser(String userId);

    /**
     * 配合导出，使用用户名称模糊查询用户id
     *
     * @param userName
     * @return
     */
    List<AdminSmUserEntity> selectIdLikeName(String userName);

    /**
     * 工作流获取用户分页信息
     *
     * @param query
     * @return
     */
    Page<AdminSmUserVo> getUsersForWf(AdminSmUserQuery query);

    /**
     * 工作流通过机构号获取用户信息
     *
     * @param orgId
     * @return
     */
    List<AdminSmUserVo> getUsersByOrgForWf(String orgId);

    /**
     * 工作流通过机构号和岗位号获取用户信息
     *
     * @param orgId,dutyId
     * @return
     */
    List<AdminSmUserVo> getUsersByOrgAndDutyForWf(String orgId, String userId);

    /**
     * 工作流通过部门id获取用户信息
     *
     * @param deptId
     * @return
     */
    List<AdminSmUserVo> getUsersByDeptForWf(String deptId);

    /**
     * 工作流通过岗位id获取用户信息
     *
     * @param dutyId
     * @return
     */
    List<AdminSmUserVo> getUsersByDutyForWf(String dutyId);

    /**
     * 工作流通过角色id获取用户信息
     *
     * @param roleId
     * @return
     */
    List<AdminSmUserVo> getUsersByRoleForWf(String roleId);

    /**
     * 工作流通过userId获取用户信息
     *
     * @param userId
     * @return
     */
    AdminSmUserVo getUserInfoForWf(String userId);

    /**
     * 获取用户的 roleId 和 objId
     *
     * @param userId
     * @return
     */
    List<AdminSmReciveVo> selectRoleAndObj(String userId);

    /**
     * 获取用户姓名
     *
     * @param userId
     * @return
     */
    String getUserNameById(String userId);

    /**
     * 根据手机号获取用户信息
     *
     * @param userMobilephone
     * @return 用户信息
     */
    ResultDto<UserEntityVo> getByPhoneNumber(String userMobilephone);

    /**
     * 根据账号获取用户信息
     *
     * @param loginCode 账号
     * @return 用户信息
     */
    AdminSmUserDto getByLoginCode(String loginCode);

    /**
     * 根据账号获取用户信息
     *
     * @param loginCode 账号
     * @return 用户信息
     */
    AdminSmUserEntity getAllByLoginCode(String loginCode);

    /**
     * 根据多账号获取多用户信息
     *
     * @param loginCodes 账号列表
     * @return 用户信息列表
     */
    List<AdminSmUserDto> getByLoginCodeList(List<String> loginCodes);

    /**
     * 根据姓名模糊查询多个用户
     *
     * @param name 姓名
     * @return 用户信息列表
     */
    List<AdminSmUserDto> getByName(String name);

    /**
     * 查询是否小微客户经理
     *
     * @param loginCode
     * @return
     */
    GetIsXwUserDto getIsXWUserByLoginCode(String loginCode);

    /**
     * 根据机构号查询用户信息
     *
     * @param orgId
     * @return
     */
    List<AdminSmUserDto> getByOrgid(String orgId);

    /**
     * 查询用户和岗位信息
     *
     * @param userAndDutyReqDto
     * @return
     */
    List<UserAndDutyRespDto> getUserAndDuty(UserAndDutyReqDto userAndDutyReqDto);

    /**
     * 通用客户经理查询
     *
     * @param commonUserQueryReqDto
     * @return
     */
    List<CommonUserQueryRespDto> getCommonUserInfo(CommonUserQueryReqDto commonUserQueryReqDto);


    /**
     * 通过流程实例号获取当前处理用户的详细信息
     *
     * @param instanceId 账号
     * @return 用户信息
     */
    AdminSmUserDto getTodoUserInfo(String instanceId);

    /**
     * 查询在途的审批流程数量
     *
     * @param cusId
     * @return
     */
    int queryCusFlowCount(String cusId);

    /**
     * 根据机构编号分页查询用户信息
     *
     * @param getUserInfoByOrgCodeDto
     * @return
     */
    List<AdminSmUserDto> getUserList(@RequestBody GetUserInfoByOrgCodeDto getUserInfoByOrgCodeDto);


    /**
     * 查询所有小微客户经理
     */
    List<AdminSmUserEntity> getAllXwUserList();

    /**
     * 集中作业总行本地机构（除小微）下的客户经理
     * @return
     */
    List<AdminSmUserEntity> getJzzyUserList();

    /**
     * 查询机构下小微客户经理
     * @author jijian_yx
     * @date 2021/11/6 19:55
     **/
    List<AdminSmUserEntity> getXwUserForOrg(List<String> orgId);

    /**
     * 查询机构下非小微用户
     * @author jijian_yx
     * @date 2021/11/6 19:55
     **/
    List<AdminSmUserEntity> getNotXwUserForOrg(List<String> orgId);
}

