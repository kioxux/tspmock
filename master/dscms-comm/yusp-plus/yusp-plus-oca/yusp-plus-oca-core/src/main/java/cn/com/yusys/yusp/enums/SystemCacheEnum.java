package cn.com.yusys.yusp.enums;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 系统缓存翻译项
 *
 * @author yangzai
 * @since 2021-07-07
 **/
public enum SystemCacheEnum {

    CACHE_KEY_USERNAME("userName", "客户名称缓存"),
    CACHE_KEY_ORGNAME("orgName", "机构名称缓存"),
    CACHE_KEY_DTPNAME("dptName", "部门名称缓存"),
    CACHE_KEY_DATADICT("datadict", "字典项缓存");

    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (SystemCacheEnum enumData : EnumSet.allOf(SystemCacheEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;

    public String value;

    SystemCacheEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String getKey(String dataValue) {
        String key = null;
        for (SystemCacheEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String getValue() {
        return keyValue.get(key);
    }

}