package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.oca.domain.entity.ProductInfoEntity;
import cn.com.yusys.yusp.common.query.PageQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *@program: yusp-plus
 *@description: 产品查出接收对象
 *@author: wujiangpeng
 *@email: wujp4@yusys.com.cn
 *@create: 2020-11-26 16:38
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductInfoQuery extends PageQuery<ProductInfoEntity> {

    private String productId;

    private String productName;

    private String productType;
}
