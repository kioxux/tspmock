package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmLogicSysBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmLogicSysEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * 系统逻辑系统表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-19 14:30:22
 */
public interface AdminSmLogicSysService extends IService<AdminSmLogicSysEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 逻辑系统列表查询
     *
     * @param params
     * @return
     */
    PageUtils getAdminSmLogicSys(Map<String, Object> params);

    /**
     * 逻辑系统-修改功能
     *
     * @param adminSmLogicSysBo
     * @param funcId
     * @return
     */
    int updateAdminSmLogic(AdminSmLogicSysBo adminSmLogicSysBo, String funcId);

    /**
     * 逻辑系统设置 状态生效/失效
     *
     * @param adminSmLogicSysBo
     * @return
     */
    ResultDto updateAdminSmLogicStat(AdminSmLogicSysBo adminSmLogicSysBo);

    int deleteLogicAndCrelInfo(String sysId);

    AdminSmLogicSysEntity insertAndCopy(AdminSmLogicSysBo adminSmLogicSys);
}

