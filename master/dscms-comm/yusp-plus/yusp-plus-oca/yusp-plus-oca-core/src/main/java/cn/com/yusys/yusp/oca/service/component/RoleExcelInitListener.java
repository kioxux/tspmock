package cn.com.yusys.yusp.oca.service.component;

import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.domain.constants.Constants;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmRoleEntity;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmRoleExcelVo;
import cn.com.yusys.yusp.oca.service.AdminSmRoleService;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 角色数据excel解析器
 */
@Slf4j
public class RoleExcelInitListener extends AnalysisEventListener<AdminSmRoleExcelVo> {


    List<AdminSmRoleEntity> list = new ArrayList<>();
    private AdminSmRoleService adminSmRoleService;
    //标识，如果传入的是0 则执行导入，如果是1 则执行删除
    //加这个标识是为了防止出现导入大量错误数据后顺利回退
    //如果在页面已经对导入的角色受过权了 那么先将授权取消掉，再执行删除
    private int flag;

    public RoleExcelInitListener(AdminSmRoleService adminSmRoleService, int flag) {
        this.adminSmRoleService = adminSmRoleService;
        this.flag = flag;
    }


    @Override
    public void invoke(AdminSmRoleExcelVo adminSmRoleExcelVo, AnalysisContext analysisContext) {
        if (!StringUtils.isEmpty(adminSmRoleExcelVo.getRoleCode())
                && !StringUtils.isEmpty(adminSmRoleExcelVo.getRoleName())
                && !StringUtils.isEmpty(adminSmRoleExcelVo.getOrgId())) {

            AdminSmRoleEntity roleEntity = BeanUtils.beanCopy(adminSmRoleExcelVo, AdminSmRoleEntity.class);
            roleEntity.setRoleId(roleEntity.getRoleCode());
            roleEntity.setRoleSts(AvailableStateEnum.ENABLED);
            list.add(roleEntity);
            if (list.size() >= Constants.ExcelInitConstance.BATCH_COUNT) {
                if (flag == 0) {
                    adminSmRoleService.saveOrUpdateBatch(list);
                } else {
                    Set<String> ids = list.stream().map(role -> role.getRoleId()).collect(Collectors.toSet());
                    adminSmRoleService.getBaseMapper().deleteBatchIds(ids);
                }
                list.clear();
            }
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        if (flag == 0) {
            adminSmRoleService.saveOrUpdateBatch(list);
            log.info("角色数据初始化完成！");
        } else {
            Set<String> ids = list.stream().map(role -> role.getRoleId()).collect(Collectors.toSet());
            adminSmRoleService.getBaseMapper().deleteBatchIds(ids);
            log.info("角色数据删除完成！");
        }
    }
}