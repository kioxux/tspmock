package cn.com.yusys.yusp.oca.dao;

import cn.com.yusys.yusp.oca.domain.entity.AdminSmDptEntity;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmDptVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统部门表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-11-12 10:47:26
 */

public interface AdminSmDptDao extends BaseMapper<AdminSmDptEntity> {
    List<AdminSmDptVo> selectAllDpt(@Param(Constants.WRAPPER) QueryWrapper<AdminSmDptVo> queryWrapper);

    Page<AdminSmDptVo> getDeptsForWf(Page<AdminSmDptVo> page, @Param(Constants.WRAPPER) QueryWrapper<AdminSmDptVo> queryWrapper);

}
