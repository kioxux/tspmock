package cn.com.yusys.yusp.service.server.xdxt0014;

import cn.com.yusys.yusp.constants.OcaTradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxt0014.req.Xdxt0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0014.resp.List;
import cn.com.yusys.yusp.dto.server.xdxt0014.resp.Xdxt0014DataRespDto;
import cn.com.yusys.yusp.enums.OcaTradeEnum;
import cn.com.yusys.yusp.oca.dao.AdminSmLookupDictDao;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmLookupDictEntity;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @version 1.0.0
 * @项目名称: yusp-oca模块
 * @类名称: Xdxt0014Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-26 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxt0014Service extends ServiceImpl<AdminSmLookupDictDao, AdminSmLookupDictEntity> {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0014Service.class);

    /**
     * 字典项对象通用列表查询
     *
     * @param xdxt0014DataReqDto
     * @return
     */
    @Transactional
    public Xdxt0014DataRespDto getXdxt0014(Xdxt0014DataReqDto xdxt0014DataReqDto) throws Exception {
        logger.info(OcaTradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0014.key, OcaTradeEnum.TRADE_CODE_XDXT0014.value, JSON.toJSONString(xdxt0014DataReqDto));
        Xdxt0014DataRespDto xdxt0014DataRespDto = new Xdxt0014DataRespDto();
        try {
            java.util.List<List> list = this.baseMapper.getXdxt0014(xdxt0014DataReqDto);
            xdxt0014DataRespDto.setList(list);
        } catch (Exception e) {
            logger.info(OcaTradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0014.key, OcaTradeEnum.TRADE_CODE_XDXT0014.value, e.getMessage());
            throw new Exception(OcaTradeEnum.EPB099999.key);
        }
        logger.info(OcaTradeLogConstants.SERVICE_END_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0014.key, OcaTradeEnum.TRADE_CODE_XDXT0014.value, JSON.toJSONString(xdxt0014DataRespDto));
        return xdxt0014DataRespDto;
    }
}
