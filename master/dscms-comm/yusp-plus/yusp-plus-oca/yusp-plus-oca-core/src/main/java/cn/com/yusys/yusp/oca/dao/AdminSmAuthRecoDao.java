package cn.com.yusys.yusp.oca.dao;

import cn.com.yusys.yusp.oca.domain.entity.AdminSmAuthRecoEntity;
import cn.com.yusys.yusp.oca.domain.vo.AdminDataTmplControlVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminRecoWithTmplVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmAuthRecoVo;
import cn.com.yusys.yusp.oca.domain.vo.MenuVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 资源对象授权记录表(含菜单、控制点、数据权限)
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-19 17:43:42
 */

public interface AdminSmAuthRecoDao extends BaseMapper<AdminSmAuthRecoEntity> {

    /**
     * 菜单查询 M
     */
    List<MenuVo> findMenuTreeList(String sysId);

    /**
     * 控制点查询 C
     */
    List<MenuVo> findContrTreeList(String sysId);

    /**
     * 数据权限查询 D
     */
    List<MenuVo> findDataAuthTreeList(String sysId);

    /**
     * 使用 menuId 关联数据模板查询授权记录
     * @param wrapper
     * @return
     */
    List<AdminRecoWithTmplVo> getByMenuIdWithTmpl(@Param(Constants.WRAPPER) QueryWrapper<AdminRecoWithTmplVo> wrapper);

    /**
     * 查询角色列表授权的数据模板列表
     * @param wrapper
     * @return
     */
    List<AdminDataTmplControlVo> getDataTmplControl(@Param(Constants.WRAPPER) QueryWrapper<AdminDataTmplControlVo> wrapper);

    /**
     * 根据菜单id查询已被授权的控制点id
     * @param queryWrapper
     * @return
     */
    List<String> selectResContrIdByMenuIds(@Param(Constants.WRAPPER) QueryWrapper<String> queryWrapper);

    void deleteInBatch(List<AdminSmAuthRecoVo> vos);
}
