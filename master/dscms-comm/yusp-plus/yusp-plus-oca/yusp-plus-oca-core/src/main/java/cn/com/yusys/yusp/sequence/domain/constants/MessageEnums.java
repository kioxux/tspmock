package cn.com.yusys.yusp.sequence.domain.constants;

public enum MessageEnums {

    SEQUENCE_ID_EXISTS("30100020","序列号模板ID已存在，请勿重复添加"),
    SEQUENCE_ADD_ERROR("30100021","创建序列出错，请联系管理员！"),
    SEQUENCE_ADD_SUCCESS("30100022","添加成功！"),
    SEQUENCE_ID_STRATEGY("30100023","序列号模板ID只能由大写字母、数字、下划线组成且首字母只能是大写字母！");

    //可以看出这在枚举类型里定义变量和方法和在普通类里面定义方法和变量没有什么区别。
    //唯一要注意的只是变量和方法定义必须放在所有枚举值定义的后面，否则编译器会给出一个错误。
    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



    MessageEnums(String code, String message){ //加上public void 上面定义枚举会报错 The constructor Color(int, String) is undefined
        this.code=code;
        this.message=message;

    }
}
