package cn.com.yusys.yusp.oca.domain.vo;

import lombok.Data;

@Data
public class AdminRecoWithTmplVo {

    private String authRecoId;

    private String authobjId;

    private String menuId;

    private String authTmplId;

    private String authTmplName;
}
