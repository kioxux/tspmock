package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmPasswordLogEntity;
import cn.com.yusys.yusp.oca.service.AdminSmPasswordLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;



/**
 * 密码修改记录表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-02 15:07:44
 */
@RestController
@RequestMapping("/api/adminsmpasswordlog")
public class AdminSmPasswordLogController {
    @Autowired
    private AdminSmPasswordLogService adminSmPasswordLogService;

    /**
     * 列表
     */
    @GetMapping("/list")
    public ResultDto list(@RequestParam Map<String, Object> params){
        PageUtils page = adminSmPasswordLogService.queryPage(params);
        return ResultDto.success().extParam("page", page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{logId}")
    public ResultDto info(@PathVariable("logId") String logId){
		AdminSmPasswordLogEntity adminSmPasswordLog = adminSmPasswordLogService.getById(logId);
        return ResultDto.success().extParam("adminSmPasswordLog", adminSmPasswordLog);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public ResultDto save(@RequestBody AdminSmPasswordLogEntity adminSmPasswordLog){
        adminSmPasswordLog.setUserId(SessionUtils.getUserId());
        adminSmPasswordLog.setLastChgDt(new Date());
        adminSmPasswordLog.setLastChgUsr(SessionUtils.getUserId());
		adminSmPasswordLogService.save(adminSmPasswordLog);
        return ResultDto.success();
    }


    /**
     * 修改
     */
    @PostMapping("/update")
    public ResultDto update(@RequestBody AdminSmPasswordLogEntity adminSmPasswordLog){
		adminSmPasswordLogService.updateById(adminSmPasswordLog);
        return ResultDto.success();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public ResultDto delete(@RequestBody String[] logIds){
		adminSmPasswordLogService.removeByIds(Arrays.asList(logIds));
        return ResultDto.success();
    }
}
