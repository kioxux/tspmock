package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.dscms.domain.EmpAttendInfo;
import cn.com.yusys.yusp.dscms.domain.EmpCheckInfo;
import cn.com.yusys.yusp.flow.ClientCons;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.NextNodeInfoDto;
import cn.com.yusys.yusp.flow.dto.WFException;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.dscms.service.EmpAttendInfoService;
import cn.com.yusys.yusp.dscms.service.EmpCheckInfoService;
import cn.com.yusys.yusp.flow.service.WorkflowCoreService;
import cn.com.yusys.yusp.flow.service.core.WorkflowEngineExtInterface;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @className ZXGL03BizService
 * @Description 请假及提前签退审批流程
 * @Date 2020/12/21 : 10:43
 */
@Service
public class XTGL01BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(XTGL01BizService.class);
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private EmpAttendInfoService empAttendInfoService;//请销假

    @Autowired
    private EmpCheckInfoService empCheckInfoService;//签到签退

    @Autowired
    private WorkflowEngineExtInterface workflowEngineExtService;


    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        log.info("后业务处理类型:" + currentOpType);
        String bizType = resultInstanceDto.getBizType();
        log.info("进入业务类型【{}】、业务处理类型【{}】流程处理逻辑开始！", bizType, currentOpType);
        if("XT001".equals(bizType)) {
            handlerXT001(resultInstanceDto);//请假
        }else{
            handlerXT002(resultInstanceDto);//t
        }
        log.info("进入业务类型【{}】流程处理逻辑结束！", bizType);
    }

    public void handlerXT001(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String iqpSerno = resultInstanceDto.getBizId();
        log.info("业务处理类型" + currentOpType);
        try {
            EmpAttendInfo empAttendInfo = empAttendInfoService.selectByPrimaryKey(iqpSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("请假申请" + iqpSerno + "流程发起操作，流程参数" + resultInstanceDto);

            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("请假申请" + iqpSerno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                empAttendInfoService.handleBusinessDZDataAfterStart(iqpSerno);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("请假申请" + iqpSerno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("征信查询务申请" + iqpSerno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //由审批中111 -> 审批通过 997
                empAttendInfo.setApprStatus(CmisFlowConstants.WF_STATUS_997);
                empAttendInfoService.updateSelective(empAttendInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("请假申请" + iqpSerno + "退回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (isFirstNodeCheck(resultInstanceDto)) {
                    empAttendInfo.setApprStatus(CmisFlowConstants.WF_STATUS_992);
                    empAttendInfoService.updateSelective(empAttendInfo);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("请假申请"+iqpSerno+"打回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回992
                if (isFirstNodeCheck(resultInstanceDto)) {
                    empAttendInfo.setApprStatus(CmisFlowConstants.WF_STATUS_992);
                    empAttendInfoService.updateSelective(empAttendInfo);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("请假申请"+iqpSerno+"拿回操作，流程参数："+ resultInstanceDto.toString());
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("请假申请" + iqpSerno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("请假申请" + iqpSerno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                empAttendInfo.setApprStatus(CmisFlowConstants.WF_STATUS_998);
                empAttendInfoService.updateSelective(empAttendInfo);
            } else {
                log.warn("请假申请" + iqpSerno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                 getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    public void handlerXT002(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String iqpSerno = resultInstanceDto.getBizId();
        log.info("业务处理类型" + currentOpType);
        try {
            EmpCheckInfo empCheckInfo = empCheckInfoService.selectByPrimaryKey(iqpSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("提前签退申请" + iqpSerno + "流程发起操作，流程参数" + resultInstanceDto);

            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("提前签退申请" + iqpSerno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                empCheckInfoService.handleBusinessDZDataAfterStart(iqpSerno);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("提前签退申请" + iqpSerno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("征信查询务申请" + iqpSerno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //由审批中111 -> 审批通过 997
                empCheckInfoService.handleBusinessDZDataAfterEnd(iqpSerno);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("提前签退申请" + iqpSerno + "退回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (isFirstNodeCheck(resultInstanceDto)) {
                    empCheckInfo.setApproveStatus(CmisFlowConstants.WF_STATUS_992);
                    empCheckInfoService.updateSelective(empCheckInfo);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("提前签退申请"+iqpSerno+"打回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回992
                if (isFirstNodeCheck(resultInstanceDto)) {
                    empCheckInfo.setApproveStatus(CmisFlowConstants.WF_STATUS_992);
                    empCheckInfoService.updateSelective(empCheckInfo);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("提前签退申请"+iqpSerno+"拿回操作，流程参数："+ resultInstanceDto.toString());
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("提前签退申请" + iqpSerno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("提前签退申请" + iqpSerno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                empCheckInfo.setApproveStatus(CmisFlowConstants.WF_STATUS_998);
                empCheckInfoService.updateSelective(empCheckInfo);
            } else {
                log.warn("提前签退申请" + iqpSerno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                 getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.XTGL01.equals(flowCode);
    }

    public boolean isFirstNodeCheck(ResultInstanceDto resultInstanceDto) {
        boolean checkFlag = false;
        List<NextNodeInfoDto> nextNodeUserInfo = resultInstanceDto.getNextNodeInfos();
        if (CollectionUtils.isEmpty(nextNodeUserInfo)) {
            throw new YuspException("BWF000001", "未获取到流程下一处理节点用户信息！");
        }
        //循环下一节点用户信息判断是否存在初始节点
        for (NextNodeInfoDto nextNodeInfoDto : nextNodeUserInfo) {
            //调用接口判断下一节点是否为初始节点
            checkFlag = workflowEngineExtService.isFisrtBizNode(nextNodeInfoDto.getNextNodeId());
            //若是初始节点，则跳出循环
            if (checkFlag) {
                break;
            }
        }
        return checkFlag;
    }

    /**
     * 封装流程异常处理以及发送消息队列的方法
     *
     * @param e            后业务处理的异常
     * @param instanceInfo 流程入参信息
     */
    public void getExceptionMessageAndSendExptionMQ(Exception e, ResultInstanceDto instanceInfo) throws JsonProcessingException {
        WFException exception = new WFException();
        exception.setPkId(StringUtils.uuid(true));
        exception.setBizId(instanceInfo.getBizId());//业务流水号
        exception.setBizType(instanceInfo.getBizType());//流程类型
        exception.setFlowName(instanceInfo.getFlowName());//流程名称
        exception.setInstanceId(instanceInfo.getInstanceId());//流程实例号
        exception.setNodeId(instanceInfo.getNodeId());//流程处理节点
        exception.setNodeName(instanceInfo.getNodeName());//流程节点名称
        exception.setUserId(instanceInfo.getCurrentUserId());//流程当前用户
        exception.setOpType(instanceInfo.getCurrentOpType());//流程处理类型
        exception.setFlowId(Long.parseLong(instanceInfo.getFlowId()));//流程编号
        exception.setBizParam(ObjectMapperUtils.instance().writeValueAsString(instanceInfo));//流程入参信息
        //流程异常类型，暂未添加该字段
        //exception.setExceptionType(e.getClass().getSimpleName());
        exception.setExceptionInfo("错误类型：" + e.getClass().getSimpleName() + "\r\n错误原因：" + e.getMessage() + "\r\n错误位置：" + ObjectMapperUtils.instance().writeValueAsString(e.getStackTrace()[0]));//异常信息
        exception.setExceptionTime(DateUtils.getCurrDateTimeStr());
        // 后业务处理失败时，将异常信息保存到异常表中
        amqpTemplate.convertAndSend(ClientCons.queue_exception, exception);
    }
}
