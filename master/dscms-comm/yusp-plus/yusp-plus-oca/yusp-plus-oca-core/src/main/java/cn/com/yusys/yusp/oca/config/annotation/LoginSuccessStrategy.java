package cn.com.yusys.yusp.oca.config.annotation;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @description: 登录动作接口拦截器
 * @author: zhangsong
 * @date: 2021/3/29
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({RestApiResponseAdvice.class})
public @interface LoginSuccessStrategy {
}