package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.common.query.PageQuery;
import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmDutyVo;
import lombok.Data;

@Data
public class AdminSmDutyQuery extends PageQuery<AdminSmDutyVo> {
    private String orgId;
    private String keyWord;
    private String dutyCode;
    private String dutyName;
    private AvailableStateEnum dutySts;
}
