package cn.com.yusys.yusp.notice.controller;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.notice.entity.AdminSmNoticeReciveEntity;
import cn.com.yusys.yusp.notice.service.AdminSmNoticeReciveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 系统公告表接收对象表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-15 13:05:33
 */
@RestController
@RequestMapping("notice/adminsmnoticerecive")
public class AdminSmNoticeReciveController {
    @Autowired
    private AdminSmNoticeReciveService adminSmNoticeReciveService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public ResultDto list(@RequestParam Map<String, Object> params) {
        PageUtils page = adminSmNoticeReciveService.queryPage(params);
        return ResultDto.success().extParam("page", page);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{reciveId}")
    public ResultDto info(@PathVariable("reciveId") String reciveId){
		AdminSmNoticeReciveEntity adminSmNoticeRecive = adminSmNoticeReciveService.getById(reciveId);
        return ResultDto.success().extParam("adminSmNoticeRecive", adminSmNoticeRecive);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public ResultDto save(@RequestBody AdminSmNoticeReciveEntity adminSmNoticeRecive){
		adminSmNoticeReciveService.save(adminSmNoticeRecive);
        return ResultDto.success();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public ResultDto update(@RequestBody AdminSmNoticeReciveEntity adminSmNoticeRecive){
		adminSmNoticeReciveService.updateById(adminSmNoticeRecive);
        return ResultDto.success();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public ResultDto delete(@RequestBody String[] reciveIds){
		adminSmNoticeReciveService.removeByIds(Arrays.asList(reciveIds));
        return ResultDto.success();
    }
}
