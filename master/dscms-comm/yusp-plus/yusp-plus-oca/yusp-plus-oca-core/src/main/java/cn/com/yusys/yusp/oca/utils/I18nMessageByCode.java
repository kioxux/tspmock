package cn.com.yusys.yusp.oca.utils;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmMessageEntity;
import cn.com.yusys.yusp.oca.service.AdminSmMessageService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class I18nMessageByCode {

    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Autowired
    AdminSmMessageService adminSmMessageService;

    public String getMessageByCode(String code) {
        //获取当前语言类型
        String language = LocaleContextHolder.getLocale().toString().toLowerCase();
        //根据码获取信息
        String message = (String) stringRedisTemplate.opsForHash().get("sysMessage_" + language, code);

        //如果从缓存拿不到,重新加载缓存
        if (StringUtils.isEmpty(message)) {

            //重新加载缓存
//            messageLoadTask.run(null);
//            message = (String) stringRedisTemplate.opsForHash().get("sysMessage_"+language,code);
            AdminSmMessageEntity adminSmMessageEntity = adminSmMessageService.getOne(new QueryWrapper<AdminSmMessageEntity>().eq("CODE", code));
            if (Objects.nonNull(adminSmMessageEntity)){
                message = adminSmMessageEntity.getMessage();
            }
        }
        return message;
    }
}
