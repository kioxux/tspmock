package cn.com.yusys.yusp.oca.domain.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * @program: yusp-plus
 * @description: 提示信息修改参数
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2021-01-14 09:38
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminSmMessageEditBo {
    /**
     * 消息编号
     */
    @NotEmpty(message = "消息编号不能为空")
    private String messageId;
    /**
     * 信息码
     */
    @NotEmpty(message = "信息码不能为空")
    private String code;
    /**
     * 信息级别:success成功 info信息 warning警告 error错误
     */
    @NotEmpty(message = "信息码不能为空")
    private String messageLevel;
    /**
     * 提示内容
     */
    @NotEmpty(message = "信息码不能为空")
    private String message;
    /**
     * 消息类别：COMINFO系统级通用提示 DBERR数据库错误提示 MODULEINFO模块提示
     */
    @NotEmpty(message = "信息码不能为空")
    private String messageType;
    /**
     * 所属模块名称
     */
    private String funcName;
}
