package cn.com.yusys.yusp.message.enumeration;

/**
 * 消息渠道类别
 *
 * @author xiaodg@yusys.com.cn
 **/
public enum MessageChannelEnum {
    /**
     * 系统消息
     */
    SYSTEM("system"),
    /**
     * 电子邮件
     */
    EMAIL("email"),
    /**
     * 手机短信
     */
    MOBILE("mobile"),

    /**
     * 微信
     */
    WECHAT("wechat");

    private final String type;

    MessageChannelEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }
}
