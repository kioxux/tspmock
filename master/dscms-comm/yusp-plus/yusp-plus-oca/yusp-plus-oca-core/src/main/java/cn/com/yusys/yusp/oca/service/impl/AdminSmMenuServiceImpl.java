package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.common.utils.GenericBuilder;
import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.common.utils.Query;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.user.impl.MenuImpl;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.oca.dao.AdminSmMenuDao;
import cn.com.yusys.yusp.oca.domain.bo.DragMenuBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmAuthRecoEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmMenuEntity;
import cn.com.yusys.yusp.oca.domain.form.AdminSmMenuConditionForm;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmAuthTreeVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmMenuVo;
import cn.com.yusys.yusp.oca.domain.vo.MenuVo;
import cn.com.yusys.yusp.oca.service.AdminSmAuthRecoService;
import cn.com.yusys.yusp.oca.service.AdminSmBusiFuncService;
import cn.com.yusys.yusp.oca.service.AdminSmMenuService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


@Service("adminSmMenuService")
public class AdminSmMenuServiceImpl extends ServiceImpl<AdminSmMenuDao, AdminSmMenuEntity> implements AdminSmMenuService {

    @Autowired
    AdminSmAuthRecoService adminSmAuthRecoService;
    @Autowired
    AdminSmBusiFuncService adminSmBusiFuncService;
    @Autowired
    ObjectMapper objectMapper;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AdminSmMenuEntity> page = this.page(
                new Query<AdminSmMenuEntity>().getPage(params),
                new QueryWrapper<AdminSmMenuEntity>()
        );
        return new PageUtils(page);
    }

    @Override
    public List<MenuImpl> getAdminSmMenu(String userId) {
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StringUtils.nonEmpty(userId), "t_user.user_id", userId);

        List<MenuImpl> adminSmMenuVoList = this.baseMapper.getadminSmMenuVoList(queryWrapper);

        return adminSmMenuVoList;
    }

    /**
     * 菜单树查询
     *
     * @param sysId
     * @param lazy
     * @param menuId
     * @return
     */
    @Override
    public List<MenuVo> getMenuTree(String sysId, boolean lazy, String menuId) {

        //如果前端未传 sysId，后端默认取 clientid test
        if (StringUtils.isEmpty(sysId)) {
            sysId = StringUtils.isEmpty(SessionUtils.getClientId()) ? "test" : SessionUtils.getClientId();
        }

        List<MenuVo> menuVoList = new ArrayList<>();
        List<AdminSmMenuEntity> entityList;
        // 懒加载
        if (lazy) {
            entityList = this.list(new QueryWrapper<AdminSmMenuEntity>()
                    .select("menu_id", "menu_name", "up_menu_id", "menu_order", "i18n_key")
                    .eq(!StringUtils.isEmpty(sysId), "sys_id", sysId)
                    .eq(!StringUtils.isEmpty(menuId), "up_menu_id", menuId)
                    .orderByAsc("menu_order")
            );
        } else {
            //菜单树全查询
            List<MenuVo> menuVos = this.baseMapper.queryMenuTreeBySysId(sysId);
            List<MenuVo> menuVosResContrs = this.baseMapper.queryMenuTreeResContrBySysId(sysId);
            menuVos.addAll(menuVosResContrs);
            return menuVos;
        }
        entityList.forEach(adminSmMenuEntity ->
                menuVoList.add(
                        GenericBuilder.of(MenuVo::new)
                                .with(MenuVo::setI18nKey, adminSmMenuEntity.getI18nKey())
                                .with(MenuVo::setMenuId, adminSmMenuEntity.getMenuId())
                                .with(MenuVo::setMenuName, adminSmMenuEntity.getMenuName())
                                .with(MenuVo::setUpMenuId, adminSmMenuEntity.getUpMenuId())
                                .with(MenuVo::setMenuOrder, adminSmMenuEntity.getMenuOrder())
                                .with(MenuVo::setMenuClassify, adminSmMenuEntity.getMenuClassify())
                                .with(MenuVo::setMenuTip, adminSmMenuEntity.getMenuTip())
                                .build()
                )
        );
        return menuVoList;

    }

    /**
     * 菜单保存,同时删除缓存(写库删缓存，失效模式)
     * 缓存,此处不处理，由 menuandcontro 接口处理
     * 缓存一致性需求高，可以采用：canal订阅binlog，异步更新缓存方式
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public MenuVo saveAdminSmMunu(AdminSmMenuEntity adminSmMenu) {

        Integer menuNameCount = this.baseMapper.selectCount(new QueryWrapper<AdminSmMenuEntity>()
                .eq("menu_classify", adminSmMenu.getMenuClassify())
                .eq(StringUtils.nonEmpty(adminSmMenu.getMenuName()), "menu_name", adminSmMenu.getMenuName())
                .eq("deleted", 0)
        );

        if (menuNameCount > 0) {
            throw BizException.error(null, "50600001",
                    null, "菜单名：" + adminSmMenu.getMenuName() + "已存在！");
        }
        //判断当前菜单的父菜单下，有多少子菜单
        List<AdminSmMenuEntity> adminSmMenuEntityList = this.baseMapper.selectList(new QueryWrapper<AdminSmMenuEntity>()
                .select("menu_id")
                .eq("up_menu_id", adminSmMenu.getUpMenuId())
        );
        String clientId = StringUtils.isEmpty(SessionUtils.getClientId()) ? "test" : SessionUtils.getClientId();
        String userId = SessionUtils.getUserId();
        String menuId = UUID.randomUUID().toString().replace("-", "");
        adminSmMenu.setMenuId(menuId);
        adminSmMenu.setSysId(clientId);
        adminSmMenu.setLastChgDt(DateUtils.getCurrDate());
        adminSmMenu.setLastChgUsr(userId);
        adminSmMenu.setMenuOrder(new AtomicInteger(5 * (adminSmMenuEntityList.size() + 1)).intValue());
        adminSmMenu.setDeleted(0);
        this.baseMapper.insert(adminSmMenu);
        //新增授权记录表
        adminSmAuthRecoService.getBaseMapper().insert(
                GenericBuilder.of(AdminSmAuthRecoEntity::new)
                        .with(AdminSmAuthRecoEntity::setAuthRecoId, StringUtils.getUUID())
                        .with(AdminSmAuthRecoEntity::setSysId, Optional.ofNullable(SessionUtils.getClientId()).orElse("test"))
                        .with(AdminSmAuthRecoEntity::setAuthobjType, "R")
                        .with(AdminSmAuthRecoEntity::setAuthobjId, "cc81a8d86f274c81bc680a0bbd27e358")
                        .with(AdminSmAuthRecoEntity::setAuthresType, "M")
                        .with(AdminSmAuthRecoEntity::setAuthresId, adminSmMenu.getMenuId())
                        .with(AdminSmAuthRecoEntity::setMenuId, adminSmMenu.getMenuId())
                        .build()
        );
        return GenericBuilder.of(MenuVo::new).with(MenuVo::setMenuId, menuId).build();
    }

    /**
     * 修改 菜单,同时删除缓存(写库删缓存，失效模式)
     *
     * @param adminSmMenu entity
     * @return boolean
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void updateMenuById(AdminSmMenuEntity adminSmMenu) {

        Integer menuNameCount = this.baseMapper.selectCount(new QueryWrapper<AdminSmMenuEntity>()
                .eq("menu_classify", adminSmMenu.getMenuClassify())
                .eq(StringUtils.nonEmpty(adminSmMenu.getMenuName()), "menu_name", adminSmMenu.getMenuName())
                .ne(StringUtils.nonEmpty(adminSmMenu.getMenuId()), "menu_id", adminSmMenu.getMenuId())
        );
        if (menuNameCount > 0) {
            throw BizException.error(null, "50600001",
                    null, "菜单名:" + adminSmMenu.getMenuName() + "已存在！");
        }

        String userId = SessionUtils.getUserId();
        adminSmMenu.setLastChgDt(DateUtils.getCurrDate());
        adminSmMenu.setLastChgUsr(userId);
        this.baseMapper.updateById(adminSmMenu);

    }

    /**
     * 向下递归 menu菜单，删除菜单使用
     *
     * @param hset
     * @param pid
     * @param allMenus
     * @return 返回菜单id
     */
    private void getDownMenuChrildren(HashSet hset, String pid, List<AdminSmMenuEntity> allMenus) {
        List<AdminSmMenuEntity> childs = allMenus.stream().filter(menu -> menu.getUpMenuId().equals(pid)).collect(Collectors.toList());
        childs.stream().forEach(clild -> {
            hset.add(clild.getMenuId());
            getDownMenuChrildren(hset, clild.getMenuId(), allMenus);
        });
    }

    /**
     * 菜单节点信息查询
     */
    @Override
    public MenuVo getMenuInfo(String menuId) {

        return this.baseMapper.getMenuInfoByMenuId(menuId);
    }

    /**
     * 树的查询
     *
     * @param sysId    系统id, 默认test
     * @param pid      菜单父id字段
     * @param treeDeep 树递归深度
     * @return
     */
    @Override
    public List<MenuVo> queryTreeMenuList(String sysId, String pid, String treeDeep) {

        AtomicInteger atomicInteger = new AtomicInteger(0);

        //参数判断
        if (StringUtils.isEmpty(sysId)) {
            sysId = StringUtils.isEmpty(SessionUtils.getClientId()) ? "test" : SessionUtils.getClientId();
        }
        if (StringUtils.isEmpty(pid)) {
            pid = "-1";
        }
        //树的递归深度，默认树递归深度5级
        int treeDeepInt = !("-1").equals(treeDeep) ? 5 : Integer.MAX_VALUE;

        List<AdminSmMenuEntity> adminSmMenuList = this.baseMapper.selectList(new QueryWrapper<AdminSmMenuEntity>()
                .select("menu_id", "func_id", "up_menu_id", "menu_name", "menu_order", "menu_icon",
                        "menu_tip", "i18n_key", "menu_classify")
                .eq("sys_id", sysId));
        //如果查询菜单list为空
        if (CollectionUtils.isEmpty(adminSmMenuList)) {
            return null;
        }
        List<MenuVo> menuVoList = getChildrens(pid, adminSmMenuList, treeDeepInt, atomicInteger);
        return menuVoList;
    }

    /**
     * 递归查找传入菜单的子菜单，拼装成子菜单树结构
     * list-->tree
     *
     * @return 子菜单树
     */
    private List<MenuVo> getChildrens(String pid, List<AdminSmMenuEntity> all, int treeDeep, AtomicInteger atomicInteger) {

        int cirVar = atomicInteger.incrementAndGet();

        List<MenuVo> children = all.parallelStream()
                .filter(mevuEntity -> !StringUtils.isEmpty(mevuEntity.getUpMenuId())
                        && mevuEntity.getUpMenuId().equals(pid) && cirVar < treeDeep)
                .map(menuEntity -> {
                    // 1. 递归查找子菜单
                    return GenericBuilder.of(MenuVo::new)
                            .with(MenuVo::setMenuId, menuEntity.getMenuId())
                            .with(MenuVo::setUpMenuId, menuEntity.getUpMenuId())
                            .with(MenuVo::setMenuName, menuEntity.getMenuName())
                            .with(MenuVo::setMenuOrder, menuEntity.getMenuOrder())
                            .with(MenuVo::setFuncId, menuEntity.getFuncId())
                            .with(MenuVo::setMenuIcon, menuEntity.getMenuIcon())
                            .with(MenuVo::setMenuTip, menuEntity.getMenuTip())
                            .with(MenuVo::setMenuClassify, menuEntity.getMenuClassify())
                            .with(MenuVo::setI18nKey, menuEntity.getI18nKey())
                            .with(MenuVo::setChildrenList, getChildrens(menuEntity.getMenuId(), all, treeDeep, atomicInteger))
                            .build();
                })
                .sorted(Comparator.comparing(MenuVo::getMenuOrder))
                .collect(Collectors.toList());

        return children;
    }

    /**
     * 搜索菜单树
     *
     * @param sysId    默认test
     * @param menuName 菜单名称
     * @return
     */
    @Override
    public List<MenuVo> querySearchTree(String sysId, String menuName) {

        //参数判断
        if (StringUtils.isEmpty(sysId)) {
            sysId = StringUtils.isEmpty(SessionUtils.getClientId()) ? "test" : SessionUtils.getClientId();
        }
        //菜单列表
        List<AdminSmMenuEntity> allSmMenuList = this.baseMapper.selectList(new QueryWrapper<AdminSmMenuEntity>()
                .select("menu_id", "func_id", "up_menu_id", "menu_name", "menu_order", "menu_icon",
                        "menu_tip", "i18n_key", "menu_classify")
                .eq("sys_id", sysId));
        List<Object> filterMenuIdList = this.baseMapper.selectObjs(new QueryWrapper<AdminSmMenuEntity>()
                .select("menu_id")
                .eq("sys_id", sysId)
                .like("menu_name", menuName));
        //拼装搜索树
        List<MenuVo> menuVoList = querySearchTreeByMenuName(allSmMenuList, filterMenuIdList);

        return menuVoList;
    }

    /**
     * 搜索菜单树,list-->tree
     *
     * @return
     */
    private List<MenuVo> querySearchTreeByMenuName(List<AdminSmMenuEntity> allMenuList, List<Object> filterMenuList) {

        List<AdminSmMenuEntity> menulist = new ArrayList<>();
        ArrayList<AdminSmMenuEntity> objects = new ArrayList<>(filterMenuList.size());
        filterMenuList.parallelStream().forEach(menuStr -> {
            menulist.addAll(getUpMenuTree(objects, (String) menuStr, allMenuList));
        });
        //去重
        List<AdminSmMenuEntity> menuDistinctlist = menulist.parallelStream().distinct().collect(Collectors.toList());
        //list to tree
        List<MenuVo> menuVoList = getChildrens("-1", menuDistinctlist, Integer.MAX_VALUE, new AtomicInteger(0));

        return menuVoList;
    }

    /**
     * 搜索菜单树-查询父菜单list
     *
     * @param menuId
     * @param allMenus
     * @return
     */
    private List<AdminSmMenuEntity> getUpMenuTree(ArrayList<AdminSmMenuEntity> menus, String menuId,
                                                  List<AdminSmMenuEntity> allMenus) {

        allMenus.stream().filter(menu -> menu.getMenuId().equals(menuId))
                .forEach(menuEntity -> {
                    menus.add(GenericBuilder.of(AdminSmMenuEntity::new)
                            .with(AdminSmMenuEntity::setMenuId, menuEntity.getMenuId())
                            .with(AdminSmMenuEntity::setUpMenuId, menuEntity.getUpMenuId())
                            .with(AdminSmMenuEntity::setMenuName, menuEntity.getMenuName())
                            .with(AdminSmMenuEntity::setMenuOrder, menuEntity.getMenuOrder())
                            .with(AdminSmMenuEntity::setFuncId, menuEntity.getFuncId())
                            .with(AdminSmMenuEntity::setMenuIcon, menuEntity.getMenuIcon())
                            .with(AdminSmMenuEntity::setMenuTip, menuEntity.getMenuTip())
                            .with(AdminSmMenuEntity::setMenuClassify, menuEntity.getMenuClassify())
                            .with(AdminSmMenuEntity::setI18nKey, menuEntity.getI18nKey())
                            .build());
                    //递归查询父菜单
                    getUpMenuTree(menus, menuEntity.getUpMenuId(), allMenus);
                });

        return menus;
    }

    /**
     * 修改菜单树,前端返回menu tree,不再使用此方法
     * 待优化点：
     * 1）整个菜单树修改保存，耗时 8s
     * 2) mp updateBatchById 是一条一条更新数据库
     *
     * @param menuVoList
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public int updateAllMenuTree(List<MenuVo> menuVoList) {

        AtomicInteger atomicInteger = new AtomicInteger(0);
        LinkedList<AdminSmMenuEntity> adminSmMenuList = new LinkedList<>();

        menuVoList.stream().forEach(menuVo -> {
            atomicInteger.getAndAdd(5);
            adminSmMenuList.addAll(getChildrenList(adminSmMenuList, menuVo, atomicInteger));
        });
        //去重
        List<AdminSmMenuEntity> menuList = adminSmMenuList.parallelStream().distinct().collect(Collectors.toList());
        // mp提供的批量保存或者更新是 遍历集合一条条保存或者更新记录，数据比较少时推荐此方法
//        boolean batchById = this.updateBatchById(menuList);
        //批量更新
        int batchMenuTree = this.baseMapper.updateBatchMenuTree(menuList);
        if (batchMenuTree > 0) {
            return 1;
        }
        return 0;
    }

    /**
     * 创建权限管理树
     *
     * @param id
     * @return
     */
    @Override
    public List<AdminSmAuthTreeVo> selectAuthTree(String id) {
        List<AdminSmAuthTreeVo> authTreeVoList = this.baseMapper.selectAuthTree(id);
        return authTreeVoList;
    }

    /**
     * 菜单 tree->list
     *
     * @return menu list
     */
    private List<AdminSmMenuEntity> getChildrenList(List<AdminSmMenuEntity> adminSmMenuList,
                                                    MenuVo menuVo, AtomicInteger atomicInteger) {

        //添加menu
        adminSmMenuList.add(GenericBuilder.of(AdminSmMenuEntity::new)
                .with(AdminSmMenuEntity::setMenuId, menuVo.getMenuId())
                .with(AdminSmMenuEntity::setSysId,
                        StringUtils.isEmpty(SessionUtils.getClientId()) ? "test" : SessionUtils.getClientId())
                .with(AdminSmMenuEntity::setUpMenuId, menuVo.getUpMenuId())
                .with(AdminSmMenuEntity::setMenuOrder, atomicInteger.intValue())
                .with(AdminSmMenuEntity::setLastChgDt, DateUtils.getCurrDate())
                .with(AdminSmMenuEntity::setLastChgUsr, SessionUtils.getUserId())
                .build()
        );
        //children list 为null,直接返回menuList
        if (CollectionUtils.isEmpty(menuVo.getChildrenList())) {
            return null;
        }
        //children list不为null,递归查询
        List<MenuVo> menuChildrenList = menuVo.getChildrenList();
        AtomicInteger atomicInt = new AtomicInteger(0);
        menuChildrenList.stream().forEach(menuVo1 -> {
            atomicInt.getAndAdd(5);
            getChildrenList(adminSmMenuList, menuVo1, atomicInt);
        });

        return adminSmMenuList;
    }

    /**
     * 更新菜单指定位置，菜单树可拖动
     *
     * @param dragMenuBo 前端传指定 拖动的menuId 参照的menuId 相对于参照点的前后顺序
     * @return
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public int updateMenuTreeByDragMenu(DragMenuBo dragMenuBo) {
        //查询拖动菜单
        List<String> menuIds = Arrays.asList(dragMenuBo.getDragMenuId(), dragMenuBo.getRefMenuId());
        List<AdminSmMenuEntity> dragMenuList = this.baseMapper.selectBatchIds(menuIds);
        if (dragMenuList.size() != 2) {
            return -1;
        }
        //菜单对象赋值
        AdminSmMenuEntity dragMenu;
        AdminSmMenuEntity refMenu;
        if (dragMenuList.get(0).getMenuId().matches(dragMenuBo.getDragMenuId())) {
            dragMenu = dragMenuList.get(0);
            refMenu = dragMenuList.get(1);
        } else {
            dragMenu = dragMenuList.get(1);
            refMenu = dragMenuList.get(0);

        }
        //查询拖动父菜单下的所有菜单
        String upMenuId = dragMenuList.get(0).getUpMenuId();
        List<AdminSmMenuEntity> menuList = this.baseMapper.selectList(new QueryWrapper<AdminSmMenuEntity>()
                .in(StringUtils.nonEmpty(upMenuId), "up_menu_id", upMenuId));
        List<AdminSmMenuEntity> sortedMenus = this.sortDragMenuList(dragMenu, refMenu, menuList, dragMenuBo);
        //批量修改
        int updateBatch = this.baseMapper.updateBatchMenuTree(sortedMenus);
        if (updateBatch > 0) {
            return 1;
        }
        return -1;
    }

    /**
     * 查询控制点关联菜单列表
     *
     * @param form
     * @return
     */
    @Override
    public Page<AdminSmMenuVo> getMenuListForContr(AdminSmMenuConditionForm form) {
        String keyWord = form.getKeyWord();
        Page<AdminSmMenuVo> page = new Page<>(form.getPage(), form.getSize());
        QueryWrapper<AdminSmMenuVo> wrapper = new QueryWrapper<>();
        wrapper.eq(form.getCheck() == 1, "m1.menu_id", form.getLastMenuId());
        wrapper.eq("m1.menu_classify", "0");
        wrapper.ne("m1.deleted", 1);
        wrapper.and(!StringUtils.isEmpty(keyWord) && !(form.getCheck() == 1),
                w -> w.like("m1.menu_name", keyWord).or().like("m2.menu_name", keyWord));
        return this.baseMapper.getMenuListForContr(page, wrapper, form.getLastMenuId());
    }

    /**
     * 批量删除菜单
     *
     * @param ids
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public int removeMenuByIds(String[] ids) {
        if (CollectionUtils.nonEmpty(ids)) {
            String sysId = StringUtils.isEmpty(SessionUtils.getClientId()) ? "test" : SessionUtils.getClientId();
            //查询菜单表中所有菜单
            List<AdminSmMenuEntity> allMenuList = this.baseMapper.selectList(new QueryWrapper<AdminSmMenuEntity>()
                    .select("menu_id", "func_id", "up_menu_id")
                    .eq("sys_id", sysId));
            //获取要删除菜单
            List<AdminSmMenuEntity> selMenuList = allMenuList.stream().filter(menu ->
                    Arrays.asList(ids).contains(menu.getMenuId())
            ).collect(Collectors.toList());
            if (CollectionUtils.isEmpty(selMenuList)) {
                throw BizException.error(null, "50600002",
                        null, "未查询到菜单，所选菜单或已被删除，请检查！");
            }
            //根据menuId向下递归查询list
            HashSet<String> delMenuIdHash = new HashSet<>();
            int deleteBatchIds = 0;
            int authresId = 0;
            selMenuList.stream()
                    .forEach(menu -> {
                        delMenuIdHash.add(menu.getMenuId());
                        getDownMenuChrildren(delMenuIdHash, menu.getMenuId(), allMenuList);
                    });
            //批量删除菜单
            deleteBatchIds = this.baseMapper.deleteBatchIds(delMenuIdHash);
            // 批量删除菜单相关授权记录
            authresId = adminSmAuthRecoService.deleteMenuAuthData(delMenuIdHash);
            if (deleteBatchIds > 0 && authresId > 0) {
                return 1;
            }
            return 0;
        }
        return 0;
    }

    private List<AdminSmMenuEntity> sortDragMenuList(AdminSmMenuEntity dragMenu, AdminSmMenuEntity refMenu,
                                                     List<AdminSmMenuEntity> menuList, DragMenuBo dragMenuBo) {

        //删除拖动的菜单
        menuList.remove(dragMenu);
        //排序
        List<AdminSmMenuEntity> sortedMenus = menuList.parallelStream().sorted(Comparator.comparing(AdminSmMenuEntity::getMenuOrder))
                .collect(Collectors.toList());
        //确定指定元素的索引
        int indexRefMenuOfList = sortedMenus.indexOf(refMenu);
        if (indexRefMenuOfList == -1) {
            return null;
        }
        // 参照元素 前插入
        if ("before".equals(dragMenuBo.getMenuOrder())) {
            sortedMenus.add(indexRefMenuOfList, dragMenu);
        } else {
            //参照元素 后插入
            sortedMenus.add(indexRefMenuOfList + 1, dragMenu);
        }
        AtomicInteger atomicInt = new AtomicInteger(0);
        //重新赋值 menu order
        List<AdminSmMenuEntity> collect = sortedMenus.stream().map(menu -> {
            menu.setLastChgUsr(SessionUtils.getUserId());
            menu.setMenuOrder(atomicInt.getAndAdd(5));
            menu.setLastChgDt(DateUtils.getCurrDate());
            return menu;
        }).collect(Collectors.toList());
        return collect;
    }

}