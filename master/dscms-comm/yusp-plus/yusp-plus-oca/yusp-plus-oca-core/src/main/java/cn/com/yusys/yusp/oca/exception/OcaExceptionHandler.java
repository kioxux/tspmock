package cn.com.yusys.yusp.oca.exception;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
@Slf4j
@RestControllerAdvice
@Order(value = Integer.MIN_VALUE)
@RequiredArgsConstructor
public class OcaExceptionHandler {

    private final StringRedisTemplate stringRedisTemplate;

    /**
     * 处理业务异常
     */
    @ExceptionHandler(BizException.class)
    public ResultDto<Object> handleBizException(BizException e) {
        log.error("统一异常处理：BizException, 异常原因：", e);
        String errorCode = e.getErrorCode();
        String language= LocaleContextHolder.getLocale().toString().toLowerCase();
        String message = (String) stringRedisTemplate.opsForHash().get("sysMessage_"+language,errorCode);
        if (StringUtils.isEmpty(message)) {
            //重新加载缓存
            message = (String) stringRedisTemplate.opsForHash().get("sysMessage_"+language,errorCode);
        }
        if (StringUtils.isEmpty(message)) {
            message = e.getMessage();
        }
        return StringUtils.isEmpty(e.getErrorCode()) ? ResultDto.error(message) : ResultDto.error(e.getErrorCode(), message);
    }

}
