package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 自动任务队列表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-12-02 18:03:52
 */
@Data
@TableName("cms_queue_task")
public class QueueTaskEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 队列任务id
	 */
	@TableId
	private String queueTaskId;
	/**
	 * 申请流水号
	 */
	private String applyId;
	/**
	 * 客户姓名
	 */
	private String cusName;
	/**
	 * 证件号码
	 */
	private String certCode;
	/**
	 * 证件类型(10110:居民身份证,10119:组织机构代码,20150:护照,99099:其他证件)
	 */
	private String certType;
	/**
	 * 执行类型01-自动，02-人工
	 */
	private String exeType;
	/**
	 * 入队时间
	 */
	private Date inTime;
	/**
	 * 开始执行时间
	 */
	private Date exeTime;
	/**
	 * 出队时间
	 */
	private Date outTime;
	/**
	 * 队列状态000-待执行；010-执行中；100-执行成功；101-执行失败；
	 */
	private String queueStatus;
	/**
	 * 错误信息
	 */
	private String errorMsg;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 自动任务队列显示状态(1-逻辑有效，0-逻辑删除)
	 */
	private String logicStatus;

}
