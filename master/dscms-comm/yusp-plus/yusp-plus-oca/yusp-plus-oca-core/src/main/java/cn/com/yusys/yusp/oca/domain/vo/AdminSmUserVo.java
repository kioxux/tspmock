package cn.com.yusys.yusp.oca.domain.vo;

import cn.com.yusys.yusp.oca.config.annotation.RedisDictTranslator;
import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.domain.constants.Constants;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author danyu
 */
@Data
public class AdminSmUserVo{

    /**
     * 记录编号
     */
    private String userId;
    /**
     * 账号
     */
    private String loginCode;
    /**
     * 姓名
     */
    private String userName;
    /**
     * 员工号
     */
    private String userCode;
    /**
     * 性别
     */
    private String userSex;
    /**
     * 证件类型
     */
    private String certType;
    /**
     * 证件号码
     */
    private String certNo;
    /**
     * 角色
     */
    private String roleCode;

    /**
     * 所属机构
     */
    @RedisDictTranslator(redisCacheKey= Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_ORG_NAME,fieldName = "orgName")
    private String orgId;

    /**
     * 所属主机构
     */
    private String belgMainOrgId;

    /**
     * 所属主机构名称
     */
    private String belgMainOrgName;

    /**
     * 所属部门
     */
    @RedisDictTranslator(redisCacheKey= Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_DPT_NAME,fieldName = "dptName")
    private String dptId;

    /**
     * 状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效
     */
    private AvailableStateEnum userSts;

    private String userAvatar;
    /**
     * 生日
     */
    private String userBirthday;
    /**
     * 邮箱
     */
    private String userEmail;
    /**
     * 移动电话
     */
    private String userMobilephone;
    /**
     * 办公电话
     */
    private String userOfficetel;

    /**
     * 最近登录时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastLoginTime;

    /**
     * 最新变更用户
     */
    @RedisDictTranslator(redisCacheKey= Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_USER_NAME,fieldName = "lastChgName")
    private String lastChgUsr;

    /**
     * 最新变更时间
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;
    /**
     * 有效期到
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date deadline;

}
