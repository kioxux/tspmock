package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmAuthRecoEntity;
import cn.com.yusys.yusp.oca.domain.form.AdminSmAuthTreeForm;
import cn.com.yusys.yusp.oca.domain.form.AdminSmAuthUpdateForm;
import cn.com.yusys.yusp.oca.domain.form.AdminTmplAuthForm;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmAuthRecoVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmAuthTreeVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmResContrAuthVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminTmplAndRecoVo;
import cn.com.yusys.yusp.oca.service.AdminSmAuthRecoService;
import cn.com.yusys.yusp.oca.service.AdminSmAuthorizationService;
import cn.com.yusys.yusp.oca.service.AdminSmDataAuthTmplService;
import cn.com.yusys.yusp.oca.service.AdminSmResContrService;
import cn.com.yusys.yusp.oca.service.cache.AuthTreeCache;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

@Slf4j
@Service("adminSmAuthorizationService")
public class AdminSmAuthorizationServiceImpl implements AdminSmAuthorizationService {

    @Autowired
    private AuthTreeCache treeCache;
    @Autowired
    private AdminSmAuthRecoService adminSmAuthRecoService;
    @Autowired
    private AdminSmDataAuthTmplService adminSmDataAuthTmplService;
    @Autowired
    private AdminSmResContrService adminSmResContrService;


    /**
     * 修改被授权人权限
     *
     * @param adminSmAuthUpdateForm
     */
    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void saveAuth(AdminSmAuthUpdateForm adminSmAuthUpdateForm) {
        /**
         * authObjId：被授权人Id
         * authFormList：被授权人新权限
         */
        List<AdminSmAuthTreeVo> authFormList = adminSmAuthUpdateForm.getAuthFormList();
        if (authFormList == null || authFormList.size() == 0) {
            return;
        }
        /**
         * 按照 state 属性分组：
         * state为 0 表示之前的权限被取消，state为 1 表示新增加的授权获取
         */
        Map<Integer, List<AdminSmAuthTreeVo>> authGroup = authFormList.stream().collect(Collectors.groupingBy(form -> form.getState(), Collectors.toList()));
        Set<AdminSmAuthRecoEntity> entitySet = new HashSet<>();
        List<AdminSmAuthTreeVo> deleteAuthList = authGroup.get(0);

        /**
         * 批量删除授权记录
         */
        /**
         * 批量删除授权记录
         */
        if (deleteAuthList != null && deleteAuthList.size() != 0) {
            /*List<String> authresIdList = deleteAuthList.stream().map(AdminSmAuthTreeVo::getAuthRecoId).collect(Collectors.toList());
            adminSmAuthRecoService.deleteByList(authresIdList);*/
            final String sysId = SessionUtils.getClientId();
            List<AdminSmAuthRecoVo> vos = deleteAuthList.stream().map(
                    auth  ->  {
                        AdminSmAuthRecoVo vo = new AdminSmAuthRecoVo();
                        vo.setSysId(sysId);
                        vo.setAuthresId(auth.getAuthresId());
                        vo.setAuthobjId(adminSmAuthUpdateForm.getAuthObjId());
                        vo.setAuthobjType(adminSmAuthUpdateForm.getAuthObjType());
                        return vo;
                    }
            ).collect(Collectors.toList());
            adminSmAuthRecoService.batchDelete(vos);
        }

        entitySet.clear();
        /**
         * insertAuthList：新增授权
         */
        List<AdminSmAuthTreeVo> insertAuthList = authGroup.get(1);
        if (insertAuthList == null || insertAuthList.size() == 0) {
            return;
        }
        insertAuthList.forEach(vo -> {
            AdminSmAuthRecoEntity saveEntity = getAdminSmAuthRecoEntity(vo, adminSmAuthUpdateForm);
            entitySet.add(saveEntity);
        });
        /**
         * 批量保存新增授权
         */
        adminSmAuthRecoService.saveBatch(entitySet);
    }

    /**
     * 复制拷贝权限
     *
     * @param adminSmAuthUpdateForm
     */
    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void copyAuth(AdminSmAuthUpdateForm adminSmAuthUpdateForm) {
        String copyForAuthObjIds = adminSmAuthUpdateForm.getCopyForAuthObjIds();
        if (StringUtils.isEmpty(copyForAuthObjIds) || adminSmAuthUpdateForm.getAuthFormList() == null) {
            return;
        }
        String[] ids = copyForAuthObjIds.split(",");
        /**
         * 删除被拷贝角色或用户原有的权限
         */
        adminSmAuthRecoService.deleteByAuthObjIds(ids);
        /**
         * 将数据转换为 entity 集合
         */
        Set<AdminSmAuthRecoEntity> entityList = new HashSet<>();
        List<AdminSmAuthTreeVo> authFormList = adminSmAuthUpdateForm.getAuthFormList();
        authFormList.forEach(treeVo -> {
            /**
             * 循环 ids，添加不同 authresId 的 entity 到新增列表中
             */
            for (int i = 0; i < ids.length; i++) {
                AdminSmAuthRecoEntity entity = getAdminSmAuthRecoEntity(treeVo, adminSmAuthUpdateForm);
                /**
                 * 如果是权限模板，menuId为上级菜单id，不是上级控制点id
                 */
                if ("D".equals(entity.getAuthresType())) {
                    AdminSmAuthTreeVo vo = new AdminSmAuthTreeVo();
                    vo.setAuthresId(treeVo.getUpTreeId());
                    entity.setMenuId(authFormList.get(authFormList.indexOf(vo)).getUpTreeId());
                }
                entity.setAuthRecoId(StringUtils.getUUID());
                entity.setAuthobjId(ids[i]);
                entityList.add(entity);
            }
        });
        /**
         * 批量保存复制拷贝授权
         */
        boolean b = adminSmAuthRecoService.saveBatch(entityList);
    }

    /**
     * 获取数据权限模板授权页面
     *
     * @param form
     */
    @Override
    public IPage<AdminSmResContrAuthVo> getAuthTmplList(AdminSmAuthTreeForm form) {
        String authObjId = form.getAuthObjId();
        /**
         * 1、分页查询被授权用户的控制点
         */
        IPage<AdminSmAuthTreeVo> resContrPage = adminSmResContrService.selectAuthTreePage(authObjId, form.getKeyWord(), form.getPage(), form.getSize());
        log.info("数据权限模板获取到的权限树数据: {}", resContrPage.getRecords());
        IPage<AdminSmResContrAuthVo> voPage = new Page<>(Long.valueOf(form.getPage()), Long.valueOf(form.getSize()));
        if (resContrPage.getRecords() != null && resContrPage.getRecords().size() > 0) {
            /**
             * 2、组装返回数据
             */
            voPage.setRecords(getResContrAuthVoList(authObjId, resContrPage.getRecords()));
            return voPage.setTotal(resContrPage.getTotal());
        }
        return voPage;
    }

    /**
     * 给被授权对象修改控制点的数据模板
     *
     * @param form
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveTmplAuth(AdminTmplAuthForm form) {

        if (!StringUtils.isEmpty(form.getLastAuthresId())) {
            /**
             * 1、删除之前的数据模板授权记录
             */
            adminSmAuthRecoService.removeByMenuIdAndAuthobjId(form);
        }
        /**
         * 2、新增授权记录
         */
        if (!StringUtils.isEmpty(form.getAuthresId())) {
//            AdminSmAuthRecoEntity menuIdEntity = adminSmAuthRecoService.getByAuthresId(form.getContrId());
            AdminSmAuthRecoEntity authRecoEntity = BeanUtils.beanCopy(form, AdminSmAuthRecoEntity.class);
            log.info("2、新增授权记录，authRecoEntity：{}", authRecoEntity);
            authRecoEntity.setAuthRecoId(StringUtils.getUUID());
//            authRecoEntity.setMenuId(menuIdEntity.getMenuId());
            authRecoEntity.setMenuId(form.getContrId());
            authRecoEntity.setAuthresType("D");
            authRecoEntity.setLastChgDt(new Date());
            authRecoEntity.setLastChgUsr(SessionUtils.getUserId());
            authRecoEntity.setSysId(SessionUtils.getClientId());
            adminSmAuthRecoService.save(authRecoEntity);
        }
    }

    /**
     * 组装数据模板授权页面的数据
     *
     * @param authObjId
     * @param authContrData
     * @return
     */
    private List<AdminSmResContrAuthVo> getResContrAuthVoList(String authObjId, List<AdminSmAuthTreeVo> authContrData) {
        List<AdminSmResContrAuthVo> resContrAuthVoList = new ArrayList<>();
        /**
         * 1、获取已授权菜单
         */
        List<AdminSmAuthTreeVo> authMenuData = treeCache.getUserAuthMenuData(authObjId, false);
        /**
         * 2、获取已授权的数据模板
         */
        List<String> contrIdList = authContrData.stream().map(AdminSmAuthTreeVo::getAuthresId).collect(Collectors.toList());
        List<AdminTmplAndRecoVo> tmplAndRecoVoList = adminSmDataAuthTmplService.getTmplAndRecoVoList(contrIdList, authObjId);
        log.info("2、获取已授权的数据模板 : {}", tmplAndRecoVoList);
        /**
         * 5、拼装返回数据
         */
        authContrData.forEach(resContr -> {
            AdminSmResContrAuthVo resContrAuthVo = new AdminSmResContrAuthVo();
            resContrAuthVo.setResContrId(resContr.getAuthresId());
            resContrAuthVo.setUpMenuId(resContr.getUpTreeId());
            List<String> pathList = new ArrayList<>();
            pathList.add(resContr.getNodeName());
            assemblyResContr(resContrAuthVo, authMenuData, pathList);
            /**
             * 6、拼装 authTmplVoList 数据
             */
            if (tmplAndRecoVoList != null && tmplAndRecoVoList.size() > 0) {
                for (AdminTmplAndRecoVo vo : tmplAndRecoVoList) {
                    log.info("vo.getContrId() : {}", vo.getContrId());
                    log.info("resContrAuthVo.getResContrId() : {}", resContrAuthVo.getResContrId());
                    if (vo.getContrId().equals(resContrAuthVo.getResContrId())) {
                        resContrAuthVo.setTmplAndRecoVo(vo);
                    }
                }
            }
            resContrAuthVoList.add(resContrAuthVo);
        });

        log.info("7、组装数据模板授权页面的数据 :");
        return resContrAuthVoList;
    }

    /**
     * 拼装 AdminSmResContrAuthVo 返回对象
     *
     * @param resContrAuthVo
     * @param authMenuData
     */
    private void assemblyResContr(AdminSmResContrAuthVo resContrAuthVo, List<AdminSmAuthTreeVo> authMenuData, List<String> pathList) {

        Map<String, AdminSmAuthTreeVo> menuDataMap = authMenuData.stream().collect(Collectors.toMap(AdminSmAuthTreeVo::getAuthresId, vo -> vo));

        AdminSmAuthTreeVo authTreeVo = null;

        String errorMsg = "控制点相关菜单已被删除或未授权！\n({})";

        while (!"0".equals(resContrAuthVo.getUpMenuId())) {
            if (!menuDataMap.containsKey(resContrAuthVo.getUpMenuId())) {
                String replace = errorMsg.replace("{}", "【控制点id=" + resContrAuthVo.getResContrId() + "，菜单id=" + resContrAuthVo.getUpMenuId() + "】");
                resContrAuthVo.setMenuPath(replace);
                break;
            } else {
                authTreeVo = menuDataMap.get(resContrAuthVo.getUpMenuId());
                resContrAuthVo.setUpMenuId(authTreeVo.getUpTreeId());
                pathList.add(authTreeVo.getNodeName() + " / ");
            }
            if ("0".equals(resContrAuthVo.getUpMenuId())) {
                Collections.reverse(pathList);
                StringBuffer stringBuffer = new StringBuffer();
                for (String path : pathList) {
                    stringBuffer.append(path);
                }
                resContrAuthVo.setMenuPath(stringBuffer.toString());
                log.info("方法：assemblyResContr 中拼装的结果：{}", resContrAuthVo);
            }
        }
    }

    private AdminSmAuthRecoEntity getAdminSmAuthRecoEntity(AdminSmAuthTreeVo vo, AdminSmAuthUpdateForm form) {
        AdminSmAuthRecoEntity entity = new AdminSmAuthRecoEntity();
        if (vo.getState() == 1) {
            entity.setAuthRecoId(StringUtils.getUUID());
        } else {
            entity.setAuthRecoId(vo.getAuthRecoId());
        }
        String sysId = StringUtils.isEmpty(SessionUtils.getClientId()) ? "test" : SessionUtils.getClientId();
        entity.setSysId(sysId);
        entity.setLastChgUsr(SessionUtils.getUserId());
        entity.setAuthresType(vo.getAuthresType());
        entity.setAuthresId(vo.getAuthresId());
        entity.setAuthobjType(form.getAuthObjType());
        entity.setAuthobjId(form.getAuthObjId());
        entity.setMenuId(vo.getUpTreeId());
        return entity;
    }

    @Override
    public List<AdminSmAuthTreeVo> getTreeQuery(AdminSmAuthTreeForm adminSmAuthTreeForm) {
        String userId = SessionUtils.getUserId();
        String authObjId = adminSmAuthTreeForm.getAuthObjId();
        String userRoleId = adminSmAuthTreeForm.getUserRoleId();
        /**
         * 获取授权人能够授权的所有数据
         */
        List<List<AdminSmAuthTreeVo>> userData = getTreeData(false, userId, userRoleId);
        /**
         * 获取被授权人已经授权的对象
         */
        List<List<AdminSmAuthTreeVo>> authorizedData = getTreeData(false, authObjId);
        /**
         * 组装树结构
         */
        List<AdminSmAuthTreeVo> treeList = this.composeTreeData(userData, authorizedData);
        /**
         * 菜单排序
         */
        return treeList;
    }

    /**
     * 树结构组装前期准备
     *
     * @param userData
     * @param authData
     * @return
     */
    private List<AdminSmAuthTreeVo> composeTreeData(List<List<AdminSmAuthTreeVo>> userData, List<List<AdminSmAuthTreeVo>> authData) {

        List<AdminSmAuthTreeVo> treeVoList = new ArrayList<>();
        /**
         * 整合授权人数据和被授权人数据
         */
        for (int i = 0; i < userData.size(); i++) {
            List<AdminSmAuthTreeVo> userList = userData.get(i);
            List<AdminSmAuthTreeVo> authList = authData.get(i);
            for (int j = 0; j < userList.size(); j++) {
                AdminSmAuthTreeVo vo = userList.get(j);
                // 分组时parentId不能为null.
                if (StringUtils.isEmpty(vo.getUpTreeId())) {
                    vo.setUpTreeId("*");
                }
                /**
                 * 如果已有该权限，将state设置为 1，否则默认为 0
                 */
                if (authList != null && authList.contains(vo)) {
                    vo.setState(1);
                    /**
                     * 修改当前节点的记录id为被授权人的记录id
                     */
                    vo.setAuthRecoId(authList.get(authList.indexOf(vo)).getAuthRecoId());
                }
            }
            treeVoList.addAll(userList);
        }
        /**
         * 将所有的树结构数据按照 upTreeId 进行分组
         */
        Map<String, List<AdminSmAuthTreeVo>> userGroup = treeVoList.stream()
                .collect(Collectors.groupingBy(vo -> vo.getUpTreeId(), Collectors.toList()));
        /**
         * 设置虚构顶级树节点，该树节点 nodeId 为 "0"
         */
        AdminSmAuthTreeVo topVo = new AdminSmAuthTreeVo();
        topVo.setAuthresId("0");
        /**
         * 使用递归进行数据的组装
         */
        topVo = this.composeMenuTreeData(userGroup, topVo);
        /**
         * 树结构排序
         */
        List<AdminSmAuthTreeVo> resultList = treeSort(topVo.getChildren());
        return resultList;
    }

    /**
     * 树结构排序方法
     *
     * @param treeList
     * @return
     */
    private List<AdminSmAuthTreeVo> treeSort(List<AdminSmAuthTreeVo> treeList) {
        if (treeList == null || treeList.size() == 0) {
            return null;
        }
        treeList.forEach(vo -> {
            if (vo.getChildren() != null) {
                List<AdminSmAuthTreeVo> childrenList = treeSort(vo.getChildren());
                vo.setChildren(childrenList);
            }
        });
        if ("M".equals(treeList.get(0).getAuthresType())) {
            treeList.sort(Comparator.comparing(vo -> vo.getOrderColumn()));
        } else {
            treeList.sort(Comparator.comparing(vo -> vo.getAuthresId()));
        }
        return treeList;
    }

    /**
     * 组装授权树结构
     *
     * @param menuGroup
     * @param topVo
     * @return
     */
    private AdminSmAuthTreeVo composeMenuTreeData(Map<String, List<AdminSmAuthTreeVo>> menuGroup, AdminSmAuthTreeVo topVo) {
        /**
         * 如果该节点拥有子节点，遍历所有子节点，设置子节点的 children 属性
         *
         * 如果没有子节点，就返回该节点自己
         */
        if (menuGroup.containsKey(topVo.getAuthresId())) {
            List<AdminSmAuthTreeVo> treeVoList = menuGroup.get(topVo.getAuthresId());
            treeVoList.forEach(vo -> composeMenuTreeData(menuGroup, vo));
            topVo.setChildren(treeVoList);
        }
        return topVo;
    }

    /**
     * 查询树结构数据
     *
     * @param ids
     * @param b
     * @return
     */
    private List<List<AdminSmAuthTreeVo>> getTreeData(boolean b, String... ids) {
        if (ids.length <= 0) {
            return null;
        }

        List<List<AdminSmAuthTreeVo>> list = new ArrayList<>();
        List<AdminSmAuthTreeVo> menuVoList = new ArrayList<>();
        List<AdminSmAuthTreeVo> contrVoList = new ArrayList<>();
        List<AdminSmAuthTreeVo> tmplVoList = new ArrayList<>();

        for (String id : ids) {
            menuVoList.addAll(treeCache.getUserAuthMenuData(id, b));
        }
        menuVoList = menuVoList.stream().collect(collectingAndThen(toCollection(() ->
                new TreeSet<>(Comparator.comparing(AdminSmAuthTreeVo::getAuthresId))), ArrayList::new));
        list.add(menuVoList);

        for (String id : ids) {
            contrVoList.addAll(treeCache.getUserAuthContrData(id, b));
        }
        contrVoList = contrVoList.stream().collect(collectingAndThen(toCollection(() ->
                new TreeSet<>(Comparator.comparing(AdminSmAuthTreeVo::getAuthresId))), ArrayList::new));
        list.add(contrVoList);

        for (String id : ids) {
            tmplVoList.addAll(treeCache.getUserAuthDataTmplData(id, b));
        }
        tmplVoList = tmplVoList.stream().collect(collectingAndThen(toCollection(() ->
                new TreeSet<>(Comparator.comparing(AdminSmAuthTreeVo::getAuthresId))), ArrayList::new));
        list.add(tmplVoList);

        return list;
    }
}