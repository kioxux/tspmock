package cn.com.yusys.yusp.oca.dao;

import cn.com.yusys.yusp.commons.session.user.impl.ControlImpl;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmResContrEntity;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmAuthTreeVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmResContrVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统功能控制点表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-19 16:38:48
 */

public interface AdminSmResContrDao extends BaseMapper<AdminSmResContrEntity> {

    /**
     * 控制点功能列表查询
     *
     * @param page
     * @param wrapper
     * @return
     */
    Page<AdminSmResContrVo> queryPageWithCondition(IPage<AdminSmResContrVo> page, @Param(Constants.WRAPPER) QueryWrapper<AdminSmResContrVo> wrapper);

    List<ControlImpl> getAdminSmContrList(@Param(Constants.WRAPPER) QueryWrapper queryWrapper);

    /**
     * 联合admin_sm_auth_reco表
     * @param id
     * @return
     */
    List<AdminSmAuthTreeVo> selectAuthTree(String id);

    /**
     * 缓存中添加 userId 现有的控制点
     * @param wrapper
     * @return
     */
    List<ControlImpl> selectControlImplList(@Param(Constants.WRAPPER) QueryWrapper<ControlImpl> wrapper);

    /**
     * 分页查询权限表中的控制点数据
     * @param result
     * @param wrapper
     * @return
     */
    IPage<AdminSmAuthTreeVo> selectAuthTreePage(IPage<AdminSmAuthTreeVo> result,@Param(Constants.WRAPPER) QueryWrapper<AdminSmAuthTreeVo> wrapper);


}
