package cn.com.yusys.yusp.message.constant;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

/**
 * 消息接收人类型枚举
 */
@Getter
public enum ReceivedUserTypeEnum {
    /**
     * 状态枚举
     */
    KHJL("1", "客户经理"),
    JKR("2", "借款人");

    @JsonValue
    @EnumValue
    private String code;

    private String message;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    ReceivedUserTypeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

}
