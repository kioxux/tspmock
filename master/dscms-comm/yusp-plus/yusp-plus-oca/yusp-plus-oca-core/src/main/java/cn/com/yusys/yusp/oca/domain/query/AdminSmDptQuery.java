package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.common.query.PageQuery;
import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmDptVo;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 部门信息 分页查询条件
 */
@Data
@NoArgsConstructor
public class AdminSmDptQuery extends PageQuery<AdminSmDptVo> {
    private String orgId;
    private String dptCode;
    private String dptName;
    private AvailableStateEnum dptSts;
    private String keyWord;
}
