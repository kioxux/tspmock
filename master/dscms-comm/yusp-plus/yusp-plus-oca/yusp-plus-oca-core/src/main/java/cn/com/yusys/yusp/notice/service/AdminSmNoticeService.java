package cn.com.yusys.yusp.notice.service;

import cn.com.yusys.yusp.notice.entity.AdminSmNoticeEntity;
import cn.com.yusys.yusp.notice.form.AdminSmNoticeCondition;
import cn.com.yusys.yusp.notice.form.AdminSmNoticeForm;
import cn.com.yusys.yusp.notice.vo.AdminSmNoticeVo;
import cn.com.yusys.yusp.notice.vo.NoticeHomePageVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;

/**
 * 系统公告表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-15 13:04:49
 */
public interface AdminSmNoticeService extends IService<AdminSmNoticeEntity> {
    /**
     * 查询自己能查看的已发布的公告
     * @param adminSmNoticeCondition
     * @return
     * @throws JsonProcessingException
     */
    IPage<NoticeHomePageVo> getViewList(AdminSmNoticeCondition adminSmNoticeCondition);

    /**
     * 查询自己有权限看的未读的公告
     * @return
     */
    List<AdminSmNoticeEntity> getUnreadList();

    /**
     * 查询自己编写的公告
     * @param adminSmNoticeCondition
     * @return
     */
    IPage<AdminSmNoticeEntity> getControlList(AdminSmNoticeCondition adminSmNoticeCondition);

    /**
     * 新增公告
     * @param adminSmNoticeForm
     */
    void createNotice(AdminSmNoticeForm adminSmNoticeForm);

    /**
     * 删除公告
     * @param noticeIds
     * @return
     */
    String deleteNotice(List<String> noticeIds);

    /**
     * 修改公告
     * @param adminSmNoticeForm
     * @return
     */
    String updateNotice(AdminSmNoticeForm adminSmNoticeForm);

    /**
     * 发布公告
     * @param noticeEntities
     * @return
     */
    void pubNotices(List<String> noticeEntities);

    /**
     * 获取公告信息
     * @param noticeId
     * @return
     */
    AdminSmNoticeVo getInfo(String noticeId);
}

