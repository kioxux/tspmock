package cn.com.yusys.yusp.oca.domain.vo;

import lombok.Data;

@Data
public class SaveApplyVo {

    private String applyId;

    private String cusId;
}
