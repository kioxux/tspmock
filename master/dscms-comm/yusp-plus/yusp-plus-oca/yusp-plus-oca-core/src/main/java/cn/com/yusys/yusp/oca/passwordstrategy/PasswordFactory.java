package cn.com.yusys.yusp.oca.passwordstrategy;

import cn.com.yusys.yusp.commons.util.StringUtils;
import com.google.common.collect.Maps;

import java.util.Map;

/**
 * @program: yusp-plus
 * @description: 密码策略工厂
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2021-03-30 17:05
 */
public class PasswordFactory {

    //保存初始化策略
    private static Map<String, Handler> strategyMap = Maps.newHashMap();

    //保存初始化错误码
    private static Map<String, String> errorCodeMap = Maps.newHashMap();

    public static Handler getInvokeStrategy(String name) {
        return strategyMap.get(name);
    }

    public static void register(String name, Handler handler) {
        if (StringUtils.isEmpty(name) || null == handler) {
            return;
        }
        strategyMap.put(name, handler);
    }

    public static String getErrorCode(String name) {
        return errorCodeMap.get(name);
    }

    public static void registerErrorCode(String name, String code) {
        if (StringUtils.isEmpty(name) || null == code) {
            return;
        }
        errorCodeMap.put(name, code);
    }
}
