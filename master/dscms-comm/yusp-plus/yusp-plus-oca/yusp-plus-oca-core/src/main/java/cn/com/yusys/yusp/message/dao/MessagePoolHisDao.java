package cn.com.yusys.yusp.message.dao;

import cn.com.yusys.yusp.message.entity.MessagePoolHisEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 消息池历史
 *
 * @author xiaodg@yusys.com.cn
 */

public interface MessagePoolHisDao extends BaseMapper<MessagePoolHisEntity> {

}
