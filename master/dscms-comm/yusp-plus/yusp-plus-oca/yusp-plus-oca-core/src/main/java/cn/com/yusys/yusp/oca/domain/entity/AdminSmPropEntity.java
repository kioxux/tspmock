package cn.com.yusys.yusp.oca.domain.entity;

import cn.com.yusys.yusp.oca.config.annotation.RedisDictTranslator;
import cn.com.yusys.yusp.oca.domain.constants.Constants;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统参数表
 *
 *
 */
@Data
@TableName("admin_sm_prop")
public class AdminSmPropEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 记录编号
     */
    @TableId
    private String propId;
    /**
     * 属性名
     */
    private String propName;
    /**
     * 属性描述
     */
    private String propDesc;
    /**
     * 属性值
     */
    private String propValue;
    /**
     * 备注
     */
    private String propRemark;
    /**
     * 金融机构编号
     */
    private String instuId;
    /**
     * 最新变更用户
     */
    @RedisDictTranslator(redisCacheKey = Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_USER_NAME,fieldName = "userName")
    @TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
    /**
     * 最新变更时间
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;
}
