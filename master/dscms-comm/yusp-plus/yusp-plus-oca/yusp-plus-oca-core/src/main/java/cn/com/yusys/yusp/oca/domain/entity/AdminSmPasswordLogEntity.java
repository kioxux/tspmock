package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 密码修改记录表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-02 15:07:44
 */
@Data
@TableName("admin_sm_password_log")
public class AdminSmPasswordLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 记录编号
	 */
	@TableId(type = IdType.UUID)
	private String logId;
	/**
	 * 密码修改时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date pwdUpTime;
	/**
	 * 被修改的密码
	 */
	private String pwdUped;
	/**
	 * 修改者id
	 */
	private String updateUser;
	/**
	 * 用户ID
	 */
	private String userId;
	/**
	 * 最近一次修改人
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
	/**
	 * 最近一次修改时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

}
