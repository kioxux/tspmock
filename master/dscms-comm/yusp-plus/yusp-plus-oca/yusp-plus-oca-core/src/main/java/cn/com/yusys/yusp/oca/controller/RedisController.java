package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.service.impl.RedisCacheLoadServicImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: yusp-plus
 * @description: redis初始化操作
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2021-01-14 10:48
 */
@RestController
@RequestMapping("/api")
public class RedisController {
    private final Logger log = LoggerFactory.getLogger(RedisController.class);

    @Autowired
    private RedisCacheLoadServicImpl redisCacheLoadServic;

    /**
     * 将用户信息初始化到redis
     */
    @GetMapping("/userredis")
    public ResultDto insertUserToRedis() {
        redisCacheLoadServic.loadUserInfoData();
        return ResultDto.success();
    }

    /**
     * 将机构信息初始化到redis
     */
    @GetMapping("/orgredis")
    public ResultDto insertOrgToRedis() {
        redisCacheLoadServic.loadOrgInfoData();
        redisCacheLoadServic.loadOrgTreeData();
        return ResultDto.success();
    }

    /**
     * 将部门信息初始化到redis
     */
    @GetMapping("/dptredis")
    public ResultDto insertDptToRedis() {
        redisCacheLoadServic.loadDeptInfoData();
        return ResultDto.success();
    }

    /**
     * 将系统提示消息初始化到redis
     */
    @GetMapping("/messageredis")
    public ResultDto insertMessageToRedis() {
        redisCacheLoadServic.loadSmMessageInfoData();
        return ResultDto.success();
    }

    /**
     * oca全量系统缓存刷新
     * 1、与系统启动时执行的RedisCacheStartLoader内容一致
     * 2、PS：小微区域相关的数据权限数据除外（在cmis-biz服务中）
     *
     */
    @GetMapping("/all/systemredis")
    public ResultDto refreshAllSystemRedis(){
        log.info("************调用接口刷新全系统Redis缓存*******");
        redisCacheLoadServic.loadAllSystemCacheData();
        return ResultDto.success();
    }
}