package cn.com.yusys.yusp.oca.domain.bo;

import cn.com.yusys.yusp.oca.domain.vo.AdminSmAuthRecoVo;
import lombok.Data;

import java.util.List;

@Data
public class MenuAndControlAuthRecoSaveBo {

    private List<AdminSmAuthRecoVo> menuData;

    private List<AdminSmAuthRecoVo> ctrData;
}
