package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 规则信息配置表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-12-02 18:03:52
 */
@Data
@TableName("cms_rule_info")
public class RuleInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 规则id
	 */
	@TableId
	private String ruleId;
	/**
	 * 规则名
	 */
	private String ruleName;
	/**
	 * 调用类型B:SpringBean调用 I:反射调用
	 */
	private String callType;
	/**
	 * 调用对象B:此处为beanName,I:此处为类全路径
	 */
	private String callObject;
	/**
	 * 描述
	 */
	private String ruleDesc;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 规则显示状态(1-逻辑有效，0-逻辑删除)
	 */
	private String logicStatus;

}
