package cn.com.yusys.yusp.oca.interceptor;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.oca.config.annotation.LoginBeforeStrategy;
import cn.com.yusys.yusp.oca.domain.constants.Constants;
import cn.com.yusys.yusp.oca.domain.constants.ResponseAndMessageEnum;
import cn.com.yusys.yusp.oca.service.AdminSmCrelStraService;
import cn.com.yusys.yusp.oca.utils.I18nMessageByCode;
import cn.com.yusys.yusp.oca.utils.JsonUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @description: 登录拦截器
 * @author: zhangsong
 * @date: 2021/3/29
 */
@Slf4j
public class LoginInterceptor implements HandlerInterceptor {

    private AdminSmCrelStraService adminSmCrelStraService;
    private StringRedisTemplate stringRedisTemplate;
    private I18nMessageByCode i18nMessageByCode;

    public LoginInterceptor(AdminSmCrelStraService adminSmCrelStraService, StringRedisTemplate stringRedisTemplate,I18nMessageByCode i18nMessageByCode) {
        this.adminSmCrelStraService = adminSmCrelStraService;
        this.stringRedisTemplate = stringRedisTemplate;
        this.i18nMessageByCode = i18nMessageByCode;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //登录拦截（优先取方法级别）
        LoginBeforeStrategy loginStrategy = null;
        if (handler instanceof HandlerMethod) {
            loginStrategy = ((HandlerMethod) handler).getMethodAnnotation(LoginBeforeStrategy.class);
            if (loginStrategy == null) {
                loginStrategy = ((HandlerMethod) handler).getMethod().getDeclaringClass().getAnnotation(LoginBeforeStrategy.class);
            }
        }
        String loginCode = getLoginCode(request);

        //检查是否因为密码输入错误次数过多，而禁止登录
        String loginCodeCountCacheKey = String.format("loginErrorCount:loginCode_%s", loginCode);
        //判断是否存在key,次数不小于限定次数--禁止登录
        Boolean hasKey = stringRedisTemplate.hasKey(loginCodeCountCacheKey);
        if (hasKey) {
            String countStr = stringRedisTemplate.opsForValue().get(loginCodeCountCacheKey);
            if (Integer.parseInt(countStr) >= Constants.SystemUserConstance.DEFAULT_MAX_PASSWORD_ERROR) {
                sendJsonMessage(response, ResultDto.error(ResponseAndMessageEnum.USER_FORBIDDEN_LOGIN.getCode(), i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.USER_FORBIDDEN_LOGIN.getCode())));
                return false;
            }
        }

        if (loginStrategy == null) {
            return true;
        }

        if (StringUtils.isEmpty(loginCode)) {
            return true;
        }

        ResultDto checkResult = adminSmCrelStraService.checkBeforeLogin(loginCode);
        if (checkResult.getCode().equals(ResponseAndMessageEnum.SUCCESS.getCode())) {
            return true;
        }
        sendJsonMessage(response, checkResult);
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }

    private void sendJsonMessage(HttpServletResponse response, ResultDto resultDto) throws Exception {
        response.setContentType("application/json; charset=utf-8");
        PrintWriter writer = response.getWriter();
        writer.print(JSON.toJSONString(resultDto));
        writer.close();
        response.flushBuffer();
    }

    private String getLoginCode(HttpServletRequest request) {
        if (request.getMethod().equals("GET")) {
            return request.getParameter("loginCode");
        } else if (request.getMethod().equals("POST")) {
            try {
                RequestWrapper requestWrapper = new RequestWrapper(request);
                String body = requestWrapper.getBody();
                Map<String, Object> paramMap = JsonUtil.parseJson(body, HashMap.class);
                return paramMap.get("loginCode") == null ? null : paramMap.get("loginCode").toString();
            } catch (Exception e) {
                log.error("loginCode getError : ", e.toString());
                return null;
            }
        }

        return null;
    }
}
