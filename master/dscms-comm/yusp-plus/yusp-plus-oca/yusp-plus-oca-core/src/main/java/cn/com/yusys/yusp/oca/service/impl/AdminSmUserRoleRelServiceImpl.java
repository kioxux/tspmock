package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.common.utils.RAMPager;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.ObjectUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.oca.dao.AdminSmUserRoleRelDao;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmOrgTreeNodeBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmRoleEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserRoleRelEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmRoleUserRelQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmOrgVo;
import cn.com.yusys.yusp.oca.domain.vo.UserRelationshipVo;
import cn.com.yusys.yusp.oca.service.AdminSmOrgService;
import cn.com.yusys.yusp.oca.service.AdminSmRoleService;
import cn.com.yusys.yusp.oca.service.AdminSmUserRoleRelService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author terry
 * @email tanrui1@yusys.com.cn
 */

@Service("adminSmUserRoleRelService")
@Slf4j
public class AdminSmUserRoleRelServiceImpl extends ServiceImpl<AdminSmUserRoleRelDao, AdminSmUserRoleRelEntity> implements AdminSmUserRoleRelService {


    @Autowired
    private AdminSmOrgService adminSmOrgService;
    @Autowired
    private AdminSmRoleService adminSmRoleService;


    /**
     * 角色关联用户弹框中的查询条件：机构树
     * 范围是角色所在机构及下级所有机构
     *
     * @param roleId
     * @return
     */
    @Override
    public List<AdminSmOrgTreeNodeBo> getOrgTree(String roleId) {
//        Asserts.notEmpty(roleId,"roleId can not be null or empty!!");
        if (roleId == null || roleId.trim().length() == 0) {
            throw BizException.error("exist", "51000003", "roleId can not be null or empty!!");
        }
        AdminSmRoleEntity role = adminSmRoleService.getById(roleId);
        return adminSmOrgService.getOrgTreeByOrgId(role.getOrgId());
    }


    /**
     * 查询指定角色下所有关联用户信息及关联状态
     * 返回：[指定角色所在机构及所有下级机构下]或者[指定机构及所有下级机构下]的所有用户，其中已关联的checked为true，否则为false
     */
    @Override
    public Page<UserRelationshipVo> memberPage(AdminSmRoleUserRelQuery query) {
        //Asserts.notEmpty(query.getRoleId(),"roleId can not be null or empty!!");
        if (query.getRoleId() == null || query.getRoleId().trim().length() == 0) {
            throw BizException.error("exist", "51000003", "roleId can not be null or empty!!");
        }
        Page<UserRelationshipVo> page = query.getIPage();
        AdminSmRoleEntity role = adminSmRoleService.getById(query.getRoleId());
        // 查询可访问用户列表
        QueryWrapper<UserRelationshipVo> userWrapper = new QueryWrapper<>();
        userWrapper.like(StringUtils.nonEmpty(query.getUserCode()), "u.user_code", query.getUserCode());
        userWrapper.like(StringUtils.nonEmpty(query.getUserName()), "u.user_name", query.getUserName());
        userWrapper.like(StringUtils.nonEmpty(query.getLoginCode()), "u.login_code", query.getLoginCode());
        List<String> orgRange;
        if (StringUtils.isEmpty(query.getOrgId())) {//指定角色所在机构及所有下级机构下
            orgRange = adminSmOrgService.getAllProgeny(role.getOrgId()).stream().map(AdminSmOrgVo::getOrgId).collect(Collectors.toList());
        } else {// 指定机构及所有下级机构下
            orgRange = adminSmOrgService.getAllProgeny(query.getOrgId()).stream().map(AdminSmOrgVo::getOrgId).collect(Collectors.toList());
        }
        userWrapper.in("u.org_id", orgRange);
        userWrapper.orderByDesc("u.last_chg_dt");
        List<UserRelationshipVo> allUserList = this.baseMapper.selectUserPage(userWrapper, query.getRoleId());
        //查询这些用户中关联了当前角色的
        List<String> allUserIds = allUserList.stream().map(UserRelationshipVo::getUserId).collect(Collectors.toList());
        List<String> checkedUserIds = this.lambdaQuery().select(AdminSmUserRoleRelEntity::getUserId)
                .eq(AdminSmUserRoleRelEntity::getRoleId, query.getRoleId())
                .in(allUserIds.size() > 0, AdminSmUserRoleRelEntity::getUserId, allUserIds)
                .list().stream().map(AdminSmUserRoleRelEntity::getUserId).collect(Collectors.toList());
        //加载allUserList所有元素的relSts属性
        allUserList.forEach((user) -> {
            if (checkedUserIds.contains(user.getUserId())) {
                user.setChecked(true);
            }
        });
        if (ObjectUtils.nonNull(query.getChecked())) {
            allUserList = allUserList.stream().filter(user -> query.getChecked().equals(user.getChecked())).collect(Collectors.toList());
        }
        RAMPager<UserRelationshipVo> pager = new RAMPager<>(allUserList, query.getSize());
        page.setRecords(pager.page(query.getPage()));
        page.setTotal(allUserList.size());
        return page;

    }

    @Override
    public Set<String> findUserIdsByRoleId(String roleId) {
        Objects.requireNonNull(roleId);
        return this.lambdaQuery().eq(AdminSmUserRoleRelEntity::getRoleId, roleId).select(AdminSmUserRoleRelEntity::getUserId).list().stream().map(AdminSmUserRoleRelEntity::getUserId).collect(Collectors.toSet());
    }

    //只查询与用户状态对应的关联信息
    @Override
    public List<AdminSmUserRoleRelEntity> findUserRoleRelsByUser(AdminSmUserEntity user) {
        //Asserts.notEmpty(user.getUserId(),"findRoleIdsByUserId(): userId required!!");
        if (user.getUserId() == null) {
            throw BizException.error(null, "51100004", "用户编号不能为空");
        }
        return this.lambdaQuery().eq(AdminSmUserRoleRelEntity::getUserId, user.getUserId()).list();
    }

    @Override
    @Transactional
    public boolean save(List<AdminSmUserRoleRelEntity> entityList) {
        Integer count = 0;
        for (AdminSmUserRoleRelEntity entity : entityList) {
            LambdaQueryWrapper<AdminSmUserRoleRelEntity> checkWrapper = new QueryWrapper<AdminSmUserRoleRelEntity>().lambda();
            checkWrapper.eq(AdminSmUserRoleRelEntity::getRoleId, entity.getRoleId());
            checkWrapper.eq(AdminSmUserRoleRelEntity::getUserId, entity.getUserId());
            AdminSmUserRoleRelEntity check = this.baseMapper.selectOne(checkWrapper);
            if (Objects.nonNull(check)) {
                throw BizException.error("exist", "51000001", "roleId:" + entity.getRoleId() + ",userId:" + entity.getUserId());
            }
            log.info("New userRoleRel data: [userId: {},roleId={}] ", entity.getUserId(), entity.getRoleId());
            count += this.baseMapper.insert(entity);
        }
        return count.equals(entityList.size());
    }

    @Override
    @Transactional
    public boolean remove(List<AdminSmUserRoleRelEntity> entityList) {
        List<String> ids = new ArrayList<>();
        for (AdminSmUserRoleRelEntity entity : entityList) {
            LambdaQueryWrapper<AdminSmUserRoleRelEntity> checkWrapper = new QueryWrapper<AdminSmUserRoleRelEntity>().lambda();
            checkWrapper.eq(AdminSmUserRoleRelEntity::getRoleId, entity.getRoleId());
            checkWrapper.eq(AdminSmUserRoleRelEntity::getUserId, entity.getUserId());
            AdminSmUserRoleRelEntity check = this.baseMapper.selectOne(checkWrapper);
            if (Objects.isNull(check)) {
                throw BizException.error("notExist", "51000002", "roleId:" + entity.getRoleId() + ",userId:" + entity.getUserId());
            }
            log.info("Delete userRoleRel data: [userId: {},roleId={}] ", entity.getUserId(), entity.getRoleId());
            ids.add(check.getUserRoleRelId());
        }
        Integer count = this.baseMapper.deleteBatchIds(ids);
        return count.equals(entityList.size());
    }


}