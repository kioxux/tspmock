package cn.com.yusys.yusp.oca.domain.constants;

/**
 * @Classname ResponseAndMessageEnum
 * @Description 用户返回code和对应的信息
 * @Date 2020/11/2 17:23
 * @Created by wujp4@yusys.com.cn
 */
public enum ResponseAndMessageEnum {
    SUCCESS("00000000","登录成功！"),
    BAD_CREDENTIALS("10000001","用户名或密码错误"),
    NON_PHONENUMBER("10000005","手机号不存在"),
    NON_USER("10101111","该登录名用户不存在！"),
    FIRSTLOGIN("10000002","首次登录请修改密码"),
    LOGIN_TIMES_ERROR("10200005", "您好，该时间段不允许登录系统，请您谅解！"),
    IPERROR("10300005","当前登录IP异常"),
    PASSWORD_NEED_CHANGE("10300006", "密码逾期失效,请联系管理员!"),
    USER_INVALID("10300007","用户未生效"),
    USER_FORBIDDEN_LOGIN("10300008","密码输入错误次数超过上限,请半个小时后再试！"),
    REDUNDANCY("50000001","%s！剩余次数：%d"),
    GREEN("10300004","当前在线人数已达上限！");

    //可以看出这在枚举类型里定义变量和方法和在普通类里面定义方法和变量没有什么区别。
    //唯一要注意的只是变量和方法定义必须放在所有枚举值定义的后面，否则编译器会给出一个错误。
    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    ResponseAndMessageEnum(String code, String message){ //加上public void 上面定义枚举会报错 The constructor Color(int, String) is undefined
        this.code=code;
        this.message=message;

    }
}
