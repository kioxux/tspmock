package cn.com.yusys.yusp.oca.dao;

import cn.com.yusys.yusp.oca.domain.entity.AdminSmDutyEntity;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmDutyVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统岗位表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-01 21:55:19
 */

public interface AdminSmDutyDao extends BaseMapper<AdminSmDutyEntity> {

    List<AdminSmDutyVo> selectAllDuty(@Param(Constants.WRAPPER) QueryWrapper<AdminSmDutyVo> queryWrapper);

    Page<AdminSmDutyVo> getDutysForWf(Page<AdminSmDutyVo> page, @Param(Constants.WRAPPER) QueryWrapper<AdminSmDutyVo> queryWrapper);

    List<String> getDutysByUserIdForWf(String userId);
}
