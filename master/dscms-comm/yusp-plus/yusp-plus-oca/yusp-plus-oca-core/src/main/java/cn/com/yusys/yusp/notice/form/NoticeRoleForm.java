package cn.com.yusys.yusp.notice.form;

import lombok.Data;

@Data
public class NoticeRoleForm {

    private String id;
    private String code;
    private String name;
    private boolean checked;
}
