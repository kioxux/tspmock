package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 金融机构表
 * 
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-19 14:30:22
 */
@Data
@TableName("admin_sm_instu")
public class AdminSmInstuEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 记录编号
	 */
	@TableId(type = IdType.UUID)
	private String instuId;
	/**
	 * 逻辑系统记录编号
	 */
	private String sysId;
	/**
	 * 金融机构代码
	 */
	private String instuCde;
	/**
	 * 金融机构名称
	 */
	private String instuName;
	/**
	 * 进入日期
	 */
	private Date joinDt;
	/**
	 * 地址
	 */
	private String instuAddr;
	/**
	 * 邮编
	 */
	private String zipCde;
	/**
	 * 联系电话
	 */
	private String contTel;
	/**
	 * 联系人
	 */
	private String contUsr;
	/**
	 * 状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效
	 */
	private String instuSts;
	/**
	 * 最新变更用户
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
	/**
	 * 最新变更时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;


}
