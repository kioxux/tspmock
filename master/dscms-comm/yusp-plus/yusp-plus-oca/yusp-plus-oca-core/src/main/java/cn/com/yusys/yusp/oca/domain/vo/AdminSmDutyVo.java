package cn.com.yusys.yusp.oca.domain.vo;

import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

/**
 * 系统岗位表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-01 21:55:19
 */
@Data
public class AdminSmDutyVo {

	private String dutyId;
	/**
	 * 岗位代码
	 */
	private String dutyCode;
	/**
	 * 岗位名称
	 */
	private String dutyName;
	/**
	 * 所属机构编号
	 */
	private String orgId;
	/**
	 * 备注
	 */
	private String dutyRemark;
	/**
	 * 状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效
	 */
	private AvailableStateEnum dutySts;
	/**
	 * 最新变更用户
	 */
	private String lastChgUsr;
	/**
	 * 最新变更时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

	private String orgName;

	private String lastChgName;

}
