package cn.com.yusys.yusp.oca.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * @类名 PasswordForgetAndResetCheckBo
 * @描述 用户忘记密码重置密码参数校验传参类
 * @作者 黄勃
 * @时间 2021/9/10 16:37
 **/
@ApiModel(value = "PasswordForgetAndResetCheckBo", description = "用户忘记密码重置密码参数校验传参类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PasswordForgetAndResetCheckBo {
    @ApiModelProperty(value = "账号", name = "loginCode", required = true)
    @NotEmpty(message = "账号不能为空")
    private String loginCode;

    @ApiModelProperty(value = "手机号", name = "mobile", required = true)
    private String mobile;

    @ApiModelProperty(value = "验证码", name = "varificationCode", required = false)
    private String varificationCode;
}