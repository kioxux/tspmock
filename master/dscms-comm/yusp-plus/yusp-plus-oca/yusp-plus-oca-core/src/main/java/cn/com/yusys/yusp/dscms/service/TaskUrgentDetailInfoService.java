/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dscms.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.dscms.domain.CfgTaskUrgentInfo;
import cn.com.yusys.yusp.dscms.domain.TaskUrgentDetailInfo;
import cn.com.yusys.yusp.dscms.repository.mapper.TaskUrgentDetailInfoMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TaskUrgentDetailInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 刘权
 * @创建时间: 2021-04-22 14:53:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class TaskUrgentDetailInfoService {

    @Autowired
    private TaskUrgentDetailInfoMapper taskUrgentDetailInfoMapper;

	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public TaskUrgentDetailInfo selectByPrimaryKey(String serno) {
        return taskUrgentDetailInfoMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<TaskUrgentDetailInfo> selectAll(QueryModel model) {
        List<TaskUrgentDetailInfo> records = (List<TaskUrgentDetailInfo>) taskUrgentDetailInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<TaskUrgentDetailInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<TaskUrgentDetailInfo> list = taskUrgentDetailInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(TaskUrgentDetailInfo record) {
        return taskUrgentDetailInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(TaskUrgentDetailInfo record) {
        return taskUrgentDetailInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(TaskUrgentDetailInfo record) {
        return taskUrgentDetailInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(TaskUrgentDetailInfo record) {
        return taskUrgentDetailInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return taskUrgentDetailInfoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return taskUrgentDetailInfoMapper.deleteByIds(ids);
    }

    public String queryCurtSurplusQnt(String userCode) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("userCode",userCode);
        queryModel.addCondition("urgentStatus","1");
        queryModel.setSort("input_date desc");
        List<TaskUrgentDetailInfo> taskUrgentDetailInfos = taskUrgentDetailInfoMapper.selectByModel(queryModel);
        if(taskUrgentDetailInfos.size() > 0) {
            TaskUrgentDetailInfo taskUrgentDetailInfo = taskUrgentDetailInfos.get(0);
            return  taskUrgentDetailInfo.getCurtSurplusQnt().toString();
        }else{
            return "0";
        }
    }

    /**
     * @方法名称: groupSum
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<Map<String, Object>> groupSum(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String, Object>> list = taskUrgentDetailInfoMapper.groupSum(model);
        PageHelper.clearPage();
        return list;
    }

    //定时任务调用方法
    /*public List<TaskUrgentDetailInfo> selectByTimeTask(QueryModel model) {

        //任务加急配置表
        CfgTaskUrgentInfo cfgTaskUrgentInfo = new CfgTaskUrgentInfo();
        //任务加急笔数明细信息表
        TaskUrgentDetailInfo taskUrgentDetailInfo = new TaskUrgentDetailInfo();
        //抵押登记(申请)
        GuarMortgageRegisterApp guarMortgageRegisterApp = new GuarMortgageRegisterApp();
        //放款申请
        PvpLoanApp pvpLoanApp = new PvpLoanApp();
        //合同申请信息表
        IqpLoanApp iqpLoanApp = new IqpLoanApp();

        //更新周期
        String updPeriod = cfgTaskUrgentInfo.getUpdPeriod();

        //优先分配比例
        BigDecimal priDivisPerc = cfgTaskUrgentInfo.getPriDivisPerc();

        //当前剩余笔数
        BigDecimal curtSurplusQnt = taskUrgentDetailInfo.getCurtSurplusQnt();

        //总优先笔数
        BigDecimal totalPriQnt = taskUrgentDetailInfo.getTotalPriQnt();

        //历史结余笔数
        BigDecimal hisSurplusQnt = taskUrgentDetailInfo.getHisSurplusQnt();

        //本年使用次数
        BigDecimal curtYeayUtilTimes = taskUrgentDetailInfo.getCurtYeayUtilTimes();
        //更新周期=每月
        if (updPeriod.equals(01)){
            //本次新增笔数计算
            //获取当前本次新增笔数
            BigDecimal curtAddQnt = taskUrgentDetailInfo.getCurtAddQnt();

        }else {      //更新周期=每自然季

        }



        PageHelper.startPage(model.getPage(), model.getSize());
        List<TaskUrgentDetailInfo> list = taskUrgentDetailInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }*/
}
