package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.oca.domain.bo.DragMenuBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmMenuEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmBusiFuncQuery;
import cn.com.yusys.yusp.oca.service.AdminSmBusiFuncService;
import cn.com.yusys.yusp.oca.service.AdminSmMenuService;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmBusiFuncVo;
import cn.com.yusys.yusp.oca.domain.vo.MenuVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * 系统菜单表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-24 11:02:41
 */
@RestController
@RequestMapping("/api/adminsmmenu")
public class AdminSmMenuController {

    @Autowired
    private AdminSmMenuService adminSmMenuService;
    @Autowired
    private AdminSmBusiFuncService adminSmBusiFuncService;

    /**
     * 递归查询菜单树列表
     */
    @GetMapping("/tree")
    public ResultDto<MenuVo> treeList(@RequestParam(required = false) String sysId,
                                      @RequestParam(required = false) String upMenuId,
                                      @RequestParam(required = false) String treeDeep) {
        List<MenuVo> menuVos = adminSmMenuService.queryTreeMenuList(sysId, upMenuId, treeDeep);
        return ResultDto.success(menuVos).total(menuVos.size());
    }


    /**
     * 搜索树
     */
    @GetMapping("/searchtree")
    public ResultDto<MenuVo> treeSearchList(@RequestParam String menuName,
                                            @RequestParam(required = false) String sysId) {
        List<MenuVo> menuVos = adminSmMenuService.querySearchTree(sysId, menuName);
        return ResultDto.success(menuVos);
    }

    /**
     * 列表
     */
    @GetMapping("/list")
    public ResultDto list(@RequestParam Map<String, Object> params) {
        PageUtils page = adminSmMenuService.queryPage(params);
        return ResultDto.success().extParam("page", page);
    }

    /**
     * @方法名称:getMenuTree
     * @方法描述:菜单树初始化查询
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/menutreequery")
    public ResultDto<MenuVo> getMenuTree(String sysId, boolean lazy, String menuId) {
        List<MenuVo> list = adminSmMenuService.getMenuTree(sysId, lazy, menuId);
        return ResultDto.success(list).total(list.size());
    }


    /**
     * 信息
     */
    @GetMapping("/info/{menuId}")
    public ResultDto<AdminSmMenuEntity> info(@PathVariable("menuId") String menuId) {
        return ResultDto.success(adminSmMenuService.getById(menuId));
    }

    /**
     * 保存
     */
    @PostMapping("/createmenu")
    public ResultDto<MenuVo> save(@RequestBody @Validated AdminSmMenuEntity adminSmMenu) {
        adminSmMenu.setMenuId(StringUtils.getUUID());
         return ResultDto.success(adminSmMenuService.saveAdminSmMunu(adminSmMenu));
    }

    /**
     * 修改菜单
     */
    @PostMapping("/editmenu")
    public ResultDto<Void> update(@RequestBody @Validated AdminSmMenuEntity adminSmMenu) {
        adminSmMenuService.updateMenuById(adminSmMenu);
        return ResultDto.success();
    }

    /**
     * 修改菜单全树，菜单树可拖动
     *
     * @param menuVoList 前端传menu tree
     * @return
     */
    @PostMapping("/update/menutree")
    public ResultDto<Object> updateAllMenuTree(@RequestBody List<MenuVo> menuVoList) {
        int batchIds = adminSmMenuService.updateAllMenuTree(menuVoList);
        if (batchIds > 0) {
            return ResultDto.success().message("修改菜单tree 成功!");
        }
        return ResultDto.error("修改菜单tree 失败!");
    }

    /**
     * 更新菜单指定位置，菜单树可拖动
     *
     * @param dragMenuBo 前端传指定 拖动的menuId 参照的menuId 相对于参照点的前后顺序
     * @return
     */
    @PostMapping("/update/specificmenutree")
    public ResultDto<Object> updateMenuTree(@RequestBody DragMenuBo dragMenuBo) {
        int batchIds = adminSmMenuService.updateMenuTreeByDragMenu(dragMenuBo);
        if (batchIds > 0) {
            return ResultDto.success().message("修改菜单tree 成功!");
        }
        return ResultDto.error("修改菜单tree 失败!");
    }

    /**
     * 批量删除菜单
     * @param ids
     * @return
     */
    @PostMapping("/deletemenu")
    public ResultDto<Object> delete(@RequestBody @NotNull String[] ids) {
        int removeMenu = adminSmMenuService.removeMenuByIds(ids);
        if (removeMenu > 0) {
            return ResultDto.success().message("菜单id:" + ids.toString() + "及其子菜单删除成功!");
        }
        if (removeMenu == -1) {
            return ResultDto.error("菜单及其子菜单已关联业务功能!");
        }
        return ResultDto.error("菜单删除失败!");
    }

    /**
     * getFuncInfo
     * 业务功能列表查询
     */
    @GetMapping("/funclistquery")
    public ResultDto<AdminSmBusiFuncVo> getFuncInfo(AdminSmBusiFuncQuery adminSmBusiFuncQuery) {
        return ResultDto.success(adminSmBusiFuncService.getFuncInfoWithCondition(adminSmBusiFuncQuery));
    }

    /**
     * getMenuInfo
     * 菜单节点信息查询
     */
    @GetMapping("/menuinfoquery")
    public ResultDto<Object> getMenuInfo(@RequestParam String menuId) {
        return ResultDto.success(adminSmMenuService.getMenuInfo(menuId));
    }

}
