package cn.com.yusys.yusp.notice.service;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.notice.entity.AdminSmNoticeReciveEntity;
import cn.com.yusys.yusp.notice.form.AdminSmNoticeReciveForm;
import cn.com.yusys.yusp.notice.form.NoticeConditionForm;
import cn.com.yusys.yusp.notice.form.NoticeRoleForm;
import cn.com.yusys.yusp.notice.vo.AdminSmNoticeReciveVo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmOrgEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmRoleEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 系统公告表接收对象表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-15 13:05:33
 */
public interface AdminSmNoticeReciveService extends IService<AdminSmNoticeReciveEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 新增公告：保存公告权限
     * @param reciveEntity
     * @param reciveCreate
     */
    void saveByAdminSmNoticeAllEntity(AdminSmNoticeReciveEntity reciveEntity, String reciveCreate);

    /**
     * noticeId 删除公告权限
     * @param noticeIds
     */
    void deleteRecive(List<String> noticeIds);

    /**
     * 按照条件查询数据
     * @param roleIdList
     * @param orgIdList
     * @return
     */
    List<AdminSmNoticeReciveEntity> findListByCondition(List<String> roleIdList, List<String> orgIdList);

    /**
     * 公告角色权限查询
     * @param noticeId
     * @return
     */
    List<AdminSmRoleEntity> selectRoles(String noticeId);

    /**
     * 公告部门权限查询
     * @param noticeId
     * @return
     */
    List<AdminSmOrgEntity> selectOrgs(String noticeId);

    /**
     * 查询公告权限的机构信息和角色信息
     * @param noticeId
     * @return
     */
    List<AdminSmNoticeReciveVo> selectReciveIdAndName(String noticeId);
}

