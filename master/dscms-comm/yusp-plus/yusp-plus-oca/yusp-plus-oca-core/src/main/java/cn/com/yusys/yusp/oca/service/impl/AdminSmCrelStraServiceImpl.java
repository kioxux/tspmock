package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.common.utils.Query;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.oca.config.CrelStraConfig;
import cn.com.yusys.yusp.oca.dao.AdminSmCrelStraDao;
import cn.com.yusys.yusp.oca.domain.constants.Constants;
import cn.com.yusys.yusp.oca.domain.constants.ResponseAndMessageEnum;
import cn.com.yusys.yusp.oca.domain.dto.AdminSmCrelStraDto;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmCrelStraEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmLoginLogEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmPasswordLogEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.domain.vo.UserEntityVo;
import cn.com.yusys.yusp.oca.service.AdminSmCrelStraService;
import cn.com.yusys.yusp.oca.service.AdminSmLoginLogService;
import cn.com.yusys.yusp.oca.service.AdminSmPasswordLogService;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import cn.com.yusys.yusp.oca.utils.I18nMessageByCode;
import cn.com.yusys.yusp.oca.utils.JsonUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.*;
import java.util.*;
import java.util.stream.Collectors;


@Service("adminSmCrelStraService")
public class AdminSmCrelStraServiceImpl extends ServiceImpl<AdminSmCrelStraDao, AdminSmCrelStraEntity> implements AdminSmCrelStraService {
    @Autowired
    private CrelStraConfig crelStraConfig;
    @Autowired
    private AdminSmCrelStraDao adminSmCrelStraDao;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private AdminSmLoginLogService adminSmLoginLogService;
    @Autowired
    private AdminSmPasswordLogService adminSmPasswordLogService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private I18nMessageByCode i18nMessageByCode;

    //@PostConstruct
    public void init() {
        List<AdminSmCrelStraEntity> crelStraEntityList = crelStraConfig.getStrategy();
        List<AdminSmCrelStraEntity> originalList = this.adminSmCrelStraDao.getAll();
        List<String> originalKeyList = originalList.stream().map(o -> o.getCrelKey()).collect(Collectors.toList());
        List<AdminSmCrelStraEntity> needAddEntityList = new ArrayList<>();
        Date now = new Date();
        String sysId = StringUtils.isEmpty(SessionUtils.getClientId()) ? "test" : SessionUtils.getClientId();
        for (AdminSmCrelStraEntity crelStraEntity : crelStraEntityList) {
            if (originalKeyList.indexOf(crelStraEntity.getCrelKey()) < 0) {
                crelStraEntity.setCrelId(UUID.randomUUID().toString().replaceAll("-", ""));
                crelStraEntity.setSysId(sysId);
                crelStraEntity.setLastChgDt(now);
                crelStraEntity.setLastChgUsr("system");
                crelStraEntity.setCrelDetail(crelStraEntity.getCrelDetail() == null ? "" : crelStraEntity.getCrelDetail());

                needAddEntityList.add(crelStraEntity);
            }
        }
        if (!needAddEntityList.isEmpty()) {
            this.adminSmCrelStraDao.inserts(needAddEntityList);
        }

    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AdminSmCrelStraEntity> page = this.page(
                new Query<AdminSmCrelStraEntity>().getPage(params),
                new QueryWrapper<AdminSmCrelStraEntity>()
        );

        return new PageUtils(page);
    }

    public ResultDto<Object> checkBeforeLogin(String loginCode) {

        //校验用户是否存在
        AdminSmUserEntity userEntity = adminSmUserService.getOne(new QueryWrapper<AdminSmUserEntity>().eq("LOGIN_CODE", loginCode));

        if (Objects.isNull(userEntity)) {
            return ResultDto.success().code(ResponseAndMessageEnum.NON_USER.getCode()).message(i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.NON_USER.getCode()));
        }

        List<AdminSmCrelStraEntity> validStraList = getValidStraList();
        List<AdminSmCrelStraEntity> validLoginStraList = validStraList.stream().filter(s -> s.getCrelKey().startsWith(Constants.SystemUserConstance.LOGIN_KEY_START)
                && s.getCrelKey().endsWith(Constants.SystemUserConstance.BEFORE_LOGIN_KEY_END)).collect(Collectors.toList());

        //无有效参数不做拦截, 交给登录接口做账号验证
        if (StringUtils.isBlank(loginCode) || validLoginStraList.isEmpty()) {
            return ResultDto.success().code(ResponseAndMessageEnum.SUCCESS.getCode());
        }

        ResultDto<Object> checkResult = new ResultDto<>();
        for (AdminSmCrelStraEntity crelStraEntity : validLoginStraList) {
            switch (crelStraEntity.getCrelKey()) {
                case Constants.SystemUserConstance.LOGIN_TIMES:
                    checkResult = checkLoginTimes(loginCode, crelStraEntity);
                    break;
                default:
                    break;
            }

            if (!checkResult.getCode().equals(ResponseAndMessageEnum.SUCCESS.getCode())) {
                break;
            }
        }

        return checkResult;
    }

    public ResultDto<Object> passwordErrorLimit(String loginCode, String ip) {
        if (StringUtils.isBlank(loginCode)) {
            return ResultDto.success().code(ResponseAndMessageEnum.BAD_CREDENTIALS.getCode()).message(i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.BAD_CREDENTIALS.getCode()));
        }

        List<AdminSmCrelStraEntity> validStraList = getValidStraList();
        List<AdminSmCrelStraEntity> validLoginStraList = validStraList.stream().filter(s -> s.getCrelKey().equals(Constants.SystemUserConstance.PASSWORD_WRONG)).collect(Collectors.toList());

        if (validLoginStraList.isEmpty()) {
            return ResultDto.success().code(ResponseAndMessageEnum.BAD_CREDENTIALS.getCode()).message(i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.BAD_CREDENTIALS.getCode()));
        }

        String loginCodeCountCacheKey = String.format("loginErrorCount:loginCode_%s", loginCode);
        String countStr = stringRedisTemplate.opsForValue().get(loginCodeCountCacheKey);
        int errorCount = countStr == null ? 0 : Integer.parseInt(countStr);
        ++errorCount;
        int totalLimitCount = StringUtils.isBlank(validLoginStraList.get(0).getCrelDetail()) ? Constants.SystemUserConstance.DEFAULT_MAX_PASSWORD_ERROR : Integer.parseInt(validLoginStraList.get(0).getCrelDetail());
        int leftCount = totalLimitCount - errorCount;
        //设置缓存
        stringRedisTemplate.opsForValue().set(loginCodeCountCacheKey, String.valueOf(errorCount), Duration.ofMinutes(Constants.SystemUserConstance.DEFAULT_CACHE_TIME_M));

        //无剩余次数处理
        if (leftCount <= 0) {

            AdminSmUserEntity userEntity = adminSmUserService.getOne(new QueryWrapper<AdminSmUserEntity>().eq("LOGIN_CODE", loginCode));
            if (userEntity == null) {
                ResultDto.success().code(ResponseAndMessageEnum.USER_FORBIDDEN_LOGIN.getCode()).message(ResponseAndMessageEnum.USER_FORBIDDEN_LOGIN.getMessage());
            }
            ResultDto checkResult = ResultDto.success().code(ResponseAndMessageEnum.USER_FORBIDDEN_LOGIN.getCode()).message(ResponseAndMessageEnum.USER_FORBIDDEN_LOGIN.getMessage());
            return checkResult;
        }

        String message = String.format(i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.REDUNDANCY.getCode()), i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.BAD_CREDENTIALS.getCode()), leftCount);
        return ResultDto.success().code(ResponseAndMessageEnum.BAD_CREDENTIALS.getCode()).message(message);
    }

    @Transactional(rollbackFor = Exception.class)
    public ResultDto<Object> checkSuccessLogin(String userId, String ip) {
        if (StringUtils.isBlank(userId)) {
            return ResultDto.success().code(ResponseAndMessageEnum.SUCCESS.getCode());
        }

        List<AdminSmCrelStraEntity> validStraList = getValidStraList();
        List<AdminSmCrelStraEntity> validLoginStraList = validStraList.stream().filter(s -> (s.getCrelKey().startsWith(Constants.SystemUserConstance.LOGIN_KEY_START)
                && s.getCrelKey().endsWith(Constants.SystemUserConstance.AFTER_LOGIN_KEY_END))
                || s.getCrelKey().equals(Constants.SystemUserConstance.PASSWORD_COMPEL_CHANGE)).collect(Collectors.toList());

        //todo 校验业务
        ResultDto<Object> checkResult = ResultDto.success().code(ResponseAndMessageEnum.SUCCESS.getCode());
        AdminSmCrelStraEntity filedStraEntity = null;
        for (AdminSmCrelStraEntity crelStraEntity : validLoginStraList) {
            switch (crelStraEntity.getCrelKey()) {
                case Constants.SystemUserConstance.LOGIN_FIRST_RULE:
                    checkResult = checkFirstLogin(userId, crelStraEntity);
                    break;
                case Constants.SystemUserConstance.LOGIN_IP_RULE:
                    checkResult = checkLoginIpChange(userId, ip, crelStraEntity);
                    break;
                case Constants.SystemUserConstance.PASSWORD_COMPEL_CHANGE:
                    checkResult = checkPasswordNeedChange(userId, crelStraEntity);
                    break;
                default:
                    break;
            }

            if (!checkResult.getCode().equals(ResponseAndMessageEnum.SUCCESS.getCode())) {
                filedStraEntity = crelStraEntity;
                break;
            }
        }

        return failedAction(userId, checkResult, filedStraEntity);
    }

    public boolean getLoignSingleAgentEnabled() {
        List<AdminSmCrelStraEntity> validStraList = getValidStraList();
        List<AdminSmCrelStraEntity> validLoginStraList = validStraList.stream().filter(s -> s.getCrelKey().equals(Constants.SystemUserConstance.LOGIN_SINGLE_AGENT)).collect(Collectors.toList());

        return !validLoginStraList.isEmpty();
    }

    /**
     * 批量修改认证策略
     *
     * @param adminSmCrelStraDto
     */
    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateById(AdminSmCrelStraDto adminSmCrelStraDto) {
        //当前用户
        String currentUser = SessionUtils.getUserId();
        //当前时间
        Date date = new Date();
        //非系统生成
        int sysDefault = 0;

        //更新实体
        AdminSmCrelStraEntity adminSmCrelStraEntity = new AdminSmCrelStraEntity();
        BeanUtils.beanCopy(adminSmCrelStraDto, adminSmCrelStraEntity);
        adminSmCrelStraEntity.setLastChgUsr(currentUser);
        adminSmCrelStraEntity.setLastChgDt(date);
        adminSmCrelStraEntity.setSysDefault(sysDefault);

        //更新
        this.updateById(adminSmCrelStraEntity);
        //更新缓存
        if (Objects.nonNull(stringRedisTemplate.opsForValue().get(Constants.SystemUserConstance.STRATEGY_CACHE_KEY))) {
            stringRedisTemplate.delete(Constants.SystemUserConstance.STRATEGY_CACHE_KEY);
        }
    }

    /**
     * 首次登录校验
     *
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto<Object> checkFirstLogin(String userId, AdminSmCrelStraEntity crelStraEntity) {
        AdminSmUserEntity adminSmUserEntity = adminSmUserService.getById(userId);
        if (Objects.isNull(adminSmUserEntity)) {
            return ResultDto.success().code(ResponseAndMessageEnum.SUCCESS.getCode());
        }

        if (Objects.isNull(adminSmUserEntity.getLastLoginTime())) {
            UserEntityVo userEntityVo = new UserEntityVo();
            BeanUtils.beanCopy(adminSmUserEntity, userEntityVo);
            return ResultDto.success().code(ResponseAndMessageEnum.FIRSTLOGIN.getCode()).message(i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.FIRSTLOGIN.getCode())).data(userEntityVo);
        }
        return ResultDto.success().code(ResponseAndMessageEnum.SUCCESS.getCode());
    }

    /**
     * 登录IP变更校验
     *
     * @param userId
     * @param ip
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto<Object> checkLoginIpChange(String userId, String ip, AdminSmCrelStraEntity crelStraEntity) {
        if (StringUtils.isBlank(ip)) {
            return ResultDto.success().code(ResponseAndMessageEnum.SUCCESS.getCode());
        }

        AdminSmLoginLogEntity lastSuccessLoginLog = adminSmLoginLogService.getLastSuccessLogin(userId);
        if (lastSuccessLoginLog == null) {
            //只记录ip
            adminSmLoginLogService.saveLog(new AdminSmLoginLogEntity(UUID.randomUUID().toString().replace("-", ""), "", userId, "", ip,
                    "", "", Constants.LoginLogConstance.SUCCESS, "", new Date(), null));
            return ResultDto.success().code(ResponseAndMessageEnum.SUCCESS.getCode());
        }

        if (!lastSuccessLoginLog.getIpAddress().equals(ip)) {
            adminSmLoginLogService.saveLog(new AdminSmLoginLogEntity(UUID.randomUUID().toString().replace("-", ""), "", userId, "", ip,
                    "", "", Constants.LoginLogConstance.SUCCESS, "", new Date(), null));
            return ResultDto.success().code(ResponseAndMessageEnum.IPERROR.getCode()).message(i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.IPERROR.getCode()));
        }

        return ResultDto.success().code(ResponseAndMessageEnum.SUCCESS.getCode());
    }

    @Transactional(rollbackFor = Exception.class)
    public ResultDto<Object> checkPasswordNeedChange(String userId, AdminSmCrelStraEntity crelStraEntity) {
        AdminSmPasswordLogEntity lastUpdateEntity = adminSmPasswordLogService.getLastChangeLog(userId);
        if (lastUpdateEntity == null || lastUpdateEntity.getLastChgDt() == null) {
            return ResultDto.success().code(ResponseAndMessageEnum.SUCCESS.getCode());
        }

        int changeDay = Integer.parseInt(crelStraEntity.getCrelDetail());
        //当前日期
        LocalDate now = LocalDate.now();
        //最后一次修改密码日期
        LocalDate lastUpdateDate = LocalDateTime.ofInstant(lastUpdateEntity.getLastChgDt().toInstant(), ZoneId.systemDefault()).toLocalDate();
        //相差天数
        long diffDays = Period.between(lastUpdateDate, now).getDays();

        if (diffDays >= changeDay) {
            return ResultDto.success().code(ResponseAndMessageEnum.PASSWORD_NEED_CHANGE.getCode()).message(i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.PASSWORD_NEED_CHANGE.getCode()));
        }
        return ResultDto.success().code(ResponseAndMessageEnum.SUCCESS.getCode());
    }

    /**
     * 登录时间段校验
     *
     * @param loginCode
     * @param crelStraEntity
     * @return
     */
    private ResultDto<Object> checkLoginTimes(String loginCode, AdminSmCrelStraEntity crelStraEntity) {
//        List<AdminSmRoleVo> roleRelList = adminSmRoleService.getUserRoleByLoginCode(loginCode);
//        AdminSmRoleVo adminRole = roleRelList.stream().filter(r -> r != null && r.getRoleName() != null && r.getRoleName().equals("系统管理员角色")).findFirst().orElse(null);
        AdminSmUserEntity userEntity = adminSmUserService.getOne(new QueryWrapper<AdminSmUserEntity>().eq("LOGIN_CODE", loginCode));

        String userId = userEntity.getUserId();

        //若为管理员admin，即不会执行登录时间策略
        if (Constants.AdminSmLogicSysConstance.ADMIN_USER_ID.equals(userId)) {
            return ResultDto.success().code(ResponseAndMessageEnum.SUCCESS.getCode());
        }

        String detail = crelStraEntity.getCrelDetail();
        String[] split = detail.split(",");
//        if(timeStrategyDto.getWeekdays()) {
//            //todo 法定工作日计算
//        }
        LocalTime now = LocalTime.now();
        LocalTime startTime = LocalTime.parse(split[0]);
        LocalTime endTime = LocalTime.parse(split[1]);

        if (!now.isAfter(startTime) || !now.isBefore(endTime)) {
            return ResultDto.success().code(ResponseAndMessageEnum.LOGIN_TIMES_ERROR.getCode()).message(i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.LOGIN_TIMES_ERROR.getCode()));
        }

        return ResultDto.success().code(ResponseAndMessageEnum.SUCCESS.getCode());
    }

    /**
     * 获取开启的策略
     *
     * @return
     */
    private List<AdminSmCrelStraEntity> getValidStraList() {
        List<AdminSmCrelStraEntity> validStraList = new ArrayList<>();

        String jsonStr = stringRedisTemplate.opsForValue().get(Constants.SystemUserConstance.STRATEGY_CACHE_KEY);
        if (!StringUtils.isBlank(jsonStr)) {
            validStraList = JsonUtil.parseJsonArray(jsonStr, AdminSmCrelStraEntity.class);
        } else {
            QueryWrapper<AdminSmCrelStraEntity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("ENABLE_FLAG", Constants.SystemUserConstance.ENABLE_FLAG_TRUE);
            validStraList = this.baseMapper.selectList(queryWrapper);
            stringRedisTemplate.opsForValue().set(Constants.SystemUserConstance.STRATEGY_CACHE_KEY, JSON.toJSONString(validStraList), Duration.ofHours(Constants.SystemUserConstance.DEFAULT_CACHE_TIME_H));
        }

        return validStraList;
    }

    @Transactional(rollbackFor = Exception.class)
    public ResultDto failedAction(String userId, ResultDto resultDto, AdminSmCrelStraEntity adminSmCrelStraEntity) {
        if (resultDto.getCode().equals(ResponseAndMessageEnum.SUCCESS) || adminSmCrelStraEntity == null) {
            return resultDto;
        }
        String actionType = adminSmCrelStraEntity.getActionType();
        resultDto.extParam("actionType", actionType);

        switch (actionType) {
            case Constants.SystemUserConstance.USER_STATE_FREEZING:
                //adminSmUserService.freezingUser(userId);
                break;
            case Constants.SystemUserConstance.USER_STATE_FORBIDDEN:
                break;
            case Constants.SystemUserConstance.USER_STATE_WARNING:
                break;    //todo 是否存在警告记录
        }

        return resultDto;
    }
}