package cn.com.yusys.yusp.oca.domain.form;

import lombok.Data;

@Data
public class AdminSmLogForm {
    /**
     * 日志类型
     */
    private String logTypeId;
    /**
     * 操作用户
     */
    private String userName;
    /**
     * 操作对象ID
     */
    private String operObjId;
    /**
     * 操作时间从
     */
    private String beginTime;
    /**
     * 操作时间至
     */
    private String endTime;
    /**
     * 分页页号
     */
    private int page;
    /**
     * 分页大小
     */
    private int size;
}
