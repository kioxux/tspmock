package cn.com.yusys.yusp.sequence.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 序列号模版配置
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2021-04-22 14:43:47
 */
@Data
@TableName("sequence_config")
public class SequenceConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId
    private String id;
    /**
     * 序列名称
     */
    private String seqName;
    /**
     * 序列ID
     */
    private String seqId;
    /**
     * 开始值
     */
    private Integer startvalue;
    /**
     * 最大值
     */
    private Integer maximumvalue;
    /**
     * 自增值
     */
    private Integer incrementvalue;
    /**
     * 是否循环
     */
    private String isCycle;
    /**
     * 缓存值
     */
    private Integer cachevalue;
    /**
     * 序列模版
     */
    private String seqTemplet;
    /**
     * 序列用的位数
     */
    private Integer seqPlace;
    /**
     * 不足位数是否用0补全
     */
    private String zeroFill;
    /**
     * 序列是否已生成
     */
    private String seqCreate;
    /**
     * 当前序列值
     */
    private String currentValue;
    /**
     * 最新变更时间
     */
    private Date lastChgDt;
    /**
     * 最新变更用户
     */
    private String lastChgUsr;

}
