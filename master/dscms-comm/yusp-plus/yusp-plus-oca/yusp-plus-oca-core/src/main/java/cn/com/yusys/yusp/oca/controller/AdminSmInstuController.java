package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmInstuEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmInstuQuery;
import cn.com.yusys.yusp.oca.service.AdminSmInstuService;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import cn.com.yusys.yusp.oca.service.JwtService;
import cn.com.yusys.yusp.oca.domain.vo.InstuExtVo;
import cn.com.yusys.yusp.oca.domain.vo.JwtVo;
import cn.com.yusys.yusp.oca.domain.vo.UserSessionVo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;


/**
 * 金融机构表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-11-19 14:30:22
 */
@RestController
@RequestMapping("/api/adminsminstu")
public class AdminSmInstuController {


    @Autowired
    private AdminSmInstuService adminSmInstuService;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    /**
     * 列表
     */
    @GetMapping("/list")
    public ResultDto list() {
        LambdaQueryWrapper<AdminSmInstuEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AdminSmInstuEntity::getInstuSts, "A");
        return ResultDto.success(adminSmInstuService.list(queryWrapper));
    }

    @GetMapping("/page")
    public ResultDto<InstuExtVo> page(AdminSmInstuQuery query) {
        return ResultDto.success(adminSmInstuService.queryPage(query));
    }


    /**
     * 信息
     */
    @GetMapping("/info/{instuId}")
    public ResultDto info(@PathVariable("instuId") String instuId) {
        return ResultDto.success(adminSmInstuService.getById(instuId));
    }

    /**
     * 保存新增
     */
    @PostMapping("/save")
    public ResultDto save(@RequestHeader(value = "Authorization", required = false) String authorization, @RequestBody AdminSmInstuEntity adminSmInstu) {
        JwtVo jwtVo = jwtService.getUserIdAndClientIdByAuthorization(authorization);
        //TODO 后续要从SessionUtils中获取用户id
        UserSessionVo userSessionVo = adminSmUserService.getUserByAuthorization(jwtVo.getUser_id(), jwtVo.getClient_id());
        adminSmInstuService.save(adminSmInstu, userSessionVo);
        return ResultDto.successMessage("新增成功");

    }

    /**
     * 批量启用
     */
    @PostMapping("/batchenable")
    public ResultDto batchEnable(@RequestBody String[] ids) {
        String message = "启用失败";
        if (adminSmInstuService.batchEnable(ids)) {
            message = "启用成功";
            return ResultDto.success(message);
        }
        return ResultDto.error(message);
    }

    /**
     * 批量停用
     */
    @PostMapping("/batchdisable")
    public ResultDto batchDisable(@RequestBody String[] ids) {
        String message = "停用失败";
        if (adminSmInstuService.batchDisable(ids)) {
            message = "停用成功";
            return ResultDto.success(message);
        }
        return ResultDto.error(message);
    }

    /**
     * 批量删除
     */
    @PostMapping("/batchdelete")
    public ResultDto batchDelete(@RequestBody String[] ids) {
        String message = "删除失败";
        if (adminSmInstuService.batchDelete(ids)) {
            message = "删除成功";
            return ResultDto.success(message);
        }
        return ResultDto.error(message);
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public ResultDto update(@RequestBody AdminSmInstuEntity adminSmInstu) {
//        return ResultDto.success().extParam("data", adminSmInstuService.updateById(adminSmInstu)).extParam("message", "success").extParam("code", "0");
        String message = "更新失败";
        if (adminSmInstuService.updateById(adminSmInstu)) {
            message = "更新成功";
            return ResultDto.success(message);
        }
        return ResultDto.error(message);
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public ResultDto delete(@RequestBody String[] instuIds) {
        adminSmInstuService.removeByIds(Arrays.asList(instuIds));
        return ResultDto.success();
    }
}
