package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 申请信息表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-25 11:39:34
 */
@Data
@TableName("cms_apply_info")
public class ApplyInfoEntity implements Serializable {
    /**
     * 申请流水号
     */
    @TableId
    private String applyId;
    /**
     * 三方流水号
     */
    private String thirdId;
    /**
     * 客户id
     */
    private String cusId;
    /**
     * 客户姓名
     */
    private String cusName;
    /**
     * 客户英文名
     */
    private String cusEname;
    /**
     * 证件类型(10110:居民身份证,10119:组织机构代码,20150:护照,99099:其他证件)
     */
    private String certType;
    /**
     * 证件号码
     */
    private String certCode;
    /**
     * 证件有效起始日期
     */
    private Date certStartDate;
    /**
     * 证件有效起始日期
     */
    private Date certEndDate;
    /**
     * 性别(1:男,2:女,5,其他,9,未知)
     */
    private String sex;
    /**
     * 生日
     */
    @TableField(fill = FieldFill.UPDATE)
    private Date birthday;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 家庭地址
     */
    private String address;
    /**
     * 居住状态(1:自置,2:按揭,3:亲属楼宇,4:集体宿舍,5:租房,6:共有住宅,8:其他,9:未知)
     */
    private String liveStatus;
    /**
     * 最高学位(1:博士,2:硕士,3:学士,9:其他)
     */
    private String degreeLevel;
    /**
     * 学历状态 1：文盲 2：初中或初中以下 3：高中 4：大专或本科 5：硕士研究生 6：博士或博士后 9：未知)
     */
    private String eduLevel;
    /**
     * 婚姻状况 1：已婚 2：未婚 3：离异 4：丧偶 5：再婚 9：未知)
     */
    private String maritalStatus;
    /**
     * 电话号码
     */
    private String phone;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 配偶客户号
     */
    private String spouseCusId;
    /**
     * 配偶姓名
     */
    private String spouseCusName;
    /**
     * 配偶证件类型(10110:居民身份证,10119:组织机构代码,20150:护照,99099:其他证件)
     */
    private String spouseCertType;
    /**
     * 配偶证件号码
     */
    private String spouseCertCode;
    /**
     * 申请金额
     */
    private BigDecimal applyAmt;
    /**
     * 产品代码
     */
    private String productId;
    /**
     * 申请期限
     */
    private Integer applyTerm;
    /**
     * 现入住时间
     */
    private Date occupancyDate;
    /**
     * 工作状态(1:全职,2:兼职,3:自雇,9:其他)
     */
    private String workStatus;
    /**
     * 工作年限
     */
    private BigDecimal workYears;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 公司地址
     */
    private String companyAddr;
    /**
     * 入职日期
     */
    private Date inCompanyDate;
    /**
     * 公司类型(01:政府机关,02:社会团体,03:事业单位,04:国有企业,05:国有控股企业,11:外资企业,12:合资企业,13:私营企业,99:其他)
     */
    private String companyType;
    /**
     * 部门
     */
    private String dutyName;
    /**
     * 职位(01:国家机关,02:党群组织,03:企事业单位负责人,11:专业技术人员,12:办事人员和有关人员,21:商业,22:服务业人员,31:农林牧渔水利业生产人员,41:生产/运输设备操作人员及有关人员,51:军人,99:其他)
     */
    private String postName;
    /**
     * 公司电话
     */
    private String companyPhone;
    /**
     * 户籍地址
     */
    private String censusAddr;
    /**
     * 户籍地址邮编
     */
    private String censusPostcode;
    /**
     * 通讯地址
     */
    private String communicationAddr;
    /**
     * 通讯邮编
     */
    private String communicationPostcode;
    /**
     * 居住地址
     */
    private String liveAddress;
    /**
     * 额度编号
     */
    private String limitId;
    /**
     * 申请日期
     */
    private Date applyDate;
    /**
     * 申请状态000-待申请 001-申请中 100-申请通过 101-申请失败'
     */
    private String applyStatus;
    /**
     * 审批状态000-待申请 001-审批中 100-审批通过 101-审批拒绝'
     */
    private String aprvStatus;
    /**
     * 申请通过时间
     */
    private Date apprPassTime;
    /**
     * 拒绝理由
     */
    private String refuseReason;
    /**
     * 录入人
     */
    private String inputUser;
    /**
     * 录入时间
     */
    private Date inputTime;
    /**
     * 更新人
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateUser;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;
    /**
     * 家庭月收入
     */
    private BigDecimal monthlyIncome;
    /**
     * 交易流水号
     */
    private String transSeq;
    /**
     * 渠道编号
     */
    private String channelCode;
    /**
     * 审批结论文件存放地址
     */
    private String aprvConcluFileAddr;
    /**
     * 申请信息显示状态(0-逻辑显示，1-逻辑删除)
     */
    private String logicStatus;
    /**
     * 产品经理的用户ID号
     */
    private String managerId;
    /**
     * 产品经理的用户姓名
     */
    private String managerName;
    /**
     * 机构的ID号
     */
    private String orgId;
    /**
     * 机构的名字
     */
    private String orgName;
    /**
     * 产品名称
     */
    private String productName;
    /**
     * 居住地邮编
     */
    @NotEmpty(message = "居住地邮编不能为空")
    private String livePostcode;

}
