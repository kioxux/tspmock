package cn.com.yusys.yusp.notice.service;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.notice.entity.AdminSmRicheditInfoEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 富文本信息表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-15 13:05:43
 */
public interface AdminSmRicheditInfoService extends IService<AdminSmRicheditInfoEntity> {

    /**
     * 删除富文本表
     * @param noticeIds
     */
    void deleteRicheditInfo(List<String> noticeIds);

    /**
     * 按照 relId 修改富文本内容
     * @param richeditInfoEntity
     */
    void updateByRelId(AdminSmRicheditInfoEntity richeditInfoEntity);
}

