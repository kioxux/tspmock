package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.oca.domain.entity.AdminSmDataAuthTmplEntity;
import cn.com.yusys.yusp.oca.domain.form.AdminSmDataAuthTmplForm;
import cn.com.yusys.yusp.oca.domain.form.AdminSmDataTmplConditionForm;
import cn.com.yusys.yusp.oca.domain.vo.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 数据权限模板表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-08 18:53:56
 */
public interface AdminSmDataAuthTmplService extends IService<AdminSmDataAuthTmplEntity> {
    /**
     * 查询模板
     *
     * @param adminSmDataAuthTmplForm
     * @return
     */
    Page<AdminSmDataAuthTmplVo> queryPage(AdminSmDataAuthTmplForm adminSmDataAuthTmplForm);

    /**
     * 批量删除
     *
     * @param ids
     */
    List<AdminSmDataTmplVo> deleteTmpl(String[] ids);

    /**
     * 新增数据权限模板
     *
     * @param adminSmDataAuthTmplEntity
     */
    void updateTmpl(AdminSmDataAuthTmplEntity adminSmDataAuthTmplEntity);

    /**
     * 新增模板
     *
     * @param adminSmDataAuthTmplEntity
     * @return
     */
    AdminSmDataAuthTmplEntity addAuthTemplate(AdminSmDataAuthTmplEntity adminSmDataAuthTmplEntity);

    /**
     * 修改之前查询模板信息
     *
     * @param authTmplId
     * @return
     */
    AdminSmDataAuthTmplEntity getInfo(String authTmplId);

    /**
     * 权限授权树数据查询，联合admin_sm_auth_reco表及admin_sm_data_auth表
     *
     * @param id
     * @return
     */
    List<AdminSmAuthTreeVo> selectAuthTree(String id);

    /**
     * 查询控制点已经关联的模板
     *
     * @param contrId
     * @return
     */
    List<AdminDataAuthVo> associatedTmpl(String contrId);

    /**
     * 控制点页面需求的数据模板条件查询列表
     *
     * @param form
     * @return
     */
    IPage getListForContr(AdminSmDataTmplConditionForm form);

    /**
     * 获取与控制点绑定的所有数据模板
     *
     * @param stringList
     * @return
     */
    List<AdminDataAuthVo> associatedTmplList(List<String> stringList);

    /**
     * 获取控制点已关联的数据模板
     *
     * @param contrId
     * @return
     */
    List<AdminSmDataAuthTmplEntity> getByContrId(String contrId);

    /**
     * 数据模板授权页面列表数据
     *
     * @param contrIdList
     * @return
     */
    List<AdminTmplAndRecoVo> getTmplAndRecoVoList(List<String> contrIdList, String authobjId);

    /**
     * 关联DataAuth表，使用contrId查询，返回AdminSmDataTmplListVo
     *
     * @param contrId
     * @return
     */
    List<AdminSmDataTmplListVo> getByContrIdForVo(String contrId);
}

