package cn.com.yusys.yusp.message.query;

import cn.com.yusys.yusp.message.validator.MessageSubscribeType;
import lombok.Data;

import java.io.Serializable;

/**
 * 根据消息类型，渠道类型，订阅类型获取订阅用户
 *
 * @author xiaodg@yusys.com.cn
 **/
@Data
public class MessageSubscribeQuery implements Serializable {
    private static final long serialVersionUID = 6086529105673776461L;

    /**
     * 消息渠道类型
     */
    private String channelType;

    /**
     * 消息类型
     */
    private String messageType;

    /**
     * 消息订阅类型
     */
    @MessageSubscribeType
    private String subscribeType;
}
