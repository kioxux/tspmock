package cn.com.yusys.yusp.oca.domain.vo;

import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
@Data
public class AdminSmOrgDetailVo {

    private String orgId;
    /**
     * 金融机构编号
     */
    private String instuId;
    /**
     * 机构代码
     */
    private String orgCode;
    /**
     * 机构名称
     */

    private String orgName;
    /**
     * 上级机构记录编号
     */

    private String upOrgId;
    private String upOrgName;
    private String upOrgCode;
    /**
     * 机构层级
     */
    private Integer orgLevel;

    /**
     * 机构类别
     */
    private String orgType;
    /**
     * 地址
     */
    private String orgAddr;
    /**
     * 邮编
     */
    private String zipCde;
    /**
     * 联系电话
     */
    private String contTel;
    /**
     * 联系人
     */
    private String contUsr;
    /**
     * 状态：对应字典项=NORM_STS A：启用 I：停用 W：待启用
     */
    private AvailableStateEnum orgSts;
    /**
     * 最新变更用户
     */
    private String lastChgUsr;
    private String lastChgName;
    /**
     * 最新变更时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastChgDt;

}
