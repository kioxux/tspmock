package cn.com.yusys.yusp.message.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 免打扰消息发送时段
 * 小时配置按24小时制
 * 默认8:00:00至21:30:00
 *
 * @author xiaodg@yusys.com.cn
 **/
@Component
@Getter
public class DoNotDisturbTimeRange {
    /**
     * 免打扰开始时间的小时配置,默认为8点
     */
    @Value("${yusp.message.do-not-disturb-start-time-hour:8}")
    int doNotDisturbStartTimeHour;

    /**
     * 免打扰开始时间的分钟配置,默认为第0分
     */
    @Value("${yusp.message.do-not-disturb-start-time-minute:0}")
    int doNotDisturbStartTimeMinute;


    /**
     * 免打扰开始时间的秒数配置,默认为第0秒
     */
    @Value("${yusp.message.do-not-disturb-start-time-second:0}")
    int doNotDisturbStartTimeSecond;

    /**
     * 免打扰结束时间的小时配置, 默认为21点
     */
    @Value("${yusp.message.do-not-disturb-end-time-hour:21}")
    int doNotDisturbEndTimeHour;

    /**
     * 免打扰结束时间的分钟配置, 默认为第30分钟
     */
    @Value("${yusp.message.do-not-disturb-end-time-minute:30}")
    int doNotDisturbEndTimeMinute;

    /**
     * 免打扰结束时间的秒数配置,默认为第0秒
     */
    @Value("${yusp.message.do-not-disturb-end-time-second:0}")
    int doNotDisturbEndTimeSecond;

}
