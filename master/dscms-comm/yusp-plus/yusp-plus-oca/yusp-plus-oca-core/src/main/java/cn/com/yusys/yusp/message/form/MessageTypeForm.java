package cn.com.yusys.yusp.message.form;

import cn.com.yusys.yusp.message.validator.MessageTempType;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 消息类型表单对象
 *
 * @author xiaodg
 */
@Data
public class MessageTypeForm implements Serializable {
    private static final long serialVersionUID = 1455189616832608639L;

    /**
     * 消息类型
     */
    @NotBlank(message = "消息类型不能为空")
    @Size(max = 32, message = "错误的消息类型")
    private String messageType;
    /**
     * 描述
     */
    private String messageDesc;
    /**
     * 消息等级[小先发]
     */
    @Positive(message = "错误的消息等级")
    private String messageLevel;
    /**
     * 模板类型[实时模板、订阅模板]
     */
    @MessageTempType
    private String templateType;


}
