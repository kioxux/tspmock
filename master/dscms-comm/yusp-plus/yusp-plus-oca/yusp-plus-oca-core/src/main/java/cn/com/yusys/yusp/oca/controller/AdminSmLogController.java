package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.commons.excelcsv.handle.IProgress;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.MimeUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.commons.util.io.IOUtils;
import cn.com.yusys.yusp.oca.config.processor.CommonLogInfo;
import cn.com.yusys.yusp.oca.config.processor.DispatchLogService;
import cn.com.yusys.yusp.oca.domain.dto.AdminSmLogDto;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmLogEntity;
import cn.com.yusys.yusp.oca.domain.form.AdminSmLogForm;
import cn.com.yusys.yusp.oca.domain.query.AdminSmLogQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmLogVo;
import cn.com.yusys.yusp.oca.service.AdminSmLogService;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 系统操作日志表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-02 22:18:19
 */
@Slf4j
@Api("日志管理接口")
@RestController
@RequestMapping("/api/monitor")
public class AdminSmLogController {

    @Autowired
    AdminSmLogService adminSmLogService;

    @Autowired
    DispatchLogService dispatchLogService;

    @Autowired
    AdminSmUserService adminSmUserService;

    @Autowired
    private IProgress iProgress;

    @ApiOperation("日志导出")
    @GetMapping("/auditlogdata/export")
    public void export(@RequestParam Map<String, String> params, HttpServletRequest request, HttpServletResponse response) throws IOException {
        File file = adminSmLogService.export(params);

        response.setContentType(MimeUtils.getMimeType(file.getName()));
        response.setHeader("Content-Disposition", "attachment; filename=" + MimeUtils.getEncodeFileName
                (request, file.getName()));
        response.getOutputStream().write(IOUtils.readBytes(new FileInputStream(file), true));
        response.getOutputStream().flush();
    }

    @ApiOperation("日志异步导出")
    @PostMapping("/translatefile")
    public ResultDto translateFile(@RequestBody AdminSmLogQuery logQuery) throws IOException {
        ProgressDto progressDto = adminSmLogService.translateFile(logQuery);
        return ResultDto.success(progressDto);
    }

    /**
     * 使用 taskId 下载日志 excel 临时文件
     * @param taskId
     * @return
     */
    @GetMapping("/downloadfile/{taskId}")
    public void asyncExport(@PathVariable String taskId, HttpServletResponse response) {
        ProgressDto progress = iProgress.progress(taskId);
        String filePath = progress.getFileId();
        String fileName = filePath.substring(filePath.lastIndexOf("\\") + 1);
        File file = null;
        try {
            // path是指欲下载的文件的路径。
            file = new File(filePath);
            // 以流的形式下载文件。
            InputStream fis = new BufferedInputStream(new FileInputStream(filePath));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            // 清空response
            response.reset();
            // 设置response的Header
            response.addHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes()));
            response.addHeader("Content-Length", "" + file.length());
            response.setContentType("application/octet-stream; charset=utf-8");
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            toClient.write(buffer);
            toClient.flush();
            toClient.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
//        finally {
//            if (file.exists()) {
//                file.delete();
//            }
//        }
    }

    /**
     * 文件上传，异步导入
     * @param uploadFile
     * @return
     */
    @PostMapping("/uploadfile")
    public ResultDto asyncImport(@RequestBody MultipartFile uploadFile) {
        if (null == uploadFile) {
            return ResultDto.error("上传文件为空！");
        }
        ProgressDto progressDto = adminSmLogService.asyncImport(uploadFile);
        return ResultDto.success(progressDto);
    }

    /**
     * 下载 excel 文件模板
     * @return
     */
    @GetMapping("/template")
    public void downLoadTemplate(HttpServletResponse response) {
        File file = adminSmLogService.downLoadTemplate();
        String fileName = file.getName();
        try {
            // 以流的形式下载文件。
            InputStream fis = new BufferedInputStream(new FileInputStream(file));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            // 清空response
            response.reset();
            // 设置response的Header
            response.addHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes()));
            response.addHeader("Content-Length", "" + file.length());
            response.setContentType("application/octet-stream");
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            toClient.write(buffer);
            toClient.flush();
            toClient.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (file.exists()) {
                file.delete();
            }
        }
    }

    /**
     * 批量删除日志
     *
     * @param logList 日志对象列表
     */
    @ApiOperation("批量删除日志")
    @PostMapping("/auditlogdata/batchdelete")
    protected ResultDto batchDelete(@RequestBody List<AdminSmLogEntity> logList) {
        List<String> collect = logList.stream().map(AdminSmLogEntity::getLogId).collect(Collectors.toList());
        adminSmLogService.removeByIds(collect);
        return ResultDto.success();
    }

    /**
     * 异步日志
     */
    @ApiOperation("异步记录日志")
    @PostMapping("/auditlogdata")
    public ResultDto addLog(@RequestBody AdminSmLogDto logDto, HttpServletRequest request) {

        // 设置请求ip
        if (StringUtils.isEmpty(logDto.getLoginIp())) {
            logDto.setLoginIp(getIpAddress(request));
        }
        /**
         * 添加前端未传数据
         */
        logDto.setOperTime(DateUtils.formatDateTimeByDef(new Date()));
        logDto.setUserId(SessionUtils.getUserId());
        logDto.setOrgId(adminSmUserService.getById(SessionUtils.getUserId()).getOrgId());
        CommonLogInfo commonLogInfo = new CommonLogInfo();
        commonLogInfo.setLogInfo(logDto);
        dispatchLogService.handleLog(commonLogInfo);

        return ResultDto.success("记录操作到日志中！").data("Success");
    }

    /**
     * 分页条件查询日志表
     *
     * @param adminSmLogForm: {"logTypeId":"7","user":"40","operObjId":"111","beginTime":"2020-12-06","endTime":"2020-12-07"}
     */
    @ApiOperation("分页条件查询日志表")
    @GetMapping("/auditlogdata/list")
    public ResultDto pageLogByCondition(AdminSmLogForm adminSmLogForm) {
        IPage<AdminSmLogVo> page = adminSmLogService.pageLogByCondition(adminSmLogForm);
        return ResultDto.success(page);
    }

    /**
     * 列表
     */
    @GetMapping("/list")
    public ResultDto list(@RequestParam Map<String, Object> params) {
        PageUtils page = adminSmLogService.queryPage(params);
        return ResultDto.success(page);
    }

    /**
     * 信息
     */
    @GetMapping("/info/{logId}")
    public ResultDto info(@PathVariable("logId") String logId) {
        AdminSmLogEntity adminSmLog = adminSmLogService.getById(logId);

        return ResultDto.success(adminSmLog);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public ResultDto save(@RequestBody AdminSmLogEntity adminSmLog) {
        adminSmLogService.save(adminSmLog);

        return ResultDto.success();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public ResultDto update(@RequestBody AdminSmLogEntity adminSmLog) {
        adminSmLogService.updateById(adminSmLog);

        return ResultDto.success();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public ResultDto delete(@RequestBody String[] logIds) {
        adminSmLogService.removeByIds(Arrays.asList(logIds));

        return ResultDto.success();
    }

    public String getIpAddress(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

}
