package cn.com.yusys.yusp.oca.domain.form;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class AdminSmLookupItemForm {

	/**
	 * 记录编号
	 */
	private String lookupItemId;
	/**
	 * 字典类别英文别名
	 */
	private String lookupCode;
	/**
	 * 上级字典内容编号
	 */
	private String upLookupItemId;
	/**
	 * 字典代码
	 */
	private String lookupItemCode;
	/**
	 * 字典名称
	 */
	private String lookupItemName;
	/**
	 * 字典备注说明
	 */
	private String lookupItemComment;
	/**
	 * 最新变更用户
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;


}
