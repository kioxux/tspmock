package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.common.query.PageQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmBusiFuncVo;
import lombok.Data;

/**
 * @author xufy1@yusys.com.cn
 * @desc
 * @date 2020-12-15 06:57
 */
@Data
public class AdminSmBusiFuncQuery extends PageQuery<AdminSmBusiFuncVo> {

    private String modId;
    private String keyWord;
    /**
     * 旧oca中 工作流查询用到的模糊 关键字查询字段
     */
    private String queryKey;
}
