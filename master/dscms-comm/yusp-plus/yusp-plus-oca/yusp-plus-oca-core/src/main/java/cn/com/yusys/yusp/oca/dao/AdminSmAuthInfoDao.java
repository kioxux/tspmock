package cn.com.yusys.yusp.oca.dao;

import cn.com.yusys.yusp.oca.domain.entity.AdminSmAuthInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 认证信息表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-11 14:11:01
 */

public interface AdminSmAuthInfoDao extends BaseMapper<AdminSmAuthInfoEntity> {
	
}
