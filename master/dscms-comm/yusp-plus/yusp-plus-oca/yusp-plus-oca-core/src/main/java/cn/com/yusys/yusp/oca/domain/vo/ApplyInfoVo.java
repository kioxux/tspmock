package cn.com.yusys.yusp.oca.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApplyInfoVo {

    private String applyId;

    private String cusId;

    private String cusName;

    private String certType;

    private String certCode;

    private String productName;

    private String applyStatus;

    private String aprvStatus;

    private String managerName;

    private String orgName;

    private String orgId;

    private String managerId;

    private String productId;
}
