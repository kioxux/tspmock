package cn.com.yusys.yusp.service.server.xdxt0015;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.OcaTradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxt0015.req.Xdxt0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0015.resp.List;
import cn.com.yusys.yusp.dto.server.xdxt0015.resp.Xdxt0015DataRespDto;
import cn.com.yusys.yusp.enums.OcaTradeEnum;
import cn.com.yusys.yusp.oca.dao.AdminSmUserDao;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @version 1.0.0
 * @项目名称: yusp-oca模块
 * @类名称: Xdxt0015Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-26 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxt0015Service extends ServiceImpl<AdminSmUserDao, AdminSmUserEntity> {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0015Service.class);

    /**
     * 用户机构角色信息列表查询
     *
     * @param xdxt0015DataReqDto
     * @return
     */
    @Transactional
    public Xdxt0015DataRespDto getXdxt0015(Xdxt0015DataReqDto xdxt0015DataReqDto) throws BizException, Exception {
        logger.info(OcaTradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0015.key, OcaTradeEnum.TRADE_CODE_XDXT0015.value, JSON.toJSONString(xdxt0015DataReqDto));
        Xdxt0015DataRespDto xdxt0014DataRespDto = new Xdxt0015DataRespDto();
        try {
            if (StringUtils.isEmpty(xdxt0015DataReqDto.getActorName()) && StringUtils.isEmpty(xdxt0015DataReqDto.getActorNo())) {
                throw BizException.error(null, OcaTradeEnum.EOCA080001.key, OcaTradeEnum.EOCA080001.value);
            }
            java.util.List<List> list = this.baseMapper.getXdxt0015(xdxt0015DataReqDto);
            xdxt0014DataRespDto.setList(list);
        } catch (BizException e) {
            logger.info(OcaTradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0015.key, OcaTradeEnum.TRADE_CODE_XDXT0015.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(OcaTradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0015.key, OcaTradeEnum.TRADE_CODE_XDXT0015.value, e.getMessage());
            throw new Exception(OcaTradeEnum.EPB099999.value);
        }
        logger.info(OcaTradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0015.key, OcaTradeEnum.TRADE_CODE_XDXT0015.value, JSON.toJSONString(xdxt0014DataRespDto));
        return xdxt0014DataRespDto;
    }
}
