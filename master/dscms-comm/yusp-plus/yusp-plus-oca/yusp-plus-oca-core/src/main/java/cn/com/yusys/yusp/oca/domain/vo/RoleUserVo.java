package cn.com.yusys.yusp.oca.domain.vo;

import lombok.Data;

/**
 * @author danyu
 */
@Data
public class RoleUserVo {

    private String orgName;
    private String loginCode;
    private String userRoleRelId;
    private String userName;
    private String userId;
    private String userCode;
    private String orgId;


}
