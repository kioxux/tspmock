package cn.com.yusys.yusp.message.service.impl;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.message.config.MessageConstants;
import cn.com.yusys.yusp.message.config.SignConstants;
import cn.com.yusys.yusp.message.dao.MessageSubscribeDao;
import cn.com.yusys.yusp.message.entity.MessageSubscribeEntity;
import cn.com.yusys.yusp.message.enumeration.MessageSubscribeTypeEnum;
import cn.com.yusys.yusp.message.form.MessageSubscribeForm;
import cn.com.yusys.yusp.message.query.MessageSubscribeQuery;
import cn.com.yusys.yusp.message.service.*;
import cn.com.yusys.yusp.message.util.MessageUtils;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmOrgTreeNodeBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmOrgEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.service.AdminSmOrgService;
import cn.com.yusys.yusp.oca.service.AdminSmUserDutyRelService;
import cn.com.yusys.yusp.oca.service.AdminSmUserRoleRelService;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 消息订阅服务类
 *
 * @author xiaodg@yusys.com.cn
 */
@Service
@Slf4j
public class MessageSubscribeServiceImpl extends ServiceImpl<MessageSubscribeDao, MessageSubscribeEntity> implements MessageSubscribeService {

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private AdminSmUserDutyRelService adminSmUserDutyRelService;

    @Autowired
    private AdminSmUserRoleRelService adminSmUserRoleRelService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Override
    public Boolean saveMessageSubscribe(MessageSubscribeForm messageSubscribeForm) {
        // 注意，MessageSubscribeForm中的subscribeValue是以英文逗号分隔的，入库需要拆分
        try {
            // 先删除数据库原有的数据
            LambdaQueryChainWrapper<MessageSubscribeEntity> removeQw = this.lambdaQuery()
                    .eq(MessageSubscribeEntity::getChannelType, messageSubscribeForm.getChannelType())
                    .eq(MessageSubscribeEntity::getMessageType, messageSubscribeForm.getMessageType())
                    .eq(MessageSubscribeEntity::getSubscribeType, messageSubscribeForm.getSubscribeType());
            this.remove(removeQw.getWrapper());
            // 处理subscribeValue
            List<MessageSubscribeEntity> entities = Stream.of(messageSubscribeForm.getSubscribeValue().split(SignConstants.MARK_COMMA))
                    .map(StringUtils::trim)
                    .filter(StringUtils::nonBlank)
                    .distinct()
                    .map(val -> initWithForm(messageSubscribeForm, val)).collect(Collectors.toList());
            if (entities.size() > 0) {
                this.saveBatch(entities);
            }
        } catch (Exception e) {
            log.error("保存订阅消息失败: ", e);
            return false;
        }
        return true;
    }

    @Override
    public String findSubscribeValBy(MessageSubscribeQuery messageSubscribeQuery) {
        return this.lambdaQuery()
                .eq(MessageSubscribeEntity::getSubscribeType, messageSubscribeQuery.getSubscribeType())
                .eq(MessageSubscribeEntity::getChannelType, messageSubscribeQuery.getChannelType())
                .eq(MessageSubscribeEntity::getMessageType, messageSubscribeQuery.getMessageType())
                .list()
                .stream()
                .map(MessageSubscribeEntity::getSubscribeValue)
                .collect(Collectors.joining(SignConstants.MARK_COMMA));
    }

    @Override
    public Set<String> findUserIdsBy(String messageType, String channelType, String sendUserId) {
        // 查询出所有的订阅关系里
        final HashSet<String> userIdS = new HashSet<>();
        this.lambdaQuery()
                .eq(MessageSubscribeEntity::getMessageType, messageType)
                .eq(MessageSubscribeEntity::getChannelType, channelType)
                .list()
                .stream()
                .map(messageSubscribeEntity -> findUserIdsBy(MessageSubscribeTypeEnum.valueOf(messageSubscribeEntity.getSubscribeType()), messageSubscribeEntity.getSubscribeValue(), sendUserId)).collect(Collectors.toSet());
        return userIdS;
    }

    private Set<String> findUserIdsBy(MessageSubscribeTypeEnum messageSubscribeTypeEnum, String subscribeValue, String sendUserId) {
        final HashSet<String> userIds = new HashSet<>();
        MessageUtils.requireNonNullAndEmptyString(subscribeValue, "查找订阅关系: 订阅值不能为空");
        switch (messageSubscribeTypeEnum) {
            case USER:
                userIds.add(subscribeValue);
                break;
            case DUTY:
                userIds.addAll(adminSmUserDutyRelService.findUserIdsByDutyId(subscribeValue));
                break;
            case ROLE:
                userIds.addAll(adminSmUserRoleRelService.findUserIdsByRoleId(subscribeValue));
                break;
            case ORG:
                userIds.addAll(adminSmUserService.findUserIdsByOrgId(subscribeValue));
                break;
            case REL:
                if (StringUtils.isEmpty(sendUserId)) {
                    log.error("查询消息关系订阅: 但发送者ID为空");
                    break;
                }
                final AdminSmUserEntity sendUser = adminSmUserService.getById(sendUserId);
                if (Objects.isNull(sendUser)) {
                    log.error("查询消息关系订阅: 数据库不能查询到发送者, 发送者ID: {}", sendUserId);
                    break;
                }
                if (MessageConstants.MESSAGE_SUBSCRIBE_REL_UP_LEVEL_ORG.equalsIgnoreCase(subscribeValue)) {
                    // 上级机构人员
                    userIds.addAll(findUpLevelOrgsUsers(sendUser));
                } else if (MessageConstants.MESSAGE_SUBSCRIBE_REL_SAME_LEVEL_ORG.equalsIgnoreCase(subscribeValue)) {
                    // 同级机构人员
                    userIds.addAll(findSiblingOrgsUsers(sendUser));
                } else {
                    log.error("不支持的关系订阅类型: " + subscribeValue);
                }
                break;
            default:
                break;
        }
        return userIds;
    }

    private Set<String> findSiblingOrgsUsers(AdminSmUserEntity sendUser) {
        final HashSet<String> userIds = new HashSet<>();
        final Set<AdminSmOrgEntity> siblingOrgs = adminSmOrgService.getSiblingOrgs(sendUser.getOrgId());
        final String sendUserId = sendUser.getUserId();
        if (siblingOrgs.size() <= 0) {
            log.warn("查询消息关系订阅: 发送者ID[{}]没有任何同级机构", sendUserId);
            return userIds;
        }
        final List<AdminSmUserEntity> userSet = adminSmUserService.lambdaQuery().select(AdminSmUserEntity::getUserId).in(AdminSmUserEntity::getOrgId, siblingOrgs.stream().map(AdminSmOrgEntity::getOrgId).collect(Collectors.toSet())).list();
        if (Objects.isNull(userSet) || userSet.size() <= 0) {
            log.warn("查询消息关系订阅: 发送者ID[{}]所有上级机构没有任何用户", sendUserId);
            return userIds;
        }
        return userSet.stream().map(AdminSmUserEntity::getUserId).collect(Collectors.toSet());
    }

    private Set<String> findUpLevelOrgsUsers(AdminSmUserEntity sendUser) {
        final HashSet<String> userIds = new HashSet<>();
        final Set<AdminSmOrgTreeNodeBo> allAncestryOrgs = adminSmOrgService.getAllAncestryOrgs(sendUser.getOrgId());
        final String sendUserId = sendUser.getUserId();
        if (allAncestryOrgs.size() <= 0) {
            log.warn("查询消息关系订阅: 发送者ID[{}]没有任何上级机构", sendUserId);
            return userIds;
        }
        final List<AdminSmUserEntity> userSet = adminSmUserService.lambdaQuery().select(AdminSmUserEntity::getUserId).in(AdminSmUserEntity::getOrgId, allAncestryOrgs.stream().map(AdminSmOrgTreeNodeBo::getOrgId).collect(Collectors.toSet())).list();
        if (Objects.isNull(userSet) || userSet.size() <= 0) {
            log.warn("查询消息关系订阅: 发送者ID[{}]所有上级机构没有任何用户", sendUserId);
            return userIds;
        }
        return userSet.stream().map(AdminSmUserEntity::getUserId).collect(Collectors.toSet());
    }

    private MessageSubscribeEntity initWithForm(MessageSubscribeForm messageSubscribeForm, String subscribeVal) {
        return new MessageSubscribeEntity(messageSubscribeForm.getChannelType(), messageSubscribeForm.getMessageType(), messageSubscribeForm.getSubscribeType(), Strings.EMPTY, subscribeVal);
    }

}