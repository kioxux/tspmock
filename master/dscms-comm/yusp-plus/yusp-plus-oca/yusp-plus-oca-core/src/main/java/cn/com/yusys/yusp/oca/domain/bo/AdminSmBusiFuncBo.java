package cn.com.yusys.yusp.oca.domain.bo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * 系统业务功能表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-20 13:43:51
 */
@Data
public class AdminSmBusiFuncBo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 记录编号
     */
    @Length
    private String funcId;
    /**
     * 所属功能模块编号
     */
    @NotBlank(message = "modId can not be empty!")
    private String modId;
    /**
     * 功能点名称
     */
    @NotBlank(message = "funcName can not be empty!")
    private String funcName;
    /**
     * 功能点描述
     */
    private String funcDesc;
    /**
     * URL链接
     */
    @NotBlank(message = "funcUrl can not be empty!")
    private String funcUrl;
    /**
     * JS链接
     */
    private String funcUrlJs;
    /**
     * CSS链接
     */
    private String funcUrlCss;
    /**
     * 顺序
     */
    private Integer funcOrder;
    /**
     * 图标
     */
    private String funcIcon;
    /**
     * 最新变更用户
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
    /**
     * 最新变更时间
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

}
