package cn.com.yusys.yusp.oca.domain.session;

import cn.com.yusys.yusp.commons.session.user.impl.UserInformation;

import java.util.List;

/**
 * 前端所需用户信息
 *
 * @author yangzai
 * @since 2021-06-17
 */
public class CmisUserInformation extends UserInformation {

    private CmisOrganizationImpl org;

    private List<DutyImpl> dutys;

    @Override
    public CmisOrganizationImpl getOrg() {
        return org;
    }

    public void setOrg(CmisOrganizationImpl org) {
        this.org = org;
    }

    public List<DutyImpl> getDutys() {
        return dutys;
    }

    public void setDutys(List<DutyImpl> dutys) {
        this.dutys = dutys;
    }
}