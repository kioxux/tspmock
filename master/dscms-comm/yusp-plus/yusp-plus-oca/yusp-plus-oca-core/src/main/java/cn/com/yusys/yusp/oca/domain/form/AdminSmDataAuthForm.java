package cn.com.yusys.yusp.oca.domain.form;

import lombok.Data;

@Data
public class AdminSmDataAuthForm {

	/**
	 * 记录编号
	 */
	private String authId;
	/**
	 * 控制点记录编号(为*时表示默认过滤器)
	 */
	private String contrId;
	/**
	 * 权限模板编号
	 */
	private String authTmplId;

}
