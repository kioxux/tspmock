package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.oca.domain.vo.InstuExtVo;
import cn.com.yusys.yusp.common.query.PageQuery;
import lombok.Data;

/**
 * 金融机构查询条件
 */
@Data
public class AdminSmInstuQuery extends PageQuery<InstuExtVo> {

    //private String instuId;
    /**
     * 逻辑系统记录编号
     */
//    private String sysId;
    /**
     * 金融机构代码
     */
    private String instuCde;
    /**
     * 金融机构名称
     */
    private String instuName;
    /**
     * 状态
     */
    private String instuSts;
}
