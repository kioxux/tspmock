package cn.com.yusys.yusp.oca.domain.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * @program: yusp-plus
 * @description: 系统参数新增接收参数
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2021-01-13 09:23
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminSmPropSaveBo {
    /**
     * 属性名
     */
    @NotEmpty(message = "参数名不能为空")
    private String propName;
    /**
     * 属性描述
     */
    private String propDesc;
    /**
     * 属性值
     */
    @NotEmpty(message = "参数值不能为空")
    private String propValue;
}
