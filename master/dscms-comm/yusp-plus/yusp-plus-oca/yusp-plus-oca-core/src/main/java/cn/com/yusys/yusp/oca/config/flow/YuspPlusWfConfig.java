/**
 * Copyright (C), 2014-2021
 * FileName: YuspPlusWfConfig
 * Author: Administrator
 * Date: 2021/3/31 10:57
 * Description:
 * History:
 * <author> <time> <version> <desc>
 * Administrator 10:57 1.0.0 新建类
 */

package cn.com.yusys.yusp.oca.config.flow;

import cn.com.yusys.yusp.flow.other.org.OrgInterface;
import cn.com.yusys.yusp.flow.service.WfBenchUserTaskService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

/**
 * 〈〉
 * @author zhui
 * @create 2021/3/31
 * @since 1.0.0
 */
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
@ConditionalOnClass(OrgInterface.class)
public class YuspPlusWfConfig {
    @Bean
    public OrgInterface wfOrgImpl(){
        return new WfLocalOrgImpl();
    }
    @Bean
    public WfBenchUserTaskService wfBenchUserTaskServiceImpl(){
        return new WfBenchUserTaskLocalServiceImpl();
    }
}
