package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.common.query.PageQuery;
import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmOrgVo;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 机构信息 分页查询条件
 */
@Data
@NoArgsConstructor
public class AdminSmOrgExtQuery extends PageQuery<AdminSmOrgVo> {
    private String upOrgId;
    private String orgCode;
    private String orgName;
    private AvailableStateEnum orgSts;
    private String keyWord;
}
