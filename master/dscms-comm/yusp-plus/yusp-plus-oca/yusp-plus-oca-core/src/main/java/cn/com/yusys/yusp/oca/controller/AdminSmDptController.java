package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmDptEntity;
import cn.com.yusys.yusp.oca.config.validation.Insert;
import cn.com.yusys.yusp.oca.config.validation.Update;
import cn.com.yusys.yusp.oca.domain.query.AdminSmDptQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmUserQuery;
import cn.com.yusys.yusp.oca.service.AdminSmDptService;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmDptVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmUserVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;


/**
 * 部门管理
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-11-12 10:47:26
 */
@RestController
@RequestMapping("/api/adminsmdpt")
public class AdminSmDptController {
    @Autowired
    private AdminSmDptService adminSmDptService;

    /**
     * 部门管理主页列表查询（分页）
     */
    @GetMapping("/page")
    public ResultDto<AdminSmDptVo> page(AdminSmDptQuery query) {
        return ResultDto.success(adminSmDptService.queryPage(query));
    }

    /**
     * 查出指定机构下所有部门以及子部门，以树形结构组装起来
     */
    @GetMapping("/tree")
    public ResultDto<AdminSmDptEntity> dptTree(AdminSmDptQuery query) {
        if (StringUtils.isEmpty(query.getOrgId())) {
            return ResultDto.success();
        }
        List<AdminSmDptEntity> dptTreeData = adminSmDptService.dptTree(query);
        return ResultDto.success(dptTreeData).total(dptTreeData.size());
    }

    /**
     * 新增部门
     */
    @PostMapping("/add")
    public ResultDto<Objects> add(@RequestBody @Validated({Insert.class}) AdminSmDptEntity entity) {
        adminSmDptService.save(entity);
        return ResultDto.successMessage("新增成功");
    }

    /**
     * 修改部门
     */
    @PostMapping("/update")
    public ResultDto<Object> update(@RequestBody @Validated({Update.class}) AdminSmDptEntity entity) {
        adminSmDptService.updateById(entity);
        return ResultDto.successMessage("修改成功");
    }

    /**
     * 批量启用部门
     */
    @PostMapping("/batchenable")
    public ResultDto<Object> batchEnable(@RequestBody @NotNull String[] ids) {
        adminSmDptService.batchEnable(ids);
        return ResultDto.successMessage("启用成功");
    }

    /**
     * 批量停用部门
     */
    @PostMapping("/batchdisable")
    public ResultDto<Object> batchDisable(@RequestBody @NotNull String[] ids) {
        adminSmDptService.batchDisable(ids);
        return ResultDto.successMessage("停用成功");
    }

    /**
     * 批量删除
     */
    @PostMapping("/batchdelete")
    public ResultDto<Object> batchDelete(@RequestBody @NotNull String[] ids) {
        adminSmDptService.batchDelete(ids);
        return ResultDto.successMessage("删除成功");
    }

    /**
     * 部门用户
     */
    @GetMapping("/memberlist")
    public ResultDto<Object> memberList(AdminSmUserQuery query) {
        Page<AdminSmUserVo> dptUserVoList = adminSmDptService.memberPage(query);
        return ResultDto.success(dptUserVoList);
    }

}
