package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 机构树查询实体
 */
@Data
@NoArgsConstructor
public class AdminSmOrgTreeQuery {
    private String orgId;
    private AvailableStateEnum orgSts;
}
