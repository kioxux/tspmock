package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.GetUserInfoByDutyCodeDto;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmDutyEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmDutyQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmDutyUserQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmDutyVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmUserDutyRelVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * 系统岗位表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-12-01 21:55:19
 */
public interface AdminSmDutyService extends IService<AdminSmDutyEntity> {

    Page<AdminSmDutyVo> queryPage(AdminSmDutyQuery query);

    /**
     * 条件分页查询岗位下所有用户
     *
     * @param query
     * @return
     */
    Page<AdminSmUserDutyRelVo> memberPage(AdminSmDutyUserQuery query);

    /**
     * 批量停用岗位
     *
     * @param ids
     * @return
     */
    void batchDisable(String[] ids);

    /**
     * 批量启用岗位
     *
     * @param ids
     * @return
     */
    void batchEnable(String[] ids);


    /**
     * 批量删除
     *
     * @return R
     */
    void batchDelete(String[] ids);

    /**
     * 工作流获取岗位信息
     *
     * @param query
     * @return
     */
    Page<AdminSmDutyVo> getDutysForWf(AdminSmDutyQuery query);

    List<String> getDutysByUserIdForWf(String userId);
}

