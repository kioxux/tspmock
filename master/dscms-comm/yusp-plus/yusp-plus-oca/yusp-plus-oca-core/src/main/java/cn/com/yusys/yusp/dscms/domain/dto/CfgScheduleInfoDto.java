package cn.com.yusys.yusp.dscms.domain.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgScheduleInfo
 * @类描述: cfg_schedule_info数据实体类
 * @功能描述:
 * @创建人: 18301
 * @创建时间: 2021-04-23 10:05:12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgScheduleInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 业务流水号 **/
	private String serno;

	/** 时间段类型 **/
	private String scheduleTimeYype;

	/** 上班时间 **/
	private String startTime;

	/** 下班时间 **/
	private String endTime;

	/** 状态 **/
	private String status;

	/** 登记人 **/
	private String inputId;

	/** 登记机构 **/
	private String inputBrId;

	/** 登记日期 **/
	private String inputDate;

	/** 最后修改人 **/
	private String updId;

	/** 最后修改机构 **/
	private String updBrId;

	/** 最后修改日期 **/
	private String updDate;

	/** 操作类型  STD_OPR_TYPE **/
	private String oprType;


	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}

	/**
	 * @return Serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param scheduleTimeYype
	 */
	public void setScheduleTimeYype(String scheduleTimeYype) {
		this.scheduleTimeYype = scheduleTimeYype == null ? null : scheduleTimeYype.trim();
	}

	/**
	 * @return ScheduleTimeYype
	 */
	public String getScheduleTimeYype() {
		return this.scheduleTimeYype;
	}

	/**
	 * @param startTime
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime == null ? null : startTime.trim();
	}

	/**
	 * @return StartTime
	 */
	public String getStartTime() {
		return this.startTime;
	}

	/**
	 * @param endTime
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime == null ? null : endTime.trim();
	}

	/**
	 * @return EndTime
	 */
	public String getEndTime() {
		return this.endTime;
	}

	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}

	/**
	 * @return Status
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}

	/**
	 * @return InputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}

	/**
	 * @return InputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}

	/**
	 * @return InputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}

	/**
	 * @return UpdId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}

	/**
	 * @return UpdBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}

	/**
	 * @return UpdDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}

	/**
	 * @return OprType
	 */
	public String getOprType() {
		return this.oprType;
	}


}
