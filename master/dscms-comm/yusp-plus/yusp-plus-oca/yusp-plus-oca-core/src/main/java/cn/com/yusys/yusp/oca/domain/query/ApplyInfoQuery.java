package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.oca.domain.entity.ApplyInfoEntity;
import cn.com.yusys.yusp.common.query.PageQuery;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ApplyInfoQuery extends PageQuery<ApplyInfoEntity> {

    private String applyId;

    private String cusName;

    private String certType;
}

