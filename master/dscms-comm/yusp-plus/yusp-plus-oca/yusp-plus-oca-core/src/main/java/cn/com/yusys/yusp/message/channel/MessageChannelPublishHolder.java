package cn.com.yusys.yusp.message.channel;

import cn.com.yusys.yusp.message.config.MessageConstants;
import cn.com.yusys.yusp.message.enumeration.MessageChannelEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 消息发布策略持有者
 *
 * @author xiaodg@yusys.com.cn
 **/
@Component
public class MessageChannelPublishHolder {

    @Autowired
    private Map<String, MessageChannelPublisher> messageChannelPublishers;

    /**
     * 根据消息发送渠道的名称获取消息发送的实现类
     * <p>
     * 此处的约定为:
     * 自定义的发送渠道名称+MessageConstants.MESSAGE_CHANNEL_PUBLISHER_SUFFIX作为发送渠道实现类的Bean名称
     * 如: 发送的渠道为system, 则其对应的发送消息的Bean名称为<pre>systemMessageChannelPublisher</pre>
     *
     * @param messageChannelName 渠道名称, 对应于 {@link MessageChannelEnum}
     * @return {@link MessageChannelPublisher}
     */
    public MessageChannelPublisher of(String messageChannelName) {
        return messageChannelPublishers.get(messageChannelName + MessageConstants.MESSAGE_CHANNEL_PUBLISHER_SUFFIX);
    }
}
