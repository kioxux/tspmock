package cn.com.yusys.yusp.oca.dao;

import cn.com.yusys.yusp.oca.domain.entity.AdminSmLogEntity;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmLogPojo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmLogVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统操作日志表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-02 22:18:19
 */

public interface AdminSmLogDao extends BaseMapper<AdminSmLogEntity> {

    IPage<AdminSmLogVo> pageLogByCondition(Page<AdminSmLogVo> page, @Param(Constants.WRAPPER) QueryWrapper<AdminSmLogVo> queryWrapper);
}
