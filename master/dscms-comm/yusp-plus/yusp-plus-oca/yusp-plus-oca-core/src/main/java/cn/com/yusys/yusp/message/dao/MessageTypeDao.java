package cn.com.yusys.yusp.message.dao;

import cn.com.yusys.yusp.message.entity.MessageTypeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 消息类型
 *
 * @author xiaodg@yusys.com.cn
 */

public interface MessageTypeDao extends BaseMapper<MessageTypeEntity> {

}
