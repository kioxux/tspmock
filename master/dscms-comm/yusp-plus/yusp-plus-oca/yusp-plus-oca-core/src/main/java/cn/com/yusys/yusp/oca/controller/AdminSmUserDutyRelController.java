package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.GetUserInfoByDutyCodeDto;
import cn.com.yusys.yusp.oca.service.AdminSmUserDutyRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 用户角色关联表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-12-01 21:55:19
 */
@RestController
@RequestMapping("/api/adminsmuserdutyrel")
public class AdminSmUserDutyRelController {
    @Autowired
    private AdminSmUserDutyRelService adminSmUserDutyRelService;

    /**
     * 查询角色下的用户-提供给后端
     */
    @PostMapping("/getuserlist")
    public ResultDto<List<AdminSmUserDto>> getUserList(@RequestBody GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto) {
        List<AdminSmUserDto> adminSmUserDtos = adminSmUserDutyRelService.getUserList(getUserInfoByDutyCodeDto);
        return ResultDto.success(adminSmUserDtos);
    }

    /**
     * 查询角色下的用户-提供给后端
     * ResultDto中会多返回从Redis中翻译字段,后端转换异常,直接返回List
     */
    @PostMapping("/getuserlistnew")
    public List<AdminSmUserDto> getUserListNew(@RequestBody GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto) {
        List<AdminSmUserDto> adminSmUserDtos = adminSmUserDutyRelService.getUserList(getUserInfoByDutyCodeDto);
        return adminSmUserDtos;
    }
}