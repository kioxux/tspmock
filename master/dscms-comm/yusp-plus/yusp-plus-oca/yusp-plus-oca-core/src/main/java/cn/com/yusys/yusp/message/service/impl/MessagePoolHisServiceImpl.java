package cn.com.yusys.yusp.message.service.impl;

import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.message.dao.MessagePoolHisDao;
import cn.com.yusys.yusp.message.entity.MessagePoolEntity;
import cn.com.yusys.yusp.message.entity.MessagePoolHisEntity;
import cn.com.yusys.yusp.message.query.MessagePoolHisPageQuery;
import cn.com.yusys.yusp.message.service.MessagePoolHisService;
import cn.com.yusys.yusp.message.service.MessagePoolService;
import cn.com.yusys.yusp.message.vo.MessagePoolHisVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 消息历史服务类
 *
 * @author xiaodg@yusys.com.cn
 */
@Service
@Slf4j
public class MessagePoolHisServiceImpl extends ServiceImpl<MessagePoolHisDao, MessagePoolHisEntity> implements MessagePoolHisService {

    @Autowired
    private MessagePoolService messagePoolService;

    private MessagePoolHisVo convert(MessagePoolHisEntity messagePoolHisEntity) {
        return BeanUtils.beanCopy(messagePoolHisEntity, MessagePoolHisVo.class);
    }

    @Override
    public IPage<MessagePoolHisVo> queryPage(MessagePoolHisPageQuery messagePoolHisQuery) {
        return this.lambdaQuery()
                .eq(StringUtils.isNotBlank(messagePoolHisQuery.getUserNo()), MessagePoolHisEntity::getUserNo, messagePoolHisQuery.getUserNo())
                .eq(StringUtils.isNotBlank(messagePoolHisQuery.getState()), MessagePoolHisEntity::getState, messagePoolHisQuery.getState())
                .eq(StringUtils.isNotBlank(messagePoolHisQuery.getSendTime()), MessagePoolHisEntity::getSendTime, messagePoolHisQuery.getSendTime())
                .page(messagePoolHisQuery)
                .convert(this::convert);
    }

    @Override
    public void recordMessagePoolHis(String sendState, MessagePoolEntity messagePoolEntity) {
        // 构建消息池历史对象
        MessagePoolHisEntity messagePoolHisEntity = BeanUtils.beanCopy(messagePoolEntity, MessagePoolHisEntity.class);
        messagePoolHisEntity.setState(sendState);
        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DateUtils.PATTERN_DATETIME);
        messagePoolHisEntity.setSendTime(dateTimeFormatter.format(LocalDateTime.now()));
        // 记录历史
        this.save(messagePoolHisEntity);
        // 移除数据库中的消息队列
        messagePoolService.remove(messagePoolService.lambdaQuery().eq(MessagePoolEntity::getPkNo, messagePoolEntity.getPkNo()).getWrapper());
    }

}