package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmDataAuthTmplEntity;
import cn.com.yusys.yusp.oca.domain.form.AdminSmDataAuthTmplForm;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmDataAuthTmplVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmDataTmplVo;
import cn.com.yusys.yusp.oca.service.AdminSmDataAuthTmplService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 数据权限模板表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-08 18:53:56
 */
@RestController
@RequestMapping("/api/adminsmdataauthtmpl")
public class AdminSmDataAuthTmplController {

    @Autowired
    private AdminSmDataAuthTmplService adminSmDataAuthTmplService;

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @PostMapping("/deletes")
    public ResultDto batchDelete(@RequestBody String[] ids) {
        List<AdminSmDataTmplVo> tmplVoList = adminSmDataAuthTmplService.deleteTmpl(ids);
        if (tmplVoList == null && tmplVoList.size() == 0) {
            return ResultDto.successMessage("删除数据权限模板成功");
        } else {
            return ResultDto.successMessage("数据模板关联了控制点，无法删除！").data(tmplVoList);
        }
    }

    /**
     * 修改模板
     * @param adminSmDataAuthTmplEntity
     * @return
     */
    @PostMapping("/updates")
    public ResultDto updateTmpl(@RequestBody AdminSmDataAuthTmplEntity adminSmDataAuthTmplEntity) {
        adminSmDataAuthTmplService.updateTmpl(adminSmDataAuthTmplEntity);
        return ResultDto.success("数据权限模板修改");
    }

    /**
     * 新增模板
     * @param adminSmDataAuthTmplEntity
     * @return
     */
    @PostMapping("/add")
    public ResultDto addAuthTemplate(@RequestBody AdminSmDataAuthTmplEntity adminSmDataAuthTmplEntity) {
        AdminSmDataAuthTmplEntity entity = adminSmDataAuthTmplService.addAuthTemplate(adminSmDataAuthTmplEntity);
        return ResultDto.success(entity);
    }

    /**
     * 查询列表
     * @param adminSmDataAuthTmplForm
     * @return
     */
    @GetMapping("/list")
    public ResultDto list(AdminSmDataAuthTmplForm adminSmDataAuthTmplForm){
        Page<AdminSmDataAuthTmplVo> page = adminSmDataAuthTmplService.queryPage(adminSmDataAuthTmplForm);
        return ResultDto.success(page.getRecords()).total(page.getTotal());
    }

    /**
     * 修改之前查询模板信息
     * @param authTmplId
     * @return
     */
    @GetMapping("/info/{authTmplId}")
    public ResultDto getInfo(@PathVariable("authTmplId") String authTmplId){
		AdminSmDataAuthTmplEntity entity = adminSmDataAuthTmplService.getInfo(authTmplId);
        return ResultDto.success(entity);
    }

    /**
     * 获取已关联的数据模板
     * @param contrId
     * @return
     */
    @GetMapping("/associated/{contrId}")
    public ResultDto getByContrId(@PathVariable("contrId") String contrId) {
        return ResultDto.success(adminSmDataAuthTmplService.getByContrId(contrId));
    }
}
