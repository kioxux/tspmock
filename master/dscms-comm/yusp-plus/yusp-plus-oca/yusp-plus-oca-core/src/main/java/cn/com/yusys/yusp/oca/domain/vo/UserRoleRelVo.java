package cn.com.yusys.yusp.oca.domain.vo;

import lombok.Data;

/**
 * 用户所在机构可关联的角色列表以及实际关联状态
 */
@Data
public class UserRoleRelVo {
    private String roleId;
    private String roleCode;
    private String roleName;
    private Boolean checked=false;
}
