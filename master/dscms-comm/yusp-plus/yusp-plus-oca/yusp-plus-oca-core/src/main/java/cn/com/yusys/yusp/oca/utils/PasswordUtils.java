package cn.com.yusys.yusp.oca.utils;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.encrypt.BCRSAUtils;
import cn.com.yusys.yusp.oca.domain.constants.MessageEnums;
import cn.com.yusys.yusp.oca.domain.constants.ResponseAndMessageEnum;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


/**
 * @program: yusp-app-framework
 * @description: 密码工具类
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2020-11-04 09:18
 */
@Slf4j
@Component
public class PasswordUtils {

    @Autowired
    AdminSmUserService adminSmUserService;

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    I18nMessageByCode i18nMessageByCode;


    /**
     * 检查密码是否正确
     *
     * @param rawPassword
     * @param loginCode
     * @return
     */
    public boolean matchUserPassword(String rawPassword, String loginCode) {

        AdminSmUserEntity userEntity = adminSmUserService.getOne(new QueryWrapper<AdminSmUserEntity>().eq("LOGIN_CODE", loginCode));
        if (userEntity != null) {
            String passwordDb = userEntity.getUserPassword();
            return checkSecret(passwordDb, rawPassword);
        } else {
            log.error("用户不存在");
            throw BizException.error(null,ResponseAndMessageEnum.NON_USER.getCode(),i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.NON_USER.getCode()));
        }
    }

    /**
     * 校验密码
     *
     * @param dbPassword
     * @param rawPassword
     * @return
     */
    public boolean checkSecret(String dbPassword, String rawPassword) {
        //密码校验
        try {
            //密码已密钥加密，先解密
            rawPassword = dePassword(rawPassword);
            //密码匹配
            return passwordEncoder.matches(rawPassword, dbPassword);
        } catch (Exception e) {
            log.error("密码校验失败");
            return false;
        }
    }

    /**
     * 密文解密
     *
     * @param password
     * @return
     */
    public String dePassword(String password) {
        try {
            password = BCRSAUtils.decryptByPrivate(password);
            return password;
        } catch (Exception e) {
            log.error("密码解密失败");
            throw BizException.error(null,MessageEnums.PASSWORD_RESOLUTION_FAILED.getCode(),i18nMessageByCode.getMessageByCode(MessageEnums.PASSWORD_RESOLUTION_FAILED.getCode()));
        }
    }

    /**
     * 密文加密
     *
     * @param password
     * @return
     */
    public String enPassword(String password) {
        try {
            String cipherText = BCRSAUtils.decryptByPrivate(password);
            password = new BCryptPasswordEncoder().encode(cipherText);
            return password;
        } catch (Exception e) {
            log.error("密码加密失败");
            throw BizException.error(null,MessageEnums.PASSWORD_ENCODE_FAILED.getCode(),i18nMessageByCode.getMessageByCode(MessageEnums.PASSWORD_ENCODE_FAILED.getCode()));
        }
    }


//    public static void main(String[] args) {
//        try {
//            String cipherText = BCRSAUtils.decryptByPrivate("kB480BK3ynB6BtMPm/9P29suhU4TmXDgL24US8w2l7+OLNm2IUBHpzkiZPpnlEYZp+GYjh09Tz9VzVPSgmWpz4hl56ttk7XTX5d1kV7JlgODiUUBs+Ctc5MUZAW92dpLnZg53nYNEHGAqrtc79Odqs1UM8MuPAcOXbuk5HewkEI=");
//            System.out.println(cipherText);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
