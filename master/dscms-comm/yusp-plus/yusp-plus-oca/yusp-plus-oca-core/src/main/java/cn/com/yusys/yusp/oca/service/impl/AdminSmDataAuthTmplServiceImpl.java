package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.oca.dao.AdminSmDataAuthTmplDao;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmAuthRecoEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmDataAuthTmplEntity;
import cn.com.yusys.yusp.oca.domain.form.AdminSmDataAuthTmplForm;
import cn.com.yusys.yusp.oca.domain.form.AdminSmDataTmplConditionForm;
import cn.com.yusys.yusp.oca.domain.vo.*;
import cn.com.yusys.yusp.oca.service.AdminSmAuthRecoService;
import cn.com.yusys.yusp.oca.service.AdminSmDataAuthService;
import cn.com.yusys.yusp.oca.service.AdminSmDataAuthTmplService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service("adminSmDataAuthTmplService")
public class AdminSmDataAuthTmplServiceImpl extends ServiceImpl<AdminSmDataAuthTmplDao, AdminSmDataAuthTmplEntity> implements AdminSmDataAuthTmplService {

    public static final QueryWrapper<AdminSmDataAuthTmplEntity> wrapper = new QueryWrapper<>();
    @Autowired
    private AdminSmDataAuthService adminSmDataAuthService;
    @Autowired
    private AdminSmAuthRecoService adminSmAuthRecoService;

    /**
     * 查询模板
     *
     * @param adminSmDataAuthTmplForm
     * @return
     */
    @Override
    public Page<AdminSmDataAuthTmplVo> queryPage(AdminSmDataAuthTmplForm adminSmDataAuthTmplForm) {
        String keyWord = adminSmDataAuthTmplForm.getKeyWord();
        String authTmplName = adminSmDataAuthTmplForm.getAuthTmplName();
        String sqlName = adminSmDataAuthTmplForm.getSqlName();
        // 组装查询条件
        Page<AdminSmDataAuthTmplVo> page = new Page<>(adminSmDataAuthTmplForm.getPage(), adminSmDataAuthTmplForm.getSize());
        QueryWrapper<AdminSmDataAuthTmplVo> queryWrapper = new QueryWrapper<>();

        if (StringUtils.isEmpty(keyWord)) {
            queryWrapper.like(!StringUtils.isEmpty(authTmplName), "auth_tmpl_name", authTmplName);
            queryWrapper.like(!StringUtils.isEmpty(sqlName), "sql_name", sqlName);
        } else {
            queryWrapper.like("auth_tmpl_name", keyWord).or();
            queryWrapper.like("sql_name", keyWord);
        }
        queryWrapper.orderByDesc("last_chg_dt");
        // 组装page分页对象
        Page<AdminSmDataAuthTmplVo> pageResult = this.baseMapper.selectByCondition(page, queryWrapper);
        return pageResult;
    }

    /**
     * 批量删除
     * 同时删除admin_sm_data_auth关联表中的记录
     *
     * @param ids
     */
    @Override
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public List<AdminSmDataTmplVo> deleteTmpl(String[] ids) {
        if (ids == null || ids.length == 0) {
            return null;
        }
        List<String> idList = new ArrayList<>(Arrays.asList(ids));
        /**
         * 查询删除的模板是否和控制点有关联
         * 如果有，已经关联的模板不能删除
         */
        List<AdminSmDataTmplVo> tmplVoList = adminSmDataAuthService.selectWithTmplIds(idList);
        for (int i = 0; i < tmplVoList.size(); i++) {
            String authTmplId = tmplVoList.get(i).getAuthTmplId();
            idList.remove(authTmplId);
        }

        if (idList != null && idList.size() > 0) {
            this.baseMapper.deleteBatchIds(idList);
        }
        return tmplVoList;
    }

    /**
     * 修改模板
     *
     * @param adminSmDataAuthTmplEntity
     */
    @Override
    public void updateTmpl(AdminSmDataAuthTmplEntity adminSmDataAuthTmplEntity) {
        adminSmDataAuthTmplEntity.setLastChgUsr(SessionUtils.getUserId());
        this.updateById(adminSmDataAuthTmplEntity);
    }

    /**
     * 新增模板
     *
     * @param adminSmDataAuthTmplEntity
     * @return
     */
    @Override
    public AdminSmDataAuthTmplEntity addAuthTemplate(AdminSmDataAuthTmplEntity adminSmDataAuthTmplEntity) {
        adminSmDataAuthTmplEntity.setAuthTmplId(StringUtils.getUUID());
        adminSmDataAuthTmplEntity.setStatus(0);
        adminSmDataAuthTmplEntity.setLastChgUsr(SessionUtils.getUserId());
        this.save(adminSmDataAuthTmplEntity);
        return adminSmDataAuthTmplEntity;
    }

    /**
     * 修改之前，查询模板信息
     *
     * @param authTmplId
     * @return
     */
    @Override
    public AdminSmDataAuthTmplEntity getInfo(String authTmplId) {
        return this.getById(authTmplId);
    }

    /**
     * 权限授权树数据查询，联合admin_sm_auth_reco表及admin_sm_data_auth表
     *
     * @param id
     * @return
     */
    @Override
    public List<AdminSmAuthTreeVo> selectAuthTree(String id) {
        List<AdminSmAuthTreeVo> treeVoList = this.baseMapper.selectAuthTree(id);
        return treeVoList;
    }

    /**
     * 查询控制点已经关联的模板
     *
     * @param contrId
     * @return
     */
    @Override
    public List<AdminDataAuthVo> associatedTmpl(String contrId) {
        QueryWrapper<AdminDataAuthVo> wrapper = new QueryWrapper<>();
        wrapper.eq("contr_id", contrId);
        return this.baseMapper.selectAssociatedTmpl(wrapper);
    }


    /**
     * 控制点关联查询
     *
     * @param form
     * @return 分页数据
     */
    @Override
    public IPage getListForContr(AdminSmDataTmplConditionForm form) {
        //分页参数
        IPage<AdminSmDataTmplListVo> page = new Page<>(form.getPage(), form.getSize());
        //查询条件
        QueryWrapper<AdminSmDataTmplListVo> wrapper = new QueryWrapper<>();
        wrapper.and(!StringUtils.isEmpty(form.getKeyWord()),
                w -> w.like("auth_tmpl_name", form.getKeyWord())
                        .or().like("sql_name", form.getKeyWord())
                        .or().like("sql_string", form.getKeyWord()));
        wrapper.eq(form.getCheck() == 1, "a.CONTR_ID", form.getContrId());
        //分页查询
        baseMapper.selectAllTmpl(page, wrapper, form.getContrId(), form.getCheck());

        //结果中是否已关联，已授权
        page.getRecords().stream().forEach(temp -> {
            if (form.getContrId().equals(temp.getContrId())) {
                //控制点与模板已关联
                temp.setMark(1);
                //查询是否已授权
                List<AdminSmAuthRecoEntity> authRecoEntity = adminSmAuthRecoService.list(new QueryWrapper<AdminSmAuthRecoEntity>()
                        .eq("MENU_ID", form.getContrId())
                        .eq("AUTHRES_ID", temp.getAuthTmplId()));
                if (CollectionUtils.nonEmpty(authRecoEntity)) {
                    temp.setAuthRecoId(authRecoEntity.get(0).getAuthRecoId());
                }
            } else {
                //控制点与模板未关联
                temp.setMark(0);
            }
        });
        return page;
    }

    /**
     * 获取与控制点绑定的所有数据模板
     *
     * @param stringList
     * @return
     */
    @Override
    public List<AdminDataAuthVo> associatedTmplList(List<String> stringList) {
        QueryWrapper<AdminDataAuthVo> wrapper = new QueryWrapper<>();
        wrapper.in("a.CONTR_ID", stringList);
        return baseMapper.associatedTmplList(wrapper);
    }

    /**
     * 获取控制点已关联的数据模板
     *
     * @param contrId
     * @return
     */
    @Override
    public List<AdminSmDataAuthTmplEntity> getByContrId(String contrId) {
        return baseMapper.selectByContrId(contrId);
    }

    /**
     * 数据模板授权页面列表数据
     *
     * @param contrIdList
     * @return
     */
    @Override
    public List<AdminTmplAndRecoVo> getTmplAndRecoVoList(List<String> contrIdList, String authobjId) {
        QueryWrapper<AdminTmplAndRecoVo> wrapper = new QueryWrapper<>();
        wrapper.in("r.menu_id", contrIdList);
        wrapper.eq("r.authobj_id", authobjId);
        return baseMapper.getTmplAndRecoVoList(wrapper);
    }

    /**
     * 关联DataAuth表，使用contrId查询，返回AdminSmDataTmplListVo
     *
     * @param contrId
     * @return
     */
    @Override
    public List<AdminSmDataTmplListVo> getByContrIdForVo(String contrId) {
        return baseMapper.getByContrIdForVo(contrId);
    }
}