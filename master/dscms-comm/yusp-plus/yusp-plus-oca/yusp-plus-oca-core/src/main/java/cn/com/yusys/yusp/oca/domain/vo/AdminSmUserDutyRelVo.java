package cn.com.yusys.yusp.oca.domain.vo;

import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author danyu
 */
@Data
@TableName("admin_sm_user_duty_rel")
public class AdminSmUserDutyRelVo {
    @TableId()
    private String dutyId;
    private String orgId;
    private String orgName;
    private String loginCode;
    private String userName;
    private String userCode;
    private AvailableStateEnum userSts;

}
