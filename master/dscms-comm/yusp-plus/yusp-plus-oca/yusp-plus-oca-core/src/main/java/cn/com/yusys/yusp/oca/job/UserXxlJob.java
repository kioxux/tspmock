package cn.com.yusys.yusp.oca.job;

import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: yusp-plus
 * @description: 用户信息相关定时任务
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2021-01-21 17:29
 */
@Component
public class UserXxlJob {

    @Autowired
    AdminSmUserService adminSmUserService;

    /**
     * 根据用户截至时间来改变用户状态
     *
     * @param param 执行参数
     * @return
     * @throws Exception
     */
    public void updateUserStatus(String param) throws Exception {
        //查询非禁用所有用户
        //创建查询条件wrapper
        QueryWrapper<AdminSmUserEntity> query = new QueryWrapper<>();
        List<AdminSmUserEntity> adminSmUserEntities = adminSmUserService.list(query.ne("USER_STS", AvailableStateEnum.DISABLED).isNotNull("DEADLINE"));

        //截至时间到了即更新用户状态为禁用状态
        Date date = new Date();
        List<AdminSmUserEntity> collect = adminSmUserEntities.stream().filter(item -> item.getDeadline().before(date))
                .map(item -> {
                    item.setUserSts(AvailableStateEnum.DISABLED);
                    return item;
                }).collect(Collectors.toList());

        //更新状态更改的用户
        adminSmUserService.updateBatchById(collect);
    }

}