package cn.com.yusys.yusp.oca.dao;

import cn.com.yusys.yusp.oca.domain.entity.AdminSmMessageEntity;
import cn.com.yusys.yusp.commons.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 提示信息管理表
 * 
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-16 11:19:42
 */

public interface AdminSmMessageDao extends BaseMapper<AdminSmMessageEntity> {
}
