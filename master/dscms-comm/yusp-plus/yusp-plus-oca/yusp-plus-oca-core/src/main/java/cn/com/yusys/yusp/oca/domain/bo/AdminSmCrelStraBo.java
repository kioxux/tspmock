package cn.com.yusys.yusp.oca.domain.bo;

import cn.com.yusys.yusp.oca.domain.entity.AdminSmCrelStraEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @类名称: AdminSmCrelStraVo
 * @类描述: 认证策略vo
 * @创建人: xufy1@yusys.com.cn
 * @创建时间: 2020-11-24 16:58
 * @修改备注:
 * @修改日期 修改人员    修改原因
 * ----------  ---------  -----------------------------
 * @Version 1.0.0
 * @Copyright (c) 2018宇信科技-版权所有
 */
public class AdminSmCrelStraBo implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<AdminSmCrelStraEntity> crelList = new ArrayList<AdminSmCrelStraEntity>();

    public List<AdminSmCrelStraEntity> getCrelList() {
        return crelList;
    }

    public void setCrelList(List<AdminSmCrelStraEntity> crelList) {
        this.crelList = crelList;
    }
}
