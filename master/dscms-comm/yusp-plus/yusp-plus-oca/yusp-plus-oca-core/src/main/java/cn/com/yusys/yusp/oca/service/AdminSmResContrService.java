package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.commons.session.user.Control;
import cn.com.yusys.yusp.commons.session.user.impl.ControlImpl;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmResContrEntity;
import cn.com.yusys.yusp.oca.domain.form.AdminSmMenuConditionForm;
import cn.com.yusys.yusp.oca.domain.form.AdminSmResContrForm;
import cn.com.yusys.yusp.oca.domain.form.AdminSmResContrSaveForm;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmAuthTreeVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmContrTreeVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmMenuVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmResContrVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 系统功能控制点表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-19 16:38:48
 */
public interface AdminSmResContrService extends IService<AdminSmResContrEntity> {

    List<ControlImpl> getAdminSmContr(String userId);

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 查询控制点列表
     *
     * @param adminSmResContrForm
     * @return
     */
    Page<AdminSmResContrVo> queryPageWithCondition(AdminSmResContrForm adminSmResContrForm);

    /**
     * 获取业务功能树
     *
     * @return
     */
    List<AdminSmContrTreeVo> getFuncTree();

    /**
     * 删除控制点
     *
     * @param ids
     */
    void deleteContr(String[] ids);

    /**
     * 验证contrCode是否重复
     *
     * @param adminSmResContrForm
     * @return
     */
    int checkCode(AdminSmResContrForm adminSmResContrForm);

    /**
     * 新增控制点
     *
     * @param adminSmResContrSaveForm
     */
    void createContr(AdminSmResContrSaveForm adminSmResContrSaveForm);

    /**
     * 修改控制点
     *
     * @param adminSmResContrSaveForm
     */
    void updateContr(AdminSmResContrSaveForm adminSmResContrSaveForm);

    /**
     * 查询权限授权树
     *
     * @param id
     * @return
     */
    List<AdminSmAuthTreeVo> selectAuthTree(String id);

    /**
     * 分页查询权限表中的控制点数据
     *
     * @param id
     * @param page
     * @param size
     * @return
     */
    IPage<AdminSmAuthTreeVo> selectAuthTreePage(String id, String keyWord, int page, int size);

    /**
     * 控制点关联数据权限模板操作
     *
     * @param adminSmResContrSaveForm
     */
    String relationTmpl(AdminSmResContrSaveForm adminSmResContrSaveForm, boolean createOrUpdate);

    /**
     * 查询控制点关联菜单列表
     *
     * @param adminSmMenuConditionForm
     * @return
     */
    Page<AdminSmMenuVo> getMenuList(AdminSmMenuConditionForm adminSmMenuConditionForm);

    /**
     * 缓存中添加 userId 现有的控制点
     *
     * @return
     */
    List<? extends Control> selectControlImplList();
}