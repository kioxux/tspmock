package cn.com.yusys.yusp.sequence.domain.constants;

public class Constants {
    public static class SequenceConfigConstance {
        /**
         * 是否--是
         */
        public static final String YES = "Y";

        /**
         * 是否--否
         */
        public static final String NO = "N";
    }

}
