package cn.com.yusys.yusp.message.service.impl;

import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.message.dao.MessagePoolDao;
import cn.com.yusys.yusp.message.entity.MessagePoolEntity;
import cn.com.yusys.yusp.message.query.MessagePoolPageQuery;
import cn.com.yusys.yusp.message.service.MessagePoolService;
import cn.com.yusys.yusp.message.vo.MessagePoolVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 消息队列服务类
 *
 * @author xiaodg@yusys.com.cn
 */
@Service
public class MessagePoolServiceImpl extends ServiceImpl<MessagePoolDao, MessagePoolEntity> implements MessagePoolService {

    private MessagePoolVo convert(MessagePoolEntity messagePoolEntity) {
        return BeanUtils.beanCopy(messagePoolEntity, MessagePoolVo.class);
    }

    @Override
    public IPage<MessagePoolVo> queryPage(MessagePoolPageQuery messagePoolQuery) {
        return this.lambdaQuery()
                .eq(StringUtils.isNotBlank(messagePoolQuery.getUserNo()), MessagePoolEntity::getUserNo, messagePoolQuery.getUserNo())
                .eq(StringUtils.isNotBlank(messagePoolQuery.getChannelType()), MessagePoolEntity::getChannelType, messagePoolQuery.getChannelType())
                .eq(StringUtils.isNotBlank(messagePoolQuery.getMessageType()), MessagePoolEntity::getMessageType, messagePoolQuery.getMessageType())
                .page(messagePoolQuery)
                .convert(this::convert);
    }

    @Override
    public List<MessagePoolEntity> findByPkHashRange(int pkHashMin, int pkHashMax) {
        return this.lambdaQuery()
                .between(MessagePoolEntity::getPkHash, pkHashMin, pkHashMax)
                .list();
    }


}