package cn.com.yusys.yusp.message.async;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 发送邮件线程池
 *
 * @author xiaodg@yusys.com.cn
 **/
@Configuration
@ConditionalOnProperty(name = "spring.mail.enabled", havingValue = "true")
public class EmailSendExecutorPoolConfig {
    /**
     * 发送Email线程池的核心线程数
     * 默认为8个线程
     */
    @Value("${yusp.message.email-send-pool.core-pool-size: 8}")
    private int corePoolSize;

    /**
     * 发送Email线程池的最大线程数
     * 默认为20个线程
     */
    @Value("${yusp.message.email-send-pool.max-pool-size: 20}")
    private int maxPoolSize;

    /**
     * 发送Email线程池的最大队列容量
     */
    @Value("${yusp.message.email-send-pool.queue-capacity: 1000}")
    private int queueCapacity;


    /**
     * 发送Email的线程池配置
     *
     * @return Executor
     */
    @Bean
    public Executor emailSendExecutor() {
        ThreadPoolTaskExecutor emailSendExecutor = new ThreadPoolTaskExecutor();
        emailSendExecutor.setCorePoolSize(corePoolSize);
        emailSendExecutor.setMaxPoolSize(maxPoolSize);
        emailSendExecutor.setQueueCapacity(queueCapacity);
        emailSendExecutor.setThreadNamePrefix("EmailSend-Executor-");
        // 当Pool已经达到max pool size后，不在新线程中执行任务，直接使用调用者所在的线程来执行
        emailSendExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        emailSendExecutor.initialize();
        return emailSendExecutor;
    }

}
