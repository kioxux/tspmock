package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 额度信息表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-12-02 18:03:52
 */
@Data
@TableName("cms_limit_info")
public class LimitInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 额度信息Id
	 */
	@TableId
	private String limitId;
	/**
	 * 客户名称
	 */
	private String cusName;
	/**
	 * 证件类型(10110:居民身份证,10119:组织机构代码,20150:护照,99099:其他证件)
	 */
	private String certType;
	/**
	 * 证件号码
	 */
	private String certCode;
	/**
	 * 预授信额度
	 */
	private BigDecimal preCreditLimit;
	/**
	 * 实际额度
	 */
	private BigDecimal actualLimit;
	/**
	 * 已用额度
	 */
	private BigDecimal usedLimit;
	/**
	 * 实际已用额度
	 */
	private BigDecimal actualUsedLimit;
	/**
	 * 额度类型01:非循环，02:循环
	 */
	private String limitType;
	/**
	 * 额度状态00-待审批 01-审批中 02-正常（审批通过） 88-冻结 99-失效 
	 */
	private String limitStatus;
	/**
	 * 额度期限（月）
	 */
	private BigDecimal limitTermsMonth;
	/**
	 * 额度开始日期
	 */
	private Date limitStartDate;
	/**
	 * 插入时间
	 */
	private Date inputTime;
	/**
	 * 插入用户
	 */
	private String inputUser;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 更新用户
	 */
	private String updateUser;
	/**
	 * 额度显示状态(1-逻辑显示，0-逻辑删除)
	 */
	private String logicStatus;

}
