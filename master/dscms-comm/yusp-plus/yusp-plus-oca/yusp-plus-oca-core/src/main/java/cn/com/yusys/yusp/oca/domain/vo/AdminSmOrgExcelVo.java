package cn.com.yusys.yusp.oca.domain.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * 系统机构Excel导入使用该实体
 */
@Data
public class AdminSmOrgExcelVo {

    @ExcelProperty("机构代码")
    private String orgCode;

    @ExcelProperty("机构名称")
    private String orgName;

    @ExcelProperty("机构级别")
    private Integer orgLevel;

    @ExcelProperty("金融机构ID")
    private String instuId;

    @ExcelProperty("上级机构代码")
    private String upOrgId;

    @ExcelProperty("邮编")
    private String zipCde;

    @ExcelProperty("联系人")
    private String contUsr;

    @ExcelProperty("联系电话")
    private String contTel;

    @ExcelProperty("地址")
    private String orgAddr;

}
