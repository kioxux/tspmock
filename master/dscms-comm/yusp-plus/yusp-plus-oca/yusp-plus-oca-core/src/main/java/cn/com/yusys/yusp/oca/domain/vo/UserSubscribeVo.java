package cn.com.yusys.yusp.oca.domain.vo;

import lombok.Data;

/**
 * 消息中心-用户订阅vo
 */
@Data
public class UserSubscribeVo {

    /**
     * 用户id
     */
    String userNo;
    /**
     * 用户名
     */
    String userName;
}
