package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.bo.MenuAndControlAuthRecoSaveBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmAuthRecoEntity;
import cn.com.yusys.yusp.oca.domain.form.AdminTmplAuthForm;
import cn.com.yusys.yusp.oca.domain.vo.AdminDataTmplControlVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminRecoWithTmplVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmAuthRecoVo;
import cn.com.yusys.yusp.oca.domain.vo.MenuVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.HashSet;
import java.util.List;

/**
 * 资源对象授权记录表(含菜单、控制点、数据权限)
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-19 17:43:42
 */
public interface AdminSmAuthRecoService extends IService<AdminSmAuthRecoEntity> {


    List<MenuVo> selectDataPowerTree(String sysId);

    List<AdminSmAuthRecoVo> queryRecoInfo(String objectType, String resType, String objectId, String sysId);

    int adminSmAuthRecoService(List<AdminSmAuthRecoEntity> authRecoList);

    /**
     * 保存角色菜单权限
     *
     * @param authRecoSaveBo
     * @return
     */
    ResultDto saveMenuAndControlAuthReco(MenuAndControlAuthRecoSaveBo authRecoSaveBo);

    /**
     * 批量删除授权记录
     *
     * @param authresIdList
     */
    void deleteByList(List<String> authresIdList);

    /**
     * 批量删除被拷贝角色或用户原有的权限
     *
     * @param ids
     */
    void deleteByAuthObjIds(String[] ids);

    /**
     * 删除数据模板授权记录
     *
     * @param authTmplId
     */
    void deleteByDataAuthId(String authTmplId);

    /**
     * 删除控制点授权，同时删除控制点关联的数据权限模板授权
     *
     * @param contrIdList
     * @param dataAuthIdList
     */
    void deleteByResContrList(List<String> contrIdList, List<String> dataAuthIdList);

    /**
     * 使用authObjId获取该用户的所有权限
     *
     * @param authObjId
     * @return
     */
    List<AdminSmAuthRecoEntity> getByAuthObjId(String authObjId);

    /**
     * 使用authresId获取一个授权记录
     *
     * @param authresId
     * @return
     */
    AdminSmAuthRecoEntity getByAuthresId(String authresId);

    /**
     * 使用授权类型获取授权记录
     *
     * @param d
     * @return
     */
    List<AdminSmAuthRecoEntity> getByAuthresType(String d);

    /**
     * 使用授权对象id和授权资源对象id删除记录
     *
     * @param form
     */
    void removeByMenuIdAndAuthobjId(AdminTmplAuthForm form);

    /**
     * 查询多个数据模板的授权记录
     *
     * @param tmplIdList
     * @return
     */
    List<AdminSmAuthRecoEntity> getByAuthresIds(List<String> tmplIdList);

    /**
     * 使用 menuId 查询授权记录
     *
     * @param contrId
     * @return
     */
    List<AdminRecoWithTmplVo> getByMenuIdWithTmpl(String contrId);

    /**
     * 使用 userId 查询该用户的数据模板授权记录
     *
     * @param userId
     * @return
     */
    List<AdminDataTmplControlVo> getDataTmplControl(String userId);

    /**
     * 批量删除菜单授权记录
     *
     * @param menuIds
     */
    int deleteMenuAuthData(HashSet<String> menuIds);

    /**
     * 批量删除菜单授权记录
     *
     * @param vos
     */
    void batchDelete(List<AdminSmAuthRecoVo> vos);
}

