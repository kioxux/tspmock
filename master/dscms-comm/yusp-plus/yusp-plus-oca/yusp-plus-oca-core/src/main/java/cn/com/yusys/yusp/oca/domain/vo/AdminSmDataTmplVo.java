package cn.com.yusys.yusp.oca.domain.vo;

import lombok.Data;

@Data
public class AdminSmDataTmplVo {

    private String authTmplId;

    private String authTmplName;
}
