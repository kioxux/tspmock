package cn.com.yusys.yusp.oca.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.handler.TableNameHandler;
import com.baomidou.mybatisplus.extension.plugins.inner.DynamicTableNameInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.HashMap;
import java.util.Locale;

/**
 * 注册oca用到的mybatis-plus过滤器
 *
 * @author tanrui1
 * @since 0.0.1
 */
@Configuration
@MapperScan("cn.com.yusys.yusp.oca.dao")
public class MybatisPlusConfig {

    /**
     * 根据当前语言环境选择操作对应的数据字典表
     * @return
     */
    @Bean
    public MybatisPlusInterceptor i18nMPInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        DynamicTableNameInnerInterceptor dynamicTableNameInnerInterceptor = new DynamicTableNameInnerInterceptor();
        HashMap<String, TableNameHandler> map = new HashMap<>(2); //目前只做了数据字典表的国际化，若需实现其他表的国际化,可在map中追加
//        map.put("admin_sm_lookup_dict", (sql, tableName) ->tableName + getTableNameExt());
//        map.put("admin_sm_menu",(sql,tableName)->tableName + getTableNameExt());
//        map.put("admin_sm_message",(sql,tableName)->tableName + getTableNameExt());
//        map.put("admin_sm_crel_stra",(sql,tableName)->tableName + getTableNameExt());
//        map.put("admin_sm_res_contr",(sql,tableName)->tableName + getTableNameExt());
//        map.put("admin_sm_user",(sql,tableName)->tableName + getTableNameExt());
//        map.put("admin_sm_role",(sql,tableName)->tableName + getTableNameExt());
        dynamicTableNameInnerInterceptor.setTableNameHandlerMap(map);
        interceptor.addInnerInterceptor(dynamicTableNameInnerInterceptor);
        return interceptor;
    }

    private String getTableNameExt(){
        String ext="";
        Locale locale= LocaleContextHolder.getLocale();//其实这里没发现过为null的情况
        if(null!=locale&&!Locale.SIMPLIFIED_CHINESE.equals(locale)){//默认中文环境下选没有后缀的表
            ext= "_"+locale.toString().toLowerCase();
        }
        return ext;
    }
}