package cn.com.yusys.yusp.oca.domain.form;

import lombok.Data;

import java.util.List;

@Data
public class AdminSmResContrSaveForm {

    private String contrId;

    private String contrName;

    private String menuId;

    private String funcId;

    private String contrCode;

    private String contrUrl;

    private String contrRemark;

    private String MethodType;
    /**
     * 以上均是控制点相关数据
     * authDataTmplIdList：关联的数据模板 id
     */
    private List<String> authDataTmplIdList;
}
