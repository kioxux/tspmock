package cn.com.yusys.yusp.oca.domain.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 部门树Vo
 */
@Data
public class DptTreeVo {

	/**
	 * 记录编号
	 */
	private String dptId;
	/**
	 * 部门代码
	 */
	private String dptCode;
	/**
	 * 部门名称
	 */
	private String dptName;
	/**
	 * 所属机构编号
	 */
	private String belongOrgId;
	/**
	 * 上级部门记录编号
	 */
	private String upDptId;
	/**
	 * 状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效
	 */
	private String dptSts;
	/**
	 * 最新变更用户
	 */
	private String lastChgUsr;
	/**
	 * 最新变更时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<DptTreeVo> children;

}
