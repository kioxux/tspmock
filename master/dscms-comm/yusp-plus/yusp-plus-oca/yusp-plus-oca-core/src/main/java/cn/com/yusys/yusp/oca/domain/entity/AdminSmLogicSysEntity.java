package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;

/**
 * 系统逻辑系统表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-19 14:30:22
 */
@Data
@TableName("admin_sm_logic_sys")
public class AdminSmLogicSysEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 记录编号
     */
    @TableId(type = IdType.ASSIGN_UUID)
    private String sysId;
    /**
     * 认证类型
     */
    private String authId;
    /**
     * 版本号
     */
    private String sysVersion;
    /**
     * 逻辑系统名称
     */
    private String sysName;
    /**
     * 逻辑系统描述
     */
    private String sysDesc;
    /**
     * 逻辑系统状态
     */
    private String sysSts;
    /**
     * 是否单点登录
     */
    private String isSso;
    /**
     * 系统简称
     */
    private String sysCode;
    /**
     * 国际化key值
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String i18nKey;

}
