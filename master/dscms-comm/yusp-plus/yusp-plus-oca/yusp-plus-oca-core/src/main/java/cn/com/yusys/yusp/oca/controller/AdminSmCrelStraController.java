package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.dto.AdminSmCrelStraDto;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmCrelStraEntity;
import cn.com.yusys.yusp.oca.service.AdminSmCrelStraService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * 认证策略参数表
 *
 * @author wujp4
 * @date 2021-03-30 11:27:32
 */
@Api("认证策略参数表接口")
@RestController
@RequestMapping("/api/adminsmcrelstra")
public class AdminSmCrelStraController {
    @Autowired
    private AdminSmCrelStraService adminSmCrelStraService;

    /**
     * 批量修改认证策略
     *
     * @param adminSmCrelStraDto
     * @return 修改认证策略结果
     */
    @ApiOperation("修改")
    @PostMapping("/update")
    public ResultDto update(@Valid @RequestBody AdminSmCrelStraDto adminSmCrelStraDto) {
        adminSmCrelStraService.updateById(adminSmCrelStraDto);

        return ResultDto.success();
    }

    /**
     * 查询所有认证策略
     */
    @ApiOperation("列表")
    @GetMapping("/getall")
    public ResultDto getall() {
        List<AdminSmCrelStraEntity> list = adminSmCrelStraService.list();

        return ResultDto.success(list);
    }
}
