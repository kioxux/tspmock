package cn.com.yusys.yusp.oca.domain.entity;

import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 系统部门表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-11-12 10:47:26
 */
@Data
@TableName("admin_sm_dpt")
public class AdminSmDptEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String  DEFAULT_UP_DPT_ID="0";

	/**
	 * 记录编号
	 */
	@TableId(type =  IdType.UUID)
	private String dptId;
	/**
	 * 部门代码
	 */
	private String dptCode;
	/**
	 * 部门名称
	 */
	private String dptName;
	/**
	 * 所属机构编号
	 */
	private String orgId;
	/**
	 * 上级部门记录编号
	 */
	private String upDptId;
	/**
	 * 状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效
	 */
	private AvailableStateEnum dptSts;
	/**
	 * 最新变更用户
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
	/**
	 * 最新变更时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@TableField(exist = false)
	private List<AdminSmDptEntity> children;

	public AdminSmDptEntity() {
		this.setUpDptId(DEFAULT_UP_DPT_ID);
	}
}
