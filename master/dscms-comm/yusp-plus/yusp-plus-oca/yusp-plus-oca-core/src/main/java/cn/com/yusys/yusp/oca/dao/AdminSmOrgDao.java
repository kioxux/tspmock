package cn.com.yusys.yusp.oca.dao;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.mybatisplus.mapper.BaseMapper;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.UserAndOrgInfoRespDto;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmOrgTreeNodeBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmOrgEntity;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmOrgDetailVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmOrgVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Classname AdminSmOrgDao
 * @Description
 * @Date 2020/11/18 23:35
 * @Created by wujp4@yusys.com.cn
 * @Updated by tanrui1@yusys.com.cn
 */

public interface AdminSmOrgDao extends BaseMapper<AdminSmOrgEntity> {

    List<AdminSmOrgVo> selectAllOrgExtVo(String sort);
    AdminSmOrgVo selectOrgExtVoByOrgId(String orgId);
    List<AdminSmOrgVo>  selectOrgExtVoByIds(@Param("orgIds") List<String> orgIds);
    Integer queryRelByOrgId(String orgId);
    AdminSmOrgTreeNodeBo getAllAncestryOrgs(String orgId);
    AdminSmOrgTreeNodeBo findParentByOrgId(String orgId);
    AdminSmOrgDetailVo getDetailById(String orgId);
    Page<AdminSmOrgVo> getOrgsForWf(Page<AdminSmOrgVo> page, @Param(Constants.WRAPPER) QueryWrapper<AdminSmOrgVo> wrapper);
    List<AdminSmOrgVo> getChildOrgCode(@Param(Constants.WRAPPER) QueryWrapper<AdminSmOrgVo> wrapper);
    List<AdminSmOrgDto> getChildListByOrgCode(@Param(Constants.WRAPPER) QueryWrapper<AdminSmOrgDto> wrapper);

    /**
     * 根据机构编号列表分页查询用户和机构信息
     * @param iPage
     * @param orgIds
     * @return
     */
    IPage<UserAndOrgInfoRespDto> getUserAndOrgInfo(IPage<UserAndOrgInfoRespDto> iPage,@Param("orgIds") List<String> orgIds);

    /**
     * 根据客户经理编号查询用账务机构编号
     * @param loginCode
     * @return
     */
    String getFinOrgbyLoginCode(@Param("loginCode")String loginCode);

    /**
     * @创建人 WH
     * @创建时间 2021/6/11 16:29
     * @注释 条件查询 条件查询为选择的机构
     */
    List querypagebyall(QueryModel model);

    /**
     * 根据机构编号及下级机构号分页查询用户和机构信息
     * @return
     */
    List<UserAndOrgInfoRespDto> getUserByLowerOrgInfo(QueryModel model);
}
