package cn.com.yusys.yusp.notice.dao;

import cn.com.yusys.yusp.notice.entity.AdminSmNoticeReadEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统公告用户查阅历史表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-15 13:05:27
 */

public interface AdminSmNoticeReadDao extends BaseMapper<AdminSmNoticeReadEntity> {
	
}
