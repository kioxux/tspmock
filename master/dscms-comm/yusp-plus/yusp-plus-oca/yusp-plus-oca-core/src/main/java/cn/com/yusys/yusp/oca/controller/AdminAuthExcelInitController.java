package cn.com.yusys.yusp.oca.controller;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmOrgExcelVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmRoleExcelVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmUserExcelVo;
import cn.com.yusys.yusp.oca.service.AdminSmOrgService;
import cn.com.yusys.yusp.oca.service.AdminSmRoleService;
import cn.com.yusys.yusp.oca.service.AdminSmUserRoleRelService;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import cn.com.yusys.yusp.oca.service.component.OrgExcelInitListener;
import cn.com.yusys.yusp.oca.service.component.RoleExcelInitListener;
import cn.com.yusys.yusp.oca.service.component.UserExcelInitListener;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.metadata.ReadSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RequestMapping("api/authinit")
@RestController
public class AdminAuthExcelInitController {

    @Autowired
    private AdminSmOrgService adminSmOrgService;


    @Autowired
    private AdminSmRoleService adminSmRoleService;

    @Autowired
    private AdminSmUserRoleRelService adminSmUserRoleRelService;


    @Autowired
    private AdminSmUserService adminSmUserService;


    @PostMapping("/excel")
    public ResultDto authInitByExcel(MultipartFile file,int flag){
        ExcelReader reader = null;
        try {
            reader = EasyExcel.read(file.getInputStream()).build();
            ReadSheet orgSheet = EasyExcel.readSheet(0).head(AdminSmOrgExcelVo.class).registerReadListener(new OrgExcelInitListener(adminSmOrgService,flag)).build();
            ReadSheet roleSheet = EasyExcel.readSheet(1).head(AdminSmRoleExcelVo.class).registerReadListener(new RoleExcelInitListener(adminSmRoleService,flag)).build();
            ReadSheet userSheet = EasyExcel.readSheet(2).head(AdminSmUserExcelVo.class).registerReadListener(new UserExcelInitListener(adminSmUserService, adminSmUserRoleRelService,flag)).build();
            reader.read(orgSheet,roleSheet,userSheet);
        } catch (IOException e) {
            e.printStackTrace();
        } if (reader != null) {
            // 这里千万别忘记关闭，读的时候会创建临时文件，到时磁盘会崩的
            reader.finish();
        }
        return ResultDto.success();
    }
}
