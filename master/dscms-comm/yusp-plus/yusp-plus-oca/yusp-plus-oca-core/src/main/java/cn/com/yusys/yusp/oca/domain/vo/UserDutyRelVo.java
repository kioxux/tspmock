package cn.com.yusys.yusp.oca.domain.vo;

import lombok.Data;

/**
 * 用户所在机构可关联的角色列表以及实际关联状态
 */
@Data
public class UserDutyRelVo {
    private String dutyId;
    private String dutyCode;
    private String dutyName;
    private Boolean checked=false;
}
