package cn.com.yusys.yusp.oca.domain.entity;

import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.config.validation.Insert;
import cn.com.yusys.yusp.oca.config.validation.Update;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * 系统岗位表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-01 21:55:19
 */
@Data
@TableName("admin_sm_duty")
public class AdminSmDutyEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 记录编号
	 */
	@TableId(type = IdType.UUID)
	@NotBlank(groups = Update.class)
	private String dutyId;
	/**
	 * 岗位代码
	 */
	@NotBlank(groups = Insert.class)
	private String dutyCode;
	/**
	 * 岗位名称
	 */
	@NotBlank(groups = Insert.class)
	private String dutyName;
	/**
	 * 所属机构编号
	 */
	@NotBlank(groups = Insert.class)
	private String orgId;
	/**
	 * 备注
	 */
	private String dutyRemark;
	/**
	 * 状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效
	 */
	private AvailableStateEnum dutySts;
	/**
	 * 最新变更用户
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
	/**
	 * 最新变更时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

}
