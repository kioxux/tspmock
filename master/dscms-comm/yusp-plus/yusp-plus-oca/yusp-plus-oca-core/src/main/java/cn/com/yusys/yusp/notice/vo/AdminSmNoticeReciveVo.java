package cn.com.yusys.yusp.notice.vo;

import lombok.Data;

@Data
public class AdminSmNoticeReciveVo {

    private String reciveId;

    private String reciveType;

    private String reciveOgjId;

    private String roleId;

    private String roleName;

    private String orgId;

    private String orgName;
}
