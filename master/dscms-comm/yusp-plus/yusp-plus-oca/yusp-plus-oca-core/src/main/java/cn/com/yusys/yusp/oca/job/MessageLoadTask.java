package cn.com.yusys.yusp.oca.job;

import cn.com.yusys.yusp.oca.domain.entity.AdminSmMessageEntity;
import cn.com.yusys.yusp.oca.service.AdminSmMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class MessageLoadTask {

    private final Logger logger = LoggerFactory.getLogger(MessageLoadTask.class);

    @Autowired
    private AdminSmMessageService adminSmMessageService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public void run(String params) {
        logger.info("messageLoadTask定时任务正在执行，参数为：{}", params);
        Locale defLanguage = LocaleContextHolder.getLocale();
        if (!Locale.SIMPLIFIED_CHINESE.equals(defLanguage)) {
            LocaleContextHolder.setLocale(new Locale("zh", "CN"));
        }
        List<AdminSmMessageEntity> list = adminSmMessageService.list();
        cache(list, "zh_cn");
        try {
            LocaleContextHolder.setLocale(new Locale("en", "us"));
            list = adminSmMessageService.list();
            cache(list, "en_us");
        } catch (Exception e) {
            logger.warn("未找到系统提示消息国际化表，请检查");
        }finally {
            LocaleContextHolder.setLocale(defLanguage);
        }
    }

    private void cache(List<AdminSmMessageEntity> list, String language) {
        stringRedisTemplate.delete("sysMessage_" + language);
        BoundHashOperations<String, String, String> hashOps = stringRedisTemplate.opsForHash().getOperations().boundHashOps("sysMessage_" + language);
        Map<String, String> collect = list.stream().collect(Collectors.toMap(AdminSmMessageEntity::getCode, AdminSmMessageEntity::getMessage));
        hashOps.putAll(collect);
    }

}