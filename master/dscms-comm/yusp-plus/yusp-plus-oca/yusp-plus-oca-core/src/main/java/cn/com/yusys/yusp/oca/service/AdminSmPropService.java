package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.dto.AdminSmPropDto;
import cn.com.yusys.yusp.dto.AdminSmPropQueryDto;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmPropEditBo;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmPropSaveBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmPropEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmPropQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 系统参数表
 */
public interface AdminSmPropService extends IService<AdminSmPropEntity> {

    /**
     * 查询系统参数列表
     *
     * @param adminSmPropQuery
     * @return 系统分页数据
     */
    Page<AdminSmPropEntity> queryPropPage(AdminSmPropQuery adminSmPropQuery);

    /**
     * 新增系统参数
     *
     * @param adminSmProp
     */
    void saveProp(AdminSmPropSaveBo adminSmProp);

    /**
     * 修改系统参数
     *
     * @param adminSmProp
     */
    void updatePropById(AdminSmPropEditBo adminSmProp);

    /**
     * 根据参数名称删除参数信息
     *
     * @param propName 参数名称
     */
    void deleteProp(String propName);

    /**
     * 查询系统参数信息
     *
     * @param adminSmPropQueryDto 查询条件
     * @return 系统参数对象
     */
    AdminSmPropDto queryProp(AdminSmPropQueryDto adminSmPropQueryDto);

}