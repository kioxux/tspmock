package cn.com.yusys.yusp.message.memorybinder.provisioners;

import cn.com.yusys.yusp.message.channel.YuMessageChannel;
import org.springframework.cloud.stream.binder.ConsumerProperties;
import org.springframework.cloud.stream.binder.ProducerProperties;
import org.springframework.cloud.stream.provisioning.ConsumerDestination;
import org.springframework.cloud.stream.provisioning.ProducerDestination;
import org.springframework.cloud.stream.provisioning.ProvisioningProvider;

/**
 * 内存消息绑定器供应者
 * <p>
 * 不支持消息分组
 *
 * @author xiaodg@yusys.com.cn
 */
public class MemoryMessageBinderProvisioner implements ProvisioningProvider<ConsumerProperties, ProducerProperties> {

    /**
     * 消息生产的目的地
     *
     * @param name       生产消息的目的地名称, 绑定通道 {@link YuMessageChannel#MESSAGE_CHANNEL_OUTPUT}
     * @param properties 配置参数
     * @return {@link ProducerDestination}
     */
    @Override
    public ProducerDestination provisionProducerDestination(final String name, final ProducerProperties properties) {
        return new MemoryMessageDestination(name);
    }

    /**
     * @param name       消费消息的目的地名称, 绑定通道 {@link YuMessageChannel#MESSAGE_CHANNEL_INPUT}
     * @param group      消息分组
     * @param properties 配置参数
     * @return {@link ConsumerDestination}
     */
    @Override
    public ConsumerDestination provisionConsumerDestination(final String name, final String group, final ConsumerProperties properties) {
        return new MemoryMessageDestination(name);
    }

    /**
     * 内存消息目的地配置
     */
    private static class MemoryMessageDestination implements ProducerDestination, ConsumerDestination {
        /**
         * 消息发送目的地
         */
        private final String destination;

        private MemoryMessageDestination(final String destination) {
            this.destination = destination;
        }

        @Override
        public String getName() {
            return destination.trim();
        }

        @Override
        public String getNameForPartition(int partition) {
            throw new UnsupportedOperationException("内存消息绑定器不支持分组");
        }

    }

}
