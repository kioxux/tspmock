package cn.com.yusys.yusp.oca.domain.vo;

import lombok.Data;

@Data
public class ContrVo {

    private String contrCode;

    private String contrName;

    private String contrUrl;

    private String sysId;

    private String funcId;

    private String methodType;
    private String details;


}