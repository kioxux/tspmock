package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.oca.domain.form.AdminSmAuthTreeForm;
import cn.com.yusys.yusp.oca.domain.form.AdminSmAuthUpdateForm;
import cn.com.yusys.yusp.oca.domain.form.AdminTmplAuthForm;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmAuthTreeVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmResContrAuthVo;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

public interface AdminSmAuthorizationService {
    /**
     * 获取权限树
     *
     * @param adminSmAuthTreeForm
     */
    List<AdminSmAuthTreeVo> getTreeQuery(AdminSmAuthTreeForm adminSmAuthTreeForm);

    /**
     * 修改被授权人权限
     *
     * @param adminSmAuthUpdateForm
     */
    void saveAuth(AdminSmAuthUpdateForm adminSmAuthUpdateForm);

    /**
     * 复制拷贝权限
     *
     * @param adminSmAuthUpdateForm
     */
    void copyAuth(AdminSmAuthUpdateForm adminSmAuthUpdateForm);

    /**
     * 获取数据权限模板授权页面
     *
     * @param adminSmAuthTreeForm
     */
    IPage<AdminSmResContrAuthVo> getAuthTmplList(AdminSmAuthTreeForm adminSmAuthTreeForm);

    /**
     * 给被授权对象修改控制点的数据模板
     *
     * @param form
     */
    void saveTmplAuth(AdminTmplAuthForm form);
}
