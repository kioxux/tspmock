package cn.com.yusys.yusp.oca.domain.form;

import lombok.Data;

@Data
public class AdminSmAuthTreeForm {
    /**
     * 被授权对象 id
     */
    private String authObjId;
    /**
     * 被授权对象的类型
     */
    private String authObjType;
    /**
     * 授权对象 id
     */
    private String userRoleId;
    /**
     * 模糊查询关键字
     */
    private String keyWord;

    private int page;

    private int size;
}
