package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmDutyEntity;
import cn.com.yusys.yusp.oca.config.validation.Insert;
import cn.com.yusys.yusp.oca.config.validation.Update;
import cn.com.yusys.yusp.oca.domain.query.AdminSmDutyQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmDutyUserQuery;
import cn.com.yusys.yusp.oca.service.AdminSmDutyService;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmDutyVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmUserDutyRelVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Objects;


/**
 * 系统岗位表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 */
@RestController
@RequestMapping("/api/adminsmduty")
public class AdminSmDutyController {
    @Autowired
    private AdminSmDutyService adminSmDutyService;

    /**
     * 分页列表
     */
    @GetMapping("/page")
    public ResultDto<AdminSmDutyVo> page(AdminSmDutyQuery query) {
        return ResultDto.success(adminSmDutyService.queryPage(query));
    }
    /**
     * 信息
     */
    @GetMapping("/info/{dutyId}")
    public ResultDto info(@PathVariable("dutyId") String dutyId) {
        AdminSmDutyEntity adminSmDuty = adminSmDutyService.getById(dutyId);
        return ResultDto.success().extParam("adminSmDuty", adminSmDuty);
    }

    @PostMapping("/add")
    public ResultDto<Objects> add(@RequestBody @Validated({Insert.class}) AdminSmDutyEntity entity) {
        adminSmDutyService.save(entity);
        return ResultDto.successMessage("保存成功");

    }

    @PostMapping("/update")
    public ResultDto<Object> update(@RequestBody @Validated({Update.class}) AdminSmDutyEntity entity) {
        adminSmDutyService.updateById(entity);
        return ResultDto.successMessage("更新成功");
    }

    /**
     * 批量启用岗位
     */
    @PostMapping("/batchenable")
    public ResultDto<Object> batchEnable(@RequestBody @NotNull String[] ids) {
        adminSmDutyService.batchEnable(ids);
        return ResultDto.successMessage("启用成功");
    }

    /**
     * 批量停用岗位
     */
    @PostMapping("/batchdisable")
    public ResultDto<Object> batchDisable(@RequestBody @NotNull String[] ids) {
        adminSmDutyService.batchDisable(ids);

        return ResultDto.successMessage("停用成功");
    }

    @PostMapping("/batchdelete")
    public ResultDto<Object> batchDelete(@RequestBody @NotNull String[] ids) {
        adminSmDutyService.batchDelete(ids);
        return ResultDto.successMessage("删除成功");
    }

    /**
     * 查询岗位下的用户
     */
    @GetMapping("/userlist")
    public ResultDto<Object> userList(AdminSmDutyUserQuery query) {
        Page<AdminSmUserDutyRelVo> dutyUserVoList = adminSmDutyService.memberPage(query);
        return ResultDto.success(dutyUserVoList);
    }
    /**
     * 查询岗位下的用户
     */
    @PostMapping("/userlistbygw")
    public ResultDto<Object> userlistbygw(@RequestBody  AdminSmDutyUserQuery query) {
        Page<AdminSmUserDutyRelVo> dutyUserVoList = adminSmDutyService.memberPage(query);
        return ResultDto.success(dutyUserVoList);
    }
}
