package cn.com.yusys.yusp.oca.config.annotation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @description:
 * @author: zhangsong
 * @date: 2021/3/29
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = PasswordStrategyCheck.class)
public @interface PasswordStrategy {
    String message() default "Invaid Password";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
