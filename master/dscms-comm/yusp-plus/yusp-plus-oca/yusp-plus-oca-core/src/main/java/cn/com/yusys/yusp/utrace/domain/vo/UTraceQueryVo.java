package cn.com.yusys.yusp.utrace.domain.vo;

import cn.com.yusys.yusp.common.query.PageQuery;
import cn.com.yusys.yusp.utrace.domain.entity.SModifyTraceEntity;
import lombok.Data;

@Data
public class UTraceQueryVo extends PageQuery<SModifyTraceEntity> {

    /**
     * 表单字段ID
     */
    private String mFieldId;


    /**
     * 数据主键
     */
    private String mPkV;

    /**
     * 月份
     */
    private String month;

}
