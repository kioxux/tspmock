package cn.com.yusys.yusp.oca.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * @program: yusp-app-framework
 * @description: 登录时用户传输Vo
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2020-11-23 15:06
 */
@ApiModel(description = "用户登录实体类", value = "用户登录实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginUserDto {

    @ApiModelProperty(name = "loginCode", value = "用户登录名", required = true, example = "test")
    @NotEmpty(message = "{loginCode}")
    private String loginCode;

    @ApiModelProperty(name = "password", value = "用户密码", required = true, example = "123456")
    @NotEmpty(message = "{password}")
    private String password;
}
