package cn.com.yusys.yusp.oca.domain.vo;

public class AdminSmLogAsyncVo {
    /**
     * 记录编号
     */
    private String logId;
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 操作时间
     */
    private String operTime;
    /**
     * 操作对象ID
     */
    private String operObjId;
    /**
     * 操作前值
     */
    private String beforeValue;
    /**
     * 操作后值
     */
    private String afterValue;
    /**
     * 操作标志
     */
    private String operFlag;
    /**
     * 日志类型
     */
    private String logTypeId;
    /**
     * 日志内容
     */
    private String content;
    /**
     * 操作者机构
     */
    private String orgId;
    /**
     * 操作机构名称
     */
    private String orgName;
    /**
     * 登录IP
     */
    private String loginIp;
}
