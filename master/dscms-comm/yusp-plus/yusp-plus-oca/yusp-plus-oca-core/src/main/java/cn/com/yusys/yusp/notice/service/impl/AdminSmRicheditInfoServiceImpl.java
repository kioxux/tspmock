package cn.com.yusys.yusp.notice.service.impl;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.common.utils.Query;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.notice.dao.AdminSmRicheditInfoDao;
import cn.com.yusys.yusp.notice.entity.AdminSmRicheditInfoEntity;
import cn.com.yusys.yusp.notice.service.AdminSmRicheditInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("adminSmRicheditInfoService")
public class AdminSmRicheditInfoServiceImpl extends ServiceImpl<AdminSmRicheditInfoDao, AdminSmRicheditInfoEntity> implements AdminSmRicheditInfoService {


    /**
     * 删除富文本表
     * @param noticeIds
     */
    @Override
    public void deleteRicheditInfo(List<String> noticeIds) {
        QueryWrapper<AdminSmRicheditInfoEntity> wrapper = Wrappers.<AdminSmRicheditInfoEntity>query().in(
                noticeIds != null && noticeIds.size() > 0,
                "REL_ID",
                noticeIds);
        this.remove(wrapper);
    }

    /**
     * 按照 relId 修改富文本内容
     * @param richeditInfoEntity
     */
    @Override
    public void updateByRelId(AdminSmRicheditInfoEntity richeditInfoEntity) {
        UpdateWrapper<AdminSmRicheditInfoEntity> wrapper = new UpdateWrapper<>();
        wrapper.eq(!StringUtils.isEmpty(
                richeditInfoEntity.getRelId()),
                "rel_id",
                richeditInfoEntity.getRelId());
        AdminSmRicheditInfoEntity updateEntity = new AdminSmRicheditInfoEntity();
        updateEntity.setContent(richeditInfoEntity.getContent());
        this.baseMapper.update(updateEntity, wrapper);
    }
}