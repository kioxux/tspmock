package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.common.query.PageQuery;
import cn.com.yusys.yusp.common.utils.GenericBuilder;
import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.common.utils.Query;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.ArrayUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.oca.dao.AdminSmLookupDictDao;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmLookupDictBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmLookupDictEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmLookupDictQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmLookupDictVo;
import cn.com.yusys.yusp.oca.service.AdminSmLookupDictService;
import cn.com.yusys.yusp.oca.utils.I18nMPCacheKey;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotBlank;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service("adminSmLookupDictService")
public class AdminSmLookupDictServiceImpl extends ServiceImpl<AdminSmLookupDictDao, AdminSmLookupDictEntity> implements AdminSmLookupDictService {

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AdminSmLookupDictEntity> page = this.page(
                new Query<AdminSmLookupDictEntity>().getPage(params),
                new QueryWrapper<AdminSmLookupDictEntity>()
        );
        return new PageUtils(page);
    }

    /**
     * 数据字典 列表查询
     *
     * @param adminSmLookupDictQuery
     * @return
     */
    @Override
    public Page<AdminSmLookupDictVo> queryLookupDictWithCondition(AdminSmLookupDictQuery adminSmLookupDictQuery) {

        Page<AdminSmLookupDictEntity> page = adminSmLookupDictQuery.getIPage();
        //搜索关键字
        String keyWord = adminSmLookupDictQuery.getKeyWord();
        QueryWrapper<AdminSmLookupDictEntity> lookupDictQuery = new QueryWrapper<>();
        lookupDictQuery.select("lookup_item_id", "lookup_code", "lookup_name", "lookup_type_id", "lookup_type_name")
                .eq("up_lookup_item_id", "-1").orderByAsc("lookup_item_order");
        //模糊查
        if (StringUtils.nonEmpty(keyWord)) {
            lookupDictQuery.and(wrapper -> wrapper
                    .like(StringUtils.nonEmpty(keyWord), "lookup_name", keyWord)
                    .or()
                    .like(StringUtils.nonEmpty(keyWord), "lookup_code", keyWord)
                    .or()
                    .like(StringUtils.nonEmpty(keyWord), "lookup_type_name", keyWord)
            );
        }
        lookupDictQuery.orderByDesc("last_chg_dt");
        //分页查询
        Page<AdminSmLookupDictEntity> dictEntityList = this.baseMapper.selectPage(page, lookupDictQuery);
        // entity 转成 vo类
        List<AdminSmLookupDictVo> dictVos = dictEntityList.getRecords().stream().map(dict -> GenericBuilder.of(AdminSmLookupDictVo::new)
                .with(AdminSmLookupDictVo::setLookupItemId, dict.getLookupItemId())
                .with(AdminSmLookupDictVo::setLookupCode, dict.getLookupCode())
                .with(AdminSmLookupDictVo::setLookupName, dict.getLookupName())
                .with(AdminSmLookupDictVo::setLookupTypeId, dict.getLookupTypeId())
                .with(AdminSmLookupDictVo::setLookupTypeName, dict.getLookupTypeName())
                .with(AdminSmLookupDictVo::setLookupItemsString, "")
                .build()).collect(Collectors.toList());
        // vo ipage 返回
        Page<AdminSmLookupDictVo> iPage = new PageQuery<AdminSmLookupDictVo>().getIPage();
        iPage.setRecords(dictVos);
        iPage.setTotal(page.getTotal());
        iPage.setSize(page.getSize());
        iPage.setPages(page.getPages());
        return iPage;
    }

    /**
     * 数据字典保存
     * 先删缓存，再更新库，再更新缓存
     *
     * @param adminSmLookupDictBo
     * @return
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void saveLookupDictByDictBo(AdminSmLookupDictBo adminSmLookupDictBo) {
        // bean copy
        AdminSmLookupDictEntity dictEntity = BeanUtils.beanCopy(adminSmLookupDictBo, AdminSmLookupDictEntity.class);
        List<AdminSmLookupDictBo.LookupItemBo> itemBos = adminSmLookupDictBo.getLookupItemBos();

        if (CollectionUtils.isEmpty(itemBos)) {
            throw BizException.error(null, "50500001", "字典项不能为空");
        }

        //校验 lookupcode lookupname 是否已存在
        Integer selectCount = this.baseMapper.selectCount(new QueryWrapper<AdminSmLookupDictEntity>()
                .eq("lookup_code", dictEntity.getLookupCode())
        );
        //校验传入item是否有重复
        Map<@NotBlank(message = "lookupItemCode can not be empty!") String, List<AdminSmLookupDictBo.LookupItemBo>> frontItemCodeMap =
                itemBos.stream().collect(Collectors.groupingBy(AdminSmLookupDictBo.LookupItemBo::getLookupItemCode));
        if (selectCount > 0 || frontItemCodeMap.size() < itemBos.size()) {
            throw BizException.error(null, "50500002", "字典项不能重复");
        }
        List<AdminSmLookupDictEntity> dicts = new ArrayList<>();
        AtomicInteger atomicInteger = new AtomicInteger(1);
        String uuid = UUID.randomUUID().toString().replace("-", "");
        itemBos.stream().forEach(lookupItemBo -> {
            dicts.add(
                    GenericBuilder.of(AdminSmLookupDictEntity::new)
                            .with(AdminSmLookupDictEntity::setLookupItemId, UUID.randomUUID().toString().replace("-", ""))
                            .with(AdminSmLookupDictEntity::setLookupCode, dictEntity.getLookupCode())
                            .with(AdminSmLookupDictEntity::setLookupName, dictEntity.getLookupName())
                            .with(AdminSmLookupDictEntity::setLookupItemCode, lookupItemBo.getLookupItemCode())
                            .with(AdminSmLookupDictEntity::setLookupItemName, lookupItemBo.getLookupItemName())
                            .with(AdminSmLookupDictEntity::setLookupItemOrder, atomicInteger.getAndAdd(1))
                            .with(AdminSmLookupDictEntity::setLookupTypeId, dictEntity.getLookupTypeId())
                            .with(AdminSmLookupDictEntity::setLookupTypeName, dictEntity.getLookupTypeName())
                            .with(AdminSmLookupDictEntity::setUpLookupItemId, uuid)
                            .build()
            );
        });
        List<AdminSmLookupDictEntity> cacheDicts = new ArrayList<>(dicts);
        dicts.add(GenericBuilder.of(AdminSmLookupDictEntity::new)
                .with(AdminSmLookupDictEntity::setLookupItemId, uuid)
                .with(AdminSmLookupDictEntity::setLookupCode, dictEntity.getLookupCode())
                .with(AdminSmLookupDictEntity::setLookupName, dictEntity.getLookupName())
                .with(AdminSmLookupDictEntity::setLookupItemCode, "0")
                .with(AdminSmLookupDictEntity::setLookupItemName, "0")
                .with(AdminSmLookupDictEntity::setLookupItemOrder, 0)
                .with(AdminSmLookupDictEntity::setLookupTypeId, dictEntity.getLookupTypeId())
                .with(AdminSmLookupDictEntity::setLookupTypeName, dictEntity.getLookupTypeName())
                .with(AdminSmLookupDictEntity::setUpLookupItemId, "-1")
                .build()
        );
        this.saveBatch(dicts);
        Map<String, List<AdminSmLookupDictEntity>> dictMap = cacheDicts.stream().collect(Collectors.groupingBy(AdminSmLookupDictEntity::getLookupCode));
        Map<String, Object> dictMaps = this.dict2RedisHashStore(dictMap);
        // 添加到缓存
        this.addRedisCache(dictMaps);
    }

    /**
     * 数据字典修改
     * 先删缓存，再更新库，再更新缓存
     *
     * @param adminSmLookupDictBo
     * @return
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void updateLookupDictByDictBo(AdminSmLookupDictBo adminSmLookupDictBo) {
        // bean copy
        AdminSmLookupDictEntity dictEntity = BeanUtils.beanCopy(adminSmLookupDictBo, AdminSmLookupDictEntity.class);
        List<AdminSmLookupDictBo.LookupItemBo> itemBos = adminSmLookupDictBo.getLookupItemBos();
        if (CollectionUtils.isEmpty(itemBos)) {
            throw BizException.error(null, "50500001", "字典项不能为空");
        }
        Map<@NotBlank(message = "lookupItemCode can not be empty!") String, List<AdminSmLookupDictBo.LookupItemBo>> frontItemCodeMap =
                itemBos.stream().collect(Collectors.groupingBy(AdminSmLookupDictBo.LookupItemBo::getLookupItemCode));
        if (frontItemCodeMap.size() < itemBos.size()) {
            throw BizException.error(null, "50500002", "字典项不能重复!");
        }
        // 删除缓存
        this.deleteRedisCache(dictEntity.getLookupCode());
        List<AdminSmLookupDictEntity> dicts = new ArrayList<>();
        // 需要更新到缓存的字典值
        AtomicInteger atomicInteger = new AtomicInteger(1);
        itemBos.stream().forEach(lookupItemBo -> dicts.add(
                GenericBuilder.of(AdminSmLookupDictEntity::new)
                        .with(AdminSmLookupDictEntity::setLookupItemId, lookupItemBo.getLookupItemId())
                        .with(AdminSmLookupDictEntity::setLookupCode, dictEntity.getLookupCode())
                        .with(AdminSmLookupDictEntity::setLookupName, dictEntity.getLookupName())
                        .with(AdminSmLookupDictEntity::setLookupItemCode, lookupItemBo.getLookupItemCode())
                        .with(AdminSmLookupDictEntity::setUpLookupItemId, dictEntity.getLookupItemId())
                        .with(AdminSmLookupDictEntity::setLookupItemName, lookupItemBo.getLookupItemName())
                        .with(AdminSmLookupDictEntity::setLookupTypeId, adminSmLookupDictBo.getLookupTypeId())
                        .with(AdminSmLookupDictEntity::setLookupTypeName, dictEntity.getLookupTypeName())
                        .with(AdminSmLookupDictEntity::setLookupItemOrder, atomicInteger.getAndAdd(1))
                        .build()
        ));
        List<AdminSmLookupDictEntity> cacheDicts = new ArrayList<>(dicts);
        dicts.add(GenericBuilder.of(AdminSmLookupDictEntity::new)
                .with(AdminSmLookupDictEntity::setLookupItemId, dictEntity.getLookupItemId())
                .with(AdminSmLookupDictEntity::setLookupCode, dictEntity.getLookupCode())
                .with(AdminSmLookupDictEntity::setLookupName, dictEntity.getLookupName())
                .with(AdminSmLookupDictEntity::setLookupItemCode, "0")
                .with(AdminSmLookupDictEntity::setLookupItemName, "0")
                .with(AdminSmLookupDictEntity::setLookupItemOrder, 0)
                .with(AdminSmLookupDictEntity::setLookupTypeId, dictEntity.getLookupTypeId())
                .with(AdminSmLookupDictEntity::setLookupTypeName, dictEntity.getLookupTypeName())
                .with(AdminSmLookupDictEntity::setUpLookupItemId, "-1")
                .build()
        );
        // 清除之前保存的记录
        this.baseMapper.delete(new QueryWrapper<AdminSmLookupDictEntity>()
                .eq("lookup_item_id", dictEntity.getLookupItemId())
                .or()
                .eq("up_lookup_item_id", dictEntity.getLookupItemId())
        );
        // 批量重新保存
        this.saveBatch(dicts);
        // 更新缓存
        Map<String, List<AdminSmLookupDictEntity>> dictMap = cacheDicts.stream().collect(Collectors.groupingBy(AdminSmLookupDictEntity::getLookupCode));
        Map<String, Object> dictMaps = this.dict2RedisHashStore(dictMap);
        // 添加到缓存
        this.addRedisCache(dictMaps);
    }

    /**
     * 数据字典 批量删除
     * 防止数据删除后，缓存未删除，故先删缓存，再删库
     *
     * @param lookupDictIds
     * @return
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void removeLookupDictByIds(String[] lookupDictIds) {
        List<AdminSmLookupDictEntity> itemIds = this.baseMapper.selectList(new QueryWrapper<AdminSmLookupDictEntity>()
                .select("lookup_item_id", "lookup_item_code", "lookup_item_name", "lookup_code")
                .in(!ArrayUtils.isEmpty(lookupDictIds), "up_lookup_item_id", Arrays.asList(lookupDictIds))
        );
        // 删除redis缓存
        Map<String, List<AdminSmLookupDictEntity>> dictMap = itemIds.stream().collect(Collectors.groupingBy(AdminSmLookupDictEntity::getLookupCode));
        // 删除redis缓存
        for (Map.Entry<String, List<AdminSmLookupDictEntity>> vo : dictMap.entrySet()) {
            this.deleteRedisCache(vo.getKey());
        }
        // 删除数据字典
        this.baseMapper.deleteBatchIds(Arrays.asList(lookupDictIds));
        // 删除上级数据字典
        this.baseMapper.delete(new QueryWrapper<AdminSmLookupDictEntity>()
                .in(!ArrayUtils.isEmpty(lookupDictIds), "up_lookup_item_id", Arrays.asList(lookupDictIds))
        );
    }

    /**
     * 字典详情
     *
     * @param lookupItemId
     * @return
     */
    @Override
    public List<AdminSmLookupDictVo> queryLookupDictInfoById(String lookupItemId) {

        List<AdminSmLookupDictEntity> itemIds = this.baseMapper.selectList(new QueryWrapper<AdminSmLookupDictEntity>()
                .select("lookup_item_id", "lookup_item_code", "lookup_item_name")
                .eq(StringUtils.nonEmpty(lookupItemId), "up_lookup_item_id", lookupItemId).orderByAsc("lookup_item_order")
        );
        List<AdminSmLookupDictVo> adminSmLookupDictVos = itemIds.stream().filter(Objects::nonNull).map(
                dict -> GenericBuilder.of(AdminSmLookupDictVo::new)
                        .with(AdminSmLookupDictVo::setLookupItemId, dict.getLookupItemId())
                        .with(AdminSmLookupDictVo::setLookupItemCode, dict.getLookupItemCode())
                        .with(AdminSmLookupDictVo::setLookupItemName, dict.getLookupItemName())
                        .build()
        ).collect(Collectors.toList());
        return adminSmLookupDictVos;
    }

    /**
     * 查询初始化 字典分类
     *
     * @return
     */
    @Override
    public List<AdminSmLookupDictVo> queryInitLookupDict() {
        List<AdminSmLookupDictEntity> dictList = this.baseMapper.selectList(new QueryWrapper<AdminSmLookupDictEntity>()
                .select("lookup_item_id", "lookup_item_code", "lookup_item_name")
                .eq("up_lookup_item_id", "COMMON_INIT_PARAM")
                .orderByAsc("lookup_item_order")
        );
        List<AdminSmLookupDictVo> adminSmLookupDictVos = dictList.stream().filter(adminSmLookupDictEntity -> adminSmLookupDictEntity != null).map(
                dict -> GenericBuilder.of(AdminSmLookupDictVo::new)
                        .with(AdminSmLookupDictVo::setLookupItemId, dict.getLookupItemId())
                        .with(AdminSmLookupDictVo::setLookupItemCode, dict.getLookupItemCode())
                        .with(AdminSmLookupDictVo::setLookupItemName, dict.getLookupItemName())
                        .build()
        ).collect(Collectors.toList());
        return adminSmLookupDictVos;
    }

    /**
     * 插入字典项
     *
     * @param adminSmLookupDictBo
     * @return
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void insertLookupDictByDictBo(AdminSmLookupDictBo adminSmLookupDictBo) {
        //bean copy
        AdminSmLookupDictEntity dictEntity = BeanUtils.beanCopy(adminSmLookupDictBo, AdminSmLookupDictEntity.class);
        List<AdminSmLookupDictBo.LookupItemBo> itemBos = adminSmLookupDictBo.getLookupItemBos();
        if (CollectionUtils.isEmpty(itemBos)) {
            throw BizException.error(null, "50500001", "字典项不能为空");
        }
        List<AdminSmLookupDictEntity> insertdicts = new ArrayList<>();
        // 需要更新到缓存的字典值
        AtomicInteger atomicInteger = new AtomicInteger(1);
        Map<@NotBlank(message = "lookupItemCode can not be empty!") String, List<AdminSmLookupDictBo.LookupItemBo>> frontItemCodeMap =
                itemBos.stream().collect(Collectors.groupingBy(AdminSmLookupDictBo.LookupItemBo::getLookupItemCode));
        if (frontItemCodeMap.size() < itemBos.size()) {
            throw BizException.error(null, "50500002", "字典项不能重复!");
        }
        //删除原有的字典项
        Map param = new HashMap<String, Object>();
        param.put("lookup_code", adminSmLookupDictBo.getLookupCode());
        this.baseMapper.deleteByMap(param);
        String uuid = UUID.randomUUID().toString().replace("-", "");
        itemBos.stream().forEach(lookupItemBo -> {
            insertdicts.add(
                    GenericBuilder.of(AdminSmLookupDictEntity::new)
                            .with(AdminSmLookupDictEntity::setLookupItemId, UUID.randomUUID().toString().replace("-", ""))
                            .with(AdminSmLookupDictEntity::setLookupCode, dictEntity.getLookupCode())
                            .with(AdminSmLookupDictEntity::setLookupName, dictEntity.getLookupName())
                            .with(AdminSmLookupDictEntity::setLookupItemCode, lookupItemBo.getLookupItemCode())
                            .with(AdminSmLookupDictEntity::setLookupItemName, lookupItemBo.getLookupItemName())
                            .with(AdminSmLookupDictEntity::setLookupItemOrder, atomicInteger.addAndGet(1))
                            .with(AdminSmLookupDictEntity::setLookupItemOrder, itemBos.indexOf(lookupItemBo))
                            .with(AdminSmLookupDictEntity::setLookupTypeId, dictEntity.getLookupTypeId())
                            .with(AdminSmLookupDictEntity::setLookupTypeName, dictEntity.getLookupTypeName())
                            .with(AdminSmLookupDictEntity::setUpLookupItemId, uuid)
                            .build()
            );
        });
        List<AdminSmLookupDictEntity> cacheDicts = new ArrayList<>(insertdicts);
        insertdicts.add(GenericBuilder.of(AdminSmLookupDictEntity::new)
                .with(AdminSmLookupDictEntity::setLookupItemId, uuid)
                .with(AdminSmLookupDictEntity::setLookupCode, dictEntity.getLookupCode())
                .with(AdminSmLookupDictEntity::setLookupName, dictEntity.getLookupName())
                .with(AdminSmLookupDictEntity::setLookupItemCode, "0")
                .with(AdminSmLookupDictEntity::setLookupItemName, "0")
                .with(AdminSmLookupDictEntity::setLookupItemOrder, 0)
                .with(AdminSmLookupDictEntity::setLookupTypeId, dictEntity.getLookupTypeId())
                .with(AdminSmLookupDictEntity::setLookupTypeName, dictEntity.getLookupTypeName())
                .with(AdminSmLookupDictEntity::setUpLookupItemId, "-1")
                .build()
        );
        this.saveBatch(insertdicts);
        // 新增缓存
        Map<String, List<AdminSmLookupDictEntity>> dictMap = cacheDicts.stream().collect(Collectors.groupingBy(AdminSmLookupDictEntity::getLookupCode));
        Map<String, Object> dictMaps = this.dict2RedisHashStore(dictMap);
        // 添加到缓存
        this.addRedisCache(dictMaps);
    }

    /**
     * 刷新数据字典dict
     * 用于启动加载字典缓存或页面按钮刷新缓存
     */
    @Override
    public void refreshLookupDict() {
        List<AdminSmLookupDictEntity> list = this.baseMapper.selectList(new QueryWrapper<AdminSmLookupDictEntity>()
                .select("lookup_code", "lookup_item_code", "lookup_item_name")
                .ne("up_lookup_item_id", "-1")
                .orderByAsc("lookup_item_order"));
        // 使用hash类型存储字典 例如: redisKey -> datadict  HashKey -> 1 HashValue -> 男
        Map<String, List<AdminSmLookupDictEntity>> dictMap = list.stream().collect(Collectors.groupingBy(AdminSmLookupDictEntity::getLookupCode));
        // 删除缓存
        this.delRedisCache();
        Map<String, Object> dictMaps = this.dict2RedisHashStore(dictMap);
        // 添加到缓存
        this.addRedisCache(dictMaps);
    }

    /**
     * db Map<String,List> 转 Map<String,String>
     *
     * @param dictMap
     * @return
     */
    private Map<String, Object> dict2RedisHashStore(Map<String, List<AdminSmLookupDictEntity>> dictMap) {
        Map<String, Object> dictMaps = new HashMap<>(dictMap.size());
        for (Map.Entry<String, List<AdminSmLookupDictEntity>> entry : dictMap.entrySet()) {
            List<Map<String, String>> mapList = new ArrayList<>();
            for (AdminSmLookupDictEntity dict : entry.getValue()) {
                Map<String, String> tempMap = new HashMap<>();
                tempMap.put("key", dict.getLookupItemCode());
                tempMap.put("value", dict.getLookupItemName());
                mapList.add(tempMap);
            }
            if (CollectionUtils.nonEmpty(mapList)) {
                dictMaps.put(entry.getKey(), mapList);
            }
        }
        return dictMaps;
    }

    /**
     * 获取字典项
     *
     * @param lookupCodes
     * @return
     */
    @Override
    public Map<String, List<Map<String, String>>> querylookupcode(String lookupCodes) {
        List<String> codes = Arrays.asList(lookupCodes.replaceAll("^,*|,*$", "").split(","));

        List<Object> objects = new ArrayList<>(codes.size());
        if (CollectionUtils.nonEmpty(codes)) {
            codes.stream().distinct().forEach(lookupCode -> {
                BoundHashOperations<String, Object, Object> dataDict = stringRedisTemplate.opsForHash().getOperations().boundHashOps(I18nMPCacheKey.DATA_DICT_CACHE_KEY);
                objects.add(dataDict.get(lookupCode));
            });
        }
        // 移除所有的null元素
        objects.removeAll(Collections.singleton(null));
        Map<String, List<Map<String, String>>> dictMap = new HashMap<>(codes.size());
        // redis 如果无数据，或只查询到部分数据，查询数据库，并存储到redis
        if (CollectionUtils.isEmpty(objects) || (codes.size() != objects.size())) {
            List<AdminSmLookupDictEntity> dictEntityListlist = this.baseMapper.selectList(new QueryWrapper<AdminSmLookupDictEntity>()
                    .select("lookup_code", "lookup_item_code", "lookup_item_name")
                    .ne("up_lookup_item_id", "-1")
                    .in("lookup_code", codes).orderByAsc("lookup_item_order")
            );
            codes.forEach(lookupCode -> {
                        List<Map<String, String>> keyValuelist = new ArrayList<>();
                        dictEntityListlist.forEach(dictEntity -> {
                                    if (lookupCode.equals(dictEntity.getLookupCode())) {
                                        HashMap<String, String> dictMaps = new HashMap<>(2);
                                        dictMaps.put("key", dictEntity.getLookupItemCode());
                                        dictMaps.put("value", dictEntity.getLookupItemName());
                                        keyValuelist.add(dictMaps);
                                    }
                                }
                        );
                        dictMap.put(lookupCode, keyValuelist);
                    }
            );
            // 分组存储到redis
            Map<String, List<AdminSmLookupDictEntity>> dictStrListMap = dictEntityListlist.stream().
                    collect(Collectors.groupingBy(AdminSmLookupDictEntity::getLookupCode));
            Map<String, Object> redisHashStore = this.dict2RedisHashStore(dictStrListMap);
            // 添加到缓存
            this.addRedisCache(redisHashStore);
            return dictMap;
        }

        // redis hash field value 转 List<Map>
        if (objects instanceof List) {
            for (int i = 0; i < objects.size(); i++) {
                if (objects.get(i) instanceof String) {
                    List list = ObjectMapperUtils.toObject(StringUtils.replaceObjNull(objects.get(i)), List.class);
                    dictMap.put(codes.get(i), list);
                }
            }
        }
        return dictMap;
    }

    /**
     * 删除redis数据字典缓存，如果删除失败，点击界面上手动刷新缓存
     */
    private void delRedisCache() {
        HashOperations<String, Object, Object> operations = stringRedisTemplate.opsForHash();
        //1.如果是大key,不建议使用delete，故使用scan查询,再删除
        Cursor<Map.Entry<Object, Object>> dictCursor = operations.scan(I18nMPCacheKey.DATA_DICT_CACHE_KEY, ScanOptions.NONE);
        StringBuilder sb = new StringBuilder();
        AtomicInteger atomicInt = new AtomicInteger(0);
        while (dictCursor.hasNext()) {
            int i = atomicInt.addAndGet(1);
            sb.append(dictCursor.next().getKey()).append(",");
            if (i % 50 == 0 || !dictCursor.hasNext()) {
                operations.delete(I18nMPCacheKey.DATA_DICT_CACHE_KEY, sb.toString().split(","));
            }
        }
    }

    /**
     * 添加redis 数据字段 缓存
     */
    private void addRedisCache(Map<String, Object> dictMaps) {
        for (Map.Entry<String, Object> vo : dictMaps.entrySet()) {
            BoundHashOperations<String, Object, Object> dataDict = stringRedisTemplate.opsForHash().getOperations().boundHashOps(I18nMPCacheKey.DATA_DICT_CACHE_KEY);
            try {
                dataDict.put(vo.getKey(), ObjectMapperUtils.instance().writeValueAsString(vo.getValue()));
            } catch (JsonProcessingException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    /**
     * 删除字典缓存
     *
     * @param dictType 字典类型
     */
    private void deleteRedisCache(String dictType) {
        BoundHashOperations<String, Object, Object> dataDict = stringRedisTemplate.opsForHash().getOperations().boundHashOps(I18nMPCacheKey.DATA_DICT_CACHE_KEY);
        dataDict.delete(dictType);
    }

}