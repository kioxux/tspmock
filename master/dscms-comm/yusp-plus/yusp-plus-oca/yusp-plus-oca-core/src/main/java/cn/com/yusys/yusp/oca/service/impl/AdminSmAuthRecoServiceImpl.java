package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.common.utils.GenericBuilder;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.Asserts;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.oca.dao.AdminSmAuthRecoDao;
import cn.com.yusys.yusp.oca.domain.bo.MenuAndControlAuthRecoSaveBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmAuthRecoEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserRoleRelEntity;
import cn.com.yusys.yusp.oca.domain.form.AdminTmplAuthForm;
import cn.com.yusys.yusp.oca.domain.vo.AdminDataTmplControlVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminRecoWithTmplVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmAuthRecoVo;
import cn.com.yusys.yusp.oca.domain.vo.MenuVo;
import cn.com.yusys.yusp.oca.service.AdminSmAuthRecoService;
import cn.com.yusys.yusp.oca.service.AdminSmDataAuthService;
import cn.com.yusys.yusp.oca.service.AdminSmUserRoleRelService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;


@Service("adminSmAuthRecoService")
public class AdminSmAuthRecoServiceImpl extends ServiceImpl<AdminSmAuthRecoDao, AdminSmAuthRecoEntity> implements AdminSmAuthRecoService {

    @Autowired
    private AdminSmUserRoleRelService adminSmUserRoleRelService;

    @Autowired
    private AdminSmDataAuthService adminSmDataAuthService;


    /**
     * 数据授权 查询菜单树（包含数据权限模版节点）
     * 替换sql中的union 函数
     *
     * @param sysId 系统id
     * @return list
     */
    @Override
    public List<MenuVo> selectDataPowerTree(String sysId) {

        List<MenuVo> menuTreeList = this.baseMapper.findMenuTreeList(sysId);
        List<MenuVo> contrTreeList = this.baseMapper.findContrTreeList(sysId);
        List<MenuVo> dataAuthTreeList = this.baseMapper.findDataAuthTreeList(sysId);

        menuTreeList.addAll(contrTreeList);
        menuTreeList.addAll(dataAuthTreeList);
        return menuTreeList;
    }

    /**
     * 查询对象资源关系数据
     */
    @Override
    public List<AdminSmAuthRecoVo> queryRecoInfo(String objectType, String resType, String objectId, String sysId) {

        String[] resTypeArray = resType.replaceAll("^,*|,*$", "").split(",");
        List<AdminSmAuthRecoEntity> authRecoList = this.baseMapper.selectList(new QueryWrapper<AdminSmAuthRecoEntity>()
                .eq(!StringUtils.isEmpty(objectType), "authobj_type", objectType)
                .in(!StringUtils.isEmpty(resType), "authres_type", resTypeArray)
                .eq(!StringUtils.isEmpty(objectId), "authobj_id", objectId)
                .eq(!StringUtils.isEmpty(sysId), "sys_id", sysId)
        );
        List<AdminSmAuthRecoVo> authRecoVoList = authRecoList.stream().map(adminSmAuthRecoEntity -> GenericBuilder.of(AdminSmAuthRecoVo::new)
                .with(AdminSmAuthRecoVo::setAuthRecoId, adminSmAuthRecoEntity.getAuthRecoId())
                .with(AdminSmAuthRecoVo::setSysId, adminSmAuthRecoEntity.getSysId())
                .with(AdminSmAuthRecoVo::setAuthobjType, adminSmAuthRecoEntity.getAuthobjType())
                .with(AdminSmAuthRecoVo::setAuthobjId, adminSmAuthRecoEntity.getAuthobjId())
                .with(AdminSmAuthRecoVo::setAuthresType, adminSmAuthRecoEntity.getAuthresType())
                .with(AdminSmAuthRecoVo::setAuthresId, adminSmAuthRecoEntity.getAuthresId())
                .with(AdminSmAuthRecoVo::setLastChgUsr, adminSmAuthRecoEntity.getLastChgUsr())
                .with(AdminSmAuthRecoVo::setLastChgDt, adminSmAuthRecoEntity.getLastChgDt())
                .with(AdminSmAuthRecoVo::setMenuId, adminSmAuthRecoEntity.getMenuId())
                .build()).collect(Collectors.toList());

        return authRecoVoList;
    }

    /**
     * 保存数据授权
     */
    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public int adminSmAuthRecoService(List<AdminSmAuthRecoEntity> authRecoList) {

        //delete 之前设置数据关系
        AdminSmAuthRecoEntity adminSmAuthReco = authRecoList.get(0);
        int delete = this.baseMapper.delete(new QueryWrapper<AdminSmAuthRecoEntity>()
                .eq(!StringUtils.isEmpty(adminSmAuthReco.getAuthobjType()), "authobj_type", adminSmAuthReco.getAuthobjType())
                .eq("authres_type", "D")
                .eq(!StringUtils.isEmpty(adminSmAuthReco.getAuthobjId()), "authobj_id", adminSmAuthReco.getAuthobjId())
                .eq(!StringUtils.isEmpty(adminSmAuthReco.getSysId()), "sys_id", adminSmAuthReco.getSysId())
        );
        boolean saveBatch = this.saveBatch(authRecoList);
        if (delete < 1 || !saveBatch) {
            return 0;
        }
        return 1;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ResultDto saveMenuAndControlAuthReco(MenuAndControlAuthRecoSaveBo authRecoSaveBo) {

        // 1. 删除原授权记录
        List<AdminSmAuthRecoVo> menuData = authRecoSaveBo.getMenuData();
        if (!CollectionUtils.isEmpty(menuData)) {
            AdminSmAuthRecoVo adminSmAuthRecoVo = menuData.get(0);
            AdminSmAuthRecoEntity adminSmAuthRecoEntity = new AdminSmAuthRecoEntity();
            BeanUtils.beanCopy(adminSmAuthRecoVo, adminSmAuthRecoEntity);
            QueryWrapper<AdminSmAuthRecoEntity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq(!StringUtils.isEmpty(adminSmAuthRecoVo.getAuthobjId()), "authobj_id", adminSmAuthRecoVo.getAuthobjId());
            queryWrapper.in("authres_type", Arrays.asList("M", "C"));
            queryWrapper.eq(!StringUtils.isEmpty(adminSmAuthRecoVo.getSysId()), "sys_id", adminSmAuthRecoVo.getSysId());

            this.remove(queryWrapper);
        }

        // 2. 新增授权记录表
        List<AdminSmAuthRecoEntity> collect = menuData.stream().map(item -> {
            AdminSmAuthRecoEntity adminSmAuthRecoEntity = new AdminSmAuthRecoEntity();
            BeanUtils.beanCopy(item, adminSmAuthRecoEntity);
            return adminSmAuthRecoEntity;
        }).collect(Collectors.toList());

        this.saveBatch(collect);

        return ResultDto.success().code(0);
    }

    /**
     * 批量删除授权记录
     *
     * @param authresIdList
     */
    @Override
    public void deleteByList(List<String> authresIdList) {
        this.baseMapper.deleteBatchIds(authresIdList);
    }

    /**
     * 删除被拷贝角色或用户原有的权限
     *
     * @param ids
     */
    @Override
    public void deleteByAuthObjIds(String[] ids) {
        QueryWrapper<AdminSmAuthRecoEntity> wrapper = new QueryWrapper<>();
        wrapper.in("authobj_id", ids);
        this.baseMapper.delete(wrapper);
    }

    /**
     * 删除数据模板授权记录
     *
     * @param authTmplId
     */
    @Override
    public void deleteByDataAuthId(String authTmplId) {
        QueryWrapper<AdminSmAuthRecoEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("authres_id", authTmplId);
        this.baseMapper.delete(wrapper);
    }

    /**
     * 删除控制点授权，同时删除控制点关联的数据权限模板授权
     *
     * @param contrIdList
     * @param dataAuthIdList
     */
    @Override
    public void deleteByResContrList(List<String> contrIdList, List<String> dataAuthIdList) {
        QueryWrapper<AdminSmAuthRecoEntity> wrapper = new QueryWrapper<>();
        /**
         * 删除控制点授权及控制点关联的数据模板授权
         */
        if (contrIdList != null && contrIdList.size() > 0) {
            List<String> list = new ArrayList<>();
            list.addAll(contrIdList);
            list.addAll(dataAuthIdList);
            wrapper.in("authres_id", contrIdList);
            this.baseMapper.delete(wrapper);
        }
    }

    /**
     * 使用authObjId获取该用户的所有权限
     *
     * @param authObjId
     * @return
     */
    @Override
    public List<AdminSmAuthRecoEntity> getByAuthObjId(String authObjId) {
        QueryWrapper<AdminSmAuthRecoEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("AUTHOBJ_ID", authObjId);
        return this.list(wrapper);
    }

    /**
     * 使用authresId获取一个授权记录
     *
     * @param authresId
     * @return
     */
    @Override
    public AdminSmAuthRecoEntity getByAuthresId(String authresId) {
        QueryWrapper<AdminSmAuthRecoEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("authres_id", authresId);
        return this.getOne(wrapper, false);
    }

    /**
     * 使用授权类型获取授权记录
     *
     * @param d
     * @return
     */
    @Override
    public List<AdminSmAuthRecoEntity> getByAuthresType(String d) {
        QueryWrapper<AdminSmAuthRecoEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("authres_type", d);
        return this.list(wrapper);
    }

    /**
     * 使用授权对象id和授权资源对象id删除记录
     *
     * @param form
     */
    @Override
    public void removeByMenuIdAndAuthobjId(AdminTmplAuthForm form) {
        QueryWrapper<AdminSmAuthRecoEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("menu_id", form.getContrId());
        wrapper.eq("authobj_id", form.getAuthobjId());
        wrapper.eq("authres_id", form.getLastAuthresId());
        this.remove(wrapper);
    }

    /**
     * 查询多个数据模板的授权记录
     *
     * @param tmplIdList
     * @return
     */
    @Override
    public List<AdminSmAuthRecoEntity> getByAuthresIds(List<String> tmplIdList) {
        QueryWrapper<AdminSmAuthRecoEntity> recoWrapper = new QueryWrapper<>();
        recoWrapper.in(tmplIdList != null && tmplIdList.size() > 0, "authres_id", tmplIdList);
        return list(recoWrapper);
    }

    /**
     * 使用 menuId 关联数据模板查询授权记录
     *
     * @param contrId
     * @return
     */
    @Override
    public List<AdminRecoWithTmplVo> getByMenuIdWithTmpl(String contrId) {
        QueryWrapper<AdminRecoWithTmplVo> wrapper = new QueryWrapper<>();
        wrapper.eq("r.menu_id", contrId);
        return baseMapper.getByMenuIdWithTmpl(wrapper);
    }

    /**
     * 使用 userId 查询该用户的数据模板授权记录
     *
     * @param userId
     * @return
     */
    @Override
    public List<AdminDataTmplControlVo> getDataTmplControl(String userId) {
        /**
         * 1、查询出该用户的角色列表
         */
        AdminSmUserEntity adminSmUserEntity = new AdminSmUserEntity();
        adminSmUserEntity.setUserId(userId);
        List<AdminSmUserRoleRelEntity> userRoleRelsByUser = adminSmUserRoleRelService.findUserRoleRelsByUser(adminSmUserEntity);
        List<String> roleIds = userRoleRelsByUser.stream().map(AdminSmUserRoleRelEntity::getRoleId).collect(Collectors.toList());
        /**
         * 2、查询角色列表授权的数据模板列表并返回
         */
        return getDataTmplControl(roleIds);
    }

    /**
     * 批量删除菜单记录
     *
     * @param deleteIds
     */
    @Override
    public int deleteMenuAuthData(HashSet<String> deleteIds) {
        // 1、查询菜单相关已授权控制点的id集合
        QueryWrapper<String> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("menu_id", deleteIds);
        List<String> resContrIdList = this.baseMapper.selectResContrIdByMenuIds(queryWrapper);
        // 2、将查询出的控制点id集合添加到deleteIds集合当中
        deleteIds.addAll(resContrIdList);
        // 3、组合删除条件，authres_id字段会删除deleteIds直接相关的授权数据，menu_id会删除上级id为deleteIds对象的授权数据
        QueryWrapper<AdminSmAuthRecoEntity> wrapper = new QueryWrapper<>();
        wrapper.in("authres_id", deleteIds);
        wrapper.or().in("menu_id", deleteIds);
        return this.baseMapper.delete(wrapper);
    }

    @Override
    public void batchDelete(List<AdminSmAuthRecoVo> vos) {
        this.baseMapper.deleteInBatch(vos);
    }

    /**
     * 查询角色列表授权的数据模板列表
     *
     * @param roleIds
     * @return
     */
    private List<AdminDataTmplControlVo> getDataTmplControl(List<String> roleIds) {
        Asserts.nonEmpty(roleIds, "当前用户不合法，未经授权!");
        QueryWrapper<AdminDataTmplControlVo> wrapper = new QueryWrapper<>();
        wrapper.in("authobj_id", roleIds).eq("authres_type", "D");
        return baseMapper.getDataTmplControl(wrapper);
    }
}