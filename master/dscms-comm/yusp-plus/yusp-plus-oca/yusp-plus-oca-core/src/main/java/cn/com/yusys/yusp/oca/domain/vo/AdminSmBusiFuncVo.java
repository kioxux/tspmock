package cn.com.yusys.yusp.oca.domain.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统业务功能表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-20 13:43:51
 */
@Data
public class AdminSmBusiFuncVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 记录编号
     */
    private String funcId;
    /**
     * 所属功能模块编号
     */
    private String modId;
    /**
     * 功能点名称
     */
    private String funcName;

    /**
     * URL链接
     */
    private String funcUrl;

    /**
     * 顺序
     */
    private Integer funcOrder;
    /**
     * 图标
     */
    private String funcIcon;
    /**
     * 最新变更用户
     */
    private String lastChgName;
    /**
     * 最新变更时间
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

    private String funcDesc;

    private String modName;

}
