package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 产品信息表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-12-02 18:03:52
 */
@Data
@TableName("cms_product_info")
public class ProductInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 产品id
	 */
	@TableId
	private String productId;
	/**
	 * 产品名称
	 */
	private String productName;
	/**
	 * 产品描述
	 */
	private String productDesc;
	/**
	 * 产品种类0-经营类，1-担保消费类，2-信用消费类，3-综合类
	 */
	private String productCategory;
	/**
	 * 产品类型0-额度产品，1-额度项下产品，2-一般贷款产品
	 */
	private String productType;
	/**
	 * 产品起始日期
	 */
	private Date startDate;
	/**
	 * 产品结束日期
	 */
	private Date endDate;
	/**
	 * 产品经理
	 */
	private String productManage;
	/**
	 * 产品状态(0-待生效，1-已生效，2-已失效)
	 */
	private String productStatus;
	/**
	 * 主担保方式0-信用，1-抵押，2-质押，3-保证
	 */
	private String mainGuaranteeType;
	/**
	 * 最高抵质押率
	 */
	private BigDecimal maxPledgeRate;
	/**
	 * 贷款担保限额
	 */
	private BigDecimal loanGuarLimitAmt;
	/**
	 * 利率形式0-固定，1-浮动
	 */
	private String interestRateMode;
	/**
	 * 利率（月）（%）
	 */
	private BigDecimal loanRate;
	/**
	 * 浮动方式0-利差，1-百分比
	 */
	private String floatingMode;
	/**
	 * 浮动百分比上限
	 */
	private BigDecimal floatingUpperLimit;
	/**
	 * 浮动百分比下限
	 */
	private BigDecimal floatingLowerLimit;
	/**
	 * 固定利率上限（%）
	 */
	private BigDecimal fixedRateUpperLimit;
	/**
	 * 固定利率下限（%）
	 */
	private BigDecimal fixedRateLowerLimit;
	/**
	 * 浮动利差上限
	 */
	private BigDecimal floatingSpreadUpperLimit;
	/**
	 * 浮动利差下限
	 */
	private BigDecimal floatingSpreadLowerLimit;
	/**
	 * 额度类型00-循环额度，01-非循环额度
	 */
	private String limitType;
	/**
	 * 最小授信期限（月）
	 */
	private Integer minCreditTerm;
	/**
	 * 最大授信期限（月）
	 */
	private Integer maxCreditTerm;
	/**
	 * 最小贷款期限（月）（含）
	 */
	private Integer minLoanTerm;
	/**
	 * 最大贷款期限（月）（含）
	 */
	private Integer maxLoanTerm;
	/**
	 * 最低贷款金额
	 */
	private BigDecimal minLoanAmt;
	/**
	 * 最高贷款金额
	 */
	private BigDecimal maxLoanAmt;
	/**
	 * 发放方式0-一次性发放，1-分次发放，2-追加发放
	 */
	private String giveOuotType;
	/**
	 * 审批有效期（天）
	 */
	private Integer aprvEffiveDays;
	/**
	 * 利率调整方式0-每年1月1日调整，1-不调整，2-满一年调整，3-立即调整
	 */
	private String inteRateAdjustMode;
	/**
	 * 还款宽限期（月）
	 */
	private Integer repaymentGracePeriod;
	/**
	 * 提前还款最低本金（元）
	 */
	private BigDecimal repaymentMinPrincipal;
	/**
	 * 产品创建时间
	 */
	private Date createTime;
	/**
	 * 产品更新时间
	 */
	private Date updateTime;
	/**
	 * 产品信息显示状态(1-逻辑有效，0-逻辑删除)
	 */
	private String logicStatus;
	/**
	 * 机构id
	 */
	private String orgId;
	/**
	 * 机构名称
	 */
	private String orgName;
	/**
	 * 产品经理id
	 */
	private String managerId;
	/**
	 * 担保方式0-信用，1-抵押，2-质押，3-保证
	 */
	private String guaranteeType;

}
