package cn.com.yusys.yusp.oca.domain.form;

import lombok.Data;

@Data
public class AdminSmResContrForm {

    private String contrId;
    /**
     * 控制点名称
     */
    private String contrName;
    /**
     * 菜单 id
     */
    private String menuId;
    /**
     * 菜单对应的业务功能 funcId
     */
    private String funcId;
    /**
     * 控制点代码
     */
    private String contrCode;
    /**
     * 控制点 URL
     */
    private String contrUrl;
    /**
     * 关键字
     */
    private String keyWord;

    private Long page;

    private Long size;
}
