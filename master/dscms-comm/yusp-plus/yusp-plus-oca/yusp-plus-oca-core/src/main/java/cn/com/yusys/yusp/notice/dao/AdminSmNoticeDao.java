package cn.com.yusys.yusp.notice.dao;

import cn.com.yusys.yusp.notice.entity.AdminSmNoticeEntity;
import cn.com.yusys.yusp.notice.form.AdminSmNoticeAllForm;
import cn.com.yusys.yusp.notice.vo.NoticeHomePageVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;

/**
 * 系统公告表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-15 13:04:49
 */

public interface AdminSmNoticeDao extends BaseMapper<AdminSmNoticeEntity> {

    AdminSmNoticeAllForm selectModifyPage(@Param("noticeId") String noticeId);

    IPage<NoticeHomePageVo> findListByCondition(
            IPage<NoticeHomePageVo> iPage,
            @Param(Constants.WRAPPER) QueryWrapper<NoticeHomePageVo> query,
            @Param("userId") String userId);
}
