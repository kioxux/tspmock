package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserMgrOrgEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 用户授权管理机构表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-12-02 16:29:50
 */
public interface AdminSmUserMgrOrgService extends IService<AdminSmUserMgrOrgEntity> {

    List<AdminSmUserMgrOrgEntity> findOrgRelsByUser(AdminSmUserEntity user);
}