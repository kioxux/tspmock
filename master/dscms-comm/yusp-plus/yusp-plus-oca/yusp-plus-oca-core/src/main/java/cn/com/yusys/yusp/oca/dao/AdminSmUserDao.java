package cn.com.yusys.yusp.oca.dao;

import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.xdxt0015.req.Xdxt0015DataReqDto;
import cn.com.yusys.yusp.notice.vo.AdminSmReciveVo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmUserDetailVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmUserVo;
import cn.com.yusys.yusp.oca.domain.vo.UserSubscribeVo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 系统用户表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-16 15:31:54
 */

public interface AdminSmUserDao extends BaseMapper<AdminSmUserEntity> {

    List<AdminSmUserVo> selectAllUser(@Param(Constants.WRAPPER) LambdaQueryWrapper<AdminSmUserVo> queryWrapper);

    List<AdminSmUserVo> selectAllUserForXw(@Param(Constants.WRAPPER) LambdaQueryWrapper<AdminSmUserVo> queryWrapper);

    AdminSmUserDetailVo getDetailById(String userId);

    List<UserSubscribeVo> selectUserSubscribeVoList();

    Integer countRel(String userId);

    void updateLoginTime(@Param("userId") String userId, @Param("lastLoginTime") Date lastLoginTime);

    void updateState(@Param("userId") String userId, @Param("userState") String userState);

    Page<AdminSmUserVo> getUsersForWf(Page<AdminSmUserVo> page, @Param(Constants.WRAPPER) QueryWrapper<AdminSmUserVo> wrapper);

    List<AdminSmUserVo> getUsersByOrgForWf(String orgId);

    List<AdminSmUserVo> getUsersByOrgAndDutyForWf(String userId);

    List<AdminSmUserVo> getUsersByDeptForWf(String deptId);

    List<AdminSmUserVo> getUsersByDutyForWf(String dutyId);

    List<AdminSmUserVo> getUsersByRoleForWf(String roleId);

    AdminSmUserVo getUserInfoForWf(String userId);

    /**
     * 获取用户的 roleId 和 objId
     *
     * @param userId
     * @return
     */
    List<AdminSmReciveVo> selectRoleAndObj(String userId);

    /**
     * xdxt0003 分页查询小微客户经理
     *
     * @param loginCode
     * @param userName
     * @return
     */
    List<AdminSmUserVo> getByLoginCodeAndUsernamePage(@Param("loginCode") String loginCode, @Param("userName") String userName);

    /**
     * 用户机构角色信息列表查询
     *
     * @param xdxt0015DataReqDto
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdxt0015.resp.List> getXdxt0015(Xdxt0015DataReqDto xdxt0015DataReqDto);

    /**
     * 查询是否小微客户经理
     *
     * @param loginCode
     * @return
     */
    int getIsXWUserByLoginCode(String loginCode);

    /**
     * 查询信贷用户的特定岗位信息
     *
     * @param userAndDutyReqDto
     * @return
     */
    List<UserAndDutyRespDto> getUserAndDuty(UserAndDutyReqDto userAndDutyReqDto);

    /**
     * 通用客户经理查询
     *
     * @param commonUserQueryReqDto
     * @return
     */
    List<CommonUserQueryRespDto> getCommonUserInfo(CommonUserQueryReqDto commonUserQueryReqDto);

    /**
     * 通过流程实例号获取当前处理用户的详细信息
     *
     * @param instanceId
     * @return
     */
    AdminSmUserEntity getTodoUserInfo(String instanceId);

    /**
     * 查询在途任务数量
     *
     * @param cusId
     * @return
     */
    int queryCusFlowCount(String cusId);

    /**
     * 根据岗位编号查询用户信息
     *
     * @param iPage
     * @param orgCode
     * @return
     */
    IPage<AdminSmUserDto> getUserInfoByOrgCode(IPage<AdminSmUserDto> iPage, @Param("orgCode") String orgCode);

    List<AdminSmUserEntity> getJzzyUserList(@Param("xwMainOrgCode") String xwMainOrgCode, @Param("localOrgTypes") String localOrgTypes, @Param("spelOrgIds") String spelOrgIds);
}
