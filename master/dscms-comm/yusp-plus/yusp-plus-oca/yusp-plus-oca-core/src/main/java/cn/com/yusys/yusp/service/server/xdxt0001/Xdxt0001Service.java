package cn.com.yusys.yusp.service.server.xdxt0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.context.UserContext;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.OcaTradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxt0001.req.Xdxt0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0001.resp.Xdxt0001DataRespDto;
import cn.com.yusys.yusp.enums.OcaTradeEnum;
import cn.com.yusys.yusp.oca.dao.AdminSmUserDao;
import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.service.AdminSmOrgService;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import cn.com.yusys.yusp.oca.utils.PasswordUtils;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: yusp-oca模块
 * @类名称: Xdxt0001Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-27 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxt0001Service extends ServiceImpl<AdminSmUserDao, AdminSmUserEntity> {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0001Service.class);

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private PasswordUtils passwordUtils;

    /**
     * 信贷同步人力资源
     *
     * @param xdxt0001DataReqDto
     * @return
     */
    @Transactional
    public Xdxt0001DataRespDto getXdxt0001(Xdxt0001DataReqDto xdxt0001DataReqDto) {
        logger.info(OcaTradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0001.key, OcaTradeEnum.TRADE_CODE_XDXT0003.value, JSON.toJSONString(xdxt0001DataReqDto));
        Xdxt0001DataRespDto xdxt0001DataRespDto = new Xdxt0001DataRespDto();
        try {
            AdminSmUserEntity adminSmUserEntity = new AdminSmUserEntity();
            //账号
            String loginCode = xdxt0001DataReqDto.getWorkNo();
            // 如果不在高管名单列表中，进行相关数据操作
            if (!SeniorManagerEnum.isSeniorManager(loginCode)) {
                //用户名称
                String userName = xdxt0001DataReqDto.getName();
                //员工状态
                String status = xdxt0001DataReqDto.getEmployeeStatusNo();
                AvailableStateEnum userSts = getState(status);
                //机构号
                String orgId = xdxt0001DataReqDto.getCoreWorkDeptNo();
                if (Objects.isNull(userSts) || StringUtils.isEmpty(userName) || StringUtils.isEmpty(loginCode)) {
                    throw BizException.error(null, OcaTradeEnum.EOCA080002.key, OcaTradeEnum.EOCA080002.value);
                }
                adminSmUserEntity.setUserSts(userSts);
                adminSmUserEntity.setLoginCode(loginCode);
                // modify by 顾银华，移除人力资源系统修改新信贷机构信息，如果推送机构为空，那么将用户默认在总行归属下，解决页面列表数据查询不出问题
                if ("".equalsIgnoreCase(org.apache.commons.lang.StringUtils.trimToEmpty(orgId))) {
                    throw BizException.error(null, OcaTradeEnum.EOCA080004.key, OcaTradeEnum.EOCA080004.value);
                }
                //出生日期转换
                String birthday = Optional.ofNullable(xdxt0001DataReqDto.getBirday()).orElse(Strings.EMPTY);
                Date userBirthday = null;
                if (Strings.isNotEmpty(birthday)) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    userBirthday = simpleDateFormat.parse(birthday);
                }
                adminSmUserEntity.setUserBirthday(userBirthday);
                //手机号
                adminSmUserEntity.setUserMobilephone(Optional.ofNullable(xdxt0001DataReqDto.getMobile()).orElse(Strings.EMPTY));
                //证件号
                adminSmUserEntity.setCertNo(Optional.ofNullable(xdxt0001DataReqDto.getCertNo()).orElse(Strings.EMPTY));
                //邮箱
                adminSmUserEntity.setUserEmail(Optional.ofNullable(xdxt0001DataReqDto.getEmail()).orElse(Strings.EMPTY));
                //员工号
                adminSmUserEntity.setUserCode(loginCode);
                //员工姓名
                adminSmUserEntity.setUserName(userName);
                //是否信贷人员标识
                adminSmUserEntity.setIsCredit(Optional.ofNullable(xdxt0001DataReqDto.getIsCretPerson()).orElse(Strings.EMPTY));
                Integer num = this.baseMapper.selectCount(new QueryWrapper<AdminSmUserEntity>().eq("LOGIN_CODE", loginCode));
                if (num > 0) {
                    //修改
                    this.baseMapper.update(adminSmUserEntity, new UpdateWrapper<AdminSmUserEntity>().eq("LOGIN_CODE", loginCode));
                } else {
                    //新增
                    adminSmUserEntity.setUserId(loginCode);
                    UserContext userContext = new UserContext();
                    userContext.setLoginCode(loginCode);
                    // 吴佳欢提出只有新增时，保留用户名称，同步不做调整
                    adminSmUserService.save(adminSmUserEntity);
                }
            }
            xdxt0001DataRespDto.setOpFlag(OcaTradeEnum.OPERAION_SUCCESS.key);
            xdxt0001DataRespDto.setOpMsg(OcaTradeEnum.OPERAION_SUCCESS.value);
        } catch (BizException e) {
            xdxt0001DataRespDto.setOpFlag(OcaTradeEnum.OPERAION_FAIL.key);
            xdxt0001DataRespDto.setOpMsg(OcaTradeEnum.OPERAION_FAIL.value);
            logger.info(OcaTradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0001.key, OcaTradeEnum.TRADE_CODE_XDXT0001.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            xdxt0001DataRespDto.setOpFlag(OcaTradeEnum.OPERAION_FAIL.key);
            xdxt0001DataRespDto.setOpMsg(OcaTradeEnum.OPERAION_FAIL.value);
            logger.info(OcaTradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0001.key, OcaTradeEnum.TRADE_CODE_XDXT0001.value, e.getMessage());
        }
        logger.info(OcaTradeLogConstants.SERVICE_END_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0001.key, OcaTradeEnum.TRADE_CODE_XDXT0001.value, JSON.toJSONString(xdxt0001DataRespDto));
        return xdxt0001DataRespDto;
    }

    /**
     * 员工状态字典转换
     * A：生效 I：失效 W：待生效
     *
     * @param status
     * @return
     */
    public AvailableStateEnum getState(String status) {
        AvailableStateEnum state = AvailableStateEnum.ENABLED;
        if (status != null && !"".equals(status)) {
            //1在岗；10提前离岗；2内退返聘；3内退；4退休；5身故；6离职；7离岗；8返岗；9内部转岗
            if ("1".equals(status) || "2".equals(status) || "8".equals(status) || "9".equals(status)) {
                //启用
                state = AvailableStateEnum.ENABLED;
            } else if ("3".equals(status) || "4".equals(status) || "5".equals(status) || "6".equals(status) || "7".equals(status) || "10".equals(status)) {
                //停用
                state = AvailableStateEnum.DISABLED;
            } else {
                state = null;
            }
        }
        return state;
    }
}