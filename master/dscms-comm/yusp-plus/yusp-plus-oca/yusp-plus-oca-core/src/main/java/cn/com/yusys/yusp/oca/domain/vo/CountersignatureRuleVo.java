package cn.com.yusys.yusp.oca.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *@program: yusp-plus
 *@description: 会签规则Vo
 *@author: wujiangpeng
 *@email: wujp4@yusys.com.cn
 *@create: 2020-12-02 14:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CountersignatureRuleVo {
    private String countersignatureRuleId;
    private String countersignatureRuleName;
}
