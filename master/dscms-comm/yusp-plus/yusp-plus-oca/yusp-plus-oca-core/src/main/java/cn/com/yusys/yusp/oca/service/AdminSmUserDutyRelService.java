package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.GetUserInfoByDutyCodeDto;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserDutyRelEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Set;

/**
 * 用户与岗位关联关系表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-12-01 21:55:19
 */
public interface AdminSmUserDutyRelService extends IService<AdminSmUserDutyRelEntity> {


    Set<String> findUserIdsByDutyId(String dutyId);

    List<AdminSmUserDutyRelEntity> findUserDutyRelsByUser(AdminSmUserEntity user);

    boolean save(List<AdminSmUserDutyRelEntity> entityList);

    String getDutyId(String userId);

    List<String> getUserIds(String dutyId);


    /**
     * 根据岗位编号分页查询用户信息
     *
     * @param getUserInfoByDutyCodeDto
     * @return
     */
    List<AdminSmUserDto> getUserList(@RequestBody GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto);
}

