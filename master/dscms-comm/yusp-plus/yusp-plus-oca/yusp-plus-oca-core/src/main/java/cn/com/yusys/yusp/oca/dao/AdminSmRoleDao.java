package cn.com.yusys.yusp.oca.dao;

import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmRoleEntity;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmRoleDetailVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmRoleVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmUserVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统角色表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-18 18:06:35
 */

public interface AdminSmRoleDao extends BaseMapper<AdminSmRoleEntity> {

    List<AdminSmRoleVo> selectAllRole(@Param(Constants.WRAPPER) QueryWrapper<AdminSmRoleVo> queryWrapper);

    AdminSmRoleDetailVo selectDetailById(String roleId);

    Page<AdminSmRoleVo> getRolesForWf(Page<AdminSmRoleVo> page, @Param(Constants.WRAPPER) QueryWrapper<AdminSmRoleVo> queryWrapper);

    List<AdminSmRoleVo> getUserRoleByLoginCode(String loginCode);

    /**
     * 根据角色编号查询用户信息
     *
     * @param iPage
     * @param roleCode
     * @return
     */
    IPage<AdminSmUserDto> getUserInfoByRoleCode(IPage<AdminSmUserDto> iPage, @Param("roleCode") String roleCode);

    /**
     * 根据角色编号查询用户信息 不分页
     * @param roleCode
     * @return
     */
    List<AdminSmUserDto> getUserInfoByRoleCode(@Param("roleCode") String roleCode);
    /**
     * 根据机构角色编号查询用户信息
     */
    Page<AdminSmUserVo> getUserInfoByOrgRoleCode(Page<AdminSmUserVo> page, @Param(Constants.WRAPPER) QueryWrapper<AdminSmUserVo> queryWrapper);
}