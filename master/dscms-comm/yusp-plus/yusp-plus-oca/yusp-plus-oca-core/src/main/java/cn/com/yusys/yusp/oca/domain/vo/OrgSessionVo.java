package cn.com.yusys.yusp.oca.domain.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *@program: yusp-plus
 *@description: session中机构信息
 *@author: wujiangpeng
 *@email: wujp4@yusys.com.cn
 *@create: 2020-11-19 11:45
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrgSessionVo {
    /**
     * 记录编号
     */
    @JsonProperty(value = "id")
    private String orgId;
    /**
     * 角色代码
     */
    @JsonProperty(value = "code")
    private String orgCode;
    /**
     * 角色名称
     */
    @JsonProperty(value = "name")
    private String orgName;

    /**
     * 机构层级
     */
    @JsonProperty(value = "orgLevel")
    private String orgLevel;
}
