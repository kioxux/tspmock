package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.oca.domain.entity.AdminSmPropEntity;
import cn.com.yusys.yusp.common.query.PageQuery;
import lombok.Data;

/**
 * 系统参数查询参数
 * @author JP
 */
@Data
public class AdminSmPropQuery extends PageQuery<AdminSmPropEntity> {
    private String propName;
    private String propDesc;
    private String keyWord;
}
