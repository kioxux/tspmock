package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.common.utils.RAMPager;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.ObjectUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.GetUserInfoByRoleCodeDto;
import cn.com.yusys.yusp.oca.dao.AdminSmRoleDao;
import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmOrgEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmRoleEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserRoleRelEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmPasteRoleQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmRoleQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmUserQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmOrgVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmRoleDetailVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmRoleVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmUserVo;
import cn.com.yusys.yusp.oca.service.AdminSmOrgService;
import cn.com.yusys.yusp.oca.service.AdminSmRoleService;
import cn.com.yusys.yusp.oca.service.AdminSmUserRoleRelService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author terry
 * @email tanrui1@yusys.com.cn
 */
@Service("adminSmRoleService")
@Slf4j
public class AdminSmRoleServiceImpl extends ServiceImpl<AdminSmRoleDao, AdminSmRoleEntity> implements AdminSmRoleService {


    @Autowired
    AdminSmOrgService adminSmOrgService;

    @Autowired
    AdminSmUserRoleRelService userRoleRelService;

    /**
     * 分页查询
     *
     * @param query
     * @return
     */
    @Override
    public Page<AdminSmRoleVo> queryPage(AdminSmRoleQuery query) {

        Page<AdminSmRoleVo> page = query.getIPage();
        //没有对应mapper的Vo或带有@TableField(exist = false) 注解的属性不能作为lambda查询的条件，因为没有加载对应缓存，需要手动缓存
        QueryWrapper<AdminSmRoleVo> wrapper = new QueryWrapper<AdminSmRoleVo>()
                .like(StringUtils.nonEmpty(query.getRoleCode()), "r.role_code", query.getRoleCode())
                .like(StringUtils.nonEmpty(query.getRoleName()), "r.role_name", query.getRoleName())
                .eq(ObjectUtils.nonNull(query.getRoleSts()), "r.role_sts", query.getRoleSts());

        if (StringUtils.nonEmpty(query.getKeyWord())) {
            wrapper.and(r -> r
                    .like(StringUtils.nonEmpty(query.getKeyWord()), "r.role_code", query.getKeyWord())//关键字模糊匹配角色编码
                    .or()
                    .like(StringUtils.nonEmpty(query.getKeyWord()), "r.role_name", query.getKeyWord())//关键字模糊匹配角色名称
            );
        }
        wrapper.orderByDesc("r.last_chg_dt");
        final List<AdminSmRoleVo> roleVoList = this.baseMapper.selectAllRole(wrapper);
        List<String> orgIds;
        if (StringUtils.isEmpty(query.getOrgId())) {
            orgIds = adminSmOrgService.getAllAccessibleOrgIds();//未指定orgId就查询当前用户有权访问的机构下的所有角色
        } else {
            orgIds = adminSmOrgService.getAllProgeny(query.getOrgId()).stream().map(AdminSmOrgVo::getOrgId).collect(Collectors.toList());//指定了orgId就查指定机构及其下级机构下的角色
        }
        List<AdminSmRoleVo> roleResultList = roleVoList.stream().filter(item -> orgIds.contains(item.getOrgId())).collect(Collectors.toList());
        List<AdminSmRoleVo> sortedResultList = roleResultList.stream().sorted((node1, node2) -> DateUtils.compare(node2.getLastChgDt(), node1.getLastChgDt())).collect(Collectors.toList());//按日期降序
        RAMPager<AdminSmRoleVo> pager = new RAMPager<>(sortedResultList, query.getSize());
        page.setRecords(pager.page(query.getPage()));
        page.setTotal(sortedResultList.size());
        return page;
    }

    @Override
    public AdminSmRoleDetailVo getDetailById(String roleId) {
        //Asserts.nonNull(roleId, "roleId can not be null or empty");
        //mxz
        if (roleId == null) {
            throw BizException.error(null, "50800003", "角色代码为空");
        }
        return this.baseMapper.selectDetailById(roleId);
    }


    /**
     * 批量启用
     *
     * @param ids
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchEnable(String[] ids) {
        if (CollectionUtils.nonEmpty(ids)) {
            List<String> idList = Arrays.asList(ids);
            idList.forEach((id) -> {
                AdminSmRoleEntity entity = new AdminSmRoleEntity();
                entity.setRoleId(id);
                entity.setRoleSts(AvailableStateEnum.ENABLED);
                this.baseMapper.updateById(entity);
            });
        }
    }

    /**
     * 批量停用
     *
     * @param ids
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchDisable(String[] ids) {
        if (CollectionUtils.nonEmpty(ids)) {
//            List<String> idList = Arrays.stream(ids.split(",")).filter(StringUtils::nonEmpty).collect(Collectors.toList());
            List<String> idList = Arrays.asList(ids);

            idList.forEach((id) -> {
//                if (checkBlocked(id)) {
//                    throw BizException.error("inUsing", null, id);
//                } else {
//                    AdminSmRoleEntity entity = new AdminSmRoleEntity();
//                    entity.setRoleId(id);
//
//                    entity.setRoleSts(AvailableStateEnum.DISABLED);
//                    this.baseMapper.updateById(entity);
//                }
                AdminSmRoleEntity entity = new AdminSmRoleEntity();
                entity.setRoleId(id);

                entity.setRoleSts(AvailableStateEnum.DISABLED);
                this.baseMapper.updateById(entity);
            });
        }
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchDelete(String[] ids) {
        for (String id : ids) {
            System.out.println(":" + id);
        }
        if (CollectionUtils.nonEmpty(ids)) {
            List<String> idList = Arrays.asList(ids);
            idList.forEach((id) -> {
                if (checkBlocked(id)) {
                    throw BizException.error(null, "50800001", "该角色绑定有用户，请删除关联信息后操作");
                } else {
                    this.baseMapper.deleteById(id);
                }
            });
        }
    }

    @Override
    public Page<AdminSmRoleVo> queryPageExcept(AdminSmPasteRoleQuery query) {

        //Asserts.notEmpty(query.getExpectedRoleId(), "Which role do you want to except?");
        //你想除去哪个角色
        if (query.getExpectedRoleId() == null || query.getExpectedRoleId().trim().length() == 0) {
            throw BizException.error(null, "50800003", "角色代码为空");
        }
        Page<AdminSmRoleVo> page = query.getIPage();

        QueryWrapper<AdminSmRoleVo> wrapper = new QueryWrapper<>();

        if (StringUtils.nonEmpty(query.getKeyWord())) {
            wrapper.and(r -> r
                    .like(StringUtils.nonEmpty(query.getKeyWord()), "r.role_code", query.getKeyWord())//关键字模糊匹配角色编码
                    .or()
                    .like(StringUtils.nonEmpty(query.getKeyWord()), "r.role_name", query.getKeyWord())//关键字模糊匹配角色名称
            );
        }
        List<AdminSmRoleVo> roleVoList = this.baseMapper.selectAllRole(wrapper);
        roleVoList = roleVoList.stream().filter((role) -> !role.getRoleId().equals(query.getExpectedRoleId())).collect(Collectors.toList());//排除指定角色id
        List<String> orgIds = adminSmOrgService.getAllAccessibleOrgIds();
        List<AdminSmRoleVo> filteredList = roleVoList.stream().filter((role) -> orgIds.contains(role.getOrgId())).collect(Collectors.toList());
        List<AdminSmRoleVo> sortedResultList = filteredList.stream().sorted((node1, node2) -> DateUtils.compare(node2.getLastChgDt(), node1.getLastChgDt())).collect(Collectors.toList());//按日期降序
        RAMPager<AdminSmRoleVo> pager = new RAMPager<>(sortedResultList, query.getSize());
        page.setRecords(pager.page(query.getPage()));
        page.setTotal(sortedResultList.size());
        return page;
    }

    @Override
    public Page<AdminSmRoleVo> getRolesForWf(AdminSmRoleQuery query) {
        Page<AdminSmRoleVo> page = query.getIPage();
        QueryWrapper<AdminSmRoleVo> wrapper = new QueryWrapper<>();
        wrapper.eq("T1.ROLE_STS", "A");
        creatWrapper(wrapper, "T1.ORG_ID", query.getOrgId());
        creatWrapper(wrapper, "T1.ROLE_CODE", query.getRoleCode());
        creatWrapper(wrapper, "T1.ROLE_NAME", query.getRoleName());
        return this.baseMapper.getRolesForWf(page, wrapper);
    }

    public List<AdminSmRoleVo> getUserRoleByLoginCode(String loginCode) {
        return this.baseMapper.getUserRoleByLoginCode(loginCode);
    }


    public void creatWrapper(QueryWrapper<AdminSmRoleVo> wrapper, String column, String value) {
        if (StringUtils.nonEmpty(value)) {
            boolean condition = value.startsWith("%") || value.endsWith("%");
            wrapper.like(condition, column, value);
            wrapper.eq(!condition, column, value);
        }
    }

    /**
     * 检查角色是否已关联用户信息
     *
     * @param roleId
     * @return
     */
    private boolean checkBlocked(String roleId) {
        LambdaQueryWrapper<AdminSmUserRoleRelEntity> relWrapper = new QueryWrapper<AdminSmUserRoleRelEntity>().lambda();
        relWrapper.eq(AdminSmUserRoleRelEntity::getRoleId, roleId);
        int countUser = this.userRoleRelService.count(relWrapper);//是否有关联用户
        return countUser > 0;
    }

    @Override
    public boolean save(AdminSmRoleEntity entity) {
        LambdaQueryWrapper<AdminSmRoleEntity> codeWrapper = new QueryWrapper<AdminSmRoleEntity>().lambda();
        codeWrapper.eq(AdminSmRoleEntity::getRoleCode, entity.getRoleCode());
        AdminSmRoleEntity check = this.baseMapper.selectOne(codeWrapper);
        if (Objects.nonNull(check)) {
            throw BizException.error("exist", "50800002", entity.getRoleCode());
        }
        AdminSmOrgEntity org = adminSmOrgService.getById(entity.getOrgId());
        if (ObjectUtils.nonNull(org)) {
            entity.setRoleLevel(org.getOrgLevel());
        }

        entity.setRoleSts(Optional.ofNullable(entity.getRoleSts()).orElse(AvailableStateEnum.UNENABLED));//新增的数据默认是待生效的
        log.info("New role data: [new role: {}] ", entity.getRoleName());
        return this.baseMapper.insert(entity) > 0;
    }


    @Override
    public boolean updateById(AdminSmRoleEntity entity) {
        if (AvailableStateEnum.DISABLED.equals(entity.getRoleSts()) && checkBlocked(entity.getRoleId())) {//改为停用时要判断是否关联其他信息
            throw BizException.error(null, "50800001", "该角色绑定有用户，请删除关联信息后操作");
        }
        entity.setRoleSts(Optional.ofNullable(entity.getRoleSts()).orElse(AvailableStateEnum.UNENABLED));
        return super.updateById(entity);
    }

    /**
     * 根据角色编号查询用户信息
     *
     * @param getUserInfoByRoleCodeDto
     * @return
     */
    @Override
    public List<AdminSmUserDto> getUserInfoByRoleCode(GetUserInfoByRoleCodeDto getUserInfoByRoleCodeDto) {
        //角色编号
        String roleCode = getUserInfoByRoleCodeDto.getRoleCode();
        //页码
        Integer pageNum = Optional.ofNullable(getUserInfoByRoleCodeDto.getPageNum()).orElse(1);
        //条数
        Integer pageSize = Optional.ofNullable(getUserInfoByRoleCodeDto.getPageSize()).orElse(10);
        IPage<AdminSmUserDto> iPage = new Page<>(pageNum, pageSize);
        iPage = this.baseMapper.getUserInfoByRoleCode(iPage, roleCode);
        List<AdminSmUserDto> list = iPage.getRecords();
        return list;
    }

    @Override
    public List<AdminSmUserDto> getUserInfoByRoleCode(String roleCode) {
        return this.baseMapper.getUserInfoByRoleCode(roleCode);
    }

    /**
     * 根据机构角色编号查询用户信息

     */
    @Override
    public Page<AdminSmUserVo> getUserInfoByOrgRoleCode(AdminSmUserQuery query) {
        Page<AdminSmUserVo> page = query.getIPage();
        QueryWrapper<AdminSmUserVo> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.nonEmpty(query.getOrgId()), "C.org_id", query.getOrgId());
        wrapper.eq(StringUtils.nonEmpty(query.getRoleCode()), "A.role_code", query.getRoleCode());
        wrapper.like(StringUtils.nonEmpty(query.getLoginCode()), "C.login_code", query.getLoginCode());
        wrapper.like(StringUtils.nonEmpty(query.getUserName()), "C.user_name", query.getUserName());
        return this.baseMapper.getUserInfoByOrgRoleCode(page, wrapper);
    }
}