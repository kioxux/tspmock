package cn.com.yusys.yusp.oca.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @program: yusp-plus
 * @description: 密码加密类型配置类
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2021-01-11 13:46
 */
@Configuration
public class PasswordEncoderConfig {

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
