package cn.com.yusys.yusp.message.dao;

import cn.com.yusys.yusp.message.entity.MessageTempEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 消息模板配置
 *
 * @author xiaodg@yusys.com.cn
 */

public interface MessageTempDao extends BaseMapper<MessageTempEntity> {

}
