package cn.com.yusys.yusp.notice.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2021-03-31 11:00:50
 */
@Data
@TableName("admin_sm_richedit_file_info")
public class AdminSmRicheditFileInfoEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 唯一主键
     */
    @TableId
    private String fileId;
    /**
     * 文件名称
     */
    private String fileName;
    /**
     * 文件存储路径
     */
    private String filePath;
    /**
     * 文件大小
     */
    private BigDecimal fileSize;
    /**
     * 文件扩展名
     */
    private String extName;
    /**
     * 文件虚拟文件夹
     */
    private String parentFolder;
    /**
     * 业务流水号
     */
    private String busNo;
    /**
     * 上传时间
     */
    private String uploadTime;
    /**
     * 备注
     */
    private String fileRemark;

}
