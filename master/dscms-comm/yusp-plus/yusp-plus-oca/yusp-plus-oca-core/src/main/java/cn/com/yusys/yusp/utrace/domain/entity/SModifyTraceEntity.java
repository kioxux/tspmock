package cn.com.yusys.yusp.utrace.domain.entity;

import cn.com.yusys.yusp.oca.config.annotation.RedisDictTranslator;
import cn.com.yusys.yusp.oca.domain.constants.Constants;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 小U留痕记录表
 *
 * @author
 * @email
 * @date 2021-05-17 15:13:18
 */
@Data
@TableName("s_modify_trace")
public class SModifyTraceEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId

    private String seqid;
    /**
     * 操作用户ID
     */
    @RedisDictTranslator(redisCacheKey = Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_USER_NAME, fieldName = "userName")
    private String usrId;
    /**
     * 菜单ID
     */
    @JsonProperty("mMenuId")
    private String mMenuId;
    /**
     * 数据主键
     */
    @JsonProperty("mPkV")
    private String mPkV;
    /**
     * 机构ID
     */
    private String orgId;
    /**
     * 表单字段ID
     */
    @JsonProperty("mFieldId")
    private String mFieldId;
    /**
     * 表单字段名称
     */
    @JsonProperty("mFieldNm")
    private String mFieldNm;
    /**
     * 字段原值
     */
    @JsonProperty("mOldV")
    private String mOldV;
    /**
     * 字段原值描述
     */
    @JsonProperty("mOldDispV")
    private String mOldDispV;
    /**
     * 字段新值
     */
    @JsonProperty("mNewV")
    private String mNewV;
    /**
     * 字段新值描述
     */
    @JsonProperty("mNewDispV")
    private String mNewDispV;
    /**
     * 记录时间
     */
    @JsonProperty("mDatetime")
    private String mDatetime;

}
