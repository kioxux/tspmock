package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 认证策略参数表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2021-03-30 11:27:32
 */
@Data
@TableName("admin_sm_crel_stra")
public class AdminSmCrelStraEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 记录编号
     */
    @TableId(type = IdType.UUID)
    private String crelId;
    /**
     * 逻辑系统编号
     */
    private String sysId;
    /**
     * 策略标识
     */
    private String crelKey;
    /**
     * 策略名称
     */
    private String crelName;
    /**
     * 是否启用 1:是 2:否
     */
    private String enableFlag;
    /**
     * 策略明细
     */
    private String crelDetail;
    /**
     * 策略描述
     */
    private String crelDescribe;
    /**
     * 执行动作1: 冻结用户 2:禁止 3：警告
     */
    private String actionType;
    /**
     * 最新变更用户
     */
    private String lastChgUsr;
    /**
     * 最新变更时间
     */
    private Date lastChgDt;
    /**
     * 是否为系统生成
     */
    private Integer sysDefault;

}
