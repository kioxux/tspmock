/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dscms.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dscms.domain.EmpAttendInfo;
import cn.com.yusys.yusp.dscms.service.EmpAttendInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: EmpAttendInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: kioxu
 * @创建时间: 2021-05-10 15:30:32
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/empattendinfo")
public class EmpAttendInfoResource {
    @Autowired
    private EmpAttendInfoService empAttendInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<EmpAttendInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<EmpAttendInfo> list = empAttendInfoService.selectAll(queryModel);
        return new ResultDto<List<EmpAttendInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<EmpAttendInfo>> index(@RequestBody QueryModel queryModel) {
        //历史查询，将条件置空
        queryModel.addCondition("default","1");
        queryModel.setSort("appTime desc");
        List<EmpAttendInfo> list = empAttendInfoService.selectByModel(queryModel);
        return new ResultDto<List<EmpAttendInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<EmpAttendInfo> show(@PathVariable("serno") String serno) {
        EmpAttendInfo empAttendInfo = empAttendInfoService.selectByPrimaryKey(serno);
        return new ResultDto<EmpAttendInfo>(empAttendInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<EmpAttendInfo> create(@RequestBody EmpAttendInfo empAttendInfo) throws URISyntaxException {
        empAttendInfoService.insert(empAttendInfo);
        return new ResultDto<EmpAttendInfo>(empAttendInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody EmpAttendInfo empAttendInfo) throws URISyntaxException {
        int result = empAttendInfoService.update(empAttendInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = empAttendInfoService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = empAttendInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/his")
    protected ResultDto<List<EmpAttendInfo>> indexHis(@RequestBody QueryModel queryModel) {
        //历史查询，将条件置空
        queryModel.addCondition("default","2");
        queryModel.setSort("appTime desc");
        List<EmpAttendInfo> list = empAttendInfoService.selectByModel(queryModel);
        return new ResultDto<List<EmpAttendInfo>>(list);
    }


    /**
     * 校验用户的签到签退状况，请假销假状况
     * @函数名称:usercheck
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/usercheck")
    protected ResultDto<String> usercheck(@RequestBody EmpAttendInfo empAttendInfo) {
        String msg = empAttendInfoService.usercheck(empAttendInfo.getUserCode());
        return new ResultDto<>(msg);
    }

}
