package cn.com.yusys.yusp.oca.domain.form;

import lombok.Data;

/**
 * @author danyu
 */
@Data
public class AdminSmDutyForm {

	/**
	 * 记录编号
	 */
	private String dutyId;
	/**
	 * 岗位代码
	 */
	private String dutyCode;
	/**
	 * 岗位名称
	 */
	private String dutyName;
	/**
	 * 所属机构编号
	 */
	private String belongOrgId;
	/**
	 * 备注
	 */
	private String dutyRemark;
	/**
	 * 状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效
	 */
	private String dutySts;


}
