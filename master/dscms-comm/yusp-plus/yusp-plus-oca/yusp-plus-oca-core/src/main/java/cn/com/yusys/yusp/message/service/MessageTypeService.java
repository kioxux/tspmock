package cn.com.yusys.yusp.message.service;


import cn.com.yusys.yusp.message.entity.MessageTypeEntity;
import cn.com.yusys.yusp.message.query.MessageTypePage;
import cn.com.yusys.yusp.message.vo.MessageTypeVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 消息类型服务类
 *
 * @author xiaodg@yusys.com.cn
 */
public interface MessageTypeService extends IService<MessageTypeEntity> {

    /**
     * 分页查询消息类型
     *
     * @param messageTypeQuery 消息类型查询对象 {@link MessageTypePage}
     * @return {@link IPage< MessageTypeVo >}
     */
    IPage<MessageTypeVo> queryPage(MessageTypePage messageTypeQuery);

    /**
     * 分页查询消息类型-含已关联的渠道类型
     *
     * @param messageTypeQuery 消息类型查询对象 {@link MessageTypePage}
     * @return {@link IPage< MessageTypeVo >}
     */
    IPage<MessageTypeVo> queryPageWithChannelTypes(MessageTypePage messageTypeQuery);
}

