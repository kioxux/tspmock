package cn.com.yusys.yusp.oca.domain.vo;

import lombok.Data;

@Data
public class AdminDataAuthVo {

    private String authId;

    private String contrId;

    private String authTmplId;

    private String authTmplName;

    private String sqlName;

    private String sqlString;

    private String priority;

    private int state = 0;
}
