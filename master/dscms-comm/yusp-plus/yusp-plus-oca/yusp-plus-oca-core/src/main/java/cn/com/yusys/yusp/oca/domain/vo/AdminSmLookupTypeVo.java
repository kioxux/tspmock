package cn.com.yusys.yusp.oca.domain.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.Date;

/**
 * @author danyu
 */
@Data
public class AdminSmLookupTypeVo {

	/**
	 * 记录编号
	 */
	private String lookupTypeId;
	/**
	 * 目录名称
	 */
	private String lookupTypeName;
	/**
	 * 上级目录编号
	 */
	private String upLookupTypeId;
	/**
	 * 上级目录编号名称
	 */
	private String upLookupTypeName;
	/**
	 * 金融机构编号
	 */
	private String instuId;
	/**
	 * 最新变更用户
	 */
	private String lastChgUsr;
	/**
	 * 最新变更时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

}
