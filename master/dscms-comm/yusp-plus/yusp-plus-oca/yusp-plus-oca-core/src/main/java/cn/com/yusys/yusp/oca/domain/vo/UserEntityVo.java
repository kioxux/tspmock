package cn.com.yusys.yusp.oca.domain.vo;


import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @program: yusp-app-framework
 * @description: 用户返回封装
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2020-10-28 17:40
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserEntityVo implements Serializable {

    private String userId;

    private String loginCode;

    private String orgId;
    /**
     * 渠道互斥标识
     */
    private Boolean loginSingleAgent;


//    private String userName;
//
//    private String certType;
//
//    private String certNo;
//
//    private String userCode;
//
//    private String deadline;

//    private String dptId;
//
//    private String userPassword;
//
//    private String userSex;
//
//    private String userBirthday;
//
//    private String userEmail;
//
//    private String userMobilephone;
//
//    private String userOfficetel;
//
//    private String userEducation;
//
//    private String userCertificate;
//
//    private String entrantsDate;
//
//    private String positionTime;
//
//    private String financialJobTime;
//
//    private String positionDegree;
//
//    private String userAvatar;
//
//    private String offenIp;
//
//    private AvailableStateEnum userSts;
//
//    private Date lastLoginTime;
//
//    private Date lastEditPassTime;
//
//    private String lastChgUsr;
//
//    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//    @TableField(fill = FieldFill.INSERT_UPDATE)
//    private Date lastChgDt;
//
//
//    private String fingerPrint;
//
//    private String voicePrint;
//
//    private String facePrint;
//
//    private String gesturePassword;
}
