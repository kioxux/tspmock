package cn.com.yusys.yusp.sequence.service;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.sequence.domain.entity.SequenceConfig;
import cn.com.yusys.yusp.sequence.domain.query.SequenceConfigQuery;
import cn.com.yusys.yusp.sequence.domain.vo.SequenceConfigAddVo;
import cn.com.yusys.yusp.sequence.domain.vo.SequenceConfigUpdateVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 序列号模版配置
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2021-04-22 14:43:47
 */
public interface SequenceConfigService extends IService<SequenceConfig> {

    /**
     * 分页查询序列化模板
     *
     * @param sequenceConfigQuery
     * @return 完成分页的序列化模板
     */
    PageUtils queryPage(SequenceConfigQuery sequenceConfigQuery);

    /**
     * 新增序列化模板
     *
     * @param sequenceConfigAddVo
     */
    void save(SequenceConfigAddVo sequenceConfigAddVo);

    /**
     * 更新序列化模板
     *
     * @param sequenceConfigUpdateVo
     */
    void updateById(SequenceConfigUpdateVo sequenceConfigUpdateVo);

    /**
     * 删除序列化模板
     *
     * @param id
     */
    void removeByIds(String id);

    /**
     * 创建序列化模板
     *
     * @param seqId
     */
    void createSequence(String seqId);

    /**
     * 重置序列化模板
     *
     * @param seqId
     */
    void reCreateSequence(String seqId);

    /**
     * 获得当前序列值序列
     *
     * @param seqId
     */
    String getCurrentSeq(String seqId);

    /**
     * 获得下一序列值
     *
     * @param seqId
     * @return 下一序列值
     */
    String getNextSeq(String seqId);

    /**
     * 获得配置的模版序列
     *
     * @param seqId
     * @return 配置的模版序列
     */
    String getSequenceWithTemplate(String seqId, Map paramsMap);

    /**
     * 获得下一批序列值
     *
     * @param seqId
     * @return下一批序列值
     */
    List<String> getNextSequenceWithCount(String seqId, Integer count);

    /**
     * 获得配置的模版序列
     *
     * @param seqId
     * @param count
     * @param paramsMap
     * @return
     */
    List<String> getSequenceWithTemplateWithCount(String seqId, Integer count, Map<String, String> paramsMap);
}

