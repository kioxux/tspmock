package cn.com.yusys.yusp.oca.domain.entity;

import cn.com.yusys.yusp.oca.config.validation.Insert;
import cn.com.yusys.yusp.oca.config.validation.Update;
import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

/**
 * 系统用户表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-16 15:31:54
 */
@Data
@TableName("admin_sm_user")
public class AdminSmUserEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 记录编号
     */
    @TableId(type = IdType.UUID)
    @NotEmpty(groups = Update.class, message = "userId can not be null or empty!!")
    private String userId;
    /**
     * 账号
     */
    @NotEmpty(groups = Insert.class, message = "loginCode can not be null or empty!!")
    private String loginCode;
    /**
     * 姓名
     */
    @NotEmpty(groups = Insert.class, message = "userName can not be null or empty!!")
    private String userName;
    /**
     * 员工号
     */
    @NotEmpty(groups = Insert.class, message = "userCode can not be null or empty!!")
    private String userCode;
    /**
     * 证件类型
     */
    private String certType;
    /**
     * 证件号码
     */
    private String certNo;

    /**
     * 有效期到
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date deadline;
    /**
     * 所属机构编号
     */
    @NotEmpty(groups = Insert.class, message = "userCode can not be null or empty!!")
    private String orgId;

    /**
     * 所属主机构编号
     */
    private String belgMainOrgId;

    /**
     * 所属部门编号
     */

    private String dptId;
    /**
     * 密码
     */
    @JsonIgnore
    private String userPassword;
    /**
     * 性别
     */
    private String userSex;
    /**
     * 生日
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date userBirthday;
    /**
     * 邮箱
     */
    @Email
    private String userEmail;
    /**
     * 移动电话
     */
    private String userMobilephone;
    /**
     * 办公电话
     */
    private String userOfficetel;
    /**
     * 学历
     */
    private String userEducation;
    /**
     * 资格证书
     */
    private String userCertificate;
    /**
     * 入职日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date entrantsDate;
    /**
     * 任职时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date positionTime;
    /**
     * 从业时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date financialJobTime;
    /**
     * 职级
     */
    private String positionDegree;
    /**
     * 用户头像
     */
    private String userAvatar;
    /**
     * 常用IP，逗号分隔
     */
    @JsonIgnore
    private String offenIp;
    /**
     * 状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效 F：冻结
     */
    private AvailableStateEnum userSts;
    /**
     * 最近登录时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastLoginTime;
    /**
     * 最近一次修改密码时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastEditPassTime;
    /**
     * 最新变更用户
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String lastChgUsr;
    /**
     * 最新变更时间
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

    /**
     * 指纹信息
     */
    private String fingerPrint;
    /**
     * 声纹信息
     */
    private String voicePrint;
    /**
     * 面部信息
     */
    private String facePrint;
    /**
     * 手势密码
     */
    private String gesturePassword;
    /**
     * 是否信贷人员标识
     */
    private String isCredit;
}