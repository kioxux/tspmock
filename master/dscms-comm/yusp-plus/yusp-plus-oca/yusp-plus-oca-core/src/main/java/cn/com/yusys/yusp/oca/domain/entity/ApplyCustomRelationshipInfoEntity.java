package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 申请客户关系表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-12-02 18:03:52
 */
@Data
@TableName("cms_apply_custom_relationship_info")
public class ApplyCustomRelationshipInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 关系表id
	 */
	@TableId
	private String relationshipId;
	/**
	 * 申请流水号
	 */
	private String applyId;
	/**
	 * 客户号
	 */
	private String cusId;
	/**
	 * 申请客户关系类型(00:配偶,01:亲属,02:同事,03:朋友,04:其他)
	 */
	private String relType;
	/**
	 * 关联人姓名
	 */
	private String cusName;
	/**
	 * 关联人联系方式
	 */
	private String relCusPhone;
	/**
	 * 证件类型(10110:居民身份证,10119:组织机构代码,20150:护照,99099:其他证件)
	 */
	private String certType;
	/**
	 * 证件号码
	 */
	private String certCode;
	/**
	 * 录入人
	 */
	private String inputUser;
	/**
	 * 录入时间
	 */
	private Date inputTime;
	/**
	 * 更新时间
	 */
	@TableField(fill = FieldFill.UPDATE)
	private Date updateTime;
	/**
	 * 申请客户显示状态(1:逻辑显示，0:逻辑删除)
	 */
	private String logicStatus;

	private String managerId;

	private String managerName;

	private String orgId;

	private String orgName;

}
