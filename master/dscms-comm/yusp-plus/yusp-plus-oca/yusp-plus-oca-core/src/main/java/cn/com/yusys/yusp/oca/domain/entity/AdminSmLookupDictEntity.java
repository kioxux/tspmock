package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 数据字典内容表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2021-01-15 16:44:33
 */
@Data
@TableName("admin_sm_lookup_dict")
public class AdminSmLookupDictEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 字典项编号，默认uuid
     */
    @TableId
    private String lookupItemId;
    /**
     * 字典类别code码
     */
    private String lookupCode;
    /**
     * 字典类别名称
     */
    private String lookupName;
    /**
     * 字典类别分类标识id
     */
    private String lookupTypeId;
    /**
     * 字典类别分类标识名称
     */
    private String lookupTypeName;
    /**
     * 上级字典内容编号
     */
    private String upLookupItemId;
    /**
     * 字典代码
     */
    private String lookupItemCode;
    /**
     * 字典名称
     */
    private String lookupItemName;
    /**
     * 字典备注说明
     */
    private String lookupItemComment;
    /**
     * 字典项排序
     */
    private Integer lookupItemOrder;
    /**
     * 金融机构编号
     */
    private String instuId;
    /**
     * 最新变更用户
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String lastChgUsr;
    /**
     * 最新变更时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

}
