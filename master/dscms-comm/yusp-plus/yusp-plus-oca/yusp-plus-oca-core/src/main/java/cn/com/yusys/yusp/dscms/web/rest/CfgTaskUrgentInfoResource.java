/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dscms.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.dscms.domain.CfgTaskUrgentInfo;
import cn.com.yusys.yusp.dscms.service.CfgTaskUrgentInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgTaskUrgentInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 刘权
 * @创建时间: 2021-04-22 21:50:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgtaskurgentinfo")
public class CfgTaskUrgentInfoResource {
    @Autowired
    private CfgTaskUrgentInfoService cfgTaskUrgentInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgTaskUrgentInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgTaskUrgentInfo> list = cfgTaskUrgentInfoService.selectAll(queryModel);
        return new ResultDto<List<CfgTaskUrgentInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<CfgTaskUrgentInfo>> index(@RequestBody QueryModel queryModel) {
        List<CfgTaskUrgentInfo> list = cfgTaskUrgentInfoService.selectByModel(queryModel);
        return new ResultDto<List<CfgTaskUrgentInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CfgTaskUrgentInfo> show(@PathVariable("serno") String serno) {
        CfgTaskUrgentInfo cfgTaskUrgentInfo = cfgTaskUrgentInfoService.selectByPrimaryKey(serno);
        return new ResultDto<CfgTaskUrgentInfo>(cfgTaskUrgentInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<CfgTaskUrgentInfo> create(@RequestBody CfgTaskUrgentInfo cfgTaskUrgentInfo) throws Exception {
        if(StringUtils.nonEmpty(cfgTaskUrgentInfo.getSerno())){
            cfgTaskUrgentInfoService.insert(cfgTaskUrgentInfo);
        }
        return new ResultDto<CfgTaskUrgentInfo>(cfgTaskUrgentInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgTaskUrgentInfo cfgTaskUrgentInfo) throws URISyntaxException {
        int result = cfgTaskUrgentInfoService.update(cfgTaskUrgentInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cfgTaskUrgentInfoService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgTaskUrgentInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
