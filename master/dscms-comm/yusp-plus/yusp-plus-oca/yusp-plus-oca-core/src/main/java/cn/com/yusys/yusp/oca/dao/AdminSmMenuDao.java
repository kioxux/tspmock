package cn.com.yusys.yusp.oca.dao;

import cn.com.yusys.yusp.commons.session.user.impl.MenuImpl;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmMenuEntity;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmAuthTreeVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmMenuVo;
import cn.com.yusys.yusp.oca.domain.vo.MenuVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统菜单表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-19 16:32:07
 */

public interface AdminSmMenuDao extends BaseMapper<AdminSmMenuEntity> {

    List<MenuImpl> getadminSmMenuVoList(@Param(Constants.WRAPPER) QueryWrapper queryWrapper);

    MenuVo getMenuInfoByMenuId(String menuId);

    List<MenuVo> queryMenuTreeBySysId(String sysId);

    List<MenuVo> queryMenuTreeResContrBySysId(String sysId);

    /**
     * 关联 admin_sm_auth_reco 创建权限管理树，获取所有菜单数据
     *
     * @param id
     * @return
     */
    List<AdminSmAuthTreeVo> selectAuthTree(String id);

    int updateBatchMenuTree(@Param("list") List<AdminSmMenuEntity> list);

    /**
     * 控制点页面需求的菜单列表
     * @param wrapper
     * @param lastMenuId
     * @return
     */
    Page<AdminSmMenuVo> getMenuListForContr(
            Page<AdminSmMenuVo> page,
            @Param(Constants.WRAPPER) QueryWrapper<AdminSmMenuVo> wrapper,
            @Param("lastMenuId") String lastMenuId);
}
