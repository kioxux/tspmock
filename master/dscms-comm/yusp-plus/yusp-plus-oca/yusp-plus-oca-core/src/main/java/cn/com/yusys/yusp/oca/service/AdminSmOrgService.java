package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.UserAndOrgInfoReqDto;
import cn.com.yusys.yusp.dto.UserAndOrgInfoRespDto;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmOrgTreeNodeBo;
import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmOrgEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmOrgExtQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmOrgTreeQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmOrgDetailVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmOrgVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Set;

/**
 * 系统机构表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-11-27 18:06:35
 */
public interface AdminSmOrgService extends IService<AdminSmOrgEntity> {

    Page<AdminSmOrgVo> queryPage(AdminSmOrgExtQuery query);

    AdminSmOrgDetailVo getDetailById(String orgId);

    List<AdminSmOrgVo> getAllProgeny(String orgId);

    List<AdminSmOrgVo> getAllProgeny(AdminSmOrgExtQuery query);

    List<AdminSmOrgTreeNodeBo> getOrgTreeByOrgId(String orgId);

    List<AdminSmOrgTreeNodeBo> getOrgTree(AdminSmOrgTreeQuery query);

    List<AdminSmOrgTreeNodeBo> getOrgTrees(List<String> orgIds, AvailableStateEnum orgSts, String... exceptOrgIds);

    List<String> getAllAccessibleOrgIds(String userId);

    List<String> getAllAccessibleOrgIds();

    boolean save(AdminSmOrgEntity entity);

    void batchDisable(String[] ids);

    void batchDelete(String[] ids);

    void batchEnable(String[] ids);

    boolean updateBy(AdminSmOrgEntity entity);

    String getOrgSeq(String orgId);

    Set<AdminSmOrgTreeNodeBo> getAllAncestryOrgs(String orgId);

    AdminSmOrgEntity getParentOrg(String orgId);

    Set<AdminSmOrgEntity> getSiblingOrgs(String orgId);

    Page<AdminSmOrgVo> getOrgsForWf(AdminSmOrgExtQuery query);

    List<String> getLowerOrgId(String orgCode);

    /**
     * 根据机构代码获取机构信息
     *
     * @param orgCode 机构代码
     * @return 机构信息
     */
    AdminSmOrgDto getByOrgCode(String orgCode);

    /**
     * 根据多个机构代码获取多条机构信息
     *
     * @param orgCodes 机构代码列表
     * @return 机构信息列表
     */
    List<AdminSmOrgDto> getByOrgCodeList(List<String> orgCodes);

    /**
     * 根据机构索引获取机构下级机构的orgCode集合（包含当前机构）
     *
     * @return
     */
    List<String> getOrgCodesByOrgSeq(String orgSeq);

    /**
     * 根据机构编号查询当前机构及下属机构信息
     * add by 顾银华 at 2021-09-21 17:07:45
     * @return
     */
    List<AdminSmOrgDto> getChildrenOrgList(String orgCode);

    /**
     * 根据类别取出异地分行(异地支行（有分行）  异地支行（无分行）),除村镇外为本地机构
     * @param queryType 0-本地机构；1-异地机构
     * @return
     */
    List<String> getRemoteOrgList(String queryType);

    /**
     * 根据机构编号列表分页查询用户和机构信息
     *
     * @param userAndOrgInfoReqDto
     * @return
     */
    List<UserAndOrgInfoRespDto> getUserAndOrgInfo(UserAndOrgInfoReqDto userAndOrgInfoReqDto);

    /**
     * 根据机构编号及下级机构号分页查询用户和机构信息
     *
     * @param queryModel
     * @return
     */
    List<UserAndOrgInfoRespDto> getUserByLowerOrgInfo(QueryModel queryModel);

    /**
     * @创建人 WH
     * @创建时间 2021/6/11 16:21
     * @注释
     */
    ResultDto querypagebyall(QueryModel queryModel);

    /**
     * @创建人 jijian
     * @创建时间 2021/9/23 16:21
     * @注释
     */
    ResultDto querypagebyallForDh(QueryModel queryModel);
}

