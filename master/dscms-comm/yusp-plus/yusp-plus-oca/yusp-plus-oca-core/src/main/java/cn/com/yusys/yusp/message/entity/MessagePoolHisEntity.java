package cn.com.yusys.yusp.message.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 消息历史
 *
 * @author xiaodg@yusys.com.cn
 */
@Data
@TableName("message_pool_his")
public class MessagePoolHisEntity implements Serializable {

    private static final long serialVersionUID = -7279414599655477808L;
    /**
     * 主键
     */
    @TableId
    private String pkNo;
    /**
     * 事件唯一编号
     */
    private String eventNo;
    /**
     * 适用渠道类型
     */
    private String channelType;
    /**
     * 用户码
     */
    private String userNo;
    /**
     * 用户接收地址或账号
     */
    private String userAddr;
    /**
     * 用户类型[1:客户经理][2:借款人]
     */
    private String userType;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 发送完成时间
     */
    private String sendTime;
    /**
     * 消息等级[小先发]
     */
    private String messageLevel;
    /**
     * 发送状态
     */
    private String state;
    /**
     * 固定发送开始时间
     */
    private String timeStart;
    /**
     * 任务id
     */
    private Integer pkHash;
    /**
     * 消息类型
     */
    private String messageType;
    /**
     * 固定发送结束时间
     */
    private String timeEnd;

}
