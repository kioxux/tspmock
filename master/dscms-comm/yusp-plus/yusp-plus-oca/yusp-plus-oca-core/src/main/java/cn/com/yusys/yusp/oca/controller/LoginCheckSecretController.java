package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.config.annotation.LoginBeforeStrategy;
import cn.com.yusys.yusp.oca.config.annotation.LoginSuccessStrategy;
import cn.com.yusys.yusp.oca.domain.vo.LoginAppClientDto;
import cn.com.yusys.yusp.oca.domain.vo.LoginUserDto;
import cn.com.yusys.yusp.oca.domain.vo.LoginUserMobileVo;
import cn.com.yusys.yusp.oca.domain.vo.UserEntityVo;
import cn.com.yusys.yusp.oca.service.impl.LoginCheckSecretServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * yusp-app-framework
 *
 * @author wujiangpeng
 * @description 用户登录验密
 * @email wujp4@yusys.com.cn
 * @since 2020-10-28 17:10
 */
@Api("用户登陆")
@RestController
@RequestMapping("/api/login")
public class LoginCheckSecretController {

    @Autowired
    LoginCheckSecretServiceImpl loginCheckSecretService;

    /**
     * 根据用户名密码验密
     */
    @ApiOperation(value = "验证用户密码并查询用户信息", notes = "验证用户密码并查询用户信息")
    @PostMapping("/queryuserandchecksecret")
    @LoginBeforeStrategy
    @LoginSuccessStrategy
    public ResultDto<UserEntityVo> queryUserAndCheckSecret(@Valid @RequestBody LoginUserDto loginUserDto) {
        ResultDto<UserEntityVo> result = loginCheckSecretService.queryUserAndCheckSecret(loginUserDto.getLoginCode(), loginUserDto.getPassword());
        return result;
    }

    /**
     * 根据渠道码和用户及机构信息判断是否为存在的用户
     *
     * @param loginAppClientDto
     * @return
     */
    @PostMapping("/checkappclientuserexist")
    public ResultDto<UserEntityVo> checkAppClientUserExist(@Validated @RequestBody LoginAppClientDto loginAppClientDto) {
        ResultDto<UserEntityVo> result = loginCheckSecretService.checkAppClientUserExist(loginAppClientDto.getServtp(), loginAppClientDto.getLoginCode(), loginAppClientDto.getOrgCode());
        return result;
    }

    @PostMapping("/checkUserMobile")
    public ResultDto<UserEntityVo> checkUserMobile(@Validated @RequestBody LoginUserMobileVo loginUserMobileVo) {
        ResultDto<UserEntityVo> resultDto = loginCheckSecretService.checkUserMobile(loginUserMobileVo.getLoginCode(), loginUserMobileVo.getMobile());
        return resultDto;
    }
}
