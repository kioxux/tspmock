package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.commons.exception.PlatformException;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.oca.dao.AdminSmMessageDao;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmMessageEditBo;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmMessageSaveBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmMessageEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmMessageQuery;
import cn.com.yusys.yusp.oca.service.AdminSmMessageService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;


@Slf4j
@Service("adminSmMessageService")
public class AdminSmMessageServiceImpl extends ServiceImpl<AdminSmMessageDao, AdminSmMessageEntity> implements AdminSmMessageService {

    @Override
    public Page<AdminSmMessageEntity> queryMessagePage(AdminSmMessageQuery adminSmMessageQuery) {
        //获取分页参数
        Page<AdminSmMessageEntity> page = adminSmMessageQuery.getIPage();
        //创建查询wrapper
        LambdaQueryWrapper<AdminSmMessageEntity> wrapper = Wrappers.lambdaQuery();
        //添加查询条件
        Optional.ofNullable(adminSmMessageQuery.getKeyWord()).ifPresent((param) -> wrapper.like(AdminSmMessageEntity::getCode, param).or().like(AdminSmMessageEntity::getMessage, param));
        Optional.ofNullable(adminSmMessageQuery.getCode()).ifPresent((param) -> wrapper.like(AdminSmMessageEntity::getCode, param));
        Optional.ofNullable(adminSmMessageQuery.getMessage()).ifPresent((param) -> wrapper.like(AdminSmMessageEntity::getMessage, param));
        wrapper.orderByDesc(AdminSmMessageEntity::getLastChgDt);
        //分页查询
        return this.baseMapper.selectPage(page, wrapper);
    }

    @Override
    public void saveMessage(AdminSmMessageSaveBo adminSmMessage) {
        //校验新增系统消息提示码是否已经存在
        List<AdminSmMessageEntity> adminSmMessageEntities = this.list(new QueryWrapper<AdminSmMessageEntity>().eq("CODE", adminSmMessage.getCode()));
        if (CollectionUtils.nonEmpty(adminSmMessageEntities)) {
            throw new PlatformException("系统提示码已存在");
        }
        //创建新增对象
        AdminSmMessageEntity adminSmMessageEntity = new AdminSmMessageEntity();
        //新增对象添加数据
        cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(adminSmMessage, adminSmMessageEntity);
        adminSmMessageEntity.setLastChgUsr(SessionUtils.getUserId());
        adminSmMessageEntity.setLastChgDt(new Date());
        //新增
        this.save(adminSmMessageEntity);
    }

    @Override
    public void updateMessageById(AdminSmMessageEditBo adminSmMessage) {
        //校验新增系统消息提示码是否已经存在
        List<AdminSmMessageEntity> adminSmMessageEntities = this.list(new QueryWrapper<AdminSmMessageEntity>().eq("CODE", adminSmMessage.getCode()).ne("MESSAGE_ID", adminSmMessage.getMessageId()));
        if (CollectionUtils.nonEmpty(adminSmMessageEntities)) {
            throw new PlatformException("系统提示码已存在");
        }
        //创建修改对象
        AdminSmMessageEntity adminSmMessageEntity = new AdminSmMessageEntity();
        //修改对象添加数据
        BeanUtils.beanCopy(adminSmMessage, adminSmMessageEntity);
        adminSmMessageEntity.setLastChgUsr(SessionUtils.getUserId());
        adminSmMessageEntity.setLastChgDt(new Date());
        //修改对象
        this.updateById(adminSmMessageEntity);
    }
}