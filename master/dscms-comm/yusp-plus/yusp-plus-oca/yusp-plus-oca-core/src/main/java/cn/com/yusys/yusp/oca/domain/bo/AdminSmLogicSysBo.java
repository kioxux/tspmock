package cn.com.yusys.yusp.oca.domain.bo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 系统逻辑 前端传入Bo
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-19 14:30:22
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminSmLogicSysBo {

    /**
     * 记录编号
     */
    private String sysId;
    /**
     * 认证类型
     */
    private String authId;
    /**
     * 版本号
     */
    private String sysVersion;
    /**
     * 逻辑系统名称
     */
    private String sysName;
    /**
     * 逻辑系统描述
     */
    private String sysDesc;
    /**
     * 逻辑系统状态
     */
    private String sysSts;
    /**
     * 是否单点登录
     */
    private String isSso;
    /**
     * 系统简称
     */
    private String sysCode;
    /**
     * 国际化key值
     */
    private String i18nKey;

    private String userId;

    private String funcId;

    private String roleId;

}
