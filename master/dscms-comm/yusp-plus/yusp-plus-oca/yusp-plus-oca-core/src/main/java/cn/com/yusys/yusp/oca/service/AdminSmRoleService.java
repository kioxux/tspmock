package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.GetUserInfoByRoleCodeDto;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmRoleEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmPasteRoleQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmRoleQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmUserQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmRoleDetailVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmRoleVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmUserVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 系统角色表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-11-18 18:06:35
 */
public interface AdminSmRoleService extends IService<AdminSmRoleEntity> {

    Page<AdminSmRoleVo> queryPage(AdminSmRoleQuery query);

    AdminSmRoleDetailVo getDetailById(String roleId);

    void batchDisable(String[] ids);

    void batchEnable(String[] ids);

    void batchDelete(String[] ids);

    Page<AdminSmRoleVo> queryPageExcept(AdminSmPasteRoleQuery query);

    /**
     * 工作流获取角色信息
     *
     * @param query
     * @return
     */
    Page<AdminSmRoleVo> getRolesForWf(AdminSmRoleQuery query);

    List<AdminSmRoleVo> getUserRoleByLoginCode(String loginCode);

    /**
     * 根据角色编号分页查询用户信息
     *
     * @param getUserInfoByRoleCodeDto
     * @return
     */
    List<AdminSmUserDto> getUserInfoByRoleCode(GetUserInfoByRoleCodeDto getUserInfoByRoleCodeDto);

    /**
     * 根据角色编号不分页查询用户信息
     *
     * @param roleCode
     * @return
     */
    List<AdminSmUserDto> getUserInfoByRoleCode(String roleCode);

    /**
     * 根据机构角色编号分页查询用户信息
     *
     */
    Page<AdminSmUserVo> getUserInfoByOrgRoleCode(AdminSmUserQuery query);
}