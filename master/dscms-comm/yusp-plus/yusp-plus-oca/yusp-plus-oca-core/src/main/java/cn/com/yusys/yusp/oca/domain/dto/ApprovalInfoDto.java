package cn.com.yusys.yusp.oca.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 *@program: yusp-plus
 *@description: 模板Vo
 *@author: wujiangpeng
 *@email: wujp4@yusys.com.cn
 *@create: 2020-12-07 20:11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApprovalInfoDto {
    @NotEmpty(message="产品编号不能为空")
    private String productId;
    @NotEmpty(message="模板内容不能为空")
    @JsonProperty("content")
    private String templateInfo;
}
