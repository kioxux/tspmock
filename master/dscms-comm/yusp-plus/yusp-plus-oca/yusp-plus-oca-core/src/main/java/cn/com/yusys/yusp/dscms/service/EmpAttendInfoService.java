/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dscms.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.dscms.domain.EmpAttendInfo;
import cn.com.yusys.yusp.dscms.domain.EmpCheckInfo;
import cn.com.yusys.yusp.dscms.repository.mapper.EmpAttendInfoMapper;
import cn.com.yusys.yusp.dscms.repository.mapper.EmpCheckInfoMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: EmpAttendInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: kioxu
 * @创建时间: 2021-05-10 15:30:32
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class EmpAttendInfoService {

    @Autowired
    private EmpAttendInfoMapper empAttendInfoMapper;


    @Autowired
    EmpCheckInfoMapper empCheckInfoMapper;

    private final Logger log = LoggerFactory.getLogger(EmpAttendInfoService.class);

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public EmpAttendInfo selectByPrimaryKey(String serno) {
        return empAttendInfoMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<EmpAttendInfo> selectAll(QueryModel model) {
        List<EmpAttendInfo> records = (List<EmpAttendInfo>) empAttendInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<EmpAttendInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<EmpAttendInfo> list = empAttendInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(EmpAttendInfo record) {
        //存在在途申请 避免重复添加
        String userCode = record.getUserCode();
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("userCode",userCode);
        queryModel.addCondition("default","1");//在途数据
        //请假申请不能有000,111,992待发起、审批中、退回；
        List<EmpAttendInfo> list = empAttendInfoMapper.selectByModel(queryModel);
        if(list.size() > 0 ){
            throw BizException.error(null, "999999","\"已存在在途请假申请，请勿重复申请\"" + "保存失败！");
        }
        // 如有审批通过的记录，时间不能交叉
        QueryModel queryModelPass = new QueryModel();
        queryModelPass.addCondition("userCode",userCode);
        queryModelPass.addCondition("apprStatus","997");//
        queryModelPass.setSort("endDate desc");
        List<EmpAttendInfo> listPass = empAttendInfoMapper.selectByModel(queryModelPass);
        if(listPass != null && listPass.size() > 0 ){
            EmpAttendInfo empAttendInfo = listPass.get(0);
            String endDate = empAttendInfo .getEndDate();//审批通过的请假最大结束时间
            //开始时间必须大于审批通过的请假最大结束时间
            if(record.getStartDate().compareTo(endDate)<= 0){
                throw BizException.error(null, "999999","\"所选日期区间存在审核通过假期申请，无法重复请假\"" + "保存失败！");
            }

        }
        record.setIsCancelVacation("0");
        return empAttendInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(EmpAttendInfo record) {

        return empAttendInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(EmpAttendInfo record) {
        if(record!= null && "444".equals(record.getApprStatus())){
            return empAttendInfoMapper.updateByPrimaryKey(record);
        }else{
            //存在在途申请 避免重复添加
            String userCode = record.getUserCode();
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("userCode",userCode);
            queryModel.addCondition("default","1");//在途数据
            //请假申请不能有000,111,992待发起、审批中、退回；
            List<EmpAttendInfo> list = empAttendInfoMapper.selectByModel(queryModel);
            if(list.size() > 1 ){
                throw BizException.error(null, "999999","\"已存在在途请假申请，请勿重复申请\"" + "保存失败！");
            }
            // 如有审批通过的记录，时间不能交叉
            QueryModel queryModelPass = new QueryModel();
            queryModelPass.addCondition("userCode",userCode);
            queryModelPass.addCondition("apprStatus","997");//
            queryModelPass.setSort("endDate desc");
            List<EmpAttendInfo> listPass = empAttendInfoMapper.selectByModel(queryModelPass);
            if(listPass != null && listPass.size() > 0 ){
                EmpAttendInfo empAttendInfo = listPass.get(0);
                String endDate = empAttendInfo .getEndDate();//审批通过的请假最大结束时间
                //开始时间必须大于审批通过的请假最大结束时间
                if(record.getStartDate().compareTo(endDate) <= 0){
                    throw BizException.error(null, "999999","\"所选日期区间存在审核通过假期申请，无法重复请假\"" + "保存失败！");
                }
            }
            return empAttendInfoMapper.updateByPrimaryKey(record);
        }
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(EmpAttendInfo record) {
        return empAttendInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return empAttendInfoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return empAttendInfoMapper.deleteByIds(ids);
    }

    /**
     * 流程发起进行的业务处理
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【1-审批中】
     * 1、更新申请主表的审批状态-111
     *
     * @param serno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDZDataAfterStart(String serno) {
        try {
            if (StringUtils.isBlank(serno)) {
                throw new YuspException("IQPWH0001", "业务申请流程处理异常-参数缺失！");
            }

            log.info("流程发起-获取请假申请" + serno + "申请主表信息");
            EmpAttendInfo empAttendInfo = empAttendInfoMapper.selectByPrimaryKey(serno);
            if (empAttendInfo == null) {
                throw new YuspException("IQPWH000003", "业务申请流程处理异常-申请主表不存在！");
            }

            int updateCount = 0;

            log.info("流程发起-更新请假申请" + serno + "流程审批状态为【111】-审批中");
            empAttendInfo.setApprStatus(CmisFlowConstants.WF_STATUS_111);
            updateCount = empAttendInfoMapper.updateByPrimaryKey(empAttendInfo);

            if (updateCount < 0) {
                throw new YuspException("IQPWH000007", "业务申请流程处理异常-更新业务主表申请状态异常！");
            }


        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("请假申请流程发起业务处理发生异常！", e);
            throw new YuspException("IQPWH000008", "业务申请流程处理异常！");
        }
    }

    /**
     * 校验用户的签到签退状况，请假销假状况
     * @param userCode
     * @return
     */
    public String usercheck(String userCode){
        String curentDate = DateUtils.getCurrDateStr();
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("userCode", userCode);
        queryModel.addCondition("optDate",curentDate);
        queryModel.setSort("opt_time desc");
        // 3、请假判断
        List<EmpAttendInfo> attendList = empAttendInfoMapper.selectAttendQj(queryModel);
        if(CollectionUtils.nonEmpty(attendList)){
            return "用户已请假";
        }

        // 1、签到签退状态
        //[{"key":"01","value":"签到"},{"key":"02","value":"签退"},{"key":"03","value":"提前签退"}]
        List<EmpCheckInfo> checklist = empCheckInfoMapper.selectByModel(queryModel);
        if(CollectionUtils.isEmpty(checklist)){
            return "用户未签到";
        }
        // 2、取第一条数据，最近一条数据标识了其当前状态
        EmpCheckInfo empCheckInfo = checklist.get(0);
        if("02".equals(empCheckInfo.getCheckOptType())){
            return "用户已签退";
        }else if("03".equals(empCheckInfo.getCheckOptType()) && ("111".equals(empCheckInfo.getApproveStatus()) || "997".equals(empCheckInfo.getApproveStatus()))){
            return "用户申请了提前签退";
        }


        return "";
    }

}
