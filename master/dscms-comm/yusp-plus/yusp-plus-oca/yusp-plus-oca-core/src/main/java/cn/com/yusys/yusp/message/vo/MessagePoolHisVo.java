package cn.com.yusys.yusp.message.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 消息历史
 *
 * @author xiaodg@yusys.com.cn
 **/
@Data
public class MessagePoolHisVo implements Serializable {
    private static final long serialVersionUID = -7292771087740655314L;
    /**
     * 主键
     */
    @TableId
    private String pkNo;
    /**
     * 事件唯一编号
     */
    private String eventNo;
    /**
     * 适用渠道类型
     */
    private String channelType;
    /**
     * 用户码
     */
    private String userNo;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 发送完成时间
     */
    private String sendTime;
    /**
     * 消息等级[小先发]
     */
    private String messageLevel;
    /**
     * 发送状态
     */
    private String state;
    /**
     * 固定发送开始时间
     */
    private String timeStart;
    /**
     * 任务id
     */
    private BigDecimal pkHash;
    /**
     * 消息类型
     */
    private String messageType;
    /**
     * 固定发送结束时间
     */
    private String timeEnd;
}
