package cn.com.yusys.yusp.oca.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 *@program: yusp-plus
 *@description: 密码校验Bo
 *@author: wujiangpeng
 *@email: wujp4@yusys.com.cn
 *@create: 2020-12-14 11:30
 */
@ApiModel(description = "用户密码验密接收类", value = "用户密码验密接收类")
@Data
public class PasswordCheckBo {

    @ApiModelProperty(name = "passwordType", value = "密码类型", required = false, example = "test")
    private String passwordType;

    @ApiModelProperty(name = "pwd", value = "用户密码", required = true, example = "test")
    @NotEmpty(message = "密码不能为空")
    private String pwd;

    @ApiModelProperty(name = "userId", value = "用户名", required = false, example = "test")
    private String userId;
}
