package cn.com.yusys.yusp.notice.service.impl;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.common.utils.Query;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.notice.constant.NoticeConstant;
import cn.com.yusys.yusp.notice.dao.AdminSmNoticeReciveDao;
import cn.com.yusys.yusp.notice.entity.AdminSmNoticeReciveEntity;
import cn.com.yusys.yusp.notice.form.NoticeConditionForm;
import cn.com.yusys.yusp.notice.form.NoticeRoleForm;
import cn.com.yusys.yusp.notice.service.AdminSmNoticeReciveService;
import cn.com.yusys.yusp.notice.vo.AdminSmNoticeReciveVo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmOrgEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmRoleEntity;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("adminNoticeReciveService")
public class AdminSmNoticeReciveServiceImpl extends ServiceImpl<AdminSmNoticeReciveDao, AdminSmNoticeReciveEntity> implements AdminSmNoticeReciveService {

    /**
     * PS: 扩展支持直接发送给指定用户
     * @param roleIdList
     * @param orgIdList
     * @return
     */
    @Override
    public List<AdminSmNoticeReciveEntity> findListByCondition(List<String> roleIdList, List<String> orgIdList) {
        roleIdList.add(NoticeConstant.RECIVE_OGJ_ID_NA);
        orgIdList.add(NoticeConstant.RECIVE_OGJ_ID_NA);
        QueryWrapper<AdminSmNoticeReciveEntity> query = Wrappers.<AdminSmNoticeReciveEntity>query();
        query = query.select("DISTINCT NOTICE_ID")
                .eq("a.RECIVE_TYPE", NoticeConstant.RECIVE_TYPE_ROLE)
                .in("a.RECIVE_OGJ_ID", roleIdList)
                .in("b.RECIVE_OGJ_ID", orgIdList)
                .eq("b.RECIVE_TYPE", NoticeConstant.RECIVE_TYPE_ORG)
                .or(wq -> wq.eq("c.RECIVE_OGJ_ID", SessionUtils.getUserId())
                        .eq("c.RECIVE_TYPE", NoticeConstant.RECIVE_TYPE_USER));
        List<AdminSmNoticeReciveEntity> entityList = baseMapper.selectByCondition(query);
        return entityList;
    }

    @Override
    public List<AdminSmRoleEntity> selectRoles(String noticeId) {
        QueryWrapper<AdminSmRoleEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("re.RECIVE_TYPE", NoticeConstant.RECIVE_TYPE_ROLE);
        queryWrapper.eq("re.NOTICE_ID", noticeId);
        List<AdminSmRoleEntity> roleEntities = baseMapper.selectRoles(queryWrapper);
        return roleEntities;
    }

    @Override
    public List<AdminSmOrgEntity> selectOrgs(String noticeId) {
        QueryWrapper<AdminSmOrgEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("re.RECIVE_TYPE", NoticeConstant.RECIVE_TYPE_ORG);
        queryWrapper.eq("re.NOTICE_ID", noticeId);
        List<AdminSmOrgEntity> roleEntities = baseMapper.selectOrgs(queryWrapper);
        return roleEntities;
    }

    /**
     * 查询公告权限的机构信息和角色信息
     * @param noticeId
     * @return
     */
    @Override
    public List<AdminSmNoticeReciveVo> selectReciveIdAndName(String noticeId) {
        return this.baseMapper.selectReciveIdAndName(noticeId);
    }

    /**
     * 新增公告：保存公告权限
     * @param reciveEntity
     * @param type
     */
    @Override
    public void saveByAdminSmNoticeAllEntity(AdminSmNoticeReciveEntity reciveEntity, String type) {
        /**
         * 按照部门 id 和角色 id 分配权限
         */
        List<AdminSmNoticeReciveEntity> entityList = getAnnouncementEntity(reciveEntity);
        if (NoticeConstant.RECIVE_UPDATE.equals(type)) {
            /**
             * 修改公告、删除公告
             */
            List<String> noticeIds = entityList.stream()
                    .map(AdminSmNoticeReciveEntity::getNoticeId)
                    .collect(Collectors.toList());
            this.deleteRecive(noticeIds);
        }
        this.saveBatch(entityList);
    }

    @Override
    public void deleteRecive(List<String> noticeIds) {
        QueryWrapper<AdminSmNoticeReciveEntity> wrapper = new QueryWrapper<>();
        wrapper.in("NOTICE_ID", noticeIds);
        this.remove(wrapper);
    }

    /**
     * 该方法对公告访问权限进行拆分
     * @param entity
     * @return
     */
    private List<AdminSmNoticeReciveEntity> getAnnouncementEntity(AdminSmNoticeReciveEntity entity) {
        List<AdminSmNoticeReciveEntity> entityList = new ArrayList<>();
        String noticeId = entity.getNoticeId();
        String reciveOgjId = entity.getReciveOgjId();
        String reciveRoleId = entity.getReciveRoleId();
        /**
         * 两种权限为空，存为NA，所有人都能访问
         */
        if (StringUtils.isEmpty(reciveOgjId) && StringUtils.isEmpty(reciveRoleId)) {
            entityList.add(new AdminSmNoticeReciveEntity(
                    StringUtils.getUUID(),
                    noticeId,
                    NoticeConstant.RECIVE_TYPE_ROLE,
                    NoticeConstant.RECIVE_OGJ_ID_NA));
            entityList.add(new AdminSmNoticeReciveEntity(
                    StringUtils.getUUID(),
                    noticeId,
                    NoticeConstant.RECIVE_TYPE_ORG,
                    NoticeConstant.RECIVE_OGJ_ID_NA));
            return entityList;
        }
        /**
         * 拆分角色访问权限
         */
        if (StringUtils.nonEmpty(reciveRoleId)) {
            if (reciveRoleId.contains(",")) {
                // 17、分解roleIds
                String[] roleIds = reciveRoleId.split(",");
                for (int i = 0; i < roleIds.length; i++) {
                    entityList.add(new AdminSmNoticeReciveEntity(
                            StringUtils.getUUID(),
                            noticeId,
                            NoticeConstant.RECIVE_TYPE_ROLE,
                            roleIds[i]));
                }
            } else {
                entityList.add(new AdminSmNoticeReciveEntity(
                        StringUtils.getUUID(),
                        noticeId,
                        NoticeConstant.RECIVE_TYPE_ROLE,
                        reciveRoleId));
            }
        }
        /**
         * 拆分部门访问权限
         */
        if (StringUtils.nonBlank(reciveOgjId)) {
            if (reciveOgjId.contains(",")) {
                String[] orgIds = reciveOgjId.split(",");
                for (int i = 0; i < orgIds.length; i++) {
                    entityList.add(new AdminSmNoticeReciveEntity(
                            StringUtils.getUUID(),
                            noticeId,
                            NoticeConstant.RECIVE_TYPE_ORG,
                            orgIds[i]));
                }
            } else {
                entityList.add(new AdminSmNoticeReciveEntity(
                        StringUtils.getUUID(),
                        noticeId,
                        NoticeConstant.RECIVE_TYPE_ORG,
                        reciveOgjId));
            }
        }
        return entityList;
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AdminSmNoticeReciveEntity> page = this.page(
                new Query<AdminSmNoticeReciveEntity>().getPage(params),
                new QueryWrapper<AdminSmNoticeReciveEntity>()
        );

        return new PageUtils(page);
    }
}