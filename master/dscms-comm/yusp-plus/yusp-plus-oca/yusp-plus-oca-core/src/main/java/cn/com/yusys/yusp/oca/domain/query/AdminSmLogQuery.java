package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.common.query.PageQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmLogVo;
import lombok.Data;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description: 日志的查询条件
 * @author lty
 * @date 2021/2/20　　
 */
@Data
public class AdminSmLogQuery extends PageQuery<AdminSmLogVo> {

    private String logTypeId;

    private String userName;

    private String operObjId;

    private String beginTime;

    private String endTime;
}
