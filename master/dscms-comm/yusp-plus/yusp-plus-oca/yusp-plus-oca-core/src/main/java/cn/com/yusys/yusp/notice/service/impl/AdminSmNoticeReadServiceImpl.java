package cn.com.yusys.yusp.notice.service.impl;

import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.notice.dao.AdminSmNoticeReadDao;
import cn.com.yusys.yusp.notice.entity.AdminSmNoticeReadEntity;
import cn.com.yusys.yusp.notice.service.AdminSmNoticeReadService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Service("adminSmNoticeReadService")
public class AdminSmNoticeReadServiceImpl extends ServiceImpl<AdminSmNoticeReadDao, AdminSmNoticeReadEntity> implements AdminSmNoticeReadService {

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void recordRead(List<String> noticeIds) {
        if (noticeIds != null && noticeIds.size() > 0) {
            /**
             * 已阅的公告，不能再次阅读
             */
            String userId = SessionUtils.getUserId();
            List<AdminSmNoticeReadEntity> alreadyEntityList = getByNoticeIds(noticeIds, userId);
            List<String> alreadyNoticeIds = alreadyEntityList.stream().map(AdminSmNoticeReadEntity::getNoticeId).collect(Collectors.toList());
            Set<AdminSmNoticeReadEntity> readEntitySet = new LinkedHashSet<>();
            for (int i = 0; i < noticeIds.size(); i++) {
                if (!alreadyNoticeIds.contains(noticeIds.get(i))) {
                    AdminSmNoticeReadEntity entity = new AdminSmNoticeReadEntity();
                    entity.setReadId(StringUtils.getUUID());
                    entity.setNoticeId(noticeIds.get(i));
                    entity.setUserId(SessionUtils.getUserId());
                    entity.setReadTime(DateUtils.formatDateTimeByDef());
                    readEntitySet.add(entity);
                }
            }
            if (readEntitySet.size() > 0) {
                this.saveBatch(readEntitySet);
            }
        }
    }

    /**
     * 使用noticeId和阅读用户id，查询阅读记录
     * @param noticeIds
     * @return
     */
    public List<AdminSmNoticeReadEntity> getByNoticeIds(List<String> noticeIds, String userId) {
        QueryWrapper<AdminSmNoticeReadEntity> wrapper = new QueryWrapper<>();
        wrapper.in("notice_id", noticeIds);
        wrapper.eq("user_id", userId);
        return list(wrapper);
    }
}