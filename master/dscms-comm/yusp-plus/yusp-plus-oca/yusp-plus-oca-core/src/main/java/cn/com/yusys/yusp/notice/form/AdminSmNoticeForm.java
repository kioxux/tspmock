package cn.com.yusys.yusp.notice.form;

import cn.com.yusys.yusp.notice.entity.AdminSmRicheditFileInfoEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AdminSmNoticeForm implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 公告编号
     */
    private String noticeId;
    /**
     * 公告标题
     */
    private String noticeTitle;
    /**
     * 公告重要程度
     */
    private String noticeLevel;
    /**
     * 有效期至
     */
    private String activeDate;
    /**
     * 是否置顶
     */
    private String isTop;
    /**
     * 置顶有效期
     */
    private String topActiveDate;
    /**
     * 富文本表关联 id
     */
    private String richeditId;

    private String noticeContent;
    /**
     * 发布状态（状态：对应字典项=NORM_STS C：未发布O：已发布）
     */
    private String pubSts;
    /**
     * 发布时间
     */
    private String pubTime;
    /**
     * 公告发布人编号
     */
    private String pubUserId;
    /**
     * 公告发布人姓名
     */
    private String pubUserName;
    /**
     * 发布机构编号
     */
    private String pubOrgId;
    /**
     * 发布机构名称
     */
    private String pubOrgName;
    /**
     * 创建人编号
     */
    private String creatorId;
    /**
     * 创建人姓名
     */
    private String creatorName;
    /**
     * 创建时间
     */
    private String creatorTime;
    /**
     * 富文本内容
     */
    private String context;
    /**
     * 公告权限s
     */
    private String reciveOrgId;
    private String reciveRoleId;
    /**
     * 公告附件
     */
    private List<AdminSmRicheditFileInfoForm> fileInfoFormList;
    /**
     * 分页查询条件
     */
    private int page;
    private int size;
}
