package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmMessageEditBo;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmMessageSaveBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmMessageEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmMessageQuery;
import cn.com.yusys.yusp.oca.service.AdminSmMessageService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Arrays;


/**
 * 提示信息管理表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-16 11:19:42
 */
@RestController
@RequestMapping("/api/adminsmmessage")
public class AdminSmMessageController {
    @Autowired
    private AdminSmMessageService adminSmMessageService;

    /**
     * 查询信息列表
     *
     * @return
     */
    @GetMapping("/page")
    public ResultDto queryMessagePage(AdminSmMessageQuery adminSmMessageQuery) {
        Page<AdminSmMessageEntity> page = adminSmMessageService.queryMessagePage(adminSmMessageQuery);
        return ResultDto.success(page);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public ResultDto save(@Valid @RequestBody AdminSmMessageSaveBo adminSmMessage) {
        adminSmMessageService.saveMessage(adminSmMessage);
        return ResultDto.success();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public ResultDto update(@Valid @RequestBody AdminSmMessageEditBo adminSmMessage) {
        adminSmMessageService.updateMessageById(adminSmMessage);
        return ResultDto.success();
    }

    /**
     * 批量删除系统提示消息
     */
    @PostMapping("/delete")
    public ResultDto delete(@RequestBody @NotNull String[] ids) {
        adminSmMessageService.removeByIds(Arrays.asList(ids));
        return ResultDto.success();
    }
}
