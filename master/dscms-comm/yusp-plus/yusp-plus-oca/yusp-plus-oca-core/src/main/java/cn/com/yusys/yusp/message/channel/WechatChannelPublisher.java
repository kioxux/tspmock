package cn.com.yusys.yusp.message.channel;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.gxp.wxpt.qywx.QywxReqDto;
import cn.com.yusys.yusp.dto.client.gxp.wxpt.qywx.QywxRespDto;
import cn.com.yusys.yusp.message.constant.ReceivedUserTypeEnum;
import cn.com.yusys.yusp.message.entity.MessageContentEntity;
import cn.com.yusys.yusp.message.entity.MessagePoolEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.service.Dscms2QywxClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

/**
 * 发布微信消息
 * <p>
 * 需要使用者结合微信平台实现
 *
 * @author liucheng3@yusys.com.cn
 **/
@Component
@Slf4j
public class WechatChannelPublisher implements MessageChannelPublisher {

    @Autowired
    Dscms2QywxClientService dscms2QywxClientService;

    @Override
    @Async
    public CompletableFuture<Boolean> publish(MessagePoolEntity messagePoolEntity, MessageContentEntity messageContentEntity, AdminSmUserEntity userEntity) {
        QywxReqDto qywxReqDto1 = new QywxReqDto();
        if(ReceivedUserTypeEnum.JKR.getCode().equals(messagePoolEntity.getUserType())){
            qywxReqDto1.setMobile(messagePoolEntity.getUserAddr());
        }else{
            // 企业微信，电话号码送工号
            qywxReqDto1.setMobile(userEntity.getLoginCode());
        }
        qywxReqDto1.setText(messageContentEntity.getContent());

        // 调用BSP服务接口，发送消息至微信平台
        try {
            ResultDto<QywxRespDto> result =  dscms2QywxClientService.qywx(qywxReqDto1);
            if("0".equals(result.getCode())){
                log.info("发送微信消息: 微信消息发送成功, 接受用户ID: {}, 消息内容: {}", userEntity.getUserId(), messageContentEntity.getContent());
                return CompletableFuture.completedFuture(true);
            }else{
                log.error("发送微信消息: 微信消息发送失败, {}", result.getMessage());
                return CompletableFuture.completedFuture(false);
            }
        }catch (Exception e){
            log.error("发送微信消息: 微信消息发送失败, ", e);
            return CompletableFuture.completedFuture(false);
        }

    }

}
