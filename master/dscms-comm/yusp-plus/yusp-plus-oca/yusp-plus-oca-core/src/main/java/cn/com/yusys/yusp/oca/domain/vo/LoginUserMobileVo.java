package cn.com.yusys.yusp.oca.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * @类名 LoginUserMobileVo
 * @描述 用户手机号数据传输类
 * @作者 黄勃
 * @时间 2021/9/23 10:25
 **/
@ApiModel(description = "用户手机号数据传输类", value = "用户手机号数据传输类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginUserMobileVo {
    @ApiModelProperty(name = "loginCode", value = "用户登录名", required = true, example = "test")
    @NotEmpty(message = "{loginCode}")
    private String loginCode;

    @ApiModelProperty(name = "mobile", value = "用户手机号", required = true, example = "123456")
    @NotEmpty(message = "手机号不能为空")
    private String mobile;
}
