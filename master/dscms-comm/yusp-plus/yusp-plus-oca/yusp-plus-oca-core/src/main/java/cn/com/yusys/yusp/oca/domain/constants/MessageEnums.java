package cn.com.yusys.yusp.oca.domain.constants;

/**
 * @Classname MessageEnums
 * @Description  返回码
 * @Date 2020/11/4 10:10
 * @Created by wujp4@yusys.com.cn
 */
public enum MessageEnums {
    PASSWORD_MISMATCH("10100000","不匹配登录用户当前密码，请检查原密码"),
    PASSWORD_RESOLUTION_FAILED("10100004","密码解析失败"),
    PASSWORD_ENCODE_FAILED("10100005","密码加密失败"),
    PASSWORD_COMPLEX_SUCCESS("10100001","密码策略通过"),
    PASSWORD_MODIFY_FAILED("10101112","密码修改失败"),
    PASSWORD_LENGTH_NOT_ENOUGH("10100021","密码不能少于位数:"),
    PASSWORD_REPEAT_NUMBERS("10100022","密码不能与最近多少次的密码重复:"),
    PASSWORD_REPEAT_LETTER_LIMIT("10100023","密码连续重复的字符数超过最大长度:"),
    PASSWORD_CONTINUE_LETTER_LIMIT("10100024","密码连续字符的字符数超过最大长度:"),
    NOT_PASSWORD_STRATEGY("10100002","无密码策略"),
    PASSWORD_MOBILE_USER_CODE_FAILED("10100025", "工号或手机号校验不通过"),
    MESSAGE_SEND_FAILED("10100027", "信息发送失败"),
    MOBILE_MODIFY_SUCCESSFUL("10100028", "绑定手机号成功"),
    MOBILE_MODIFY_FAILED("10100029", "绑定手机号失败");


    //可以看出这在枚举类型里定义变量和方法和在普通类里面定义方法和变量没有什么区别。
    //唯一要注意的只是变量和方法定义必须放在所有枚举值定义的后面，否则编译器会给出一个错误。
    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



    MessageEnums(String code, String message){ //加上public void 上面定义枚举会报错 The constructor Color(int, String) is undefined
        this.code=code;
        this.message=message;

    }
}
