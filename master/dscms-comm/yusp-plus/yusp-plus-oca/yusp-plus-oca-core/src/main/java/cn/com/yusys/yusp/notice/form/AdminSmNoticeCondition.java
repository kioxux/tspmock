package cn.com.yusys.yusp.notice.form;

import lombok.Data;

@Data
public class AdminSmNoticeCondition {

    /**
     * 公告标题
     */
    private String noticeTitle;

    private String noticeContent;
    /**
     * 公告重要程度
     */
    private String noticeLevel;
    /**
     * 发布状态（状态：对应字典项=NORM_STS C：未发布O：已发布）
     */
    private String pubSts;
    /**
     * 是否阅读
     */
    private String readSts;
    /**
     * 模糊查询关键字
     */
    private String keyWord;
    /**
     * 分页查询条件
     */
    private int page;
    private int size;
}
