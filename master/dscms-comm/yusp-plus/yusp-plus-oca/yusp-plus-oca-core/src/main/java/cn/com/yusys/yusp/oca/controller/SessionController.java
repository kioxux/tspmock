package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.session.SessionService;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.user.UserIdentity;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmDutyEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserDutyRelEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.domain.session.CmisOrganizationImpl;
import cn.com.yusys.yusp.oca.domain.session.CmisUserInformation;
import cn.com.yusys.yusp.oca.domain.session.DutyImpl;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmOrgDetailVo;
import cn.com.yusys.yusp.oca.service.AdminSmDutyService;
import cn.com.yusys.yusp.oca.service.AdminSmOrgService;
import cn.com.yusys.yusp.oca.service.AdminSmUserDutyRelService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 获取用户信息
 *
 * @author wujiangpeng
 * @email wujp4@yusys.com.cn
 * @since 2020-11-20 17:31
 */
@RestController
@RequestMapping("/api/session")
public class SessionController {

    @Autowired
    SessionService sessionService;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private AdminSmUserDutyRelService adminSmUserDutyRelService;

    @Autowired
    private AdminSmDutyService adminSmDutyService;

    /**
     * 根据authorization查询用户信息
     * @return 用户信息
     */
    @GetMapping(value = "/info")
    public User getUserByAuthorization() {
        stringRedisTemplate.delete("Session:" + SessionUtils.getClientId() + "-" + SessionUtils.getUserId());
        User userInformation = sessionService.getUserInfo(SessionUtils.getClientId(), SessionUtils.getUserId());
        // 定制前端返回添加机构属性和岗位信息
        UserIdentity org = userInformation.getOrg();
        CmisUserInformation cmisUserInformation = BeanUtils.beanCopy(userInformation, CmisUserInformation.class);
        if(org != null){
            CmisOrganizationImpl cmisOrganization = new CmisOrganizationImpl();
            String orgId = org.getId();
            AdminSmOrgDetailVo adminSmOrgDetailVo = adminSmOrgService.getDetailById(orgId);
            if(adminSmOrgDetailVo != null){
                cmisOrganization.setOrgId(adminSmOrgDetailVo.getOrgId());
                cmisOrganization.setOrgCode(adminSmOrgDetailVo.getOrgCode());
                cmisOrganization.setOrgLevel(String.valueOf(adminSmOrgDetailVo.getOrgLevel()));
                cmisOrganization.setOrgName(adminSmOrgDetailVo.getOrgName());
                cmisOrganization.setOrgType(adminSmOrgDetailVo.getOrgType());
            }
            cmisUserInformation.setOrg(cmisOrganization);
            // 岗位信息
            AdminSmUserEntity user = new AdminSmUserEntity();
            user.setUserId(userInformation.getUserId());
            List<AdminSmUserDutyRelEntity> userDutyRelList = adminSmUserDutyRelService.findUserDutyRelsByUser(user);
            if(CollectionUtils.isNotEmpty(userDutyRelList)){
                List<String> dutyIdList = userDutyRelList.stream().map(AdminSmUserDutyRelEntity::getDutyId).distinct().collect(Collectors.toList());
                List<AdminSmDutyEntity> adminSmDutyList = adminSmDutyService.listByIds(dutyIdList);
                List<DutyImpl> dutyList = (List<DutyImpl>)BeanUtils.beansCopy(adminSmDutyList, DutyImpl.class);
                cmisUserInformation.setDutys(dutyList);
            }
        }
        return cmisUserInformation;
    }

}