package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmOrgTreeNodeBo;
import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmOrgEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmOrgTreeQuery;
import cn.com.yusys.yusp.oca.service.AdminSmDptService;
import cn.com.yusys.yusp.oca.service.AdminSmOrgService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author danyu
 */

@RestController
@RequestMapping("/api/util")
@Slf4j
public class SystemUtilController {

    @Autowired
    AdminSmDptService adminSmDptService;

    @Autowired
    AdminSmOrgService adminSmOrgService;

//    @GetMapping("/getdpt")
//    public R getDptByParam(@RequestParam(required = false) String orgCode) {
//        PageUtils pageUtils = adminSmDptService.getDptByParam(orgCode);
//
//        return R.ok().put("data", pageUtils.getList()).put("total", pageUtils.getTotalCount());
//    }

    @GetMapping("/getorgById")
    public ResultDto getorgById(String orgId) {
        List<AdminSmOrgEntity> orgList = adminSmOrgService
                .list(new QueryWrapper<AdminSmOrgEntity>()
                        .eq(!StringUtils.isEmpty(orgId), "ORG_ID", orgId));
        return ResultDto.success().extParam("data", orgList.get(0)).extParam("total", orgList.size()).extParam("code", "0");
    }

    /**
     * 不太完美的接口
     */
    @GetMapping("/getorgtree")
    public ResultDto getOrgtreeByParam(@RequestParam String orgId, String userId,
                               String orgLevel, boolean needManage, boolean
                                       needFin, boolean needDpt, boolean lazy) {
        AdminSmOrgTreeQuery query = new AdminSmOrgTreeQuery();
        query.setOrgId(orgId);
        query.setOrgSts(AvailableStateEnum.ENABLED);
        List<AdminSmOrgTreeNodeBo> orgTree = adminSmOrgService.getOrgTree(query);
        return ResultDto.success().extParam("data", orgTree).extParam("total", 1);
    }

    /**
     * 不太完美的接口
     */
    @GetMapping("/getorg")
    public ResultDto getOrgByParam(String userId, String orgId,
                           boolean needManageOrg, boolean lazy) {

        AdminSmOrgTreeQuery query = new AdminSmOrgTreeQuery();
        query.setOrgId(orgId);
        query.setOrgSts(AvailableStateEnum.ENABLED);
        List<AdminSmOrgTreeNodeBo> orgTree = adminSmOrgService.getOrgTree(query);
        return ResultDto.success().extParam("data", orgTree).extParam("total", 1);
    }
}
