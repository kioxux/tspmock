package cn.com.yusys.yusp.notice.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 富文本信息表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-15 13:05:43
 */
@Data
@TableName("admin_sm_richedit_info")
public class AdminSmRicheditInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 富文本编号
	 */
	@TableId
	private String richeditId;
	/**
	 * 关联业务模块（NOTICE-公告；）
	 */
	private String relMod;
	/**
	 * 关联业务主表编号
	 */
	private String relId;
	/**
	 * 文本内容
	 */
	private String content;

}
