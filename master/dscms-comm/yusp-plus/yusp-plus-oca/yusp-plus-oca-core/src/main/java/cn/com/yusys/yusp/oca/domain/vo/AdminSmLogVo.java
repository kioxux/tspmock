package cn.com.yusys.yusp.oca.domain.vo;

import cn.com.yusys.yusp.oca.config.annotation.RedisDictTranslator;
import cn.com.yusys.yusp.oca.domain.constants.Constants;
import lombok.Data;

/**
 * @author danyu
 */
@Data
public class AdminSmLogVo {

	/**
	 * 记录编号
	 */
	private String logId;
	/**
	 * 用户ID
	 */
	@RedisDictTranslator(redisCacheKey = Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_USER_NAME,fieldName = "userName")
	private String userId;
	/**
	 * 操作时间
	 */
	private String operTime;
	/**
	 * 操作对象ID
	 */
	private String operObjId;
	/**
	 * 操作前值
	 */
	private String beforeValue;
	/**
	 * 操作后值
	 */
	private String afterValue;
	/**
	 * 操作标志
	 */
	private String operFlag;
	/**
	 * 日志类型
	 */
	private String logTypeId;
	/**
	 * 日志内容
	 */
	private String content;
	/**
	 * 操作者机构
	 */
	@RedisDictTranslator(redisCacheKey = Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_ORG_NAME,fieldName = "orgName")
	private String orgId;
	/**
	 * 登录IP
	 */
	private String loginIp;
	/**
	 * 用户名 上面去翻译了
	 */
//	private String userName;
	/**
	 * 机构名
	 */
//	private String orgName;

}
