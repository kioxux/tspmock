package cn.com.yusys.yusp.message.enumeration;

/**
 * 消息模板发送类型
 *
 * @author xiaodg@yusys.com.cn
 **/
public enum MessageTempSendTypeEnum {
    /**
     * 实时消息
     */
    REAL("S"),
    /**
     * 订阅消息
     */
    SUBSCRIBE("D");

    private final String type;

    MessageTempSendTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
