package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmLookupDictBo;
import cn.com.yusys.yusp.oca.domain.query.AdminSmLookupDictQuery;
import cn.com.yusys.yusp.oca.service.AdminSmLookupDictService;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmLookupDictVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Map;


/**
 * 数据字典内容表，新数据字典接口
 *
 * @author danyb1
 * @date 2021-01-15 16:44:33
 */
@Api("数据字典内容表接口")
@RestController
@RequestMapping("/api/adminsmlookupdict")
public class AdminSmLookupDictController {
    @Autowired
    private AdminSmLookupDictService adminSmLookupDictService;

    /**
     * 列表，包括 模糊条件查询
     */
    @ApiOperation("列表")
    @GetMapping("/list")
    public ResultDto<AdminSmLookupDictVo> list(AdminSmLookupDictQuery adminSmLookupDictQuery) {
        return ResultDto.success(adminSmLookupDictService.queryLookupDictWithCondition(adminSmLookupDictQuery));
    }


    /**
     * 详情信息
     */
    @ApiOperation("详情信息")
    @PostMapping("/info/{lookupItemId}")
    public ResultDto info(@PathVariable("lookupItemId") String lookupItemId) {
        return ResultDto.success(adminSmLookupDictService.queryLookupDictInfoById(lookupItemId));
    }

    /**
     * 查询初始化 字典分类
     */
    @ApiOperation("查询初始化字典分类")
    @GetMapping("/queryinitdict")
    public ResultDto info() {
        return ResultDto.success(adminSmLookupDictService.queryInitLookupDict());
    }

    /**
     * 保存
     */
    @ApiOperation("保存")
    @PostMapping("/save")
    public ResultDto save(@RequestBody @Validated AdminSmLookupDictBo adminSmLookupDictBo) {
        adminSmLookupDictService.saveLookupDictByDictBo(adminSmLookupDictBo);
        return ResultDto.success();
    }

    /**
     * 插入字典项
     */
    @ApiOperation("插入字典项")
    @PostMapping("/insertdictitem")
    public ResultDto insertdicttime(@RequestBody @Validated AdminSmLookupDictBo adminSmLookupDictBo) {
        adminSmLookupDictService.insertLookupDictByDictBo(adminSmLookupDictBo);
        return ResultDto.success();

    }

    /**
     * 修改
     */
    @ApiOperation("修改")
    @PostMapping("/update")
    public ResultDto update(@RequestBody @Validated AdminSmLookupDictBo adminSmLookupDictBo) {
        adminSmLookupDictService.updateLookupDictByDictBo(adminSmLookupDictBo);
        return ResultDto.success();
    }

    /**
     * batch del
     */
    @ApiOperation("删除")
    @PostMapping("/delete")
    public ResultDto delete(@RequestBody @NotNull String[] ids) {
        adminSmLookupDictService.removeLookupDictByIds(ids);
        return ResultDto.success();
    }

    /**
     * 刷新缓存
     *
     * @return
     */
    @GetMapping("/refreshdict")
    public ResultDto refreshItemCache() {
        adminSmLookupDictService.refreshLookupDict();
        return ResultDto.success("数据字典缓存更新成功");
    }

    /**
     * 查询数据字典
     *
     * @return ResultDto
     */
    @GetMapping("/querylookupcode")
    public ResultDto querylookupcode(@RequestParam("lookupCodes") String lookupCodes) {
        return ResultDto.success(adminSmLookupDictService.querylookupcode(lookupCodes));
    }

    /**
     * 查询数据字典
     *
     * @return ResultDto
     */
    @PostMapping("/querylookupcodeForOa")
    public ResultDto querylookupcodeForOa(@RequestBody Map<String, String> param) {
        String lookupCodes = param.get("lookupCodes");
        return ResultDto.success(adminSmLookupDictService.querylookupcode(lookupCodes));
    }


}
