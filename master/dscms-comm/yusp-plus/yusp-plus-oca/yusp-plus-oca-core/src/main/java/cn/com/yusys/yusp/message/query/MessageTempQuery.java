package cn.com.yusys.yusp.message.query;

import lombok.Data;

import java.io.Serializable;

/**
 * 消息模板分页查询对象
 *
 * @author xiaodg@yusys.com.cn
 **/
@Data
public class MessageTempQuery implements Serializable {
    private static final long serialVersionUID = 1259410152905747712L;

    /**
     * 消息渠道
     */
    private String channelType;

    /**
     * 消息类型
     */
    private String messageType;
}
