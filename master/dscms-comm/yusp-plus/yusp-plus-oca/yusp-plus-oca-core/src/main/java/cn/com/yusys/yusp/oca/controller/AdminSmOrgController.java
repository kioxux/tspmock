package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.UserAndOrgInfoReqDto;
import cn.com.yusys.yusp.dto.UserAndOrgInfoRespDto;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmOrgTreeNodeBo;
import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmOrgEntity;
import cn.com.yusys.yusp.oca.config.validation.Insert;
import cn.com.yusys.yusp.oca.config.validation.Update;
import cn.com.yusys.yusp.oca.domain.query.AdminSmOrgExtQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmOrgTreeQuery;
import cn.com.yusys.yusp.oca.service.AdminSmOrgService;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmOrgDetailVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmOrgVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;


/**
 * 系统机构表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-11-27 18:06:35
 */
@RestController
@RequestMapping("/api/adminsmorg")
@Slf4j
public class AdminSmOrgController {

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    /**
     * 列表
     */
    @GetMapping("/querypage")
    public ResultDto<AdminSmOrgVo> querypage(AdminSmOrgExtQuery query) {
        return ResultDto.success(adminSmOrgService.queryPage(query));
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/11 16:20
     * @注释 列表查询 条件查询
     */
    @PostMapping("/querypagebyall")
    public ResultDto querypagebyall(@RequestBody QueryModel queryModel) {

        return   adminSmOrgService.querypagebyall(queryModel);
    }

    /**
     * @创建人 jijian
     * @创建时间 2021/6/11 16:20
     * @注释 列表查询 条件查询 (贷后专用-适配角色数据授权)
     */
    @PostMapping("/querypagebyallForDh")
    public ResultDto querypagebyallForDh(@RequestBody QueryModel queryModel) {
        return   adminSmOrgService.querypagebyallForDh(queryModel);
    }

    /**
     * 详情
     * @param orgId
     * @return
     */
    @GetMapping("/info/{orgId}")
    public ResultDto<AdminSmOrgDetailVo> querypage(@PathVariable String orgId) {
        return ResultDto.success(adminSmOrgService.getDetailById(orgId));
    }

    /**
     *
     * @方法名称: orgTreeQuery
     * @方法描述:  机构树查询
     * @参数与返回说明:
     *  - orgId：默认值是当前用户所在机构
     *  - orgSts：为null或空字串时查询所有状态的机构，有值时只返回对应状态的机构树
     *  - sort：默认值是last_chg_dt desc
     * @算法描述: 从数据库中查出全量机构数据，通过代码去组织树形数据结构，从输入orgId算起最多返回5级（）
     */
    @GetMapping("/orgtreequery")
    public ResultDto<AdminSmOrgTreeNodeBo> orgTreeQuery(AdminSmOrgTreeQuery query){
        log.info("Query organization tree");
        query.setOrgSts(Optional.ofNullable(query.getOrgSts()).orElse(AvailableStateEnum.ENABLED));
        List<AdminSmOrgTreeNodeBo> list = adminSmOrgService.getOrgTree(query);
        return ResultDto.success(list).total(list.size());
    }

    /**
     * 新增
     */
    @PostMapping("/save")
    public ResultDto<Objects> save(@RequestBody @Validated({Insert.class}) AdminSmOrgEntity adminSmOrg){
		adminSmOrgService.save(adminSmOrg);
        return ResultDto.successMessage("新增成功");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public ResultDto<Object> update(@RequestBody @Validated({Update.class}) AdminSmOrgEntity adminSmOrg){
		adminSmOrgService.updateBy(adminSmOrg);
        return ResultDto.successMessage("修改成功");
    }


    /**
     * 批量启用
     */
    @PostMapping("/batchenable")
    public ResultDto<Object> batchEnable(@RequestBody @NotNull String[] ids){
        adminSmOrgService.batchEnable(ids);
        return ResultDto.successMessage("启用成功");
    }


    /**
     * 批量停用
     */
    @PostMapping("/batchdisable")
    public ResultDto<Object> batchDisable(@RequestBody @NotNull String[] ids){
        adminSmOrgService.batchDisable(ids);
        return ResultDto.successMessage("停用成功");
    }


    /**
     * 批量删除
     */
    @PostMapping("/batchdelete")
    public ResultDto<Object> batchDelete(@RequestBody @NotNull String[] ids){
        adminSmOrgService.batchDelete(ids);
        return ResultDto.successMessage("删除成功");
    }

    /**
     * @方法名称: getallancestryorg
     * @方法描述: 获取指定机构所有先辈机构详情
     * @参数与返回说明:
     *
     * 默认取当前用户所在机构上级机构
     */
    @GetMapping("/getallancestryorgs")
    public ResultDto<AdminSmOrgTreeNodeBo> getAllAncestryOrgs(String orgId){
        log.info("Query parent organization");
        Set<AdminSmOrgTreeNodeBo> allAncestryOrgs = adminSmOrgService.getAllAncestryOrgs(orgId);
        return ResultDto.success(allAncestryOrgs);
    }


    /**
     * @方法名称: getparentorg
     * @方法描述: 获取指定机构父机构详情
     * @参数与返回说明:
     *
     * 默认取当前用户所在机构上级机构
     */
    @GetMapping("/getparentorg")
    public ResultDto<AdminSmOrgEntity> getParentOrg(String orgId){
        log.info("Query parent organization");
        AdminSmOrgEntity parent = adminSmOrgService.getParentOrg(orgId);
        return ResultDto.success(parent);
    }

    /**
     * @方法名称: getsiblingtorgs
     * @方法描述: 获取指定机构兄弟机构详情
     * @参数与返回说明:
     *
     * 默认取当前用户所在机构上级机构
     */
    @GetMapping("/getsiblingorgs")
    public ResultDto<AdminSmOrgEntity> getSiblingOrgs(String orgId){
        log.info("Query sibling organization");
        Set<AdminSmOrgEntity> siblings = adminSmOrgService.getSiblingOrgs(orgId);
        return ResultDto.success(siblings);
    }

    /**
     * 通过机构代码查询机构
     *
     * @param orgCode 机构代码
     * @return
     */
    @PostMapping("/getbyorgcode")
    public ResultDto<AdminSmOrgDto> getByOrgCode(@RequestBody String orgCode) {
        return ResultDto.success(adminSmOrgService.getByOrgCode(orgCode));
    }

    /**
     * 通过多条机构代码查询多个机构
     *
     * @param orgCodes 机构代码列表
     * @return
     */
    @PostMapping("/getbyorgcodelist")
    public ResultDto<List<AdminSmOrgDto>> getByOrgCodeList(@RequestBody List<String> orgCodes) {
        List<AdminSmOrgDto> adminSmUserDtoList = adminSmOrgService.getByOrgCodeList(orgCodes);
        return ResultDto.success(adminSmUserDtoList);
    }

    /**
     * 根据机构编号列表分页查询用户和机构信息
     * @param userAndOrgInfoReqDto
     * @return
     */
    @PostMapping("/getuserandorginfo")
    public ResultDto<List<UserAndOrgInfoRespDto>> getUserAndOrgInfo(@RequestBody UserAndOrgInfoReqDto userAndOrgInfoReqDto){
        List<UserAndOrgInfoRespDto> userAndOrgInfoRespDtos = adminSmOrgService.getUserAndOrgInfo(userAndOrgInfoReqDto);
        return ResultDto.success(userAndOrgInfoRespDtos);
    }

    /**
     * 根据机构编号及下级机构号分页查询用户和机构信息
     * @author ：zhangliang15
     * @date 2021/09/28 14:43
     */
    @PostMapping("/getUserByLowerOrgInfo")
    public ResultDto<List<UserAndOrgInfoRespDto>> getUserByLowerOrgInfo(@RequestBody QueryModel queryModel){
        List<UserAndOrgInfoRespDto> userAndOrgInfoRespDtos = adminSmOrgService.getUserByLowerOrgInfo(queryModel);
        return ResultDto.success(userAndOrgInfoRespDtos);
    }

    /**
     * 获取下级机构号
     * @author jijian_yx
     * @date 2021/8/24 14:43
     */
    @PostMapping("/getlowerorgid")
    public ResultDto<List<String>> getLowerOrgId(@RequestBody String orgCode){
        List<String> orgCodes = adminSmOrgService.getLowerOrgId(orgCode);
        return ResultDto.success(orgCodes);
    }

    /**
     * 获取下级机构号
     * @author 顾银华
     * @date 2021/8/24 14:43
     */
    @PostMapping("/getChildrenOrgList")
    public ResultDto<List<AdminSmOrgDto>> getChildrenOrgList(@RequestBody String orgCode){
        List<AdminSmOrgDto> orgCodes = adminSmOrgService.getChildrenOrgList(orgCode);
        return ResultDto.success(orgCodes);
    }

    /**
     * 获取下级机构号
     * @param queryType  0-本地机构；1-异地机构
     * @author 顾银华
     * @date 2021/8/24 14:43
     */
    @PostMapping("/getRemoteOrgList")
    public ResultDto<List<String>> getRemoteOrgList(@RequestBody String queryType){
        List<String> orgCodes = adminSmOrgService.getRemoteOrgList(queryType);
        return ResultDto.success(orgCodes);
    }
}