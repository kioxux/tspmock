package cn.com.yusys.yusp.oca.dao;

import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserMgrOrgEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户授权管理机构表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-02 16:29:50
 */

public interface AdminSmUserMgrOrgDao extends BaseMapper<AdminSmUserMgrOrgEntity> {
	
}
