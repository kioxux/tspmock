package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 产品审批信息表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-12-02 18:03:52
 */
@Data
@TableName("cms_product_approval_info")
public class ProductApprovalInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 产品审批模板id
	 */
	@TableId
	private String productApprovalId;
	/**
	 * 产品Id
	 */
	private String productId;
	/**
	 * 模板信息
	 */
	private String templateInfo;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 产品审批显示状态（1-逻辑有效，0-逻辑删除）
	 */
	private String logicStatus;

}
