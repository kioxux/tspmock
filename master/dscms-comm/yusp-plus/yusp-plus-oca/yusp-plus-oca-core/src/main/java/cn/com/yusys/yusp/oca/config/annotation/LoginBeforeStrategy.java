package cn.com.yusys.yusp.oca.config.annotation;

import java.lang.annotation.*;

/**
 * @description: 登录前校验动作（拦截器实现）
 * @author: zhangsong
 * @date: 2021/3/31
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LoginBeforeStrategy {
    String loginCode() default "";
}