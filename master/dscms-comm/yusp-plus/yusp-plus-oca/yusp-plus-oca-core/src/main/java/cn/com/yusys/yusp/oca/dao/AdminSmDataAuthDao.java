package cn.com.yusys.yusp.oca.dao;

import cn.com.yusys.yusp.oca.domain.entity.AdminSmDataAuthEntity;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmDataTmplVo;
import cn.com.yusys.yusp.oca.domain.vo.ResControlDataAuthTmplVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 数据权限表
 * 
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-12-01 09:52:42
 */

public interface AdminSmDataAuthDao extends BaseMapper<AdminSmDataAuthEntity> {

    IPage<ResControlDataAuthTmplVo> pageResControlDataAuthTmpl(Page<ResControlDataAuthTmplVo> page,
                                                               @Param(Constants.WRAPPER) QueryWrapper<ResControlDataAuthTmplVo> queryWrapper);

    List<AdminSmDataTmplVo> selectWithTmplIds(@Param(Constants.WRAPPER) QueryWrapper<AdminSmDataTmplVo> wrapper);
}
