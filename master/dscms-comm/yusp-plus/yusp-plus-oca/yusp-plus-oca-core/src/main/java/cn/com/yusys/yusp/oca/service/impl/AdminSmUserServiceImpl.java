package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.common.utils.RAMPager;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.redis.template.YuspRedisTemplate;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.Asserts;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.ObjectUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.OcaTradeCommonConstance;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.notice.vo.AdminSmReciveVo;
import cn.com.yusys.yusp.oca.dao.AdminSmUserDao;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmOrgTreeNodeBo;
import cn.com.yusys.yusp.oca.domain.bo.PasswordForgetAndResetCheckBo;
import cn.com.yusys.yusp.oca.domain.bo.PasswordResetBo;
import cn.com.yusys.yusp.oca.domain.constants.*;
import cn.com.yusys.yusp.oca.domain.dto.PasswordLogDto;
import cn.com.yusys.yusp.oca.domain.entity.*;
import cn.com.yusys.yusp.oca.domain.query.AdminSmPasteUserQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmUserDutyRelQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmUserQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmUserRoleRelQuery;
import cn.com.yusys.yusp.oca.domain.vo.*;
import cn.com.yusys.yusp.oca.service.*;
import cn.com.yusys.yusp.oca.utils.I18nMessageByCode;
import cn.com.yusys.yusp.oca.utils.PasswordUtils;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.builder.MapperBuilderAssistant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author terry
 * @email tanrui1@yusys.com.cn
 */

@Slf4j
@Service("adminSmUserService")
public class AdminSmUserServiceImpl extends ServiceImpl<AdminSmUserDao, AdminSmUserEntity> implements AdminSmUserService {

    @Autowired
    I18nMessageByCode i18nMessageByCode;
    @Autowired
    private AdminSmDptService adminSmDptService;
    @Autowired
    private AdminSmUserRoleRelService adminSmUserRoleRelService;
    @Autowired
    private AdminSmUserDutyRelService adminSmUserDutyRelService;
    @Autowired
    private AdminSmRoleService adminSmRoleService;

    //    @Autowired
//    private SmsComponent smsComponent;
    @Autowired
    private AdminSmOrgService adminSmOrgService;
    @Autowired
    private AdminSmLogicSysService adminSmLogicSysService;
    @Autowired
    private AdminSmInstuService adminSmInstuService;
    @Autowired
    private AdminSmPasswordLogService passwordLogService;
    @Autowired
    private AdminSmUserMgrOrgService userMgrOrgService;
    @Autowired
    private AdminSmDutyService adminSmDutyService;
    @Autowired
    private PasswordUtils passwordUtils;
    @Autowired
    private YuspRedisTemplate yuspRedisTemplate;

    @Override
    public Page<AdminSmUserVo> queryPage(AdminSmUserQuery query) {
        if (StringUtils.isEmpty(query.getOrgId())) {
            query.setOrgId(SessionUtils.getUserOrganizationId());
        }
        //使用LambdaQueryWrapper分页查询且返回Vo类实体时需要手动预加载缓存
        TableInfoHelper.initTableInfo(new MapperBuilderAssistant(new MybatisConfiguration(), ""), AdminSmUserVo.class);
        Page<AdminSmUserVo> page = query.getIPage();
        LambdaQueryWrapper<AdminSmUserVo> queryWrapper = new QueryWrapper<AdminSmUserVo>().lambda();
        queryWrapper.eq(StringUtils.nonEmpty(query.getDptId()), AdminSmUserVo::getDptId, query.getDptId());
        queryWrapper.like(StringUtils.nonEmpty(query.getLoginCode()), AdminSmUserVo::getLoginCode, query.getLoginCode());
        queryWrapper.like(StringUtils.nonEmpty(query.getUserCode()), AdminSmUserVo::getUserCode, query.getUserCode());
        queryWrapper.like(StringUtils.nonEmpty(query.getUserName()), AdminSmUserVo::getUserName, query.getUserName());
        queryWrapper.eq(StringUtils.nonEmpty(query.getCertType()), AdminSmUserVo::getCertType, query.getCertType());
        queryWrapper.like(StringUtils.nonEmpty(query.getCertNo()), AdminSmUserVo::getCertNo, query.getCertNo());
        queryWrapper.lt(ObjectUtils.nonNull(query.getDeadline()), AdminSmUserVo::getDeadline, query.getDeadline());
        queryWrapper.eq(StringUtils.nonEmpty(query.getUserSex()), AdminSmUserVo::getUserSex, query.getUserSex());
        queryWrapper.eq(ObjectUtils.nonNull(query.getUserSts()), AdminSmUserVo::getUserSts, query.getUserSts());
        if (StringUtils.nonEmpty(query.getKeyWord())) {
            queryWrapper.and(wrapper -> wrapper
                    .like(StringUtils.nonEmpty(query.getKeyWord()), AdminSmUserVo::getLoginCode, query.getKeyWord())//关键字模糊匹配账号
                    .or()
                    .like(StringUtils.nonEmpty(query.getKeyWord()), AdminSmUserVo::getUserCode, query.getKeyWord())//关键字模糊匹配工号
                    .or()
                    .like(StringUtils.nonEmpty(query.getKeyWord()), AdminSmUserVo::getUserName, query.getKeyWord())//关键字模糊匹配用户名
            );
        }

        queryWrapper.orderByDesc(AdminSmUserVo::getLastChgDt);
        List<AdminSmUserVo> userVoList = this.baseMapper.selectAllUser(queryWrapper);

        List<String> orgIds;
        List<String> belgOrgIds;
        //由前端机构树组件限制可以查询的机构范围，若前端未传入，则默认取当前用户有权访问的所有机构
        if (StringUtils.isEmpty(query.getOrgId())) {
            orgIds = adminSmOrgService.getAllAccessibleOrgIds();
        } else {
            // 获取根节点本身及所有后裔列表
            List<AdminSmOrgVo> allProgeny = this.adminSmOrgService.getAllProgeny(query.getOrgId());
            orgIds = allProgeny.stream().map(AdminSmOrgVo::getOrgId).collect(Collectors.toList());
        }
        // 根据机构访问权限范围过滤
        List<AdminSmUserVo> filteredList = userVoList.stream().filter((user) -> orgIds.contains(user.getOrgId())).collect(Collectors.toList());
        //代码分页
        RAMPager<AdminSmUserVo> pager = new RAMPager<>(filteredList, query.getSize());
        page.setRecords(pager.page(query.getPage()));
        page.setTotal(filteredList.size());
        return page;
    }

    @Override
    public Page<AdminSmUserVo> queryPageForXw(AdminSmUserQuery query) {
        if (StringUtils.isEmpty(query.getOrgId())) {
            query.setOrgId(SessionUtils.getUserOrganizationId());
        }
        //使用LambdaQueryWrapper分页查询且返回Vo类实体时需要手动预加载缓存
        TableInfoHelper.initTableInfo(new MapperBuilderAssistant(new MybatisConfiguration(), ""), AdminSmUserVo.class);
        Page<AdminSmUserVo> page = query.getIPage();
        LambdaQueryWrapper<AdminSmUserVo> queryWrapper = new QueryWrapper<AdminSmUserVo>().lambda();
        queryWrapper.eq(StringUtils.nonEmpty(query.getDptId()), AdminSmUserVo::getDptId, query.getDptId());
        queryWrapper.like(StringUtils.nonEmpty(query.getLoginCode()), AdminSmUserVo::getLoginCode, query.getLoginCode());
        queryWrapper.like(StringUtils.nonEmpty(query.getUserCode()), AdminSmUserVo::getUserCode, query.getUserCode());
        queryWrapper.like(StringUtils.nonEmpty(query.getUserName()), AdminSmUserVo::getUserName, query.getUserName());
        queryWrapper.eq(StringUtils.nonEmpty(query.getCertType()), AdminSmUserVo::getCertType, query.getCertType());
        queryWrapper.like(StringUtils.nonEmpty(query.getCertNo()), AdminSmUserVo::getCertNo, query.getCertNo());
        queryWrapper.lt(ObjectUtils.nonNull(query.getDeadline()), AdminSmUserVo::getDeadline, query.getDeadline());
        queryWrapper.eq(StringUtils.nonEmpty(query.getUserSex()), AdminSmUserVo::getUserSex, query.getUserSex());
        queryWrapper.eq(ObjectUtils.nonNull(query.getUserSts()), AdminSmUserVo::getUserSts, query.getUserSts());
        if (StringUtils.nonEmpty(query.getKeyWord())) {
            queryWrapper.and(wrapper -> wrapper
                    .like(StringUtils.nonEmpty(query.getKeyWord()), AdminSmUserVo::getLoginCode, query.getKeyWord())//关键字模糊匹配账号
                    .or()
                    .like(StringUtils.nonEmpty(query.getKeyWord()), AdminSmUserVo::getUserCode, query.getKeyWord())//关键字模糊匹配工号
                    .or()
                    .like(StringUtils.nonEmpty(query.getKeyWord()), AdminSmUserVo::getUserName, query.getKeyWord())//关键字模糊匹配用户名
            );
        }

        queryWrapper.orderByDesc(AdminSmUserVo::getLastChgDt);
        queryWrapper.eq(AdminSmUserVo::getBelgMainOrgId, "016000");
        List<AdminSmUserVo> userVoList = this.baseMapper.selectAllUserForXw(queryWrapper);

        //代码分页
        RAMPager<AdminSmUserVo> pager = new RAMPager<>(userVoList, query.getSize());
        page.setRecords(pager.page(query.getPage()));
        page.setTotal(userVoList.size());
        return page;
    }


    @Override
    public Page<AdminSmUserVo> queryPageForDh(AdminSmUserQuery query) {
        if (StringUtils.isEmpty(query.getOrgId())) {
            query.setOrgId(SessionUtils.getUserOrganizationId());
        }
        //使用LambdaQueryWrapper分页查询且返回Vo类实体时需要手动预加载缓存
        TableInfoHelper.initTableInfo(new MapperBuilderAssistant(new MybatisConfiguration(), ""), AdminSmUserVo.class);
        Page<AdminSmUserVo> page = query.getIPage();
        LambdaQueryWrapper<AdminSmUserVo> queryWrapper = new QueryWrapper<AdminSmUserVo>().lambda();
        queryWrapper.eq(StringUtils.nonEmpty(query.getDptId()), AdminSmUserVo::getDptId, query.getDptId());
        queryWrapper.like(StringUtils.nonEmpty(query.getLoginCode()), AdminSmUserVo::getLoginCode, query.getLoginCode());
        queryWrapper.like(StringUtils.nonEmpty(query.getUserCode()), AdminSmUserVo::getUserCode, query.getUserCode());
        queryWrapper.like(StringUtils.nonEmpty(query.getUserName()), AdminSmUserVo::getUserName, query.getUserName());
        queryWrapper.eq(StringUtils.nonEmpty(query.getCertType()), AdminSmUserVo::getCertType, query.getCertType());
        queryWrapper.like(StringUtils.nonEmpty(query.getCertNo()), AdminSmUserVo::getCertNo, query.getCertNo());
        queryWrapper.lt(ObjectUtils.nonNull(query.getDeadline()), AdminSmUserVo::getDeadline, query.getDeadline());
        queryWrapper.eq(StringUtils.nonEmpty(query.getUserSex()), AdminSmUserVo::getUserSex, query.getUserSex());
        queryWrapper.eq(ObjectUtils.nonNull(query.getUserSts()), AdminSmUserVo::getUserSts, query.getUserSts());
        if (StringUtils.nonEmpty(query.getKeyWord())) {
            queryWrapper.and(wrapper -> wrapper
                    .like(StringUtils.nonEmpty(query.getKeyWord()), AdminSmUserVo::getLoginCode, query.getKeyWord())//关键字模糊匹配账号
                    .or()
                    .like(StringUtils.nonEmpty(query.getKeyWord()), AdminSmUserVo::getUserCode, query.getKeyWord())//关键字模糊匹配工号
                    .or()
                    .like(StringUtils.nonEmpty(query.getKeyWord()), AdminSmUserVo::getUserName, query.getKeyWord())//关键字模糊匹配用户名
            );
        }
        queryWrapper.orderByDesc(AdminSmUserVo::getLastChgDt);
        List<AdminSmUserVo> userVoList = this.baseMapper.selectAllUser(queryWrapper);
        //代码分页
        RAMPager<AdminSmUserVo> pager = new RAMPager<>(userVoList, query.getSize());
        page.setRecords(pager.page(query.getPage()));
        page.setTotal(userVoList.size());
        return page;
    }

    public Page<AdminSmUserVo> queryPageAll(AdminSmUserQuery query) {
        if (StringUtils.isEmpty(query.getOrgId())) {
            query.setOrgId(SessionUtils.getUserOrganizationId());
            query.setBelgMainOrgId(SessionUtils.getUserOrganizationId());
        }
        //使用LambdaQueryWrapper分页查询且返回Vo类实体时需要手动预加载缓存
        TableInfoHelper.initTableInfo(new MapperBuilderAssistant(new MybatisConfiguration(), ""), AdminSmUserVo.class);
        Page<AdminSmUserVo> page = query.getIPage();
        LambdaQueryWrapper<AdminSmUserVo> queryWrapper = new QueryWrapper<AdminSmUserVo>().lambda();
        queryWrapper.eq(StringUtils.nonEmpty(query.getDptId()), AdminSmUserVo::getDptId, query.getDptId());
        queryWrapper.like(StringUtils.nonEmpty(query.getLoginCode()), AdminSmUserVo::getLoginCode, query.getLoginCode());
        queryWrapper.like(StringUtils.nonEmpty(query.getUserCode()), AdminSmUserVo::getUserCode, query.getUserCode());
        queryWrapper.like(StringUtils.nonEmpty(query.getUserName()), AdminSmUserVo::getUserName, query.getUserName());
        queryWrapper.eq(StringUtils.nonEmpty(query.getCertType()), AdminSmUserVo::getCertType, query.getCertType());
        queryWrapper.like(StringUtils.nonEmpty(query.getCertNo()), AdminSmUserVo::getCertNo, query.getCertNo());
        queryWrapper.lt(ObjectUtils.nonNull(query.getDeadline()), AdminSmUserVo::getDeadline, query.getDeadline());
        queryWrapper.eq(StringUtils.nonEmpty(query.getUserSex()), AdminSmUserVo::getUserSex, query.getUserSex());
        queryWrapper.eq(ObjectUtils.nonNull(query.getUserSts()), AdminSmUserVo::getUserSts, query.getUserSts());
        if (StringUtils.nonEmpty(query.getKeyWord())) {
            queryWrapper.and(wrapper -> wrapper
                    .like(StringUtils.nonEmpty(query.getKeyWord()), AdminSmUserVo::getLoginCode, query.getKeyWord())//关键字模糊匹配账号
                    .or()
                    .like(StringUtils.nonEmpty(query.getKeyWord()), AdminSmUserVo::getUserCode, query.getKeyWord())//关键字模糊匹配工号
                    .or()
                    .like(StringUtils.nonEmpty(query.getKeyWord()), AdminSmUserVo::getUserName, query.getKeyWord())//关键字模糊匹配用户名
            );
        }

        queryWrapper.orderByDesc(AdminSmUserVo::getLastChgDt);
        List<AdminSmUserVo> userVoList = this.baseMapper.selectAllUser(queryWrapper);
        //代码分页
        RAMPager<AdminSmUserVo> pager = new RAMPager<>(userVoList, query.getSize());
        page.setRecords(pager.page(query.getPage()));
        page.setTotal(userVoList.size());
        return page;
    }

    @Override
    public AdminSmUserDetailVo getDetailById(String userId) {
        return this.baseMapper.getDetailById(userId);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public boolean save(AdminSmUserEntity entity) {
        LambdaQueryWrapper<AdminSmUserEntity> checkWrapper = new QueryWrapper<AdminSmUserEntity>().lambda();
        checkWrapper.eq(AdminSmUserEntity::getUserCode, entity.getUserCode());
        checkWrapper.or().eq(AdminSmUserEntity::getLoginCode, entity.getLoginCode());//工号和账号不可重复，用户名可重复
        AdminSmUserEntity check = this.baseMapper.selectOne(checkWrapper);
        if (Objects.nonNull(check)) {
            throw BizException.error("exist", "51100001", "userCode:" + entity.getUserCode() + ",loginCode:" + entity.getLoginCode());
        }
        String defaultPassword = "123456";
        entity.setUserId(entity.getLoginCode());// 用户表主键强制等于登录码
        entity.setUserPassword(new BCryptPasswordEncoder().encode(defaultPassword));
        entity.setUserSts(Optional.ofNullable(entity.getUserSts()).orElse(AvailableStateEnum.UNENABLED));//新增的数据默认是待启用的

        log.info("New user data: [new user: {}] ", entity.getUserName());
        int count = this.baseMapper.insert(entity);

        LambdaQueryWrapper<AdminSmUserEntity> wrapper = new QueryWrapper<AdminSmUserEntity>().lambda();
        wrapper.eq(AdminSmUserEntity::getUserCode, entity.getUserCode());
        AdminSmUserEntity user = this.baseMapper.selectOne(wrapper);

        AdminSmPasswordLogEntity passwordLogEntity = new AdminSmPasswordLogEntity();
        passwordLogEntity.setUserId(user.getUserId());
        passwordLogEntity.setPwdUped(entity.getUserPassword());
        passwordLogEntity.setPwdUpTime(new Date());
        passwordLogEntity.setUpdateUser(SessionUtils.getLoginCode());
        passwordLogService.save(passwordLogEntity);
        if (StringUtils.nonEmpty(user.getUserMobilephone())) {
            //发送密码短信
//            SendSmsResponse smsRes = smsComponent.sendSms("password", new String[]{defaultPassword}, new String[]{"+86" + user.getUserMobilephone()}, user.getUserId());
//            String sendStatusCode = smsRes.getSendStatusSet()[0].getCode();
//            if (!"Ok".equals(sendStatusCode)) {
//                throw BizException.error("smsSendFail", null, smsRes.getRequestId());
//            }
        }
        // 新增用户缓存信息
        yuspRedisTemplate.hset(Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_USER_NAME, entity.getLoginCode(), entity.getUserName());
        return count > 0;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public boolean updateById(AdminSmUserEntity entity) {
        entity.setUserSts(Optional.ofNullable(entity.getUserSts()).orElse(AvailableStateEnum.UNENABLED));
        boolean result = super.updateById(entity);
        if (result) {
            // 更新用户缓存信息
            yuspRedisTemplate.hset(Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_USER_NAME, entity.getLoginCode(), entity.getUserName());
        }
        return result;
    }

    private boolean checkBlocked(String userId) {
        Integer count = this.baseMapper.countRel(userId);//是否有关联的角色、岗位、授权机构
        return count > 0;
    }

    /**
     * 批量启用
     *
     * @param ids
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchEnable(String[] ids) {
        if (CollectionUtils.nonEmpty(ids)) {
            List<String> idList = Arrays.asList(ids);
            idList.forEach((id) -> {
                AdminSmUserEntity entity = new AdminSmUserEntity();
                entity.setUserId(id);
                entity.setUserSts(AvailableStateEnum.ENABLED);

                this.baseMapper.updateById(entity);
            });
        }
    }

    /**
     * 批量停用
     *
     * @param ids
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchDisable(String[] ids) {
        if (CollectionUtils.nonEmpty(ids)) {
            List<String> idList = Arrays.asList(ids);
            idList.forEach((id) -> {
                AdminSmUserEntity entity = new AdminSmUserEntity();
                entity.setUserId(id);
                entity.setUserSts(AvailableStateEnum.DISABLED);
                this.baseMapper.updateById(entity);
            });
        }
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchDelete(String[] ids) {
        if (CollectionUtils.nonEmpty(ids)) {
            List<String> idList = Arrays.asList(ids);
            idList.forEach((id) -> {
                if (checkBlocked(id)) {
                    throw BizException.error(null, "51100002", "该用户存在其它关联信息，请删除关联信息后操作");//这里也可以选择删除用户关联信息
                } else {
                    AdminSmUserDetailVo smUserDetail = baseMapper.getDetailById(id);
                    this.baseMapper.deleteById(id);
                    // 删除用户缓存信息
                    yuspRedisTemplate.hdel(Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_USER_NAME, smUserDetail.getLoginCode());
                }
            });
        }
    }

    /**
     * 查询用户所在机构及其上级机构下可选的所有角色列表,并附带该用户与这些角色的关联状态标志位
     *
     * @param query
     * @return
     */
    @Override
    public List<UserRoleRelVo> getUserRoleList(AdminSmUserRoleRelQuery query) {
        String userId = query.getUserId();
        //Asserts.notEmpty(userId, "getUserRoleList(): userId required!!");
        if (userId == null) {
            throw BizException.error(null, "51100004", "用户编号不能为空");
        }
        AdminSmUserEntity user = this.getById(userId);
        String orgSeq = adminSmOrgService.getOrgSeq(user.getOrgId());//获取机构向上继承路径
        List<String> orgIdList = Arrays.stream(orgSeq.split(",")).filter(StringUtils::nonEmpty).collect(Collectors.toList());

        LambdaQueryWrapper<AdminSmRoleEntity> queryWrapper = new QueryWrapper<AdminSmRoleEntity>().lambda()
                .eq(AdminSmRoleEntity::getRoleSts, AvailableStateEnum.ENABLED)
                .in(AdminSmRoleEntity::getOrgId, orgIdList);
        if (StringUtils.nonEmpty(query.getKeyWord())) {
            queryWrapper.and(wrapper -> wrapper
                    .like(AdminSmRoleEntity::getRoleCode, query.getKeyWord())
                    .or()
                    .like(AdminSmRoleEntity::getRoleName, query.getKeyWord())
            );
        }
        //所有可选角色，只包含启用状态的
        List<AdminSmRoleEntity> allRoleList = adminSmRoleService.list(queryWrapper);
        //用户已关联角色
        List<AdminSmUserRoleRelEntity> selectedRoleRels = adminSmUserRoleRelService.findUserRoleRelsByUser(user);
        List<String> selectedRoleIds = selectedRoleRels.stream().map(AdminSmUserRoleRelEntity::getRoleId).collect(Collectors.toList());
        List<UserRoleRelVo> res = allRoleList.stream().map(entity -> {
            UserRoleRelVo vo = new UserRoleRelVo();
            BeanUtils.beanCopy(entity, vo);
            if (selectedRoleIds.contains(entity.getRoleId())) {
                vo.setChecked(true);
            }
            return vo;
        }).collect(Collectors.toList());
        if (ObjectUtils.nonNull(query.getChecked())) {
            res = res.stream().filter(rel -> query.getChecked().equals(rel.getChecked())).collect(Collectors.toList());
        }
        return res;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveUserRoleList(String userId, List<AdminSmUserRoleRelEntity> list) {
//        Asserts.notEmpty(userId, "saveUserRoleList(): userId required!!");
        if (userId == null) {
            throw BizException.error(null, "51100004", "用户编号不能为空");
        }
        // AdminSmUserEntity user = this.getById(userId);
        LambdaQueryWrapper<AdminSmUserRoleRelEntity> deleteWrapper = new QueryWrapper<AdminSmUserRoleRelEntity>().lambda();
        deleteWrapper.eq(AdminSmUserRoleRelEntity::getUserId, userId);
        adminSmUserRoleRelService.remove(deleteWrapper);
        if (list.size() > 0) {
            adminSmUserRoleRelService.saveBatch(list);
        }
    }

    @Override
    public List<UserDutyRelVo> getUserDutyList(AdminSmUserDutyRelQuery query) {
        String userId = query.getUserId();
//        Asserts.notEmpty(userId, "getUserDutyList(): userId required!!");
        if (userId == null) {
            throw BizException.error(null, "51100004", "用户编号不能为空");
        }
        AdminSmUserEntity user = this.getById(userId);
        //获取机构向上继承路径
        String orgSeq = adminSmOrgService.getOrgSeq(user.getOrgId());
        List<String> orgIdList = Arrays.stream(orgSeq.split(",")).filter(StringUtils::nonEmpty).collect(Collectors.toList());

        LambdaQueryWrapper<AdminSmDutyEntity> queryWrapper = new QueryWrapper<AdminSmDutyEntity>().lambda()
                .eq(AdminSmDutyEntity::getDutySts, AvailableStateEnum.ENABLED)
                .in(AdminSmDutyEntity::getOrgId, orgIdList);
        if (StringUtils.nonEmpty(query.getKeyWord())) {
            queryWrapper.and(wrapper -> wrapper
                    .like(AdminSmDutyEntity::getDutyCode, query.getKeyWord())
                    .or()
                    .like(AdminSmDutyEntity::getDutyName, query.getKeyWord())
            );
        }
        //所有可选岗位
        List<AdminSmDutyEntity> allDutyList = adminSmDutyService.list(queryWrapper);
        List<AdminSmUserDutyRelEntity> selectedDutyRels = adminSmUserDutyRelService.findUserDutyRelsByUser(user);//用户已关联角色
        List<String> selectedDutyIds = selectedDutyRels.stream().map(AdminSmUserDutyRelEntity::getDutyId).collect(Collectors.toList());
        List<UserDutyRelVo> res = allDutyList.stream().map(entity -> {
            UserDutyRelVo vo = new UserDutyRelVo();
            BeanUtils.beanCopy(entity, vo);
            if (selectedDutyIds.contains(entity.getDutyId())) {
                vo.setChecked(true);
            }
            return vo;
        }).collect(Collectors.toList());
        if (ObjectUtils.nonNull(query.getChecked())) {
            res = res.stream().filter(rel -> query.getChecked().equals(rel.getChecked())).collect(Collectors.toList());
        }
        return res;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveUserDutyList(String userId, List<AdminSmUserDutyRelEntity> list) {
//        Asserts.notEmpty(userId, "saveUserDutyList(): userId required!!");
        if (userId == null) {
            throw BizException.error(null, "51100004", "用户编号不能为空");
        }
        LambdaQueryWrapper<AdminSmUserDutyRelEntity> deleteWrapper = new QueryWrapper<AdminSmUserDutyRelEntity>().lambda();
        deleteWrapper.eq(AdminSmUserDutyRelEntity::getUserId, userId);
        adminSmUserDutyRelService.remove(deleteWrapper);
        if (list.size() > 0) {
            adminSmUserDutyRelService.saveBatch(list);
        }
    }

    /**
     * 返回整个机构树，从中扣掉参数指定的用户所在机构为根节点的子树
     *
     * @param userId
     * @return
     */

    @Override
    public List<UserMgrOrgVo> getUserMgrOrgList(String userId) {
//        Asserts.notEmpty(userId, "userId can not be null or empty!!");
        if (userId == null) {
            throw BizException.error(null, "51100004", "用户编号不能为空");
        }
        AdminSmUserEntity user = this.getById(userId);
        List<String> level1orgIds = adminSmOrgService.lambdaQuery()
                .select(AdminSmOrgEntity::getOrgId)
                .eq(AdminSmOrgEntity::getOrgLevel, 1)
                .eq(AdminSmOrgEntity::getOrgSts, AvailableStateEnum.ENABLED)
                .list().stream().map(AdminSmOrgEntity::getOrgId).collect(Collectors.toList());//一级机构ids
        level1orgIds.removeIf((id) -> id.equals(user.getOrgId()));
        List<AdminSmOrgTreeNodeBo> allTree = adminSmOrgService.getOrgTrees(level1orgIds, AvailableStateEnum.ENABLED, user.getOrgId());//全行机构树，排除指定用户所在机构及其所有子节点
        List<AdminSmUserMgrOrgEntity> selectedOrgRels = userMgrOrgService.findOrgRelsByUser(user);//用户已关联机构
        List<UserMgrOrgVo> res = allTree.stream().map((orgTree) -> getOrgRelTree(orgTree, selectedOrgRels)).collect(Collectors.toList());
        return res;
    }

    private UserMgrOrgVo getOrgRelTree(AdminSmOrgTreeNodeBo orgTree, List<AdminSmUserMgrOrgEntity> selectedOrgRels) {
        List<String> selectedOrgIds = selectedOrgRels.stream().map(AdminSmUserMgrOrgEntity::getOrgId).collect(Collectors.toList());
        UserMgrOrgVo rel = new UserMgrOrgVo();
        rel.setOrgId(orgTree.getOrgId());
        rel.setOrgName(orgTree.getOrgName());
        rel.setUpOrgId(orgTree.getUpOrgId());
        if (selectedOrgIds.contains(orgTree.getOrgId())) {
            rel.setChecked(true);
        }
        if (ObjectUtils.nonNullOrEmpty(orgTree.getChildren())) {
            rel.setChildren(orgTree.getChildren().stream().map(node -> getOrgRelTree(node, selectedOrgRels)).collect(Collectors.toList()));
        }
        return rel;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveUserMgrOrg(String userId, List<AdminSmUserMgrOrgEntity> list) {
        LambdaQueryWrapper<AdminSmUserMgrOrgEntity> deleteWrapper = new QueryWrapper<AdminSmUserMgrOrgEntity>().lambda();
        deleteWrapper.eq(AdminSmUserMgrOrgEntity::getUserId, userId);
        userMgrOrgService.remove(deleteWrapper);
        if (list.size() > 0) {
            userMgrOrgService.saveBatch(list);
        }
    }

    @Override
    public Page<AdminSmUserVo> queryPageExcept(AdminSmPasteUserQuery query) {
        Asserts.notEmpty(query.getExpectedUserId(), "Which user do you want to except?");
        Page<AdminSmUserVo> page = query.getIPage();
        LambdaQueryWrapper<AdminSmUserVo> queryWrapper = new QueryWrapper<AdminSmUserVo>().lambda();
        if (StringUtils.nonEmpty(query.getKeyWord())) {
            queryWrapper.and(wrapper -> wrapper
                    .like(StringUtils.nonEmpty(query.getKeyWord()), AdminSmUserVo::getLoginCode, query.getKeyWord())//关键字模糊匹配账号
                    .or()
                    .like(StringUtils.nonEmpty(query.getKeyWord()), AdminSmUserVo::getUserCode, query.getKeyWord())//关键字模糊匹配工号
                    .or()
                    .like(StringUtils.nonEmpty(query.getKeyWord()), AdminSmUserVo::getUserName, query.getKeyWord())//关键字模糊匹配用户名
            );
        }

        queryWrapper.orderByDesc(AdminSmUserVo::getLastChgDt);
        List<AdminSmUserVo> userVoList = this.baseMapper.selectAllUser(queryWrapper);
        userVoList = userVoList.stream().filter((user) -> !user.getUserId().equals(query.getExpectedUserId())).collect(Collectors.toList());//排除指定用户id
        List<String> orgIds = adminSmOrgService.getAllAccessibleOrgIds();
        List<AdminSmUserVo> filteredList = userVoList.stream().filter((user) -> orgIds.contains(user.getOrgId())).collect(Collectors.toList());
        //代码分页
        RAMPager<AdminSmUserVo> pager = new RAMPager<>(filteredList, query.getSize());
        page.setRecords(pager.page(query.getPage()));
        page.setTotal(filteredList.size());
        return page;
    }

    @Override
    public void freezingUser(String userId) {
        this.baseMapper.updateState(userId, AvailableStateEnum.FREEZING.getCode());
    }

    /**
     * 配合导出，使用用户名称模糊查询用户id
     *
     * @param userName
     * @return
     */
    @Override
    public List<AdminSmUserEntity> selectIdLikeName(String userName) {
        QueryWrapper<AdminSmUserEntity> wrapper = new QueryWrapper<>();
        wrapper.like("user_name", userName);
        return this.list(wrapper);
    }


    @Override
    public Page<AdminSmUserVo> getUsersForWf(AdminSmUserQuery query) {
        Page<AdminSmUserVo> page = query.getIPage();
        QueryWrapper<AdminSmUserVo> wrapper = new QueryWrapper<>();
        wrapper.eq("T0.USER_STS", "A");
        wrapper.eq(StringUtils.nonEmpty(query.getLoginCode()), "T0.LOGIN_CODE", query.getLoginCode());
        wrapper.like(StringUtils.nonEmpty(query.getUserName()), "T0.USER_NAME", query.getUserName());
        return this.baseMapper.getUsersForWf(page, wrapper);
    }

    @Override
    public List<AdminSmUserVo> getUsersByOrgForWf(String orgId) {
        return this.baseMapper.getUsersByOrgForWf(orgId);
    }

    @Override
    public List<AdminSmUserVo> getUsersByOrgAndDutyForWf(String orgId, String userId) {
        return this.baseMapper.getUsersByOrgAndDutyForWf(userId);
    }

    @Override
    public List<AdminSmUserVo> getUsersByDeptForWf(String deptId) {
        return this.baseMapper.getUsersByDeptForWf(deptId);
    }

    @Override
    public List<AdminSmUserVo> getUsersByDutyForWf(String dutyId) {
        return this.baseMapper.getUsersByDutyForWf(dutyId);
    }

    @Override
    public List<AdminSmUserVo> getUsersByRoleForWf(String roleId) {
        return this.baseMapper.getUsersByRoleForWf(roleId);
    }

    @Override
    public AdminSmUserVo getUserInfoForWf(String userId) {
        return this.baseMapper.getUserInfoForWf(userId);
    }

    /**
     * 获取用户姓名
     *
     * @param userId
     * @return
     */
    @Override
    public String getUserNameById(String userId) {
        QueryWrapper<AdminSmUserEntity> wrapper = new QueryWrapper<>();
        wrapper.select("USER_NAME").eq("USER_ID", userId);
        AdminSmUserEntity userEntity = this.getOne(wrapper);
        return userEntity.getUserName();
    }

    @Override
    public ResultDto<UserEntityVo> getByPhoneNumber(String userMobilephone) {
        //根据手机号查询用户信息
        AdminSmUserEntity userEntity = this.getOne(new QueryWrapper<AdminSmUserEntity>().eq("USER_MOBILEPHONE", userMobilephone));

        //封装返回数据
        if (Objects.nonNull(userEntity)) {

            userEntity.setUserPassword(null);
            UserEntityVo userEntityVoDb = new UserEntityVo();
            org.springframework.beans.BeanUtils.copyProperties(userEntity, userEntityVoDb);
            return ResultDto.success().code(ResponseAndMessageEnum.SUCCESS.getCode()).message(ResponseAndMessageEnum.SUCCESS.getMessage()).data(userEntityVoDb);

        } else {
            log.error("手机号不存在！");
            return ResultDto.error(ResponseAndMessageEnum.NON_PHONENUMBER.getCode(), ResponseAndMessageEnum.NON_PHONENUMBER.getMessage());
        }
    }

    /**
     * 通过登录账号查询用户
     *
     * @param loginCode 账号
     * @return
     */
    @Override
    public AdminSmUserDto getByLoginCode(String loginCode) {
        // 根据账号查询用户信息
        AdminSmUserEntity userEntity = this.getOne(new QueryWrapper<AdminSmUserEntity>().eq("LOGIN_CODE", loginCode));
        AdminSmUserDto adminSmUserDto = new AdminSmUserDto();
        // 封装返回数据
        if (Objects.nonNull(userEntity)) {
            adminSmUserDto = BeanUtils.beanCopy(userEntity, AdminSmUserDto.class);
        } else {
            log.error("账号不存在！");
        }
        return adminSmUserDto;
    }

    /**
     * 通过登录账号查询用户
     *
     * @param loginCode 账号
     * @return
     */
    @Override
    public AdminSmUserEntity getAllByLoginCode(String loginCode) {
        // 根据账号查询用户信息
        AdminSmUserEntity userEntity = this.getOne(new QueryWrapper<AdminSmUserEntity>().eq("LOGIN_CODE", loginCode));
        if (Objects.isNull(userEntity)) {
            log.error("账号不存在！");
        }
        return userEntity;
    }

    /**
     * 通过多条登录账号查询多个用户
     *
     * @param loginCodes 账号列表
     * @return
     */
    @Override
    public List<AdminSmUserDto> getByLoginCodeList(List<String> loginCodes) {
        //根据账号查询用户信息
        List<AdminSmUserEntity> userEntities = this.list(new QueryWrapper<AdminSmUserEntity>().in("LOGIN_CODE", loginCodes));
        List<AdminSmUserDto> adminSmUserDtoList = new ArrayList<>();
        //封装返回数据
        if (CollectionUtils.nonEmpty(userEntities)) {
            adminSmUserDtoList = (List<AdminSmUserDto>) BeanUtils.beansCopy(userEntities, AdminSmUserDto.class);
        } else {
            log.error("账号不存在！");
        }
        return adminSmUserDtoList;
    }

    /**
     * 通过姓名模糊查询多个用户
     *
     * @param name 姓名
     * @return
     */
    @Override
    public List<AdminSmUserDto> getByName(String name) {
        //根据账号查询用户信息
        List<AdminSmUserEntity> userEntities = this.list(new QueryWrapper<AdminSmUserEntity>().like("USER_NAME", name));
        List<AdminSmUserDto> adminSmUserDtoList = new ArrayList<>();
        //封装返回数据
        if (CollectionUtils.nonEmpty(userEntities)) {
            adminSmUserDtoList = (List<AdminSmUserDto>) BeanUtils.beansCopy(userEntities, AdminSmUserDto.class);
        } else {
            log.error("姓名不存在！");
        }
        return adminSmUserDtoList;
    }

    /**
     * 获取用户的 roleId 和 objId
     *
     * @param userId
     * @return
     */
    @Override
    public List<AdminSmReciveVo> selectRoleAndObj(String userId) {
        return this.baseMapper.selectRoleAndObj(userId);
    }

    /**
     * 用户密码修改
     *
     * @param password
     * @param loginCode
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultDto modifyPassword(String password, String loginCode) {
        //密码加密
        if (StringUtils.nonEmpty(password) && StringUtils.nonEmpty(loginCode)) {
            password = passwordUtils.enPassword(password);
            //更新用户密码
            AdminSmUserEntity userEntity = this.baseMapper.selectOne(new QueryWrapper<AdminSmUserEntity>().eq("LOGIN_CODE", loginCode));
            userEntity.setUserPassword(password);
            userEntity.setLastLoginTime(new Date());
            userEntity.setLastEditPassTime(new Date());
            this.baseMapper.updateById(userEntity);

            //新增密码历史记录
            PasswordLogDto passwordLogDto = new PasswordLogDto();
            passwordLogDto.setPwdUped(password);
            passwordLogDto.setUpdateUser(userEntity.getUserId());
            passwordLogDto.setUserId(userEntity.getUserId());

            passwordLogService.updatePwdLog(passwordLogDto);

            return ResultDto.success().code(MessageEnums.PASSWORD_COMPLEX_SUCCESS.getCode()).message(i18nMessageByCode.getMessageByCode(MessageEnums.PASSWORD_COMPLEX_SUCCESS.getCode()));
        } else {
            return ResultDto.error(ResponseAndMessageEnum.BAD_CREDENTIALS.getCode(), i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.BAD_CREDENTIALS.getCode()));
        }
    }

    /**
     * 密码重置
     *
     * @param passwordResetBo
     * @return R
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultDto resetPassword(PasswordResetBo passwordResetBo) {
        //密码加密
        if (StringUtils.nonEmpty(passwordResetBo.getLoginCode())) {
            String password = passwordUtils.enPassword(PasswordEnum.INITIAL_PASSWORD.getCode());
            //更新用户密码
            AdminSmUserEntity userEntity = this.baseMapper.selectOne(new QueryWrapper<AdminSmUserEntity>().eq("LOGIN_CODE", passwordResetBo.getLoginCode()));
            LambdaUpdateWrapper<AdminSmUserEntity> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.set(AdminSmUserEntity::getLastLoginTime, null);
            updateWrapper.eq(AdminSmUserEntity::getUserId, userEntity.getUserId());
            updateWrapper.set(AdminSmUserEntity::getUserPassword, password);
            updateWrapper.set(AdminSmUserEntity::getLastEditPassTime, new Date());
            updateWrapper.set(AdminSmUserEntity::getLastChgDt, new Date());
            updateWrapper.set(AdminSmUserEntity::getLastChgUsr, SessionUtils.getUserId());
            this.update(updateWrapper);

            //新增密码历史记录
            PasswordLogDto passwordLogDto = new PasswordLogDto();
            passwordLogDto.setPwdUped(password);
            passwordLogDto.setUpdateUser(userEntity.getUserId());
            passwordLogDto.setUserId(userEntity.getUserId());

            passwordLogService.updatePwdLog(passwordLogDto);

            return ResultDto.success();
        } else {
            return ResultDto.error(MessageEnums.PASSWORD_RESOLUTION_FAILED.getCode(), MessageEnums.PASSWORD_RESOLUTION_FAILED.getMessage());
        }
    }

    @Override
    public ResultDto forgetPasswordCheck(PasswordForgetAndResetCheckBo passwordForgetAndResetCheckBo) {
        //查询输入的账号是否存在
        AdminSmUserEntity userEntity = this.baseMapper.selectOne(new QueryWrapper<AdminSmUserEntity>()
                .eq("LOGIN_CODE", passwordForgetAndResetCheckBo.getLoginCode()));

        //如果无账号 || 账号绑定的手机号与输入的手机号不一致
        if (userEntity == null || !StringUtils.equals(userEntity.getUserMobilephone(),
                passwordForgetAndResetCheckBo.getMobile())) {
            return ResultDto.error(MessageEnums.PASSWORD_MOBILE_USER_CODE_FAILED.getCode(),
                    MessageEnums.PASSWORD_MOBILE_USER_CODE_FAILED.getMessage());
        }
        return ResultDto.success();
    }

    @Override
    public ResultDto modifyUserMobilePhone(String userMobilePhone, String loginCode) {
        //更新用户绑定的手机号
        AdminSmUserEntity userEntity = this.baseMapper.selectOne(new QueryWrapper<AdminSmUserEntity>().eq("LOGIN_CODE", loginCode));
        userEntity.setUserMobilephone(userMobilePhone);
        int result = this.baseMapper.updateById(userEntity);
        if (result != 0) {
            return ResultDto.success().code(MessageEnums.MOBILE_MODIFY_SUCCESSFUL.getCode())
                    .message(i18nMessageByCode.getMessageByCode(MessageEnums.MOBILE_MODIFY_SUCCESSFUL.getCode()));
        } else {
            return ResultDto.error(MessageEnums.MOBILE_MODIFY_FAILED.getCode(),
                    i18nMessageByCode.getMessageByCode(MessageEnums.MOBILE_MODIFY_FAILED.getCode()));
        }

    }

    @Override
    public List<UserSubscribeVo> selectUserSubscribeVoList() {
        List<UserSubscribeVo> list = this.baseMapper.selectUserSubscribeVoList();
        return list;
    }


    @Override
    public Set<String> findUserIdsByOrgId(String orgId) {
        Objects.requireNonNull(orgId);
        return this.lambdaQuery().eq(AdminSmUserEntity::getOrgId, orgId).select(AdminSmUserEntity::getUserId).list().stream().map(AdminSmUserEntity::getUserId).collect(Collectors.toSet());
    }

    @Override
    public void updateLoginTime(String userId, Date lastLoginTime) {
        this.baseMapper.updateLoginTime(userId, lastLoginTime);
    }


    /**
     * 根据authorization查询用户信息
     *
     * @param userId
     * @param clientId
     * @return
     */
    @Override
    public UserSessionVo getUserByAuthorization(String userId, String clientId) {
        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(clientId)) {
//            throw new BizException("userIdOrClientIdNull", null, null);
            //mxz
            throw BizException.error(null, "51100003", "用户账号或客户端编号不能为空");
        }

        UserSessionVo userSessionVo = new UserSessionVo();
        //根据jwttoken信息解密出的sysId查询用户逻辑系统信息
        AdminSmLogicSysEntity logicSysEntity = adminSmLogicSysService.getOne(new QueryWrapper<AdminSmLogicSysEntity>().eq("SYS_ID", clientId));
        if (Objects.nonNull(logicSysEntity)) {
            LogicSysSessionVo logicSysSessionVo = new LogicSysSessionVo();
            BeanUtils.beanCopy(logicSysEntity, logicSysSessionVo);
            userSessionVo.setLogicSys(logicSysSessionVo);
        }

        //根据jwttoken信息解密出的loginCode查询用户详细信息
        AdminSmUserEntity userEntity = this.getOne(new QueryWrapper<AdminSmUserEntity>().eq("USER_ID", userId));

        if (Objects.nonNull(userEntity)) {
            BeanUtils.beanCopy(userEntity, userSessionVo);
            //根据用户部门id查询部门详细信息
            AdminSmDptEntity dptEntity = adminSmDptService.getOne(new QueryWrapper<AdminSmDptEntity>().eq("DPT_ID", userEntity.getDptId()));

            if (Objects.nonNull(dptEntity)) {
                DptSessionVo dptSessionVo = new DptSessionVo();
                BeanUtils.beanCopy(dptEntity, dptSessionVo);
                userSessionVo.setDpt(dptSessionVo);

                //根据用户部门id查询上级部门部门详细信息
                if (StringUtils.nonEmpty(dptEntity.getUpDptId())) {
                    AdminSmDptEntity upDptEntity = adminSmDptService.getOne(new QueryWrapper<AdminSmDptEntity>().eq("DPT_ID", dptEntity.getUpDptId()));

                    if (Objects.nonNull(upDptEntity)) {
                        DptSessionVo upDptSessionVo = new DptSessionVo();
                        BeanUtils.beanCopy(upDptEntity, upDptSessionVo);
                        userSessionVo.setUpDpt(upDptSessionVo);
                    }
                }
            }

            //根据用户id查询用户角色
            AdminSmUserRoleRelEntity adminSmUserRoleRelServiceOne = adminSmUserRoleRelService.getOne(new QueryWrapper<AdminSmUserRoleRelEntity>().eq("USER_ID", userEntity.getUserId()));

            //根据用户角色Id查询用户角色详细信息
            if (Objects.nonNull(adminSmUserRoleRelServiceOne)) {
                List<AdminSmRoleEntity> roleEntity = adminSmRoleService.list(new QueryWrapper<AdminSmRoleEntity>().eq("ROLE_ID", adminSmUserRoleRelServiceOne.getRoleId()));
                List<RoleSessionVo> roles = new ArrayList<>();
                if (CollectionUtils.nonEmpty(roleEntity)) {
                    for (AdminSmRoleEntity roleEntity1 : roleEntity) {
                        RoleSessionVo role = new RoleSessionVo();
                        BeanUtils.beanCopy(roleEntity1, role);
                        roles.add(role);
                    }
                }
                userSessionVo.setRoles(roles);
            }

            //根据用户机构Id查询机构详细信息
            AdminSmOrgEntity orgEntity = adminSmOrgService.getOne(new QueryWrapper<AdminSmOrgEntity>().eq("ORG_ID", userEntity.getOrgId()));

            if (Objects.nonNull(orgEntity)) {
                OrgSessionVo org = new OrgSessionVo();
                BeanUtils.beanCopy(orgEntity, org);
                userSessionVo.setOrg(org);

                if (StringUtils.nonEmpty(orgEntity.getUpOrgId())) {
                    //根据机构id查询上级机构详细信息
                    AdminSmOrgEntity upOrgEntity = adminSmOrgService.getOne(new QueryWrapper<AdminSmOrgEntity>().eq("ORG_ID", orgEntity.getUpOrgId()));

                    if (Objects.nonNull(upOrgEntity)) {
                        OrgSessionVo upOrgSessionVo = new OrgSessionVo();
                        BeanUtils.beanCopy(upOrgEntity, upOrgSessionVo);
                        userSessionVo.setUpOrg(upOrgSessionVo);
                    }
                }

                if (StringUtils.nonEmpty(orgEntity.getOrgLevel().toString())) {
                    userSessionVo.setOrgLevel(orgEntity.getOrgLevel().toString());
                }

                //根据金融机构ID查询金融机构信息
                if (StringUtils.nonEmpty(orgEntity.getInstuId())) {
                    AdminSmInstuEntity instuEntity = adminSmInstuService.getOne(new QueryWrapper<AdminSmInstuEntity>().eq("INSTU_ID", orgEntity.getInstuId()));

                    if (Objects.nonNull(instuEntity)) {
                        InstuSessionVo instuSessionVo = new InstuSessionVo();
                        BeanUtils.beanCopy(instuEntity, instuSessionVo);
                        userSessionVo.setInstuOrg(instuSessionVo);
                    }
                }
            }

        }
        return userSessionVo;
    }

    /**
     * 查询是否小微客户经理 Y 是;N 否
     *
     * @param loginCode
     * @return
     */
    @Override
    public GetIsXwUserDto getIsXWUserByLoginCode(String loginCode) {
        GetIsXwUserDto getIsXwUserDto = new GetIsXwUserDto();
        try {
            getIsXwUserDto = new GetIsXwUserDto();
            String isXWUser = null;
            int num = this.baseMapper.getIsXWUserByLoginCode(loginCode);
            if (num > 0) {
                isXWUser = OcaTradeCommonConstance.COMMON_YES_NO_Y;
            } else {
                isXWUser = OcaTradeCommonConstance.COMMON_YES_NO_N;
            }
            getIsXwUserDto.setIsXWUser(isXWUser);
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return getIsXwUserDto;
    }

    /**
     * 根据机构号查询用户信息
     *
     * @param orgId
     * @return
     */
    @Override
    public List<AdminSmUserDto> getByOrgid(String orgId) {
        //根据账号查询用户信息
        List<AdminSmUserEntity> userEntities = this.list(new QueryWrapper<AdminSmUserEntity>().in("ORG_ID", orgId));
        List<AdminSmUserDto> adminSmUserDtoList = new ArrayList<>();
        //封装返回数据
        if (CollectionUtils.nonEmpty(userEntities)) {
            adminSmUserDtoList = (List<AdminSmUserDto>) BeanUtils.beansCopy(userEntities, AdminSmUserDto.class);
        } else {
            log.error("账号不存在！");
        }
        return adminSmUserDtoList;
    }

    /**
     * 查询用户和岗位信息
     *
     * @param userAndDutyReqDto
     * @return
     */
    @Override
    public List<UserAndDutyRespDto> getUserAndDuty(UserAndDutyReqDto userAndDutyReqDto) {
        return this.baseMapper.getUserAndDuty(userAndDutyReqDto);
    }

    /**
     * 通用客户经理查询
     *
     * @param commonUserQueryReqDto
     * @return
     */
    @Override
    public List<CommonUserQueryRespDto> getCommonUserInfo(CommonUserQueryReqDto commonUserQueryReqDto) {
        return this.baseMapper.getCommonUserInfo(commonUserQueryReqDto);
    }

    /**
     * 通过流程实例号获取当前处理用户的详细信息
     *
     * @param instanceId 账号
     * @return
     */
    @Override
    public AdminSmUserDto getTodoUserInfo(String instanceId) {
        AdminSmUserEntity userEntity = this.baseMapper.getTodoUserInfo(instanceId);
        AdminSmUserDto adminSmUserDto = new AdminSmUserDto();
        // 封装返回数据
        if (Objects.nonNull(userEntity)) {
            adminSmUserDto = BeanUtils.beanCopy(userEntity, AdminSmUserDto.class);
        } else {
            log.error("账号不存在！");
            return null;
        }
        return adminSmUserDto;
    }

    /**
     * 查询在途任务数量
     *
     * @param cusId
     * @return
     */
    @Override
    public int queryCusFlowCount(String cusId) {
        return this.baseMapper.queryCusFlowCount(cusId);
    }

    @Override
    public List<AdminSmUserDto> getUserList(GetUserInfoByOrgCodeDto getUserInfoByOrgCodeDto) {
        //角色编号
        String orgCode = getUserInfoByOrgCodeDto.getOrgCode();
        //页码
        Integer pageNum = Optional.ofNullable(getUserInfoByOrgCodeDto.getPageNum()).orElse(1);
        //条数
        Integer pageSize = Optional.ofNullable(getUserInfoByOrgCodeDto.getPageSize()).orElse(10);
        IPage<AdminSmUserDto> iPage = new Page<>(pageNum, pageSize);
        iPage = this.baseMapper.getUserInfoByOrgCode(iPage, orgCode);
        return iPage.getRecords();
    }

    /**
     * 查询所有小微客户经理
     */
    @Override
    public List<AdminSmUserEntity> getAllXwUserList() {
        QueryWrapper<AdminSmUserEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("belg_main_org_id", Constants.SystemUserConstance.ORG_CODE_XW_BELG_MAIN);
        return this.list(wrapper);
    }

    /**
     * 集中作业总行本地机构（除小微）下的客户经理
     *
     * @return
     */
    @Override
    public List<AdminSmUserEntity> getJzzyUserList() {
        return this.baseMapper.getJzzyUserList(Constants.SystemUserConstance.ORG_CODE_XW_BELG_MAIN, org.apache.commons.lang.StringUtils.join(Constants.SystemUserConstance.ORG_TYPES_ZH_LOCAL, ","), org.apache.commons.lang.StringUtils.join(Constants.SystemUserConstance.ORG_CODES_JZZY_CENTER));
    }

    /**
     * 查询机构下小微客户经理
     *
     * @author jijian_yx
     * @date 2021/11/6 20:07
     **/
    @Override
    public List<AdminSmUserEntity> getXwUserForOrg(List<String> orgId) {
        QueryWrapper<AdminSmUserEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("belg_main_org_id", Constants.SystemUserConstance.ORG_CODE_XW_BELG_MAIN);
        wrapper.in("org_id", orgId);
        return this.list(wrapper);
    }

    /**
     * 查询机构下非小微用户
     *
     * @author jijian_yx
     * @date 2021/11/6 20:07
     **/
    @Override
    public List<AdminSmUserEntity> getNotXwUserForOrg(List<String> orgId) {
        List<String> belgMainOrg = new ArrayList<>();
        belgMainOrg.add(Constants.SystemUserConstance.ORG_CODE_XW_BELG_MAIN);
        QueryWrapper<AdminSmUserEntity> wrapper = new QueryWrapper<>();
        wrapper.notIn("belg_main_org_id", belgMainOrg);
        wrapper.in("org_id", orgId);
        return this.list(wrapper);
    }
}