package cn.com.yusys.yusp.oca.dao;

import cn.com.yusys.yusp.dto.server.xdxt0014.req.Xdxt0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0014.resp.List;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmLookupDictEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据字典内容表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2021-01-15 16:44:33
 */

public interface AdminSmLookupDictDao extends BaseMapper<AdminSmLookupDictEntity> {
    /**
     * 字典项对象通用列表查询
     * @param xdxt0014DataReqDto
     * @return
     */
    java.util.List<List> getXdxt0014(Xdxt0014DataReqDto xdxt0014DataReqDto);
}
