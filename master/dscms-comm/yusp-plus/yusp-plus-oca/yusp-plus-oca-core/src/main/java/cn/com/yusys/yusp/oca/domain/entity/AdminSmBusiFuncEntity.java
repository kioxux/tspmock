package cn.com.yusys.yusp.oca.domain.entity;

import cn.com.yusys.yusp.oca.domain.vo.AdminSmContrTreeVo;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统业务功能表
 * 
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-20 13:43:51
 */
@Data
@TableName("admin_sm_busi_func")
public class AdminSmBusiFuncEntity extends AdminSmContrTreeVo implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 记录编号
	 */
	@TableId
	private String funcId;
	/**
	 * 所属功能模块编号
	 */
	private String modId;
	/**
	 * 功能点名称
	 */
	private String funcName;
	/**
	 * 功能点描述
	 */
	private String funcDesc;
	/**
	 * URL链接
	 */
	private String funcUrl;
	/**
	 * JS链接
	 */
	private String funcUrlJs;
	/**
	 * CSS链接
	 */
	private String funcUrlCss;
	/**
	 * 顺序
	 */
	private Integer funcOrder;
	/**
	 * 图标
	 */
	private String funcIcon;
	/**
	 * 最新变更用户
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
	/**
	 * 最新变更时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

}
