package cn.com.yusys.yusp.oca.domain.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统功能模块表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-26 10:50:57
 */
@Data
public class AdminSmFuncModVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 记录编号
     */
    private String modId;
    /**
     * 模块名称
     */
    private String modName;
    /**
     * 是否外部系统
     */
    private String isOuter;
    /**
     * 是否APP功能
     */
    private String isApp;
    /**
     * 最新变更时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

    private String lastChgName;
    /**
     * 添加模块描述
     */
    private String modDesc;

}
