package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 公安信息表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-12-02 18:03:52
 */
@Data
@TableName("cms_public_security_info")
public class PublicSecurityInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 公安信息id
	 */
	@TableId
	private String publicSecurityInfo;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 证件类型(10110:居民身份证,10119:组织机构代码,20150:护照,99099:其他证件)
	 */
	private String certType;
	/**
	 * 证件号码
	 */
	private String certCode;
	/**
	 * 状态 1：正常 2：在逃人员 3：注销
	 */
	private String status;
	/**
	 * 录入时间
	 */
	private Date inputTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 公安信息显示状态(1-逻辑有效，0-逻辑删除)
	 */
	private String logicStatus;

}
