package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.dto.GetUserInfoByRoleCodeDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.oca.config.validation.Insert;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmOrgTreeNodeBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmRoleEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserRoleRelEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmPasteRoleQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmRoleQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmRoleUserRelQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmUserQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmRoleDetailVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmRoleVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmUserVo;
import cn.com.yusys.yusp.oca.domain.vo.UserRelationshipVo;
import cn.com.yusys.yusp.oca.service.AdminSmRoleService;
import cn.com.yusys.yusp.oca.service.AdminSmUserRoleRelService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * 系统角色表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-11-18 18:06:35
 */
@RestController
@RequestMapping("/api/adminsmrole")
@Slf4j
public class AdminSmRoleController {
    @Autowired
    private AdminSmRoleService adminSmRoleService;

    @Autowired
    private AdminSmUserRoleRelService adminSmUserRoleRelService;


    /**
     * 列表
     */
    @GetMapping("/page")
    public ResultDto<AdminSmRoleVo> page(AdminSmRoleQuery query){
        return ResultDto.success(adminSmRoleService.queryPage(query));
    }

    /**
     * 信息
     */
    @GetMapping("/info/{roleId}")
    public ResultDto<AdminSmRoleDetailVo> info(@PathVariable("roleId") String roleId) {
        return ResultDto.success(adminSmRoleService.getDetailById(roleId));
    }

    /**
     * 保存
     */
    @PostMapping("/add")
    public ResultDto<Objects> add(@RequestBody  @Validated({Insert.class}) AdminSmRoleEntity entity) {
        adminSmRoleService.save(entity);
        return ResultDto.successMessage("新增成功");
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public ResultDto<Objects> update(@RequestBody AdminSmRoleEntity entity) {
        adminSmRoleService.updateById(entity);
        return ResultDto.success();
    }

    /**
     * 批量启用角色
     */
    @PostMapping("/batchenable")
    public ResultDto<Object> batchEnable(@RequestBody @NotNull String[] ids) {
        adminSmRoleService.batchEnable(ids);
        return ResultDto.successMessage("更新成功");
    }

    /**
     * 批量停用角色
     */
    @PostMapping("/batchdisable")
    public ResultDto<Object> batchDisable(@RequestBody @NotNull String[] ids) {
        adminSmRoleService.batchDisable(ids);
        return ResultDto.successMessage("更新成功");
    }

    @PostMapping("/batchdelete")
    public ResultDto<Object> batchDelete(@RequestBody @NotNull String[] ids){
        adminSmRoleService.batchDelete(ids);
        return ResultDto.successMessage("更新成功");
    }

    /**
     *
     * @方法名称: orgTreeQuery
     * @方法描述:  机构树查询
     * @参数与返回说明:
     *  - orgId：默认值是当前用户所在机构
     *  - orgSts：为null或空字串时查询所有状态的机构，有值时只返回对应状态的机构树
     *  - sort：默认值是last_chg_dt desc
     * @算法描述: 从数据库中查出全量机构数据，通过代码去组织树形数据结构，从输入orgId算起最多返回5级（）
     */
    @GetMapping("/orgtree")
    public ResultDto<AdminSmOrgTreeNodeBo> orgTreeQuery(@RequestParam String roleId){
        log.info("Query organization tree in role page");
        List<AdminSmOrgTreeNodeBo> list = adminSmUserRoleRelService.getOrgTree(roleId);
        return ResultDto.success(list).total(list.size());
    }

    /**
     * 查询角色下的用户
     */
    @GetMapping("/userlist")
    public ResultDto<UserRelationshipVo> userList(@Validated AdminSmRoleUserRelQuery query){
        Page<UserRelationshipVo> userVoList = adminSmUserRoleRelService.memberPage(query);
        return ResultDto.success(userVoList);
    }

    /**
     * 给角色批量新增关联用户
     * @param roleId 要关联的角色id
     * @param ids 要与之关联的用户id列表
     * @return
     */
    @PostMapping("/adduserrolerel/{roleId}")
    public ResultDto<Objects> addRel(@PathVariable(name="roleId") String roleId,@RequestBody @NotNull String[] ids){
        List<String> idList= Arrays.asList(ids);
        List<AdminSmUserRoleRelEntity> entityList=idList.stream().map(userId->new AdminSmUserRoleRelEntity(userId,roleId)).collect(Collectors.toList());
        adminSmUserRoleRelService.save(entityList);
        return ResultDto.successMessage("关联关系新增成功");
    }

    /**
     * 给角色批量移除关联用户
     * @param roleId 目标角色id
     * @param ids 与之解除关联关系的用户id列表
     * @return
     */
    @PostMapping("/removeuserrolerel/{roleId}")
    public ResultDto<Objects> removeRel(@PathVariable(name="roleId") String roleId,@RequestBody @NotNull String[] ids){
        List<String> idList= Arrays.asList(ids);
        List<AdminSmUserRoleRelEntity> entityList=idList.stream().map(userId->new AdminSmUserRoleRelEntity(userId,roleId)).collect(Collectors.toList());
        adminSmUserRoleRelService.remove(entityList);
        return ResultDto.successMessage("解除关联关系成功");
    }


    /**
     * 功能授权 粘贴用
     * 分页查询当前用户有权访问的所有角色列表,排除指定roleId，按最后修改时间降序
     */
    @GetMapping("/waitpasterolepage")
    public ResultDto<AdminSmUserVo> page(@Validated AdminSmPasteRoleQuery query) {
        return ResultDto.success(adminSmRoleService.queryPageExcept(query));
    }

    /**
     * 查询角色下的用户-提供给后端
     */
    @PostMapping("/getuserlist")
    public ResultDto<List<AdminSmUserDto>> getUserList(@RequestBody GetUserInfoByRoleCodeDto getUserInfoByRoleCodeDto){
        List<AdminSmUserDto> adminSmUserDtos = adminSmRoleService.getUserInfoByRoleCode(getUserInfoByRoleCodeDto);
        return ResultDto.success(adminSmUserDtos);
    }

    /**
     * 根据机构查询角色下的用户-提供给后端
     */
    @PostMapping("/getuserbyorgrole")
    public ResultDto<AdminSmUserVo> getuserbyorgrole(@RequestBody AdminSmUserQuery query){
        return ResultDto.success(adminSmRoleService.getUserInfoByOrgRoleCode(query));
    }
    /**
     * 根据userId查询关联的角色信息，若为
     * 小微客户经理  R0010
     * 	客户经理（东海）RDH01
     *     综合客户经理 R0020
     *     零售客户经理 R0030
     *     小企业客户经理 R0050
     *     客户经理（寿光）RSG01
     * 	投资经理 R0100则进行短信登录验证
     */
    @PostMapping("/findUserRoleRelsByUser")
    public boolean findUserRoleRelsByUser(@RequestBody AdminSmUserEntity user){
        List<AdminSmUserRoleRelEntity> list = adminSmUserRoleRelService.findUserRoleRelsByUser(user);
        List<String> role = new ArrayList<>();
        role.add("R0010");
        role.add("RDH01");
        role.add("R0020");
        role.add("R0030");
        role.add("R0050");
        role.add("RSG01");
        role.add("R0100");
        List<AdminSmUserRoleRelEntity> userRoleRelsByUser = adminSmUserRoleRelService.findUserRoleRelsByUser(user);
        List<String> roleIds = userRoleRelsByUser.stream().map(AdminSmUserRoleRelEntity::getRoleId).collect(Collectors.toList());
        role.retainAll(roleIds);
        if(role.size()>0){
            return true;
        }else{
            return false;
        }
    }
}
