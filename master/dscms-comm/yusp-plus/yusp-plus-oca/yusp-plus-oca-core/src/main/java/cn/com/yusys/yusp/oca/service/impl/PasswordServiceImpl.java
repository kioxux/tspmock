package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxRespDto;
import cn.com.yusys.yusp.oca.domain.bo.*;
import cn.com.yusys.yusp.oca.domain.constants.MessageEnums;
import cn.com.yusys.yusp.oca.domain.constants.PasswordEnum;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import cn.com.yusys.yusp.oca.service.PasswordService;
import cn.com.yusys.yusp.oca.utils.I18nMessageByCode;
import cn.com.yusys.yusp.oca.utils.PasswordUtils;
import cn.com.yusys.yusp.service.Dscms2DxptClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @program: yusp-plus
 * @description: 密码相关实现类
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2020-12-14 11:34
 */
@Service
@Slf4j
public class PasswordServiceImpl implements PasswordService {

    @Autowired
    I18nMessageByCode i18nMessageByCode;
    @Autowired
    private PasswordUtils passwordUtils;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private Dscms2DxptClientService dscms2DxptClientService;

    // redis密码重置手机号
    private String PASSWORD_RESET_MOBILE_CODE = "PASSWORD_RESET_MOBILE_CODE";

    //验证码在redis中存在时间
    private Integer CODE_EXIST_TIME = 120;

    /**
     * 密码验证
     *
     * @param passwordCheckBo
     * @return 密码验证码
     */
    @Override
    public ResultDto checkPasswrod(PasswordCheckBo passwordCheckBo) {
        passwordUtils.dePassword(passwordCheckBo.getPwd());
        return ResultDto.success().code(MessageEnums.PASSWORD_COMPLEX_SUCCESS.getCode()).message(i18nMessageByCode.getMessageByCode(MessageEnums.PASSWORD_COMPLEX_SUCCESS.getCode()));
    }

    /**
     * 密码修改
     *
     * @param passwordModifyBo
     * @return 密码修改码
     */
    @Override
    public ResultDto passwordModification(PasswordModifyBo passwordModifyBo) {

        //首次登录修改密码时有loginCode,修改密码时无loginCode
        if (StringUtils.isEmpty(passwordModifyBo.getLoginCode())) {
            passwordModifyBo.setLoginCode(SessionUtils.getLoginCode());
        }

        //匹配登录密码
        boolean isMatchUserPassword;
        isMatchUserPassword = passwordUtils.matchUserPassword(passwordModifyBo.getRawPassword(), passwordModifyBo.getLoginCode());
        if (!isMatchUserPassword) {
            return ResultDto.error(MessageEnums.PASSWORD_MISMATCH.getCode(), i18nMessageByCode.getMessageByCode(MessageEnums.PASSWORD_MISMATCH.getCode()));
        }
        //密码修改
        ResultDto r = new ResultDto();
        if (passwordModifyBo.getPassword() != null && passwordModifyBo.getLoginCode() != null) {
            //密码修改
            r = adminSmUserService.modifyPassword(passwordModifyBo.getPassword(), passwordModifyBo.getLoginCode());
        }
        if (r.getCode() == MessageEnums.PASSWORD_COMPLEX_SUCCESS.getCode()) {
            return ResultDto.success().code(PasswordEnum.SUCCESSFULLY_CHECKED.getCode()).message(i18nMessageByCode.getMessageByCode(PasswordEnum.SUCCESSFULLY_CHECKED.getCode()));
        } else {
            return ResultDto.error(MessageEnums.PASSWORD_MODIFY_FAILED.getCode(), i18nMessageByCode.getMessageByCode(MessageEnums.PASSWORD_MODIFY_FAILED.getCode()));
        }
    }

    /**
     * 密码重置
     *
     * @param
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultDto resetPassword(PasswordResetBo passwordResetBo) {

        //删除缓存中因为密码输入错误次数过多，而禁止登录的数据
        String loginCodeCountCacheKey = String.format("loginErrorCount:loginCode_%s", passwordResetBo.getLoginCode());
        //判断是否存在key,有--删除
        Boolean hasKey = stringRedisTemplate.hasKey(loginCodeCountCacheKey);
        if (hasKey) {
            stringRedisTemplate.delete(loginCodeCountCacheKey);
        }
        return adminSmUserService.resetPassword(passwordResetBo);
    }

    /**
     * 忘记密码参数校验并发送手机验证码
     * @param passwordForgetAndResetCheckBo
     * @return
     */
    @Override
    public ResultDto sendVerificationCode(PasswordForgetAndResetCheckBo passwordForgetAndResetCheckBo) {
        // 验证账号和手机号
        ResultDto resultDto = adminSmUserService.forgetPasswordCheck(passwordForgetAndResetCheckBo);

        String code = stringRedisTemplate.opsForValue().get(PASSWORD_RESET_MOBILE_CODE + "-" +passwordForgetAndResetCheckBo.getMobile());
        if(code != null) {
            return ResultDto.error("验证码已发送，请勿重复发送！");
        }

        // 验证通过，发送手机验证码
        if("0".equals(resultDto.getCode())) {
            // 发送手机验证码
            String verifycode = generateVerificationCode();
            String msgContent = "本次验证码是：" + verifycode + "，用于登录/修改密码，2分钟内有效，请勿泄露，谨防被骗！";
            SenddxReqDto senddxReqDto = new SenddxReqDto();
            senddxReqDto.setInfopt("dx");
            SenddxReqList senddxReqList = new SenddxReqList();
            senddxReqList.setMobile(passwordForgetAndResetCheckBo.getMobile());
            senddxReqList.setSmstxt(msgContent);
            ArrayList<SenddxReqList> list = new ArrayList<>();
            list.add(senddxReqList);
            senddxReqDto.setSenddxReqList(list);

            // 调用BSP服务接口，发送消息至短信平台
            try{
                ResultDto<SenddxRespDto> result = dscms2DxptClientService.senddx(senddxReqDto);
                if("0".equals(result.getCode())){
                    log.info("发送手机验证码成功: 短信消息发送成功, 接收账号: {}, 消息内容: {}",
                            passwordForgetAndResetCheckBo.getLoginCode(),
                            msgContent);
                }else{
                    log.error("发送手机验证码失败: 短信消息发送失败, {}", result.getMessage());
                    return ResultDto.error(MessageEnums.MESSAGE_SEND_FAILED.getCode(),
                            MessageEnums.MESSAGE_SEND_FAILED.getMessage());
                }
            }catch (Exception e){
                log.error("发送手机验证码失败: 短信消息发送失败, ", e);
                ResultDto.error(MessageEnums.MESSAGE_SEND_FAILED.getCode(),
                        MessageEnums.MESSAGE_SEND_FAILED.getMessage());
            }
            // 验证码发送成功，存入redis

            stringRedisTemplate.opsForValue().set(PASSWORD_RESET_MOBILE_CODE + "-"
                            + passwordForgetAndResetCheckBo.getMobile(),
                    verifycode, CODE_EXIST_TIME, TimeUnit.SECONDS);

            return ResultDto.success();
        }else {
            return resultDto;
        }
    }

    @Override
    public ResultDto checkVerificationCode(PasswordForgetAndResetCheckBo passwordForgetAndResetCheckBo) {
        if(passwordForgetAndResetCheckBo.getVarificationCode() == null) {
            return ResultDto.error("验证码不能为空！");
        }

        // 验证账号和手机号
        ResultDto resultDto = adminSmUserService.forgetPasswordCheck(passwordForgetAndResetCheckBo);

        //获取缓存中的验证码
        if("0".equals(resultDto.getCode())) {
            String verificationCode = stringRedisTemplate.opsForValue().get(PASSWORD_RESET_MOBILE_CODE + "-"
                    + passwordForgetAndResetCheckBo.getMobile());
            if(passwordForgetAndResetCheckBo.getVarificationCode().equals(verificationCode)) {
                return ResultDto.success();
            }else {
                return ResultDto.error("验证码错误或验证码已过期");
            }
        }else {
            return resultDto;
        }
    }

    /**
     * 忘记密码重置密码
     * @param passwordForgetAndResetBo
     * @return
     */
    @Override
    public ResultDto forgetAndResetPassword(PasswordForgetAndResetBo passwordForgetAndResetBo) {
        ResultDto resultDto = adminSmUserService.modifyPassword(passwordForgetAndResetBo.getNewPassword(),
                passwordForgetAndResetBo.getLoginCode());
        if (resultDto.getCode() == MessageEnums.PASSWORD_COMPLEX_SUCCESS.getCode()) {
            return ResultDto.success()
                    .code(PasswordEnum.SUCCESSFULLY_CHECKED.getCode())
                    .message(i18nMessageByCode.getMessageByCode(PasswordEnum.SUCCESSFULLY_CHECKED.getCode()));
        } else {
            return ResultDto.error(MessageEnums.PASSWORD_MODIFY_FAILED.getCode(),
                    i18nMessageByCode.getMessageByCode(MessageEnums.PASSWORD_MODIFY_FAILED.getCode()));
        }
    }

    private String generateVerificationCode() {
        String codes = "0123456789";
        Random random =  new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < 6; i++) {
            int index = random.nextInt(codes.length());
            sb.append(codes.charAt(index));
        }
        return sb.toString();
    }

    @Override
    public ResultDto mobileBinding(MobileBindingBo mobileBindingBo) {
        //匹配登录密码
        boolean isMatchUserPassword;
        isMatchUserPassword = passwordUtils.matchUserPassword(mobileBindingBo.getOldPassword(),
                mobileBindingBo.getLoginCode());
        if (!isMatchUserPassword) {
            return ResultDto.error(MessageEnums.PASSWORD_MISMATCH.getCode(),
                    i18nMessageByCode.getMessageByCode(MessageEnums.PASSWORD_MISMATCH.getCode()));
        }

        //绑定手机号
        ResultDto r = adminSmUserService.modifyUserMobilePhone(mobileBindingBo.getUserMobilePhone(),
                mobileBindingBo.getLoginCode());
        if (r.getCode() == MessageEnums.MOBILE_MODIFY_SUCCESSFUL.getCode()) {
            return ResultDto.success().code(PasswordEnum.SUCCESSFULLY_CHECKED.getCode()).message(i18nMessageByCode.getMessageByCode(PasswordEnum.SUCCESSFULLY_CHECKED.getCode()));
        } else {
            return ResultDto.error(MessageEnums.PASSWORD_MODIFY_FAILED.getCode(), i18nMessageByCode.getMessageByCode(MessageEnums.PASSWORD_MODIFY_FAILED.getCode()));
        }

//        return adminSmUserService.modifyUserMobilePhone(mobileBindingBo.getUserMobilePhone(),
//                mobileBindingBo.getLoginCode());
    }
}
