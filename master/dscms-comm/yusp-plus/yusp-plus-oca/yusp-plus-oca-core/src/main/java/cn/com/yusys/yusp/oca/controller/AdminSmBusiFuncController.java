package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmBusiFuncBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmBusiFuncEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmBusiFuncQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmBusiFuncVo;
import cn.com.yusys.yusp.oca.service.AdminSmBusiFuncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;


/**
 * 系统业务功能表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-26 14:46:26
 */
@RestController
@RequestMapping("/api/adminsmbusifunc")
public class AdminSmBusiFuncController {

    @Autowired
    private AdminSmBusiFuncService adminSmBusiFuncService;

    /**
     * 业务功能信息查询
     */
    @GetMapping("/queryfunc")
    public ResultDto<AdminSmBusiFuncVo> list(AdminSmBusiFuncQuery adminSmBusiFuncQuery) {
        return ResultDto.success(adminSmBusiFuncService.queryPageWithCondition(adminSmBusiFuncQuery));
    }


    /**
     * 信息
     * 此接口暂不使用
     */
    @GetMapping("/info/{funcId}")
    public ResultDto<AdminSmBusiFuncEntity> info(@PathVariable("funcId") String funcId) {
        AdminSmBusiFuncEntity adminSmBusiFunc = adminSmBusiFuncService.getById(funcId);
        return ResultDto.success(adminSmBusiFunc);
    }

    /**
     * 新增业务功能信息
     */
    @PostMapping("/createfunc")
    public ResultDto<Object> save(@RequestBody @Validated AdminSmBusiFuncBo adminSmBusiFuncBo) {
        int resultInt = adminSmBusiFuncService.saveBusiFuncByBo(adminSmBusiFuncBo);
        if (resultInt >= 1) {
            return ResultDto.success().message("新增" + adminSmBusiFuncBo.getFuncName() + "成功!");
        }
        if (resultInt == -1) {
            return ResultDto.error("业务功能：" + adminSmBusiFuncBo.getFuncName() + "已存在!");
        }
        return ResultDto.error("data", "新增" + adminSmBusiFuncBo.getFuncName() + "失败!");
    }

    /**
     * 修改业务功能信息
     */
    @PostMapping("/editfunc")
    public ResultDto<Object> update(@RequestBody @Validated AdminSmBusiFuncBo adminSmBusiFuncBo) {
        //bean copy
        AdminSmBusiFuncEntity adminSmBusiFuncEntity = new AdminSmBusiFuncEntity();
        BeanUtils.beanCopy(adminSmBusiFuncBo, adminSmBusiFuncEntity);
        boolean updateBoolean = adminSmBusiFuncService.updateById(adminSmBusiFuncEntity);
        if (updateBoolean) {
            return ResultDto.success().message("修改" + adminSmBusiFuncEntity.getFuncName() + "模块成功!");
        }
        return ResultDto.error("修改" + adminSmBusiFuncEntity.getFuncName() + "模块失败!");
    }

    /**
     * 删除业务功能
     */
    @PostMapping("/deletefunc")
    public ResultDto<Object> delete(@RequestBody @NotNull String[] ids) {
        adminSmBusiFuncService.removeFuncByIds(ids);
        return ResultDto.success();
    }

}
