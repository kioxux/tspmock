package cn.com.yusys.yusp.notice.form;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * 该类包含了四张表中的所有主要字段
 */
@Data
public class AdminSmNoticeAllForm {

    private String noticeId;

    private String activeDate;

    private String creatorId;

    private String creatorName;

    private String creatorTime;

    private String isTop;

    private String noticeContent;

    private String noticeLevel;

    private String noticeTitle;

    private String pubOrgId;

    private String pubOrgName;

    private String pubSts;

    private String pubTime;

    private String pubUserId;

    private String pubUserName;

    private String topActiveDate;

    private String reciveOrgId;

    private String reciveOrgName;

    private String reciveRoleId;

    private String reciveRoleName;

    private String reciveType;

    private String content;

    private String reciveId;

    private String richeditId;

    private String readUserId;

}
