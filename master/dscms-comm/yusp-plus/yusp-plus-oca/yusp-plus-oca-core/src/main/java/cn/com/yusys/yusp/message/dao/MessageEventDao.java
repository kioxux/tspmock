package cn.com.yusys.yusp.message.dao;

import cn.com.yusys.yusp.message.entity.MessageEventEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 消息事件
 *
 * @author xiaodg@yusys.com.cn
 */

public interface MessageEventDao extends BaseMapper<MessageEventEntity> {

}
