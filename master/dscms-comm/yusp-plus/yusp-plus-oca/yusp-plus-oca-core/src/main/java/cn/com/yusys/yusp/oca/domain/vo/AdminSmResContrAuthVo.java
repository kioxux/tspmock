package cn.com.yusys.yusp.oca.domain.vo;

import lombok.Data;

@Data
public class AdminSmResContrAuthVo {

    private String resContrId;

    private String upMenuId;

    private String menuPath;

    private AdminTmplAndRecoVo tmplAndRecoVo;
}
