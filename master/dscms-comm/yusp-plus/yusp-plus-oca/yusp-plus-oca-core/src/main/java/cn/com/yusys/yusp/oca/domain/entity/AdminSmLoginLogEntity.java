package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @description: 登录记录表
 * @author: zhangsong
 * @date: 2021/4/1
 */
@Data
@TableName("admin_sm_login_log")
@NoArgsConstructor
@AllArgsConstructor
public class AdminSmLoginLogEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 记录编号
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String logId;
    /**
     * 交易全局流水
     */
    private String tradeId;
    /**
     * 用户id
     */
    private String loginCode;
    /**
     * 渠道编号
     */
    private String chnlNo;
    /**
     * 客户端IP
     */
    private String ipAddress;
    /**
     * MAC地址/系统唯一标识
     */
    private String deviceId;
    /**
     * 交易码/服务名，可以是restful的URL
     */
    private String tradeCode;
    /**
     * 操作结果。0：成功，1：失败
     */
    private String operResult;
    /**
     * 记录失败原因详情
     */
    private String operDetail;
    /**
     * 操作日期
     */
    private Date operDate;
    /**
     * 操作时间
     */
    private Timestamp operTime;

}
