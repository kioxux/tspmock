package cn.com.yusys.yusp.oca.domain.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

/**
 * 系统金融机构表拓展实体
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-12-2 18:06:35
 */
@Data
public class InstuExtVo {
    private String instuId;
    private String sysId;
    private String instuCde;
    private String instuName;
    private String joinDt;
    private String instuAddr;
    private String zipCde;
    private String contTel;
    private String contUsr;
    private String instuSts;
    private String lastChgUsr;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;
    private String userName;

}
