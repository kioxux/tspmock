package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.oca.dao.AdminSmUserMgrOrgDao;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserMgrOrgEntity;
import cn.com.yusys.yusp.oca.service.AdminSmUserMgrOrgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author terry
 * @email tanrui1@yusys.com.cn
 */
@Service("adminSmUserMgrOrgService")
public class AdminSmUserMgrOrgServiceImpl extends ServiceImpl<AdminSmUserMgrOrgDao, AdminSmUserMgrOrgEntity> implements AdminSmUserMgrOrgService {
    @Override
    public List<AdminSmUserMgrOrgEntity> findOrgRelsByUser(AdminSmUserEntity user) {
        Objects.requireNonNull(user.getUserId());
        return this.lambdaQuery().eq(AdminSmUserMgrOrgEntity::getUserId, user.getUserId()).list();
    }

}