package cn.com.yusys.yusp.web.server.xdxt0008;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.OcaTradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxt0008.req.Xdxt0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0008.resp.Xdxt0008DataRespDto;
import cn.com.yusys.yusp.enums.OcaTradeEnum;
import cn.com.yusys.yusp.service.server.xdxt0008.Xdxt0008Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据客户经理号查询账务机构号
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0008:根据客户经理号查询账务机构号")
@RestController
@RequestMapping("/api/ocaxt4bsp")
public class OcaXdxt0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(OcaXdxt0008Resource.class);

    @Autowired
    private Xdxt0008Service xdxt0008Service;
    /**
     * 交易码：xdxt0008
     * 交易描述：根据客户经理号查询账务机构号
     *
     * @param xdxt0008DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户经理号查询账务机构号")
    @PostMapping("/xdxt0008")
    protected @ResponseBody
    ResultDto<Xdxt0008DataRespDto> xdxt0008(@Validated @RequestBody Xdxt0008DataReqDto xdxt0008DataReqDto) throws Exception {
        logger.info(OcaTradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0008.key, OcaTradeEnum.TRADE_CODE_XDXT0008.value, JSON.toJSONString(xdxt0008DataReqDto));
        Xdxt0008DataRespDto xdxt0008DataRespDto = new Xdxt0008DataRespDto();// 响应Dto:根据客户经理号查询账务机构号
        ResultDto<Xdxt0008DataRespDto> xdxt0008DataResultDto = new ResultDto<>();
        String managerId = xdxt0008DataReqDto.getManagerId();//客户经理号
        try {
            // 从xdxt0008DataReqDto获取业务值进行业务逻辑处理
            logger.info(OcaTradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0008.key, OcaTradeEnum.TRADE_CODE_XDXT0008.value, JSON.toJSONString(xdxt0008DataReqDto));
            xdxt0008DataRespDto = xdxt0008Service.getXdxt0008(xdxt0008DataReqDto);
            logger.info(OcaTradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0008.key, OcaTradeEnum.TRADE_CODE_XDXT0008.value, JSON.toJSONString(xdxt0008DataRespDto));
            // 封装xdxt0008DataResultDto中正确的返回码和返回信息
            xdxt0008DataResultDto.setCode(OcaTradeEnum.CMIS_SUCCSESS.key);
            xdxt0008DataResultDto.setMessage(OcaTradeEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(OcaTradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0008.key, OcaTradeEnum.TRADE_CODE_XDXT0008.value, e.getMessage());
            // 封装xdxt0008DataResultDto中异常返回码和返回信息
            xdxt0008DataResultDto.setCode(OcaTradeEnum.EPB099999.key);
            xdxt0008DataResultDto.setMessage(OcaTradeEnum.EPB099999.value);
        }
        // 封装xdxt0008DataRespDto到xdxt0008DataResultDto中
        xdxt0008DataResultDto.setData(xdxt0008DataRespDto);
        logger.info(OcaTradeLogConstants.RESOURCE_END_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0008.key, OcaTradeEnum.TRADE_CODE_XDXT0008.value, JSON.toJSONString(xdxt0008DataResultDto));
        return xdxt0008DataResultDto;
    }
}
