package cn.com.yusys.yusp.message.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 消息模板配置
 *
 * @author xiaodg@yusys.com.cn
 */
@Data
@TableName("message_temp")
public class MessageTempEntity implements Serializable {

    private static final long serialVersionUID = -7956314591656486956L;
    /**
     * 消息类型
     */
    @TableId
    private String messageType;
    /**
     * 适用渠道类型[system,email,mobile]
     */
    private String channelType;
    /**
     * 异常重发次数
     */
    private Integer sendNum;
    /**
     * 模板内容
     */
    private String templateContent;
    /**
     * 邮件/系统消息标题
     */
    private String emailTitle;
    /**
     * 发送开始时间
     */
    private String timeStart;
    /**
     * 发送结束时间
     */
    private String timeEnd;
    /**
     * 是否固定时间发送
     */
    private String isTime;

}
