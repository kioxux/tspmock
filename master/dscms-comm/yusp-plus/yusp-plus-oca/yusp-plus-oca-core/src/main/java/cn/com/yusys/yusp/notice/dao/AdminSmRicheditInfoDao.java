package cn.com.yusys.yusp.notice.dao;

import cn.com.yusys.yusp.notice.entity.AdminSmRicheditInfoEntity;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 富文本信息表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-15 13:05:43
 */

public interface AdminSmRicheditInfoDao extends BaseMapper<AdminSmRicheditInfoEntity> {

}