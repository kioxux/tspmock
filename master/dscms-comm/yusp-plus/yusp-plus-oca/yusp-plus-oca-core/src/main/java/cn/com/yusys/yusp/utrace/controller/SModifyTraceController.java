package cn.com.yusys.yusp.utrace.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.utrace.domain.entity.SModifyTraceEntity;
import cn.com.yusys.yusp.utrace.domain.vo.UTraceQueryVo;
import cn.com.yusys.yusp.utrace.service.SModifyTraceService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 小U留痕记录表
 *
 * @author
 * @date 2021-05-17 15:13:18
 */
@Api("小U留痕记录表接口")
@RestController
@RequestMapping("/api/utrace")
public class SModifyTraceController {
    @Autowired
    private SModifyTraceService sModifyTraceService;

    /**
     * 查询修改记录列表信息，分页
     */
    @ApiOperation("列表")
    @GetMapping("/listPage")
    public ResultDto list(UTraceQueryVo params) {
        Page page = sModifyTraceService.queryPage(params);
        return ResultDto.success(page);
    }


    /**
     * 根据 数据主键 mPkV查询是否有修改记录
     */
    @ApiOperation("信息")
    @GetMapping("/listByPk")
    public ResultDto listAll(UTraceQueryVo params) {
        List<SModifyTraceEntity> sModifyTrace = sModifyTraceService.queryUtraceByPk(params);
        return ResultDto.success(sModifyTrace);
    }

    /**
     * 保存修改记录
     */
    @ApiOperation("保存")
    @PostMapping("/save")
    public ResultDto save(@RequestBody List<SModifyTraceEntity> t) {
        sModifyTraceService.addUtrace(t);
        return ResultDto.success();
    }
}