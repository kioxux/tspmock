/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dscms.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TaskUrgentDetailInfo
 * @类描述: task_urgent_detail_info数据实体类
 * @功能描述: 
 * @创建人: 刘权
 * @创建时间: 2021-04-22 14:53:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "task_urgent_detail_info")
public class TaskUrgentDetailInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 员工号 **/
	@Column(name = "USER_CODE", unique = false, nullable = true, length = 20)
	private String userCode;
	
	/** 员工姓名 **/
	@Column(name = "USER_NAME", unique = false, nullable = true, length = 40)
	private String userName;
	
	/** 当前剩余笔数 **/
	@Column(name = "CURT_SURPLUS_QNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtSurplusQnt;
	
	/** 本次新增笔数  **/
	@Column(name = "CURT_ADD_QNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtAddQnt;
	
	/** 总优先笔数  **/
	@Column(name = "TOTAL_PRI_QNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalPriQnt;
	
	/** 历史结余笔数  **/
	@Column(name = "HIS_SURPLUS_QNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal hisSurplusQnt;
	
	/** 本年使用次数  **/
	@Column(name = "CURT_YEAY_UTIL_TIMES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtYeayUtilTimes;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 起始日期 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 20)
	private String startDate;

	/** 终止日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 20)
	private String endDate;

	/** 生效状态 **/
	@Column(name = "URGENT_STATUS", unique = false, nullable = true, length = 2)
	private String urgentStatus;

	/** 机构编号 **/
	@Column(name = "ORG_ID", unique = false, nullable = true, length = 32)
	private String orgId;

	/** 本期使用次数 **/
	@Column(name = "CURT_PERIOD_UTIL_TIMES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtPeriodUtilTimes;

	public TaskUrgentDetailInfo() {
	}

	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param userCode
	 */
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	
    /**
     * @return userCode
     */
	public String getUserCode() {
		return this.userCode;
	}
	
	/**
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
    /**
     * @return userName
     */
	public String getUserName() {
		return this.userName;
	}
	
	/**
	 * @param curtSurplusQnt
	 */
	public void setCurtSurplusQnt(java.math.BigDecimal curtSurplusQnt) {
		this.curtSurplusQnt = curtSurplusQnt;
	}
	
    /**
     * @return curtSurplusQnt
     */
	public java.math.BigDecimal getCurtSurplusQnt() {
		return this.curtSurplusQnt;
	}
	
	/**
	 * @param curtAddQnt
	 */
	public void setCurtAddQnt(java.math.BigDecimal curtAddQnt) {
		this.curtAddQnt = curtAddQnt;
	}
	
    /**
     * @return curtAddQnt
     */
	public java.math.BigDecimal getCurtAddQnt() {
		return this.curtAddQnt;
	}
	
	/**
	 * @param totalPriQnt
	 */
	public void setTotalPriQnt(java.math.BigDecimal totalPriQnt) {
		this.totalPriQnt = totalPriQnt;
	}
	
    /**
     * @return totalPriQnt
     */
	public java.math.BigDecimal getTotalPriQnt() {
		return this.totalPriQnt;
	}
	
	/**
	 * @param hisSurplusQnt
	 */
	public void setHisSurplusQnt(java.math.BigDecimal hisSurplusQnt) {
		this.hisSurplusQnt = hisSurplusQnt;
	}
	
    /**
     * @return hisSurplusQnt
     */
	public java.math.BigDecimal getHisSurplusQnt() {
		return this.hisSurplusQnt;
	}
	
	/**
	 * @param curtYeayUtilTimes
	 */
	public void setCurtYeayUtilTimes(java.math.BigDecimal curtYeayUtilTimes) {
		this.curtYeayUtilTimes = curtYeayUtilTimes;
	}
	
    /**
     * @return curtYeayUtilTimes
     */
	public java.math.BigDecimal getCurtYeayUtilTimes() {
		return this.curtYeayUtilTimes;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getUrgentStatus() {
		return urgentStatus;
	}

	public void setUrgentStatus(String urgentStatus) {
		this.urgentStatus = urgentStatus;
	}

	public String getOrgId() { return orgId;}

	public void setOrgId(String orgId) {this.orgId = orgId;	}

	public BigDecimal getCurtPeriodUtilTimes() {return curtPeriodUtilTimes;	}

	public void setCurtPeriodUtilTimes(BigDecimal curtPeriodUtilTimes) {this.curtPeriodUtilTimes = curtPeriodUtilTimes;	}
}