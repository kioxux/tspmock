package cn.com.yusys.yusp.notice.form;

import lombok.Data;

@Data
public class AdminSmNoticeReciveForm {

    private String reciveRoleId;
    private String reciveObjId;
}
