/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dscms.service;

//import cn.com.yusys.yusp.batch.dto.server.cmisbatch0008.Cmisbatch0008ReqDto;
//import cn.com.yusys.yusp.batch.dto.server.cmisbatch0008.Cmisbatch0008RespDto;
//import cn.com.yusys.yusp.batch.service.CmisBatchClientService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.dscms.domain.CfgTaskUrgentInfo;
import cn.com.yusys.yusp.dscms.repository.mapper.CfgTaskUrgentInfoMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgTaskUrgentInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 刘权
 * @创建时间: 2021-04-22 21:50:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgTaskUrgentInfoService {
    private final Logger log = LoggerFactory.getLogger(CfgTaskUrgentInfoService.class);


    @Autowired
    private CfgTaskUrgentInfoMapper cfgTaskUrgentInfoMapper;

//    @Autowired
//    private CmisBatchClientService cmisBatchClientService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CfgTaskUrgentInfo selectByPrimaryKey(String serno) {
        return cfgTaskUrgentInfoMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CfgTaskUrgentInfo> selectAll(QueryModel model) {
        List<CfgTaskUrgentInfo> records = (List<CfgTaskUrgentInfo>) cfgTaskUrgentInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CfgTaskUrgentInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgTaskUrgentInfo> list = cfgTaskUrgentInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CfgTaskUrgentInfo record) throws Exception {
        int num= this.updateAndInsertInfo(record);
//        if( num == 1 ) {
//            //任务加急配置数据 批量 初始化
//            Cmisbatch0008ReqDto reqDto = new Cmisbatch0008ReqDto();
//            reqDto.setBizType(record.getBizType());//业务类型
//            reqDto.setPriDivisPerc((record.getPriDivisPerc()).toString());//除100分配比例
//            reqDto.setUpdPeriod(record.getUpdPeriod());//更新周期
//            reqDto.setInputBrId(record.getInputBrId());
//            reqDto.setInputId(record.getInputId());
//            reqDto.setInputDate(record.getInputDate());
//            ResultDto<Cmisbatch0008RespDto> result = cmisBatchClientService.cmisbatch0008(reqDto);
//            if (!"0".equals(result.getCode())) {
//                log.error("任务加急配置数据初始化失败，调用批量服务失败：", result.getMessage());
//                throw new Exception("任务加急配置数据初始化失败，调用批量服务失败！");
//            }
//        }
        return num;
    }

    /**
     * @方法名称: updateAndInsertInfo
     * @方法描述: 更新数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int updateAndInsertInfo(CfgTaskUrgentInfo record){
        //存量生效数据 失效
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("Status","1");
        List<CfgTaskUrgentInfo> list = cfgTaskUrgentInfoMapper.selectByModel(queryModel);
        for (CfgTaskUrgentInfo item : list) {
            item.setStatus("0");
            cfgTaskUrgentInfoMapper.updateByPrimaryKey(item);
        }

        record.setStatus("1");// 新增 状态为生效
        record.setOprType("01");
        record.setPriDivisPerc(record.getPriDivisPerc().divide(new BigDecimal(100)));//除100
        return cfgTaskUrgentInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CfgTaskUrgentInfo record) {
        return cfgTaskUrgentInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CfgTaskUrgentInfo record) {
        return cfgTaskUrgentInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CfgTaskUrgentInfo record) {
        return cfgTaskUrgentInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cfgTaskUrgentInfoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgTaskUrgentInfoMapper.deleteByIds(ids);
    }


    public CfgTaskUrgentInfo selectBizType() {
        return cfgTaskUrgentInfoMapper.selectBizType();
    }

}
