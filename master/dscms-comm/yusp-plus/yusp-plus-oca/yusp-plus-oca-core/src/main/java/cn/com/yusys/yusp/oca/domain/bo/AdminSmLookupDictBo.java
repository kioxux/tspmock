package cn.com.yusys.yusp.oca.domain.bo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author xufy1@yusys.com.cn
 * @desc
 * @date 2021-01-15 18:39
 */

@Data
public class AdminSmLookupDictBo implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 字典类别主键id
     */
    private String lookupItemId;

    /**
     * 父字典类别id
     */
    private String upLookupItemId;
    /**
     * 字典类别code码
     */
    @NotBlank(message = "lookupCode can not be empty!")
    private String lookupCode;
    /**
     * 字典类别名称
     */
    @NotBlank(message = "lookupName can not be empty!")
    private String lookupName;
    /**
     * 字典类别分类标识id
     */
    @NotBlank(message = "lookupTypeId can not be empty!")
    private String lookupTypeId;

    private String lookupTypeName;

    List<LookupItemBo> lookupItemBos;

    @Data
    public static class LookupItemBo {

        public LookupItemBo() {
        }

        /**
         * 字典类别主键id
         */
        private String lookupItemId;
        /**
         * 字典代码
         */
        @NotBlank(message = "lookupItemCode can not be empty!")
        private String lookupItemCode;
        /**
         * 字典名称
         */
        @NotBlank(message = "lookupItemName can not be empty!")
        private String lookupItemName;
    }
}
