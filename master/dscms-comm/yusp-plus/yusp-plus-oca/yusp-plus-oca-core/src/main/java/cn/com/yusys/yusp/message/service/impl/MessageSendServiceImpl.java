package cn.com.yusys.yusp.message.service.impl;

import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.commons.util.RandomUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.dto.ReceivedUserDto;
import cn.com.yusys.yusp.message.config.DoNotDisturbTimeRange;
import cn.com.yusys.yusp.message.config.MessageConstants;
import cn.com.yusys.yusp.message.config.PkHash;
import cn.com.yusys.yusp.message.constant.ReceivedUserTypeEnum;
import cn.com.yusys.yusp.message.entity.*;
import cn.com.yusys.yusp.message.enumeration.MessageChannelEnum;
import cn.com.yusys.yusp.message.enumeration.MessageTempSendTypeEnum;
import cn.com.yusys.yusp.message.service.*;
import cn.com.yusys.yusp.message.util.MessageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 发送消息服务类
 *
 * @author xiaodg@yusys.com.cn
 **/
@Service
@Slf4j
public class MessageSendServiceImpl implements MessageSendService {

    @Autowired
    private MessageTypeService messageTypeService;

    @Autowired
    private MessageTempService messageTempService;

    @Autowired
    private MessageEventService messageEventService;

    @Autowired
    private MessageContentService messageContentService;

    @Autowired
    private MessagePoolService messagePoolService;

    @Autowired
    private MessagePoolHisService messagePoolHisService;

    @Autowired
    private MessageBindService messageBindService;

    @Autowired
    private MessageSubscribeService messageSubscribeService;

    @Autowired
    private PkHash pkHash;

    @Autowired
    private DoNotDisturbTimeRange notDisturbTimeRange;

    @Override
    public void sendMessage(String messageType, MessageTempEntity messageTempEntity, Set<String> receiveUserIds, String sendUserId, Map<String, String> params) {
        // 防止接收者为null
        receiveUserIds = Optional.ofNullable(receiveUserIds).orElseGet(HashSet::new);
        // 先查询消息类型
        MessageTypeEntity messageTypeEntity = this.messageTypeService.lambdaQuery().eq(MessageTypeEntity::getMessageType, messageType).one();
        if (Objects.isNull(messageTypeEntity)) {
            log.error("发送消息: 数据库中没有找到消息类型[{}]", messageType);
            return;
        }
        // 若消息类型为订阅消息，根据订阅关系查询出消息的所有接收者
        if (MessageTempSendTypeEnum.SUBSCRIBE.getType().equalsIgnoreCase(messageTypeEntity.getMessageType())) {
            receiveUserIds.addAll(messageSubscribeService.findUserIdsBy(messageType, messageTempEntity.getChannelType(), sendUserId));
        }
        if (receiveUserIds.size() <= 0) {
            log.error("发送消息: 发送消息类型[{}],渠道[{}]，没有任何接收用户，请检查配置!", messageType, messageTempEntity.getChannelType());
            return;
        }
        receiveUserIds.forEach(userId -> {
            // 生成消息事件
            String eventNo = this.generateEvenNo();
            generateMessageEvent(eventNo, messageTypeEntity, params);
            // 生成消息
            generateMessageContent(messageTypeEntity, messageTempEntity, eventNo, params);
            // 是否是免打扰模式
            String isTime = MessageConstants.DO_NOT_DISTURB_MODE.equalsIgnoreCase(messageTempEntity.getIsTime()) ? MessageConstants.DO_NOT_DISTURB_MODE : MessageConstants.NON_DO_NOT_DISTURB_MODE;
            // 生成消息队列
            generateMessagePool(messageTypeEntity, userId, messageTempEntity.getChannelType(), eventNo, params.get(MessageConstants.MESSAGE_PARAM_START_TIME), params.get(MessageConstants.MESSAGE_PARAM_END_TIME), isTime);
        });
    }

    @Override
    @Async
    public void sendMessage(String messageType, Set<String> receiveUserIds, String sendUserId, Map<String, String> params) {
        // 防止接收者为null
        receiveUserIds = Optional.ofNullable(receiveUserIds).orElseGet(HashSet::new);
        // 根据消息类型查询出已经配置过的所有消息模板（渠道）
        List<MessageTempEntity> temps = this.messageTempService.lambdaQuery().eq(MessageTempEntity::getMessageType, messageType).list();
        if (Objects.isNull(temps) || temps.size() <= 0) {
            log.error("发送消息: 没有在数据库中找到消息类型[{}]对应的消息模板", messageType);
            return;
        }
        // 遍历消息模板的渠道
        Set<String> finalReceiveUserIds = receiveUserIds;
        temps.forEach(temp -> sendMessage(messageType, temp, finalReceiveUserIds, sendUserId, params));
    }

    @Override
    public void sendMessage(String messageType, MessageChannelEnum messageChannelEnum, Set<String> receiveUserIds, String sendUserId, Map<String, String> params) {
        // 防止接收者为null
        receiveUserIds = Optional.ofNullable(receiveUserIds).orElseGet(HashSet::new);
        // 根据消息类型查询出已经配置过的所有消息模板（渠道）
        Set<String> finalReceiveUserIds = receiveUserIds;
        List<MessageTempEntity> temps = this.messageTempService.lambdaQuery().eq(MessageTempEntity::getMessageType, messageType).list();
        if (this.messageTempService.lambdaQuery().eq(MessageTempEntity::getMessageType, messageType).list().stream().noneMatch(temp -> temp.getChannelType().equalsIgnoreCase(messageChannelEnum.getType()))) {
            log.error("发送消息: 消息类型[{}]没有任何[{}]渠道模板", messageType, messageChannelEnum.getType());
            return;
        }
        temps.stream().filter(temp -> temp.getChannelType().equalsIgnoreCase(messageChannelEnum.getType())).collect(Collectors.toSet()).forEach(temp -> {
            sendMessage(messageType, temp, finalReceiveUserIds, sendUserId, params);
        });
    }

    @Override
    public void resendMessage(String pkNo) {
        try {
            // 从数据库查询发送不成功的历史记录
            final MessagePoolHisEntity messagePoolHisEntity = messagePoolHisService.getById(pkNo);
            if (Objects.nonNull(messagePoolHisEntity)) {
                if (!MessageConstants.SEND_EXCEPTION.equalsIgnoreCase(messagePoolHisEntity.getState())) {
                    log.error("重发消息: 状态正确, 无需重发, pkNo: {}", pkNo);
                    return;
                }
                // 删除发送不成功的历史记录
                messagePoolHisService.removeById(pkNo);
                // 生成消息队列并发送
                final MessageTypeEntity messageTypeEntity = messageTypeService.getById(messagePoolHisEntity.getMessageType());
                if (Objects.isNull(messageTypeEntity)) {
                    log.error("重发消息: 重发消息时未查找到messageType, pk: {}", pkNo);
                    return;
                }
                if (ReceivedUserTypeEnum.JKR.getCode().equals(messagePoolHisEntity.getUserType())) {
                    ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                    receivedUserDto.setUserId(messagePoolHisEntity.getUserNo());
                    receivedUserDto.setMobilePhone(messagePoolHisEntity.getUserAddr());
                    receivedUserDto.setReceivedUserType(messagePoolHisEntity.getUserType());
                    generateMessagePool(messageTypeEntity, receivedUserDto, messagePoolHisEntity.getChannelType(), messagePoolHisEntity.getEventNo(), messagePoolHisEntity.getTimeStart(), messagePoolHisEntity.getTimeEnd(), MessageConstants.NON_DO_NOT_DISTURB_MODE);
                } else {
                    generateMessagePool(messageTypeEntity, messagePoolHisEntity.getUserNo(), messagePoolHisEntity.getChannelType(), messagePoolHisEntity.getEventNo(), messagePoolHisEntity.getTimeStart(), messagePoolHisEntity.getTimeEnd(), MessageConstants.NON_DO_NOT_DISTURB_MODE);
                }
            } else {
                log.error("重发消息: 不能找到重发的消息历史主键：{}", pkNo);
            }
        } catch (Exception e) {
            log.error("重发消息: 重发失败,pkNo: {}, err:  {}", pkNo, e);
        }
    }

    @Override
    @Async
    public void sendMessage(String messageType, List<ReceivedUserDto> receivedUserList, String sendUserId, Map<String, String> params) {
        // 防止接收者为null
        receivedUserList = Optional.ofNullable(receivedUserList).orElseGet(ArrayList<ReceivedUserDto>::new);
        // 根据消息类型查询出已经配置过的所有消息模板（渠道）
        List<MessageTempEntity> temps = this.messageTempService.lambdaQuery().eq(MessageTempEntity::getMessageType, messageType).list();
        if (Objects.isNull(temps) || temps.size() <= 0) {
            log.error("发送消息: 没有在数据库中找到消息类型[{}]对应的消息模板", messageType);
            return;
        }
        // 遍历消息模板的渠道
        List<ReceivedUserDto> finalReceivedUserList = receivedUserList;
        temps.forEach(temp -> sendMessage(messageType, temp, finalReceivedUserList, sendUserId, params));
    }

    @Override
    public void sendMessage(String messageType, MessageTempEntity messageTempEntity, List<ReceivedUserDto> receivedUserList, String sendUserId, Map<String, String> params) {
        // 防止接收者为null
        receivedUserList = Optional.ofNullable(receivedUserList).orElseGet(ArrayList<ReceivedUserDto>::new);
        // 先查询消息类型
        MessageTypeEntity messageTypeEntity = this.messageTypeService.lambdaQuery().eq(MessageTypeEntity::getMessageType, messageType).one();
        if (Objects.isNull(messageTypeEntity)) {
            log.error("发送消息: 数据库中没有找到消息类型[{}]", messageType);
            return;
        }
        // 若消息类型为订阅消息，根据订阅关系查询出消息的所有接收者---暂不考虑订阅场景
//        if (MessageTempSendTypeEnum.SUBSCRIBE.getType().equalsIgnoreCase(messageTypeEntity.getMessageType())) {
//            receiveUserIds.addAll(messageSubscribeService.findUserIdsBy(messageType, messageTempEntity.getChannelType(), sendUserId));
//        }
        if (receivedUserList.size() <= 0) {
            log.error("发送消息: 发送消息类型[{}],渠道[{}]，没有任何接收用户，请检查配置!", messageType, messageTempEntity.getChannelType());
            return;
        }
        receivedUserList.forEach(receivedUserDto -> {
            // 生成消息事件
            String eventNo = this.generateEvenNo();
            generateMessageEvent(eventNo, messageTypeEntity, params);
            // 生成消息
            generateMessageContent(messageTypeEntity, messageTempEntity, eventNo, params);
            // 20211104 如果params中获取开始时间和结束时间为空，则手动赋值 开始
            LocalDate now = LocalDate.now();
            log.info("获取当前时间为:[{}]", now.toString());
            String timeStart = messageTempEntity.getTimeStart();
            String timeEnd = messageTempEntity.getTimeEnd();
            if (!"".equals(messageTempEntity.getTimeStart()) && null != messageTempEntity.getTimeStart()) {
                timeStart = now.toString().concat(" ").concat(messageTempEntity.getTimeStart()).concat(":00");
            }
            if (!"".equals(messageTempEntity.getTimeEnd()) && null != messageTempEntity.getTimeEnd()) {
                timeEnd = now.toString().concat(" ").concat(messageTempEntity.getTimeEnd()).concat(":00");
            }
            log.info("timeStart:" + timeStart);
            log.info("timeEnd:" + timeEnd);
            Optional.ofNullable(params.get(MessageConstants.MESSAGE_PARAM_START_TIME)).orElse(params.put(MessageConstants.MESSAGE_PARAM_START_TIME, timeStart));
            Optional.ofNullable(params.get(MessageConstants.MESSAGE_PARAM_END_TIME)).orElse(params.put(MessageConstants.MESSAGE_PARAM_END_TIME, timeEnd));
            // 20211104 如果params中获取开始时间和结束时间为空，则手动赋值 end
            // 是否是免打扰模式
            String isTime = MessageConstants.DO_NOT_DISTURB_MODE.equalsIgnoreCase(messageTempEntity.getIsTime()) ? MessageConstants.DO_NOT_DISTURB_MODE : MessageConstants.NON_DO_NOT_DISTURB_MODE;
            // 生成消息队列
            generateMessagePool(messageTypeEntity, receivedUserDto, messageTempEntity.getChannelType(), eventNo, params.get(MessageConstants.MESSAGE_PARAM_START_TIME), params.get(MessageConstants.MESSAGE_PARAM_END_TIME), isTime);
        });
    }

    /**
     * 生成消息事件
     *
     * @param eventNo           事件编号 {@code String}
     * @param messageTypeEntity 消息类型  {@link MessageTypeEntity}
     * @param param             模板参数  {@code String}
     */
    private void generateMessageEvent(String eventNo, MessageTypeEntity messageTypeEntity, Map<String, String> param) {
        this.messageEventService.save(new MessageEventEntity(eventNo, ObjectMapperUtils.toJson(param), LocalDateTime.now().format(DateTimeFormatter.ofPattern(DateFormatEnum.DATETIME.getValue())), messageTypeEntity.getMessageType()));
    }

    /**
     * 生成消息内容
     *
     * @param messageTypeEntity 消息类型  {@link MessageTypeEntity}
     * @param messageTempEntity 消息模板 {@link MessageTempEntity}
     * @param eventNo           事件编号  {@code String}
     * @param params            模板参数 {@link Map}
     * @return 消息内容 {@link MessageContentEntity}
     */
    private MessageContentEntity generateMessageContent(MessageTypeEntity messageTypeEntity, MessageTempEntity messageTempEntity, String eventNo, Map<String, String> params) {
        MessageContentEntity messageContentEntity = new MessageContentEntity();
        messageContentEntity.setEventNo(eventNo);
        messageContentEntity.setMessageType(messageTypeEntity.getMessageType());
        messageContentEntity.setChannelType(messageTempEntity.getChannelType());
        messageContentEntity.setContent(MessageUtils.parseParam(messageTempEntity.getTemplateContent(), params));
        messageContentEntity.setSendNum(messageTempEntity.getSendNum());
        messageContentEntity.setTimeStart(messageTempEntity.getTimeStart());
        messageContentEntity.setTimeEnd(messageTempEntity.getTimeEnd());
        // 标题
        messageContentEntity.setEmailTitle(messageTempEntity.getEmailTitle());
        // 保存消息
        messageContentService.save(messageContentEntity);
        return messageContentEntity;
    }


    /**
     * 生成消息队列
     *
     * @param messageTypeEntity  消息类型  {@link MessageTypeEntity}
     * @param userId             用户ID  {@code String}
     * @param messageChannelType 消息发送渠道类型  {@code String}
     * @param eventNo            消息事件编号  {@code String}
     * @param startTime          开始时间, 格式为: yyyy-MM-dd HH:mm:ss  {@code String}
     * @param endTime            结束时间, 格式为: yyyy-MM-dd HH:mm:ss  {@code String}
     * @param isTime             是否免打扰  {@code String}
     */
    private void generateMessagePool(MessageTypeEntity messageTypeEntity, String userId, String messageChannelType, String eventNo, String startTime, String endTime, String isTime) {
        MessagePoolEntity messagePoolEntity = new MessagePoolEntity();
        try {
            messagePoolEntity.setMessageType(messageTypeEntity.getMessageType());
            messagePoolEntity.setChannelType(messageChannelType);
            messagePoolEntity.setPkNo(generatePkNo());
            messagePoolEntity.setCreateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern(DateFormatEnum.DATETIME.getValue())));
            messagePoolEntity.setEventNo(eventNo);
            messagePoolEntity.setMessageLevel(messageTypeEntity.getMessageLevel());
            messagePoolEntity.setState(MessageConstants.SENDING);
            messagePoolEntity.setUserNo(userId);
            messagePoolEntity.setTimeStart(startTime);
            messagePoolEntity.setTimeEnd(endTime);
            messagePoolEntity.setPkHash(RandomUtils.nextInt(pkHash.getPkHashMin(), pkHash.getPkHashMax()));
            messagePoolService.save(messagePoolEntity);
            sendMessagePoolEntityToBind(messagePoolEntity, isTime);
        } catch (Exception e) {
            log.error("生成消息队列错误: ", e);
        }
    }

    /**
     * 生成消息队列
     *
     * @param messageTypeEntity  消息类型  {@link MessageTypeEntity}
     * @param receivedUserDto    接收用户实体类  {@code String}
     * @param messageChannelType 消息发送渠道类型  {@code String}
     * @param eventNo            消息事件编号  {@code String}
     * @param startTime          开始时间, 格式为: yyyy-MM-dd HH:mm:ss  {@code String}
     * @param endTime            结束时间, 格式为: yyyy-MM-dd HH:mm:ss  {@code String}
     * @param isTime             是否免打扰  {@code String}
     */
    private void generateMessagePool(MessageTypeEntity messageTypeEntity, ReceivedUserDto receivedUserDto, String messageChannelType, String eventNo, String startTime, String endTime, String isTime) {
        MessagePoolEntity messagePoolEntity = new MessagePoolEntity();
        try {
            messagePoolEntity.setMessageType(messageTypeEntity.getMessageType());
            messagePoolEntity.setChannelType(messageChannelType);
            messagePoolEntity.setPkNo(generatePkNo());
            messagePoolEntity.setCreateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern(DateFormatEnum.DATETIME.getValue())));
            messagePoolEntity.setEventNo(eventNo);
            messagePoolEntity.setMessageLevel(messageTypeEntity.getMessageLevel());
            messagePoolEntity.setState(MessageConstants.SENDING);
            messagePoolEntity.setUserNo(receivedUserDto.getUserId());
            messagePoolEntity.setUserType(receivedUserDto.getReceivedUserType());
            messagePoolEntity.setUserAddr(receivedUserDto.getMobilePhone());
            messagePoolEntity.setTimeStart(startTime);
            messagePoolEntity.setTimeEnd(endTime);
            messagePoolEntity.setPkHash(RandomUtils.nextInt(pkHash.getPkHashMin(), pkHash.getPkHashMax()));
            messagePoolService.save(messagePoolEntity);
            sendMessagePoolEntityToBind(messagePoolEntity, isTime);
        } catch (Exception e) {
            log.error("生成消息队列错误: ", e);
        }
    }

    /**
     * 发送消息到Bind
     *
     * @param messagePoolEntity 消息队列
     */
    @Override
    public void sendMessagePoolEntityToBind(MessagePoolEntity messagePoolEntity, String isTime) {
        // 发送消息队列到Binder
        final String timeStart = messagePoolEntity.getTimeStart();
        final String timeEnd = messagePoolEntity.getTimeEnd();
        final LocalDateTime now = LocalDateTime.now();
        // 默认为及时发送
        LocalDateTime shouldSendStartTime = now;
        LocalDateTime shouldSendEndTime = now;
        if (StringUtils.nonEmpty(timeStart)) {
            // 获取消息队列设定的开始发送时间点
            shouldSendStartTime = LocalDateTime.parse(timeStart, DateTimeFormatter.ofPattern(DateFormatEnum.DATETIME.getValue()));
        }
        if (StringUtils.nonEmpty(timeEnd)) {
            // 获取消息队列设定的结束发送时间点
            shouldSendEndTime = LocalDateTime.parse(timeEnd, DateTimeFormatter.ofPattern(DateFormatEnum.DATETIME.getValue()));
        }
        if (now.isAfter(shouldSendEndTime)) {
            // 当前时间超过了消息队列设定的结束发送时间
            log.error("发送消息到Bind: 消息队列超过了消息发送的结束时间，不予发送.pkNo: {}", messagePoolEntity.getPkNo());
            return;
        }
        // 获取延迟发送秒数
        long delaySeconds = MessageConstants.MESSAGE_DEFAULT_DELAY_SEND_SECONDS;
        // 处理免打扰
        if (MessageConstants.DO_NOT_DISTURB_MODE.equalsIgnoreCase(isTime)) {
            delaySeconds = calculateDelaySeconds(now, shouldSendStartTime, shouldSendEndTime);
        }
        // 发送消息队列到Bind
        messageBindService.send(messagePoolEntity, delaySeconds);
    }

    /**
     * 免打扰模式下计算消息需要延迟发送的秒数
     *
     * @return 延迟发送的秒数
     */
    private long calculateDelaySeconds(LocalDateTime now, LocalDateTime shouldSendStartTime, LocalDateTime shouldSendEndTime) {
        // 消息队列设定的发送开始时间
        long shouldSendStartSecond = shouldSendStartTime.getSecond();
        log.info("消息队列设定的发送开始时间:[{}]", shouldSendStartSecond);
        // 消息队列设定的发送结束时间
        long shouldSendEndSecond = shouldSendEndTime.getSecond();
        log.info("消息队列设定的发送结束时间:[{}]", shouldSendEndSecond);
        // 消息队列设定的发送时间当天对应的免扰时间点(取小)
        LocalDateTime doNotDisturbStart = LocalDateTime.of(shouldSendStartTime.getYear(), shouldSendStartTime.getMonth(), shouldSendStartTime.getDayOfMonth(),
                notDisturbTimeRange.getDoNotDisturbStartTimeHour(), notDisturbTimeRange.getDoNotDisturbStartTimeMinute(), notDisturbTimeRange.getDoNotDisturbStartTimeSecond());
        long doNotDisturbStartSecond = LocalDateTime.of(shouldSendStartTime.getYear(), shouldSendStartTime.getMonth(), shouldSendStartTime.getDayOfMonth(),
                notDisturbTimeRange.getDoNotDisturbStartTimeHour(), notDisturbTimeRange.getDoNotDisturbStartTimeMinute(), notDisturbTimeRange.getDoNotDisturbStartTimeSecond()).getSecond();

        log.info("消息队列设定的发送时间当天对应的免扰时间点(取小):[{}],对应的秒数为:[{}]", doNotDisturbStart, doNotDisturbStartSecond);
        // 消息队列设定的发送结束时间当天对应的免扰时间点(取大)
        LocalDateTime doNotDisturbEnd = LocalDateTime.of(shouldSendEndTime.getYear(), shouldSendEndTime.getMonth(), shouldSendEndTime.getDayOfMonth(),
                notDisturbTimeRange.getDoNotDisturbEndTimeHour(), notDisturbTimeRange.getDoNotDisturbEndTimeMinute(), notDisturbTimeRange.getDoNotDisturbEndTimeSecond());
        long doNotDisturbEndSecond = LocalDateTime.of(shouldSendEndTime.getYear(), shouldSendEndTime.getMonth(), shouldSendEndTime.getDayOfMonth(),
                notDisturbTimeRange.getDoNotDisturbEndTimeHour(), notDisturbTimeRange.getDoNotDisturbEndTimeMinute(), notDisturbTimeRange.getDoNotDisturbEndTimeSecond()).getSecond();
        log.info("消息队列设定的发送结束时间当天对应的免扰时间点(取大):[{}],对应的秒数为:[{}]", doNotDisturbEnd, doNotDisturbEndSecond);

        // 当前时间点
        log.info("当前时间点:[{}]", now);

        // 当前时间点小于消息设定的应该发送的时间点
        if (now.isBefore(shouldSendStartTime)) {
            // 如果当前时间小于消息队列设定的开始发送时间并且消息队列的开始发送时间处于免打扰时间段,则延迟的秒数为: 消息队列应该发送的开始时间点 减去 当前时间点 的秒数
            long shouldSendStartBetweenSecond = Duration.between(now, shouldSendStartTime).getSeconds();
            log.info("如果当前时间小于消息队列设定的开始发送时间并且消息队列的开始发送时间处于免打扰时间段,则延迟的秒数为: 消息队列应该发送的开始时间点 减去 当前时间点 的秒数,秒数为:[{}]", shouldSendStartBetweenSecond);
            // 当前时间小于消息队列设定的开始发送时间并且消息队列的开始发送时间 比免打扰开始时间点小, 则延迟的数秒为: 消息队列应该发送当前的开始免打扰时间点 减去 当前时间点 的秒数
            long doNotDisturbStartBetweenSecond = Duration.between(now, doNotDisturbStart).getSeconds();
            log.info("当前时间小于消息队列设定的开始发送时间并且消息队列的开始发送时间 比免打扰开始时间点小, 则延迟的数秒为: 消息队列应该发送当前的开始免打扰时间点 减去 当前时间点 的秒数,秒数为:[{}]", doNotDisturbStartBetweenSecond);

            return (shouldSendStartSecond >= doNotDisturbStartSecond) ? shouldSendStartBetweenSecond : doNotDisturbStartBetweenSecond;
        } else if (shouldSendEndTime.isBefore(now)) {
            // 如果当前时间大于消息队列设定的结束发送时间,将在次日免打扰开始的时间段内发送, 延迟发送时间为: 消息队列应该发送的开始时间点 减去 当前时间点 的秒数
            long shouldSendEndBetweenSecond = Duration.between(now, shouldSendStartTime.plusDays(1)).getSeconds();
            log.info("如果当前时间大于消息队列设定的结束发送时间,将在次日免打扰开始的时间段内发送, 延迟发送时间为: 消息队列应该发送的开始时间点 减去 当前时间点 的秒数,秒数为:[{}]", shouldSendEndBetweenSecond);
            // 消息队列设定的结束发送时间超过了结束发送当天的免打扰时间,将在次日免打扰开始的时间段内发送, 延迟发送时间为: 次日开始免打扰的时间 减去 当前时间
            long doNotDisturbEndBetweenSecond = Duration.between(now, doNotDisturbEnd.plusDays(1)).getSeconds();
            log.info("消息队列设定的结束发送时间超过了结束发送当天的免打扰时间,将在次日免打扰开始的时间段内发送, 延迟发送时间为: 次日开始免打扰的时间 减去 当前时间 的秒数,秒数为:[{}]", doNotDisturbEndBetweenSecond);
            return (shouldSendEndSecond <= doNotDisturbEndSecond) ? shouldSendEndBetweenSecond : doNotDisturbEndBetweenSecond;
        }
        return MessageConstants.MESSAGE_DEFAULT_DELAY_SEND_SECONDS;
    }

    /**
     * 生成消息事件编号
     *
     * @return 消息事件编号 {@code String}
     */
    private String generateEvenNo() {
        return MessageConstants.MESSAGE_EVENT_NO_PREFIX + DateUtils.formatDate(DateUtils.PATTERN_DATETIME_COMPACT_SSS) + StringUtils.getUUID().substring(0, 7);
    }

    /**
     * 生成PkNo
     *
     * @return PkNo {@code String}
     */
    private String generatePkNo() {
        return MessageConstants.MESSAGE_PK_NO_PREFIX + DateUtils.formatDate(DateUtils.PATTERN_DATETIME_COMPACT_SSS) + StringUtils.getUUID().substring(0, 10);
    }
}
