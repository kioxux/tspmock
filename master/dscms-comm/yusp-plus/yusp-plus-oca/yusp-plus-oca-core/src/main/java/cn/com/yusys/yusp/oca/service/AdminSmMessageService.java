package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.oca.domain.bo.AdminSmMessageEditBo;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmMessageSaveBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmMessageEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmMessageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 提示信息管理表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-16 11:19:42
 */
public interface AdminSmMessageService extends IService<AdminSmMessageEntity> {

    /**
     * 查询提示信息分页
     *
     * @param adminSmMessageQuery
     * @return
     */
    Page<AdminSmMessageEntity> queryMessagePage(AdminSmMessageQuery adminSmMessageQuery);

    /**
     * 新增提示信息
     *
     * @param adminSmMessage
     */
    void saveMessage(AdminSmMessageSaveBo adminSmMessage);

    /**
     * 修改提示信息
     *
     * @param adminSmMessage
     */
    void updateMessageById(AdminSmMessageEditBo adminSmMessage);
}

