package cn.com.yusys.yusp.message.memorybinder.config;

import cn.com.yusys.yusp.message.memorybinder.MemoryMessageBinder;
import cn.com.yusys.yusp.message.memorybinder.provisioners.MemoryMessageBinderProvisioner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 内存模式发送消息绑定器配置
 * <p>
 * 当配置<pre>spring.cloud.stream.default-binder</pre>为memory时生效
 * <p>
 * 绑定器的设计可参考 <a href="https://cloud.spring.io/spring-cloud-stream/multi/multi_spring-cloud-stream-overview-binders.html">Spring Cloud Stream Binder SPI</a>
 * <p>
 * 当配置文件中没有配置<pre>spring.cloud.stream.default-binder</pre>时，默认使用该模式
 *
 * @author xiaodg@yusys.com.cn
 */
@Configuration
@ConditionalOnProperty(name = "spring.cloud.stream.default-binder", havingValue = "memory", matchIfMissing = true)
public class MemoryMessageBinderConfiguration {

    /**
     * 内存消息绑定器
     *
     * @param memoryMessageBinderProvisioner {@link MemoryMessageBinderProvisioner }
     * @return {@link MemoryMessageBinder}
     */
    @Bean
    @ConditionalOnMissingBean
    public MemoryMessageBinder memoryMessageBinder(MemoryMessageBinderProvisioner memoryMessageBinderProvisioner) {
        return new MemoryMessageBinder(null, memoryMessageBinderProvisioner);
    }

    /**
     * 内存消息绑定器供应者
     *
     * @return {@link MemoryMessageBinderProvisioner}
     */
    @Bean
    @ConditionalOnMissingBean
    public MemoryMessageBinderProvisioner memoryMessageBinderProvisioner() {
        return new MemoryMessageBinderProvisioner();
    }

}
