package cn.com.yusys.yusp.oca.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: yusp-plus
 * @description: 密码历史记录Dto
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2020-12-24 10:21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PasswordLogDto {
    /**
     * 被修改的密码
     */
    private String pwdUped;
    /**
     * 修改者id
     */
    private String updateUser;
    /**
     * 用户ID
     */
    private String userId;
}
