package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.oca.dao.AdminSmInstuDao;
import cn.com.yusys.yusp.oca.dao.AdminSmOrgDao;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmInstuEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmOrgEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmInstuQuery;
import cn.com.yusys.yusp.oca.domain.vo.InstuExtVo;
import cn.com.yusys.yusp.oca.domain.vo.UserSessionVo;
import cn.com.yusys.yusp.oca.service.AdminSmInstuService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Service("adminSmInstuService")
@Slf4j
public class AdminSmInstuServiceImpl extends ServiceImpl<AdminSmInstuDao, AdminSmInstuEntity> implements AdminSmInstuService {

    @Autowired
    private AdminSmOrgDao adminSmOrgDao;

    /**
     * 分页查询
     *
     * @param query AdminSmInstuQuery
     * @return page
     */
    @Override
    public Page<InstuExtVo> queryPage(AdminSmInstuQuery query) {
        Page<InstuExtVo> page = query.getIPage();
        QueryWrapper<InstuExtVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(!StringUtils.isEmpty(query.getInstuCde()), "i.instu_cde", query.getInstuCde());
        queryWrapper.like(!StringUtils.isEmpty(query.getInstuName()), "i.instu_name", query.getInstuName());
        queryWrapper.eq(!StringUtils.isEmpty(query.getInstuSts()), "i.instu_sts", query.getInstuSts());
        queryWrapper.orderByDesc("i.last_chg_dt");
        return this.baseMapper.pageInstuExtVo(page, queryWrapper);
    }

    /**
     * @方法名称: save
     * @方法描述: 新增金融机构数据
     */
    @Override
    @Transactional
    public boolean save(AdminSmInstuEntity entity, UserSessionVo userSessionVo) {
        if (StringUtils.isEmpty(entity.getInstuCde())) {
            throw BizException.error(null, "50100003", "金融机构代码为必填项");
        }
        QueryWrapper<AdminSmInstuEntity> wrapper = Wrappers.query();
        Integer exist = this.baseMapper.selectCount(wrapper.eq("INSTU_CDE", entity.getInstuCde()));
        if (exist > 0) {
            throw BizException.error(null, "50100004", "金融机构代码已经存在");
        }
//        entity.setInstuId(entity.getInstuCde());
        entity.setInstuSts(Optional.ofNullable(entity.getInstuSts()).orElse("W"));
        entity.setSysId(userSessionVo.getLogicSys().getSysId());
        log.info("New data of financial institutions: [new financial institutions: {}]", entity.getInstuName());
        this.baseMapper.insert(entity);
        return true;
    }

    /**
     * 批量启用
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean batchEnable(String[] ids) {
        if (CollectionUtils.nonEmpty(ids)) {
//        if(StringUtils.nonEmpty(ids)){
//            List<String> idList= Arrays.stream(ids.split(",")).filter(StringUtils::nonEmpty).collect(Collectors.toList());
            LambdaQueryWrapper<AdminSmInstuEntity> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.in(AdminSmInstuEntity::getInstuId, ids);
            AdminSmInstuEntity model = new AdminSmInstuEntity();
            model.setInstuSts("A");
            this.baseMapper.update(model, queryWrapper);
            return true;
        }
        return false;
    }

    @Override
    public boolean batchDisable(String[] ids) {
        if (CollectionUtils.nonEmpty(ids)) {
//        if(StringUtils.nonEmpty(ids)) {
//            List<String> idList = Arrays.stream(ids.split(",")).filter(StringUtils::nonEmpty).collect(Collectors.toList());
            // 检查是否有关联未停用的机构
            LambdaQueryWrapper<AdminSmOrgEntity> orgWrapper = new LambdaQueryWrapper<>();
            orgWrapper.in(AdminSmOrgEntity::getInstuId, ids).eq(AdminSmOrgEntity::getOrgSts, "A");
            Integer countSon = this.adminSmOrgDao.selectCount(orgWrapper);
            if (countSon > 0) {
                return false;
            }
            //批量更新
            AdminSmInstuEntity model = new AdminSmInstuEntity();
            model.setInstuSts("I");
            QueryWrapper<AdminSmInstuEntity> instuWrapper = new QueryWrapper<>();
            instuWrapper.in("instu_id", ids);
            this.baseMapper.update(model, instuWrapper);
            return true;
        }
        return false;
    }

    @Override
    @Transactional
    public boolean updateById(AdminSmInstuEntity entity) {
        AdminSmInstuEntity old = this.baseMapper.selectById(entity.getInstuId());
        if (!old.getInstuName().equals(entity.getInstuName())) {
            //对应根机构也要改名
            AdminSmOrgEntity orgEntity = new AdminSmOrgEntity();
            //TODO 注释掉的写法，长度太长了，应该咋处理
            orgEntity.setOrgId(entity.getInstuId());
            orgEntity.setOrgName(entity.getInstuName());
            this.adminSmOrgDao.updateById(orgEntity);
        }
//        entity.setInstuSts("W");
        log.info("Modified data of financial institutions: [modify financial institutions: {}] ", entity.getInstuName());
        this.baseMapper.updateById(entity);
        return false;
    }


    /**
     * 批量删除
     * 待删列表中存在【启用】状态的金融机构不得删除，整个删除操作失败
     */
    @Override
    @Transactional
    public boolean batchDelete(String[] ids) {
        if (CollectionUtils.nonEmpty(ids)) {
//        if(StringUtils.nonEmpty(ids)) {
//            List<String> idList= Arrays.stream(ids.split(",")).filter(StringUtils::nonEmpty).collect(Collectors.toList());
            LambdaQueryWrapper<AdminSmInstuEntity> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.in(AdminSmInstuEntity::getInstuId, ids).eq(AdminSmInstuEntity::getInstuSts, "A");
            int blocked = this.count(queryWrapper);
            if (blocked > 0) {
                return false;
            }
            LambdaQueryWrapper<AdminSmInstuEntity> deleteInstuWrapper = new LambdaQueryWrapper<>();
            deleteInstuWrapper.in(AdminSmInstuEntity::getInstuId, ids);
            this.baseMapper.delete(deleteInstuWrapper);

            LambdaQueryWrapper<AdminSmOrgEntity> deleteOrgWrapper = new LambdaQueryWrapper<>();
            deleteOrgWrapper.in(AdminSmOrgEntity::getInstuId, ids);
            this.adminSmOrgDao.delete(deleteOrgWrapper);
            return true;
        }
        return false;
    }
}