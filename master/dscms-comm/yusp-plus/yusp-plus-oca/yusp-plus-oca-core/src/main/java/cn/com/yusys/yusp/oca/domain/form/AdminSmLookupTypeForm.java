package cn.com.yusys.yusp.oca.domain.form;

import cn.com.yusys.yusp.oca.domain.vo.AdminSmDataTmplListVo;
import lombok.Data;

/**
 * @author danyu
 */
@Data
public class AdminSmLookupTypeForm {

	/**
	 * 记录编号
	 */
	private String contrId;
	/**
	 * 所属业务功能编号
	 */
	private String funcId;
	/**
	 * 控制操作代码
	 */
	private String contrCode;
	/**
	 * 控制操作名称
	 */
	private String contrName;
	/**
	 * 控制操作URL(用于后台校验时使用)
	 */
	private String contrUrl;
	/**
	 * 备注
	 */
	private String contrRemark;
	/**
	 * 请求类型
	 */
	private String methodType;
	/**
	 * 关联的模板
	 */
	private AdminSmDataTmplListVo adminSmDataTmplListVo;
}
