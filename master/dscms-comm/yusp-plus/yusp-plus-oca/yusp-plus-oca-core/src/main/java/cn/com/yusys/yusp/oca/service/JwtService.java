package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.oca.domain.vo.JwtVo;

/**
 * @Classname JwtService
 * @Description
 * @Date 2020/11/20 13:37
 * @Created by wujp4@yusys.com.cn
 */
public interface JwtService {
    /**
     * 根据authorization或去用户Id和客户端Id
     *
     * @param authorization
     * @return
     */
    JwtVo getUserIdAndClientIdByAuthorization(String authorization);
}
