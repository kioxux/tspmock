package cn.com.yusys.yusp.oca.interceptor;

import cn.com.yusys.yusp.commons.redis.template.YuspRedisTemplate;
import cn.com.yusys.yusp.commons.util.SpringContextUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.enums.SystemCacheEnum;

import java.util.Objects;

/**
 * 组织机构代码翻译
 *
 * @author yangzai
 * @since 2021-07-07
 */
public class OcaTranslatorUtils {

    private static YuspRedisTemplate instance;
    /**
     * 处理缓存默认使用的RedisTemplate
     */
    private static final String REDIS_TEMPLATE_NAME = "yuspRedisTemplate";

    private OcaTranslatorUtils() {
        throw new IllegalStateException("Utility class");
    }

    private static synchronized void init() {
        if (instance == null) {
            instance = SpringContextUtils.getBean(REDIS_TEMPLATE_NAME);
        }
    }

    private static YuspRedisTemplate instance() {
        if (instance == null) {
            init();
        }
        return instance;
    }

    /**
     * 通过缓存获取机构码对应的机构名
     *
     * @param orgCode 机构码
     * @return 机构名称
     */
    public static String getOrgName(String orgCode) {
        String value = "";
        if (StringUtils.nonBlank(orgCode) && instance().hget(SystemCacheEnum.CACHE_KEY_ORGNAME.key, orgCode) != null) {
            value = Objects.toString(instance().hget(SystemCacheEnum.CACHE_KEY_ORGNAME.key, orgCode));
        }
        return value;
    }

    /**
     * 通过缓存获取登录码对应的用户名
     *
     * @param loginCode 登录码
     * @return 用户名
     */
    public static String getUserName(String loginCode) {
        String value = "";
        if (StringUtils.nonBlank(loginCode) && instance().hget(SystemCacheEnum.CACHE_KEY_USERNAME.key, loginCode) != null) {
            value = Objects.toString(instance().hget(SystemCacheEnum.CACHE_KEY_USERNAME.key, loginCode));
        }
        return value;
    }

}