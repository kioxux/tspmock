package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.dto.AdminSmLogDto;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmLogEntity;
import cn.com.yusys.yusp.oca.domain.form.AdminSmLogForm;
import cn.com.yusys.yusp.oca.domain.query.AdminSmLogQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmLogPojo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmLogVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * 系统操作日志表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-02 22:18:19
 */
public interface AdminSmLogService extends IService<AdminSmLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 分页条件查询日志表
     *
     * @param adminSmLogForm: {"logTypeId":"7","user":"40","operObjId":"111","beginTime":"2020-12-06","endTime":"2020-12-07"}
     * @return
     */
    IPage<AdminSmLogVo> pageLogByCondition(AdminSmLogForm adminSmLogForm);

    /**
     * 异步记录日志
     *
     * @param logDto 日志传输对象
     * @return R对象
     */
    ResultDto addLog(AdminSmLogDto logDto);

    /**
     * excel 导出
     *
     * @param params
     * @return
     */
    File export(Map<String, String> params);

    /**
     * 异步 excel 导出
     *
     * @param params
     * @return
     */
    File asyncExportFile(Map<String, String> params);

    /**
     * @description： 批量插入
     * @author： lty
     * @date： 2021/1/14
     */
    int insertBatch(List<AdminSmLogEntity> list);

    /**
     * 导出匹配查询接口
     *
     * @param logQuery
     */
    List<AdminSmLogEntity> selectByForm(AdminSmLogQuery logQuery, IPage<AdminSmLogEntity> page);

    /**
     * 批量保存的方法
     *
     * @param entityList
     * @return
     */
    Integer saveBatchByEntity(List<AdminSmLogPojo> entityList);

    /**
     * 异步导出文件
     *
     * @param logQuery
     * @return
     */
    ProgressDto translateFile(AdminSmLogQuery logQuery);

    /**
     * 文件上传，异步导入
     *
     * @param uploadFile
     * @return
     */
    ProgressDto asyncImport(MultipartFile uploadFile);

    /**
     * 导出空模板
     *
     * @return
     */
    File downLoadTemplate();
}

