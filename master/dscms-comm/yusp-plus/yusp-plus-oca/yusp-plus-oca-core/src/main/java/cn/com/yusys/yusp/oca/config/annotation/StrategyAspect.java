package cn.com.yusys.yusp.oca.config.annotation;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.constants.Constants;
import cn.com.yusys.yusp.oca.domain.constants.ResponseAndMessageEnum;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.domain.vo.UserEntityVo;
import cn.com.yusys.yusp.oca.service.AdminSmCrelStraService;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Objects;

/**
 * @program: yusp-plus
 * @description: 认证策略切面
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2021-04-15 17:06
 */
@ConditionalOnBean(name = "userDetailsServiceImpl")
@Slf4j
@Aspect
@Component
public class StrategyAspect {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private AdminSmCrelStraService adminSmCrelStraService;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private AdminSmUserService adminSmUserService;

    @Pointcut("@annotation(cn.com.yusys.yusp.oca.config.annotation.Strategy)")
    public void executeStrategy() {
    }

    @Around("executeStrategy() && args(loginCode,password)")
    public ResultDto before(ProceedingJoinPoint proceedingJoinPoint, String loginCode, String password) {
        //检查是否因为密码输入错误次数过多，而禁止登录
        String loginCodeCountCacheKey = String.format("loginErrorCount:loginCode_%s", loginCode);
        //判断是否存在key,次数不小于限定次数--禁止登录
        Boolean hasKey = stringRedisTemplate.hasKey(loginCodeCountCacheKey);
        if (hasKey) {
            String countStr = stringRedisTemplate.opsForValue().get(loginCodeCountCacheKey);
            if (Integer.parseInt(countStr) >= Constants.SystemUserConstance.DEFAULT_MAX_PASSWORD_ERROR) {
                return ResultDto.error(ResponseAndMessageEnum.USER_FORBIDDEN_LOGIN.getCode(), ResponseAndMessageEnum.USER_FORBIDDEN_LOGIN.getMessage());
            }
        }
//        ResultDto checkResult = null;
        //执行登录前的认证策略
        ResultDto checkResult = adminSmCrelStraService.checkBeforeLogin(loginCode);
        if (!checkResult.getCode().equals(ResponseAndMessageEnum.SUCCESS.getCode())) {
            return ResultDto.error(checkResult.getCode(), checkResult.getMessage());
        }
        //执行登录验密流程
        Object returnValue = null;

        ResultDto<UserEntityVo> resultDto = new ResultDto<>();

        try {
            returnValue = proceedingJoinPoint.proceed(proceedingJoinPoint.getArgs());
            resultDto = (ResultDto<UserEntityVo>) returnValue;
        } catch (Throwable e) {
            log.error("登录方法出错: " + e);
            return ResultDto.error(ResponseAndMessageEnum.BAD_CREDENTIALS.getCode(), ResponseAndMessageEnum.BAD_CREDENTIALS.getMessage());
        }

        //执行认证策略
        if (resultDto.getCode() == ResponseAndMessageEnum.SUCCESS.getCode()) {
            AdminSmUserEntity adminSmUserEntity = adminSmUserService.getOne(new QueryWrapper<AdminSmUserEntity>().eq("LOGIN_CODE", loginCode));
            if (Objects.nonNull(adminSmUserEntity)) {
                String userId = adminSmUserEntity.getUserId();
                ResultDto resultDto1 = new ResultDto();
                resultDto1 = adminSmCrelStraService.checkSuccessLogin(userId, null);

                if (!resultDto1.getCode().equals(ResponseAndMessageEnum.SUCCESS.getCode())) {
                    return resultDto1;
                }

                //更新用户最近登录时间信息
                if (resultDto1.getCode().equals(ResponseAndMessageEnum.SUCCESS.getCode())) {
                    adminSmUserService.updateLoginTime(userId, new Date());
                }

                //查询用户loginCode
                //删除缓存中因为密码输入错误次数过多，而禁止登录的数据
                String loginCodeCountCacheKey1 = String.format("loginErrorCount:loginCode_%s", loginCode);
                //判断是否存在key,有--删除
                Boolean hasKey1 = stringRedisTemplate.hasKey(loginCodeCountCacheKey1);
                if (hasKey1) {
                    stringRedisTemplate.delete(loginCodeCountCacheKey);
                }
            }
        } else {
            //验密出错，增加错误次数
            //密码错误次数校验
            if (resultDto.getCode().equals(ResponseAndMessageEnum.BAD_CREDENTIALS.getCode())) {
                checkResult = adminSmCrelStraService.passwordErrorLimit(loginCode, null);
                return checkResult;
            }
            return resultDto;
        }
        return resultDto;
    }
}
