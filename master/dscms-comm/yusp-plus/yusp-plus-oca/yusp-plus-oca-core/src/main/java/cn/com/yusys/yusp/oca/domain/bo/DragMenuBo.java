package cn.com.yusys.yusp.oca.domain.bo;

import lombok.Data;

/**
 * @author xufy1@yusys.com.cn
 * @desc 拖拽菜单
 * @date 2021-01-14 18:54
 */
@Data
public class DragMenuBo {

    /**
     * 拖动的菜单
     */
    private String dragMenuId;
    /**
     * 参照的菜单
     */
    private String refMenuId;
    /**
     * 相对于参照菜单的前后顺序标识
     */
    private String menuOrder;

}
