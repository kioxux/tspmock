package cn.com.yusys.yusp.oca.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.util.Date;

/**
 *@program: yusp-plus
 *@description: 产品信息Dto
 *@author: wujiangpeng
 *@email: wujp4@yusys.com.cn
 *@create: 2020-11-25 14:11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductInfoDto {

    /**
     * 产品id
     */
    private String productId;
    /**
     * 产品名称
     */
    @NotEmpty(message = "产品名称不能为空")
    private String productName;
    /**
     * 产品描述
     */
    @NotEmpty(message = "产品描述不能为空")
    private String productDesc;
    /**
     * 产品种类0-经营类，1-担保消费类，2-信用消费类，3-综合类
     */
    @NotEmpty(message = "产品种类不能为空")
    private String productCategory;
    /**
     * 产品类型0-额度产品，1-额度项下产品，2-一般贷款产品
     */
    @NotEmpty(message = "产品类型不能为空")
    private String productType;
    /**
     * 产品起始日期
     */
    @NotEmpty(message = "产品起始日期不能为空")

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startDate;
    /**
     * 产品结束日期
     */
    @NotEmpty(message = "产品结束日期不能为空")

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endDate;
    /**
     * 产品经理
     */
    @NotEmpty(message = "产品经理不能为空")
    private String productManage;
    /**
     * 产品状态(0-待生效，1-已生效，2-已失效)
     */
    @NotEmpty(message = "产品状态不能为空")
    private String productStatus;
    /**
     * 主担保方式0-信用，1-抵押，2-质押，3-保证
     */
    @NotEmpty(message = "主担保方式不能为空")
    private String mainGuaranteeType;
    /**
     * 担保方式0-信用，1-抵押，2-质押，3-保证
     */
    @NotEmpty(message = "担保方式不能为空")
    private String guaranteeType;
    /**
     * 最高抵质押率
     */
    @NotEmpty(message = "最高抵质押率不能为空")
    private BigDecimal maxPledgeRate;
    /**
     * 贷款担保限额
     */
    @NotEmpty(message = "贷款担保限额不能为空")
    private BigDecimal loanGuarLimitAmt;
    /**
     * 利率形式0-固定，1-浮动
     */
    @NotEmpty(message = "利率形式不能为空")
    private String interestRateMode;
    /**
     * 利率（月）（%）
     */
    @NotEmpty(message = "利率不能为空")
    private BigDecimal loanRate;
    /**
     * 浮动方式0-利差，1-百分比
     */
    private String floatingMode;
    /**
     * 浮动百分比上限
     */
    private BigDecimal floatingUpperLimit;
    /**
     * 浮动百分比下限
     */
    private BigDecimal floatingLowerLimit;
    /**
     * 固定利率上限（%）
     */
    private BigDecimal fixedRateUpperLimit;
    /**
     * 固定利率下限（%）
     */
    private BigDecimal fixedRateLowerLimit;
    /**
     * 浮动利差上限
     */
    private BigDecimal floatingSpreadUpperLimit;
    /**
     * 浮动利差下限
     */
    private BigDecimal floatingSpreadLowerLimit;
    /**
     * 额度类型00-循环额度，01-非循环额度
     */
    @NotEmpty(message = "额度类型不能为空")
    private String limitType;
    /**
     * 最小授信期限（月）
     */
    @NotEmpty(message = "最小授信期限不能为空")
    private Integer minCreditTerm;
    /**
     * 最大授信期限（月）
     */
    @NotEmpty(message = "最大授信期限不能为空")
    private Integer maxCreditTerm;
    /**
     * 最小贷款期限（月）（含）
     */
    @NotEmpty(message = "最小贷款期限不能为空")
    private Integer minLoanTerm;
    /**
     * 最大贷款期限（月）（含）
     */
    @NotEmpty(message = "最大贷款期限不能为空")
    private Integer maxLoanTerm;
    /**
     * 最低贷款金额
     */
    @NotEmpty(message = "最低贷款金额不能为空")
    private BigDecimal minLoanAmt;
    /**
     * 最高贷款金额
     */
    @NotEmpty(message = "最高贷款金额不能为空")
    private BigDecimal maxLoanAmt;
    /**
     * 发放方式0-一次性发放，1-分次发放，2-追加发放
     */
    @NotEmpty(message = "发放方式不能为空")
    private String giveOuotType;
    /**
     * 审批有效期（天）
     */
    @NotEmpty(message = "审批有效期不能为空")
    private Integer aprvEffiveDays;
    /**
     * 利率调整方式0-每年1月1日调整，1-不调整，2-满一年调整，3-立即调整
     */
    @NotEmpty(message = "利率调整方式不能为空")
    private String inteRateAdjustMode;
    /**
     * 还款宽限期（月）
     */
    @NotEmpty(message = "还款宽限期不能为空")
    private Integer repaymentGracePeriod;
    /**
     * 提前还款最低本金（元）
     */
    @NotEmpty(message = "提前还款最低本金不能为空")
    private BigDecimal repaymentMinPrincipal;
    /**
     * 产品创建时间
     */
    private Date createTime;
    /**
     * 产品更新时间
     */
    private Date updateTime;
    /**
     * 产品信息显示状态(1-逻辑有效，0-逻辑删除)
     */
    private String logicStatus;
    /**
     * 所属机构id
     */
    private String orgId;
    /**
     * 所属机构名称
     */
    private String orgName;
}
