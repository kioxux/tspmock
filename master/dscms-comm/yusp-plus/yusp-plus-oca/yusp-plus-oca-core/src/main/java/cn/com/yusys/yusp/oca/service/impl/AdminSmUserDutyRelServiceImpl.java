package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.GetUserInfoByDutyCodeDto;
import cn.com.yusys.yusp.oca.dao.AdminSmUserDutyRelDao;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserDutyRelEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.service.AdminSmUserDutyRelService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author terry
 * @email tanrui1@yusys.com.cn
 */
@Service("adminSmUserDutyRelService")
@Slf4j
public class AdminSmUserDutyRelServiceImpl extends ServiceImpl<AdminSmUserDutyRelDao, AdminSmUserDutyRelEntity> implements AdminSmUserDutyRelService {


    @Override
    public Set<String> findUserIdsByDutyId(String dutyId) {
        Objects.requireNonNull(dutyId);
        return this.lambdaQuery().eq(AdminSmUserDutyRelEntity::getDutyId, dutyId).select(AdminSmUserDutyRelEntity::getUserId).list().stream().map(AdminSmUserDutyRelEntity::getUserId).collect(Collectors.toSet());
    }

    @Override
    public List<AdminSmUserDutyRelEntity> findUserDutyRelsByUser(AdminSmUserEntity user) {
        Objects.requireNonNull(user.getUserId());
        return this.lambdaQuery().eq(AdminSmUserDutyRelEntity::getUserId, user.getUserId()).list();
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public boolean save(List<AdminSmUserDutyRelEntity> entityList) {
        Integer count = 0;
        for (AdminSmUserDutyRelEntity entity : entityList) {
            LambdaQueryWrapper<AdminSmUserDutyRelEntity> checkWrapper = new QueryWrapper<AdminSmUserDutyRelEntity>().lambda();
            checkWrapper.eq(AdminSmUserDutyRelEntity::getDutyId, entity.getDutyId());
            checkWrapper.eq(AdminSmUserDutyRelEntity::getUserId, entity.getUserId());
            AdminSmUserDutyRelEntity check = this.baseMapper.selectOne(checkWrapper);
            if (Objects.nonNull(check)) {
                throw BizException.error("exist", "50900001", "dutyId:" + entity.getDutyId() + ",userId:" + entity.getUserId());
            }
            log.info("New userDutyRel data: [userId: {},dutyId={}] ", entity.getUserId(), entity.getDutyId());
            count += this.baseMapper.insert(entity);
        }
        return count.equals(entityList.size());
    }

    @Override
    public String getDutyId(String userId) {
        LambdaQueryWrapper<AdminSmUserDutyRelEntity> wrapper = new QueryWrapper<AdminSmUserDutyRelEntity>().lambda();
        wrapper.eq(AdminSmUserDutyRelEntity::getUserId, userId);
        List<AdminSmUserDutyRelEntity> userList = this.baseMapper.selectList(wrapper);
        String dutyId = null;
        if (userList != null && !userList.isEmpty()) {
            dutyId = userList.get(0).getDutyId();
        }
        return dutyId;
    }

    @Override
    public List<String> getUserIds(String dutyId) {
        LambdaQueryWrapper<AdminSmUserDutyRelEntity> wrapper = new QueryWrapper<AdminSmUserDutyRelEntity>().lambda();
        wrapper.eq(AdminSmUserDutyRelEntity::getDutyId, dutyId);
        List<AdminSmUserDutyRelEntity> udList = this.baseMapper.selectList(wrapper);
        List<String> userIds = new ArrayList<>();
        if (udList != null && !udList.isEmpty()) {
            for (AdminSmUserDutyRelEntity ud : udList) {
                userIds.add(ud.getUserId());
            }
            return userIds;
        }
        return null;
    }

    /**
     * @param getUserInfoByDutyCodeDto
     * @return
     * @Descrption:根据岗位编号查询用户信息
     */
    @Override
    public List<AdminSmUserDto> getUserList(GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto) {
        //角色编号
        String dutyCode = getUserInfoByDutyCodeDto.getDutyCode();
        //页码
        Integer pageNum = Optional.ofNullable(getUserInfoByDutyCodeDto.getPageNum()).orElse(1);
        //条数
        Integer pageSize = Optional.ofNullable(getUserInfoByDutyCodeDto.getPageSize()).orElse(10);
        IPage<AdminSmUserDto> iPage = new Page<>(pageNum, pageSize);
        iPage = this.baseMapper.getUserInfoByDutyCode(iPage, dutyCode);
        return iPage.getRecords();
    }
}