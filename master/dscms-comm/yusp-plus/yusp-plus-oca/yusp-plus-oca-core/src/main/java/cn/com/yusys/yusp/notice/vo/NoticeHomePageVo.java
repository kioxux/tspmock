package cn.com.yusys.yusp.notice.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class NoticeHomePageVo {
    /**
     * 公告编号
     */
    @TableId
    private String noticeId;
    /**
     * 公告标题
     */
    private String noticeTitle;
    /**
     * 公告重要程度
     */
    private String noticeLevel;
    /**
     * 有效期至
     */
    private String activeDate;
    /**
     * 是否置顶
     */
    private String isTop;
    /**
     * 置顶有效期
     */
    private String topActiveDate;
    /**
     * 公告内容(存富文本表记录编号)
     */
    private String noticeContent;
    /**
     * 发布状态（状态：对应字典项=NORM_STS C：未发布O：已发布）
     */
    private String pubSts;
    /**
     * 发布时间
     */
    private String pubTime;
    /**
     * 公告发布人编号
     */
    private String pubUserId;
    /**
     * 公告发布人姓名
     */
    private String pubUserName;
    /**
     * 发布机构编号
     */
    private String pubOrgId;
    /**
     * 发布机构名称
     */
    private String pubOrgName;
    /**
     * 创建人编号
     */
    private String creatorId;
    /**
     * 创建人姓名
     */
    private String creatorName;
    /**
     * 创建时间
     */
    private String creatorTime;
    /**
     * 是否阅读
     */
    private String readSts;

}
