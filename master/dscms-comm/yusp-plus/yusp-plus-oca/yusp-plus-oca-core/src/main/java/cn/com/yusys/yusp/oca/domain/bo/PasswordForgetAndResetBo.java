package cn.com.yusys.yusp.oca.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 *@类名 PasswordForgetAndResetBo
 *@描述 用户忘记密码重置密码参数校验传参类
 *@作者 黄勃
 *@时间 2021/9/10 10:39
 **/
@ApiModel(value = "PasswordForgetAndResetBo", description = "用户忘记密码重置密码传参类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PasswordForgetAndResetBo {
    @ApiModelProperty(value = "账号", name = "loginCode", required = true)
    @NotEmpty(message = "账号不能为空")
    private String loginCode;

    @ApiModelProperty(value = "新密码", name = "newPassword", required = true)
    @NotEmpty(message = "新密码不能为空")
    private String newPassword;
}