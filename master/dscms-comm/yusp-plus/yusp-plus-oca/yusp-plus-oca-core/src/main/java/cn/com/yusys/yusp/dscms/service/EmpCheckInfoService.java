/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dscms.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.dscms.domain.CfgScheduleInfo;
import cn.com.yusys.yusp.dscms.domain.EmpAttendInfo;
import cn.com.yusys.yusp.dscms.domain.EmpCheckInfo;
import cn.com.yusys.yusp.dscms.domain.EmpScheduleInfo;
import cn.com.yusys.yusp.dscms.repository.mapper.CfgScheduleInfoMapper;
import cn.com.yusys.yusp.dscms.repository.mapper.EmpAttendInfoMapper;
import cn.com.yusys.yusp.dscms.repository.mapper.EmpCheckInfoMapper;
import cn.com.yusys.yusp.dscms.repository.mapper.EmpScheduleInfoMapper;
import cn.com.yusys.yusp.sequence.service.SequenceConfigService;
import com.github.pagehelper.PageHelper;
import org.apache.poi.util.SystemOutLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: EmpCheckInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-22 11:19:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class EmpCheckInfoService {

    @Autowired
    private EmpCheckInfoMapper empCheckInfoMapper;
    @Autowired
    private EmpAttendInfoMapper empAttendInfoMapper;
    @Autowired
    private CfgScheduleInfoMapper cfgScheduleInfoMapper;
    @Autowired
    private EmpScheduleInfoMapper empScheduleInfoMapper;
    @Autowired
    private SequenceConfigService sequenceConfigService;

    private final Logger log = LoggerFactory.getLogger(EmpCheckInfoService.class);


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public EmpCheckInfo selectByPrimaryKey(String serno) {
        return empCheckInfoMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<EmpCheckInfo> selectAll(QueryModel model) {
        List<EmpCheckInfo> records = (List<EmpCheckInfo>) empCheckInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<EmpCheckInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<EmpCheckInfo> list = empCheckInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     * @创建人: 茂茂
     */

    public int insert(EmpCheckInfo record) {
        String userCode = record.getUserCode();
        String checkOptType = record.getCheckOptType();
        QueryModel queryModel = new QueryModel();
        String curentDate = DateUtils.getCurrDateStr();
        queryModel.addCondition("optDate",curentDate);
        queryModel.addCondition("userCode",userCode);
        List<EmpAttendInfo> list1 = empAttendInfoMapper.selectAttendQj(queryModel);
        if(list1.size() >= 1 ){
            throw BizException.error(null, "999999","\"正在休假员工未销假不可签到签退！\"" + "保存失败！");
        }
        // 1、查询当前最新一条签到签退记录，如果是签到，则不允许继续签到，
        List<EmpCheckInfo> list = empCheckInfoMapper.selectMaxRecodeByUserCode(userCode);
       //如果是签到则校验最新一条状态，签退不校验可以重复签退。
        if( CmisFlowConstants.CHECK_OPT_TYPE_01.equals(checkOptType)){
            if (list.size() > 0 && CmisFlowConstants.CHECK_OPT_TYPE_01.equals(list.get(0).getCheckOptType())) {
                throw BizException.error(null, "999999","\"当前是签到状态，请勿重复签到\"" + "保存失败！");
            }
        }
        // 3、如果提前签退则走审批(先查询排班表，通过排班表查询今天的工作时间区间，不在区间内正常签退)
        //String aa=empCheckInfoMapper.checkOptType(record);

        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        // 申请人
        record.setInputId(userInfo.getLoginCode());
        // 申请机构
        record.setInputBrId(userInfo.getOrg().getCode());
        // 申请时间
        record.setInputDate(DateUtils.getCurrDateStr());
        // 创建时间
        record.setCreateTime(DateUtils.getCurrTimestamp());
        // 操作时间
        record.setOptDate(DateUtils.getCurrDateStr());
        // 操作日期
        record.setOptTime(DateUtils.getCurrTimestamp());

        return empCheckInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(EmpCheckInfo record) {
        return empCheckInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(EmpCheckInfo record) {
        return empCheckInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(EmpCheckInfo record) {
        return empCheckInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return empCheckInfoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return empCheckInfoMapper.deleteByIds(ids);
    }

    /**
     * 流程发起进行的业务处理
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【1-审批中】
     * 1、更新申请主表的审批状态-111
     *
     * @param serno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDZDataAfterStart(String serno) {
        try {
            if (StringUtils.isBlank(serno)) {
                throw new YuspException("IQPWH0001", "业务申请流程处理异常-参数缺失！");
            }

            log.info("流程发起-获取提前签退申请" + serno + "申请主表信息");
            EmpCheckInfo empCheckInfo = empCheckInfoMapper.selectByPrimaryKey(serno);
            if (empCheckInfo == null) {
                throw new YuspException("IQPWH000003", "业务申请流程处理异常-申请主表不存在！");
            }

            int updateCount = 0;

            log.info("流程发起-更新提前签退申请" + serno + "流程审批状态为【111】-审批中");
            empCheckInfo.setApproveStatus(CmisFlowConstants.WF_STATUS_111);
            updateCount = empCheckInfoMapper.updateByPrimaryKey(empCheckInfo);
            if (updateCount < 0) {
                throw new YuspException("IQPWH000007", "业务申请流程处理异常-更新业务主表申请状态异常！");
            }


        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("提前签退业务申请流程发起业务处理发生异常！", e);
            throw new YuspException("IQPWH000008", "业务申请流程处理异常！");
        }
    }

    /**
     * 流程发起进行的业务处理
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【1-审批中】
     * 1、更新申请主表的审批状态-997 提前签到审批成功
     *
     * @param serno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDZDataAfterEnd(String serno) {
        try {
            if (StringUtils.isBlank(serno)) {
                throw new YuspException("IQPWH0001", "业务申请流程处理异常-参数缺失！");
            }

            log.info("流程发起-获取提前签退申请" + serno + "申请主表信息");
            EmpCheckInfo empCheckInfo = empCheckInfoMapper.selectByPrimaryKey(serno);
            if (empCheckInfo == null) {
                throw new YuspException("IQPWH000003", "业务申请流程处理异常-申请主表不存在！");
            }

            empCheckInfo.setDataSource("03");
            empCheckInfo.setApproveStatus("997");

            int updateCount = 0;
            updateCount = empCheckInfoMapper.updateByPrimaryKey(empCheckInfo);

            if (updateCount < 0) {
                throw new YuspException("IQPWH000007", "业务申请流程处理异常-更新业务主表申请状态异常！");
            }


        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("提前签退申请流程发起业务处理发生异常！", e);
            throw new YuspException("IQPWH000008", "业务申请流程处理异常！");
        }
    }

    /**
     * 签到状态检查，两位标识，第一位标识提前签到，第二位标识未休假状态，如11，1是0否；
     * 1、检查是否在排班时间内，如不在排班时间内，返回标识1是0否 提前签到
     * 2、检查是否在休假时间范围内，如在，返回标识1是0否 提前签到
     * @author xuchi
     * @param userId
     */
    public String checkOutUserStatus(String  userId) {
        String returnString ="00";
        return returnString;
    }


    //    private static final String JJZY_DUTY_CODE = ""; // 集中作业岗位编码
    /**
     * 异步调起签到任务
     * @param dateTimeStr
     */
    @Async
    public void checkinTimer(String dateTimeStr){
        // 1、查询当前时间所处的排班时间段区间
        String currTime = dateTimeStr.substring(11, 16);
        List<CfgScheduleInfo> cfgList = cfgScheduleInfoMapper.selectLastSchedules(currTime);
        if(CollectionUtils.nonEmpty(cfgList)){
            List<String> scheduleArray = new ArrayList();
            for (CfgScheduleInfo cfgScheduleInfo:cfgList) {
                scheduleArray.add(cfgScheduleInfo.getScheduleTimeYype());
            }
            // 2、查询出本区间需要执行系统签到的人员
            /**
             * 规则：
             * 1、岗位为集中作业岗
             * 2、当日未请假（审批通过的）
             * 3、最近一次签到签退状态为【签退】或者最近一次签到签退状态为非当日的【提前签退】
             * 4、有排班计划
             *
             */
            QueryModel model = new QueryModel();
            model.addCondition("currYearMonth", dateTimeStr.substring(0, 7));
//            model.addCondition("dutyDate", dateTimeStr.substring(0, 10).replaceAll("-", ""));
            model.addCondition("currDate", dateTimeStr.substring(0, 10));
            model.addCondition("scheduleTimeYypes", org.apache.commons.lang.StringUtils.join(scheduleArray.toArray(), ","));
//            model.addCondition("", JJZY_DUTY_CODE);
            List<EmpScheduleInfo> empScheduleInfoList = empScheduleInfoMapper.selectSysCheckinUsersByModel(model);

            // 遍历每个需要执行系统签到的人员，插入数据至签到签退表
            if(CollectionUtils.nonEmpty(empScheduleInfoList)){
                for (EmpScheduleInfo empScheduleInfo: empScheduleInfoList) {
                    EmpCheckInfo empCheckInfo = new EmpCheckInfo();
                    java.util.Random random = new  java.util.Random();
                    int a = random.nextInt(9000)+1000;
                    SimpleDateFormat df = new SimpleDateFormat("yyyyMMddhhmmssSSS");
                    String dateString = df.format(new Date());
                    String serno = "XTQD" + dateString + a ;
                    empCheckInfo.setSerno(serno);
                    empCheckInfo.setUserCode(empScheduleInfo.getUserCode());
                    empCheckInfo.setUserName(empScheduleInfo.getUserName());
                    empCheckInfo.setCheckOptType("01");// 签到
                    empCheckInfo.setDataSource("04"); // 系统自动签到成功
                    empCheckInfo.setOptDate(dateTimeStr.substring(0, 10));
                    empCheckInfo.setOptTime(new Date());
                    empCheckInfo.setApproveStatus("997"); // 审批状态默认通过
                    if(log.isDebugEnabled()){
                        log.debug("系统轮询，集中作业人员：{}，{}，执行系统签到！", empScheduleInfo.getUserCode(), empScheduleInfo.getUserName());
                    }
                    empCheckInfoMapper.insert(empCheckInfo);
                }
            }
        }
    }
}
