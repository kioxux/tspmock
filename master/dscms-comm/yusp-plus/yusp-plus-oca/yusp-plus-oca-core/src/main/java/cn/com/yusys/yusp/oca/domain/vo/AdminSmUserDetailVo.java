package cn.com.yusys.yusp.oca.domain.vo;

import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

/**
 * 用户详情实体
 */
@Data
public class AdminSmUserDetailVo {

    private String userId;
    /**
     * 账号
     */
    private String loginCode;
    /**
     * 姓名
     */
    private String userName;
    /**
     * 证件类型
     */
    private String certType;
    /**
     * 证件号码
     */
    private String certNo;
    /**
     * 员工号
     */
    private String userCode;
    /**
     * 有效期到
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date deadline;
    /**
     * 所属机构编号
     */
    private String orgId;

    /**
     * 所属机构编号
     */
    private String belgMainOrgId;

    /**
     * 所属机构编号
     */
    private String belgMainOrgName;

    private String orgName;
    /**
     * 所属部门编号
     */
    private String dptId;

    private String dptName;

    /**
     * 性别
     */
    private String userSex;
    /**
     * 生日
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date userBirthday;
    /**
     * 邮箱
     */
    private String userEmail;
    /**
     * 移动电话
     */
    private String userMobilephone;
    /**
     * 办公电话
     */
    private String userOfficetel;
    /**
     * 学历
     */
    private String userEducation;
    /**
     * 资格证书
     */
    private String userCertificate;
    /**
     * 入职日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String entrantsDate;
    /**
     * 任职时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String positionTime;
    /**
     * 从业时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String financialJobTime;
    /**
     * 职级
     */
    private String positionDegree;
    /**
     * 用户头像
     */
    private String userAvatar;

    /**
     * 状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效
     */
    private AvailableStateEnum userSts;
    /**
     * 最近登录时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastLoginTime;
    /**
     * 最近一次修改密码时间
     */

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastEditPassTime;
    /**
     * 最新变更用户
     */
    private String lastChgUsr;
    /**
     * 最新变更时间
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;




}
