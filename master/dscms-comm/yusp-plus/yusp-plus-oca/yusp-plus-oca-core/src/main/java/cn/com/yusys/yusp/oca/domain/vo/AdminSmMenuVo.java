package cn.com.yusys.yusp.oca.domain.vo;

import lombok.Data;

@Data
public class AdminSmMenuVo {
    /**
     * 菜单编号
     */
    private String menuId;
    /**
     * 上级菜单编号
     */
    private String upMenuId;
    /**
     * 菜单名称
     */
    private String menuName;
    /**
     * 上级菜单名称
     */
    private String upMenuName;
    /**
     * 业务功能编号
     */
    private String funcId;
    /**
     * 是否已经选择的字段
     */
    private int mark;
}
