/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dscms.service;

import java.util.List;

import cn.com.yusys.yusp.dscms.domain.CfgScheduleInfo;
import cn.com.yusys.yusp.dscms.repository.mapper.CfgScheduleInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgScheduleInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-23 10:05:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgScheduleInfoService {

    @Autowired
    private CfgScheduleInfoMapper cfgScheduleInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CfgScheduleInfo selectByPrimaryKey(String serno) {
        return cfgScheduleInfoMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CfgScheduleInfo> selectAll(QueryModel model) {
        List<CfgScheduleInfo> records = (List<CfgScheduleInfo>) cfgScheduleInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CfgScheduleInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgScheduleInfo> list = cfgScheduleInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CfgScheduleInfo record) {
        return cfgScheduleInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CfgScheduleInfo record) {
        return cfgScheduleInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CfgScheduleInfo record) {
        return cfgScheduleInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CfgScheduleInfo record) {
        return cfgScheduleInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cfgScheduleInfoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgScheduleInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: batchUpdate
     * @方法描述: 批量修改
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int batchUpdate(List<CfgScheduleInfo> cfgScheduleInfolist) {
        int result = 0;
        for (CfgScheduleInfo cfgScheduleInfo:cfgScheduleInfolist) {
            result = cfgScheduleInfoMapper.updateByPrimaryKey(cfgScheduleInfo);
        }
        return result;
    }

    public CfgScheduleInfo queryByScheduleTimeYype(String scheduleTimeYype) {
        CfgScheduleInfo cfgScheduleInfo = cfgScheduleInfoMapper.queryByScheduleTimeYype(scheduleTimeYype);
        return cfgScheduleInfo;
    }
}
