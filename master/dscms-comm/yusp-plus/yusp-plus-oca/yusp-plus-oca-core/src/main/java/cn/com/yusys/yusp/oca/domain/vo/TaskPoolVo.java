package cn.com.yusys.yusp.oca.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *@program: yusp-plus
 *@description: 任务池vo
 *@author: wujiangpeng
 *@email: wujp4@yusys.com.cn
 *@create: 2020-12-01 15:57
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskPoolVo {
    private String taskPoolId;
    private String taskPoolName;
}
