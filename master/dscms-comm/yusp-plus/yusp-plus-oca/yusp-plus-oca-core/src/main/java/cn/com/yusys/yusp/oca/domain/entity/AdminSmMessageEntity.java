package cn.com.yusys.yusp.oca.domain.entity;

import cn.com.yusys.yusp.oca.config.annotation.RedisDictTranslator;
import cn.com.yusys.yusp.oca.domain.constants.Constants;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 提示信息管理表
 * 
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-16 11:19:42
 */
@Data
@TableName("admin_sm_message")
public class AdminSmMessageEntity implements Serializable {


	private static final long serialVersionUID = 1L;

	/**
	 * 消息编号
	 */
	@TableId
	private String messageId;
	/**
	 * 信息码
	 */
	private String code;
	/**
	 * 信息级别:success成功 info信息 warning警告 error错误
	 */
	private String messageLevel;
	/**
	 * 提示内容
	 */
	private String message;
	/**
	 * 消息类别：COMINFO系统级通用提示 DBERR数据库错误提示 MODULEINFO模块提示
	 */
	private String messageType;
	/**
	 * 所属模块名称
	 */
	private String funcName;
	/**
	 * 最后修改用户
	 */
	@RedisDictTranslator(redisCacheKey = Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_USER_NAME,fieldName = "userName")
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
	/**
	 * 最后修改时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

}
