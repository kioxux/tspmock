/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dscms.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dscms.domain.EmpCheckInfo;
import cn.com.yusys.yusp.dscms.service.EmpCheckInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: EmpCheckInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-22 11:19:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/empcheckinfo")
public class EmpCheckInfoResource {
    @Autowired
    private EmpCheckInfoService empCheckInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<EmpCheckInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<EmpCheckInfo> list = empCheckInfoService.selectAll(queryModel);
        return new ResultDto<List<EmpCheckInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<EmpCheckInfo>> index(QueryModel queryModel) {
        List<EmpCheckInfo> list = empCheckInfoService.selectByModel(queryModel);
        return new ResultDto<List<EmpCheckInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<EmpCheckInfo> show(@PathVariable("serno") String serno) {
        EmpCheckInfo empCheckInfo = empCheckInfoService.selectByPrimaryKey(serno);
        return new ResultDto<EmpCheckInfo>(empCheckInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<EmpCheckInfo> create(@RequestBody EmpCheckInfo empCheckInfo) throws URISyntaxException {
        empCheckInfoService.insert(empCheckInfo);
        return new ResultDto<EmpCheckInfo>(empCheckInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody EmpCheckInfo empCheckInfo) throws URISyntaxException {
        int result = empCheckInfoService.update(empCheckInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = empCheckInfoService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = empCheckInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 签到前检查当前日期是否在休假且未销假
     *
     * @return
     */
    @PostMapping("/checkattendinfo")
    protected ResultDto<Integer> checkAttendInfo(@RequestBody EmpCheckInfo empCheckInfo){

        empCheckInfoService.insert(empCheckInfo);
        return new ResultDto<Integer>();
    }
    /**
     * 签到状态检查，两位标识，第一位标识提前签到，第二位标识未休假状态，如11，1是0否；
     * 1、检查是否在排班时间内，如不在排班时间内，返回标识1是0否
     * 2、检查是否在休假时间范围内，如在，返回标识1是0否
     * 提供给前端跳转不通页面
     */
    @PostMapping("/checkoutuserstatus")
    protected String checkOutUserStatus(@RequestBody String  userId) {
        return "11";
    }

    /**
     * 签退状态检查,两位标识，第一位标识提前签退，第二位标识未休假状态，如11，1是0否；
     * 1、检查是否在排班时间内，如在排班时间内，返回标识1
     * 提供给前端跳转不通页面
     */




    /**
     * 签到轮询接口
     * @return
     */
    @GetMapping("/timer/checkin")
    protected ResultDto<Void> checkInTimer(){
        String dateTimeStr = DateUtils.getCurrDateTimeStr();
        empCheckInfoService.checkinTimer(dateTimeStr);
        return new ResultDto<>();
    }

}
