package cn.com.yusys.yusp.oca.domain.constants;

/**
 * @Classname PasswordEnum
 * @Description 密码相关枚举类
 * @Date 2020/11/3 17:58
 * @Created by wujp4@yusys.com.cn
 */
public enum PasswordEnum {

    SUCCESSFULLY_CHECKED("00000000","重置密码成功"),
    NO_STRATEGY("1002","没有相应的密码策略，暂不需要校验密码"),
    UNSUCCESSFULLY_CHECKED("1003","没有通过校验策略"),
    UNSUCCESSFULLY_DECRYPTED("1004","密文解密失败"),
    PASSWORD_CIPHER("2","密码类型为密文"),
    INITIAL_PASSWORD("R/Wny5UD6luZ9SqOFd9xQesW1VZ0py/Zjpab9QLVuFFr6GYa52Je/nV+tjNfsH4TyhDNPTGS3YNax0GMuFQT3N3XbuTqF7JJXkexotmNBmpdHUnOXTpMYnicYek0AoHq3FR3rh2qrKgT/jFJQgCWJeVqXyrLGo7Zno4abi4HULo=","用户默认密码");

    //可以看出这在枚举类型里定义变量和方法和在普通类里面定义方法和变量没有什么区别。
    //唯一要注意的只是变量和方法定义必须放在所有枚举值定义的后面，否则编译器会给出一个错误。
    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



    PasswordEnum(String code, String message){ //加上public void 上面定义枚举会报错 The constructor Color(int, String) is undefined
        this.code=code;
        this.message=message;

    }
}
