package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.oca.domain.entity.ApplyCustomRelationshipInfoEntity;
import cn.com.yusys.yusp.common.query.PageQuery;
import lombok.Data;

@Data
public class RelQuery extends PageQuery<ApplyCustomRelationshipInfoEntity> {

    private String applyId;

    private String cusId;
}
