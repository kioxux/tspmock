package cn.com.yusys.yusp.oca.config.validation;

import javax.validation.groups.Default;
/**
 * 参数校验分组:更新
 * 若不继承Default，实体参数校验时将不校验未分组字段
 */
public interface Update  extends Default {
}
