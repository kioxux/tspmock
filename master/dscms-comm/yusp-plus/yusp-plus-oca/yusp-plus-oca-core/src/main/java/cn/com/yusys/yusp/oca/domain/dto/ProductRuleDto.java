package cn.com.yusys.yusp.oca.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 *@program: yusp-plus
 *@description: 产品附加规则Vo
 *@author: wujiangpeng
 *@email: wujp4@yusys.com.cn
 *@create: 2020-12-03 16:59
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductRuleDto {
    private String productId;
    @JsonProperty("ruleId")
    private String addValue;
    @JsonProperty("ruleName")
    private String addName;
    @JsonProperty("ruleDesc")
    private String addDesc;
}
