package cn.com.yusys.yusp.oca.passwordstrategy;

import org.passay.Rule;
import org.springframework.beans.factory.InitializingBean;

import java.util.List;

/**
 * 密码策略
 */
public interface Handler extends InitializingBean {

    public List<Rule> getPasswordStrategy(String name, String detail);
}
