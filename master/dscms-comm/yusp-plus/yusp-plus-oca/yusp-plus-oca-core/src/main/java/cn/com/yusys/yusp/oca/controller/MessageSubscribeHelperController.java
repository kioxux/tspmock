package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmDutyEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmOrgEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmRoleEntity;
import cn.com.yusys.yusp.oca.service.AdminSmDutyService;
import cn.com.yusys.yusp.oca.service.AdminSmOrgService;
import cn.com.yusys.yusp.oca.service.AdminSmRoleService;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import cn.com.yusys.yusp.oca.domain.vo.UserSubscribeVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author danyu
 */

@RestController
@RequestMapping("/api")
@Slf4j
public class MessageSubscribeHelperController {

    @Autowired
    AdminSmDutyService adminSmDutyService;

    @Autowired
    AdminSmOrgService adminSmOrgService;

    @Autowired
    AdminSmUserService adminSmUserService;

    @Autowired
    AdminSmRoleService adminSmRoleService;

    @GetMapping("/selectAllOrg")
    public ResultDto selectAllOrg() {
        List<AdminSmOrgEntity> list = adminSmOrgService.list();

        return ResultDto.success(list).code(0).total(list.size());
    }

    @GetMapping("/selectAllUser")
    public ResultDto selectUserSubscribeVoList() {
        List<UserSubscribeVo> list = adminSmUserService.selectUserSubscribeVoList();

        return ResultDto.success(list).code(0).total(list.size());
    }

    @GetMapping("/selectAllRole")
    public ResultDto selectAllRole() {
        List<AdminSmRoleEntity> list = adminSmRoleService.list();

        return ResultDto.success(list).code(0).total(list.size());
    }

    @GetMapping("/selectAllDuty")
    public ResultDto selectAllDuty() {
        List<AdminSmDutyEntity> list = adminSmDutyService.list();

        return ResultDto.success(list).code(0).total(list.size());
    }
}
