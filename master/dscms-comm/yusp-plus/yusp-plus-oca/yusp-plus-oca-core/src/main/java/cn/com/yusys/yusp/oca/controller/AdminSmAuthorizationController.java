package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.form.AdminSmAuthTreeForm;
import cn.com.yusys.yusp.oca.domain.form.AdminSmAuthUpdateForm;
import cn.com.yusys.yusp.oca.domain.form.AdminTmplAuthForm;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmAuthTreeVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmResContrAuthVo;
import cn.com.yusys.yusp.oca.service.AdminSmAuthorizationService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/authorization")
public class AdminSmAuthorizationController {

    @Autowired
    private AdminSmAuthorizationService adminSmAuthorizationService;

    /**
     * 获取数据权限模板授权页面
     * @param form：包含 userID|roleId，两个只能有一个
     * userID：授权对象的主键 userId
     * roleId：授权角色的主键 roleId
     * @return
     */
    @PostMapping("/authList")
    public ResultDto getAuthTmplList(@RequestBody AdminSmAuthTreeForm form) {
        IPage<AdminSmResContrAuthVo> result = adminSmAuthorizationService.getAuthTmplList(form);
        return ResultDto.success().data(result.getRecords()).total(result.getTotal());
    }

    /**
     * 获取权限树
     * @param adminSmAuthTreeForm：包含 userID|roleId，两个只能有一个
     * userID：授权对象的主键 userId
     * roleId：授权角色的主键 roleId
     * @return
     */
    @PostMapping("/treeQuery")
    public ResultDto treeQuery(@RequestBody AdminSmAuthTreeForm adminSmAuthTreeForm) {
        List<AdminSmAuthTreeVo> voList = adminSmAuthorizationService.getTreeQuery(adminSmAuthTreeForm);
        return ResultDto.success(voList);
    }


    /**
     * 修改被授权人权限
     * @param adminSmAuthUpdateForm
     * @return
     */
    @PostMapping("/saveAuth")
    public ResultDto saveAuth(@RequestBody AdminSmAuthUpdateForm adminSmAuthUpdateForm) {
        adminSmAuthorizationService.saveAuth(adminSmAuthUpdateForm);
        return ResultDto.success();
    }
    /**
     * 复制拷贝权限
     * @param adminSmAuthUpdateForm
     * @return
     */
    @PostMapping("/copyAuth")
    public ResultDto copyAuth(@RequestBody AdminSmAuthUpdateForm adminSmAuthUpdateForm) {
        adminSmAuthorizationService.copyAuth(adminSmAuthUpdateForm);
        return ResultDto.success();
    }

    /**
     * 给被授权对象修改控制点的数据模板
     * @param form
     * @return
     */
    @PostMapping("/saveTmplAuth")
    public ResultDto saveTmplAuth(@RequestBody AdminTmplAuthForm form) {
        adminSmAuthorizationService.saveTmplAuth(form);
        return ResultDto.success();
    }
}
