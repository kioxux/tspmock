package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmLookupDictBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmLookupDictEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmLookupDictQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmLookupDictVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 数据字典内容表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2021-01-15 16:44:33
 */
public interface AdminSmLookupDictService extends IService<AdminSmLookupDictEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 数据字典 列表查询
     *
     * @param adminSmLookupDictQuery
     * @return
     */
    Page<AdminSmLookupDictVo> queryLookupDictWithCondition(AdminSmLookupDictQuery adminSmLookupDictQuery);

    /**
     * 字典项保存
     *
     * @param adminSmLookupDictBo
     * @return
     */
    void saveLookupDictByDictBo(AdminSmLookupDictBo adminSmLookupDictBo);

    /**
     * 修改字典项
     *
     * @param adminSmLookupDictBo
     * @return
     */
    void updateLookupDictByDictBo(AdminSmLookupDictBo adminSmLookupDictBo);

    /**
     * batch del  字典项
     *
     * @param ids
     * @return
     */
    void removeLookupDictByIds(String[] ids);

    /**
     * 字典详情
     *
     * @param lookupItemId
     * @return
     */
    List<AdminSmLookupDictVo> queryLookupDictInfoById(String lookupItemId);

    /**
     * 初始化字典分类
     *
     * @return
     */
    List<AdminSmLookupDictVo> queryInitLookupDict();

    /**
     * 插入字典项
     *
     * @param adminSmLookupDictBo
     * @return
     */
    void insertLookupDictByDictBo(AdminSmLookupDictBo adminSmLookupDictBo);

    /**
     * 刷新数据字典缓存
     */
    void refreshLookupDict();

    /**
     * 查询数据字典
     *
     * @param lookupCodes
     * @return
     */
    Map<String, List<Map<String, String>>> querylookupcode(String lookupCodes);

}
