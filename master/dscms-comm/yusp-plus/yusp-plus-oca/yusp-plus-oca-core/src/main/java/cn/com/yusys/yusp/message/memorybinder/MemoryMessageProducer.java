package cn.com.yusys.yusp.message.memorybinder;

import org.springframework.cloud.stream.provisioning.ConsumerDestination;
import org.springframework.integration.endpoint.MessageProducerSupport;

/**
 * 内存模式消息生产者
 *
 * @author xiaodg@yusys.com.cn
 */
public class MemoryMessageProducer extends MessageProducerSupport {

    private final ConsumerDestination destination;

    public MemoryMessageProducer(ConsumerDestination destination) {
        this.destination = destination;
    }

    /**
     * 应用程序启动之后会执行到这里, 又自定义逻辑可添加到这里
     */
    @Override
    public void doStart() {
    }

}
