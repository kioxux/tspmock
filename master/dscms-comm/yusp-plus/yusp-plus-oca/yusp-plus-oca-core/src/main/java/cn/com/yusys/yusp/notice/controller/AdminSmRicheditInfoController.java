package cn.com.yusys.yusp.notice.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.notice.entity.AdminSmRicheditInfoEntity;
import cn.com.yusys.yusp.notice.service.AdminSmRicheditInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;


/**
 * 富文本信息表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-15 13:05:43
 */
@Slf4j
@RestController
@RequestMapping("/api/notice/adminsmricheditinfo")
public class AdminSmRicheditInfoController {
    @Autowired
    private AdminSmRicheditInfoService adminSmRicheditInfoService;

    /**
     * 信息
     */
    @RequestMapping("/info/{richeditId}")
    public ResultDto info(@PathVariable("richeditId") String richeditId){
		AdminSmRicheditInfoEntity adminSmRicheditInfo = adminSmRicheditInfoService.getById(richeditId);
        return ResultDto.success().extParam("adminSmRicheditInfo", adminSmRicheditInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public ResultDto save(@RequestBody AdminSmRicheditInfoEntity adminSmRicheditInfo){
		adminSmRicheditInfoService.save(adminSmRicheditInfo);
        return ResultDto.success();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public ResultDto update(@RequestBody AdminSmRicheditInfoEntity adminSmRicheditInfo){
		adminSmRicheditInfoService.updateById(adminSmRicheditInfo);
        return ResultDto.success();
    }

    /**
     * 删除公告
     */
    @PostMapping("/delete")
    public ResultDto delete(@RequestBody String[] richeditIds){
		adminSmRicheditInfoService.removeByIds(Arrays.asList(richeditIds));
        return ResultDto.success();
    }
}
