package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.vo.UserEntityVo;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @program: yusp-plus
 * @description: 获取用户信息
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2021-03-30 09:53
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/userinfo")
public class GetUserInformationController {

    private final AdminSmUserService adminSmUserService;

    @GetMapping("/getbyphonenumber")
    public ResultDto<UserEntityVo> getByPhoneNumber(@RequestParam("userMobilephone") String userMobilephone) {
        ResultDto<UserEntityVo> result = adminSmUserService.getByPhoneNumber(userMobilephone);
        return result;
    }
}
