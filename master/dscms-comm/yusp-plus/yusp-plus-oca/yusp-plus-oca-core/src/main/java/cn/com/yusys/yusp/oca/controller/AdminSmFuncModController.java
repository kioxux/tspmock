package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmFuncModBo;
import cn.com.yusys.yusp.oca.domain.query.AdminSmFuncModQuery;
import cn.com.yusys.yusp.oca.service.AdminSmFuncModService;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmFuncModVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 系统功能模块表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-26 10:50:57
 */
@RestController
@RequestMapping("/api/adminsmfuncmod")
public class AdminSmFuncModController {
    @Autowired
    private AdminSmFuncModService adminSmFuncModService;

    /**
     * 列表，查询模块信息
     */
    @GetMapping("/querymod")
    public ResultDto<AdminSmFuncModVo> list(AdminSmFuncModQuery adminSmFuncModQuery) {

        return ResultDto.success(adminSmFuncModService.queryPageWithCondition(adminSmFuncModQuery));
    }


    /**
     * 信息
     */
    @GetMapping("/info/{modId}")
    public ResultDto<Object> info(@PathVariable("modId") String modId) {
        return ResultDto.success(adminSmFuncModService.getById(modId));
    }

    /**
     * 保存 新增模块信息
     */
    @PostMapping("/createmod")
    public ResultDto<Object> save(@RequestBody @Validated AdminSmFuncModBo adminSmFuncBo) {


        int resultInt = adminSmFuncModService.saveFuncMod(adminSmFuncBo);
        if (resultInt > 0) {
            return ResultDto.success().message("新增" + adminSmFuncBo.getModName() + "模块成功!");
        }
        if (resultInt == -1) {
            return ResultDto.error("模块名称：" + adminSmFuncBo.getModName() + "已存在!");
        }
        return ResultDto.error("新增" + adminSmFuncBo.getModName() + "模块失败!");
    }

    /**
     * 修改模块信息
     */
    @PostMapping("/editmod")
    public ResultDto<Object> update(@RequestBody @Validated AdminSmFuncModBo adminSmFuncBo) {

        int updateId = adminSmFuncModService.updateFuncMod(adminSmFuncBo);
        if (updateId > 0) {
            return ResultDto.success().message("修改" + adminSmFuncBo.getModName() + "模块成功!");
        }
        if (updateId == -1) {
            return ResultDto.error("模块名称：" + adminSmFuncBo.getModName() + "已存在!");
        }
        return ResultDto.error("修改" + adminSmFuncBo.getModName() + "模块失败!");
    }

    /**
     * 删除模块信息
     */
    @PostMapping("/deletemod")
    public ResultDto<Object> delete(@RequestBody String[] modId) {

        int modResult = adminSmFuncModService.removeByModId(modId);
        if (modResult == -1) {
            return ResultDto.error("已关联业务功能!");
        }
        if (modResult > 0) {
            return ResultDto.success().message("删除成功");
        }
        return ResultDto.error("删除失败!");
    }

    /**
     * 保存数据前查询模块名称是否已经存在
     */
    @GetMapping("/checkname")
    public ResultDto<Object> checkName(@RequestParam(required = false) String modName, @RequestParam(required = false) String modId) {
        List<String> funcModIdList = adminSmFuncModService.checkName(modName, modId);
        return ResultDto.success(funcModIdList).total(funcModIdList.size());
    }

}
