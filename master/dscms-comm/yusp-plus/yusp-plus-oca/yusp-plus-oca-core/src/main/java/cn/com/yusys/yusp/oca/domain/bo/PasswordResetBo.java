package cn.com.yusys.yusp.oca.domain.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 *@program: yusp-plus
 *@description: 密码重置Bo
 *@author: wujiangpeng
 *@email: wujp4@yusys.com.cn
 *@create: 2020-12-15 11:34
 */
@ApiModel(description = "用户密码修改接收类", value = "用户密码修改接收类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PasswordResetBo {

    @ApiModelProperty(name = "loginCode", value = "用户名", required = true, example = "test")
    @NotEmpty(message = "登录名不能为空")
    private String loginCode;
}
