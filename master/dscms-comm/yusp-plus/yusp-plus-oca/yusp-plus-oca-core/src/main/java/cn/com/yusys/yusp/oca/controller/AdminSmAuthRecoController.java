package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.bo.MenuAndControlAuthRecoSaveBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmAuthRecoEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmUserQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmAuthRecoVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmUserVo;
import cn.com.yusys.yusp.oca.domain.vo.MenuVo;
import cn.com.yusys.yusp.oca.service.AdminSmAuthRecoService;
import cn.com.yusys.yusp.oca.service.AdminSmMenuService;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * 资源对象授权记录表(含菜单、控制点、数据权限)
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-12-01 10:44:28
 */
@RestController
@RequestMapping("/api/adminsmauthteco")
public class AdminSmAuthRecoController {

    @Autowired
    private AdminSmAuthRecoService adminSmAuthRecoService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private AdminSmMenuService adminSmMenuService;

    @PostMapping("/saveinfo")
    public ResultDto saveMenuAndControlAuthReco(@RequestBody MenuAndControlAuthRecoSaveBo authRecoSaveBo) {
        return adminSmAuthRecoService.saveMenuAndControlAuthReco(authRecoSaveBo);

    }

    /**
     * 信息
     */
    @GetMapping("/info/{authRecoId}")
    public ResultDto info(@PathVariable("authRecoId") String authRecoId) {
        AdminSmAuthRecoEntity adminSmAuthReco = adminSmAuthRecoService.getById(authRecoId);

        return ResultDto.success().extParam("adminSmAuthReco", adminSmAuthReco);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public ResultDto save(@RequestBody AdminSmAuthRecoEntity adminSmAuthReco) {
        adminSmAuthRecoService.save(adminSmAuthReco);

        return ResultDto.success();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public ResultDto update(@RequestBody AdminSmAuthRecoEntity adminSmAuthReco) {
        adminSmAuthRecoService.updateById(adminSmAuthReco);

        return ResultDto.success();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public ResultDto delete(@RequestBody String[] authRecoIds) {
        adminSmAuthRecoService.removeByIds(Arrays.asList(authRecoIds));
        return ResultDto.success();
    }

    /**
     * 分页查询orgId下所有已激活的用户列表,按最后修改时间逆序
     * 实际调用 adminSmUserService.queryPageNew() 接口实现
     */
    @PostMapping("/querypage")
    public ResultDto<AdminSmUserVo> page(AdminSmUserQuery query) {
        return ResultDto.success(adminSmUserService.queryPage(query));
    }

    /**
     * 菜单树初始化查询
     * 实际调用adminSmMenuService.getMenuTree() 接口实现
     * lazy 判断是否支持懒加载
     */
    @GetMapping("/menutreequery")
    public ResultDto getMenuTree(@RequestParam String sysId, boolean lazy, String menuId) {
        List<MenuVo> list = adminSmMenuService.getMenuTree(sysId, lazy, menuId);
        return ResultDto.success().extParam("data", list).extParam("total", list.size());
    }


    /**
     * 数据权限树初始化查询
     */
    @GetMapping("/datapowertreequery")
    public ResultDto selectDataPowerTree(String sysId) {
        List<MenuVo> list = adminSmAuthRecoService.selectDataPowerTree(sysId);
        return ResultDto.success().extParam("data", list).extParam("total", list.size());
    }

    /**
     * 查询对象资源关系数据
     */
    @GetMapping("/queryinfo")
    public ResultDto getRecoInfo(@RequestParam(required = false) String objectType,
                         @RequestParam(required = false) String resType,
                         @RequestParam(required = false) String objectId,
                         @RequestParam(required = false) String sysId) {

        List<AdminSmAuthRecoVo> list = adminSmAuthRecoService.queryRecoInfo(objectType, resType, objectId, sysId);
        return ResultDto.success().extParam("data", list);
    }

    /**
     * 批量保存 数据授权
     */
    @PostMapping("/savedatapowerinfo")
    public ResultDto saveDataPowerInfo(@RequestBody ArrayList<AdminSmAuthRecoEntity> authRecoList) {

        int saveInt = adminSmAuthRecoService.adminSmAuthRecoService(authRecoList);
        if (saveInt >= 1) {
            return ResultDto.success("保存成功!");
        }
        return ResultDto.error("保存失败!");
    }
}
