package cn.com.yusys.yusp.oca.service.component;

import cn.com.yusys.yusp.oca.domain.constants.Constants;
import cn.com.yusys.yusp.oca.utils.RedisUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @program: yusp-plus
 * @description: 清除redis
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2021-02-20 16:43
 */
@RequiredArgsConstructor
@Component
public class ClearRedis implements ApplicationRunner {

    private final RedisUtils redisUtils;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        // 删除控制点权限
        redisUtils.delRedisCache(Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_CONTROL_NAME);
        redisUtils.delRedisCache(Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_CONTROL_KEY);

        // 删除用户信息
        redisUtils.delRedisCache(Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_USER_NAME);

        // 删除机构信息
        redisUtils.delRedisCache(Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_ORG_NAME);

        // 删除部门信息
        redisUtils.delRedisCache(Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_DPT_NAME);

        // 删除系统提示信息
        redisUtils.delRedisCache(Constants.SystemUserConstance.TRANSLATE_REDIS_KEY_SYSTEM_TIP_MESSAGE);
    }
}
