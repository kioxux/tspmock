package cn.com.yusys.yusp.oca.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *@program: yusp-plus
 *@description: 会签规则保存Dto
 *@author: wujiangpeng
 *@email: wujp4@yusys.com.cn
 *@create: 2020-12-04 10:49
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CountersignatureRuleDto {
    private String productId;
    @JsonProperty("countersignatureRuleId")
    private String addValue;
    @JsonProperty("countersignatureRuleName")
    private String addName;
}
