package cn.com.yusys.yusp.message.config;

/**
 * 符号常量
 *
 * @author xiaodg@yusys.com.cn
 **/
public interface SignConstants {

    /**
     * 逗号
     */
    String MARK_COMMA = ",";
}
