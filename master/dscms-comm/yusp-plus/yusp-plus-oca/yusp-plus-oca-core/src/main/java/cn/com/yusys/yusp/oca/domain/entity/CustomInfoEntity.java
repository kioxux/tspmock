package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 客户信息表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-12-02 18:03:52
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("cms_custom_info")
public class CustomInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 客户id
	 */
	@TableId
	private String cusId;
	/**
	 * 客户姓名
	 */
	private String cusName;
	/**
	 * 客户英文名称
	 */
	private String cusEname;
	/**
	 * 证件类型(10110:居民身份证,10119:组织机构代码,20150:护照,99099:其他证件)
	 */
	private String certType;
	/**
	 * 证件号码
	 */
	private String certCode;
	/**
	 * 客户电话
	 */
	private String phone;
	/**
	 * 地址
	 */
	private String address;
	/**
	 * 性别(1:男,2:女,5,其他,9,未知)
	 */
	private String sex;
	/**
	 * 生日
	 */
	private Date birthday;
	/**
	 * 居住状态(1:自置,2:按揭,3:亲属楼宇,4:集体宿舍,5:租房,6:共有住宅,8:其他,9:未知)
	 */
	private String liveStatus;
	/**
	 * 学历状态 1：文盲 2：初中或初中以下 3：高中 4：大专或本科 5：硕士研究生 6：博士或博士后 9：未知)
	 */
	private String eduLevel;
	/**
	 * 婚姻状况 1：已婚 2：未婚 3：离异 4：丧偶 5：再婚 9：未知)
	 */
	private String maritalStatus;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 配偶客户号
	 */
	private String spouseCusId;
	/**
	 * 录入时间
	 */
	private Date inputTime;
	/**
	 * 录入人
	 */
	private String inputUser;
	/**
	 * 更新人
	 */
	private String updateUser;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 家庭月收入
	 */
	private BigDecimal monthlyIncome;
	/**
	 * 客户显示状态（1-逻辑有效，0-逻辑删除）
	 */
	private String logicStatus;

}
