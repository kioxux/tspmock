package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.ImportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.io.IOUtils;
import cn.com.yusys.yusp.oca.service.AdminSmLogService;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmLogPojo;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description: Excel导入
 * @author lty
 * @date 2021/1/13　　
 */
@RequestMapping("api/excelimport")
@RestController
public class ExcelImportController {

    private final AdminSmLogService adminSmLogService;

    public ExcelImportController(AdminSmLogService adminSmLogService) {
        this.adminSmLogService = adminSmLogService;
    }

    @PostMapping("/log")
    public ResultDto ExcelImportLog(MultipartFile file){
        File file1 = new File(file.getOriginalFilename());
        try {
            IOUtils.copy(file.getInputStream(),new FileOutputStream(file1));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 将文件内容导入数据库，StudentScore为导入数据的类
        ProgressDto progressDto = ExcelUtils.asyncImport(ImportContext.of(AdminSmLogPojo.class)
                // 批量操作需要将batch设置为true
                .batch(true)
                .file(file1)
                // 使用batchInsert对数据进行批量操作
                .dataStorage(ExcelUtils.batchConsumer(adminSmLogService::insertBatch)));
        return ResultDto.success(progressDto);
    }
}
