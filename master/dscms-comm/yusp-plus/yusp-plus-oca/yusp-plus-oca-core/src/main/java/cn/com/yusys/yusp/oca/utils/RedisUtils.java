package cn.com.yusys.yusp.oca.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @program: yusp-plus
 * @description: Redis工具类
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2021-02-20 16:09
 */
@Component
public class RedisUtils {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 删除redis中Hash结构缓存，如果删除失败，点击界面上手动刷新缓存
     */
    public void delRedisCache(String redisKey) {
        HashOperations<String, Object, Object> operations = stringRedisTemplate.opsForHash();
        // 1.如果是大key,不建议使用delete
        // operations.delete("datadict");
        // 2.使用scan查询,再删除
        Cursor<Map.Entry<Object, Object>> dictCursor = operations.scan(redisKey, ScanOptions.NONE);
        StringBuffer stringBuffer = new StringBuffer();
        AtomicInteger atomicInt = new AtomicInteger(0);
        while (dictCursor.hasNext()) {
            int i = atomicInt.addAndGet(1);
            stringBuffer.append(dictCursor.next().getKey() + ",");
            if (i % 50 == 0 || !dictCursor.hasNext()) {
                operations.delete(redisKey, stringBuffer.toString().split(","));
            }
        }
        //3.lua 脚本删除
    }

    /**
     * 添加redis Hash结构缓存
     *
     * @param key
     * @param map
     * @return
     */
    public boolean hmset(String key, Map<String, String> map) {
        Assert.notNull(key, "Key must not be null!");
        Assert.notNull(map, "Item must not be null!");
        BoundHashOperations<String, String, String> hashOps = stringRedisTemplate.opsForHash().getOperations().boundHashOps(key);
        hashOps.putAll(map);
        return true;
    }

    /**
     * hash获取缓存值
     *
     * @param key
     * @param item
     * @return
     */
    public Object hget(String key, String item) {
        Assert.notNull(key, "Key must not be null!");
        Assert.notNull(item, "Item must not be null!");
        return stringRedisTemplate.opsForHash().get(key, item);
    }

    /**
     * hash赋值
     *
     * @param key
     * @param item
     * @param value
     * @return
     */
    public boolean hset(String key, String item, Object value) {
        Assert.notNull(key, "Key must not be null!");
        Assert.notNull(item, "Item must not be null!");
        Assert.notNull(value, "Value must not be null!");
        stringRedisTemplate.opsForHash().put(key, item, value);
        return true;
    }

    /**
     * hash删除缓存
     *
     * @param key
     * @param item
     * @return
     */
    public boolean hdel(String key, Object... item) {
        Assert.notNull(key, "Key must not be null!");
        Assert.notNull(item, "Item must not be null!");
        stringRedisTemplate.opsForHash().delete(key, item);
        return true;
    }

}