package cn.com.yusys.yusp.oca.domain.vo;

import lombok.Data;

@Data
public class AdminSmDataAuthTmplVo {

    /**
     * 记录编号
     */
    private String authTmplId;
    /**
     * 数据权限模板名
     */
    private String authTmplName;
    /**
     * 数据权限SQL条件
     */
    private String sqlString;
    /**
     * SQL占位符名称
     */
    private String sqlName;
    /**
     * 可用的控制点记录编号(*表示都可用)
     */
    private int status;
    /**
     * 最新变更用户
     */
    private String lastChgUsr;
    /**
     * 最新变更时间
     */
    private String lastChgDt;
    /**
     * 优先级,值越小优先级越高
     */
    private String priority;
    /**
     * 修改人名称
     */
    private String lastChgName;
}
