/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dscms.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dscms.domain.TaskUrgentDetailInfo;
import cn.com.yusys.yusp.dscms.service.TaskUrgentDetailInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TaskUrgentDetailInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 刘权
 * @创建时间: 2021-04-22 14:53:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/taskurgentdetailinfo")
public class TaskUrgentDetailInfoResource {
    @Autowired
    private TaskUrgentDetailInfoService taskUrgentDetailInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<TaskUrgentDetailInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<TaskUrgentDetailInfo> list = taskUrgentDetailInfoService.selectAll(queryModel);
        return new ResultDto<List<TaskUrgentDetailInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<TaskUrgentDetailInfo>> index(@RequestBody QueryModel queryModel) {
        List<TaskUrgentDetailInfo> list = taskUrgentDetailInfoService.selectByModel(queryModel);
        return new ResultDto<List<TaskUrgentDetailInfo>>(list);
    }
    

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param userCode
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryCurtSurplusQnt/{userCode}")
    protected ResultDto<String> queryCurtSurplusQnt(@PathVariable("userCode") String userCode) {
        String result = taskUrgentDetailInfoService.queryCurtSurplusQnt(userCode);
        return new ResultDto<String>(result);
    }


    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<TaskUrgentDetailInfo> show(@PathVariable("serno") String serno) {
        TaskUrgentDetailInfo taskUrgentDetailInfo = taskUrgentDetailInfoService.selectByPrimaryKey(serno);
        return new ResultDto<TaskUrgentDetailInfo>(taskUrgentDetailInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<TaskUrgentDetailInfo> create(@RequestBody TaskUrgentDetailInfo taskUrgentDetailInfo) throws URISyntaxException {
        taskUrgentDetailInfoService.insert(taskUrgentDetailInfo);
        return new ResultDto<TaskUrgentDetailInfo>(taskUrgentDetailInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody TaskUrgentDetailInfo taskUrgentDetailInfo) throws URISyntaxException {
        int result = taskUrgentDetailInfoService.update(taskUrgentDetailInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = taskUrgentDetailInfoService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = taskUrgentDetailInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:groupSum
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/groupSum")
    protected ResultDto<List<Map<String, Object>>> groupSum(@RequestBody QueryModel queryModel) {
        List<Map<String, Object>> list = taskUrgentDetailInfoService.groupSum(queryModel);
        return new ResultDto<List<Map<String, Object>>>(list);
    }
}
