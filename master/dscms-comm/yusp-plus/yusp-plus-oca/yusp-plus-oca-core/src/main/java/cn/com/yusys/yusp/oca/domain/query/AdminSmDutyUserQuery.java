package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.common.query.PageQuery;
import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmUserDutyRelVo;

public class AdminSmDutyUserQuery extends PageQuery<AdminSmUserDutyRelVo> {

    private String dutyId;

    /**
     * 账号
     */
    private String loginCode;

    /**
     * 姓名
     */
    private String userName;

    /**
     * 员工号
     */
    private String userCode;

    /**
     * 机构名称
     */
    private String orgName;

    /**
     * 机构ID
     */
    private String orgId;

    /**
     * 状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效
     */
    private AvailableStateEnum userSts;

    public String getDutyId() {
        return dutyId;
    }

    public void setDutyId(String dutyId) {
        this.dutyId = dutyId;
    }

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public AvailableStateEnum getUserSts() {
        return userSts;
    }

    public void setUserSts(AvailableStateEnum userSts) {
        this.userSts = userSts;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}