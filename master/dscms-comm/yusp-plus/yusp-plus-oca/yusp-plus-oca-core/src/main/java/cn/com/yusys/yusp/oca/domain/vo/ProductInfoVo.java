package cn.com.yusys.yusp.oca.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 *@program: yusp-plus
 *@description: 产品信息展示Vo
 *@author: wujiangpeng
 *@email: wujp4@yusys.com.cn
 *@create: 2020-11-26 16:44
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductInfoVo {
    /**
     * 产品id
     */
    private String productId;
    /**
     * 产品名称
     */
    private String productName;
    /**
     * 产品类型0-额度产品，1-额度项下产品，2-一般贷款产品
     */
    private String productType;
    /**
     * 产品起始日期
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startDate;
    /**
     * 产品经理
     */
    private String productManage;
    /**
     * 产品状态(0-待生效，1-已生效，2-已失效)
     */
    private String productStatus;/**
     * 所属机构名称
     */
    private String orgName;

}
