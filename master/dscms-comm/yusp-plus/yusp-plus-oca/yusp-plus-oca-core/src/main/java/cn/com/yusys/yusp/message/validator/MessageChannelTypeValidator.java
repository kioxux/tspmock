package cn.com.yusys.yusp.message.validator;


import cn.com.yusys.yusp.message.enumeration.MessageChannelEnum;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 消息渠道校验器
 *
 * @author xiaodg@yusys.com.cn
 **/
public class MessageChannelTypeValidator implements ConstraintValidator<MessageChannelType, String> {

    /**
     * 消息渠道类型集合
     */
    public static final Set<String> TYPES = Stream.of(MessageChannelEnum.values()).map(MessageChannelEnum::getType).collect(Collectors.toSet());

    /**
     * 输入参数是否是合法的消息渠道类型
     *
     * @param value   消息渠道类型字符串 {@code String}
     * @param context 校验器上下文 {@link ConstraintValidatorContext}
     * @return {@code boolean}
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return TYPES.contains(value);
    }
}
