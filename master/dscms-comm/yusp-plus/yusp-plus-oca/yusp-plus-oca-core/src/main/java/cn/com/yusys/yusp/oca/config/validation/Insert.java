package cn.com.yusys.yusp.oca.config.validation;


import javax.validation.groups.Default;

/**
 * 参数校验分组:新增
 * 若不继承Default，实体参数校验时将不校验未分组字段
 */
public interface Insert extends Default {
}
