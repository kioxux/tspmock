package cn.com.yusys.yusp.notice.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 系统公告表接收对象表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-15 13:05:33
 */
@Data
@TableName("admin_sm_notice_recive")
public class AdminSmNoticeReciveEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 记录编号
	 */
	@TableId
	private String reciveId;
	/**
	 * 公告编号
	 */
	private String noticeId;
	/**
	 * 对象类型
	 */
	private String reciveType;
	/**
	 * 对象记录编号
	 */
	private String reciveOgjId;
	/**
	 * 能够访问的角色名称
	 */
	@TableField(exist = false)
	private String reciveRoleId;

	public AdminSmNoticeReciveEntity() {}

	public AdminSmNoticeReciveEntity(String noticeId, String reciveOgjId, String reciveRoleId) {
		this.noticeId = noticeId;
		this.reciveOgjId = reciveOgjId;
		this.reciveRoleId = reciveRoleId;
	}

	public AdminSmNoticeReciveEntity(String reciveId, String noticeId, String reciveType, String reciveOgjId) {
		this.reciveId = reciveId;
		this.noticeId = noticeId;
		this.reciveType = reciveType;
		this.reciveOgjId = reciveOgjId;
	}
}
