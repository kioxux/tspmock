package cn.com.yusys.yusp.oca.domain.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *@program: yusp-plus
 *@description: 金融机构在session中Vo
 *@author: wujiangpeng
 *@email: wujp4@yusys.com.cn
 *@create: 2020-11-19 14:49
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InstuSessionVo {
    /**
     * 记录编号
     */
    @JsonProperty(value = "id")
    private String instuId;
    /**
     * 部门代码
     */
    @JsonProperty(value = "code")
    private String instuCde;
    /**
     * 部门名称
     */
    @JsonProperty(value = "name")
    private String instuName;
}
