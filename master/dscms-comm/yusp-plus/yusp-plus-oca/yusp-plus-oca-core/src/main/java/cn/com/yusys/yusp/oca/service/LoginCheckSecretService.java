package cn.com.yusys.yusp.oca.service;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.vo.UserEntityVo;

/**
 * @author JP
 */
public interface LoginCheckSecretService {
    /**
     * 校验用户密码
     *
     * @param loginCode
     * @param password
     * @return 用户信息
     */
    ResultDto<UserEntityVo> queryUserAndCheckSecret(String loginCode, String password);

    /**
     * 校验下机构下用户是否存在
     *
     * @param servtp    渠道码
     * @param loginCode 登录码
     * @param orgCode   机构码
     * @return
     */
    ResultDto<UserEntityVo> checkAppClientUserExist(String servtp, String loginCode, String orgCode);

    /**
     * 校验用户手机号
     * @param loginCode
     * @return
     */
    ResultDto<UserEntityVo> checkUserMobile(String loginCode, String mobile);
}
