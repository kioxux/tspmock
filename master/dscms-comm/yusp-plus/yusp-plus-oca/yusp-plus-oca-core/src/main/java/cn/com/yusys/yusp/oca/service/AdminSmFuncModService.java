package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmFuncModBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmFuncModEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmFuncModQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmFuncModVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 系统功能模块表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-26 10:50:57
 */
public interface AdminSmFuncModService extends IService<AdminSmFuncModEntity> {

    /**
     * 默认生产查询
     *
     * @param params
     * @return
     */
    PageUtils queryPage(Map<String, Object> params);

    /**
     * 业务功能模块查询
     *
     * @param adminSmFuncModQuery
     * @return
     */
    Page<AdminSmFuncModVo> queryPageWithCondition(AdminSmFuncModQuery adminSmFuncModQuery);


    int removeByModId(String[] modId);

    List<String> checkName(String modName, String modId);

    /**
     * 保存 模块
     *
     * @param adminSmFuncBo
     * @return
     */
    int saveFuncMod(AdminSmFuncModBo adminSmFuncBo);

    /**
     * 修改 模块
     *
     * @param adminSmFuncBo
     * @return
     */
    int updateFuncMod(AdminSmFuncModBo adminSmFuncBo);

}

