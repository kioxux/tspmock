package cn.com.yusys.yusp.service.server.xdxt0008;

import cn.com.yusys.yusp.constants.OcaTradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxt0008.req.Xdxt0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0008.resp.Xdxt0008DataRespDto;
import cn.com.yusys.yusp.enums.OcaTradeEnum;
import cn.com.yusys.yusp.oca.dao.AdminSmOrgDao;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmOrgEntity;
import cn.com.yusys.yusp.service.server.xdxt0014.Xdxt0014Service;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @version 1.0.0
 * @项目名称: yusp-oca模块
 * @类名称: Xdxt0008Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-06-07 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxt0008Service extends ServiceImpl<AdminSmOrgDao, AdminSmOrgEntity> {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0014Service.class);

    /**
     * 查询账务机构
     * @param xdxt0008DataReqDto
     * @return
     */
    @Transactional
    public Xdxt0008DataRespDto getXdxt0008(Xdxt0008DataReqDto xdxt0008DataReqDto){
        logger.info(OcaTradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0014.key, OcaTradeEnum.TRADE_CODE_XDXT0014.value, JSON.toJSONString(xdxt0008DataReqDto));
        Xdxt0008DataRespDto xdxt0008DataRespDto = new Xdxt0008DataRespDto();
        //账务机构编号
        String orgId = this.baseMapper.getFinOrgbyLoginCode(xdxt0008DataReqDto.getManagerId());
        xdxt0008DataRespDto.setOrgNo(orgId);
        logger.info(OcaTradeLogConstants.SERVICE_END_PREFIX_LOGGER, OcaTradeEnum.TRADE_CODE_XDXT0014.key, OcaTradeEnum.TRADE_CODE_XDXT0014.value, JSON.toJSONString(xdxt0008DataRespDto));
        return xdxt0008DataRespDto;
    }
}
