package cn.com.yusys.yusp.oca.domain.vo;


import lombok.Data;
import java.util.List;

@Data
public class UserMenuAndContrVo {
    private List<MenuVo> menu;
    private List<ContrVo> contr;
}
