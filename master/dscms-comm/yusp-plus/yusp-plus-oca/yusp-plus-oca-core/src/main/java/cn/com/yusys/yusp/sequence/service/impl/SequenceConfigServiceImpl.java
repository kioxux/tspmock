package cn.com.yusys.yusp.sequence.service.impl;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.sequence.dao.SequenceConfigDao;
import cn.com.yusys.yusp.sequence.domain.constants.Constants;
import cn.com.yusys.yusp.sequence.domain.constants.MessageEnums;
import cn.com.yusys.yusp.sequence.domain.entity.SequenceConfig;
import cn.com.yusys.yusp.sequence.domain.query.SequenceConfigQuery;
import cn.com.yusys.yusp.sequence.domain.vo.SequenceConfigAddVo;
import cn.com.yusys.yusp.sequence.domain.vo.SequenceConfigUpdateVo;
import cn.com.yusys.yusp.sequence.service.SequenceConfigService;
import cn.com.yusys.yusp.sequence.util.SequenceUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import groovy.util.logging.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Slf4j
@Service("sequenceConfigService")
public class SequenceConfigServiceImpl extends ServiceImpl<SequenceConfigDao, SequenceConfig> implements SequenceConfigService {

//    @Override
//    public PageUtils queryPage(Map<String, Object> params) {
//        IPage<SequenceConfig> page = this.page(
//                new Query<SequenceConfig>().getPage(params),
//                new QueryWrapper<SequenceConfig>()
//        );
//
//        return new PageUtils(page);
//    }

    /**
     * 分页查询序列化模板
     *
     * @param sequenceConfigQuery
     * @return 完成分页的序列化模板
     */
    @Override
    public PageUtils queryPage(SequenceConfigQuery sequenceConfigQuery) {
        //分页条件
        QueryWrapper<SequenceConfig> queryWrapper = new QueryWrapper<SequenceConfig>()
                .like(StringUtils.nonEmpty(sequenceConfigQuery.getSeqId()), "SEQ_ID", sequenceConfigQuery.getSeqId())
                .like(StringUtils.nonEmpty(sequenceConfigQuery.getSeqName()), "SEQ_NAME", sequenceConfigQuery.getSeqName());
        //分页参数
        Page<SequenceConfig> page = sequenceConfigQuery.getIPage();
        //分页查询
        IPage<SequenceConfig> pageResult = this.baseMapper.selectPage(page, queryWrapper);
        //返回结果
        return new PageUtils(pageResult);
    }

    /**
     * 新增序列化模板
     *
     * @param sequenceConfigAddVo
     */
    @Transactional
    @Override
    public void save(SequenceConfigAddVo sequenceConfigAddVo) {

        //校验序列化模板ID是否复合规则
        boolean reg = SequenceUtils.reg(sequenceConfigAddVo.getSeqId());
        if (!reg) {
            throw BizException.error(null, MessageEnums.SEQUENCE_ID_STRATEGY.getCode(), MessageEnums.SEQUENCE_ID_STRATEGY.getMessage());
        }

        //校验序列化模板Id是否已经存在s
        List<SequenceConfig> sequenceConfigs = this.list(new QueryWrapper<SequenceConfig>().eq("SEQ_ID", sequenceConfigAddVo.getSeqId()));
        if (CollectionUtils.nonEmpty(sequenceConfigs)) {
            throw BizException.error(null, MessageEnums.SEQUENCE_ID_EXISTS.getCode(), MessageEnums.SEQUENCE_ID_EXISTS.getMessage());
        }

        try {
            //序列化模板持久化
            SequenceConfig sequenceConfig = new SequenceConfig();
            sequenceConfig.setLastChgDt(new Date());
            sequenceConfig.setLastChgUsr(cn.com.yusys.yusp.commons.session.util.SessionUtils.getLoginCode());
            sequenceConfig.setSeqCreate(Constants.SequenceConfigConstance.YES);
            this.baseMapper.insert(sequenceConfig);

            //缓存更新
            SequenceUtils.addSequenceConfig(sequenceConfig);
        } catch (Exception e) {
            log.error("新增序列化模板出错");
            throw BizException.error(null, MessageEnums.SEQUENCE_ADD_ERROR.getCode(), MessageEnums.SEQUENCE_ADD_ERROR.getMessage());
        }
    }

    /**
     * 更新序列化模板
     *
     * @param sequenceConfigUpdateVo
     */
    @Override
    public void updateById(SequenceConfigUpdateVo sequenceConfigUpdateVo) {

        //校验序列化id是否符合要求
        boolean reg = SequenceUtils.reg(sequenceConfigUpdateVo.getSeqId());

        //如果序列化id符合要求,更新序列化模板
        if (reg) {
            SequenceConfig sequenceConfig = new SequenceConfig();
            BeanUtils.beanCopy(sequenceConfigUpdateVo, sequenceConfig);
            sequenceConfig.setLastChgDt(new Date());
            sequenceConfig.setLastChgUsr(SessionUtils.getLoginCode());
            this.baseMapper.updateById(sequenceConfig);
        } else {
            throw BizException.error(null, MessageEnums.SEQUENCE_ID_STRATEGY.getCode(), MessageEnums.SEQUENCE_ID_STRATEGY.getMessage());
        }
    }

    /**
     * 删除序列化模板
     *
     * @param id
     */
    @Override
    public void removeByIds(String id) {
        //查询数据库中序列化模板
        SequenceConfig sequenceConfig = this.baseMapper.selectById(id);

        if (Objects.nonNull(sequenceConfig)) {
            //删除数据库中的值
            this.baseMapper.deleteById(id);
            //删除缓存中的值
            try {
                SequenceUtils.dropSequence(sequenceConfig.getSeqId());
            } catch (Exception e) {
                log.error("序列化模板删除错误");
            }
        }
    }

    /**
     * 创建序列化模板
     *
     * @param seqId
     */
    @Override
    public void createSequence(String seqId) {

        //查询序列化模板
        SequenceConfig sequenceConfig = this.getOne(new QueryWrapper<SequenceConfig>().eq("SEQ_ID", seqId));

        if (Objects.nonNull(sequenceConfig)) {
            sequenceConfig.setSeqCreate(Constants.SequenceConfigConstance.YES);
            //更新数据库
            this.updateById(sequenceConfig);
        }
    }

    /**
     * 重置序列化模板
     *
     * @param seqId
     */
    @Override
    public void reCreateSequence(String seqId) {
        //删除缓存中序列
        boolean dropSequence = SequenceUtils.dropSequence(seqId);

        if (dropSequence) {
            //查询序列化模板
            SequenceConfig sequenceConfig = this.getOne(new QueryWrapper<SequenceConfig>().eq("SEQ_ID", seqId));
            sequenceConfig.setSeqCreate(Constants.SequenceConfigConstance.YES);
            SequenceUtils.addSequenceConfig(sequenceConfig);
            this.updateById(sequenceConfig);
        }
    }

    /**
     * 获得当前序列值序列
     *
     * @param seqId
     */
    @Override
    public String getCurrentSeq(String seqId) {
        return String.valueOf(SequenceUtils.getCurrentValue(seqId));

    }

    /**
     * 获得下一序列值
     *
     * @param seqId
     * @return 下一序列值
     */
    @Override
    public String getNextSeq(String seqId) {
        return String.valueOf(SequenceUtils.getNextSequenceNo(seqId));
    }

    /**
     * 获得配置的模版序列
     *
     * @param seqId
     * @return 配置的模版序列
     */
    @Override
    public String getSequenceWithTemplate(String seqId, Map paramsMap) {
        return SequenceUtils.getSequence(seqId, paramsMap);
    }

    /**
     * 获得下一批序列值
     *
     * @param seqId
     * @return 下一批序列值
     */
    @Override
    public List<String> getNextSequenceWithCount(String seqId, Integer count) {
        LinkedList<String> seqList = new LinkedList<>();
        if (StringUtils.nonBlank(seqId)) {
            for (int i = 0; i < count; i++) {
                // 调用序列生成API
                seqList.add(SequenceUtils.getSequence(seqId, null));
            }
        }
        return seqList;
    }

    /**
     * 获得配置的模版序列
     *
     * @param seqId
     * @param count
     * @param paramsMap
     * @return
     */
    @Override
    public List<String> getSequenceWithTemplateWithCount(String seqId, Integer count, Map<String, String> paramsMap) {
        List<String> seqList = new LinkedList<>();
        if (StringUtils.nonBlank(seqId)) {
            for (int i = 0; i < count; i++) {
                // 调用序列生成API
                seqList.add(SequenceUtils.getSequence(seqId, paramsMap));
            }
        }
        return seqList;
    }
}