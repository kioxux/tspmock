package cn.com.yusys.yusp.oca.domain.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 *@program: yusp-plus
 *@description: 提示信息管理表Vo
 *@author: wujiangpeng
 *@email: wujp4@yusys.com.cn
 *@create: 2020-11-16 15:41
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminSmMessageVo {
    /**
     * 消息编号
     */
    private String messageId;
    /**
     * 信息码
     */
    private String code;
    /**
     * 信息级别:success成功 info信息 warning警告 error错误
     */
    private String messageLevel;
    /**
     * 提示内容
     */
    private String message;
    /**
     * 消息类别：COMINFO系统级通用提示 DBERR数据库错误提示 MODULEINFO模块提示
     */
    private String messageType;
    /**
     * 所属模块名称
     */
    private String funcName;
    /**
     * 最后修改用户
     */
    private String lastChgUsr;
    /**
     * 最后修改时间
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;
}
