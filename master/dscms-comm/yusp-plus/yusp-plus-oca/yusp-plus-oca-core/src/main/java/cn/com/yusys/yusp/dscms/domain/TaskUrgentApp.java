/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dscms.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: yusp-plus-oca-core模块
 * @类名称: TaskUrgentApp
 * @类描述: task_urgent_app数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-29 14:06:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "task_urgent_app")
public class TaskUrgentApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 关联流水号 **/
	@Column(name = "PWBR_SERNO", unique = false, nullable = true, length = 40)
	private String pwbrSerno;
	
	/** 业务条线 **/
	@Column(name = "BIZ_LINE", unique = false, nullable = true, length = 5)
	private String bizLine;
	
	/** 业务阶段 **/
	@Column(name = "BIZ_TYPE", unique = false, nullable = true, length = 5)
	private String bizType;
	
	/** 业务流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 客户编号  **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称  **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 40)
	private String cusName;
	
	/** 责任人  **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 责任人  **/
	@Column(name = "MANAGER_ID_NAME", unique = false, nullable = true, length = 20)
	private String managerIdName;

	/** 责任机构  **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 责任机构  **/
	@Column(name = "MANAGER_BR_ID_NAME", unique = false, nullable = true, length = 20)
	private String managerBrIdName;

	/** 加急原因  **/
	@Column(name = "URGENT_RESN", unique = false, nullable = true, length = 1000)
	private String urgentResn;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记人 **/
	@Column(name = "INPUT_ID_NAME", unique = false, nullable = true, length = 20)
	private String inputIdName;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID_NAME", unique = false, nullable = true, length = 20)
	private String inputBrIdName;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 加急类型 **/
	@Column(name = "URGENT_TYPE", unique = false, nullable = true, length = 5)
	private String urgentType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param pwbrSerno
	 */
	public void setPwbrSerno(String pwbrSerno) {
		this.pwbrSerno = pwbrSerno;
	}
	
    /**
     * @return pwbrSerno
     */
	public String getPwbrSerno() {
		return this.pwbrSerno;
	}
	
	/**
	 * @param bizLine
	 */
	public void setBizLine(String bizLine) {
		this.bizLine = bizLine;
	}
	
    /**
     * @return bizLine
     */
	public String getBizLine() {
		return this.bizLine;
	}
	
	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
	
    /**
     * @return bizType
     */
	public String getBizType() {
		return this.bizType;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerIdName
	 */
	public void setManagerIdName(String managerIdName) {
		this.managerIdName = managerIdName;
	}

	/**
	 * @return managerIdName
	 */
	public String getManagerIdName() {
		return this.managerIdName;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerBrIdName
	 */
	public void setManagerBrIdName(String managerBrIdName) {
		this.managerBrIdName = managerBrIdName;
	}

	/**
	 * @return managerBrIdName
	 */
	public String getManagerBrIdName() {
		return this.managerBrIdName;
	}
	
	/**
	 * @param urgentResn
	 */
	public void setUrgentResn(String urgentResn) {
		this.urgentResn = urgentResn;
	}
	
    /**
     * @return urgentResn
     */
	public String getUrgentResn() {
		return this.urgentResn;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputIdName
	 */
	public void setInputIdName(String inputIdName) {
		this.inputIdName = inputIdName;
	}

	/**
	 * @return inputIdName
	 */
	public String getInputIdName() {
		return this.inputIdName;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputBrIdName
	 */
	public void setInputBrIdName(String inputBrIdName) {
		this.inputBrIdName = inputBrIdName;
	}

	/**
	 * @return inputBrIdName
	 */
	public String getInputBrIdName() {
		return this.inputBrIdName;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param urgentType
	 */
	public void setUrgentType(String urgentType) {
		this.urgentType = urgentType;
	}
	
    /**
     * @return urgentType
     */
	public String getUrgentType() {
		return this.urgentType;
	}


}