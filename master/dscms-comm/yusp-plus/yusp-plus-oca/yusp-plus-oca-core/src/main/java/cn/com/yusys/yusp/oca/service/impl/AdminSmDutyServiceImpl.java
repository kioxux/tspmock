package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.common.utils.RAMPager;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.ObjectUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.GetUserInfoByDutyCodeDto;
import cn.com.yusys.yusp.oca.dao.AdminSmDutyDao;
import cn.com.yusys.yusp.oca.dao.AdminSmDutyUserDao;
import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmDutyEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmDutyQuery;
import cn.com.yusys.yusp.oca.domain.query.AdminSmDutyUserQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmDutyVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmOrgVo;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmUserDutyRelVo;
import cn.com.yusys.yusp.oca.service.AdminSmDutyService;
import cn.com.yusys.yusp.oca.service.AdminSmOrgService;
import cn.com.yusys.yusp.oca.service.AdminSmUserDutyRelService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author terry
 * @email tanrui1@yusys.com.cn
 */
@Service("adminSmDutyService")
@Slf4j
public class AdminSmDutyServiceImpl extends ServiceImpl<AdminSmDutyDao, AdminSmDutyEntity> implements AdminSmDutyService {

    @Autowired
    AdminSmOrgService adminSmOrgService;
    @Autowired
    AdminSmUserDutyRelService userDutyRelService;
    @Autowired
    private AdminSmDutyUserDao adminSmDutyUserDao;

    /**
     * 分页查询
     *
     * @param query
     * @return
     */
    @Override
    public Page<AdminSmDutyVo> queryPage(AdminSmDutyQuery query) {
        Page<AdminSmDutyVo> page = query.getIPage();
        QueryWrapper<AdminSmDutyVo> queryWrapper = new QueryWrapper<AdminSmDutyVo>()
                .like(!StringUtils.isEmpty(query.getDutyCode()), "d.DUTY_CODE", query.getDutyCode())//岗位编码模糊匹配
                .like(!StringUtils.isEmpty(query.getDutyName()), "d.DUTY_NAME", query.getDutyName())//岗位名称模糊匹配
                .eq(ObjectUtils.nonNull(query.getDutySts()), "d.DUTY_STS", query.getDutySts());//岗位状态精确匹配
        if (StringUtils.nonEmpty(query.getKeyWord())) {
            queryWrapper.and(wrapper -> wrapper
                    .like(StringUtils.nonEmpty(query.getKeyWord()), "d.DUTY_CODE", query.getKeyWord())//关键字模糊匹配岗位编码
                    .or()
                    .like(StringUtils.nonEmpty(query.getKeyWord()), "d.DUTY_NAME", query.getKeyWord())//关键字模糊匹配岗位名称
            );
        }

        queryWrapper.orderByDesc("d.LAST_CHG_DT");
        // 3. 查询所有的岗位
        final List<AdminSmDutyVo> dutyVoList = this.baseMapper.selectAllDuty(queryWrapper);

        // 3.1 调用orgService,拿到orgId机构的所有子机构列表
        List<String> orgIds;
        if (StringUtils.isEmpty(query.getOrgId())) {
            orgIds = adminSmOrgService.getAllAccessibleOrgIds();//未指定orgId就查询当前用户有权访问的机构下的所有岗位
        } else {
            orgIds = adminSmOrgService.getAllProgeny(query.getOrgId()).stream().map(AdminSmOrgVo::getOrgId).collect(Collectors.toList());//指定了orgId就查指定机构及其下级机构下的岗位
        }
        List<AdminSmDutyVo> dutyResultList = dutyVoList.stream().filter(item -> orgIds.contains(item.getOrgId())).collect(Collectors.toList());
        List<AdminSmDutyVo> sortedResultList = dutyResultList.stream().sorted((node1, node2) -> DateUtils.compare(node2.getLastChgDt(), node1.getLastChgDt())).collect(Collectors.toList());//按日期降序
        //代码分页
        RAMPager<AdminSmDutyVo> pager = new RAMPager<>(sortedResultList, query.getSize());
        page.setRecords(pager.page(query.getPage()));
        page.setTotal(sortedResultList.size());
        return page;
    }

    @Override
    public Page<AdminSmUserDutyRelVo> memberPage(AdminSmDutyUserQuery query) {
        // Asserts.nonNull(query.getDutyId(),"duty id can not be null");
        if (query.getDutyId() == null) {
            throw BizException.error(null, "50200003", "岗位代码不能为空");
        }
        // 组装查询条件
        Page<AdminSmUserDutyRelVo> page = query.getIPage();
        QueryWrapper<AdminSmUserDutyRelVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("ud.DUTY_ID", query.getDutyId());
        queryWrapper.like(!StringUtils.isEmpty(query.getLoginCode()), "u.LOGIN_CODE", query.getLoginCode());
        queryWrapper.like(!StringUtils.isEmpty(query.getUserName()), "u.USER_NAME", query.getUserName());
        queryWrapper.like(!StringUtils.isEmpty(query.getUserCode()), "u.USER_CODE", query.getUserCode());
        queryWrapper.like(!StringUtils.isEmpty(query.getOrgId()), "o.ORG_ID", query.getOrgId());
        queryWrapper.like(!StringUtils.isEmpty(query.getOrgName()), "o.ORG_NAME", query.getOrgName());
        queryWrapper.eq(ObjectUtils.nonNull(query.getUserSts()), "u.USER_STS", query.getUserSts());
        return this.adminSmDutyUserDao.pageDutyUserByDutyId(page, queryWrapper);
    }

    /**
     * 批量启用
     *
     * @param ids
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchEnable(String[] ids) {
        if (CollectionUtils.nonEmpty(ids)) {
            List<String> idList = Arrays.asList(ids);
            idList.forEach((id) -> {
                AdminSmDutyEntity entity = new AdminSmDutyEntity();
                entity.setDutyId(id);
                entity.setDutySts(AvailableStateEnum.ENABLED);
                this.baseMapper.updateById(entity);
            });
        }
    }

    /**
     * 批量停用
     *
     * @param ids
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchDisable(String[] ids) {
        if (CollectionUtils.nonEmpty(ids)) {
            List<String> idList = Arrays.asList(ids);
            idList.forEach((id) -> {
                if (checkBlocked(id)) {
                    throw BizException.error(null, "50200002", "该岗位绑定有用户，请删除关联信息后操作");
                } else {
                    AdminSmDutyEntity entity = new AdminSmDutyEntity();
                    entity.setDutyId(id);
                    entity.setDutySts(AvailableStateEnum.DISABLED);
                    this.baseMapper.updateById(entity);
                }
            });
        }
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchDelete(String[] ids) {
        if (CollectionUtils.nonEmpty(ids)) {
            List<String> idList = Arrays.asList(ids);
            idList.forEach((id) -> {
                if (checkBlocked(id)) {
                    throw BizException.error(null, "50200002", "该岗位绑定有用户，请删除关联信息后操作");
                } else {
                    this.baseMapper.deleteById(id);
                }
            });
        }
    }

    @Override
    public Page<AdminSmDutyVo> getDutysForWf(AdminSmDutyQuery query) {
        Page<AdminSmDutyVo> page = query.getIPage();
        QueryWrapper<AdminSmDutyVo> wrapper = new QueryWrapper<>();
        wrapper.eq("T1.DUTY_STS", "A");
        creatWrapper(wrapper, "T1.ORG_ID", query.getOrgId());
        creatWrapper(wrapper, "T1.DUTY_CODE", query.getDutyCode());
        creatWrapper(wrapper, "T1.DUTY_NAME", query.getDutyName());
        return this.baseMapper.getDutysForWf(page, wrapper);
    }

    public void creatWrapper(QueryWrapper<AdminSmDutyVo> wrapper, String column, String value) {
        if (StringUtils.nonEmpty(value)) {
            boolean condition = value.startsWith("%") || value.endsWith("%");
            wrapper.like(condition, column, value);
            wrapper.eq(!condition, column, value);
        }
    }

    @Override
    public List<String> getDutysByUserIdForWf(String userId) {
        return this.baseMapper.getDutysByUserIdForWf(userId);
    }


    /**
     * 检查岗位是否已关联其他信息
     *
     * @param dutyId
     * @return
     */
    private boolean checkBlocked(String dutyId) {
        LambdaQueryWrapper<AdminSmUserDutyRelVo> userWrapper = new QueryWrapper<AdminSmUserDutyRelVo>().lambda();
        userWrapper.eq(AdminSmUserDutyRelVo::getDutyId, dutyId);
        Integer countUser = this.adminSmDutyUserDao.selectCount(userWrapper);//是否有关联用户
        return countUser > 0;
    }

    @Override
    public boolean save(AdminSmDutyEntity entity) {
        LambdaQueryWrapper<AdminSmDutyEntity> codeWrapper = new QueryWrapper<AdminSmDutyEntity>().lambda();
        codeWrapper.eq(AdminSmDutyEntity::getDutyCode, entity.getDutyCode());
        AdminSmDutyEntity check = this.baseMapper.selectOne(codeWrapper);
        if (Objects.nonNull(check)) {
            throw BizException.error("exist", "50200001", entity.getDutyCode());
        }
        entity.setDutySts(Optional.ofNullable(entity.getDutySts()).orElse(AvailableStateEnum.UNENABLED));//新增的数据默认是待启用的
        log.info("New duty data: [new duty: {}] ", entity.getDutyName());
        return this.baseMapper.insert(entity) > 0;
    }

    @Override
    public boolean updateById(AdminSmDutyEntity entity) {
        if (AvailableStateEnum.DISABLED.equals(entity.getDutySts()) && checkBlocked(entity.getDutyId())) {//改为停用时要判断是否关联其他信息
            throw BizException.error(null, "50200002", "该岗位绑定有用户，请删除关联信息后操作");
        }
        entity.setDutySts(Optional.ofNullable(entity.getDutySts()).orElse(AvailableStateEnum.UNENABLED));//新增的数据默认是待启用的
        return super.updateById(entity);
    }
}