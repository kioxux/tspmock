package cn.com.yusys.yusp.oca.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author danyu
 */
@Data
@ApiModel(description = "日志对象Dto", value = "日志对象")
public class AdminSmLogDto {

	/**
	 * 记录编号
	 */
	@ApiModelProperty(name = "logId", value = "日志id", required = true)
	private String logId;
	/**
	 * 用户ID
	 */
	@ApiModelProperty(name = "userId", value = "用户id", required = true)
	private String userId;
	/**
	 * 操作时间
	 */
	@ApiModelProperty(name = "operTime", value = "操作时间", required = true)
	private String operTime;
	/**
	 * 操作对象ID
	 */
	@ApiModelProperty(name = "operObjId", value = "操作对象id", required = true)
	private String operObjId;
	/**
	 * 操作前值
	 */
	@ApiModelProperty(name = "beforeValue", value = "操作前值", required = false)
	private String beforeValue;
	/**
	 * 操作后值
	 */
	@ApiModelProperty(name = "afterValue", value = "操作后值", required = false)
	private String afterValue;
	/**
	 * 操作标志
	 */
	@ApiModelProperty(name = "operFlag", value = "操作标志", required = true)
	private String operFlag;
	/**
	 * 日志类型
	 */
	@ApiModelProperty(name = "logTypeId", value = "日志类型", required = true)
	private String logTypeId;
	/**
	 * 日志内容
	 */
	@ApiModelProperty(name = "content", value = "日志内容", required = true)
	private String content;
	/**
	 * 操作者机构
	 */
	@ApiModelProperty(name = "orgId", value = "操作者机构", required = true)
	private String orgId;
	/**
	 * 登录IP
	 */
	@ApiModelProperty(name = "loginIp", value = "登录IP", required = true)
	private String loginIp;

}
