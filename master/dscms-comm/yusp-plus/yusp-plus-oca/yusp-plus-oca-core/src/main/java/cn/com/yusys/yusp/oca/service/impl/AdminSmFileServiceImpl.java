package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.async.ImportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmLogEntity;
import cn.com.yusys.yusp.oca.domain.form.AdminSmLogForm;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmLogPojo;
import cn.com.yusys.yusp.oca.service.AdminSmFileService;
import cn.com.yusys.yusp.oca.service.AdminSmLogService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Service("adminSmFileService")
public class AdminSmFileServiceImpl implements AdminSmFileService {

    @Autowired
    private AdminSmLogService adminSmLogService;
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public String fileCode() {
        /**
         * 生成唯一验证码
         */
        String fileCode = StringUtils.getUUID() + "." + SessionUtils.getUserId();
        return fileCode;
    }

    @Async
    @Override
    public void asyncSendFile(Map<String, String> params, String fileCode) {
        File file = adminSmLogService.asyncExportFile(params);
        // 设置请求的格式类型
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("multipart/form-data");
        headers.setContentType(type);
        // 将 file 转成 FileSystemResource 类型，存入请求体中
        FileSystemResource fileSystemResource = new FileSystemResource(file);
        MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();
        // 文件服务接收文件时的 parameterKey 为：file 和 code
        form.add("file", fileSystemResource);
        form.add("code", fileCode);
        // 使用 RestTemplate 发送请求
        String url = "http://file.yusp.com.cn:8400/api/file/provider/tempuploadfile";
        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(form, headers);
        restTemplate.postForEntity(url, httpEntity, ResultDto.class);
        file.delete();
    }

    /**
     * 按照条件查询并组装excel文件
     *
     * @param logPojo
     * @return
     */
    @Override
    public ProgressDto translateFile(AdminSmLogPojo logPojo) {
        ExportContext context = ExportContext.of(AdminSmLogPojo.class);
        DataAcquisition dataAcquisition = new DataAcquisition() {
            /**
             * 自定义数据查询逻辑
             * @param i：yml 配置文件中的 max-pages；
             * @param i1：yml 配置文件中的 page-size；
             * @param object：context.data(dataAcquisition, logForm) 中的 logForm，也就是查询条件封装类 AdminSmLogForm
             * @return Collection<?>：返回类型就是 List<AdminSmLogEntity>
             */
            @SneakyThrows
            @Override
            public Collection<?> getData(int i, int i1, Object object) {
                System.out.println("getData: ");
                AdminSmLogForm form = null;
                List<AdminSmLogEntity> list = null;
//                if (logQuery != null) {
////                    form = (AdminSmLogForm) logForm;
////                    System.out.println("selectByForm----------------------------------------------------------------------------------");
////                    list = adminSmLogService.selectByForm(form);
////                    System.out.println("AdminSmFileServiceImpl.getData - list : " + list.size());
//                    ResultDto resultDto = adminSmLogService.pageLogByCondition(logQuery);
//                } else {
//                    QueryWrapper<AdminSmLogEntity> wrapper = new QueryWrapper<>();
//                    wrapper.eq("log_id", "-1");
//                    list = adminSmLogService.list(wrapper);
//                }
                return list;
            }
        };
        ProgressDto progressDto = ExcelUtils.asyncExport(context.data(dataAcquisition, null));
        return progressDto;
    }

    /**
     * 异步导入文件
     *
     * @param uploadFile
     * @return
     */
    @Override
    public ProgressDto asyncImport(MultipartFile uploadFile) {
        ProgressDto progressDto = null;
        String fileName = uploadFile.getOriginalFilename();
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        try {
            /**
             * 将 MultipartFile 文件转成
             */
            File temporaryFile = ExcelUtils.getTemporaryFile(suffixName);
            uploadFile.transferTo(temporaryFile);
            ImportContext importContext = new ImportContext();
            importContext.setHeadClass(AdminSmLogPojo.class);
            importContext.setFile(temporaryFile.getAbsolutePath());
            /**
             * true 为批量执行导入 Object o 为 List 集合
             * false 为逐条执行导入 Object o 为 AdminSmLogPojo 对象
             */
            importContext.batch(true);
            progressDto = ExcelUtils.asyncImport(ImportContext.of(AdminSmLogPojo.class)
                    // 批量操作需要将batch设置为true
                    .batch(true)
                    // 传入 excel 临时文件
                    .file(temporaryFile)
                    // 使用batchInsert对数据进行批量操作
                    .dataStorage(ExcelUtils.batchConsumer(adminSmLogService::saveBatchByEntity)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return progressDto;
    }

    /**
     * 导出空模板
     */
    @Override
    public File downLoadTemplate() {
        File file = ExcelUtils.syncExport(AdminSmLogPojo.class, new ArrayList<AdminSmLogPojo>());
        return file;
    }
}
