package cn.com.yusys.yusp.oca.controller;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmLogicSysBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmLogicSysEntity;
import cn.com.yusys.yusp.oca.service.AdminSmLogicSysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 系统逻辑系统表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-19 14:30:22
 */
@RestController
@RequestMapping("/api/adminsmlogicsys")
public class AdminSmLogicSysController {
    @Autowired
    private AdminSmLogicSysService adminSmLogicSysService;

    /**
     * 逻辑系统-列表查询
     */
    @GetMapping("/")
    public ResultDto list(@RequestParam Map<String, Object> params) {
        PageUtils page = adminSmLogicSysService.getAdminSmLogicSys(params);
        return ResultDto.success().extParam("total", page.getTotalCount()).extParam("data", page.getList());
    }

    /**
     * 信息
     */
    @GetMapping("/info/{sysId}")
    public ResultDto info(@PathVariable("sysId") String sysId) {
        AdminSmLogicSysEntity adminSmLogicSys = adminSmLogicSysService.getById(sysId);
        return ResultDto.success().extParam("adminSmLogicSys", adminSmLogicSys);
    }

    /**
     * 保存 初始化逻辑系统
     */
    @PostMapping("/copy")
    public ResultDto initLogicSys(@RequestBody AdminSmLogicSysBo adminSmLogicSys) {
        AdminSmLogicSysEntity logicSysEntity = adminSmLogicSysService.insertAndCopy(adminSmLogicSys);

        return ResultDto.success().extParam("data", logicSysEntity).extParam("message", "成功创建逻辑系统!");
    }

    /**
     * 修改逻辑系统
     */
    @PostMapping("/update/{funcId}")
    public ResultDto update(@RequestBody AdminSmLogicSysBo adminSmLogicSysBo, @PathVariable String funcId) {
        int isLogicSysVoBoolean = adminSmLogicSysService.updateAdminSmLogic(adminSmLogicSysBo, funcId);
        if (isLogicSysVoBoolean > 0) {
            return ResultDto.success();
        }
        return ResultDto.error(300002, "修改逻辑系统失败!");
    }

    /**
     * 逻辑系统 删除
     */
    @PostMapping("/delete")
    public ResultDto delete(@RequestParam("id") String id) {
        int delSysIdInt = adminSmLogicSysService.deleteLogicAndCrelInfo(id);
        if (delSysIdInt == -1) {
            return ResultDto.error("逻辑系统中状态为生效不能删除");
        }
        return ResultDto.success().extParam("data", delSysIdInt).extParam("message", "逻辑系统已删除!");
    }

    /**
     * @方法名称: update Status
     * @方法描述: 逻辑系统状态修改
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatestatu")
    protected ResultDto updateStatu(@RequestBody AdminSmLogicSysBo adminSmLogicSysBo) {
        this.adminSmLogicSysService.updateAdminSmLogicStat(adminSmLogicSysBo);
        return ResultDto.success();
    }
}
