package cn.com.yusys.yusp.oca.domain.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class AdminSmRoleExcelVo {

    @ExcelProperty("角色代码")
    private String roleCode;

    @ExcelProperty("角色名称")
    private String roleName;

    @ExcelProperty("机构代码")
    private String orgId;

    @ExcelProperty("角色级别")
    private Integer roleLevel;




}
