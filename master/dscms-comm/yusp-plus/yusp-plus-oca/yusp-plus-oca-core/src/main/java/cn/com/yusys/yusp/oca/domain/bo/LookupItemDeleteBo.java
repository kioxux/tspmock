package cn.com.yusys.yusp.oca.domain.bo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class LookupItemDeleteBo {

	/**
	 * 记录编号
	 */
	private String lookupItemId;
	/**
	 * 字典类别英文别名
	 */
	private String lookupCode;

}
