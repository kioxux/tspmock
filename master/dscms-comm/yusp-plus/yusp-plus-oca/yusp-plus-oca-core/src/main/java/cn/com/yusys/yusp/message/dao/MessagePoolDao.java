package cn.com.yusys.yusp.message.dao;

import cn.com.yusys.yusp.message.entity.MessagePoolEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 消息队列
 *
 * @author xiaodg@yusys.com.cn
 */

public interface MessagePoolDao extends BaseMapper<MessagePoolEntity> {

}
