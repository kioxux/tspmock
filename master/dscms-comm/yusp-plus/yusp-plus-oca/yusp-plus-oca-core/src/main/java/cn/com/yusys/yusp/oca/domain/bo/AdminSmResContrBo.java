package cn.com.yusys.yusp.oca.domain.bo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统功能控制点表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-19 16:38:48
 */
@Data
public class AdminSmResContrBo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 记录编号
     */
    private String contrId;
    /**
     * 所属业务功能编号
     */
    private String funcId;
    /**
     * 控制操作代码
     */
    private String contrCode;
    /**
     * 控制操作名称
     */
    private String contrName;
    /**
     * 控制操作URL(用于后台校验时使用)
     */
    private String contrUrl;
    /**
     * 备注
     */
    private String contrRemark;
    /**
     * 最新变更用户
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
    /**
     * 最新变更时间
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;
    /**
     * 请求类型
     */
    private String methodType;

    public AdminSmResContrBo() {
    }



    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof AdminSmResContrBo)) return false;
        final AdminSmResContrBo other = (AdminSmResContrBo) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$contrId = this.getContrId();
        final Object other$contrId = other.getContrId();
        if (this$contrId == null ? other$contrId != null : !this$contrId.equals(other$contrId)) return false;
        final Object this$funcId = this.getFuncId();
        final Object other$funcId = other.getFuncId();
        if (this$funcId == null ? other$funcId != null : !this$funcId.equals(other$funcId)) return false;
        final Object this$contrCode = this.getContrCode();
        final Object other$contrCode = other.getContrCode();
        if (this$contrCode == null ? other$contrCode != null : !this$contrCode.equals(other$contrCode)) return false;
        final Object this$contrName = this.getContrName();
        final Object other$contrName = other.getContrName();
        if (this$contrName == null ? other$contrName != null : !this$contrName.equals(other$contrName)) return false;
        final Object this$contrUrl = this.getContrUrl();
        final Object other$contrUrl = other.getContrUrl();
        if (this$contrUrl == null ? other$contrUrl != null : !this$contrUrl.equals(other$contrUrl)) return false;
        final Object this$contrRemark = this.getContrRemark();
        final Object other$contrRemark = other.getContrRemark();
        if (this$contrRemark == null ? other$contrRemark != null : !this$contrRemark.equals(other$contrRemark))
            return false;
        final Object this$lastChgUsr = this.getLastChgUsr();
        final Object other$lastChgUsr = other.getLastChgUsr();
        if (this$lastChgUsr == null ? other$lastChgUsr != null : !this$lastChgUsr.equals(other$lastChgUsr))
            return false;
        final Object this$lastChgDt = this.getLastChgDt();
        final Object other$lastChgDt = other.getLastChgDt();
        if (this$lastChgDt == null ? other$lastChgDt != null : !this$lastChgDt.equals(other$lastChgDt)) return false;
        final Object this$methodType = this.getMethodType();
        final Object other$methodType = other.getMethodType();
        if (this$methodType == null ? other$methodType != null : !this$methodType.equals(other$methodType))
            return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof AdminSmResContrBo;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $contrId = this.getContrId();
        result = result * PRIME + ($contrId == null ? 43 : $contrId.hashCode());
        final Object $funcId = this.getFuncId();
        result = result * PRIME + ($funcId == null ? 43 : $funcId.hashCode());
        final Object $contrCode = this.getContrCode();
        result = result * PRIME + ($contrCode == null ? 43 : $contrCode.hashCode());
        final Object $contrName = this.getContrName();
        result = result * PRIME + ($contrName == null ? 43 : $contrName.hashCode());
        final Object $contrUrl = this.getContrUrl();
        result = result * PRIME + ($contrUrl == null ? 43 : $contrUrl.hashCode());
        final Object $contrRemark = this.getContrRemark();
        result = result * PRIME + ($contrRemark == null ? 43 : $contrRemark.hashCode());
        final Object $lastChgUsr = this.getLastChgUsr();
        result = result * PRIME + ($lastChgUsr == null ? 43 : $lastChgUsr.hashCode());
        final Object $lastChgDt = this.getLastChgDt();
        result = result * PRIME + ($lastChgDt == null ? 43 : $lastChgDt.hashCode());
        final Object $methodType = this.getMethodType();
        result = result * PRIME + ($methodType == null ? 43 : $methodType.hashCode());
        return result;
    }

    public String toString() {
        return "AdminSmResContrBo(contrId=" + this.getContrId() + ", funcId=" + this.getFuncId() + ", contrCode=" + this.getContrCode() + ", contrName=" + this.getContrName() + ", contrUrl=" + this.getContrUrl() + ", contrRemark=" + this.getContrRemark() + ", lastChgUsr=" + this.getLastChgUsr() + ", lastChgDt=" + this.getLastChgDt() + ", methodType=" + this.getMethodType() + ")";
    }
}
