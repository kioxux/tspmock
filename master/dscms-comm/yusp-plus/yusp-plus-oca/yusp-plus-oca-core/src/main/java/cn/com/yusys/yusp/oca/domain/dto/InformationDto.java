package cn.com.yusys.yusp.oca.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *@program: yusp-plus
 *@description: 资料Vo
 *@author: wujiangpeng
 *@email: wujp4@yusys.com.cn
 *@create: 2020-12-07 17:46
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InformationDto {
    private String productId;
    @JsonProperty("id")
    private String addValue;
    @JsonProperty("informationName")
    private String addName;
}
