package cn.com.yusys.yusp.oca.domain.form;

import lombok.Data;

/**
 * @author danyu
 */
@Data
public class AdminSmDptForm {

	private String dptId;
	/**
	 * 部门代码
	 */
	private String dptCode;
	/**
	 * 部门名称
	 */
	private String dptName;
	/**
	 * 所属机构编号
	 */
	private String belongOrgId;
	/**
	 * 上级部门记录编号
	 */
	private String upDptId;
	/**
	 * 状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效
	 */
	private String dptSts;


}
