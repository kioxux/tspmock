package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.common.utils.PageUtils;
import cn.com.yusys.yusp.oca.domain.bo.AdminSmBusiFuncBo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmBusiFuncEntity;
import cn.com.yusys.yusp.oca.domain.query.AdminSmBusiFuncQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmBusiFuncVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * 系统业务功能表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-20 13:43:51
 */
public interface AdminSmBusiFuncService extends IService<AdminSmBusiFuncEntity> {

    /**
     * 默认生成查询
     *
     * @param params
     * @return
     */
    PageUtils queryPage(Map<String, Object> params);

    /**
     * 业务功能管理查询
     *
     * @param adminSmBusiFuncQuery
     * @return
     */
    Page<AdminSmBusiFuncVo> queryPageWithCondition(AdminSmBusiFuncQuery adminSmBusiFuncQuery);

    /**
     * 批量删除业务功能
     *
     * @param funcIds
     */
    void removeFuncByIds(String[] funcIds);

    Page<AdminSmBusiFuncVo> getFuncInfoWithCondition(AdminSmBusiFuncQuery adminSmBusiFuncQuery);

    /**
     * 保存 业务功能
     *
     * @param adminSmBusiFuncBo
     * @return
     */
    int saveBusiFuncByBo(AdminSmBusiFuncBo adminSmBusiFuncBo);
}

