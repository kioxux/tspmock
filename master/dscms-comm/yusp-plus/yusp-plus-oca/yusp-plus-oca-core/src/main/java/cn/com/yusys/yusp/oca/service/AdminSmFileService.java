package cn.com.yusys.yusp.oca.service;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmLogPojo;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Map;

public interface AdminSmFileService {
    /**
     * 生成获取文件的唯一 id
     *
     * @return
     */
    String fileCode();

    /**
     * 生成文件并发送到文件系统
     *
     * @param params
     */
    void asyncSendFile(Map<String, String> params, String fileCode);

    /**
     * 按照条件查询并组装excel文件
     *
     * @param logPojo
     * @return
     */
    ProgressDto translateFile(AdminSmLogPojo logPojo);

    /**
     * 异步导入文件
     *
     * @param uploadFile
     * @return
     */
    ProgressDto asyncImport(MultipartFile uploadFile);

    /**
     * 导出空模板
     */
    File downLoadTemplate();
}
