package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.oca.domain.entity.AdminSmMessageEntity;
import cn.com.yusys.yusp.common.query.PageQuery;
import lombok.Data;

/**
 * @author JP
 */
@Data
public class AdminSmMessageQuery extends PageQuery<AdminSmMessageEntity> {

    /**
     * 信息码
     */
    private String code;
    /**
     * 提示内容
     */
    private String message;

    /**
     * 查询关键字
     */
    private String keyWord;
}
