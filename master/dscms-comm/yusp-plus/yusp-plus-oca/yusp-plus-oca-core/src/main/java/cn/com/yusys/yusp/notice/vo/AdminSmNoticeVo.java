package cn.com.yusys.yusp.notice.vo;

import cn.com.yusys.yusp.notice.entity.AdminSmRicheditFileInfoEntity;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class AdminSmNoticeVo {

    private static final long serialVersionUID = 1L;
    /**
     * 公告编号.
     */
    private String noticeId;
    /**
     * 公告标题.
     */
    private String noticeTitle;
    /**
     * 公告重要程度.
     */
    private String noticeLevel;
    /**
     * 有效期至.
     */
    private String activeDate;
    /**
     * 是否置顶.
     */
    private String isTop;
    /**
     * 置顶有效期.
     */
    private String topActiveDate;
    /**
     * 富文本表关联 id.
     */
    private String richeditId;

    private String noticeContent;
    /**
     * 富文本内容.
     */
    private String context;
    /**
     * 发布状态（状态：对应字典项=NORM_STS C：未发布O：已发布）.
     */
    private String pubSts;
    /**
     * 发布时间.
     */
    private String pubTime;
    /**
     * 公告发布人姓名
     */
    private String pubUserName;
    /**
     * 发布机构名称
     */
    private String pubOrgName;
    /**
     * 创建人姓名
     */
    private String creatorName;
    /**
     * 创建时间
     */
    private String creatorTime;
    /**
     * 公告权限
     */
    private Map<String, String> reciveOrgMap;
    private Map<String, String> reciveRoleMap;
    /**
     * 公告附件信息
     */
    private List<AdminSmRicheditFileInfoEntity> fileInfoFormList;
}
