package cn.com.yusys.yusp.oca.domain.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 资源对象授权记录表(含菜单、控制点、数据权限)
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-19 17:43:42
 */
@Data
@TableName("admin_sm_auth_reco")
public class AdminSmAuthRecoEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 记录编号
     */
    @TableId
    private String authRecoId;
    /**
     * 逻辑系统记录编号
     */
    private String sysId;
    /**
     * 授权对象类型（R-角色，U-用户，D-部门，G-机构，OU-对象组）
     */
    private String authobjType;
    /**
     * 授权对象记录编号
     */
    private String authobjId;
    /**
     * 授权资源类型（M-菜单，C-控制点，D-数据权限）
     */
    private String authresType;
    /**
     * 授权资源记录编号
     */
    private String authresId;
    /**
     * 最新变更用户
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
    /**
     * 最新变更时间
     */
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;
    /**
     * 菜单ID
     */
    private String menuId;

}
