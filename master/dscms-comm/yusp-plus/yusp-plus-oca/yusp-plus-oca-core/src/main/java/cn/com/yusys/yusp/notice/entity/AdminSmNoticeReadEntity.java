package cn.com.yusys.yusp.notice.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 系统公告用户查阅历史表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-15 13:05:27
 */
@Data
@TableName("admin_sm_notice_read")
public class AdminSmNoticeReadEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 记录编号
	 */
	@TableId
	private String readId;
	/**
	 * 公告编号
	 */
	private String noticeId;
	/**
	 * 用户编号
	 */
	private String userId;
	/**
	 * 阅读时间
	 */
	private String readTime;

}
