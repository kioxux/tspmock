package cn.com.yusys.yusp.oca.dao;

import cn.com.yusys.yusp.oca.domain.entity.AdminSmDataAuthTmplEntity;
import cn.com.yusys.yusp.oca.domain.vo.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 数据权限模板表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-08 18:53:56
 */

public interface AdminSmDataAuthTmplDao extends BaseMapper<AdminSmDataAuthTmplEntity> {

    Page<AdminSmDataAuthTmplVo> selectByCondition(Page<AdminSmDataAuthTmplVo> page, @Param(Constants.WRAPPER) QueryWrapper<AdminSmDataAuthTmplVo> queryWrapper);

    /**
     * 权限授权树数据查询，联合admin_sm_auth_reco表及admin_sm_data_auth表
     *
     * @param id
     * @return
     */
    List<AdminSmAuthTreeVo> selectAuthTree(String id);

    /**
     * 查询控制点已经关联的模板
     *
     * @param wrapper
     * @return
     */
    List<AdminDataAuthVo> selectAssociatedTmpl(@Param(Constants.WRAPPER) QueryWrapper<AdminDataAuthVo> wrapper);

    /**
     * 控制点页面需求的数据模板条件查询列表
     *
     * @param page
     * @param wrapper
     * @param contrId
     */
    Page<AdminSmDataTmplListVo> getListForContr(
            IPage<AdminSmDataTmplListVo> page,
            @Param(Constants.WRAPPER) QueryWrapper<AdminSmDataTmplListVo> wrapper,
            @Param("contrId") String contrId,
            @Param("lastTmplIds") String[] lastTmplIds);

    /**
     * 获取与控制点绑定的所有数据模板
     *
     * @param wrapper
     * @return
     */
    List<AdminDataAuthVo> associatedTmplList(@Param(Constants.WRAPPER) QueryWrapper<AdminDataAuthVo> wrapper);

    /**
     * 获取控制点已关联的数据模板
     *
     * @param contrId
     * @return
     */
    List<AdminSmDataAuthTmplEntity> selectByContrId(@Param("contrId") String contrId);

    /**
     * 数据模板授权页面列表数据
     *
     * @param queryWrapper
     * @return
     */
    List<AdminTmplAndRecoVo> getTmplAndRecoVoList(@Param(Constants.WRAPPER) QueryWrapper<AdminTmplAndRecoVo> queryWrapper);

    /**
     * 查询条件为已经勾选的数据
     *
     * @param page
     * @param wrapper
     */
    IPage<AdminSmDataTmplListVo> selectByCheck(
            IPage<AdminSmDataTmplListVo> page,
            @Param(Constants.WRAPPER) QueryWrapper<AdminSmDataTmplListVo> wrapper,
            @Param("contrId") String contrId);

    /**
     * 查询条件为已经勾选的数据
     *
     * @param page
     * @param wrapper
     */
    IPage<AdminSmDataTmplListVo> selectByContrCondition(
            IPage<AdminSmDataTmplListVo> page,
            @Param(Constants.WRAPPER) QueryWrapper<AdminSmDataTmplListVo> wrapper,
            @Param("contrId") String contrId);

    /**
     * 关联DataAuth表，使用contrId查询，返回AdminSmDataTmplListVo
     *
     * @param contrId
     * @return
     */
    List<AdminSmDataTmplListVo> getByContrIdForVo(@Param("contrId") String contrId);

    /**
     * 分页查询所有模板
     *
     * @param page
     * @param wrapper
     * @param contrId
     * @param check
     * @return 分页模板数据
     */
    IPage<AdminSmDataTmplListVo> selectAllTmpl(IPage<AdminSmDataTmplListVo> page,
                                               @Param(Constants.WRAPPER) QueryWrapper<AdminSmDataTmplListVo> wrapper,
                                               @Param("contrId") String contrId,
                                               @Param("check") int check);
}
