package cn.com.yusys.yusp.oca.domain.query;

import cn.com.yusys.yusp.common.query.PageQuery;
import cn.com.yusys.yusp.oca.domain.vo.AdminSmRoleVo;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class AdminSmPasteRoleQuery extends PageQuery<AdminSmRoleVo> {

    private String keyWord;

    @NotEmpty(message = "Which role do you want to copy from?")
    private String expectedRoleId;
}
