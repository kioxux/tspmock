package cn.com.yusys.yusp.message.form;

import cn.com.yusys.yusp.message.validator.MessageChannelType;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 消息模板表单
 *
 * @author xiaodg@yusys.com.cn
 **/
@Data
public class MessageTempForm implements Serializable {
    private static final long serialVersionUID = 5648282235034091630L;

    @NotBlank(message = "消息类型不能为空")
    @Size(max = 32, message = "错误的消息类型")
    private String messageType;
    /**
     * 适用渠道类型[system,email,mobile]
     */
    @NotBlank(message = "消息渠道不能为空")
    @MessageChannelType
    private String channelType;
    /**
     * 异常重发次数
     */
    @Positive(message = "错误的重发次数")
    private Integer sendNum;
    /**
     * 模板内容
     */
    @NotBlank(message = "模板内容不能为空")
    @Size(max = 2500, message = "错误的消息模板")
    private String templateContent;
    /**
     * 邮件/系统消息标题
     */
    private String emailTitle;
    /**
     * 发送开始时间
     */
    private String timeStart;
    /**
     * 发送结束时间
     */
    private String timeEnd;
    /**
     * 是否固定时间发送
     */
    private String isTime;
}
