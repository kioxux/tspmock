package cn.com.yusys.yusp.oca.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.oca.config.annotation.Strategy;
import cn.com.yusys.yusp.oca.domain.constants.AvailableStateEnum;
import cn.com.yusys.yusp.oca.domain.constants.ResponseAndMessageEnum;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.oca.domain.vo.UserEntityVo;
import cn.com.yusys.yusp.oca.service.AdminSmCrelStraService;
import cn.com.yusys.yusp.oca.service.AdminSmUserService;
import cn.com.yusys.yusp.oca.service.LoginCheckSecretService;
import cn.com.yusys.yusp.oca.utils.I18nMessageByCode;
import cn.com.yusys.yusp.oca.utils.PasswordUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @program: yusp-app-framework
 * @description: 登录验证
 * @author: wujiangpeng
 * @email: wujp4@yusys.com.cn
 * @create: 2020-10-29 10:57
 */
@Slf4j
@Service
public class LoginCheckSecretServiceImpl implements LoginCheckSecretService {

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private PasswordUtils passwordUtils;

    @Autowired
    private AdminSmCrelStraService adminSmCrelStraService;

    @Autowired
    private I18nMessageByCode i18nMessageByCode;

    /**
     * 校验用户密码
     *
     * @param loginCode
     * @param password
     * @return 用户信息
     */
    @Override
    @Strategy
    public ResultDto<UserEntityVo> queryUserAndCheckSecret(String loginCode, String password) {
        //查询用户信息
        AdminSmUserEntity userEntity = adminSmUserService.getOne(new QueryWrapper<AdminSmUserEntity>().eq("LOGIN_CODE", loginCode));

        if (Objects.nonNull(userEntity)) {
            //判断用户是否有效
            if (StringUtils.nonEmpty(userEntity.getUserSts().getCode()) && !StringUtils.equals(AvailableStateEnum.ENABLED.getCode(), userEntity.getUserSts().getCode())) {
                log.error("用户未生效");
                return ResultDto.error(ResponseAndMessageEnum.USER_INVALID.getCode(), i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.USER_INVALID.getCode()));
            }

            UserEntityVo userEntityVoDb = new UserEntityVo();
            BeanUtils.copyProperties(userEntity, userEntityVoDb);
            //当前密码
            String rawPassword = password;
            boolean isValid;
            //校验密码
            isValid = passwordUtils.checkSecret(userEntity.getUserPassword(), rawPassword);
            if (isValid) {
                //设置渠道互斥标识
                userEntityVoDb.setLoginSingleAgent(adminSmCrelStraService.getLoignSingleAgentEnabled());
                return ResultDto.success().code(ResponseAndMessageEnum.SUCCESS.getCode()).message(i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.SUCCESS.getCode())).data(userEntityVoDb);
            } else {
                log.error("用户密码校验失败");
                return ResultDto.error(ResponseAndMessageEnum.BAD_CREDENTIALS.getCode(), i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.BAD_CREDENTIALS.getCode())).extParam("loginCode", loginCode);
            }
        } else {
            log.error("用户不存在");
            return ResultDto.error(ResponseAndMessageEnum.NON_USER.getCode(), i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.NON_USER.getCode()));
        }
    }

    @Override
    public ResultDto<UserEntityVo> checkAppClientUserExist(String servtp, String loginCode, String orgCode) {
        //查询用户信息
        LambdaQueryWrapper<AdminSmUserEntity> queryWrapper = new QueryWrapper<AdminSmUserEntity>().lambda();
        queryWrapper.eq(AdminSmUserEntity::getLoginCode, loginCode);
        queryWrapper.eq(AdminSmUserEntity::getOrgId, orgCode);// 系统中机构ID即为机构码
        AdminSmUserEntity userEntity = adminSmUserService.getOne(queryWrapper);
        if (Objects.nonNull(userEntity)) {
            //判断用户是否有效
            if (StringUtils.nonEmpty(userEntity.getUserSts().getCode()) && !StringUtils.equals(AvailableStateEnum.ENABLED.getCode(), userEntity.getUserSts().getCode())) {
                log.error("用户未生效");
                return ResultDto.error(ResponseAndMessageEnum.USER_INVALID.getCode(), i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.USER_INVALID.getCode()));
            }
            UserEntityVo userEntityVoDb = new UserEntityVo();
            BeanUtils.copyProperties(userEntity, userEntityVoDb);
            return ResultDto.success().message("移动端用户登录验证成功！").data(userEntityVoDb);
        } else {
            log.error("机构[{}]下用户[{}]不存在!", loginCode, orgCode);
            return ResultDto.error(ResponseAndMessageEnum.NON_USER.getCode(), ResponseAndMessageEnum.NON_USER.getMessage());
        }
    }

    @Override
    public ResultDto<UserEntityVo> checkUserMobile(String loginCode, String mobile) {
        //查询用户信息
        AdminSmUserEntity userEntity = adminSmUserService.getOne(new QueryWrapper<AdminSmUserEntity>().eq("LOGIN_CODE", loginCode));
        if (Objects.nonNull(userEntity)) {
            //判断用户是否有效
            if (StringUtils.nonEmpty(userEntity.getUserSts().getCode()) && !StringUtils.equals(AvailableStateEnum.ENABLED.getCode(), userEntity.getUserSts().getCode())) {
                log.error("用户未生效");
                return ResultDto.error(ResponseAndMessageEnum.USER_INVALID.getCode(), i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.USER_INVALID.getCode()));
            }

            //校验手机号
            if(mobile.equals(userEntity.getUserMobilephone())) {
                return ResultDto.success().code(ResponseAndMessageEnum.SUCCESS.getCode()).message(i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.SUCCESS.getCode())).data(userEntity);
            }else {
                log.error("用户手机号校验不通过！");
                return ResultDto.error(ResponseAndMessageEnum.BAD_CREDENTIALS.getCode(), i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.BAD_CREDENTIALS.getCode())).extParam("loginCode", loginCode);
            }
        } else {
            log.error("用户不存在");
            return ResultDto.error(ResponseAndMessageEnum.NON_USER.getCode(), i18nMessageByCode.getMessageByCode(ResponseAndMessageEnum.NON_USER.getCode()));
        }
    }
}
