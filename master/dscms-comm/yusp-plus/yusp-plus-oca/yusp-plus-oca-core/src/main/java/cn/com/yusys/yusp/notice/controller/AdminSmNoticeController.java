package cn.com.yusys.yusp.notice.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.notice.entity.AdminSmNoticeEntity;
import cn.com.yusys.yusp.notice.form.AdminSmNoticeCondition;
import cn.com.yusys.yusp.notice.form.AdminSmNoticeForm;
import cn.com.yusys.yusp.notice.service.AdminSmNoticeService;
import cn.com.yusys.yusp.notice.vo.AdminSmNoticeVo;
import cn.com.yusys.yusp.notice.vo.NoticeHomePageVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 系统公告表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-15 13:04:49
 */
@Slf4j
@RestController
@RequestMapping("/api/adminsmnotice")
public class AdminSmNoticeController {

    @Autowired
    private AdminSmNoticeService adminSmNoticeService;

    /**
     * 查询有权看的已发布的公告
     * @param adminSmNoticeCondition：需要前端传递条件参数
     * @return
     */
    @GetMapping("/view/list")
    public ResultDto getViewList(AdminSmNoticeCondition adminSmNoticeCondition) {
        IPage<NoticeHomePageVo> iPage = adminSmNoticeService.getViewList(adminSmNoticeCondition);
        return ResultDto.success(iPage);
    }

    /**
     * 查询有权限看的未读的公告
     * @return
     */
    @GetMapping("/unread/list")
    public ResultDto getUnreadList() {
        List<AdminSmNoticeEntity> list = adminSmNoticeService.getUnreadList();
        return ResultDto.success(list);
    }

    /**
     * 查询自己编写的公告
     * @param adminSmNoticeCondition
     * @return
     */
    @GetMapping("/control/list")
    public ResultDto getControlList(AdminSmNoticeCondition adminSmNoticeCondition) {
        log.info("/api/adminsmnotice/control/list? 接口获取的参数：{}", adminSmNoticeCondition);
        IPage<AdminSmNoticeEntity> iPage = adminSmNoticeService.getControlList(adminSmNoticeCondition);
        return ResultDto.success(iPage);
    }

    /**
     * 新增公告
     * @param adminSmNoticeForm
     * @return
     */
    @PostMapping("/add")
    public ResultDto createNotice(@RequestBody AdminSmNoticeForm adminSmNoticeForm) {
        log.info("富文本中的附件信息 - createNotice.adminSmNoticeForm.getList: " + adminSmNoticeForm.getFileInfoFormList());
        adminSmNoticeService.createNotice(adminSmNoticeForm);
        return ResultDto.success();
    }

    /**
     * 删除公告
     * @param noticeIds
     * @return
     */
    @PostMapping("/delete")
    public ResultDto deleteNotice(@RequestBody List<String> noticeIds) {
        String message = adminSmNoticeService.deleteNotice(noticeIds);
        if (message == null) {
            return ResultDto.success();
        } else {
            return ResultDto.error(message);
        }
    }

    /**
     * 修改公告
     * @param adminSmNoticeForm
     * @return
     */
    @PostMapping("/update")
    public ResultDto updateNotice(@RequestBody AdminSmNoticeForm adminSmNoticeForm) {
        log.info("富文本中的附件信息 - updateNotice.adminSmNoticeForm.getList: " + adminSmNoticeForm.getFileInfoFormList());
        String message = adminSmNoticeService.updateNotice(adminSmNoticeForm);
        if (message != null) {
            return ResultDto.error(message);
        }
        return ResultDto.success();
    }

    /**
     * 发布公告(批量)
     * @param noticeIds
     * @return
     */
    @PostMapping("/pub")
    public ResultDto pubNotices(@RequestBody List<String> noticeIds) {
        adminSmNoticeService.pubNotices(noticeIds);
        return ResultDto.success();
    }

    /**
     * 公告信息查询
     * @param noticeId
     * @return
     */
    @GetMapping("/info/{noticeId}")
    public ResultDto getInfo(@PathVariable("noticeId") String noticeId) {
        AdminSmNoticeVo noticeVo = adminSmNoticeService.getInfo(noticeId);
        return ResultDto.success(noticeVo);
    }

}
