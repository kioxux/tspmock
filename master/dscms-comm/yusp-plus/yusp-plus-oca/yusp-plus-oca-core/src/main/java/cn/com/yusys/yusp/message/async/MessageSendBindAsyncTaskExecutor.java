package cn.com.yusys.yusp.message.async;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 消息发送异步任务服务类
 * <p>
 * 异步(延迟)发送消息到Binder
 *
 * @author xiaodg@yusys.com.cn
 **/
@Component
@Slf4j
public class MessageSendBindAsyncTaskExecutor implements DisposableBean {
    ScheduledThreadPoolExecutor executor;

    @PostConstruct
    public void init() {
        // 核心线程池大小取CPU的核数
        int corePoolSize = Runtime.getRuntime().availableProcessors() / 2;
        if (corePoolSize < 1) {
            // 至少一个核心线程
            corePoolSize = 1;
        }
        executor = new ScheduledThreadPoolExecutor(corePoolSize, new CustomizableThreadFactory("message-send-pool-"));
    }


    /**
     * 延迟发送消息
     *
     * @param runnable     发送消息线程 {@link Runnable}
     * @param delaySeconds 延迟秒数
     */
    public void doSend(Runnable runnable, long delaySeconds) {
        executor.schedule(runnable, delaySeconds, TimeUnit.SECONDS);
    }

    /**
     * 发送消息
     *
     * @param runnable 发送消息线程 {@link Runnable}
     */
    public void doSend(Runnable runnable) {
        executor.execute(runnable);
    }


    @Override
    public void destroy() {
        if (Objects.nonNull(executor)) {
            executor.shutdown();
        }
    }
}
