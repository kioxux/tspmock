package cn.com.yusys.yusp.message.query;

import cn.com.yusys.yusp.message.entity.MessagePoolEntity;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 消息队列分页查询对象
 * @author xiaodg@yusys.com.cn
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MessagePoolPageQuery extends Page<MessagePoolEntity> {

    private static final long serialVersionUID = -2363641603102296411L;
    /**
     * 用户码
     */
    private String userNo;
    /**
     * 渠道类型(email,system,mobile)
     */
    private String channelType;
    /**
     * 消息类型
     */
    private String messageType;

}
