package cn.com.yusys.yusp.oca.domain.form;

import lombok.Data;

@Data
public class AdminSmDataAuthTmplForm {

    private String authTmplName;

    private String sqlName;

    private String keyWord;

    private Long page;

    private Long size;
}
