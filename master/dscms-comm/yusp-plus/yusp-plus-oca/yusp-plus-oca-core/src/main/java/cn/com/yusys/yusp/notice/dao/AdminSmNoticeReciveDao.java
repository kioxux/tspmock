package cn.com.yusys.yusp.notice.dao;

import cn.com.yusys.yusp.notice.entity.AdminSmNoticeReciveEntity;
import cn.com.yusys.yusp.notice.vo.AdminSmNoticeReciveVo;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmOrgEntity;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmRoleEntity;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统公告表接收对象表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-15 13:05:33
 */

public interface AdminSmNoticeReciveDao extends BaseMapper<AdminSmNoticeReciveEntity> {

    List<AdminSmNoticeReciveEntity> selectByCondition(@Param(Constants.WRAPPER) QueryWrapper<AdminSmNoticeReciveEntity> query);

    /**
     * 查询该公告角色权限的角色ID和角色名称
     * @param queryWrapper
     * @return
     */
    List<AdminSmRoleEntity> selectRoles(@Param(Constants.WRAPPER) QueryWrapper<AdminSmRoleEntity> queryWrapper);

    /**
     * 查询该公告部门权限的角色ID和部门名称
     * @param queryWrapper
     * @return
     */
    List<AdminSmOrgEntity> selectOrgs(@Param(Constants.WRAPPER) QueryWrapper<AdminSmOrgEntity> queryWrapper);

    /**
     * 查询公告权限的机构信息和角色信息
     * @param noticeId
     * @return
     */
    List<AdminSmNoticeReciveVo> selectReciveIdAndName(String noticeId);
}
