package cn.com.yusys.yusp.message.channel;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.message.config.MessageDefaultTitle;
import cn.com.yusys.yusp.message.constant.ReceivedUserTypeEnum;
import cn.com.yusys.yusp.message.entity.MessageContentEntity;
import cn.com.yusys.yusp.message.entity.MessagePoolEntity;
import cn.com.yusys.yusp.message.util.MessageUtils;
import cn.com.yusys.yusp.notice.constant.NoticeConstant;
import cn.com.yusys.yusp.notice.entity.AdminSmNoticeEntity;
import cn.com.yusys.yusp.notice.entity.AdminSmNoticeReciveEntity;
import cn.com.yusys.yusp.notice.service.AdminSmNoticeReciveService;
import cn.com.yusys.yusp.notice.service.AdminSmNoticeService;
import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

/**
 * 发布系统消息
 *
 * @author xiaodg@yusys.com.cn
 **/
@Component
@Slf4j
public class SystemChannelPublisher implements MessageChannelPublisher {

    @Autowired
    private AdminSmNoticeService noticeService;

    @Autowired
    private AdminSmNoticeReciveService adminSmNoticeReciveService;

    @Autowired
    MessageDefaultTitle messageDefaultTitle;

    @Override
    @Async
    public CompletableFuture<Boolean> publish(MessagePoolEntity messagePoolEntity, MessageContentEntity messageContentEntity, AdminSmUserEntity userEntity) {
        // 将消息转为系统公告保存
        if(ReceivedUserTypeEnum.JKR.getCode().equals(messagePoolEntity.getUserType())){
            log.error("发送系统消息: 接收人类型为借款人，非本系统用户，无法发送系统消息！");
            return CompletableFuture.completedFuture(false);
        }
        try {
            // 消息接收表添加记录
            String noticeId = StringUtils.getUUID();
            final AdminSmNoticeEntity adminSmNoticeEntity = new AdminSmNoticeEntity();
            adminSmNoticeEntity.setNoticeTitle(MessageUtils.requireNonNullAndEmptyString(messageContentEntity.getEmailTitle(), () -> messageDefaultTitle.getDefaultMessageTitle()));
            adminSmNoticeEntity.setNoticeContent(messageContentEntity.getContent());
            adminSmNoticeEntity.setPubTime(DateUtils.formatDateTimeByDef());
            adminSmNoticeEntity.setPubUserId("System"); // 默认固定，系统推送
            adminSmNoticeEntity.setPubSts("O");
            adminSmNoticeEntity.setNoticeId(noticeId);
            boolean result1 = noticeService.save(adminSmNoticeEntity);

            AdminSmNoticeReciveEntity adminSmNoticeReciveEntity = new AdminSmNoticeReciveEntity();
            adminSmNoticeReciveEntity.setReciveOgjId(messagePoolEntity.getUserNo());
            adminSmNoticeReciveEntity.setReciveType(NoticeConstant.RECIVE_TYPE_USER);
            adminSmNoticeReciveEntity.setNoticeId(noticeId);
            boolean result2 = adminSmNoticeReciveService.save(adminSmNoticeReciveEntity);
            return CompletableFuture.completedFuture(result1 && result2);
        } catch (Exception e) {
            log.error("发送系统消息: 系统消息转公告失败, ", e);
            return CompletableFuture.completedFuture(false);
        }
    }

}
