package cn.com.yusys.yusp.sequence.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SequenceConfigUpdateVo {
    /**
     * 主键
     */
    @NotNull(message = "ID cannot be empty!")
    private String id;
    /**
     * 序列名称
     */
    @NotNull(message = "Sequence name cannot be empty!")
    @Size(min = 0, max = 25)
    private String seqName;
    /**
     * 序列ID
     */
    @NotNull(message = "Sequence ID cannot be empty!")
    @Size(min = 0, max = 25)
    private String seqId;
    /**
     * 开始值
     */
    @NotNull(message = "The start value cannot be empty!")
    private Integer startvalue;
    /**
     * 最大值
     */
    @NotNull(message = "The maximum value cannot be empty!")
    private Integer maximumvalue;
    /**
     * 自增值
     */
    @NotNull(message = "The auto-increment value cannot be empty!")
    private Integer incrementvalue;
    /**
     * 是否循环
     */
    @Size(min = 0, max = 2, message = "Characters cannot be longer than 2!")
    private String isCycle;
    /**
     * 缓存值
     */
    @NotNull(message = "The cache value cannot be empty!")
    private Integer cachevalue;
    /**
     * 序列模版
     */
    @NotNull(message = "Sequence template cannot be empty!")
    private String seqTemplet;
    /**
     * 序列用的位数
     */
    @NotNull(message = "The number of digits used in the sequence cannot be empty!")
    private Integer seqPlace;
    /**
     * 不足位数是否用0补全
     */
    @Size(max = 2)
    private String zeroFill;
    /**
     * 序列是否已生成
     */
    @Size(max = 2)
    private String seqCreate;

}
