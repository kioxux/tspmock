package cn.com.yusys.yusp.oca.dao;

import cn.com.yusys.yusp.oca.domain.entity.AdminSmUserRoleRelEntity;
import cn.com.yusys.yusp.oca.domain.vo.UserRelationshipVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户角色关联表
 * 
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-18 18:06:35
 */

public interface AdminSmUserRoleRelDao extends BaseMapper<AdminSmUserRoleRelEntity> {

    List<UserRelationshipVo> selectUserPage(@Param(Constants.WRAPPER) QueryWrapper<UserRelationshipVo> queryWrapper, @Param("roleId") String role_id);

}
