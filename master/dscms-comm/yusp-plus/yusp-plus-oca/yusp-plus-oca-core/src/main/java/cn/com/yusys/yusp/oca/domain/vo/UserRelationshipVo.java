package cn.com.yusys.yusp.oca.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 带关联状态的用户列表（机构、角色、岗位）
 */
@Data
public class UserRelationshipVo implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 记录编号
	 */
	private String userId;
	/**
	 * 账号
	 */
	private String loginCode;
	/**
	 * 姓名
	 */
	private String userName;
	/**
	 * 员工号
	 */
	private String userCode;
	/**
	 * 有效期到
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date deadline;
	/**
	 * 所属机构编号
	 */
	private String orgId;

	private String orgName;
	/**
	 * 关联信息状态
	 */
	private Boolean checked=false;

	/**
	 * 最新变更用户
	 */
	private String lastChgUsr;
	/**
	 * 最新变更时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastChgDt;

}
