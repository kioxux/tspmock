package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.TaskUrgentAppDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "yusp-app-oca", path = "/api/taskurgentapp", fallback = TaskUrgentAppClientServiceHystrix.class)
public interface TaskUrgentAppClientService {

    @PostMapping("/create")
    ResultDto<Integer> createTaskUrgentApp(@RequestBody TaskUrgentAppDto taskUrgentAppDto);
}
