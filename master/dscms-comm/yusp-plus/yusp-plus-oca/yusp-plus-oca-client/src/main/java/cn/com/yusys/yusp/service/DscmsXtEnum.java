package cn.com.yusys.yusp.service;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

public enum DscmsXtEnum {

    TRADE_CODE_XDXT0001("xdxt0001", "信贷同步人力资源"),//信贷同步人力资源
    TRADE_CODE_XDXT0002("xdxt0002", "根据直营团队类型查询客户经理工号"),
    TRADE_CODE_XDXT0003("xdxt0003", "分页查询小微客户经理"),//分页查询小微客户经理
    TRADE_CODE_XDXT0004("xdxt0004", "根据工号获取所辖区域"),//根据工号获取所辖区域
    TRADE_CODE_XDXT0005("xdxt0005", "查询客户经理所在分部编号"),//查询客户经理所在分部编号
    TRADE_CODE_XDXT0006("xdxt0006", "查询信贷用户的特定岗位信息"),//查询信贷用户的特定岗位信息
    TRADE_CODE_XDXT0007("xdxt0007", "根据客户经理所在机构号查询机构名称"),//根据客户经理所在机构号查询机构名称
    TRADE_CODE_XDXT0008("xdxt0008", "根据客户经理号查询账务机构号"),//根据客户经理号查询账务机构号
    TRADE_CODE_XDXT0009("xdxt0009", "客户经理是否为小微客户经理"),//客户经理是否为小微客户经理
    TRADE_CODE_XDXT0010("xdxt0010", "根据分中心负责人工号查询客户经理名单，包括工号、姓名"),//根据分中心负责人工号查询客户经理名单，包括工号、姓名
    TRADE_CODE_XDXT0011("xdxt0011", "客户经理信息详情查看"),//客户经理信息详情查看
    TRADE_CODE_XDXT0012("xdxt0012", "根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码"),//根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码
    TRADE_CODE_XDXT0013("xdxt0013", "树形字典通用列表查询"),//树形字典通用列表查询
    TRADE_CODE_XDXT0014("xdxt0014", "字典项对象通用列表查询"),//字典项对象通用列表查询
    TRADE_CODE_XDXT0015("xdxt0015", "用户机构角色信息列表查询"),

    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (DscmsXtEnum enumData : EnumSet.allOf(DscmsXtEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private DscmsXtEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (DscmsXtEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
