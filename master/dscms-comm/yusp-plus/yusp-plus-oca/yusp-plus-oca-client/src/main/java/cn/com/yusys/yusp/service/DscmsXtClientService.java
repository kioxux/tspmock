package cn.com.yusys.yusp.service;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdxt0001.req.Xdxt0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0001.resp.Xdxt0001DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0003.req.Xdxt0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0003.resp.Xdxt0003DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0008.req.Xdxt0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0008.resp.Xdxt0008DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0014.req.Xdxt0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0014.resp.Xdxt0014DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0015.req.Xdxt0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0015.resp.Xdxt0015DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsXtClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * 封装的接口类: 系统管理服务接口
 *
 * @author leehuang
 * @version 1.0
 */
@FeignClient(name = "yusp-app-oca", path = "/api", fallback = DscmsXtClientServiceImpl.class)
public interface DscmsXtClientService {
    /**
     * 交易码：xdxt0001
     * 交易描述：信贷同步人力资源
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/ocaxt4bsp/xdxt0001")
    public ResultDto<Xdxt0001DataRespDto> xdxt0001(Xdxt0001DataReqDto reqDto);

    /**
     * 交易码：xdxt0003
     * 交易描述：分页查询小微客户经理
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/ocaxt4bsp/xdxt0003")
    public ResultDto<Xdxt0003DataRespDto> xdxt0003(Xdxt0003DataReqDto reqDto);

    /**
     * 交易码：xdxt0008
     * 交易描述：根据客户经理号查询账务机构号
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/ocaxt4bsp/xdxt0008")
    public ResultDto<Xdxt0008DataRespDto> xdxt0008(Xdxt0008DataReqDto reqDto);


    /**
     * 交易码：xdxt0014
     * 交易描述：字典项对象通用列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/ocaxt4bsp/xdxt0014")
    public ResultDto<Xdxt0014DataRespDto> xdxt0014(Xdxt0014DataReqDto reqDto);

    /**
     * 交易码：xdxt0015
     * 交易描述：用户机构角色信息列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/ocaxt4bsp/xdxt0015")
    public ResultDto<Xdxt0015DataRespDto> xdxt0015(Xdxt0015DataReqDto reqDto);

    /**
     * 交易码：getInstanceNameByBizId
     * 交易描述：获取流程节点信息
     *
     * @param bizId
     * @return
     */
    @PostMapping("/flow/getInstanceNameByBizId")
    public ResultDto<List<String>> getInstanceNameByBizId(String bizId);
}