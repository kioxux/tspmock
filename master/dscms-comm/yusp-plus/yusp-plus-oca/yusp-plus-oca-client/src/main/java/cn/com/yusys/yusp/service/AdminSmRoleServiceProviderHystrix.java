package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.GetUserInfoByRoleCodeDto;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 查询用户信息调用oca feign接口
 *
 * @author xuchao
 * @date 2021-06-07 10:06:35
 */
@Component
public class AdminSmRoleServiceProviderHystrix implements AdminSmRoleService {

    @Override
    public ResultDto<List<AdminSmUserDto>> getuserlist(GetUserInfoByRoleCodeDto getUserInfoByRoleCodeDto) {
        return null;
    }
}