package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.AdminSmPropDto;
import cn.com.yusys.yusp.dto.AdminSmPropQueryDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 查询系统参数信息调用oca feign接口
 *
 * @author yangzai
 * @since 2021-06-21
 */
@FeignClient(name = "yusp-app-oca", path = "/api/adminsmprop", fallback = AdminSmPropServiceProviderHystrix.class)
public interface AdminSmPropService {

    /**
     * 查询系统参数对象信息
     *
     * @param queryDto 查询条件
     * @return 返回对象
     */
    @PostMapping(value = "/getpropvalue")
    ResultDto<AdminSmPropDto> getPropValue(@RequestBody AdminSmPropQueryDto queryDto);
}