package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.GetUserInfoByDutyCodeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 查询机构信息调用熔断
 *
 * @author zhangsm
 * @date 2021-05-14 10:06:35
 */
@Component
public class AdminSmDutyServiceProviderHystrix implements AdminSmDutyService {
    private static final Logger logger = LoggerFactory.getLogger(AdminSmDutyServiceProviderHystrix.class);
}