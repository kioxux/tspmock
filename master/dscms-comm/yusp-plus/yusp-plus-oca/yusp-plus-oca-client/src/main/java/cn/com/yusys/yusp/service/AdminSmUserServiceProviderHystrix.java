package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 查询用户信息调用熔断
 *
 * @author zhangsm
 * @date 2021-05-14 10:06:35
 */
@Component
public class AdminSmUserServiceProviderHystrix implements AdminSmUserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdminSmUserServiceProviderHystrix.class);


    @Override
    public ResultDto<AdminSmUserDto> getByLoginCode(String loginCode) {
        return null;
    }

    @Override
    public ResultDto<List<AdminSmUserDto>> getByLoginCodeList(List<String> loginCodes) {
        return null;
    }

    @Override
    public ResultDto<List<AdminSmUserDto>> getByName(String name) {
        return null;
    }

    @Override
    public ResultDto<GetIsXwUserDto> getIsXWUserByLoginCode(String loginCode) {
        return null;
    }

    @Override
    public ResultDto<List<AdminSmUserDto>> getByOrgId(String name) {
        return null;
    }

    @Override
    public ResultDto<List<UserAndDutyRespDto>> getUserAndDuty(UserAndDutyReqDto userAndDutyReqDto) {
        return null;
    }

    @Override
    public ResultDto<List<CommonUserQueryRespDto>> getCommonUserInfo(CommonUserQueryReqDto commonUserQueryReqDto) {
        return null;
    }

    @Override
    public ResultDto<AdminSmUserDto> getTodoUserInfo(String instanceId) {
        return null;
    }

    @Override
    public ResultDto<Integer> queryCusFlowCount(String cusId) {
        return null;
    }

    @Override
    public ResultDto<List<AdminSmUserDto>> getUserList(GetUserInfoByOrgCodeDto getUserInfoByOrgCodeDto) {
        LOGGER.error("根据机构编号查询客户信息失败，触发熔断！");
        return null;
    }
}