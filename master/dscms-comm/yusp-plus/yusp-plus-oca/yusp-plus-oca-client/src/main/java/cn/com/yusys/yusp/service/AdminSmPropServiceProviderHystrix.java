package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.AdminSmPropDto;
import cn.com.yusys.yusp.dto.AdminSmPropQueryDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 查询系统参数调用熔断
 *
 * @author yangzai
 * @since 2021-06-21
 */
@Component
public class AdminSmPropServiceProviderHystrix implements AdminSmPropService {

    private static final Logger logger = LoggerFactory.getLogger(AdminSmPropServiceProviderHystrix.class);

    @Override
    public ResultDto<AdminSmPropDto> getPropValue(AdminSmPropQueryDto queryDto) {
        logger.error("查询系统参数对象信息异常，触发熔断！");
        return null;
    }
}