package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.GetUserInfoByDutyCodeDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * 查询岗位信息调用oca feign接口
 *
 * @author 顾银华
 * @date 2021-08-13 14:45:35
 */
@FeignClient(name = "yusp-app-oca", path = "/api", fallback = AdminSmUserDutyRelServiceProviderHystrix.class)
public interface AdminSmUserDutyRelService {

    /**
     * 调用oca接口，通过岗位编号分页查询用户列表
     *
     * @param getUserInfoByDutyCodeDto 请求参数
     * @return
     */
    @PostMapping(value = "/adminsmuserdutyrel/getuserlist")
    ResultDto<List<AdminSmUserDto>> getUserList(@RequestBody GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto);

    /**
     * 调用oca接口，通过岗位编号分页查询用户列表
     *
     * @param getUserInfoByDutyCodeDto 请求参数
     * @return
     */
    @PostMapping(value = "/adminsmuserdutyrel/getuserlistnew")
    List<AdminSmUserDto> getUserListNew(@RequestBody GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto);
}