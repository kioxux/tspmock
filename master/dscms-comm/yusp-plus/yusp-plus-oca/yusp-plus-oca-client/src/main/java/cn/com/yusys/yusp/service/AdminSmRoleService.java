package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.GetUserInfoByRoleCodeDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * 查询用户信息调用oca feign接口
 *
 * @author xuchao
 * @date 2021-06-07 10:06:35
 */
@FeignClient(name = "yusp-app-oca", path = "/api", fallback = AdminSmRoleServiceProviderHystrix.class)
public interface AdminSmRoleService {

    /**
     * 调用oca接口，通过角色编号分页查询用户列表
     *
     * @param getUserInfoByRoleCodeDto 请求参数
     * @return
     */
    @PostMapping(value = "/adminsmrole/getuserlist")
    ResultDto<List<AdminSmUserDto>> getuserlist(@RequestBody GetUserInfoByRoleCodeDto getUserInfoByRoleCodeDto);
}