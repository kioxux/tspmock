package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.UserAndOrgInfoReqDto;
import cn.com.yusys.yusp.dto.UserAndOrgInfoRespDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * 查询机构信息调用oca feign接口
 *
 * @author zhangsm
 * @date 2021-05-14 10:06:35
 */
@FeignClient(name = "yusp-app-oca", path = "/api", fallback = AdminSmOrgServiceProviderHystrix.class)
public interface AdminSmOrgService {

    /**
     * 调用oca接口，通过机构代码获取机构信息
     *
     * @param orgCode 机构代码
     * @return
     */
    @PostMapping(value = "/adminsmorg/getbyorgcode")
    ResultDto<AdminSmOrgDto> getByOrgCode(@RequestBody String orgCode);

    /**
     * 调用oca接口，通过多个机构代码获取多条机构信息
     *
     * @param orgCodes 机构代码列表
     * @return
     */
    @PostMapping(value = "/adminsmorg/getbyorgcodelist")
    ResultDto<List<AdminSmOrgDto>> getByOrgCodeList(@RequestBody List<String> orgCodes);

    /**
     * 根据机构编号列表分页查询用户和机构信息
     *
     * @return
     */
    @PostMapping(value = "/adminsmorg/getuserandorginfo")
    ResultDto<List<UserAndOrgInfoRespDto>> getUserAndOrgInfo(@RequestBody UserAndOrgInfoReqDto userAndOrgInfoReqDto);

    /**
     * 根据机构编号获取下级机构号
     *
     * @author jijian_yx
     * @date 2021/8/24 14:44
     **/
    @PostMapping(value = "/adminsmorg/getlowerorgid")
    ResultDto<List<String>> getLowerOrgId(@RequestBody String orgCode);

    /**
     * 根据机构编号获取当前及下级机构信息
     *
     * @author 顾银华
     * @date add by 顾银华 at 2021-09-21 17:07:45
     **/
    @PostMapping(value = "/adminsmorg/getChildrenOrgList")
    ResultDto<List<AdminSmOrgDto>> getChildrenOrgList(@RequestBody String orgCode);

    /**
     * 根据查询类别查询当地机构与异地机构信息
     *
     * @author 顾银华
     * @date add by 顾银华 at 2021-09-21 17:07:45
     **/
    @PostMapping(value = "/adminsmorg/getRemoteOrgList")
    ResultDto<List<String>> getRemoteOrgList(@RequestBody String queryType);
}