package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.GetUserInfoByDutyCodeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 查询机构信息调用熔断
 *
 * @author zhangsm
 * @date 2021-05-14 10:06:35
 */
@Component
public class AdminSmUserDutyRelServiceProviderHystrix implements AdminSmUserDutyRelService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdminSmUserDutyRelServiceProviderHystrix.class);


    @Override
    public ResultDto<List<AdminSmUserDto>> getUserList(GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto) {
        LOGGER.error("根据岗位编号查询客户信息失败，触发熔断！");
        return null;
    }

    @Override
    public List<AdminSmUserDto> getUserListNew(GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto) {
        LOGGER.error("根据岗位编号查询客户信息失败，触发熔断！");
        return null;
    }
}