package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * 查询用户信息调用oca feign接口
 *
 * @author zhangsm
 * @date 2021-05-14 10:06:35
 */
@FeignClient(name = "yusp-app-oca", path = "/api", fallback = AdminSmUserServiceProviderHystrix.class)
public interface AdminSmUserService {

    /**
     * 调用oca接口，通过账号获取用户信息
     *
     * @param loginCode 账号
     * @return
     */
    @PostMapping(value = "/adminsmuser/getbylogincode")
    ResultDto<AdminSmUserDto> getByLoginCode(@RequestBody(required = true) String loginCode);

    /**
     * 调用oca接口，通过多个账号获取多个用户信息
     *
     * @param loginCodes 账号列表
     * @return
     */
    @PostMapping(value = "/adminsmuser/getbylogincodelist")
    ResultDto<List<AdminSmUserDto>> getByLoginCodeList(@RequestBody(required = true) List<String> loginCodes);

    /**
     * 调用oca接口，通过姓名模糊查询获取多个用户信息
     *
     * @param name 姓名
     * @return
     */
    @PostMapping(value = "/adminsmuser/getbyname")
    ResultDto<List<AdminSmUserDto>> getByName(@RequestBody(required = true) String name);

    /**
     * 调用oca接口，通过账号查询用户是否小微客户经理
     *
     * @param loginCode 账号
     * @return
     */
    @PostMapping(value = "/adminsmuser/getisxwuserbylogincode")
    ResultDto<GetIsXwUserDto> getIsXWUserByLoginCode(@RequestBody(required = true) String loginCode);

    /**
     * 调用oca接口，通过机构号查询用户信息
     *
     * @param orgId 机构号
     * @return
     */
    @PostMapping(value = "/adminsmuser/getbyorgid")
    ResultDto<List<AdminSmUserDto>> getByOrgId(@RequestBody(required = true) String orgId);

    /**
     * 调用oca接口，查询用户和岗位信息
     *
     * @param userAndDutyReqDto
     * @return
     */
    @PostMapping(value = "/adminsmuser/getuserandduty")
    ResultDto<List<UserAndDutyRespDto>> getUserAndDuty(@RequestBody UserAndDutyReqDto userAndDutyReqDto);

    /**
     * 调用oca接口，通用客户经理查询
     *
     * @param commonUserQueryReqDto
     * @return
     */
    @PostMapping(value = "/adminsmuser/getcommonuserinfo")
    ResultDto<List<CommonUserQueryRespDto>> getCommonUserInfo(@RequestBody CommonUserQueryReqDto commonUserQueryReqDto);

    @PostMapping(value = "/adminsmuser/instance/todouserinfo")
    ResultDto<AdminSmUserDto> getTodoUserInfo(@RequestBody String instanceId);

    /**
     * 调用oca接口，查询在途任务数量
     *
     * @param cusId
     * @return
     */
    @PostMapping(value = "/adminsmuser/queryCusFlowCount")
    ResultDto<Integer> queryCusFlowCount(@RequestBody String cusId);

    /**
     * 调用oca接口，通过机构编号分页查询用户列表
     * @author 顾银华
     * @param getUserInfoByOrgCodeDto 请求参数
     * @return
     */
    @PostMapping(value = "/adminsmuser/getuserlist")
    ResultDto<List<AdminSmUserDto>> getUserList(@RequestBody GetUserInfoByOrgCodeDto getUserInfoByOrgCodeDto);
}