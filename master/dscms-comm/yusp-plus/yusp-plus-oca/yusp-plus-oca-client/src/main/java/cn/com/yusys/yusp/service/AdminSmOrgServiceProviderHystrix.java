package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.UserAndOrgInfoReqDto;
import cn.com.yusys.yusp.dto.UserAndOrgInfoRespDto;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 查询机构信息调用熔断
 *
 * @author zhangsm
 * @date 2021-05-14 10:06:35
 */
@Component
public class AdminSmOrgServiceProviderHystrix implements AdminSmOrgService {

    @Override
    public ResultDto<AdminSmOrgDto> getByOrgCode(String loginCode) {
        return null;
    }

    @Override
    public ResultDto<List<AdminSmOrgDto>> getByOrgCodeList(List<String> loginCodes) {
        return null;
    }

    @Override
    public ResultDto<List<UserAndOrgInfoRespDto>> getUserAndOrgInfo(UserAndOrgInfoReqDto userAndOrgInfoReqDto) {
        return null;
    }

    @Override
    public ResultDto<List<String>> getLowerOrgId(String orgCode) {
        return null;
    }

    @Override
    public ResultDto<List<AdminSmOrgDto>> getChildrenOrgList(String orgCode) {
        return null;
    }

    @Override
    public ResultDto<List<String>> getRemoteOrgList(String queryType) {
        return null;
    }
}