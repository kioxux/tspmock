package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdxt0001.req.Xdxt0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0001.resp.Xdxt0001DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0003.req.Xdxt0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0003.resp.Xdxt0003DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0008.req.Xdxt0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0008.resp.Xdxt0008DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0014.req.Xdxt0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0014.resp.Xdxt0014DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0015.req.Xdxt0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0015.resp.Xdxt0015DataRespDto;
import cn.com.yusys.yusp.service.DscmsXtClientService;
import cn.com.yusys.yusp.service.DscmsXtEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DscmsXtClientServiceImpl implements DscmsXtClientService {
    private static final Logger logger = LoggerFactory.getLogger(DscmsXtClientService.class);

    /**
     * 交易码：xdxt0001
     * 交易描述：信贷同步人力资源
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdxt0001DataRespDto> xdxt0001(Xdxt0001DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsXtEnum.TRADE_CODE_XDXT0001.key.concat("|").concat(DscmsXtEnum.TRADE_CODE_XDXT0001.value));
        return null;
    }

    /**
     * 交易码：xdxt0003
     * 交易描述：分页查询小微客户经理
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdxt0003DataRespDto> xdxt0003(Xdxt0003DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsXtEnum.TRADE_CODE_XDXT0003.key.concat("|").concat(DscmsXtEnum.TRADE_CODE_XDXT0003.value));
        return null;
    }

    /**
     * 交易码：xdxt0008
     * 交易描述：根据客户经理号查询账务机构号
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdxt0008DataRespDto> xdxt0008(Xdxt0008DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsXtEnum.TRADE_CODE_XDXT0008.key.concat("|").concat(DscmsXtEnum.TRADE_CODE_XDXT0008.value));
        return null;
    }

    /**
     * 交易码：xdxt0014
     * 交易描述：字典项对象通用列表查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdxt0014DataRespDto> xdxt0014(Xdxt0014DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsXtEnum.TRADE_CODE_XDXT0014.key.concat("|").concat(DscmsXtEnum.TRADE_CODE_XDXT0014.value));
        return null;
    }

    /**
     * 交易码：xdxt0015
     * 交易描述：用户机构角色信息列表查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdxt0015DataRespDto> xdxt0015(Xdxt0015DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsXtEnum.TRADE_CODE_XDXT0015.key.concat("|").concat(DscmsXtEnum.TRADE_CODE_XDXT0015.value));
        return null;
    }

    /**
     * 交易码：getInstanceNameByBizId
     * 交易描述：获取流程节点信息
     *
     * @param bizId
     * @return
     */
    @Override
    public ResultDto<List<String>> getInstanceNameByBizId(String bizId) {
        logger.error("访问getInstanceNameByBizId失败，触发熔断。");
        return null;
    }
}
