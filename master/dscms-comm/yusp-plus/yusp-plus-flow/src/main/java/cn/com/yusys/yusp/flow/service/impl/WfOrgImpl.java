/**
 * Copyright (C), 2014-2021
 * FileName: WfOrgImpl
 * Author: Administrator
 * Date: 2021/3/17 15:14
 * Description:
 * History:
 * <author> <time> <version> <desc>
 * Administrator 15:14 1.0.0 新建类
 */

package cn.com.yusys.yusp.flow.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.domain.vo.*;
import cn.com.yusys.yusp.flow.dto.WFUserDto;
import cn.com.yusys.yusp.flow.dto.WFUserExtDto;
import cn.com.yusys.yusp.flow.other.org.*;
import cn.com.yusys.yusp.flow.repository.mapper.NWfOcaTaskpoolMapper;
import cn.com.yusys.yusp.flow.service.OcaServiceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


/**
 * 〈〉
 * @author zhui
 * @create 2021/3/17
 * @since 1.0.0
 */
public class WfOrgImpl implements OrgInterface {
    @Autowired
    private OcaServiceProvider ocaServiceProvider;
    @Autowired
    private NWfOcaTaskpoolMapper nWfOcaTaskpoolMapper;
    @Override
    public List<WFUser> getUsers(QueryModel param) {
        WfPageVo<WfAdminSmUserVo> usersForWf = ocaServiceProvider.getUsersForWf(param);
        List<WfAdminSmUserVo> users = usersForWf.getRecords();
        com.github.pagehelper.Page<WFUser> wfUsers = users.stream().map(vo -> {
            WFUser user = new WFUser();
            user.setUserId(vo.getUserId());
            user.setUserName(vo.getUserName());
            user.setUserMobile(vo.getUserMobilephone());
            user.setUserEmail(vo.getUserEmail());
            return user;
        }).collect(Collectors.toCollection(com.github.pagehelper.Page<WFUser>::new));
        wfUsers.setTotal(usersForWf.getTotal());
        return wfUsers;
    }

    @Override
    public List<WFDept> getDepts(QueryModel param) {
        WfPageVo<WfAdminSmDptVo> deptsForWf = ocaServiceProvider.getDeptsForWf(param);
        List<WfAdminSmDptVo> depts = deptsForWf.getRecords();

        com.github.pagehelper.Page<WFDept> wfDepts = depts.stream().map(vo -> {
            WFDept dept = new WFDept();
            dept.setDeptId(vo.getDptId());
            dept.setDeptName(vo.getDptName());
            return dept;
        }).collect(Collectors.toCollection(com.github.pagehelper.Page<WFDept>::new));
        wfDepts.setTotal(deptsForWf.getTotal());
        return wfDepts;
    }

    @Override
    public List<WFDuty> getDutys(QueryModel param) {
        WfPageVo<WfAdminSmDutyVo> dutysForWf = ocaServiceProvider.getDutysForWf(param);
        List<WfAdminSmDutyVo> dutys = dutysForWf.getRecords();

        com.github.pagehelper.Page<WFDuty> wfDuties = dutys.stream().map(vo -> {
            WFDuty duty = new WFDuty();
            duty.setDutyId(vo.getDutyId());
            duty.setDutyName(vo.getDutyName());
            return duty;
        }).collect(Collectors.toCollection(com.github.pagehelper.Page<WFDuty>::new));
        wfDuties.setTotal(dutysForWf.getTotal());
        return wfDuties;
    }

    @Override
    public List<WFOrg> getOrgs(QueryModel param) {
        WfPageVo<WfAdminSmOrgVo> orgsForWf = ocaServiceProvider.getOrgsForWf(param);
        List<WfAdminSmOrgVo> orgs = orgsForWf.getRecords();

        com.github.pagehelper.Page<WFOrg> wfOrgs = orgs.stream().map(vo -> {
            WFOrg org = new WFOrg();
            org.setOrgId(vo.getOrgId());
            org.setOrgName(vo.getOrgName());
            org.setOrgLevel(vo.getOrgLevel().toString());
            return org;
        }).collect(Collectors.toCollection(com.github.pagehelper.Page<WFOrg>::new));
        wfOrgs.setTotal(orgsForWf.getTotal());
        return wfOrgs;
    }

    @Override
    public List<WFRole> getRoles(QueryModel param) {
        WfPageVo<WfAdminSmRoleVo> rolesForWf = ocaServiceProvider.getRolesForWf(param);
        List<WfAdminSmRoleVo> roles = rolesForWf.getRecords();

        com.github.pagehelper.Page<WFRole> wfRoles = roles.stream().map(vo -> {
            WFRole role = new WFRole();
            role.setRoleId(vo.getRoleId());
            role.setRoleName(vo.getRoleName());
            return role;
        }).collect(Collectors.toCollection(com.github.pagehelper.Page<WFRole>::new));
        wfRoles.setTotal(rolesForWf.getTotal());
        return wfRoles;
    }

    @Override
    public List<WFUser> getUsersByOrg(String systemId, String orgId) {
        List<WfAdminSmUserVo> users = ocaServiceProvider.getUsersByOrgForWf(systemId,orgId);
        return getWfUserList(users);
    }

    public List<WFUser> getUsersByOrgAndDuty(String systemId, String orgId, String dutyId) {
        List<WfAdminSmUserVo> users = ocaServiceProvider.getUsersByOrgAndDutyForWf(orgId,dutyId);
        return getWfUserList(users);
    }

    @Override
    public List<WFUser> getUsersByDept(String systemId, String deptId) {
        List<WfAdminSmUserVo> users = ocaServiceProvider.getUsersByDeptForWf(systemId,deptId);
        return getWfUserList(users);
    }

    @Override
    public List<WFUser> getUsersByDuty(String systemId, String dutyId) {
        List<WfAdminSmUserVo> users = ocaServiceProvider.getUsersByDutyForWf(systemId,dutyId);
        return getWfUserList(users);
    }

    @Override
    public List<WFUser> getUsersByRole(String systemId, String roleId) {
        List<WfAdminSmUserVo> users = ocaServiceProvider.getUsersByRoleForWf(systemId,roleId);
        return getWfUserList(users);
    }

    @Override
    public WFUser getUserInfo(String systemId, String userId) {
        WfAdminSmUserVo userInfoForWf = ocaServiceProvider.getUserInfoForWf(systemId,userId);
        WFUser user = new WFUser();
        user.setUserId(userInfoForWf.getUserId());
        user.setUserName(userInfoForWf.getUserName());
        user.setUserMobile(userInfoForWf.getUserMobilephone());
        user.setUserEmail(userInfoForWf.getUserEmail());
        return user;
    }

    @Override
    public List<String> getLowerOrgId(String orgCode) {
        return ocaServiceProvider.getLowerOrgId(orgCode);
    }

    @Override
    public List<WFUserExtDto> getUserDetailBatch(List<WFUserDto> list) {
        return nWfOcaTaskpoolMapper.getUserDetailBatch(list);
    }

    public List<WFUser> getWfUserList(List<WfAdminSmUserVo> users){
        return users.stream().map(vo -> {
            WFUser user = new WFUser();
            user.setUserId(vo.getUserId());
            user.setUserName(vo.getUserName());
            user.setUserMobile(vo.getUserMobilephone());
            user.setUserEmail(vo.getUserEmail());
            return user;
        }).collect(Collectors.toList());
    }
}
