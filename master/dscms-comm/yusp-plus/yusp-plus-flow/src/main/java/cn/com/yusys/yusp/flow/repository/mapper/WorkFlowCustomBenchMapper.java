package cn.com.yusys.yusp.flow.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.domain.NWfUserDone;
import cn.com.yusys.yusp.flow.domain.dto.ResultInstanceTodoDto;
import cn.com.yusys.yusp.flow.domain.vo.ResultTodoOrderVo;
import cn.com.yusys.yusp.flow.domain.vo.ResultUserTodoNum;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/6/817:11
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
public interface WorkFlowCustomBenchMapper {


    /***
     * @param queryModel
     * @return java.util.Map<java.lang.String, java.lang.Integer>
     * @author tangxun
     * @date 2021/6/8 17:18
     * @version 1.0.0
     * @desc 首页统计条数
     * @修改历史: 修改时间    修改人员    修改原因
     */
    Map<String, Integer> queryFlowCont(QueryModel queryModel);

    /***
     * @param model
     * @return java.util.List<cn.com.yusys.yusp.flow.domain.dto.ResultInstanceTodoDto>
     * @author tangxun
     * @date 2021/6/16 19:27
     * @version 1.0.0
     * @desc 查询用户待办
     * @修改历史: 修改时间    修改人员    修改原因
     */
    List<ResultInstanceTodoDto> getInstanceInfoUserCallback(QueryModel model);

    /**
     * 获取用户待办数量
     *
     * @param userIds         需要统计的用户id，多个用逗号分隔
     * @param excludeBizTypes 需要排除的业务类型，多个用逗号分隔（被排除的业务类型不计入待办数量统计）
     * @return
     */
    List<ResultUserTodoNum> getUserTodoNum(@Param("userIds") String userIds, @Param("excludeBizTypes") String excludeBizTypes);

    List<ResultUserTodoNum> getUserTodoNumCreditCard(@Param("userIds") String userIds, @Param("excludeBizTypes") String excludeBizTypes);
    /**
     *  获取签到、且未请假、未提前签退的人员
     * @param userIds
     * @param currDateStr
     * @return
     */
    List<ResultUserTodoNum> getCentralDistributionUsers(@Param("userIds") String userIds, @Param("currDateStr") String currDateStr);


    /**
     * 获取未请假的人员
     * @param userIds
     * @param currDateStr
     * @return
     */
    List<ResultUserTodoNum> getCentralDistributionUsers2(@Param("userIds") String userIds, @Param("currDateStr") String currDateStr);

    /**
     * 获取客户在途流程
     * @param cusId
     * @return
     */
    List<NWfInstance> queryCusFlow(@Param("cusId") String cusId);


    /**
     * 查询客户在途业务,不包括退回到发起的
     * @param cusId
     * @return
     */
    List<NWfInstance> queryCusFlowNotIncludeReturnBack(@Param("cusId") String cusId);

    /**
     * 根据流程实例号或者发起人获取实例当前节点处理人
     * @param instanceIds
     * @return
     */
    List<ResultTodoOrderVo>  getProcessUserByInstanceIds(@Param("instanceIds") String instanceIds, @Param("flowStarter") String flowStarter);

    /**
     * 获取指定流程实例再指定人员待办中的排序（加急类型-加急时间（正）-流程流转时间）
     * @param userId
     * @param instanceId
     * @param excludeBizTypes 需要排除的业务类型，多个用逗号分隔（被排除的业务类型不计入待办数量统计）
     * @param inBizTypes 需要计入待办数量统计胡业务类型
     * @return
     */
    ResultTodoOrderVo getOrderInUserTodos(@Param("userId") String userId, @Param("instanceId") String instanceId, @Param("excludeBizTypes") String excludeBizTypes, @Param("inBizTypes") String inBizTypes);

    Integer queryFlowContByBizType(QueryModel queryModel);

    /**
     * 获取该笔业务数数据
     * @param instanceId
     * @return
     */
    NWfInstance queryDataInfo(@Param("instanceId") String instanceId);


    Integer checkUserLastCheckStatus(@Param("userCode") String userCode);

    /**
     *  获取岗位下未签到的用户
     * @param dutyId
     * @param currDateStr
     * @return
     */
    List<ResultUserTodoNum> getDutyUserNoLeave(@Param("dutyId") String dutyId, @Param("currDateStr") String currDateStr);

    /**
     *  获取所有节点的最后一条数据
     * @param dutyId
     * @return
     */
    List<NWfUserDone> selectNWfUserDoneByNodeId(@Param("nodeId") String dutyId);
}