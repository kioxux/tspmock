package cn.com.yusys.yusp.flow.constant;


public class DscmsCommonConstance {
    public static final String CREDIT_CARD_FLOW_BIZTYPES = "XK001,XK002,XK003,XK004,XK005,XK006"; // 信用卡相关流程业务类型 20211117-00092增加请假/提前签退
    public static final String JZZY_FLOW_BIZTYPES_NOT_INCLUDE = "XK001,XK002,XK003,XK004,XK005,XK006,XT001,XT002"; // 集中作业待办需排除的相关流程业务类型 20211117-00092增加请假/提前签退
    /**
     * // TODO  马顺补充
     * 信贷管理部核查岗，分配规则需要纳入业务量统计的（单一客户授信申报、合作方准入、集团首授信申报）的业务类型
     */
    public static final String XDB13_COUNT_FLOW_BIZTYPES = "SX001,SX002,SX008,SX009";
}
