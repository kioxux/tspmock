/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.flow.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.flow.domain.vo.NWfMonitorLogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.flow.domain.NWfMonitorLog;
import cn.com.yusys.yusp.flow.service.NWfMonitorLogService;

/**
 * @项目名称: yusp-plus-flow模块
 * @类名称: NWfMonitorLogResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-06 23:00:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/nwfmonitorlog")
public class NWfMonitorLogResource {
    @Autowired
    private NWfMonitorLogService nWfMonitorLogService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<NWfMonitorLog>> query() {
        QueryModel queryModel = new QueryModel();
        List<NWfMonitorLog> list = nWfMonitorLogService.selectAll(queryModel);
        return new ResultDto<List<NWfMonitorLog>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<NWfMonitorLog>> index(QueryModel queryModel) {
        List<NWfMonitorLog> list = nWfMonitorLogService.selectByModel(queryModel);
        return new ResultDto<List<NWfMonitorLog>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<NWfMonitorLog> show(@PathVariable("pkId") String pkId) {
        NWfMonitorLog nWfMonitorLog = nWfMonitorLogService.selectByPrimaryKey(pkId);
        return new ResultDto<NWfMonitorLog>(nWfMonitorLog);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<NWfMonitorLog> create(@RequestBody NWfMonitorLog nWfMonitorLog) throws URISyntaxException {
        nWfMonitorLogService.insert(nWfMonitorLog);
        return new ResultDto<NWfMonitorLog>(nWfMonitorLog);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody NWfMonitorLog nWfMonitorLog) throws URISyntaxException {
        int result = nWfMonitorLogService.update(nWfMonitorLog);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = nWfMonitorLogService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = nWfMonitorLogService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:save
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/logging")
    protected ResultDto saveLog(@RequestBody NWfMonitorLogVo nWfMonitorLog) {
        nWfMonitorLogService.loggingSave(nWfMonitorLog);
        return new ResultDto<>();
    }
}
