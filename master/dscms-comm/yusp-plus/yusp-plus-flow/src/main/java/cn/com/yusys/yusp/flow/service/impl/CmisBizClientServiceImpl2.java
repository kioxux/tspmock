package cn.com.yusys.yusp.flow.service.impl;
import cn.com.yusys.yusp.flow.service.CmisBizClientService;
import cn.com.yusys.yusp.flow.service.CmisBizClientService2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Component
public class CmisBizClientServiceImpl2 implements CmisBizClientService2 {

    private static final Logger logger = LoggerFactory.getLogger(CmisBizClientService.class);

    @Override
    public BigDecimal getCusGrpMemberLmtAmt(String serno,String grpNo) {
        return null;
    }

    @Override
    public String getAreaUsersByOrgId(String orgId) {
        return null;
    }
}
