//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package cn.com.yusys.yusp.flow.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.domain.vo.NWfMonitorLogVo;
import cn.com.yusys.yusp.flow.dto.*;
import cn.com.yusys.yusp.flow.dto.result.*;
import cn.com.yusys.yusp.flow.other.exception.WorkflowException;
import cn.com.yusys.yusp.flow.service.NWfInstanceService;
import cn.com.yusys.yusp.flow.service.NWfMonitorLogService;
import cn.com.yusys.yusp.flow.service.WorkflowBatchService;
import cn.com.yusys.yusp.flow.service.core.WorkflowBenchInterface;
import cn.com.yusys.yusp.flow.service.core.WorkflowEngineExtInterface;
import cn.com.yusys.yusp.flow.service.core.WorkflowEngineInterface;
import cn.com.yusys.yusp.flow.util.WorkFlowUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/api/core"})
public class WorkflowCoreResource {
    @Autowired
    private WorkflowEngineInterface workflowEngineService;
    @Autowired
    private WorkflowEngineExtInterface workflowEngineExtService;
    @Autowired
    private NWfInstanceService instanceService;
    @Autowired
    private WorkflowBatchService workflowEBatchService;
    @Autowired
    private WorkflowBenchInterface workflowBenchService;
    @Autowired
    private NWfMonitorLogService nWfMonitorLogService;

    private static final Logger log = LoggerFactory.getLogger(WorkflowCoreResource.class);

    public WorkflowCoreResource() {
    }

    @GetMapping({"/instanceInfo"})
    public ResultDto<ResultInstanceDto> instanceInfo(String instanceId, String nodeId, String userId) {
        ResultInstanceDto instanceInfo = this.workflowEngineService.getInstanceInfo(instanceId, nodeId, userId);
        return new ResultDto(instanceInfo);
    }

    @GetMapping({"/getComments"})
    public ResultDto<List<ResultCommentDto>> getComments(String mainInstanceId) {
        List<ResultCommentDto> comments = this.workflowEngineService.getComments(mainInstanceId);
        return new ResultDto(comments);
    }

    @GetMapping({"/getCommentsHis"})
    public ResultDto<List<ResultCommentDto>> getCommentsHis(String mainInstanceId) {
        List<ResultCommentDto> comments = this.workflowEngineService.getCommentsHis(mainInstanceId);
        return new ResultDto(comments);
    }

    @GetMapping({"/getAllComments"})
    public ResultDto<List<ResultCommentDto>> getAllComments(String mainInstanceId, String nodeId) {
        List<ResultCommentDto> comments = this.workflowEngineService.getAllComments(mainInstanceId, nodeId);
        return new ResultDto(comments);
    }

    @GetMapping({"/getAllCommentsByBizId"})
    public ResultDto<List<ResultCommentDto>> getAllCommentsByBizId(String bizId) {
        List<ResultCommentDto> comments = this.workflowEngineService.getAllCommentsByBizId(bizId);
        return new ResultDto(comments);
    }

    @PostMapping({"/saveComment"})
    public ResultDto<ResultCommentDto> saveComment(@Valid @RequestBody WFCommentDto comment) {
        ResultCommentDto instanceInfo = this.workflowEngineService.saveComment(comment);
        return new ResultDto(instanceInfo);
    }

    @GetMapping({"/signIn"})
    public ResultDto<ResultMessageDto> signIn(String instanceId, String nodeId, String userId) {
        return new ResultDto(this.workflowEngineService.signIn(instanceId, nodeId, userId));
    }

    @GetMapping({"/unSignIn"})
    public ResultDto<ResultMessageDto> unSignIn(String instanceId, String nodeId, String userId) {
        return new ResultDto(this.workflowEngineService.unSignIn(instanceId, nodeId, userId));
    }

    @PostMapping({"/start"})
    public ResultDto<ResultInstanceDto> start(@Valid @RequestBody WFStratDto stratDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineService.start(stratDto));
    }

    @PostMapping({"/startQuickly"})
    public ResultDto<List<ResultMessageDto>> startQuickly(@Valid @RequestBody WFStratDto stratDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineService.startQuickly(stratDto));
    }

    @PostMapping({"/startBatch"})
    public ResultDto<List<ResultMessageDto>> startBatch(@Valid @RequestBody WFStratBatchDto stratDto) throws WorkflowException {
        return new ResultDto(this.workflowEBatchService.startBatch(stratDto));
    }

    @PostMapping({"/startQuicklyBatch"})
    public ResultDto<List<ResultMessageDto>> startQuicklyBatch(@Valid @RequestBody WFStratBatchDto stratDto) throws WorkflowException {
        return new ResultDto(this.workflowEBatchService.startQuicklyBatch(stratDto));
    }

    @PostMapping({"/submit"})
    public ResultDto<List<ResultMessageDto>> submit(@Valid @RequestBody WFSubmitDto submitDto) throws WorkflowException {
        // 原待办数据
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("userId", submitDto.getComment().getUserId());
        queryModel.addCondition("instanceId", submitDto.getComment().getInstanceId());
        queryModel.addCondition("nodeId", submitDto.getComment().getNodeId());
        List<ResultInstanceTodoDto> todoList = this.workflowBenchService.getInstanceInfoUserTodo(queryModel);

        List<ResultMessageDto> result = this.workflowEngineService.submit(submitDto);
        // 流程提交日志记录
        if(CollectionUtils.isEmpty(todoList)){
            log.error("提交：提交前备份此笔待办以便记录日志时，未查询到待办详细信息");
        }else{
            NWfMonitorLogVo nWfMonitorLogVo = new NWfMonitorLogVo();
            BeanUtils.beanCopy(todoList.get(0), nWfMonitorLogVo);
            nWfMonitorLogVo.setNodeStartTime(todoList.get(0).getStartTime());
            nWfMonitorLogVo.setNodeOptType("01");
            nWfMonitorLogVo.setNextNodeInfos(submitDto.getNextNodeInfos());
            nWfMonitorLogService.loggingSave(nWfMonitorLogVo);
        }
        return new ResultDto(result);
    }

    @PostMapping({"/submitBatch"})
    public ResultDto<List<ResultMessageDto>> submitBatch(@Valid @RequestBody WFSubmitBatchDto wFSubmitBatchDto) throws WorkflowException {
        return new ResultDto(this.workflowEBatchService.submitBatch(wFSubmitBatchDto));
    }

    @GetMapping({"/getNextNodeInfos"})
    public ResultDto<List<ResultNodeDto>> getNextNodeInfos(String instanceId, String nodeId) throws WorkflowException {
        return new ResultDto(this.workflowEngineService.getNextNodeInfos(instanceId, nodeId));
    }

    @PostMapping({"/getNextNodeInfos"})
    public ResultDto<List<ResultNodeDto>> getNextNodeInfos(@RequestBody WFNextNodeDto param) throws WorkflowException {
        return this.workflowEngineService.getNextNodeInfos(param.getInstanceId(), param.getNodeId(), param.getParam());
    }

    @GetMapping({"/getNodeUsers"})
    public ResultDto<List<WFUserExtDto>> getNodeUsers(QueryModel queryModel) throws WorkflowException {
        ResultDto<List<WFUserExtDto>> data = new ResultDto();
        if (null != queryModel.getCondition().get("instanceId") && null != queryModel.getCondition().get("nodeId")) {
            String instanceId = queryModel.getCondition().get("instanceId").toString();
            String nodeId = queryModel.getCondition().get("nodeId").toString();
            NWfInstance instanceInfo = this.instanceService.selectByPrimaryKey(instanceId);
            Map<String, Object> param = WorkFlowUtil.strToMap(instanceInfo.getFlowParam());
            param.put("nextNodeId", nodeId);
            instanceInfo.setFlowParam(WorkFlowUtil.mapToStr(param));
            this.instanceService.updateByPrimaryKeySelective(instanceInfo);
            List<WFUserExtDto> users = this.workflowEngineService.getNodeUserDetail(instanceInfo.getInstanceId(), nodeId, instanceInfo.getOrgId(), instanceInfo.getSystemId());
            if (queryModel.getCondition().get("orgId") != null && !CollectionUtils.isEmpty(users)) {
                String orgId = queryModel.getCondition().get("orgId").toString();
                users = (List)users.stream().filter((item) -> {
                    return orgId.equals(item.getOrgId()) || "system_user".equals(item.getUserId()) || item.getUserId().startsWith("T.");
                }).collect(Collectors.toList());
            }

            data.setData(users);
            return data;
        } else {
            data.setMessage("部分参数为空！");
            return data;
        }
    }

    @PostMapping({"/returnBack"})
    public ResultDto<ResultMessageDto> returnBack(@Valid @RequestBody WFReturnDto paramDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.returnBack(paramDto));
    }

    @PostMapping({"/getCallBackNodes"})
    public ResultDto<List<ResultNodeDto>> getcallbackNodes(@Valid @RequestBody WFBaseDto paramDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.getCallbackNodes(paramDto));
    }

    @PostMapping({"/callBack"})
    public ResultDto<ResultMessageDto> callBack(@Valid @RequestBody WFCallbackDto callbackDto) throws WorkflowException {
        // 原待办数据
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("userId", callbackDto.getComment().getUserId());
        queryModel.addCondition("instanceId", callbackDto.getComment().getInstanceId());
        queryModel.addCondition("nodeId", callbackDto.getComment().getNodeId());
        List<ResultInstanceTodoDto> todoList = this.workflowBenchService.getInstanceInfoUserTodo(queryModel);
        ResultMessageDto result = this.workflowEngineExtService.callBack(callbackDto);

        // 流程提交日志记录
        if(CollectionUtils.isEmpty(todoList)){
            log.error("打回：提交前备份此笔待办以便记录日志时，未查询到待办详细信息");
        }else{
            NWfMonitorLogVo nWfMonitorLogVo = new NWfMonitorLogVo();
            BeanUtils.beanCopy(todoList.get(0), nWfMonitorLogVo);
            nWfMonitorLogVo.setNodeStartTime(todoList.get(0).getStartTime());
            nWfMonitorLogVo.setNodeOptType("01");
            List<NextNodeInfoDto> nextNodeInfos = new ArrayList<>();
            NextNodeInfoDto nextNodeInfoDto = new NextNodeInfoDto();
            nextNodeInfoDto.setNextNodeId(callbackDto.getCallbackNodeId());
            List<String> nextNodeUserIds = new ArrayList<>();
            nextNodeUserIds.add(callbackDto.getCallbackUserId());
            nextNodeInfoDto.setNextNodeUserIds(nextNodeUserIds);
            nextNodeInfos.add(nextNodeInfoDto);
            nWfMonitorLogVo.setNextNodeInfos(nextNodeInfos);
            nWfMonitorLogService.loggingSave(nWfMonitorLogVo);
        }
        return new ResultDto(result);
    }

    @PostMapping({"/getJumpNodes"})
    public ResultDto<List<ResultNodeDto>> getJumpNodes(@Valid @RequestBody WFBaseDto paramDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.getJumpNodes(paramDto));
    }

    @PostMapping({"/jump"})
    public ResultDto<ResultMessageDto> jump(@Valid @RequestBody WFJumpDto jumpDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.jump(jumpDto));
    }

    @PostMapping({"/tackback"})
    public ResultDto<ResultMessageDto> tackback(@Valid @RequestBody WFTakebackDto paramDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.tackback(paramDto));
    }

    @GetMapping({"/hangup"})
    public ResultDto<ResultMessageDto> hangup(String instanceId) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.hangup(instanceId));
    }

    @GetMapping({"/wakeup"})
    public ResultDto<ResultMessageDto> wakeup(String instanceId) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.wakeup(instanceId));
    }

    @GetMapping({"/urge"})
    public ResultDto<ResultMessageDto> urge(String instanceId, String nodeId) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.urge(instanceId, nodeId));
    }

    @PostMapping({"/getChangeUsers"})
    public ResultDto<List<WFUserDto>> getChangeUsers(@Valid @RequestBody WFBaseDto baseDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.getChangeUsers(baseDto));
    }

    @PostMapping({"/getAssistUsers"})
    public ResultDto<List<WFUserDto>> getAssistUsers(@Valid @RequestBody WFBaseDto baseDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.getAssistUsers(baseDto));
    }

    @PostMapping({"/assist"})
    public ResultDto<ResultMessageDto> assist(@Valid @RequestBody WFAssistDto baseDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.assist(baseDto));
    }

    @PostMapping({"/change"})
    public ResultDto<ResultMessageDto> change(@Valid @RequestBody WFChangeDto baseDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.change(baseDto));
    }

    @PostMapping({"/refuse"})
    public ResultDto<ResultMessageDto> refuse(@Valid @RequestBody WFRefuseDto baseDto) throws WorkflowException {
        // 原待办数据
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("userId", baseDto.getComment().getUserId());
        queryModel.addCondition("instanceId", baseDto.getComment().getInstanceId());
        queryModel.addCondition("nodeId", baseDto.getComment().getNodeId());
        List<ResultInstanceTodoDto> todoList = this.workflowBenchService.getInstanceInfoUserTodo(queryModel);
        ResultMessageDto result = this.workflowEngineExtService.refuse(baseDto);
        // 流程提交日志记录
        if(CollectionUtils.isEmpty(todoList)){
            log.error("否决：提交前备份此笔待办以便记录日志时，未查询到待办详细信息");
        }else{
            NWfMonitorLogVo nWfMonitorLogVo = new NWfMonitorLogVo();
            BeanUtils.beanCopy(todoList.get(0), nWfMonitorLogVo);
            nWfMonitorLogVo.setNodeStartTime(todoList.get(0).getStartTime());
            nWfMonitorLogVo.setNodeOptType("01");
            nWfMonitorLogService.loggingSave(nWfMonitorLogVo);
        }
        return new ResultDto(result);
    }

    @PostMapping({"/getCopyUsers"})
    public ResultDto<List<WFUserDto>> getCopyUsers(@Valid @RequestBody WFCopyDto baseDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.getCopyUsers(baseDto));
    }

    @PostMapping({"/subFlowStart"})
    public ResultDto<ResultMessageDto> subFlowStart(@Valid @RequestBody WFSubFlowDto baseDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.subFlowStart(baseDto));
    }

    @PostMapping({"/resetNodeUser"})
    public ResultDto<ResultMessageDto> resetNodeUser(@Valid @RequestBody WFResetUserDto baseDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.resetNodeUser(baseDto));
    }

    @GetMapping({"/myinstanceInfo"})
    public ResultDto<ResultInstanceDto> myinstanceInfo(String instanceId, String nodeId) {
        ResultInstanceDto instanceInfo = this.workflowEngineExtService.getInstanceInfo(instanceId, nodeId);
        return new ResultDto(instanceInfo);
    }

    @GetMapping({"/myinstanceInfoHis"})
    public ResultDto<ResultInstanceDto> myinstanceInfoHis(String instanceId, String nodeId) {
        ResultInstanceDto instanceInfo = this.workflowEngineExtService.getInstanceInfoHis(instanceId, nodeId);
        return new ResultDto(instanceInfo);
    }

    @GetMapping({"/delete"})
    public ResultDto<ResultMessageDto> delete(String instanceId) {
        ResultMessageDto ms = this.workflowEngineExtService.delete(instanceId);
        return new ResultDto(ms);
    }

    @PostMapping({"/deleteBatch"})
    public ResultDto<ResultMessageDto> deleteBatch(@RequestBody List<String> instanceIds) {
        ResultMessageDto ms = this.workflowEngineExtService.deleteBatch(instanceIds);
        return new ResultDto(ms);
    }

    @GetMapping({"/deleteByBizId"})
    public ResultDto<ResultMessageDto> deleteByBizId(String bizId) {
        ResultMessageDto ms = this.workflowEngineExtService.deleteByBizId(bizId);
        return new ResultDto(ms);
    }

    @PostMapping({"/deleteByBizIdBatch"})
    public ResultDto<ResultMessageDto> deleteByBizIdBatch(@RequestBody List<String> bizIds) {
        ResultMessageDto ms = this.workflowEngineExtService.deleteBatch(bizIds);
        return new ResultDto(ms);
    }

    @PostMapping({"/tackbackToFirst"})
    public ResultDto<ResultMessageDto> tackbackToFirst(@Valid @RequestBody WFTakebackDto baseDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.tackbackToFirst(baseDto));
    }

    @PostMapping({"/signTaskPool"})
    ResultDto<ResultMessageDto> signTaskPool(@Valid @RequestBody WFTaskpoolDto baseDto) {
        ResultMessageDto result = this.workflowEngineExtService.signTaskPool(baseDto.getInstanceId(), baseDto.getNodeId(), baseDto.getUserId(), baseDto.getPoolId());
        // 流程提交日志记录
        NWfMonitorLogVo nWfMonitorLogVo = new NWfMonitorLogVo();
        nWfMonitorLogVo.setInstanceId(baseDto.getInstanceId());
        nWfMonitorLogVo.setNodeId(baseDto.getNodeId());
        nWfMonitorLogVo.setUserId(baseDto.getUserId());
        nWfMonitorLogVo.setNodeOptType("06");
        nWfMonitorLogService.loggingSave(nWfMonitorLogVo);
         return new ResultDto(result);
    }

    @PostMapping({"/unsignTaskPool"})
    ResultDto<ResultMessageDto> unsignTaskPool(@Valid @RequestBody WFTaskpoolDto baseDto) {
        return new ResultDto(this.workflowEngineExtService.unsignTaskPool(baseDto.getInstanceId(), baseDto.getNodeId(), baseDto.getUserId(), baseDto.getPoolId()));
    }

    @GetMapping({"/activate"})
    public ResultDto<ResultMessageDto> activate(String instanceId, String currentUserId) {
        ResultMessageDto ms = this.workflowEngineExtService.activate(instanceId, currentUserId);
        return new ResultDto(ms);
    }

    @GetMapping({"/getInstanceByBiz"})
    public ResultDto<List<NWfUserTodoDto>> getInstanceByBiz(String bizId) throws WorkflowException {
        List<NWfUserTodoDto> result = this.workflowEngineExtService.getInstanceByBiz(bizId);
        return new ResultDto(result);
    }

    @GetMapping({"/isfirstbiznode"})
    public ResultDto<Boolean> isFirstNode(String nodeId) {
        return new ResultDto(this.workflowEngineExtService.isFisrtBizNode(nodeId));
    }

    @GetMapping({"/getCommentsByUserId"})
    public ResultDto<List<ResultCommentDto>> getCommentsByUserId(@RequestParam("userId") String userId, @RequestParam("type") String type) {
        return new ResultDto(this.workflowEngineExtService.getCommentsByUserId(userId, type));
    }

    @PostMapping({"/cancel"})
    public ResultDto<ResultMessageDto> cancel(@Valid @RequestBody WFCancelDto baseDto) {
        return new ResultDto(this.workflowEngineExtService.cancel(baseDto));
    }

    @PostMapping({"/getInstanceInfoBatch"})
    public ResultDto<List<ResultInstanceDto>> getInstanceInfoBatch(@RequestBody @Valid WFInstanceQueryDto wfInstanceQueryDto) {
        return new ResultDto(this.workflowEngineExtService.getInstanceInfoBatch(wfInstanceQueryDto));
    }

    @PostMapping({"/directSubmit"})
    public ResultDto<List<ResultMessageDto>> directSubmit(@RequestBody @Valid WFSubmitDto submitDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineService.directSubmit(submitDto));
    }

    @PostMapping({"/updateFlowParam"})
    public ResultDto<ResultMessageDto> updateFlowParam(@Valid @RequestBody WFBizParamDto paramDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineService.updateFlowParam(paramDto));
    }

    @PostMapping({"/cancelSubmit"})
    public ResultDto<ResultMessageDto> cancelSubmit(@Valid @RequestBody WFCancelDto paramDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.cancelSubmit(paramDto));
    }

    @PostMapping({"/changeBatch"})
    public ResultDto<ResultMessageDto> changeBatch(@Valid @RequestBody WFChangeBatchDto paramDto) throws WorkflowException {
        return new ResultDto(this.workflowEBatchService.changeBatch(paramDto));
    }

    @PostMapping({"/reStart"})
    public ResultDto<ResultMessageDto> reStart(@Valid @RequestBody WFTakebackDto paramDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.reStart(paramDto));
    }

    @PostMapping({"/getNodeAction"})
    public ResultDto<ResultOpTypeDto> getNodeAction(@Valid @RequestBody WFBaseDto paramDto) throws WorkflowException {
        return new ResultDto(this.workflowEngineExtService.getNodeAction(paramDto));
    }
}
