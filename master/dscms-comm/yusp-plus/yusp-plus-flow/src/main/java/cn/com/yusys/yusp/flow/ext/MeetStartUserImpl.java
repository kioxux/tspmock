package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.domain.NWfMetting;
import cn.com.yusys.yusp.flow.dto.WFUserDto;
import cn.com.yusys.yusp.flow.other.engine.NodeInfo;
import cn.com.yusys.yusp.flow.other.engine.init.EngineInterface;
import cn.com.yusys.yusp.flow.other.org.OrgInterface;
import cn.com.yusys.yusp.flow.other.org.WFUser;
import cn.com.yusys.yusp.flow.repository.mapper.NWfMettingMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service("MeetStartUserImpl")
public class MeetStartUserImpl extends StudioStrategyInterface{
    @Autowired
    private EngineInterface engineInfo;
    @Autowired
    private OrgInterface orgService;
    @Autowired
    private NWfMettingMapper meetMapper;
    @Override
    public List<WFUserDto> selectUser(List<WFUserDto> user, String instanceId, String nodeId, String orgId, String systemId) {
        List<WFUserDto> re = new ArrayList<>();
        WFUserDto User = new WFUserDto();
        // 查询会办会议记录
        QueryModel model = new QueryModel();
        model.addCondition("instanceId", instanceId);
        model.setSort("START_TIME DESC");
        List<NWfMetting> meets = meetMapper.selectByModel(model);
        for(NWfMetting meet:meets){
            String meetUser = meet.getUserId();
            List<String> node = engineInfo.getAllNextNodesId(meet.getNodeId());
            if(node.indexOf(nodeId)>-1){
                User.setUserId(meetUser);
                WFUser UserT = orgService.getUserInfo(null, meetUser);
                if(null!=UserT) {
                    BeanUtils.copyProperties(UserT, User);
                }
                break;
            }
        }
        re.add(User);
        return re;
    }

    @Override
    public int getOrder() {
        return 2;
    }

    @Override
    public String desc() {
        return "会议发起人";
    }

    @Override
    public String key() {
        return "MeetStartUserImpl";
    }

    @Override
    public String orgId() {
        return null;
    }
}
