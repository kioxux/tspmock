package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.other.engine.NodeInfo;
import cn.com.yusys.yusp.flow.other.engine.init.EngineInterface;
import cn.com.yusys.yusp.flow.other.org.OrgInterface;
import cn.com.yusys.yusp.flow.other.org.WFUser;
import cn.com.yusys.yusp.flow.repository.mapper.NWfInstanceMapper;
import cn.com.yusys.yusp.flow.util.WorkFlowUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("SelectUserImpl")
public class SelectUserImpl extends StudioUserInterface{


    @Autowired
    private NWfInstanceMapper nWfInstanceMapper;
    @Autowired
    private EngineInterface engineInfo;
    @Autowired
    private OrgInterface orgService;

    @Override
    public List<String> customUser(String instanceId, String orgId, String systemId) {
        NWfInstance nWfInstance = nWfInstanceMapper.selectByPrimaryKey(instanceId);
        //获取流程号和上一节点提交人id
        String flowParam = nWfInstance.getFlowParam();
        Map<String, Object> map = WorkFlowUtil.strToMap(flowParam);
        //获取所有下一节点号
        List<String> list = engineInfo.getNextNodeIds((String) map.get("submitNodeId"));
        //下一节点个上一节点相同的岗位号
        String dutyId = "";
        for (String nodeId:list) {
            //从缓存中获取下一节点配置信息
            NodeInfo node = engineInfo.getNodeInfo(nodeId);
            //下一节点配置人员（岗位+自定义类）
            List<String> nodeUsers = WorkFlowUtil.splitNodeUser(node.getNodeUser());
            if(nodeUsers.contains("E.SelectUserImpl")){
                for(String key:nodeUsers) {
                    if (key.startsWith("G")) {
                        dutyId = key.substring(2);
                        break;
                    }
                }
                break;
            }
        }
        //从下一节点的岗位人员中去除上一节点的发起人
        List<WFUser> users = orgService.getUsersByDuty(systemId, dutyId);
        List<String> re = new ArrayList<>();
        for(WFUser user:users){
            if(user.getUserId().equals((String) map.get("submitUserId"))){
                continue;
            }
            re.add(user.getUserId());
        }
        return re;
    }

    @Override
    public int getOrder() {
        return 5;
    }

    @Override
    public String desc() {
        return "节点人员筛选（去除上一节点提交人）";
    }

    @Override
    public String key() {
        return "SelectUserImpl";
    }

    @Override
    public String orgId() {
        return null;
    }
}
