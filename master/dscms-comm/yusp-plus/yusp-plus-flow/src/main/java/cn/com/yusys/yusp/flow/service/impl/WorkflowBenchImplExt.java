package cn.com.yusys.yusp.flow.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.domain.NWfMettingParamDto;
import cn.com.yusys.yusp.flow.domain.dto.ResultInstanceTodoDto;
import cn.com.yusys.yusp.flow.domain.dto.ResultMeettingDto;
import cn.com.yusys.yusp.flow.domain.dto.ResultVoteListDto;
import cn.com.yusys.yusp.flow.domain.dto.ResultVoteUserDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceTodoWithParamDto;
import cn.com.yusys.yusp.flow.repository.mapper.WorkflowBenchMapperExt;
import cn.com.yusys.yusp.flow.service.core.WorkflowBenchInterfaceExt;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 我的控制台
 *
 * @author figue
 */
@Service
public class WorkflowBenchImplExt implements WorkflowBenchInterfaceExt {

    @Autowired
    private WorkflowBenchMapperExt workflowBenchMapperExt;

    @Override
    public List<ResultInstanceTodoDto> getInstanceInfoUserTodo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ResultInstanceTodoDto> list = workflowBenchMapperExt.getInstanceInfoUserTodo(model);
        PageHelper.clearPage();
        return list;
    }

    @Override
    public List<ResultInstanceTodoDto> getInstanceInfoUserDone(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ResultInstanceTodoDto> list = workflowBenchMapperExt.getInstanceInfoUserDone(model);
        PageHelper.clearPage();
        return list;
    }

    @Override
    public List<ResultInstanceTodoDto> getInstanceInfoUserHis(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ResultInstanceTodoDto> list = workflowBenchMapperExt.getInstanceInfoUserHis(model);
        PageHelper.clearPage();
        return list;
    }

    @Override
    public List<ResultInstanceTodoDto> getStarterDoingInstance(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ResultInstanceTodoDto> list = workflowBenchMapperExt.getStarterDoingInstance(model);
        PageHelper.clearPage();
        return list;
    }

    @Override
    public List<ResultInstanceTodoDto> getInstanceInfoUserEntrust(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ResultInstanceTodoDto> list = workflowBenchMapperExt.getInstanceInfoUserEntrust(model);
        PageHelper.clearPage();
        return list;
    }

    @Override
    public List<ResultInstanceTodoDto> getInstanceInfoUserCopy(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ResultInstanceTodoDto> list = workflowBenchMapperExt.getInstanceInfoUserCopy(model);
        PageHelper.clearPage();
        return list;
    }

    @Override
    public List<ResultInstanceTodoWithParamDto> getInstanceInfoUserTodoWithParam(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ResultInstanceTodoWithParamDto> list = workflowBenchMapperExt.getInstanceInfoUserTodoWithParam(model);
        PageHelper.clearPage();
        return list;
    }

    @Override
    public List<ResultInstanceTodoDto> getTodosByBizTypes(String bizTypes) {
        return workflowBenchMapperExt.getTodosByBizTypes(bizTypes);
    }

    @Override
    public List<ResultInstanceTodoDto> cusmonitorTodo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ResultInstanceTodoDto> list = this.workflowBenchMapperExt.cusmonitorTodo(model);
        PageHelper.clearPage();
        return list;
    }

    @Override
    public List<ResultMeettingDto> myStartList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ResultMeettingDto> list = this.workflowBenchMapperExt.myStartList(model);
        PageHelper.clearPage();
        return list;
    }

    @Override
    public List<ResultVoteListDto> myOpList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ResultVoteListDto> list = this.workflowBenchMapperExt.myOpList(model);
        PageHelper.clearPage();
        return list;
    }

    @Override
    public List<ResultVoteUserDto> selectVoteMemByMeetting(Map map) {
        List<ResultVoteUserDto> list = this.workflowBenchMapperExt.selectVoteMemByMeetting(map);
        return list;
    }

    @Override
    public boolean updateAllSubDataByMeetId(Map map) {
        boolean res = false;
        int num = this.workflowBenchMapperExt.updateAllSubDataByMeetId(map);
        if(num >= 0){
            res = true;
        }
        return res;
    }

    @Override
    public boolean updateMeetStatusUnderThreeDiscuss(Map map) {
        boolean res = false;
        int num = this.workflowBenchMapperExt.updateMeetStatusUnderThreeDiscuss(map);
        if(num >= 0){
            res = true;
        }
        return res;
    }

    @Override
    public int updateFlowDataByParam(Map map) {
        return  this.workflowBenchMapperExt.updateFlowDataByParam(map);
    }

}
