package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.domain.NWfNodeDone;
import cn.com.yusys.yusp.flow.domain.NWfUserDone;
import cn.com.yusys.yusp.flow.dto.NextNodeInfoDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.engine.init.EngineInterface;
import cn.com.yusys.yusp.flow.repository.mapper.NWfUserDoneMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("DscmsDefaultCallBackImpl")
public class DscmsDefaultCallBackImpl extends StudioCallBackInterface{

    private static final Logger log = LoggerFactory.getLogger(DscmsDefaultCallBackImpl.class);

    @Autowired
    private EngineInterface engineInfo;

    @Autowired
    private NWfUserDoneMapper userDoneMapper;

    @Override
    public List<NextNodeInfoDto> getCallbackUser(ResultInstanceDto instanceDto) {
        QueryModel model = new QueryModel();
        model.addCondition("instanceId", instanceDto.getInstanceId());
        model.setSort("end_time");
        List<NWfUserDone> userDone = this.userDoneMapper.selectByModel(model);
        List<String> nextsNodes = this.engineInfo.getAllNextNodesId(instanceDto.getNodeId()); // 获取后续所有节点

        List<NextNodeInfoDto> nextNodeInfo = new ArrayList();
        Map<String, Set> nodesT = new LinkedHashMap<>();
        Iterator it = userDone.iterator();
        while(it.hasNext()) {
            NWfUserDone done = (NWfUserDone)it.next();
            // 获取已办节点详细信息
            if(nextsNodes.indexOf(done.getNodeId()) > -1){
                log.info("DSCMS-默认打回--已办节点【{}】，已办人员【{}】非当前节点的后续节点，不允许打回给后续节点，过滤掉！", done.getNodeId(), done.getUserId());
            }else{
                if (nodesT.containsKey(done.getNodeId())) {
                    ((Set)nodesT.get(done.getNodeId())).add(done.getUserId());
                } else {
                    Set<String> userT = new HashSet();
                    userT.add(done.getUserId());
                    nodesT.put(done.getNodeId(), userT);
                }
            }
        }

        nodesT.remove(instanceDto.getNodeId());
        it = nodesT.keySet().iterator();

        while(it.hasNext()) {
            String key = (String)it.next();
            NextNodeInfoDto nodeUser = new NextNodeInfoDto();
            nodeUser.setNextNodeId(key);
            List<String> nextNodeUserIds = new ArrayList();
            nextNodeUserIds.addAll((Collection)nodesT.get(key));
            nodeUser.setNextNodeUserIds(nextNodeUserIds);
            nextNodeInfo.add(nodeUser);
        }

        return nextNodeInfo;
    }

    @Override
    public int getOrder() {
        return 4;
    }

    @Override
    public String desc() {
        return "默认打回(标准)";
    }

    @Override
    public String key() {
        return "DscmsDefaultCallBackImpl";
    }

    @Override
    public String orgId() {
        return null;
    }
}
