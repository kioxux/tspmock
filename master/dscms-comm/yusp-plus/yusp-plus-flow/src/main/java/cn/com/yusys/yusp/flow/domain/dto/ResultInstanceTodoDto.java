package cn.com.yusys.yusp.flow.domain.dto;


import java.io.Serializable;

public class ResultInstanceTodoDto implements Serializable {
    private String instanceId;
    private String mainInstanceId;
    private String flowName;
    private String flowId;
    private String flowAdmin;
    private String flowStarter;
    private String startTime;
    private String systemId;
    private String orgId;
    private String orgName;
    private String flowState;
    private String bizId;
    private String bizType;
    private String bizUserName;
    private String bizUserId;
    private String flowParam;
    private String lastNodeId;
    private String lastNodeName;
    private String nodeId;
    private String nodeSign;
    private String nodeName;
    private String nodeState;
    private String userId;
    private String userName;
    private String lastUserId;
    private String lastUserName;
    private String signIn;
    private String endTime;
    private String flowStarterName;
    private static final long serialVersionUID = 1L;
    private String nodeType;
    private String handleType;

    private String urgentDate;
    private String urgentType;

    public ResultInstanceTodoDto() {
    }

    public String getInstanceId() {
        return this.instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId == null ? null : instanceId.trim();
    }

    public String getFlowName() {
        return this.flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName == null ? null : flowName.trim();
    }

    public String getFlowId() {
        return this.flowId;
    }

    public void setFlowId(String flowId) {
        this.flowId = flowId == null ? null : flowId.trim();
    }

    public String getFlowAdmin() {
        return this.flowAdmin;
    }

    public void setFlowAdmin(String flowAdmin) {
        this.flowAdmin = flowAdmin == null ? null : flowAdmin.trim();
    }

    public String getFlowStarter() {
        return this.flowStarter;
    }

    public void setFlowStarter(String flowStarter) {
        this.flowStarter = flowStarter == null ? null : flowStarter.trim();
    }

    public String getStartTime() {
        return this.startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime == null ? null : startTime.trim();
    }

    public String getSystemId() {
        return this.systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId == null ? null : systemId.trim();
    }

    public String getOrgId() {
        return this.orgId;
    }

    public String getOrgName() {
        return this.orgName;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName == null ? null : orgName.trim();
    }

    public String getFlowState() {
        return this.flowState;
    }

    public void setFlowState(String flowState) {
        this.flowState = flowState == null ? null : flowState.trim();
    }

    public String getBizId() {
        return this.bizId;
    }

    public void setBizId(String bizId) {
        this.bizId = bizId == null ? null : bizId.trim();
    }

    public String getBizUserName() {
        return this.bizUserName;
    }

    public void setBizUserName(String bizUserName) {
        this.bizUserName = bizUserName == null ? null : bizUserName.trim();
    }

    public String getBizUserId() {
        return this.bizUserId;
    }

    public void setBizUserId(String bizUserId) {
        this.bizUserId = bizUserId == null ? null : bizUserId.trim();
    }

    public String getFlowParam() {
        return this.flowParam;
    }

    public void setFlowParam(String flowParam) {
        this.flowParam = flowParam == null ? null : flowParam.trim();
    }

    public String getNodeId() {
        return this.nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeSign() {
        return this.nodeSign;
    }

    public void setNodeSign(String nodeSign) {
        this.nodeSign = nodeSign;
    }

    public String getNodeName() {
        return this.nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getNodeState() {
        return this.nodeState;
    }

    public void setNodeState(String nodeState) {
        this.nodeState = nodeState;
    }

    public String getLastNodeId() {
        return this.lastNodeId;
    }

    public void setLastNodeId(String lastNodeId) {
        this.lastNodeId = lastNodeId;
    }

    public String getLastNodeName() {
        return this.lastNodeName;
    }

    public void setLastNodeName(String lastNodeName) {
        this.lastNodeName = lastNodeName;
    }

    public String getBizType() {
        return this.bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLastUserId() {
        return this.lastUserId;
    }

    public void setLastUserId(String lastUserId) {
        this.lastUserId = lastUserId;
    }

    public String getLastUserName() {
        return this.lastUserName;
    }

    public void setLastUserName(String lastUserName) {
        this.lastUserName = lastUserName;
    }

    public String getSignIn() {
        return this.signIn;
    }

    public void setSignIn(String signIn) {
        this.signIn = signIn;
    }

    public String getNodeType() {
        return this.nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    public String getHandleType() {
        return this.handleType;
    }

    public void setHandleType(String handleType) {
        this.handleType = handleType;
    }

    public String getEndTime() {
        return this.endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getFlowStarterName() {
        return this.flowStarterName;
    }

    public void setFlowStarterName(String flowStarterName) {
        this.flowStarterName = flowStarterName;
    }

    public String getMainInstanceId() {
        return this.mainInstanceId;
    }

    public void setMainInstanceId(String mainInstanceId) {
        this.mainInstanceId = mainInstanceId;
    }

    public String getUrgentDate() {
        return urgentDate;
    }

    public void setUrgentDate(String urgentDate) {
        this.urgentDate = urgentDate;
    }

    public String getUrgentType() {
        return urgentType;
    }

    public void setUrgentType(String urgentType) {
        this.urgentType = urgentType;
    }

    @Override
    public String toString() {
        return "ResultInstanceTodoDto{" +
                "instanceId='" + instanceId + '\'' +
                ", mainInstanceId='" + mainInstanceId + '\'' +
                ", flowName='" + flowName + '\'' +
                ", flowId='" + flowId + '\'' +
                ", flowAdmin='" + flowAdmin + '\'' +
                ", flowStarter='" + flowStarter + '\'' +
                ", startTime='" + startTime + '\'' +
                ", systemId='" + systemId + '\'' +
                ", orgId='" + orgId + '\'' +
                ", flowState='" + flowState + '\'' +
                ", bizId='" + bizId + '\'' +
                ", bizType='" + bizType + '\'' +
                ", bizUserName='" + bizUserName + '\'' +
                ", bizUserId='" + bizUserId + '\'' +
                ", flowParam='" + flowParam + '\'' +
                ", lastNodeId='" + lastNodeId + '\'' +
                ", lastNodeName='" + lastNodeName + '\'' +
                ", nodeId='" + nodeId + '\'' +
                ", nodeSign='" + nodeSign + '\'' +
                ", nodeName='" + nodeName + '\'' +
                ", nodeState='" + nodeState + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", lastUserId='" + lastUserId + '\'' +
                ", lastUserName='" + lastUserName + '\'' +
                ", signIn='" + signIn + '\'' +
                ", endTime='" + endTime + '\'' +
                ", flowStarterName='" + flowStarterName + '\'' +
                ", nodeType='" + nodeType + '\'' +
                ", handleType='" + handleType + '\'' +
                ", urgentDate='" + urgentDate + '\'' +
                ", urgentType='" + urgentType + '\'' +
                '}';
    }
}
