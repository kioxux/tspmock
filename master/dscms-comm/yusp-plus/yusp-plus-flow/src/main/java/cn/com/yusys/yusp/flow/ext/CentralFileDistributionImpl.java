package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.constant.DscmsCommonConstance;
import cn.com.yusys.yusp.flow.domain.*;
import cn.com.yusys.yusp.flow.domain.vo.ResultUserTodoNum;
import cn.com.yusys.yusp.flow.dto.WFUserDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceTodoDto;
import cn.com.yusys.yusp.flow.other.engine.NodeInfo;
import cn.com.yusys.yusp.flow.other.engine.init.EngineInterface;
import cn.com.yusys.yusp.flow.other.enums.ReDoUserSelect;
import cn.com.yusys.yusp.flow.repository.mapper.*;
import cn.com.yusys.yusp.flow.service.NWfInstanceHisService;
import cn.com.yusys.yusp.flow.service.NWfInstanceService;
import cn.com.yusys.yusp.flow.service.core.WorkflowOrgInterface;
import cn.com.yusys.yusp.flow.util.WorkFlowUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 集中作业分配规则
 * 1、首先判断是否为打回再提交且重办人员为上次办理人，若是则直接返回；若否则走分配规则
 * 2、排除当前提交人
 * 3、前一流程同岗位审批节点人（约定在bizParam1传递），若仍在本岗位且已签到则不走分配规则，直接再分配给此人
 * 4、档案派发规则
 * （1）分配给满足以下所有条件的人员
 * 用户为签到状态。
 * 用户不存在在途审批中且当前日期在请假起始日期和请假结束日期范围内。
 * 用户不存在审批中的提前签退流程。
 * 当前名下待办任务量（信用卡待办任务不纳入统计范围）最少；
 * （2）若不存在满足（1），则分配给满足以下所有条件的人员
 * 用户当前日期未休假（若存在审批中的且休假日期包含当前日期也不能分配）
 * 当前名下待办任务量（信用卡待办任务不纳入统计范围）最少；
 */
@Service("CentralFileDistributionImpl")
public class CentralFileDistributionImpl extends StudioStrategyInterface{

    private static final Logger log = LoggerFactory.getLogger(CentralFileDistributionImpl.class);

    @Autowired
    WorkFlowCustomBenchMapper workFlowCustomBenchMapper;
    @Autowired
    WorkflowBenchMapper workflowBenchMapper;
    @Autowired
    private NWfInstanceMapper instanceMapper;
    @Autowired
    private NWfNodeHisMapper nodeHisMapper;
    @Autowired
    private NWfUserHisMapper userHisMapper;
    @Autowired
    private EngineInterface engineInfo;
    @Autowired
    private NWfUserDoneMapper userDoneService;
    @Autowired
    private WorkflowOrgInterface userService;
    @Autowired
    private NWfInstanceService nWfInstanceService;
    @Autowired
    private NWfInstanceHisService nWfInstanceHisService;

    public List<WFUserDto> selectUser(List<WFUserDto> user, String instanceId, String nodeId, String orgId, String systemId) {
        List<WFUserDto> result = new ArrayList<>();
        if(user.size() > 0){
            // 1、首先判断是否为打回再提交且重办人员为上次办理人，若是则直接返回；若否则走分配规则
            NodeInfo nodeInfo = this.engineInfo.getNodeInfo(nodeId);
            String reDoUserSelect = nodeInfo.getReDoUserSelect();
            if (ReDoUserSelect.HIS_USER.equals(reDoUserSelect)) {
                QueryModel model = new QueryModel();
                model.getCondition().put("instanceId", instanceId);
                model.getCondition().put("nodeId", nodeId);
                model.setSort("start_time desc");
                List<NWfUserDone> usersT = this.userDoneService.selectByModel(model);
                for (NWfUserDone userDone: usersT) {
                    WFUserDto userDto = this.userService.getUserInfo(systemId, userDone.getUserId());
                    if(null != userDto){
                        result.add(userDto);
                        break;
                    }
                }
                if(!CollectionUtils.isEmpty(result)){
                    return result;
                }
            }
            // 2、 排除当前节点提交人，避免自己自动分配给自己
            List<String> userIdsList = new ArrayList<>();
            String currentUserId = null;
            QueryModel model = new QueryModel();
            model.addCondition("instanceId", instanceId);
            List<ResultInstanceTodoDto> currentNTodoNodeList = workflowBenchMapper.getInstanceInfoUserTodo(model); // 此时待办表还未更新数据，nodeId为下一节点id不能作为查询条件，故仅根据实例id查询
            if(!CollectionUtils.isEmpty(currentNTodoNodeList)){
                currentUserId = currentNTodoNodeList.get(0).getUserId();
            }
            for (WFUserDto wFUserDto: user) {
                // 排除掉自己，即避免自己自动分配给自己（针对同岗位复核，或者连续的两个节点处理岗位相同的情况）
                if(StringUtils.isEmpty(currentUserId) || !wFUserDto.getUserId().equals(currentUserId)){
                    userIdsList.add(wFUserDto.getUserId());
                }
            }
            NWfInstance instanceInfo = instanceMapper.selectByPrimaryKey(instanceId);
            // PS: DB003 抵押登记_本地机构放款后抵押模式 114   特殊处理
            if("DB003".equals(instanceInfo.getBizType())){
                log.info("集中作业自动分配规则：DB003抵押登记_本地机构放款后抵押模式流程特殊处理");
                // 查询本流程已办人员列表
                model.getCondition().put("instanceId", instanceId);
                model.setSort("start_time desc");
                List<NWfUserDone> usersT = this.userDoneService.selectByModel(model);
                for (NWfUserDone userDone: usersT) {
                    if(user.indexOf(userDone.getUserId()) > -1){
                        WFUserDto userDto = this.userService.getUserInfo(systemId, userDone.getUserId());
                        if(null != userDto){
                            result.add(userDto);
                            break;
                        }
                    }
                }
                if(!CollectionUtils.isEmpty(result)){
                    return result;
                }
            }


            // 3、前一流程同岗位审批节点人
            // 3.1（约定在bizParam1传递指定人员）
            String lastuser = instanceInfo.getBizParam1();
            if(StringUtils.isEmpty(lastuser)){
                // 3.2（约定在params:{serno: ****}传递上一流程业务流水号）
                lastuser = this.getLastFlowUser(instanceId, nodeId);
            }

            if(StringUtils.isNotEmpty(lastuser)){
                // 判断其仍在本岗位且已签到则不走分配规则，直接再分配给此人
                if(userIdsList.indexOf(lastuser) > -1){
                    // 判断是否签到
                    Integer i = workFlowCustomBenchMapper.checkUserLastCheckStatus(lastuser);
                    if(i != null && i >  0){
                        WFUserDto userDto = this.userService.getUserInfo(systemId, lastuser);
                        if(null != userDto){
                            result.add(userDto);
                        }
                        if(!CollectionUtils.isEmpty(result)){
                            log.info("集中作业自动分配规则：上一审批流程同岗位审批人【{}】在岗且签到，不执行自动分配规则，流程实例：{}，节点ID：{}", lastuser, instanceId, nodeId);
                            return result;
                        }
                    }else{
                        log.info("集中作业自动分配规则：上一审批流程同岗位审批人【{}】在岗但未签到，执行自动分配规则，流程实例：{}，节点ID：{}", lastuser, instanceId, nodeId);
                    }
                }else{
                    log.info("集中作业自动分配规则：上一审批流程同岗位审批人【{}】已不在本岗位，将执行自动分配规则，流程实例：{}，节点ID：{}", lastuser, instanceId, nodeId);
                }
            }

            // 4、 执行分配规则
            String allUserIds = StringUtils.join(userIdsList.toArray(), ",");
            log.info("集中作业自动分配规则：可选分配人员列表{}，流程实例：{}，节点ID：{}",allUserIds, instanceId, nodeId);
            List<ResultUserTodoNum> userFirstFilterList = new ArrayList<>();
            /**
             * 4.1.1、（1）筛选已签到、未请假、未提前签退的人员
             */
            String currDateStr = (new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA)).format(new Date());
            userFirstFilterList = workFlowCustomBenchMapper.getCentralDistributionUsers(allUserIds, currDateStr);

            /**
             * 4.1.2、（2）若不存在满足（1）,筛选当前日期未休假的用户
             */
            if(!(userFirstFilterList.size() > 0)){
                userFirstFilterList = workFlowCustomBenchMapper.getCentralDistributionUsers2(allUserIds, currDateStr);
            }

            /**
             * 4.1.3、筛选出待办最少的人
             */
            List<String> firstUsersList = new ArrayList<>();
            for (ResultUserTodoNum item: userFirstFilterList) {
                firstUsersList.add(item.getUserId());
            }

            if(firstUsersList.size() > 0){ // 前提筛选剩余人数大于0
                List<ResultUserTodoNum> userTodoNumList = workFlowCustomBenchMapper.getUserTodoNum(StringUtils.join(firstUsersList.toArray(), ","), DscmsCommonConstance.JZZY_FLOW_BIZTYPES_NOT_INCLUDE);
                log.info("集中作业自动分配规则：获取可选分配人员列表的待办信息{}，流程实例：{}，节点ID：{}",StringUtils.join(userTodoNumList.toArray(), ","), instanceId, nodeId);
                ResultUserTodoNum resultUserTodoNum = userTodoNumList.stream().min(Comparator.comparing(vo -> vo.getTodoNum())).get();
                log.info("集中作业自动分配规则：获取可选分配人员列表最少的用户号{}，用户名称{}，流程实例：{}，节点ID：{}",resultUserTodoNum.getUserId(),resultUserTodoNum.getUserId(), instanceId, nodeId);
                WFUserDto wFUserDto = new WFUserDto();
                wFUserDto.setUserId(resultUserTodoNum.getUserId());
                wFUserDto.setUserName(resultUserTodoNum.getUserName());
                result.add(wFUserDto);
            }else{
                throw BizException.error(null, "999999", "集中作业自动规则分配规则未筛选到符合条件人员！");
            }
        }else{
            log.error("集中作业自动分配规则：可选分配人员列表为空，流程实例：{}，节点ID：{}", instanceId, nodeId);
        }
        //System.out.println("集中作业自动分配规则：可选分配人员列表"+StringUtils.join(result.toArray()));
        log.info("集中作业自动分配规则：可选分配人员列表{}，流程实例：{}，节点ID：{}",StringUtils.join(result.toArray(), ","), instanceId, nodeId);
        return result;
    }

    /**
     * 获取前一个审批通过的流程，同岗位节点（多个时取最后一个审批通过的）办理人
     * 类似：影像补扫流程节点自动分配规则
     * @return
     */
    private String getLastFlowUser(String instanceId, String nodeId){
        String result = null;
        // 现审批流程实例
        NWfInstance instance = nWfInstanceService.selectByPrimaryKey(instanceId);
        // 参数信息
        Map<String, Object> map = WorkFlowUtil.strToMap(instance.getFlowParam());
        // 原流程关联业务流水号
        String serno = Optional.ofNullable((String) map.get("serno")).orElse("");
        if (StringUtils.isEmpty(serno)){
            if(log.isDebugEnabled()){
                log.info("集中作业自动分配规则：上一审批流程缺少业务流水号无法查找其流程，将执行自动分配规则，流程实例：{}，节点ID：{}", instanceId, nodeId);
            }
        }else{
            // 根据关联业务流水号获取最近一笔历史通过审批流程实例
            QueryModel model = new QueryModel();
            model.addCondition("bizId", serno);
            model.addCondition("flowState", "E");
            model.setSort("endTime desc");
            List<NWfInstanceHis> instanceHisList = nWfInstanceHisService.selectByModel(model);
            if(CollectionUtils.isEmpty(instanceHisList)){
                if(log.isDebugEnabled()){
                    log.info("集中作业自动分配规则：上一审批流程根据业务流水号未查找到其已办结流程，将执行自动分配规则，流程实例：{}，节点ID：{}", instanceId, nodeId);
                }
            }else{
                String oldInstanceId = instanceHisList.get(0).getInstanceId();
                // 当前流程节点信息
                NodeInfo nodeInfo = engineInfo.getNodeInfo(nodeId);
                String nodeUser = nodeInfo.getNodeUser(); // 节点配置的办理人，岗位信息(G.xxx;)
                // 根据原流程实例获取流程审批节点信息
                QueryModel model1 = new QueryModel();
                model1.addCondition("instanceId", oldInstanceId);
                model1.setSort("endTime desc");
                List<NWfNodeHis> nodeHisList = nodeHisMapper.selectByModel(model1);
                // 获取原流程同岗位节点ID
                String oldNodeId = "";
                if (!CollectionUtils.isEmpty(nodeHisList)) {
                    for (NWfNodeHis his : nodeHisList) {
                        NodeInfo hisNodeInfo = engineInfo.getNodeInfo(his.getNodeId());
                        String hisNodeUser = hisNodeInfo.getNodeUser();
                        // 选取同岗位人员返回
                        if(hisNodeUser.contains(nodeUser)){
                            oldNodeId = his.getNodeId();
                            break;
                        }
                    }
                }
                // 根据原流程实例和节点ID获取人员办理信息
                if(StringUtils.isNotEmpty(oldNodeId)){
                    QueryModel model2 = new QueryModel();
                    model2.addCondition("instanceId", oldInstanceId);
                    model2.addCondition("nodeId", oldNodeId);
                    model2.setSort("endTime desc");
                    List<NWfUserHis> userDoneList = userHisMapper.selectByModel(model2);
                    if(!CollectionUtils.isEmpty(userDoneList)){
                        result = userDoneList.get(0).getUserId();
                    }
                }
            }

        }
        return result;
    }

    @Override
    public int getOrder() {
        return 3;
    }

    @Override
    public String desc() {
        return "集中作业分配规则";
    }

    @Override
    public String key() {
        return "CentralFileDistributionImpl";
    }

    @Override
    public String orgId() {
        return null;
    }
}
