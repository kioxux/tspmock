package cn.com.yusys.yusp.flow.listen;

import cn.com.yusys.yusp.flow.ClientCons;
import cn.com.yusys.yusp.flow.domain.NWfException;
import cn.com.yusys.yusp.flow.dto.WFException;
import cn.com.yusys.yusp.flow.repository.mapper.NWfExceptionMapper;
import cn.com.yusys.yusp.flow.service.core.WorkflowPkIdInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 业务处理异常消息监听
 *
 * @author figue
 * @since 4.0
 */
public class BizExceptionListener {
    private final Logger log = LoggerFactory.getLogger(BizExceptionListener.class);

    @Autowired
    private WorkflowPkIdInterface uuidUtil;

    @Autowired
    private NWfExceptionMapper exceptionMapper;

    @RabbitListener(queuesToDeclare = @Queue(ClientCons.queue_exception))// 客户端业务处理发生异常，监听来自队列的异常
    @RabbitHandler
    public void receiveQueue(WFException record) {
    	if(log.isDebugEnabled()){
    		log.debug("监听到来自客户端的异常");
    	}

    	NWfException exception = new NWfException();
    	BeanUtils.copyProperties(record, exception);
    	exception.setPkId("E"+uuidUtil.getUUID());
		exceptionMapper.insertSelective(exception);
    }
}