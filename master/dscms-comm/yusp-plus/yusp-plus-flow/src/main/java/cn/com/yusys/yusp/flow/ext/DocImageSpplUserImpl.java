package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.domain.*;
import cn.com.yusys.yusp.flow.domain.vo.WfAdminSmUserVo;
import cn.com.yusys.yusp.flow.dto.WFUserDto;
import cn.com.yusys.yusp.flow.other.engine.NodeInfo;
import cn.com.yusys.yusp.flow.other.engine.init.EngineInterface;
import cn.com.yusys.yusp.flow.repository.mapper.NWfNodeDoneMapper;
import cn.com.yusys.yusp.flow.repository.mapper.NWfNodeHisMapper;
import cn.com.yusys.yusp.flow.repository.mapper.NWfUserDoneMapper;
import cn.com.yusys.yusp.flow.repository.mapper.NWfUserHisMapper;
import cn.com.yusys.yusp.flow.service.NWfInstanceHisService;
import cn.com.yusys.yusp.flow.service.NWfInstanceService;
import cn.com.yusys.yusp.flow.service.OcaServiceProvider;
import cn.com.yusys.yusp.flow.util.WorkFlowUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 影像补扫流程节点自动分配规则
 * 1、根据原业务流水获取原历史流程实例
 * 2、根据当前流程下一节点配置的岗人员位获取原流程同审批岗位名称
 * 3、根据岗位名称获取原流程最后一岗审批人员信息
 * 4、判断审批人员是否在职：
 * （1）在职且未转岗就分配给原审批人员；（2）不在职或者转岗就随机分配给同机构岗位一人
 *
 * @author jijian_yx
 * @date 2021/9/2 20:36
 **/
@Service("DocImageSpplUserImpl")
public class DocImageSpplUserImpl extends StudioStrategyInterface {

    private static final Logger log = LoggerFactory.getLogger(DocImageSpplUserImpl.class);

    Random r = new Random(1L);

    private static final String FLAG_TODO = "TODO";// 审批中
    private static final String FLAG_HIS = "HIS";// 已办结
    private static final String FLAG_NONE = "NONE";// 无流程

    @Autowired
    private NWfInstanceService nWfInstanceService;
    @Autowired
    private EngineInterface engineInfo;
    @Autowired
    private NWfUserDoneMapper userDoneMapper;
    @Autowired
    private NWfUserHisMapper userHisMapper;
    @Autowired
    private NWfNodeDoneMapper nodeDoneMapper;
    @Autowired
    private NWfNodeHisMapper nodeHisMapper;
    @Autowired
    private OcaServiceProvider ocaServiceProvider;
    @Autowired
    private NWfInstanceHisService nWfInstanceHisService;

    @Override
    public List<WFUserDto> selectUser(List<WFUserDto> users, String instanceId, String nodeId, String orgId, String systemId) {
        List<WFUserDto> result = new ArrayList<>();

        // 现审批流程实例
        NWfInstance instance = nWfInstanceService.selectByPrimaryKey(instanceId);
        // 参数信息
        Map<String, Object> map = WorkFlowUtil.strToMap(instance.getFlowParam());
        // 原业务流程实例
        String bizInstanceId = Optional.ofNullable((String) map.get("bizInstanceId")).orElse("");
        // 原业务流水号
        String serno = Optional.ofNullable((String) map.get("serno")).orElse("");

        // 下一节点岗位信息
        String nodeUser = "";
        // 原流程同岗位节点ID
        String oldNodeId = "";

        // 手工发起未存流程实例,从审批历史中获取
        if (StringUtils.isEmpty(bizInstanceId)) {
            // 根据关联业务流水号获取最近一笔历史通过审批流程实例
            QueryModel model = new QueryModel();
            model.addCondition("bizId", serno);
            model.addCondition("flowState", "E");
            model.setSort("endTime desc");
            List<NWfInstanceHis> instanceHisList = nWfInstanceHisService.selectByModel(model);
            // 原流程实例号
            if (null != instanceHisList && instanceHisList.size() > 0) {
                NWfInstanceHis nWfInstanceHis = instanceHisList.get(0);
                if (null != nWfInstanceHis) {
                    bizInstanceId = nWfInstanceHis.getInstanceId();
                }
            }
        }
        // 如原流程实例和业务流水号获取的流程实例均为空,随机分配
        if (StringUtils.isNotEmpty(bizInstanceId)) {
            // 获取原流程信息
            String flag = "";
            NWfInstanceHis wfInstanceHis = nWfInstanceHisService.selectByPrimaryKey(bizInstanceId);
            NWfInstance wfInstance = nWfInstanceService.selectByPrimaryKey(bizInstanceId);
            if (null != wfInstanceHis) {
                flag = FLAG_HIS;// 原流程已结束
            } else if (null != wfInstance) {
                flag = FLAG_TODO;// 原流程审批中
            } else {
                flag = FLAG_NONE;// 未获取到原流程信息
            }

            if (!FLAG_NONE.equals(flag)) {
                // 当前流程下一节点信息 (影像补扫节点都是单岗位人员审批)
                NodeInfo nodeInfo = engineInfo.getNodeInfo(nodeId);
                // 下一节点岗位信息(G.xxx;)
                nodeUser = nodeInfo.getNodeUser();
                // 根据原流程实例获取流程审批节点信息
                QueryModel model1 = new QueryModel();
                model1.addCondition("instanceId", bizInstanceId);
                model1.setSort("endTime desc");
                if (FLAG_HIS.equals(flag)) {
                    List<NWfNodeHis> nodeHisList = nodeHisMapper.selectByModel(model1);
                    if (null != nodeHisList && nodeHisList.size() > 0) {
                        for (NWfNodeHis his : nodeHisList) {
                            NodeInfo hisNodeInfo = engineInfo.getNodeInfo(his.getNodeId());
                            String hisNodeUser = hisNodeInfo.getNodeUser();
                            // 选取同岗位人员返回
                            if (hisNodeUser.contains(nodeUser)) {
                                oldNodeId = his.getNodeId();
                                break;
                            }
                        }
                    }
                } else {
                    List<NWfNodeDone> nodeDoneList = nodeDoneMapper.selectByModel(model1);
                    if (null != nodeDoneList && nodeDoneList.size() > 0) {
                        for (NWfNodeDone nodeDone : nodeDoneList) {
                            NodeInfo nodeInfo1 = engineInfo.getNodeInfo(nodeDone.getNodeId());
                            String nodeUser1 = nodeInfo1.getNodeUser();
                            // 选取同岗位人员返回
                            if (nodeUser1.contains(nodeUser)) {
                                oldNodeId = nodeDone.getNodeId();
                                break;
                            }
                        }
                    }
                }
                // 根据原流程实例和节点ID获取人员办理信息
                if (StringUtils.isNotEmpty(oldNodeId)) {
                    QueryModel model2 = new QueryModel();
                    model2.addCondition("instanceId", bizInstanceId);
                    model2.addCondition("nodeId", oldNodeId);
                    model2.setSort("endTime desc");
                    if (FLAG_HIS.equals(flag)) {
                        List<NWfUserHis> userHisList = userHisMapper.selectByModel(model2);
                        // 原业务流程同岗位人员
                        if (null != userHisList && userHisList.size() > 0) {
                            tag1:
                            for (NWfUserHis userHis : userHisList) {
                                String oldUserId = userHis.getUserId();
                                // 根据用户编号和用户状态生效(A)获取用户信息
                                WfAdminSmUserVo userInfoForWf = ocaServiceProvider.getUserInfoForWf(systemId, oldUserId);
                                if (null != userInfoForWf) {
                                    // 用户存在且状态为生效
                                    // 获取用户所有岗位
                                    List<String> dutyList = ocaServiceProvider.getDutyCodeByUserId(oldUserId);
                                    boolean dutyFlag = false;// 原岗标识
                                    if (null != dutyList && dutyList.size() > 0) {
                                        tag2:
                                        for (String duty : dutyList) {
                                            if (nodeUser.contains(duty)) {
                                                dutyFlag = true;
                                                break tag2;
                                            }
                                        }
                                    }
                                    if (dutyFlag) {
                                        // 原用户未转岗
                                        String userName = userInfoForWf.getUserName();
                                        WFUserDto userDto = new WFUserDto();
                                        userDto.setUserId(oldUserId);
                                        userDto.setUserName(userName);
                                        result.add(userDto);
                                        log.info("影像补扫自动分配规则 => 现流程实例：" + instanceId + "，原流程实例：" + bizInstanceId + "，岗位：" + nodeUser + "" +
                                                "，原节点：" + oldNodeId + "，原岗位审批人：" + oldUserId + "-" + userName);
                                        break tag1;
                                    }
                                }
                            }
                        }
                    } else {
                        List<NWfUserDone> userDoneList = userDoneMapper.selectByModel(model2);
                        // 原业务流程同岗位人员
                        if (null != userDoneList && userDoneList.size() > 0) {
                            tag1:
                            for (NWfUserDone userDone : userDoneList) {
                                String oldUserId = userDone.getUserId();
                                // 根据用户编号和用户状态生效(A)获取用户信息
                                WfAdminSmUserVo userInfoForWf = ocaServiceProvider.getUserInfoForWf(systemId, oldUserId);
                                if (null != userInfoForWf) {
                                    // 用户存在且状态为生效
                                    // 获取用户所有岗位
                                    List<String> dutyList = ocaServiceProvider.getDutyCodeByUserId(oldUserId);
                                    boolean dutyFlag = false;// 原岗标识
                                    if (null != dutyList && dutyList.size() > 0) {
                                        tag2:
                                        for (String duty : dutyList) {
                                            if (nodeUser.contains(duty)) {
                                                dutyFlag = true;
                                                break tag2;
                                            }
                                        }
                                    }
                                    if (dutyFlag) {
                                        // 原用户未转岗
                                        String userName = userInfoForWf.getUserName();
                                        WFUserDto userDto = new WFUserDto();
                                        userDto.setUserId(oldUserId);
                                        userDto.setUserName(userName);
                                        result.add(userDto);
                                        log.info("影像补扫自动分配规则 => 现流程实例：" + instanceId + "，原流程实例：" + bizInstanceId + "，岗位：" + nodeUser + "" +
                                                "，原节点：" + oldNodeId + "，原岗位审批人：" + oldUserId + "-" + userName);
                                        break tag1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (result.size() < 1) {
            // 随机分配一人
            if (null != users && !users.isEmpty()) {
                WFUserDto user = (WFUserDto) users.get(this.r.nextInt(users.size()));
                result.add(user);
                log.info("影像补扫自动分配规则 => 现流程实例：" + instanceId + "，原流程实例：" + bizInstanceId + "，岗位：" + nodeUser + "" +
                        "，随机分配审批人：" + user.getUserId() + "-" + user.getUserName());
            } else {
                log.info("影像补扫自动分配规则 => 现流程实例：" + instanceId + "，原流程实例：" + bizInstanceId + "，岗位：" + nodeUser + "" +
                        "，随机分配审批人为空");
            }
        }
        return result;
    }

    @Override
    public int getOrder() {
        return 9;
    }

    @Override
    public String desc() {
        return "影像补扫分配规则";
    }

    @Override
    public String key() {
        return "DocImageSpplUserImpl";
    }

    @Override
    public String orgId() {
        return null;
    }
}
