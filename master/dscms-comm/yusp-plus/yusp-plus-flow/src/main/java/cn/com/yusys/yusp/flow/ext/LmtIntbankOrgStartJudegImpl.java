package cn.com.yusys.yusp.flow.ext;


import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.engine.NodeInfo;
import cn.com.yusys.yusp.flow.other.engine.init.EngineInterface;
import cn.com.yusys.yusp.flow.service.OcaServiceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("LmtIntbankOrgStartJudegImpl")
public class LmtIntbankOrgStartJudegImpl extends StudioRouteInterface{

    @Autowired
    private EngineInterface engineInfo;

    @Autowired
    private OcaServiceProvider ocaServiceProvider;

    @Override
    public boolean show(ResultInstanceDto resultInstanceDto, String nextNodeId) {
        //获取发起人id
        String flowStarter = resultInstanceDto.getFlowStarter();
        //根据发起人id查询对应的岗位信息
        String dutyId = ocaServiceProvider.getDutyId(flowStarter);
        // 节点配置属性信息
        NodeInfo nextNode = engineInfo.getNodeInfo(nextNodeId);
        String nodeSign = nextNode.getNodeSign();
        // 根据参数以及后续连接的节点的节点标识组合判断返回true或者false
        if (dutyId.equals("JRB07")&&nodeSign.equals("JRB06")){
            return true;
        }
        if (dutyId.equals("JRB08")&&nodeSign.equals("JRB11")){
            return true;
        }
        if (dutyId.equals("THB02")&&nodeSign.equals("THB01")){
            return true;
        }
        if (dutyId.equals("CPB01")&&nodeSign.equals("CPB02")){
            return true;
        }
        return false;
    }

    @Override
    public int getOrder() {
        return 3;
    }

    @Override
    public String desc() {
        return "同业机构准入发起机构判断";
    }

    @Override
    public String key() {
        return "LmtIntbankOrgStartJudegImpl";
    }

    @Override
    public String orgId() {
        return null;
    }
}
