/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.flow.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.flow.domain.CreditApproveAllotRule;
import cn.com.yusys.yusp.flow.service.CreditApproveAllotRuleService;

/**
 * @项目名称: yusp-plus-flow模块
 * @类名称: CreditApproveAllotRuleResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-08 21:20:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/creditapproveallotrule")
public class CreditApproveAllotRuleResource {
    @Autowired
    private CreditApproveAllotRuleService creditApproveAllotRuleService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditApproveAllotRule>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditApproveAllotRule> list = creditApproveAllotRuleService.selectAll(queryModel);
        return new ResultDto<List<CreditApproveAllotRule>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CreditApproveAllotRule>> index(QueryModel queryModel) {
        List<CreditApproveAllotRule> list = creditApproveAllotRuleService.selectByModel(queryModel);
        return new ResultDto<List<CreditApproveAllotRule>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CreditApproveAllotRule> create(@RequestBody CreditApproveAllotRule creditApproveAllotRule) throws URISyntaxException {
        creditApproveAllotRuleService.insert(creditApproveAllotRule);
        return new ResultDto<CreditApproveAllotRule>(creditApproveAllotRule);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditApproveAllotRule creditApproveAllotRule) throws URISyntaxException {
        int result = creditApproveAllotRuleService.update(creditApproveAllotRule);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String ruleType, String brId, String reviewId) {
        int result = creditApproveAllotRuleService.deleteByPrimaryKey(ruleType, brId, reviewId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:queryListByParams
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryListByParams")
    protected ResultDto<List<CreditApproveAllotRule>> queryList(@RequestBody QueryModel queryModel) {
        List<CreditApproveAllotRule> list = creditApproveAllotRuleService.selectAllByModel(queryModel);
        return new ResultDto<List<CreditApproveAllotRule>>(list);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveList")
    protected ResultDto<CreditApproveAllotRule> saveList(@RequestBody CreditApproveAllotRule creditApproveAllotRule) throws URISyntaxException {
        creditApproveAllotRuleService.saveList(creditApproveAllotRule);
        return new ResultDto<CreditApproveAllotRule>(creditApproveAllotRule);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateList")
    protected ResultDto<Integer> updateList(@RequestBody CreditApproveAllotRule creditApproveAllotRule) throws URISyntaxException {
        int result = creditApproveAllotRuleService.updateList(creditApproveAllotRule);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleteListByParams
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteListByParams")
    protected ResultDto<Integer> deleteListByParams(@RequestBody CreditApproveAllotRule creditApproveAllotRule) {
        int result = creditApproveAllotRuleService.deleteListByParams(creditApproveAllotRule);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @return
     */
    @PostMapping("/show")
    protected ResultDto<CreditApproveAllotRule> show(@RequestBody CreditApproveAllotRule creditApproveAllotRule) {
        CreditApproveAllotRule result = creditApproveAllotRuleService.show(creditApproveAllotRule);
        return new ResultDto<CreditApproveAllotRule>(result);
    }
}
