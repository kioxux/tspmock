package cn.com.yusys.yusp.flow.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.constant.DscmsCommonConstance;
import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.domain.vo.ResultTodoOrderVo;
import cn.com.yusys.yusp.flow.domain.dto.ResultInstanceTodoDto;
import cn.com.yusys.yusp.flow.repository.mapper.WorkFlowCustomBenchMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/6/817:13
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class WorkFlowCustomBenchService {

    @Autowired
    private WorkFlowCustomBenchMapper workFlowCustomBenchMapper;

    public Map<String, Integer> queryFlowCont(QueryModel queryModel) {
        return workFlowCustomBenchMapper.queryFlowCont(queryModel);
    }

    public Integer queryFlowContByBizType(QueryModel queryModel){
        return workFlowCustomBenchMapper.queryFlowContByBizType(queryModel);
    }

    public List<ResultInstanceTodoDto> getInstanceInfoUserCallback(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ResultInstanceTodoDto> list = workFlowCustomBenchMapper.getInstanceInfoUserCallback(model);
        PageHelper.clearPage();
        return list;
    }

    public List<ResultTodoOrderVo>  getOrderInUserTodos(String instanceIds, String flowStarter){
        List<ResultTodoOrderVo> result = workFlowCustomBenchMapper.getProcessUserByInstanceIds(instanceIds, flowStarter);
        if(!CollectionUtils.isEmpty(result)){
            result.stream().forEach(item -> {
                ResultTodoOrderVo order = null;
                // 信用卡业务与非信用卡业务分开统计待办排序
                if(DscmsCommonConstance.CREDIT_CARD_FLOW_BIZTYPES.contains(item.getBizType())){// 信用卡业务
                    order = workFlowCustomBenchMapper.getOrderInUserTodos(item.getUserId(), item.getInstanceId(), null, DscmsCommonConstance.CREDIT_CARD_FLOW_BIZTYPES);
                }else{// 非信用卡业务
                    order = workFlowCustomBenchMapper.getOrderInUserTodos(item.getUserId(), item.getInstanceId(), DscmsCommonConstance.JZZY_FLOW_BIZTYPES_NOT_INCLUDE, null);
                }
                if(Objects.nonNull(order)){
                    item.setTodoOrder(order.getTodoOrder());
                    item.setUrgentType(order.getUrgentType());
                    if(order.getUserId()!=null && order.getUserId().startsWith("T.")){ // 项目池中
                        item.setTodoOrder(null);
                    }
                }
            });
        }
        return result;
    }

    /**
     * 查询客户在途业务
     * @param cusId 客户编号
     * @return
     */
    public List<NWfInstance> queryCusFlow(String cusId) {
        return workFlowCustomBenchMapper.queryCusFlow(cusId);
    }

    /**
     * 查询客户在途业务,不包括退回到发起的
     * @param cusId 客户编号
     * @return
     */
    public List<NWfInstance> queryCusFlowNotIncludeReturnBack(String cusId) {
        return workFlowCustomBenchMapper.queryCusFlowNotIncludeReturnBack(cusId);
    }
    /**
     * 查询该笔业务数
     * @param instanceId 流程实例
     * @return
     */
    public NWfInstance queryDataInfo(String instanceId) {
        return workFlowCustomBenchMapper.queryDataInfo(instanceId);
    }
}
