package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.UserAndDutyReqDto;
import cn.com.yusys.yusp.dto.UserAndDutyRespDto;
import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.domain.vo.WfAdminSmUserVo;
import cn.com.yusys.yusp.flow.dto.WFUserDto;
import cn.com.yusys.yusp.flow.repository.mapper.NWfInstanceMapper;
import cn.com.yusys.yusp.flow.service.OcaServiceProvider;
import cn.com.yusys.yusp.flow.service.core.WorkflowOrgInterface;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.AdminSmUserService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("SameOrgUserSGH01AndSGH21Impl")
public class SameOrgUserSGH01AndSGH21Impl extends StudioUserInterface{
    /**
     * 用户信息获取服务
     */
    @Autowired
    private NWfInstanceMapper instanceMapper;
    @Autowired
    private WorkflowOrgInterface userService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private NWfInstanceMapper nWfInstanceMapper;
    @Autowired
    private AdminSmOrgService adminSmOrgService;

    private static final Log log = LogFactory.getLog(SameOrgUserSGH01AndSGH21Impl.class);
    @Override
    public int getOrder() {
        return 2;
    }

    @Override
    public String desc() {
        return "发起人之外的所有寿光村镇银行客户经理与资产保全经理";
    }

    @Override
    public String key() {
        return "SameOrgUserSGH01AndSGH21Impl";
    }

    @Override
    public String orgId() {
        return null;
    }

    @Override
    public List<String> customUser(String instanceId, String orgId, String systemId) {
        List<String> result = new ArrayList<>();
        NWfInstance nWfInstance = nWfInstanceMapper.selectByPrimaryKey(instanceId);
        //获取流程提交人id
        String flowStarter = nWfInstance.getFlowStarter();

        //获取所有寿光客户经理与资产保全客户经理
        UserAndDutyReqDto userAndDutyReqDto = new UserAndDutyReqDto();
        userAndDutyReqDto.setDutyNo("SGH01,SGH21");
        ResultDto<List<UserAndDutyRespDto>> users = adminSmUserService.getUserAndDuty(userAndDutyReqDto);

        if(users.getData() != null) {
            for (UserAndDutyRespDto userAndDutyRespDto : users.getData()) {
                if(userAndDutyRespDto.getActorNo().equals(flowStarter)){//排除发起人
                    continue;
                }
                result.add(userAndDutyRespDto.getActorNo());
            }
        }else{
            log.error("查询岗位【SGH01,SGH21】下办理人员为空！");
        }

        return result;
    }

}
