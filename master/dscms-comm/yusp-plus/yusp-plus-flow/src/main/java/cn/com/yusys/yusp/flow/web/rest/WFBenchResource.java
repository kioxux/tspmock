package cn.com.yusys.yusp.flow.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.domain.NWfInstanceHis;
import cn.com.yusys.yusp.flow.domain.NWfMetting;
import cn.com.yusys.yusp.flow.domain.NWfMettingParamDto;
import cn.com.yusys.yusp.flow.domain.dto.ResultInstanceTodoDto;
import cn.com.yusys.yusp.flow.domain.dto.ResultMeettingDto;
import cn.com.yusys.yusp.flow.domain.dto.ResultVoteListDto;
import cn.com.yusys.yusp.flow.domain.dto.ResultVoteUserDto;
import cn.com.yusys.yusp.flow.dto.WFTaskpoolDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceTodoWithParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.flow.dto.result.ResultTaskpoolDto;
import cn.com.yusys.yusp.flow.service.NWfInstanceHisService;
import cn.com.yusys.yusp.flow.service.NWfInstanceService;
import cn.com.yusys.yusp.flow.service.NWfMettingService;
import cn.com.yusys.yusp.flow.service.core.WorkflowBenchInterfaceExt;
import cn.com.yusys.yusp.flow.service.core.WorkflowEngineExtInterface;
import cn.com.yusys.yusp.flow.util.WorkFlowUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping({"/api/bench"})
public class WFBenchResource {

    @Autowired
    private WorkflowBenchInterfaceExt workflowBenchInterfaceExt;
    @Autowired
    private WorkflowEngineExtInterface workflowEngineExtService;
    @Autowired
    private NWfInstanceHisService instanceHisService;
    @Autowired
    private NWfInstanceService instanceService;
    @Autowired
    private NWfMettingService nWfMettingService;

    public WFBenchResource() {
    }

    @GetMapping({"/todo"})
    protected ResultDto<List<ResultInstanceTodoDto>> todo(QueryModel queryModel) {
        queryModel.getCondition().put("userId", this.getCurrentUserId(queryModel));
        queryModel.setSort("urgent_type, urgent_date, u.start_time asc");
        return new ResultDto(this.workflowBenchInterfaceExt.getInstanceInfoUserTodo(queryModel));
    }

    @GetMapping({"/done"})
    protected ResultDto<List<ResultInstanceTodoDto>> done(QueryModel queryModel) {
        queryModel.getCondition().put("userId", this.getCurrentUserId(queryModel));
        queryModel.setSort("u.start_time desc");
        return new ResultDto(this.workflowBenchInterfaceExt.getInstanceInfoUserDone(queryModel));
    }

    @GetMapping({"/his"})
    protected ResultDto<List<ResultInstanceTodoDto>> his(QueryModel queryModel) {
        queryModel.getCondition().put("userId", this.getCurrentUserId(queryModel));
        queryModel.setSort("u.start_time desc");
        return new ResultDto(this.workflowBenchInterfaceExt.getInstanceInfoUserHis(queryModel));
    }

    @GetMapping({"/start/doing"})
    public ResultDto<List<ResultInstanceTodoDto>> myStartDoing(QueryModel queryModel) {
        queryModel.getCondition().put("flowStarter", this.getCurrentUserId(queryModel));
        queryModel.setSort("i.start_time desc");
//        List<NWfInstance> insatnceInfos = this.instanceService.selectByModel(queryModel);
        List<ResultInstanceTodoDto> insatnceInfos = this.workflowBenchInterfaceExt.getStarterDoingInstance(queryModel);
        return new ResultDto(insatnceInfos);
    }

    @GetMapping({"/start/his"})
    public ResultDto<List<NWfInstanceHis>> myStartHis(QueryModel queryModel) {
        queryModel.getCondition().put("flowStarter", this.getCurrentUserId(queryModel));
        queryModel.setSort("start_time desc");
        List<NWfInstanceHis> insatnceInfos = this.instanceHisService.selectByModel(queryModel);
        return new ResultDto(insatnceInfos);
    }

    @GetMapping({"/entrust"})
    protected ResultDto<List<ResultInstanceTodoDto>> entrust(QueryModel queryModel) {
        queryModel.getCondition().put("userId", this.getCurrentUserId(queryModel));
        queryModel.setSort("i.start_time desc");
        return new ResultDto(this.workflowBenchInterfaceExt.getInstanceInfoUserEntrust(queryModel));
    }

    @GetMapping({"/copy"})
    protected ResultDto<List<ResultInstanceTodoDto>> copy(QueryModel queryModel) {
        queryModel.getCondition().put("userId", this.getCurrentUserId(queryModel));
        queryModel.setSort("i.start_time desc");
        return new ResultDto(this.workflowBenchInterfaceExt.getInstanceInfoUserCopy(queryModel));
    }

    @GetMapping({"/userTaskPool"})
    protected ResultDto<List<ResultTaskpoolDto>> userTaskPool(String userId, String orgId) {
        return new ResultDto(this.workflowEngineExtService.getUserTaskPool(userId, orgId));
    }

    private String getCurrentUserId(QueryModel queryModel) {
        Object userIdT = queryModel.getCondition().get("currentUserId");
        if (null == userIdT) {
            userIdT = queryModel.getCondition().get("userId");
        }

        String userId = userIdT == null ? null : userIdT.toString();
        userId = WorkFlowUtil.getCurrentUserId(userId);
        return userId;
    }

    @GetMapping({"/todoWithParam"})
    protected ResultDto<List<ResultInstanceTodoWithParamDto>> todoWithParam(QueryModel queryModel) {
        queryModel.getCondition().put("userId", this.getCurrentUserId(queryModel));
        queryModel.setSort("u.start_time desc");
        return new ResultDto(this.workflowBenchInterfaceExt.getInstanceInfoUserTodoWithParam(queryModel));
    }

    @GetMapping({"/getTodosByBizTypes"})
    public ResultDto<List<ResultInstanceTodoDto>> getTodosByBizTypes(@RequestParam("bizTypes") String bizTypes) {
        return new ResultDto(this.workflowBenchInterfaceExt.getTodosByBizTypes(bizTypes));
    }

    /**
     * 扩展流程监控-办理中实例-排除加急实例
     * @param model
     * @return
     */
    @GetMapping({"/instance/cusmonitor/todo"})
    protected ResultDto<List<ResultInstanceTodoDto>> cusmonitorTodo(QueryModel model) {
        model.setSort("i.start_time desc");
        String bizUserName = (String)model.getCondition().get("bizUserName");
        if(bizUserName != null && !"".equals(bizUserName)) {
            model.addCondition("bizUserName", "%"+bizUserName+"%");
        }
        List<ResultInstanceTodoDto> data = this.workflowBenchInterfaceExt.cusmonitorTodo(model);
        return new ResultDto(data);
    }

    /**
     * 贷审会会议列表改造
     * @param model
     * @return
     */
    @GetMapping({"/meetting/myStartList"})
    protected ResultDto<List<ResultMeettingDto>> myStartList(QueryModel model) {
        model.setSort("start_time desc");
        model.getCondition().put("userId", this.getCurrentUserId(model));
        List<ResultMeettingDto> data = this.workflowBenchInterfaceExt.myStartList(model);
        return new ResultDto(data);
    }

    /**
     * 贷审会会议投票列表改造
     * @param model
     * @return
     */
    @GetMapping({"/meetting/myOpList"})
    protected ResultDto<List<ResultVoteListDto>> myOpList(QueryModel model) {
        List<ResultVoteListDto> data = this.workflowBenchInterfaceExt.myOpList(model);
        return new ResultDto(data);
    }

    /**
     * 获取会议参与投票人
     * @param meetId
     * @return
     */
    @GetMapping({"/meetting/selectVoteMemByMeetting"})
    protected ResultDto<List<ResultVoteUserDto>> selectVoteMemByMeetting(@RequestParam("meetId") String meetId) {
        HashMap map = new HashMap();
        map.put("meetId",meetId);
        List<ResultVoteUserDto> data = this.workflowBenchInterfaceExt.selectVoteMemByMeetting(map);
        return new ResultDto(data);
    }

    /**
     * 修改会议下所有投票人的会议主题相关信息
     * @param map
     * @return
     */
    @PostMapping({"/meetting/updateAllSubDataByMeetId"})
    protected ResultDto<Boolean> updateAllSubDataByMeetId(@RequestBody Map map) {
        boolean data = this.workflowBenchInterfaceExt.updateAllSubDataByMeetId(map);
        return new ResultDto(data);
    }

    /**
     * 现发现三人会商时自动开启会议没有同步更新会议状态,现做以下处理,手动更新状态信息
     * @param map
     * @return
     */
    @PostMapping({"/meetting/updatemeetstatusunderthreediscuss"})
    protected ResultDto<Boolean> updateMeetStatusUnderThreeDiscuss(@RequestBody Map map) {
        boolean data = this.workflowBenchInterfaceExt.updateMeetStatusUnderThreeDiscuss(map);
        return new ResultDto(data);
    }


    @PostMapping({"/signTaskPoolBatch"})
    ResultDto<List<ResultMessageDto>> signTaskPool(@Valid @RequestBody List<WFTaskpoolDto> baseDtoList) {
        List<ResultMessageDto> resultList = new ArrayList<>();
        if(!CollectionUtils.isEmpty(baseDtoList)){
            for (WFTaskpoolDto baseDto:baseDtoList) {
                resultList.add(this.workflowEngineExtService.signTaskPool(baseDto.getInstanceId(), baseDto.getNodeId(), baseDto.getUserId(), baseDto.getPoolId()));
            }
        }
        return new ResultDto(resultList);
    }

    /**
     * 贷审会会议投票列表改造
     * @param model
     * @return
     */
    @GetMapping({"/meetting/query"})
    protected ResultDto<List<NWfMetting>> meettingQuery(QueryModel model) {
        List<NWfMetting> data = this.nWfMettingService.selectByModel(model);
        return new ResultDto(data);
    }

    /**
     * 贷审会会议投票列表改造
     * @param map
     * @return
     */
    @PostMapping({"/flow/updateFlowDataByParam"})
    protected ResultDto<Integer> updateFlowDataByParam(@RequestBody Map map) {
        int data = this.workflowBenchInterfaceExt.updateFlowDataByParam(map);
        return new ResultDto(data);
    }

}