package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.repository.mapper.NWfInstanceMapper;
import cn.com.yusys.yusp.flow.util.WorkFlowUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service("UpdateNodeParam")
public class UpdateNodeParam extends StudioNodeScriptInterface{
    @Autowired
    private NWfInstanceMapper instanceMapper;
    @Override
    public void run(ResultInstanceDto resultInstanceDto) {
        String instanceId = resultInstanceDto.getInstanceId();
        Map<String, Object> map = new HashMap<>();
        NWfInstance ins = instanceMapper.selectByPrimaryKey(instanceId);
        if(null != ins && null != ins.getFlowParam()){
            map.putAll(WorkFlowUtil.strToMap(ins.getFlowParam()));
        }
        if(null != resultInstanceDto.getParam() && !resultInstanceDto.getParam().isEmpty()){
            map.putAll(resultInstanceDto.getParam());
        }
        ins.setFlowParam(WorkFlowUtil.mapToStr(map));

        instanceMapper.updateByPrimaryKeySelective(ins);
    }

    @Override
    public int getOrder() {
        return 1;
    }

    @Override
    public String desc() {
        return "更新流程参数";
    }

    @Override
    public String key() {
        return "UpdateNodeParam";
    }

    @Override
    public String orgId() {
        return null;
    }
}
