/**
 * Copyright (C), 2014-2021
 * FileName: WfBenchUserTaskResource
 * Author: Administrator
 * Date: 2021/3/21 9:52
 * Description:
 * History:
 * <author> <time> <version> <desc>
 * Administrator 9:52 1.0.0 新建类
 */

package cn.com.yusys.yusp.flow.controller;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.flow.dto.result.ResultTaskpoolDto;
import cn.com.yusys.yusp.flow.service.WfBenchUserTaskService;
import cn.com.yusys.yusp.flow.util.WorkFlowUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 我的项目池
 * @author zhui
 * @create 2021/3/21
 * @since 1.0.0
 */
@RestController
@RequestMapping("/api/wf/bench")
public class WfBenchUserTaskResource {
    @Autowired
    private WfBenchUserTaskService wfBenchUserTaskService;

    @GetMapping("/userTaskPool")
    public ResultDto<List<ResultTaskpoolDto>> userTaskPool(String userId) {
        List<ResultTaskpoolDto> userTaskPool = wfBenchUserTaskService.getUserTaskPool(userId);
        return ResultDto.success(userTaskPool);
    }

    @GetMapping("/userTaskPoolByModel")
    public ResultDto<List<ResultTaskpoolDto>> userTaskPoolByModel(QueryModel queryModel) {
        String userId = (String) queryModel.getCondition().put("userId", getCurrentUserId(queryModel));
        List<ResultTaskpoolDto> userTaskPoolByModel = wfBenchUserTaskService.getUserTaskPoolByModel(userId, queryModel);
        return ResultDto.success(userTaskPoolByModel);
    }

    private String getCurrentUserId(QueryModel queryModel){
        Object userIdT = queryModel.getCondition().get("currentUserId");
        if(null == userIdT){
            userIdT = queryModel.getCondition().get("userId");
        }
        String userId = userIdT==null?null:userIdT.toString();
        userId = WorkFlowUtil.getCurrentUserId(userId);
        return userId;
    }
}
