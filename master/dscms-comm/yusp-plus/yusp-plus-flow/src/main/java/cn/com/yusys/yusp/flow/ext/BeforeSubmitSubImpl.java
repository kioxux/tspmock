package cn.com.yusys.yusp.flow.ext;

import java.util.List;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.domain.NWfInstanceSub;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.repository.mapper.NWfInstanceMapper;
import cn.com.yusys.yusp.flow.repository.mapper.NWfInstanceSubMapper;
/**
 * 有子流程没有结束时主流程不能结束，适用于异步子流程；
 * @author 11851
 *
 */
//@Service("BeforeSubmitSubImpl")
public class BeforeSubmitSubImpl extends StudioBeforeSubmitInterface {
	@Autowired
	private NWfInstanceSubMapper instanceSubMapper;
	@Autowired
	private NWfInstanceMapper instanceMapper;
	@Override
	public int getOrder() {
		return 1;
	}

	@Override
	public String desc() {
		return "子流程结束";
	}

	@Override
	public String key() {
		return "BeforeSubmitSubImpl";
	}

	@Override
	public String orgId() {
		return null;
	}

	@Override
	public boolean beforeBizOp(ResultInstanceDto instanceInfo) {
		QueryModel model = new QueryModel();
		String mainInstanceId = instanceInfo.getMainInstanceId();
		String instanceId = instanceInfo.getInstanceId();
		model.addCondition("mainInstanceId", mainInstanceId);
		if(!instanceId.equals(mainInstanceId)) {
			model.addCondition("mainInstanceId", instanceId);
		}
		
		List<NWfInstanceSub> subInfos = instanceSubMapper.selectByModel(model);
		if(null==subInfos || subInfos.size() == 0) {
			// 没有子流程记录
			return true;
		}
		
		for(NWfInstanceSub subInfo:subInfos) {
			// 判断所有的子流程是否结束
			String subInstanceId = subInfo.getInstanceId();
			NWfInstance instance = instanceMapper.selectByPrimaryKey(subInstanceId);
			if(null != instance) {
				// 有子流程没有结束
				instanceInfo.setTip("有子流程没有结束，不能提交至结束");
				return false;
			}
		}
		
		return true;
	}

}
