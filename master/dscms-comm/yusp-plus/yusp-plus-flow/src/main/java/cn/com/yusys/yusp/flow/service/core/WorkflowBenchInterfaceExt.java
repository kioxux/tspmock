package cn.com.yusys.yusp.flow.service.core;


import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.domain.NWfMettingParamDto;
import cn.com.yusys.yusp.flow.domain.dto.ResultInstanceTodoDto;
import cn.com.yusys.yusp.flow.domain.dto.ResultMeettingDto;
import cn.com.yusys.yusp.flow.domain.dto.ResultVoteListDto;
import cn.com.yusys.yusp.flow.domain.dto.ResultVoteUserDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceTodoWithParamDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

public interface WorkflowBenchInterfaceExt {
    List<ResultInstanceTodoDto> getInstanceInfoUserTodo(QueryModel var1);

    List<ResultInstanceTodoDto> getInstanceInfoUserDone(QueryModel var1);

    List<ResultInstanceTodoDto> getInstanceInfoUserHis(QueryModel var1);

    List<ResultInstanceTodoDto> getStarterDoingInstance(QueryModel var1);

    List<ResultInstanceTodoDto> getInstanceInfoUserEntrust(QueryModel var1);

    List<ResultInstanceTodoDto> getInstanceInfoUserCopy(QueryModel var1);

    List<ResultInstanceTodoWithParamDto> getInstanceInfoUserTodoWithParam(QueryModel var1);

    List<ResultInstanceTodoDto> getTodosByBizTypes(String var1);

    List<ResultInstanceTodoDto> cusmonitorTodo(QueryModel model);

    List<ResultMeettingDto> myStartList(QueryModel model);

    List<ResultVoteListDto> myOpList(QueryModel model);

    List<ResultVoteUserDto> selectVoteMemByMeetting(Map map);

    boolean updateAllSubDataByMeetId(Map map);

    boolean updateMeetStatusUnderThreeDiscuss(Map map);

    int updateFlowDataByParam(Map map);
}
