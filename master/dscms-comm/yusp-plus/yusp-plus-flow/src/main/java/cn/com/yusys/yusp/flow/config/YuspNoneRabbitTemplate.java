package cn.com.yusys.yusp.flow.config;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.*;
import org.springframework.core.ParameterizedTypeReference;

public class YuspNoneRabbitTemplate implements AmqpTemplate {
    @Override
    public void send(Message message) throws AmqpException {

    }

    @Override
    public void send(String routingKey, Message message) throws AmqpException {

    }

    @Override
    public void send(String exchange, String routingKey, Message message) throws AmqpException {

    }

    @Override
    public void convertAndSend(Object message) throws AmqpException {

    }

    @Override
    public void convertAndSend(String routingKey, Object message) throws AmqpException {

    }

    @Override
    public void convertAndSend(String exchange, String routingKey, Object message) throws AmqpException {

    }

    @Override
    public void convertAndSend(Object message, MessagePostProcessor messagePostProcessor) throws AmqpException {

    }

    @Override
    public void convertAndSend(String routingKey, Object message, MessagePostProcessor messagePostProcessor) throws AmqpException {

    }

    @Override
    public void convertAndSend(String exchange, String routingKey, Object message, MessagePostProcessor messagePostProcessor) throws AmqpException {

    }

    @Override
    public Message receive() throws AmqpException {
        return null;
    }

    @Override
    public Message receive(String queueName) throws AmqpException {
        return null;
    }

    @Override
    public Message receive(long timeoutMillis) throws AmqpException {
        return null;
    }

    @Override
    public Message receive(String queueName, long timeoutMillis) throws AmqpException {
        return null;
    }

    @Override
    public Object receiveAndConvert() throws AmqpException {
        return null;
    }

    @Override
    public Object receiveAndConvert(String queueName) throws AmqpException {
        return null;
    }

    @Override
    public Object receiveAndConvert(long timeoutMillis) throws AmqpException {
        return null;
    }

    @Override
    public Object receiveAndConvert(String queueName, long timeoutMillis) throws AmqpException {
        return null;
    }

    @Override
    public <T> T receiveAndConvert(ParameterizedTypeReference<T> type) throws AmqpException {
        return null;
    }

    @Override
    public <T> T receiveAndConvert(String queueName, ParameterizedTypeReference<T> type) throws AmqpException {
        return null;
    }

    @Override
    public <T> T receiveAndConvert(long timeoutMillis, ParameterizedTypeReference<T> type) throws AmqpException {
        return null;
    }

    @Override
    public <T> T receiveAndConvert(String queueName, long timeoutMillis, ParameterizedTypeReference<T> type) throws AmqpException {
        return null;
    }

    @Override
    public <R, S> boolean receiveAndReply(ReceiveAndReplyCallback<R, S> callback) throws AmqpException {
        return false;
    }

    @Override
    public <R, S> boolean receiveAndReply(String queueName, ReceiveAndReplyCallback<R, S> callback) throws AmqpException {
        return false;
    }

    @Override
    public <R, S> boolean receiveAndReply(ReceiveAndReplyCallback<R, S> callback, String replyExchange, String replyRoutingKey) throws AmqpException {
        return false;
    }

    @Override
    public <R, S> boolean receiveAndReply(String queueName, ReceiveAndReplyCallback<R, S> callback, String replyExchange, String replyRoutingKey) throws AmqpException {
        return false;
    }

    @Override
    public <R, S> boolean receiveAndReply(ReceiveAndReplyCallback<R, S> callback, ReplyToAddressCallback<S> replyToAddressCallback) throws AmqpException {
        return false;
    }

    @Override
    public <R, S> boolean receiveAndReply(String queueName, ReceiveAndReplyCallback<R, S> callback, ReplyToAddressCallback<S> replyToAddressCallback) throws AmqpException {
        return false;
    }

    @Override
    public Message sendAndReceive(Message message) throws AmqpException {
        return null;
    }

    @Override
    public Message sendAndReceive(String routingKey, Message message) throws AmqpException {
        return null;
    }

    @Override
    public Message sendAndReceive(String exchange, String routingKey, Message message) throws AmqpException {
        return null;
    }

    @Override
    public Object convertSendAndReceive(Object message) throws AmqpException {
        return null;
    }

    @Override
    public Object convertSendAndReceive(String routingKey, Object message) throws AmqpException {
        return null;
    }

    @Override
    public Object convertSendAndReceive(String exchange, String routingKey, Object message) throws AmqpException {
        return null;
    }

    @Override
    public Object convertSendAndReceive(Object message, MessagePostProcessor messagePostProcessor) throws AmqpException {
        return null;
    }

    @Override
    public Object convertSendAndReceive(String routingKey, Object message, MessagePostProcessor messagePostProcessor) throws AmqpException {
        return null;
    }

    @Override
    public Object convertSendAndReceive(String exchange, String routingKey, Object message, MessagePostProcessor messagePostProcessor) throws AmqpException {
        return null;
    }

    @Override
    public <T> T convertSendAndReceiveAsType(Object message, ParameterizedTypeReference<T> responseType) throws AmqpException {
        return null;
    }

    @Override
    public <T> T convertSendAndReceiveAsType(String routingKey, Object message, ParameterizedTypeReference<T> responseType) throws AmqpException {
        return null;
    }

    @Override
    public <T> T convertSendAndReceiveAsType(String exchange, String routingKey, Object message, ParameterizedTypeReference<T> responseType) throws AmqpException {
        return null;
    }

    @Override
    public <T> T convertSendAndReceiveAsType(Object message, MessagePostProcessor messagePostProcessor, ParameterizedTypeReference<T> responseType) throws AmqpException {
        return null;
    }

    @Override
    public <T> T convertSendAndReceiveAsType(String routingKey, Object message, MessagePostProcessor messagePostProcessor, ParameterizedTypeReference<T> responseType) throws AmqpException {
        return null;
    }

    @Override
    public <T> T convertSendAndReceiveAsType(String exchange, String routingKey, Object message, MessagePostProcessor messagePostProcessor, ParameterizedTypeReference<T> responseType) throws AmqpException {
        return null;
    }
}
