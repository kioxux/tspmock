/**
 * Copyright (C), 2014-2021
 * FileName: WfBenchUserTaskMapper
 * Author: Administrator
 * Date: 2021/3/22 16:16
 * Description:
 * History:
 * <author> <time> <version> <desc>
 * Administrator 16:16 1.0.0 新建类
 */

package cn.com.yusys.yusp.flow.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.dto.WFUserDto;
import cn.com.yusys.yusp.flow.dto.WFUserExtDto;
import cn.com.yusys.yusp.flow.dto.result.ResultTaskpoolDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 〈〉
 * @author zhui
 * @create 2021/3/22
 * @since 1.0.0
 */

public interface NWfOcaTaskpoolMapper {
    List<ResultTaskpoolDto> selectUserTaskPool(@Param("dutyCodes") List<String> dutyCodes);

    List<ResultTaskpoolDto> selectUserTaskPoolByModel(@Param("condition") Map<String, Object> condition, @Param("dutyCodes") List<String> dutyCodes);

    /**
     * 批量查询用户详细信息
     * @param users
     * @return
     */
    List<WFUserExtDto> getUserDetailBatch(List<WFUserDto> users);
}
