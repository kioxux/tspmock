package cn.com.yusys.yusp.flow.service;

import cn.com.yusys.yusp.flow.domain.vo.LmtAppRelCusInfoVo;
import cn.com.yusys.yusp.flow.service.impl.CmisBizClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * cmis-biz服务对外提供服务接口
 * @author monchi
 * @date 2020-12-19 11:50:00
 */
@FeignClient(name = "${service.feignclient.name.yusp-app-biz:yusp-app-biz}",path = "/api",fallback = CmisBizClientServiceImpl.class )
public interface CmisBizClientService {


    /**
     * @函数名称:selectBySernoAndCusId
     * @函数描述:根据serno和cusId获取企业基本信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/lmtapprelcusinfo/selectBySernoAndCusId")
    public Integer selectBySernoAndCusId(@RequestBody Map map);

}
