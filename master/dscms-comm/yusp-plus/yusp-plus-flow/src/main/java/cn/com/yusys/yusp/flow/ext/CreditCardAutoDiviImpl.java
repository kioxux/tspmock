package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.flow.constant.DscmsCommonConstance;
import cn.com.yusys.yusp.flow.domain.vo.ResultUserTodoNum;
import cn.com.yusys.yusp.flow.dto.WFUserDto;
import cn.com.yusys.yusp.flow.repository.mapper.WorkFlowCustomBenchMapper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 信用卡自动规则
 * （1）分配给满足以下所有条件的人员
 * 用户为签到状态。
 * 用户不存在在途审批中且当前日期在请假起始日期和请假结束日期范围内。
 * 用户不存在审批中的提前签退流程。
 * 当前名下待办任务量（信用卡待办任务不纳入统计范围）最少；
 * （2）若不存在满足（1），则分配给满足以下所有条件的人员
 * 用户当前日期未休假（若存在审批中的且休假日期包含当前日期也不能分配）
 * 当前名下待办任务量（信用卡待办任务不纳入统计范围）最少；
 */
@Service("CreditCardAutoDiviImpl")
public class CreditCardAutoDiviImpl extends StudioStrategyInterface{

    private static final Logger log = LoggerFactory.getLogger(CreditCardAutoDiviImpl.class);

    @Autowired
    WorkFlowCustomBenchMapper workFlowCustomBenchMapper;

    public List<WFUserDto> selectUser(List<WFUserDto> user, String instanceId, String nodeId, String orgId, String systemId) {
        List<WFUserDto> result = new ArrayList<>();
        if(user.size() > 0){
            List<String> userIdsList = new ArrayList<>();

            for (WFUserDto wFUserDto: user) {
                userIdsList.add(wFUserDto.getUserId());
            }
            String allUserIds = StringUtils.join(userIdsList.toArray(), ",");
            log.info("集中作业自动分配规则：可选分配人员列表{}，流程实例：{}，节点ID：{}",allUserIds, instanceId, nodeId);
            List<ResultUserTodoNum> userFirstFilterList = new ArrayList<>();
            /**
             * 1、（1）筛选已签到、未请假、未提前签退的人员
             */
            String currDateStr = (new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA)).format(new Date());
            userFirstFilterList = workFlowCustomBenchMapper.getCentralDistributionUsers(allUserIds, currDateStr);

            /**
             * 2、（2）若不存在满足（1）,筛选当前日期未休假的用户
             */
            if(!(userFirstFilterList.size() > 0)){
                userFirstFilterList = workFlowCustomBenchMapper.getCentralDistributionUsers2(allUserIds, currDateStr);
            }

            /**
             * 3、筛选出待办最少的人
             */
            List<String> firstUsersList = new ArrayList<>();
            for (ResultUserTodoNum item: userFirstFilterList) {
                firstUsersList.add(item.getUserId());
            }

            if(firstUsersList.size() > 0){ // 前提筛选剩余人数大于0
                List<ResultUserTodoNum> userTodoNumList = workFlowCustomBenchMapper.getUserTodoNumCreditCard(StringUtils.join(firstUsersList.toArray(), ","), DscmsCommonConstance.CREDIT_CARD_FLOW_BIZTYPES);
                log.info("集中作业自动分配规则：获取可选分配人员列表的待办信息{}，流程实例：{}，节点ID：{}",StringUtils.join(userTodoNumList.toArray(), ","), instanceId, nodeId);
                ResultUserTodoNum resultUserTodoNum = userTodoNumList.stream().min(Comparator.comparing(vo -> vo.getTodoNum())).get();
                log.info("集中作业自动分配规则：获取可选分配人员列表最少的用户号{}，用户名称{}，流程实例：{}，节点ID：{}",resultUserTodoNum.getUserId(),resultUserTodoNum.getUserId(), instanceId, nodeId);
                WFUserDto wFUserDto = new WFUserDto();
                wFUserDto.setUserId(resultUserTodoNum.getUserId());
                wFUserDto.setUserName(resultUserTodoNum.getUserName());
                result.add(wFUserDto);
            }else{
                throw BizException.error(null, "999999", "集中作业自动规则分配规则未筛选到符合条件人员！");
            }
        }else{
            log.error("集中作业自动分配规则：可选分配人员列表为空，流程实例：{}，节点ID：{}", instanceId, nodeId);
        }
        //System.out.println("集中作业自动分配规则：可选分配人员列表"+StringUtils.join(result.toArray()));
        log.info("集中作业自动分配规则：可选分配人员列表{}，流程实例：{}，节点ID：{}",StringUtils.join(result.toArray(), ","), instanceId, nodeId);
        return result;
    }

    @Override
    public int getOrder() {
        return 4;
    }

    @Override
    public String desc() {
        return "信用卡自动分配规则";
    }

    @Override
    public String key() {
        return "CreditCardAutoDiviImpl";
    }

    @Override
    public String orgId() {
        return null;
    }
}
