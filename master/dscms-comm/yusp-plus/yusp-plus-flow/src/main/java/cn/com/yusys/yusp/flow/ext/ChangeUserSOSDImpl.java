package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.domain.NWfTaskpoolConfig;
import cn.com.yusys.yusp.flow.dto.WFUserDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.engine.NodeInfo;
import cn.com.yusys.yusp.flow.other.engine.init.EngineInterface;
import cn.com.yusys.yusp.flow.other.org.OrgInterface;
import cn.com.yusys.yusp.flow.other.org.WFUser;
import cn.com.yusys.yusp.flow.service.NWfTaskpoolConfigService;
import cn.com.yusys.yusp.flow.util.WorkFlowUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.util.StringUtil;

import java.util.*;

@Service("ChangeUserSOSDImpl")
public class ChangeUserSOSDImpl extends StudioChangeInterface{
    @Autowired
    private OrgInterface orgInterface;
    @Autowired
    private EngineInterface engineInfo;
    @Autowired
    private NWfTaskpoolConfigService  nWfTaskpoolConfigService;

    @Override
    public List<WFUserDto> getChangeUsers(ResultInstanceDto resultInstanceDto) {
        List<WFUserDto> usersT = new ArrayList();
        Map<String, WFUserDto> usersRs = new HashMap();
        String systemId = resultInstanceDto.getSystemId();
        String orgId = resultInstanceDto.getCurrentOrgId();
        String nodeId = resultInstanceDto.getCurrentNodeId();
        String userId = resultInstanceDto.getCurrentUserId();
        NodeInfo nodeInfo = engineInfo.getNodeInfo(nodeId);
        String nodeUser = nodeInfo.getNodeUser();
        List<String> nodeUsers = WorkFlowUtil.splitNodeUser(nodeUser);

        List<WFUser> orgUsers = orgInterface.getUsersByOrg(systemId, orgId);
        usersT.addAll(transUser(orgUsers));

        List<WFUser> dutyUsers = new ArrayList<>();
        for(String key:nodeUsers){
            if(key.startsWith("G")){ // 岗位
                List<WFUser> users = orgInterface.getUsersByDuty(systemId, key.substring(2));
                if(null!=users && !users.isEmpty()){
                    dutyUsers.addAll(users);
                }
            }else if(key.startsWith("T")){ // 项目池
                // 获取池对应的岗位
                QueryModel model = new QueryModel();
                model.addCondition("poolId", key.substring(2));
                List<NWfTaskpoolConfig> dutyList = nWfTaskpoolConfigService.selectByModel(model);
                // 根据岗位获取岗位下人员
                if(!CollectionUtils.isEmpty(dutyList)){
                    for (NWfTaskpoolConfig nWfTaskpoolConfig : dutyList) {
                        String dutyCode = nWfTaskpoolConfig.getCode();
                        List<WFUser> users = orgInterface.getUsersByDuty(systemId, dutyCode);
                        if(null!=users && !users.isEmpty()){
                            dutyUsers.addAll(users);
                        }
                    }
                }
            }
        }
        if(null != dutyUsers && !dutyUsers.isEmpty()) {
            usersT.retainAll(transUser(dutyUsers));
        }
        if(StringUtil.isNotEmpty(userId)){
            for (int i =0;i<usersT.size();i++){
                WFUserDto wFUserDto = usersT.get(i);
                if(userId.equals(wFUserDto.getUserId())){
                    usersT.remove(i);
                }
            }
        }
        return usersT;
    }
    public Collection<WFUserDto> transUser(List<WFUser> users){
        Map<String, WFUserDto> usersRs = new HashMap();
        if (null == users || users.isEmpty()) {
            return usersRs.values();
        }
        for(WFUser user:users){
            WFUserDto userTT = new WFUserDto();
            BeanUtils.copyProperties(user, userTT);
            usersRs.put(user.getUserId(), userTT);
        }
        return usersRs.values();
    }

    @Override
    public int getOrder() {
        return 1;
    }

    @Override
    public String desc() {
        return "同机构同岗位转办";
    }

    @Override
    public String key() {
        return "ChangeUserSOSDImpl";
    }

    @Override
    public String orgId() {
        return null;
    }
}
