package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.other.org.OrgInterface;
import cn.com.yusys.yusp.flow.other.org.WFOrg;
import cn.com.yusys.yusp.flow.other.org.WFUser;
import cn.com.yusys.yusp.flow.util.WorkFlowUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

//@Service("UpOrgUserImpl")
public class UpOrgUserImpl extends StudioUserInterface{
    @Autowired
    private OrgInterface orgService;
    @Override
    public List<String> customUser(String instanceId, String orgId, String systemId) {
        List<String> re = new ArrayList<>();
        QueryModel model = new QueryModel();
        model.addCondition("upOrgId", orgId);
        List<WFOrg> upOrgs = orgService.getOrgs(model);
        if(null == upOrgs || upOrgs.isEmpty()){
            // 父机构为空时查本机构的人
            WFOrg org = new WFOrg();
            org.setOrgId(orgId);
            upOrgs.add(org);
        }
        List<WFUser> users = new ArrayList<>();
        for(WFOrg org:upOrgs){
            String orgid = org.getOrgId();
            String orgName = org.getOrgName();
            List<WFUser> orgUsers = orgService.getUsersByOrg(null, orgid);
            if(null == orgUsers || orgUsers.isEmpty()){
                continue;
            }
            users.addAll(orgUsers);
        }

        for(WFUser user:users){
            re.add(user.getUserId());
        }
        WorkFlowUtil.removeSame(re);
        return re;
    }

    @Override
    public int getOrder() {
        return 4;
    }

    @Override
    public String desc() {
        return "父机构下所有人";
    }

    @Override
    public String key() {
        return "UpOrgUserImpl";
    }

    @Override
    public String orgId() {
        return null;
    }
}
