package cn.com.yusys.yusp.flow.config;

import cn.com.yusys.yusp.flow.listen.BizExceptionListener;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class WorkFlowConfig{
	
	/**
	 * 异常消息落地的监听
	 * @return
	 */
	@Bean
	public BizExceptionListener bizExceptionListener(){
		return new BizExceptionListener();
	}
	
	/*@Bean
	public FilterRegistrationBean userDataAuthFilter() {
		FilterRegistrationBean bean = new FilterRegistrationBean(new UserDataAuthFilter());
		bean.setOrder(Integer.MIN_VALUE+1);
		return bean;
	}*/

//	@Bean
//	@ConditionalOnMissingBean
//	public AmqpTemplate rabbitTemplate(){
//		return new YuspNoneRabbitTemplate();
//	}
}
