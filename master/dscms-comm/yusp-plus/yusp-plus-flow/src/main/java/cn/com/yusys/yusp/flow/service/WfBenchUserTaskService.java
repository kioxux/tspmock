/**
 * Copyright (C), 2014-2021
 * FileName: WfBenchUserTaskService
 * Author: Administrator
 * Date: 2021/3/21 11:09
 * Description:
 * History:
 * <author> <time> <version> <desc>
 * Administrator 11:09 1.0.0 新建类
 */

package cn.com.yusys.yusp.flow.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.dto.result.ResultTaskpoolDto;

import java.util.List;

/**
 * 〈〉
 * @author zhui
 * @create 2021/3/21
 * @since 1.0.0
 */

public interface WfBenchUserTaskService {
    /**
    * 功能描述: 获取用户项目池信息
    * @param userId
    * @return
    */
    public List<ResultTaskpoolDto> getUserTaskPool(String userId);

    public List<ResultTaskpoolDto> getUserTaskPoolByModel(String userId, QueryModel model);
}
