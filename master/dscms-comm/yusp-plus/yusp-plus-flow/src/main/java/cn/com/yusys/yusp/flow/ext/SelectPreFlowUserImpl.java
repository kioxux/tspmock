package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.domain.NWfInstanceHis;
import cn.com.yusys.yusp.flow.domain.NWfNodeHis;
import cn.com.yusys.yusp.flow.domain.NWfUserHis;
import cn.com.yusys.yusp.flow.domain.dto.ResultInstanceTodoDto;
import cn.com.yusys.yusp.flow.dto.NextNodeInfoDto;
import cn.com.yusys.yusp.flow.dto.WFNextNodeDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.engine.NodeInfo;
import cn.com.yusys.yusp.flow.other.engine.RouteInfo;
import cn.com.yusys.yusp.flow.other.engine.init.EngineInterface;
import cn.com.yusys.yusp.flow.other.groovy.OelFactory;
import cn.com.yusys.yusp.flow.other.org.OrgInterface;
import cn.com.yusys.yusp.flow.other.org.WFUser;
import cn.com.yusys.yusp.flow.repository.mapper.NWfInstanceMapper;
import cn.com.yusys.yusp.flow.repository.mapper.NWfNodeHisMapper;
import cn.com.yusys.yusp.flow.repository.mapper.NWfUserHisMapper;
import cn.com.yusys.yusp.flow.repository.mapper.WorkflowBenchMapperExt;
import cn.com.yusys.yusp.flow.service.NWfInstanceHisService;
import cn.com.yusys.yusp.flow.service.NWfInstanceService;
import cn.com.yusys.yusp.flow.service.WorkflowCoreService;
import cn.com.yusys.yusp.flow.service.core.WorkflowEngineInterface;
import cn.com.yusys.yusp.flow.service.core.WorkflowRouteInterface;
import cn.com.yusys.yusp.flow.util.ApplicationContextUtil;
import cn.com.yusys.yusp.flow.util.WorkFlowUtil;
import cn.com.yusys.yusp.service.AdminSmUserService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 选择前一流程的同岗位审批人
 * PS: 流程图-节点人员列表需要配置  审批岗位 + 前一流程的同岗位审批人（自定义）  交集
 *
 */
@Service("SelectPreFlowUserImpl")
public class SelectPreFlowUserImpl extends StudioUserInterface{

    private static final Logger log = LoggerFactory.getLogger(SelectPreFlowUserImpl.class);

    @Autowired
    private NWfInstanceMapper instanceMapper;
    @Autowired
    private NWfNodeHisMapper nodeHisMapper;
    @Autowired
    private NWfUserHisMapper userHisMapper;
    @Autowired
    private EngineInterface engineInfo;
    @Autowired
    private WorkflowCoreService workflowCoreService;
    @Autowired
    private OrgInterface orgService;
    @Autowired
    private WorkflowRouteInterface routeService;
    @Autowired
    AdminSmUserService adminSmUserService;
    @Autowired
    private NWfInstanceService nWfInstanceService;
    @Autowired
    private NWfInstanceHisService nWfInstanceHisService;
    @Autowired
    private WorkflowBenchMapperExt workflowBenchMapperExt;

    @Override
    public List<String> customUser(String instanceId, String orgId, String systemId) {
        List<String> result = new ArrayList<>();
        NWfInstance nWfInstance = instanceMapper.selectByPrimaryKey(instanceId);
        //1、获取所有下一节点号

        String flowParam = nWfInstance.getFlowParam();
        Map<String, Object> map = WorkFlowUtil.strToMap(flowParam);
        // 获取流程数据
        ResultInstanceTodoDto resultInstanceTodoDto = workflowBenchMapperExt.getInstanceInfoByInstanceId(instanceId);
        String curNodeId = "";
        if(resultInstanceTodoDto != null && StringUtils.isNotEmpty(resultInstanceTodoDto.getNodeId())){
            curNodeId = resultInstanceTodoDto.getNodeId();
        }
        ResultInstanceDto resultInstanceDto = this.workflowCoreService.getInstanceInfo(instanceId, curNodeId);
        resultInstanceDto.setParam(map);
        List<NextNodeInfoDto> list = this.getWFNextNodeInfos(resultInstanceDto, curNodeId);
        //2、下一节点处理人岗位
        String dutyId = "";
        String nodeId = list.get(0).getNextNodeId(); // 认为下一节点就一个
        // 2.1、从缓存中获取下一节点配置信息
        NodeInfo node = engineInfo.getNodeInfo(nodeId);
        // 2.2、下一节点配置人员（岗位+自定义类）
        List<String> nodeUsers = WorkFlowUtil.splitNodeUser(node.getNodeUser());
        if(nodeUsers.contains("E.SelectPreFlowUserImpl")){
            for(String key:nodeUsers) {
                if (key.startsWith("G")) {
                    dutyId = key.substring(2);
                    break;
                }
            }
        }
        // 3、下一节点所有处理人
        List<WFUser> users = orgService.getUsersByDuty(systemId, dutyId);

        // 4、获取前一流程的同岗位审批人
        // 4.1、（约定在bizParam1传递指定人员）
        String lastuser = nWfInstance.getBizParam1();
        if(StringUtils.isEmpty(lastuser)){
            // 2、（约定在params:{serno: ****}传递上一流程业务流水号）
            lastuser = this.getLastFlowUser(instanceId, nodeId);
        }
        if(StringUtils.isEmpty(lastuser)){
            log.info("前一流程的同岗位审批人: 无前一流程或前一流程同岗位审批人未获取到，将返回岗位所有人供其选择");
            result.addAll(users.stream().map(WFUser::getUserId).collect(Collectors.toList()));
        }else{
            //
            for(WFUser user : users){
                if(lastuser.equals(user.getUserId()) ){
                    result.add(lastuser);
                    log.info("前一流程的同岗位同机构审批人: 前一流程同岗位同机构审批人为{}，将仅返回此人供其选择", lastuser);
                    return result;
                }
            }
            if(CollectionUtils.isEmpty(result)){
                log.info("前一流程的同岗位审批人: 无前一流程或前一流程同岗位审批人岗位调整，将返回岗位所有人供其选择");
                result.addAll(users.stream().map(WFUser::getUserId).collect(Collectors.toList()));
            }

        }
        return result;
    }

    /**
     * 获取前一个审批通过的流程，同岗位节点（多个时取最后一个审批通过的）办理人
     * 类似：影像补扫流程节点自动分配规则
     * @return
     */
    private String getLastFlowUser(String instanceId, String nodeId){
        String result = null;
        // 现审批流程实例
        NWfInstance instance = nWfInstanceService.selectByPrimaryKey(instanceId);
        // 参数信息
        Map<String, Object> map = WorkFlowUtil.strToMap(instance.getFlowParam());
        // 原流程关联业务流水号
        String serno = Optional.ofNullable((String) map.get("lastSerno")).orElse("");
        if (StringUtils.isEmpty(serno)){
            if(log.isDebugEnabled()){
                log.info("前一流程的同岗位审批人：上一审批流程缺少业务流水号无法查找其流程，将执行自动分配规则，流程实例：{}，节点ID：{}", instanceId, nodeId);
            }
        }else{
            // 根据关联业务流水号获取最近一笔历史通过审批流程实例
            QueryModel model = new QueryModel();
            model.addCondition("bizId", serno);
            model.addCondition("flowState", "E");
            model.setSort("endTime desc");
            List<NWfInstanceHis> instanceHisList = nWfInstanceHisService.selectByModel(model);
            if(CollectionUtils.isEmpty(instanceHisList)){
                if(log.isDebugEnabled()){
                    log.info("前一流程的同岗位审批人：上一审批流程根据业务流水号未查找到其已办结流程，将执行自动分配规则，流程实例：{}，节点ID：{}", instanceId, nodeId);
                }
            }else{
                String oldInstanceId = instanceHisList.get(0).getInstanceId();
                // 当前流程节点信息
                NodeInfo nodeInfo = engineInfo.getNodeInfo(nodeId);
                String nodeUser = nodeInfo.getNodeUser(); // 节点配置的办理人，岗位信息(G.xxx;)
                // 根据原流程实例获取流程审批节点信息
                QueryModel model1 = new QueryModel();
                model1.addCondition("instanceId", oldInstanceId);
                model1.setSort("endTime desc");
                List<NWfNodeHis> nodeHisList = nodeHisMapper.selectByModel(model1);
                // 获取原流程同岗位节点ID
                String oldNodeId = "";
                if (!CollectionUtils.isEmpty(nodeHisList)) {
                    for (NWfNodeHis his : nodeHisList) {
                        if(nodeId.equals(his.getNodeId())){
                            NodeInfo hisNodeInfo = engineInfo.getNodeInfo(his.getNodeId());
                            String hisNodeUser = hisNodeInfo.getNodeUser();
                            // 选取同岗位人员返回
                            if(hisNodeUser.contains(nodeUser)){
                                oldNodeId = his.getNodeId();
                                break;
                            }
                        }
                    }
                }
                // 根据原流程实例和节点ID获取人员办理信息
                if(StringUtils.isNotEmpty(oldNodeId)){
                    QueryModel model2 = new QueryModel();
                    model2.addCondition("instanceId", oldInstanceId);
                    model2.addCondition("nodeId", oldNodeId);
                    model2.setSort("endTime desc");
                    List<NWfUserHis> userDoneList = userHisMapper.selectByModel(model2);
                    if(!CollectionUtils.isEmpty(userDoneList)){
                        result = userDoneList.get(0).getUserId();
                    }
                }
            }

        }
        return result;
    }

    @Override
    public int getOrder() {
        return 6;
    }

    @Override
    public String desc() {
        return "前一流程的同岗位审批人";
    }

    @Override
    public String key() {
        return "SelectPreFlowUserImpl";
    }

    @Override
    public String orgId() {
        return null;
    }

    // 获取流程下一节点的方法，带路由参数的
    private List<NextNodeInfoDto> getWFNextNodeInfos(ResultInstanceDto instanceInfo, String nodeId) {
        NodeInfo nodeInfo = this.engineInfo.getNodeInfo(nodeId);
        if (null == nodeInfo) {
            log.error("获取后续节点原始信息异常 :获取节点 信息为空》》" + nodeId);
        }

        instanceInfo.setNodeType(nodeInfo.getNodeType());
        String nodeScript = nodeInfo.getNodeScript();
        if (!WorkFlowUtil.isNullOrEmpty(nodeScript) && !nodeScript.equals("0")) {
            if (nodeScript.equals("NodeScriptTxtImpl")) {
                try {
                    OelFactory.getInstance().getJavaScriptInstance(nodeInfo.getNodeScriptTxt()).execute(instanceInfo);
                } catch (Exception var11) {
                    log.debug("执行节点脚本异常 :" + instanceInfo);
                    var11.printStackTrace();
                }
            } else {
                StudioNodeScriptInterface nodeScriptBean = (StudioNodeScriptInterface) ApplicationContextUtil.getBean(nodeScript);
                nodeScriptBean.run(instanceInfo);
            }
        }

        List<NodeInfo> nodeInfos = new ArrayList();
        Iterator var6 = nodeInfo.getNextNodes().iterator();

        NodeInfo nextNodeInfo;
        while(var6.hasNext()) {
            String nodeIdT = (String)var6.next();
            nextNodeInfo = this.engineInfo.getNodeInfo(nodeIdT);
            nodeInfos.add(nextNodeInfo);
        }

        if (log.isDebugEnabled()) {
            log.debug("获取节点条数:" + nodeInfos.size());
        }

        List<RouteInfo> routes = nodeInfo.getRouteInfos();
        Iterator var15;
        if (null != routes) {
            var15 = routes.iterator();

            while(var15.hasNext()) {
                RouteInfo route = (RouteInfo)var15.next();
                if (!WorkFlowUtil.isNullOrEmpty(route.getIsContinueBeanId())) {
                    String nextNodeId = route.getNextNodeId();
                    if (!this.runRoute(instanceInfo, nextNodeId, route.getIsContinueBeanId(), route.getRouteScriptTxt())) {
                        NodeInfo nodeInfoT = this.engineInfo.getNodeInfo(nextNodeId);
                        nodeInfos.remove(nodeInfoT);
                        if (log.isDebugEnabled()) {
                            log.debug("节点路由返回false,去除此节点:" + nextNodeId);
                        }
                    }
                }
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("过滤剩余条数:" + nodeInfos.size());
        }

        List<NextNodeInfoDto> data = new ArrayList();
        var15 = nodeInfos.iterator();

        while(var15.hasNext()) {
            nextNodeInfo = (NodeInfo)var15.next();
            NextNodeInfoDto nextNodeInfoDto = new NextNodeInfoDto();
            nextNodeInfoDto.setNextNodeId(nextNodeInfo.getNodeId());
            List<String> userIds = WorkFlowUtil.splitNodeUser(nextNodeInfo.getNodeUser());
            if ("2".equals(nextNodeInfo.getOpUsersType())) {
                userIds = new ArrayList();
                ((List)userIds).add("system_user");
            }
            nextNodeInfoDto.setNextNodeUserIds((List)userIds);
            data.add(nextNodeInfoDto);
        }

        return data;
    }

    private boolean runRoute(ResultInstanceDto instanceInfo, String nextNodeId, String isContinueBeanId, String scriptText) {
        return this.routeService.run(instanceInfo, nextNodeId, isContinueBeanId, scriptText);
    }
}
