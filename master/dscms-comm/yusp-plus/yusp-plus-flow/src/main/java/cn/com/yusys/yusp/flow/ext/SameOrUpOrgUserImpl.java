package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.dto.NextNodeInfoDto;
import cn.com.yusys.yusp.flow.dto.WFUserDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.engine.NodeInfo;
import cn.com.yusys.yusp.flow.other.engine.RouteInfo;
import cn.com.yusys.yusp.flow.other.engine.init.EngineInterface;
import cn.com.yusys.yusp.flow.other.groovy.OelFactory;
import cn.com.yusys.yusp.flow.other.org.OrgInterface;
import cn.com.yusys.yusp.flow.repository.mapper.NWfInstanceMapper;
import cn.com.yusys.yusp.flow.service.WorkflowCoreService;
import cn.com.yusys.yusp.flow.service.core.WorkflowOrgInterface;
import cn.com.yusys.yusp.flow.service.core.WorkflowRouteInterface;
import cn.com.yusys.yusp.flow.util.ApplicationContextUtil;
import cn.com.yusys.yusp.flow.util.WorkFlowUtil;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service("SameOrUpOrgUserImpl")
public class SameOrUpOrgUserImpl extends StudioUserInterface{
	/**
	 * 用户信息获取服务
	 */
	@Autowired
	private WorkflowOrgInterface userService;
	@Autowired
	private NWfInstanceMapper instanceMapper;
	@Autowired
	private WorkflowCoreService workflowCoreService;
	@Autowired
	private EngineInterface engineInfo;
	@Autowired
	private WorkflowRouteInterface routeService;
	@Autowired
	private AdminSmOrgService adminSmOrgService;

	private static final Log log = LogFactory.getLog(SameOrUpOrgUserImpl.class);
	@Override
	public int getOrder() {
		return 1;
	}

	@Override
	public String desc() {
		return "同机构或上机构人员(提交机构)";
	}

	@Override
	public String key() {
		return "SameOrUpOrgUserImpl";
	}

	@Override
	public String orgId() {
		return null;
	}

	@Override
	public List<String> customUser(String instanceId, String orgId, String systemId) {

		List<String> result = new ArrayList<>();
		NWfInstance nWfInstance = instanceMapper.selectByPrimaryKey(instanceId);
		//1、获取所有下一节点号

		String flowParam = nWfInstance.getFlowParam();
		Map<String, Object> map = WorkFlowUtil.strToMap(flowParam);
		// 在用户需要选取流程节点中的用户的时候，会将 nextNodeId 与 submitNodeId 赋值,所有优先去拿nextNodeId 如果拿不到则拿submitNodeId
		String curNodeId  = (String) map.get("nextSubmitNodeId");

		ResultInstanceDto resultInstanceDto = this.workflowCoreService.getInstanceInfo(instanceId, curNodeId);
		resultInstanceDto.setParam(map);
		List<NextNodeInfoDto> list = this.getWFNextNodeInfos(resultInstanceDto, curNodeId);
		//2、下一节点处理人岗位
		String dutyId = "";
		String nodeId = list.get(0).getNextNodeId(); // 认为下一节点就一个
		// 2.1、从缓存中获取下一节点配置信息
		NodeInfo node = engineInfo.getNodeInfo(nodeId);
		// 2.2、下一节点配置人员（岗位+自定义类）
		List<String> nodeUsers = WorkFlowUtil.splitNodeUser(node.getNodeUser());
		if(nodeUsers.contains("E.SameOrUpOrgUserImpl")){
			for(String key:nodeUsers) {
				if (key.startsWith("G")) {
					dutyId = key.substring(2);
					break;
				}
			}
		}

		// 获取
		List<WFUserDto> userOrgdatas = userService.getUsersByOrgId(systemId, orgId);
		List<WFUserDto> userDutyDtos = userService.getUsersByDutyId(systemId, dutyId);
		if(null == userOrgdatas) {
			log.info("查询同机构【"+orgId+"】下办理人员为空！");
		}
		if(null == userDutyDtos){
			log.error("查询同岗位【"+dutyId+"】下办理人员为空！");
		}
		if(userOrgdatas != null && userDutyDtos != null){
			log.info("同机构【"+orgId+"】下，同岗位【"+dutyId+"】办理人员匹配");
			for (WFUserDto userOrgdata: userOrgdatas ) {
				for (WFUserDto userDutyDto: userDutyDtos) {
					if(userOrgdata.getUserId().equals(userDutyDto.getUserId())){
						result.add(userOrgdata.getUserId());
					}
				}
			}
			if(result.size() == 0){
				log.info("同机构【"+orgId+"】下，同岗位【"+dutyId+"】办理人员匹配不上，查询上一机构人员");
				AdminSmOrgDto adminSmOrgDto = adminSmOrgService.getByOrgCode(orgId).getData();
				if (adminSmOrgDto != null && StringUtils.isNotEmpty(adminSmOrgDto.getUpOrgId())){
					userOrgdatas = userService.getUsersByOrgId(systemId, adminSmOrgDto.getUpOrgId());
					if(null == userOrgdatas) {
						log.error("查询上一机构【" + adminSmOrgDto.getUpOrgId() + "】下办理人员为空！");
					}
					if(userOrgdatas != null && userDutyDtos != null) {
						log.info("上一机构【"+adminSmOrgDto.getUpOrgId()+"】下，同岗位【"+dutyId+"】办理人员匹配");
						for (WFUserDto userOrgdata : userOrgdatas) {
							for (WFUserDto userDutyDto : userDutyDtos) {
								if (userOrgdata.getUserId().equals(userDutyDto.getUserId())) {
									result.add(userOrgdata.getUserId());
								}
							}
						}
					}
				}
			}
		}
		return result;
	}

	// 获取流程下一节点的方法，带路由参数的
	private List<NextNodeInfoDto> getWFNextNodeInfos(ResultInstanceDto instanceInfo, String nodeId) {
		NodeInfo nodeInfo = this.engineInfo.getNodeInfo(nodeId);
		if (null == nodeInfo) {
			log.error("获取后续节点原始信息异常 :获取节点 信息为空》》" + nodeId);
		}

		instanceInfo.setNodeType(nodeInfo.getNodeType());
		String nodeScript = nodeInfo.getNodeScript();
		if (!WorkFlowUtil.isNullOrEmpty(nodeScript) && !nodeScript.equals("0")) {
			if (nodeScript.equals("NodeScriptTxtImpl")) {
				try {
					OelFactory.getInstance().getJavaScriptInstance(nodeInfo.getNodeScriptTxt()).execute(instanceInfo);
				} catch (Exception var11) {
					log.debug("执行节点脚本异常 :" + instanceInfo);
					var11.printStackTrace();
				}
			} else {
				StudioNodeScriptInterface nodeScriptBean = (StudioNodeScriptInterface) ApplicationContextUtil.getBean(nodeScript);
				nodeScriptBean.run(instanceInfo);
			}
		}

		List<NodeInfo> nodeInfos = new ArrayList();
		Iterator var6 = nodeInfo.getNextNodes().iterator();

		NodeInfo nextNodeInfo;
		while(var6.hasNext()) {
			String nodeIdT = (String)var6.next();
			nextNodeInfo = this.engineInfo.getNodeInfo(nodeIdT);
			nodeInfos.add(nextNodeInfo);
		}

		if (log.isDebugEnabled()) {
			log.debug("获取节点条数:" + nodeInfos.size());
		}

		List<RouteInfo> routes = nodeInfo.getRouteInfos();
		Iterator var15;
		if (null != routes) {
			var15 = routes.iterator();

			while(var15.hasNext()) {
				RouteInfo route = (RouteInfo)var15.next();
				if (!WorkFlowUtil.isNullOrEmpty(route.getIsContinueBeanId())) {
					String nextNodeId = route.getNextNodeId();
					if (!this.runRoute(instanceInfo, nextNodeId, route.getIsContinueBeanId(), route.getRouteScriptTxt())) {
						NodeInfo nodeInfoT = this.engineInfo.getNodeInfo(nextNodeId);
						nodeInfos.remove(nodeInfoT);
						if (log.isDebugEnabled()) {
							log.debug("节点路由返回false,去除此节点:" + nextNodeId);
						}
					}
				}
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("过滤剩余条数:" + nodeInfos.size());
		}

		List<NextNodeInfoDto> data = new ArrayList();
		var15 = nodeInfos.iterator();

		while(var15.hasNext()) {
			nextNodeInfo = (NodeInfo)var15.next();
			NextNodeInfoDto nextNodeInfoDto = new NextNodeInfoDto();
			nextNodeInfoDto.setNextNodeId(nextNodeInfo.getNodeId());
			List<String> userIds = WorkFlowUtil.splitNodeUser(nextNodeInfo.getNodeUser());
			if ("2".equals(nextNodeInfo.getOpUsersType())) {
				userIds = new ArrayList();
				((List)userIds).add("system_user");
			}
			nextNodeInfoDto.setNextNodeUserIds((List)userIds);
			data.add(nextNodeInfoDto);
		}

		return data;
	}

	private boolean runRoute(ResultInstanceDto instanceInfo, String nextNodeId, String isContinueBeanId, String scriptText) {
		return this.routeService.run(instanceInfo, nextNodeId, isContinueBeanId, scriptText);
	}
}
