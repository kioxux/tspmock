/**
 * Copyright (C), 2014-2021
 * FileName: OcaServiceProvider
 * Author: Administrator
 * Date: 2021/3/21 17:42
 * Description:
 * History:
 * <author> <time> <version> <desc>
 * Administrator 17:42 1.0.0 新建类
 */

package cn.com.yusys.yusp.flow.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.domain.vo.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 工作流调用oca feign接口
 * @author zhui
 * @create 2021/3/21
 * @since 1.0.0
 */

@FeignClient(name = "${service.feignclient.name.yusp-app-oca:yusp-app-oca}", path = "/api/wf",fallback = OcaServiceProviderHystrix.class)
public interface OcaServiceProvider {
    @RequestMapping(method = RequestMethod.GET,value="/getusers")
    public WfPageVo<WfAdminSmUserVo> getUsersForWf(QueryModel param);

    @RequestMapping(method = RequestMethod.GET,value="/getdepts")
    public WfPageVo<WfAdminSmDptVo> getDeptsForWf(QueryModel param);

    @RequestMapping(method = RequestMethod.GET,value="/getdutys")
    public WfPageVo<WfAdminSmDutyVo> getDutysForWf(QueryModel param);

    @RequestMapping(method = RequestMethod.GET,value="/getorgs")
    public WfPageVo<WfAdminSmOrgVo> getOrgsForWf(QueryModel param);

    @RequestMapping(method = RequestMethod.GET,value="/getroles")
    public WfPageVo<WfAdminSmRoleVo> getRolesForWf(QueryModel param);

    @RequestMapping(method = RequestMethod.GET,value="/getorgusers")
    public List<WfAdminSmUserVo> getUsersByOrgForWf(@RequestParam("systemId") String systemId, @RequestParam("orgId") String orgId);

    @RequestMapping(method = RequestMethod.GET,value="/getorganddutyusers")
    public List<WfAdminSmUserVo> getUsersByOrgAndDutyForWf(@RequestParam("orgId") String orgId, @RequestParam("userId") String userId);

    @RequestMapping(method = RequestMethod.GET,value="/getdeptusers")
    public List<WfAdminSmUserVo> getUsersByDeptForWf(@RequestParam("systemId") String systemId, @RequestParam("deptId") String deptId);

    @RequestMapping(method = RequestMethod.GET,value="/getdutyusers")
    public List<WfAdminSmUserVo> getUsersByDutyForWf(@RequestParam("systemId") String systemId, @RequestParam("dutyId") String dutyId);

    @RequestMapping(method = RequestMethod.GET,value="/getroleusers")
    public List<WfAdminSmUserVo> getUsersByRoleForWf(@RequestParam("systemId") String systemId, @RequestParam("roleId") String roleId);

    @RequestMapping(method = RequestMethod.GET,value="/getuserinfo")
    public WfAdminSmUserVo getUserInfoForWf(@RequestParam("systemId") String systemId, @RequestParam("userId") String userId);

    @RequestMapping(method = RequestMethod.GET,value="/getlowerorg")
    public List<String> getLowerOrgId(@RequestParam("orgCode") String orgCode);

    @RequestMapping(method = RequestMethod.GET,value="/getuserdutys")
    public List<String> getDutyCodeByUserId(@RequestParam("userId") String userId);

    @RequestMapping(method = RequestMethod.GET,value="/getuserduty")
    public String getDutyId(@RequestParam("userId") String userId);

    @RequestMapping(method = RequestMethod.GET,value="/getuserids")
    public List<String> getUserIds(@RequestParam("dutyId") String dutyId);

    @RequestMapping(method = RequestMethod.GET,value="/getparentorg")
    public String getParentOrg(@RequestParam("orgId") String orgId);
}
