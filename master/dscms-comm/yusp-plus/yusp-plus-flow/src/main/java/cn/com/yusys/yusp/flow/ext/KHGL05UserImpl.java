package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.dto.WFUserDto;
import cn.com.yusys.yusp.flow.repository.mapper.NWfInstanceMapper;
import cn.com.yusys.yusp.flow.service.core.WorkflowOrgInterface;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("KHGL05UserImpl")
public class KHGL05UserImpl extends StudioUserInterface{

	@Autowired
	private NWfInstanceMapper instanceMapper;
	private static final Log log = LogFactory.getLog(KHGL05UserImpl.class);
	@Override
	public int getOrder() {
		return 2;
	}

	@Override
	public String desc() {
		return "指定Param1为审批人";
	}

	@Override
	public String key() {
		return "KHGL05UserImpl";
	}

	@Override
	public String orgId() {
		return null;
	}

	@Override
	public List<String> customUser(String instanceId, String orgId, String systemId) {
		List<String> result = new ArrayList<>();
		NWfInstance ins = instanceMapper.selectByPrimaryKey(instanceId);
		if(null != ins && null != ins.getBizParam1()){
			result.add(ins.getBizParam1());
		}else{
			log.error("Param1审批人为空！");
		}
		return result;
	}

}
