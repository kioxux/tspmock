package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.engine.NodeInfo;
import cn.com.yusys.yusp.flow.other.engine.init.EngineInterface;
import cn.com.yusys.yusp.flow.service.CmisBizClientService2;
import cn.com.yusys.yusp.flow.service.OcaServiceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service("LmtApplyAmountLimtImpl")
public class LmtApplyAmountLimtImpl extends StudioRouteInterface {

    @Autowired
    private EngineInterface engineInfo;

    @Autowired
    private OcaServiceProvider ocaServiceProvider;

    @Autowired
    private CmisBizClientService2 cmisBizClientService;

    @Override
    public boolean show(ResultInstanceDto resultInstanceDto, String nextNodeId) {

        String dutys = (String)resultInstanceDto.getParam().get("dutys");
        boolean flag = false;
        if(dutys.indexOf("FZH01") > -1 || dutys.indexOf("FZH02") > -1 || dutys.indexOf("XWB01") > -1 || dutys.indexOf("WJB04") > -1) {
            //客户经理、授信额度大于1500万，公司业务部核查岗
            String serno = resultInstanceDto.getBizId();
            String grpNo = resultInstanceDto.getBizUserId();
            BigDecimal amount = cmisBizClientService.getCusGrpMemberLmtAmt(serno, grpNo);
            if(amount.compareTo(new BigDecimal("15000000")) > 0) {
                //flag = "84_27".equals(nextNodeId) ? true : false;
                flag = true;
            }
        }
        return flag;

    }

    @Override
    public int getOrder() {
        return 8;
    }

    @Override
    public String desc() {
        return "授信额度是否超过1500万";
    }

    @Override
    public String key() {
        return "LmtApplyAmountLimtImpl";
    }

    @Override
    public String orgId() {
        return null;
    }
}
