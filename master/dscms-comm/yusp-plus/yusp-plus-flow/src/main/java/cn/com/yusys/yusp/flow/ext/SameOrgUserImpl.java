package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.flow.dto.WFUserDto;
import cn.com.yusys.yusp.flow.ext.StudioUserInterface;
import cn.com.yusys.yusp.flow.service.core.WorkflowOrgInterface;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("SameOrgUserImpl")
public class SameOrgUserImpl  extends StudioUserInterface{
	/**
	 * 用户信息获取服务
	 */
	@Autowired
	private WorkflowOrgInterface userService;
	private static final Log log = LogFactory.getLog(SameOrgUserImpl.class);
	@Override
	public int getOrder() {
		return 2;
	}

	@Override
	public String desc() {
		return "同机构人员(提交机构)";
	}

	@Override
	public String key() {
		return "SameOrgUserImpl";
	}

	@Override
	public String orgId() {
		return null;
	}

	@Override
	public List<String> customUser(String instanceId, String orgId, String systemId) {
		List<String> result = new ArrayList<>();
		List<WFUserDto> datas = userService.getUsersByOrgId(systemId, orgId);
		if(null==datas) {
			log.error("查询同机构【"+orgId+"】下办理人员为空！");
		}
		for(WFUserDto data:datas) {
			if(null != data) {
				result.add(data.getUserId());
			}
		}
		return result;
	}

}
