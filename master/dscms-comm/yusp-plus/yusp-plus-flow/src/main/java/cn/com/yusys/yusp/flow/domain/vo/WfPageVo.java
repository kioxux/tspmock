/**
 * Copyright (C), 2014-2021
 * FileName: PageVo
 * Author: Administrator
 * Date: 2021/3/22 10:24
 * Description:
 * History:
 * <author> <time> <version> <desc>
 * Administrator 10:24 1.0.0 新建类
 */

package cn.com.yusys.yusp.flow.domain.vo;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * 〈〉
 * @author zhui
 * @create 2021/3/22
 * @since 1.0.0
 */

public class WfPageVo<T> implements Serializable {
    List<T> records = Collections.emptyList();
    long total = 0;

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }
}
