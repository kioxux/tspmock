package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.service.OcaServiceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("IsDifferentImpl")
public class IsDifferentImpl extends StudioRouteInterface{

    @Autowired
    private OcaServiceProvider ocaServiceProvider;

    @Override
    public boolean show(ResultInstanceDto resultInstanceDto, String nextNodeId) {
        //获取金融市场总部总裁对应的用户编号
        List<String> userIds = ocaServiceProvider.getUserIds("JRB10");
        //获取金融市场总部风险合规部负责人对应的用户编号
        List<String> userIdList = ocaServiceProvider.getUserIds("JRB04");
        //如果两个岗位由同一个人担任，则返回true
        if(userIds.get(0).equals(userIdList.get(0))){
            return false;
        }
        return true;
    }

    @Override
    public int getOrder() {
        return 6;
    }

    @Override
    public String desc() {
        return "（不同）总裁和风险合规部负责人是否为同一人判断";
    }

    @Override
    public String key() {
        return "IsDifferentImpl";
    }

    @Override
    public String orgId() {
        return null;
    }



}
