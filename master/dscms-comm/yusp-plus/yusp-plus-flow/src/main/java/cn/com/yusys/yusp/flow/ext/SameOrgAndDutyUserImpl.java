package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.domain.vo.WfAdminSmUserVo;
import cn.com.yusys.yusp.flow.dto.WFUserDto;
import cn.com.yusys.yusp.flow.other.engine.NodeInfo;
import cn.com.yusys.yusp.flow.other.engine.init.EngineInterface;
import cn.com.yusys.yusp.flow.other.org.OrgInterface;
import cn.com.yusys.yusp.flow.other.org.WFUser;
import cn.com.yusys.yusp.flow.repository.mapper.NWfInstanceMapper;
import cn.com.yusys.yusp.flow.service.OcaServiceProvider;
import cn.com.yusys.yusp.flow.service.core.WorkflowOrgInterface;
import cn.com.yusys.yusp.flow.service.impl.WfOrgImpl;
import cn.com.yusys.yusp.flow.util.WorkFlowUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("SameOrgAndDutyUserImpl")
public class SameOrgAndDutyUserImpl extends StudioUserInterface{
	/**
	 * 用户信息获取服务
	 */
	@Autowired
	private NWfInstanceMapper nWfInstanceMapper;
	@Autowired
	private EngineInterface engineInfo;
	@Autowired
	private OcaServiceProvider ocaServiceProvider;
	private static final Log log = LogFactory.getLog(SameOrgAndDutyUserImpl.class);
	@Override
	public int getOrder() {
		return 6;
	}

	@Override
	public String desc() {
		return "同机构同岗位人员(提交机构)";
	}

	@Override
	public String key() {
		return "SameOrgAndDutyUserImpl";
	}

	@Override
	public String orgId() {
		return null;
	}

	@Override
	public List<String> customUser(String instanceId, String orgId, String systemId) {
		NWfInstance nWfInstance = nWfInstanceMapper.selectByPrimaryKey(instanceId);
		//获取流程提交人id
		String flowStarter = nWfInstance.getFlowStarter();
//		String flowParam = nWfInstance.getFlowParam();
//		Map<String, Object> map = WorkFlowUtil.strToMap(flowParam);
		//获取所有下一节点号
//		List<String> list = engineInfo.getNextNodeIds((String) map.get("submitNodeId"));
//		String dutyId = "";
//		for (String nodeId:list) {
//			//从缓存中获取下一节点配置信息
//			NodeInfo node = engineInfo.getNodeInfo(nodeId);
//			//下一节点配置人员（岗位+自定义类）
//			List<String> nodeUsers = WorkFlowUtil.splitNodeUser(node.getNodeUser());
//			if(nodeUsers.contains("E.SameOrgAndDutyUserImpl")){
//				for(String key:nodeUsers) {
//					if (key.startsWith("G")) {
//						dutyId = key.substring(2);
//						break;
//					}
//				}
//				break;
//			}
//		}
		//下一节点和流程发起人相同的岗位号
		List<WfAdminSmUserVo> smusers = ocaServiceProvider.getUsersByOrgAndDutyForWf(orgId,flowStarter);
		if(smusers == null){
			return new ArrayList<>();
		}
		List<WFUser> users = getWfUserList(smusers);
		//从下一节点的岗位人员中去除流程发起人
		List<String> re = new ArrayList<>();
		for(WFUser user:users){
			if(user.getUserId().equals(flowStarter)){
				continue;
			}
			re.add(user.getUserId());
		}
		return re;
	}

	public List<WFUser> getWfUserList(List<WfAdminSmUserVo> users){
		return users.stream().map(vo -> {
			WFUser user = new WFUser();
			user.setUserId(vo.getUserId());
			user.setUserName(vo.getUserName());
			user.setUserMobile(vo.getUserMobilephone());
			user.setUserEmail(vo.getUserEmail());
			return user;
		}).collect(Collectors.toList());
	}

}
