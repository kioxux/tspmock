package cn.com.yusys.yusp.flow.domain.dto;


import java.io.Serializable;

public class ResultVoteListDto implements Serializable {
    private String instanceId;
    private String meetId;
    private String voteId;
    private String bizId;
    private String startTime;
    private String mettingSub;
    private String mettingYear;
    private String mettingNo;
    private String mettingComment;
    private String voteSts;
    private String isCheckBiz;
    private String bizPage;
    private String userId;
    private String orgId;
    private String orgName;
    private String flowName;
    private String meetType;
    private Integer meetOrder;
    private String mettingSts;
    private String bizType;
    private String appOrgId;
    private String bizUserId;
    private String flowStarter;
    private String flowStarterName;
    private String bizUserName;
    private String appOrgName;
    private static final long serialVersionUID = 1L;

    public ResultVoteListDto() {
    }

    public String getIsCheckBiz() { return this.isCheckBiz; }

    public void setIsCheckBiz(String isCheckBiz) { this.isCheckBiz = isCheckBiz == null ? null : isCheckBiz; }

    public String getVoteSts() { return this.voteSts; }

    public void setVoteSts(String voteSts) { this.voteSts = voteSts == null ? null : voteSts; }

    public String getMettingSub() { return this.mettingSub; }

    public void setMettingSub(String mettingSub) { this.mettingSub = mettingSub == null ? null : mettingSub; }

    public String getMettingYear() { return this.mettingYear; }

    public void setMettingYear(String mettingYear) { this.mettingYear = mettingYear == null ? null : mettingYear; }

    public String getMettingNo() { return this.mettingNo; }

    public void setMettingNo(String mettingNo) { this.mettingNo = mettingNo == null ? null : mettingNo; }

    public String getMettingComment() { return this.mettingComment; }

    public void setMettingComment(String mettingComment) { this.mettingComment = mettingComment == null ? null : mettingComment; }

    public String getVoteId() {
        return this.voteId;
    }

    public void setVoteId(String voteId) {
        this.voteId = voteId == null ? null : voteId;
    }

    public String getFlowStarterName() {
        return this.flowStarterName;
    }

    public void setFlowStarterName(String flowStarterName) {
        this.flowStarterName = flowStarterName == null ? null : flowStarterName;
    }

    public String getFlowStarter() {
        return this.flowStarter;
    }

    public void setFlowStarter(String flowStarter) {
        this.flowStarter = flowStarter == null ? null : flowStarter;
    }

    public String getAppOrgName() {
        return this.appOrgName;
    }

    public void setAppOrgName(String appOrgName) {
        this.appOrgName = appOrgName == null ? null : appOrgName;
    }

    public String getAppOrgId() {
        return this.appOrgId;
    }

    public void setAppOrgId(String appOrgId) {
        this.appOrgId = appOrgId == null ? null : appOrgId;
    }

    public Integer getMeetOrder() {
        return this.meetOrder;
    }

    public void setMeetOrder(int meetOrder) {
        this.meetOrder =  meetOrder;
    }

    public String getMeetType() {
        return this.meetType;
    }

    public void setMeetType(String meetType) {
        this.meetType = meetType == null ? null : meetType.trim();
    }

    public String getBizPage() {
        return this.bizPage;
    }

    public void setBizPage(String bizPage) {
        this.bizPage = bizPage == null ? null : bizPage.trim();
    }

    public String getBizType() {
        return this.bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType == null ? null : bizType.trim();
    }

    public String getMettingSts() {
        return this.mettingSts;
    }

    public void setMettingSts(String mettingSts) {
        this.mettingSts = mettingSts == null ? null : mettingSts.trim();
    }

    public String getMeetId() {
        return this.meetId;
    }

    public void setMeetId(String meetId) {
        this.meetId = meetId == null ? null : meetId.trim();
    }

    public String getBizUserId() {
        return this.bizUserId;
    }

    public void setBizUserId(String bizUserId) {
        this.bizUserId = bizUserId == null ? null : bizUserId.trim();
    }

    public String getInstanceId() {
        return this.instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId == null ? null : instanceId.trim();
    }

    public String getFlowName() {
        return this.flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName == null ? null : flowName.trim();
    }

    public String getStartTime() {
        return this.startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime == null ? null : startTime.trim();
    }

    public String getOrgId() {
        return this.orgId;
    }

    public String getOrgName() {
        return this.orgName;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName == null ? null : orgName.trim();
    }

    public String getBizId() {
        return this.bizId;
    }

    public void setBizId(String bizId) {
        this.bizId = bizId == null ? null : bizId.trim();
    }

    public String getBizUserName() {
        return this.bizUserName;
    }

    public void setBizUserName(String bizUserName) {
        this.bizUserName = bizUserName == null ? null : bizUserName.trim();
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
