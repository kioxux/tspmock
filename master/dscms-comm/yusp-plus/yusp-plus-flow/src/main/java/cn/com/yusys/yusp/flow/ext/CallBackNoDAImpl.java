package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.domain.NWfNodeDone;
import cn.com.yusys.yusp.flow.domain.NWfUserDone;
import cn.com.yusys.yusp.flow.dto.NextNodeInfoDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.engine.init.EngineInterface;
import cn.com.yusys.yusp.flow.repository.mapper.NWfNodeDoneMapper;
import cn.com.yusys.yusp.flow.repository.mapper.NWfUserDoneMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 打回节点过滤--排除【集中作业档案岗节点】
 * PS: 流程定义中，集中作业档案岗节点必须设置节点标识【JZZYDAG】或者节点名称必须为【集中作业档案岗】；此过滤规则才会生效
 */
@Service("CallBackNoDAImpl")
public class CallBackNoDAImpl extends StudioCallBackInterface {
    private static final Logger log = LoggerFactory.getLogger(CallBackNoDAImpl.class);

    @Autowired
    private NWfUserDoneMapper userDoneMapper;

    @Autowired
    private NWfNodeDoneMapper nodeDoneMapper;

    @Autowired
    private EngineInterface engineInfo;

    @Override
    public List<NextNodeInfoDto> getCallbackUser(ResultInstanceDto instanceDto) {
        QueryModel model = new QueryModel();
        model.addCondition("instanceId", instanceDto.getInstanceId());
        model.setSort("end_time");
        List<NWfUserDone> userDone = this.userDoneMapper.selectByModel(model);
        List<String> nextsNodes = this.engineInfo.getAllNextNodesId(instanceDto.getNodeId()); // 获取后续所有节点

        List<NextNodeInfoDto> nextNodeInfo = new ArrayList();
        Map<String, Set> nodesT = new HashMap();
        Iterator var6 = userDone.iterator();

        while(var6.hasNext()) {
            NWfUserDone done = (NWfUserDone)var6.next();
            // 获取已办节点详细信息
            NWfNodeDone nodeDone = this.nodeDoneMapper.selectByPrimaryKey(instanceDto.getInstanceId(), done.getNodeId());
            if(nodeDone.getNodeName().indexOf("集中作业档案岗") > -1 || "JZZYDAG".equalsIgnoreCase(nodeDone.getNodeSign())){
                log.info("打回非集中作业档案岗-已办节点【{}】，已办人员【{}】为集中作业档案岗，过滤掉！", done.getNodeId(), done.getUserId());
            }else if(nextsNodes.indexOf(done.getNodeId()) > -1){
                log.info("打回非集中作业档案岗--已办节点【{}】，已办人员【{}】非当前节点的后续节点，不允许打回给后续节点，过滤掉！", done.getNodeId(), done.getUserId());
            }else{
                if (nodesT.containsKey(done.getNodeId())) {
                    ((Set)nodesT.get(done.getNodeId())).add(done.getUserId());
                } else {
                    Set<String> userT = new HashSet();
                    userT.add(done.getUserId());
                    nodesT.put(done.getNodeId(), userT);
                }
            }
        }

        nodesT.remove(instanceDto.getNodeId());
        var6 = nodesT.keySet().iterator();

        while(var6.hasNext()) {
            String key = (String)var6.next();
            NextNodeInfoDto nodeUser = new NextNodeInfoDto();
            nodeUser.setNextNodeId(key);
            List<String> nextNodeUserIds = new ArrayList();
            nextNodeUserIds.addAll((Collection)nodesT.get(key));
            nodeUser.setNextNodeUserIds(nextNodeUserIds);
            nextNodeInfo.add(nodeUser);
        }

        return nextNodeInfo;
    }

    @Override
    public int getOrder() {
        return 3;
    }

    @Override
    public String desc() {
        return "打回非集中作业档案岗";
    }

    @Override
    public String key() {
        return "CallBackNoDAImpl";
    }

    @Override
    public String orgId() {
        return null;
    }
}
