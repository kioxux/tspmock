/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.flow.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: yusp-plus-flow模块
 * @类名称: CreditApproveAllotRule
 * @类描述: credit_approve_allot_rule数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-08 21:20:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "credit_approve_allot_rule")
public class CreditApproveAllotRule extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 规则类型 **/
	@Id
	@Column(name = "RULE_TYPE")
	private String ruleType;
	
	/** 适用机构 **/
	@Id
	@Column(name = "BR_ID")
	private String brId;
	
	/** 评审人员 **/
	@Id
	@Column(name = "REVIEW_ID")
	private String reviewId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param ruleType
	 */
	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}
	
    /**
     * @return ruleType
     */
	public String getRuleType() {
		return this.ruleType;
	}
	
	/**
	 * @param brId
	 */
	public void setBrId(String brId) {
		this.brId = brId;
	}
	
    /**
     * @return brId
     */
	public String getBrId() {
		return this.brId;
	}
	
	/**
	 * @param reviewId
	 */
	public void setReviewId(String reviewId) {
		this.reviewId = reviewId;
	}
	
    /**
     * @return reviewId
     */
	public String getReviewId() {
		return this.reviewId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}