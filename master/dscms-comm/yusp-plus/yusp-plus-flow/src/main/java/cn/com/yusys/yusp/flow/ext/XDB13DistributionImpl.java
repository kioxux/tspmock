package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.dto.AdminSmPropDto;
import cn.com.yusys.yusp.dto.AdminSmPropQueryDto;
import cn.com.yusys.yusp.flow.constant.DscmsCommonConstance;
import cn.com.yusys.yusp.flow.domain.*;
import cn.com.yusys.yusp.flow.domain.dto.ResultInstanceTodoDto;
import cn.com.yusys.yusp.flow.domain.vo.ResultUserTodoNum;
import cn.com.yusys.yusp.flow.dto.WFUserDto;
import cn.com.yusys.yusp.flow.other.engine.NodeInfo;
import cn.com.yusys.yusp.flow.other.engine.init.EngineInterface;
import cn.com.yusys.yusp.flow.other.enums.ReDoUserSelect;
import cn.com.yusys.yusp.flow.repository.mapper.*;
import cn.com.yusys.yusp.flow.service.CreditApproveAllotRuleService;
import cn.com.yusys.yusp.flow.service.NWfInstanceHisService;
import cn.com.yusys.yusp.flow.service.NWfInstanceService;
import cn.com.yusys.yusp.flow.service.core.WorkflowOrgInterface;
import cn.com.yusys.yusp.flow.util.WorkFlowUtil;
import cn.com.yusys.yusp.service.AdminSmPropService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 信贷管理部核查岗自动分配规则
 */
@Service("XDB13DistributionImpl")
public class XDB13DistributionImpl extends StudioStrategyInterface {
    private static final Logger log = LoggerFactory.getLogger(XDB13DistributionImpl.class);

    @Autowired
    private NWfUserHisMapper userHisMapper;
    @Autowired
    private NWfNodeHisMapper nodeHisMapper;
    @Autowired
    private WorkFlowCustomBenchMapper workFlowCustomBenchMapper;
    @Autowired
    private EngineInterface engineInfo;
    @Autowired
    private NWfInstanceService nWfInstanceService;
    @Autowired
    private NWfInstanceHisService nWfInstanceHisService;
    @Autowired
    private NWfUserDoneMapper userDoneService;
    @Autowired
    private WorkflowOrgInterface userService;
    @Autowired
    private AdminSmPropService adminSmPropService;
    @Autowired
    private CreditApproveAllotRuleService creditApproveAllotRuleService;
    @Autowired
    private WorkflowBenchMapperExt workflowBenchMapperExt;
    // 审批人员分配阈值
    private static final String MAX_THRESHOLD_VALUE = "MAX_THRESHOLD_VALUE";
    // 需要获取原流程审批人流程
    private static final String XDGLB_ORIGIN_APPR_USER_BIZ_TYPE = "XDGLB_ORIGIN_APPR_USER_BIZ_TYPE";

    private Random random = new Random(1L);

    @Override
    public List<WFUserDto> selectUser(List<WFUserDto> user, String instanceId, String nodeId, String orgId, String systemId) {
        List<WFUserDto> result = new ArrayList<>();
        // 现审批流程实例
        NWfInstance instance = nWfInstanceService.selectByPrimaryKey(instanceId);
        // 参数信息
        Map<String, Object> map = WorkFlowUtil.strToMap(instance.getFlowParam());
        // 查询流程数据
        ResultInstanceTodoDto resultInstanceTodoDto = workflowBenchMapperExt.getInstanceInfoByInstanceId(instanceId);
        orgId = resultInstanceTodoDto.getOrgId();
        // 1、判断是否为退回再提交，若是直接提交给上次审批人
        WFUserDto wfUserDto = this.getCallBackRedoUser(instanceId, nodeId, systemId);
        if(wfUserDto != null){
            result.add(wfUserDto);
            return result;
        }

        AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
        adminSmPropQueryDto.setPropName(XDGLB_ORIGIN_APPR_USER_BIZ_TYPE);
        AdminSmPropDto adminSmPropDto = adminSmPropService.getPropValue(adminSmPropQueryDto).getData();
        String originApproveBizType = String.valueOf(adminSmPropDto.getPropValue());

        // 2、判断流程类型 指定流程的信贷管理部需要优先找原流程审批人
        if(originApproveBizType.contains(instance.getBizType())){
            // 判断场景，执行A规则：获取前一流程同岗位审批人(在职)
            wfUserDto = this.getLastUserForA(instanceId, nodeId, systemId);
            if(wfUserDto != null){
                result.add(wfUserDto);
                return result;
            }
        }

        // 3、判断是否是标准化产品 流程路由参数中放参数 isAllStdPrd
        String isAllStdPrd = Optional.ofNullable((String) map.get("isAllStdPrd")).orElse("0");
        if("1".equals(isAllStdPrd)){
            // 判断场景，执行D规则：全行标准产品、异地支行、异地分行
            wfUserDto = this.getLowTodoCountUserByCfgMapForD("000000", systemId);
            if(wfUserDto != null){
                result.add(wfUserDto);
                return result;
            }
        }

        // 4、判断自动分配时是否计算业务量 流程路由参数中放参数 isAssignByTodoNum
        String isAssignByTodoNum = Optional.ofNullable((String) map.get("isAssignByTodoNum")).orElse("0");
        if("1".equals(isAssignByTodoNum)){
            // 判断场景，执行C规则：本地非标准化产品
            wfUserDto = this.getLowTodoCountUserByCfgMapForC(orgId, systemId);
        }else{
            // 判断场景，执行B规则：自动分配时不计算审批业务量
            wfUserDto = this.getRandomUserByCfgMapForB(orgId, systemId);
        }
        if(wfUserDto != null){
            result.add(wfUserDto);
            return result;
        }
        return result;
    }

    @Override
    public int getOrder() {
        return 4;
    }

    @Override
    public String desc() {
        return "信贷管理部核查岗自动分配规则";
    }

    @Override
    public String key() {
        return "XDB13DistributionImpl";
    }

    @Override
    public String orgId() {
        return null;
    }


    /**
     * 若为打回，（基于流程图配置）获取重办人员（在职）
     * @param instanceId
     * @param nodeId
     * @return
     */
    private WFUserDto getCallBackRedoUser(String instanceId, String nodeId, String systemId){
        NodeInfo nodeInfo = this.engineInfo.getNodeInfo(nodeId);
        String reDoUserSelect = nodeInfo.getReDoUserSelect();
        if (ReDoUserSelect.HIS_USER.equals(reDoUserSelect)) {
            QueryModel model = new QueryModel();
            model.getCondition().put("instanceId", instanceId);
            model.getCondition().put("nodeId", nodeId);
            model.setSort("start_time desc");
            List<NWfUserDone> usersT = this.userDoneService.selectByModel(model);
            for (NWfUserDone userDone: usersT) {
                WFUserDto userDto = this.userService.getUserInfo(systemId, userDone.getUserId());
                if(null != userDto){
                    return userDto;
                }
            }
        }
        return null;
    }

    /**
     * A规则：获取前一流程同岗位审批人(在职)
     * @param instanceId
     * @return
     */
    public WFUserDto getLastUserForA(String instanceId, String nodeId, String systemId){
        // 3、前一流程同岗位审批节点人
        NWfInstance instanceInfo = nWfInstanceService.selectByPrimaryKey(instanceId);
        // 3.1（约定在bizParam1传递指定人员）
        String lastuser = instanceInfo.getBizParam1();
        if(StringUtils.isBlank(lastuser)){
            // 3.2（约定在params:{serno: ****}传递上一流程业务流水号）
            lastuser = this.getLastFlowUser(instanceId, nodeId);
        }
        if(StringUtils.isNotBlank(lastuser)){
            WFUserDto userDto = this.userService.getUserInfo(systemId, lastuser);
            return userDto;
        }

        return null;
    }

    /**
     * 获取前一个审批通过的流程，同岗位节点（多个时取最后一个审批通过的）办理人
     * 类似：影像补扫流程节点自动分配规则
     * @return
     */
    private String getLastFlowUser(String instanceId, String nodeId){
        String result = null;
        // 现审批流程实例
        NWfInstance instance = nWfInstanceService.selectByPrimaryKey(instanceId);
        // 参数信息
        Map<String, Object> map = WorkFlowUtil.strToMap(instance.getFlowParam());
        // 原流程关联业务流水号
        String serno = Optional.ofNullable((String) map.get("lastSerno")).orElse("");
        if (StringUtils.isEmpty(serno)){
            if(log.isDebugEnabled()){
                log.info("信贷管理部核查岗自动分配规则:A规则：上一审批流程缺少业务流水号无法查找其流程，将执行自动分配规则，流程实例：{}，节点ID：{}", instanceId, nodeId);
            }
        }else{
            // 根据关联业务流水号获取最近一笔历史通过审批流程实例
            QueryModel model = new QueryModel();
            model.addCondition("bizId", serno);
            model.addCondition("flowState", "E");
            model.setSort("endTime desc");
            List<NWfInstanceHis> instanceHisList = nWfInstanceHisService.selectByModel(model);
            if(CollectionUtils.isEmpty(instanceHisList)){
                if(log.isDebugEnabled()){
                    log.info("信贷管理部核查岗自动分配规则:A规则：上一审批流程根据业务流水号未查找到其已办结流程，将执行自动分配规则，流程实例：{}，节点ID：{}", instanceId, nodeId);
                }
            }else{
                String oldInstanceId = instanceHisList.get(0).getInstanceId();
                // 当前流程节点信息
                NodeInfo nodeInfo = engineInfo.getNodeInfo(nodeId);
                String nodeUser = nodeInfo.getNodeUser(); // 节点配置的办理人，岗位信息(G.xxx;)
                // 根据原流程实例获取流程审批节点信息
                QueryModel model1 = new QueryModel();
                model1.addCondition("instanceId", oldInstanceId);
                model1.setSort("endTime desc");
                List<NWfNodeHis> nodeHisList = nodeHisMapper.selectByModel(model1);
                // 获取原流程同岗位节点ID
                String oldNodeId = "";
                if (!CollectionUtils.isEmpty(nodeHisList)) {
                    for (NWfNodeHis his : nodeHisList) {
                        NodeInfo hisNodeInfo = engineInfo.getNodeInfo(his.getNodeId());
                        String hisNodeUser = hisNodeInfo.getNodeUser();
                        // 选取同岗位人员返回
                        if(hisNodeUser.contains(nodeUser)){
                            oldNodeId = his.getNodeId();
                            break;
                        }
                    }
                }
                // 根据原流程实例和节点ID获取人员办理信息
                if(StringUtils.isNotEmpty(oldNodeId)){
                    QueryModel model2 = new QueryModel();
                    model2.addCondition("instanceId", oldInstanceId);
                    model2.addCondition("nodeId", oldNodeId);
                    model2.setSort("endTime desc");
                    List<NWfUserHis> userDoneList = userHisMapper.selectByModel(model2);
                    if(!CollectionUtils.isEmpty(userDoneList)){
                        result = userDoneList.get(0).getUserId();
                    }
                }
            }

        }
        return result;
    }

    /**
     * B规则：自动分配时不计算审批业务量
     * 根据配置的【机构-评审】映射关系随机分配一评审
     * @param orgCode
     * @param systemId
     * @return 可能为null   人员离职
     */
    public WFUserDto getRandomUserByCfgMapForB(String orgCode, String systemId){
        String userCode;
        // 获取本机构对应（未请假）评审
        List<CreditApproveAllotRule> userList = creditApproveAllotRuleService.selectReviewIdByBrId(orgCode);
        // 本机构对应（未请假）评审为空
        if(CollectionUtils.isEmpty(userList)){
            log.info("信贷管理部核查岗自动分配规则:B规则:本机构对应（未请假）评审为空，将从本机构所属组内随机选择一人");
            userList = creditApproveAllotRuleService.selectGroupInfoByRuleType(orgCode);
            if(CollectionUtils.isEmpty(userList)){
                log.error("信贷管理部核查岗自动分配规则:B规则:本机构所属组内对应（未请假）评审为空，请检查配置");
            }else{
                userCode = userList.get(random.nextInt(userList.size())).getReviewId();
                if(StringUtils.isNotBlank(userCode)){
                    WFUserDto userDto = this.userService.getUserInfo(systemId, userCode);
                    return userDto;
                }
            }

        }else{
            if(userList.size() == 1){
                userCode = userList.get(0).getReviewId();
            }else{
                log.info("信贷管理部核查岗自动分配规则:B规则:本机构对应（未请假）评审为多个，将随机选择一人");
                userCode = userList.get(random.nextInt(userList.size())).getReviewId();
            }
            if(StringUtils.isNotBlank(userCode)){
                WFUserDto userDto = this.userService.getUserInfo(systemId, userCode);
                return userDto;
            }
        }
        log.error("信贷管理部核查岗自动分配规则:B规则:未获取到有效评审，可能未配置机构（及其分组）对应评审或对应评审离职");
        return null;
    }

    /**
     * C规则：根据配置的【机构-评审】映射关系，计算并分配给对应评审
     * 本地非标准化产品
     * 1、超过阈值，分给同组内最少的评审
     * 2、未超阈值，随机分配对应评审任意一评审
     * @return
     */
    public WFUserDto getLowTodoCountUserByCfgMapForC(String orgCode, String systemId){
        String userCode;
        // 1、获取本机构对应（未请假）评审
        List<CreditApproveAllotRule> userList = creditApproveAllotRuleService.selectReviewIdByBrId(orgCode);
        // 2、本机构对应（未请假）评审为空
        if(CollectionUtils.isEmpty(userList)){
            log.info("信贷管理部核查岗自动分配规则:C规则:本机构对应（未请假）评审为空，将从本机构所属组内按业务量分配");
            userList = creditApproveAllotRuleService.selectGroupInfoByRuleType(orgCode);
            if(CollectionUtils.isEmpty(userList)){
                log.error("信贷管理部核查岗自动分配规则:C规则:本机构所属组内对应（未请假）评审为空，请检查配置");
            }
            String userCodes = userList.stream().map(CreditApproveAllotRule::getReviewId).collect(Collectors.joining(","));
            List<ResultUserTodoNum> userTodoNumList = workFlowCustomBenchMapper.getUserTodoNumCreditCard(userCodes, DscmsCommonConstance.XDB13_COUNT_FLOW_BIZTYPES);
            userCode = userTodoNumList.stream().min(Comparator.comparing(vo -> vo.getTodoNum())).get().getUserId();
            if(StringUtils.isNotBlank(userCode)){
                WFUserDto userDto = this.userService.getUserInfo(systemId, userCode);
                return userDto;
            }
        }else{
            // 3、本机构对应（未请假）评审有值
            log.info("信贷管理部核查岗自动分配规则:C规则:本机构对应（未请假）评审为一个或多个，先检索出业务量最少评审");
            String userCodes = userList.stream().map(CreditApproveAllotRule::getReviewId).collect(Collectors.joining(","));
            List<ResultUserTodoNum> userTodoNumList = workFlowCustomBenchMapper.getUserTodoNumCreditCard(userCodes, DscmsCommonConstance.XDB13_COUNT_FLOW_BIZTYPES);
            ResultUserTodoNum currentOrgUserMinTodoNum = null;
            if(userTodoNumList != null && userTodoNumList.size() == 1){
                currentOrgUserMinTodoNum = userTodoNumList.get(0);
            }else{
                currentOrgUserMinTodoNum = userTodoNumList.stream().min(Comparator.comparing(vo -> vo.getTodoNum())).get();
            }

            // 3.1 校验是否超过阈值
            log.info("信贷管理部核查岗自动分配规则:C规则:本机构对应（未请假）评审为多个，校验组内唯一或业务量最少评审是否超阈值");
            // 3.2 获取组内业务量最少的人
            userList = creditApproveAllotRuleService.selectGroupInfoByRuleType(orgCode);
            String groupUserCodes = userList.stream().map(CreditApproveAllotRule::getReviewId).collect(Collectors.joining(","));
            List<ResultUserTodoNum> groupUserTodoNumList = workFlowCustomBenchMapper.getUserTodoNumCreditCard(groupUserCodes, DscmsCommonConstance.XDB13_COUNT_FLOW_BIZTYPES);
            ResultUserTodoNum groupUserMinTodoNum = groupUserTodoNumList.stream().min(Comparator.comparing(vo -> vo.getTodoNum())).get();
            int distance = currentOrgUserMinTodoNum.getTodoNum() - groupUserMinTodoNum.getTodoNum();
            log.info("信贷管理部核查岗自动分配规则:C规则:本机构对应（未请假）评审最少业务量与组内业务量最少评审相差【{}】", distance);

            AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
            adminSmPropQueryDto.setPropName(MAX_THRESHOLD_VALUE);
            AdminSmPropDto adminSmPropDto = adminSmPropService.getPropValue(adminSmPropQueryDto).getData();
            int maxThresholdValue = Integer.parseInt(adminSmPropDto.getPropValue());
            if(distance > maxThresholdValue){
                log.info("信贷管理部核查岗自动分配规则:C规则:本机构对应（未请假）评审最少业务量与组内业务量最少评审超阈值");
                userCode = groupUserMinTodoNum.getUserId();
            }else{
                log.info("信贷管理部核查岗自动分配规则:C规则:本机构对应（未请假）评审最少业务量与组内业务量最少评审未超阈值");
                userCode = currentOrgUserMinTodoNum.getUserId();
            }
            if(StringUtils.isNotBlank(userCode)){
                WFUserDto userDto = this.userService.getUserInfo(systemId, userCode);
                return userDto;
            }
        }
        return null;
    }

    /**
     * D规则：根据配置的【机构-评审】映射关系，计算并分配给对应评审
     * 全行标准产品、异地支行、异地分行
     * 1、机构对应评审一个的直接分配
     * 2、机构对应评审多个的按业务量分配
     *
     * @return
     */
    public WFUserDto getLowTodoCountUserByCfgMapForD(String orgCode, String systemId){
        String userCode;
        // 获取本机构对应（未请假）评审
        List<CreditApproveAllotRule> userList = creditApproveAllotRuleService.selectReviewIdByBrId(orgCode);
        // 本机构对应（未请假）评审为空
        if(CollectionUtils.isEmpty(userList)){
            log.info("信贷管理部核查岗自动分配规则:D规则:本机构对应（未请假）评审为空，将从本机构所属组内随机选择一人");
            userList = creditApproveAllotRuleService.selectGroupInfoByRuleType(orgCode);
            if(CollectionUtils.isEmpty(userList)){
                log.error("信贷管理部核查岗自动分配规则:D规则:本机构所属组内对应（未请假）评审为空，请检查配置");
            }
            userCode = userList.get(random.nextInt(userList.size())).getReviewId();
            if(StringUtils.isNotBlank(userCode)){
                WFUserDto userDto = this.userService.getUserInfo(systemId, userCode);
                return userDto;
            }
        }else{
            if(userList.size() == 1){
                userCode = userList.get(0).getReviewId();
            }else{
                log.info("信贷管理部核查岗自动分配规则:D规则:本机构对应（未请假）评审为多个，按业务量分配");
                String userCodes = userList.stream().map(CreditApproveAllotRule::getReviewId).collect(Collectors.joining(","));
                List<ResultUserTodoNum> userTodoNumList = workFlowCustomBenchMapper.getUserTodoNumCreditCard(userCodes, DscmsCommonConstance.XDB13_COUNT_FLOW_BIZTYPES);
                userCode = userTodoNumList.stream().min(Comparator.comparing(vo -> vo.getTodoNum())).get().getUserId();
            }
            if(StringUtils.isNotBlank(userCode)){
                WFUserDto userDto = this.userService.getUserInfo(systemId, userCode);
                return userDto;
            }
        }
        log.error("信贷管理部核查岗自动分配规则:D规则:未获取到有效评审，可能未配置机构（及其分组）对应评审或对应评审离职");
        return null;
    }


}
