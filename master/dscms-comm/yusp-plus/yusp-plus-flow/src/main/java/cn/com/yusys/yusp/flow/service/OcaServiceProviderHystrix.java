/**
 * Copyright (C), 2014-2021
 * FileName: OcaServiceProviderHystrix
 * Author: Administrator
 * Date: 2021/3/29 11:36
 * Description:
 * History:
 * <author> <time> <version> <desc>
 * Administrator 11:36 1.0.0 新建类
 */

package cn.com.yusys.yusp.flow.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.flow.domain.vo.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 〈〉
 *
 * @author Administrator
 * @create 2021/3/29
 * @since 1.0.0
 */
@Component
public class OcaServiceProviderHystrix implements OcaServiceProvider {
    @Override
    public WfPageVo<WfAdminSmUserVo> getUsersForWf(QueryModel param) {
        return null;
    }

    @Override
    public WfPageVo<WfAdminSmDptVo> getDeptsForWf(QueryModel param) {
        return null;
    }

    @Override
    public WfPageVo<WfAdminSmDutyVo> getDutysForWf(QueryModel param) {
        return null;
    }

    @Override
    public WfPageVo<WfAdminSmOrgVo> getOrgsForWf(QueryModel param) {
        return null;
    }

    @Override
    public WfPageVo<WfAdminSmRoleVo> getRolesForWf(QueryModel param) {
        return null;
    }

    @Override
    public List<WfAdminSmUserVo> getUsersByOrgForWf(String systemId, String orgId) {
        return null;
    }

    @Override
    public List<WfAdminSmUserVo> getUsersByOrgAndDutyForWf(String orgId, String dutyId) {
        return null;
    }

    @Override
    public List<WfAdminSmUserVo> getUsersByDeptForWf(String systemId, String deptId) {
        return null;
    }

    @Override
    public List<WfAdminSmUserVo> getUsersByDutyForWf(String systemId, String dutyId) {
        return null;
    }

    @Override
    public List<WfAdminSmUserVo> getUsersByRoleForWf(String systemId, String roleId) {
        return null;
    }

    @Override
    public WfAdminSmUserVo getUserInfoForWf(String systemId, String userId) {
        return null;
    }

    @Override
    public List<String> getLowerOrgId(String orgCode) {
        return null;
    }

    @Override
    public List<String> getDutyCodeByUserId(String userId) {
        return null;
    }

    @Override
    public String getDutyId(String userId) {
        return null;
    }

    @Override
    public List<String> getUserIds(String dutyId) {
        return null;
    }

    @Override
    public String getParentOrg(String orgId) {
        return null;
    }
}
