package cn.com.yusys.yusp.flow.config;


import cn.com.yusys.yusp.flow.demo.BizMessageListener;
import cn.com.yusys.yusp.flow.demo.ClientBizDemo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DemoWorkFlowConfig{
	
	@Bean
	public BizMessageListener bizMessageListener(){

		return new BizMessageListener();
	}
	
	@Bean
	public ClientBizDemo clientBizDemo(){
		return new ClientBizDemo();
	}
}
