package cn.com.yusys.yusp.flow.service;

import cn.com.yusys.yusp.flow.service.impl.CmisBizClientServiceImpl2;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;


/**
 * cmis-biz服务对外提供服务接口
 * @author monchi
 * @date 2020-12-19 11:50:00
 */
@FeignClient(name = "cmis-biz",path = "/api",fallback = CmisBizClientServiceImpl2.class )
public interface CmisBizClientService2 {

    @PostMapping("/lmtgrpreply/getGrpMemberLmtAmt")
    public BigDecimal getCusGrpMemberLmtAmt(@RequestParam("serno") String serno, @RequestParam("grpNo") String grpNo);

    @PostMapping("/areauser/getAreaUsersByOrgId")
    public String getAreaUsersByOrgId(@RequestBody String orgId);

}
