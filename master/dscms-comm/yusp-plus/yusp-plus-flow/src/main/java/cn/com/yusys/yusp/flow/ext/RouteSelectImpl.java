package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.engine.NodeInfo;
import cn.com.yusys.yusp.flow.other.engine.init.EngineInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

//@Service("RouteSelectImpl")
public class RouteSelectImpl extends StudioRouteInterface{
    @Autowired
    private EngineInterface engineInfo;
    @Override
    public boolean show(ResultInstanceDto resultInstanceDto, String nextNodeId) {
        // 流水号
        String bizId = resultInstanceDto.getBizId();
        // 实例号
        String instanceId = resultInstanceDto.getInstanceId();
        // 业务参数
        Map<String, Object> bizParam = resultInstanceDto.getParam();
        Integer key = (Integer) bizParam.get("money");
        // 节点配置属性信息
        NodeInfo nextNode = engineInfo.getNodeInfo(nextNodeId);
        String nodeSign = nextNode.getNodeSign();
        // 根据参数以及后续连接的节点的节点标识组合判断返回true或者false
        if(key < 100 && "百".equals(nodeSign)){
            return true;
        }
        if(key < 1000 && "千".equals(nodeSign)){
            return true;
        }
        if(key < 10000 && "万".equals(nodeSign)){
            return true;
        }
        return false;
    }
    @Override
    public int getOrder() {
        return 2;
    }
    @Override
    public String desc() {
        return "金额等级判断条件";
    }
    @Override
    public String key() {
        return "RouteSelectImpl";
    }
    @Override
    public String orgId() {
        return null;
    }
}
