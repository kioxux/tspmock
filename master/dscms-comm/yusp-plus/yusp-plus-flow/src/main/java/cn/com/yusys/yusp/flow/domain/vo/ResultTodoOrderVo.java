package cn.com.yusys.yusp.flow.domain.vo;

public class ResultTodoOrderVo {
    private String instanceId;
    private String bizType;
    private String userId;
    private Integer todoOrder;
    private String urgentType;

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getTodoOrder() {
        return todoOrder;
    }

    public void setTodoOrder(Integer todoOrder) {
        this.todoOrder = todoOrder;
    }

    public String getUrgentType() {
        return urgentType;
    }

    public void setUrgentType(String urgentType) {
        this.urgentType = urgentType;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    @Override
    public String toString() {
        return "ResultTodoOrderVo{" +
                "instanceId='" + instanceId + '\'' +
                ", bizType='" + bizType + '\'' +
                ", userId='" + userId + '\'' +
                ", todoOrder=" + todoOrder +
                ", urgentType='" + urgentType + '\'' +
                '}';
    }
}
