package cn.com.yusys.yusp.flow.demo;

import cn.com.yusys.yusp.flow.client.ClientBizFactory;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

public class BizMessageListener {
    private final Logger log = LoggerFactory.getLogger(BizMessageListener.class);
    
    @Autowired
    private ClientBizFactory clientBizFactory;
    @RabbitListener(queuesToDeclare = @Queue("yusp-flow.demo"))// 队列名称，多个时以逗号隔开；队列名称为【yusp-flow.流程申请类型】，队列在mq中手动添加
    @RabbitHandler
    public void receiveQueue(ResultInstanceDto message){
    	log.info("客户端业务监听:{}",message);
    	clientBizFactory.processBiz(message);
    }
}