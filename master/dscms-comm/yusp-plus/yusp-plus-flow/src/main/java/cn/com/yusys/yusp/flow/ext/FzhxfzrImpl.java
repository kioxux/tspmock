package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.flow.dto.WFUserDto;
import cn.com.yusys.yusp.flow.ext.StudioUserInterface;
import cn.com.yusys.yusp.flow.service.CmisBizClientService2;
import cn.com.yusys.yusp.flow.service.core.WorkflowOrgInterface;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("FzhxfzrImpl")
public class FzhxfzrImpl  extends StudioUserInterface{
    /**
     * 用户信息获取服务
     */
    private static final Log log = LogFactory.getLog(SameOrgUserImpl.class);

    @Autowired
    private CmisBizClientService2 cmisBizClientService;

    @Override
    public int getOrder() {
        return 6;
    }

    @Override
    public String desc() {
        return "小微分中心负责人";
    }

    @Override
    public String key() {
        return "FzhxfzrImpl";
    }

    @Override
    public String orgId() {
        return null;
    }

    @Override
    public List<String> customUser(String instanceId, String orgId, String systemId) {
        List<String> result = new ArrayList<>();

        String managerId = cmisBizClientService.getAreaUsersByOrgId(orgId);

        if(null==managerId) {
            log.error("查询分中心负责人为空！");
        }
        result.add(managerId);

        return result;
    }

}
