package cn.com.yusys.yusp.flow.controller;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.domain.NWfUserTodo;
import cn.com.yusys.yusp.flow.domain.dto.ResultInstanceTodoDto;
import cn.com.yusys.yusp.flow.dto.NWfUserTodoDto;
import cn.com.yusys.yusp.flow.dto.WFInstanceQueryDto;
import cn.com.yusys.yusp.flow.dto.result.ResultCommentDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.flow.dto.result.ResultTaskpoolDto;
import cn.com.yusys.yusp.flow.other.exception.WorkflowException;
import cn.com.yusys.yusp.flow.repository.mapper.WorkflowBenchMapper;
import cn.com.yusys.yusp.flow.repository.mapper.WorkflowBenchMapperExt;
import cn.com.yusys.yusp.flow.service.WfBenchUserTaskService;
import cn.com.yusys.yusp.flow.service.NWfInstanceService;
import cn.com.yusys.yusp.flow.service.core.WorkflowBenchInterface;
import cn.com.yusys.yusp.flow.service.core.WorkflowEngineExtInterface;
import cn.com.yusys.yusp.flow.service.core.WorkflowEngineInterface;
import cn.com.yusys.yusp.flow.util.WorkFlowUtil;
import com.github.pagehelper.PageHelper;
import oracle.ucp.proxy.annotation.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author lihh
 * @version 1.0.0
 * @date 2021/6/816:34
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
@RestController
@RequestMapping("/api/flow")
public class WfCoreResource {

    @Autowired
    private WorkflowBenchInterface workflowBenchService;

    @Autowired
    private WorkflowEngineExtInterface workflowEngineExtService;

    @Autowired
    private WorkflowEngineInterface workflowEngineService;
    @Autowired
    private NWfInstanceService nWfInstanceService;

    @Autowired
    private WorkflowBenchMapperExt workflowBenchMapper;

    @Autowired
    private WfBenchUserTaskService wfBenchUserTaskService;


    @PostMapping("/todo")
    protected ResultDto<List<ResultInstanceTodoDto>> todo(@RequestBody QueryModel queryModel) {
        queryModel.getCondition().put("userId", this.getCurrentUserId(queryModel));
        queryModel.setSort("u.start_time desc");
        return new ResultDto(this.getInstanceInfoUserTodo(queryModel));
    }

    private List<cn.com.yusys.yusp.flow.dto.result.ResultInstanceTodoDto> getInstanceInfoUserTodo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<cn.com.yusys.yusp.flow.dto.result.ResultInstanceTodoDto> list = this.workflowBenchMapper.getInstanceInfoUserTodoForOa(model);
        PageHelper.clearPage();
        return list;
    }

    @PostMapping("/getInstanceByBiz")
    public ResultDto<List<NWfUserTodoDto>> getInstanceByBiz(@RequestBody Map<String, String> param) throws WorkflowException {
        String bizId = param.get("bizId");
        List<NWfUserTodoDto> result = this.workflowEngineExtService.getInstanceByBiz(bizId);
        return new ResultDto(result);
    }

    @PostMapping("/getAllComments")
    public ResultDto<List<ResultCommentDto>> getAllComments(@RequestBody Map<String, String> param) {
        String mainInstanceId = param.get("mainInstanceId");
        String nodeId = param.get("nodeId");
        List<ResultCommentDto> comments = this.workflowEngineService.getAllComments(mainInstanceId, nodeId);
        return new ResultDto(comments);
    }

    @PostMapping({"/getAllCommentsByBizId"})
    public ResultDto<List<ResultCommentDto>> getAllCommentsByBizId(@RequestBody Map<String, String> param) {
        String bizId = param.get("bizId");
        List<ResultCommentDto> comments = this.workflowEngineService.getAllCommentsByBizId(bizId);
        return new ResultDto(comments);
    }

    @PostMapping("/instanceInfo")
    public ResultDto<ResultInstanceDto> instanceInfo(@RequestBody Map<String, String> param) {
        String instanceId = param.get("instanceId");
        String nodeId = param.get("nodeId");
        String userId = param.get("userId");
        ResultInstanceDto instanceInfo = this.workflowEngineService.getInstanceInfo(instanceId, nodeId, userId);
        return new ResultDto(instanceInfo);
    }

    @PostMapping("/deleteByBizId")
    public ResultDto<ResultMessageDto> deleteByBizId(@RequestBody Map<String, String> param) {
        String bizId = param.get("bizId");
        ResultMessageDto ms = this.workflowEngineExtService.deleteByBizId(bizId);
        return new ResultDto(ms);
    }

    @PostMapping("/userTaskPoolByModel")
    public ResultDto<List<ResultTaskpoolDto>> userTaskPoolByModel(@RequestBody QueryModel queryModel) {
        String userId = (String) queryModel.getCondition().put("userId", getCurrentUserId(queryModel));
        List<ResultTaskpoolDto> userTaskPoolByModel = wfBenchUserTaskService.getUserTaskPoolByModel(userId, queryModel);
        return ResultDto.success(userTaskPoolByModel);
    }

    @PostMapping({"/benchTodo"})
    protected ResultDto<List<cn.com.yusys.yusp.flow.dto.result.ResultInstanceTodoDto>> benchTodo(@RequestBody QueryModel queryModel) {
        queryModel.getCondition().put("userId", this.getCurrentUserId(queryModel));
        queryModel.setSort("u.start_time desc");
        return new ResultDto(this.workflowBenchService.getInstanceInfoUserTodo(queryModel));
    }

    private String getCurrentUserId(QueryModel queryModel) {
        Object userIdT = queryModel.getCondition().get("currentUserId");
        if (null == userIdT) {
            userIdT = queryModel.getCondition().get("userId");
        }

        String userId = userIdT == null ? null : userIdT.toString();
        userId = WorkFlowUtil.getCurrentUserId(userId);
        return userId;
    }

    @PostMapping("/getInstanceInfoByInstanceId")
    public ResultDto<Map> getInstanceInfoByInstanceId(@RequestBody String instanceId) {
        NWfInstance nWfInstance= nWfInstanceService.selectByPrimaryKey(instanceId);
        String flowParam = nWfInstance.getFlowParam();
        Map flowParamMap = WorkFlowUtil.strToMap(flowParam);
        return new ResultDto(flowParamMap);
    }

    @PostMapping("/getInstanceNameByBizId")
    public ResultDto<List<String>> getInstanceNameByBizId(@RequestBody String bizId) {
        List<ResultCommentDto> comments = this.workflowEngineService.getAllCommentsByBizId(bizId);
        List<String> nodeNames = comments.stream().filter(e -> Objects.equals("O-12", e.getCommentSign())).map(e -> e.getNodeName()).collect(Collectors.toList());
        return new ResultDto(nodeNames);
    }
}
