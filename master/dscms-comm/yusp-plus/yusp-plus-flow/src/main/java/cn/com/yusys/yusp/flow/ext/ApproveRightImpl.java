package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.engine.init.EngineInterface;
import cn.com.yusys.yusp.flow.repository.mapper.NWfInstanceMapper;
import cn.com.yusys.yusp.flow.service.OcaServiceProvider;
import cn.com.yusys.yusp.flow.util.WorkFlowUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("ApproveRightImpl")
public class ApproveRightImpl extends StudioRouteInterface {

    @Autowired
    private EngineInterface engineInfo;

    @Autowired
    private OcaServiceProvider ocaServiceProvider;

    @Autowired
    private NWfInstanceMapper nWfInstanceMapper;

    @Override
    public boolean show(ResultInstanceDto resultInstanceDto, String nextNodeId) {

        NWfInstance nWfInstance = nWfInstanceMapper.selectByPrimaryKey(resultInstanceDto.getInstanceId());
        String flowParam = nWfInstance.getFlowParam();
        Map<String, Object> map = WorkFlowUtil.strToMap(flowParam);
        //同业授信的信贷管理部权限
        //1表示在权限内，0表示不在权限内
        int rightXDGL = (int) map.get("rightXDGL");
        int rightFHZ = 0;
        if (rightXDGL == 0){
            rightFHZ = (int) map.get("rightFHZ");
        }

        return false;
    }

    @Override
    public int getOrder() {
        return 4;
    }

    @Override
    public String desc() {
        return "审批权限判断";
    }

    @Override
    public String key() {
        return "ApproveRightImpl";
    }

    @Override
    public String orgId() {
        return null;
    }
}
