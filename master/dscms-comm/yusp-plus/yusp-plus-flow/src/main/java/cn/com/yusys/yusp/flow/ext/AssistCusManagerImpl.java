package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.service.WorkFlowCustomBenchService;
import cn.com.yusys.yusp.flow.util.WorkFlowUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("AssistCusManagerImpl")
public class AssistCusManagerImpl extends StudioUserInterface{

	@Autowired
	private WorkFlowCustomBenchService workFlowCustomBenchService;

	private static final Log log = LogFactory.getLog(AssistCusManagerImpl.class);

	@Override
	public int getOrder() {
		return 8;
	}

	@Override
	public String desc() {
		return "协办客户经理";
	}

	@Override
	public String key() {
		return "AssistCusManagerImpl";
	}

	@Override
	public String orgId() {
		return null;
	}

	@Override
	public List<String> customUser(String instanceId, String orgId, String systemId) {
		NWfInstance nWfInstance = workFlowCustomBenchService.queryDataInfo(instanceId);
		String flowParam = nWfInstance.getFlowParam();
		Map<String, Object> map = WorkFlowUtil.strToMap(flowParam);
		List<String> result = new ArrayList<>();
		String managerId = null;
		if (map.containsKey("managerId") && !"".equals(map.get("managerId")) && map.get("managerId") != null) {
			log.info("根据实例号【" + instanceId + "】查询协办客户经理人员，返回为【"+ String.valueOf(map.get("managerId")) +"】");
			managerId = (String) map.get("managerId");
		} else {
			throw BizException.error(null,"9999","该客户暂未维护协办客户经理，请打回至客户经理节点，联系客户经理维护分成比例录入后再审批！");
		}
		result.add(managerId);
		return result;
	}
}
