package cn.com.yusys.yusp.flow.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;

public class WfAdminSmDutyVo {

	private String dutyId;
	/**
	 * 岗位代码
	 */
	private String dutyCode;
	/**
	 * 岗位名称
	 */
	private String dutyName;
	/**
	 * 所属机构编号
	 */
	private String orgId;
	/**
	 * 备注
	 */
	private String dutyRemark;
	/**
	 * 状态：对应字典项=NORM_STS A：生效 I：失效 W：待生效
	 */
	private String dutySts;
	/**
	 * 最新变更用户
	 */
	private String lastChgUsr;
	/**
	 * 最新变更时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastChgDt;

	private String orgName;

	private String lastChgName;

	public String getDutyId() {
		return dutyId;
	}

	public void setDutyId(String dutyId) {
		this.dutyId = dutyId;
	}

	public String getDutyCode() {
		return dutyCode;
	}

	public void setDutyCode(String dutyCode) {
		this.dutyCode = dutyCode;
	}

	public String getDutyName() {
		return dutyName;
	}

	public void setDutyName(String dutyName) {
		this.dutyName = dutyName;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getDutyRemark() {
		return dutyRemark;
	}

	public void setDutyRemark(String dutyRemark) {
		this.dutyRemark = dutyRemark;
	}

	public String getDutySts() {
		return dutySts;
	}

	public void setDutySts(String dutySts) {
		this.dutySts = dutySts;
	}

	public String getLastChgUsr() {
		return lastChgUsr;
	}

	public void setLastChgUsr(String lastChgUsr) {
		this.lastChgUsr = lastChgUsr;
	}

	public Date getLastChgDt() {
		return lastChgDt;
	}

	public void setLastChgDt(Date lastChgDt) {
		this.lastChgDt = lastChgDt;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getLastChgName() {
		return lastChgName;
	}

	public void setLastChgName(String lastChgName) {
		this.lastChgName = lastChgName;
	}
}
