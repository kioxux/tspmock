package cn.com.yusys.yusp.flow.domain.dto;


import java.io.Serializable;

public class ResultMeettingDto implements Serializable {
    private String instanceId;
    private String bizUserId;
    private String bizUserName;
    private java.lang.String meetId;
    private java.lang.String flowName;
    private java.lang.String userId;
    private java.lang.String bizId;
    private java.lang.String mettingSts;
    private java.lang.String startTime;
    private java.lang.String bizType;
    private java.lang.String bizPage;
    private java.lang.String nodeId;
    private java.lang.String meetType;
    private java.lang.Integer meetOrder;
    private java.lang.String orgId;
    private java.lang.String orgName;
    private java.lang.String appOrgId;
    private java.lang.String appOrgName;
    private java.lang.String flowStarter;
    private java.lang.String flowStarterName;
    private java.lang.Integer todoNum;
    private java.lang.Integer totalNum;
    private static final long serialVersionUID = 1L;

    public ResultMeettingDto() {
    }

    public Integer getTodoNum() {
        return this.todoNum;
    }

    public void setTodoNum(int todoNum) {
        this.todoNum =  todoNum;
    }

    public Integer getTotalNum() {
        return this.totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum =  totalNum;
    }


    public String getFlowStarterName() {
        return this.flowStarterName;
    }

    public void setFlowStarterName(String flowStarterName) {
        this.flowStarterName = flowStarterName == null ? null : flowStarterName;
    }

    public String getFlowStarter() {
        return this.flowStarter;
    }

    public void setFlowStarter(String flowStarter) {
        this.flowStarter = flowStarter == null ? null : flowStarter;
    }

    public String getAppOrgName() {
        return this.appOrgName;
    }

    public void setAppOrgName(String appOrgName) {
        this.appOrgName = appOrgName == null ? null : appOrgName;
    }

    public String getAppOrgId() {
        return this.appOrgId;
    }

    public void setAppOrgId(String appOrgId) {
        this.appOrgId = appOrgId == null ? null : appOrgId;
    }

    public Integer getMeetOrder() {
        return this.meetOrder;
    }

    public void setMeetOrder(int meetOrder) {
        this.meetOrder = meetOrder;
    }

    public String getMeetType() {
        return this.meetType;
    }

    public void setMeetType(String meetType) {
        this.meetType = meetType == null ? null : meetType.trim();
    }

    public String getBizPage() {
        return this.bizPage;
    }

    public void setBizPage(String bizPage) {
        this.bizPage = bizPage == null ? null : bizPage.trim();
    }

    public String getBizType() {
        return this.bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType == null ? null : bizType.trim();
    }

    public String getMettingSts() {
        return this.mettingSts;
    }

    public void setMettingSts(String mettingSts) {
        this.mettingSts = mettingSts == null ? null : mettingSts.trim();
    }

    public String getMeetId() {
        return this.meetId;
    }

    public void setMeetId(String meetId) {
        this.meetId = meetId == null ? null : meetId.trim();
    }

    public String getBizUserId() {
        return this.bizUserId;
    }

    public void setBizUserId(String bizUserId) {
        this.bizUserId = bizUserId == null ? null : bizUserId.trim();
    }

    public String getInstanceId() {
        return this.instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId == null ? null : instanceId.trim();
    }

    public String getFlowName() {
        return this.flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName == null ? null : flowName.trim();
    }

    public String getStartTime() {
        return this.startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime == null ? null : startTime.trim();
    }

    public String getOrgId() {
        return this.orgId;
    }

    public String getOrgName() {
        return this.orgName;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName == null ? null : orgName.trim();
    }

    public String getBizId() {
        return this.bizId;
    }

    public void setBizId(String bizId) {
        this.bizId = bizId == null ? null : bizId.trim();
    }

    public String getBizUserName() {
        return this.bizUserName;
    }

    public void setBizUserName(String bizUserName) {
        this.bizUserName = bizUserName == null ? null : bizUserName.trim();
    }

    public String getNodeId() {
        return this.nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
