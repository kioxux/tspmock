/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.flow.domain.vo;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.flow.dto.NextNodeInfoDto;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

/**
 * @项目名称: yusp-plus-flow模块
 * @类名称: NWfMonitorLog
 * @类描述: n_wf_monitor_log数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-06 23:00:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class NWfMonitorLogVo implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 流程实例ID **/
	private String instanceId;
	
	/** 节点ID **/
	private String nodeId;
	
	/** 节点开始时间 **/
	private String nodeStartTime;
	
	/** 节点名称 **/
	private String nodeName;
	
	/** 节点处理人 **/
	private String userId;
	
	/** 节点处理人 **/
	private String userName;
	
	/** 操作开始时间 **/
	private String optStartTime;
	
	/** 操作结束时间 **/
	private String optEndTime;
	
	/** 操作状态 **/
	private String optStatus;
	
	/** 是否移交 **/
	private String isTurn;
	
	/** 备注 **/
	private String optRemark;


	/** 节点操作类型   01提交  02处理  03挂起  04唤醒  05移交 */
	private String nodeOptType;

	/**
	 * 移交用户id
	 */
	private String changeUserId;

	private List<NextNodeInfoDto> nextNodeInfos;


	/**
	 * @param instanceId
	 */
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	
    /**
     * @return instanceId
     */
	public String getInstanceId() {
		return this.instanceId;
	}
	
	/**
	 * @param nodeId
	 */
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	
    /**
     * @return nodeId
     */
	public String getNodeId() {
		return this.nodeId;
	}
	
	/**
	 * @param nodeStartTime
	 */
	public void setNodeStartTime(String nodeStartTime) {
		this.nodeStartTime = nodeStartTime;
	}
	
    /**
     * @return nodeStartTime
     */
	public String getNodeStartTime() {
		return this.nodeStartTime;
	}
	
	/**
	 * @param nodeName
	 */
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}
	
    /**
     * @return nodeName
     */
	public String getNodeName() {
		return this.nodeName;
	}
	
	/**
	 * @param userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
    /**
     * @return userId
     */
	public String getUserId() {
		return this.userId;
	}
	
	/**
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
    /**
     * @return userName
     */
	public String getUserName() {
		return this.userName;
	}
	
	/**
	 * @param optStartTime
	 */
	public void setOptStartTime(String optStartTime) {
		this.optStartTime = optStartTime;
	}
	
    /**
     * @return optStartTime
     */
	public String getOptStartTime() {
		return this.optStartTime;
	}
	
	/**
	 * @param optEndTime
	 */
	public void setOptEndTime(String optEndTime) {
		this.optEndTime = optEndTime;
	}
	
    /**
     * @return optEndTime
     */
	public String getOptEndTime() {
		return this.optEndTime;
	}
	
	/**
	 * @param optStatus
	 */
	public void setOptStatus(String optStatus) {
		this.optStatus = optStatus;
	}
	
    /**
     * @return optStatus
     */
	public String getOptStatus() {
		return this.optStatus;
	}
	
	/**
	 * @param isTurn
	 */
	public void setIsTurn(String isTurn) {
		this.isTurn = isTurn;
	}
	
    /**
     * @return isTurn
     */
	public String getIsTurn() {
		return this.isTurn;
	}
	
	/**
	 * @param optRemark
	 */
	public void setOptRemark(String optRemark) {
		this.optRemark = optRemark;
	}
	
    /**
     * @return optRemark
     */
	public String getOptRemark() {
		return this.optRemark;
	}


	public String getNodeOptType() {
		return nodeOptType;
	}

	public void setNodeOptType(String nodeOptType) {
		this.nodeOptType = nodeOptType;
	}

	public String getChangeUserId() {
		return changeUserId;
	}

	public void setChangeUserId(String changeUserId) {
		this.changeUserId = changeUserId;
	}

	public List<NextNodeInfoDto> getNextNodeInfos() {
		return nextNodeInfos;
	}

	public void setNextNodeInfos(List<NextNodeInfoDto> nextNodeInfos) {
		this.nextNodeInfos = nextNodeInfos;
	}
}