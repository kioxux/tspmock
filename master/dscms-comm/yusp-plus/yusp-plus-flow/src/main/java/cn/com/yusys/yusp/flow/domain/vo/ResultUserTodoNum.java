package cn.com.yusys.yusp.flow.domain.vo;

public class ResultUserTodoNum {
    private String userName;
    private String userId;
    private Integer todoNum;

    public ResultUserTodoNum(){}

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getTodoNum() {
        return todoNum;
    }

    public void setTodoNum(Integer todoNum) {
        this.todoNum = todoNum;
    }

    public String toString() {
        return "ResultUserTodoNum [userName=" + this.userName + ", userId=" + this.userId + ", todoNum=" + this.todoNum + "]";
    }
}
