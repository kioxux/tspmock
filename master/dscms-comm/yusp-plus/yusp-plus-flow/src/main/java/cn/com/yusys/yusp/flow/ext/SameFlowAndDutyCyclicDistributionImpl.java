package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.domain.*;
import cn.com.yusys.yusp.flow.domain.vo.ResultUserTodoNum;
import cn.com.yusys.yusp.flow.domain.vo.WfAdminSmUserVo;
import cn.com.yusys.yusp.flow.dto.WFUserDto;
import cn.com.yusys.yusp.flow.other.engine.NodeInfo;
import cn.com.yusys.yusp.flow.other.engine.init.EngineInterface;
import cn.com.yusys.yusp.flow.other.enums.ReDoUserSelect;
import cn.com.yusys.yusp.flow.repository.mapper.*;
import cn.com.yusys.yusp.flow.service.NWfInstanceHisService;
import cn.com.yusys.yusp.flow.service.NWfInstanceService;
import cn.com.yusys.yusp.flow.service.OcaServiceProvider;
import cn.com.yusys.yusp.flow.service.core.WorkflowOrgInterface;
import cn.com.yusys.yusp.flow.util.WorkFlowUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 同一岗位轮训分配
 * 1、获取该岗位未休假的用户并按userCode排序
 * 2、获取同流程同岗位审批的最新一条数据，获取审批人员编号
 * 3、按照排序获取下一审批人
 * <p>
 * 小微放款影像补扫也走此分配规则
 * 1、根据原业务流水获取原历史流程实例
 * 2、根据当前流程下一节点配置的岗人员位获取原流程同审批岗位名称
 * 3、根据岗位名称获取原流程最后一岗审批人员信息
 * 4、判断审批人员是否在职：
 * （1）在职且未转岗就分配给原审批人员；（2）不在职或者转岗就随机分配给同机构岗位一人
 */
@Service("SameFlowAndDutyCyclicDistributionImpl")
public class SameFlowAndDutyCyclicDistributionImpl extends StudioStrategyInterface {

    private static final Logger log = LoggerFactory.getLogger(SameFlowAndDutyCyclicDistributionImpl.class);

    Random r = new Random(1L);

    private static final String FLAG_TODO = "TODO";// 审批中
    private static final String FLAG_HIS = "HIS";// 已办结
    private static final String FLAG_NONE = "NONE";// 无流程

    @Autowired
    private WorkFlowCustomBenchMapper workFlowCustomBenchMapper;
    @Autowired
    private WorkflowBenchMapper workflowBenchMapper;
    @Autowired
    private NWfInstanceMapper instanceMapper;
    @Autowired
    private NWfNodeHisMapper nodeHisMapper;
    @Autowired
    private EngineInterface engineInfo;
    @Autowired
    private NWfUserDoneMapper userDoneService;
    @Autowired
    private WorkflowOrgInterface userService;
    @Autowired
    private NWfInstanceService nWfInstanceService;
    @Autowired
    private NWfInstanceHisService nWfInstanceHisService;
    @Autowired
    private NWfNodeDoneMapper nodeDoneMapper;
    @Autowired
    private NWfUserHisMapper userHisMapper;
    @Autowired
    private NWfUserDoneMapper userDoneMapper;
    @Autowired
    private OcaServiceProvider ocaServiceProvider;

    public List<WFUserDto> selectUser(List<WFUserDto> user, String instanceId, String nodeId, String orgId, String systemId) {
        List<WFUserDto> result = new ArrayList<>();

        // 现审批流程实例
        NWfInstance instance = nWfInstanceService.selectByPrimaryKey(instanceId);
        String bizId = instance.getBizId();
        String bizType = instance.getBizType();
        if (bizId.startsWith("DABS") || Objects.equals(bizType, "DA010")) {
            // 如果是小微放款影像补扫业务,取原放款审批流程同岗位审批人员
            // 参数信息
            Map<String, Object> map = WorkFlowUtil.strToMap(instance.getFlowParam());
            // 原业务流程实例
            String bizInstanceId = Optional.ofNullable((String) map.get("bizInstanceId")).orElse("");
            // 原业务流水号
            String serno = Optional.ofNullable((String) map.get("serno")).orElse("");
            // 下一节点岗位信息
            String nodeUser = "";
            // 原流程同岗位节点ID
            String oldNodeId = "";
            // 手工发起未存流程实例,从审批历史中获取
            if (StringUtils.isEmpty(bizInstanceId)) {
                // 根据关联业务流水号获取最近一笔历史通过审批流程实例
                QueryModel model = new QueryModel();
                model.addCondition("bizId", serno);
                model.addCondition("flowState", "E");
                model.addCondition("flowId", "296");
                model.setSort("endTime desc");
                List<NWfInstanceHis> instanceHisList = nWfInstanceHisService.selectByModel(model);
                // 原流程实例号
                if (null != instanceHisList && instanceHisList.size() > 0) {
                    NWfInstanceHis nWfInstanceHis = instanceHisList.get(0);
                    if (null != nWfInstanceHis) {
                        bizInstanceId = nWfInstanceHis.getInstanceId();
                    }
                }
            }
            // 如原流程实例和业务流水号获取的流程实例均为空,随机分配
            if (StringUtils.isNotEmpty(bizInstanceId)) {
                // 获取原流程信息
                String flag = "";
                NWfInstanceHis wfInstanceHis = nWfInstanceHisService.selectByPrimaryKey(bizInstanceId);
                NWfInstance wfInstance = nWfInstanceService.selectByPrimaryKey(bizInstanceId);
                if (null != wfInstanceHis) {
                    flag = FLAG_HIS;// 原流程已结束
                } else if (null != wfInstance) {
                    flag = FLAG_TODO;// 原流程审批中
                } else {
                    flag = FLAG_NONE;// 未获取到原流程信息
                }

                if (!FLAG_NONE.equals(flag)) {
                    // 当前流程下一节点信息 (影像补扫节点都是单岗位人员审批)
                    NodeInfo nodeInfo = engineInfo.getNodeInfo(nodeId);
                    // 下一节点岗位信息(G.xxx;)
                    nodeUser = nodeInfo.getNodeUser();
                    // 根据原流程实例获取流程审批节点信息
                    QueryModel model1 = new QueryModel();
                    model1.addCondition("instanceId", bizInstanceId);
                    model1.setSort("endTime desc");
                    if (FLAG_HIS.equals(flag)) {
                        List<NWfNodeHis> nodeHisList = nodeHisMapper.selectByModel(model1);
                        if (null != nodeHisList && nodeHisList.size() > 0) {
                            for (NWfNodeHis his : nodeHisList) {
                                NodeInfo hisNodeInfo = engineInfo.getNodeInfo(his.getNodeId());
                                String hisNodeUser = hisNodeInfo.getNodeUser();
                                // 选取同岗位人员返回
                                if (hisNodeUser.contains(nodeUser)) {
                                    oldNodeId = his.getNodeId();
                                    break;
                                }
                            }
                        }
                    } else {
                        List<NWfNodeDone> nodeDoneList = nodeDoneMapper.selectByModel(model1);
                        if (null != nodeDoneList && nodeDoneList.size() > 0) {
                            for (NWfNodeDone nodeDone : nodeDoneList) {
                                NodeInfo nodeInfo1 = engineInfo.getNodeInfo(nodeDone.getNodeId());
                                String nodeUser1 = nodeInfo1.getNodeUser();
                                // 选取同岗位人员返回
                                if (nodeUser1.contains(nodeUser)) {
                                    oldNodeId = nodeDone.getNodeId();
                                    break;
                                }
                            }
                        }
                    }
                    // 根据原流程实例和节点ID获取人员办理信息
                    if (StringUtils.isNotEmpty(oldNodeId)) {
                        QueryModel model2 = new QueryModel();
                        model2.addCondition("instanceId", bizInstanceId);
                        model2.addCondition("nodeId", oldNodeId);
                        model2.setSort("endTime desc");
                        if (FLAG_HIS.equals(flag)) {
                            List<NWfUserHis> userHisList = userHisMapper.selectByModel(model2);
                            // 原业务流程同岗位人员
                            if (null != userHisList && userHisList.size() > 0) {
                                tag1:
                                for (NWfUserHis userHis : userHisList) {
                                    String oldUserId = userHis.getUserId();
                                    // 根据用户编号和用户状态生效(A)获取用户信息
                                    WfAdminSmUserVo userInfoForWf = ocaServiceProvider.getUserInfoForWf(systemId, oldUserId);
                                    if (null != userInfoForWf) {
                                        // 用户存在且状态为生效
                                        // 获取用户所有岗位
                                        List<String> dutyList = ocaServiceProvider.getDutyCodeByUserId(oldUserId);
                                        boolean dutyFlag = false;// 原岗标识
                                        if (null != dutyList && dutyList.size() > 0) {
                                            tag2:
                                            for (String duty : dutyList) {
                                                if (nodeUser.contains(duty)) {
                                                    dutyFlag = true;
                                                    break tag2;
                                                }
                                            }
                                        }
                                        if (dutyFlag) {
                                            // 原用户未转岗
                                            String userName = userInfoForWf.getUserName();
                                            WFUserDto userDto = new WFUserDto();
                                            userDto.setUserId(oldUserId);
                                            userDto.setUserName(userName);
                                            result.add(userDto);
                                            log.info("影像补扫自动分配规则 => 现流程实例：" + instanceId + "，原流程实例：" + bizInstanceId + "，岗位：" + nodeUser + "" +
                                                    "，原节点：" + oldNodeId + "，原岗位审批人：" + oldUserId + "-" + userName);
                                            break tag1;
                                        }
                                    }
                                }
                            }
                        } else {
                            List<NWfUserDone> userDoneList = userDoneMapper.selectByModel(model2);
                            // 原业务流程同岗位人员
                            if (null != userDoneList && userDoneList.size() > 0) {
                                tag1:
                                for (NWfUserDone userDone : userDoneList) {
                                    String oldUserId = userDone.getUserId();
                                    // 根据用户编号和用户状态生效(A)获取用户信息
                                    WfAdminSmUserVo userInfoForWf = ocaServiceProvider.getUserInfoForWf(systemId, oldUserId);
                                    if (null != userInfoForWf) {
                                        // 用户存在且状态为生效
                                        // 获取用户所有岗位
                                        List<String> dutyList = ocaServiceProvider.getDutyCodeByUserId(oldUserId);
                                        boolean dutyFlag = false;// 原岗标识
                                        if (null != dutyList && dutyList.size() > 0) {
                                            tag2:
                                            for (String duty : dutyList) {
                                                if (nodeUser.contains(duty)) {
                                                    dutyFlag = true;
                                                    break tag2;
                                                }
                                            }
                                        }
                                        if (dutyFlag) {
                                            // 原用户未转岗
                                            String userName = userInfoForWf.getUserName();
                                            WFUserDto userDto = new WFUserDto();
                                            userDto.setUserId(oldUserId);
                                            userDto.setUserName(userName);
                                            result.add(userDto);
                                            log.info("影像补扫自动分配规则 => 现流程实例：" + instanceId + "，原流程实例：" + bizInstanceId + "，岗位：" + nodeUser + "" +
                                                    "，原节点：" + oldNodeId + "，原岗位审批人：" + oldUserId + "-" + userName);
                                            break tag1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (result.size() < 1) {
                // 随机分配一人
                if (null != user && !user.isEmpty()) {
                    WFUserDto userDto = (WFUserDto) user.get(this.r.nextInt(user.size()));
                    result.add(userDto);
                    log.info("影像补扫自动分配规则 => 现流程实例：" + instanceId + "，原流程实例：" + bizInstanceId + "，岗位：" + nodeUser + "" +
                            "，随机分配审批人：" + userDto.getUserId() + "-" + userDto.getUserName());
                } else {
                    log.info("影像补扫自动分配规则 => 现流程实例：" + instanceId + "，原流程实例：" + bizInstanceId + "，岗位：" + nodeUser + "" +
                            "，随机分配审批人为空");
                }
            }
        } else {
            // 如果是小微放款业务,走原分配规则
            if (user.size() > 0) {
                // 1、首先判断是否为打回再提交且重办人员为上次办理人，若是则直接返回；若否则走分配规则
                NodeInfo nodeInfo = this.engineInfo.getNodeInfo(nodeId);
                String nodeUser = nodeInfo.getNodeUser(); // 节点配置的办理人，岗位信息(G.xxx;)
                String dutyId;
                if (StringUtils.isNotEmpty(nodeUser)) {
                    dutyId = nodeUser.replace("G.", "").replace(";", "");
                } else {
                    throw BizException.error(null, "999999", "同岗位轮询分配规则为获取到岗位信息！流程实例：{}", instanceId);
                }
                String reDoUserSelect = nodeInfo.getReDoUserSelect();
                if (ReDoUserSelect.HIS_USER.equals(reDoUserSelect)) {
                    QueryModel model = new QueryModel();
                    model.getCondition().put("instanceId", instanceId);
                    model.getCondition().put("nodeId", nodeId);
                    model.setSort("start_time desc");
                    List<NWfUserDone> usersT = this.userDoneService.selectByModel(model);
                    for (NWfUserDone userDone : usersT) {
                        WFUserDto userDto = this.userService.getUserInfo(systemId, userDone.getUserId());
                        if (null != userDto) {
                            result.add(userDto);
                            break;
                        }
                    }
                    if (!CollectionUtils.isEmpty(result)) {
                        return result;
                    }
                }

                //1、获取该岗位未休假的用户并按userCode排序
                List<ResultUserTodoNum> userDutyList = new ArrayList<>();
                String currDateStr = (new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA)).format(new Date());
                userDutyList = workFlowCustomBenchMapper.getDutyUserNoLeave(dutyId, currDateStr);
                if (CollectionUtils.isEmpty(userDutyList)) {
                    throw BizException.error(null, "999999", "同岗位轮询分配规则未筛选到符合条件人员！流程实例：{}", instanceId);
                }
                // 3、获取同流程同岗位审批的最新一条数据，获取审批人员编号
                String lastuser = this.getLastFlowUser(instanceId, nodeId);
                //如果同流程同岗位审批的最新一条数据为空，则取第一条
                if (StringUtils.isEmpty(lastuser) && !CollectionUtils.isEmpty(userDutyList)) {
                    WFUserDto userDto = this.userService.getUserInfo(systemId, userDutyList.get(0).getUserId());
                    if (null != userDto) {
                        result.add(userDto);
                    }
                    return result;
                } else if (!StringUtils.isEmpty(lastuser) && !CollectionUtils.isEmpty(userDutyList)) {
                    String[] userDuty = new String[userDutyList.size()];
                    int index = -1;
                    for (int i = 0; i < userDutyList.size(); i++) {
                        userDuty[i] = userDutyList.get(i).getUserId();
                        if (lastuser.equals(userDutyList.get(i).getUserId())) {
                            index = i;
                        }
                    }
                    WFUserDto userDto;
                    if (index == -1) {//最后一个审批人不在该笔流程分配的人员中
                        //最后一个审批人，审批完后，刚好请假了，不考虑吧，取可分配人员的第一个
                        userDto = this.userService.getUserInfo(systemId, userDuty[0]);
                    } else if (userDuty.length - index == 1) {
                        //最后一个审批人为可分配人员的最后一位，此时取可分配人员的第一个
                        userDto = this.userService.getUserInfo(systemId, userDuty[0]);
                    } else {
                        userDto = this.userService.getUserInfo(systemId, userDuty[index + 1]);
                    }

                    if (null != userDto) {
                        result.add(userDto);
                    }
                    return result;
                }

            } else {
                throw BizException.error(null, "999999", "同岗位轮询分配规则未筛选到符合条件人员！流程实例：{}", instanceId);
            }
        }
        return result;
    }

    /**
     * 获取前一个审批通过的流程，通流程同节点的办理人
     *
     * @return
     */
    private String getLastFlowUser(String instanceId, String nodeId) {
        // 根据nodeId查询最近一笔审批的节点信息，可以是待办、已办为结束，已办已结束
        QueryModel model = new QueryModel();
        model.addCondition("NodeId", nodeId);
        model.setSort("endTime desc");
        List<NWfUserDone> NWfUserDoneList = workFlowCustomBenchMapper.selectNWfUserDoneByNodeId(nodeId);
        if (!CollectionUtils.isEmpty(NWfUserDoneList)) {
            return NWfUserDoneList.get(0).getUserId();
        } else {
            return "";
        }
    }

    @Override
    public int getOrder() {
        return 3;
    }

    @Override
    public String desc() {
        return "同岗位轮询分配规则";
    }

    @Override
    public String key() {
        return "SameFlowAndDutyCyclicDistributionImpl";
    }

    @Override
    public String orgId() {
        return null;
    }
}
