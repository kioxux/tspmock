package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.engine.NodeInfo;
import cn.com.yusys.yusp.flow.other.engine.init.EngineInterface;
import cn.com.yusys.yusp.flow.service.OcaServiceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service("BankOrgJudgeImpl")
public class BankOrgJudgeImpl extends StudioRouteInterface {

    @Autowired
    private EngineInterface engineInfo;

    @Autowired
    private OcaServiceProvider ocaServiceProvider;

    @Override
    public boolean show(ResultInstanceDto resultInstanceDto, String nextNodeId) {
        String orgId = resultInstanceDto.getOrgId();
        //获取上一节点的机构类型
        String orgType = ocaServiceProvider.getParentOrg(orgId);

        // 节点配置属性信息
        NodeInfo nextNode = engineInfo.getNodeInfo(nextNodeId);
        String nodeSign = nextNode.getNodeSign();

        if(orgType.equals("6")&&nodeSign.equals("6")){
            return true;
        }
        if(orgType.equals("4")&&nodeSign.equals("4")){
            return true;
        }
        if(orgType.equals("3")&&nodeSign.equals("3")){
            return true;
        }
        if(orgType.equals("1")&&nodeSign.equals("1")){
            return true;
        }
        if(orgType.equals("2")&&nodeSign.equals("2")){
            return true;
        }
        return false;
    }

    @Override
    public int getOrder() {
        return 5;
    }

    @Override
    public String desc() {
        return "支行机构判断";
    }

    @Override
    public String key() {
        return "BankOrgJudgeImpl";
    }

    @Override
    public String orgId() {
        return null;
    }
}
