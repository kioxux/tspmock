/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.flow.service;

import java.util.Iterator;
import java.util.List;

import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.flow.domain.NWfNode;
import cn.com.yusys.yusp.flow.domain.NWfNodeDone;
import cn.com.yusys.yusp.flow.domain.NWfNodeHis;
import cn.com.yusys.yusp.flow.domain.dto.ResultInstanceTodoDto;
import cn.com.yusys.yusp.flow.domain.vo.NWfMonitorLogVo;
import cn.com.yusys.yusp.flow.dto.NextNodeInfoDto;
import cn.com.yusys.yusp.flow.other.engine.NodeInfo;
import cn.com.yusys.yusp.flow.other.engine.init.EngineInterface;
import cn.com.yusys.yusp.flow.repository.mapper.NWfNodeDoneMapper;
import cn.com.yusys.yusp.flow.repository.mapper.NWfNodeHisMapper;
import cn.com.yusys.yusp.flow.repository.mapper.NWfNodeMapper;
import cn.com.yusys.yusp.flow.service.core.WorkflowBenchInterfaceExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.domain.NWfMonitorLog;
import cn.com.yusys.yusp.flow.repository.mapper.NWfMonitorLogMapper;

/**
 * @项目名称: yusp-plus-flow模块
 * @类名称: NWfMonitorLogService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-06 23:00:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class NWfMonitorLogService {

    private static final Logger log = LoggerFactory.getLogger(NWfMonitorLogService.class);

    @Autowired
    private NWfMonitorLogMapper nWfMonitorLogMapper;
    @Autowired
    NWfNodeMapper nWfNodeMapper;
    @Autowired
    NWfNodeDoneMapper nWfNodeDoneMapper;
    @Autowired
    NWfNodeHisMapper nWfNodeHisMapper;
    @Autowired
    private WorkflowBenchInterfaceExt workflowBenchInterfaceExt;
    @Autowired
    private EngineInterface engineInfo;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public NWfMonitorLog selectByPrimaryKey(String pkId) {
        return nWfMonitorLogMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<NWfMonitorLog> selectAll(QueryModel model) {
        List<NWfMonitorLog> records = (List<NWfMonitorLog>) nWfMonitorLogMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<NWfMonitorLog> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<NWfMonitorLog> list = nWfMonitorLogMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(NWfMonitorLog record) {
        return nWfMonitorLogMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(NWfMonitorLog record) {
        return nWfMonitorLogMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(NWfMonitorLog record) {
        return nWfMonitorLogMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(NWfMonitorLog record) {
        return nWfMonitorLogMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return nWfMonitorLogMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return nWfMonitorLogMapper.deleteByIds(ids);
    }

    /**
     * 新增或更新节点监控数据
     * 1、任务状态：1：处理中 2：处理结束 3、挂起中 4、挂起结束 (PS:不记录排队的时间，因为节点总的处理时间减去处理与挂起时间即为排队时间)
     * 2、用户在待办非空的情况下，会始终有一条在处理中
     * 3、流程实例ID，节点ID，节点开始时间、用户码   视为联合主键，限定用户在某一流程某一节点某次审批（同一节点可能处理多次）的操作记录
     * 4、逻辑中有无待办要注意是否含本次操作的节点，要根据埋点时机在提交前还是提交后统一约定
     * @param nWfMonitorLogVo
     */
    @Async
    public void loggingSave(NWfMonitorLogVo nWfMonitorLogVo){
        try {
            String nodeOptType = nWfMonitorLogVo.getNodeOptType();
            String instanceId = nWfMonitorLogVo.getInstanceId();
            String nodeId = nWfMonitorLogVo.getNodeId();
            String userId = nWfMonitorLogVo.getUserId();
            String currentDateTime = DateUtils.getCurrDateTimeStr(); // 当前时间

            if("01".equals(nodeOptType)){ // 提交
                // 一、节点提交（泛指流程节点流转，含同意、打回、否决）操作
                // 1 当前节点、当前处理人、本次节点操作（用节点开始时间区分；）记录更新
                // 1.1 根据流程实例ID、节点、处理人、节点开始时间 更新记录状态【2处理结束】及结束时间【当前时间】
                NWfMonitorLog logQueryModel = new NWfMonitorLog();
                logQueryModel.setUserId(userId);
                logQueryModel.setOptStatus("1");
                logQueryModel.setInstanceId(instanceId);
                logQueryModel.setNodeId(nodeId);
                List<NWfMonitorLog> logList = this.selectAll(logQueryModel);
                String nodeEndTime = this.getNodeEndTime(instanceId, nodeId, nWfMonitorLogVo.getNodeStartTime());
                if(CollectionUtils.nonEmpty(logList)){
                    for (NWfMonitorLog oldNWfMonitorLog:logList) {
                        oldNWfMonitorLog.setOptStatus("2");
                        if(StringUtils.nonBlank(nodeEndTime)){
                            oldNWfMonitorLog.setOptEndTime(nodeEndTime); // 节点结束时间
                        }else{
                            oldNWfMonitorLog.setOptEndTime(currentDateTime); // 当前时间
                        }
                        this.update(oldNWfMonitorLog);
                    }
                } else{
                    log.error("用户【{}】提交流程【{}】节点【{}】时，未查询到其对应的【办理中】日志！", userId, instanceId, nodeId);
                }
                // 1.2 用户下一条待办数据自动添加【1办理中】日志
                this.autoLogWfNextTodo(userId);

                // 2 下一节点、下一处理人、本次节点操作（用节点开始时间区分；）记录更新 PS:如果是池模式，按挂起处理
                List<NextNodeInfoDto> nextNodeInfoDtos = nWfMonitorLogVo.getNextNodeInfos();
                if (null != nextNodeInfoDtos && !nextNodeInfoDtos.isEmpty()){
                    Iterator nextNodeIt = nextNodeInfoDtos.iterator();
                    while(nextNodeIt.hasNext()) {
                        NextNodeInfoDto nextNodeDto = (NextNodeInfoDto)nextNodeIt.next();
                        // 2.1 判断是否结束节点，
                        String nextNodeId = nextNodeDto.getNextNodeId();
                        NodeInfo nodeInfoT = this.engineInfo.getNodeInfo(nextNodeId);
                        String nodeUser = nodeInfoT.getNodeUser();
                        String nodeType = nodeInfoT.getNodeType();
                        if (nextNodeId.contains("E") || "E".equals(nodeType)) {
                            // 2.1.1 是：将所有关于此流程的办理中数据全部结束掉
                            NWfMonitorLog queryModel = new NWfMonitorLog();
                            queryModel.setInstanceId(instanceId);
                            queryModel.setOptStatus("1");
                            List<NWfMonitorLog> list = this.selectAll(queryModel);
                            if(CollectionUtils.nonEmpty(list)){
                                for (NWfMonitorLog oldNWfMonitorLog : list) {
                                    oldNWfMonitorLog.setOptStatus("2");
                                    oldNWfMonitorLog.setOptEndTime(currentDateTime);
                                    this.update(oldNWfMonitorLog);
                                }
                            }
                        }else{
                            List<String> nextUserIds = nextNodeDto.getNextNodeUserIds();
                            if(CollectionUtils.nonEmpty(nextUserIds)){
                                for (String nextUserId:nextUserIds) {
                                    if("system_user".equals(nextUserId)){ // 2.1.3 判断下一节点为系统指定处理人system_user
                                        // 获取系统指定的办理人
                                        QueryModel queryModel = new QueryModel();
                                        queryModel.addCondition("instanceId", instanceId);
                                        queryModel.addCondition("nodeId", nodeId);
                                        queryModel.addCondition("flowState", "R");
                                        queryModel.setPage(1);
                                        queryModel.setSize(5);
                                        List<ResultInstanceTodoDto> todoList = this.workflowBenchInterfaceExt.getStarterDoingInstance(queryModel);
//                                        List<ResultInstanceTodoDto> todoList = this.workflowBenchInterfaceExt.getInstanceInfoUserTodo(queryModel);
                                        if(CollectionUtils.nonEmpty(todoList)){
                                            String nextUserIdBySys =  todoList.get(0).getUserId();
                                            if(nextUserIdBySys.startsWith("T.")){ // 池签收模式
                                                // 执行【挂起】 操作
                                                //此流程所有相关日志更新 【1处理中】》》【2处理结束】及结束时间【当前时间】
                                                NWfMonitorLog logqueryModel = new NWfMonitorLog();
                                                logqueryModel.setInstanceId(instanceId);
                                                logqueryModel.setOptStatus("1");
                                                List<NWfMonitorLog> list = this.selectAll(queryModel);
                                                if(CollectionUtils.nonEmpty(list)){
                                                    for (NWfMonitorLog oldNWfMonitorLog : list) {
                                                        oldNWfMonitorLog.setOptStatus("2");
                                                        oldNWfMonitorLog.setOptEndTime(currentDateTime);
                                                        this.update(oldNWfMonitorLog);
                                                    }
                                                }
                                                // 添加【3挂起中】日志（PS: 认领后会将其更新为【4挂起结束】）
                                                NWfMonitorLogVo newNWfMonitorLogVo = new NWfMonitorLogVo();
                                                BeanUtils.beanCopy(nWfMonitorLogVo, newNWfMonitorLogVo);
                                                newNWfMonitorLogVo.setUserId(nextUserIdBySys);
                                                newNWfMonitorLogVo.setNodeId(nextNodeId);
                                                newNWfMonitorLogVo.setOptRemark("项目池中等待认领");
                                                this.insertLog(newNWfMonitorLogVo,"3", false);

                                            }else{ // 自动分配模式
                                                this.autoLogWfNextTodo(nextUserIdBySys);
                                            }
                                        }else{
                                            log.error("用户【{}】提交流程【{}】节点【{}】时，未查询系统指定的办理人！", userId, instanceId, nodeId, nextNodeId);
                                        }
                                    }else{
                                        this.autoLogWfNextTodo(nextUserId);
                                    }
                                }
                            }else{
                                log.error("用户【{}】提交流程【{}】节点【{}】时，未查询下一节点【{}】对应的办理人！", userId, instanceId, nodeId, nextNodeId);
                            }
                        }
                    }
                }else{
                    log.error("用户【{}】提交流程【{}】节点【{}】时，未获取提交的下一节点信息！", userId, instanceId, nodeId);
                }
            }else if("02".equals(nodeOptType)){//处理
                // 二、节点处理操作（交互方式待定，点击按钮或进入审批详情页）
                // 1 判断用户是否有【1处理中】日志
                NWfMonitorLog queryModel = new NWfMonitorLog();
                queryModel.setUserId(userId);
                queryModel.setOptStatus("1");
                List<NWfMonitorLog> list = this.selectAll(queryModel);
                // 1.1 有：判断是否就是当前这笔待办
                if (CollectionUtils.nonEmpty(list)) {
                    if(list.size() > 1){
                        log.error("用户{}的【处理中】日志存在多条记录，将仅更新第一条数据！并将其他记录强制更新为【处理结束】", userId);
                        for (int i = 1; i < list.size(); i++) {
                            // 更新多余记录【1处理中】》》【2处理结束】及结束时间【当前时间】
                            NWfMonitorLog oldnWfMonitorLog = list.get(0);
                            oldnWfMonitorLog.setOptStatus("2");
                            oldnWfMonitorLog.setOptEndTime(currentDateTime);
                            this.update(oldnWfMonitorLog);
                        }
                    }
                    // 1.1 有：判断是否就是当前这笔待办
                    if (instanceId.equals(list.get(0).getInstanceId())){
                        // 1.1.1 是：不做任何处理
                    }else{
                        // 1.1.2 否：
                        // 1.1.2.1 更新已存在记录【1处理中】》》【2处理结束】及结束时间【当前时间】
                        NWfMonitorLog oldnWfMonitorLog = list.get(0);
                        oldnWfMonitorLog.setOptStatus("2");
                        oldnWfMonitorLog.setOptEndTime(currentDateTime);
                        this.update(oldnWfMonitorLog);
                        // 1.1.2.2 新增日志记录，添加【1处理中】日志
                        this.insertLog(nWfMonitorLogVo, "1", false);
                    }
                }else{
                    // 1.2 无（此场景较少）：新增日志记录，添加【1处理中】日志
                    this.insertLog(nWfMonitorLogVo, "1", false);
                }
            }else if("03".equals(nodeOptType)){//挂起
                // 三、节点挂起操作
                // 1 判断用户是否有【1处理中】日志
                NWfMonitorLog queryModel = new NWfMonitorLog();
                queryModel.setUserId(userId);
                queryModel.setOptStatus("1");
                List<NWfMonitorLog> list = this.selectAll(queryModel);
                // 1.1 有：判断是否就是当前这笔待办
                if (CollectionUtils.nonEmpty(list)) {
                    if(list.size() > 1){
                        log.error("用户{}的【处理中】日志存在多条记录，将仅更新第一条数据！并将其他记录强制更新为【处理结束】", userId);
                        for (int i = 1; i < list.size(); i++) {
                            // 更新多余记录【1处理中】》》【2处理结束】及结束时间【当前时间】
                            NWfMonitorLog oldnWfMonitorLog = list.get(i);
                            oldnWfMonitorLog.setOptStatus("2");
                            oldnWfMonitorLog.setOptEndTime(currentDateTime);
                            this.update(oldnWfMonitorLog);
                        }
                    }
                    // 1.1 有：判断是否就是当前这笔待办
                    if (instanceId.equals(list.get(0).getInstanceId())){
                        // 1.1.1 是：
                        // 1.1.1.1 更新记录【1处理中】》》【2处理结束】及结束时间【当前时间】
                        NWfMonitorLog oldnWfMonitorLog = list.get(0);
                        oldnWfMonitorLog.setOptStatus("2");
                        oldnWfMonitorLog.setOptEndTime(currentDateTime);
                        this.update(oldnWfMonitorLog);
                        // 1.1.1.2 新增日志记录，添加【3挂起中】日志
                        this.insertLog(nWfMonitorLogVo, "3", false);
                        // 1.1.1.3 用户下一条待办数据自动添加【1办理中】日志
                        this.autoLogWfNextTodo(userId);
                    }else{
                        // 1.1.2 否：
                        // 1.1.2.2 新增日志记录，添加【3挂起中】日志
                        this.insertLog(nWfMonitorLogVo, "3", false);
                    }
                }else{
                    // 1.2 无（此场景较少）：新增日志记录，添加【3挂起中】日志
                    this.insertLog(nWfMonitorLogVo, "3", false);
                }

            }else if("04".equals(nodeOptType)){//唤醒
                // 四、节点唤醒操作
                // 1 更新日志记录，更新为【3挂起中】》》》【4挂起结束】
                NWfMonitorLog queryModel = new NWfMonitorLog();
                queryModel.setInstanceId(instanceId);
                queryModel.setNodeId(nodeId);
//             queryModel.setUserId(userId);
                queryModel.setOptStatus("3");
                List<NWfMonitorLog> list = this.selectAll(queryModel);
                if(CollectionUtils.nonEmpty(list)){
                    NWfMonitorLog oldNWfMonitorLog = list.get(0);
                    oldNWfMonitorLog.setOptStatus("4");
                    oldNWfMonitorLog.setOptEndTime(currentDateTime);
                    this.update(oldNWfMonitorLog);
                }else{
                    log.error("流程唤醒未找到对应的【挂起中】监控日志记录！");
                }
                // 2 用户下一条待办数据自动添加【1办理中】日志
                this.autoLogWfNextTodo(userId);
            }else if("05".equals(nodeOptType)){//移交
                // 五、节点移交操作  是否移交字段是否用到？ TODO
                // 1 原用户此待办是否【1处理中】
                NWfMonitorLog logQueryModel = new NWfMonitorLog();
                logQueryModel.setUserId(userId);
                logQueryModel.setOptStatus("1");
                logQueryModel.setInstanceId(instanceId);
                logQueryModel.setNodeId(nodeId);
                List<NWfMonitorLog> logList = this.selectAll(logQueryModel);
                if(CollectionUtils.nonEmpty(logList)){
                    // 1.1 是：更新记录【1处理中】》》【2处理结束】及结束时间【当前时间】
                    for (NWfMonitorLog oldNWfMonitorLog:logList) {
                        oldNWfMonitorLog.setOptStatus("2");
                        oldNWfMonitorLog.setOptEndTime(currentDateTime);
                        this.update(oldNWfMonitorLog);
                    }
                } else{
                    // 1.2 否：无任何操作
                }
                // 2 移交用户，是否有【1处理中】
                NWfMonitorLog log2QueryModel = new NWfMonitorLog();
                log2QueryModel.setUserId(nWfMonitorLogVo.getChangeUserId());
                log2QueryModel.setOptStatus("1");
                List<NWfMonitorLog> log2List = this.selectAll(log2QueryModel);
                if(CollectionUtils.nonEmpty(log2List)){
                    // 2.1 是：无任何操作
                }else{
                    // 2.2 否：用户下一条待办数据自动添加【1办理中】日志
                    this.autoLogWfNextTodo(nWfMonitorLogVo.getChangeUserId());
                }
            }else if("06".equals(nodeOptType)){ // 签收即认领
                // 六、节点签收操作
                // 1 将此流程相关日志记录，更新为【3挂起中】》》》【4挂起结束】
                NWfMonitorLog logQueryModel = new NWfMonitorLog();
                logQueryModel.setInstanceId(instanceId);
                logQueryModel.setNodeId(nodeId);
                logQueryModel.setOptStatus("3");
                List<NWfMonitorLog> logList = this.selectAll(logQueryModel);
                if(CollectionUtils.nonEmpty(logList)){
                    for (NWfMonitorLog nWfMonitorLog: logList) {
                        nWfMonitorLog.setOptStatus("4");
                        nWfMonitorLog.setOptEndTime(currentDateTime);
                        this.update(nWfMonitorLog);
                    }
                }
                // 2 签收人员日志更新
                this.autoLogWfNextTodo(userId);
            }
        }catch (Exception e){
            // 将异常吞掉，避免影响主流程
            log.error("添加流程监控日志时发生异常，异常原因：{}", e.getMessage());
        }

    }

    /**
     * 用户若无【1办理中】日志，则用户下一条待办数据自动添加【1办理中】日志
     * @param userId
     * @return
     */
    private void autoLogWfNextTodo(String userId){
        // 1 判断用户是否有【1处理中】日志
        NWfMonitorLog logQueryModel = new NWfMonitorLog();
        logQueryModel.setUserId(userId);
        logQueryModel.setOptStatus("1");
        List<NWfMonitorLog> logList = this.selectAll(logQueryModel);
        if(CollectionUtils.nonEmpty(logList)){
            // 1.1 有：不做任何操作
        }else{
            // 1.2 否：下一待办自动转【1办理中】
            // 1.2.1 获取下一待办数据
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("userId", userId);
            queryModel.addCondition("flowState", "R");
            queryModel.setSort("urgent_type, urgent_date, u.start_time asc");
            queryModel.setPage(1);
            queryModel.setSize(5);
            List<ResultInstanceTodoDto> todoList = this.workflowBenchInterfaceExt.getInstanceInfoUserTodo(queryModel);
            if(CollectionUtils.nonEmpty(todoList)){
                ResultInstanceTodoDto resultInstanceTodoDto = todoList.get(0);
                // 1.2.2 待办自动转为【1处理中】
                NWfMonitorLogVo nWfMonitorLogVo = new NWfMonitorLogVo();
                BeanUtils.beanCopy(resultInstanceTodoDto, nWfMonitorLogVo);
                nWfMonitorLogVo.setNodeStartTime(resultInstanceTodoDto.getStartTime());

                this.insertLog(nWfMonitorLogVo, "1", true);
            }
        }
    }




    @Transactional(readOnly=true)
    public List<NWfMonitorLog> selectAll(NWfMonitorLog nWfMonitorLog) {
        QueryModel model = new QueryModel();
        model.addCondition("instanceId", nWfMonitorLog.getInstanceId());
        model.addCondition("nodeId", nWfMonitorLog.getNodeId());
        model.addCondition("nodeStartTime", nWfMonitorLog.getNodeStartTime());
        model.addCondition("userId", nWfMonitorLog.getUserId());
        model.addCondition("optStatus", nWfMonitorLog.getOptStatus());
        List<NWfMonitorLog> records = (List<NWfMonitorLog>) nWfMonitorLogMapper.selectByModel(model);
        return records;
    }

    /**
     *
     * @param nWfMonitorLogVo
     * @param isNoPd  是否无排队直接进入处理中
     * @return
     */
    private int insertLog(NWfMonitorLogVo nWfMonitorLogVo, String optStatus, boolean isNoPd){
        NWfMonitorLog newnWfMonitorLog = new NWfMonitorLog();
        BeanUtils.beanCopy(nWfMonitorLogVo, newnWfMonitorLog);
        //获取节点相关信息
        if(isNoPd || StringUtils.isBlank(newnWfMonitorLog.getNodeName()) || StringUtils.isBlank(newnWfMonitorLog.getNodeStartTime())){
            NWfNode nWfNode = this.queryNWfNodeInfo(newnWfMonitorLog.getInstanceId(), newnWfMonitorLog.getNodeId(), null);
            newnWfMonitorLog.setNodeStartTime(nWfNode.getStartTime());
            newnWfMonitorLog.setNodeName(nWfNode.getNodeName());
            // 排队转处理中的，时间为当前时间，若为默认直接进入处理中的  时间为节点开始时间
            newnWfMonitorLog.setOptStartTime(nWfNode.getStartTime());
        }
        if(!isNoPd){
            newnWfMonitorLog.setOptStartTime(DateUtils.getCurrDateTimeStr());
        }
        newnWfMonitorLog.setOptStatus(optStatus);

        return this.insert(newnWfMonitorLog);
    }

    private NWfNode queryNWfNodeInfo(String instanceId, String nodeId, String startTime){
        QueryModel model = new QueryModel();
        model.addCondition("instanceId", instanceId);
        model.addCondition("nodeId", nodeId);
        model.addCondition("startTime", startTime);
        List<NWfNode> list = nWfNodeMapper.selectByModel(model);
        if(CollectionUtils.nonEmpty(list)){
            return list.get(0);
        }else{
            return null;
        }
    }

    /**
     * 获取节点办理结束时间
     * 先从办结表取，取不到再从历史表取
     * @param instanceId
     * @param nodeId
     * @param startTime
     * @return
     */
    private String getNodeEndTime(String instanceId, String nodeId, String startTime){
        QueryModel model = new QueryModel();
        model.addCondition("instanceId", instanceId);
        model.addCondition("nodeId", nodeId);
        model.addCondition("startTime", startTime);
        model.setSort("start_time desc");
        List<NWfNodeDone> list = nWfNodeDoneMapper.selectByModel(model);
        if(CollectionUtils.nonEmpty(list)){
            return list.get(0).getEndTime();
        }else{
            List<NWfNodeHis> hisList = nWfNodeHisMapper.selectByModel(model);
            if(CollectionUtils.nonEmpty(hisList)){
                return hisList.get(0).getEndTime();
            }
        }
        return null;
    }



    /**
     * 处理某个具体人员的日志数据
     * @param nWfMonitorLogVo
     */
    private void processLogByUser(NWfMonitorLogVo nWfMonitorLogVo){
        String nodeOptType = nWfMonitorLogVo.getNodeOptType();
        String instanceId = nWfMonitorLogVo.getInstanceId();
        String userId = nWfMonitorLogVo.getUserId();
        String currentDateTime = DateUtils.getCurrDateTimeStr(); // 当前时间
        // 1 判断用户是否有【1处理中】日志
        NWfMonitorLog queryModel = new NWfMonitorLog();
        queryModel.setUserId(nWfMonitorLogVo.getUserId());
        queryModel.setOptStatus("1");
        List<NWfMonitorLog> list = this.selectAll(queryModel);
        // 1.1 有：判断是否就是当前这笔待办
        if (CollectionUtils.nonEmpty(list)) {
            if(list.size() > 1){
                log.error("用户{}的【处理中】日志存在多条记录，将仅更新第一条数据！并将其他记录强制更新为【处理结束】", userId);
                for (int i = 1; i < list.size(); i++) {
                    // 更新多余记录【1处理中】》》【2处理结束】及结束时间【当前时间】
                    NWfMonitorLog oldnWfMonitorLog = list.get(0);
                    oldnWfMonitorLog.setOptStatus("2");
                    oldnWfMonitorLog.setOptEndTime(currentDateTime);
                    this.update(oldnWfMonitorLog);
                }
            }
            // 1.1 有：判断是否就是当前这笔待办
            if("02".equals(nodeOptType)){ // 处理
                if (instanceId.equals(list.get(0).getInstanceId())){
                    // 1.1.1 是：不做任何处理
                }else{
                    // 1.1.2 否：
                    // 1.1.2.1 更新已存在记录【1处理中】》》【2处理结束】及结束时间【当前时间】
                    NWfMonitorLog oldnWfMonitorLog = list.get(0);
                    oldnWfMonitorLog.setOptStatus("2");
                    oldnWfMonitorLog.setOptEndTime(currentDateTime);
                    this.update(oldnWfMonitorLog);
                    // 1.1.2.2 新增日志记录，添加【1处理中】日志
                    this.insertLog(nWfMonitorLogVo, "1", false);
                }
            }

        }else{
            if("02".equals(nodeOptType)) { // 处理
                // 1.2 无（此场景较少）：新增日志记录，添加【1处理中】日志
                this.insertLog(nWfMonitorLogVo, "1", false);
            }

        }
    }
}
