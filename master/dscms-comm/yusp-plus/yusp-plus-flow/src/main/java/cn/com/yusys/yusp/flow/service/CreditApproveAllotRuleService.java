/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.flow.service;

import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.PageHelper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.domain.CreditApproveAllotRule;
import cn.com.yusys.yusp.flow.repository.mapper.CreditApproveAllotRuleMapper;

/**
 * @项目名称: yusp-plus-flow模块
 * @类名称: CreditApproveAllotRuleService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-08 21:20:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CreditApproveAllotRuleService {

    @Autowired
    private CreditApproveAllotRuleMapper creditApproveAllotRuleMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CreditApproveAllotRule selectByPrimaryKey(String ruleType, String brId, String reviewId) {
        return creditApproveAllotRuleMapper.selectByPrimaryKey(ruleType, brId, reviewId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CreditApproveAllotRule> selectAll(QueryModel model) {
        List<CreditApproveAllotRule> records = (List<CreditApproveAllotRule>) creditApproveAllotRuleMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CreditApproveAllotRule> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditApproveAllotRule> list = creditApproveAllotRuleMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CreditApproveAllotRule record) {
        return creditApproveAllotRuleMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CreditApproveAllotRule record) {
        return creditApproveAllotRuleMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CreditApproveAllotRule record) {
        return creditApproveAllotRuleMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CreditApproveAllotRule record) {
        return creditApproveAllotRuleMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String ruleType, String brId, String reviewId) {
        return creditApproveAllotRuleMapper.deleteByPrimaryKey(ruleType, brId, reviewId);
    }

    /**
     * @方法名称: selectAllByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CreditApproveAllotRule> selectAllByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditApproveAllotRule> list = creditApproveAllotRuleMapper.selectAllByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: saveList
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int saveList(CreditApproveAllotRule record) {
        return creditApproveAllotRuleMapper.insert(record);
    }

    /**
     * @方法名称: updateList
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateList(CreditApproveAllotRule record) {
        return creditApproveAllotRuleMapper.insert(record);
    }

    /**
     * @方法名称: deleteListByParams
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteListByParams(CreditApproveAllotRule record) {
        String ruleType = record.getRuleType();
        String brId = record.getBrId();
        String reviewId = record.getReviewId();
        return creditApproveAllotRuleMapper.deleteByPrimaryKey(ruleType, brId, reviewId);
    }

    /**
     * @方法名称: selectReviewIdByBrId
     * @方法描述: 根据机构id查询评审id(未请假)
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CreditApproveAllotRule> selectReviewIdByBrId(String brId) {
        String currDateStr = (new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA)).format(new Date());
        List<CreditApproveAllotRule> result = creditApproveAllotRuleMapper.selectReviewIdByBrId(brId, currDateStr);
        return result;
    }

    /**
     * @方法名称: selectGroupInfoByRuleType
     * @方法描述: 根据规则类型查询当前机构所属规则类型内所有配置数据信息(未请假)
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CreditApproveAllotRule> selectGroupInfoByRuleType(String brId) {
        String ruleType = null;
        List<CreditApproveAllotRule> list2 = null;
        String currDateStr = (new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA)).format(new Date());
        List<CreditApproveAllotRule> list = creditApproveAllotRuleMapper.selectReviewIdByBrId(brId, null);
        if (list.size() > 0) {
            if (!"".equals(list.get(0).getRuleType()) && list.get(0).getRuleType() != null) {
                ruleType = list.get(0).getRuleType();
                list2 = creditApproveAllotRuleMapper.selectGroupInfoByRuleType(ruleType, currDateStr);
            }
        }
        return list2;
    }

    /**
     * @方法名称: show
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CreditApproveAllotRule show(CreditApproveAllotRule record) {
        String ruleType = null;
        String brId = null;
        String reviewId = null;
        CreditApproveAllotRule creditApproveAllotRule = new CreditApproveAllotRule();
        if (record == null) {
            ruleType = record.getRuleType();
            brId = record.getBrId();
            reviewId = record.getReviewId();
            creditApproveAllotRule = creditApproveAllotRuleMapper.selectByPrimaryKey(ruleType, brId, reviewId);
        }
        return creditApproveAllotRule;
    }

}
