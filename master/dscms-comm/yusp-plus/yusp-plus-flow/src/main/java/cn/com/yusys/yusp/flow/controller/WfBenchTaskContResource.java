package cn.com.yusys.yusp.flow.controller;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.domain.vo.ResultTodoOrderVo;
import cn.com.yusys.yusp.flow.domain.dto.ResultInstanceTodoDto;
import cn.com.yusys.yusp.flow.service.WorkFlowCustomBenchService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

//import cn.com.yusys.yusp.flow.service.core.WorkflowBenchInterface;
//import cn.com.yusys.yusp.flow.util.WorkFlowUtil;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/6/816:34
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
@RestController
@RequestMapping("/api/custom/bench")
public class WfBenchTaskContResource {

    @Autowired
    private WorkFlowCustomBenchService workFlowCustomBenchService;

    /**
     * 统计总条数
     * @param queryModel 查询参数
     * @return
     */
    @PostMapping({"/count"})
    protected ResultDto<Map<String,Integer>> todo(@RequestBody QueryModel queryModel) {
        Map<String,Integer> result = workFlowCustomBenchService.queryFlowCont(queryModel);
        return new ResultDto(result);
    }

    private String getCurrentUserId(QueryModel queryModel) {
        Object userIdT = queryModel.getCondition().get("currentUserId");
        if (null == userIdT) {
            userIdT = queryModel.getCondition().get("userId");
        }
        String userId = userIdT == null ? null : userIdT.toString();
//        userId = WorkFlowUtil.getCurrentUserId(userId);
        return userId;
    }

    /**
     * 查询我的打回
     * @param queryModel 查询参数
     * @return
     */
    @PostMapping({"/callback"})
    protected ResultDto<List<ResultInstanceTodoDto>> callback(@RequestBody QueryModel queryModel) {
        queryModel.getCondition().put("userId", this.getCurrentUserId(queryModel));
        queryModel.setSort("u.start_time desc");
        return new ResultDto(workFlowCustomBenchService.getInstanceInfoUserCallback(queryModel));
    }

    /**
     * 获取实例或
     * @param requestMap
     * @return
     */
    @PostMapping("/instanceOrders")
    protected ResultDto<List<ResultTodoOrderVo>> todoOrdersInUser(@RequestBody Map<String, String> requestMap){
        String instanceIds = null;
        String flowStarter = null;
        if(requestMap.containsKey("instanceIds")){
            instanceIds = requestMap.get("instanceIds");
        }

        if(requestMap.containsKey("flowStarter")){
            flowStarter = requestMap.get("flowStarter");
        }

        if(StringUtils.isBlank(instanceIds) && StringUtils.isBlank(flowStarter)){
            throw BizException.error(null, "999999", "根据流程实例号或者发起人获取实例在处理人中的排序时缺失查询条件！");
        }
        List<ResultTodoOrderVo> result = workFlowCustomBenchService.getOrderInUserTodos(instanceIds, flowStarter);
        return new ResultDto<>(result);
    }

    /**
     * 查询客户在途业务
     * @param cusId 客户编号
     * @return
     */
    @PostMapping({"/querycusflow"})
    protected ResultDto<List<NWfInstance>> queryCusFlow(@RequestParam String cusId) {
        List<NWfInstance> result = workFlowCustomBenchService.queryCusFlow(cusId);
        return new ResultDto(result);
    }

    /**
     * 查询客户在途业务,不包括退回到发起的
     * @param cusId 客户编号
     * @return
     */
    @PostMapping({"/querycusflownotincludereturnback"})
    protected ResultDto<List<NWfInstance>> queryCusFlowNotIncludeReturnBack(@RequestParam String cusId) {
        List<NWfInstance> result = workFlowCustomBenchService.queryCusFlowNotIncludeReturnBack(cusId);
        return new ResultDto(result);
    }

    /**
     * 统计指定业务类型指定人员的待办条数
     * @param queryModel 查询参数
     * @return
     */
    @PostMapping({"/biztypes/count"})
    protected ResultDto<Integer> bizTypeTodo(@RequestBody QueryModel queryModel) {
        Integer result = workFlowCustomBenchService.queryFlowContByBizType(queryModel);
        return new ResultDto(result);
    }
}
