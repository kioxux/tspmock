package cn.com.yusys.yusp.flow.repository.mapper;

import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.flow.domain.NWfInstance;
import cn.com.yusys.yusp.flow.domain.NWfMettingParamDto;
import cn.com.yusys.yusp.flow.domain.dto.ResultInstanceTodoDto;
import cn.com.yusys.yusp.flow.domain.dto.ResultMeettingDto;
import cn.com.yusys.yusp.flow.domain.dto.ResultVoteListDto;
import cn.com.yusys.yusp.flow.domain.dto.ResultVoteUserDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceTodoWithParamDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

@Service("WorkflowBenchMapperExt")
public interface WorkflowBenchMapperExt {

	/**
	 * 用户待办实例查询
	 * @param instanceId
	 * @return
	 */
	ResultInstanceTodoDto getInstanceInfoByInstanceId(String instanceId);

	/**
	 * 用户待办实例查询
	 * @param queryModel
	 * @return
	 */
	List<ResultInstanceTodoDto> getInstanceInfoUserTodo(QueryModel queryModel);
	
	/**
	 * 用户已办查询
	 * @param queryModel
	 * @return
	 */
	List<ResultInstanceTodoDto> getInstanceInfoUserDone(QueryModel queryModel);

	/**
	 * 发起人办理中查询
	 * @param queryModel
	 * @return
	 */
	List<ResultInstanceTodoDto> getStarterDoingInstance(QueryModel queryModel);

	/**
	 * 用户办结查询
	 * @param queryModel
	 * @return
	 */
	List<ResultInstanceTodoDto> getInstanceInfoUserHis(QueryModel queryModel);
	
	/**
	 * 我委托的待办
	 * @param queryModel
	 * @return
	 */
	List<ResultInstanceTodoDto> getInstanceInfoUserEntrust(QueryModel queryModel);
	
	/**
	 * 抄送给我的待办
	 * @param queryModel
	 * @return
	 */
	List<ResultInstanceTodoDto> getInstanceInfoUserCopy(QueryModel queryModel);
	
	/**
	 * 用户待办实例查询(含业务字段)
	 * @param queryModel
	 * @return
	 */
	List<ResultInstanceTodoWithParamDto> getInstanceInfoUserTodoWithParam(QueryModel queryModel);

	/**
	 * 根据业务类型查询用户待办
	 * @param bizTypes 业务申请类型,多个以逗号分隔
	 */
	List<ResultInstanceTodoDto> getTodosByBizTypes(String bizTypes);

	/**
	 * 扩展流程监控-办理中实例-排除加急实例
	 * @param model
	 * @return
	 */
	List<ResultInstanceTodoDto> cusmonitorTodo(QueryModel model);

	/**
	 * 待办查询 移动OA
	 * @param queryModel
	 * @return
	 */
	List<cn.com.yusys.yusp.flow.dto.result.ResultInstanceTodoDto> getInstanceInfoUserTodoForOa(QueryModel queryModel);

	/**
	 * 贷审会会议列表改造
	 * @param model
	 * @return
	 */

	List<ResultMeettingDto> myStartList(QueryModel model);

	/**
	 * 贷审会会议投票列表改造
	 * @param model
	 * @return
	 */
	List<ResultVoteListDto> myOpList(QueryModel model);

	/**
	 * 获取会议参与投票人
	 * @param map
	 * @return
	 */

	List<ResultVoteUserDto> selectVoteMemByMeetting(Map map);

	int updateAllSubDataByMeetId(Map map);

	int updateMeetStatusUnderThreeDiscuss(Map map);

	int updateFlowDataByParam(Map map);
}