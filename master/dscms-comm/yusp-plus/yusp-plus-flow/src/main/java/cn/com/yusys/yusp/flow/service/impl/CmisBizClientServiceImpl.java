package cn.com.yusys.yusp.flow.service.impl;
import cn.com.yusys.yusp.flow.service.CmisBizClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * cmis-biz服务对外提供服务接口实现类
 * @author monchi
 * @date 2020-12-19 11:50:00
 */
@Component
public class CmisBizClientServiceImpl implements CmisBizClientService {

    private static final Logger logger = LoggerFactory.getLogger(CmisBizClientService.class);

    @Override
    public Integer selectBySernoAndCusId(Map map) {
        return null;
    }
}
