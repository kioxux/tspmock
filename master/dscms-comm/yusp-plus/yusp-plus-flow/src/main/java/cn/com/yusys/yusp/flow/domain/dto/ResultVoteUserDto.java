package cn.com.yusys.yusp.flow.domain.dto;


import java.io.Serializable;

public class ResultVoteUserDto implements Serializable {
    private String userId;
    private String userName;
    private String isCheckBiz;

    private static final long serialVersionUID = 1L;

    public ResultVoteUserDto() {
    }

    public String getIsCheckBiz() { return this.isCheckBiz; }

    public void setIsCheckBiz(String isCheckBiz) { this.isCheckBiz = isCheckBiz == null ? null : isCheckBiz; }

    public String getUserName() { return this.userName; }

    public void setUserName(String userName) { this.userName = userName == null ? null : userName; }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
