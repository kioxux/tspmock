package cn.com.yusys.yusp.flow.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class WfAdminSmOrgVo {
    private String orgId;
    private String orgCode;
    private String orgName;
    private Integer orgLevel;
    private String orgSts;
    private String instuId;
    private String instuName;
    private String instuSts;
    private String upOrgId;
    private String upOrgName;
    private String upOrgCode;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastChgDt;
    private String lastChgUsr;
    private String lastChgName;

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Integer getOrgLevel() {
        return orgLevel;
    }

    public void setOrgLevel(Integer orgLevel) {
        this.orgLevel = orgLevel;
    }

    public String getOrgSts() {
        return orgSts;
    }

    public void setOrgSts(String orgSts) {
        this.orgSts = orgSts;
    }

    public String getInstuId() {
        return instuId;
    }

    public void setInstuId(String instuId) {
        this.instuId = instuId;
    }

    public String getInstuName() {
        return instuName;
    }

    public void setInstuName(String instuName) {
        this.instuName = instuName;
    }

    public String getInstuSts() {
        return instuSts;
    }

    public void setInstuSts(String instuSts) {
        this.instuSts = instuSts;
    }

    public String getUpOrgId() {
        return upOrgId;
    }

    public void setUpOrgId(String upOrgId) {
        this.upOrgId = upOrgId;
    }

    public String getUpOrgName() {
        return upOrgName;
    }

    public void setUpOrgName(String upOrgName) {
        this.upOrgName = upOrgName;
    }

    public String getUpOrgCode() {
        return upOrgCode;
    }

    public void setUpOrgCode(String upOrgCode) {
        this.upOrgCode = upOrgCode;
    }

    public Date getLastChgDt() {
        return lastChgDt;
    }

    public void setLastChgDt(Date lastChgDt) {
        this.lastChgDt = lastChgDt;
    }

    public String getLastChgUsr() {
        return lastChgUsr;
    }

    public void setLastChgUsr(String lastChgUsr) {
        this.lastChgUsr = lastChgUsr;
    }

    public String getLastChgName() {
        return lastChgName;
    }

    public void setLastChgName(String lastChgName) {
        this.lastChgName = lastChgName;
    }
}
