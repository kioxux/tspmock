package cn.com.yusys.yusp.flow.ext;

import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.flow.dto.WFUserDto;
import cn.com.yusys.yusp.flow.service.core.WorkflowOrgInterface;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("SameOrgUserFZH04Impl")
public class SameOrgUserFZH04Impl extends StudioUserInterface{
    /**
     * 用户信息获取服务
     */
    @Autowired
    private WorkflowOrgInterface userService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    private static final Log log = LogFactory.getLog(SameOrgUserFZH04Impl.class);
    @Override
    public int getOrder() {
        return 2;
    }

    @Override
    public String desc() {
        return "分支机构征信查询初审岗(本机构或上级机构)";
    }

    @Override
    public String key() {
        return "SameOrgUserFZH04Impl";
    }

    @Override
    public String orgId() {
        return null;
    }

    @Override
    public List<String> customUser(String instanceId, String orgId, String systemId) {
        List<String> result = new ArrayList<>();
        String dutyId = "FZH04";
        if(null != orgId && null != orgId){
            List<WFUserDto> datas = userService.getUsersByOrgId(systemId, orgId);
            List<WFUserDto> userDutyDtos = userService.getUsersByDutyId(systemId, dutyId);
            if(null==datas) {
                log.error("查询同机构【"+orgId+"】下办理人员为空！");
            }
            for (WFUserDto userOrgdata : datas) {
                for (WFUserDto userDutyDto : userDutyDtos) {
                    if (userOrgdata.getUserId().equals(userDutyDto.getUserId())) {
                        result.add(userOrgdata.getUserId());
                    }
                }
            }
            if(result.size() == 0){
                log.info("同机构【"+orgId+"】下，办理人员匹配不上，查询上一机构人员");
                AdminSmOrgDto adminSmOrgDto = adminSmOrgService.getByOrgCode(orgId).getData();
                if (adminSmOrgDto != null && StringUtils.isNotEmpty(adminSmOrgDto.getUpOrgId())){
                    datas = userService.getUsersByOrgId(systemId, adminSmOrgDto.getUpOrgId());
                    if(CollectionUtils.isEmpty(datas)) {
                        log.error("查询上一机构【" + adminSmOrgDto.getUpOrgId() + "】下办理人员为空！");
                    }
                    if(CollectionUtils.nonEmpty(datas)) {
                        log.info("上一机构【"+adminSmOrgDto.getUpOrgId()+"】下，办理人员匹配");
                        for (WFUserDto userOrgdata : datas) {
                            for (WFUserDto userDutyDto : userDutyDtos) {
                                if (userOrgdata.getUserId().equals(userDutyDto.getUserId())) {
                                    result.add(userOrgdata.getUserId());
                                }
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

}
