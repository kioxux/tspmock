/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package cn.com.yusys.yusp.job.core.utils;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description: 常量
 * @author lty
 * @date 2021/3/1　　
 */
public class Constant {

    public enum ScheduleStatus {
        /**
         * 正常
         */
        NORMAL(0),
        /**
         * 暂停
         */
        PAUSE(1);

        private int value;

        ScheduleStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }


}
