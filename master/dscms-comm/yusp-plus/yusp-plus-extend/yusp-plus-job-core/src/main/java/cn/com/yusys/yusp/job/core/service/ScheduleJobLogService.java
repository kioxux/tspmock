/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package cn.com.yusys.yusp.job.core.service;

import cn.com.yusys.yusp.job.core.entity.ScheduleJobLogEntity;
import cn.com.yusys.yusp.job.core.query.ScheduleJobLogQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description: 定时任务日志接口
 * @author lty
 * @date 2021/2/25　　
 */
public interface ScheduleJobLogService extends IService<ScheduleJobLogEntity> {

	Page queryJobLogPage(ScheduleJobLogQuery scheduleJobLogQuery);
	
}
