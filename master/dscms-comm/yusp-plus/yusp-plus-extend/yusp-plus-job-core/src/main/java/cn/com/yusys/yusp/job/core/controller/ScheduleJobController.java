/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package cn.com.yusys.yusp.job.core.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.job.core.entity.ScheduleJobEntity;
import cn.com.yusys.yusp.job.core.query.ScheduleJobIdQuery;
import cn.com.yusys.yusp.job.core.query.ScheduleJobQuery;
import cn.com.yusys.yusp.job.core.service.ScheduleJobService;
import cn.com.yusys.yusp.job.core.utils.SubStringUtils;
import cn.com.yusys.yusp.job.core.utils.ValidatorUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description: quartz的调度controller
 * @author lty
 * @date 2021/3/1　　
 */
@RestController
@RequestMapping("/api/schedule")
public class ScheduleJobController {
	@Autowired
	private ScheduleJobService scheduleJobService;
	
	/**
	 * 定时任务列表
	 */
	@GetMapping("/list")
	public ResultDto list(ScheduleJobQuery scheduleJobQuery){
		Page page = scheduleJobService.queryPage(scheduleJobQuery);

		return ResultDto.success(page);
	}
	
	/**
	 * 定时任务信息
	 */
	@GetMapping("/info/{jobId}")
	public ResultDto info(@PathVariable("jobId") Long jobId){
		ScheduleJobEntity schedule = scheduleJobService.getById(jobId);

		return ResultDto.success(schedule);
	}
	
	/**
	 * 保存定时任务
	 */
	@PostMapping("/save")
	public ResultDto save(@RequestBody ScheduleJobEntity scheduleJob){
		ValidatorUtils.validateEntity(scheduleJob);
		
		scheduleJobService.saveJob(scheduleJob);
		
		return ResultDto.success();
	}
	
	/**
	 * 修改定时任务
	 */
	@PostMapping("/update")
	public ResultDto update(@RequestBody ScheduleJobEntity scheduleJob){
		ValidatorUtils.validateEntity(scheduleJob);
				
		scheduleJobService.update(scheduleJob);
		
		return ResultDto.success();
	}
	
	/**
	 * 删除定时任务
	 */
	@PostMapping("/delete")
	public ResultDto delete(@RequestBody ScheduleJobIdQuery scheduleJobIdQuery){
		Long[] jobId = SubStringUtils.StringToLongArray(scheduleJobIdQuery.getJobIds());
		scheduleJobService.deleteBatch(jobId);
		
		return ResultDto.success();
	}
	/**
	 * 立即执行任务
	 */
	@PostMapping("/run")
	public ResultDto run(@RequestBody ScheduleJobIdQuery scheduleJobIdQuery){
		Long[] jobId = SubStringUtils.StringToLongArray(scheduleJobIdQuery.getJobIds());
		scheduleJobService.run(jobId);
		
		return ResultDto.success();
	}
	/**
	 * 暂停定时任务
	 */

	@PostMapping("/pause")
	public ResultDto pause(@RequestBody ScheduleJobIdQuery scheduleJobIdQuery){
		Long[] jobId = SubStringUtils.StringToLongArray(scheduleJobIdQuery.getJobIds());
		scheduleJobService.pause(jobId);
		
		return ResultDto.success();
	}
	
	/**
	 * 恢复定时任务
	 */
	@PostMapping("/resume")
	public ResultDto resume(@RequestBody ScheduleJobIdQuery scheduleJobIdQuery){
		Long[] jobId = SubStringUtils.StringToLongArray(scheduleJobIdQuery.getJobIds());
		scheduleJobService.resume(jobId);
		
		return ResultDto.success();
	}

}
