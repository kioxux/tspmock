package cn.com.yusys.yusp.job.core.query;

import lombok.Data;

@Data
public class ScheduleJobIdQuery {
    private String jobIds;
}
