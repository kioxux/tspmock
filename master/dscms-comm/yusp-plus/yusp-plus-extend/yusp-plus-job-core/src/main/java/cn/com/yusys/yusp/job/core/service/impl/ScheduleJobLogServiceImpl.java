/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package cn.com.yusys.yusp.job.core.service.impl;


import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.job.core.dao.ScheduleJobLogDao;
import cn.com.yusys.yusp.job.core.entity.ScheduleJobLogEntity;
import cn.com.yusys.yusp.job.core.query.ScheduleJobLogQuery;
import cn.com.yusys.yusp.job.core.service.ScheduleJobLogService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description: 调度日志的服务实现
 * @author lty
 * @date 2021/3/1　　
 */
@Service("scheduleJobLogService")
public class ScheduleJobLogServiceImpl extends ServiceImpl<ScheduleJobLogDao, ScheduleJobLogEntity> implements ScheduleJobLogService {
	@Override
	public Page queryJobLogPage(ScheduleJobLogQuery scheduleJobLogQuery) {

		String jobId = scheduleJobLogQuery.getJobId();

		//获取分页参数(curPage当前页, limit每页大小)
		Page<ScheduleJobLogEntity> page =new Page<ScheduleJobLogEntity>(scheduleJobLogQuery.getPage(),scheduleJobLogQuery.getSize());
		//创建查询wrapper
		QueryWrapper<ScheduleJobLogEntity> wrapper = new QueryWrapper<>();
		//添加查询条件
		wrapper.eq(!StringUtils.isEmpty(jobId),"job_id",jobId);
		//分页查询
		return this.baseMapper.selectPage(page,wrapper);
	}

}
