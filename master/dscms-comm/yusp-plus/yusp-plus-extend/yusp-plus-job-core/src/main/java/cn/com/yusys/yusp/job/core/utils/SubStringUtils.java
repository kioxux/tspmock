package cn.com.yusys.yusp.job.core.utils;

public class SubStringUtils {

    public static Long[] StringToLongArray(String s){
        String[] temps = s.split(",");
        Long[] sToLong = new Long[temps.length];
        for(int i = 0 ;i<temps.length;i++){
            sToLong[i] = Long.valueOf(temps[i]);
        }
        return sToLong;
    }
}
