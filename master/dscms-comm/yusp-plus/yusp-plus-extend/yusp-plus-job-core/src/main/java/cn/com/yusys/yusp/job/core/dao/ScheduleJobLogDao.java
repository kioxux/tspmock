/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package cn.com.yusys.yusp.job.core.dao;


import cn.com.yusys.yusp.commons.mybatisplus.mapper.BaseMapper;
import cn.com.yusys.yusp.job.core.entity.ScheduleJobLogEntity;

/**
 * @version: 3.1.1-SNAPSHOT
 * @description:  调度日志的dao
 * @author lty
 * @date 2021/3/1　　
 */
public interface ScheduleJobLogDao extends BaseMapper<ScheduleJobLogEntity> {
	
}
