package cn.com.yusys.yusp.job.core.query;

import cn.com.yusys.yusp.common.query.PageQuery;
import cn.com.yusys.yusp.job.core.vo.ScheduleJobVo;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ScheduleJobLogQuery extends PageQuery<ScheduleJobVo> {
    private String jobId;
}
