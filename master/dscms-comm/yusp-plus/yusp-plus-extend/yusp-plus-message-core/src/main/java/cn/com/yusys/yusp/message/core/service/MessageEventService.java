package cn.com.yusys.yusp.message.core.service;

import cn.com.yusys.yusp.message.core.entity.MessageEventEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 消息事件表
 *
 * @author xiaodg@yusys.com.cn
 */
public interface MessageEventService extends IService<MessageEventEntity> {

}

