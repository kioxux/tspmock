package cn.com.yusys.yusp.message.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 消息队列
 *
 * @author xiaodg@yusys.com.cn
 */
@Data
@TableName("message_pool")
public class MessagePoolEntity implements Serializable {

    private static final long serialVersionUID = 550926817447895123L;
    /**
     * 主键
     */
    @TableId
    private String pkNo;
    /**
     * 事件唯一编号
     */
    private String eventNo;
    /**
     * 适用渠道类型
     */
    private String channelType;
    /**
     * 用户码
     */
    private String userNo;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 发送完成时间
     */
    private String sendTime;
    /**
     * 消息等级[小先发]
     */
    private String messageLevel;
    /**
     * 发送状态
     */
    private String state;
    /**
     * 任务id
     */
    private Integer pkHash;
    /**
     * 消息类型
     */
    private String messageType;
    /**
     * 固定发送时间
     * 格式为: yyyy-MM-dd HH:mm:ss
     */
    private String timeStart;
    /**
     * 固定发送结束时间
     * 格式为: yyyy-MM-dd HH:mm:ss
     */
    private String timeEnd;

}
