package cn.com.yusys.yusp.message.core.channel;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.message.core.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.message.core.entity.MessageContentEntity;
import cn.com.yusys.yusp.message.core.entity.MessagePoolEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;


import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.event.TransportAdapter;
import javax.mail.event.TransportEvent;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

/**
 * 发布邮件
 *
 * @author xiaodg@yusys.com.cn
 **/
@Component
@ConditionalOnProperty(name = "spring.mail.enabled", havingValue = "true")
@Slf4j
public class EmailChannelPublisher implements MessageChannelPublisher {

    @Autowired
    private JavaMailSenderImpl mailSender;

    @Value("${spring.mail.username:yusys}")
    private String from;


    @Override
    @Async("emailSendExecutor")
    public CompletableFuture<Boolean> publish(MessagePoolEntity messagePoolEntity, MessageContentEntity messageContentEntity, AdminSmUserEntity userEntity) {
        // email的发送是异步的, 当email的发送状态为EmailSendStat.MESSAGE_DELIVERED时才认为发送成功
        try {
            final EmailSendStat emailSendStat = this.sendMailWithAttached(userEntity.getUserEmail(), messageContentEntity.getEmailTitle(), messageContentEntity.getContent(), null);
            if (!EmailSendStat.MESSAGE_DELIVERED.equals(emailSendStat)) {
                return CompletableFuture.completedFuture(false);
            }
        } catch (Exception e) {
            log.error("发送邮件消息失败: ", e);
            return CompletableFuture.completedFuture(false);
        }
        return CompletableFuture.completedFuture(true);
    }

    /**
     * 发送邮件
     *
     * @param to            邮件接受者 {@code String}
     * @param title         邮件标题 {@code String}
     * @param text          邮件内容,支持符文本 {@code String}
     * @param attachedPaths 附件的文件地址列表 {@link List}
     * @return EmailSendStat 邮件发送状态 {@link EmailSendStat}
     * @throws MessagingException 邮件异常 {@link MessagingException}
     */
    private EmailSendStat sendMailWithAttached(String to, String title, String text, List<String> attachedPaths) throws MessagingException {
        if (!StringUtils.isEmail(to)) {
            log.error("发送邮件消息失败: [{}]不是邮箱地址", to);
            return EmailSendStat.CONFIG_ERROR;
        }
        // 构建邮件内容
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom(from);
        final InternetAddress toAddress = new InternetAddress(to);
        helper.setTo(toAddress);
        helper.setSubject(title);
        helper.setText(text);
        addAttached(attachedPaths, helper);
        try {
            // 将异步发送邮件的操作变成同步等待操作,以获取邮件是否发送成功
            final EmailSendStat.EmailSendStatFuture sendStatFuture = new EmailSendStat.EmailSendStatFuture();
            final Session session = mailSender.getSession();
            final Transport transport = session.getTransport();
            log.info("发送邮件消息: 开始发送邮件消息给: {}", to);
            if (!transport.isConnected()) {
                transport.connect(mailSender.getHost(), mailSender.getPort(), mailSender.getUsername(), mailSender.getPassword());
            }
            transport.addTransportListener(new TransportAdapter() {
                @Override
                public void messageDelivered(TransportEvent e) {
                    sendStatFuture.setStat(EmailSendStat.MESSAGE_DELIVERED);
                }

                @Override
                public void messageNotDelivered(TransportEvent e) {
                    sendStatFuture.setStat(EmailSendStat.MESSAGE_NOT_DELIVERED);
                }

                @Override
                public void messagePartiallyDelivered(TransportEvent e) {
                    sendStatFuture.setStat(EmailSendStat.MESSAGE_PARTIALLY_DELIVERED);
                }
            });
            // 一定要使用transport.sendMessage上面的监听才有效
            transport.sendMessage(mimeMessage, new Address[]{toAddress});
            sendStatFuture.waitForReady();
            return sendStatFuture.getStat();
        } catch (Exception e) {
            log.error("发送邮件消息失败: ", e);
            return EmailSendStat.SEND_ERROR;
        }
    }

    private void addAttached(List<String> attachedPaths, MimeMessageHelper helper) {
        if (Objects.nonNull(attachedPaths) && attachedPaths.size() > 0) {
            attachedPaths.forEach(filePath -> {
                FileSystemResource attached = new FileSystemResource(new File(filePath));
                if (attached.isFile()) {
                    try {
                        helper.addAttachment(Objects.requireNonNull(attached.getFilename()), attached);
                    } catch (Exception e) {
                        log.error("发送邮件消息: 添加附件失败: {}", filePath);
                    }
                } else {
                    log.error("发送邮件消息: 没有找到附件: {}", filePath);
                }
            });
        }
    }
}
