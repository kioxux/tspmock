package cn.com.yusys.yusp.message.core.service.impl;

import cn.com.yusys.yusp.message.core.dao.AdminSmUserRoleRelDao;
import cn.com.yusys.yusp.message.core.entity.AdminSmUserRoleRelEntity;
import cn.com.yusys.yusp.message.core.service.AdminSmUserRoleRelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author terry
 * @email tanrui1@yusys.com.cn
 */

@Service("adminSmUserRoleRelService")
@Slf4j
public class AdminSmUserRoleRelServiceImpl extends ServiceImpl<AdminSmUserRoleRelDao, AdminSmUserRoleRelEntity> implements AdminSmUserRoleRelService {



    @Override
    public Set<String> findUserIdsByRoleId(String roleId) {
        Objects.requireNonNull(roleId);
        return this.lambdaQuery().eq(AdminSmUserRoleRelEntity::getRoleId, roleId).select(AdminSmUserRoleRelEntity::getUserId).list().stream().map(AdminSmUserRoleRelEntity::getUserId).collect(Collectors.toSet());
    }




}