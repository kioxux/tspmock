package cn.com.yusys.yusp.message.core.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;

import cn.com.yusys.yusp.message.core.entity.AdminSmUserEntity;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Date;
import java.util.List;
import java.util.Set;

;

/**
 * 系统用户表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-11-16 15:31:54
 */
public interface AdminSmUserService extends IService<AdminSmUserEntity> {


    Set<String> findUserIdsByOrgId(String orgId);


}

