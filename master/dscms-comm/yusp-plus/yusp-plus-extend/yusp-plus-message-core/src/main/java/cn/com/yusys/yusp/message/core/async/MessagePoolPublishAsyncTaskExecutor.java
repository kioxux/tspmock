package cn.com.yusys.yusp.message.core.async;

import cn.com.yusys.yusp.message.core.channel.MessageChannelPublishHolder;
import cn.com.yusys.yusp.message.core.channel.MessageChannelPublisher;
import cn.com.yusys.yusp.message.core.config.MessageConstants;
import cn.com.yusys.yusp.message.core.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.message.core.entity.MessageContentEntity;
import cn.com.yusys.yusp.message.core.entity.MessagePoolEntity;
import cn.com.yusys.yusp.message.core.service.AdminSmUserService;
import cn.com.yusys.yusp.message.core.service.MessageContentService;
import cn.com.yusys.yusp.message.core.service.MessagePoolHisService;
import cn.com.yusys.yusp.message.core.service.MessagePoolService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * 消息发布任务处理类
 *
 * @author xiaodg@yusys.com.cn
 **/
@Component
@Slf4j
public class MessagePoolPublishAsyncTaskExecutor {

    @Autowired
    private MessagePoolService messagePoolService;

    @Autowired
    private MessageContentService messageContentService;

    @Autowired
    private MessagePoolHisService messagePoolHisService;

    @Autowired
    private MessageChannelPublishHolder messageChannelPublishHolder;

    @Autowired
    private AdminSmUserService adminSmUserService;

    /**
     * 发布消息
     *
     * @param messagePoolEntity 消息队列 {@link MessagePoolEntity}
     */
    @Async
    public void publishMessagePool(@NotNull MessagePoolEntity messagePoolEntity) {
        // 使用主键从数据库查询消息队列
        final String pkNo = messagePoolEntity.getPkNo();
        if (!StringUtils.isEmpty(pkNo)) {
            final MessagePoolEntity messagePoolEntityDb = messagePoolService.lambdaQuery().eq(MessagePoolEntity::getPkNo, pkNo).one();
            if (Objects.isNull(messagePoolEntityDb)) {
                log.error("发布消息: 无法从数据库查询到消息队列: {} ", pkNo);
                return;
            }
            final String channelType = messagePoolEntity.getChannelType();
            final List<MessageContentEntity> messageContentList = messageContentService.getByEventNoAndMessageChannelType(messagePoolEntity.getEventNo(), channelType);
            if (messageContentList.size() <= 0) {
                log.error("发布消息: 无法从数据库查询到消息队列的消息内容, pkNo: {}", pkNo);
                return;
            }
            // 用户消息接收用户信息
            final AdminSmUserEntity userEntity = adminSmUserService.getById(Objects.requireNonNull(messagePoolEntityDb.getUserNo()));
            if (Objects.isNull(userEntity)) {
                log.error("发布消息: 发送消息时没有找到接收的用户, messagePoolEntity pkNo: {}", pkNo);
                return;
            }
            // 获取对应的消息渠道发送实现类
            final MessageChannelPublisher messageChannelPublisher = messageChannelPublishHolder.of(channelType);
            if (Objects.isNull(messageChannelPublisher)) {
                log.error("发布消息: 不支持的消息渠道发送类型[{}]没有对应的消息发送渠道实现类)", channelType);
                return;
            }
            messageContentList.forEach(messageContent -> {
                RetryTemplate retryTemplate = RetryTemplate.builder()
                        .maxAttempts(Optional.ofNullable(messageContent.getSendNum()).orElse(MessageConstants.MESSAGE_SEND_DEFAULT_RETRY_TIMES))
                        // 重试间隔时间, 单位毫秒
                        .fixedBackoff(MessageConstants.MESSAGE_SEND_DEFAULT_FIXED_BACKOFF_MILS)
                        .retryOn(Exception.class)
                        .build();

                try {
                    retryTemplate.execute(context -> {
                        if (!messageChannelPublisher.publish(messagePoolEntityDb, messageContent, userEntity).get()) {
                            String errMsg = "第[" + (context.getRetryCount() + 1) + "]次发送消息[pkNo: " + messagePoolEntityDb.getPkNo() + "]失败";
                            log.error(errMsg);
                            // 抛出异常触发Spring retry重试机制
                            throw new Exception(errMsg);
                        } else {
                            // 记录消息发送历史
                            messagePoolHisService.recordMessagePoolHis(MessageConstants.SEND_END, messagePoolEntity);
                        }
                        return messagePoolEntityDb;
                    }, context -> {
                        // 重试完成失败进入这里, 记录消息发送历史
                        messagePoolHisService.recordMessagePoolHis(MessageConstants.SEND_EXCEPTION, messagePoolEntity);
                        return messagePoolEntityDb;
                    });

                } catch (Exception e) {
                    log.error("发布消息异常: ", e);
                }

            });
        } else {
            log.error("收到不存在的消息队列: {}", messagePoolEntity);
        }
    }
}
