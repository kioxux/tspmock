package cn.com.yusys.yusp.message.core.config;

/**
 * 消息中心-业务常量
 *
 * @author xiaodg@yusys.com.cn
 **/
public interface MessageConstants {

    /**
     * 消息等级，最高
     */
    String MESSAGE_LEVEL_HIGH = "5";

    /**
     * 消息等级,最低
     */
    String MESSAGE_LEVEL_LOW = "1";

    /**
     * 免打扰发送消息
     */
    String DO_NOT_DISTURB_MODE = "1";

    /**
     * 非免打扰模式
     */
    String NON_DO_NOT_DISTURB_MODE = "0";

    /**
     * 消息默认延迟发送秒数, 默认为：0
     */
    long MESSAGE_DEFAULT_DELAY_SEND_SECONDS = 0;

    /**
     * 消息发送开始时间
     * 从消息得参数中获取
     */
    String MESSAGE_PARAM_START_TIME = "_startTime_";

    /**
     * 消息发送结束时间
     * 从消息得参数中获取
     */
    String MESSAGE_PARAM_END_TIME = "_endTime_";

    /**
     * 消息状态: 发送中
     */
    String SENDING = "R";

    /**
     * 消息状态: 发送完成
     */
    String SEND_END = "S";

    /**
     * 消息状态: 发送异常
     */
    String SEND_EXCEPTION = "F";

    /**
     * 消息渠道发送类Bean名称后缀
     */
    String MESSAGE_CHANNEL_PUBLISHER_SUFFIX = "ChannelPublisher";

    /**
     * 消息池表消息事件编号以E开头
     */
    String MESSAGE_EVENT_NO_PREFIX = "E";

    /**
     * 消息池表消息主键编号以P开头
     */
    String MESSAGE_PK_NO_PREFIX = "P";

    /**
     * 消息发送默认重试次数
     */
    int MESSAGE_SEND_DEFAULT_RETRY_TIMES = 0;

    /**
     * 消息发送重试间隔, 单位: 毫秒
     */
    int MESSAGE_SEND_DEFAULT_FIXED_BACKOFF_MILS = 1000;

    /**
     * 消息订阅: 关系订阅 - 上级机构人员
     */
    String MESSAGE_SUBSCRIBE_REL_UP_LEVEL_ORG = "uplevel";

    /**
     * 消息订阅: 关系订阅 - 同级机构人员
     */
    String MESSAGE_SUBSCRIBE_REL_SAME_LEVEL_ORG = "sameevel";
}
