package cn.com.yusys.yusp.message.core.service;

import cn.com.yusys.yusp.message.core.entity.AdminSmUserRoleRelEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Set;

/**
 * 用户角色关联表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-11-18 18:06:35
 */
public interface AdminSmUserRoleRelService extends IService<AdminSmUserRoleRelEntity> {



    Set<String> findUserIdsByRoleId(String roleId);


}

