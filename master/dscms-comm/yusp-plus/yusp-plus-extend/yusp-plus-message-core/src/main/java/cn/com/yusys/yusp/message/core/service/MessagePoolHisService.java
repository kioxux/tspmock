package cn.com.yusys.yusp.message.core.service;

import cn.com.yusys.yusp.message.core.entity.MessagePoolEntity;
import cn.com.yusys.yusp.message.core.entity.MessagePoolHisEntity;
import cn.com.yusys.yusp.message.core.query.MessagePoolHisPageQuery;
import cn.com.yusys.yusp.message.core.vo.MessagePoolHisVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 消息池历史表
 *
 * @author xiaodg@yusys.com.cn
 */
public interface MessagePoolHisService extends IService<MessagePoolHisEntity> {

    /**
     * 分页查询消息历史
     *
     * @param messagePoolHisQuery 消息历史查询对象 {@link MessagePoolHisPageQuery}
     * @return {@link IPage<MessagePoolHisVo>}
     */
    IPage<MessagePoolHisVo> queryPage(MessagePoolHisPageQuery messagePoolHisQuery);


    /**
     * 将消息队列记录进入消息历史
     *
     * @param sendState         消息发送状态 {@code String}
     * @param messagePoolEntity 消息队列 {@link MessagePoolEntity}
     */
    void recordMessagePoolHis(String sendState, MessagePoolEntity messagePoolEntity);
}

