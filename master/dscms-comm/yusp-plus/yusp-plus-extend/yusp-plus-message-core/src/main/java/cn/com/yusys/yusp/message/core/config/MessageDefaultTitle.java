package cn.com.yusys.yusp.message.core.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 消息没有标题时的默认配置
 *
 * @author xiaodg@yusys.com.cn
 **/
@Component
@Getter
public class MessageDefaultTitle {

    /**
     * 当消息转系统公告的时候没有标题时的默认标题
     */
    @Value("${yusp.message.default-message-title:No Title}")
    private String defaultMessageTitle;
}
