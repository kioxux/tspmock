package cn.com.yusys.yusp.message.core.channel;

import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.message.core.config.MessageDefaultTitle;
import cn.com.yusys.yusp.message.core.entity.AdminSmNoticeEntity;
import cn.com.yusys.yusp.message.core.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.message.core.entity.MessageContentEntity;
import cn.com.yusys.yusp.message.core.entity.MessagePoolEntity;
import cn.com.yusys.yusp.message.core.service.AdminSmNoticeService;
import cn.com.yusys.yusp.message.core.util.MessageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

/**
 * 发布系统消息
 *
 * @author xiaodg@yusys.com.cn
 **/
@Component
@Slf4j
public class SystemChannelPublisher implements MessageChannelPublisher {

    @Autowired
    private AdminSmNoticeService noticeService;

    @Autowired
    MessageDefaultTitle messageDefaultTitle;

    @Override
    @Async
    public CompletableFuture<Boolean> publish(MessagePoolEntity messagePoolEntity, MessageContentEntity messageContentEntity, AdminSmUserEntity userEntity) {
        // 将消息转为系统公告保存
        final AdminSmNoticeEntity adminSmNoticeEntity = new AdminSmNoticeEntity();
        adminSmNoticeEntity.setNoticeTitle(MessageUtils.requireNonNullAndEmptyString(messageContentEntity.getEmailTitle(), () -> messageDefaultTitle.getDefaultMessageTitle()));
//        adminSmNoticeEntity.setNoticeContent(messageContentEntity.getContent());
        adminSmNoticeEntity.setPubTime(DateUtils.formatDateTimeByDef());
        adminSmNoticeEntity.setPubUserId(messagePoolEntity.getUserNo());
        try {
            return CompletableFuture.completedFuture(noticeService.save(adminSmNoticeEntity));
        } catch (Exception e) {
            log.error("发送系统消息: 系统消息转公告失败, ", e);
            return CompletableFuture.completedFuture(false);
        }
    }

}
