package cn.com.yusys.yusp.message.core.memorybinder;

import cn.com.yusys.yusp.message.core.channel.YuMessageChannel;
import cn.com.yusys.yusp.message.core.memorybinder.provisioners.MemoryMessageBinderProvisioner;
import org.springframework.cloud.stream.binder.AbstractMessageChannelBinder;
import org.springframework.cloud.stream.binder.ConsumerProperties;
import org.springframework.cloud.stream.binder.ProducerProperties;
import org.springframework.cloud.stream.provisioning.ConsumerDestination;
import org.springframework.cloud.stream.provisioning.ProducerDestination;
import org.springframework.integration.core.MessageProducer;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

/**
 * 内存模式消息绑定器
 *
 * @author xiaodg@yusys.com.cn
 */
public class MemoryMessageBinder extends AbstractMessageChannelBinder<ConsumerProperties, ProducerProperties, MemoryMessageBinderProvisioner> {

    /**
     * 内存模式消息绑定器
     *
     * @param headersToEmbed       嵌入头部的标签
     * @param provisioningProvider 内存消息绑定器供应者 {@link MemoryMessageBinderProvisioner}
     */
    public MemoryMessageBinder(String[] headersToEmbed, MemoryMessageBinderProvisioner provisioningProvider) {
        super(headersToEmbed, provisioningProvider);
    }

    @Override
    protected MessageHandler createProducerMessageHandler(final ProducerDestination destination, final ProducerProperties producerProperties, final MessageChannel errorChannel) {
        return message -> {
        };
    }

    /**
     * 创建消息消费目的地
     *
     * @param destination 绑定通道{@link YuMessageChannel#MESSAGE_CHANNEL_INPUT}
     * @param group       分组
     * @param properties  参数
     * @return {@link MessageProducer}
     */
    @Override
    protected MessageProducer createConsumerEndpoint(final ConsumerDestination destination, final String group, final ConsumerProperties properties) {
        return new MemoryMessageProducer(destination);
    }

}
