package cn.com.yusys.yusp.message.core.service.impl;


import cn.com.yusys.yusp.message.core.dao.AdminSmUserDutyRelDao;
import cn.com.yusys.yusp.message.core.entity.AdminSmUserDutyRelEntity;
import cn.com.yusys.yusp.message.core.service.AdminSmUserDutyRelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author terry
 * @email tanrui1@yusys.com.cn
 */
@Service("adminSmUserDutyRelService")
@Slf4j
public class AdminSmUserDutyRelServiceImpl extends ServiceImpl<AdminSmUserDutyRelDao, AdminSmUserDutyRelEntity> implements AdminSmUserDutyRelService {


    @Override
    public Set<String> findUserIdsByDutyId(String dutyId) {
        Objects.requireNonNull(dutyId);
        return this.lambdaQuery().eq(AdminSmUserDutyRelEntity::getDutyId, dutyId).select(AdminSmUserDutyRelEntity::getUserId).list().stream().map(AdminSmUserDutyRelEntity::getUserId).collect(Collectors.toSet());
    }



}