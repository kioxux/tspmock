package cn.com.yusys.yusp.message.core.config;

import cn.com.yusys.yusp.message.core.channel.YuMessageChannel;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 是否启用消息服务的配置类
 * <p>
 *
 * @author xiaodg@yusys.com.cn
 **/
@ComponentScan("cn.com.yusys.yusp.message")
@MapperScan("cn.com.yusys.yusp.message.dao")
@EnableBinding(YuMessageChannel.class)
@EnableAsync(proxyTargetClass = true)
@EnableRetry
@Configuration
public class EnableMessageServiceConfig {
}
