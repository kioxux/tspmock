package cn.com.yusys.yusp.message.core.query;

import cn.com.yusys.yusp.message.core.entity.MessagePoolHisEntity;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 消息历史f分页查询对象
 *
 * @author xiaodg@yusys.com.cn
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MessagePoolHisPageQuery extends Page<MessagePoolHisEntity> {

    private static final long serialVersionUID = 3574500419259550913L;
    /**
     * 用户码
     */
    private String userNo;
    /**
     * 发送状态
     */
    private String state;
    /**
     * 发送时间
     */
    private String sendTime;

}
