package cn.com.yusys.yusp.message.core.channel;

/**
 * Email发送状态
 *
 * @author xiaodg@yusys.com.cn
 **/
public enum EmailSendStat {
    /**
     * 邮件发送的相关配置错误
     */
    CONFIG_ERROR,
    /**
     * 邮件发送初始化状态
     */
    INITIAL,
    /**
     * 邮件发送完毕
     */
    MESSAGE_DELIVERED,
    /**
     * 邮件未发送
     */
    MESSAGE_NOT_DELIVERED,
    /**
     * 邮件部分发送
     */
    MESSAGE_PARTIALLY_DELIVERED,
    /**
     * 发送失败
     */
    SEND_ERROR;

    /**
     * 邮件发送状态
     */
    public static class EmailSendStatFuture {
        private EmailSendStat stat = EmailSendStat.INITIAL;

        synchronized void waitForReady() throws InterruptedException {
            if (stat == EmailSendStat.INITIAL) {
                // 设置5秒超时
                wait(10 * 1000);
            }
        }

        synchronized EmailSendStat getStat() {
            return stat;
        }

        synchronized void setStat(EmailSendStat newState) {
            stat = newState;
            notifyAll();
        }
    }
}
