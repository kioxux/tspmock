package cn.com.yusys.yusp.message.core.startup;

import cn.com.yusys.yusp.message.core.config.MessageConstants;
import cn.com.yusys.yusp.message.core.config.PkHash;
import cn.com.yusys.yusp.message.core.entity.MessageTempEntity;
import cn.com.yusys.yusp.message.core.service.MessagePoolService;
import cn.com.yusys.yusp.message.core.service.MessageSendService;
import cn.com.yusys.yusp.message.core.service.MessageTempService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 程序启动后从数据库加载消息队列进行发送处理
 *
 * @author xiaodg@yusys.com.cn
 **/
@Component
@Slf4j
public class MessageApplicationStartUpRunner implements CommandLineRunner {

    @Autowired
    private MessagePoolService messagePoolService;

    @Autowired
    private MessageTempService messageTempService;

    @Autowired
    private MessageSendService messageSendService;

    @Autowired
    private PkHash pkHash;

    /**
     * 程序启动后从数据库加载消息队列进行发送处理
     */
    @Override
    public void run(String... args) {
        final List<MessageTempEntity> messageTemps = messageTempService.list();
        messagePoolService.findByPkHashRange(pkHash.getPkHashMin(), pkHash.getPkHashMax())
                .parallelStream()
                .forEach(messagePoolEntity -> {
                    final List<MessageTempEntity> matchedMessageTypes = messageTemps.stream().filter(messageTemp -> messageTemp.getMessageType().equalsIgnoreCase(messagePoolEntity.getMessageType()) && messageTemp.getChannelType().equalsIgnoreCase(messagePoolEntity.getChannelType())).collect(Collectors.toList());
                    messageSendService.sendMessagePoolEntityToBind(messagePoolEntity, matchedMessageTypes.size() > 0 ? matchedMessageTypes.get(0).getIsTime() : MessageConstants.NON_DO_NOT_DISTURB_MODE);
                });
    }
}
