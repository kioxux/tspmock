package cn.com.yusys.yusp.message.core.async;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class MessagePoolAsyncTaskExecutorConfig {
    private static final Logger logger = LoggerFactory.getLogger(MessagePoolAsyncTaskExecutorConfig.class);

    @Bean(name = "messagePoolAsyncTaskExecutor")
    public ThreadPoolTaskExecutor threadPoolConfig() {
        int corePoolSize = 30;
        int maxPoolSize = 200;
        int queueCapacity = 200;
        int keepAliveSeconds = 30;
        logger.info("初始化[MessagePoolAsyncTaskExecutor]配置开始");

        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();// 线程池对象
        //线程最小数量
        logger.info("初始化[MessagePoolAsyncTaskExecutor]配置项中线程最小数量为[{}]", corePoolSize);
        threadPoolTaskExecutor.setCorePoolSize(corePoolSize);
        //线程最大数量
        logger.info("初始化[MessagePoolAsyncTaskExecutor]配置项中线程最大数量为[{}]", maxPoolSize);
        threadPoolTaskExecutor.setMaxPoolSize(maxPoolSize);
        //等待队列长度
        logger.info("初始化[MessagePoolAsyncTaskExecutor]配置项中等待队列长度为[{}]", queueCapacity);
        threadPoolTaskExecutor.setQueueCapacity(queueCapacity);
        //空闲时间
        logger.info("初始化[MessagePoolAsyncTaskExecutor]配置项中空闲时间为[{}]", keepAliveSeconds);
        threadPoolTaskExecutor.setKeepAliveSeconds(keepAliveSeconds);
        threadPoolTaskExecutor.setThreadNamePrefix("[MessagePoolAsyncTaskExecutor]");
        threadPoolTaskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        logger.info("初始化[MessagePoolAsyncTaskExecutor]配置结束");
        return threadPoolTaskExecutor;
    }
}
