package cn.com.yusys.yusp.message.core.vo;

import cn.com.yusys.yusp.message.core.constant.AvailableStateEnum;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;


/**
 * 机构树节点数据库实体
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-11-27 18:06:35
 */
@Data
@TableName("admin_sm_org")
@NoArgsConstructor
public class AdminSmOrgTreeNodeBo {

    /**
     * 机构id
     */
    @TableId
    private String orgId;

    /**
     * 机构名称
     */
    private String orgName;

    /**
     * 机构码
     */
    private String orgCode;
    /**
     * 金融机构id
     */
    private String instuId;
    /**
     * 机构层级
     */
    private Integer orgLevel;
    /**
     * 机构类型
     */
    //private String orgType;

    /**
     * 状态
     */
    private AvailableStateEnum orgSts;


    /**
     * 上级机构id
     */
    private String upOrgId;
    /**
     * 最后修改时间
     */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;


    private List<AdminSmOrgTreeNodeBo> children;
}
