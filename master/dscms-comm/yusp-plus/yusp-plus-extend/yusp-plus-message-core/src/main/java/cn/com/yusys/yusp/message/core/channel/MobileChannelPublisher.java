package cn.com.yusys.yusp.message.core.channel;


import cn.com.yusys.yusp.message.core.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.message.core.entity.MessageContentEntity;
import cn.com.yusys.yusp.message.core.entity.MessagePoolEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

/**
 * 发布手机短信消息
 * <p>
 * 需要使用者结合短信平台实现
 *
 * @author xiaodg@yusys.com.cn
 **/
@Component
@Slf4j
public class MobileChannelPublisher implements MessageChannelPublisher {


    @Override
    @Async
    public CompletableFuture<Boolean> publish(MessagePoolEntity messagePoolEntity, MessageContentEntity messageContentEntity, AdminSmUserEntity userEntity) {
        log.info("发送短信消息: 短信消息发送成功, 接受用户ID: {}, 消息内容: {}", userEntity.getUserId(), messageContentEntity.getContent());
        return CompletableFuture.completedFuture(true);
    }

}
