package cn.com.yusys.yusp.message.core.dao;

import cn.com.yusys.yusp.message.core.entity.MessageEventEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 消息事件
 *
 * @author xiaodg@yusys.com.cn
 */

public interface MessageEventDao extends BaseMapper<MessageEventEntity> {

}
