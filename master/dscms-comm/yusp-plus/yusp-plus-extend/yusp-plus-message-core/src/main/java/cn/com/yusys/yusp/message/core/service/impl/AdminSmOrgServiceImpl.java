package cn.com.yusys.yusp.message.core.service.impl;

import cn.com.yusys.yusp.commons.util.Asserts;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.message.core.dao.AdminSmOrgDao;
import cn.com.yusys.yusp.message.core.entity.AdminSmOrgEntity;
import cn.com.yusys.yusp.message.core.service.AdminSmOrgService;
import cn.com.yusys.yusp.message.core.vo.AdminSmOrgTreeNodeBo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * 系统机构表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-11-27 18:06:35
 */
@Service("adminSmOrgService")
@Slf4j
public class AdminSmOrgServiceImpl extends ServiceImpl<AdminSmOrgDao, AdminSmOrgEntity> implements AdminSmOrgService {





    @Override
    public Set<AdminSmOrgTreeNodeBo> getAllAncestryOrgs(String orgId) {
        if(StringUtils.isEmpty(orgId)){
//            orgId = SessionUtils.getUserOrganizationId();
        }
        Asserts.notEmpty(orgId,"orgId can not be empty!");
        AdminSmOrgTreeNodeBo leaf=this.baseMapper.getAllAncestryOrgs(orgId);//这里的children属性其实是当前节点的父级节点列表
        Set<AdminSmOrgTreeNodeBo> res=new HashSet<>();
        addToResult(leaf,res);
        res.forEach((item)->item.setChildren(null));
        return res;
    }
    private void addToResult(AdminSmOrgTreeNodeBo currentNode,Set<AdminSmOrgTreeNodeBo> res){
        res.add(currentNode);
        List<AdminSmOrgTreeNodeBo> list=currentNode.getChildren();
        if(Objects.nonNull(list)&&list.size()>0){
            addToResult(list.get(0),res);
        }
    }



    @Override
    public Set<AdminSmOrgEntity> getSiblingOrgs(String orgId) {
        if(StringUtils.isEmpty(orgId)){
//            orgId = SessionUtils.getUserOrganizationId();
        }
        AdminSmOrgEntity self=getById(orgId);
        LambdaQueryWrapper<AdminSmOrgEntity> wrapper=new QueryWrapper<AdminSmOrgEntity>().lambda();
        wrapper.eq(AdminSmOrgEntity::getUpOrgId,self.getUpOrgId());
        List<AdminSmOrgEntity> siblingOrgs=this.baseMapper.selectList(wrapper);
        Asserts.nonEmpty(siblingOrgs,"orgId:"+self.getUpOrgId()+" does not exist!");
        return new HashSet<>(siblingOrgs);
    }






    public AdminSmOrgEntity getById(String orgId){
        Asserts.notEmpty(orgId,"orgId can not be empty!");
        AdminSmOrgEntity res=super.getById(orgId);
        Asserts.nonNull(res,"orgId:"+orgId+" does not exist!");
        return res;
    }





















}