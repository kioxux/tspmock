package cn.com.yusys.yusp.message.core.service;

import cn.com.yusys.yusp.message.core.entity.MessageContentEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 消息发送具体内容表
 *
 * @author xiaodg@yusys.com.cn
 */
public interface MessageContentService extends IService<MessageContentEntity> {

    /**
     * 根据事件编号和消息渠道查询消息内容
     *
     * @param eventNo            事件编号
     * @param messageChannelType 消息渠道类型
     * @return {@link List<MessageContentEntity>}
     */
    List<MessageContentEntity> getByEventNoAndMessageChannelType(String eventNo, String messageChannelType);
}

