package cn.com.yusys.yusp.message.core.dao;

import cn.com.yusys.yusp.commons.mybatisplus.mapper.BaseMapper;
import cn.com.yusys.yusp.message.core.entity.AdminSmOrgEntity;
import cn.com.yusys.yusp.message.core.vo.AdminSmOrgTreeNodeBo;

/**
 * @Classname AdminSmOrgDao
 * @Description
 * @Date 2020/11/18 23:35
 * @Created by wujp4@yusys.com.cn
 * @Updated by tanrui1@yusys.com.cn
 */

public interface AdminSmOrgDao extends BaseMapper<AdminSmOrgEntity> {


    AdminSmOrgTreeNodeBo getAllAncestryOrgs(String orgId);
}
