package cn.com.yusys.yusp.message.core.service.impl;

import cn.com.yusys.yusp.message.core.dao.AdminSmNoticeDao;
import cn.com.yusys.yusp.message.core.entity.AdminSmNoticeEntity;
import cn.com.yusys.yusp.message.core.service.AdminSmNoticeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@Service("adminSmNoticeService")
public class AdminSmNoticeServiceImpl extends ServiceImpl<AdminSmNoticeDao, AdminSmNoticeEntity> implements AdminSmNoticeService {


}

