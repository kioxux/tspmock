package cn.com.yusys.yusp.message.core.service;

import cn.com.yusys.yusp.message.core.entity.AdminSmUserDutyRelEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Set;

/**
 * 用户角色关联表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-12-01 21:55:19
 */
public interface AdminSmUserDutyRelService extends IService<AdminSmUserDutyRelEntity> {


    Set<String> findUserIdsByDutyId(String dutyId);



}

