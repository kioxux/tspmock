package cn.com.yusys.yusp.message.core.query;

import cn.com.yusys.yusp.message.core.entity.MessageTypeEntity;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 消息类型查询对象
 *
 * @author xiaodg@yusys.com.cn
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MessageTypePage extends Page<MessageTypeEntity> {
    private static final long serialVersionUID = 1799067875211563109L;

    /**
     * 消息类型
     */
    private String templateType;

    /**
     * 消息类型描述
     */
    private String messageDesc;
}
