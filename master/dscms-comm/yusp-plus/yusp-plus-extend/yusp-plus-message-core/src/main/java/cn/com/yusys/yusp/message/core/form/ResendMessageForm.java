package cn.com.yusys.yusp.message.core.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 消息重发数据对象
 *
 * @author xiaodg@yusys.com.cn
 **/
@Data
public class ResendMessageForm {
    /**
     * 消息历史主键
     * 为兼容之前得接口，pkNos中得pkNo以逗号分隔
     */
    @NotBlank(message = "消息历史主键不能为空")
    @Size(max = 600, message = "消息历史主键过长")
    private String pkNos;
}
