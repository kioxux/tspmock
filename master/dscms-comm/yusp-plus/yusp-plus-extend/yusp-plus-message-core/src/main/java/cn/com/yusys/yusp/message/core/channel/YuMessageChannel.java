package cn.com.yusys.yusp.message.core.channel;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * Binder消息通道
 *
 * @author xiaodg@yusys.com.cn
 **/
public interface YuMessageChannel {
    /**
     * 发送消息通道名称
     * 如果是RabbitMQ,为Exchange名称
     */
    String MESSAGE_CHANNEL_OUTPUT = "message-channel-output";

    /**
     * 接收消息通道名称
     * 如果是RabbitMQ,为Exchange名称
     */
    String MESSAGE_CHANNEL_INPUT = "message-channel-input";


    /**
     * 自定义消息接收通道
     *
     * @return {@link MessageChannel}
     */
    @Input(YuMessageChannel.MESSAGE_CHANNEL_INPUT)
    SubscribableChannel input();

    /**
     * 自定义消息发送通道
     *
     * @return {@link MessageChannel}
     */
    @Output(YuMessageChannel.MESSAGE_CHANNEL_OUTPUT)
    MessageChannel output();
}
