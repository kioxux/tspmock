package cn.com.yusys.yusp.message.core.service;

import cn.com.yusys.yusp.message.core.entity.AdminSmOrgEntity;
import cn.com.yusys.yusp.message.core.vo.AdminSmOrgTreeNodeBo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Set;

/**
 * 系统机构表
 *
 * @author terry
 * @email tanrui1@yusys.com.cn
 * @date 2020-11-27 18:06:35
 */
public interface AdminSmOrgService extends IService<AdminSmOrgEntity> {


    Set<AdminSmOrgTreeNodeBo> getAllAncestryOrgs(String orgId);

    Set<AdminSmOrgEntity> getSiblingOrgs(String orgId);


}

