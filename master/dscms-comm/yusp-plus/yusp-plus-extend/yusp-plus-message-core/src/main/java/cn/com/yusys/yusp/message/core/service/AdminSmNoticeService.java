package cn.com.yusys.yusp.message.core.service;


import cn.com.yusys.yusp.message.core.entity.AdminSmNoticeEntity;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * 系统公告表
 *
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-15 13:04:49
 */
public interface AdminSmNoticeService extends IService<AdminSmNoticeEntity> {

}

