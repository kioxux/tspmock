package cn.com.yusys.yusp.message.core.entity;


import cn.com.yusys.yusp.message.core.constant.AvailableStateEnum;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.conditions.update.Update;
import com.baomidou.mybatisplus.core.injector.methods.Insert;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * 系统机构表
 * 
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-18 18:06:35
 */
@Data
@TableName("admin_sm_org")
@NoArgsConstructor
public class AdminSmOrgEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 记录编号
	 */
	@TableId(type = IdType.UUID)
	@NotBlank(groups = Update.class,message="orgId can not be empty!")
	private String orgId;
	/**
	 * 金融机构编号
	 */
	private String instuId;
	/**
	 * 机构代码
	 */
	@NotBlank(groups = Insert.class,message="orgCode can not be empty!")
	private String orgCode;
	/**
	 * 机构名称
	 */
	@NotBlank(groups = Insert.class,message="orgName can not be empty!")
	private String orgName;
	/**
	 * 上级机构记录编号
	 */
	@NotBlank(groups = Insert.class,message="upOrgId can not be empty!")
	private String upOrgId;
	/**
	 * 机构层级
	 */
	private Integer orgLevel;
	/**
	 * 地址
	 */
	private String orgAddr;
	/**
	 * 邮编
	 */
	@Size(max = 6,message = "邮编接受最长6位")
	private String zipCde;
	/**
	 * 联系电话
	 */
	private String contTel;
	/**
	 * 联系人
	 */
	private String contUsr;
	/**
	 * 状态：对应字典项=NORM_STS A：启用 I：停用 W：待启用
	 */
	private AvailableStateEnum orgSts;
	/**
	 * 最新变更用户
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
	/**
	 * 最新变更时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;


}
