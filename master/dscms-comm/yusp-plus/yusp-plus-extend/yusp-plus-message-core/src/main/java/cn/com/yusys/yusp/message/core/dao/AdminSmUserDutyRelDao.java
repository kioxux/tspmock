package cn.com.yusys.yusp.message.core.dao;

import cn.com.yusys.yusp.message.core.entity.AdminSmUserDutyRelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户角色关联表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-01 21:55:19
 */

public interface AdminSmUserDutyRelDao extends BaseMapper<AdminSmUserDutyRelEntity> {
	
}
