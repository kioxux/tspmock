package cn.com.yusys.yusp.message.core.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 消息渠道类型校验注解
 *
 * @author xiaodg@yusys.com.cn
 */
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MessageChannelTypeValidator.class)
public @interface MessageChannelType {
    String message() default "错误的消息渠道类型";

    /**
     * 分组
     *
     * @return {@code Class<?>[]}
     */
    Class<?>[] groups() default {};

    /**
     * 内容
     *
     * @return {@code Class<? extends Payload>[]}
     */
    Class<? extends Payload>[] payload() default {};
}
