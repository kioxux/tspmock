package cn.com.yusys.yusp.message.core.annotation;

import cn.com.yusys.yusp.message.core.config.EnableMessageServiceConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启用消息服务注解
 *
 * @author xiaodg@yusys.com.cn
 **/
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({EnableMessageServiceConfig.class})
@Configuration
public @interface EnableMessageService {
}
