package cn.com.yusys.yusp.message.core.service;

import cn.com.yusys.yusp.message.core.entity.MessagePoolEntity;
import cn.com.yusys.yusp.message.core.query.MessagePoolPageQuery;
import cn.com.yusys.yusp.message.core.vo.MessagePoolVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 消息队列
 *
 * @author xiaodg@yusys.com.cn
 */
public interface MessagePoolService extends IService<MessagePoolEntity> {

    /**
     * 分页查询消息队列
     *
     * @param messagePoolQuery 消息队列查询对象 {@link MessagePoolPageQuery}
     * @return {@link IPage<MessagePoolVo>}
     */
    IPage<MessagePoolVo> queryPage(MessagePoolPageQuery messagePoolQuery);

    /**
     * 根据pkHash范围查询
     *
     * @param pkHashMin pkHash区间最小值
     * @param pkHashMax pkHash区间最大值
     * @return {@link List<MessagePoolEntity>}
     */
    List<MessagePoolEntity> findByPkHashRange(int pkHashMin, int pkHashMax);
}

