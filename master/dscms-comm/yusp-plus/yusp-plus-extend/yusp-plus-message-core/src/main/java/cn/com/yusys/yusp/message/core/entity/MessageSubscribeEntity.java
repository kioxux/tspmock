package cn.com.yusys.yusp.message.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 消息订阅
 *
 * @author xiaodg@yusys.com.cn
 */
@Data
@TableName("message_subscribe")
@AllArgsConstructor
@NoArgsConstructor
public class MessageSubscribeEntity implements Serializable {

    private static final long serialVersionUID = -6091643438543305892L;
    /**
     * 渠道类型
     */
    @TableId
    private String channelType;
    /**
     * 消息类型
     */
    private String messageType;
    /**
     * 订阅类型[U R O G]
     */
    private String subscribeType;
    /**
     * 最后编辑人
     */
    private String opUserNo;
    /**
     * 订阅类型对应值
     */
    private String subscribeValue;

}
