package cn.com.yusys.yusp.message.core.dao;

import cn.com.yusys.yusp.message.core.entity.AdminSmUserRoleRelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户角色关联表
 * 
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-18 18:06:35
 */

public interface AdminSmUserRoleRelDao extends BaseMapper<AdminSmUserRoleRelEntity> {


}
