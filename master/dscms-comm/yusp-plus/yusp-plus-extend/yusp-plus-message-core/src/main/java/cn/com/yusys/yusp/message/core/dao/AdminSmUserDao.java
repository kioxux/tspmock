package cn.com.yusys.yusp.message.core.dao;



import cn.com.yusys.yusp.commons.mybatisplus.mapper.BaseMapper;
import cn.com.yusys.yusp.message.core.entity.AdminSmUserEntity;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 系统用户表
 *
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-16 15:31:54
 */

public interface AdminSmUserDao extends BaseMapper<AdminSmUserEntity> {

}
