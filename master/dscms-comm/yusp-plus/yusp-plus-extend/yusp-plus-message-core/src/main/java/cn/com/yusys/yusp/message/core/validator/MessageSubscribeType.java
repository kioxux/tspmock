package cn.com.yusys.yusp.message.core.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 消息订阅类型校验注解
 *
 * @author xiaodg@yusys.com.cn
 */
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MessageSubscribeTypeValidator.class)
public @interface MessageSubscribeType {
    String message() default "错误的消息订阅类型";

    /**
     * 分组
     *
     * @return {@code Class<?>[]}
     */
    Class<?>[] groups() default {};

    /**
     * 内容
     *
     * @return {@code Class<? extends Payload>[]}
     */
    Class<? extends Payload>[] payload() default {};
}
