package cn.com.yusys.yusp.message.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * 消息发送具体内容
 *
 * @author xiaodg@yusys.com.cn
 */
@Data
@TableName("message_content")
@ToString
public class MessageContentEntity implements Serializable {

    private static final long serialVersionUID = 9076909646453897377L;
    /**
     * 事件唯一编号
     */
    @TableId
    private String eventNo;
    /**
     * 消息类型
     */
    private String messageType;
    /**
     * 适用渠道类型
     */
    private String channelType;
    /**
     * 消息内容
     */
    private String content;
    /**
     * 邮件/系统消息标题
     */
    private String emailTitle;
    /**
     * 异常重发次数
     */
    private Integer sendNum;
    /**
     * 发送开始时间
     */
    private String timeStart;
    /**
     * 发送结束时间
     */
    private String timeEnd;

}
