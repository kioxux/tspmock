package cn.com.yusys.yusp.message.core.entity;


import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.injector.methods.Insert;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户角色关联表
 * 
 * @author wujp4
 * @email wujp4@yusys.com.cn
 * @date 2020-11-18 18:06:35
 */
@Data
@NoArgsConstructor
@TableName("admin_sm_user_role_rel")
public class AdminSmUserRoleRelEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 记录编号
	 */
	@TableId(type = IdType.UUID)
	private String userRoleRelId;
	/**
	 * 用户编号
	 */
	@NotBlank(groups = Insert.class,message="userId can not be empty!")
	private String userId;
	/**
	 * 角色编号
	 */
	@NotBlank(groups = Insert.class,message="roleId can not be empty!")
	private String roleId;

	/**
	 * 最新变更用户
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private String lastChgUsr;
	/**
	 * 最新变更时间
	 */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastChgDt;

	public AdminSmUserRoleRelEntity(@NotBlank(groups = Insert.class, message = "userId can not be empty!") String userId, @NotBlank(groups = Insert.class, message = "roleId can not be empty!") String roleId) {
		this.userId = userId;
		this.roleId = roleId;
	}
}
