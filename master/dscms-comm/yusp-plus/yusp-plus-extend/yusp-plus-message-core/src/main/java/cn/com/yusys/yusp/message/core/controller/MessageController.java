package cn.com.yusys.yusp.message.core.controller;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;

import cn.com.yusys.yusp.message.core.config.ErrorCodes;
import cn.com.yusys.yusp.message.core.config.SignConstants;
import cn.com.yusys.yusp.message.core.entity.MessageTempEntity;
import cn.com.yusys.yusp.message.core.entity.MessageTypeEntity;
import cn.com.yusys.yusp.message.core.enumeration.MessageTempSendTypeEnum;
import cn.com.yusys.yusp.message.core.form.MessageSubscribeForm;
import cn.com.yusys.yusp.message.core.form.MessageTempForm;
import cn.com.yusys.yusp.message.core.form.MessageTypeForm;
import cn.com.yusys.yusp.message.core.form.ResendMessageForm;
import cn.com.yusys.yusp.message.core.query.*;

import cn.com.yusys.yusp.message.core.service.*;
import cn.com.yusys.yusp.message.core.vo.MessagePoolHisVo;
import cn.com.yusys.yusp.message.core.vo.MessagePoolVo;
import cn.com.yusys.yusp.message.core.vo.MessageTempVo;
import cn.com.yusys.yusp.message.core.vo.MessageTypeVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.*;
import java.util.stream.Stream;


/**
 * 消息中心接口（兼容前端接口）
 *
 * @author xiaodg@yusys.com.cn
 */
@RestController
@RequestMapping("/api/template")
@Validated
@Slf4j
public class MessageController {

    @Autowired
    private MessageTypeService messageTypeService;

    @Autowired
    private MessagePoolHisService messagePoolHisService;

    @Autowired
    private MessagePoolService messagePoolService;

    @Autowired
    private MessageTempService messageTempService;

    @Autowired
    private MessageSubscribeService messageSubscribeService;

    @Autowired
    private MessageSendService messageSendService;

    // 消息类型

    /**
     * 分页查询消息类型
     *
     * @param messageTypePage 消息类型分页查询对象 {@link MessageTypePage}
     * @return {@link ResultDto<IPage<MessageTypeVo>>}
     */
    @GetMapping("/getMessageTypeList")
    public ResultDto<IPage<MessageTypeVo>> findMessageTypePage(MessageTypePage messageTypePage) {
        return ResultDto.success(messageTypeService.queryPageWithChannelTypes(messageTypePage));
    }

    /**
     * 保存消息类型
     *
     * @param messageTypeform 消息类型表单 {@link MessageTypeForm}
     * @return {@link ResultDto<MessageTypeVo>}
     */
    @PostMapping("/addMessageType")
    public ResultDto<MessageTypeVo> saveMessageType(@RequestBody @Valid MessageTypeForm messageTypeform) {
        if (Objects.nonNull(messageTypeService.getById(messageTypeform.getMessageType()))) {
            return ResultDto.error(ErrorCodes.BAD_REQUEST_PARAM, "消息类型已存在，请重新输入");
        }
        messageTypeService.save(BeanUtils.beanCopy(messageTypeform, MessageTypeEntity.class));
        // 为了兼容前端接口，故如此返回
        return ResultDto.success("0");
    }

    /**
     * 修改消息类型
     *
     * @param messageTypeform 消息类型表单 {@link MessageTypeForm}
     * @return {@link ResultDto<String>}
     */
    @PostMapping("/editMessageType")
    public ResultDto<String> updateMessageType(@RequestBody @Valid MessageTypeForm messageTypeform) {
        if (Objects.isNull(messageTypeService.getById(messageTypeform.getMessageType()))) {
            return ResultDto.error(ErrorCodes.BAD_REQUEST_PARAM, "不存在的消息类型，请检查");
        }
        messageTypeService.updateById(BeanUtils.beanCopy(messageTypeform, MessageTypeEntity.class));
        // 为了兼容前端接口，故如此返回
        return ResultDto.success("0");
    }

    /**
     * 删除消息类型
     *
     * @param messageType 消息类型 {@code String}
     * @return {@link ResultDto<String>}
     */
    @PostMapping("/deleteMessageType/{messageType}")
    public ResultDto<String> deleteMessageType(@PathVariable @NotBlank(message = "消息类型不能为空") @Size(max = 32, message = "错误的消息类型") String messageType) {
        if (Objects.isNull(messageTypeService.getById(messageType))) {
            return ResultDto.error(ErrorCodes.BAD_REQUEST_PARAM, "不存在的消息类型，请检查");
        }
        messageTypeService.removeById(messageType);
        return ResultDto.success("删除成功");
    }

    // 消息队列

    /**
     * 分页查询消息历史
     *
     * @param condition 消息队列查询对象 {@link MessagePoolPageQuery}
     * @return {@link ResultDto<IPage<MessagePoolVo>>}
     */
    @GetMapping("/queryMessagePool")
    public ResultDto<IPage<MessagePoolVo>> findMessagePoolPage(@RequestParam String condition) {
        return ResultDto.success(messagePoolService.queryPage(ObjectMapperUtils.toObject(condition, MessagePoolPageQuery.class)));
    }

    // 消息历史

    /**
     * 分页查询消息历史
     *
     * @param condition 消息历史查询对象 {@link MessagePoolHisPageQuery}
     * @return {@link ResultDto<IPage<MessagePoolHisVo>>}
     */
    @GetMapping("/queryMessageResult")
    public ResultDto<IPage<MessagePoolHisVo>> findMessageHisPage(@RequestParam String condition) {
        return ResultDto.success(messagePoolHisService.queryPage(ObjectMapperUtils.toObject(condition, MessagePoolHisPageQuery.class)));
    }

    /**
     * 获取消息模板
     *
     * @param messageTempQuery 消息模板查询对象 {@link MessageTempQuery}
     * @return {@link ResultDto<MessageTempVo>}
     */
    @PostMapping("/getTemplateInfo")
    public ResultDto<MessageTempVo> getTemplateInfo(@RequestBody MessageTempQuery messageTempQuery) {
        // 兼容前端，当没有查询到时返回一个空对象
        return ResultDto.success(messageTempService.queryBy(messageTempQuery).stream().findFirst().orElse(new MessageTempVo()));
    }

    /**
     * 新增或修改 消息模板
     *
     * @param messageTempForm 消息模板表单 {@link MessageTempForm}
     * @return {@link ResultDto<Void>}
     */
    @PostMapping("/addOrUpdateTemplate")
    public ResultDto<String> addOrUpdateTemplate(@RequestBody @Valid MessageTempForm messageTempForm) {
        // 为了兼容前端，成功时，响应数据为1
        return (messageTempService.saveOrUpdateMessageTemp(BeanUtils.beanCopy(messageTempForm, MessageTempEntity.class))) ? ResultDto.success("1") : ResultDto.error(ErrorCodes.BAD_REQUEST_PARAM, "操作失败");
    }

    // 消息订阅

    /**
     * 消息订阅分页展示列表
     *
     * @param messageTypePage 消息类型查询对象 {@link MessageTypePage}
     * @return {@link ResultDto<IPage<MessageTypeVo>>}
     */
    @GetMapping("/getChannelSubscribeList")
    public ResultDto<IPage<MessageTypeVo>> getChannelSubscribeList(MessageTypePage messageTypePage) {
        // 与findMessageTypePage方法逻辑一样，只是消息的类型固定为了订阅类型
        messageTypePage.setTemplateType(MessageTempSendTypeEnum.SUBSCRIBE.getType());
        return this.findMessageTypePage(messageTypePage);
    }

    /**
     * 保存消息订阅
     *
     * @param messageSubscribeForm 消息订阅表单 {@link MessageSubscribeForm}
     * @return {@link ResultDto<String>}
     */
    @PostMapping("/saveSubscribe")
    public ResultDto<String> saveSubscribe(@RequestBody @Valid MessageSubscribeForm messageSubscribeForm) {
        // 兼容前端，成功返回数据0
        return messageSubscribeService.saveMessageSubscribe(messageSubscribeForm) ? ResultDto.success("0") : ResultDto.error(ErrorCodes.BAD_REQUEST_PARAM, "操作失败");
    }

    /**
     * 获取订阅值
     *
     * @param messageSubscribeQuery 消息订阅查询对象 {@link MessageSubscribeQuery}
     * @return {@link ResultDto<String>}
     */
    @PostMapping("/getSubscribe")
    public ResultDto<String> getSubscribe(@RequestBody @Valid MessageSubscribeQuery messageSubscribeQuery) {
        return ResultDto.success(messageSubscribeService.findSubscribeValBy(messageSubscribeQuery));
    }

    /**
     * 发送消息测试
     *
     * @param messageType 消息类型 {@code String}
     * @return {@link ResultDto<String>}
     */
    @PostMapping("/sendMessageTest/{messageType}")
    public ResultDto<Integer> sendMessageTest(@PathVariable @NotBlank(message = "消息类型不能为空") @Size(max = 32, message = "错误的消息类型") String messageType) {
        Map<String, String> param = new HashMap<>();
        param.put("orgName", "研发中心");
        param.put("tel", "028-858969878");

        Set<String> receivedUserIds = new HashSet<>();
        receivedUserIds.add("40");

        messageSendService.sendMessage(messageType, receivedUserIds, "40", param);
        //messageSendService.sendMessage(messageType, MessageChannelEnum.EMAIL, receivedUserIds, "40", param);
        return ResultDto.success(0);
    }

    /**
     * 消息重发
     *
     * @param resendMessageForm 需要重发的消息历史主键对象 {@link ResendMessageForm}
     * @return {@link ResultDto<Integer>}
     */
    @PostMapping("sendAgain")
    public ResultDto<Integer> resendMessage(@RequestBody @Valid ResendMessageForm resendMessageForm) {
        Stream.of(resendMessageForm.getPkNos().split(SignConstants.MARK_COMMA)).forEach(messageSendService::resendMessage);
        return ResultDto.success(0);
    }

}
