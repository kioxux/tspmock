package cn.com.yusys.yusp.message.core.enumeration;

/**
 * 消息渠道类别
 *
 * @author xiaodg@yusys.com.cn
 **/
public enum MessageChannelEnum {
    /**
     * 系统消息
     */
    SYSTEM("system"),
    /**
     * 电子邮件
     */
    EMAIL("email"),
    /**
     * 手机
     */
    MOBILE("mobile");

    private final String type;

    MessageChannelEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }
}
