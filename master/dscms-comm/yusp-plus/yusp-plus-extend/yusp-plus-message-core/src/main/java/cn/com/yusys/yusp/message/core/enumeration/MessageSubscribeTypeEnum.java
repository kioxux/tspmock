package cn.com.yusys.yusp.message.core.enumeration;

/**
 * 消息订阅类型
 *
 * @author xiaodg@yusys.com.cn
 **/
public enum MessageSubscribeTypeEnum {
    /**
     * 消息订阅类型-用户
     */
    USER("U"),

    /**
     * 消息订阅类型-岗位
     */
    DUTY("G"),
    /**
     * 消息订阅类型-角色
     */
    ROLE("R"),
    /**
     * 消息订阅类型-机构
     */
    ORG("O"),

    /**
     * 消息订阅类型-关系
     */
    REL("X");

    private final String type;

    MessageSubscribeTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
