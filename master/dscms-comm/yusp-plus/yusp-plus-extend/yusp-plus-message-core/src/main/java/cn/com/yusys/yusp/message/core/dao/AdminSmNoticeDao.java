package cn.com.yusys.yusp.message.core.dao;

import cn.com.yusys.yusp.message.core.entity.AdminSmNoticeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 系统公告表
 * 
 * @author danyb1
 * @email danyb1@yusys.com.cn
 * @date 2020-12-15 13:04:49
 */

public interface AdminSmNoticeDao extends BaseMapper<AdminSmNoticeEntity> {

}
