package cn.com.yusys.yusp.message.core.config;

import cn.com.yusys.yusp.commons.exception.PlatformException;
import cn.com.yusys.yusp.message.core.entity.MessagePoolEntity;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 消息队列分片配置
 * <p>
 * 消息队列对象{@link MessagePoolEntity} 有一个<pre>pkHash</pre>字段，为保证服务重启后只会从数据库加载该服务实例需要处理得消息队列
 * <p>
 * 避免多服务实例重启后多次加载同一个消息队列，造成消息多次发送
 * <p>
 * 注意：pkHashStart，pkHashEnd参数对于每一个服务实例应保证不同，可通过配置注入
 * <p>
 * 实际上的处理是，生成消息队列时，会为<pre>MessagePoolEntity</pre>的<pre>hashPk</pre>按照pkHashStart，pkHashEnd的区间生成一个随机数， 实例启动时，会按照这个随机数区间从数据库加载消息队列
 *
 * @author xiaodg@yusys.com.cn
 **/
@Component
@Getter
public final class PkHash {

    /**
     * pkHash随机数区间最小值， 默认为0
     */
    @Value("${yusp.message.pk-hash-min:0}")
    private int pkHashMin;

    /**
     * pkHash随机数区间最大值
     */
    @Value("${yusp.message.pk-hash-max:50}")
    private int pkHashMax;

    @PostConstruct
    public void init() {
        if (pkHashMin >= pkHashMax) {
            throw new PlatformException("pkHash随机数区间区间配置错误，最小值:" + pkHashMin + "最大值: " + pkHashMax);
        }
    }
}
