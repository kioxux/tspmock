package cn.com.yusys.yusp.message.core.channel;


import cn.com.yusys.yusp.message.core.entity.AdminSmUserEntity;
import cn.com.yusys.yusp.message.core.entity.MessageContentEntity;
import cn.com.yusys.yusp.message.core.entity.MessagePoolEntity;

import java.util.concurrent.CompletableFuture;

/**
 * 消息渠道发布接口
 * <p>
 * 自定义的消息渠道发布接口实现类命名须满足: XyyChannelPublisher
 * 实现类不要重新定义Bean名称, 由{@link MessageChannelPublishHolder}处理
 *
 * @author xiaodg@yusys.com.cn
 **/
public interface MessageChannelPublisher {


    /**
     * 发布消息
     * <p>
     * 发送失败, 抛出异常, 供Spring Retry框架进行重试
     *
     * @param userEntity           用户信息 {@link AdminSmUserEntity}
     * @param messagePoolEntity    消息队列 {@link MessagePoolEntity}
     * @param messageContentEntity 消息内容 {@link MessageContentEntity}
     * @return CompletableFuture<Boolean> 发送是否成功 {@code CompletableFuture<Boolean>}
     */
    CompletableFuture<Boolean> publish(MessagePoolEntity messagePoolEntity, MessageContentEntity messageContentEntity, AdminSmUserEntity userEntity);
}
