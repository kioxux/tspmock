package cn.com.yusys.yusp.message.core.service;


import cn.com.yusys.yusp.message.core.entity.MessageTempEntity;
import cn.com.yusys.yusp.message.core.query.MessageTempQuery;
import cn.com.yusys.yusp.message.core.vo.MessageTempVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 消息模板配置表
 *
 * @author xiaodg@yusys.com.cn
 */
public interface MessageTempService extends IService<MessageTempEntity> {

    /**
     * 根据查询对象查询消息模板
     *
     * @param messageTempQuery 消息模板查询对象 {@link MessageTempQuery}
     * @return {@link List<MessageTempVo>}
     */
    List<MessageTempVo> queryBy(MessageTempQuery messageTempQuery);

    /**
     * 保存或更新消息类型模板
     *
     * @param messageTempEntity {@link MessageTempEntity}
     * @return {@code Boolean}
     */
    Boolean saveOrUpdateMessageTemp(MessageTempEntity messageTempEntity);
}

