package cn.com.yusys.yusp.message.core.service.impl;


import cn.com.yusys.yusp.message.core.dao.MessageContentDao;
import cn.com.yusys.yusp.message.core.entity.MessageContentEntity;
import cn.com.yusys.yusp.message.core.service.MessageContentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 消息内容服务类
 *
 * @author xiaodg@yusys.com.cn
 */
@Service
@Slf4j
public class MessageContentServiceImpl extends ServiceImpl<MessageContentDao, MessageContentEntity> implements MessageContentService {


    @Override
    public List<MessageContentEntity> getByEventNoAndMessageChannelType(String eventNo, String messageChannelType) {
        return this.lambdaQuery()
                .eq(MessageContentEntity::getEventNo, eventNo)
                .eq(MessageContentEntity::getChannelType, messageChannelType)
                .list();
    }
}