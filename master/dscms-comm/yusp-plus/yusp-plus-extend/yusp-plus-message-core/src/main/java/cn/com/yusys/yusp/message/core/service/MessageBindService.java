package cn.com.yusys.yusp.message.core.service;

import cn.com.yusys.yusp.message.core.entity.MessagePoolEntity;
import org.springframework.messaging.handler.annotation.Payload;

/**
 * 消息处理通道服务类
 *
 * @author xiaodg@yusys.com.cn
 **/
public interface MessageBindService {

    /**
     * 发送消息
     *
     * @param messagePoolEntity 消息队列实体 {@link MessagePoolEntity}
     */
    void send(MessagePoolEntity messagePoolEntity);

    /**
     * 发送消息
     *
     * @param messagePoolEntity 消息队列实体 {@link MessagePoolEntity}
     * @param delaySeconds      消息延迟发送的秒数
     */
    void send(MessagePoolEntity messagePoolEntity, Long delaySeconds);

    /**
     * 收到消息
     *
     * @param messagePoolEntity 消息队列实体 {@link MessagePoolEntity}
     */
    void receive(@Payload MessagePoolEntity messagePoolEntity);
}
