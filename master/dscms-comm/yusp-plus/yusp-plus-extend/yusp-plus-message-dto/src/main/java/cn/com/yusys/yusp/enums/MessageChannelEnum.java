package cn.com.yusys.yusp.enums;

/**
 * 消息渠道类别
 *
 * @author liucheng3@yusys.com.cn
 **/
public enum MessageChannelEnum {
    /**
     * 系统消息
     */
    SYSTEM("system"),
    /**
     * 电子邮件
     */
    EMAIL("email"),
    /**
     * 手机短信
     */
    MOBILE("mobile");

    private final String type;

    MessageChannelEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }
}
