package cn.com.yusys.yusp.dto;

import java.io.Serializable;

public class ReceivedUserDto implements Serializable {
    /**
     * 消息接收人类型
     * 1：客户经理
     * 2：借款人
     */
    private String receivedUserType;

    /**
     * 接收人ID
     */
    private String userId;

    /**
     * 接收人电话
     * 仅在消息接收人类型为 2：借款人时，必填；
     */
    private String mobilePhone;

    public String getReceivedUserType() {
        return receivedUserType;
    }

    public void setReceivedUserType(String receivedUserType) {
        this.receivedUserType = receivedUserType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }
}

