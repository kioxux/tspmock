package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * 消息发送实体类
 */
public class MessageSendDto implements Serializable {

    /**
     * 消息类型（一般一个类型对应一个模板；也可以一个类型对应多个消息模板，以便同一消息分不同渠道都发送）
     * 在【系统管理】》【消息配置】》【消息模板】中配置
     */
    private String messageType;


    /**
     * 消息接收人
     */
    private List<ReceivedUserDto> receivedUserList;


    /**
     * 模板参数
     * 例如模板配置为：
     * 您好，您管户下的${cusName}客户申请的${flowName}流程已经审批通过/退回，请关注！
     * 则参数传递为：
     * {
     *     "cusName":"张三",
     *     "flowName":"授信申报流程"
     * }
     *
     */
    private Map<String, String> params;

    /**
     * 发送人员ID， 可为空
     */
    private String sendUserId;


    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public List<ReceivedUserDto> getReceivedUserList() {
        return receivedUserList;
    }

    public void setReceivedUserList(List<ReceivedUserDto> receivedUserList) {
        this.receivedUserList = receivedUserList;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public String getSendUserId() {
        return sendUserId;
    }

    public void setSendUserId(String sendUserId) {
        this.sendUserId = sendUserId;
    }
}