package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.GetUserInfoByDutyCodeDto;
import cn.com.yusys.yusp.dto.MessageSendDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * 消息发送 feign接口
 *
 * @author liucheng3
 * @date 2021-08-13 14:45:35
 */
@FeignClient(name = "yusp-app-oca", path = "/api", fallback = MessageSendServiceProviderHystrix.class)
public interface MessageSendService {
    @PostMapping("/template/sendMessage")
    public ResultDto<Integer> sendMessage(MessageSendDto messageSendDto);


    @PostMapping("/template/sendonlinepldremind")
    public ResultDto<Integer> sendOnlinePldRemind(MessageSendDto messageSendDto);
}