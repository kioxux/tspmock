package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.GetUserInfoByDutyCodeDto;
import cn.com.yusys.yusp.dto.MessageSendDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 消息发送 feign接口调用熔断
 *
 * @author liucheng3
 * @date 2021-05-14 10:06:35
 */
@Component
public class MessageSendServiceProviderHystrix implements MessageSendService {
    private static final Logger logger = LoggerFactory.getLogger(MessageSendServiceProviderHystrix.class);

    @Override
    public ResultDto<Integer> sendMessage(MessageSendDto messageSendDto) {
        logger.error("添加消息发送任务失败，触发熔断！消息内容{}", messageSendDto.toString());
        return null;
    }

    @Override
    public ResultDto<Integer> sendOnlinePldRemind(MessageSendDto messageSendDto) {
        logger.error("添加消息发送任务失败，触发熔断！消息内容{}", messageSendDto.toString());
        return null;
    }

}