### 平台级微服务
统一服务平台，平台级微服务

- yusp-file 文件上传
- yusp-job-admin 分布式定时任务
- yusp-sequence 全局模版序列号

### V3.1.1.20210120.RELEASE
变更信息如下：
1. file文件服务包路径修改，由cn.com.yusys.yusp.admin修改为cn.com.yusys.yusp.file
2. file-core和sequence-core有关与数据库交互的操作由tkmapper修改为使用mybatisplus
3. platform工程适配新版yusp-common工程,各个模块调整
4. file, job-admin, sequence新增starter快速启动模块，引入该模块后即可实时使用和启动该微服务模块
5. sequence生成序列号核心逻辑使用yusp-common-sequence组件实现

### V2.3.1.20200313.RELEASE
1. 修复
  - xxljob在mysql下的bug
  - 文件批量下载后zip不能解压的bug

### V2.3.1.20200313.RELEASE
1. 优化
  - 引入日志模块，优化日志输出
2. 修复
  - 全局序列号catch时未打印对应的日志信息

### V2.2.3.20191224.RELEASE
1. 新增
  - 国际化——日志翻译修改
2. 修复
  - 使用RedisHelper的keys方法代替stringRedisTemplate的keys方法扫描Key
  - 文件服务批量打包下载，里面会有个0k的空文件

### V2.2.2.20190822.RELEASE
1. 修复
  - 在DB2数据库执行器无法自动注册问题
  - 在DB2数据库GLUE模式代码保存失败
  - 在DB2数据库子任务查询数据为空问题

### V2.2.1.20190716.RELEASE
1. 新增
  - 添加yusp-common-actuator-web扩展展示包
2. 优化
  - yusp-sequence优化redis模式下，判断序列是否创建的校验方法
  - yusp-sequence完善接口端的字段校验
  - yusp-sequence添加序列ID只能是大写字母、数字、下划线组成且首字母必须为大写字母的校验
3. 修复
  - yusp-job-admin工程在mysql数据库下执行器无法自动注册
  - yusp-sequence在redis模式下页面配置的最大值不生效的问题
  - yusp-sequence全局序列号，序列ID可重复的问题
  - yusp-sequence序列号删除后，无法对序列号模版配置字段进行编辑的问题

### V2.1.1.20190403.RELEASE更改记录：
- 1：日志优化，减少默认的日志配置，提供精简版日志输出配置
- 2：yusp-license模块移至yusp-common包下，去除循环依赖问题
- 3：子模块工程修改，按照client,dto,core,starter方式提供
- 4：代码优化，部分bug修改
- 5：starter工程配置剔除，统一修改为使用yusp-common中配置，通过文件配置属性控制是否启用
- 6：集成Apollo配置中心
- 7：剔除jhipster代码，剔除多余无用代码，规范部分代码写法
- 8：精简启动类，精简工程依赖包，剔除多余用不到的jar
- 9：修改部分工具类使用yusp-common-base中提供的
- 10：统一配置文件格式，并添加对应配置属性说明