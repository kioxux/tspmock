### 记录当前工程修改历史
记录格式
`### 时间
【新增】jira编号:001,XX功能
【修复】jira编号:002,XX问题

### 20210521
【优化】整序列号服务接口（api全小写）
【规范】按行里日志规范调整日志打印

### 20210427
【优化】与web2.0统一，正确的返回码统一使用code值为0
【规范】按mybatis-plus查询规范，分页查询，不再使用condition map存放查询条件
### 20210414
【修复】修复序列号模板页面列表查询带条件时，均无结果的问题

### 20201210
变更信息如下：
1. file文件服务包路径修改，由cn.com.yusys.yusp.admin修改为cn.com.yusys.yusp.file
2. file-core和sequence-core有关与数据库交互的操作由tkmapper修改为使用mybatisplus
3. platform工程适配新版yusp-common工程,各个模块调整
4. file, job-admin, sequence新增starter快速启动模块，引入该模块后即可实时使用和启动该微服务模块
5. sequence生成序列号核心逻辑使用yusp-common-sequence组件实现

## dev_2.3.X
### 2020-03-02
【修复】 xxlJob 在mysql下出现的删除任务的bug

### 2020-03-02
【修复】 BUG1094:全局序列号组件，创建异常catch后，未打印异常信息

### 2020-01-14
【新增】 日志规范

## dev_2.2.X

### 2019-11-28
【优化】使用RedisHelper的keys方法代替stringRedisTemplate的keys方法扫描Key

### 2019-11-21
【新增】国际化——日志翻译修改

### 2019-09-23
【修复】jira编号：YUSP-644，文件服务批量打包下载，里面会有个0k的空文件

### 2019-08-20
【修复】jira编号:YUSP-619
1.yusp-job-admin工程在DB2数据库执行器无法自动注册
【修复】jira编号:YUSP-618
1.yusp-job-admin工程在DB2数据库GLUE模式代码保存失败
【修复】jira编号:YUSP-635
1.yusp-job-admin工程在DB2数据库子任务查询数据为空问题

### 2019-07-02
【修复】jira编号：YUSP-466、YUSP-467
1. 修复yusp-sequence全局序列号，序列ID可重复的问题
2. 修复yusp-sequence序列号删除后，无法对序列号模版配置字段进行编辑的问题
【优化】yusp-sequence组件
1. 优化redis模式下，判断序列是否创建的校验方法
2. 完善接口端的字段校验
3. 添加序列ID只能是大写字母、数字、下划线组成且首字母必须为大写字母的校验

### 2019-06-12
【修复】jira编号：YUSP-460
1. 修复yusp-sequence在redis模式下页面配置的最大值不生效的问题

### 2019-05-08 
【修复】jira编号：YUSP-450
1. yusp-job-admin工程在mysql数据库下执行器无法自动注册；