package cn.com.yusys.yusp.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * file微服务组件启用类，优化于dev_2.1.x版本<br/>
 *
 * @author tangxb
 * @since 2.1.1
 */
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"cn.com.yusys.yusp"})
public class FileMicroserviceApp {

    public static void main(String[] args) {
        SpringApplication.run(FileMicroserviceApp.class, args);
    }

}