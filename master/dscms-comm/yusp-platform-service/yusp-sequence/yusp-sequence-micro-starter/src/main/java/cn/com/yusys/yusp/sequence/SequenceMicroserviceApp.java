package cn.com.yusys.yusp.sequence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * sequence微服务组件启用类，优化于dev_2.1.x版本<br/>
 *
 * @author tangxb
 * @since 2.1.1
 */
@EnableDiscoveryClient
@SpringBootApplication
public class SequenceMicroserviceApp {

    public static void main(String[] args) {
        SpringApplication.run(SequenceMicroserviceApp.class, args);
    }
}
